<s>
Zemský	zemský	k2eAgInSc1d1
plášť	plášť	k1gInSc1
</s>
<s>
Řez	řez	k1gInSc1
Zemí	zem	k1gFnSc7
od	od	k7c2
jádra	jádro	k1gNnSc2
k	k	k7c3
exosféře	exosféra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levá	levý	k2eAgFnSc1d1
část	část	k1gFnSc1
obrázku	obrázek	k1gInSc2
není	být	k5eNaImIp3nS
ve	v	k7c6
správném	správný	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1
plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
vrstev	vrstva	k1gFnPc2
Země	Země	k1gFnSc2
<g/>
,	,	kIx,
shora	shora	k6eAd1
vymezená	vymezený	k2eAgFnSc1d1
zemskou	zemský	k2eAgFnSc7d1
kůrou	kůra	k1gFnSc7
a	a	k8xC
zespodu	zespodu	k6eAd1
zemským	zemský	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
geofyzikálního	geofyzikální	k2eAgNnSc2d1
i	i	k8xC
geochemického	geochemický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c6
</s>
<s>
svrchní	svrchní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
spodní	spodní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
a	a	k8xC
</s>
<s>
přechodovou	přechodový	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
<g/>
,	,	kIx,
zvanou	zvaný	k2eAgFnSc4d1
také	také	k9
Repettiho	Repetti	k1gMnSc4
diskontinuita	diskontinuita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Většinu	většina	k1gFnSc4
současných	současný	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
o	o	k7c6
plášti	plášť	k1gInSc6
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
podrobnou	podrobný	k2eAgFnSc7d1
analýzou	analýza	k1gFnSc7
příchodů	příchod	k1gInPc2
seismických	seismický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
plášti	plášť	k1gInSc6
probíhá	probíhat	k5eAaImIp3nS
neustále	neustále	k6eAd1
plášťová	plášťový	k2eAgFnSc1d1
konvekce	konvekce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
deskovou	deskový	k2eAgFnSc7d1
tektonikou	tektonika	k1gFnSc7
a	a	k8xC
jejíž	jejíž	k3xOyRp3gInSc4
obraz	obraz	k1gInSc4
můžeme	moct	k5eAaImIp1nP
získat	získat	k5eAaPmF
pomocí	pomocí	k7c2
seismické	seismický	k2eAgFnSc2d1
tomografie	tomografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1
plášť	plášť	k1gInSc1
jako	jako	k8xC,k8xS
celé	celý	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
69	#num#	k4
%	%	kIx~
zemské	zemský	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
a	a	k8xC
84	#num#	k4
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
objemu	objem	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svrchní	svrchní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
</s>
<s>
Svrchní	svrchní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
shora	shora	k6eAd1
<g/>
,	,	kIx,
vně	vně	k6eAd1
ohraničen	ohraničen	k2eAgMnSc1d1
od	od	k7c2
zemské	zemský	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
Mohorovičićovou	Mohorovičićová	k1gFnSc7
diskontinuitou	diskontinuita	k1gFnSc7
(	(	kIx(
<g/>
MOHO	MOHO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
pod	pod	k7c7
oceány	oceán	k1gInPc7
do	do	k7c2
hloubky	hloubka	k1gFnSc2
0	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
km	km	kA
a	a	k8xC
pod	pod	k7c7
kontinenty	kontinent	k1gInPc7
do	do	k7c2
hloubky	hloubka	k1gFnSc2
20	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spodní	spodní	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
(	(	kIx(
<g/>
Repettiho	Repetti	k1gMnSc2
diskontinuita	diskontinuita	k1gFnSc1
<g/>
)	)	kIx)
svrchního	svrchní	k2eAgInSc2d1
pláště	plášť	k1gInSc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
v	v	k7c6
hloubce	hloubka	k1gFnSc6
650	#num#	k4
km	km	kA
a	a	k8xC
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
fázovým	fázový	k2eAgInSc7d1
přechodem	přechod	k1gInSc7
v	v	k7c6
hornině	hornina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
této	tento	k3xDgFnSc2
svrchní	svrchní	k2eAgFnSc2d1
části	část	k1gFnSc2
pláště	plášť	k1gInSc2
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
na	na	k7c4
3,27	3,27	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
pohyb	pohyb	k1gInSc4
nadložních	nadložní	k2eAgFnPc2d1
litosférických	litosférický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
hloubkách	hloubka	k1gFnPc6
okolo	okolo	k7c2
100	#num#	k4
až	až	k9
150	#num#	k4
km	km	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
plastická	plastický	k2eAgFnSc1d1
astenosféra	astenosféra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
svrchním	svrchní	k2eAgInSc6d1
plášti	plášť	k1gInSc6
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
rozlišit	rozlišit	k5eAaPmF
tři	tři	k4xCgFnPc4
oblasti	oblast	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Zóna	zóna	k1gFnSc1
snížené	snížený	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
–	–	k?
astenosféra	astenosféra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
od	od	k7c2
60	#num#	k4
<g/>
–	–	k?
<g/>
250	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevuje	projevovat	k5eAaImIp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zbrzďuje	zbrzďovat	k5eAaImIp3nS
procházející	procházející	k2eAgFnPc4d1
zemětřesné	zemětřesný	k2eAgFnPc4d1
vlny	vlna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
nad	nad	k7c7
prvním	první	k4xOgInSc7
fázovým	fázový	k2eAgInSc7d1
přechodem	přechod	k1gInSc7
–	–	k?
alokována	alokován	k2eAgMnSc4d1
je	být	k5eAaImIp3nS
do	do	k7c2
hloubky	hloubka	k1gFnSc2
cca	cca	kA
400	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
vrstvě	vrstva	k1gFnSc6
rychlost	rychlost	k1gFnSc1
seismických	seismický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
opět	opět	k6eAd1
narůstá	narůstat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
mezi	mezi	k7c7
prvním	první	k4xOgInSc7
a	a	k8xC
druhým	druhý	k4xOgInSc7
fázovým	fázový	k2eAgInSc7d1
přechodem	přechod	k1gInSc7
–	–	k?
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
jako	jako	k9
hloubka	hloubka	k1gFnSc1
rozhraní	rozhraní	k1gNnSc2
mezi	mezi	k7c7
svrchním	svrchní	k2eAgInSc7d1
a	a	k8xC
spodním	spodní	k2eAgInSc7d1
pláštěm	plášť	k1gInSc7
udává	udávat	k5eAaImIp3nS
650	#num#	k4
km	km	kA
±	±	k?
100	#num#	k4
km	km	kA
(	(	kIx(
<g/>
samotné	samotný	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
není	být	k5eNaImIp3nS
sférické	sférický	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
deformované	deformovaný	k2eAgFnSc2d1
díky	díky	k7c3
dynamickým	dynamický	k2eAgInPc3d1
procesům	proces	k1gInPc3
probíhajícím	probíhající	k2eAgInPc3d1
v	v	k7c6
plášti	plášť	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mineralogické	mineralogický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
svrchního	svrchní	k2eAgInSc2d1
pláště	plášť	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
strukturám	struktura	k1gFnPc3
olivínu	olivín	k1gInSc2
<g/>
,	,	kIx,
pyroxenu	pyroxen	k1gInSc2
<g/>
,	,	kIx,
granátu	granát	k1gInSc2
a	a	k8xC
spinelu	spinel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
klesající	klesající	k2eAgFnSc7d1
hloubkou	hloubka	k1gFnSc7
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
tlak	tlak	k1gInSc1
a	a	k8xC
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
změnám	změna	k1gFnPc3
v	v	k7c6
minerálech	minerál	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
olivín	olivín	k1gInSc1
začne	začít	k5eAaPmIp3nS
přeměňovat	přeměňovat	k5eAaImF
na	na	k7c4
spinel	spinel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
stoupajícím	stoupající	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
spinel	spinel	k1gInSc1
dále	daleko	k6eAd2
měnit	měnit	k5eAaImF
na	na	k7c4
perovskit	perovskit	k1gInSc4
a	a	k8xC
magnesiowüstit	magnesiowüstit	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
horninového	horninový	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
plášť	plášť	k1gInSc1
tvořen	tvořit	k5eAaImNgInS
z	z	k7c2
andezitu	andezit	k1gInSc2
<g/>
,	,	kIx,
dacitu	dacit	k1gInSc2
a	a	k8xC
eklogitu	eklogit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
autorů	autor	k1gMnPc2
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
eklogitové	eklogitový	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
mezi	mezi	k7c7
svrchním	svrchní	k2eAgInSc7d1
a	a	k8xC
spodním	spodní	k2eAgInSc7d1
pláštěm	plášť	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
</s>
<s>
Spodní	spodní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
alokován	alokovat	k5eAaImNgInS
od	od	k7c2
hloubky	hloubka	k1gFnSc2
650	#num#	k4
km	km	kA
až	až	k8xS
k	k	k7c3
zemskému	zemský	k2eAgNnSc3d1
jádru	jádro	k1gNnSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
přibližně	přibližně	k6eAd1
do	do	k7c2
hloubky	hloubka	k1gFnSc2
2900	#num#	k4
km	km	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
oddělen	oddělit	k5eAaPmNgInS
Gutenbergovou	Gutenbergův	k2eAgFnSc7d1
diskontinuitou	diskontinuita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
nejspíše	nejspíše	k9
silikáty	silikát	k1gInPc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
obohacen	obohatit	k5eAaPmNgInS
o	o	k7c4
prvky	prvek	k1gInPc4
kobalt	kobalt	k1gInSc4
<g/>
,	,	kIx,
hliník	hliník	k1gInSc4
a	a	k8xC
titan	titan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hustota	hustota	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
zde	zde	k6eAd1
k	k	k7c3
přeměně	přeměna	k1gFnSc3
gama-spinelu	gama-spinel	k1gInSc2
a	a	k8xC
majoritu	majorita	k1gFnSc4
na	na	k7c4
perovskit	perovskit	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
spodním	spodní	k2eAgInSc6d1
plášti	plášť	k1gInSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
jen	jen	k9
velmi	velmi	k6eAd1
málo	málo	k4c4
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlouběji	hluboko	k6eAd2
do	do	k7c2
nitra	nitro	k1gNnSc2
Země	zem	k1gFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
jeví	jevit	k5eAaImIp3nS
spodní	spodní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
jako	jako	k9
chemicky	chemicky	k6eAd1
homogenní	homogenní	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
šíření	šíření	k1gNnSc2
seismických	seismický	k2eAgInPc2d1
P	P	kA
vln	vlna	k1gFnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
14	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plášť	plášť	k1gInSc1
obecně	obecně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
zkoumat	zkoumat	k5eAaImF
jen	jen	k9
pomocí	pomocí	k7c2
kosmochemického	kosmochemický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
xenolitů	xenolit	k1gInPc2
(	(	kIx(
<g/>
uzavřenina	uzavřenina	k1gFnSc1
v	v	k7c6
magmatu	magma	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
magmatů	magmat	k1gInPc2
a	a	k8xC
nebo	nebo	k8xC
pomocí	pomocí	k7c2
petrochemického	petrochemický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Leccos	leccos	k3yInSc1
-	-	kIx~
špalíček	špalíček	k1gInSc4
moudrosti	moudrost	k1gFnSc2
a	a	k8xC
poučení	poučení	k1gNnSc2
-	-	kIx~
zemský	zemský	k2eAgInSc1d1
plášť	plášť	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Země	země	k1gFnSc1
-	-	kIx~
svrchní	svrchní	k2eAgInSc1d1
plášť	plášť	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SIGURDSSON	SIGURDSSON	kA
<g/>
,	,	kIx,
Haraldur	Haraldur	k1gMnSc1
<g/>
;	;	kIx,
JEANLOZ	JEANLOZ	kA
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
<g/>
;	;	kIx,
ASIMOW	ASIMOW	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
D.	D.	kA
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Volcanoes	Volcanoes	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
643140	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Mantle	Mantle	k?
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
a	a	k8xC
Melting	Melting	k1gInSc1
the	the	k?
Mantle	Mantle	k?
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4138985-2	4138985-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
4537	#num#	k4
</s>
