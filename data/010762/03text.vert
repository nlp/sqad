<p>
<s>
Andradit	Andradit	k5eAaImF	Andradit
(	(	kIx(	(
<g/>
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
23	[number]	k4	23
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krychlový	krychlový	k2eAgInSc4d1	krychlový
minerál	minerál	k1gInSc4	minerál
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
granátu	granát	k1gInSc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
Minerál	minerál	k1gInSc1	minerál
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgMnSc1d1	nazván
po	po	k7c6	po
brazilském	brazilský	k2eAgMnSc6d1	brazilský
politikovi	politik	k1gMnSc6	politik
<g/>
,	,	kIx,	,
mineralogovi	mineralog	k1gMnSc6	mineralog
a	a	k8xC	a
básníkovi	básník	k1gMnSc6	básník
Josém	Josý	k1gMnSc6	Josý
Bonifáciu	Bonifácius	k1gMnSc6	Bonifácius
de	de	k?	de
Andrade	Andrad	k1gInSc5	Andrad
e	e	k0	e
Silva	Silva	k1gFnSc1	Silva
(	(	kIx(	(
<g/>
1763	[number]	k4	1763
<g/>
–	–	k?	–
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
v	v	k7c6	v
kontaktně	kontaktně	k6eAd1	kontaktně
metasomatických	metasomatický	k2eAgFnPc6d1	metasomatický
horninách	hornina	k1gFnPc6	hornina
na	na	k7c6	na
styku	styk	k1gInSc6	styk
kyselých	kyselý	k2eAgFnPc2d1	kyselá
vyvřelin	vyvřelina	k1gFnPc2	vyvřelina
s	s	k7c7	s
karbonáty	karbonát	k1gInPc7	karbonát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
krystalických	krystalický	k2eAgFnPc6d1	krystalická
břidlicích	břidlice	k1gFnPc6	břidlice
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
magmatický	magmatický	k2eAgMnSc1d1	magmatický
<g/>
.	.	kIx.	.
</s>
<s>
Druhotně	druhotně	k6eAd1	druhotně
sedimentární	sedimentární	k2eAgFnSc1d1	sedimentární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morfologie	morfologie	k1gFnSc2	morfologie
==	==	k?	==
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
tvoří	tvořit	k5eAaImIp3nP	tvořit
rombododekaedrické	rombododekaedrický	k2eAgInPc1d1	rombododekaedrický
a	a	k8xC	a
hexaoktoedrické	hexaoktoedrický	k2eAgInPc1d1	hexaoktoedrický
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Zrnité	zrnitý	k2eAgInPc1d1	zrnitý
a	a	k8xC	a
kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
agregáty	agregát	k1gInPc1	agregát
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
časté	častý	k2eAgFnPc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náplavech	náplav	k1gInPc6	náplav
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
zrna	zrno	k1gNnPc1	zrno
nebo	nebo	k8xC	nebo
ohlazené	ohlazený	k2eAgInPc1d1	ohlazený
oblázky	oblázek	k1gInPc1	oblázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
6,5	[number]	k4	6,5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
3,8	[number]	k4	3,8
<g/>
–	–	k?	–
<g/>
3,9	[number]	k4	3,9
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
velmi	velmi	k6eAd1	velmi
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
110	[number]	k4	110
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc4	lom
nerovný	rovný	k2eNgInSc4d1	nerovný
<g/>
,	,	kIx,	,
lasturnatý	lasturnatý	k2eAgInSc4d1	lasturnatý
<g/>
,	,	kIx,	,
tříštivý	tříštivý	k2eAgInSc4d1	tříštivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
hedvábný	hedvábný	k2eAgInSc1d1	hedvábný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
až	až	k8xS	až
neprůhledný	průhledný	k2eNgInSc1d1	neprůhledný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
podle	podle	k7c2	podle
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
Ca	ca	kA	ca
23,66	[number]	k4	23,66
%	%	kIx~	%
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
21,98	[number]	k4	21,98
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
16,58	[number]	k4	16,58
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
37,78	[number]	k4	37,78
%	%	kIx~	%
<g/>
,	,	kIx,	,
příměsi	příměs	k1gFnSc2	příměs
Ti	ten	k3xDgMnPc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Lehce	lehko	k6eAd1	lehko
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
<g/>
,	,	kIx,	,
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Polymorfie	polymorfie	k1gFnSc2	polymorfie
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
==	==	k?	==
</s>
</p>
<p>
<s>
Vytváří	vytvářet	k5eAaImIp3nP	vytvářet
dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
andradit	andradit	k5eAaImF	andradit
–	–	k?	–
grosulár	grosulár	k1gInSc4	grosulár
</s>
</p>
<p>
<s>
andradit	andradit	k5eAaImF	andradit
–	–	k?	–
schorlomit	schorlomit	k5eAaPmF	schorlomit
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Topazolit	Topazolit	k1gInSc4	Topazolit
===	===	k?	===
</s>
</p>
<p>
<s>
Topazolit	Topazolit	k1gInSc1	Topazolit
je	být	k5eAaImIp3nS	být
překrásná	překrásný	k2eAgFnSc1d1	překrásná
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
drahokamová	drahokamový	k2eAgFnSc1d1	drahokamová
odrůda	odrůda	k1gFnSc1	odrůda
andraditu	andradita	k1gFnSc4	andradita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vzácnější	vzácný	k2eAgMnSc1d2	vzácnější
než	než	k8xS	než
démantoid	démantoid	k1gInSc1	démantoid
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
krystalů	krystal	k1gInPc2	krystal
dosahující	dosahující	k2eAgFnSc4d1	dosahující
šperkařských	šperkařský	k2eAgFnPc2d1	šperkařská
kvalit	kvalita	k1gFnPc2	kvalita
se	se	k3xPyFc4	se
tolik	tolik	k6eAd1	tolik
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
sbírkovou	sbírkový	k2eAgFnSc4d1	sbírková
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
brazilským	brazilský	k2eAgMnSc7d1	brazilský
státníkem	státník	k1gMnSc7	státník
a	a	k8xC	a
mineralogem	mineralog	k1gMnSc7	mineralog
Josém	Josý	k2eAgMnSc6d1	Josý
Bonifáciu	Bonifácius	k1gMnSc6	Bonifácius
de	de	k?	de
Andrade	Andrad	k1gInSc5	Andrad
e	e	k0	e
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
snažil	snažit	k5eAaImAgMnS	snažit
vylíčit	vylíčit	k5eAaPmF	vylíčit
podobnost	podobnost	k1gFnSc4	podobnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
čirosti	čirost	k1gFnSc6	čirost
<g/>
)	)	kIx)	)
s	s	k7c7	s
topazem	topaz	k1gInSc7	topaz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
barva	barva	k1gFnSc1	barva
bývá	bývat	k5eAaImIp3nS	bývat
medově	medově	k6eAd1	medově
až	až	k9	až
zlatavě	zlatavě	k6eAd1	zlatavě
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
lokality	lokalita	k1gFnPc4	lokalita
výskytu	výskyt	k1gInSc2	výskyt
patří	patřit	k5eAaImIp3nS	patřit
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Namibie	Namibie	k1gFnSc1	Namibie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Démantoid	Démantoid	k1gInSc4	Démantoid
===	===	k?	===
</s>
</p>
<p>
<s>
Démantoid	Démantoid	k1gInSc1	Démantoid
je	být	k5eAaImIp3nS	být
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
mála	málo	k4c2	málo
drahokamových	drahokamový	k2eAgFnPc2d1	drahokamová
odrůd	odrůda	k1gFnPc2	odrůda
méně	málo	k6eAd2	málo
známého	známý	k2eAgInSc2d1	známý
granátu	granát	k1gInSc2	granát
andraditu	andradita	k1gFnSc4	andradita
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
Urale	Ural	k1gInSc6	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
smaragd	smaragd	k1gInSc4	smaragd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
odrůdu	odrůda	k1gFnSc4	odrůda
granátu	granát	k1gInSc2	granát
a	a	k8xC	a
dle	dle	k7c2	dle
vykazujícího	vykazující	k2eAgMnSc2d1	vykazující
velmi	velmi	k6eAd1	velmi
silného	silný	k2eAgInSc2d1	silný
diamantového	diamantový	k2eAgInSc2d1	diamantový
lesku	lesk	k1gInSc2	lesk
připomínající	připomínající	k2eAgInSc1d1	připomínající
samotný	samotný	k2eAgInSc1d1	samotný
diamant	diamant	k1gInSc1	diamant
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
démantoid	démantoid	k1gInSc1	démantoid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zabarven	zabarven	k2eAgInSc1d1	zabarven
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
sytě	sytě	k6eAd1	sytě
zelené	zelený	k2eAgFnPc4d1	zelená
až	až	k8xS	až
žlutozelené	žlutozelený	k2eAgFnPc4d1	žlutozelená
barvy	barva	k1gFnPc4	barva
díky	díky	k7c3	díky
příměsi	příměs	k1gFnSc3	příměs
chromu	chrom	k1gInSc2	chrom
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
nalézán	nalézán	k2eAgMnSc1d1	nalézán
v	v	k7c6	v
náplavech	náplav	k1gInPc6	náplav
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
Uralu	Ural	k1gInSc6	Ural
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Démantoid	Démantoid	k1gInSc1	Démantoid
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
vyhledáváným	vyhledáváný	k2eAgInSc7d1	vyhledáváný
drahokamem	drahokam	k1gInSc7	drahokam
a	a	k8xC	a
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
téměř	téměř	k6eAd1	téměř
vyčerpaným	vyčerpaný	k2eAgNnPc3d1	vyčerpané
nalezištím	naleziště	k1gNnPc3	naleziště
na	na	k7c6	na
Uralu	Ural	k1gInSc6	Ural
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ceněn	ceněn	k2eAgInSc1d1	ceněn
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
jeho	on	k3xPp3gInSc4	on
výrazný	výrazný	k2eAgInSc4d1	výrazný
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
indexu	index	k1gInSc3	index
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
zaměněn	zaměněn	k2eAgInSc1d1	zaměněn
za	za	k7c4	za
Malijský	malijský	k2eAgInSc4d1	malijský
granát	granát	k1gInSc4	granát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Melanit	Melanit	k1gInSc4	Melanit
===	===	k?	===
</s>
</p>
<p>
<s>
Melanit	Melanit	k1gInSc1	Melanit
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
černá	černý	k2eAgFnSc1d1	černá
odrůda	odrůda	k1gFnSc1	odrůda
granátu	granát	k1gInSc2	granát
andraditu	andradita	k1gFnSc4	andradita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
melan	melana	k1gFnPc2	melana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
černé	černá	k1gFnPc4	černá
barvě	barva	k1gFnSc3	barva
může	moct	k5eAaImIp3nS	moct
vděčit	vděčit	k5eAaImF	vděčit
hlavně	hlavně	k9	hlavně
velké	velký	k2eAgFnPc4d1	velká
příměsi	příměs	k1gFnPc4	příměs
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
koncentrace	koncentrace	k1gFnSc2	koncentrace
až	až	k9	až
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	díky	k7c3	díky
titanu	titan	k1gInSc3	titan
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
též	též	k9	též
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
titanový	titanový	k2eAgInSc1d1	titanový
andradit	andradit	k5eAaImF	andradit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgInPc1d1	malý
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
sbírkové	sbírkový	k2eAgInPc4d1	sbírkový
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krystaly	krystal	k1gInPc1	krystal
dostatečně	dostatečně	k6eAd1	dostatečně
veliké	veliký	k2eAgInPc1d1	veliký
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
brousit	brousit	k5eAaImF	brousit
jako	jako	k8xS	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gInSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c4	na
Ti	ten	k3xDgMnPc1	ten
bohatých	bohatý	k2eAgInPc6d1	bohatý
magmatitech	magmatit	k1gInPc6	magmatit
nebo	nebo	k8xC	nebo
skarnech	skarn	k1gInPc6	skarn
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Chihuahua	Chihuahua	k1gFnSc1	Chihuahua
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
nebo	nebo	k8xC	nebo
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Parageneze	parageneze	k1gFnSc2	parageneze
==	==	k?	==
</s>
</p>
<p>
<s>
chlority	chlorit	k1gInPc1	chlorit
<g/>
,	,	kIx,	,
biotit	biotit	k1gInSc1	biotit
<g/>
,	,	kIx,	,
živec	živec	k1gInSc1	živec
<g/>
,	,	kIx,	,
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
vesuvian	vesuvian	k1gInSc1	vesuvian
<g/>
,	,	kIx,	,
epidot	epidot	k1gInSc1	epidot
<g/>
,	,	kIx,	,
spinel	spinel	k1gInSc1	spinel
<g/>
,	,	kIx,	,
kalcit	kalcit	k1gInSc1	kalcit
<g/>
,	,	kIx,	,
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
magnetit	magnetit	k1gInSc1	magnetit
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Technické	technický	k2eAgNnSc1d1	technické
užití	užití	k1gNnSc1	užití
<g/>
:	:	kIx,	:
řezné	řezný	k2eAgInPc1d1	řezný
<g/>
,	,	kIx,	,
brusné	brusný	k2eAgInPc1d1	brusný
a	a	k8xC	a
vrtné	vrtný	k2eAgInPc1d1	vrtný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
některé	některý	k3yIgFnSc2	některý
variety	varieta	k1gFnSc2	varieta
jako	jako	k8xC	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
fasetové	fasetový	k2eAgInPc1d1	fasetový
brusy	brus	k1gInPc1	brus
<g/>
,	,	kIx,	,
kabošony	kabošon	k1gInPc1	kabošon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
minerál	minerál	k1gInSc1	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc4	Česko
–	–	k?	–
Malešov	Malešov	k1gInSc1	Malešov
a	a	k8xC	a
Vlastějovice	Vlastějovice	k1gFnSc1	Vlastějovice
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skarnech	skarn	k1gInPc6	skarn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bludov	Bludov	k1gInSc1	Bludov
(	(	kIx(	(
<g/>
v	v	k7c6	v
erlánech	erlán	k1gInPc6	erlán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
melanit	melanit	k1gInSc1	melanit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Breznička	Breznička	k1gFnSc1	Breznička
<g/>
,	,	kIx,	,
Hnúšťa	Hnúšťa	k1gFnSc1	Hnúšťa
<g/>
,	,	kIx,	,
Kremnica	Kremnica	k1gFnSc1	Kremnica
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
Wurliz	Wurliz	k1gMnSc1	Wurliz
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Rehau	Rehaus	k1gInSc2	Rehaus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Smrčinách	Smrčiny	k1gFnPc6	Smrčiny
<g/>
,	,	kIx,	,
Schwarzenberg	Schwarzenberg	k1gInSc1	Schwarzenberg
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
,	,	kIx,	,
Kaiserstuhl	Kaiserstuhl	k1gFnSc1	Kaiserstuhl
v	v	k7c6	v
Bádensku	Bádensko	k1gNnSc6	Bádensko
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Salzburgu	Salzburg	k1gInSc2	Salzburg
topazolit	topazolit	k5eAaImF	topazolit
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Chibiny	Chibina	k1gFnSc2	Chibina
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
Ural	Ural	k1gInSc1	Ural
–	–	k?	–
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Bobrovka	bobrovka	k1gFnSc1	bobrovka
ve	v	k7c6	v
Sverdlovské	sverdlovský	k2eAgFnSc6d1	Sverdlovská
oblasti	oblast	k1gFnSc6	oblast
andradity	andradita	k1gFnSc2	andradita
použitelné	použitelný	k2eAgFnSc2d1	použitelná
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
</s>
</p>
<p>
<s>
USA	USA	kA	USA
–	–	k?	–
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
Aljaška	Aljaška	k1gFnSc1	Aljaška
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
</p>
<p>
<s>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Dana	Dana	k1gFnSc1	Dana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
system	syst	k1gInSc7	syst
of	of	k?	of
mineralogy	mineralog	k1gMnPc7	mineralog
,	,	kIx,	,
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
th	th	k?	th
edition	edition	k1gInSc1	edition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
437-447	[number]	k4	437-447
</s>
</p>
<p>
<s>
===	===	k?	===
Citace	citace	k1gFnPc1	citace
===	===	k?	===
</s>
</p>
<p>
<s>
DUĎA	DUĎA	kA	DUĎA
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
REJL	REJL	kA	REJL
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
František	František	k1gMnSc1	František
Rejl	Rejl	k1gInSc1	Rejl
<g/>
;	;	kIx,	;
Fotografie	fotografia	k1gFnSc2	fotografia
Dušan	Dušan	k1gMnSc1	Dušan
Slivka	slivka	k1gFnSc1	slivka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
AVENTINUM	AVENTINUM	kA	AVENTINUM
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
520	[number]	k4	520
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7151	[number]	k4	7151
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
458	[number]	k4	458
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minerály	minerál	k1gInPc1	minerál
a	a	k8xC	a
drahokamy	drahokam	k1gInPc1	drahokam
:	:	kIx,	:
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
,	,	kIx,	,
určování	určování	k1gNnSc1	určování
a	a	k8xC	a
sběr	sběr	k1gInSc1	sběr
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Miroslava	Miroslava	k1gFnSc1	Miroslava
Procházková	Procházková	k1gFnSc1	Procházková
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Pavel	Pavel	k1gMnSc1	Pavel
Povondra	Povondra	k1gFnSc1	Povondra
<g/>
;	;	kIx,	;
Obálku	obálka	k1gFnSc4	obálka
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
Jana	Jana	k1gFnSc1	Jana
Šťastná	Šťastná	k1gFnSc1	Šťastná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Euromedia	Euromedium	k1gNnSc2	Euromedium
Group	Group	k1gMnSc1	Group
k.s.	k.s.	k?	k.s.
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
přírodou	příroda	k1gFnSc7	příroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
696	[number]	k4	696
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Kapitola	kapitola	k1gFnSc1	kapitola
Křemičitany	křemičitan	k1gInPc4	křemičitan
<g/>
,	,	kIx,	,
s.	s.	k?	s.
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andradit	Andradit	k1gFnSc2	Andradit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Andradit	Andradit	k1gFnSc1	Andradit
na	na	k7c6	na
webu	web	k1gInSc6	web
Mindat	Mindat	k1gFnSc2	Mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Andradit	Andradit	k1gFnSc1	Andradit
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Andradit	Andradit	k1gFnSc1	Andradit
v	v	k7c6	v
Atlasu	Atlas	k1gInSc6	Atlas
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mineral	Mineral	k1gMnSc1	Mineral
data	datum	k1gNnSc2	datum
publishing	publishing	k1gInSc1	publishing
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
