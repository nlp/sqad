<p>
<s>
Lodi	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
italskyProvincia	italskyProvincia	k1gFnSc1	italskyProvincia
di	di	k?	di
Lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
provincie	provincie	k1gFnSc1	provincie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
provinciemi	provincie	k1gFnPc7	provincie
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Piacenza	Piacenz	k1gMnSc2	Piacenz
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Pavia	Pavium	k1gNnSc2	Pavium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provincie	provincie	k1gFnSc1	provincie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
odtržením	odtržení	k1gNnSc7	odtržení
61	[number]	k4	61
obcí	obec	k1gFnPc2	obec
od	od	k7c2	od
provincie	provincie	k1gFnSc2	provincie
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
</p>
