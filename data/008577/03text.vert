<p>
<s>
Internet	Internet	k1gInSc1	Internet
je	být	k5eAaImIp3nS	být
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
systém	systém	k1gInSc1	systém
propojených	propojený	k2eAgFnPc2d1	propojená
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
síť	síť	k1gFnSc1	síť
sítí	síť	k1gFnPc2	síť
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
počítače	počítač	k1gInPc4	počítač
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
rodiny	rodina	k1gFnSc2	rodina
protokolů	protokol	k1gInPc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
cílem	cíl	k1gInSc7	cíl
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
využívajících	využívající	k2eAgMnPc2d1	využívající
internet	internet	k1gInSc4	internet
je	být	k5eAaImIp3nS	být
bezproblémová	bezproblémový	k2eAgFnSc1d1	bezproblémová
komunikace	komunikace	k1gFnSc1	komunikace
(	(	kIx(	(
<g/>
výměna	výměna	k1gFnSc1	výměna
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
službou	služba	k1gFnSc7	služba
poskytovanou	poskytovaný	k2eAgFnSc7d1	poskytovaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
internetu	internet	k1gInSc2	internet
je	být	k5eAaImIp3nS	být
WWW	WWW	kA	WWW
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
multimédií	multimédium	k1gNnPc2	multimédium
propojených	propojený	k2eAgFnPc2d1	propojená
hypertextovými	hypertextový	k2eAgInPc7d1	hypertextový
odkazy	odkaz	k1gInPc7	odkaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
e-mail	eail	k1gInSc1	e-mail
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
desítky	desítka	k1gFnPc1	desítka
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc4	Internet
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
propojené	propojený	k2eAgFnPc1d1	propojená
počítačové	počítačový	k2eAgFnPc1d1	počítačová
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
jeho	jeho	k3xOp3gInPc4	jeho
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
síťové	síťový	k2eAgInPc4d1	síťový
uzly	uzel	k1gInPc4	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Uzlem	uzel	k1gInSc7	uzel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
specializované	specializovaný	k2eAgNnSc4d1	specializované
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
router	router	k1gInSc1	router
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
počítač	počítač	k1gInSc1	počítač
připojený	připojený	k2eAgInSc1d1	připojený
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodiny	rodina	k1gFnSc2	rodina
protokolů	protokol	k1gInPc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
svoji	svůj	k3xOyFgFnSc4	svůj
IP	IP	kA	IP
adresu	adresa	k1gFnSc4	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
používají	používat	k5eAaImIp3nP	používat
doménová	doménový	k2eAgNnPc4d1	doménové
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
www.wikipedia.org	www.wikipedia.org	k1gMnSc1	www.wikipedia.org
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
internet	internet	k1gInSc1	internet
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
latinské	latinský	k2eAgFnSc2d1	Latinská
<g/>
)	)	kIx)	)
předpony	předpona	k1gFnSc2	předpona
inter	inter	k1gInSc1	inter
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
mezi	mezi	k7c4	mezi
<g/>
)	)	kIx)	)
a	a	k8xC	a
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
net	net	k?	net
(	(	kIx(	(
<g/>
network	network	k1gInPc2	network
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
sítí	síť	k1gFnPc2	síť
připojených	připojený	k2eAgFnPc2d1	připojená
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zobecnění	zobecnění	k1gNnSc3	zobecnění
pojmu	pojem	k1gInSc2	pojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
celou	celý	k2eAgFnSc4d1	celá
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Orientace	orientace	k1gFnSc1	orientace
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
informací	informace	k1gFnPc2	informace
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
do	do	k7c2	do
WWW	WWW	kA	WWW
(	(	kIx(	(
<g/>
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
orientace	orientace	k1gFnSc2	orientace
ve	v	k7c6	v
stránkách	stránka	k1gFnPc6	stránka
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hledáme	hledat	k5eAaImIp1nP	hledat
<g/>
,	,	kIx,	,
používáme	používat	k5eAaImIp1nP	používat
tzv.	tzv.	kA	tzv.
odkazy	odkaz	k1gInPc7	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
službami	služba	k1gFnPc7	služba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
systematicky	systematicky	k6eAd1	systematicky
s	s	k7c7	s
odkazy	odkaz	k1gInPc7	odkaz
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Internetový	internetový	k2eAgInSc1d1	internetový
katalog	katalog	k1gInSc1	katalog
–	–	k?	–
seznam	seznam	k1gInSc1	seznam
logicky	logicky	k6eAd1	logicky
roztříděných	roztříděný	k2eAgInPc2d1	roztříděný
odkazů	odkaz	k1gInPc2	odkaz
<g/>
,	,	kIx,	,
udržovaný	udržovaný	k2eAgMnSc1d1	udržovaný
obvykle	obvykle	k6eAd1	obvykle
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
katalog	katalog	k1gInSc1	katalog
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
<g/>
)	)	kIx)	)
<g/>
Internetový	internetový	k2eAgMnSc1d1	internetový
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
–	–	k?	–
automatizovaný	automatizovaný	k2eAgInSc4d1	automatizovaný
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
hledání	hledání	k1gNnSc4	hledání
podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
zadaných	zadaný	k2eAgNnPc2d1	zadané
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Bing	bingo	k1gNnPc2	bingo
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
vizi	vize	k1gFnSc4	vize
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
agentura	agentura	k1gFnSc1	agentura
ARPA	ARPA	kA	ARPA
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
DARPA	DARPA	kA	DARPA
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
grantová	grantový	k2eAgFnSc1d1	Grantová
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
krátkodobých	krátkodobý	k2eAgInPc2d1	krátkodobý
projektů	projekt	k1gInPc2	projekt
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
týmech	tým	k1gInPc6	tým
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
vypuštění	vypuštění	k1gNnSc6	vypuštění
Sputniku	sputnik	k1gInSc2	sputnik
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
období	období	k1gNnSc6	období
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
obnovení	obnovení	k1gNnSc2	obnovení
vedoucího	vedoucí	k2eAgNnSc2d1	vedoucí
technologického	technologický	k2eAgNnSc2d1	Technologické
postavení	postavení	k1gNnSc2	postavení
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
síť	síť	k1gFnSc1	síť
ARPANET	ARPANET	kA	ARPANET
se	s	k7c7	s
4	[number]	k4	4
uzly	uzel	k1gInPc7	uzel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
představovaly	představovat	k5eAaImAgInP	představovat
univerzitní	univerzitní	k2eAgInPc1d1	univerzitní
počítače	počítač	k1gInPc1	počítač
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
decentralizovaná	decentralizovaný	k2eAgFnSc1d1	decentralizovaná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgNnSc4	žádný
snadno	snadno	k6eAd1	snadno
zničitelné	zničitelný	k2eAgNnSc4d1	zničitelné
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
používala	používat	k5eAaImAgFnS	používat
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
přepojováním	přepojování	k1gNnSc7	přepojování
paketů	paket	k1gInPc2	paket
(	(	kIx(	(
<g/>
data	datum	k1gNnSc2	datum
putují	putovat	k5eAaImIp3nP	putovat
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
po	po	k7c6	po
malých	malý	k2eAgFnPc6d1	malá
samostatných	samostatný	k2eAgFnPc6d1	samostatná
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
směrovány	směrovat	k5eAaImNgFnP	směrovat
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
uzly	uzel	k1gInPc7	uzel
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
počet	počet	k1gInSc1	počet
připojených	připojený	k2eAgMnPc2d1	připojený
počítačů	počítač	k1gMnPc2	počítač
i	i	k8xC	i
uživatelů	uživatel	k1gMnPc2	uživatel
exponenciálně	exponenciálně	k6eAd1	exponenciálně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
služby	služba	k1gFnPc4	služba
internetu	internet	k1gInSc2	internet
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
internetu	internet	k1gInSc2	internet
mohou	moct	k5eAaImIp3nP	moct
uživatelé	uživatel	k1gMnPc1	uživatel
využívat	využívat	k5eAaPmF	využívat
mnoho	mnoho	k4c4	mnoho
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
zajišťovány	zajišťován	k2eAgInPc4d1	zajišťován
počítačovými	počítačový	k2eAgInPc7d1	počítačový
programy	program	k1gInPc7	program
a	a	k8xC	a
programy	program	k1gInPc7	program
navzájem	navzájem	k6eAd1	navzájem
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
protokolů	protokol	k1gInPc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Protokoly	protokol	k1gInPc1	protokol
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
definovány	definovat	k5eAaBmNgFnP	definovat
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
RFC	RFC	kA	RFC
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nejsou	být	k5eNaImIp3nP	být
normami	norma	k1gFnPc7	norma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
doporučeními	doporučení	k1gNnPc7	doporučení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
snaží	snažit	k5eAaImIp3nP	snažit
dodržovat	dodržovat	k5eAaImF	dodržovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
bezproblémové	bezproblémový	k2eAgFnPc4d1	bezproblémová
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolnost	dobrovolnost	k1gFnSc4	dobrovolnost
dodržování	dodržování	k1gNnSc2	dodržování
těchto	tento	k3xDgInPc2	tento
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
naplňování	naplňování	k1gNnSc6	naplňování
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
podstatě	podstata	k1gFnSc3	podstata
svobodného	svobodný	k2eAgNnSc2d1	svobodné
fungování	fungování	k1gNnSc2	fungování
samotného	samotný	k2eAgInSc2d1	samotný
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
služby	služba	k1gFnPc4	služba
internetu	internet	k1gInSc2	internet
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
WWW	WWW	kA	WWW
–	–	k?	–
systém	systém	k1gInSc1	systém
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
zobrazovaných	zobrazovaný	k2eAgFnPc2d1	zobrazovaná
pomocí	pomocí	k7c2	pomocí
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
</s>
</p>
<p>
<s>
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
protokol	protokol	k1gInSc1	protokol
HTTP	HTTP	kA	HTTP
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
zabezpečený	zabezpečený	k2eAgInSc4d1	zabezpečený
přenos	přenos	k1gInSc4	přenos
používá	používat	k5eAaImIp3nS	používat
protokol	protokol	k1gInSc1	protokol
HTTPS	HTTPS	kA	HTTPS
</s>
</p>
<p>
<s>
E-mail	eail	k1gInSc1	e-mail
–	–	k?	–
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
zpráv	zpráva	k1gFnPc2	zpráva
používá	používat	k5eAaImIp3nS	používat
protokol	protokol	k1gInSc1	protokol
SMTP	SMTP	kA	SMTP
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
poštovními	poštovní	k2eAgInPc7d1	poštovní
programy	program	k1gInPc7	program
používá	používat	k5eAaImIp3nS	používat
protokoly	protokol	k1gInPc1	protokol
POP	pop	k1gInSc1	pop
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
IMAP	IMAP	kA	IMAP
</s>
</p>
<p>
<s>
Instant	Instant	k1gInSc1	Instant
messaging	messaging	k1gInSc1	messaging
–	–	k?	–
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
,	,	kIx,	,
živá	živý	k2eAgFnSc1d1	živá
<g/>
)	)	kIx)	)
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
</s>
</p>
<p>
<s>
využívá	využívat	k5eAaImIp3nS	využívat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
protokoly	protokol	k1gInPc4	protokol
</s>
</p>
<p>
<s>
aplikace	aplikace	k1gFnPc1	aplikace
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
protokol	protokol	k1gInSc1	protokol
(	(	kIx(	(
<g/>
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
Jabber	Jabber	k1gInSc1	Jabber
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VoIP	VoIP	k?	VoIP
–	–	k?	–
telefonování	telefonování	k1gNnSc2	telefonování
pomocí	pomocí	k7c2	pomocí
internetu	internet	k1gInSc2	internet
</s>
</p>
<p>
<s>
SIP	SIP	kA	SIP
</s>
</p>
<p>
<s>
Skype	Skypat	k5eAaPmIp3nS	Skypat
–	–	k?	–
proprietární	proprietární	k2eAgInSc4d1	proprietární
protokol	protokol	k1gInSc4	protokol
</s>
</p>
<p>
<s>
FTP	FTP	kA	FTP
–	–	k?	–
přenos	přenos	k1gInSc1	přenos
souborů	soubor	k1gInPc2	soubor
</s>
</p>
<p>
<s>
služba	služba	k1gFnSc1	služba
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
protokol	protokol	k1gInSc4	protokol
</s>
</p>
<p>
<s>
DNS	DNS	kA	DNS
–	–	k?	–
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
jmen	jméno	k1gNnPc2	jméno
počítačů	počítač	k1gMnPc2	počítač
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
využívá	využívat	k5eAaImIp3nS	využívat
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
protokol	protokol	k1gInSc4	protokol
</s>
</p>
<p>
<s>
sdílení	sdílení	k1gNnSc4	sdílení
souborů	soubor	k1gInPc2	soubor
</s>
</p>
<p>
<s>
NFS	NFS	kA	NFS
<g/>
,	,	kIx,	,
GFS	GFS	kA	GFS
<g/>
,	,	kIx,	,
AFS	AFS	kA	AFS
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
protokol	protokol	k1gInSc1	protokol
SMB	SMB	kA	SMB
–	–	k?	–
sdílení	sdílení	k1gNnSc2	sdílení
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
s	s	k7c7	s
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
</s>
</p>
<p>
<s>
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
vzdálenému	vzdálený	k2eAgInSc3d1	vzdálený
počítači	počítač	k1gInSc3	počítač
</s>
</p>
<p>
<s>
Telnet	Telnet	k1gInSc1	Telnet
–	–	k?	–
klasický	klasický	k2eAgInSc1d1	klasický
textový	textový	k2eAgInSc1d1	textový
terminálový	terminálový	k2eAgInSc1d1	terminálový
přístup	přístup	k1gInSc1	přístup
</s>
</p>
<p>
<s>
SSH	SSH	kA	SSH
–	–	k?	–
zabezpečená	zabezpečený	k2eAgFnSc1d1	zabezpečená
náhrada	náhrada	k1gFnSc1	náhrada
protokolu	protokol	k1gInSc2	protokol
telnet	telneta	k1gFnPc2	telneta
</s>
</p>
<p>
<s>
VNC	VNC	kA	VNC
–	–	k?	–
připojení	připojení	k1gNnSc2	připojení
ke	k	k7c3	k
grafickému	grafický	k2eAgNnSc3d1	grafické
uživatelskému	uživatelský	k2eAgNnSc3d1	Uživatelské
prostředí	prostředí	k1gNnSc3	prostředí
</s>
</p>
<p>
<s>
RDP	RDP	kA	RDP
–	–	k?	–
připojení	připojení	k1gNnSc2	připojení
ke	k	k7c3	k
grafickému	grafický	k2eAgNnSc3d1	grafické
uživatelskému	uživatelský	k2eAgNnSc3d1	Uživatelské
prostředí	prostředí	k1gNnSc3	prostředí
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
proprietární	proprietární	k2eAgInSc1d1	proprietární
protokol	protokol	k1gInSc1	protokol
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
služební	služební	k2eAgInPc1d1	služební
protokoly	protokol	k1gInPc1	protokol
</s>
</p>
<p>
<s>
DHCP	DHCP	kA	DHCP
–	–	k?	–
automatická	automatický	k2eAgFnSc1d1	automatická
konfigurace	konfigurace	k1gFnSc1	konfigurace
stanic	stanice	k1gFnPc2	stanice
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
s	s	k7c7	s
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
</s>
</p>
<p>
<s>
SNMP	SNMP	kA	SNMP
–	–	k?	–
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
monitorování	monitorování	k1gNnSc1	monitorování
síťových	síťový	k2eAgInPc2d1	síťový
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
<s>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
služby	služba	k1gFnPc1	služba
a	a	k8xC	a
protokoly	protokol	k1gInPc1	protokol
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
===	===	k?	===
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
sítě	síť	k1gFnPc1	síť
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
novým	nový	k2eAgInSc7d1	nový
komunikačním	komunikační	k2eAgInSc7d1	komunikační
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetu	internet	k1gInSc2	internet
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
fyzicky	fyzicky	k6eAd1	fyzicky
nemohli	moct	k5eNaImAgMnP	moct
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc1	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
prožívají	prožívat	k5eAaImIp3nP	prožívat
sociální	sociální	k2eAgFnPc4d1	sociální
sítě	síť	k1gFnPc4	síť
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
urychlován	urychlovat	k5eAaImNgInS	urychlovat
nově	nově	k6eAd1	nově
vznikajícími	vznikající	k2eAgFnPc7d1	vznikající
technologiemi	technologie	k1gFnPc7	technologie
(	(	kIx(	(
<g/>
Web	web	k1gInSc1	web
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
blog	blog	k1gInSc1	blog
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
popularitě	popularita	k1gFnSc6	popularita
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
připojuje	připojovat	k5eAaImIp3nS	připojovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Účel	účel	k1gInSc1	účel
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
sdílení	sdílení	k1gNnSc3	sdílení
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
hledat	hledat	k5eAaImF	hledat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
etnika	etnikum	k1gNnPc4	etnikum
nebo	nebo	k8xC	nebo
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
sociální	sociální	k2eAgFnPc1d1	sociální
sítě	síť	k1gFnPc1	síť
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
<g/>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Twitter	Twitter	k1gMnSc1	Twitter
</s>
</p>
<p>
<s>
Diaspora	diaspora	k1gFnSc1	diaspora
<g/>
*	*	kIx~	*
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
LinkedIn	LinkedIn	k1gMnSc1	LinkedIn
</s>
</p>
<p>
<s>
MySpace	MySpace	k1gFnSc1	MySpace
</s>
</p>
<p>
<s>
Reddit	Reddit	k5eAaPmF	Reddit
</s>
</p>
<p>
<s>
Instagram	Instagram	k1gInSc1	Instagram
</s>
</p>
<p>
<s>
==	==	k?	==
Způsoby	způsob	k1gInPc1	způsob
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
dálkové	dálkový	k2eAgInPc1d1	dálkový
spoje	spoj	k1gInPc1	spoj
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
internetu	internet	k1gInSc6	internet
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc2d1	vysoká
přenosových	přenosový	k2eAgFnPc2d1	přenosová
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgInPc1	tento
vysokorychlostní	vysokorychlostní	k2eAgInPc1d1	vysokorychlostní
spoje	spoj	k1gInPc1	spoj
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
až	až	k9	až
ke	k	k7c3	k
koncovým	koncový	k2eAgMnPc3d1	koncový
uživatelům	uživatel	k1gMnPc3	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
připojeni	připojen	k2eAgMnPc1d1	připojen
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgFnSc2d1	poslední
míle	míle	k1gFnSc2	míle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
připojení	připojení	k1gNnSc1	připojení
uživatelů	uživatel	k1gMnPc2	uživatel
je	být	k5eAaImIp3nS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
různými	různý	k2eAgFnPc7d1	různá
technologiemi	technologie	k1gFnPc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ušetřili	ušetřit	k5eAaPmAgMnP	ušetřit
náklady	náklad	k1gInPc4	náklad
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
na	na	k7c4	na
dražší	drahý	k2eAgNnSc4d2	dražší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
připojení	připojení	k1gNnSc1	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Zprostředkovatele	zprostředkovatel	k1gMnSc2	zprostředkovatel
připojení	připojení	k1gNnSc1	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
označujeme	označovat	k5eAaImIp1nP	označovat
Internet	Internet	k1gInSc4	Internet
service	service	k1gFnSc2	service
provider	provider	k1gInSc1	provider
(	(	kIx(	(
<g/>
ISP	ISP	kA	ISP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
možností	možnost	k1gFnPc2	možnost
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
počítače	počítač	k1gInSc2	počítač
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
telefonní	telefonní	k2eAgFnSc1d1	telefonní
linka	linka	k1gFnSc1	linka
(	(	kIx(	(
<g/>
majitelem	majitel	k1gMnSc7	majitel
linky	linka	k1gFnSc2	linka
je	být	k5eAaImIp3nS	být
telefonní	telefonní	k2eAgInSc4d1	telefonní
operátor	operátor	k1gInSc4	operátor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
modem	modem	k1gInSc1	modem
</s>
</p>
<p>
<s>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
vytáčené	vytáčený	k2eAgNnSc1d1	vytáčené
připojení	připojení	k1gNnSc1	připojení
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ISDN	ISDN	kA	ISDN
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
DSL	DSL	kA	DSL
</s>
</p>
<p>
<s>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
datové	datový	k2eAgInPc4d1	datový
přenosy	přenos	k1gInPc4	přenos
</s>
</p>
<p>
<s>
kabelová	kabelový	k2eAgFnSc1d1	kabelová
přípojka	přípojka	k1gFnSc1	přípojka
</s>
</p>
<p>
<s>
bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
datová	datový	k2eAgFnSc1d1	datová
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
satelitní	satelitní	k2eAgFnSc1d1	satelitní
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
mobilní	mobilní	k2eAgFnSc1d1	mobilní
telefonní	telefonní	k2eAgFnSc1d1	telefonní
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Wi-Fi	Wi-Fi	k6eAd1	Wi-Fi
</s>
</p>
<p>
<s>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
rozvodná	rozvodný	k2eAgFnSc1d1	rozvodná
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
jiné	jiná	k1gFnSc3	jiná
možnostiO	možnostiO	k?	možnostiO
kvalitě	kvalita	k1gFnSc3	kvalita
připojení	připojený	k2eAgMnPc1d1	připojený
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
agregace	agregace	k1gFnSc1	agregace
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kolik	kolik	k4yIc4	kolik
uživatelů	uživatel	k1gMnPc2	uživatel
sdílí	sdílet	k5eAaImIp3nS	sdílet
jednu	jeden	k4xCgFnSc4	jeden
linku	linka	k1gFnSc4	linka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
doba	doba	k1gFnSc1	doba
odezvy	odezva	k1gFnSc2	odezva
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
odezvy	odezva	k1gFnPc1	odezva
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
např.	např.	kA	např.
při	při	k7c6	při
internetové	internetový	k2eAgFnSc6d1	internetová
telefonii	telefonie	k1gFnSc6	telefonie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rychlost	rychlost	k1gFnSc1	rychlost
připojení	připojení	k1gNnSc2	připojení
poslední	poslední	k2eAgFnSc2d1	poslední
míle	míle	k1gFnSc2	míle
</s>
</p>
<p>
<s>
technologie	technologie	k1gFnSc1	technologie
použitá	použitý	k2eAgFnSc1d1	použitá
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgFnSc1d1	poslední
míle	míle	k1gFnSc1	míle
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
připojena	připojen	k2eAgFnSc1d1	připojena
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
předvedena	předveden	k2eAgFnSc1d1	předvedena
funkční	funkční	k2eAgFnSc1d1	funkční
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
linka	linka	k1gFnSc1	linka
do	do	k7c2	do
města	město	k1gNnSc2	město
Linec	Linec	k1gInSc1	Linec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
mělo	mít	k5eAaImAgNnS	mít
připojení	připojení	k1gNnSc1	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
32	[number]	k4	32
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2009	[number]	k4	2009
mělo	mít	k5eAaImAgNnS	mít
přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
domácích	domácí	k2eAgMnPc2d1	domácí
počítačů	počítač	k1gMnPc2	počítač
v	v	k7c6	v
ČR	ČR	kA	ČR
možnost	možnost	k1gFnSc4	možnost
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
typem	typ	k1gInSc7	typ
připojení	připojení	k1gNnSc2	připojení
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
bylo	být	k5eAaImAgNnS	být
bezdrátové	bezdrátový	k2eAgNnSc1d1	bezdrátové
(	(	kIx(	(
<g/>
36	[number]	k4	36
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
realizované	realizovaný	k2eAgFnPc1d1	realizovaná
technologií	technologie	k1gFnSc7	technologie
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
pevnou	pevný	k2eAgFnSc7d1	pevná
telefonní	telefonní	k2eAgFnSc7d1	telefonní
linkou	linka	k1gFnSc7	linka
s	s	k7c7	s
ADSL	ADSL	kA	ADSL
(	(	kIx(	(
<g/>
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
kabelovou	kabelový	k2eAgFnSc7d1	kabelová
přípojkou	přípojka	k1gFnSc7	přípojka
(	(	kIx(	(
<g/>
23	[number]	k4	23
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připojení	připojení	k1gNnSc1	připojení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
vykázalo	vykázat	k5eAaPmAgNnS	vykázat
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
%	%	kIx~	%
a	a	k8xC	a
kdysi	kdysi	k6eAd1	kdysi
nejvyužívanější	využívaný	k2eAgFnSc1d3	nejvyužívanější
technologie	technologie	k1gFnSc1	technologie
dial-up	dialp	k1gInSc1	dial-up
(	(	kIx(	(
<g/>
vytáčené	vytáčený	k2eAgNnSc1d1	vytáčené
připojení	připojení	k1gNnSc1	připojení
<g/>
)	)	kIx)	)
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
počítačů	počítač	k1gMnPc2	počítač
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
rychlostí	rychlost	k1gFnSc7	rychlost
2	[number]	k4	2
Mbit	Mbita	k1gFnPc2	Mbita
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
Mbit	Mbitum	k1gNnPc2	Mbitum
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
19	[number]	k4	19
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uživatelů	uživatel	k1gMnPc2	uživatel
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2	do
1	[number]	k4	1
Mbit	Mbitum	k1gNnPc2	Mbitum
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
bylo	být	k5eAaImAgNnS	být
36	[number]	k4	36
%	%	kIx~	%
<g/>
.	.	kIx.	.
14	[number]	k4	14
%	%	kIx~	%
uživatelů	uživatel	k1gMnPc2	uživatel
internetu	internet	k1gInSc2	internet
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
vědomo	vědom	k2eAgNnSc4d1	vědomo
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
rychlost	rychlost	k1gFnSc4	rychlost
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
<g/>
Rychlost	rychlost	k1gFnSc1	rychlost
připojení	připojení	k1gNnSc2	připojení
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
byla	být	k5eAaImAgFnS	být
poskytovateli	poskytovatel	k1gMnSc3	poskytovatel
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
internet	internet	k1gInSc1	internet
průběžně	průběžně	k6eAd1	průběžně
navyšována	navyšovat	k5eAaImNgFnS	navyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
rychlostí	rychlost	k1gFnSc7	rychlost
2	[number]	k4	2
Mbit	Mbita	k1gFnPc2	Mbita
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
36	[number]	k4	36
%	%	kIx~	%
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
již	již	k9	již
na	na	k7c4	na
51	[number]	k4	51
%	%	kIx~	%
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bezdrátovému	bezdrátový	k2eAgNnSc3d1	bezdrátové
připojení	připojení	k1gNnSc3	připojení
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
především	především	k9	především
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
parametrem	parametr	k1gInSc7	parametr
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
rychlost	rychlost	k1gFnSc1	rychlost
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
nechtějí	chtít	k5eNaImIp3nP	chtít
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
pevnou	pevný	k2eAgFnSc4d1	pevná
linku	linka	k1gFnSc4	linka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kabelu	kabel	k1gInSc2	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
volí	volit	k5eAaImIp3nP	volit
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
připojení	připojení	k1gNnSc2	připojení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
Michal	Michal	k1gMnSc1	Michal
Peca	Peca	k1gMnSc1	Peca
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Factum	Factum	k1gNnSc1	Factum
Invenio	Invenio	k1gNnSc1	Invenio
<g/>
.	.	kIx.	.
<g/>
Vládní	vládní	k2eAgFnSc1d1	vládní
strategie	strategie	k1gFnSc1	strategie
Digitální	digitální	k2eAgNnSc4d1	digitální
Česko	Česko	k1gNnSc4	Česko
počítala	počítat	k5eAaImAgFnS	počítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
Mbit	Mbita	k1gFnPc2	Mbita
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
To	to	k9	to
se	se	k3xPyFc4	se
ale	ale	k9	ale
nenaplnilo	naplnit	k5eNaPmAgNnS	naplnit
ani	ani	k8xC	ani
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
reálné	reálný	k2eAgFnPc4d1	reálná
rychlosti	rychlost	k1gFnPc4	rychlost
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Strategie	strategie	k1gFnSc1	strategie
Digitální	digitální	k2eAgNnSc1d1	digitální
Česko	Česko	k1gNnSc1	Česko
2.0	[number]	k4	2.0
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
bude	být	k5eAaImBp3nS	být
všude	všude	k6eAd1	všude
dostupné	dostupný	k2eAgNnSc1d1	dostupné
minimálně	minimálně	k6eAd1	minimálně
připojení	připojení	k1gNnSc4	připojení
30	[number]	k4	30
Mbit	Mbitum	k1gNnPc2	Mbitum
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Závislost	závislost	k1gFnSc4	závislost
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Studií	studie	k1gFnPc2	studie
a	a	k8xC	a
materiálu	materiál	k1gInSc2	materiál
zpracovaného	zpracovaný	k2eAgInSc2d1	zpracovaný
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
minimum	minimum	k1gNnSc1	minimum
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
závislosti	závislost	k1gFnSc2	závislost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
podceňována	podceňovat	k5eAaImNgFnS	podceňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
UIN	UIN	kA	UIN
</s>
</p>
<p>
<s>
Internetová	internetový	k2eAgFnSc1d1	internetová
horečka	horečka	k1gFnSc1	horečka
</s>
</p>
<p>
<s>
Webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
internetu	internet	k1gInSc2	internet
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
umožňující	umožňující	k2eAgInSc1d1	umožňující
dálkový	dálkový	k2eAgInSc1d1	dálkový
přístup	přístup	k1gInSc1	přístup
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
Internetu	Internet	k1gInSc2	Internet
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
internet	internet	k1gInSc1	internet
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Srovnání	srovnání	k1gNnSc2	srovnání
mobilních	mobilní	k2eAgInPc2d1	mobilní
datových	datový	k2eAgInPc2d1	datový
tarifů	tarif	k1gInPc2	tarif
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Internet	Internet	k1gInSc1	Internet
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
internet	internet	k1gInSc1	internet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
ISOC	ISOC	kA	ISOC
<g/>
)	)	kIx)	)
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
globální	globální	k2eAgFnSc4d1	globální
koordinaci	koordinace	k1gFnSc4	koordinace
a	a	k8xC	a
kooperaci	kooperace	k1gFnSc4	kooperace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
internetu	internet	k1gInSc2	internet
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Architecture	Architectur	k1gMnSc5	Architectur
Board	Board	k1gMnSc1	Board
(	(	kIx(	(
<g/>
IAB	IAB	kA	IAB
<g/>
)	)	kIx)	)
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
poradní	poradní	k2eAgFnSc1d1	poradní
skupina	skupina	k1gFnSc1	skupina
ISOC	ISOC	kA	ISOC
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
protokolovou	protokolový	k2eAgFnSc4d1	protokolová
architekturu	architektura	k1gFnSc4	architektura
internetu	internet	k1gInSc2	internet
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Task	k1gInSc4	Task
Force	force	k1gFnSc2	force
(	(	kIx(	(
<g/>
IETF	IETF	kA	IETF
<g/>
)	)	kIx)	)
-	-	kIx~	-
pracuje	pracovat	k5eAaImIp3nS	pracovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
IAB	IAB	kA	IAB
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
specifikace	specifikace	k1gFnSc1	specifikace
pro	pro	k7c4	pro
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
domluvené	domluvený	k2eAgNnSc1d1	domluvené
de	de	k?	de
facto	facto	k1gNnSc1	facto
normy	norma	k1gFnSc2	norma
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
jako	jako	k8xS	jako
RFC	RFC	kA	RFC
dokumenty	dokument	k1gInPc1	dokument
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Engineering	Engineering	k1gInSc1	Engineering
Steering	Steering	k1gInSc1	Steering
Group	Group	k1gInSc1	Group
(	(	kIx(	(
<g/>
IESG	IESG	kA	IESG
<g/>
)	)	kIx)	)
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ISOC	ISOC	kA	ISOC
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
technických	technický	k2eAgFnPc2d1	technická
činností	činnost	k1gFnPc2	činnost
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Research	Research	k1gInSc1	Research
Task	Task	k1gInSc4	Task
Force	force	k1gFnSc2	force
(	(	kIx(	(
<g/>
IRTF	IRTF	kA	IRTF
<g/>
)	)	kIx)	)
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Assigned	Assigned	k1gInSc1	Assigned
Numbers	Numbers	k1gInSc4	Numbers
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
IANA	Ianus	k1gMnSc2	Ianus
<g/>
)	)	kIx)	)
-	-	kIx~	-
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
globální	globální	k2eAgNnSc4d1	globální
přidělování	přidělování	k1gNnSc4	přidělování
internetových	internetový	k2eAgInPc2d1	internetový
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
čísel	číslo	k1gNnPc2	číslo
portů	port	k1gInPc2	port
<g/>
,	,	kIx,	,
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
MIB	MIB	kA	MIB
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Internet	Internet	k1gInSc1	Internet
Corporation	Corporation	k1gInSc1	Corporation
for	forum	k1gNnPc2	forum
Assigned	Assigned	k1gMnSc1	Assigned
Names	Names	k1gMnSc1	Names
and	and	k?	and
Numbers	Numbers	k1gInSc1	Numbers
(	(	kIx(	(
<g/>
ICANN	ICANN	kA	ICANN
<g/>
)	)	kIx)	)
-	-	kIx~	-
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c6	o
přidělování	přidělování	k1gNnSc6	přidělování
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
adresního	adresní	k2eAgInSc2d1	adresní
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
protokolových	protokolový	k2eAgInPc2d1	protokolový
parametrů	parametr	k1gInPc2	parametr
</s>
</p>
