<s>
Lublaň	Lublaň	k1gFnSc1	Lublaň
(	(	kIx(	(
<g/>
slovinsky	slovinsky	k6eAd1	slovinsky
Ljubljana	Ljubljan	k1gMnSc2	Ljubljan
[	[	kIx(	[
<g/>
lj	lj	k?	lj
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Lubiana	Lubiana	k1gFnSc1	Lubiana
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Laibach	Laibach	k1gInSc1	Laibach
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Aemona	Aemona	k1gFnSc1	Aemona
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Ľubľana	Ľubľana	k1gFnSc1	Ľubľana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
275	[number]	k4	275
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
281	[number]	k4	281
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
počtu	počet	k1gInSc3	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Lublaň	Lublaň	k1gFnSc1	Lublaň
spíše	spíše	k9	spíše
než	než	k8xS	než
dojmem	dojem	k1gInSc7	dojem
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
provinční	provinční	k2eAgNnSc1d1	provinční
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
bulvárem	bulvár	k1gInSc7	bulvár
je	být	k5eAaImIp3nS	být
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
Slovenska	Slovensko	k1gNnSc2	Slovensko
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lublaň	Lublaň	k1gFnSc1	Lublaň
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
ústřední	ústřední	k2eAgFnSc2d1	ústřední
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
slovinských	slovinský	k2eAgFnPc2d1	slovinská
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
dominuje	dominovat	k5eAaImIp3nS	dominovat
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
Ljubljanski	Ljubljansk	k1gFnSc2	Ljubljansk
grad	grad	k1gInSc1	grad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
věží	věž	k1gFnPc2	věž
je	být	k5eAaImIp3nS	být
výhled	výhled	k1gInSc4	výhled
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
až	až	k9	až
k	k	k7c3	k
Julským	Julský	k2eAgFnPc3d1	Julský
Alpám	Alpy	k1gFnPc3	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Lublaňky	Lublaňka	k1gFnSc2	Lublaňka
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
Prešerenovo	Prešerenův	k2eAgNnSc1d1	Prešerenovo
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
Prešernov	Prešernov	k1gInSc1	Prešernov
trg	trg	k?	trg
<g/>
)	)	kIx)	)
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
básníka	básník	k1gMnSc2	básník
Franceho	France	k1gMnSc2	France
Prešerena	Prešeren	k1gMnSc2	Prešeren
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
múzou	múza	k1gFnSc7	múza
Urškou	Urška	k1gFnSc7	Urška
a	a	k8xC	a
s	s	k7c7	s
barokním	barokní	k2eAgInSc7d1	barokní
kostelem	kostel	k1gInSc7	kostel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Trojmostí	Trojmost	k1gFnSc7	Trojmost
(	(	kIx(	(
<g/>
Tromostovje	Tromostovje	k1gFnSc1	Tromostovje
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
světoznámým	světoznámý	k2eAgMnSc7d1	světoznámý
architektem	architekt	k1gMnSc7	architekt
Jože	Joža	k1gFnSc6	Joža
Plečnikem	Plečnik	k1gInSc7	Plečnik
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
zřídili	zřídit	k5eAaPmAgMnP	zřídit
tábor	tábor	k1gInSc4	tábor
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Emona	Emoen	k2eAgFnSc1d1	Emoen
a	a	k8xC	a
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
34	[number]	k4	34
př.n.l.	př.n.l.	k?	př.n.l.
dostal	dostat	k5eAaPmAgInS	dostat
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1144	[number]	k4	1144
dostala	dostat	k5eAaPmAgFnS	dostat
německý	německý	k2eAgInSc4d1	německý
název	název	k1gInSc4	název
Laibach	Laibacha	k1gFnPc2	Laibacha
a	a	k8xC	a
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
habsburské	habsburský	k2eAgFnSc2d1	habsburská
říše	říš	k1gFnSc2	říš
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Kraňska	Kraňsko	k1gNnSc2	Kraňsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
sídlil	sídlit	k5eAaImAgInS	sídlit
zde	zde	k6eAd1	zde
Kraňský	kraňský	k2eAgInSc1d1	kraňský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
Francouzi	Francouz	k1gMnPc1	Francouz
zřízených	zřízený	k2eAgFnPc2d1	zřízená
Ilyrských	ilyrský	k2eAgFnPc2d1	ilyrská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
království	království	k1gNnSc2	království
SHS	SHS	kA	SHS
byla	být	k5eAaImAgFnS	být
kulturní	kulturní	k2eAgFnSc7d1	kulturní
metropolí	metropol	k1gFnSc7	metropol
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
pak	pak	k6eAd1	pak
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Drávské	Drávský	k2eAgFnSc2d1	Drávská
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ji	on	k3xPp3gFnSc4	on
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Italové	Ital	k1gMnPc1	Ital
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1941	[number]	k4	1941
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uplatňovali	uplatňovat	k5eAaImAgMnP	uplatňovat
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
antislovinskou	antislovinský	k2eAgFnSc4d1	antislovinský
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
ale	ale	k9	ale
město	město	k1gNnSc4	město
napadaly	napadat	k5eAaImAgInP	napadat
různé	různý	k2eAgInPc1d1	různý
protifašistické	protifašistický	k2eAgInPc1d1	protifašistický
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
titul	titul	k1gInSc1	titul
Město	město	k1gNnSc1	město
Hrdina	Hrdina	k1gMnSc1	Hrdina
Josipem	Josip	k1gMnSc7	Josip
Titem	Tit	k1gMnSc7	Tit
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnPc2	metropol
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
se	se	k3xPyFc4	se
Lublaň	Lublaň	k1gFnSc1	Lublaň
stala	stát	k5eAaPmAgFnS	stát
oficiálně	oficiálně	k6eAd1	oficiálně
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
výskyt	výskyt	k1gInSc4	výskyt
velkých	velký	k2eAgFnPc2d1	velká
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Lublaně	Lublaň	k1gFnSc2	Lublaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
baroko	baroko	k1gNnSc1	baroko
s	s	k7c7	s
art	art	k?	art
nouveau	nouveau	k6eAd1	nouveau
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
architekturou	architektura	k1gFnSc7	architektura
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
Solnohradu	Solnohrad	k1gInSc2	Solnohrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1511	[number]	k4	1511
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
město	město	k1gNnSc1	město
částečně	částečně	k6eAd1	částečně
poničilo	poničit	k5eAaPmAgNnS	poničit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lublaň	Lublaň	k1gFnSc1	Lublaň
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
směsí	směs	k1gFnSc7	směs
různých	různý	k2eAgInPc2d1	různý
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
postavené	postavený	k2eAgFnPc4d1	postavená
po	po	k7c6	po
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
známým	známý	k2eAgMnSc7d1	známý
slovinským	slovinský	k2eAgMnSc7d1	slovinský
architektem	architekt	k1gMnSc7	architekt
Jožem	Jož	k1gMnSc7	Jož
Plečnikem	Plečnik	k1gMnSc7	Plečnik
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dominantu	dominanta	k1gFnSc4	dominanta
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
Lublaňský	lublaňský	k2eAgInSc4d1	lublaňský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Lublaňkou	Lublaňka	k1gFnSc7	Lublaňka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
markrabat	markrabě	k1gNnPc2	markrabě
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vévodů	vévoda	k1gMnPc2	vévoda
korutanských	korutanský	k2eAgMnPc2d1	korutanský
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Lublaňského	lublaňský	k2eAgInSc2d1	lublaňský
hradu	hrad	k1gInSc2	hrad
mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
architektonická	architektonický	k2eAgNnPc4d1	architektonické
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Prešerenovo	Prešerenův	k2eAgNnSc1d1	Prešerenovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
Svatopetrský	svatopetrský	k2eAgInSc1d1	svatopetrský
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
františkánský	františkánský	k2eAgInSc1d1	františkánský
kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnPc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Klášter	klášter	k1gInSc1	klášter
Rakung	Rakunga	k1gFnPc2	Rakunga
<g/>
,	,	kIx,	,
Trojmostí	Trojmost	k1gFnPc2	Trojmost
a	a	k8xC	a
Dračí	dračí	k2eAgInSc4d1	dračí
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Lublaň	Lublaň	k1gFnSc1	Lublaň
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
%	%	kIx~	%
slovinského	slovinský	k2eAgInSc2d1	slovinský
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
6,5	[number]	k4	6,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
7,7	[number]	k4	7,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
míra	mír	k1gInSc2	mír
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
8,7	[number]	k4	8,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejběžnějším	běžný	k2eAgNnSc7d3	nejběžnější
zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgMnSc6d1	farmaceutický
<g/>
,	,	kIx,	,
petrochemickém	petrochemický	k2eAgMnSc6d1	petrochemický
a	a	k8xC	a
potravinářském	potravinářský	k2eAgNnSc6d1	potravinářské
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
sektory	sektor	k1gInPc4	sektor
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
,	,	kIx,	,
finance	finance	k1gFnPc4	finance
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc1	služba
a	a	k8xC	a
turismus	turismus	k1gInSc1	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Veřejný	veřejný	k2eAgInSc1d1	veřejný
sektor	sektor	k1gInSc1	sektor
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
místní	místní	k2eAgFnSc3d1	místní
správě	správa	k1gFnSc3	správa
<g/>
.	.	kIx.	.
</s>
<s>
Lublaňská	lublaňský	k2eAgFnSc1d1	Lublaňská
burza	burza	k1gFnSc1	burza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
koupena	koupen	k2eAgFnSc1d1	koupena
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
burzou	burza	k1gFnSc7	burza
<g/>
,	,	kIx,	,
obchodují	obchodovat	k5eAaImIp3nP	obchodovat
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
akcie	akcie	k1gFnPc4	akcie
největších	veliký	k2eAgFnPc2d3	veliký
slovinských	slovinský	k2eAgFnPc2d1	slovinská
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
operuje	operovat	k5eAaImIp3nS	operovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
000	[number]	k4	000
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Lublaň	Lublaň	k1gFnSc1	Lublaň
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
slovinské	slovinský	k2eAgFnSc2d1	slovinská
silniční	silniční	k2eAgFnSc2d1	silniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Dostanete	dostat	k5eAaPmIp2nP	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
fungoval	fungovat	k5eAaImAgMnS	fungovat
mýtný	mýtný	k1gMnSc1	mýtný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dálničními	dálniční	k2eAgFnPc7d1	dálniční
známkami	známka	k1gFnPc7	známka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
jihozápadem	jihozápad	k1gInSc7	jihozápad
(	(	kIx(	(
<g/>
italská	italský	k2eAgFnSc1d1	italská
města	město	k1gNnSc2	město
Terst	Terst	k1gInSc1	Terst
a	a	k8xC	a
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
)	)	kIx)	)
spojeno	spojit	k5eAaPmNgNnS	spojit
dálnicí	dálnice	k1gFnSc7	dálnice
A1	A1	k1gFnSc2	A1
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
severozápadem	severozápad	k1gInSc7	severozápad
(	(	kIx(	(
<g/>
rakouská	rakouský	k2eAgNnPc1d1	rakouské
města	město	k1gNnPc1	město
Villach	Villacha	k1gFnPc2	Villacha
a	a	k8xC	a
Solnohrad	Solnohrady	k1gInPc2	Solnohrady
<g/>
)	)	kIx)	)
dálnicí	dálnice	k1gFnSc7	dálnice
A2	A2	k1gFnSc2	A2
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
61	[number]	k4	61
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
severovýchodem	severovýchod	k1gInSc7	severovýchod
(	(	kIx(	(
<g/>
rakouská	rakouský	k2eAgFnSc1d1	rakouská
města	město	k1gNnSc2	město
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dálnice	dálnice	k1gFnSc2	dálnice
A1	A1	k1gFnSc2	A1
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
s	s	k7c7	s
východem	východ	k1gInSc7	východ
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
(	(	kIx(	(
<g/>
chorvatský	chorvatský	k2eAgInSc1d1	chorvatský
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
)	)	kIx)	)
dálnicí	dálnice	k1gFnSc7	dálnice
A2	A2	k1gFnSc2	A2
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
městem	město	k1gNnSc7	město
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
Ljubljanski	Ljubljansk	k1gMnSc3	Ljubljansk
potniški	potnišk	k1gMnSc3	potnišk
promet	promést	k5eAaPmDgInS	promést
(	(	kIx(	(
<g/>
Lublaňská	lublaňský	k2eAgFnSc1d1	Lublaňská
přeprava	přeprava	k1gFnSc1	přeprava
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lublaňským	lublaňský	k2eAgInPc3d1	lublaňský
autobusům	autobus	k1gInPc3	autobus
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
trole	trolit	k5eAaImSgInS	trolit
(	(	kIx(	(
<g/>
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
trolejbus	trolejbus	k1gInSc1	trolejbus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
město	město	k1gNnSc1	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
-	-	kIx~	-
1971	[number]	k4	1971
mělo	mít	k5eAaImAgNnS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
trolejbusové	trolejbusový	k2eAgFnPc4d1	trolejbusová
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
populárním	populární	k2eAgInSc7d1	populární
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
je	být	k5eAaImIp3nS	být
také	také	k9	také
kolo	kolo	k1gNnSc1	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Lublaně	Lublaň	k1gFnSc2	Lublaň
jezdí	jezdit	k5eAaImIp3nS	jezdit
3	[number]	k4	3
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
vlakové	vlakový	k2eAgInPc4d1	vlakový
spoje	spoj	k1gInPc4	spoj
(	(	kIx(	(
<g/>
Mnichov-Lublaň-Záhřeb	Mnichov-Lublaň-Záhřba	k1gFnPc2	Mnichov-Lublaň-Záhřba
<g/>
,	,	kIx,	,
Vídeň-Lublaň	Vídeň-Lublaň	k1gFnPc2	Vídeň-Lublaň
a	a	k8xC	a
Benátky-Lublaň-Budapešť	Benátky-Lublaň-Budapešť	k1gFnPc2	Benátky-Lublaň-Budapešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
do	do	k7c2	do
Lublaně	Lublaň	k1gFnSc2	Lublaň
pohodlně	pohodlně	k6eAd1	pohodlně
dostat	dostat	k5eAaPmF	dostat
jak	jak	k6eAd1	jak
přes	přes	k7c4	přes
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
trasu	trasa	k1gFnSc4	trasa
tak	tak	k9	tak
přes	přes	k7c4	přes
Solnohrad	Solnohrad	k1gInSc4	Solnohrad
<g/>
.	.	kIx.	.
</s>
<s>
Lublaňské	lublaňský	k2eAgNnSc1d1	Lublaňské
letiště	letiště	k1gNnSc1	letiště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
26	[number]	k4	26
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odtud	odtud	k6eAd1	odtud
letět	letět	k5eAaImF	letět
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
destinací	destinace	k1gFnPc2	destinace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Amsterodam	Amsterodam	k1gInSc1	Amsterodam
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
nebo	nebo	k8xC	nebo
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
