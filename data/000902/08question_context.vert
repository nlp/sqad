<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1316	[number]	k4	1316
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1378	[number]	k4	1378
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
římsko-německý	římskoěmecký	k2eAgMnSc1d1	římsko-německý
král	král	k1gMnSc1	král
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1346	[number]	k4	1346
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
císař	císař	k1gMnSc1	císař
římský	římský	k2eAgInSc1d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
italský	italský	k2eAgMnSc1d1	italský
(	(	kIx(	(
<g/>
lombardský	lombardský	k2eAgMnSc1d1	lombardský
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
,	,	kIx,	,
burgundský	burgundský	k2eAgMnSc1d1	burgundský
(	(	kIx(	(
<g/>
arelatský	arelatský	k2eAgMnSc1d1	arelatský
<g/>
)	)	kIx)	)
král	král	k1gMnSc1	král
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1333	[number]	k4	1333
až	až	k9	až
1349	[number]	k4	1349
a	a	k8xC	a
lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
hrabě	hrabě	k1gMnSc1	hrabě
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1346	[number]	k4	1346
až	až	k6eAd1	až
1353	[number]	k4	1353
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
<g/>
.	.	kIx.	.
</s>

