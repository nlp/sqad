<s>
Rektascenze	rektascenze	k1gFnSc1	rektascenze
je	být	k5eAaImIp3nS	být
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
úhel	úhel	k1gInSc4	úhel
mezi	mezi	k7c7	mezi
rovinou	rovina	k1gFnSc7	rovina
deklinační	deklinační	k2eAgFnSc2d1	deklinační
kružnice	kružnice	k1gFnSc2	kružnice
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
rovinou	rovina	k1gFnSc7	rovina
deklinační	deklinační	k2eAgFnSc2d1	deklinační
kružnice	kružnice	k1gFnSc2	kružnice
procházející	procházející	k2eAgInSc1d1	procházející
jarním	jarní	k2eAgInSc7d1	jarní
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Rektascenze	rektascenze	k1gFnSc1	rektascenze
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
neuvádí	uvádět	k5eNaImIp3nS	uvádět
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
za	za	k7c4	za
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
jarním	jarní	k2eAgInSc6d1	jarní
bodu	bod	k1gInSc6	bod
projde	projít	k5eAaPmIp3nS	projít
měřený	měřený	k2eAgInSc1d1	měřený
bod	bod	k1gInSc1	bod
stejným	stejný	k2eAgNnSc7d1	stejné
místem	místo	k1gNnSc7	místo
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Deklinace	deklinace	k1gFnSc1	deklinace
Rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
souřadnice	souřadnice	k1gFnSc2	souřadnice
Sférická	sférický	k2eAgFnSc1d1	sférická
astronomie	astronomie	k1gFnSc1	astronomie
</s>
