<s>
Kilometr	kilometr	k1gInSc1	kilometr
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
v	v	k7c6	v
metrické	metrický	k2eAgFnSc6d1	metrická
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
tisíci	tisíc	k4xCgInPc7	tisíc
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
