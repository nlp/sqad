<s>
Adrenokortikotropní	Adrenokortikotropní	k2eAgInSc1d1
hormon	hormon	k1gInSc1
(	(	kIx(
<g/>
kortikotropin	kortikotropin	k1gInSc1
<g/>
,	,	kIx,
ACTH	ACTH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hormon	hormon	k1gInSc1
produkovaný	produkovaný	k2eAgInSc1d1
v	v	k7c4
pars	pars	k1gInSc4
distalis	distalis	k1gFnSc2
adenohypofýzy	adenohypofýza	k1gFnSc2
chromofilními	chromofilní	k2eAgFnPc7d1
(	(	kIx(
<g/>
bazofilními	bazofilní	k2eAgFnPc7d1
<g/>
)	)	kIx)
buňkami	buňka	k1gFnPc7
<g/>
,	,	kIx,
zvanými	zvaný	k2eAgInPc7d1
též	též	k9
buňky	buňka	k1gFnPc1
kortikotropní	kortikotropní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>