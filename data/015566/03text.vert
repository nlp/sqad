<s>
Adrenokortikotropní	Adrenokortikotropní	k2eAgInSc1d1
hormon	hormon	k1gInSc1
</s>
<s>
Adrenokortikotropní	Adrenokortikotropní	k2eAgInSc1d1
hormon	hormon	k1gInSc1
(	(	kIx(
<g/>
kortikotropin	kortikotropin	k1gInSc1
<g/>
,	,	kIx,
ACTH	ACTH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hormon	hormon	k1gInSc1
produkovaný	produkovaný	k2eAgInSc1d1
v	v	k7c4
pars	pars	k1gInSc4
distalis	distalis	k1gFnSc2
adenohypofýzy	adenohypofýza	k1gFnSc2
chromofilními	chromofilní	k2eAgFnPc7d1
(	(	kIx(
<g/>
bazofilními	bazofilní	k2eAgFnPc7d1
<g/>
)	)	kIx)
buňkami	buňka	k1gFnPc7
<g/>
,	,	kIx,
zvanými	zvaný	k2eAgInPc7d1
též	též	k9
buňky	buňka	k1gFnPc1
kortikotropní	kortikotropní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Stimuluje	stimulovat	k5eAaImIp3nS
růst	růst	k1gInSc1
kůry	kůra	k1gFnSc2
nadledvin	nadledvina	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
produkci	produkce	k1gFnSc4
glukokortikoidů	glukokortikoid	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
kortizolu	kortizol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
na	na	k7c6
stimulaci	stimulace	k1gFnSc6
tvorby	tvorba	k1gFnSc2
prekurzorů	prekurzor	k1gMnPc2
aldosteronu	aldosteron	k1gInSc2
–	–	k?
hormonu	hormon	k1gInSc2
ze	z	k7c2
skupiny	skupina	k1gFnSc2
mineralokortikoidů	mineralokortikoid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ACTH	ACTH	kA
působí	působit	k5eAaImIp3nS
i	i	k9
na	na	k7c4
melanocyty	melanocyt	k1gInPc4
v	v	k7c6
nichž	jenž	k3xRgFnPc6
zvyšuje	zvyšovat	k5eAaImIp3nS
produkci	produkce	k1gFnSc4
tmavého	tmavý	k2eAgNnSc2d1
kožního	kožní	k2eAgNnSc2d1
barviva	barvivo	k1gNnSc2
melaninu	melanin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ACTH	ACTH	kA
také	také	k9
stimuluje	stimulovat	k5eAaImIp3nS
lipolýzu	lipolýza	k1gFnSc4
–	–	k?
odbourávaní	odbourávaný	k2eAgMnPc1d1
tuků	tuk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
stresový	stresový	k2eAgInSc1d1
hormon	hormon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Chemie	chemie	k1gFnSc1
</s>
