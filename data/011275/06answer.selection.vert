<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
povedlo	povést	k5eAaPmAgNnS	povést
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
orbitální	orbitální	k2eAgFnPc4d1	orbitální
dráhy	dráha	k1gFnPc4	dráha
Země	zem	k1gFnSc2	zem
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
motorů	motor	k1gInPc2	motor
na	na	k7c4	na
kapalné	kapalný	k2eAgNnSc4d1	kapalné
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jako	jako	k8xC	jako
první	první	k4xOgFnSc2	první
soukromé	soukromý	k2eAgFnSc2d1	soukromá
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
povedlo	povést	k5eAaPmAgNnS	povést
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
vyslat	vyslat	k5eAaPmF	vyslat
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
stroj	stroj	k1gInSc1	stroj
a	a	k8xC	a
pak	pak	k6eAd1	pak
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
přistát	přistát	k5eAaPmF	přistát
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
