<s>
Filatelie	filatelie	k1gFnSc1	filatelie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
poštovními	poštovní	k2eAgFnPc7d1	poštovní
známkami	známka	k1gFnPc7	známka
a	a	k8xC	a
kolky	kolek	k1gInPc7	kolek
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
použitím	použití	k1gNnSc7	použití
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
vydání	vydání	k1gNnSc1	vydání
poštovními	poštovní	k2eAgInPc7d1	poštovní
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
filatelii	filatelie	k1gFnSc4	filatelie
se	s	k7c7	s
sbíráním	sbírání	k1gNnSc7	sbírání
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
filatelie	filatelie	k1gFnSc1	filatelie
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
širší	široký	k2eAgInSc4d2	širší
pojem	pojem	k1gInSc4	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
filatelista	filatelista	k1gMnSc1	filatelista
může	moct	k5eAaImIp3nS	moct
studovat	studovat	k5eAaImF	studovat
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgFnPc4d1	vzácná
známky	známka	k1gFnPc4	známka
a	a	k8xC	a
ani	ani	k8xC	ani
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
bude	být	k5eAaImBp3nS	být
vlastnit	vlastnit	k5eAaImF	vlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Některých	některý	k3yIgFnPc2	některý
známek	známka	k1gFnPc2	známka
totiž	totiž	k9	totiž
například	například	k6eAd1	například
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
exemplářů	exemplář	k1gInPc2	exemplář
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc6d1	soukromá
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výskyt	výskyt	k1gInSc1	výskyt
slova	slovo	k1gNnSc2	slovo
filatelie	filatelie	k1gFnSc2	filatelie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
podobě	podoba	k1gFnSc6	podoba
philatélie	philatélie	k1gFnSc2	philatélie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
jej	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgMnS	použít
Georges	Georges	k1gMnSc1	Georges
Herpin	Herpin	k1gMnSc1	Herpin
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Le	Le	k1gMnSc1	Le
Collectioneur	Collectioneur	k1gMnSc1	Collectioneur
de	de	k?	de
timbres-poste	timbresosit	k5eAaPmRp2nP	timbres-posit
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
filatelie	filatelie	k1gFnSc2	filatelie
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
philos	philos	k1gMnSc1	philos
(	(	kIx(	(
<g/>
přítel	přítel	k1gMnSc1	přítel
<g/>
)	)	kIx)	)
a	a	k8xC	a
ateleia	ateleia	k1gFnSc1	ateleia
(	(	kIx(	(
<g/>
zaplacení	zaplacení	k1gNnSc1	zaplacení
poplatku	poplatek	k1gInSc2	poplatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
i	i	k8xC	i
alternativy	alternativa	k1gFnPc1	alternativa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevžily	vžít	k5eNaPmAgFnP	vžít
jako	jako	k9	jako
timbrofilie	timbrofilie	k1gFnPc1	timbrofilie
nebo	nebo	k8xC	nebo
timbrologie	timbrologie	k1gFnPc1	timbrologie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
filatelii	filatelie	k1gFnSc3	filatelie
můžeme	moct	k5eAaImIp1nP	moct
ze	z	k7c2	z
zdánlivě	zdánlivě	k6eAd1	zdánlivě
stejně	stejně	k6eAd1	stejně
vypadajících	vypadající	k2eAgFnPc2d1	vypadající
známek	známka	k1gFnPc2	známka
podrobnějším	podrobný	k2eAgNnPc3d2	podrobnější
zkoumáním	zkoumání	k1gNnPc3	zkoumání
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jejich	jejich	k3xOp3gFnPc4	jejich
různé	různý	k2eAgFnPc4d1	různá
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
druhy	druh	k1gInPc1	druh
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
průsvitky	průsvitka	k1gFnPc1	průsvitka
(	(	kIx(	(
<g/>
vodoznaky	vodoznak	k1gInPc1	vodoznak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
variace	variace	k1gFnSc1	variace
v	v	k7c6	v
odstínu	odstín	k1gInSc6	odstín
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
zoubkování	zoubkování	k1gNnSc2	zoubkování
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Porovnáním	porovnání	k1gNnSc7	porovnání
se	se	k3xPyFc4	se
záznamy	záznam	k1gInPc1	záznam
vydávajících	vydávající	k2eAgInPc2d1	vydávající
poštovních	poštovní	k2eAgInPc2d1	poštovní
úřadů	úřad	k1gInPc2	úřad
můžeme	moct	k5eAaImIp1nP	moct
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgFnPc1	tento
odchylky	odchylka	k1gFnPc1	odchylka
byly	být	k5eAaImAgFnP	být
úmyslné	úmyslný	k2eAgFnPc1d1	úmyslná
<g/>
,	,	kIx,	,
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
bádáním	bádání	k1gNnSc7	bádání
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
a	a	k8xC	a
proč	proč	k6eAd1	proč
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
známka	známka	k1gFnSc1	známka
je	být	k5eAaImIp3nS	být
cenina	cenina	k1gFnSc1	cenina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
padělků	padělek	k1gInPc2	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
znalost	znalost	k1gFnSc1	znalost
filatelie	filatelie	k1gFnSc2	filatelie
je	být	k5eAaImIp3nS	být
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
filatelie	filatelie	k1gFnSc1	filatelie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
techniky	technika	k1gFnSc2	technika
výroby	výroba	k1gFnSc2	výroba
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Původní	původní	k2eAgInSc1d1	původní
proces	proces	k1gInSc1	proces
návrhu	návrh	k1gInSc6	návrh
Rytina	rytina	k1gFnSc1	rytina
Tisk	tisk	k1gInSc1	tisk
Papír	papír	k1gInSc1	papír
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
průsvitek	průsvitka	k1gFnPc2	průsvitka
<g/>
)	)	kIx)	)
Lep	lepit	k5eAaImRp2nS	lepit
Separace	separace	k1gFnSc1	separace
-	-	kIx~	-
oddělování	oddělování	k1gNnSc1	oddělování
(	(	kIx(	(
<g/>
zoubkování	zoubkování	k1gNnSc1	zoubkování
<g/>
)	)	kIx)	)
Přetisky	přetisk	k1gInPc1	přetisk
na	na	k7c6	na
již	již	k6eAd1	již
existujících	existující	k2eAgFnPc6d1	existující
známkách	známka	k1gFnPc6	známka
Znalecká	znalecký	k2eAgFnSc1d1	znalecká
(	(	kIx(	(
<g/>
soudní	soudní	k2eAgFnSc1d1	soudní
<g/>
)	)	kIx)	)
filatelie	filatelie	k1gFnSc1	filatelie
-	-	kIx~	-
identifikace	identifikace	k1gFnSc1	identifikace
padělků	padělek	k1gInPc2	padělek
Námětová	námětový	k2eAgFnSc1d1	námětová
filatelie	filatelie	k1gFnSc1	filatelie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
náměty	námět	k1gInPc7	námět
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
na	na	k7c6	na
známce	známka	k1gFnSc6	známka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
námět	námět	k1gInSc1	námět
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
téměř	téměř	k6eAd1	téměř
cokoliv	cokoliv	k3yInSc4	cokoliv
(	(	kIx(	(
<g/>
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námětová	námětový	k2eAgFnSc1d1	námětová
filatelie	filatelie	k1gFnSc1	filatelie
se	se	k3xPyFc4	se
například	například	k6eAd1	například
zabývá	zabývat	k5eAaImIp3nS	zabývat
chybami	chyba	k1gFnPc7	chyba
vzniklými	vzniklý	k2eAgFnPc7d1	vzniklá
už	už	k6eAd1	už
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
<g/>
,	,	kIx,	,
postupnými	postupný	k2eAgFnPc7d1	postupná
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
jaký	jaký	k3yIgInSc1	jaký
námět	námět	k1gInSc1	námět
použit	použít	k5eAaPmNgInS	použít
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1	poštovní
historie	historie	k1gFnSc1	historie
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
užití	užití	k1gNnSc4	užití
známek	známka	k1gFnPc2	známka
při	při	k7c6	při
doručování	doručování	k1gNnSc6	doručování
zásilek	zásilka	k1gFnPc2	zásilka
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
studiem	studio	k1gNnSc7	studio
poštovních	poštovní	k2eAgNnPc2d1	poštovní
razítek	razítko	k1gNnPc2	razítko
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
poštovních	poštovní	k2eAgFnPc2d1	poštovní
autorit	autorita	k1gFnPc2	autorita
a	a	k8xC	a
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgFnPc6	který
zásilka	zásilka	k1gFnSc1	zásilka
putuje	putovat	k5eAaImIp3nS	putovat
od	od	k7c2	od
odesilatele	odesilatel	k1gMnSc2	odesilatel
k	k	k7c3	k
adresátovi	adresát	k1gMnSc3	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Aerofilatelie	Aerofilatelie	k1gFnSc1	Aerofilatelie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
leteckou	letecký	k2eAgFnSc7d1	letecká
poštou	pošta	k1gFnSc7	pošta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zejména	zejména	k9	zejména
použitím	použití	k1gNnSc7	použití
leteckých	letecký	k2eAgFnPc2d1	letecká
známek	známka	k1gFnPc2	známka
a	a	k8xC	a
nálepek	nálepka	k1gFnPc2	nálepka
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
leteckých	letecký	k2eAgFnPc2d1	letecká
celistvostí	celistvost	k1gFnPc2	celistvost
apod.	apod.	kA	apod.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
o	o	k7c4	o
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
přepravu	přeprava	k1gFnSc4	přeprava
poštovních	poštovní	k2eAgFnPc2d1	poštovní
zásilek	zásilka	k1gFnPc2	zásilka
-	-	kIx~	-
tedy	tedy	k9	tedy
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
balónem	balón	k1gInSc7	balón
nebo	nebo	k8xC	nebo
vzducholodí	vzducholoď	k1gFnSc7	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
odštěpila	odštěpit	k5eAaPmAgFnS	odštěpit
astrofilatelie	astrofilatelie	k1gFnSc1	astrofilatelie
<g/>
.	.	kIx.	.
</s>
<s>
Geografilatelie	Geografilatelie	k1gFnSc1	Geografilatelie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vydanými	vydaný	k2eAgFnPc7d1	vydaná
známkami	známka	k1gFnPc7	známka
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zeměpisného	zeměpisný	k2eAgNnSc2d1	zeměpisné
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
a	a	k8xC	a
kým	kdo	k3yQnSc7	kdo
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
určité	určitý	k2eAgNnSc4d1	určité
území	území	k1gNnSc4	území
vydány	vydán	k2eAgInPc1d1	vydán
známky	známka	k1gFnPc4	známka
a	a	k8xC	a
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
naplněna	naplnit	k5eAaPmNgFnS	naplnit
poštovní	poštovní	k2eAgFnSc1d1	poštovní
funkce	funkce	k1gFnSc1	funkce
těchto	tento	k3xDgFnPc2	tento
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc3	označení
takových	takový	k3xDgNnPc2	takový
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
pojem	pojem	k1gInSc1	pojem
známková	známkový	k2eAgFnSc1d1	známková
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Filatelie	filatelie	k1gFnSc1	filatelie
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
rozpoznávání	rozpoznávání	k1gNnSc6	rozpoznávání
a	a	k8xC	a
studiu	studio	k1gNnSc6	studio
filatelistického	filatelistický	k2eAgInSc2d1	filatelistický
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
smysly	smysl	k1gInPc1	smysl
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
přesné	přesný	k2eAgFnPc1d1	přesná
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bychom	by	kYmCp1nP	by
někdy	někdy	k6eAd1	někdy
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jim	on	k3xPp3gMnPc3	on
musíme	muset	k5eAaImIp1nP	muset
pomáhat	pomáhat	k5eAaImF	pomáhat
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
průsvitky	průsvitka	k1gFnPc1	průsvitka
můžeme	moct	k5eAaImIp1nP	moct
snadno	snadno	k6eAd1	snadno
zjistit	zjistit	k5eAaPmF	zjistit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
známku	známka	k1gFnSc4	známka
podíváme	podívat	k5eAaPmIp1nP	podívat
proti	proti	k7c3	proti
světlu	světlo	k1gNnSc3	světlo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
musíme	muset	k5eAaImIp1nP	muset
použít	použít	k5eAaPmF	použít
speciální	speciální	k2eAgInSc4d1	speciální
zvýrazňovací	zvýrazňovací	k2eAgInSc4d1	zvýrazňovací
roztok	roztok	k1gInSc4	roztok
nebo	nebo	k8xC	nebo
ultrafialovou	ultrafialový	k2eAgFnSc4d1	ultrafialová
lampu	lampa	k1gFnSc4	lampa
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgNnPc1d1	využívající
světla	světlo	k1gNnPc1	světlo
určitého	určitý	k2eAgNnSc2d1	určité
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různá	různý	k2eAgNnPc1d1	různé
zvětšovací	zvětšovací	k2eAgNnPc1d1	zvětšovací
skla	sklo	k1gNnPc1	sklo
(	(	kIx(	(
<g/>
lupy	lupa	k1gFnSc2	lupa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zoubkoměry	zoubkoměr	k1gInPc1	zoubkoměr
nebo	nebo	k8xC	nebo
mikrometry	mikrometr	k1gInPc1	mikrometr
(	(	kIx(	(
<g/>
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
tloušťky	tloušťka	k1gFnSc2	tloušťka
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
lze	lze	k6eAd1	lze
také	také	k9	také
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
používat	používat	k5eAaImF	používat
počítač	počítač	k1gInSc4	počítač
včetně	včetně	k7c2	včetně
scanneru	scanner	k1gInSc2	scanner
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
libovolný	libovolný	k2eAgInSc4d1	libovolný
detail	detail	k1gInSc4	detail
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
rozlišit	rozlišit	k5eAaPmF	rozlišit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
typy	typ	k1gInPc4	typ
známek	známka	k1gFnPc2	známka
či	či	k8xC	či
razítek	razítko	k1gNnPc2	razítko
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
lze	lze	k6eAd1	lze
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
použít	použít	k5eAaPmF	použít
také	také	k9	také
u	u	k7c2	u
celistvostí	celistvost	k1gFnPc2	celistvost
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
českých	český	k2eAgMnPc2d1	český
filatelistů	filatelista	k1gMnPc2	filatelista
(	(	kIx(	(
<g/>
SČF	SČF	kA	SČF
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
filatelistická	filatelistický	k2eAgFnSc1d1	filatelistická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
Philatélie	Philatélie	k1gFnSc2	Philatélie
-	-	kIx~	-
FIP	FIP	kA	FIP
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
sdružení	sdružení	k1gNnSc1	sdružení
vydavatelů	vydavatel	k1gMnPc2	vydavatel
katalogů	katalog	k1gInPc2	katalog
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
alb	album	k1gNnPc2	album
a	a	k8xC	a
filatelistických	filatelistický	k2eAgFnPc2d1	filatelistická
publikací	publikace	k1gFnPc2	publikace
(	(	kIx(	(
<g/>
Association	Association	k1gInSc1	Association
Internationale	Internationale	k1gFnSc2	Internationale
des	des	k1gNnSc2	des
Editeurs	Editeursa	k1gFnPc2	Editeursa
de	de	k?	de
catalogues	catalogues	k1gMnSc1	catalogues
de	de	k?	de
timbres-poste	timbresosit	k5eAaPmRp2nP	timbres-posit
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
albums	albums	k1gInSc1	albums
<g />
.	.	kIx.	.
</s>
<s>
et	et	k?	et
de	de	k?	de
publications	publications	k1gInSc1	publications
philatéliques	philatéliques	k1gMnSc1	philatéliques
-	-	kIx~	-
ASCAT	ASCAT	kA	ASCAT
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
sdružení	sdružení	k1gNnSc1	sdružení
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
poštovními	poštovní	k2eAgFnPc7d1	poštovní
známkami	známka	k1gFnPc7	známka
(	(	kIx(	(
<g/>
International	International	k1gFnPc1	International
Federation	Federation	k1gInSc1	Federation
of	of	k?	of
Stamp	Stamp	k1gInSc1	Stamp
Dealers	Dealers	k1gInSc1	Dealers
Associations	Associations	k1gInSc1	Associations
-	-	kIx~	-
IFSDA	IFSDA	kA	IFSDA
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
sdružení	sdružení	k1gNnSc1	sdružení
filatelistických	filatelistický	k2eAgMnPc2d1	filatelistický
znalců	znalec	k1gMnPc2	znalec
((	((	k?	((
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Association	Association	k1gInSc1	Association
internationale	internationale	k6eAd1	internationale
des	des	k1gNnSc4	des
experts	experts	k6eAd1	experts
en	en	k?	en
philatélie	philatélie	k1gFnSc1	philatélie
-	-	kIx~	-
AIEP	AIEP	kA	AIEP
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Universal	Universal	k1gFnSc1	Universal
Postal	Postal	k1gMnSc1	Postal
Union	union	k1gInSc1	union
-	-	kIx~	-
UPU	UPU	kA	UPU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
OSN	OSN	kA	OSN
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
filatelie	filatelie	k1gFnSc2	filatelie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
filatelie	filatelie	k1gFnSc2	filatelie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Infofila	Infofil	k1gMnSc2	Infofil
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
online	onlinout	k5eAaPmIp3nS	onlinout
magazín	magazín	k1gInSc4	magazín
o	o	k7c6	o
filatelii	filatelie	k1gFnSc6	filatelie
NEWSPHILA	NEWSPHILA	kA	NEWSPHILA
informační	informační	k2eAgInSc4d1	informační
blog	blog	k1gInSc4	blog
o	o	k7c6	o
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
dění	dění	k1gNnSc6	dění
ve	v	k7c6	v
filatelii	filatelie	k1gFnSc6	filatelie
Japhila	Japhila	k1gFnSc1	Japhila
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cz	cz	k?	cz
online	onlinout	k5eAaPmIp3nS	onlinout
magazín	magazín	k1gInSc1	magazín
o	o	k7c4	o
filatelii	filatelie	k1gFnSc4	filatelie
Filaso	Filasa	k1gFnSc5	Filasa
Filaso	Filasa	k1gFnSc5	Filasa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
filatelisté	filatelista	k1gMnPc1	filatelista
sobě	se	k3xPyFc3	se
stránky	stránka	k1gFnPc4	stránka
sdružení	sdružení	k1gNnSc2	sdružení
sběratelů	sběratel	k1gMnPc2	sběratel
známkových	známkový	k2eAgFnPc2d1	známková
zemí	zem	k1gFnPc2	zem
GEOPHILA	GEOPHILA	kA	GEOPHILA
NEZ	NEZ	kA	NEZ
Nové	Nové	k2eAgFnSc2d1	Nové
Evropské	evropský	k2eAgFnSc2d1	Evropská
Známky	známka	k1gFnSc2	známka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
ASCAT	ASCAT	kA	ASCAT
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
IFSDA	IFSDA	kA	IFSDA
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
AIEP	AIEP	kA	AIEP
</s>
