<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
poštovními	poštovní	k2eAgFnPc7d1	poštovní
známkami	známka	k1gFnPc7	známka
a	a	k8xC	a
kolky	kolek	k1gInPc7	kolek
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
použitím	použití	k1gNnSc7	použití
<g/>
?	?	kIx.	?
</s>
