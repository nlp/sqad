<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc4d1	západogermánský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
němčina	němčina	k1gFnSc1	němčina
s	s	k7c7	s
asi	asi	k9	asi
100	[number]	k4	100
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluvčími	mluvčí	k1gFnPc7	mluvčí
druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
po	po	k7c6	po
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
nejpoužívanějším	používaný	k2eAgMnSc7d3	nejpoužívanější
mateřským	mateřský	k2eAgMnSc7d1	mateřský
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejpoužívanějších	používaný	k2eAgInPc2d3	nejpoužívanější
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
latinkou	latinka	k1gFnSc7	latinka
se	s	k7c7	s
spřežkovým	spřežkový	k2eAgInSc7d1	spřežkový
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
velkými	velký	k2eAgFnPc7d1	velká
počátečními	počáteční	k2eAgFnPc7d1	počáteční
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
reforma	reforma	k1gFnSc1	reforma
pravopisu	pravopis	k1gInSc2	pravopis
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
přepracována	přepracován	k2eAgFnSc1d1	přepracována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
80	[number]	k4	80
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
7,9	[number]	k4	7,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1	Lichtenštejnsko
(	(	kIx(	(
<g/>
31	[number]	k4	31
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
,	,	kIx,	,
italštinou	italština	k1gFnSc7	italština
a	a	k8xC	a
rétorománštinou	rétorománština	k1gFnSc7	rétorománština
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
(	(	kIx(	(
<g/>
270	[number]	k4	270
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lucemburštinou	lucemburština	k1gFnSc7	lucemburština
a	a	k8xC	a
francouzštinou	francouzština	k1gFnSc7	francouzština
Belgie	Belgie	k1gFnSc2	Belgie
(	(	kIx(	(
<g/>
110	[number]	k4	110
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Německojazyčné	německojazyčný	k2eAgNnSc4d1	německojazyčné
společenství	společenství	k1gNnSc4	společenství
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Lutych	Lutycha	k1gFnPc2	Lutycha
<g />
.	.	kIx.	.
</s>
<s>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nizozemštinou	nizozemština	k1gFnSc7	nizozemština
a	a	k8xC	a
francouzštinou	francouzština	k1gFnSc7	francouzština
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
260	[number]	k4	260
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Jižní	jižní	k2eAgNnSc4d1	jižní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
(	(	kIx(	(
<g/>
autonomní	autonomní	k2eAgFnSc2d1	autonomní
provincie	provincie	k1gFnSc2	provincie
Bolzano	Bolzana	k1gFnSc5	Bolzana
<g/>
)	)	kIx)	)
Německy	německy	k6eAd1	německy
dále	daleko	k6eAd2	daleko
hovoří	hovořit	k5eAaImIp3nP	hovořit
menšiny	menšina	k1gFnPc1	menšina
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Alsasko	Alsasko	k1gNnSc1	Alsasko
a	a	k8xC	a
Lotrinsko	Lotrinsko	k1gNnSc1	Lotrinsko
<g />
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
25	[number]	k4	25
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Šlesvicko	Šlesvicko	k1gNnSc1	Šlesvicko
(	(	kIx(	(
<g/>
region	region	k1gInSc1	region
Syddanmark	Syddanmark	k1gInSc1	Syddanmark
<g/>
)	)	kIx)	)
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
380	[number]	k4	380
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Banát	Banát	k1gInSc1	Banát
<g/>
,	,	kIx,	,
Sedmihradsko	Sedmihradsko	k1gNnSc1	Sedmihradsko
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
200	[number]	k4	200
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Polsko	Polsko	k1gNnSc1	Polsko
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
500	[number]	k4	500
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
SNS	SNS	kA	SNS
(	(	kIx(	(
<g/>
1,9	[number]	k4	1,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
Povolží	Povolží	k1gNnSc1	Povolží
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
zejména	zejména	k9	zejména
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Dakota	Dakota	k1gFnSc1	Dakota
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
560	[number]	k4	560
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
400	[number]	k4	400
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
135	[number]	k4	135
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Paraguay	Paraguay	k1gFnSc4	Paraguay
(	(	kIx(	(
<g/>
38	[number]	k4	38
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Namibie	Namibie	k1gFnSc1	Namibie
(	(	kIx(	(
<g/>
25	[number]	k4	25
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
do	do	k7c2	do
r.	r.	kA	r.
1989	[number]	k4	1989
regionální	regionální	k2eAgInSc4d1	regionální
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
JAR	jar	k1gFnSc1	jar
(	(	kIx(	(
<g/>
60	[number]	k4	60
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
mluv	mluv	k1gInSc1	mluv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
<g />
.	.	kIx.	.
</s>
<s>
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
mluv	mluva	k1gFnPc2	mluva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
mluv	mluva	k1gFnPc2	mluva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
až	až	k6eAd1	až
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
mluv	mluva	k1gFnPc2	mluva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
–	–	k?	–
(	(	kIx(	(
<g/>
unserdeutsch	unserdeutsch	k1gMnSc1	unserdeutsch
<g/>
)	)	kIx)	)
kreolský	kreolský	k2eAgInSc1d1	kreolský
jazyk	jazyk	k1gInSc1	jazyk
na	na	k7c6	na
základě	základ	k1gInSc6	základ
němčiny	němčina	k1gFnSc2	němčina
Počty	počet	k1gInPc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
menšin	menšina	k1gFnPc2	menšina
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nutné	nutný	k2eAgNnSc1d1	nutné
brát	brát	k5eAaImF	brát
s	s	k7c7	s
rezervou	rezerva	k1gFnSc7	rezerva
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
průzkumů	průzkum	k1gInPc2	průzkum
mohou	moct	k5eAaImIp3nP	moct
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
především	především	k9	především
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
došlo	dojít	k5eAaPmAgNnS	dojít
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
vlasti	vlast	k1gFnSc2	vlast
Německem	Německo	k1gNnSc7	Německo
po	po	k7c6	po
r.	r.	kA	r.
1990	[number]	k4	1990
k	k	k7c3	k
rozhodujícímu	rozhodující	k2eAgInSc3d1	rozhodující
odlivu	odliv	k1gInSc3	odliv
etnických	etnický	k2eAgMnPc2d1	etnický
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
reprodukčně	reprodukčně	k6eAd1	reprodukčně
perspektivním	perspektivní	k2eAgInSc6d1	perspektivní
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tam	tam	k6eAd1	tam
lze	lze	k6eAd1	lze
během	během	k7c2	během
příštích	příští	k2eAgNnPc2d1	příští
desetiletí	desetiletí	k1gNnPc2	desetiletí
očekávat	očekávat	k5eAaImF	očekávat
praktický	praktický	k2eAgInSc4d1	praktický
zánik	zánik	k1gInSc4	zánik
výskytu	výskyt	k1gInSc2	výskyt
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
standardizovanou	standardizovaný	k2eAgFnSc7d1	standardizovaná
podobou	podoba	k1gFnSc7	podoba
hornoněmčiny	hornoněmčina	k1gFnSc2	hornoněmčina
(	(	kIx(	(
<g/>
Hochdeutsch	Hochdeutsch	k1gInSc1	Hochdeutsch
<g/>
,	,	kIx,	,
hornoněmecké	hornoněmecký	k2eAgFnPc1d1	hornoněmecký
nářeční	nářeční	k2eAgFnPc1d1	nářeční
skupiny	skupina	k1gFnPc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
liší	lišit	k5eAaImIp3nP	lišit
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
druhého	druhý	k4xOgNnSc2	druhý
posouvání	posouvání	k1gNnSc2	posouvání
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
vývoje	vývoj	k1gInSc2	vývoj
spisovného	spisovný	k2eAgInSc2d1	spisovný
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tato	tento	k3xDgNnPc1	tento
období	období	k1gNnPc1	období
<g/>
:	:	kIx,	:
prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
<g/>
/	/	kIx~	/
<g/>
germánské	germánský	k2eAgFnSc2d1	germánská
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
stará	starý	k2eAgFnSc1d1	stará
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
Althochdeutsch	Althochdeutsch	k1gInSc1	Althochdeutsch
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
–	–	k?	–
<g/>
1050	[number]	k4	1050
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
střední	střední	k2eAgFnSc1d1	střední
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
Mittelhochdeutsch	Mittelhochdeutsch	k1gInSc1	Mittelhochdeutsch
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1050	[number]	k4	1050
<g/>
–	–	k?	–
<g/>
1350	[number]	k4	1350
<g/>
)	)	kIx)	)
raná	raný	k2eAgFnSc1d1	raná
nová	nový	k2eAgFnSc1d1	nová
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
Frühneuhochdeutsch	Frühneuhochdeutsch	k1gInSc1	Frühneuhochdeutsch
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
nová	nový	k2eAgFnSc1d1	nová
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
Neuhochdeutsch	Neuhochdeutsch	k1gInSc1	Neuhochdeutsch
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Základní	základní	k2eAgNnSc1d1	základní
členění	členění	k1gNnSc1	členění
nářečí	nářečí	k1gNnSc2	nářečí
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
druhého	druhý	k4xOgNnSc2	druhý
posouvání	posouvání	k1gNnSc2	posouvání
hlásek	hláska	k1gFnPc2	hláska
v	v	k7c6	v
jihoseverním	jihoseverní	k2eAgInSc6d1	jihoseverní
směru	směr	k1gInSc6	směr
<g/>
:	:	kIx,	:
horní	horní	k2eAgFnSc1d1	horní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
rakouské	rakouský	k2eAgFnSc2d1	rakouská
a	a	k8xC	a
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
–	–	k?	–
úplné	úplný	k2eAgFnSc3d1	úplná
střední	střední	k2eAgFnSc3d1	střední
němčina	němčina	k1gFnSc1	němčina
–	–	k?	–
částečné	částečný	k2eAgFnSc3d1	částečná
Dolní	dolní	k2eAgFnSc1d1	dolní
němčina	němčina	k1gFnSc1	němčina
(	(	kIx(	(
<g/>
Platt	Platt	k1gMnSc1	Platt
<g/>
)	)	kIx)	)
–	–	k?	–
žádné	žádný	k3yNgNnSc1	žádný
posouvání	posouvání	k1gNnSc1	posouvání
hlásek	hláska	k1gFnPc2	hláska
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
nářečími	nářečí	k1gNnPc7	nářečí
a	a	k8xC	a
dialekty	dialekt	k1gInPc7	dialekt
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dialekt	dialekt	k1gInSc1	dialekt
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnohého	mnohý	k2eAgMnSc4d1	mnohý
Němce	Němec	k1gMnSc4	Němec
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
oblasti	oblast	k1gFnSc2	oblast
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc1d1	značná
potíže	potíž	k1gFnPc1	potíž
pak	pak	k6eAd1	pak
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
severoněmecké	severoněmecký	k2eAgNnSc4d1	severoněmecké
tzv.	tzv.	kA	tzv.
Plattdütsch	Plattdütsch	k1gMnSc1	Plattdütsch
z	z	k7c2	z
Fríska	Frísko	k1gNnSc2	Frísko
nebo	nebo	k8xC	nebo
o	o	k7c4	o
dialekty	dialekt	k1gInPc4	dialekt
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
Bádenska	Bádensko	k1gNnSc2	Bádensko
či	či	k8xC	či
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
nesrozumitelné	srozumitelný	k2eNgNnSc1d1	nesrozumitelné
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
Schwyzerdüütsch	Schwyzerdüütscha	k1gFnPc2	Schwyzerdüütscha
<g/>
,	,	kIx,	,
mluvené	mluvený	k2eAgFnSc2d1	mluvená
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
:	:	kIx,	:
švýcarské	švýcarský	k2eAgInPc1d1	švýcarský
filmy	film	k1gInPc1	film
nebo	nebo	k8xC	nebo
televizní	televizní	k2eAgInPc1d1	televizní
rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
,	,	kIx,	,
vysílané	vysílaný	k2eAgInPc1d1	vysílaný
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
podtitulky	podtitulek	k1gInPc4	podtitulek
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
již	již	k6eAd1	již
v	v	k7c6	v
originále	originál	k1gInSc6	originál
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
s	s	k7c7	s
týmiž	týž	k3xTgMnPc7	týž
herci	herec	k1gMnPc7	herec
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
-	-	kIx~	-
pro	pro	k7c4	pro
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
a	a	k8xC	a
pro	pro	k7c4	pro
export	export	k1gInSc4	export
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
dabovány	dabován	k2eAgInPc4d1	dabován
do	do	k7c2	do
spisovné	spisovný	k2eAgFnSc2d1	spisovná
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přehlasovaných	přehlasovaný	k2eAgInPc2d1	přehlasovaný
Ä	Ä	kA	Ä
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
<g/>
,	,	kIx,	,
Ü	Ü	kA	Ü
nevyužívá	využívat	k5eNaImIp3nS	využívat
diakritická	diakritický	k2eAgNnPc4d1	diakritické
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
tzv.	tzv.	kA	tzv.
ostrého	ostrý	k2eAgNnSc2d1	ostré
S	s	k7c7	s
(	(	kIx(	(
<g/>
scharfes	scharfes	k1gMnSc1	scharfes
S	s	k7c7	s
<g/>
,	,	kIx,	,
ß	ß	k?	ß
–	–	k?	–
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
verzálkami	verzálka	k1gFnPc7	verzálka
přepisováno	přepisovat	k5eAaImNgNnS	přepisovat
jako	jako	k9	jako
SS	SS	kA	SS
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
reformou	reforma	k1gFnSc7	reforma
pravopisu	pravopis	k1gInSc2	pravopis
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
omezeno	omezen	k2eAgNnSc1d1	omezeno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
se	s	k7c7	s
ß	ß	k?	ß
dokonce	dokonce	k9	dokonce
jako	jako	k9	jako
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
důsledně	důsledně	k6eAd1	důsledně
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
za	za	k7c4	za
ss	ss	k?	ss
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
nejběžnější	běžný	k2eAgFnSc2d3	nejběžnější
odchylky	odchylka	k1gFnSc2	odchylka
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
ä	ä	k?	ä
–	–	k?	–
e	e	k0	e
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
;	;	kIx,	;
älter	älter	k1gMnSc1	älter
<g/>
,	,	kIx,	,
spät	spät	k1gMnSc1	spät
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ck	ck	k?	ck
–	–	k?	–
k	k	k7c3	k
(	(	kIx(	(
<g/>
packen	packen	k2eAgMnSc1d1	packen
<g/>
,	,	kIx,	,
Stück	Stück	k1gMnSc1	Stück
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
chs	chs	k?	chs
–	–	k?	–
ks	ks	kA	ks
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
chs	chs	k?	chs
<g/>
,	,	kIx,	,
když	když	k8xS	když
-s-	-	k?	-s-
část	část	k1gFnSc1	část
koncovky	koncovka	k1gFnSc2	koncovka
(	(	kIx(	(
<g/>
du	du	k?	du
machst	machst	k1gInSc1	machst
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
e	e	k0	e
–	–	k?	–
když	když	k8xS	když
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
í	í	k0	í
(	(	kIx(	(
<g/>
gewehr	gewehr	k1gInSc4	gewehr
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
krátké	krátká	k1gFnPc1	krátká
v	v	k7c6	v
nepřízvučné	přízvučný	k2eNgFnSc6d1	nepřízvučná
slabice	slabika	k1gFnSc6	slabika
oslabeno	oslaben	k2eAgNnSc1d1	oslabeno
<g/>
,	,	kIx,	,
před	před	k7c7	před
l	l	kA	l
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
r	r	kA	r
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vysloveno	vysloven	k2eAgNnSc4d1	vysloveno
<g/>
)	)	kIx)	)
mizí	mizet	k5eAaImIp3nS	mizet
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemizí	mizet	k5eNaImIp3nS	mizet
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
koncovkach	koncovka	k1gFnPc6	koncovka
-eln	ln	k1gMnSc1	-eln
<g/>
,	,	kIx,	,
-ern	rn	k1gMnSc1	-ern
po	po	k7c6	po
r	r	kA	r
a	a	k8xC	a
l	l	kA	l
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
-en	n	k?	-en
po	po	k7c6	po
n	n	k0	n
<g/>
;	;	kIx,	;
ei	ei	k?	ei
<g/>
,	,	kIx,	,
ai	ai	k?	ai
<g/>
,	,	kIx,	,
ey	ey	k?	ey
<g/>
,	,	kIx,	,
ay	ay	k?	ay
–	–	k?	–
aj	aj	kA	aj
(	(	kIx(	(
<g/>
ein	ein	k?	ein
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
klein	klein	k1gInSc1	klein
<g/>
,	,	kIx,	,
drei	drei	k1gNnSc1	drei
<g/>
,	,	kIx,	,
Mai	Mai	k1gFnSc1	Mai
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
eu	eu	k?	eu
<g/>
,	,	kIx,	,
äu	äu	k?	äu
–	–	k?	–
oj	oj	k1gInSc1	oj
(	(	kIx(	(
<g/>
euch	euch	k1gInSc1	euch
<g/>
,	,	kIx,	,
neu	neu	k?	neu
<g/>
,	,	kIx,	,
räuspern	räuspern	k1gMnSc1	räuspern
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
g	g	kA	g
–	–	k?	–
ch	ch	k0	ch
-	-	kIx~	-
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
flugzeug	flugzeug	k1gInSc1	flugzeug
<g/>
)	)	kIx)	)
h	h	k?	h
–	–	k?	–
po	po	k7c6	po
samohlásce	samohláska	k1gFnSc6	samohláska
a	a	k8xC	a
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
předchozí	předchozí	k2eAgFnSc4d1	předchozí
samohlásku	samohláska	k1gFnSc4	samohláska
(	(	kIx(	(
<g/>
Ruhe	Ruhe	k1gFnSc1	Ruhe
<g/>
,	,	kIx,	,
gehen	gehen	k1gInSc1	gehen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
souhláska	souhláska	k1gFnSc1	souhláska
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
(	(	kIx(	(
<g/>
aha	aha	k0	aha
<g/>
,	,	kIx,	,
Uhu	Uhu	k1gMnSc1	Uhu
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ie	ie	k?	ie
i	i	k8xC	i
<g/>
:	:	kIx,	:
–	–	k?	–
(	(	kIx(	(
<g/>
viel	viel	k1gMnSc1	viel
<g/>
,	,	kIx,	,
sieben	sieben	k2eAgMnSc1d1	sieben
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ije	ije	k?	ije
<g/>
:	:	kIx,	:
–	–	k?	–
(	(	kIx(	(
<g/>
Familie	Familie	k1gFnSc1	Familie
<g/>
,	,	kIx,	,
Linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
Italien	Italien	k1gInSc1	Italien
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ng	ng	k?	ng
–	–	k?	–
podobné	podobný	k2eAgFnPc4d1	podobná
českému	český	k2eAgMnSc3d1	český
mango	mango	k1gNnSc1	mango
<g/>
,	,	kIx,	,
banka	banka	k1gFnSc1	banka
(	(	kIx(	(
<g/>
singen	singen	k1gInSc1	singen
<g/>
,	,	kIx,	,
lange	lange	k1gFnSc1	lange
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c6	o
–	–	k?	–
když	když	k8xS	když
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
ú	ú	k0	ú
(	(	kIx(	(
<g/>
holen	holen	k2eAgInSc4d1	holen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ö	ö	k?	ö
–	–	k?	–
mezi	mezi	k7c7	mezi
o	o	k7c4	o
a	a	k8xC	a
e	e	k0	e
(	(	kIx(	(
<g/>
können	können	k1gInSc1	können
<g/>
)	)	kIx)	)
ph	ph	kA	ph
–	–	k?	–
f	f	k?	f
(	(	kIx(	(
<g/>
Philosophie	Philosophie	k1gFnSc1	Philosophie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
r	r	kA	r
–	–	k?	–
téměř	téměř	k6eAd1	téměř
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
e	e	k0	e
(	(	kIx(	(
<g/>
aber	aber	k1gMnSc1	aber
<g/>
,	,	kIx,	,
liefern	liefern	k1gMnSc1	liefern
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
s	s	k7c7	s
–	–	k?	–
z	z	k7c2	z
–	–	k?	–
(	(	kIx(	(
<g/>
Seite	Seit	k1gInSc5	Seit
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
ss	ss	k?	ss
–	–	k?	–
s	s	k7c7	s
–	–	k?	–
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
neužije	užít	k5eNaPmIp3nS	užít
ß	ß	k?	ß
(	(	kIx(	(
<g/>
Strasse	Strasse	k1gFnSc1	Strasse
–	–	k?	–
např.	např.	kA	např.
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
nebo	nebo	k8xC	nebo
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
-	-	kIx~	-
<g/>
li	li	k8xS	li
ß	ß	k?	ß
na	na	k7c6	na
klávesnici	klávesnice	k1gFnSc6	klávesnice
či	či	k8xC	či
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ß	ß	k?	ß
–	–	k?	–
s	s	k7c7	s
–	–	k?	–
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
při	při	k7c6	při
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g />
.	.	kIx.	.
</s>
<s>
předchozí	předchozí	k2eAgFnPc1d1	předchozí
samohlásky	samohláska	k1gFnPc1	samohláska
(	(	kIx(	(
<g/>
Straße	Straße	k1gFnSc1	Straße
<g/>
,	,	kIx,	,
Fuß	Fuß	k1gFnSc1	Fuß
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
sch	sch	k?	sch
–	–	k?	–
š	š	k?	š
(	(	kIx(	(
<g/>
schön	schön	k1gMnSc1	schön
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
sp	sp	k?	sp
–	–	k?	–
šp	šp	k?	šp
–	–	k?	–
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
kořenů	kořen	k1gInPc2	kořen
(	(	kIx(	(
<g/>
spät	spät	k5eAaPmF	spät
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
st	st	kA	st
–	–	k?	–
št	št	k?	št
–	–	k?	–
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
kořenů	kořen	k1gInPc2	kořen
(	(	kIx(	(
<g/>
stehen	stehno	k1gNnPc2	stehno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tion	tion	k1gInSc1	tion
–	–	k?	–
cijon	cijon	k1gInSc1	cijon
(	(	kIx(	(
<g/>
Funktion	Funktion	k1gInSc1	Funktion
<g/>
,	,	kIx,	,
Nation	Nation	k1gInSc1	Nation
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tsch	tsch	k1gInSc1	tsch
–	–	k?	–
č	č	k0	č
(	(	kIx(	(
<g/>
tschechisch	tschechisch	k1gInSc4	tschechisch
<g/>
)	)	kIx)	)
;	;	kIx,	;
z	z	k0	z
<g/>
,	,	kIx,	,
tz	tz	k?	tz
–	–	k?	–
c	c	k0	c
(	(	kIx(	(
<g/>
sitzen	sitzna	k1gFnPc2	sitzna
<g/>
,	,	kIx,	,
zehn	zehna	k1gFnPc2	zehna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ü	ü	k?	ü
–	–	k?	–
mezi	mezi	k7c7	mezi
u	u	k7c2	u
a	a	k8xC	a
i	i	k8xC	i
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
müssen	müssen	k1gInSc1	müssen
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
f	f	k?	f
(	(	kIx(	(
<g/>
Vater	vatra	k1gFnPc2	vatra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
–	–	k?	–
v	v	k7c4	v
(	(	kIx(	(
<g/>
Vase	Vase	k1gFnSc4	Vase
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
w	w	k?	w
–	–	k?	–
v	v	k7c4	v
(	(	kIx(	(
<g/>
warten	warten	k2eAgInSc4d1	warten
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kmenová	kmenový	k2eAgFnSc1d1	kmenová
samohláska	samohláska	k1gFnSc1	samohláska
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
před	před	k7c7	před
jedinou	jediný	k2eAgFnSc7d1	jediná
souhláskou	souhláska	k1gFnSc7	souhláska
(	(	kIx(	(
<g/>
lesen	lesen	k2eAgMnSc1d1	lesen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc4	žádný
(	(	kIx(	(
<g/>
wo	wo	k?	wo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Německá	německý	k2eAgFnSc1d1	německá
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
určitý	určitý	k2eAgInSc1d1	určitý
(	(	kIx(	(
<g/>
der	drát	k5eAaImRp2nS	drát
<g/>
,	,	kIx,	,
die	die	k?	die
<g/>
,	,	kIx,	,
das	das	k?	das
<g/>
)	)	kIx)	)
a	a	k8xC	a
neurčitý	určitý	k2eNgInSc1d1	neurčitý
(	(	kIx(	(
<g/>
ein	ein	k?	ein
<g/>
,	,	kIx,	,
eine	eine	k1gFnSc1	eine
<g/>
)	)	kIx)	)
člen	člen	k1gMnSc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgInPc1	čtyři
pády	pád	k1gInPc1	pád
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
a	a	k8xC	a
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřen	k2eAgFnPc1d1	vyjádřena
většinou	většina	k1gFnSc7	většina
jen	jen	k6eAd1	jen
tvarem	tvar	k1gInSc7	tvar
členu	člen	k1gInSc2	člen
<g/>
,	,	kIx,	,
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
ale	ale	k9	ale
patří	patřit	k5eAaImIp3nS	patřit
koncovky	koncovka	k1gFnPc4	koncovka
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnSc1d2	bohatší
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
systém	systém	k1gInSc1	systém
významu	význam	k1gInSc2	význam
6	[number]	k4	6
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
předminulý	předminulý	k2eAgInSc1d1	předminulý
[	[	kIx(	[
<g/>
plusquamperfektum	plusquamperfektum	k1gNnSc1	plusquamperfektum
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc1d1	minulý
[	[	kIx(	[
<g/>
préteritum	préteritum	k1gNnSc1	préteritum
<g/>
]]	]]	k?	]]
a	a	k8xC	a
[	[	kIx(	[
<g/>
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
[	[	kIx(	[
<g/>
prézens	prézens	k1gInSc1	prézens
<g/>
]	]	kIx)	]
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc1d1	budoucí
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
[	[	kIx(	[
<g/>
futurum	futurum	k1gNnSc1	futurum
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
tu	ten	k3xDgFnSc4	ten
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
<g/>
,	,	kIx,	,
minulosti	minulost	k1gFnSc6	minulost
nebo	nebo	k8xC	nebo
budoucnosti	budoucnost	k1gFnSc6	budoucnost
o	o	k7c4	o
ukončený	ukončený	k2eAgInSc4d1	ukončený
či	či	k8xC	či
probíhající	probíhající	k2eAgInSc4d1	probíhající
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
využívá	využívat	k5eAaImIp3nS	využívat
velmi	velmi	k6eAd1	velmi
omezeně	omezeně	k6eAd1	omezeně
a	a	k8xC	a
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
konverzaci	konverzace	k1gFnSc6	konverzace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
již	již	k6eAd1	již
jen	jen	k9	jen
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
též	též	k9	též
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
minulé	minulý	k2eAgInPc4d1	minulý
časy	čas	k1gInPc4	čas
perfektum	perfektum	k1gNnSc4	perfektum
a	a	k8xC	a
préteritum	préteritum	k1gNnSc4	préteritum
<g/>
.	.	kIx.	.
</s>
<s>
Podmíněný	podmíněný	k2eAgInSc1d1	podmíněný
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
konjunktivem	konjunktiv	k1gInSc7	konjunktiv
a	a	k8xC	a
kondicionálem	kondicionál	k1gInSc7	kondicionál
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
trpného	trpný	k2eAgInSc2d1	trpný
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
flexibilnosti	flexibilnost	k1gFnSc3	flexibilnost
jazyka	jazyk	k1gInSc2	jazyk
mnohem	mnohem	k6eAd1	mnohem
volnější	volný	k2eAgFnSc1d2	volnější
než	než	k8xS	než
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
existují	existovat	k5eAaImIp3nP	existovat
jistá	jistý	k2eAgNnPc1d1	jisté
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
variant	varianta	k1gFnPc2	varianta
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
použitých	použitý	k2eAgFnPc6d1	použitá
spojkách	spojka	k1gFnPc6	spojka
či	či	k8xC	či
příslovečných	příslovečný	k2eAgNnPc6d1	příslovečné
určeních	určení	k1gNnPc6	určení
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
němčiny	němčina	k1gFnSc2	němčina
je	být	k5eAaImIp3nS	být
odsouvání	odsouvání	k1gNnSc1	odsouvání
ostatních	ostatní	k2eAgInPc2d1	ostatní
slovesných	slovesný	k2eAgInPc2d1	slovesný
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
hlavního	hlavní	k2eAgNnSc2d1	hlavní
slovesa	sloveso	k1gNnSc2	sloveso
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
větě	věta	k1gFnSc6	věta
<g/>
)	)	kIx)	)
na	na	k7c4	na
konec	konec	k1gInSc4	konec
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
sejít	sejít	k5eAaPmF	sejít
i	i	k9	i
několik	několik	k4yIc4	několik
sloves	sloveso	k1gNnPc2	sloveso
najednou	najednou	k6eAd1	najednou
<g/>
:	:	kIx,	:
Wenn	Wenn	k1gInSc1	Wenn
ich	ich	k?	ich
das	das	k?	das
früher	frühra	k1gFnPc2	frühra
gewusst	gewusst	k5eAaPmF	gewusst
hätte	hätte	k5eAaPmIp2nP	hätte
<g/>
,	,	kIx,	,
hätte	hätte	k5eAaPmIp2nP	hätte
ich	ich	k?	ich
es	es	k1gNnSc1	es
nicht	nicht	k1gMnSc1	nicht
mehr	mehr	k1gMnSc1	mehr
tun	tuna	k1gFnPc2	tuna
müssen	müssna	k1gFnPc2	müssna
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
Když	když	k8xS	když
já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
dříve	dříve	k6eAd2	dříve
věděl	vědět	k5eAaImAgMnS	vědět
by	by	k9	by
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
ne	ne	k9	ne
více	hodně	k6eAd2	hodně
činit	činit	k5eAaImF	činit
muset	muset	k5eAaImF	muset
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
hlavně	hlavně	k6eAd1	hlavně
latinou	latina	k1gFnSc7	latina
a	a	k8xC	a
románskými	románský	k2eAgInPc7d1	románský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
silný	silný	k2eAgInSc1d1	silný
vliv	vliv	k1gInSc1	vliv
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Výpůjček	výpůjčka	k1gFnPc2	výpůjčka
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
však	však	k9	však
k	k	k7c3	k
většině	většina	k1gFnSc3	většina
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
německý	německý	k2eAgInSc1d1	německý
protějšek	protějšek	k1gInSc1	protějšek
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
složeninu	složenina	k1gFnSc4	složenina
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
magnetofon	magnetofon	k1gInSc1	magnetofon
<g/>
:	:	kIx,	:
r	r	kA	r
Rekorder	Rekorder	k1gInSc1	Rekorder
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
s	s	k7c7	s
Tonbandgerät	Tonbandgeräta	k1gFnPc2	Tonbandgeräta
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zvukopáskopřístroj	zvukopáskopřístroj	k1gInSc1	zvukopáskopřístroj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byznys	byznys	k1gInSc1	byznys
<g/>
,	,	kIx,	,
podnikání	podnikání	k1gNnSc1	podnikání
<g/>
:	:	kIx,	:
s	s	k7c7	s
Business	business	k1gInSc1	business
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
Geschäft	Geschäftum	k1gNnPc2	Geschäftum
Zvláštností	zvláštnost	k1gFnPc2	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
budí	budit	k5eAaImIp3nP	budit
dojem	dojem	k1gInSc4	dojem
neněmeckého	německý	k2eNgMnSc2d1	neněmecký
<g/>
,	,	kIx,	,
anglického	anglický	k2eAgInSc2d1	anglický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Handy	Hando	k1gNnPc7	Hando
-	-	kIx~	-
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
čistě	čistě	k6eAd1	čistě
německá	německý	k2eAgFnSc1d1	německá
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
z	z	k7c2	z
s	s	k7c7	s
Handfernsprecher	Handfernsprechra	k1gFnPc2	Handfernsprechra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
telefon	telefon	k1gInSc1	telefon
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
anglicky	anglicky	k6eAd1	anglicky
händy	händy	k6eAd1	händy
<g/>
.	.	kIx.	.
</s>
<s>
Výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
ovšem	ovšem	k9	ovšem
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgNnPc1d1	německé
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
slovo	slovo	k1gNnSc1	slovo
'	'	kIx"	'
<g/>
r	r	kA	r
'	'	kIx"	'
<g/>
Bauplan	Bauplan	k1gMnSc1	Bauplan
(	(	kIx(	(
<g/>
stavební	stavební	k2eAgInSc1d1	stavební
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
skica	skica	k1gFnSc1	skica
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
technické	technický	k2eAgNnSc4d1	technické
označení	označení	k1gNnSc4	označení
morfologie	morfologie	k1gFnSc2	morfologie
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Arbeito	Arbeita	k1gMnSc5	Arbeita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
znamená	znamenat	k5eAaImIp3nS	znamenat
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
pracovní	pracovní	k2eAgInSc1d1	pracovní
poměr	poměr	k1gInSc1	poměr
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
Arbeit	Arbeita	k1gFnPc2	Arbeita
<g/>
,	,	kIx,	,
f.	f.	k?	f.
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
