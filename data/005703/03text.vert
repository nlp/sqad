<s>
Relikviář	relikviář	k1gInSc1	relikviář
je	být	k5eAaImIp3nS	být
schránka	schránka	k1gFnSc1	schránka
na	na	k7c4	na
relikvie	relikvie	k1gFnPc4	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uloženy	uložit	k5eAaPmNgFnP	uložit
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
vystaveny	vystavit	k5eAaPmNgInP	vystavit
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
těl	tělo	k1gNnPc2	tělo
světců	světec	k1gMnPc2	světec
nebo	nebo	k8xC	nebo
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
Ježíšem	Ježíš	k1gMnSc7	Ježíš
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Relikviář	relikviář	k1gInSc1	relikviář
měl	mít	k5eAaImAgInS	mít
devoční	devoční	k2eAgInSc4d1	devoční
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uchovával	uchovávat	k5eAaImAgInS	uchovávat
ostatky	ostatek	k1gInPc4	ostatek
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
jakousi	jakýsi	k3yIgFnSc4	jakýsi
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Dotýkali	dotýkat	k5eAaImAgMnP	dotýkat
se	se	k3xPyFc4	se
relikviářů	relikviář	k1gInPc2	relikviář
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
mělo	mít	k5eAaImAgNnS	mít
přinést	přinést	k5eAaPmF	přinést
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
modlitbou	modlitba	k1gFnSc7	modlitba
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
požehnání	požehnání	k1gNnSc4	požehnání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolických	katolický	k2eAgInPc6d1	katolický
kostelech	kostel	k1gInPc6	kostel
jsou	být	k5eAaImIp3nP	být
relikviáře	relikviář	k1gInPc1	relikviář
celkem	celkem	k6eAd1	celkem
běžné	běžný	k2eAgInPc1d1	běžný
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
drobných	drobný	k2eAgInPc2d1	drobný
ostatkových	ostatkový	k2eAgInPc2d1	ostatkový
relikviářů	relikviář	k1gInPc2	relikviář
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
drobné	drobný	k2eAgFnSc2d1	drobná
monstrance	monstrance	k1gFnSc2	monstrance
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
uchovávány	uchováván	k2eAgInPc1d1	uchováván
drobné	drobný	k2eAgInPc1d1	drobný
ostatky	ostatek	k1gInPc1	ostatek
světců	světec	k1gMnPc2	světec
(	(	kIx(	(
<g/>
kůstky	kůstka	k1gFnPc1	kůstka
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
oděvu	oděv	k1gInSc2	oděv
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
částky	částka	k1gFnPc1	částka
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
dotýkané	dotýkaný	k2eAgFnSc2d1	dotýkaná
se	s	k7c7	s
svatým	svatý	k2eAgInSc7d1	svatý
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
relikviáře	relikviář	k1gInPc4	relikviář
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
retabula	retabulum	k1gNnSc2	retabulum
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
jsou	být	k5eAaImIp3nP	být
celá	celý	k2eAgNnPc4d1	celé
těla	tělo	k1gNnPc4	tělo
mučedníků	mučedník	k1gMnPc2	mučedník
a	a	k8xC	a
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
uchovávané	uchovávaný	k2eAgMnPc4d1	uchovávaný
v	v	k7c6	v
prosklených	prosklený	k2eAgFnPc6d1	prosklená
tumbách	tumba	k1gFnPc6	tumba
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vystavená	vystavený	k2eAgFnSc1d1	vystavená
úctě	úcta	k1gFnSc6	úcta
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
kostela	kostel	k1gInSc2	kostel
nebo	nebo	k8xC	nebo
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
relikviáře	relikviář	k1gInPc1	relikviář
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
podobu	podoba	k1gFnSc4	podoba
malých	malý	k2eAgFnPc2d1	malá
schránek	schránka	k1gFnPc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
zdobené	zdobený	k2eAgFnPc1d1	zdobená
truhly	truhla	k1gFnPc1	truhla
a	a	k8xC	a
rakve	rakev	k1gFnPc1	rakev
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
velkého	velký	k2eAgInSc2d1	velký
bohatě	bohatě	k6eAd1	bohatě
zdobeného	zdobený	k2eAgInSc2d1	zdobený
relikviáře	relikviář	k1gInSc2	relikviář
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Relikviář	relikviář	k1gInSc1	relikviář
svatého	svatý	k2eAgMnSc2d1	svatý
Maura	Maur	k1gMnSc2	Maur
v	v	k7c6	v
Bečově	Bečov	k1gInSc6	Bečov
nad	nad	k7c7	nad
Teplou	Teplá	k1gFnSc7	Teplá
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
relikviářů	relikviář	k1gInPc2	relikviář
jsou	být	k5eAaImIp3nP	být
schránky	schránka	k1gFnPc1	schránka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
uchovávané	uchovávaný	k2eAgFnSc2d1	uchovávaná
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
nazývány	nazýván	k2eAgInPc1d1	nazýván
"	"	kIx"	"
<g/>
mluvící	mluvící	k2eAgInPc1d1	mluvící
relikviáře	relikviář	k1gInPc1	relikviář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
deskové	deskový	k2eAgInPc1d1	deskový
relikviáře	relikviář	k1gInPc1	relikviář
zvané	zvaný	k2eAgInPc1d1	zvaný
plenáře	plenář	k1gInPc1	plenář
<g/>
.	.	kIx.	.
</s>
<s>
Stolec	stolec	k1gInSc1	stolec
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
Talisman	talisman	k1gInSc1	talisman
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
Remeš	Remeš	k1gFnSc1	Remeš
Relikviář	relikviář	k1gInSc1	relikviář
svatého	svatý	k2eAgMnSc2d1	svatý
Maura	Maur	k1gMnSc2	Maur
v	v	k7c6	v
Bečově	Bečov	k1gInSc6	Bečov
Ostatky	ostatek	k1gInPc4	ostatek
sv.	sv.	kA	sv.
Auraciána	Auracián	k1gMnSc2	Auracián
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
Ostatky	ostatek	k1gInPc4	ostatek
sv.	sv.	kA	sv.
Konkordie	Konkordie	k1gFnSc1	Konkordie
v	v	k7c6	v
Bavorově	Bavorův	k2eAgFnSc6d1	Bavorova
PODLAHA	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
;	;	kIx,	;
ŠITTLER	ŠITTLER	kA	ŠITTLER
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Chrámový	chrámový	k2eAgInSc1d1	chrámový
poklad	poklad	k1gInSc1	poklad
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnPc1	jeho
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
popis	popis	k1gInSc1	popis
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Dědictví	dědictví	k1gNnSc1	dědictví
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
,	,	kIx,	,
s.	s.	k?	s.
181	[number]	k4	181
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
jen	jen	k9	jen
jako	jako	k8xC	jako
stáhnutelné	stáhnutelný	k2eAgNnSc1d1	stáhnutelné
pdf	pdf	k?	pdf
–	–	k?	–
vždy	vždy	k6eAd1	vždy
maximálně	maximálně	k6eAd1	maximálně
20	[number]	k4	20
stran	strana	k1gFnPc2	strana
stáhnutených	stáhnutený	k2eAgFnPc2d1	stáhnutený
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
relikviář	relikviář	k1gInSc1	relikviář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
