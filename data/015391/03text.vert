<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
(	(	kIx(
<g/>
Hora	hora	k1gFnSc1
Svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Šebestiánav	Šebestiánav	k1gMnSc2
Hoře	hoře	k1gNnSc2
Svatého	svatý	k1gMnSc2
Šebestiána	Šebestián	k1gMnSc2
Místo	místo	k7c2
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Chomutov	Chomutov	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Hora	hora	k1gFnSc1
Svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
39,01	39,01	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
5,41	5,41	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kostel	kostel	k1gInSc1
svatéhoŠebestiána	svatéhoŠebestián	k1gMnSc2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
litoměřická	litoměřický	k2eAgFnSc1d1
Vikariát	vikariát	k1gInSc1
</s>
<s>
krušnohorský	krušnohorský	k2eAgInSc1d1
Farnost	farnost	k1gFnSc4
</s>
<s>
děkanství	děkanství	k1gNnSc1
Chomutov	Chomutov	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
farní	farní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Datum	datum	k1gNnSc4
posvěcení	posvěcení	k1gNnSc2
</s>
<s>
1877	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1967	#num#	k4
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
novorománský	novorománský	k2eAgInSc1d1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1874	#num#	k4
<g/>
–	–	k?
<g/>
1876	#num#	k4
Specifikace	specifikace	k1gFnSc2
Stavební	stavební	k2eAgInSc1d1
materiál	materiál	k1gInSc1
</s>
<s>
zděný	zděný	k2eAgInSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
stával	stávat	k5eAaImAgInS
v	v	k7c6
Hoře	hora	k1gFnSc6
Svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Chomutov	Chomutov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
svého	svůj	k3xOyFgInSc2
zániku	zánik	k1gInSc2
býval	bývat	k5eAaImAgInS
farním	farní	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Hora	hora	k1gFnSc1
Svatého	svatý	k1gMnSc2
Šebestiána	Šebestián	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stával	stávat	k5eAaImAgMnS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
náměstí	náměstí	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
demolici	demolice	k1gFnSc6
kostela	kostel	k1gInSc2
změněna	změnit	k5eAaPmNgFnS
na	na	k7c4
park	park	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
na	na	k7c6
historické	historický	k2eAgFnSc6d1
pohlednici	pohlednice	k1gFnSc6
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Původní	původní	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
založený	založený	k2eAgInSc1d1
v	v	k7c6
šestnáctém	šestnáctý	k4xOgNnSc6
století	století	k1gNnSc6
Šebestiánem	Šebestián	k1gMnSc7
z	z	k7c2
Veitmile	Veitmila	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
vyhořel	vyhořet	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgNnPc7d1
109	#num#	k4
domy	dům	k1gInPc4
při	při	k7c6
velkém	velký	k2eAgInSc6d1
požáru	požár	k1gInSc6
města	město	k1gNnSc2
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1854	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
trosky	troska	k1gFnPc1
byly	být	k5eAaImAgFnP
odklizeny	odklidit	k5eAaPmNgFnP
až	až	k9
o	o	k7c4
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
chomutovským	chomutovský	k2eAgMnSc7d1
stavitelem	stavitel	k1gMnSc7
Josefem	Josef	k1gMnSc7
Dauschem	Dausch	k1gMnSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
zahájena	zahájen	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
nového	nový	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vysvěcen	vysvěcen	k2eAgInSc1d1
roku	rok	k1gInSc2
1877	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
špatný	špatný	k2eAgInSc4d1
technický	technický	k2eAgInSc4d1
stav	stav	k1gInSc4
rozhodnuto	rozhodnut	k2eAgNnSc1d1
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
demolici	demolice	k1gFnSc6
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
došlo	dojít	k5eAaPmAgNnS
15	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1967	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zboření	zboření	k1gNnSc6
kostela	kostel	k1gInSc2
žádala	žádat	k5eAaImAgFnS
také	také	k9
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
významným	významný	k2eAgInSc7d1
navigačním	navigační	k2eAgInSc7d1
bodem	bod	k1gInSc7
při	při	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
blízké	blízký	k2eAgFnPc4d1
kasárny	kasárny	k1gFnPc4
raketového	raketový	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
měl	mít	k5eAaImAgInS
obdélný	obdélný	k2eAgInSc1d1
půdorys	půdorys	k1gInSc1
a	a	k8xC
pravoúhlý	pravoúhlý	k2eAgInSc1d1
presbytář	presbytář	k1gInSc1
zakončený	zakončený	k2eAgInSc1d1
půlkruhovou	půlkruhový	k2eAgFnSc7d1
apsidou	apsida	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jižní	jižní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
lodi	loď	k1gFnSc2
stála	stát	k5eAaImAgFnS
štíhlá	štíhlý	k2eAgFnSc1d1
hranolová	hranolový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
malá	malý	k2eAgFnSc1d1
sakristie	sakristie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drobná	drobný	k2eAgFnSc1d1
sanktusová	sanktusový	k2eAgFnSc1d1
vížka	vížka	k1gFnSc1
se	se	k3xPyFc4
zvedala	zvedat	k5eAaImAgFnS
také	také	k9
nad	nad	k7c7
vyvrcholením	vyvrcholení	k1gNnSc7
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interiér	interiér	k1gInSc1
byl	být	k5eAaImAgInS
zaklenutý	zaklenutý	k2eAgInSc1d1
pravděpodobně	pravděpodobně	k6eAd1
plackovou	plackový	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
a	a	k8xC
apsida	apsida	k1gFnSc1
konchou	koncha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zařízení	zařízení	k1gNnSc1
</s>
<s>
Většina	většina	k1gFnSc1
zařízení	zařízení	k1gNnSc2
byla	být	k5eAaImAgFnS
ze	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřily	patřit	k5eAaImAgFnP
k	k	k7c3
němu	on	k3xPp3gNnSc3
obraz	obraz	k1gInSc1
Čtrnácti	čtrnáct	k4xCc2
svatých	svatý	k2eAgMnPc2d1
pomocníků	pomocník	k1gMnPc2
od	od	k7c2
malíře	malíř	k1gMnSc2
V.	V.	kA
Hendlicha	Hendlich	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
a	a	k8xC
Pieta	pieta	k1gFnSc1
pod	pod	k7c7
kruchtou	kruchta	k1gFnSc7
od	od	k7c2
J.	J.	kA
Wertschitzkého	Wertschitzký	k1gMnSc4
z	z	k7c2
Chomutova	Chomutov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
staršího	starý	k2eAgInSc2d2
kostela	kostel	k1gInSc2
pocházely	pocházet	k5eAaImAgFnP
křtitelnice	křtitelnice	k1gFnPc1
s	s	k7c7
cínovou	cínový	k2eAgFnSc7d1
nádobou	nádoba	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1730	#num#	k4
a	a	k8xC
krucifix	krucifix	k1gInSc1
z	z	k7c2
konce	konec	k1gInSc2
osmnáctého	osmnáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
AD	ad	k7c4
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litoměřice	Litoměřice	k1gInPc4
<g/>
:	:	kIx,
Biskupství	biskupství	k1gNnSc3
litoměřické	litoměřický	k2eAgInPc1d1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
430	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Přehled	přehled	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
LAURENČÍKOVÁ	LAURENČÍKOVÁ	kA
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
;	;	kIx,
GAŽA	GAŽA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniklé	zaniklý	k2eAgInPc1d1
kostely	kostel	k1gInPc1
na	na	k7c6
Chomutovsku	Chomutovsko	k1gNnSc6
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chomutov	Chomutov	k1gInSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Chomutově	Chomutov	k1gInSc6
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
48	#num#	k4
s.	s.	k?
S.	S.	kA
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgInSc1d1
města	město	k1gNnSc2
Krušných	krušný	k2eAgFnPc6d1
hor.	hor.	k?
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolov	Sokolov	k1gInSc1
<g/>
:	:	kIx,
Fornica	Fornica	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
328	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87194	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Zmizelé	zmizelý	k2eAgInPc1d1
šebestiánské	šebestiánský	k2eAgInPc1d1
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Obce	obec	k1gFnPc4
chomutovského	chomutovský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Zdena	Zdena	k1gFnSc1
Binterová	Binterová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chomutov	Chomutov	k1gInSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Chomutově	Chomutov	k1gInSc6
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
302	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
173	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Hora	hora	k1gFnSc1
Svatého	svatý	k2eAgMnSc2d1
Šebestiána	Šebestián	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Šebestiána	Šebestián	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškozené	poškozený	k2eAgInPc1d1
a	a	k8xC
zničené	zničený	k2eAgInPc1d1
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
kaple	kaple	k1gFnPc1
a	a	k8xC
synagogy	synagoga	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Umělecké	umělecký	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Emanuel	Emanuela	k1gFnPc2
Poche	Poch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
I.	I.	kA
A	A	kA
<g/>
/	/	kIx~
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
644	#num#	k4
s.	s.	k?
Heslo	heslo	k1gNnSc1
Hora	hora	k1gFnSc1
Svatého	svatý	k1gMnSc2
Šebestiána	Šebestián	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
398	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
