<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
vlastencem	vlastenec	k1gMnSc7	vlastenec
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
snil	snít	k5eAaImAgInS	snít
o	o	k7c4	o
realizaci	realizace	k1gFnSc4	realizace
cyklu	cyklus	k1gInSc2	cyklus
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
shrnout	shrnout	k5eAaPmF	shrnout
dějiny	dějiny	k1gFnPc4	dějiny
slovanského	slovanský	k2eAgInSc2d1	slovanský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
