<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
lat.	lat.	k?	lat.
medicinae	medicinae	k6eAd1	medicinae
universae	universaat	k5eAaPmIp3nS	universaat
doctor	doctor	k1gInSc1	doctor
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
MUDr.	MUDr.	kA	MUDr.
psaná	psaný	k2eAgFnSc1d1	psaná
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
absolventům	absolvent	k1gMnPc3	absolvent
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
udělován	udělován	k2eAgInSc4d1	udělován
po	po	k7c6	po
šestiletém	šestiletý	k2eAgNnSc6d1	šestileté
studiu	studio	k1gNnSc6	studio
oboru	obor	k1gInSc2	obor
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
lékařství	lékařství	k1gNnSc4	lékařství
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
a	a	k8xC	a
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
státní	státní	k2eAgFnSc7d1	státní
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
získání	získání	k1gNnSc1	získání
ekvivalentního	ekvivalentní	k2eAgInSc2d1	ekvivalentní
titulu	titul	k1gInSc2	titul
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
umožnění	umožnění	k1gNnSc3	umožnění
vykonávání	vykonávání	k1gNnSc2	vykonávání
povolání	povolání	k1gNnSc2	povolání
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
profesní	profesní	k2eAgInSc4d1	profesní
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c4	o
titul	titul	k1gInSc4	titul
spjatý	spjatý	k2eAgInSc4d1	spjatý
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
určité	určitý	k2eAgFnSc2d1	určitá
profese	profes	k1gFnSc2	profes
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
slovním	slovní	k2eAgNnSc7d1	slovní
vyjádřením	vyjádření	k1gNnSc7	vyjádření
titulu	titul	k1gInSc2	titul
MUDr.	MUDr.	kA	MUDr.
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
medicinae	medicinae	k1gInSc1	medicinae
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
obligatorní	obligatorní	k2eAgInSc4d1	obligatorní
<g/>
"	"	kIx"	"
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
<g/>
:	:	kIx,	:
malý	malý	k2eAgInSc1d1	malý
doktorát	doktorát	k1gInSc1	doktorát
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
užívalo	užívat	k5eAaImAgNnS	užívat
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
veškerého	veškerý	k3xTgNnSc2	veškerý
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
lékaři	lékař	k1gMnPc1	lékař
získávají	získávat	k5eAaImIp3nP	získávat
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Medical	Medical	k1gMnSc1	Medical
Doctor	Doctor	k1gMnSc1	Doctor
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
medicinæ	medicinæ	k?	medicinæ
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
píše	psát	k5eAaImIp3nS	psát
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
lékaři	lékař	k1gMnPc1	lékař
používají	používat	k5eAaImIp3nP	používat
titul	titul	k1gInSc4	titul
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
přiznání	přiznání	k1gNnSc3	přiznání
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
obhájit	obhájit	k5eAaPmF	obhájit
disertační	disertační	k2eAgFnSc4d1	disertační
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
<g/>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
celistvé	celistvý	k2eAgNnSc4d1	celistvé
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
prezenční	prezenční	k2eAgNnSc4d1	prezenční
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
rozdělené	rozdělený	k2eAgInPc1d1	rozdělený
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
a	a	k8xC	a
na	na	k7c4	na
navazující	navazující	k2eAgInSc4d1	navazující
magisterský	magisterský	k2eAgInSc4d1	magisterský
studijní	studijní	k2eAgInSc4d1	studijní
program	program	k1gInSc4	program
<g/>
)	)	kIx)	)
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
řádně	řádně	k6eAd1	řádně
se	se	k3xPyFc4	se
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
rigorózní	rigorózní	k2eAgFnSc7d1	rigorózní
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
součástí	součást	k1gFnSc7	součást
není	být	k5eNaImIp3nS	být
povinně	povinně	k6eAd1	povinně
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
řádné	řádný	k2eAgNnSc4d1	řádné
zakončení	zakončení	k1gNnSc4	zakončení
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
případně	případně	k6eAd1	případně
možnost	možnost	k1gFnSc4	možnost
i	i	k9	i
dále	daleko	k6eAd2	daleko
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
další	další	k2eAgFnSc1d1	další
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
akademickou	akademický	k2eAgFnSc4d1	akademická
činnost	činnost	k1gFnSc4	činnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
získávat	získávat	k5eAaImF	získávat
příslušné	příslušný	k2eAgFnPc4d1	příslušná
atestace	atestace	k1gFnPc4	atestace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chirurgie	chirurgie	k1gFnSc1	chirurgie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nejprv	nejprv	k6eAd1	nejprv
znamená	znamenat	k5eAaImIp3nS	znamenat
2-3	[number]	k4	2-3
roky	rok	k1gInPc4	rok
nástavby	nástavba	k1gFnSc2	nástavba
až	až	k9	až
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
kmene	kmen	k1gInSc2	kmen
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
dřívější	dřívější	k2eAgFnSc2d1	dřívější
1	[number]	k4	1
<g/>
.	.	kIx.	.
atestace	atestace	k1gFnPc4	atestace
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
2-4	[number]	k4	2-4
další	další	k2eAgInPc4d1	další
roky	rok	k1gInPc4	rok
do	do	k7c2	do
příslušné	příslušný	k2eAgFnSc2d1	příslušná
oborové	oborový	k2eAgFnSc2d1	oborová
atestace	atestace	k1gFnSc2	atestace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
specializační	specializační	k2eAgNnSc4d1	specializační
vzdělání	vzdělání	k1gNnSc4	vzdělání
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
složení	složení	k1gNnSc6	složení
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
vykonávat	vykonávat	k5eAaImF	vykonávat
povolání	povolání	k1gNnSc4	povolání
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
oboru	obor	k1gInSc6	obor
bez	bez	k7c2	bez
odborného	odborný	k2eAgInSc2d1	odborný
dohledu	dohled	k1gInSc2	dohled
či	či	k8xC	či
dozoru	dozor	k1gInSc2	dozor
lékaře	lékař	k1gMnSc2	lékař
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
zcela	zcela	k6eAd1	zcela
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
provozovat	provozovat	k5eAaImF	provozovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
soukromou	soukromý	k2eAgFnSc4d1	soukromá
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Karel	Karel	k1gMnSc1	Karel
Rokytanský	Rokytanský	k2eAgMnSc1d1	Rokytanský
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Carl	Carl	k1gMnSc1	Carl
Freiherr	Freiherra	k1gFnPc2	Freiherra
von	von	k1gInSc1	von
Rokitansky	Rokitansky	k1gMnSc1	Rokitansky
(	(	kIx(	(
<g/>
baron	baron	k1gMnSc1	baron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
letech	let	k1gInPc6	let
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
patolog	patolog	k1gMnSc1	patolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
liberální	liberální	k2eAgMnSc1d1	liberální
politik	politik	k1gMnSc1	politik
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
užívání	užívání	k1gNnSc3	užívání
titulu	titul	k1gInSc2	titul
MUDr.	MUDr.	kA	MUDr.
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc4	doktor
medicíny	medicína	k1gFnSc2	medicína
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
udělován	udělován	k2eAgInSc4d1	udělován
doktorům	doktor	k1gMnPc3	doktor
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gMnSc2	on
začalo	začít	k5eAaPmAgNnS	začít
užívat	užívat	k5eAaImF	užívat
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
chirurgie	chirurgie	k1gFnSc2	chirurgie
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
chirurgie	chirurgie	k1gFnSc2	chirurgie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
udělovat	udělovat	k5eAaImF	udělovat
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
veškerého	veškerý	k3xTgNnSc2	veškerý
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MUDr.	MUDr.	kA	MUDr.
<g/>
,	,	kIx,	,
Medicinae	Medicina	k1gMnSc2	Medicina
universae	universa	k1gMnSc2	universa
doctor	doctor	k1gMnSc1	doctor
<g/>
)	)	kIx)	)
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přijat	přijat	k2eAgInSc1d1	přijat
Nejedlého	Nejedlého	k2eAgInSc1d1	Nejedlého
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
zrušil	zrušit	k5eAaPmAgInS	zrušit
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
stavovská	stavovský	k2eAgNnPc4d1	Stavovské
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
absolventy	absolvent	k1gMnPc4	absolvent
a	a	k8xC	a
těm	ten	k3xDgMnPc3	ten
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
udíleny	udílet	k5eAaImNgInP	udílet
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
mnohá	mnohé	k1gNnPc1	mnohé
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
tak	tak	k9	tak
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
získávali	získávat	k5eAaImAgMnP	získávat
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
promovaný	promovaný	k2eAgMnSc1d1	promovaný
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
zpětně	zpětně	k6eAd1	zpětně
přiznán	přiznán	k2eAgInSc4d1	přiznán
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
udělován	udělovat	k5eAaImNgInS	udělovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
titul	titul	k1gInSc1	titul
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
následně	následně	k6eAd1	následně
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
medicíny	medicína	k1gFnSc2	medicína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
ECTS	ECTS	kA	ECTS
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
k	k	k7c3	k
řádnému	řádný	k2eAgNnSc3d1	řádné
absolvování	absolvování	k1gNnSc3	absolvování
studia	studio	k1gNnSc2	studio
nutné	nutný	k2eAgFnSc2d1	nutná
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
dobu	doba	k1gFnSc4	doba
získat	získat	k5eAaPmF	získat
360	[number]	k4	360
kreditů	kredit	k1gInPc2	kredit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc4	titul
MUDr.	MUDr.	kA	MUDr.
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
získávali	získávat	k5eAaImAgMnP	získávat
i	i	k9	i
zubní	zubní	k2eAgMnPc1d1	zubní
lékaři	lékař	k1gMnPc1	lékař
–	–	k?	–
stomatologové	stomatolog	k1gMnPc1	stomatolog
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
předcházejícím	předcházející	k2eAgNnSc6d1	předcházející
období	období	k1gNnSc6	období
ještě	ještě	k6eAd1	ještě
získávali	získávat	k5eAaImAgMnP	získávat
titul	titul	k1gInSc4	titul
MSDr	MSDra	k1gFnPc2	MSDra
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
harmonizace	harmonizace	k1gFnSc2	harmonizace
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
oddělen	oddělit	k5eAaPmNgInS	oddělit
zvlášť	zvlášť	k6eAd1	zvlášť
titul	titul	k1gInSc1	titul
pro	pro	k7c4	pro
stomatology	stomatolog	k1gMnPc4	stomatolog
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
tak	tak	k6eAd1	tak
začali	začít	k5eAaPmAgMnP	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
získávat	získávat	k5eAaImF	získávat
v	v	k7c6	v
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
zubní	zubní	k2eAgNnSc4d1	zubní
lékařství	lékařství	k1gNnSc4	lékařství
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
zubní	zubní	k2eAgMnSc1d1	zubní
lékař	lékař	k1gMnSc1	lékař
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
zákon	zákon	k1gInSc1	zákon
novelizován	novelizován	k2eAgInSc1d1	novelizován
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MDDr	MDDr	k1gInSc1	MDDr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doctor	doctor	k1gMnSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dnes	dnes	k6eAd1	dnes
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
v	v	k7c6	v
mezinárodně	mezinárodně	k6eAd1	mezinárodně
standardní	standardní	k2eAgFnSc6d1	standardní
zkratce	zkratka	k1gFnSc6	zkratka
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
vědce	vědec	k1gMnSc4	vědec
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
"	"	kIx"	"
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
MUC	MUC	kA	MUC
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Studenti	student	k1gMnPc1	student
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
dříve	dříve	k6eAd2	dříve
používali	používat	k5eAaImAgMnP	používat
akademické	akademický	k2eAgNnSc4d1	akademické
označení	označení	k1gNnSc4	označení
MUC	MUC	kA	MUC
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
medicinae	medicinae	k1gNnSc1	medicinae
universae	universa	k1gFnPc1	universa
candidatus	candidatus	k1gMnSc1	candidatus
–	–	k?	–
kandidát	kandidát	k1gMnSc1	kandidát
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
například	například	k6eAd1	například
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
v	v	k7c6	v
nemocničních	nemocniční	k2eAgFnPc6d1	nemocniční
dokumentacích	dokumentace	k1gFnPc6	dokumentace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
budoucí	budoucí	k2eAgNnSc4d1	budoucí
povolání	povolání	k1gNnSc4	povolání
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
studenti	student	k1gMnPc1	student
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
současnost	současnost	k1gFnSc4	současnost
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
má	mít	k5eAaImIp3nS	mít
student	student	k1gMnSc1	student
právo	právo	k1gNnSc4	právo
používat	používat	k5eAaImF	používat
již	již	k6eAd1	již
po	po	k7c6	po
proběhlé	proběhlý	k2eAgFnSc6d1	proběhlá
imatrikulaci	imatrikulace	k1gFnSc6	imatrikulace
<g/>
,	,	kIx,	,
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
určitých	určitý	k2eAgFnPc2d1	určitá
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
po	po	k7c6	po
šesti	šest	k4xCc6	šest
semestrech	semestr	k1gInPc6	semestr
či	či	k8xC	či
až	až	k9	až
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
státní	státní	k2eAgFnSc2d1	státní
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
z	z	k7c2	z
období	období	k1gNnSc2	období
první	první	k4xOgFnSc2	první
československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dřívější	dřívější	k2eAgNnSc4d1	dřívější
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
lékařské	lékařský	k2eAgFnSc6d1	lékařská
fakultě	fakulta	k1gFnSc6	fakulta
probíhalo	probíhat	k5eAaImAgNnS	probíhat
formou	forma	k1gFnSc7	forma
postupných	postupný	k2eAgFnPc2d1	postupná
státních	státní	k2eAgFnPc2d1	státní
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
volitelných	volitelný	k2eAgFnPc2d1	volitelná
zkoušek	zkouška	k1gFnPc2	zkouška
rigorózních	rigorózní	k2eAgFnPc2d1	rigorózní
(	(	kIx(	(
<g/>
přísných	přísný	k2eAgFnPc2d1	přísná
zkoušek	zkouška	k1gFnPc2	zkouška
doktorských	doktorský	k2eAgFnPc2d1	doktorská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chtěl	chtít	k5eAaImAgMnS	chtít
student	student	k1gMnSc1	student
později	pozdě	k6eAd2	pozdě
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
MUDr.	MUDr.	kA	MUDr.
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
titul	titul	k1gInSc1	titul
MUC	MUC	kA	MUC
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
začít	začít	k5eAaPmF	začít
používat	používat	k5eAaImF	používat
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
složení	složení	k1gNnSc6	složení
první	první	k4xOgMnPc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lékařské	lékařský	k2eAgInPc4d1	lékařský
tituly	titul	k1gInPc4	titul
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
titul	titul	k1gInSc1	titul
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
profesní	profesní	k2eAgInSc1d1	profesní
doktorát	doktorát	k1gInSc1	doktorát
<g/>
)	)	kIx)	)
psaný	psaný	k2eAgInSc1d1	psaný
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
přiznán	přiznat	k5eAaPmNgInS	přiznat
po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
legislativy	legislativa	k1gFnSc2	legislativa
USA	USA	kA	USA
ekvivalentním	ekvivalentní	k2eAgInSc7d1	ekvivalentní
titulem	titul	k1gInSc7	titul
je	být	k5eAaImIp3nS	být
titul	titul	k1gInSc1	titul
DO	do	k7c2	do
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
osteopatické	osteopatický	k2eAgFnSc2d1	osteopatická
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pseudovědecké	pseudovědecký	k2eAgFnSc2d1	pseudovědecká
osteopatie	osteopatie	k1gFnSc2	osteopatie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
škála	škála	k1gFnSc1	škála
titulů	titul	k1gInPc2	titul
vycházejících	vycházející	k2eAgInPc2d1	vycházející
z	z	k7c2	z
označení	označení	k1gNnSc2	označení
bakalář	bakalář	k1gMnSc1	bakalář
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
bakalář	bakalář	k1gMnSc1	bakalář
chirurgie	chirurgie	k1gFnSc1	chirurgie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
v	v	k7c6	v
udílející	udílející	k2eAgFnSc6d1	udílející
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Používané	používaný	k2eAgFnPc1d1	používaná
zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
MBBS	MBBS	kA	MBBS
<g/>
,	,	kIx,	,
MB	MB	kA	MB
<g/>
,	,	kIx,	,
MB	MB	kA	MB
BCh	BCh	k1gFnSc2	BCh
BAO	BAO	kA	BAO
<g/>
,	,	kIx,	,
BMBS	BMBS	kA	BMBS
<g/>
,	,	kIx,	,
MBBChir	MBBChira	k1gFnPc2	MBBChira
a	a	k8xC	a
MBChB	MBChB	k1gFnPc2	MBChB
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
univerzálně	univerzálně	k6eAd1	univerzálně
<g/>
,	,	kIx,	,
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
doktorského	doktorský	k2eAgInSc2d1	doktorský
titulu	titul	k1gInSc2	titul
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
k	k	k7c3	k
PhD	PhD	k1gFnSc3	PhD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německo	Německo	k1gNnSc1	Německo
===	===	k?	===
</s>
</p>
<p>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
medicíny	medicína	k1gFnSc2	medicína
nezískává	získávat	k5eNaImIp3nS	získávat
žádný	žádný	k3yNgInSc4	žádný
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
až	až	k9	až
po	po	k7c6	po
obhájení	obhájení	k1gNnSc6	obhájení
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
vlastním	vlastní	k2eAgInSc6d1	vlastní
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
medicíny	medicína	k1gFnSc2	medicína
získá	získat	k5eAaPmIp3nS	získat
titul	titul	k1gInSc1	titul
lek	lek	k1gInSc1	lek
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
doktorským	doktorský	k2eAgInSc7d1	doktorský
titulem	titul	k1gInSc7	titul
ekvivalentním	ekvivalentní	k2eAgInSc7d1	ekvivalentní
k	k	k7c3	k
PhD	PhD	k1gFnPc3	PhD
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
dr	dr	kA	dr
med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
MUDr.	MUDr.	kA	MUDr.
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
