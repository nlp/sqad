<s>
V	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
lékaři	lékař	k1gMnPc1	lékař
získávají	získávat	k5eAaImIp3nP	získávat
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Medical	Medical	k1gMnSc1	Medical
Doctor	Doctor	k1gMnSc1	Doctor
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
medicinæ	medicinæ	k?	medicinæ
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
píše	psát	k5eAaImIp3nS	psát
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
