<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
Oscarů	Oscar	k1gMnPc2	Oscar
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
Mickey	Micke	k2eAgFnPc4d1	Micke
Mouse	Mouse	k1gFnPc4	Mouse
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
celkem	celkem	k6eAd1	celkem
-	-	kIx~	-
22	[number]	k4	22
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1931	[number]	k4	1931
<g/>
/	/	kIx~	/
<g/>
1932	[number]	k4	1932
až	až	k9	až
1939	[number]	k4	1939
osm	osm	k4xCc4	osm
sošek	soška	k1gFnPc2	soška
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
(	(	kIx(	(
<g/>
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
kategoriemi	kategorie	k1gFnPc7	kategorie
deset	deset	k4xCc4	deset
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
drží	držet	k5eAaImIp3nP	držet
další	další	k2eAgMnPc1d1	další
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
