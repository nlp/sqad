<s>
Panarea	Panarea	k1gFnSc1	Panarea
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
ostrova	ostrov	k1gInSc2	ostrov
Punta	punto	k1gNnSc2	punto
Cardosi	Cardose	k1gFnSc4	Cardose
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
402	[number]	k4	402
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Osídlena	osídlen	k2eAgFnSc1d1	osídlena
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
osad	osada	k1gFnPc2	osada
tam	tam	k6eAd1	tam
postupně	postupně	k6eAd1	postupně
přecházejí	přecházet	k5eAaImIp3nP	přecházet
ve	v	k7c4	v
vilky	vilka	k1gFnPc4	vilka
se	s	k7c7	s
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
příkrých	příkrý	k2eAgInPc2d1	příkrý
skalních	skalní	k2eAgInPc2d1	skalní
útesů	útes	k1gInPc2	útes
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
za	za	k7c2	za
příznivého	příznivý	k2eAgNnSc2d1	příznivé
počasí	počasí	k1gNnSc2	počasí
výhledy	výhled	k1gInPc1	výhled
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
souostroví	souostroví	k1gNnSc4	souostroví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
skalní	skalní	k2eAgInPc4d1	skalní
útesy	útes	k1gInPc4	útes
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
okolo	okolo	k7c2	okolo
Panarei	Panare	k1gFnSc2	Panare
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
skalnatý	skalnatý	k2eAgInSc4d1	skalnatý
poloostrov	poloostrov	k1gInSc4	poloostrov
Capo	capa	k1gFnSc5	capa
Milazzese	Milazzese	k1gFnPc4	Milazzese
s	s	k7c7	s
archeologickými	archeologický	k2eAgInPc7d1	archeologický
nálezy	nález	k1gInPc7	nález
kulatých	kulatý	k2eAgFnPc2d1	kulatá
obydlí	obydlí	k1gNnSc4	obydlí
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
atraktivními	atraktivní	k2eAgInPc7d1	atraktivní
útesy	útes	k1gInPc7	útes
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
záliv	záliv	k1gInSc4	záliv
Caletta	Caletto	k1gNnSc2	Caletto
dei	dei	k?	dei
Zimmari	Zimmari	k1gNnSc2	Zimmari
s	s	k7c7	s
pláží	pláž	k1gFnSc7	pláž
černého	černý	k2eAgInSc2d1	černý
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
značena	značit	k5eAaImNgFnS	značit
červeno-bílou	červenoílý	k2eAgFnSc7d1	červeno-bílá
turistickou	turistický	k2eAgFnSc7d1	turistická
značkou	značka	k1gFnSc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
také	také	k9	také
nabízí	nabízet	k5eAaImIp3nS	nabízet
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
sousední	sousední	k2eAgFnSc6d1	sousední
Stromboli	Strombole	k1gFnSc6	Strombole
s	s	k7c7	s
kouřící	kouřící	k2eAgFnSc7d1	kouřící
sopkou	sopka	k1gFnSc7	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
městu	město	k1gNnSc3	město
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
270	[number]	k4	270
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
