<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Gabrielle	Gabrielle	k1gInSc1	Gabrielle
Chanel	Chanel	k1gInSc1	Chanel
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1883	[number]	k4	1883
Saumur	Saumura	k1gFnPc2	Saumura
<g/>
,	,	kIx,	,
Pays	Paysa	k1gFnPc2	Paysa
de	de	k?	de
la	la	k1gNnSc4	la
Loire	Loir	k1gInSc5	Loir
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
osobnost	osobnost	k1gFnSc4	osobnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
utváření	utváření	k1gNnSc2	utváření
šatníku	šatník	k1gInSc2	šatník
moderní	moderní	k2eAgFnSc2d1	moderní
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
haute	haute	k5eAaPmIp2nP	haute
couture	coutur	k1gMnSc5	coutur
udávala	udávat	k5eAaImAgFnS	udávat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tón	tón	k1gInSc4	tón
světovému	světový	k2eAgInSc3d1	světový
módnímu	módní	k2eAgInSc3d1	módní
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
podomního	podomní	k2eAgMnSc2d1	podomní
obchodníka	obchodník	k1gMnSc2	obchodník
Alberta	Albert	k1gMnSc2	Albert
Chanela	Chanel	k1gMnSc2	Chanel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
družky	družka	k1gFnSc2	družka
Jeanne	Jeann	k1gMnSc5	Jeann
Devolle	Devoll	k1gMnSc5	Devoll
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
Julii	Julie	k1gFnSc4	Julie
a	a	k8xC	a
Antoinettu	Antoinetta	k1gFnSc4	Antoinetta
<g/>
,	,	kIx,	,
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
Alphonse	Alphons	k1gMnPc4	Alphons
a	a	k8xC	a
Luciana	Lucian	k1gMnSc4	Lucian
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
otec	otec	k1gMnSc1	otec
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
klášterního	klášterní	k2eAgInSc2d1	klášterní
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c6	v
Aubazine	Aubazin	k1gInSc5	Aubazin
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Julií	Julie	k1gFnPc2	Julie
byla	být	k5eAaImAgNnP	být
později	pozdě	k6eAd2	pozdě
přijata	přijmout	k5eAaPmNgNnP	přijmout
do	do	k7c2	do
klášterní	klášterní	k2eAgFnSc2d1	klášterní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
začala	začít	k5eAaPmAgFnS	začít
pomáhat	pomáhat	k5eAaImF	pomáhat
své	svůj	k3xOyFgFnSc3	svůj
tetě	teta	k1gFnSc3	teta
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
oděvy	oděv	k1gInPc7	oděv
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Moulins	Moulinsa	k1gFnPc2	Moulinsa
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
pověst	pověst	k1gFnSc4	pověst
mimořádně	mimořádně	k6eAd1	mimořádně
schopné	schopný	k2eAgFnPc1d1	schopná
švadleny	švadlena	k1gFnPc1	švadlena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905-1908	[number]	k4	1905-1908
si	se	k3xPyFc3	se
po	po	k7c6	po
večerech	večer	k1gInPc6	večer
navíc	navíc	k6eAd1	navíc
přivydělávala	přivydělávat	k5eAaImAgFnS	přivydělávat
zpěvem	zpěv	k1gInSc7	zpěv
v	v	k7c6	v
kavárnách	kavárna	k1gFnPc6	kavárna
a	a	k8xC	a
nočních	noční	k2eAgInPc6d1	noční
podnicích	podnik	k1gInPc6	podnik
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
v	v	k7c4	v
Moulins	Moulins	k1gInSc4	Moulins
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c4	v
Vichy	Vicha	k1gFnPc4	Vicha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
získala	získat	k5eAaPmAgFnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
Coco	Coco	k6eAd1	Coco
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
znamená	znamenat	k5eAaImIp3nS	znamenat
dušička	dušička	k1gFnSc1	dušička
nebo	nebo	k8xC	nebo
miláček	miláček	k1gMnSc1	miláček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lázeňském	lázeňský	k2eAgNnSc6d1	lázeňské
městě	město	k1gNnSc6	město
Vichy	Vicha	k1gFnSc2	Vicha
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
z	z	k7c2	z
průmyslnické	průmyslnický	k2eAgFnSc2d1	průmyslnická
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
chovatelem	chovatel	k1gMnSc7	chovatel
koní	kůň	k1gMnPc2	kůň
Etiennem	Etienn	k1gMnSc7	Etienn
Balsanem	Balsan	k1gMnSc7	Balsan
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
milenkou	milenka	k1gFnSc7	milenka
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k9	tak
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Royallieu	Royallieus	k1gInSc6	Royallieus
u	u	k7c2	u
Compiè	Compiè	k1gFnSc2	Compiè
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
poznala	poznat	k5eAaPmAgFnS	poznat
dalšího	další	k2eAgMnSc4d1	další
osudového	osudový	k2eAgMnSc4d1	osudový
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
britského	britský	k2eAgMnSc2d1	britský
diplomata	diplomat	k1gMnSc2	diplomat
a	a	k8xC	a
majitele	majitel	k1gMnSc2	majitel
dolů	dolů	k6eAd1	dolů
Arthura	Arthur	k1gMnSc4	Arthur
Capela	Capel	k1gMnSc4	Capel
<g/>
,	,	kIx,	,
přezdívaného	přezdívaný	k2eAgMnSc4d1	přezdívaný
Boy	boa	k1gFnPc5	boa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
podporou	podpora	k1gFnSc7	podpora
otevřela	otevřít	k5eAaPmAgFnS	otevřít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
mondénním	mondénní	k2eAgNnSc6d1	mondénní
přímořském	přímořský	k2eAgNnSc6d1	přímořské
letovisku	letovisko	k1gNnSc6	letovisko
Deauville	Deauville	k1gInSc1	Deauville
několik	několik	k4yIc1	několik
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
luxusními	luxusní	k2eAgInPc7d1	luxusní
klobouky	klobouk	k1gInPc7	klobouk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
časem	časem	k6eAd1	časem
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
sortiment	sortiment	k1gInSc4	sortiment
o	o	k7c4	o
sportovní	sportovní	k2eAgNnSc4d1	sportovní
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
pak	pak	k6eAd1	pak
otevřela	otevřít	k5eAaPmAgFnS	otevřít
další	další	k2eAgInSc4d1	další
módní	módní	k2eAgInSc4d1	módní
salon	salon	k1gInSc4	salon
v	v	k7c6	v
letovisku	letovisko	k1gNnSc6	letovisko
Biarritz	Biarritza	k1gFnPc2	Biarritza
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
první	první	k4xOgFnSc1	první
ucelená	ucelený	k2eAgFnSc1d1	ucelená
kolekce	kolekce	k1gFnSc1	kolekce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
originální	originální	k2eAgInPc4d1	originální
košilové	košilový	k2eAgInPc4d1	košilový
šaty	šat	k1gInPc4	šat
(	(	kIx(	(
<g/>
jednodílný	jednodílný	k2eAgInSc4d1	jednodílný
<g/>
,	,	kIx,	,
volný	volný	k2eAgInSc4d1	volný
oděv	oděv	k1gInSc4	oděv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nosil	nosit	k5eAaImAgInS	nosit
bez	bez	k7c2	bez
korzetu	korzet	k1gInSc2	korzet
<g/>
,	,	kIx,	,
svázaný	svázaný	k2eAgInSc1d1	svázaný
v	v	k7c6	v
pase	pas	k1gInSc6	pas
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
proslulé	proslulý	k2eAgFnPc1d1	proslulá
"	"	kIx"	"
<g/>
malé	malý	k2eAgFnPc1d1	malá
černé	černý	k2eAgFnPc1d1	černá
šaty	šata	k1gFnPc1	šata
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
do	do	k7c2	do
dámské	dámský	k2eAgFnSc2d1	dámská
módy	móda	k1gFnSc2	móda
zavedla	zavést	k5eAaPmAgFnS	zavést
úplet	úplet	k1gInSc4	úplet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
materiál	materiál	k1gInSc4	materiál
vhodný	vhodný	k2eAgInSc4d1	vhodný
jen	jen	k9	jen
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
prádla	prádlo	k1gNnSc2	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
nebylo	být	k5eNaImAgNnS	být
jen	jen	k9	jen
rozšířit	rozšířit	k5eAaPmF	rozšířit
módu	móda	k1gFnSc4	móda
aristokracie	aristokracie	k1gFnSc2	aristokracie
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
tříd	třída	k1gFnPc2	třída
do	do	k7c2	do
"	"	kIx"	"
<g/>
ulic	ulice	k1gFnPc2	ulice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
umožnit	umožnit	k5eAaPmF	umožnit
nositelce	nositelka	k1gFnSc6	nositelka
každého	každý	k3xTgInSc2	každý
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
sama	sám	k3xTgFnSc1	sám
sebou	se	k3xPyFc7	se
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pohodlně	pohodlně	k6eAd1	pohodlně
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
krédem	krédo	k1gNnSc7	krédo
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
funkčnost	funkčnost	k1gFnSc1	funkčnost
a	a	k8xC	a
prostota	prostota	k1gFnSc1	prostota
<g/>
.	.	kIx.	.
</s>
<s>
Způsobila	způsobit	k5eAaPmAgFnS	způsobit
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
oděvním	oděvní	k2eAgInSc6d1	oděvní
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
podniky	podnik	k1gInPc1	podnik
měly	mít	k5eAaImAgInP	mít
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
až	až	k9	až
3500	[number]	k4	3500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
nejbohatší	bohatý	k2eAgFnSc1d3	nejbohatší
módní	módní	k2eAgFnSc7d1	módní
návrhářkou	návrhářka	k1gFnSc7	návrhářka
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
královna	královna	k1gFnSc1	královna
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Capel	Capel	k1gMnSc1	Capel
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
největší	veliký	k2eAgFnSc1d3	veliký
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
nehodě	nehoda	k1gFnSc6	nehoda
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neprovdala	provdat	k5eNaPmAgFnS	provdat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jejím	její	k3xOp3gInSc7	její
životem	život	k1gInSc7	život
prošla	projít	k5eAaPmAgFnS	projít
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
ještě	ještě	k6eAd1	ještě
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
velkokníže	velkokníže	k1gMnSc1	velkokníže
Dmitrij	Dmitrij	k1gFnSc2	Dmitrij
Romanov	Romanov	k1gInSc1	Romanov
(	(	kIx(	(
<g/>
spojovaný	spojovaný	k2eAgInSc1d1	spojovaný
s	s	k7c7	s
případem	případ	k1gInSc7	případ
vraždy	vražda	k1gFnSc2	vražda
Grigorije	Grigorije	k1gMnSc2	Grigorije
Rasputina	Rasputin	k1gMnSc2	Rasputin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
"	"	kIx"	"
<g/>
perly	perla	k1gFnPc1	perla
Romanovců	Romanovec	k1gMnPc2	Romanovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gMnSc1	Hugh
Grosvenor	Grosvenor	k1gMnSc1	Grosvenor
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Westminsteru	Westminster	k1gInSc2	Westminster
<g/>
,	,	kIx,	,
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
žurnalista	žurnalista	k1gMnSc1	žurnalista
Paul	Paul	k1gMnSc1	Paul
Iribe	Irib	k1gMnSc5	Irib
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jediného	jediný	k2eAgInSc2d1	jediný
butiku	butik	k1gInSc2	butik
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
její	její	k3xOp3gFnSc7	její
slavný	slavný	k2eAgInSc1d1	slavný
parfém	parfém	k1gInSc1	parfém
v	v	k7c6	v
charakteristickém	charakteristický	k2eAgInSc6d1	charakteristický
flakonu	flakon	k1gInSc6	flakon
<g/>
,	,	kIx,	,
Chanel	Chanel	k1gInSc1	Chanel
No	no	k9	no
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
)	)	kIx)	)
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
salony	salon	k1gInPc4	salon
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
prožila	prožít	k5eAaPmAgFnS	prožít
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
důstojníkem	důstojník	k1gMnSc7	důstojník
Hansem	Hans	k1gMnSc7	Hans
Güntherem	Günther	k1gMnSc7	Günther
von	von	k1gInSc4	von
Dincklage	Dincklag	k1gFnSc2	Dincklag
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přátelila	přátelit	k5eAaImAgFnS	přátelit
s	s	k7c7	s
Walterem	Walter	k1gMnSc7	Walter
Schellenbergem	Schellenberg	k1gMnSc7	Schellenberg
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
musela	muset	k5eAaImAgFnS	muset
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zrádkyně	zrádkyně	k1gFnSc1	zrádkyně
<g/>
"	"	kIx"	"
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
posléze	posléze	k6eAd1	posléze
strávila	strávit	k5eAaPmAgFnS	strávit
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
však	však	k9	však
svoji	svůj	k3xOyFgFnSc4	svůj
dílnu	dílna	k1gFnSc4	dílna
a	a	k8xC	a
pařížský	pařížský	k2eAgInSc4d1	pařížský
butik	butik	k1gInSc4	butik
opět	opět	k6eAd1	opět
otevřela	otevřít	k5eAaPmAgFnS	otevřít
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
udávala	udávat	k5eAaImAgFnS	udávat
módní	módní	k2eAgInSc4d1	módní
trend	trend	k1gInSc4	trend
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k6eAd1	Coco
Chanel	Chanela	k1gFnPc2	Chanela
zemřela	zemřít	k5eAaPmAgFnS	zemřít
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
87	[number]	k4	87
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
návrhářem	návrhář	k1gMnSc7	návrhář
značky	značka	k1gFnSc2	značka
Chanel	Chanela	k1gFnPc2	Chanela
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
byla	být	k5eAaImAgFnS	být
prostá	prostý	k2eAgFnSc1d1	prostá
venkovská	venkovský	k2eAgFnSc1d1	venkovská
dívka	dívka	k1gFnSc1	dívka
<g/>
;	;	kIx,	;
neměla	mít	k5eNaImAgFnS	mít
tedy	tedy	k9	tedy
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
ani	ani	k8xC	ani
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
však	však	k9	však
neobyčejnou	obyčejný	k2eNgFnSc4d1	neobyčejná
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	on	k3xPp3gFnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
předběhla	předběhnout	k5eAaPmAgFnS	předběhnout
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
-	-	kIx~	-
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společnost	společnost	k1gFnSc1	společnost
bránila	bránit	k5eAaImAgFnS	bránit
ženám	žena	k1gFnPc3	žena
projevit	projevit	k5eAaPmF	projevit
svou	svůj	k3xOyFgFnSc4	svůj
přirozenost	přirozenost	k1gFnSc4	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kurtizánou	kurtizána	k1gFnSc7	kurtizána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
využívala	využívat	k5eAaImAgFnS	využívat
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Kariéra	kariéra	k1gFnSc1	kariéra
módní	módní	k2eAgFnSc2d1	módní
návrhářky	návrhářka	k1gFnSc2	návrhářka
byla	být	k5eAaImAgFnS	být
důsledkem	důsledek	k1gInSc7	důsledek
jejího	její	k3xOp3gMnSc2	její
tvořivého	tvořivý	k2eAgMnSc2d1	tvořivý
ducha	duch	k1gMnSc2	duch
i	i	k8xC	i
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
nezávislosti	nezávislost	k1gFnSc6	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přepracovala	přepracovat	k5eAaPmAgFnS	přepracovat
pánské	pánský	k2eAgInPc4d1	pánský
střihy	střih	k1gInPc4	střih
na	na	k7c4	na
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
dámské	dámský	k2eAgNnSc4d1	dámské
oblečení	oblečení	k1gNnSc4	oblečení
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
vynalezla	vynaleznout	k5eAaPmAgFnS	vynaleznout
prakticky	prakticky	k6eAd1	prakticky
sportovní	sportovní	k2eAgNnSc4d1	sportovní
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
propagovala	propagovat	k5eAaImAgFnS	propagovat
své	svůj	k3xOyFgFnPc4	svůj
kreace	kreace	k1gFnPc4	kreace
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
pletených	pletený	k2eAgFnPc2d1	pletená
vest	vesta	k1gFnPc2	vesta
<g/>
,	,	kIx,	,
žerzejových	žerzejový	k2eAgInPc2d1	žerzejový
kostýmků	kostýmek	k1gInPc2	kostýmek
<g/>
,	,	kIx,	,
svetrů	svetr	k1gInPc2	svetr
a	a	k8xC	a
námořnických	námořnický	k2eAgFnPc2d1	námořnická
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
nosila	nosit	k5eAaImAgNnP	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
jejího	její	k3xOp3gInSc2	její
šatníku	šatník	k1gInSc2	šatník
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
"	"	kIx"	"
<g/>
malé	malý	k2eAgInPc1d1	malý
černé	černý	k2eAgInPc1d1	černý
šaty	šat	k1gInPc1	šat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
košilové	košilový	k2eAgFnSc2d1	košilová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jejích	její	k3xOp3gInPc2	její
doplňků	doplněk	k1gInPc2	doplněk
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
kabelky	kabelka	k1gFnPc1	kabelka
<g/>
,	,	kIx,	,
pásky	pásek	k1gInPc1	pásek
<g/>
,	,	kIx,	,
šály	šála	k1gFnPc4	šála
a	a	k8xC	a
bižuterii	bižuterie	k1gFnSc4	bižuterie
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zavedla	zavést	k5eAaPmAgFnS	zavést
do	do	k7c2	do
módy	móda	k1gFnSc2	móda
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pážecí	pážecí	k2eAgInSc1d1	pážecí
účes	účes	k1gInSc1	účes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
velkoknížete	velkokníže	k1gNnSc2wR	velkokníže
Dmitrije	Dmitrije	k1gFnSc2	Dmitrije
Romanova	Romanův	k2eAgFnSc1d1	Romanova
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
parfumérem	parfumér	k1gMnSc7	parfumér
Ernestem	Ernest	k1gMnSc7	Ernest
Beauxem	Beaux	k1gInSc7	Beaux
z	z	k7c2	z
Monte	Mont	k1gInSc5	Mont
Carla	Carl	k1gMnSc2	Carl
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
klasický	klasický	k2eAgInSc4d1	klasický
parfém	parfém	k1gInSc4	parfém
Chanel	Chanel	k1gMnSc1	Chanel
No	no	k9	no
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
71	[number]	k4	71
letech	léto	k1gNnPc6	léto
přerušila	přerušit	k5eAaPmAgFnS	přerušit
odpočinek	odpočinek	k1gInSc4	odpočinek
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
snad	snad	k9	snad
nejtrvalejší	trvalý	k2eAgFnSc1d3	nejtrvalejší
a	a	k8xC	a
nejznámější	známý	k2eAgInSc1d3	nejznámější
typ	typ	k1gInSc1	typ
oděvu	oděv	k1gInSc2	oděv
-	-	kIx~	-
kostým	kostým	k1gInSc1	kostým
"	"	kIx"	"
<g/>
Chanel	Chanel	k1gInSc1	Chanel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
krátkého	krátký	k2eAgNnSc2d1	krátké
saka	sako	k1gNnSc2	sako
bez	bez	k7c2	bez
límce	límec	k1gInSc2	límec
a	a	k8xC	a
sukně	sukně	k1gFnSc2	sukně
po	po	k7c4	po
kolena	koleno	k1gNnPc4	koleno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
významným	významný	k2eAgInSc7d1	významný
ozdobným	ozdobný	k2eAgInSc7d1	ozdobný
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
lemování	lemování	k1gNnSc1	lemování
saka	sako	k1gNnSc2	sako
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
portou	porta	k1gFnSc7	porta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
<g/>
,	,	kIx,	,
eleganci	elegance	k1gFnSc3	elegance
a	a	k8xC	a
kráse	krása	k1gFnSc3	krása
ho	on	k3xPp3gNnSc4	on
stále	stále	k6eAd1	stále
nosí	nosit	k5eAaImIp3nP	nosit
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časech	čas	k1gInPc6	čas
největší	veliký	k2eAgFnSc2d3	veliký
slávy	sláva	k1gFnSc2	sláva
oblékala	oblékat	k5eAaImAgFnS	oblékat
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
módní	módní	k2eAgFnPc4d1	módní
ikony	ikona	k1gFnPc4	ikona
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
se	se	k3xPyFc4	se
řadily	řadit	k5eAaImAgFnP	řadit
například	například	k6eAd1	například
Audrey	Audre	k2eAgFnPc1d1	Audre
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
,	,	kIx,	,
Grace	Grace	k1gFnSc1	Grace
Kellyová	Kellyová	k1gFnSc1	Kellyová
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
monacká	monacký	k2eAgFnSc1d1	monacká
kněžna	kněžna	k1gFnSc1	kněžna
Gracia	Gracius	k1gMnSc2	Gracius
Patricia	Patricius	k1gMnSc2	Patricius
<g/>
)	)	kIx)	)
či	či	k8xC	či
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
dílně	dílna	k1gFnSc6	dílna
vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
mnohé	mnohý	k2eAgInPc1d1	mnohý
divadelní	divadelní	k2eAgInPc1d1	divadelní
a	a	k8xC	a
filmové	filmový	k2eAgInPc1d1	filmový
kostýmy	kostým	k1gInPc1	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ošklivých	ošklivý	k2eAgFnPc2d1	ošklivá
žen	žena	k1gFnPc2	žena
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neumějí	umět	k5eNaImIp3nP	umět
udělat	udělat	k5eAaPmF	udělat
krásnými	krásný	k2eAgInPc7d1	krásný
<g/>
.	.	kIx.	.
</s>
<s>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
nelíbil	líbit	k5eNaImAgMnS	líbit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nenosí	nosit	k5eNaImIp3nP	nosit
parfém	parfém	k1gInSc4	parfém
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Móda	móda	k1gFnSc1	móda
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
styl	styl	k1gInSc1	styl
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Coco	Coco	k1gNnSc1	Coco
Chanel	Chanela	k1gFnPc2	Chanela
úspěšně	úspěšně	k6eAd1	úspěšně
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
její	její	k3xOp3gFnSc4	její
postavu	postava	k1gFnSc4	postava
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Audrey	Audrea	k1gFnSc2	Audrea
Tatou	tata	k1gMnSc7	tata
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Anne	Annus	k1gMnSc5	Annus
Fontaine	Fontain	k1gMnSc5	Fontain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uveden	uvést	k5eAaPmNgInS	uvést
také	také	k9	také
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
&	&	k?	&
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gFnSc4	její
postavu	postava	k1gFnSc4	postava
hrála	hrát	k5eAaImAgFnS	hrát
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Anna	Anna	k1gFnSc1	Anna
Mouglalis	Mouglalis	k1gFnSc1	Mouglalis
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kounen	Kounen	k2eAgMnSc1d1	Kounen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
jako	jako	k8xC	jako
koprodukce	koprodukce	k1gFnSc1	koprodukce
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
arte	art	k1gMnSc2	art
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hrála	hrát	k5eAaImAgFnS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
mladé	mladý	k2eAgNnSc1d1	mladé
Coco	Coco	k1gNnSc1	Coco
Chanel	Chanel	k1gInSc1	Chanel
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
Barbora	Barbora	k1gFnSc1	Barbora
Bobuľová	Bobuľová	k1gFnSc1	Bobuľová
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
jejího	její	k3xOp3gInSc2	její
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
ztělesnila	ztělesnit	k5eAaPmAgFnS	ztělesnit
Shirley	Shirle	k2eAgFnPc4d1	Shirle
MacLaine	MacLain	k1gMnSc5	MacLain
<g/>
.	.	kIx.	.
</s>
<s>
Režii	režie	k1gFnSc4	režie
vedl	vést	k5eAaImAgMnS	vést
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
Christian	Christian	k1gMnSc1	Christian
Duguay	Duguaa	k1gFnSc2	Duguaa
<g/>
.	.	kIx.	.
</s>
<s>
HUGHES	HUGHES	kA	HUGHES
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
obrazová	obrazový	k2eAgFnSc1d1	obrazová
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
256	[number]	k4	256
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Móda	móda	k1gFnSc1	móda
<g/>
,	,	kIx,	,
proměnlivé	proměnlivý	k2eAgInPc4d1	proměnlivý
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc1	Chanel
-	-	kIx~	-
Umění	umění	k1gNnSc1	umění
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
a	a	k8xC	a
móda	móda	k1gFnSc1	móda
<g/>
,	,	kIx,	,
s.	s.	k?	s.
566	[number]	k4	566
<g/>
.	.	kIx.	.
</s>
<s>
GIDEL	GIDEL	kA	GIDEL
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7407	[number]	k4	7407
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
308	[number]	k4	308
<g/>
.	.	kIx.	.
</s>
<s>
Anja	Anja	k1gMnSc1	Anja
Gockel	Gockel	k1gMnSc1	Gockel
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
Horst	Horst	k1gMnSc1	Horst
P.	P.	kA	P.
Horst	Horst	k1gMnSc1	Horst
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc4	Chanel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Coco	Coco	k6eAd1	Coco
Chanel	Chanela	k1gFnPc2	Chanela
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
Další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
Coco	Coco	k1gNnSc4	Coco
Chanel	Chanela	k1gFnPc2	Chanela
</s>
