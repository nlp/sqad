<s>
Grada	Grada	k1gFnSc1
</s>
<s>
GradaZákladní	GradaZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
U	u	k7c2
průhonu	průhon	k1gInSc2
466	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
170	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Identifikátory	identifikátor	k1gInPc1
IČO	IČO	kA
</s>
<s>
48110248	#num#	k4
LEI	lei	k1gInSc2
</s>
<s>
31570054CFXUU1D9EX76	31570054CFXUU1D9EX76	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
nakladatelství	nakladatelství	k1gNnSc4
odborné	odborný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
působící	působící	k2eAgFnSc2d1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
2	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgInSc1d3
mezi	mezi	k7c7
českými	český	k2eAgMnPc7d1
nakladateli	nakladatel	k1gMnPc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Působí	působit	k5eAaImIp3nS
také	také	k9
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokrývá	pokrývat	k5eAaImIp3nS
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
odborné	odborný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
z	z	k7c2
nejrůznějších	různý	k2eAgFnPc2d3
oblastí	oblast	k1gFnPc2
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
produkci	produkce	k1gFnSc6
jsou	být	k5eAaImIp3nP
tituly	titul	k1gInPc1
s	s	k7c7
právní	právní	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc1
ekonomické	ekonomický	k2eAgFnPc1d1
<g/>
,	,	kIx,
tituly	titul	k1gInPc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
financí	finance	k1gFnPc2
a	a	k8xC
účetnictví	účetnictví	k1gNnSc2
<g/>
,	,	kIx,
manažerské	manažerský	k2eAgInPc1d1
tituly	titul	k1gInPc1
<g/>
,	,	kIx,
psychologické	psychologický	k2eAgInPc1d1
<g/>
,	,	kIx,
zdravotnické	zdravotnický	k2eAgInPc1d1
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc1
s	s	k7c7
počítačovou	počítačový	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
<g/>
,	,	kIx,
o	o	k7c6
architektuře	architektura	k1gFnSc6
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc6
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc4
technické	technický	k2eAgFnPc4d1
z	z	k7c2
nejrůznějších	různý	k2eAgInPc2d3
oborů	obor	k1gInPc2
a	a	k8xC
profesí	profes	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
škálu	škála	k1gFnSc4
doplňuje	doplňovat	k5eAaImIp3nS
i	i	k9
naučně	naučně	k6eAd1
odborná	odborný	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nabízí	nabízet	k5eAaImIp3nS
řadu	řada	k1gFnSc4
publikací	publikace	k1gFnPc2
s	s	k7c7
velkým	velký	k2eAgInSc7d1
záběrem	záběr	k1gInSc7
pro	pro	k7c4
denní	denní	k2eAgInSc4d1
praktický	praktický	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
sport	sport	k1gInSc1
a	a	k8xC
cestování	cestování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1
akcionářem	akcionář	k1gMnSc7
společnosti	společnost	k1gFnSc2
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
je	být	k5eAaImIp3nS
Roman	Roman	k1gMnSc1
Sviták	Sviták	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Šestice	šestice	k1gFnSc2
odborných	odborný	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
získala	získat	k5eAaPmAgFnS
Ceny	cena	k1gFnPc1
Grady	grad	k1gInPc7
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-06-03	2010-06-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Roman	Roman	k1gMnSc1
Sviták	Sviták	k1gMnSc1
<g/>
:	:	kIx,
E-knihy	E-knih	k1gInPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
nás	my	k3xPp1nPc4
v	v	k7c6
tržbách	tržba	k1gFnPc6
"	"	kIx"
<g/>
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
nic	nic	k3yNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-03	2013-01-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
STINGL	STINGL	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ROMAN	Romana	k1gFnPc2
SVITÁK	SVITÁK	kA
<g/>
:	:	kIx,
Kolegové	kolega	k1gMnPc1
mi	já	k3xPp1nSc3
říkají	říkat	k5eAaImIp3nP
Tlačič	Tlačič	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-10-16	2006-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
BLAŽEK	Blažek	k1gMnSc1
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgNnSc1d1
fakta	faktum	k1gNnPc1
o	o	k7c6
produkci	produkce	k1gFnSc6
knih	kniha	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaz	svaz	k1gInSc4
českých	český	k2eAgMnPc2d1
knihkupců	knihkupec	k1gMnPc2
a	a	k8xC
nakladatelů	nakladatel	k1gMnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Grada	Grada	k1gFnSc1
v	v	k7c6
Adresáři	adresář	k1gInSc6
nakladatelů	nakladatel	k1gMnPc2
v	v	k7c6
ČR	ČR	kA
(	(	kIx(
<g/>
NAK	NAK	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
http://ekonom.ihned.cz/c1-59374970-pirati-ohrozuji-zivot-ceske-knizky	http://ekonom.ihned.cz/c1-59374970-pirati-ohrozuji-zivot-ceske-knizka	k1gFnPc1
</s>
<s>
https://web.archive.org/web/20110920032232/http://old.1000leaders.cz/hledej.asp?S=	https://web.archive.org/web/20110920032232/http://old.1000leaders.cz/hledej.asp?S=	k4
</s>
<s>
http://www.novinky.cz/veda-skoly/vzdelavani/181021-nakladatel-roman-svitak-internet-je-plny-informaci-ale-knihy-nabizeji-znalosti.html	http://www.novinky.cz/veda-skoly/vzdelavani/181021-nakladatel-roman-svitak-internet-je-plny-informaci-ale-knihy-nabizeji-znalosti.html	k1gMnSc1
</s>
