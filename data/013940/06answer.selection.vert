<s>
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
nakladatelství	nakladatelství	k1gNnSc4
odborné	odborný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
působící	působící	k2eAgFnSc2d1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
2	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgInSc1d3
mezi	mezi	k7c7
českými	český	k2eAgMnPc7d1
nakladateli	nakladatel	k1gMnPc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Působí	působit	k5eAaImIp3nS
také	také	k9
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>