<s>
Vlak	vlak	k1gInSc1	vlak
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc1	několik
pevně	pevně	k6eAd1	pevně
spojených	spojený	k2eAgNnPc2d1	spojené
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc3d1	jiná
pevné	pevný	k2eAgFnSc3d1	pevná
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
slovo	slovo	k1gNnSc1	slovo
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
mimodrážní	mimodrážní	k2eAgFnPc4d1	mimodrážní
jízdní	jízdní	k2eAgFnPc4d1	jízdní
soupravy	souprava	k1gFnPc4	souprava
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
délkou	délka	k1gFnSc7	délka
nebo	nebo	k8xC	nebo
vzhledem	vzhled	k1gInSc7	vzhled
kolejový	kolejový	k2eAgInSc4d1	kolejový
vlak	vlak	k1gInSc4	vlak
připomínají	připomínat	k5eAaImIp3nP	připomínat
(	(	kIx(	(
<g/>
silniční	silniční	k2eAgInSc4d1	silniční
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgInSc4d1	silniční
vláček	vláček	k1gInSc4	vláček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
češtině	čeština	k1gFnSc6	čeština
slovo	slovo	k1gNnSc1	slovo
označovalo	označovat	k5eAaImAgNnS	označovat
náčiní	náčiní	k1gNnSc4	náčiní
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
vláčí	vláčet	k5eAaImIp3nS	vláčet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
dějového	dějový	k2eAgNnSc2d1	dějové
jména	jméno	k1gNnSc2	jméno
volkъ	volkъ	k?	volkъ
<g/>
,	,	kIx,	,
označujího	označují	k2eAgNnSc2d1	označují
vlečení	vlečení	k1gNnSc2	vlečení
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
význam	význam	k1gInSc1	význam
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
kalk	kalk	k1gInSc1	kalk
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
der	drát	k5eAaImRp2nS	drát
Zug	Zug	k1gFnSc1	Zug
(	(	kIx(	(
<g/>
tah	tah	k1gInSc1	tah
i	i	k8xC	i
vlak	vlak	k1gInSc1	vlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bývaly	bývat	k5eAaImAgInP	bývat
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
i	i	k8xC	i
veřejném	veřejný	k2eAgInSc6d1	veřejný
provozu	provoz	k1gInSc6	provoz
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
a	a	k8xC	a
tažené	tažený	k2eAgFnSc2d1	tažená
koňmi	kůň	k1gMnPc7	kůň
(	(	kIx(	(
<g/>
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
parní	parní	k2eAgFnSc1d1	parní
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vléci	vléct	k5eAaImF	vléct
více	hodně	k6eAd2	hodně
železničních	železniční	k2eAgInPc2d1	železniční
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
těžší	těžký	k2eAgInPc4d2	těžší
vlaky	vlak	k1gInPc4	vlak
i	i	k8xC	i
vozy	vůz	k1gInPc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
parní	parní	k2eAgFnPc1d1	parní
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
dieselovými	dieselový	k2eAgFnPc7d1	dieselová
a	a	k8xC	a
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgInPc2d1	tradiční
vlaků	vlak	k1gInPc2	vlak
vedených	vedený	k2eAgInPc2d1	vedený
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
prosadily	prosadit	k5eAaPmAgFnP	prosadit
dieselové	dieselový	k2eAgInPc1d1	dieselový
i	i	k8xC	i
elektrické	elektrický	k2eAgInPc1d1	elektrický
motorové	motorový	k2eAgInPc1d1	motorový
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
ucelené	ucelený	k2eAgFnPc1d1	ucelená
obousměrné	obousměrný	k2eAgFnPc1d1	obousměrná
vlakové	vlakový	k2eAgFnPc1d1	vlaková
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
vlaků	vlak	k1gInPc2	vlak
směřuje	směřovat	k5eAaImIp3nS	směřovat
vývoj	vývoj	k1gInSc4	vývoj
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
původně	původně	k6eAd1	původně
převažující	převažující	k2eAgInPc1d1	převažující
obousměrné	obousměrný	k2eAgInPc1d1	obousměrný
motorové	motorový	k2eAgInPc1d1	motorový
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
téměř	téměř	k6eAd1	téměř
vytlačeny	vytlačen	k2eAgInPc4d1	vytlačen
jednosměrnými	jednosměrný	k2eAgInPc7d1	jednosměrný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
elektrická	elektrický	k2eAgFnSc1d1	elektrická
trakce	trakce	k1gFnSc1	trakce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
obecně	obecně	k6eAd1	obecně
závazných	závazný	k2eAgInPc2d1	závazný
drážních	drážní	k2eAgInPc2d1	drážní
předpisů	předpis	k1gInPc2	předpis
je	být	k5eAaImIp3nS	být
vlak	vlak	k1gInSc1	vlak
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
souprava	souprava	k1gFnSc1	souprava
<g/>
)	)	kIx)	)
spojených	spojený	k2eAgNnPc2d1	spojené
drážních	drážní	k2eAgNnPc2d1	drážní
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
kolejových	kolejový	k2eAgInPc2d1	kolejový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc4	jeden
je	být	k5eAaImIp3nS	být
hnací	hnací	k2eAgFnSc1d1	hnací
respektive	respektive	k9	respektive
trakční	trakční	k2eAgFnSc1d1	trakční
<g/>
,	,	kIx,	,
opatřená	opatřený	k2eAgFnSc1d1	opatřená
předepsanými	předepsaný	k2eAgFnPc7d1	předepsaná
návěstmi	návěst	k1gFnPc7	návěst
(	(	kIx(	(
<g/>
např.	např.	kA	např.
začátek	začátek	k1gInSc1	začátek
a	a	k8xC	a
konec	konec	k1gInSc1	konec
vlaku	vlak	k1gInSc2	vlak
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlakovým	vlakový	k2eAgInSc7d1	vlakový
doprovodem	doprovod	k1gInSc7	doprovod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vlakové	vlakový	k2eAgFnPc4d1	vlaková
náležitosti	náležitost	k1gFnPc4	náležitost
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedoucí	jedoucí	k2eAgFnPc1d1	jedoucí
podle	podle	k7c2	podle
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
pokynů	pokyn	k1gInPc2	pokyn
osoby	osoba	k1gFnSc2	osoba
odborně	odborně	k6eAd1	odborně
způsobilé	způsobilý	k2eAgFnSc2d1	způsobilá
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
výpravčího	výpravčí	k1gMnSc4	výpravčí
nebo	nebo	k8xC	nebo
dispečera	dispečer	k1gMnSc4	dispečer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
samostatné	samostatný	k2eAgNnSc1d1	samostatné
drážní	drážní	k2eAgNnSc1d1	drážní
vozidlo	vozidlo	k1gNnSc1	vozidlo
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
hnací	hnací	k2eAgFnSc7d1	hnací
nebo	nebo	k8xC	nebo
speciální	speciální	k2eAgFnSc7d1	speciální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dopravním	dopravní	k2eAgInSc6d1	dopravní
řádu	řád	k1gInSc6	řád
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
§	§	k?	§
1	[number]	k4	1
písm	písma	k1gFnPc2	písma
<g/>
.	.	kIx.	.
k	k	k7c3	k
<g/>
)	)	kIx)	)
vyhl	vyhl	k1gMnSc1	vyhl
<g/>
.	.	kIx.	.
173	[number]	k4	173
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
definován	definovat	k5eAaBmNgInS	definovat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
drážní	drážní	k2eAgFnSc4d1	drážní
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
železnice	železnice	k1gFnSc2	železnice
včetně	včetně	k7c2	včetně
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
trolejbusovou	trolejbusový	k2eAgFnSc4d1	trolejbusová
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgFnPc4d1	pozemní
<g/>
,	,	kIx,	,
visuté	visutý	k2eAgFnPc4d1	visutá
kabinové	kabinový	k2eAgFnPc4d1	kabinová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
teoreticky	teoreticky	k6eAd1	teoreticky
také	také	k9	také
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
například	například	k6eAd1	například
pro	pro	k7c4	pro
důlní	důlní	k2eAgFnPc4d1	důlní
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnPc4d1	průmyslová
dráhy	dráha	k1gFnPc4	dráha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
dráhách	dráha	k1gFnPc6	dráha
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
vozidel	vozidlo	k1gNnPc2	vozidlo
spojených	spojený	k2eAgFnPc2d1	spojená
a	a	k8xC	a
přemisťovaných	přemisťovaný	k2eAgFnPc2d1	přemisťovaný
při	při	k7c6	při
posunu	posun	k1gInSc6	posun
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
spouštěných	spouštěný	k2eAgNnPc2d1	spouštěné
vozidel	vozidlo	k1gNnPc2	vozidlo
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohonu	pohon	k1gInSc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Souprava	souprava	k1gFnSc1	souprava
automatického	automatický	k2eAgNnSc2d1	automatické
metra	metro	k1gNnSc2	metro
bez	bez	k7c2	bez
vlakového	vlakový	k2eAgInSc2d1	vlakový
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
,	,	kIx,	,
o	o	k7c6	o
jakých	jaký	k3yRgFnPc6	jaký
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
D	D	kA	D
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
definice	definice	k1gFnSc2	definice
vlaku	vlak	k1gInSc2	vlak
rovněž	rovněž	k9	rovněž
nespadala	spadat	k5eNaPmAgFnS	spadat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
v	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
významu	význam	k1gInSc6	význam
používají	používat	k5eAaImIp3nP	používat
termíny	termín	k1gInPc4	termín
jízdní	jízdní	k2eAgFnSc1d1	jízdní
souprava	souprava	k1gFnSc1	souprava
nebo	nebo	k8xC	nebo
potahové	potahový	k2eAgNnSc1d1	potahové
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
však	však	k9	však
někdy	někdy	k6eAd1	někdy
obdobné	obdobný	k2eAgNnSc4d1	obdobné
označení	označení	k1gNnSc4	označení
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
(	(	kIx(	(
<g/>
silniční	silniční	k2eAgInSc1d1	silniční
vláček	vláček	k1gInSc1	vláček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
se	s	k7c7	s
vlakem	vlak	k1gInSc7	vlak
rozumí	rozumět	k5eAaImIp3nS	rozumět
též	též	k9	též
spoj	spoj	k1gInSc1	spoj
uvedený	uvedený	k2eAgInSc1d1	uvedený
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jízda	jízda	k1gFnSc1	jízda
vlaku	vlak	k1gInSc2	vlak
v	v	k7c6	v
určené	určený	k2eAgFnSc6d1	určená
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
časech	čas	k1gInPc6	čas
pod	pod	k7c7	pod
stanoveným	stanovený	k2eAgNnSc7d1	stanovené
označením	označení	k1gNnSc7	označení
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
vlaky	vlak	k1gInPc4	vlak
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
vlak	vlak	k1gInSc4	vlak
osobní	osobní	k2eAgFnSc2d1	osobní
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
a	a	k8xC	a
spěšnin	spěšnina	k1gFnPc2	spěšnina
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
vlaky	vlak	k1gInPc4	vlak
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
vlaky	vlak	k1gInPc4	vlak
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
soupravové	soupravový	k2eAgInPc4d1	soupravový
vlaky	vlak	k1gInPc4	vlak
a	a	k8xC	a
neveřejné	veřejný	k2eNgFnPc4d1	neveřejná
jízdy	jízda	k1gFnPc4	jízda
<g/>
.	.	kIx.	.
nákladní	nákladní	k2eAgInPc4d1	nákladní
smíšené	smíšený	k2eAgInPc4d1	smíšený
<g/>
;	;	kIx,	;
vlaky	vlak	k1gInPc4	vlak
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
také	také	k9	také
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
lokomotivní	lokomotivní	k2eAgFnSc4d1	lokomotivní
služební	služební	k2eAgFnSc4d1	služební
Specifickým	specifický	k2eAgInSc7d1	specifický
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
vojenské	vojenský	k2eAgInPc1d1	vojenský
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
a	a	k8xC	a
povahy	povaha	k1gFnSc2	povaha
přepravy	přeprava	k1gFnSc2	přeprava
lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
veřejné	veřejný	k2eAgFnSc2d1	veřejná
přepravy	přeprava	k1gFnSc2	přeprava
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vlaky	vlak	k1gInPc1	vlak
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
provozovatelé	provozovatel	k1gMnPc1	provozovatel
či	či	k8xC	či
regulátoři	regulátor	k1gMnPc1	regulátor
drah	draha	k1gFnPc2	draha
či	či	k8xC	či
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dopravu	doprava	k1gFnSc4	doprava
dálkovou	dálkový	k2eAgFnSc4d1	dálková
nebo	nebo	k8xC	nebo
místní	místní	k2eAgFnSc4d1	místní
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvalitativních	kvalitativní	k2eAgInPc2d1	kvalitativní
standardů	standard	k1gInPc2	standard
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
přesnost	přesnost	k1gFnSc4	přesnost
(	(	kIx(	(
<g/>
stupně	stupeň	k1gInSc2	stupeň
preference	preference	k1gFnSc1	preference
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
vlaky	vlak	k1gInPc7	vlak
<g/>
)	)	kIx)	)
a	a	k8xC	a
komfort	komfort	k1gInSc4	komfort
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgInPc1d1	uvedený
typy	typ	k1gInPc1	typ
vlaků	vlak	k1gInPc2	vlak
byla	být	k5eAaImAgFnS	být
či	či	k8xC	či
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
dráhy	dráha	k1gFnSc2	dráha
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
nebo	nebo	k8xC	nebo
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
dráhy	dráha	k1gFnSc2	dráha
SŽDC	SŽDC	kA	SŽDC
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
jiných	jiný	k2eAgMnPc2d1	jiný
provozovatelů	provozovatel	k1gMnPc2	provozovatel
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používán	k2eAgFnPc4d1	používána
stejné	stejný	k2eAgFnPc4d1	stejná
nebo	nebo	k8xC	nebo
obdobné	obdobný	k2eAgFnPc4d1	obdobná
kategorie	kategorie	k1gFnPc4	kategorie
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Expresy	expres	k1gInPc1	expres
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
<g/>
)	)	kIx)	)
-	-	kIx~	-
vlaky	vlak	k1gInPc1	vlak
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
u	u	k7c2	u
vlaků	vlak	k1gInPc2	vlak
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
zvolit	zvolit	k5eAaPmF	zvolit
i	i	k9	i
tzv.	tzv.	kA	tzv.
přepravní	přepravní	k2eAgNnSc1d1	přepravní
označení	označení	k1gNnSc1	označení
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
řízení	řízení	k1gNnSc2	řízení
provozu	provoz	k1gInSc2	provoz
ovšem	ovšem	k9	ovšem
nemá	mít	k5eNaImIp3nS	mít
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
předpisy	předpis	k1gInPc4	předpis
ani	ani	k8xC	ani
IS	IS	kA	IS
SŽDC	SŽDC	kA	SŽDC
tyto	tento	k3xDgFnPc4	tento
kategorie	kategorie	k1gFnPc4	kategorie
neznají	znát	k5eNaImIp3nP	znát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
ČD	ČD	kA	ČD
a.s.	a.s.	k?	a.s.
používá	používat	k5eAaImIp3nS	používat
následující	následující	k2eAgFnSc1d1	následující
přepravní	přepravní	k2eAgFnSc1d1	přepravní
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
dopravci	dopravce	k1gMnPc1	dopravce
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
svá	svůj	k3xOyFgNnPc4	svůj
označení	označení	k1gNnPc4	označení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ex	ex	k6eAd1	ex
vlaky	vlak	k1gInPc4	vlak
dopravce	dopravce	k1gMnSc1	dopravce
Leo	Leo	k1gMnSc1	Leo
expres	expres	k6eAd1	expres
mají	mít	k5eAaImIp3nP	mít
přepravní	přepravní	k2eAgNnSc4d1	přepravní
označení	označení	k1gNnSc4	označení
LE	LE	kA	LE
<g/>
,	,	kIx,	,
alebo	aleba	k1gFnSc5	aleba
IC	IC	kA	IC
vlaky	vlak	k1gInPc1	vlak
dopr.	dopr.	k?	dopr.
<g/>
RegioJet	RegioJet	k1gInSc4	RegioJet
mají	mít	k5eAaImIp3nP	mít
přepravní	přepravní	k2eAgNnSc4d1	přepravní
označení	označení	k1gNnSc4	označení
RJ	RJ	kA	RJ
<g/>
.	.	kIx.	.
</s>
<s>
EuroCity	EuroCita	k1gFnPc1	EuroCita
(	(	kIx(	(
<g/>
EC	EC	kA	EC
<g/>
)	)	kIx)	)
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
vlaky	vlak	k1gInPc4	vlak
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kvality	kvalita	k1gFnSc2	kvalita
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
cestovním	cestovní	k2eAgInSc7d1	cestovní
komfortem	komfort	k1gInSc7	komfort
<g/>
,	,	kIx,	,
krátkou	krátký	k2eAgFnSc7d1	krátká
cestovní	cestovní	k2eAgFnSc7d1	cestovní
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
možnou	možný	k2eAgFnSc7d1	možná
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
službami	služba	k1gFnPc7	služba
InterCity	InterCita	k1gFnSc2	InterCita
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
–	–	k?	–
především	především	k6eAd1	především
vnitrostátní	vnitrostátní	k2eAgInPc1d1	vnitrostátní
vlaky	vlak	k1gInPc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
cestovním	cestovní	k2eAgInSc7d1	cestovní
komfortem	komfort	k1gInSc7	komfort
SuperCity	SuperCita	k1gFnSc2	SuperCita
(	(	kIx(	(
<g/>
SC	SC	kA	SC
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
nebo	nebo	k8xC	nebo
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
vlaky	vlak	k1gInPc4	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
s	s	k7c7	s
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
cestovní	cestovní	k2eAgFnSc7d1	cestovní
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
především	především	k9	především
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
jednotkou	jednotka	k1gFnSc7	jednotka
680	[number]	k4	680
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Pendolino	Pendolin	k2eAgNnSc5d1	Pendolino
<g/>
.	.	kIx.	.
</s>
<s>
EuroNight	EuroNight	k1gMnSc1	EuroNight
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
–	–	k?	–
noční	noční	k2eAgInPc1d1	noční
vlaky	vlak	k1gInPc1	vlak
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
a	a	k8xC	a
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
centra	centrum	k1gNnPc4	centrum
<g/>
,	,	kIx,	,
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
vysokým	vysoký	k2eAgInSc7d1	vysoký
cestovním	cestovní	k2eAgInSc7d1	cestovní
komfortem	komfort	k1gInSc7	komfort
a	a	k8xC	a
omezeným	omezený	k2eAgNnSc7d1	omezené
zastavováním	zastavování	k1gNnSc7	zastavování
od	od	k7c2	od
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
h.	h.	k?	h.
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
<g />
.	.	kIx.	.
</s>
<s>
kategorie	kategorie	k1gFnSc1	kategorie
vlaku	vlak	k1gInSc2	vlak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
zavedena	zavést	k5eAaPmNgFnS	zavést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
jezdí	jezdit	k5eAaImIp3nS	jezdit
již	již	k6eAd1	již
4	[number]	k4	4
páry	pára	k1gFnSc2	pára
vlaků	vlak	k1gInPc2	vlak
EN	EN	kA	EN
<g/>
)	)	kIx)	)
rychlíky	rychlík	k1gInPc1	rychlík
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
přepravu	přeprava	k1gFnSc4	přeprava
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
spěšné	spěšný	k2eAgFnSc2d1	spěšná
<g />
.	.	kIx.	.
</s>
<s>
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
Sp	Sp	k1gFnSc1	Sp
<g/>
)	)	kIx)	)
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
přepravu	přeprava	k1gFnSc4	přeprava
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
městech	město	k1gNnPc6	město
osobní	osobní	k2eAgInSc4d1	osobní
vlaky	vlak	k1gInPc4	vlak
(	(	kIx(	(
<g/>
Os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
přepravu	přeprava	k1gFnSc4	přeprava
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zastávek	zastávka	k1gFnPc2	zastávka
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
trati	trať	k1gFnSc6	trať
soupravové	soupravový	k2eAgInPc1d1	soupravový
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
Sv	sv	kA	sv
<g/>
)	)	kIx)	)
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
zajišťující	zajišťující	k2eAgInPc1d1	zajišťující
přemístění	přemístění	k1gNnSc3	přemístění
soupravy	souprava	k1gFnSc2	souprava
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
do	do	k7c2	do
výchozí	výchozí	k2eAgFnSc2d1	výchozí
nebo	nebo	k8xC	nebo
koncové	koncový	k2eAgFnSc2d1	koncová
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
nepřepravují	přepravovat	k5eNaImIp3nP	přepravovat
cestující	cestující	k2eAgInPc4d1	cestující
Vlaky	vlak	k1gInPc4	vlak
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
či	či	k8xC	či
vozy	vůz	k1gInPc4	vůz
lze	lze	k6eAd1	lze
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
i	i	k9	i
podle	podle	k7c2	podle
dalších	další	k2eAgNnPc2d1	další
kritérií	kritérion	k1gNnPc2	kritérion
na	na	k7c4	na
povinně	povinně	k6eAd1	povinně
či	či	k8xC	či
nepovinně	povinně	k6eNd1	povinně
místenkové	místenkový	k2eAgFnSc2d1	místenková
<g/>
,	,	kIx,	,
lůžkové	lůžkový	k2eAgFnSc2d1	lůžková
či	či	k8xC	či
lehátkové	lehátkový	k2eAgFnSc2d1	lehátková
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sestavy	sestava	k1gFnSc2	sestava
a	a	k8xC	a
typu	typ	k1gInSc2	typ
vozů	vůz	k1gInPc2	vůz
mohou	moct	k5eAaImIp3nP	moct
vlaky	vlak	k1gInPc1	vlak
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
být	být	k5eAaImF	být
buď	buď	k8xC	buď
být	být	k5eAaImF	být
mezi	mezi	k7c4	mezi
vozy	vůz	k1gInPc4	vůz
průchozí	průchozí	k2eAgInPc4d1	průchozí
po	po	k7c6	po
přechodových	přechodový	k2eAgInPc6d1	přechodový
můstcích	můstek	k1gInPc6	můstek
či	či	k8xC	či
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
soupravy	souprava	k1gFnPc1	souprava
nebo	nebo	k8xC	nebo
neprůchozí	průchozí	k2eNgFnPc1d1	neprůchozí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nákladní	nákladní	k2eAgInSc4d1	nákladní
vlak	vlak	k1gInSc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nákladní	nákladní	k2eAgInSc1d1	nákladní
vlak	vlak	k1gInSc1	vlak
je	být	k5eAaImIp3nS	být
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
zboží	zboží	k1gNnSc2	zboží
po	po	k7c4	po
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
soupravy	souprava	k1gFnSc2	souprava
nákladních	nákladní	k2eAgInPc2d1	nákladní
vozů	vůz	k1gInPc2	vůz
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
prázdné	prázdný	k2eAgFnPc1d1	prázdná
nebo	nebo	k8xC	nebo
ložené	ložený	k2eAgFnPc1d1	ložená
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pohyb	pohyb	k1gInSc4	pohyb
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
síti	síť	k1gFnSc6	síť
SŽDC	SŽDC	kA	SŽDC
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
několik	několik	k4yIc1	několik
kategorií	kategorie	k1gFnPc2	kategorie
nákladních	nákladní	k2eAgInPc2d1	nákladní
vlaků	vlak	k1gInPc2	vlak
<g/>
:	:	kIx,	:
expresní	expresní	k2eAgFnSc7d1	expresní
(	(	kIx(	(
<g/>
NEx	NEx	k1gFnSc7	NEx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgNnSc4d1	rychlé
(	(	kIx(	(
<g/>
Rn	Rn	k1gFnSc4	Rn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
u	u	k7c2	u
ČD	ČD	kA	ČD
též	též	k9	též
spěšné	spěšný	k2eAgFnSc6d1	spěšná
(	(	kIx(	(
<g/>
Sn	Sn	k1gFnSc6	Sn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průběžné	průběžný	k2eAgNnSc4d1	průběžné
(	(	kIx(	(
<g/>
Pn	Pn	k1gFnSc4	Pn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyrovnávkové	vyrovnávek	k1gMnPc1	vyrovnávek
(	(	kIx(	(
<g/>
Vn	Vn	k1gMnSc1	Vn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manipulační	manipulační	k2eAgFnSc1d1	manipulační
(	(	kIx(	(
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
u	u	k7c2	u
ČD	ČD	kA	ČD
též	též	k9	též
přestavovací	přestavovací	k2eAgMnPc1d1	přestavovací
(	(	kIx(	(
<g/>
Pv	Pv	k1gMnPc1	Pv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc4	vlak
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c4	na
vlečku	vlečka	k1gFnSc4	vlečka
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
vlečkové	vlečkový	k2eAgNnSc1d1	vlečkové
(	(	kIx(	(
<g/>
Vleč	vléct	k5eAaImRp2nS	vléct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
existují	existovat	k5eAaImIp3nP	existovat
tyto	tento	k3xDgFnPc1	tento
kategorie	kategorie	k1gFnPc1	kategorie
<g/>
:	:	kIx,	:
lokomotivní	lokomotivní	k2eAgInPc1d1	lokomotivní
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
Lv	Lv	k1gFnSc1	Lv
<g/>
)	)	kIx)	)
–	–	k?	–
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
hnacích	hnací	k2eAgNnPc2d1	hnací
vozidel	vozidlo	k1gNnPc2	vozidlo
jedoucí	jedoucí	k2eAgInSc1d1	jedoucí
jako	jako	k8xC	jako
vlak	vlak	k1gInSc1	vlak
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
soupravový	soupravový	k2eAgInSc4d1	soupravový
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
služební	služební	k2eAgInPc4d1	služební
vlaky	vlak	k1gInPc4	vlak
(	(	kIx(	(
<g/>
Služ	sloužit	k5eAaImRp2nS	sloužit
<g/>
)	)	kIx)	)
–	–	k?	–
vlaky	vlak	k1gInPc1	vlak
zaváděné	zaváděný	k2eAgInPc1d1	zaváděný
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnSc2	potřeba
provozovatele	provozovatel	k1gMnSc4	provozovatel
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
pomocné	pomocný	k2eAgInPc1d1	pomocný
vlaky	vlak	k1gInPc1	vlak
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
Pom	Pom	k1gFnSc4	Pom
U	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
provozovatelů	provozovatel	k1gMnPc2	provozovatel
dráhy	dráha	k1gFnSc2	dráha
tomu	ten	k3xDgMnSc3	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
na	na	k7c6	na
vlečkách	vlečka	k1gFnPc6	vlečka
některých	některý	k3yIgMnPc2	některý
hutních	hutní	k2eAgMnPc2d1	hutní
podniků	podnik	k1gInPc2	podnik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vítkovice	Vítkovice	k1gInPc1	Vítkovice
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgFnPc4d1	dopravní
jednotky	jednotka	k1gFnPc4	jednotka
podobné	podobný	k2eAgFnPc4d1	podobná
vlakům	vlak	k1gInPc3	vlak
používáno	používán	k2eAgNnSc1d1	používáno
označení	označení	k1gNnSc1	označení
přesun	přesun	k1gInSc1	přesun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
vlečkách	vlečka	k1gFnPc6	vlečka
provozovaný	provozovaný	k2eAgInSc4d1	provozovaný
firmou	firma	k1gFnSc7	firma
OKD	OKD	kA	OKD
<g/>
,	,	kIx,	,
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
provozovány	provozovat	k5eAaImNgFnP	provozovat
tzv.	tzv.	kA	tzv.
přestavné	přestavný	k2eAgFnPc4d1	přestavná
jízdy	jízda	k1gFnPc4	jízda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
něčím	něco	k3yInSc7	něco
mezi	mezi	k7c7	mezi
vlakem	vlak	k1gInSc7	vlak
a	a	k8xC	a
posunem	posun	k1gInSc7	posun
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
železniční	železniční	k2eAgFnSc4d1	železniční
drahou	drahá	k1gFnSc4	drahá
speciální	speciální	k2eAgFnSc4d1	speciální
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
vlaků	vlak	k1gInPc2	vlak
upravuje	upravovat	k5eAaImIp3nS	upravovat
část	část	k1gFnSc1	část
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Dopravního	dopravní	k2eAgInSc2d1	dopravní
předpisu	předpis	k1gInSc2	předpis
pro	pro	k7c4	pro
metro	metro	k1gNnSc4	metro
<g/>
,	,	kIx,	,
D	D	kA	D
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
určení	určení	k1gNnSc2	určení
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
osobní	osobní	k2eAgFnSc4d1	osobní
<g/>
,	,	kIx,	,
služební	služební	k2eAgFnSc4d1	služební
a	a	k8xC	a
zkušební	zkušební	k2eAgFnSc4d1	zkušební
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
a	a	k8xC	a
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
vlak	vlak	k1gInSc1	vlak
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
legislativě	legislativa	k1gFnSc6	legislativa
a	a	k8xC	a
předpisech	předpis	k1gInPc6	předpis
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
předpisech	předpis	k1gInPc6	předpis
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
se	se	k3xPyFc4	se
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
vlak	vlak	k1gInSc1	vlak
nazývá	nazývat	k5eAaImIp3nS	nazývat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
zejména	zejména	k9	zejména
podle	podle	k7c2	podle
dopravního	dopravní	k2eAgNnSc2d1	dopravní
určení	určení	k1gNnSc2	určení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dopravní	dopravní	k2eAgInSc1d1	dopravní
a	a	k8xC	a
návěstní	návěstní	k2eAgInSc1d1	návěstní
předpis	předpis	k1gInSc1	předpis
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
a.	a.	k?	a.
s.	s.	k?	s.
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
tyto	tento	k3xDgFnPc4	tento
typy	typa	k1gFnPc4	typa
vlaků	vlak	k1gInPc2	vlak
<g/>
:	:	kIx,	:
vlaky	vlak	k1gInPc1	vlak
pro	pro	k7c4	pro
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gMnPc2	cestující
–	–	k?	–
podle	podle	k7c2	podle
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
nebo	nebo	k8xC	nebo
služebního	služební	k2eAgInSc2d1	služební
příkazu	příkaz	k1gInSc2	příkaz
vlaky	vlak	k1gInPc4	vlak
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
–	–	k?	–
objednané	objednaný	k2eAgFnSc2d1	objednaná
smluvní	smluvní	k2eAgFnSc2d1	smluvní
jízdy	jízda	k1gFnSc2	jízda
vlaky	vlak	k1gInPc1	vlak
služební	služební	k2eAgInPc1d1	služební
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
pracovníků	pracovník	k1gMnPc2	pracovník
nebo	nebo	k8xC	nebo
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
oprav	oprava	k1gFnPc2	oprava
nebo	nebo	k8xC	nebo
údržby	údržba	k1gFnSc2	údržba
tratí	trať	k1gFnPc2	trať
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
manipulační	manipulační	k2eAgFnSc2d1	manipulační
jízdy	jízda	k1gFnSc2	jízda
(	(	kIx(	(
<g/>
převozy	převoz	k1gInPc1	převoz
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
zkušební	zkušební	k2eAgFnSc2d1	zkušební
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
nepřepravující	přepravující	k2eNgFnSc2d1	přepravující
cestující	cestující	k1gFnSc2	cestující
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
vlaky	vlak	k1gInPc1	vlak
cvičné	cvičný	k2eAgInPc1d1	cvičný
Podle	podle	k7c2	podle
sestavy	sestava	k1gFnSc2	sestava
lze	lze	k6eAd1	lze
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
vlaky	vlak	k1gInPc1	vlak
jednovozové	jednovozový	k2eAgInPc1d1	jednovozový
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
jedním	jeden	k4xCgInSc7	jeden
hnacím	hnací	k2eAgNnSc7d1	hnací
vozidlem	vozidlo	k1gNnSc7	vozidlo
vlaky	vlak	k1gInPc1	vlak
tvořené	tvořený	k2eAgInPc1d1	tvořený
více	hodně	k6eAd2	hodně
hnacími	hnací	k2eAgFnPc7d1	hnací
vozidly	vozidlo	k1gNnPc7	vozidlo
vlaky	vlak	k1gInPc1	vlak
sunoucí	sunoucí	k2eAgInSc1d1	sunoucí
–	–	k?	–
funkční	funkční	k2eAgNnSc1d1	funkční
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
vůz	vůz	k1gInSc4	vůz
nebo	nebo	k8xC	nebo
vozy	vůz	k1gInPc4	vůz
nemotorové	motorový	k2eNgInPc4d1	nemotorový
<g/>
,	,	kIx,	,
neprovozní	provozní	k2eNgInPc4d1	neprovozní
nebo	nebo	k8xC	nebo
neprovozuschopné	provozuschopný	k2eNgInPc4d1	provozuschopný
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
voze	vůz	k1gInSc6	vůz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
brzdař	brzdař	k1gMnSc1	brzdař
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
souprava	souprava	k1gFnSc1	souprava
řízena	řízen	k2eAgFnSc1d1	řízena
z	z	k7c2	z
funkčního	funkční	k2eAgNnSc2d1	funkční
hnacího	hnací	k2eAgNnSc2d1	hnací
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
řidič	řidič	k1gMnSc1	řidič
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
vozu	vůz	k1gInSc2	vůz
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
soupravu	souprava	k1gFnSc4	souprava
řídit	řídit	k5eAaImF	řídit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
havarijní	havarijní	k2eAgInSc1d1	havarijní
pojezd	pojezd	k1gInSc1	pojezd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
vlaky	vlak	k1gInPc4	vlak
vlekoucí	vlekoucí	k2eAgFnSc2d1	vlekoucí
–	–	k?	–
funkční	funkční	k2eAgNnSc1d1	funkční
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g />
.	.	kIx.	.
</s>
<s>
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
vůz	vůz	k1gInSc4	vůz
nebo	nebo	k8xC	nebo
vozy	vůz	k1gInPc4	vůz
nemotorové	motorový	k2eNgInPc4d1	nemotorový
(	(	kIx(	(
<g/>
vlečné	vlečný	k2eAgInPc4d1	vlečný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neprovozní	provozní	k2eNgInPc1d1	neprovozní
nebo	nebo	k8xC	nebo
neprovozuschopné	provozuschopný	k2eNgInPc1d1	provozuschopný
vlaky	vlak	k1gInPc1	vlak
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
(	(	kIx(	(
<g/>
sunoucí	sunoucí	k2eAgFnSc2d1	sunoucí
a	a	k8xC	a
vlekoucí	vlekoucí	k2eAgFnSc2d1	vlekoucí
<g/>
)	)	kIx)	)
–	–	k?	–
hnací	hnací	k2eAgNnSc1d1	hnací
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
jsou	být	k5eAaImIp3nP	být
vůz	vůz	k1gInSc4	vůz
nebo	nebo	k8xC	nebo
vozy	vůz	k1gInPc4	vůz
nemotorové	motorový	k2eNgInPc4d1	nemotorový
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
neprovozní	provozní	k2eNgInPc1d1	neprovozní
nebo	nebo	k8xC	nebo
neprovozuschopné	provozuschopný	k2eNgInPc4d1	provozuschopný
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
dopravního	dopravní	k2eAgInSc2d1	dopravní
a	a	k8xC	a
návěstního	návěstní	k2eAgInSc2d1	návěstní
předpisu	předpis	k1gInSc2	předpis
vlaky	vlak	k1gInPc1	vlak
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
vozy	vůz	k1gInPc7	vůz
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
35	[number]	k4	35
m.	m.	k?	m.
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
manipulační	manipulační	k2eAgFnPc1d1	manipulační
jízdy	jízda	k1gFnPc1	jízda
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
může	moct	k5eAaImIp3nS	moct
vlak	vlak	k1gInSc1	vlak
tvořit	tvořit	k5eAaImF	tvořit
až	až	k9	až
šest	šest	k4xCc4	šest
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
povolená	povolený	k2eAgFnSc1d1	povolená
délka	délka	k1gFnSc1	délka
vlaku	vlak	k1gInSc2	vlak
je	být	k5eAaImIp3nS	být
105	[number]	k4	105
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vlakovou	vlakový	k2eAgFnSc4d1	vlaková
četu	četa	k1gFnSc4	četa
tramvajového	tramvajový	k2eAgInSc2d1	tramvajový
vlaku	vlak	k1gInSc2	vlak
tvoří	tvořit	k5eAaImIp3nS	tvořit
řidič	řidič	k1gMnSc1	řidič
nebo	nebo	k8xC	nebo
řidiči	řidič	k1gMnPc1	řidič
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
brzdař	brzdař	k1gMnSc1	brzdař
nebo	nebo	k8xC	nebo
brzdaři	brzdař	k1gMnPc1	brzdař
<g/>
,	,	kIx,	,
průvodčí	průvodčí	k1gMnPc1	průvodčí
či	či	k8xC	či
poučení	poučený	k2eAgMnPc1d1	poučený
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucím	vedoucí	k1gMnSc7	vedoucí
vlakové	vlakový	k2eAgFnSc2d1	vlaková
čety	četa	k1gFnSc2	četa
je	být	k5eAaImIp3nS	být
řidič	řidič	k1gMnSc1	řidič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinovaném	kombinovaný	k2eAgInSc6d1	kombinovaný
vlaku	vlak	k1gInSc6	vlak
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
dopravním	dopravní	k2eAgInSc7d1	dopravní
předpisem	předpis	k1gInSc7	předpis
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnSc1	první
řidič	řidič	k1gMnSc1	řidič
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
nadřízen	nadřídit	k5eAaPmNgInS	nadřídit
ostatním	ostatní	k2eAgMnSc7d1	ostatní
řidičům	řidič	k1gMnPc3	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Zacvičující	zacvičující	k2eAgInSc1d1	zacvičující
řidič	řidič	k1gInSc1	řidič
je	být	k5eAaImIp3nS	být
nadřízen	nadřízen	k2eAgInSc1d1	nadřízen
zacvičovanému	zacvičovaný	k2eAgInSc3d1	zacvičovaný
řidiči	řidič	k1gInSc3	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
instruktor	instruktor	k1gMnSc1	instruktor
při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
nebo	nebo	k8xC	nebo
zkoušce	zkouška	k1gFnSc6	zkouška
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
řídícího	řídící	k2eAgInSc2d1	řídící
řidiče	řidič	k1gInSc2	řidič
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
řídící	řídící	k2eAgMnSc1d1	řídící
řidič	řidič	k1gMnSc1	řidič
nemá	mít	k5eNaImIp3nS	mít
platný	platný	k2eAgInSc4d1	platný
průkaz	průkaz	k1gInSc4	průkaz
pro	pro	k7c4	pro
žádný	žádný	k3yNgInSc4	žádný
typ	typ	k1gInSc4	typ
tramvajového	tramvajový	k2eAgInSc2d1	tramvajový
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
