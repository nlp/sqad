<s>
New	New	k?
Canaan	Canaan	k1gInSc1
</s>
<s>
New	New	k?
Canaan	Canaan	k1gInSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
73	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
105	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
federální	federální	k2eAgFnSc2d1
stát	stát	k1gInSc1
</s>
<s>
Connecticut	Connecticut	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Fairfield	Fairfield	k1gInSc1
County	Counta	k1gFnSc2
</s>
<s>
Connecticut	Connecticut	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
New	New	k?
Canaan	Canaan	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
58,3	58,3	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
19	#num#	k4
984	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
349	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1731	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.newcanaan.info	www.newcanaan.info	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
203	#num#	k4
PSČ	PSČ	kA
</s>
<s>
06840	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
New	New	k?
Canaan	Canaan	k1gInSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
v	v	k7c6
okrese	okres	k1gInSc6
Fairfield	Fairfield	k1gInSc1
County	Counta	k1gFnSc2
v	v	k7c6
Connecticutu	Connecticut	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc6d1
13	#num#	k4
kilometrů	kilometr	k1gInPc2
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Stamfordu	Stamford	k1gInSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Five	Fiv	k1gMnSc2
Mile	mile	k6eAd1
River	Rivra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
19	#num#	k4
395	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejbohatší	bohatý	k2eAgFnPc4d3
komunity	komunita	k1gFnPc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
Money	Monea	k1gFnPc4
zařadila	zařadit	k5eAaPmAgFnS
New	New	k1gFnSc1
Canaan	Canaany	k1gInPc2
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
průměrném	průměrný	k2eAgInSc6d1
příjmu	příjem	k1gInSc6
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významní	významný	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
Florence	Florenc	k1gFnPc1
Harding	Harding	k1gInSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
</s>
<s>
Katherine	Katherin	k1gInSc5
Heigl	Heigl	k1gInSc1
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
–	–	k?
ve	v	k7c6
městě	město	k1gNnSc6
vyrůstala	vyrůstat	k5eAaImAgFnS
</s>
<s>
David	David	k1gMnSc1
Letterman	Letterman	k1gMnSc1
<g/>
,	,	kIx,
moderátor	moderátor	k1gMnSc1
Noční	noční	k2eAgFnSc2d1
show	show	k1gFnSc2
Davida	David	k1gMnSc2
Lettermana	Letterman	k1gMnSc2
–	–	k?
bývalý	bývalý	k2eAgMnSc1d1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
textař	textař	k1gMnSc1
–	–	k?
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
Max	Max	k1gMnSc1
Pacioretty	Pacioretta	k1gFnSc2
<g/>
,	,	kIx,
hokejový	hokejový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
týmu	tým	k1gInSc2
Montreal	Montreal	k1gInSc1
Canadiens	Canadiens	k1gInSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
ZOH	ZOH	kA
2014	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
New	New	k1gMnSc1
Canaan	Canaan	k1gMnSc1
<g/>
,	,	kIx,
Connecticut	Connecticut	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
Population	Population	k1gInSc1
Estimates	Estimates	k1gInSc1
<g/>
.	.	kIx.
www.census.gov	www.census.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CNN	CNN	kA
Money	Monea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
25	#num#	k4
Top-Earning	Top-Earning	k1gInSc1
Towns	Towns	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
2008-6-16	2008-6-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
New	New	k1gFnSc2
Canaan	Canaany	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4433161-7	4433161-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80017630	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
128756471	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80017630	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
