<s>
Statek	statek	k1gInSc1	statek
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
teorii	teorie	k1gFnSc6	teorie
označuje	označovat	k5eAaImIp3nS	označovat
cokoliv	cokoliv	k3yInSc1	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
statek	statek	k1gInSc4	statek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
užitek	užitek	k1gInSc1	užitek
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Statkem	statek	k1gInSc7	statek
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
fyzický	fyzický	k2eAgInSc4d1	fyzický
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
statky	statek	k1gInPc7	statek
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
také	také	k9	také
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Statkem	statek	k1gInSc7	statek
nelze	lze	k6eNd1	lze
chápat	chápat	k5eAaImF	chápat
pouze	pouze	k6eAd1	pouze
určitý	určitý	k2eAgInSc4d1	určitý
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
objektů	objekt	k1gInPc2	objekt
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenou	spojený	k2eAgFnSc4d1	spojená
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
řekách	řeka	k1gFnPc6	řeka
je	být	k5eAaImIp3nS	být
statek	statek	k1gInSc1	statek
volný	volný	k2eAgInSc1d1	volný
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
kohoutku	kohoutek	k1gInSc2	kohoutek
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
volný	volný	k2eAgInSc1d1	volný
statek	statek	k1gInSc1	statek
není	být	k5eNaImIp3nS	být
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
i	i	k9	i
služby	služba	k1gFnSc2	služba
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
pitná	pitný	k2eAgFnSc1d1	pitná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
otočíme	otočit	k5eAaPmIp1nP	otočit
kohoutkem	kohoutek	k1gInSc7	kohoutek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
chceme	chtít	k5eAaImIp1nP	chtít
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
statek	statek	k1gInSc1	statek
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
statek	statek	k1gInSc1	statek
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
statek	statek	k1gInSc1	statek
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
něj	on	k3xPp3gMnSc2	on
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
ochotni	ochoten	k2eAgMnPc1d1	ochoten
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
teorie	teorie	k1gFnSc1	teorie
člení	členit	k5eAaImIp3nS	členit
statky	statek	k1gInPc4	statek
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
vzácnosti	vzácnost	k1gFnSc2	vzácnost
<g/>
:	:	kIx,	:
vzácný	vzácný	k2eAgInSc4d1	vzácný
-	-	kIx~	-
statku	statek	k1gInSc2	statek
je	být	k5eAaImIp3nS	být
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
jsou	být	k5eAaImIp3nP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
platit	platit	k5eAaImF	platit
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
statků	statek	k1gInPc2	statek
volný	volný	k2eAgMnSc1d1	volný
-	-	kIx~	-
statek	statek	k1gInSc1	statek
volně	volně	k6eAd1	volně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
Podle	podle	k7c2	podle
vlivu	vliv	k1gInSc2	vliv
spotřeby	spotřeba	k1gFnSc2	spotřeba
statku	statek	k1gInSc2	statek
na	na	k7c4	na
užitek	užitek	k1gInSc4	užitek
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
<g/>
:	:	kIx,	:
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
-	-	kIx~	-
spotřeba	spotřeba	k1gFnSc1	spotřeba
statku	statek	k1gInSc2	statek
<g />
.	.	kIx.	.
</s>
<s>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
užitek	užitek	k1gInSc4	užitek
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
-	-	kIx~	-
spotřeba	spotřeba	k1gFnSc1	spotřeba
statku	statek	k1gInSc2	statek
snižuje	snižovat	k5eAaImIp3nS	snižovat
užitek	užitek	k1gInSc1	užitek
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
-	-	kIx~	-
spotřeba	spotřeba	k1gFnSc1	spotřeba
statku	statek	k1gInSc2	statek
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
užitek	užitek	k1gInSc4	užitek
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
Podle	podle	k7c2	podle
vlivu	vliv	k1gInSc2	vliv
změny	změna	k1gFnSc2	změna
důchodu	důchod	k1gInSc2	důchod
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
:	:	kIx,	:
normální	normální	k2eAgMnSc1d1	normální
-	-	kIx~	-
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
důchodu	důchod	k1gInSc2	důchod
je	být	k5eAaImIp3nS	být
poptáváno	poptáván	k2eAgNnSc1d1	poptáváno
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
statku	statek	k1gInSc2	statek
luxusní	luxusní	k2eAgNnSc1d1	luxusní
-	-	kIx~	-
růst	růst	k1gInSc1	růst
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
statku	statek	k1gInSc6	statek
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
růst	růst	k1gInSc1	růst
důchodu	důchod	k1gInSc2	důchod
<g />
.	.	kIx.	.
</s>
<s>
nezbytný	zbytný	k2eNgInSc1d1	zbytný
-	-	kIx~	-
růst	růst	k1gInSc1	růst
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
statku	statek	k1gInSc6	statek
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
růst	růst	k1gInSc1	růst
důchodu	důchod	k1gInSc2	důchod
méněcenný	méněcenný	k2eAgMnSc1d1	méněcenný
-	-	kIx~	-
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
důchodu	důchod	k1gInSc2	důchod
klesá	klesat	k5eAaImIp3nS	klesat
poptáváné	poptáváný	k2eAgNnSc1d1	poptáváný
množství	množství	k1gNnSc1	množství
statku	statek	k1gInSc2	statek
Podle	podle	k7c2	podle
možnosti	možnost	k1gFnSc2	možnost
vyloučit	vyloučit	k5eAaPmF	vyloučit
ze	z	k7c2	z
spotřeby	spotřeba	k1gFnSc2	spotřeba
a	a	k8xC	a
rivality	rivalita	k1gFnSc2	rivalita
statku	statek	k1gInSc2	statek
<g/>
:	:	kIx,	:
veřejný	veřejný	k2eAgInSc1d1	veřejný
-	-	kIx~	-
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
dalších	další	k2eAgFnPc2d1	další
jednotek	jednotka	k1gFnPc2	jednotka
nezmenšuje	zmenšovat	k5eNaImIp3nS	zmenšovat
a	a	k8xC	a
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
spotřeby	spotřeba	k1gFnSc2	spotřeba
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vyloučit	vyloučit	k5eAaPmF	vyloučit
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
např.	např.	kA	např.
národní	národní	k2eAgFnSc1d1	národní
obrana	obrana	k1gFnSc1	obrana
<g/>
)	)	kIx)	)
soukromý	soukromý	k2eAgInSc1d1	soukromý
-	-	kIx~	-
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k9	ani
jednu	jeden	k4xCgFnSc4	jeden
vlastnost	vlastnost	k1gFnSc4	vlastnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
statku	statek	k1gInSc2	statek
smíšený	smíšený	k2eAgInSc1d1	smíšený
-	-	kIx~	-
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
vlastnost	vlastnost	k1gFnSc4	vlastnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
statku	statek	k1gInSc2	statek
Podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
tržní	tržní	k2eAgFnSc2d1	tržní
ceny	cena	k1gFnSc2	cena
jako	jako	k8xC	jako
alokačního	alokační	k2eAgInSc2d1	alokační
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
:	:	kIx,	:
tržní	tržní	k2eAgFnSc1d1	tržní
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
interakce	interakce	k1gFnSc2	interakce
mezi	mezi	k7c7	mezi
nabídkou	nabídka	k1gFnSc7	nabídka
a	a	k8xC	a
poptávkou	poptávka	k1gFnSc7	poptávka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
intervencí	intervence	k1gFnPc2	intervence
státu	stát	k1gInSc2	stát
polotržní	polotržní	k2eAgMnSc1d1	polotržní
-	-	kIx~	-
procházejí	procházet	k5eAaImIp3nP	procházet
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
různými	různý	k2eAgFnPc7d1	různá
státními	státní	k2eAgFnPc7d1	státní
intervencemi	intervence	k1gFnPc7	intervence
netržní	tržní	k2eNgFnSc2d1	netržní
-	-	kIx~	-
neprocházejí	procházet	k5eNaImIp3nP	procházet
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
uměle	uměle	k6eAd1	uměle
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
stát	stát	k5eAaPmF	stát
</s>
