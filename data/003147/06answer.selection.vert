<s>
Chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
jednoho	jeden	k4xCgMnSc2	jeden
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
