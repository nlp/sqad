<p>
<s>
Myrta	myrta	k1gFnSc1	myrta
(	(	kIx(	(
<g/>
Myrtus	Myrtus	k1gInSc1	Myrtus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
myrtovité	myrtovitý	k2eAgFnSc2d1	myrtovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
saharské	saharský	k2eAgFnSc6d1	saharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
zplaněl	zplanět	k5eAaPmAgInS	zplanět
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
klimaticky	klimaticky	k6eAd1	klimaticky
příhodných	příhodný	k2eAgFnPc6d1	příhodná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Myrty	myrta	k1gFnPc1	myrta
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
keře	keř	k1gInPc4	keř
s	s	k7c7	s
nevelkými	velký	k2eNgInPc7d1	nevelký
sytě	sytě	k6eAd1	sytě
zelenými	zelený	k2eAgInPc7d1	zelený
listy	list	k1gInPc7	list
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
květy	květ	k1gInPc7	květ
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
nápadně	nápadně	k6eAd1	nápadně
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Myrta	myrta	k1gFnSc1	myrta
obecná	obecná	k1gFnSc1	obecná
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tradiční	tradiční	k2eAgNnSc1d1	tradiční
široké	široký	k2eAgNnSc1d1	široké
využití	využití	k1gNnSc1	využití
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
vonných	vonný	k2eAgFnPc2d1	vonná
silic	silice	k1gFnPc2	silice
<g/>
,	,	kIx,	,
léčivá	léčivý	k2eAgFnSc1d1	léčivá
a	a	k8xC	a
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jako	jako	k9	jako
kontejnerova	kontejnerův	k2eAgFnSc1d1	kontejnerův
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
využíván	využíván	k2eAgInSc4d1	využíván
i	i	k8xC	i
saharský	saharský	k2eAgInSc4d1	saharský
druh	druh	k1gInSc4	druh
Myrtus	Myrtus	k1gInSc4	Myrtus
nivellei	nivelle	k1gFnSc2	nivelle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Myrty	myrta	k1gFnPc1	myrta
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
keře	keř	k1gInPc4	keř
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
výšky	výška	k1gFnSc2	výška
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
3	[number]	k4	3
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
vstřícné	vstřícný	k2eAgFnPc1d1	vstřícná
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
řapíkaté	řapíkatý	k2eAgNnSc1d1	řapíkaté
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozemnutí	rozemnutí	k1gNnSc6	rozemnutí
aromaticky	aromaticky	k6eAd1	aromaticky
voní	vonět	k5eAaImIp3nP	vonět
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
<g/>
,	,	kIx,	,
pětičetné	pětičetný	k2eAgInPc1d1	pětičetný
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
v	v	k7c6	v
paždí	paždí	k1gNnSc6	paždí
listů	list	k1gInPc2	list
a	a	k8xC	a
sladce	sladko	k6eAd1	sladko
voní	vonět	k5eAaImIp3nP	vonět
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
5	[number]	k4	5
korunních	korunní	k2eAgInPc2d1	korunní
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
nitky	nitka	k1gFnPc4	nitka
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
bobule	bobule	k1gFnSc1	bobule
obsahující	obsahující	k2eAgFnSc1d1	obsahující
několik	několik	k4yIc4	několik
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
myrta	myrta	k1gFnSc1	myrta
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Myrta	myrta	k1gFnSc1	myrta
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Myrtus	Myrtus	k1gInSc1	Myrtus
communis	communis	k1gInSc1	communis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc3d1	severní
Africe	Afrika	k1gFnSc3	Afrika
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc3d1	severovýchodní
tropické	tropický	k2eAgFnSc3d1	tropická
Africe	Afrika	k1gFnSc3	Afrika
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Jemen	Jemen	k1gInSc4	Jemen
až	až	k6eAd1	až
do	do	k7c2	do
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
keřových	keřový	k2eAgInPc2d1	keřový
porostů	porost	k1gInPc2	porost
známých	známá	k1gFnPc2	známá
jako	jako	k8xS	jako
makchie	makchie	k1gFnSc2	makchie
a	a	k8xC	a
garigue	garigu	k1gFnSc2	garigu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oázách	oáza	k1gFnPc6	oáza
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
M.	M.	kA	M.
nivellei	nivellei	k6eAd1	nivellei
je	být	k5eAaImIp3nS	být
endemit	endemit	k1gInSc1	endemit
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
Sahary	Sahara	k1gFnSc2	Sahara
na	na	k7c6	na
území	území	k1gNnSc6	území
Alžíru	Alžír	k1gInSc2	Alžír
a	a	k8xC	a
Čadu	Čad	k1gInSc2	Čad
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
zde	zde	k6eAd1	zde
glaciální	glaciální	k2eAgInSc1d1	glaciální
relikt	relikt	k1gInSc1	relikt
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
Ahaggar	Ahaggara	k1gFnPc2	Ahaggara
a	a	k8xC	a
Tassili	Tassili	k1gFnPc2	Tassili
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Myrta	myrta	k1gFnSc1	myrta
obecná	obecná	k1gFnSc1	obecná
roste	růst	k5eAaImIp3nS	růst
zplaněle	zplaněle	k6eAd1	zplaněle
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Myrtus	Myrtus	k1gInSc1	Myrtus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
řazen	řazen	k2eAgInSc4d1	řazen
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
Myrtoideae	Myrtoidea	k1gFnSc2	Myrtoidea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nejrozsáhlejšího	rozsáhlý	k2eAgInSc2d3	nejrozsáhlejší
tribu	trib	k1gInSc2	trib
čeledi	čeleď	k1gFnSc2	čeleď
myrtovitých	myrtovitý	k2eAgInPc2d1	myrtovitý
<g/>
,	,	kIx,	,
Myrteae	Myrteae	k1gFnSc4	Myrteae
<g/>
,	,	kIx,	,
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
jeho	jeho	k3xOp3gFnSc4	jeho
bazální	bazální	k2eAgFnSc4d1	bazální
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Blízce	blízce	k6eAd1	blízce
příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
skupinou	skupina	k1gFnSc7	skupina
je	být	k5eAaImIp3nS	být
monofyletická	monofyletický	k2eAgFnSc1d1	monofyletická
vývojová	vývojový	k2eAgFnSc1d1	vývojová
větev	větev	k1gFnSc1	větev
vesměs	vesměs	k6eAd1	vesměs
asijských	asijský	k2eAgInPc2d1	asijský
a	a	k8xC	a
australských	australský	k2eAgInPc2d1	australský
rodů	rod	k1gInPc2	rod
<g/>
:	:	kIx,	:
Austromyrtus	Austromyrtus	k1gInSc1	Austromyrtus
<g/>
,	,	kIx,	,
Gossia	Gossia	k1gFnSc1	Gossia
<g/>
,	,	kIx,	,
Rhodomyrtus	Rhodomyrtus	k1gInSc1	Rhodomyrtus
<g/>
,	,	kIx,	,
Octamyrtus	Octamyrtus	k1gInSc1	Octamyrtus
<g/>
,	,	kIx,	,
Decaspermum	Decaspermum	k1gInSc1	Decaspermum
a	a	k8xC	a
Rhodamnia	Rhodamnium	k1gNnSc2	Rhodamnium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechny	všechen	k3xTgInPc1	všechen
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
rody	rod	k1gInPc1	rod
tohoto	tento	k3xDgInSc2	tento
tribu	trib	k1gInSc2	trib
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
jihoamerické	jihoamerický	k2eAgFnPc1d1	jihoamerická
<g/>
.	.	kIx.	.
<g/>
Rod	rod	k1gInSc1	rod
Myrtus	Myrtus	k1gInSc1	Myrtus
má	mít	k5eAaImIp3nS	mít
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
synonym	synonymum	k1gNnPc2	synonymum
(	(	kIx(	(
<g/>
Catalogue	Catalogue	k1gInSc1	Catalogue
of	of	k?	of
Life	Lif	k1gInSc2	Lif
jich	on	k3xPp3gMnPc2	on
uvádí	uvádět	k5eAaImIp3nS	uvádět
přes	přes	k7c4	přes
600	[number]	k4	600
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
taxonomickou	taxonomický	k2eAgFnSc7d1	Taxonomická
historií	historie	k1gFnSc7	historie
myrtovitých	myrtovitý	k2eAgFnPc2d1	myrtovitý
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
zejména	zejména	k9	zejména
z	z	k7c2	z
tribu	trib	k1gInSc2	trib
Myrteae	Myrteae	k1gNnSc1	Myrteae
různými	různý	k2eAgMnPc7d1	různý
autory	autor	k1gMnPc7	autor
přesouvány	přesouvat	k5eAaImNgFnP	přesouvat
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
myrta	myrta	k1gFnSc1	myrta
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Myrtus	Myrtus	k1gInSc1	Myrtus
communis	communis	k1gFnSc2	communis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Myrta	myrta	k1gFnSc1	myrta
obecná	obecná	k1gFnSc1	obecná
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
vonných	vonný	k2eAgFnPc2d1	vonná
silic	silice	k1gFnPc2	silice
používaných	používaný	k2eAgFnPc2d1	používaná
zejména	zejména	k9	zejména
v	v	k7c6	v
parfumerii	parfumerie	k1gFnSc6	parfumerie
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
pěstována	pěstován	k2eAgFnSc1d1	pěstována
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
okrasný	okrasný	k2eAgInSc1d1	okrasný
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
ceněný	ceněný	k2eAgInSc1d1	ceněný
pro	pro	k7c4	pro
úhledné	úhledný	k2eAgInPc4d1	úhledný
sytě	sytě	k6eAd1	sytě
zelené	zelený	k2eAgInPc4d1	zelený
listy	list	k1gInPc4	list
a	a	k8xC	a
pohledné	pohledný	k2eAgInPc4d1	pohledný
bílé	bílý	k2eAgInPc4d1	bílý
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jako	jako	k9	jako
balkonová	balkonový	k2eAgFnSc1d1	balkonová
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pokojová	pokojový	k2eAgFnSc1d1	pokojová
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Snítky	snítka	k1gFnPc1	snítka
myrty	myrta	k1gFnSc2	myrta
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
svatbách	svatba	k1gFnPc6	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
myrta	myrta	k1gFnSc1	myrta
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k9	jako
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
<g/>
,	,	kIx,	,
při	při	k7c6	při
respiračních	respirační	k2eAgNnPc6d1	respirační
onemocněních	onemocnění	k1gNnPc6	onemocnění
<g/>
,	,	kIx,	,
zánětech	zánět	k1gInPc6	zánět
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
proti	proti	k7c3	proti
parazitům	parazit	k1gMnPc3	parazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
jižním	jižní	k2eAgNnSc6d1	jižní
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kůra	kůra	k1gFnSc1	kůra
a	a	k8xC	a
kořeny	kořen	k1gInPc1	kořen
myrty	myrta	k1gFnSc2	myrta
k	k	k7c3	k
vydělávání	vydělávání	k1gNnSc3	vydělávání
kůží	kůže	k1gFnPc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pěknou	pěkný	k2eAgFnSc4d1	pěkná
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
<g/>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
využití	využití	k1gNnSc1	využití
myrty	myrta	k1gFnSc2	myrta
obecné	obecná	k1gFnSc2	obecná
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
aromatická	aromatický	k2eAgFnSc1d1	aromatická
rostlina	rostlina	k1gFnSc1	rostlina
mnohostranné	mnohostranný	k2eAgNnSc1d1	mnohostranné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
používají	používat	k5eAaImIp3nP	používat
Tuaregové	Tuareg	k1gMnPc1	Tuareg
listy	lista	k1gFnSc2	lista
místního	místní	k2eAgInSc2d1	místní
druhu	druh	k1gInSc2	druh
Myrtus	Myrtus	k1gInSc1	Myrtus
nivellei	nivelle	k1gFnSc2	nivelle
při	při	k7c6	při
průjmech	průjem	k1gInPc6	průjem
<g/>
,	,	kIx,	,
kapavce	kapavka	k1gFnSc6	kapavka
a	a	k8xC	a
zevně	zevně	k6eAd1	zevně
při	při	k7c6	při
dermatózách	dermatóza	k1gFnPc6	dermatóza
<g/>
.	.	kIx.	.
</s>
<s>
Účinné	účinný	k2eAgFnPc1d1	účinná
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
odlišné	odlišný	k2eAgNnSc1d1	odlišné
než	než	k8xS	než
u	u	k7c2	u
myrty	myrta	k1gFnSc2	myrta
obecné	obecný	k2eAgFnSc2d1	obecná
<g/>
.	.	kIx.	.
</s>
<s>
Medicínské	medicínský	k2eAgFnPc1d1	medicínská
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
antimykotický	antimykotický	k2eAgInSc4d1	antimykotický
a	a	k8xC	a
protizánětlivý	protizánětlivý	k2eAgInSc4d1	protizánětlivý
účinek	účinek	k1gInSc4	účinek
vonného	vonný	k2eAgInSc2d1	vonný
oleje	olej	k1gInSc2	olej
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
myrta	myrta	k1gFnSc1	myrta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Myrtus	Myrtus	k1gInSc1	Myrtus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
myrta	myrta	k1gFnSc1	myrta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
