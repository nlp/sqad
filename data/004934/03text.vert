<s>
Ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
р	р	k?	р
я	я	k?	я
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
jɪ	jɪ	k?	jɪ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejužívanější	užívaný	k2eAgInSc1d3	nejužívanější
slovanský	slovanský	k2eAgInSc1d1	slovanský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
i	i	k8xC	i
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
jazyk	jazyk	k1gInSc4	jazyk
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
ruština	ruština	k1gFnSc1	ruština
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
obrovské	obrovský	k2eAgFnSc6d1	obrovská
rozloze	rozloha	k1gFnSc6	rozloha
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
rusky	rusky	k6eAd1	rusky
mluvící	mluvící	k2eAgFnPc1d1	mluvící
menšiny	menšina	k1gFnPc1	menšina
žijí	žít	k5eAaImIp3nP	žít
doposud	doposud	k6eAd1	doposud
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jinak	jinak	k6eAd1	jinak
většinově	většinově	k6eAd1	většinově
jinojazyčných	jinojazyčný	k2eAgFnPc6d1	jinojazyčná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
získaly	získat	k5eAaPmAgFnP	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
ruština	ruština	k1gFnSc1	ruština
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
dorozumívacím	dorozumívací	k2eAgInSc7d1	dorozumívací
jazykem	jazyk	k1gInSc7	jazyk
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
RVHP	RVHP	kA	RVHP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
ruština	ruština	k1gFnSc1	ruština
povinně	povinně	k6eAd1	povinně
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
USA	USA	kA	USA
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
volí	volit	k5eAaImIp3nP	volit
zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c4	o
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
nejčastěji	často	k6eAd3	často
ruštinu	ruština	k1gFnSc4	ruština
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
řečí	řeč	k1gFnPc2	řeč
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
sebevzdělávání	sebevzdělávání	k1gNnSc4	sebevzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
Kyrgyzstánu	Kyrgyzstán	k1gInSc6	Kyrgyzstán
dále	daleko	k6eAd2	daleko
též	též	k9	též
v	v	k7c6	v
moldavském	moldavský	k2eAgInSc6d1	moldavský
Podněstří	Podněstří	k1gFnSc4	Podněstří
a	a	k8xC	a
Gagauzsku	Gagauzska	k1gFnSc4	Gagauzska
v	v	k7c6	v
Abcházii	Abcházie	k1gFnSc6	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Osetii	Osetie	k1gFnSc3	Osetie
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
ruské	ruský	k2eAgFnPc1d1	ruská
menšiny	menšina	k1gFnPc1	menšina
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
<g/>
,	,	kIx,	,
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
,	,	kIx,	,
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
a	a	k8xC	a
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
politickému	politický	k2eAgInSc3d1	politický
významu	význam	k1gInSc3	význam
se	se	k3xPyFc4	se
ruština	ruština	k1gFnSc1	ruština
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
nástupnický	nástupnický	k2eAgInSc1d1	nástupnický
stát	stát	k1gInSc1	stát
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Staroruština	Staroruština	k1gFnSc1	Staroruština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
ruském	ruský	k2eAgNnSc6d1	ruské
území	území	k1gNnSc6	území
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
šířila	šířit	k5eAaImAgFnS	šířit
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
a	a	k8xC	a
stávala	stávat	k5eAaImAgFnS	stávat
se	se	k3xPyFc4	se
úředním	úřední	k2eAgInSc7d1	úřední
a	a	k8xC	a
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
jako	jako	k9	jako
lidový	lidový	k2eAgInSc4d1	lidový
jazyk	jazyk	k1gInSc4	jazyk
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
v	v	k7c6	v
kronikách	kronika	k1gFnPc6	kronika
<g/>
,	,	kIx,	,
právních	právní	k2eAgInPc6d1	právní
a	a	k8xC	a
správních	správní	k2eAgInPc6d1	správní
dokumentech	dokument	k1gInPc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
slábl	slábnout	k5eAaImAgInS	slábnout
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stoupal	stoupat	k5eAaImAgInS	stoupat
význam	význam	k1gInSc1	význam
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
spisovné	spisovný	k2eAgFnSc2d1	spisovná
ruštiny	ruština	k1gFnSc2	ruština
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
úpravy	úprava	k1gFnSc2	úprava
cyrilice	cyrilice	k1gFnSc2	cyrilice
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
graždanku	graždanka	k1gFnSc4	graždanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
rozkvětu	rozkvět	k1gInSc3	rozkvět
klasické	klasický	k2eAgFnSc2d1	klasická
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pravopisné	pravopisný	k2eAgFnSc3d1	pravopisná
reformě	reforma	k1gFnSc3	reforma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mj.	mj.	kA	mj.
opět	opět	k6eAd1	opět
zjednodušila	zjednodušit	k5eAaPmAgFnS	zjednodušit
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
abeceda	abeceda	k1gFnSc1	abeceda
se	se	k3xPyFc4	se
též	též	k9	též
nazývá	nazývat	k5eAaImIp3nS	nazývat
azbuka	azbuka	k1gFnSc1	azbuka
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
Rusové	Rus	k1gMnPc1	Rus
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
měkkého	měkký	k2eAgNnSc2d1	měkké
a	a	k8xC	a
tvrdého	tvrdé	k1gNnSc2	tvrdé
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
Y	Y	kA	Y
–	–	k?	–
zatímco	zatímco	k8xS	zatímco
И	И	k?	И
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
české	český	k2eAgFnSc2d1	Česká
I	i	k8xC	i
(	(	kIx(	(
<g/>
o	o	k7c4	o
něco	něco	k3yInSc4	něco
měkčeji	měkko	k6eAd2	měkko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ы	Ы	k?	Ы
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
<g />
.	.	kIx.	.
</s>
<s>
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
(	(	kIx(	(
<g/>
artikuluje	artikulovat	k5eAaImIp3nS	artikulovat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
vzadu	vzadu	k6eAd1	vzadu
<g/>
)	)	kIx)	)
Měkký	měkký	k2eAgInSc1d1	měkký
znak	znak	k1gInSc1	znak
a	a	k8xC	a
Е	Е	k?	Е
<g/>
,	,	kIx,	,
Ё	Ё	k?	Ё
<g/>
,	,	kIx,	,
И	И	k?	И
<g/>
,	,	kIx,	,
Ю	Ю	k?	Ю
<g/>
,	,	kIx,	,
Я	Я	k?	Я
změkčují	změkčovat	k5eAaImIp3nP	změkčovat
předcházející	předcházející	k2eAgMnPc1d1	předcházející
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
д	д	k?	д
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
ďeťi	ďeťi	k6eAd1	ďeťi
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
м	м	k?	м
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
mať	matit	k5eAaImRp2nS	matit
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
počáteční	počáteční	k2eAgFnSc1d1	počáteční
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
trochu	trochu	k6eAd1	trochu
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vyčerpá	vyčerpat	k5eAaPmIp3nS	vyčerpat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
na	na	k7c6	na
změkčení	změkčení	k1gNnSc6	změkčení
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
znak	znak	k1gInSc1	znak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
výjimečně	výjimečně	k6eAd1	výjimečně
pro	pro	k7c4	pro
vyznačení	vyznačení	k1gNnSc4	vyznačení
švu	šev	k1gInSc2	šev
mezi	mezi	k7c7	mezi
předponou	předpona	k1gFnSc7	předpona
a	a	k8xC	a
kmenem	kmen	k1gInSc7	kmen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naopak	naopak	k6eAd1	naopak
ke	k	k7c3	k
změkčování	změkčování	k1gNnSc3	změkčování
docházet	docházet	k5eAaImF	docházet
nemá	mít	k5eNaImIp3nS	mít
<g/>
:	:	kIx,	:
в	в	k?	в
čti	číst	k5eAaImRp2nS	číst
[	[	kIx(	[
<g/>
vjezd	vjezd	k1gInSc1	vjezd
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Silový	silový	k2eAgInSc1d1	silový
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
slabikách	slabika	k1gFnPc6	slabika
<g/>
,	,	kIx,	,
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
však	však	k9	však
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jazykových	jazykový	k2eAgFnPc2d1	jazyková
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
slovníků	slovník	k1gInPc2	slovník
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
čárkou	čárka	k1gFnSc7	čárka
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
má	mít	k5eAaImIp3nS	mít
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
м	м	k?	м
[	[	kIx(	[
<g/>
máma	máma	k1gFnSc1	máma
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
х	х	k?	х
[	[	kIx(	[
<g/>
chačú	chačú	k?	chačú
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
б	б	k?	б
[	[	kIx(	[
<g/>
barabán	barabán	k2eAgInSc1d1	barabán
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgMnSc1d2	silnější
a	a	k8xC	a
dynamičtější	dynamický	k2eAgMnSc1d2	dynamičtější
než	než	k8xS	než
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
významotvornou	významotvorný	k2eAgFnSc4d1	významotvorná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
з	з	k?	з
[	[	kIx(	[
<g/>
zámъ	zámъ	k?	zámъ
<g/>
]	]	kIx)	]
–	–	k?	–
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
з	з	k?	з
[	[	kIx(	[
<g/>
zamók	zamók	k1gMnSc1	zamók
<g/>
]	]	kIx)	]
–	–	k?	–
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
с	с	k?	с
[	[	kIx(	[
<g/>
stóit	stóit	k1gMnSc1	stóit
<g/>
]	]	kIx)	]
–	–	k?	–
stojí	stát	k5eAaImIp3nS	stát
(	(	kIx(	(
<g/>
o	o	k7c6	o
penězích	peníze	k1gInPc6	peníze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
с	с	k?	с
[	[	kIx(	[
<g/>
staít	staít	k5eAaPmF	staít
<g/>
]	]	kIx)	]
–	–	k?	–
něco	něco	k3yInSc4	něco
stojí	stát	k5eAaImIp3nS	stát
někde	někde	k6eAd1	někde
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
п	п	k?	п
[	[	kIx(	[
<g/>
pláču	plakat	k5eAaImIp1nS	plakat
<g/>
]	]	kIx)	]
–	–	k?	–
pláču	plakat	k5eAaImIp1nS	plakat
<g/>
,	,	kIx,	,
п	п	k?	п
[	[	kIx(	[
<g/>
plačú	plačú	k?	plačú
<g/>
]	]	kIx)	]
–	–	k?	–
platím	platit	k5eAaImIp1nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
dynamický	dynamický	k2eAgInSc1d1	dynamický
přízvuk	přízvuk	k1gInSc1	přízvuk
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
i	i	k9	i
důsledky	důsledek	k1gInPc1	důsledek
kvalitativní	kvalitativní	k2eAgInPc1d1	kvalitativní
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
vokál	vokál	k1gInSc1	vokál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nepřízvučných	přízvučný	k2eNgFnPc6d1	nepřízvučná
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
м	м	k?	м
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
mъ	mъ	k?	mъ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
О	О	k?	О
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
skoro	skoro	k6eAd1	skoro
jako	jako	k9	jako
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
с	с	k?	с
čti	číst	k5eAaImRp2nS	číst
[	[	kIx(	[
<g/>
svabóda	svabóda	k1gMnSc1	svabóda
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
Ё	Ё	k?	Ё
(	(	kIx(	(
<g/>
čteno	čten	k2eAgNnSc4d1	čteno
[	[	kIx(	[
<g/>
JO	jo	k0	jo
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
Е	Е	k?	Е
a	a	k8xC	a
Rusové	Rus	k1gMnPc1	Rus
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
zřídka	zřídka	k6eAd1	zřídka
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
-	-	kIx~	-
pokud	pokud	k8xS	pokud
nevznikne	vzniknout	k5eNaPmIp3nS	vzniknout
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
významu	význam	k1gInSc2	význam
slova	slovo	k1gNnSc2	slovo
napíší	napsat	k5eAaBmIp3nP	napsat
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
Е	Е	k?	Е
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sousední	sousední	k2eAgFnSc2d1	sousední
běloruštiny	běloruština	k1gFnSc2	běloruština
a	a	k8xC	a
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
(	(	kIx(	(
<g/>
a	a	k8xC	a
ovšem	ovšem	k9	ovšem
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
praslovanské	praslovanský	k2eAgFnSc2d1	praslovanská
[	[	kIx(	[
<g/>
g	g	kA	g
<g/>
]	]	kIx)	]
nezměnilo	změnit	k5eNaPmAgNnS	změnit
na	na	k7c4	na
[	[	kIx(	[
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
a	a	k8xC	a
jihoslovanských	jihoslovanský	k2eAgInPc6d1	jihoslovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
Й	Й	k?	Й
-	-	kIx~	-
poměrně	poměrně	k6eAd1	poměrně
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
J	J	kA	J
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
(	(	kIx(	(
<g/>
samotné	samotný	k2eAgNnSc1d1	samotné
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
í	í	k0	í
kratkoje	kratkoj	k1gInSc2	kratkoj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
přeloženo	přeložen	k2eAgNnSc4d1	přeloženo
krátké	krátký	k2eAgNnSc4d1	krátké
i.	i.	k?	i.
Zřejmě	zřejmě	k6eAd1	zřejmě
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
češtinskými	češtinský	k2eAgNnPc7d1	češtinský
nabodeníčky	nabodeníčko	k1gNnPc7	nabodeníčko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgInPc1d3	nejvýraznější
rozdíly	rozdíl	k1gInPc1	rozdíl
oproti	oproti	k7c3	oproti
české	český	k2eAgFnSc3d1	Česká
gramatice	gramatika	k1gFnSc3	gramatika
<g/>
:	:	kIx,	:
Skloňování	skloňování	k1gNnSc1	skloňování
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
má	mít	k5eAaImIp3nS	mít
6	[number]	k4	6
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
češtině	čeština	k1gFnSc3	čeština
chybí	chybit	k5eAaPmIp3nS	chybit
vokativ	vokativ	k1gInSc1	vokativ
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
funkci	funkce	k1gFnSc4	funkce
přebírá	přebírat	k5eAaImIp3nS	přebírat
tvar	tvar	k1gInSc1	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časování	časování	k1gNnSc1	časování
Slovesa	sloveso	k1gNnSc2	sloveso
se	se	k3xPyFc4	se
časují	časovat	k5eAaImIp3nP	časovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
časování	časování	k1gNnSc2	časování
(	(	kIx(	(
<g/>
=	=	kIx~	=
základní	základní	k2eAgNnSc4d1	základní
rozdělení	rozdělení	k1gNnSc4	rozdělení
sloves	sloveso	k1gNnPc2	sloveso
podle	podle	k7c2	podle
časování	časování	k1gNnSc2	časování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
časování	časování	k1gNnSc6	časování
převládají	převládat	k5eAaImIp3nP	převládat
koncovky	koncovka	k1gFnPc1	koncovka
s	s	k7c7	s
hláskou	hláska	k1gFnSc7	hláska
(	(	kIx(	(
<g/>
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
е	е	k?	е
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
д	д	k?	д
[	[	kIx(	[
<g/>
dělajet	dělajet	k1gMnSc1	dělajet
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
и	и	k?	и
[	[	kIx(	[
<g/>
igrajem	igraj	k1gMnSc7	igraj
<g/>
]	]	kIx)	]
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
časování	časování	k1gNnSc6	časování
převládají	převládat	k5eAaImIp3nP	převládat
koncovky	koncovka	k1gFnPc1	koncovka
s	s	k7c7	s
hláskou	hláska	k1gFnSc7	hláska
(	(	kIx(	(
<g/>
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
в	в	k?	в
[	[	kIx(	[
<g/>
varit	varit	k1gMnSc1	varit
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
у	у	k?	у
[	[	kIx(	[
<g/>
uvidim	uvidim	k?	uvidim
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
rozdělení	rozdělení	k1gNnSc2	rozdělení
existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
časování	časování	k1gNnSc2	časování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
způsobem	způsob	k1gInSc7	způsob
časování	časování	k1gNnSc2	časování
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
věty	věta	k1gFnSc2	věta
Ve	v	k7c6	v
spisovném	spisovný	k2eAgInSc6d1	spisovný
projevu	projev	k1gInSc6	projev
se	se	k3xPyFc4	se
nevypouští	vypouštět	k5eNaImIp3nS	vypouštět
podmět	podmět	k1gInSc1	podmět
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
alespoň	alespoň	k9	alespoň
osobním	osobní	k2eAgNnSc7d1	osobní
zájmenem	zájmeno	k1gNnSc7	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
pomocné	pomocný	k2eAgNnSc4d1	pomocné
sloveso	sloveso	k1gNnSc4	sloveso
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
я	я	k?	я
н	н	k?	н
=	=	kIx~	=
ja	ja	k?	ja
napisal	napisat	k5eAaPmAgMnS	napisat
=	=	kIx~	=
napsal	napsat	k5eAaPmAgMnS	napsat
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
chybí	chybět	k5eAaImIp3nS	chybět
spona	spona	k1gFnSc1	spona
(	(	kIx(	(
<g/>
významové	významový	k2eAgNnSc1d1	významové
sloveso	sloveso	k1gNnSc1	sloveso
být	být	k5eAaImF	být
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
о	о	k?	о
у	у	k?	у
=	=	kIx~	=
on	on	k3xPp3gMnSc1	on
učitel	učitel	k1gMnSc1	učitel
=	=	kIx~	=
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
jsou	být	k5eAaImIp3nP	být
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
z	z	k7c2	z
turkických	turkický	k2eAgInPc2d1	turkický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
л	л	k?	л
=	=	kIx~	=
lošaď	lošadit	k5eAaPmRp2nS	lošadit
=	=	kIx~	=
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
také	také	k9	také
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
к	к	k?	к
=	=	kIx~	=
kaban	kaban	k1gMnSc1	kaban
=	=	kIx~	=
kanec	kanec	k1gMnSc1	kanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
zásobě	zásoba	k1gFnSc6	zásoba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
slova	slovo	k1gNnPc1	slovo
přejatá	přejatý	k2eAgNnPc1d1	přejaté
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
má	mít	k5eAaImIp3nS	mít
cca	cca	kA	cca
500	[number]	k4	500
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
