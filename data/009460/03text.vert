<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
nebo	nebo	k8xC	nebo
též	též	k9	též
nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
je	být	k5eAaImIp3nS	být
poledník	poledník	k1gInSc4	poledník
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
Královskou	královský	k2eAgFnSc7d1	královská
observatoří	observatoř	k1gFnSc7	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
jižní	jižní	k2eAgNnSc4d1	jižní
předměstí	předměstí	k1gNnSc4	předměstí
Londýna	Londýn	k1gInSc2	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
též	též	k9	též
Greenwichský	greenwichský	k2eAgInSc1d1	greenwichský
poledník	poledník	k1gInSc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
definici	definice	k1gFnSc3	definice
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
Airyho	Airy	k1gMnSc4	Airy
pasážník	pasážník	k1gInSc4	pasážník
Greenwichské	greenwichský	k2eAgFnSc2d1	greenwichská
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
astronomie	astronomie	k1gFnSc2	astronomie
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gFnSc7	její
důležitou	důležitý	k2eAgFnSc7d1	důležitá
aplikací	aplikace	k1gFnSc7	aplikace
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
pro	pro	k7c4	pro
navigaci	navigace	k1gFnSc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
sluneční	sluneční	k2eAgInSc1d1	sluneční
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
místě	místo	k1gNnSc6	místo
svázán	svázat	k5eAaPmNgInS	svázat
s	s	k7c7	s
místním	místní	k2eAgInSc7d1	místní
poledníkem	poledník	k1gInSc7	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
jednotného	jednotný	k2eAgInSc2d1	jednotný
času	čas	k1gInSc2	čas
na	na	k7c6	na
větším	veliký	k2eAgNnSc6d2	veliký
území	území	k1gNnSc6	území
proto	proto	k8xC	proto
nutně	nutně	k6eAd1	nutně
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
stanovením	stanovení	k1gNnSc7	stanovení
významného	významný	k2eAgInSc2d1	významný
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
úředních	úřední	k2eAgInPc2d1	úřední
časů	čas	k1gInPc2	čas
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
svázaných	svázaný	k2eAgInPc2d1	svázaný
významných	významný	k2eAgInPc2d1	významný
poledníků	poledník	k1gInPc2	poledník
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
navigace	navigace	k1gFnSc2	navigace
si	se	k3xPyFc3	se
alespoň	alespoň	k9	alespoň
každá	každý	k3xTgFnSc1	každý
koloniální	koloniální	k2eAgFnSc1d1	koloniální
mocnost	mocnost	k1gFnSc1	mocnost
zvolila	zvolit	k5eAaPmAgFnS	zvolit
svůj	svůj	k3xOyFgInSc4	svůj
systém	systém	k1gInSc4	systém
souřadnic	souřadnice	k1gFnPc2	souřadnice
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
základním	základní	k2eAgInSc7d1	základní
poledníkem	poledník	k1gInSc7	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
si	se	k3xPyFc3	se
koncem	konec	k1gInSc7	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vynutil	vynutit	k5eAaPmAgInS	vynutit
přijetí	přijetí	k1gNnSc4	přijetí
jednotného	jednotný	k2eAgInSc2d1	jednotný
systému	systém	k1gInSc2	systém
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
a	a	k8xC	a
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
systému	systém	k1gInSc6	systém
používán	používán	k2eAgMnSc1d1	používán
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
na	na	k7c6	na
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
konferenci	konference	k1gFnSc6	konference
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Meridian	Meridian	k1gInSc1	Meridian
Conference	Conference	k1gFnSc1	Conference
<g/>
)	)	kIx)	)
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1884	[number]	k4	1884
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
41	[number]	k4	41
delegátů	delegát	k1gMnPc2	delegát
z	z	k7c2	z
25	[number]	k4	25
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
tyto	tento	k3xDgInPc1	tento
základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
vhodné	vhodný	k2eAgNnSc1d1	vhodné
přijmout	přijmout	k5eAaPmF	přijmout
jeden	jeden	k4xCgInSc4	jeden
světový	světový	k2eAgInSc4d1	světový
poledník	poledník	k1gInSc4	poledník
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
tak	tak	k6eAd1	tak
všechny	všechen	k3xTgFnPc4	všechen
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgFnPc4d1	existující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poledník	poledník	k1gInSc1	poledník
procházející	procházející	k2eAgInSc1d1	procházející
hlavním	hlavní	k2eAgInSc7d1	hlavní
pasážníkem	pasážník	k1gInSc7	pasážník
na	na	k7c6	na
Greenwichské	greenwichský	k2eAgFnSc6d1	greenwichská
hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
počáteční	počáteční	k2eAgInSc1d1	počáteční
poledník	poledník	k1gInSc1	poledník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
délky	délka	k1gFnPc1	délka
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
180	[number]	k4	180
<g/>
°	°	k?	°
na	na	k7c4	na
východ	východ	k1gInSc4	východ
i	i	k9	i
na	na	k7c4	na
západ	západ	k1gInSc4	západ
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
počítány	počítat	k5eAaImNgFnP	počítat
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
země	zem	k1gFnPc1	zem
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
přijmout	přijmout	k5eAaPmF	přijmout
univerzální	univerzální	k2eAgInSc4d1	univerzální
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
univerzální	univerzální	k2eAgInSc4d1	univerzální
den	den	k1gInSc4	den
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
střední	střední	k2eAgInSc1d1	střední
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
24	[number]	k4	24
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nautický	nautický	k2eAgInSc4d1	nautický
a	a	k8xC	a
astronomický	astronomický	k2eAgInSc4d1	astronomický
den	den	k1gInSc4	den
by	by	kYmCp3nP	by
všude	všude	k6eAd1	všude
měly	mít	k5eAaImAgFnP	mít
začínat	začínat	k5eAaImF	začínat
o	o	k7c6	o
střední	střední	k2eAgFnSc6d1	střední
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
podporovány	podporovat	k5eAaImNgInP	podporovat
všechny	všechen	k3xTgInPc1	všechen
technické	technický	k2eAgInPc1d1	technický
studie	studie	k1gFnPc4	studie
regulující	regulující	k2eAgNnSc4d1	regulující
a	a	k8xC	a
rozšiřující	rozšiřující	k2eAgNnSc4d1	rozšiřující
používání	používání	k1gNnSc4	používání
desítkového	desítkový	k2eAgInSc2d1	desítkový
systému	systém	k1gInSc2	systém
na	na	k7c4	na
dělení	dělení	k1gNnSc4	dělení
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
<g/>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
základní	základní	k2eAgMnSc1d1	základní
poledník	poledník	k1gMnSc1	poledník
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
hlasováním	hlasování	k1gNnSc7	hlasování
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Domingo	Domingo	k1gMnSc1	Domingo
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Haiti	Haiti	k1gNnSc1	Haiti
<g/>
)	)	kIx)	)
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
se	se	k3xPyFc4	se
zdržely	zdržet	k5eAaPmAgFnP	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
nepřijala	přijmout	k5eNaPmAgFnS	přijmout
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
Země	zem	k1gFnSc2	zem
oproti	oproti	k7c3	oproti
nultému	nultý	k4xOgInSc3	nultý
poledníku	poledník	k1gInSc3	poledník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
datová	datový	k2eAgFnSc1d1	datová
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jako	jako	k9	jako
základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
používal	používat	k5eAaImAgInS	používat
zejména	zejména	k9	zejména
ostrov	ostrov	k1gInSc1	ostrov
El	Ela	k1gFnPc2	Ela
Hierro	Hierro	k1gNnSc1	Hierro
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
ferrský	ferrský	k2eAgInSc1d1	ferrský
poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
bod	bod	k1gInSc1	bod
20	[number]	k4	20
<g/>
°	°	k?	°
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
20	[number]	k4	20
<g/>
°	°	k?	°
<g/>
23	[number]	k4	23
<g/>
'	'	kIx"	'
<g/>
9	[number]	k4	9
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
18	[number]	k4	18
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
'	'	kIx"	'
<g/>
51	[number]	k4	51
<g/>
"	"	kIx"	"
západně	západně	k6eAd1	západně
od	od	k7c2	od
Greenwiche	Greenwich	k1gInSc2	Greenwich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
užívané	užívaný	k2eAgInPc1d1	užívaný
základní	základní	k2eAgInPc1d1	základní
poledníky	poledník	k1gInPc1	poledník
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
20	[number]	k4	20
<g/>
'	'	kIx"	'
<g/>
14	[number]	k4	14
<g/>
"	"	kIx"	"
východně	východně	k6eAd1	východně
od	od	k7c2	od
Greenwiche	Greenwich	k1gInSc2	Greenwich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
(	(	kIx(	(
<g/>
poledník	poledník	k1gInSc4	poledník
procházející	procházející	k2eAgInSc4d1	procházející
hvězdárnou	hvězdárna	k1gFnSc7	hvězdárna
Pulkovo	Pulkův	k2eAgNnSc1d1	Pulkovo
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
19	[number]	k4	19
<g/>
'	'	kIx"	'
<g/>
42,09	[number]	k4	42,09
<g/>
"	"	kIx"	"
východně	východně	k6eAd1	východně
od	od	k7c2	od
Greenwiche	Greenwich	k1gInSc2	Greenwich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
8,04	[number]	k4	8,04
<g/>
"	"	kIx"	"
východně	východně	k6eAd1	východně
od	od	k7c2	od
Greenwiche	Greenwich	k1gInSc2	Greenwich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
Pise	Pisa	k1gFnSc6	Pisa
či	či	k8xC	či
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
jiných	jiný	k2eAgNnPc2d1	jiné
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
obdobně	obdobně	k6eAd1	obdobně
libovolně	libovolně	k6eAd1	libovolně
definované	definovaný	k2eAgInPc1d1	definovaný
základní	základní	k2eAgInPc1d1	základní
poledníky	poledník	k1gInPc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
základní	základní	k2eAgInSc4d1	základní
poledník	poledník	k1gInSc4	poledník
Měsíce	měsíc	k1gInSc2	měsíc
leží	ležet	k5eAaImIp3nS	ležet
přesně	přesně	k6eAd1	přesně
uprostřed	uprostřed	k7c2	uprostřed
jeho	jeho	k3xOp3gFnSc2	jeho
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nultý	nultý	k4xOgInSc1	nultý
poledník	poledník	k1gInSc1	poledník
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
prochází	procházet	k5eAaImIp3nS	procházet
kráterem	kráter	k1gInSc7	kráter
Airy-	Airy-	k1gFnSc2	Airy-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jemným	jemný	k2eAgInPc3d1	jemný
pohybům	pohyb	k1gInPc3	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
by	by	k9	by
definování	definování	k1gNnSc4	definování
souřadného	souřadný	k2eAgInSc2d1	souřadný
systému	systém	k1gInSc2	systém
polohou	poloha	k1gFnSc7	poloha
pasážníku	pasážník	k1gInSc2	pasážník
bylo	být	k5eAaImAgNnS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
zafixování	zafixování	k1gNnSc3	zafixování
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
souřadnic	souřadnice	k1gFnPc2	souřadnice
používá	používat	k5eAaImIp3nS	používat
jiné	jiný	k2eAgFnPc4d1	jiná
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
souřadnicí	souřadnice	k1gFnSc7	souřadnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
definic	definice	k1gFnPc2	definice
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc1	čas
UTC	UTC	kA	UTC
(	(	kIx(	(
<g/>
Universal	Universal	k1gMnSc5	Universal
Time	Timus	k1gMnSc5	Timus
Coordinate	Coordinat	k1gMnSc5	Coordinat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
korekcí	korekce	k1gFnSc7	korekce
na	na	k7c4	na
nerovnoměrnost	nerovnoměrnost	k1gFnSc4	nerovnoměrnost
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
vztažen	vztažen	k2eAgInSc1d1	vztažen
k	k	k7c3	k
času	čas	k1gInSc3	čas
TAI	TAI	kA	TAI
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
TAI	TAI	kA	TAI
udržuje	udržovat	k5eAaImIp3nS	udržovat
soubor	soubor	k1gInSc1	soubor
atomových	atomový	k2eAgFnPc2d1	atomová
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Prostorové	prostorový	k2eAgFnPc4d1	prostorová
souřadnice	souřadnice	k1gFnPc4	souřadnice
pak	pak	k6eAd1	pak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
času	čas	k1gInSc3	čas
určuje	určovat	k5eAaImIp3nS	určovat
standard	standard	k1gInSc4	standard
International	International	k1gFnSc2	International
Terrestrial	Terrestrial	k1gInSc4	Terrestrial
Reference	reference	k1gFnSc2	reference
System	Systo	k1gNnSc7	Systo
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
SI	si	k1gNnSc6	si
jednotkách	jednotka	k1gFnPc6	jednotka
a	a	k8xC	a
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
těžišti	těžiště	k1gNnSc6	těžiště
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praktickou	praktický	k2eAgFnSc7d1	praktická
realizací	realizace	k1gFnSc7	realizace
standardu	standard	k1gInSc2	standard
je	být	k5eAaImIp3nS	být
International	International	k1gFnSc4	International
Terrestrial	Terrestrial	k1gInSc1	Terrestrial
Reference	reference	k1gFnPc1	reference
Frame	Fram	k1gMnSc5	Fram
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
bodů	bod	k1gInPc2	bod
s	s	k7c7	s
co	co	k9	co
nejpřesněji	přesně	k6eAd3	přesně
určenými	určený	k2eAgFnPc7d1	určená
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
ITRS	ITRS	kA	ITRS
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dráhy	dráha	k1gFnPc1	dráha
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
systém	systém	k1gInSc1	systém
nebeských	nebeský	k2eAgFnPc2d1	nebeská
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
navázaný	navázaný	k2eAgInSc1d1	navázaný
ke	k	k7c3	k
vzdáleným	vzdálený	k2eAgInPc3d1	vzdálený
kvasarům	kvasar	k1gInPc3	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
Význačný	význačný	k2eAgInSc1d1	význačný
poledník	poledník	k1gInSc1	poledník
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
International	International	k1gMnSc1	International
Reference	reference	k1gFnSc2	reference
Meridian	Meridian	k1gMnSc1	Meridian
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Airyho	Airy	k1gMnSc2	Airy
poledníku	poledník	k1gInSc2	poledník
posunutý	posunutý	k2eAgInSc4d1	posunutý
o	o	k7c6	o
102,5	[number]	k4	102,5
metru	metro	k1gNnSc6	metro
(	(	kIx(	(
<g/>
V	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
s	s	k7c7	s
časem	čas	k1gInSc7	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
IRM	IRM	kA	IRM
a	a	k8xC	a
Greenwichského	greenwichský	k2eAgInSc2d1	greenwichský
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
co	co	k9	co
se	s	k7c7	s
"	"	kIx"	"
<g/>
definičnosti	definičnost	k1gFnPc4	definičnost
<g/>
"	"	kIx"	"
týče	týkat	k5eAaImIp3nS	týkat
je	on	k3xPp3gInPc4	on
dosti	dosti	k6eAd1	dosti
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
<g/>
.	.	kIx.	.
</s>
<s>
Nesporné	sporný	k2eNgNnSc1d1	nesporné
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgInSc4d2	veliký
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
praktické	praktický	k2eAgInPc4d1	praktický
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
důležitější	důležitý	k2eAgFnSc4d2	důležitější
IRM	IRM	kA	IRM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Časové	časový	k2eAgNnSc1d1	časové
pásmo	pásmo	k1gNnSc1	pásmo
</s>
</p>
<p>
<s>
Poledník	poledník	k1gMnSc1	poledník
</s>
</p>
<p>
<s>
UTC	UTC	kA	UTC
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
základní	základní	k2eAgInSc4d1	základní
poledník	poledník	k1gInSc4	poledník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sborník	sborník	k1gInSc1	sborník
International	International	k1gFnSc2	International
Meridian	Meridiana	k1gFnPc2	Meridiana
Conference	Conference	k1gFnSc2	Conference
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
terestrial	terestrial	k1gInSc1	terestrial
Reference	reference	k1gFnSc1	reference
System	Syst	k1gInSc7	Syst
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
