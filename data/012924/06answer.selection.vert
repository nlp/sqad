<s>
Při	při	k7c6	při
vývinu	vývin	k1gInSc6	vývin
nepřímém	přímý	k2eNgInSc6d1	nepřímý
probíhá	probíhat	k5eAaImIp3nS	probíhat
ontogeneze	ontogeneze	k1gFnPc1	ontogeneze
přes	přes	k7c4	přes
stadium	stadium	k1gNnSc4	stadium
larvy	larva	k1gFnSc2	larva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
dospělce	dospělec	k1gMnSc2	dospělec
<g/>
.	.	kIx.	.
</s>
