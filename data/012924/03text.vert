<p>
<s>
Ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
ώ	ώ	k?	ώ
<g/>
,	,	kIx,	,
ón	ón	k?	ón
=	=	kIx~	=
jsoucí	jsoucí	k2eAgInSc1d1	jsoucí
a	a	k8xC	a
γ	γ	k?	γ
<g/>
,	,	kIx,	,
genesis	genesis	k1gFnSc1	genesis
=	=	kIx~	=
zrození	zrození	k1gNnSc1	zrození
<g/>
,	,	kIx,	,
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
morfogeneze	morfogeneze	k1gFnSc1	morfogeneze
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
μ	μ	k?	μ
=	=	kIx~	=
původ	původ	k1gInSc1	původ
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
původ	původ	k1gInSc4	původ
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
jedince	jedinec	k1gMnSc4	jedinec
(	(	kIx(	(
<g/>
organizmu	organizmus	k1gInSc2	organizmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
fylogenezi	fylogeneze	k1gFnSc3	fylogeneze
<g/>
,	,	kIx,	,
vývoji	vývoj	k1gInSc3	vývoj
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
oplodněním	oplodnění	k1gNnSc7	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
dospělé	dospělý	k2eAgFnSc3d1	dospělá
formě	forma	k1gFnSc3	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fylogenezí	fylogeneze	k1gFnSc7	fylogeneze
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
rekapituluje	rekapitulovat	k5eAaBmIp3nS	rekapitulovat
fylogenezi	fylogeneze	k1gFnSc4	fylogeneze
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
zvaný	zvaný	k2eAgInSc1d1	zvaný
Haeckelův	Haeckelův	k2eAgInSc1d1	Haeckelův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
jedince	jedinec	k1gMnSc4	jedinec
přesně	přesně	k6eAd1	přesně
zrcadlí	zrcadlit	k5eAaImIp3nP	zrcadlit
vývoj	vývoj	k1gInSc4	vývoj
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
překonaná	překonaný	k2eAgFnSc1d1	překonaná
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
však	však	k9	však
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
ontogenezí	ontogeneze	k1gFnSc7	ontogeneze
a	a	k8xC	a
fylogenezí	fylogeneze	k1gFnSc7	fylogeneze
existují	existovat	k5eAaImIp3nP	existovat
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ontogeneze	ontogeneze	k1gFnSc1	ontogeneze
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
prochází	procházet	k5eAaImIp3nS	procházet
jedinec	jedinec	k1gMnSc1	jedinec
od	od	k7c2	od
splynutí	splynutí	k1gNnSc2	splynutí
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
až	až	k9	až
po	po	k7c4	po
dospělost	dospělost	k1gFnSc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
velmi	velmi	k6eAd1	velmi
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ontogeneze	ontogeneze	k1gFnPc1	ontogeneze
živočichů	živočich	k1gMnPc2	živočich
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
přímý	přímý	k2eAgInSc1d1	přímý
a	a	k8xC	a
nepřímý	přímý	k2eNgInSc1d1	nepřímý
===	===	k?	===
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
vývin	vývin	k1gInSc4	vývin
<g/>
)	)	kIx)	)
přímý	přímý	k2eAgInSc4d1	přímý
a	a	k8xC	a
nepřímý	přímý	k2eNgInSc4d1	nepřímý
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
vývin	vývin	k1gInSc1	vývin
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
mladý	mladý	k2eAgMnSc1d1	mladý
jedinec	jedinec	k1gMnSc1	jedinec
morfologicky	morfologicky	k6eAd1	morfologicky
i	i	k9	i
anatomicky	anatomicky	k6eAd1	anatomicky
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
dospělci	dospělec	k1gMnPc1	dospělec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývinu	vývin	k1gInSc6	vývin
nepřímém	přímý	k2eNgInSc6d1	nepřímý
probíhá	probíhat	k5eAaImIp3nS	probíhat
ontogeneze	ontogeneze	k1gFnPc1	ontogeneze
přes	přes	k7c4	přes
stadium	stadium	k1gNnSc4	stadium
larvy	larva	k1gFnSc2	larva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
dospělce	dospělec	k1gMnSc2	dospělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
tělesná	tělesný	k2eAgFnSc1d1	tělesná
proměna	proměna	k1gFnSc1	proměna
jednoho	jeden	k4xCgInSc2	jeden
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Zvykání	zvykání	k1gNnSc1	zvykání
na	na	k7c6	na
měnící	měnící	k2eAgFnSc6d1	měnící
se	se	k3xPyFc4	se
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
U	u	k7c2	u
nepřímého	přímý	k2eNgInSc2d1	nepřímý
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
proměna	proměna	k1gFnSc1	proměna
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
a	a	k8xC	a
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
larva	larva	k1gFnSc1	larva
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
dospělce	dospělec	k1gMnPc4	dospělec
přes	přes	k7c4	přes
stadium	stadium	k1gNnSc4	stadium
kukly	kukla	k1gFnSc2	kukla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
jedince	jedinko	k6eAd1	jedinko
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
začíná	začínat	k5eAaImIp3nS	začínat
oplodněním	oplodnění	k1gNnSc7	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
splývá	splývat	k5eAaImIp3nS	splývat
zralá	zralý	k2eAgFnSc1d1	zralá
samčí	samčí	k2eAgFnSc1d1	samčí
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
spermie	spermie	k1gFnSc1	spermie
<g/>
)	)	kIx)	)
se	s	k7c7	s
zralou	zralý	k2eAgFnSc7d1	zralá
samičí	samičí	k2eAgFnSc7d1	samičí
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
buňkou	buňka	k1gFnSc7	buňka
(	(	kIx(	(
<g/>
vajíčkem	vajíčko	k1gNnSc7	vajíčko
–	–	k?	–
oocytem	oocyt	k1gInSc7	oocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
buňce	buňka	k1gFnSc6	buňka
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
kromě	kromě	k7c2	kromě
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
chromozomy	chromozom	k1gInPc1	chromozom
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dělení	dělení	k1gNnSc3	dělení
takovýchto	takovýto	k3xDgFnPc2	takovýto
buněk	buňka	k1gFnPc2	buňka
dochází	docházet	k5eAaImIp3nS	docházet
způsobem	způsob	k1gInSc7	způsob
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
mitóza	mitóza	k1gFnSc1	mitóza
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
buňky	buňka	k1gFnPc1	buňka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jeden	jeden	k4xCgInSc4	jeden
chromozóm	chromozóm	k1gInSc4	chromozóm
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
páru	pár	k1gInSc2	pár
(	(	kIx(	(
<g/>
haploidní	haploidní	k2eAgFnSc1d1	haploidní
sada	sada	k1gFnSc1	sada
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
způsobem	způsob	k1gInSc7	způsob
dělení	dělení	k1gNnSc2	dělení
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
meióza	meióza	k1gFnSc1	meióza
<g/>
.	.	kIx.	.
</s>
<s>
Splynutím	splynutí	k1gNnSc7	splynutí
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
jader	jádro	k1gNnPc2	jádro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oplodněném	oplodněný	k2eAgNnSc6d1	oplodněné
vajíčku	vajíčko	k1gNnSc6	vajíčko
obnovená	obnovený	k2eAgFnSc1d1	obnovená
výbava	výbava	k1gFnSc1	výbava
jádra	jádro	k1gNnSc2	jádro
s	s	k7c7	s
dvojicemi	dvojice	k1gFnPc7	dvojice
chromozomů	chromozom	k1gInPc2	chromozom
(	(	kIx(	(
<g/>
diploidní	diploidní	k2eAgFnSc1d1	diploidní
sada	sada	k1gFnSc1	sada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
vajíčko	vajíčko	k1gNnSc1	vajíčko
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
dělí	dělit	k5eAaImIp3nS	dělit
mitózami	mitóza	k1gFnPc7	mitóza
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dceřinými	dceřin	k2eAgFnPc7d1	dceřina
buňkami	buňka	k1gFnPc7	buňka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vajíčka	vajíčko	k1gNnSc2	vajíčko
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
rýhy	rýha	k1gFnPc1	rýha
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
oplodněného	oplodněný	k2eAgNnSc2d1	oplodněné
vajíčka	vajíčko	k1gNnSc2	vajíčko
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
jinak	jinak	k6eAd1	jinak
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
rýhování	rýhování	k1gNnSc4	rýhování
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
rýhujícího	rýhující	k2eAgMnSc2d1	rýhující
se	se	k3xPyFc4	se
vajíčka	vajíčko	k1gNnSc2	vajíčko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
na	na	k7c6	na
příkladě	příklad	k1gInSc6	příklad
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
rýhovacích	rýhovací	k2eAgFnPc6d1	rýhovací
dělení	dělení	k1gNnSc3	dělení
vzniká	vznikat	k5eAaImIp3nS	vznikat
shluk	shluk	k1gInSc1	shluk
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
morula	morula	k1gFnSc1	morula
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
dělením	dělení	k1gNnSc7	dělení
se	se	k3xPyFc4	se
morula	morula	k1gFnSc1	morula
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
jednovrstvý	jednovrstvý	k2eAgInSc4d1	jednovrstvý
dutý	dutý	k2eAgInSc4d1	dutý
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
blastulu	blastula	k1gFnSc4	blastula
<g/>
.	.	kIx.	.
</s>
<s>
Jednovrstvová	jednovrstvový	k2eAgFnSc1d1	jednovrstvový
blastula	blastula	k1gFnSc1	blastula
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
gastrulace	gastrulace	k1gFnSc1	gastrulace
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c4	na
dvojvrstvý	dvojvrstvý	k2eAgInSc4d1	dvojvrstvý
zárodek	zárodek	k1gInSc4	zárodek
<g/>
,	,	kIx,	,
gastrulu	gastrula	k1gFnSc4	gastrula
<g/>
.	.	kIx.	.
</s>
<s>
Vrstvami	vrstva	k1gFnPc7	vrstva
gastruly	gastrula	k1gFnPc4	gastrula
jsou	být	k5eAaImIp3nP	být
vnější	vnější	k2eAgInSc1d1	vnější
zárodeční	zárodeční	k2eAgInSc1d1	zárodeční
list	list	k1gInSc1	list
–	–	k?	–
ektoderm	ektoderm	k1gInSc1	ektoderm
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
zárodeční	zárodeční	k2eAgInSc1d1	zárodeční
list	list	k1gInSc1	list
–	–	k?	–
entoderm	entoderm	k1gInSc1	entoderm
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
gastrulace	gastrulace	k1gFnSc1	gastrulace
probíhá	probíhat	k5eAaImIp3nS	probíhat
vchlípením	vchlípení	k1gNnSc7	vchlípení
části	část	k1gFnSc2	část
buněk	buňka	k1gFnPc2	buňka
blastuly	blastula	k1gFnSc2	blastula
<g/>
,	,	kIx,	,
invaginací	invaginace	k1gFnPc2	invaginace
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
dvouvrstvový	dvouvrstvový	k2eAgInSc4d1	dvouvrstvový
útvar	útvar	k1gInSc4	útvar
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
otvorem	otvor	k1gInSc7	otvor
jako	jako	k8xC	jako
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
vchlípení	vchlípení	k1gNnSc2	vchlípení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
otvor	otvor	k1gInSc1	otvor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
blastoporus	blastoporus	k1gInSc1	blastoporus
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
dutina	dutina	k1gFnSc1	dutina
gastruly	gastrula	k1gFnSc2	gastrula
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prvostřevo	prvostřevo	k1gNnSc1	prvostřevo
–	–	k?	–
archenteron	archenteron	k1gMnSc1	archenteron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
gastrulaci	gastrulace	k1gFnSc6	gastrulace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ektoderm	ektoderm	k1gInSc4	ektoderm
a	a	k8xC	a
entoderm	entoderm	k1gInSc4	entoderm
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
třetí	třetí	k4xOgInSc1	třetí
zárodečný	zárodečný	k2eAgInSc1d1	zárodečný
list	list	k1gInSc1	list
–	–	k?	–
mezoderm	mezoderm	k1gInSc1	mezoderm
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
způsob	způsob	k1gInSc4	způsob
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
u	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vrstvy	vrstva	k1gFnSc2	vrstva
gastruly	gastrula	k1gFnSc2	gastrula
se	se	k3xPyFc4	se
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
blastoporu	blastopor	k1gInSc2	blastopor
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vsouvají	vsouvat	k5eAaImIp3nP	vsouvat
mezi	mezi	k7c4	mezi
ektoderm	ektoderm	k1gInSc4	ektoderm
a	a	k8xC	a
entoderm	entoderm	k1gInSc4	entoderm
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
budoucí	budoucí	k2eAgFnSc2d1	budoucí
zádové	zádový	k2eAgFnSc2d1	zádová
části	část	k1gFnSc2	část
zárodku	zárodek	k1gInSc2	zárodek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
čáře	čára	k1gFnSc6	čára
tohoto	tento	k3xDgMnSc2	tento
vysouvajícího	vysouvající	k2eAgMnSc2d1	vysouvající
se	se	k3xPyFc4	se
útvaru	útvar	k1gInSc2	útvar
vzniká	vznikat	k5eAaImIp3nS	vznikat
materiál	materiál	k1gInSc1	materiál
budoucí	budoucí	k2eAgFnSc2d1	budoucí
struny	struna	k1gFnSc2	struna
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
(	(	kIx(	(
<g/>
chorda	chorda	k1gFnSc1	chorda
dorsalis	dorsalis	k1gFnSc2	dorsalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
po	po	k7c6	po
jejích	její	k3xOp3gFnPc6	její
stranách	strana	k1gFnPc6	strana
tvoří	tvořit	k5eAaImIp3nP	tvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
mezoderm	mezoderm	k1gInSc4	mezoderm
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
útvar	útvar	k1gInSc1	útvar
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
chordomezodermální	chordomezodermální	k2eAgInSc4d1	chordomezodermální
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
hřbetní	hřbetní	k2eAgNnSc1d1	hřbetní
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
trvalým	trvalý	k2eAgInSc7d1	trvalý
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nS	udržet
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezoderm	mezoderm	k1gInSc1	mezoderm
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
mezi	mezi	k7c4	mezi
ektoderm	ektoderm	k1gInSc4	ektoderm
a	a	k8xC	a
entoderm	entoderm	k1gInSc4	entoderm
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
struny	struna	k1gFnSc2	struna
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
úseky	úsek	k1gInPc4	úsek
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
segmenty	segment	k1gInPc4	segment
–	–	k?	–
somity	somita	k1gFnSc2	somita
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
od	od	k7c2	od
chordy	chorda	k1gFnSc2	chorda
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
a	a	k8xC	a
mezoderm	mezoderm	k1gInSc1	mezoderm
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
přikládá	přikládat	k5eAaImIp3nS	přikládat
k	k	k7c3	k
ektodermu	ektoderm	k1gInSc3	ektoderm
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
somatopleura	somatopleura	k1gFnSc1	somatopleura
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
k	k	k7c3	k
entodermu	entoderm	k1gInSc2	entoderm
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
splanchnopleura	splanchnopleura	k1gFnSc1	splanchnopleura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dutina	dutina	k1gFnSc1	dutina
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
coelom	coelom	k1gInSc4	coelom
anebo	anebo	k8xC	anebo
célomová	célomový	k2eAgFnSc1d1	célomový
dutina	dutina	k1gFnSc1	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postnatální	postnatální	k2eAgInSc4d1	postnatální
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Postnatální	postnatální	k2eAgInSc1d1	postnatální
vývoj	vývoj	k1gInSc1	vývoj
člověka	člověk	k1gMnSc2	člověk
</s>
</p>
<p>
<s>
Ontogenetický	ontogenetický	k2eAgInSc1d1	ontogenetický
vývoj	vývoj	k1gInSc1	vývoj
měkkýšů	měkkýš	k1gMnPc2	měkkýš
</s>
</p>
<p>
<s>
Embryonální	embryonální	k2eAgInSc1d1	embryonální
vývoj	vývoj	k1gInSc1	vývoj
kardiovaskulární	kardiovaskulární	k2eAgFnSc2d1	kardiovaskulární
soustavy	soustava	k1gFnSc2	soustava
člověka	člověk	k1gMnSc2	člověk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Hezky	hezky	k6eAd1	hezky
názorné	názorný	k2eAgInPc4d1	názorný
obrázky	obrázek	k1gInPc4	obrázek
v	v	k7c6	v
podkapitole	podkapitola	k1gFnSc6	podkapitola
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Vznik	vznik	k1gInSc1	vznik
tkání	tkáň	k1gFnPc2	tkáň
<g/>
:	:	kIx,	:
Histologie	histologie	k1gFnSc1	histologie
</s>
</p>
