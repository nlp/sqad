<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vývin	vývin	k1gInSc1	vývin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíha	probíha	k1gFnSc1	probíha
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
přes	přes	k7c4	přes
stadium	stadium	k1gNnSc4	stadium
larvy	larva	k1gFnSc2	larva
<g/>
?	?	kIx.	?
</s>
