<p>
<s>
Cchuan-ti-sia	Cchuaniia	k1gFnSc1	Cchuan-ti-sia
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Cchuan-ti-sia	Cchuaniium	k1gNnSc2	Cchuan-ti-sium
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Cuandixia	Cuandixius	k1gMnSc2	Cuandixius
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
爨	爨	k?	爨
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnička	vesnička	k1gFnSc1	vesnička
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Čaj-tchangu	Čajchang	k1gInSc6	Čaj-tchang
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
obvodě	obvod	k1gInSc6	obvod
Men-tchou-kou	Menchouá	k1gFnSc4	Men-tchou-ká
a	a	k8xC	a
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
architekturu	architektura	k1gFnSc4	architektura
z	z	k7c2	z
období	období	k1gNnSc2	období
dynastií	dynastie	k1gFnPc2	dynastie
Ming	Minga	k1gFnPc2	Minga
a	a	k8xC	a
Čching	Čching	k1gInSc1	Čching
je	být	k5eAaImIp3nS	být
vyhledávanou	vyhledávaný	k2eAgFnSc7d1	vyhledávaná
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
středu	střed	k1gInSc2	střed
Pekingu	Peking	k1gInSc2	Peking
je	být	k5eAaImIp3nS	být
Cchuan-ti-sia	Cchuaniia	k1gFnSc1	Cchuan-ti-sia
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
je	být	k5eAaImIp3nS	být
napojena	napojit	k5eAaPmNgFnS	napojit
autobusy	autobus	k1gInPc7	autobus
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Pching-kuo-jüan	Pchinguoüany	k1gInPc2	Pching-kuo-jüany
linky	linka	k1gFnSc2	linka
1	[number]	k4	1
pekingského	pekingský	k2eAgNnSc2d1	pekingské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ves	ves	k1gFnSc1	ves
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Jung-le	Jung	k1gMnSc4	Jung-l
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gMnSc1	Ming
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přicestovala	přicestovat	k5eAaPmAgFnS	přicestovat
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Šan-si	Šane	k1gFnSc4	Šan-se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Cuandixia	Cuandixium	k1gNnSc2	Cuandixium
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Cuandixia	Cuandixia	k1gFnSc1	Cuandixia
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cchuan-ti-sia	Cchuaniium	k1gNnSc2	Cchuan-ti-sium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Cchuan-ti-sia	Cchuaniia	k1gFnSc1	Cchuan-ti-sia
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Men-tchou-kou	Menchouá	k1gFnSc4	Men-tchou-ká
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cchuan-ti-sia	Cchuaniia	k1gFnSc1	Cchuan-ti-sia
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Men-tchou-kou	Menchouá	k1gFnSc4	Men-tchou-ká
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
