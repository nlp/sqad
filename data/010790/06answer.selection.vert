<s>
Cchuan-ti-sia	Cchuaniia	k1gFnSc1	Cchuan-ti-sia
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Cchuan-ti-sia	Cchuaniium	k1gNnSc2	Cchuan-ti-sium
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Cuandixia	Cuandixius	k1gMnSc2	Cuandixius
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
爨	爨	k?	爨
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnička	vesnička	k1gFnSc1	vesnička
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
