<p>
<s>
Mohyla	mohyla	k1gFnSc1	mohyla
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Bradlo	bradlo	k1gNnSc4	bradlo
v	v	k7c6	v
Myjavské	myjavský	k2eAgFnSc6d1	Myjavská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Mohyla	mohyla	k1gFnSc1	mohyla
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Bradlo	bradlo	k1gNnSc1	bradlo
dominující	dominující	k2eAgFnSc2d1	dominující
krajině	krajina	k1gFnSc6	krajina
svojí	svůj	k3xOyFgFnSc7	svůj
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
543	[number]	k4	543
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
silnicí	silnice	k1gFnSc7	silnice
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
Brezová	Brezová	k1gFnSc1	Brezová
pod	pod	k7c4	pod
Bradlom	Bradlom	k1gInSc4	Bradlom
a	a	k8xC	a
Košariskami	Košariska	k1gFnPc7	Košariska
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Priepasné	Priepasný	k2eAgFnSc2d1	Priepasný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Generál	generál	k1gMnSc1	generál
Štefánik	Štefánik	k1gMnSc1	Štefánik
tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
havárii	havárie	k1gFnSc6	havárie
a	a	k8xC	a
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
pohřbili	pohřbít	k5eAaPmAgMnP	pohřbít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
posádkou	posádka	k1gFnSc7	posádka
jeho	on	k3xPp3gNnSc2	on
letadla	letadlo	k1gNnSc2	letadlo
poblíž	poblíž	k7c2	poblíž
jeho	jeho	k3xOp3gNnPc2	jeho
rodných	rodný	k2eAgNnPc2d1	rodné
Košarisek	Košarisko	k1gNnPc2	Košarisko
na	na	k7c6	na
Bradle	bradlo	k1gNnSc6	bradlo
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc1d1	položen
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
památníku	památník	k1gInSc2	památník
navrženého	navržený	k2eAgInSc2d1	navržený
architektem	architekt	k1gMnSc7	architekt
Dušanem	Dušan	k1gMnSc7	Dušan
Jurkovičem	Jurkovič	k1gMnSc7	Jurkovič
<g/>
.	.	kIx.	.
</s>
<s>
Mohyla	mohyla	k1gFnSc1	mohyla
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
září	září	k1gNnSc6	září
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
bíle	bíle	k6eAd1	bíle
se	se	k3xPyFc4	se
skvoucí	skvoucí	k2eAgFnSc1d1	skvoucí
terasovitá	terasovitý	k2eAgFnSc1d1	terasovitá
stavba	stavba	k1gFnSc1	stavba
představuje	představovat	k5eAaImIp3nS	představovat
typickou	typický	k2eAgFnSc4d1	typická
čistotu	čistota	k1gFnSc4	čistota
architektonického	architektonický	k2eAgInSc2d1	architektonický
projevu	projev	k1gInSc2	projev
Dušana	Dušan	k1gMnSc2	Dušan
Jurkoviče	Jurkovič	k1gMnSc2	Jurkovič
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
travertinových	travertinový	k2eAgInPc2d1	travertinový
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
pomníku	pomník	k1gInSc2	pomník
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
výhled	výhled	k1gInSc1	výhled
do	do	k7c2	do
kopanického	kopanický	k2eAgInSc2d1	kopanický
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
mohyla	mohyla	k1gFnSc1	mohyla
byla	být	k5eAaImAgFnS	být
restaurována	restaurovat	k5eAaBmNgFnS	restaurovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
chátrající	chátrající	k2eAgFnSc1d1	chátrající
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
opravena	opravit	k5eAaPmNgFnS	opravit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
její	její	k3xOp3gInSc4	její
význam	význam	k1gInSc4	význam
nepochybně	pochybně	k6eNd1	pochybně
zasluhuje	zasluhovat	k5eAaImIp3nS	zasluhovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čelních	čelní	k2eAgFnPc6d1	čelní
a	a	k8xC	a
bočních	boční	k2eAgNnPc6d1	boční
schodištích	schodiště	k1gNnPc6	schodiště
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjít	vyjít	k5eAaPmF	vyjít
až	až	k9	až
k	k	k7c3	k
samotnému	samotný	k2eAgInSc3d1	samotný
hrobu	hrob	k1gInSc3	hrob
velikána	velikán	k1gMnSc2	velikán
našich	náš	k3xOp1gFnPc2	náš
národních	národní	k2eAgFnPc2d1	národní
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
náhrobku	náhrobek	k1gInSc6	náhrobek
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
jih-východ-sever-západ	jihýchodeverápad	k6eAd1	jih-východ-sever-západ
umístěné	umístěný	k2eAgInPc1d1	umístěný
tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
nápisy	nápis	k1gInPc1	nápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
minister	minister	k1gMnSc1	minister
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milan	Milan	k1gMnSc1	Milan
R.	R.	kA	R.
Štefánik	Štefánik	k1gMnSc1	Štefánik
21	[number]	k4	21
<g/>
.	.	kIx.	.
júla	júla	k6eAd1	júla
1880	[number]	k4	1880
4	[number]	k4	4
<g/>
.	.	kIx.	.
mája	máj	k1gInSc2	máj
1919	[number]	k4	1919
</s>
</p>
<p>
<s>
Zahynul	zahynout	k5eAaPmAgInS	zahynout
pádom	pádom	k1gInSc1	pádom
lietadla	lietadlo	k1gNnSc2	lietadlo
dňa	dňa	k?	dňa
4	[number]	k4	4
<g/>
.	.	kIx.	.
mája	máj	k1gInSc2	máj
1919	[number]	k4	1919
pri	pri	k?	pri
Bratislave	Bratislav	k1gMnSc5	Bratislav
</s>
</p>
<p>
<s>
S	s	k7c7	s
ním	on	k3xPp3gInSc7	on
kráľ	kráľ	k?	kráľ
<g/>
.	.	kIx.	.
taliansky	taliansky	k6eAd1	taliansky
serg	serg	k1gMnSc1	serg
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
Merlino	Merlin	k2eAgNnSc1d1	Merlino
a	a	k8xC	a
sol	sol	k1gNnSc1	sol
<g/>
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
Aggiunti	Aggiunti	k1gNnSc1	Aggiunti
</s>
</p>
<p>
<s>
Veľkému	Veľký	k2eAgMnSc3d1	Veľký
synovi	syn	k1gMnSc3	syn
oslobodený	oslobodený	k2eAgInSc4d1	oslobodený
národ	národ	k1gInSc4	národ
československý	československý	k2eAgInSc4d1	československý
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
Brezová	Brezový	k2eAgFnSc1d1	Brezová
pod	pod	k7c7	pod
Bradlem	bradlo	k1gNnSc7	bradlo
vede	vést	k5eAaImIp3nS	vést
označená	označený	k2eAgFnSc1d1	označená
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
silnice	silnice	k1gFnSc1	silnice
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
památníku	památník	k1gInSc3	památník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
záchytné	záchytný	k2eAgNnSc4d1	záchytné
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
z	z	k7c2	z
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
magistrála	magistrála	k1gFnSc1	magistrála
<g/>
)	)	kIx)	)
se	s	k7c7	s
zelenou	zelený	k2eAgFnSc7d1	zelená
odbočkou	odbočka	k1gFnSc7	odbočka
na	na	k7c4	na
Bradlo	bradlo	k1gNnSc4	bradlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Košarisek	Košariska	k1gFnPc2	Košariska
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
potom	potom	k6eAd1	potom
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mohyla	mohyla	k1gFnSc1	mohyla
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
(	(	kIx(	(
<g/>
Bradlo	bradlo	k1gNnSc1	bradlo
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mohyla	mohyla	k1gFnSc1	mohyla
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
O	o	k7c6	o
mohyle	mohyla	k1gFnSc6	mohyla
</s>
</p>
