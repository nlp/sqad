<p>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
dílem	dílo	k1gNnSc7	dílo
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
dílem	dílo	k1gNnSc7	dílo
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
oddíle	oddíl	k1gInSc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
"	"	kIx"	"
<g/>
dvojkolejnost	dvojkolejnost	k1gFnSc1	dvojkolejnost
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Foglara	Foglar	k1gMnSc2	Foglar
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
zasazeny	zasadit	k5eAaPmNgFnP	zasadit
do	do	k7c2	do
jediného	jediný	k2eAgNnSc2d1	jediné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Propagace	propagace	k1gFnSc1	propagace
skautingu	skauting	k1gInSc2	skauting
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
autorských	autorský	k2eAgInPc2d1	autorský
záměrů	záměr	k1gInPc2	záměr
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
čtivosti	čtivost	k1gFnSc2	čtivost
nebo	nebo	k8xC	nebo
realističnosti	realističnost	k1gFnSc2	realističnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
Foglar	Foglar	k1gMnSc1	Foglar
napsal	napsat	k5eAaBmAgMnS	napsat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
odehrávajících	odehrávající	k2eAgFnPc2d1	odehrávající
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
skauty	skaut	k1gMnPc4	skaut
(	(	kIx(	(
<g/>
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
Poklad	poklad	k1gInSc1	poklad
Černého	Černého	k2eAgMnSc2d1	Černého
delfína	delfín	k1gMnSc2	delfín
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
školní	školní	k2eAgFnSc4d1	školní
třídu	třída	k1gFnSc4	třída
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
už	už	k6eAd1	už
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
Když	když	k8xS	když
Duben	duben	k1gInSc1	duben
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Hrdinou	Hrdina	k1gMnSc7	Hrdina
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Solnar	Solnar	k1gMnSc1	Solnar
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
začíná	začínat	k5eAaImIp3nS	začínat
nový	nový	k2eAgInSc1d1	nový
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
v	v	k7c6	v
Petrově	Petrův	k2eAgFnSc6d1	Petrova
chlapecké	chlapecký	k2eAgFnSc6d1	chlapecká
třídě	třída	k1gFnSc6	třída
nejsou	být	k5eNaImIp3nP	být
bezproblémové	bezproblémový	k2eAgFnPc1d1	bezproblémová
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
různé	různý	k2eAgFnPc1d1	různá
skupinky	skupinka	k1gFnPc1	skupinka
a	a	k8xC	a
nepsaným	psaný	k2eNgNnSc7d1	nepsané
"	"	kIx"	"
<g/>
kápem	kápo	k1gNnSc7	kápo
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pohledný	pohledný	k2eAgMnSc1d1	pohledný
a	a	k8xC	a
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
Ruda	Ruda	k1gMnSc1	Ruda
Lorenc	Lorenc	k1gMnSc1	Lorenc
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
také	také	k9	také
namyšlený	namyšlený	k2eAgMnSc1d1	namyšlený
<g/>
,	,	kIx,	,
sobecký	sobecký	k2eAgMnSc1d1	sobecký
a	a	k8xC	a
zákeřný	zákeřný	k2eAgMnSc1d1	zákeřný
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
osobní	osobní	k2eAgFnSc7d1	osobní
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
méněcennosti	méněcennost	k1gFnSc2	méněcennost
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
se	se	k3xPyFc4	se
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
soustředit	soustředit	k5eAaPmF	soustředit
a	a	k8xC	a
postrádá	postrádat	k5eAaImIp3nS	postrádat
blízkého	blízký	k2eAgMnSc4d1	blízký
kamaráda	kamarád	k1gMnSc4	kamarád
a	a	k8xC	a
nějaké	nějaký	k3yIgNnSc4	nějaký
životní	životní	k2eAgNnSc4d1	životní
naplnění	naplnění	k1gNnSc4	naplnění
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc3	jeho
čestné	čestný	k2eAgFnSc3d1	čestná
povaze	povaha	k1gFnSc3	povaha
vadí	vadit	k5eAaImIp3nS	vadit
pokřivené	pokřivený	k2eAgInPc4d1	pokřivený
poměry	poměr	k1gInPc4	poměr
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
odpůrcem	odpůrce	k1gMnSc7	odpůrce
Rudových	Rudových	k2eAgNnSc2d1	Rudových
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
pořádků	pořádek	k1gInPc2	pořádek
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získávat	získávat	k5eAaImF	získávat
další	další	k2eAgMnPc4d1	další
spolužáky	spolužák	k1gMnPc4	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnSc1	jejich
třídní	třídní	k2eAgMnSc1d1	třídní
učitel	učitel	k1gMnSc1	učitel
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
rozkol	rozkol	k1gInSc4	rozkol
vyřešit	vyřešit	k5eAaPmF	vyřešit
volbou	volba	k1gFnSc7	volba
předsedy	předseda	k1gMnSc2	předseda
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Ruda	ruda	k1gFnSc1	ruda
ve	v	k7c6	v
strachu	strach	k1gInSc6	strach
z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
zfalšuje	zfalšovat	k5eAaPmIp3nS	zfalšovat
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podvod	podvod	k1gInSc1	podvod
je	být	k5eAaImIp3nS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
a	a	k8xC	a
ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
úvaze	úvaha	k1gFnSc6	úvaha
vybere	vybrat	k5eAaPmIp3nS	vybrat
dva	dva	k4xCgMnPc4	dva
zástupce	zástupce	k1gMnPc4	zástupce
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
svou	svůj	k3xOyFgFnSc4	svůj
nabytou	nabytý	k2eAgFnSc4d1	nabytá
autoritu	autorita	k1gFnSc4	autorita
využívat	využívat	k5eAaPmF	využívat
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
celé	celý	k2eAgFnSc2d1	celá
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
Petrovo	Petrův	k2eAgNnSc1d1	Petrovo
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úspěch	úspěch	k1gInSc1	úspěch
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
hledané	hledaný	k2eAgNnSc1d1	hledané
zakotvení	zakotvení	k1gNnSc1	zakotvení
<g/>
,	,	kIx,	,
když	když	k8xS	když
náhodně	náhodně	k6eAd1	náhodně
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
na	na	k7c4	na
plakát	plakát	k1gInSc4	plakát
zvoucí	zvoucí	k2eAgInSc4d1	zvoucí
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nalezl	nalézt	k5eAaBmAgMnS	nalézt
naplnění	naplnění	k1gNnSc1	naplnění
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
tužeb	tužba	k1gFnPc2	tužba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
skutečného	skutečný	k2eAgNnSc2d1	skutečné
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vycházela	vycházet	k5eAaImAgFnS	vycházet
jako	jako	k9	jako
příběh	příběh	k1gInSc4	příběh
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Skaut-Junák	Skaut-Junák	k1gInSc1	Skaut-Junák
v	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
vydáních	vydání	k1gNnPc6	vydání
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
obnoveném	obnovený	k2eAgInSc6d1	obnovený
časopise	časopis	k1gInSc6	časopis
Skaut-Junák	Skaut-Junák	k1gInSc1	Skaut-Junák
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
knižně	knižně	k6eAd1	knižně
a	a	k8xC	a
poté	poté	k6eAd1	poté
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
A.	A.	kA	A.
L.	L.	kA	L.
Salač	Salač	k1gMnSc1	Salač
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
144	[number]	k4	144
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
A.	A.	kA	A.
L.	L.	kA	L.
Salač	Salač	k1gMnSc1	Salač
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
dotisk	dotisk	k1gInSc1	dotisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
144	[number]	k4	144
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
A.	A.	kA	A.
L.	L.	kA	L.
Salač	Salač	k1gMnSc1	Salač
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
A.	A.	kA	A.
L.	L.	kA	L.
Salač	Salač	k1gMnSc1	Salač
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
dotisk	dotisk	k1gInSc1	dotisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnPc1	ilustrace
A.	A.	kA	A.
L.	L.	kA	L.
Salač	Salač	k1gMnSc1	Salač
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Kobes	Kobes	k1gMnSc1	Kobes
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
140	[number]	k4	140
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Bohumír	Bohumír	k1gMnSc1	Bohumír
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Bohumír	Bohumír	k1gMnSc1	Bohumír
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Milan	Milan	k1gMnSc1	Milan
Zezula	Zezula	k1gMnSc1	Zezula
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
Cestování	cestování	k1gNnSc2	cestování
po	po	k7c6	po
tábořištích	tábořiště	k1gNnPc6	tábořiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Milan	Milan	k1gMnSc1	Milan
Zezula	Zezula	k1gMnSc1	Zezula
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
Cestování	cestování	k1gNnSc2	cestování
po	po	k7c6	po
tábořištích	tábořiště	k1gNnPc6	tábořiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Břetislav	Břetislav	k1gMnSc1	Břetislav
Charwot	Charwot	k1gMnSc1	Charwot
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kárász	Kárász	k1gMnSc1	Kárász
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Atos	Atos	k1gInSc1	Atos
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
112	[number]	k4	112
s.	s.	k?	s.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Gustav	Gustav	k1gMnSc1	Gustav
Krum	Krum	k1gMnSc1	Krum
<g/>
,	,	kIx,	,
Marko	Marko	k1gMnSc1	Marko
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Gustav	Gustav	k1gMnSc1	Gustav
Krum	Krum	k1gMnSc1	Krum
<g/>
,	,	kIx,	,
Marko	Marko	k1gMnSc1	Marko
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Foglara	Foglar	k1gMnSc2	Foglar
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
-	-	kIx~	-
Obrazový	obrazový	k2eAgInSc1d1	obrazový
soupis	soupis	k1gInSc1	soupis
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Lexikon	lexikon	k1gNnSc1	lexikon
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Boj	boj	k1gInSc4	boj
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
s.	s.	k?	s.
13	[number]	k4	13
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
a	a	k8xC	a
badatelskou	badatelský	k2eAgFnSc4d1	badatelská
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
-	-	kIx~	-
Obrazový	obrazový	k2eAgInSc1d1	obrazový
soupis	soupis	k1gInSc1	soupis
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Lexikon	lexikon	k1gNnSc1	lexikon
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Textová	textový	k2eAgFnSc1d1	textová
příloha	příloha	k1gFnSc1	příloha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
323-	[number]	k4	323-
325	[number]	k4	325
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určen	k2eAgNnSc1d1	určeno
pro	pro	k7c4	pro
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
a	a	k8xC	a
badatelskou	badatelský	k2eAgFnSc4d1	badatelská
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
