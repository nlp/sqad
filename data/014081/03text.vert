<s>
Seaborgium	Seaborgium	k1gNnSc1
</s>
<s>
Seaborgium	Seaborgium	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
4	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
wolframu	wolfram	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
271	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sg	Sg	kA
</s>
<s>
106	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Seaborgium	Seaborgium	k1gNnSc1
<g/>
,	,	kIx,
Sg	Sg	k1gFnSc1
<g/>
,	,	kIx,
106	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Seaborgium	Seaborgium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
54038-81-2	54038-81-2	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
271,13	271,13	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
132	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
63	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
[	[	kIx(
<g/>
Rn	Rn	k1gFnSc1
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
4	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
wolframu	wolfram	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgFnSc3d1
pevné	pevný	k2eAgFnSc3d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
W	W	kA
<g/>
⋏	⋏	k?
</s>
<s>
Dubnium	Dubnium	k1gNnSc1
≺	≺	k?
<g/>
Sg	Sg	k1gMnSc2
<g/>
≻	≻	k?
Bohrium	Bohrium	k1gNnSc1
</s>
<s>
Seaborgium	Seaborgium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sg	Sg	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtrnáctým	čtrnáctý	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc4d1
uměle	uměle	k6eAd1
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Seaborgium	Seaborgium	k1gNnSc1
doposud	doposud	k6eAd1
nebylo	být	k5eNaImAgNnS
izolováno	izolovat	k5eAaBmNgNnS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
poloze	poloha	k1gFnSc6
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
prvků	prvek	k1gInPc2
by	by	k9
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
mělo	mít	k5eAaImAgNnS
připomínat	připomínat	k5eAaImF
wolfram	wolfram	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
261	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
decay	decaa	k1gFnSc2
scheme	schat	k5eAaBmIp1nP,k5eAaImIp1nP,k5eAaPmIp1nP
2006	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
příprava	příprava	k1gFnSc1
prvku	prvek	k1gInSc2
s	s	k7c7
atomovým	atomový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
106	#num#	k4
byla	být	k5eAaImAgFnS
ohlášena	ohlášet	k5eAaImNgFnS,k5eAaPmNgFnS
téměř	téměř	k6eAd1
současně	současně	k6eAd1
ve	v	k7c6
dvou	dva	k4xCgFnPc6
světových	světový	k2eAgFnPc6d1
laboratořích	laboratoř	k1gFnPc6
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
ohlásila	ohlásit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Georgijem	Georgij	k1gInSc7
Fljorovem	Fljorov	k1gInSc7
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
Ústavu	ústav	k1gInSc2
jaderného	jaderný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
v	v	k7c6
Dubně	Dubna	k1gFnSc6
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
přípravu	příprava	k1gFnSc4
nového	nový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
s	s	k7c7
atomovým	atomový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
106	#num#	k4
<g/>
,	,	kIx,
relativní	relativní	k2eAgFnSc1d1
atomovou	atomový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
261	#num#	k4
a	a	k8xC
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
0,48	0,48	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
bombardováním	bombardování	k1gNnSc7
atomů	atom	k1gInPc2
olova	olovo	k1gNnSc2
jádry	jádro	k1gNnPc7
atomů	atom	k1gInPc2
chromu	chrom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
20882	#num#	k4
Pb	Pb	k1gFnSc1
+	+	kIx~
5424	#num#	k4
Cr	cr	k0
→	→	k?
261106	#num#	k4
Sg	Sg	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
V	v	k7c4
září	září	k1gNnSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
oznámil	oznámit	k5eAaPmAgInS
tým	tým	k1gInSc1
vedený	vedený	k2eAgInSc1d1
Albertem	Albert	k1gMnSc7
Ghiorsem	Ghiors	k1gMnSc7
z	z	k7c2
kalifornské	kalifornský	k2eAgFnSc2d1
university	universita	k1gFnSc2
v	v	k7c4
Berkeley	Berkele	k2eAgFnPc4d1
syntézu	syntéza	k1gFnSc4
izotopu	izotop	k1gInSc2
s	s	k7c7
relativní	relativní	k2eAgFnSc7d1
atomovou	atomový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
263	#num#	k4
a	a	k8xC
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
1,0	1,0	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
objev	objev	k1gInSc1
byl	být	k5eAaImAgInS
jako	jako	k9
první	první	k4xOgInSc1
potvrzen	potvrzen	k2eAgInSc1d1
nezávislým	závislý	k2eNgInSc7d1
experimentem	experiment	k1gInSc7
a	a	k8xC
Američané	Američan	k1gMnPc1
navrhli	navrhnout	k5eAaPmAgMnP
pro	pro	k7c4
nový	nový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
název	název	k1gInSc1
seaborgium	seaborgium	k1gNnSc1
na	na	k7c4
počest	počest	k1gFnSc4
jaderného	jaderný	k2eAgMnSc2d1
fyzika	fyzik	k1gMnSc2
Glena	Glen	k1gMnSc2
T.	T.	kA
Seaborga	Seaborg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připraven	připraven	k2eAgInSc1d1
byl	být	k5eAaImAgInS
reakcí	reakce	k1gFnPc2
jader	jádro	k1gNnPc2
kalifornia	kalifornium	k1gNnPc1
s	s	k7c7
jádry	jádro	k1gNnPc7
prvku	prvek	k1gInSc2
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
24998	#num#	k4
Cf	Cf	k1gFnSc1
+	+	kIx~
188	#num#	k4
O	o	k7c6
→	→	k?
263106	#num#	k4
Sg	Sg	k1gFnPc2
+	+	kIx~
4	#num#	k4
10	#num#	k4
n	n	k0
</s>
<s>
Návrh	návrh	k1gInSc1
na	na	k7c4
pojmenování	pojmenování	k1gNnSc4
prvku	prvek	k1gInSc2
po	po	k7c6
dosud	dosud	k6eAd1
žijícím	žijící	k2eAgMnSc6d1
vědci	vědec	k1gMnPc7
vzbudil	vzbudit	k5eAaPmAgInS
značné	značný	k2eAgInPc4d1
rozpaky	rozpak	k1gInPc4
a	a	k8xC
diskuze	diskuze	k1gFnPc4
ve	v	k7c6
vědeckém	vědecký	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mnoha	mnoho	k4c6
debatách	debata	k1gFnPc6
bylo	být	k5eAaImAgNnS
konečně	konečně	k6eAd1
na	na	k7c6
zasedání	zasedání	k1gNnSc6
IUPAC	IUPAC	kA
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
definitivně	definitivně	k6eAd1
potvrzeno	potvrzen	k2eAgNnSc1d1
pojmenování	pojmenování	k1gNnSc1
prvku	prvek	k1gInSc2
seaborgium	seaborgium	k1gNnSc1
a	a	k8xC
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sg.	Sg.	k1gFnSc2
</s>
<s>
Glenn	Glenn	k1gMnSc1
Seaborg	Seaborg	k1gMnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
16	#num#	k4
izotopů	izotop	k1gInPc2
seaborgia	seaborgius	k1gMnSc2
<g/>
,	,	kIx,
nejstálejší	stálý	k2eAgInSc1d3
známý	známý	k2eAgInSc1d1
izotop	izotop	k1gInSc1
269	#num#	k4
<g/>
Sg	Sg	k1gFnPc2
se	se	k3xPyFc4
jako	jako	k9
α	α	k?
zářič	zářič	k1gInSc1
rozpadá	rozpadat	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
poločasem	poločas	k1gInSc7
3,1	3,1	k4
minuty	minuta	k1gFnSc2
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
stálý	stálý	k2eAgInSc4d1
izotop	izotop	k1gInSc4
258	#num#	k4
<g/>
Sg	Sg	k1gMnSc1
má	mít	k5eAaImIp3nS
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
pouhé	pouhý	k2eAgFnSc2d1
2,9	2,9	k4
milisekundy	milisekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poločasy	poločas	k1gInPc1
přeměny	přeměna	k1gFnSc2
268	#num#	k4
<g/>
Sg	Sg	k1gFnPc2
<g/>
,	,	kIx,
270	#num#	k4
<g/>
Sg	Sg	k1gFnPc2
<g/>
,	,	kIx,
272	#num#	k4
<g/>
Sg	Sg	k1gMnPc2
a	a	k8xC
273	#num#	k4
<g/>
Sg	Sg	k1gFnPc2
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
rozpadu	rozpad	k1gInSc2
</s>
<s>
258	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
209	#num#	k4
<g/>
Bi	Bi	k1gFnSc1
<g/>
(	(	kIx(
<g/>
51	#num#	k4
<g/>
V	v	k7c4
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
2,9	2,9	k4
ms	ms	k?
</s>
<s>
259	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
1985	#num#	k4
</s>
<s>
207	#num#	k4
<g/>
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
54	#num#	k4
<g/>
Cr	cr	k0
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
0,29	0,29	k4
s	s	k7c7
</s>
<s>
260	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
1985	#num#	k4
</s>
<s>
208	#num#	k4
<g/>
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
54	#num#	k4
<g/>
Cr	cr	k0
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
4,95	4,95	k4
ms	ms	k?
</s>
<s>
261	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
1985	#num#	k4
</s>
<s>
208	#num#	k4
<g/>
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
54	#num#	k4
<g/>
Cr	cr	k0
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
178	#num#	k4
ms	ms	k?
</s>
<s>
262	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
207	#num#	k4
<g/>
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
6,9	6,9	k4
ms	ms	k?
</s>
<s>
263	#num#	k4
<g/>
Sgm	Sgm	k1gFnSc1
</s>
<s>
1974	#num#	k4
</s>
<s>
249	#num#	k4
<g/>
Cf	Cf	k1gFnSc1
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
O	o	k7c4
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
1,0	1,0	k4
s	s	k7c7
</s>
<s>
263	#num#	k4
<g/>
Sgg	Sgg	k1gFnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
208	#num#	k4
<g/>
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
0,12	0,12	k4
s	s	k7c7
</s>
<s>
264	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
238	#num#	k4
<g/>
U	u	k7c2
<g/>
(	(	kIx(
<g/>
30	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
37	#num#	k4
ms	ms	k?
</s>
<s>
265	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
Ne	ne	k9
<g/>
,5	,5	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
14,4	14,4	k4
s	s	k7c7
</s>
<s>
266	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
26	#num#	k4
<g/>
Mg	mg	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
s	s	k7c7
</s>
<s>
267	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
26	#num#	k4
<g/>
Mg	mg	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
</s>
<s>
268	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
</s>
<s>
269	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
3,1	3,1	k4
min	mina	k1gFnPc2
</s>
<s>
270	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
</s>
<s>
271	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
242	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
</s>
<s>
2,4	2,4	k4
min	mina	k1gFnPc2
</s>
<s>
272	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
</s>
<s>
273	#num#	k4
<g/>
Sg	Sg	k1gFnSc1
</s>
<s>
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Arnold	Arnold	k1gMnSc1
F.	F.	kA
Holleman	Holleman	k1gMnSc1
<g/>
,	,	kIx,
Nils	Nils	k1gInSc1
Wiberg	Wiberg	k1gInSc1
<g/>
:	:	kIx,
Lehrbuch	Lehrbuch	k1gInSc1
der	drát	k5eAaImRp2nS
Anorganischen	Anorganischen	k1gInSc1
Chemie	chemie	k1gFnSc1
102	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auflage	Auflag	k1gInSc2
<g/>
,	,	kIx,
de	de	k?
Gruyter	Gruytra	k1gFnPc2
<g/>
,	,	kIx,
Berlin	berlina	k1gFnPc2
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17770	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
seaborgium	seaborgium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
seaborgium	seaborgium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4355423-4	4355423-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2004005779	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2004005779	#num#	k4
</s>
