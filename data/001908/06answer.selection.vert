<s>
Feoplast	Feoplast	k1gInSc1	Feoplast
je	být	k5eAaImIp3nS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
fotosynteticky	fotosynteticky	k6eAd1	fotosynteticky
aktivní	aktivní	k2eAgInSc1d1	aktivní
plastid	plastid	k1gInSc1	plastid
obsahující	obsahující	k2eAgInSc4d1	obsahující
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
fukoxantin	fukoxantin	k1gInSc4	fukoxantin
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
u	u	k7c2	u
hnědých	hnědý	k2eAgFnPc2d1	hnědá
řas	řasa	k1gFnPc2	řasa
(	(	kIx(	(
<g/>
Phaeophyta	Phaeophyta	k1gFnSc1	Phaeophyta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
