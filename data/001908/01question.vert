<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
plastid	plastid	k1gInSc1	plastid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
fukoxantin	fukoxantin	k1gInSc4	fukoxantin
<g/>
?	?	kIx.	?
</s>
