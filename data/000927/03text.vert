<s>
Josef	Josef	k1gMnSc1	Josef
Lada	lado	k1gNnSc2	lado
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1887	[number]	k4	1887
Hrusice	Hrusice	k1gFnSc1	Hrusice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1957	[number]	k4	1957
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
hrusickému	hrusický	k2eAgMnSc3d1	hrusický
ševci	švec	k1gMnSc3	švec
Josefu	Josef	k1gMnSc3	Josef
Ladovi	Lada	k1gMnSc3	Lada
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Alžbětě	Alžběta	k1gFnSc3	Alžběta
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc3d1	rozená
Janovské	Janovská	k1gFnSc3	Janovská
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
bratra	bratr	k1gMnSc4	bratr
Františka	František	k1gMnSc4	František
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
Antonii	Antonie	k1gFnSc4	Antonie
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marii	Maria	k1gFnSc3	Maria
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Hrusic	Hrusice	k1gFnPc2	Hrusice
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
také	také	k9	také
čerpal	čerpat	k5eAaImAgInS	čerpat
řadu	řada	k1gFnSc4	řada
námětů	námět	k1gInPc2	námět
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
Královských	královský	k2eAgInPc6d1	královský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
vyučil	vyučit	k5eAaPmAgInS	vyučit
malířem	malíř	k1gMnSc7	malíř
pokojů	pokoj	k1gInPc2	pokoj
a	a	k8xC	a
divadelních	divadelní	k2eAgFnPc2d1	divadelní
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
na	na	k7c4	na
knihaře	knihař	k1gMnSc4	knihař
a	a	k8xC	a
zlatiče	zlatič	k1gMnSc4	zlatič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kresbě	kresba	k1gFnSc6	kresba
byl	být	k5eAaImAgInS	být
samoukem	samouk	k1gMnSc7	samouk
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
osobitý	osobitý	k2eAgInSc4d1	osobitý
styl	styl	k1gInSc4	styl
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
silnou	silný	k2eAgFnSc7d1	silná
linkou	linka	k1gFnSc7	linka
a	a	k8xC	a
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
tvary	tvar	k1gInPc7	tvar
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
půlroční	půlroční	k2eAgMnSc1d1	půlroční
chlapec	chlapec	k1gMnSc1	chlapec
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c4	na
knejp	knejp	k1gInSc4	knejp
(	(	kIx(	(
<g/>
ševcovský	ševcovský	k2eAgInSc4d1	ševcovský
nůž	nůž	k1gInSc4	nůž
<g/>
)	)	kIx)	)
a	a	k8xC	a
poranil	poranit	k5eAaPmAgMnS	poranit
si	se	k3xPyFc3	se
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
viděl	vidět	k5eAaImAgMnS	vidět
jen	jen	k9	jen
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
a	a	k8xC	a
chybělo	chybět	k5eAaImAgNnS	chybět
mu	on	k3xPp3gMnSc3	on
prostorové	prostorový	k2eAgNnSc1d1	prostorové
vidění	vidění	k1gNnSc1	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
karikaturista	karikaturista	k1gMnSc1	karikaturista
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaBmAgMnS	napsat
(	(	kIx(	(
<g/>
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Kocour	kocour	k1gMnSc1	kocour
Mikeš	Mikeš	k1gMnSc1	Mikeš
a	a	k8xC	a
O	o	k7c6	o
chytré	chytrý	k2eAgFnSc6d1	chytrá
kmotře	kmotra	k1gFnSc6	kmotra
lišce	liška	k1gFnSc6	liška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
především	především	k9	především
Osudy	osud	k1gInPc4	osud
dobrého	dobrý	k2eAgMnSc4d1	dobrý
vojáka	voják	k1gMnSc4	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Švejka	Švejk	k1gMnSc4	Švejk
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
1339	[number]	k4	1339
kreseb	kresba	k1gFnPc2	kresba
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
knihy	kniha	k1gFnPc4	kniha
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Tyrolské	tyrolský	k2eAgFnPc4d1	tyrolská
elegie	elegie	k1gFnPc4	elegie
<g/>
,	,	kIx,	,
Krále	Král	k1gMnPc4	Král
Lávru	Lávra	k1gMnSc4	Lávra
a	a	k8xC	a
Epigramy	epigram	k1gInPc4	epigram
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
<g/>
,	,	kIx,	,
Erbenovy	Erbenův	k2eAgFnPc4d1	Erbenova
a	a	k8xC	a
Drdovy	Drdův	k2eAgFnPc4d1	Drdova
pohádky	pohádka	k1gFnPc4	pohádka
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
scén	scéna	k1gFnPc2	scéna
a	a	k8xC	a
kostýmů	kostým	k1gInPc2	kostým
k	k	k7c3	k
divadelním	divadelní	k2eAgFnPc3d1	divadelní
hrám	hra	k1gFnPc3	hra
i	i	k8xC	i
operám	opera	k1gFnPc3	opera
uváděným	uváděný	k2eAgFnPc3d1	uváděná
Národním	národní	k2eAgNnSc7d1	národní
divadlem	divadlo	k1gNnSc7	divadlo
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
režiséra	režisér	k1gMnSc2	režisér
Josefa	Josef	k1gMnSc2	Josef
Macha	Mach	k1gMnSc2	Mach
Hrátky	hrátky	k1gFnPc4	hrátky
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
natočeného	natočený	k2eAgInSc2d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Jana	Jan	k1gMnSc2	Jan
Drdy	Drda	k1gMnSc2	Drda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
asi	asi	k9	asi
400	[number]	k4	400
volných	volný	k2eAgInPc2d1	volný
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
okolo	okolo	k7c2	okolo
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
ilustrací	ilustrace	k1gFnPc2	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
ilustrace	ilustrace	k1gFnPc1	ilustrace
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
Chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
malebným	malebný	k2eAgInSc7d1	malebný
slovním	slovní	k2eAgInSc7d1	slovní
doprovodem	doprovod	k1gInSc7	doprovod
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
známým	známý	k2eAgInPc3d1	známý
obrázkům	obrázek	k1gInPc3	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Čapkem	Čapek	k1gMnSc7	Čapek
<g/>
,	,	kIx,	,
Nezvalem	Nezval	k1gMnSc7	Nezval
a	a	k8xC	a
Vančurou	Vančura	k1gMnSc7	Vančura
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgFnSc2d1	moderní
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
"	"	kIx"	"
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
titul	titul	k1gInSc4	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1923	[number]	k4	1923
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
Budějickou	Budějickou	k?	Budějickou
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
a	a	k8xC	a
narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dcery	dcera	k1gFnPc4	dcera
Alena	Alena	k1gFnSc1	Alena
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Alena	Alena	k1gFnSc1	Alena
Ladová	Ladová	k1gFnSc1	Ladová
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
otci	otec	k1gMnSc3	otec
napsala	napsat	k5eAaBmAgFnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
knihu	kniha	k1gFnSc4	kniha
Můj	můj	k3xOp1gMnSc1	můj
táta	táta	k1gMnSc1	táta
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
přes	přes	k7c4	přes
9000	[number]	k4	9000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
byly	být	k5eAaImAgInP	být
také	také	k9	také
použity	použit	k2eAgFnPc1d1	použita
ilustrace	ilustrace	k1gFnPc1	ilustrace
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
Ladových	Ladová	k1gFnPc2	Ladová
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Eva	Eva	k1gFnSc1	Eva
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nadaná	nadaný	k2eAgFnSc1d1	nadaná
klavíristka	klavíristka	k1gFnSc1	klavíristka
a	a	k8xC	a
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
bombardování	bombardování	k1gNnSc6	bombardování
u	u	k7c2	u
Emauzského	emauzský	k2eAgInSc2d1	emauzský
kláštera	klášter	k1gInSc2	klášter
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
asteroid	asteroid	k1gInSc4	asteroid
17625	[number]	k4	17625
Joseflada	Joseflada	k1gFnSc1	Joseflada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ladově	Ladův	k2eAgInSc6d1	Ladův
kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
naučné	naučný	k2eAgFnPc1d1	naučná
stezky	stezka	k1gFnPc1	stezka
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
kocoura	kocour	k1gMnSc2	kocour
Mikeše	Mikeš	k1gMnSc2	Mikeš
a	a	k8xC	a
Pohádkové	pohádkový	k2eAgFnSc2d1	pohádková
Hrusice	Hrusice	k1gFnSc2	Hrusice
<g/>
.	.	kIx.	.
</s>
<s>
Tancovačka	tancovačka	k1gFnSc1	tancovačka
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Sváteční	svátečnět	k5eAaImIp3nS	svátečnět
hospoda	hospoda	k?	hospoda
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Rodná	rodný	k2eAgFnSc1d1	rodná
chalupa	chalupa	k1gFnSc1	chalupa
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Zabijačka	zabijačka	k1gFnSc1	zabijačka
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
Triptych	triptych	k1gInSc1	triptych
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
krajinou	krajina	k1gFnSc7	krajina
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
cyklus	cyklus	k1gInSc1	cyklus
4	[number]	k4	4
Dětských	dětský	k2eAgFnPc2d1	dětská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
Pokoj	pokoj	k1gInSc1	pokoj
lidem	lid	k1gInSc7	lid
<g />
.	.	kIx.	.
</s>
<s>
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
Vodník	vodník	k1gMnSc1	vodník
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
cyklus	cyklus	k1gInSc1	cyklus
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Betlém	Betlém	k1gInSc1	Betlém
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Ponocný	ponocný	k1gMnSc1	ponocný
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
noci	noc	k1gFnSc6	noc
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
na	na	k7c6	na
splavu	splav	k1gInSc6	splav
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Rvačka	rvačka	k1gFnSc1	rvačka
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Slouha	slouha	k1gMnSc1	slouha
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
anděl	anděl	k1gMnSc1	anděl
a	a	k8xC	a
čert	čert	k1gMnSc1	čert
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Hastrmanův	hastrmanův	k2eAgInSc4d1	hastrmanův
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Ponocný	ponocný	k1gMnSc1	ponocný
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Tichá	Tichá	k1gFnSc1	Tichá
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Sáňkující	sáňkující	k2eAgFnPc1d1	sáňkující
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Podzim	podzim	k1gInSc1	podzim
(	(	kIx(	(
<g/>
U	u	k7c2	u
ohníčku	ohníček	k1gInSc2	ohníček
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Jaro	jaro	k1gNnSc1	jaro
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
U	u	k7c2	u
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromku	stromek	k1gInSc2	stromek
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Hastrman	hastrman	k1gMnSc1	hastrman
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gFnSc1	můj
abeceda	abeceda	k1gFnSc1	abeceda
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
podklad	podklad	k1gInSc4	podklad
posloužila	posloužit	k5eAaPmAgFnS	posloužit
národní	národní	k2eAgFnSc1d1	národní
říkadla	říkadlo	k1gNnSc2	říkadlo
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
asi	asi	k9	asi
o	o	k7c4	o
první	první	k4xOgFnSc4	první
barevnou	barevný	k2eAgFnSc4d1	barevná
dětskou	dětský	k2eAgFnSc4d1	dětská
knížku	knížka	k1gFnSc4	knížka
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalamajka	kalamajka	k1gFnSc1	kalamajka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Haškem	Hašek	k1gMnSc7	Hašek
<g/>
,	,	kIx,	,
Veselý	Veselý	k1gMnSc1	Veselý
přírodopis	přírodopis	k1gInSc1	přírodopis
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Illustrovaná	Illustrovaný	k2eAgFnSc1d1	Illustrovaný
frazeologie	frazeologie	k1gFnSc1	frazeologie
a	a	k8xC	a
přísloví	přísloví	k1gNnSc1	přísloví
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ladovy	Ladův	k2eAgFnPc1d1	Ladova
veselé	veselý	k2eAgFnPc4d1	veselá
učebnice	učebnice	k1gFnPc4	učebnice
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
díly	díl	k1gInPc4	díl
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
názvy	název	k1gInPc1	název
Ptáci	pták	k1gMnPc1	pták
,	,	kIx,	,
Savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
Brouci	brouk	k1gMnPc1	brouk
a	a	k8xC	a
hmyz	hmyz	k1gInSc1	hmyz
a	a	k8xC	a
Vodní	vodní	k2eAgNnSc1d1	vodní
živočišstvo	živočišstvo	k1gNnSc1	živočišstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Tondy	Tonda	k1gFnSc2	Tonda
Čutala	čutat	k5eAaImAgFnS	čutat
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mňoukačky	Mňoukačka	k1gFnSc2	Mňoukačka
naší	náš	k3xOp1gFnSc2	náš
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ezopské	ezopský	k2eAgFnPc1d1	ezopský
bajky	bajka	k1gFnPc1	bajka
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Halekačky	halekačka	k1gFnSc2	halekačka
naší	náš	k3xOp1gFnSc2	náš
Kačky	Kačka	k1gFnSc2	Kačka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kocour	kocour	k1gMnSc1	kocour
Mikeš	Mikeš	k1gMnSc1	Mikeš
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
díly	díl	k1gInPc4	díl
-	-	kIx~	-
O	o	k7c6	o
Mikešovi	Mikeš	k1gMnSc6	Mikeš
<g/>
,	,	kIx,	,
Do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Cirkus	cirkus	k1gInSc1	cirkus
Mikeš	Mikeš	k1gMnSc1	Mikeš
a	a	k8xC	a
Kludský	Kludský	k2eAgMnSc1d1	Kludský
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
domov	domov	k1gInSc1	domov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
mluvícím	mluvící	k2eAgMnSc6d1	mluvící
kocourkovi	kocourek	k1gMnSc6	kocourek
Mikešovi	Mikeš	k1gMnSc6	Mikeš
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
neuvěřitelných	uvěřitelný	k2eNgFnPc6d1	neuvěřitelná
příhodách	příhoda	k1gFnPc6	příhoda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
prožil	prožít	k5eAaPmAgMnS	prožít
sám	sám	k3xTgMnSc1	sám
i	i	k8xC	i
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kamarády	kamarád	k1gMnPc7	kamarád
<g/>
,	,	kIx,	,
vepříkem	vepřík	k1gMnSc7	vepřík
Pašíkem	pašík	k1gMnSc7	pašík
a	a	k8xC	a
kozlem	kozel	k1gMnSc7	kozel
Bobešem	Bobeš	k1gMnSc7	Bobeš
<g/>
,	,	kIx,	,
O	o	k7c6	o
chytré	chytrý	k2eAgFnSc6d1	chytrá
kmotře	kmotra	k1gFnSc6	kmotra
lišce	liška	k1gFnSc6	liška
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádkové	pohádkový	k2eAgNnSc4d1	pohádkové
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
chytré	chytrý	k2eAgFnSc6d1	chytrá
lišce	liška	k1gFnSc6	liška
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vychována	vychovat	k5eAaPmNgFnS	vychovat
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
naučila	naučit	k5eAaPmAgFnS	naučit
se	se	k3xPyFc4	se
mluvit	mluvit	k5eAaImF	mluvit
a	a	k8xC	a
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
když	když	k8xS	když
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
proslavila	proslavit	k5eAaPmAgFnS	proslavit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
šprýmovnými	šprýmovný	k2eAgInPc7d1	šprýmovný
kousky	kousek	k1gInPc7	kousek
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
vzorným	vzorný	k2eAgFnPc3d1	vzorná
hajným	hajná	k1gFnPc3	hajná
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pohádky	pohádka	k1gFnSc2	pohádka
naruby	naruby	k6eAd1	naruby
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozmarné	rozmarný	k2eAgFnSc2d1	rozmarná
<g/>
,	,	kIx,	,
na	na	k7c4	na
ruby	rub	k1gInPc4	rub
převrácené	převrácený	k2eAgFnSc2d1	převrácená
pohádky	pohádka	k1gFnSc2	pohádka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
např.	např.	kA	např.
o	o	k7c6	o
Popelákovi	Popelák	k1gMnSc6	Popelák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bubáci	bubák	k1gMnPc1	bubák
a	a	k8xC	a
hastrmani	hastrman	k1gMnPc1	hastrman
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Straky	straka	k1gFnPc4	straka
na	na	k7c6	na
vrbě	vrba	k1gFnSc6	vrba
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veselé	veselý	k2eAgFnPc4d1	veselá
příhody	příhoda	k1gFnPc4	příhoda
z	z	k7c2	z
rodných	rodný	k2eAgFnPc2d1	rodná
Hrusic	Hrusice	k1gFnPc2	Hrusice
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
Kronika	kronika	k1gFnSc1	kronika
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorovy	autorův	k2eAgFnPc4d1	autorova
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
dětství	dětství	k1gNnSc6	dětství
v	v	k7c6	v
Hrusicích	Hrusice	k1gFnPc6	Hrusice
<g/>
,	,	kIx,	,
na	na	k7c4	na
hry	hra	k1gFnPc4	hra
s	s	k7c7	s
hrusickými	hrusický	k2eAgMnPc7d1	hrusický
kluky	kluk	k1gMnPc7	kluk
a	a	k8xC	a
děvčaty	děvče	k1gNnPc7	děvče
<g/>
,	,	kIx,	,
na	na	k7c4	na
první	první	k4xOgInPc4	první
malířské	malířský	k2eAgInPc4d1	malířský
začátky	začátek	k1gInPc4	začátek
<g/>
,	,	kIx,	,
na	na	k7c4	na
svízelnou	svízelný	k2eAgFnSc4d1	svízelná
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
na	na	k7c4	na
zážitky	zážitek	k1gInPc4	zážitek
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
malířské	malířský	k2eAgFnSc2d1	malířská
<g/>
,	,	kIx,	,
spisovatelské	spisovatelský	k2eAgFnSc2d1	spisovatelská
a	a	k8xC	a
redaktorské	redaktorský	k2eAgFnSc2d1	redaktorská
<g/>
,	,	kIx,	,
Nezbedné	nezbedný	k2eAgFnSc2d1	nezbedná
pohádky	pohádka	k1gFnSc2	pohádka
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Veselé	Veselé	k2eAgFnPc1d1	Veselé
kresby	kresba	k1gFnPc1	kresba
Josefa	Josef	k1gMnSc2	Josef
Lady	Lada	k1gFnSc2	Lada
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
dětem	dítě	k1gFnPc3	dítě
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Říkadla	říkadlo	k1gNnPc1	říkadlo
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
posmrtně	posmrtně	k6eAd1	posmrtně
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kreslený	kreslený	k2eAgInSc1d1	kreslený
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
text	text	k1gInSc1	text
Lada	lado	k1gNnSc2	lado
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
jako	jako	k9	jako
úpravu	úprava	k1gFnSc4	úprava
<g />
.	.	kIx.	.
</s>
<s>
světově	světově	k6eAd1	světově
proslulého	proslulý	k2eAgInSc2d1	proslulý
Haškova	Haškův	k2eAgInSc2d1	Haškův
románu	román	k1gInSc2	román
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pod	pod	k7c4	pod
své	svůj	k3xOyFgInPc4	svůj
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
Ladovy	Ladův	k2eAgFnPc4d1	Ladova
pohádky	pohádka	k1gFnPc4	pohádka
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
vydání	vydání	k1gNnSc1	vydání
knih	kniha	k1gFnPc2	kniha
Bubáci	bubák	k1gMnPc1	bubák
a	a	k8xC	a
hastrmani	hastrman	k1gMnPc1	hastrman
<g/>
,	,	kIx,	,
O	o	k7c6	o
chytré	chytrý	k2eAgFnSc6d1	chytrá
kmotře	kmotra	k1gFnSc6	kmotra
lišce	liška	k1gFnSc6	liška
a	a	k8xC	a
Nezbedné	nezbedný	k2eAgFnPc4d1	nezbedná
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
Bajky	bajka	k1gFnPc4	bajka
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
a	a	k8xC	a
převyprávění	převyprávění	k1gNnSc1	převyprávění
Ezopových	Ezopův	k2eAgFnPc2d1	Ezopova
bajek	bajka	k1gFnPc2	bajka
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
českých	český	k2eAgNnPc2d1	české
říkadel	říkadlo	k1gNnPc2	říkadlo
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
českých	český	k2eAgMnPc2d1	český
lidových	lidový	k2eAgMnPc2d1	lidový
říkadel	říkadlo	k1gNnPc2	říkadlo
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnSc6	lady
<g/>
.	.	kIx.	.
</s>
<s>
Čáry	čára	k1gFnPc4	čára
máry	máry	k0	máry
fuk	fuk	k6eAd1	fuk
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
jsem	být	k5eAaImIp1nS	být
zase	zase	k9	zase
kluk	kluk	k1gMnSc1	kluk
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
Jiřího	Jiří	k1gMnSc2	Jiří
Žáčka	Žáček	k1gMnSc2	Žáček
inspirované	inspirovaný	k2eAgInPc1d1	inspirovaný
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnSc6	lady
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
i	i	k8xC	i
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
dokumenty	dokument	k1gInPc4	dokument
režíroval	režírovat	k5eAaImAgMnS	režírovat
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
Hugo	Hugo	k1gMnSc1	Hugo
Huška	Hušek	k1gMnSc2	Hušek
<g/>
:	:	kIx,	:
Ladovi	Ladův	k2eAgMnPc1d1	Ladův
furianti	furiant	k1gMnPc1	furiant
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
