<s>
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1
korunaSvensk	korunaSvensk	k1gInSc4
krona	kron	k1gInSc2
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
<g/>
)	)	kIx)
mince	mince	k1gFnSc1
50	#num#	k4
öre	öre	k?
(	(	kIx(
<g/>
neplatná	platný	k2eNgFnSc1d1
od	od	k7c2
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
Země	zem	k1gFnSc2
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
SEK	sek	k1gInSc1
Inflace	inflace	k1gFnSc2
</s>
<s>
1,9	1,9	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
kr	kr	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
öre	öre	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
korun	koruna	k1gFnPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
krona	kroen	k2eAgFnSc1d1
<g/>
,	,	kIx,
množné	množný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
kronor	kronor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
skandinávského	skandinávský	k2eAgInSc2d1
státu	stát	k1gInSc2
Švédsko	Švédsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
SEK	SEK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
koruny	koruna	k1gFnSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
„	„	k?
<g/>
öre	öre	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
avšak	avšak	k8xC
mince	mince	k1gFnSc1
nejnižší	nízký	k2eAgFnSc2d3
nominální	nominální	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
je	být	k5eAaImIp3nS
1	#num#	k4
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1873	#num#	k4
a	a	k8xC
1914	#num#	k4
fungovala	fungovat	k5eAaImAgFnS
mezi	mezi	k7c7
Švédskem	Švédsko	k1gNnSc7
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
unii	unie	k1gFnSc6
s	s	k7c7
Norskem	Norsko	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
Dánskem	Dánsko	k1gNnSc7
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
Island	Island	k1gInSc1
<g/>
)	)	kIx)
Skandinávská	skandinávský	k2eAgFnSc1d1
měnová	měnový	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
zmíněných	zmíněný	k2eAgInPc6d1
státech	stát	k1gInPc6
platilo	platit	k5eAaImAgNnS
jednotnou	jednotný	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
–	–	k?
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
unie	unie	k1gFnSc2
začal	začít	k5eAaPmAgMnS
každý	každý	k3xTgMnSc1
stát	stát	k5eAaImF,k5eAaPmF
používat	používat	k5eAaImF
vlastní	vlastní	k2eAgFnSc2d1
nezávislé	závislý	k2eNgFnSc2d1
měny	měna	k1gFnSc2
odvozené	odvozený	k2eAgFnSc2d1
od	od	k7c2
zaniklé	zaniklý	k2eAgFnSc2d1
společné	společný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
nástupnické	nástupnický	k2eAgFnPc4d1
měny	měna	k1gFnPc4
si	se	k3xPyFc3
ale	ale	k9
zachovaly	zachovat	k5eAaPmAgFnP
stejný	stejný	k2eAgInSc4d1
název	název	k1gInSc4
„	„	k?
<g/>
koruna	koruna	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
přizpůsobil	přizpůsobit	k5eAaPmAgMnS
místní	místní	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
a	a	k8xC
pravopisu	pravopis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vznikem	vznik	k1gInSc7
Skandinávské	skandinávský	k2eAgFnSc2d1
měnové	měnový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
měna	měna	k1gFnSc1
riksdaler	riksdaler	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
1982	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tzv.	tzv.	kA
Velkému	velký	k2eAgInSc3d1
třesku	třesk	k1gInSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Big	Big	k1gMnSc1
Bang	Bang	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vláda	vláda	k1gFnSc1
musela	muset	k5eAaImAgFnS
nuceně	nuceně	k6eAd1
devalvovat	devalvovat	k5eAaBmF
korunu	koruna	k1gFnSc4
o	o	k7c4
16	#num#	k4
%	%	kIx~
v	v	k7c6
důsledku	důsledek	k1gInSc6
ekonomického	ekonomický	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
Velký	velký	k2eAgInSc1d1
třesk	třesk	k1gInSc1
je	být	k5eAaImIp3nS
milníkem	milník	k1gInSc7
-	-	kIx~
označuje	označovat	k5eAaImIp3nS
pro	pro	k7c4
Švédsko	Švédsko	k1gNnSc4
začátek	začátek	k1gInSc4
nového	nový	k2eAgNnSc2d1
období	období	k1gNnSc2
ve	v	k7c6
finančním	finanční	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
členem	člen	k1gMnSc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
začít	začít	k5eAaPmF
používat	používat	k5eAaImF
euro	euro	k1gNnSc4
<g/>
,	,	kIx,
až	až	k6eAd1
splní	splnit	k5eAaPmIp3nS
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
zavedení	zavedení	k1gNnSc4
eura	euro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
tyto	tento	k3xDgInPc4
podmínky	podmínka	k1gFnPc1
po	po	k7c4
většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
členství	členství	k1gNnSc2
v	v	k7c6
EU	EU	kA
plní	plnit	k5eAaImIp3nS
<g/>
,	,	kIx,
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
však	však	k9
politická	politický	k2eAgFnSc1d1
vůle	vůle	k1gFnSc1
euro	euro	k1gNnSc1
zavést	zavést	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Referendum	referendum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
dopadlo	dopadnout	k5eAaPmAgNnS
56,1	56,1	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
proti	proti	k7c3
přijetí	přijetí	k1gNnSc3
eura	euro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
Švédsko	Švédsko	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
rozhodlo	rozhodnout	k5eAaPmAgNnS
pro	pro	k7c4
nepřijetí	nepřijetí	k1gNnSc4
eura	euro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
by	by	kYmCp3nS
de	de	k?
iure	iure	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
zavedeno	zavést	k5eAaPmNgNnS
<g/>
,	,	kIx,
de	de	k?
facto	facto	k1gNnSc1
má	mít	k5eAaImIp3nS
však	však	k9
země	země	k1gFnSc1
opt-out	opt-out	k5eAaImF,k5eAaPmF
na	na	k7c4
zavedení	zavedení	k1gNnSc4
jednotné	jednotný	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
V	v	k7c6
oběhu	oběh	k1gInSc6
jsou	být	k5eAaImIp3nP
mince	mince	k1gFnPc1
nominálních	nominální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
a	a	k8xC
10	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
mincích	mince	k1gFnPc6
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
vyobrazen	vyobrazen	k2eAgMnSc1d1
monarcha	monarcha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tisknuty	tisknout	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
lícových	lícový	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
bankovek	bankovka	k1gFnPc2
jsou	být	k5eAaImIp3nP
vyobrazeny	vyobrazen	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
švédské	švédský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Astrid	Astrida	k1gFnPc2
Lindgrenová	Lindgrenový	k2eAgFnSc1d1
</s>
<s>
50	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Evert	Everta	k1gFnPc2
Taube	Taub	k1gMnSc5
</s>
<s>
100	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Greta	Greta	k1gMnSc1
Garbo	Garba	k1gFnSc5
</s>
<s>
200	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Ingmar	Ingmar	k1gMnSc1
Bergman	Bergman	k1gMnSc1
</s>
<s>
500	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Birgit	Birgita	k1gFnPc2
Nilssonová	Nilssonová	k1gFnSc1
</s>
<s>
1000	#num#	k4
korun	koruna	k1gFnPc2
–	–	k?
Dag	dag	kA
Hammarskjöld	Hammarskjöld	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
-	-	kIx~
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Švédská	švédský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měnou	měna	k1gFnSc7
Švédska	Švédsko	k1gNnSc2
je	být	k5eAaImIp3nS
„	„	k?
<g/>
vyváženost	vyváženost	k1gFnSc1
a	a	k8xC
perspektiva	perspektiva	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
měny	měna	k1gFnSc2
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
И	И	k?
<g/>
:	:	kIx,
https://bitcoin-times.ru/cs/nacionalnaya-valyuta-shvecii-valyuta-shvecii-vzveshennost-i-perspektiva/	https://bitcoin-times.ru/cs/nacionalnaya-valyuta-shvecii-valyuta-shvecii-vzveshennost-i-perspektiva/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Valid	Valid	k1gInSc1
coins	coins	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sveriges	Sveriges	k1gMnSc1
Riksbank	Riksbank	k1gMnSc1
-	-	kIx~
Švédská	švédský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Valid	Valid	k1gInSc1
banksnotes	banksnotes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sveriges	Sveriges	k1gMnSc1
Riksbank	Riksbank	k1gMnSc1
-	-	kIx~
Švédská	švédský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Švédské	švédský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Švédsko	Švédsko	k1gNnSc1
</s>
