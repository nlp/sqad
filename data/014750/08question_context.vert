<s>
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
krona	kroen	k2eAgFnSc1d1
<g/>
,	,	kIx,
množné	množný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
kronor	kronor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
skandinávského	skandinávský	k2eAgInSc2d1
státu	stát	k1gInSc2
Švédsko	Švédsko	k1gNnSc1
<g/>
.	.	kIx.
</s>