<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
PřF	PřF	k1gFnSc2
MU	MU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
v	v	k7c6
biologických	biologický	k2eAgFnPc6d1
<g/>
,	,	kIx,
fyzikálních	fyzikální	k2eAgInPc2d1
<g/>
,	,	kIx,
chemických	chemický	k2eAgInPc2d1
<g/>
,	,	kIx,
matematických	matematický	k2eAgInPc2d1
<g/>
,	,	kIx,
geologických	geologický	k2eAgInPc2d1
a	a	k8xC
geografických	geografický	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
a	a	k8xC
ve	v	k7c6
vyučování	vyučování	k1gNnSc6
těmto	tento	k3xDgFnPc3
vědám	věda	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výuku	výuka	k1gFnSc4
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
zahájila	zahájit	k5eAaPmAgFnS
na	na	k7c4
podzim	podzim	k1gInSc4
1920	#num#	k4
<g/>
.	.	kIx.
</s>