<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
UIAA	UIAA	kA	UIAA
Climbing	Climbing	k1gInSc1	Climbing
World	World	k1gInSc1	World
Championship	Championship	k1gInSc4	Championship
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Championnats	Championnats	k1gInSc1	Championnats
du	du	k?	du
monde	mond	k1gInSc5	mond
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
escalade	escalad	k1gInSc5	escalad
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Campionato	Campionat	k2eAgNnSc1d1	Campionat
del	del	k?	del
mondo	mondo	k1gNnSc1	mondo
di	di	k?	di
arrampicata	arrampicat	k1gMnSc2	arrampicat
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Kletterweltmeisterschaft	Kletterweltmeisterschaft	k2eAgMnSc1d1	Kletterweltmeisterschaft
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
jako	jako	k9	jako
sedmý	sedmý	k4xOgInSc1	sedmý
ročník	ročník	k1gInSc1	ročník
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
UIAA	UIAA	kA	UIAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
závodilo	závodit	k5eAaImAgNnS	závodit
se	se	k3xPyFc4	se
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
také	také	k9	také
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
<g/>
.	.	kIx.	.
</s>
