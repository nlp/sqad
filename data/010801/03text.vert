<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
UIAA	UIAA	kA	UIAA
Climbing	Climbing	k1gInSc1	Climbing
World	World	k1gInSc1	World
Championship	Championship	k1gInSc4	Championship
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Championnats	Championnats	k1gInSc1	Championnats
du	du	k?	du
monde	mond	k1gInSc5	mond
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
escalade	escalad	k1gInSc5	escalad
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Campionato	Campionat	k2eAgNnSc1d1	Campionat
del	del	k?	del
mondo	mondo	k1gNnSc1	mondo
di	di	k?	di
arrampicata	arrampicat	k1gMnSc2	arrampicat
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Kletterweltmeisterschaft	Kletterweltmeisterschaft	k2eAgMnSc1d1	Kletterweltmeisterschaft
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
jako	jako	k9	jako
sedmý	sedmý	k4xOgInSc1	sedmý
ročník	ročník	k1gInSc1	ročník
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
UIAA	UIAA	kA	UIAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
závodilo	závodit	k5eAaImAgNnS	závodit
se	se	k3xPyFc4	se
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
také	také	k9	také
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
závodů	závod	k1gInPc2	závod
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Češi	Čech	k1gMnPc1	Čech
na	na	k7c6	na
MS	MS	kA	MS
===	===	k?	===
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Mrázek	Mrázek	k1gMnSc1	Mrázek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
(	(	kIx(	(
<g/>
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
zde	zde	k6eAd1	zde
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgFnSc4	třetí
českou	český	k2eAgFnSc4d1	Česká
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
Tereza	Tereza	k1gFnSc1	Tereza
Kysilková	Kysilková	k1gFnSc1	Kysilková
a	a	k8xC	a
Soňa	Soňa	k1gFnSc1	Soňa
Hnízdilová	Hnízdilová	k1gFnSc1	Hnízdilová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
rychlsot	rychlsot	k1gInSc4	rychlsot
jsme	být	k5eAaImIp1nP	být
zde	zde	k6eAd1	zde
neměli	mít	k5eNaImAgMnP	mít
reprezentanta	reprezentant	k1gMnSc4	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Kotasová-Kostruhová	Kotasová-Kostruhová	k1gFnSc1	Kotasová-Kostruhová
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
boulderingu	bouldering	k1gInSc6	bouldering
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
(	(	kIx(	(
<g/>
se	s	k7c7	s
šesti	šest	k4xCc7	šest
topy	topy	k1gInPc7	topy
na	na	k7c6	na
devět	devět	k4xCc4	devět
pokusů	pokus	k1gInPc2	pokus
postupovala	postupovat	k5eAaImAgFnS	postupovat
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
vynikajícím	vynikající	k2eAgNnSc6d1	vynikající
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výsledky	výsledek	k1gInPc1	výsledek
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čeští	český	k2eAgMnPc1d1	český
Mistři	mistr	k1gMnPc1	mistr
a	a	k8xC	a
medailisté	medailista	k1gMnPc1	medailista
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Medaile	medaile	k1gFnSc1	medaile
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zúčastněné	zúčastněný	k2eAgFnPc1d1	zúčastněná
země	zem	k1gFnPc1	zem
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
(	(	kIx(	(
<g/>
ME	ME	kA	ME
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
soutěžním	soutěžní	k2eAgNnSc6d1	soutěžní
lezení	lezení	k1gNnSc6	lezení
(	(	kIx(	(
<g/>
MČR	MČR	kA	MČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
horolezecký	horolezecký	k2eAgInSc1d1	horolezecký
svaz	svaz	k1gInSc1	svaz
–	–	k?	–
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
sport	sport	k1gInSc1	sport
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
závodů	závod	k1gInPc2	závod
IFSC	IFSC	kA	IFSC
</s>
</p>
