<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Slow	Slow	k1gMnSc1	Slow
Food	Food	k1gMnSc1	Food
založil	založit	k5eAaPmAgMnS	založit
Carlo	Carlo	k1gNnSc4	Carlo
Petrini	Petrin	k1gMnPc1	Petrin
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jako	jako	k8xC	jako
protiklad	protiklad	k1gInSc4	protiklad
fast	fasta	k1gFnPc2	fasta
foodu	food	k1gInSc2	food
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
hnutí	hnutí	k1gNnSc2	hnutí
je	být	k5eAaImIp3nS	být
naučit	naučit	k5eAaPmF	naučit
vážit	vážit	k5eAaImF	vážit
si	se	k3xPyFc3	se
kultury	kultura	k1gFnPc4	kultura
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
chránit	chránit	k5eAaImF	chránit
a	a	k8xC	a
vychutnávat	vychutnávat	k5eAaImF	vychutnávat
místní	místní	k2eAgInPc4d1	místní
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
odsouzeny	odsouzet	k5eAaImNgInP	odsouzet
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
standardizace	standardizace	k1gFnSc2	standardizace
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgFnSc2d1	rozvíjející
"	"	kIx"	"
<g/>
kultury	kultura	k1gFnSc2	kultura
<g/>
"	"	kIx"	"
Fast	Fast	k1gMnSc1	Fast
Food	Food	k1gMnSc1	Food
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
místní	místní	k2eAgInPc4d1	místní
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgFnPc4d1	domácí
odrůdy	odrůda	k1gFnPc4	odrůda
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
dávno	dávno	k6eAd1	dávno
zapomenuté	zapomenutý	k2eAgInPc4d1	zapomenutý
recepty	recept	k1gInPc4	recept
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
globálně	globálně	k6eAd1	globálně
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
disponuje	disponovat	k5eAaBmIp3nS	disponovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
členy	člen	k1gMnPc7	člen
ve	v	k7c6	v
132	[number]	k4	132
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Oxford	Oxford	k1gInSc1	Oxford
Companion	Companion	k1gInSc1	Companion
to	ten	k3xDgNnSc1	ten
Food	Food	k1gMnSc1	Food
<g/>
,	,	kIx,	,
Slow	Slow	k1gMnSc1	Slow
Food	Food	k1gMnSc1	Food
an	an	k?	an
Excerpt	excerpt	k1gInSc1	excerpt
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oxford	Oxford	k1gInSc1	Oxford
Companion	Companion	k1gInSc1	Companion
to	ten	k3xDgNnSc4	ten
Food	Food	k1gMnSc1	Food
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geoff	Geoff	k1gInSc1	Geoff
Andrews	Andrewsa	k1gFnPc2	Andrewsa
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Slow	Slow	k1gMnSc1	Slow
Food	Food	k1gMnSc1	Food
Story	story	k1gFnSc1	story
<g/>
:	:	kIx,	:
Politics	Politics	k1gInSc1	Politics
and	and	k?	and
Pleasure	Pleasur	k1gMnSc5	Pleasur
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Pluto	Pluto	k1gMnSc1	Pluto
Press	Press	k1gInSc1	Press
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Carlo	Carlo	k1gNnSc4	Carlo
Petrini	Petrin	k2eAgMnPc1d1	Petrin
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Slow	Slow	k1gFnSc1	Slow
Food	Food	k1gMnSc1	Food
Nation	Nation	k1gInSc1	Nation
<g/>
:	:	kIx,	:
Why	Why	k1gMnSc1	Why
Our	Our	k1gMnSc1	Our
Food	Food	k1gMnSc1	Food
Should	Should	k1gMnSc1	Should
Be	Be	k1gMnSc1	Be
Good	Good	k1gMnSc1	Good
<g/>
,	,	kIx,	,
Clean	Clean	k1gMnSc1	Clean
<g/>
,	,	kIx,	,
and	and	k?	and
Fair	fair	k2eAgMnSc2d1	fair
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Rizzoli	Rizzole	k1gFnSc6	Rizzole
International	International	k1gFnPc2	International
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Carlo	Carlo	k1gNnSc4	Carlo
Petrini	Petrin	k2eAgMnPc1d1	Petrin
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Slow	Slow	k1gFnSc1	Slow
Food	Food	k1gMnSc1	Food
Revolution	Revolution	k1gInSc1	Revolution
<g/>
:	:	kIx,	:
A	a	k9	a
New	New	k1gFnSc1	New
Culture	Cultur	k1gMnSc5	Cultur
for	forum	k1gNnPc2	forum
Dining	Dining	k1gInSc1	Dining
and	and	k?	and
Living	Living	k1gInSc1	Living
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Rizzoli	Rizzole	k1gFnSc6	Rizzole
International	International	k1gFnPc2	International
Publications	Publicationsa	k1gFnPc2	Publicationsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
Odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slow	Slow	k?	Slow
Food	Food	k1gInSc4	Food
oficiální	oficiální	k2eAgInSc4d1	oficiální
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slow	Slow	k?	Slow
Food	Food	k1gInSc4	Food
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
pražského	pražský	k2eAgNnSc2d1	Pražské
konvivia	konvivium	k1gNnSc2	konvivium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slow	Slow	k?	Slow
Food	Food	k1gInSc4	Food
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
pro	pro	k7c4	pro
konvivia	konvivium	k1gNnPc4	konvivium
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
