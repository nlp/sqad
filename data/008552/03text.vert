<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
Be	Be	k1gFnSc1	Be
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
<g/>
Cr	cr	k0	cr
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
18	[number]	k4	18
(	(	kIx(	(
<g/>
hlinitokřemičitan	hlinitokřemičitan	k1gInSc1	hlinitokřemičitan
berylnatý	berylnatý	k2eAgInSc1d1	berylnatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šesterečný	šesterečný	k2eAgInSc4d1	šesterečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
semitského	semitský	k2eAgInSc2d1	semitský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc7d3	nejznámější
a	a	k8xC	a
nejcennější	cenný	k2eAgFnSc7d3	nejcennější
odrůdou	odrůda	k1gFnSc7	odrůda
minerálu	minerál	k1gInSc2	minerál
berylu	beryl	k1gInSc2	beryl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
je	být	k5eAaImIp3nS	být
nejdražší	drahý	k2eAgFnSc1d3	nejdražší
odrůda	odrůda	k1gFnSc1	odrůda
berylu	beryl	k1gInSc2	beryl
s	s	k7c7	s
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
sytě	sytě	k6eAd1	sytě
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
obsahu	obsah	k1gInSc2	obsah
chrómu	chróm	k1gInSc2	chróm
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
7,5	[number]	k4	7,5
–	–	k?	–
8	[number]	k4	8
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,6	[number]	k4	2,6
–	–	k?	–
2,8	[number]	k4	2,8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
,	,	kIx,	,
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k8xS	až
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Be	Be	k1gFnSc1	Be
5,03	[number]	k4	5,03
%	%	kIx~	%
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
10,04	[number]	k4	10,04
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
31,35	[number]	k4	31,35
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
53,58	[number]	k4	53,58
%	%	kIx~	%
s	s	k7c7	s
příměs	příměs	k1gFnSc4	příměs
Cr	cr	k0	cr
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
HF	HF	kA	HF
<g/>
,	,	kIx,	,
před	před	k7c7	před
dmuchavkou	dmuchavka	k1gFnSc7	dmuchavka
se	se	k3xPyFc4	se
netaví	tavit	k5eNaImIp3nS	tavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
fasetové	fasetový	k2eAgInPc1d1	fasetový
brusy	brus	k1gInPc1	brus
<g/>
,	,	kIx,	,
kabošony	kabošon	k1gInPc1	kabošon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgInPc4d3	nejslavnější
šperky	šperk	k1gInPc4	šperk
se	s	k7c7	s
smaragdy	smaragd	k1gInPc7	smaragd
patří	patřit	k5eAaImIp3nS	patřit
Náhrdelník	náhrdelník	k1gInSc4	náhrdelník
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
Hookerova	Hookerův	k2eAgFnSc1d1	Hookerova
smaragdová	smaragdový	k2eAgFnSc1d1	Smaragdová
brož	brož	k1gFnSc1	brož
nebo	nebo	k8xC	nebo
Andská	andský	k2eAgFnSc1d1	andská
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
(	(	kIx(	(
<g/>
smaragdos	smaragdos	k1gInSc1	smaragdos
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
užíván	užívat	k5eAaImNgInS	užívat
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
kameny	kámen	k1gInPc4	kámen
již	již	k6eAd1	již
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
za	za	k7c2	za
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
a	a	k8xC	a
za	za	k7c2	za
královny	královna	k1gFnSc2	královna
Kleopatry	Kleopatra	k1gFnSc2	Kleopatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
Smaragd	smaragd	k1gInSc1	smaragd
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
marakata	marakata	k1gFnSc1	marakata
<g/>
,	,	kIx,	,
překonavatel	překonavatel	k1gMnSc1	překonavatel
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
dělili	dělit	k5eAaImAgMnP	dělit
své	svůj	k3xOyFgInPc4	svůj
smaragdy	smaragd	k1gInPc4	smaragd
z	z	k7c2	z
Palistánu	Palistán	k1gInSc2	Palistán
(	(	kIx(	(
<g/>
údolí	údolí	k1gNnPc1	údolí
Swát	Swáta	k1gFnPc2	Swáta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rádžástánu	Rádžástán	k2eAgFnSc4d1	Rádžástán
na	na	k7c4	na
odstíny	odstín	k1gInPc4	odstín
podle	podle	k7c2	podle
kast	kasta	k1gFnPc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
starověké	starověký	k2eAgMnPc4d1	starověký
myslitele	myslitel	k1gMnPc4	myslitel
kamenem	kámen	k1gInSc7	kámen
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
praktické	praktický	k2eAgFnSc2d1	praktická
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc2d1	obchodní
zdatnosti	zdatnost	k1gFnSc2	zdatnost
(	(	kIx(	(
<g/>
budha-ratna	budhaatna	k6eAd1	budha-ratna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jadeit	jadeit	k1gInSc1	jadeit
<g/>
,	,	kIx,	,
nefrit	nefrit	k1gInSc1	nefrit
a	a	k8xC	a
smaragd	smaragd	k1gInSc1	smaragd
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
byly	být	k5eAaImAgFnP	být
vyřezávány	vyřezáván	k2eAgFnPc4d1	vyřezávána
sošky	soška	k1gFnPc4	soška
Buddhy	Buddha	k1gMnSc2	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
perské	perský	k2eAgMnPc4d1	perský
učence	učenec	k1gMnPc4	učenec
byl	být	k5eAaImAgInS	být
smaragd	smaragd	k1gInSc1	smaragd
kamenem	kámen	k1gInSc7	kámen
věčného	věčný	k2eAgInSc2d1	věčný
života	život	k1gInSc2	život
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Římané	Říman	k1gMnPc1	Říman
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
cenili	cenit	k5eAaImAgMnP	cenit
smaragd	smaragd	k1gInSc4	smaragd
<g/>
,	,	kIx,	,
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
jeho	jeho	k3xOp3gFnSc4	jeho
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
smaragdu	smaragd	k1gInSc2	smaragd
je	být	k5eAaImIp3nS	být
řídký	řídký	k2eAgInSc1d1	řídký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
Habachtal	Habachtal	k1gMnSc1	Habachtal
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Takovaja	Takovaj	k1gInSc2	Takovaj
(	(	kIx(	(
<g/>
Ural	Ural	k1gInSc1	Ural
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Egypt	Egypt	k1gInSc1	Egypt
–	–	k?	–
Zabarah	Zabarah	k1gInSc1	Zabarah
(	(	kIx(	(
<g/>
další	další	k2eAgFnPc1d1	další
historické	historický	k2eAgFnPc1d1	historická
lokality	lokalita	k1gFnPc1	lokalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
–	–	k?	–
Muso	Musa	k1gFnSc5	Musa
a	a	k8xC	a
Chivor	Chivora	k1gFnPc2	Chivora
–	–	k?	–
nejznámější	známý	k2eAgFnSc2d3	nejznámější
lokality	lokalita	k1gFnSc2	lokalita
vysoce	vysoce	k6eAd1	vysoce
ceněných	ceněný	k2eAgInPc2d1	ceněný
smaragdů	smaragd	k1gInPc2	smaragd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
staří	starý	k2eAgMnPc1d1	starý
Inkové	Ink	k1gMnPc1	Ink
získávali	získávat	k5eAaImAgMnP	získávat
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
exempláře	exemplář	k1gInPc4	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
vývozce	vývozce	k1gMnSc1	vývozce
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
-19	-19	k4	-19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
španělských	španělský	k2eAgMnPc2d1	španělský
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
Carnaiba	Carnaiba	k1gMnSc1	Carnaiba
<g/>
,	,	kIx,	,
Minas	Minas	k1gMnSc1	Minas
Gerais	Gerais	k1gFnSc1	Gerais
<g/>
,	,	kIx,	,
Bahía	Bahía	k1gFnSc1	Bahía
<g/>
,	,	kIx,	,
Ceará	Cearý	k2eAgFnSc1d1	Ceará
<g/>
,	,	kIx,	,
Goiás	Goiás	k1gInSc1	Goiás
</s>
</p>
<p>
<s>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
Hiddenit	Hiddenit	k1gFnSc1	Hiddenit
Mine	minout	k5eAaImIp3nS	minout
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
:	:	kIx,	:
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
,	,	kIx,	,
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnSc1	Zambie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
smaragdy	smaragd	k1gInPc1	smaragd
==	==	k?	==
</s>
</p>
<p>
<s>
Kokovinův	Kokovinův	k2eAgInSc1d1	Kokovinův
smaragd	smaragd	k1gInSc1	smaragd
-	-	kIx~	-
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
karátů	karát	k1gInPc2	karát
</s>
</p>
<p>
<s>
Váza	váza	k1gFnSc1	váza
vybroušená	vybroušený	k2eAgFnSc1d1	vybroušená
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
smaragdu	smaragd	k1gInSc2	smaragd
-	-	kIx~	-
2681	[number]	k4	2681
karátů	karát	k1gInPc2	karát
(	(	kIx(	(
<g/>
Kunsthistorisches	Kunsthistorisches	k1gInSc1	Kunsthistorisches
Museum	museum	k1gNnSc1	museum
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hrdost	hrdost	k1gFnSc1	hrdost
Ameriky	Amerika	k1gFnSc2	Amerika
-	-	kIx~	-
1470	[number]	k4	1470
karátů	karát	k1gInPc2	karát
</s>
</p>
<p>
<s>
Stephensonův	Stephensonův	k2eAgInSc1d1	Stephensonův
smaragd	smaragd	k1gInSc1	smaragd
-	-	kIx~	-
1438	[number]	k4	1438
karátů	karát	k1gInPc2	karát
</s>
</p>
<p>
<s>
Stalen	Stalen	k2eAgInSc1d1	Stalen
smaragd	smaragd	k1gInSc1	smaragd
-	-	kIx~	-
1270	[number]	k4	1270
karátů	karát	k1gInPc2	karát
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Smaragdová	smaragdový	k2eAgFnSc1d1	Smaragdová
souprava	souprava	k1gFnSc1	souprava
klenotů	klenot	k1gInPc2	klenot
ruské	ruský	k2eAgFnSc2d1	ruská
carevny	carevna	k1gFnSc2	carevna
-	-	kIx~	-
Almaznyj	Almaznyj	k1gFnSc1	Almaznyj
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Maura	Maur	k1gMnSc2	Maur
nesoucího	nesoucí	k2eAgInSc2d1	nesoucí
podnos	podnos	k1gInSc1	podnos
se	s	k7c7	s
smaragdy	smaragd	k1gInPc7	smaragd
-	-	kIx~	-
Grünes	Grünes	k1gMnSc1	Grünes
Gewölbe	Gewölb	k1gInSc5	Gewölb
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
</s>
</p>
<p>
<s>
Smaragdový	smaragdový	k2eAgInSc1d1	smaragdový
řád	řád	k1gInSc1	řád
saského	saský	k2eAgMnSc2d1	saský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
-	-	kIx~	-
Grünes	Grünes	k1gMnSc1	Grünes
Gewölbe	Gewölb	k1gInSc5	Gewölb
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc7	Drážďany
</s>
</p>
<p>
<s>
Smaragdy	smaragd	k1gInPc1	smaragd
na	na	k7c6	na
klenotech	klenot	k1gInPc6	klenot
Svatohorské	svatohorský	k2eAgFnSc2d1	Svatohorská
madony	madona	k1gFnSc2	madona
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Hora	hora	k1gFnSc1	hora
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
</s>
</p>
<p>
<s>
Smaragdová	smaragdový	k2eAgFnSc1d1	Smaragdová
souprava	souprava	k1gFnSc1	souprava
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1	Uměleckoprůmyslové
museum	museum	k1gNnSc1	museum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Smaragdová	smaragdový	k2eAgFnSc1d1	Smaragdová
slánka	slánka	k1gFnSc1	slánka
pro	pro	k7c4	pro
císaře	císař	k1gMnSc4	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Dionýsio	Dionýsio	k6eAd1	Dionýsio
Miseroni	Miseroň	k1gFnSc3	Miseroň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1641	[number]	k4	1641
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Kunsthstorisches	Kunsthstorisches	k1gMnSc1	Kunsthstorisches
Museum	museum	k1gNnSc1	museum
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Grundmann	Grundmann	k1gMnSc1	Grundmann
<g/>
,	,	kIx,	,
Smaragd	smaragd	k1gInSc1	smaragd
<g/>
,	,	kIx,	,
grünes	grünes	k1gMnSc1	grünes
feuer	feuer	k1gMnSc1	feuer
unterm	unterm	k1gInSc4	unterm
Eis	eis	k1gNnSc2	eis
<g/>
.	.	kIx.	.
</s>
<s>
ExtraLAPIS	ExtraLAPIS	k?	ExtraLAPIS
Nr	Nr	k1gFnSc1	Nr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Onyx	onyx	k1gInSc1	onyx
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Smaragde	smaragd	k1gInSc5	smaragd
der	drát	k5eAaImRp2nS	drát
Welt	Welt	k1gMnSc1	Welt
<g/>
,	,	kIx,	,
ExtraLAPIS	ExtraLAPIS	k1gMnSc1	ExtraLAPIS
Nr	Nr	k1gMnSc1	Nr
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Ďuďa	Ďuďa	k1gMnSc1	Ďuďa
-	-	kIx~	-
Luboš	Luboš	k1gMnSc1	Luboš
Rejl	Rejl	k1gMnSc1	Rejl
<g/>
:	:	kIx,	:
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Aventinum	Aventinum	k1gInSc1	Aventinum
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
s.	s.	k?	s.
70	[number]	k4	70
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
smaragd	smaragd	k1gInSc1	smaragd
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
smaragd	smaragd	k1gInSc1	smaragd
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaImF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smaragd	smaragd	k1gInSc1	smaragd
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
