<s>
Pes	pes	k1gMnSc1	pes
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
f.	f.	k?	f.
familiaris	familiaris	k1gInSc1	familiaris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
šelma	šelma	k1gFnSc1	šelma
a	a	k8xC	a
nejstarší	starý	k2eAgNnSc1d3	nejstarší
domestikované	domestikovaný	k2eAgNnSc1d1	domestikované
zvíře	zvíře	k1gNnSc1	zvíře
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
provázející	provázející	k2eAgFnSc1d1	provázející
člověka	člověk	k1gMnSc2	člověk
minimálně	minimálně	k6eAd1	minimálně
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zdomácnělého	zdomácnělý	k2eAgMnSc4d1	zdomácnělý
a	a	k8xC	a
umělým	umělý	k2eAgInSc7d1	umělý
výběrem	výběr	k1gInSc7	výběr
změněného	změněný	k2eAgMnSc2d1	změněný
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
populace	populace	k1gFnSc1	populace
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
toulavých	toulavý	k2eAgMnPc2d1	toulavý
a	a	k8xC	a
opuštěných	opuštěný	k2eAgMnPc2d1	opuštěný
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
370	[number]	k4	370
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
600	[number]	k4	600
<g/>
million	million	k1gInSc1	million
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc4	počet
jen	jen	k9	jen
toulavých	toulavý	k2eAgMnPc2d1	toulavý
psů	pes	k1gMnPc2	pes
na	na	k7c6	na
světě	svět	k1gInSc6	svět
právě	právě	k9	právě
na	na	k7c4	na
600	[number]	k4	600
miliónů	milión	k4xCgInPc2	milión
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
psa	pes	k1gMnSc2	pes
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
člověku	člověk	k1gMnSc6	člověk
je	on	k3xPp3gMnPc4	on
pomocníkem	pomocník	k1gMnSc7	pomocník
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
nebo	nebo	k8xC	nebo
při	při	k7c6	při
přehánění	přehánění	k1gNnSc6	přehánění
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
funkci	funkce	k1gFnSc4	funkce
strážce	strážce	k1gMnSc1	strážce
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
svého	svůj	k1gMnSc2	svůj
majitele	majitel	k1gMnSc2	majitel
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
domácích	domácí	k1gMnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tažný	tažný	k2eAgInSc1d1	tažný
nebo	nebo	k8xC	nebo
saňový	saňový	k2eAgMnSc1d1	saňový
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
cvičen	cvičit	k5eAaImNgInS	cvičit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
složkách	složka	k1gFnPc6	složka
či	či	k8xC	či
k	k	k7c3	k
asistenci	asistence	k1gFnSc3	asistence
hendikepovaným	hendikepovaný	k2eAgFnPc3d1	hendikepovaná
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
je	být	k5eAaImIp3nS	být
nezastupitelná	zastupitelný	k2eNgFnSc1d1	nezastupitelná
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
jako	jako	k9	jako
společníka	společník	k1gMnSc4	společník
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
sloužil	sloužit	k5eAaImAgMnS	sloužit
pes	pes	k1gMnSc1	pes
i	i	k9	i
jako	jako	k9	jako
potravinové	potravinový	k2eAgNnSc4d1	potravinové
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
psí	psí	k2eAgNnSc4d1	psí
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitým	důležitý	k2eAgNnSc7d1	důležité
laboratorním	laboratorní	k2eAgNnSc7d1	laboratorní
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
domorodých	domorodý	k2eAgFnPc2d1	domorodá
vesnic	vesnice	k1gFnPc2	vesnice
psi	pes	k1gMnPc1	pes
obecně	obecně	k6eAd1	obecně
nazývaní	nazývaný	k2eAgMnPc1d1	nazývaný
páriové	pária	k1gMnPc1	pária
jen	jen	k9	jen
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
dingo	dingo	k1gMnSc1	dingo
je	být	k5eAaImIp3nS	být
zdivočelý	zdivočelý	k2eAgMnSc1d1	zdivočelý
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
i	i	k9	i
jejich	jejich	k3xOp3gMnPc4	jejich
chovatele	chovatel	k1gMnPc4	chovatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gFnPc4	on
chovají	chovat	k5eAaImIp3nP	chovat
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
obyčejné	obyčejný	k2eAgMnPc4d1	obyčejný
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
bývají	bývat	k5eAaImIp3nP	bývat
spíše	spíše	k9	spíše
kritizování	kritizování	k1gNnSc4	kritizování
<g/>
.	.	kIx.	.
</s>
<s>
Zdivočelí	zdivočelý	k2eAgMnPc1d1	zdivočelý
a	a	k8xC	a
toulaví	toulavý	k2eAgMnPc1d1	toulavý
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
hygienickým	hygienický	k2eAgInSc7d1	hygienický
problémem	problém	k1gInSc7	problém
mnoha	mnoho	k4c2	mnoho
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
zdivočelý	zdivočelý	k2eAgMnSc1d1	zdivočelý
pes	pes	k1gMnSc1	pes
považovaný	považovaný	k2eAgInSc4d1	považovaný
za	za	k7c4	za
škodnou	škodná	k1gFnSc4	škodná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
rezervoárem	rezervoár	k1gInSc7	rezervoár
vztekliny	vzteklina	k1gFnSc2	vzteklina
a	a	k8xC	a
v	v	k7c6	v
99	[number]	k4	99
%	%	kIx~	%
případů	případ	k1gInPc2	případ
vztekliny	vzteklina	k1gFnSc2	vzteklina
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
nákaza	nákaza	k1gFnSc1	nákaza
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
od	od	k7c2	od
toulavých	toulavý	k2eAgMnPc2d1	toulavý
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
chovány	chovat	k5eAaImNgFnP	chovat
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
upravován	upravovat	k5eAaImNgInS	upravovat
zákonem	zákon	k1gInSc7	zákon
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
proti	proti	k7c3	proti
týrání	týrání	k1gNnSc3	týrání
<g/>
,	,	kIx,	,
veterinárním	veterinární	k2eAgMnSc7d1	veterinární
a	a	k8xC	a
mysliveckým	myslivecký	k2eAgInSc7d1	myslivecký
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc1d1	místní
vyhlášky	vyhláška	k1gFnPc1	vyhláška
pak	pak	k6eAd1	pak
upravují	upravovat	k5eAaImIp3nP	upravovat
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
podmínky	podmínka	k1gFnPc4	podmínka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
držení	držení	k1gNnSc2	držení
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc2	jejich
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
prostranstvích	prostranství	k1gNnPc6	prostranství
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
ze	z	k7c2	z
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Celostátní	celostátní	k2eAgFnSc1d1	celostátní
evidence	evidence	k1gFnSc1	evidence
psů	pes	k1gMnPc2	pes
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
evidován	evidován	k2eAgInSc1d1	evidován
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc1	počet
lovecky	lovecky	k6eAd1	lovecky
upotřebitelných	upotřebitelný	k2eAgMnPc2d1	upotřebitelný
psů	pes	k1gMnPc2	pes
podle	podle	k7c2	podle
mysliveckého	myslivecký	k2eAgInSc2d1	myslivecký
zákona	zákon	k1gInSc2	zákon
–	–	k?	–
30	[number]	k4	30
624	[number]	k4	624
kusů	kus	k1gInPc2	kus
k	k	k7c3	k
31.12	[number]	k4	31.12
<g/>
.2006	.2006	k4	.2006
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
plemenné	plemenný	k2eAgFnPc4d1	plemenná
knihy	kniha	k1gFnPc4	kniha
evidují	evidovat	k5eAaImIp3nP	evidovat
počty	počet	k1gInPc1	počet
zapsaných	zapsaný	k2eAgNnPc2d1	zapsané
štěňat	štěně	k1gNnPc2	štěně
s	s	k7c7	s
průkazem	průkaz	k1gInSc7	průkaz
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
soužití	soužití	k1gNnSc2	soužití
psa	pes	k1gMnSc2	pes
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
bylo	být	k5eAaImAgNnS	být
vyšlechtěno	vyšlechtěn	k2eAgNnSc1d1	vyšlechtěno
nespočet	nespočet	k1gInSc1	nespočet
plemen	plemeno	k1gNnPc2	plemeno
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
proporcí	proporce	k1gFnPc2	proporce
<g/>
,	,	kIx,	,
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
srsti	srst	k1gFnSc2	srst
i	i	k8xC	i
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
uznává	uznávat	k5eAaImIp3nS	uznávat
343	[number]	k4	343
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
uznáno	uznán	k2eAgNnSc1d1	uznáno
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kynologie	kynologie	k1gFnSc1	kynologie
a	a	k8xC	a
chov	chov	k1gInSc1	chov
psů	pes	k1gMnPc2	pes
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zaštítěny	zaštítěn	k2eAgInPc1d1	zaštítěn
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Česko	Česko	k1gNnSc4	Česko
v	v	k7c6	v
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
kynologické	kynologický	k2eAgFnSc6d1	kynologická
federaci	federace	k1gFnSc6	federace
FCI	FCI	kA	FCI
<g/>
,	,	kIx,	,
a	a	k8xC	a
minoritně	minoritně	k6eAd1	minoritně
též	též	k9	též
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
federací	federace	k1gFnSc7	federace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
United	United	k1gInSc4	United
Kennel	Kennel	k1gInSc1	Kennel
Clubs	Clubsa	k1gFnPc2	Clubsa
international	internationat	k5eAaPmAgInS	internationat
(	(	kIx(	(
<g/>
UCI	UCI	kA	UCI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgMnSc1d3	nejbližší
příbuzný	příbuzný	k1gMnSc1	příbuzný
domácího	domácí	k2eAgMnSc2d1	domácí
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
–	–	k?	–
od	od	k7c2	od
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
nanejvýš	nanejvýš	k6eAd1	nanejvýš
0,2	[number]	k4	0,2
%	%	kIx~	%
sekvence	sekvence	k1gFnSc1	sekvence
mtDNA	mtDNA	k?	mtDNA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vlka	vlk	k1gMnSc2	vlk
se	se	k3xPyFc4	se
ale	ale	k9	ale
pes	pes	k1gMnSc1	pes
liší	lišit	k5eAaImIp3nS	lišit
morfologicky	morfologicky	k6eAd1	morfologicky
–	–	k?	–
psi	pes	k1gMnPc1	pes
mají	mít	k5eAaImIp3nP	mít
kratší	krátký	k2eAgInSc4d2	kratší
a	a	k8xC	a
širší	široký	k2eAgInSc4d2	širší
čenich	čenich	k1gInSc4	čenich
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
širší	široký	k2eAgFnSc4d2	širší
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
dopředu	dopředu	k6eAd1	dopředu
postavené	postavený	k2eAgNnSc4d1	postavené
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
robustní	robustní	k2eAgInPc4d1	robustní
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
plošší	plochý	k2eAgFnPc4d2	plošší
bubínkové	bubínkový	k2eAgFnPc4d1	bubínková
výdutě	výduť	k1gFnPc4	výduť
(	(	kIx(	(
<g/>
bullae	bullae	k6eAd1	bullae
tympanicae	tympanicae	k6eAd1	tympanicae
<g/>
)	)	kIx)	)
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
morfologické	morfologický	k2eAgInPc1d1	morfologický
znaky	znak	k1gInPc1	znak
včetně	včetně	k7c2	včetně
povrchové	povrchový	k2eAgFnSc2d1	povrchová
struktury	struktura	k1gFnSc2	struktura
mozku	mozek	k1gInSc2	mozek
jsou	být	k5eAaImIp3nP	být
vlastní	vlastní	k2eAgFnPc4d1	vlastní
též	též	k6eAd1	též
šakalům	šakal	k1gMnPc3	šakal
a	a	k8xC	a
kojotům	kojot	k1gMnPc3	kojot
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
podobnost	podobnost	k1gFnSc1	podobnost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
teoriím	teorie	k1gFnPc3	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
psa	pes	k1gMnSc2	pes
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
šelem	šelma	k1gFnPc2	šelma
nebo	nebo	k8xC	nebo
o	o	k7c6	o
polyfyletickém	polyfyletický	k2eAgInSc6d1	polyfyletický
původu	původ	k1gInSc6	původ
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkem	předek	k1gInSc7	předek
psa	pes	k1gMnSc2	pes
jsou	být	k5eAaImIp3nP	být
kříženci	kříženec	k1gMnPc1	kříženec
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
genetické	genetický	k2eAgInPc1d1	genetický
testy	test	k1gInPc1	test
ale	ale	k8xC	ale
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
spřízněnost	spřízněnost	k1gFnSc4	spřízněnost
psa	pes	k1gMnSc2	pes
s	s	k7c7	s
šakalem	šakal	k1gMnSc7	šakal
<g/>
,	,	kIx,	,
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadá	připadat	k5eAaImIp3nS	připadat
jen	jen	k9	jen
přikřížení	přikřížení	k1gNnSc1	přikřížení
vlčka	vlček	k1gMnSc2	vlček
etiopského	etiopský	k2eAgMnSc2d1	etiopský
nebo	nebo	k8xC	nebo
dhoula	dhoul	k1gMnSc2	dhoul
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc1	žádný
přímé	přímý	k2eAgInPc1d1	přímý
důkazy	důkaz	k1gInPc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
analýzy	analýza	k1gFnPc1	analýza
mtDNA	mtDNA	k?	mtDNA
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkové	předek	k1gMnPc1	předek
psích	psí	k2eAgNnPc2d1	psí
plemen	plemeno	k1gNnPc2	plemeno
se	se	k3xPyFc4	se
od	od	k7c2	od
vlka	vlk	k1gMnSc2	vlk
oddělily	oddělit	k5eAaPmAgFnP	oddělit
asi	asi	k9	asi
před	před	k7c7	před
135	[number]	k4	135
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
výzkumy	výzkum	k1gInPc4	výzkum
toto	tento	k3xDgNnSc1	tento
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
76	[number]	k4	76
<g/>
–	–	k?	–
<g/>
121	[number]	k4	121
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výzkumy	výzkum	k1gInPc1	výzkum
odhalily	odhalit	k5eAaPmAgInP	odhalit
velkou	velký	k2eAgFnSc4d1	velká
různorodost	různorodost	k1gFnSc4	různorodost
genů	gen	k1gInPc2	gen
místních	místní	k2eAgMnPc2d1	místní
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
na	na	k7c4	na
centrum	centrum	k1gNnSc4	centrum
domestikace	domestikace	k1gFnSc2	domestikace
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
analýza	analýza	k1gFnSc1	analýza
polymorfismu	polymorfismus	k1gInSc2	polymorfismus
mtDNA	mtDNA	k?	mtDNA
provedená	provedený	k2eAgFnSc1d1	provedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc4	výsledek
tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
podobnou	podobný	k2eAgFnSc4d1	podobná
diversitu	diversit	k1gInSc6	diversit
i	i	k9	i
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Afrických	africký	k2eAgFnPc2d1	africká
vesnic	vesnice	k1gFnPc2	vesnice
i	i	k9	i
v	v	k7c6	v
Portoriku	Portorico	k1gNnSc6	Portorico
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
analyzovala	analyzovat	k5eAaImAgFnS	analyzovat
již	již	k6eAd1	již
celý	celý	k2eAgInSc4d1	celý
mitochondriální	mitochondriální	k2eAgInSc4d1	mitochondriální
genom	genom	k1gInSc4	genom
a	a	k8xC	a
ne	ne	k9	ne
jen	jen	k9	jen
vybrané	vybraný	k2eAgInPc4d1	vybraný
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
prokázala	prokázat	k5eAaPmAgFnS	prokázat
největší	veliký	k2eAgFnSc1d3	veliký
diversitu	diversit	k1gInSc2	diversit
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
jako	jako	k9	jako
centrum	centrum	k1gNnSc4	centrum
domestikace	domestikace	k1gFnSc2	domestikace
psa	pes	k1gMnSc2	pes
Jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Asii	Asie	k1gFnSc4	Asie
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
počet	počet	k1gInSc1	počet
předků	předek	k1gInPc2	předek
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
nejméně	málo	k6eAd3	málo
51	[number]	k4	51
vlčic	vlčice	k1gFnPc2	vlčice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
žily	žít	k5eAaImAgFnP	žít
před	před	k7c7	před
5400-16	[number]	k4	5400-16
300	[number]	k4	300
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c4	v
dané	daný	k2eAgFnPc4d1	daná
oblasti	oblast	k1gFnPc4	oblast
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
rýži	rýže	k1gFnSc4	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
jihoasijském	jihoasijský	k2eAgInSc6d1	jihoasijský
původu	původ	k1gInSc6	původ
psa	pes	k1gMnSc4	pes
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
analýzy	analýza	k1gFnPc1	analýza
chromozomu	chromozom	k1gInSc2	chromozom
Y	Y	kA	Y
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc2	který
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nalezena	nalezen	k2eAgFnSc1d1	nalezena
největší	veliký	k2eAgFnSc1d3	veliký
diversita	diversita	k1gFnSc1	diversita
právě	právě	k9	právě
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
v	v	k7c6	v
Jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
výzkumů	výzkum	k1gInPc2	výzkum
pochází	pocházet	k5eAaImIp3nS	pocházet
pes	pes	k1gMnSc1	pes
z	z	k7c2	z
13-24	[number]	k4	13-24
vlků-zakladatelů	vlkůakladatel	k1gMnPc2	vlků-zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
přikřižování	přikřižování	k1gNnSc4	přikřižování
vlka	vlk	k1gMnSc2	vlk
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
v	v	k7c6	v
genetické	genetický	k2eAgFnSc6d1	genetická
výbavě	výbava	k1gFnSc6	výbava
současného	současný	k2eAgMnSc2d1	současný
psa	pes	k1gMnSc2	pes
příliš	příliš	k6eAd1	příliš
neprojevilo	projevit	k5eNaPmAgNnS	projevit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
určitě	určitě	k6eAd1	určitě
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgInPc1d1	jediný
významnější	významný	k2eAgInPc1d2	významnější
důkazy	důkaz	k1gInPc1	důkaz
místního	místní	k2eAgNnSc2d1	místní
křížení	křížení	k1gNnSc2	křížení
psa	pes	k1gMnSc2	pes
s	s	k7c7	s
vlkem	vlk	k1gMnSc7	vlk
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
(	(	kIx(	(
<g/>
regionální	regionální	k2eAgFnSc1d1	regionální
haploskupina	haploskupina	k1gFnSc1	haploskupina
mtDNA	mtDNA	k?	mtDNA
d	d	k?	d
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
regionální	regionální	k2eAgFnSc1d1	regionální
haploskupina	haploskupina	k1gFnSc1	haploskupina
d	d	k?	d
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
někteří	některý	k3yIgMnPc1	některý
psi	pes	k1gMnPc1	pes
původních	původní	k2eAgNnPc2d1	původní
japonských	japonský	k2eAgNnPc2d1	Japonské
plemen	plemeno	k1gNnPc2	plemeno
nesou	nést	k5eAaImIp3nP	nést
mtDNA	mtDNA	k?	mtDNA
již	již	k6eAd1	již
vyhynulého	vyhynulý	k2eAgMnSc2d1	vyhynulý
vlka	vlk	k1gMnSc2	vlk
japonského	japonský	k2eAgMnSc2d1	japonský
(	(	kIx(	(
<g/>
C.	C.	kA	C.
lupus	lupus	k1gInSc1	lupus
hodophilax	hodophilax	k1gInSc1	hodophilax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
genů	gen	k1gInPc2	gen
původních	původní	k2eAgNnPc2d1	původní
amerických	americký	k2eAgNnPc2d1	americké
plemen	plemeno	k1gNnPc2	plemeno
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
teorii	teorie	k1gFnSc4	teorie
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
domestikace	domestikace	k1gFnSc2	domestikace
vlka	vlk	k1gMnSc2	vlk
i	i	k9	i
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mexický	mexický	k2eAgInSc1d1	mexický
naháč	naháč	k1gInSc1	naháč
je	být	k5eAaImIp3nS	být
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
ostatním	ostatní	k2eAgMnPc3d1	ostatní
psům	pes	k1gMnPc3	pes
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
severoamerickým	severoamerický	k2eAgMnSc7d1	severoamerický
vlkům	vlk	k1gMnPc3	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Kosterní	kosterní	k2eAgInPc1d1	kosterní
nálezy	nález	k1gInPc1	nález
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
studií	studie	k1gFnPc2	studie
DNA	DNA	kA	DNA
poněkud	poněkud	k6eAd1	poněkud
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
<g/>
:	:	kIx,	:
nejstarší	starý	k2eAgFnSc1d3	nejstarší
nalezená	nalezený	k2eAgFnSc1d1	nalezená
kostra	kostra	k1gFnSc1	kostra
přisuzovaná	přisuzovaný	k2eAgFnSc1d1	přisuzovaná
psovi	pes	k1gMnSc3	pes
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
33	[number]	k4	33
000	[number]	k4	000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ruského	ruský	k2eAgNnSc2d1	ruské
pohoří	pohoří	k1gNnSc2	pohoří
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
lebka	lebka	k1gFnSc1	lebka
psa	pes	k1gMnSc2	pes
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
Goyet	Goyeta	k1gFnPc2	Goyeta
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
31	[number]	k4	31
700	[number]	k4	700
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
lebky	lebka	k1gFnPc4	lebka
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
nalezené	nalezený	k2eAgFnPc4d1	nalezená
v	v	k7c6	v
Předmostí	předmostí	k1gNnSc6	předmostí
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
s	s	k7c7	s
mamutí	mamutí	k2eAgFnSc7d1	mamutí
kostí	kost	k1gFnSc7	kost
v	v	k7c6	v
tlamě	tlama	k1gFnSc6	tlama
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
mladého	mladý	k2eAgInSc2d1	mladý
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
,	,	kIx,	,
asi	asi	k9	asi
28-22	[number]	k4	28-22
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc1d1	starý
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chauvetově	Chauvetův	k2eAgFnSc6d1	Chauvetova
jeskyni	jeskyně	k1gFnSc6	jeskyně
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
stopy	stopa	k1gFnPc1	stopa
dítěte	dítě	k1gNnSc2	dítě
doprovázeného	doprovázený	k2eAgInSc2d1	doprovázený
velkým	velký	k2eAgMnSc7d1	velký
psem	pes	k1gMnSc7	pes
<g/>
/	/	kIx~	/
<g/>
vlkem	vlk	k1gMnSc7	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
kostry	kostra	k1gFnPc1	kostra
postrádají	postrádat	k5eAaImIp3nP	postrádat
některé	některý	k3yIgInPc1	některý
znaky	znak	k1gInPc1	znak
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
moderního	moderní	k2eAgMnSc4d1	moderní
domácího	domácí	k2eAgMnSc4d1	domácí
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	s	k7c7	s
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
o	o	k7c4	o
ochočeného	ochočený	k2eAgMnSc4d1	ochočený
vlka	vlk	k1gMnSc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
lebka	lebka	k1gFnSc1	lebka
psa	pes	k1gMnSc2	pes
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
Goyet	Goyet	k1gInSc1	Goyet
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
vlčích	vlčí	k2eAgFnPc2d1	vlčí
lebek	lebka	k1gFnPc2	lebka
liší	lišit	k5eAaImIp3nS	lišit
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
čenichu	čenich	k1gInSc2	čenich
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
spíše	spíše	k9	spíše
psovi	psův	k2eAgMnPc1d1	psův
<g/>
.	.	kIx.	.
</s>
<s>
Nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
archeologickými	archeologický	k2eAgInPc7d1	archeologický
nálezy	nález	k1gInPc7	nález
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
mtDNA	mtDNA	k?	mtDNA
současných	současný	k2eAgMnPc2d1	současný
psů	pes	k1gMnPc2	pes
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
mtDNA	mtDNA	k?	mtDNA
11	[number]	k4	11
neolitických	neolitický	k2eAgMnPc2d1	neolitický
psů	pes	k1gMnPc2	pes
nalezených	nalezený	k2eAgMnPc2d1	nalezený
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
nalezla	naleznout	k5eAaPmAgFnS	naleznout
haplotyp	haplotyp	k1gInSc4	haplotyp
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
současných	současný	k2eAgMnPc2d1	současný
psů	pes	k1gMnPc2	pes
vůbec	vůbec	k9	vůbec
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
zastoupení	zastoupení	k1gNnSc1	zastoupení
haplotypu	haplotyp	k1gInSc2	haplotyp
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
současných	současný	k2eAgMnPc2d1	současný
evropských	evropský	k2eAgMnPc2d1	evropský
psů	pes	k1gMnPc2	pes
přítomen	přítomno	k1gNnPc2	přítomno
jen	jen	k9	jen
u	u	k7c2	u
5	[number]	k4	5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
ukazovat	ukazovat	k5eAaImF	ukazovat
na	na	k7c4	na
domestikační	domestikační	k2eAgNnSc4d1	domestikační
centrum	centrum	k1gNnSc4	centrum
psa	pes	k1gMnSc2	pes
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
populace	populace	k1gFnSc1	populace
byla	být	k5eAaImAgFnS	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
psy	pes	k1gMnPc7	pes
původem	původ	k1gInSc7	původ
právě	právě	k9	právě
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc2	existence
domácího	domácí	k2eAgMnSc4d1	domácí
psa	pes	k1gMnSc4	pes
potvrzena	potvrzen	k2eAgMnSc4d1	potvrzen
v	v	k7c6	v
nálezech	nález	k1gInPc6	nález
starých	starý	k2eAgInPc2d1	starý
12	[number]	k4	12
000-14	[number]	k4	000-14
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
lokalita	lokalita	k1gFnSc1	lokalita
Bonn-Oberkassel	Bonn-Oberkassel	k1gMnSc1	Bonn-Oberkassel
<g/>
)	)	kIx)	)
–	–	k?	–
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
hrob	hrob	k1gInSc1	hrob
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
(	(	kIx(	(
<g/>
lokalita	lokalita	k1gFnSc1	lokalita
Çayönü	Çayönü	k1gFnSc2	Çayönü
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
(	(	kIx(	(
<g/>
natufiánská	natufiánský	k2eAgFnSc1d1	natufiánský
kultura	kultura	k1gFnSc1	kultura
z	z	k7c2	z
Levanty	Levanta	k1gFnSc2	Levanta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hroby	hrob	k1gInPc4	hrob
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
nalezené	nalezený	k2eAgFnPc1d1	nalezená
kostry	kostra	k1gFnPc1	kostra
psů	pes	k1gMnPc2	pes
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
v	v	k7c6	v
neolitickém	neolitický	k2eAgNnSc6d1	neolitické
sídlišti	sídliště	k1gNnSc6	sídliště
Nanzhuangtou	Nanzhuangtá	k1gFnSc4	Nanzhuangtá
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Hebei	Hebei	k1gNnSc2	Hebei
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
před	před	k7c7	před
10	[number]	k4	10
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
9700	[number]	k4	9700
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Jiahu	Jiah	k1gInSc2	Jiah
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Henan	Henany	k1gInPc2	Henany
(	(	kIx(	(
<g/>
7000	[number]	k4	7000
<g/>
-	-	kIx~	-
<g/>
5800	[number]	k4	5800
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
dobře	dobře	k6eAd1	dobře
zdokumentovaný	zdokumentovaný	k2eAgInSc1d1	zdokumentovaný
nález	nález	k1gInSc1	nález
domácího	domácí	k2eAgMnSc2d1	domácí
psa	pes	k1gMnSc2	pes
9000	[number]	k4	9000
let	léto	k1gNnPc2	léto
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jeskyně	jeskyně	k1gFnSc2	jeskyně
Danger	Danger	k1gInSc1	Danger
Cave	Cave	k1gFnSc4	Cave
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
kostry	kostra	k1gFnPc1	kostra
psů	pes	k1gMnPc2	pes
i	i	k8xC	i
v	v	k7c6	v
odpadních	odpadní	k2eAgFnPc6d1	odpadní
jámách	jáma	k1gFnPc6	jáma
se	se	k3xPyFc4	se
znaky	znak	k1gInPc7	znak
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
hroby	hrob	k1gInPc1	hrob
jiných	jiný	k1gMnPc2	jiný
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
status	status	k1gInSc4	status
domácího	domácí	k2eAgMnSc2d1	domácí
psa	pes	k1gMnSc2	pes
jako	jako	k8xS	jako
člena	člen	k1gMnSc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
psů	pes	k1gMnPc2	pes
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
a	a	k8xC	a
bronzové	bronzový	k2eAgInPc1d1	bronzový
můžeme	moct	k5eAaImIp1nP	moct
odvodit	odvodit	k5eAaPmF	odvodit
šest	šest	k4xCc4	šest
původních	původní	k2eAgInPc2d1	původní
evropských	evropský	k2eAgInPc2d1	evropský
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
jakýchsi	jakýsi	k3yIgInPc2	jakýsi
primitivních	primitivní	k2eAgFnPc2d1	primitivní
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
<g/>
:	:	kIx,	:
Canis	Canis	k1gFnSc1	Canis
familiaris	familiaris	k1gFnSc1	familiaris
palustris	palustris	k1gFnSc1	palustris
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
bažinný	bažinný	k2eAgMnSc1d1	bažinný
(	(	kIx(	(
<g/>
rašelinný	rašelinný	k2eAgMnSc1d1	rašelinný
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
pes	pes	k1gMnSc1	pes
menší	malý	k2eAgFnSc2d2	menší
až	až	k8xS	až
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
kohoutková	kohoutkový	k2eAgFnSc1d1	kohoutková
výška	výška	k1gFnSc1	výška
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
kulatou	kulatý	k2eAgFnSc7d1	kulatá
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
výrazným	výrazný	k2eAgInSc7d1	výrazný
stopem	stop	k1gInSc7	stop
a	a	k8xC	a
špičatým	špičatý	k2eAgInSc7d1	špičatý
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Kostry	kostra	k1gFnPc1	kostra
psů	pes	k1gMnPc2	pes
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
nalézány	nalézat	k5eAaImNgFnP	nalézat
v	v	k7c6	v
rašelinných	rašelinný	k2eAgFnPc6d1	rašelinná
polích	pole	k1gFnPc6	pole
neolitických	neolitický	k2eAgNnPc2d1	neolitické
sídlišť	sídliště	k1gNnPc2	sídliště
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
(	(	kIx(	(
<g/>
Hauterive-Champreveyres	Hauterive-Champreveyres	k1gMnSc1	Hauterive-Champreveyres
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Oberkassel	Oberkassel	k1gInSc1	Oberkassel
<g/>
,	,	kIx,	,
Teufelsbrucke	Teufelsbrucke	k1gInSc1	Teufelsbrucke
<g/>
,	,	kIx,	,
Oelknitz	Oelknitz	k1gInSc1	Oelknitz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
(	(	kIx(	(
<g/>
Saint-Thibaud-de-Couz	Saint-Thibaude-Couz	k1gInSc1	Saint-Thibaud-de-Couz
<g/>
,	,	kIx,	,
Pont	Pont	k1gInSc1	Pont
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ambon	ambon	k1gInSc1	ambon
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
Erralia	Erralium	k1gNnSc2	Erralium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
prapředka	prapředek	k1gMnSc4	prapředek
teriérů	teriér	k1gMnPc2	teriér
<g/>
,	,	kIx,	,
pinčů	pinč	k1gMnPc2	pinč
<g/>
,	,	kIx,	,
kníračů	knírač	k1gMnPc2	knírač
<g/>
,	,	kIx,	,
špiců	špic	k1gMnPc2	špic
či	či	k8xC	či
čivav	čivava	k1gFnPc2	čivava
<g/>
.	.	kIx.	.
</s>
<s>
Canis	Canis	k1gFnSc1	Canis
familiaris	familiaris	k1gFnPc2	familiaris
intermedius	intermedius	k1gMnSc1	intermedius
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
popelištní	popelištní	k2eAgMnSc1d1	popelištní
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
pes	pes	k1gMnSc1	pes
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
čelem	čelo	k1gNnSc7	čelo
a	a	k8xC	a
kratší	krátký	k2eAgFnSc7d2	kratší
širokou	široký	k2eAgFnSc7d1	široká
čenichovou	čenichová	k1gFnSc7	čenichová
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
i	i	k8xC	i
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předka	předek	k1gMnSc4	předek
honičů	honič	k1gMnPc2	honič
<g/>
,	,	kIx,	,
barvářů	barvář	k1gMnPc2	barvář
<g/>
,	,	kIx,	,
španělů	španěl	k1gMnPc2	španěl
<g/>
,	,	kIx,	,
retrívrů	retrívr	k1gMnPc2	retrívr
<g/>
,	,	kIx,	,
ohařů	ohař	k1gMnPc2	ohař
<g/>
,	,	kIx,	,
setrů	setr	k1gMnPc2	setr
<g/>
,	,	kIx,	,
jezevčíků	jezevčík	k1gMnPc2	jezevčík
<g/>
,	,	kIx,	,
pudlů	pudl	k1gMnPc2	pudl
<g/>
,	,	kIx,	,
pekinézů	pekinéz	k1gMnPc2	pekinéz
a	a	k8xC	a
maltézských	maltézský	k2eAgMnPc2d1	maltézský
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Canis	Canis	k1gInSc1	Canis
familiaris	familiaris	k1gFnSc2	familiaris
inostranzewi	inostranzew	k1gFnSc2	inostranzew
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
při	při	k7c6	při
vykopávkách	vykopávka	k1gFnPc6	vykopávka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
velcí	velký	k2eAgMnPc1d1	velký
psi	pes	k1gMnPc1	pes
byli	být	k5eAaImAgMnP	být
nalezeni	nalézt	k5eAaBmNgMnP	nalézt
také	také	k9	také
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Mezin	Mezina	k1gFnPc2	Mezina
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Kniegrotte	Kniegrott	k1gInSc5	Kniegrott
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mohutnější	mohutný	k2eAgInSc1d2	mohutnější
a	a	k8xC	a
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
pes	pes	k1gMnSc1	pes
bažinný	bažinný	k2eAgMnSc1d1	bažinný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
původ	původ	k1gInSc1	původ
severských	severský	k2eAgMnPc2d1	severský
tažných	tažný	k2eAgMnPc2d1	tažný
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
severských	severský	k2eAgFnPc2d1	severská
loveckých	lovecký	k2eAgFnPc2d1	lovecká
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
lajek	lajka	k1gFnPc2	lajka
<g/>
,	,	kIx,	,
eskymáckých	eskymácký	k2eAgMnPc2d1	eskymácký
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
samojedů	samojed	k1gMnPc2	samojed
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
též	též	k9	též
buldoků	buldok	k1gMnPc2	buldok
<g/>
,	,	kIx,	,
buldočků	buldoček	k1gMnPc2	buldoček
<g/>
,	,	kIx,	,
boxerů	boxer	k1gMnPc2	boxer
<g/>
,	,	kIx,	,
velkých	velká	k1gFnPc2	velká
horských	horský	k2eAgNnPc2d1	horské
molossoidních	molossoidní	k2eAgNnPc2d1	molossoidní
plemen	plemeno	k1gNnPc2	plemeno
a	a	k8xC	a
salašnických	salašnický	k2eAgMnPc2d1	salašnický
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Canis	Canis	k1gInSc1	Canis
familiaris	familiaris	k1gFnSc2	familiaris
leineri	leiner	k1gFnSc2	leiner
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
pes	pes	k1gMnSc1	pes
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
velký	velký	k2eAgMnSc1d1	velký
pes	pes	k1gMnSc1	pes
s	s	k7c7	s
úzkou	úzký	k2eAgFnSc7d1	úzká
lebkou	lebka	k1gFnSc7	lebka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgMnSc1d1	možný
prapředek	prapředek	k1gMnSc1	prapředek
dnešních	dnešní	k2eAgMnPc2d1	dnešní
chrtů	chrt	k1gMnPc2	chrt
<g/>
,	,	kIx,	,
štíhlých	štíhlý	k2eAgMnPc2d1	štíhlý
psů	pes	k1gMnPc2	pes
lovící	lovící	k2eAgFnSc7d1	lovící
pomocí	pomoc	k1gFnSc7	pomoc
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
faraonský	faraonský	k2eAgMnSc1d1	faraonský
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
afgánský	afgánský	k2eAgMnSc1d1	afgánský
chrt	chrt	k1gMnSc1	chrt
<g/>
,	,	kIx,	,
barzoj	barzoj	k1gMnSc1	barzoj
<g/>
,	,	kIx,	,
saluka	saluk	k1gMnSc2	saluk
<g/>
,	,	kIx,	,
irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
<g/>
,	,	kIx,	,
deerhound	deerhound	k1gMnSc1	deerhound
<g/>
,	,	kIx,	,
greyhound	greyhound	k1gMnSc1	greyhound
a	a	k8xC	a
whippet	whippet	k1gMnSc1	whippet
<g/>
.	.	kIx.	.
</s>
<s>
Canis	Canis	k1gFnSc1	Canis
familiaris	familiaris	k1gFnSc1	familiaris
matris	matris	k1gFnSc1	matris
optimae	optimae	k1gFnSc1	optimae
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
bronzový	bronzový	k2eAgMnSc1d1	bronzový
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejmladším	mladý	k2eAgNnSc7d3	nejmladší
plemenem	plemeno	k1gNnSc7	plemeno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
objevilo	objevit	k5eAaPmAgNnS	objevit
asi	asi	k9	asi
4-5	[number]	k4	4-5
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byl	být	k5eAaImAgInS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
lebku	lebka	k1gFnSc4	lebka
s	s	k7c7	s
plochým	plochý	k2eAgNnSc7d1	ploché
čelem	čelo	k1gNnSc7	čelo
<g/>
,	,	kIx,	,
nevýrazným	výrazný	k2eNgInSc7d1	nevýrazný
stopem	stop	k1gInSc7	stop
a	a	k8xC	a
špičatým	špičatý	k2eAgInSc7d1	špičatý
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předka	předek	k1gMnSc4	předek
všech	všecek	k3xTgMnPc2	všecek
ovčáckých	ovčácký	k2eAgMnPc2d1	ovčácký
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
<g/>
,	,	kIx,	,
belgický	belgický	k2eAgMnSc1d1	belgický
ovčák	ovčák	k1gMnSc1	ovčák
<g/>
,	,	kIx,	,
kolie	kolie	k1gFnSc1	kolie
<g/>
,	,	kIx,	,
bobtail	bobtail	k1gInSc1	bobtail
<g/>
,	,	kIx,	,
briard	briard	k1gInSc1	briard
<g/>
,	,	kIx,	,
velškorgi	velškorgi	k1gNnSc1	velškorgi
<g/>
.	.	kIx.	.
</s>
<s>
Canis	Canis	k1gFnSc1	Canis
familiaris	familiaris	k1gFnSc1	familiaris
decumanus	decumanus	k1gInSc4	decumanus
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
pes	pes	k1gMnSc1	pes
robustní	robustní	k2eAgFnSc2d1	robustní
tělesné	tělesný	k2eAgFnSc2d1	tělesná
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
předek	předek	k1gInSc1	předek
dnešních	dnešní	k2eAgInPc2d1	dnešní
mastifů	mastif	k1gInPc2	mastif
a	a	k8xC	a
dog	doga	k1gFnPc2	doga
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
za	za	k7c2	za
předka	předek	k1gMnSc2	předek
molossoidních	molossoidní	k2eAgNnPc2d1	molossoidní
plemen	plemeno	k1gNnPc2	plemeno
považují	považovat	k5eAaImIp3nP	považovat
psa	pes	k1gMnSc4	pes
typu	typ	k1gInSc2	typ
Canis	Canis	k1gInSc1	Canis
familiaris	familiaris	k1gFnSc2	familiaris
inostranzewi	inostranzew	k1gFnSc2	inostranzew
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
psovité	psovitý	k2eAgFnPc4d1	psovitá
šelmy	šelma	k1gFnPc4	šelma
a	a	k8xC	a
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
svým	svůj	k3xOyFgMnPc3	svůj
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
štíhlé	štíhlý	k2eAgNnSc1d1	štíhlé
svalnaté	svalnatý	k2eAgNnSc1d1	svalnaté
tělo	tělo	k1gNnSc1	tělo
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
hrudníkem	hrudník	k1gInSc7	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrudník	hrudník	k1gInSc4	hrudník
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
asi	asi	k9	asi
40	[number]	k4	40
°	°	k?	°
nasazen	nasazen	k2eAgInSc1d1	nasazen
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nese	nést	k5eAaImIp3nS	nést
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
čenichovou	čenichová	k1gFnSc7	čenichová
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgNnPc1d2	menší
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
nápadné	nápadný	k2eAgInPc4d1	nápadný
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vzpřímené	vzpřímený	k2eAgInPc1d1	vzpřímený
a	a	k8xC	a
zašpičatělé	zašpičatělý	k2eAgInPc1d1	zašpičatělý
<g/>
,	,	kIx,	,
u	u	k7c2	u
prošlechtěných	prošlechtěný	k2eAgNnPc2d1	prošlechtěné
plemen	plemeno	k1gNnPc2	plemeno
též	též	k9	též
klopené	klopený	k2eAgInPc4d1	klopený
a	a	k8xC	a
převislé	převislý	k2eAgInPc4d1	převislý
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgFnP	stavit
pro	pro	k7c4	pro
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
v	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
i	i	k8xC	i
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
prstochodec	prstochodec	k1gMnSc1	prstochodec
<g/>
,	,	kIx,	,
našlapuje	našlapovat	k5eAaImIp3nS	našlapovat
na	na	k7c4	na
pružné	pružný	k2eAgInPc4d1	pružný
nášlapové	nášlapový	k2eAgInPc4d1	nášlapový
polštáře	polštář	k1gInPc4	polštář
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
zrohovatělé	zrohovatělý	k2eAgInPc1d1	zrohovatělý
bezsrsté	bezsrstý	k2eAgInPc1d1	bezsrstý
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
<g/>
,	,	kIx,	,
drápy	dráp	k1gInPc4	dráp
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgInPc4d1	pevný
<g/>
,	,	kIx,	,
tupé	tupý	k2eAgInPc4d1	tupý
a	a	k8xC	a
nezatažitelné	zatažitelný	k2eNgInPc4d1	zatažitelný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrudní	hrudní	k2eAgFnSc6d1	hrudní
končetině	končetina	k1gFnSc6	končetina
má	mít	k5eAaImIp3nS	mít
pes	pes	k1gMnSc1	pes
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
palec	palec	k1gInSc1	palec
je	být	k5eAaImIp3nS	být
zakrnělý	zakrnělý	k2eAgInSc1d1	zakrnělý
a	a	k8xC	a
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
na	na	k7c6	na
pánevních	pánevní	k2eAgFnPc6d1	pánevní
končetinách	končetina	k1gFnPc6	končetina
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k6eAd1	jen
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
šavlovitý	šavlovitý	k2eAgInSc1d1	šavlovitý
<g/>
,	,	kIx,	,
srpovitý	srpovitý	k2eAgInSc1d1	srpovitý
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
jeho	jeho	k3xOp3gInSc1	jeho
konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
stočený	stočený	k2eAgInSc1d1	stočený
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
kryté	krytý	k2eAgFnPc1d1	krytá
srstí	srst	k1gFnSc7	srst
různé	různý	k2eAgFnPc1d1	různá
délky	délka	k1gFnPc1	délka
i	i	k8xC	i
struktury	struktura	k1gFnPc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgNnPc1d1	primitivní
plemena	plemeno	k1gNnPc1	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
zdivočelí	zdivočelý	k2eAgMnPc1d1	zdivočelý
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
dingo	dingo	k1gMnSc1	dingo
a	a	k8xC	a
novoguinejský	novoguinejský	k2eAgMnSc1d1	novoguinejský
zpívající	zpívající	k2eAgMnSc1d1	zpívající
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
a	a	k8xC	a
páriové	pária	k1gMnPc1	pária
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
velmi	velmi	k6eAd1	velmi
podobní	podobný	k2eAgMnPc1d1	podobný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
vypadal	vypadat	k5eAaImAgInS	vypadat
původní	původní	k2eAgMnSc1d1	původní
"	"	kIx"	"
<g/>
prapes	prapes	k1gMnSc1	prapes
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
psi	pes	k1gMnPc1	pes
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
vážící	vážící	k2eAgMnSc1d1	vážící
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
kg	kg	kA	kg
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
těla	tělo	k1gNnSc2	tělo
připomínají	připomínat	k5eAaImIp3nP	připomínat
malého	malý	k2eAgMnSc4d1	malý
vlka	vlk	k1gMnSc4	vlk
nebo	nebo	k8xC	nebo
lišku	liška	k1gFnSc4	liška
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vzpřímené	vzpřímený	k2eAgInPc1d1	vzpřímený
uši	ucho	k1gNnPc4	ucho
klínovitého	klínovitý	k2eAgInSc2d1	klínovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
špičatý	špičatý	k2eAgInSc4d1	špičatý
čenich	čenich	k1gInSc4	čenich
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
mandlovitého	mandlovitý	k2eAgInSc2d1	mandlovitý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
stočený	stočený	k2eAgInSc4d1	stočený
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
krátkosrstí	krátkosrstý	k2eAgMnPc1d1	krátkosrstý
a	a	k8xC	a
typické	typický	k2eAgNnSc1d1	typické
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgNnSc1d1	hnědé
nebo	nebo	k8xC	nebo
červenohnědé	červenohnědý	k2eAgNnSc1d1	červenohnědé
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
i	i	k8xC	i
lebky	lebka	k1gFnSc2	lebka
jsou	být	k5eAaImIp3nP	být
tito	tento	k3xDgMnPc1	tento
původní	původní	k2eAgMnPc1d1	původní
psi	pes	k1gMnPc1	pes
podobní	podobný	k2eAgMnPc1d1	podobný
vlku	vlk	k1gMnSc3	vlk
indickému	indický	k2eAgInSc3d1	indický
(	(	kIx(	(
<g/>
C.	C.	kA	C.
lupus	lupus	k1gInSc1	lupus
pallipes	pallipes	k1gInSc1	pallipes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlku	vlk	k1gMnSc3	vlk
himálajskému	himálajský	k2eAgMnSc3d1	himálajský
(	(	kIx(	(
<g/>
C.	C.	kA	C.
lupus	lupus	k1gInSc1	lupus
chanco	chanco	k1gNnSc1	chanco
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
vyhynulému	vyhynulý	k2eAgNnSc3d1	vyhynulé
malému	malé	k1gNnSc3	malé
druhu	druh	k1gInSc2	druh
vlka	vlk	k1gMnSc2	vlk
Canis	Canis	k1gFnSc2	Canis
variabilis	variabilis	k1gFnSc2	variabilis
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Číny	Čína	k1gFnSc2	Čína
před	před	k7c7	před
200	[number]	k4	200
000-500	[number]	k4	000-500
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dnešními	dnešní	k2eAgNnPc7d1	dnešní
plemeny	plemeno	k1gNnPc7	plemeno
psů	pes	k1gMnPc2	pes
existují	existovat	k5eAaImIp3nP	existovat
obrovské	obrovský	k2eAgInPc1d1	obrovský
rozdíly	rozdíl	k1gInPc1	rozdíl
co	co	k9	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
i	i	k8xC	i
tělesných	tělesný	k2eAgFnPc2d1	tělesná
proporcí	proporce	k1gFnPc2	proporce
<g/>
:	:	kIx,	:
nejmenším	malý	k2eAgNnSc7d3	nejmenší
plemenem	plemeno	k1gNnSc7	plemeno
je	být	k5eAaImIp3nS	být
čivava	čivava	k1gFnSc1	čivava
<g/>
,	,	kIx,	,
s	s	k7c7	s
ideální	ideální	k2eAgFnSc7d1	ideální
hmotností	hmotnost	k1gFnSc7	hmotnost
1,5	[number]	k4	1,5
až	až	k9	až
3	[number]	k4	3
kg	kg	kA	kg
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
největšími	veliký	k2eAgMnPc7d3	veliký
psem	pes	k1gMnSc7	pes
je	být	k5eAaImIp3nS	být
irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
s	s	k7c7	s
kohoutkovou	kohoutkový	k2eAgFnSc7d1	kohoutková
výškou	výška	k1gFnSc7	výška
minimálně	minimálně	k6eAd1	minimálně
71	[number]	k4	71
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rekordmanem	rekordman	k1gMnSc7	rekordman
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
německá	německý	k2eAgFnSc1d1	německá
doga	doga	k1gFnSc1	doga
s	s	k7c7	s
kohoutkovou	kohoutkový	k2eAgFnSc7d1	kohoutková
výškou	výška	k1gFnSc7	výška
přesahující	přesahující	k2eAgInSc4d1	přesahující
1	[number]	k4	1
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
nejtěžšími	těžký	k2eAgMnPc7d3	nejtěžší
psy	pes	k1gMnPc7	pes
jsou	být	k5eAaImIp3nP	být
bernardýn	bernardýn	k1gMnSc1	bernardýn
a	a	k8xC	a
anglický	anglický	k2eAgMnSc1d1	anglický
mastif	mastif	k1gMnSc1	mastif
<g/>
,	,	kIx,	,
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
(	(	kIx(	(
<g/>
samice	samice	k1gFnSc1	samice
psa	pes	k1gMnSc2	pes
domácího	domácí	k1gMnSc2	domácí
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
menšího	malý	k2eAgInSc2d2	menší
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnPc4d2	nižší
váhy	váha	k1gFnPc4	váha
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
mohutné	mohutný	k2eAgMnPc4d1	mohutný
než	než	k8xS	než
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Chováním	chování	k1gNnSc7	chování
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
něžnější	něžný	k2eAgInPc4d2	něžnější
<g/>
,	,	kIx,	,
přítulnější	přítulný	k2eAgInPc4d2	přítulnější
a	a	k8xC	a
věrnější	věrný	k2eAgInPc4d2	věrnější
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
klidné	klidný	k2eAgFnPc1d1	klidná
<g/>
,	,	kIx,	,
vyrovnané	vyrovnaný	k2eAgNnSc1d1	vyrovnané
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
lépe	dobře	k6eAd2	dobře
cvičit	cvičit	k5eAaImF	cvičit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
dominantní	dominantní	k2eAgNnSc4d1	dominantní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Obtížněji	obtížně	k6eAd2	obtížně
ovladatelná	ovladatelný	k2eAgFnSc1d1	ovladatelná
je	být	k5eAaImIp3nS	být
fena	fena	k1gFnSc1	fena
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hárání	hárání	k1gNnSc2	hárání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
utíkat	utíkat	k5eAaImF	utíkat
ke	k	k7c3	k
psům	pes	k1gMnPc3	pes
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
fen	fena	k1gFnPc2	fena
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
objevuje	objevovat	k5eAaImIp3nS	objevovat
tzv.	tzv.	kA	tzv.
falešná	falešný	k2eAgFnSc1d1	falešná
březost	březost	k1gFnSc1	březost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
očekávaly	očekávat	k5eAaImAgFnP	očekávat
štěňata	štěně	k1gNnPc1	štěně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
samci	samec	k1gMnPc1	samec
psa	pes	k1gMnSc2	pes
domácího	domácí	k1gMnSc2	domácí
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
většího	veliký	k2eAgInSc2d2	veliký
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
a	a	k8xC	a
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
vytrvalejší	vytrvalý	k2eAgMnPc1d2	vytrvalejší
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
i	i	k8xC	i
při	při	k7c6	při
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
naučit	naučit	k5eAaPmF	naučit
více	hodně	k6eAd2	hodně
cviků	cvik	k1gInPc2	cvik
než	než	k8xS	než
feny	fena	k1gFnSc2	fena
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dominantnější	dominantní	k2eAgNnSc4d2	dominantnější
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
větší	veliký	k2eAgFnPc4d2	veliký
ambice	ambice	k1gFnPc4	ambice
být	být	k5eAaImF	být
pánem	pán	k1gMnSc7	pán
smečky	smečka	k1gFnSc2	smečka
nebo	nebo	k8xC	nebo
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
hůře	zle	k6eAd2	zle
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutná	nutný	k2eAgFnSc1d1	nutná
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psům	pes	k1gMnPc3	pes
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
výcvik	výcvik	k1gInSc1	výcvik
nevadí	vadit	k5eNaImIp3nS	vadit
tolik	tolik	k6eAd1	tolik
jako	jako	k9	jako
fenám	fena	k1gFnPc3	fena
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
problémy	problém	k1gInPc1	problém
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ovládáním	ovládání	k1gNnSc7	ovládání
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
háravé	háravý	k2eAgFnSc2d1	háravá
feny	fena	k1gFnSc2	fena
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
problémy	problém	k1gInPc1	problém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nechtěná	chtěný	k2eNgNnPc1d1	nechtěné
štěňata	štěně	k1gNnPc1	štěně
<g/>
,	,	kIx,	,
utíkání	utíkání	k1gNnSc1	utíkání
za	za	k7c7	za
háravými	háravý	k2eAgFnPc7d1	háravá
fenami	fena	k1gFnPc7	fena
<g/>
,	,	kIx,	,
dominantní	dominantní	k2eAgNnSc1d1	dominantní
chování	chování	k1gNnSc1	chování
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
omezit	omezit	k5eAaPmF	omezit
nebo	nebo	k8xC	nebo
vyřešit	vyřešit	k5eAaPmF	vyřešit
kastrací	kastrace	k1gFnSc7	kastrace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anatomie	anatomie	k1gFnSc2	anatomie
psa	pes	k1gMnSc2	pes
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
plemennou	plemenný	k2eAgFnSc4d1	plemenná
příslušnost	příslušnost	k1gFnSc4	příslušnost
psa	pes	k1gMnSc2	pes
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
271	[number]	k4	271
<g/>
–	–	k?	–
<g/>
282	[number]	k4	282
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
části	část	k1gFnPc4	část
–	–	k?	–
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
silné	silný	k2eAgFnPc4d1	silná
čelisti	čelist	k1gFnPc4	čelist
umožňující	umožňující	k2eAgInSc4d1	umožňující
masožravý	masožravý	k2eAgInSc4d1	masožravý
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
osový	osový	k2eAgInSc1d1	osový
skelet	skelet	k1gInSc1	skelet
–	–	k?	–
páteř	páteř	k1gFnSc1	páteř
a	a	k8xC	a
hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
osu	osa	k1gFnSc4	osa
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
kostru	kostra	k1gFnSc4	kostra
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
růst	růst	k1gInSc1	růst
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
dokončen	dokončit	k5eAaPmNgInS	dokončit
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc3	měsíc
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
Kosterní	kosterní	k2eAgInPc1d1	kosterní
svaly	sval	k1gInPc1	sval
psa	pes	k1gMnSc2	pes
se	s	k7c7	s
šlachami	šlacha	k1gFnPc7	šlacha
upínají	upínat	k5eAaImIp3nP	upínat
na	na	k7c4	na
kosti	kost	k1gFnPc4	kost
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
svalovou	svalový	k2eAgFnSc7d1	svalová
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pohyb	pohyb	k1gInSc4	pohyb
celého	celý	k2eAgNnSc2d1	celé
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psích	psí	k2eAgInPc6d1	psí
svalech	sval	k1gInPc6	sval
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k6eAd1	málo
vazivové	vazivový	k2eAgFnPc4d1	vazivová
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
svalová	svalový	k2eAgFnSc1d1	svalová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
hlubokých	hluboký	k2eAgInPc6d1	hluboký
svalech	sval	k1gInPc6	sval
končetin	končetina	k1gFnPc2	končetina
zcela	zcela	k6eAd1	zcela
převládají	převládat	k5eAaImIp3nP	převládat
pomalá	pomalý	k2eAgFnSc1d1	pomalá
červená	červený	k2eAgFnSc1d1	červená
vlákna	vlákna	k1gFnSc1	vlákna
typu	typ	k1gInSc2	typ
I	I	kA	I
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
neustálé	neustálý	k2eAgNnSc4d1	neustálé
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
unavila	unavit	k5eAaPmAgFnS	unavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povrchových	povrchový	k2eAgInPc6d1	povrchový
svalech	sval	k1gInPc6	sval
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgNnPc1d1	červené
vlákna	vlákno	k1gNnPc1	vlákno
společně	společně	k6eAd1	společně
s	s	k7c7	s
rychlými	rychlý	k2eAgMnPc7d1	rychlý
bílými	bílý	k1gMnPc7	bílý
vlákny	vlákna	k1gFnSc2	vlákna
typu	typ	k1gInSc2	typ
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
silový	silový	k2eAgInSc4d1	silový
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
únavě	únava	k1gFnSc3	únava
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc1	sval
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
psa	pes	k1gMnSc2	pes
jako	jako	k8xC	jako
vytrvalého	vytrvalý	k2eAgMnSc2d1	vytrvalý
běžce	běžec	k1gMnSc2	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
ale	ale	k9	ale
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
neúnavné	únavný	k2eNgNnSc4d1	neúnavné
stání	stání	k1gNnSc4	stání
a	a	k8xC	a
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
usedá	usedat	k5eAaImIp3nS	usedat
nebo	nebo	k8xC	nebo
lehá	lehat	k5eAaImIp3nS	lehat
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
mimické	mimický	k2eAgInPc1d1	mimický
svaly	sval	k1gInPc1	sval
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
širokou	široký	k2eAgFnSc4d1	široká
obličejovou	obličejový	k2eAgFnSc4d1	obličejová
mimiku	mimika	k1gFnSc4	mimika
<g/>
.	.	kIx.	.
</s>
<s>
Žvýkací	žvýkací	k2eAgInPc1d1	žvýkací
svaly	sval	k1gInPc1	sval
jsou	být	k5eAaImIp3nP	být
mohutné	mohutný	k2eAgMnPc4d1	mohutný
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
20	[number]	k4	20
kg	kg	kA	kg
dokáže	dokázat	k5eAaPmIp3nS	dokázat
při	při	k7c6	při
skusu	skus	k1gInSc6	skus
vyvinout	vyvinout	k5eAaPmF	vyvinout
tlak	tlak	k1gInSc4	tlak
až	až	k9	až
165	[number]	k4	165
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
42	[number]	k4	42
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
štěně	štěně	k1gNnSc4	štěně
28	[number]	k4	28
mléčných	mléčný	k2eAgInPc2d1	mléčný
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
začíná	začínat	k5eAaImIp3nS	začínat
výměna	výměna	k1gFnSc1	výměna
zubů	zub	k1gInPc2	zub
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
měsícem	měsíc	k1gInSc7	měsíc
věku	věk	k1gInSc2	věk
a	a	k8xC	a
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
má	mít	k5eAaImIp3nS	mít
skus	skus	k1gInSc1	skus
nůžkový	nůžkový	k2eAgInSc1d1	nůžkový
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
se	se	k3xPyFc4	se
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
čenichem	čenich	k1gInSc7	čenich
mívají	mívat	k5eAaImIp3nP	mívat
předkus	předkus	k1gInSc4	předkus
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
přizpůsobené	přizpůsobený	k2eAgInPc1d1	přizpůsobený
k	k	k7c3	k
chycení	chycení	k1gNnSc3	chycení
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
oddělování	oddělování	k1gNnSc2	oddělování
masa	maso	k1gNnSc2	maso
od	od	k7c2	od
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
žvýkat	žvýkat	k5eAaImF	žvýkat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc1d1	ústní
ji	on	k3xPp3gFnSc4	on
rozmělnit	rozmělnit	k5eAaPmF	rozmělnit
na	na	k7c4	na
malá	malý	k2eAgNnPc4d1	malé
sousta	sousto	k1gNnPc4	sousto
<g/>
,	,	kIx,	,
kusy	kus	k1gInPc1	kus
potravy	potrava	k1gFnSc2	potrava
polykají	polykat	k5eAaImIp3nP	polykat
vcelku	vcelku	k6eAd1	vcelku
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
soustava	soustava	k1gFnSc1	soustava
také	také	k9	také
svou	svůj	k3xOyFgFnSc7	svůj
stavbou	stavba	k1gFnSc7	stavba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
především	především	k9	především
masožravec	masožravec	k1gMnSc1	masožravec
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
jeho	jeho	k3xOp3gFnSc2	jeho
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
žaludek	žaludek	k1gInSc4	žaludek
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgNnSc1d1	krátké
tenké	tenký	k2eAgNnSc1d1	tenké
střevo	střevo	k1gNnSc1	střevo
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
podkovovitého	podkovovitý	k2eAgInSc2d1	podkovovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
stejně	stejně	k6eAd1	stejně
velkému	velký	k2eAgMnSc3d1	velký
vlku	vlk	k1gMnSc3	vlk
až	až	k9	až
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
menší	malý	k2eAgFnSc1d2	menší
hmotnost	hmotnost	k1gFnSc1	hmotnost
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
psí	psí	k2eAgInSc1d1	psí
mozek	mozek	k1gInSc1	mozek
váží	vážit	k5eAaImIp3nS	vážit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
psa	pes	k1gMnSc4	pes
68	[number]	k4	68
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
g	g	kA	g
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k9	tak
0,2	[number]	k4	0,2
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Čichové	čichový	k2eAgInPc1d1	čichový
laloky	lalok	k1gInPc1	lalok
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
část	část	k1gFnSc1	část
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zpracovávány	zpracováván	k2eAgInPc4d1	zpracováván
čichové	čichový	k2eAgInPc4d1	čichový
vjemy	vjem	k1gInPc4	vjem
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
60	[number]	k4	60
g	g	kA	g
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
4	[number]	k4	4
<g/>
×	×	k?	×
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
–	–	k?	–
apokrinní	apokrinný	k2eAgMnPc1d1	apokrinný
potní	potní	k2eAgMnPc1d1	potní
žlázy	žláza	k1gFnSc2	žláza
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
chlupovými	chlupový	k2eAgInPc7d1	chlupový
folikuly	folikul	k1gInPc7	folikul
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
především	především	k9	především
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
slabin	slabina	k1gFnPc2	slabina
a	a	k8xC	a
mezinoží	mezinožit	k5eAaPmIp3nS	mezinožit
<g/>
,	,	kIx,	,
na	na	k7c6	na
plecích	plec	k1gFnPc6	plec
a	a	k8xC	a
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
typický	typický	k2eAgInSc4d1	typický
pach	pach	k1gInSc4	pach
psí	psí	k2eAgFnSc2d1	psí
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řitního	řitní	k2eAgInSc2d1	řitní
otvoru	otvor	k1gInSc2	otvor
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
anální	anální	k2eAgInPc1d1	anální
váčky	váček	k1gInPc1	váček
(	(	kIx(	(
<g/>
sinus	sinus	k1gInSc1	sinus
paranalis	paranalis	k1gFnSc2	paranalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
párové	párový	k2eAgInPc4d1	párový
váčkovité	váčkovitý	k2eAgInPc4d1	váčkovitý
duté	dutý	k2eAgInPc4d1	dutý
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
mm	mm	kA	mm
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
páchnoucí	páchnoucí	k2eAgInSc4d1	páchnoucí
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
žlutohnědý	žlutohnědý	k2eAgInSc4d1	žlutohnědý
nebo	nebo	k8xC	nebo
hnědý	hnědý	k2eAgInSc4d1	hnědý
sekret	sekret	k1gInSc4	sekret
<g/>
,	,	kIx,	,
ulpívající	ulpívající	k2eAgInSc4d1	ulpívající
na	na	k7c6	na
výkalech	výkal	k1gInPc6	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Pachové	pachový	k2eAgFnPc1d1	pachová
potní	potní	k2eAgFnPc1d1	potní
žlázy	žláza	k1gFnPc1	žláza
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
také	také	k9	také
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
prstních	prstní	k2eAgInPc2d1	prstní
polštářů	polštář	k1gInPc2	polštář
na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
<g/>
.	.	kIx.	.
</s>
<s>
Tlapky	tlapka	k1gFnPc1	tlapka
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jediné	jediný	k2eAgNnSc4d1	jediné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pravé	pravá	k1gFnPc1	pravá
<g/>
,	,	kIx,	,
ekrinní	ekrinný	k2eAgMnPc1d1	ekrinný
potní	potní	k2eAgMnPc1d1	potní
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
na	na	k7c4	na
1	[number]	k4	1
cm	cm	kA	cm
<g/>
2	[number]	k4	2
polštáře	polštář	k1gInSc2	polštář
nachází	nacházet	k5eAaImIp3nS	nacházet
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Sekret	sekret	k1gInSc1	sekret
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
při	při	k7c6	při
označování	označování	k1gNnSc6	označování
teritoria	teritorium	k1gNnSc2	teritorium
–	–	k?	–
hrabání	hrabání	k1gNnSc2	hrabání
po	po	k7c4	po
vykonání	vykonání	k1gNnSc4	vykonání
potřeby	potřeba	k1gFnSc2	potřeba
není	být	k5eNaImIp3nS	být
pokusem	pokus	k1gInSc7	pokus
výkaly	výkal	k1gInPc7	výkal
zahrabat	zahrabat	k5eAaPmF	zahrabat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozetření	rozetření	k1gNnSc3	rozetření
výměšků	výměšek	k1gInPc2	výměšek
těchto	tento	k3xDgFnPc2	tento
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Sekrece	sekrece	k1gFnSc1	sekrece
také	také	k9	také
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
přilnavost	přilnavost	k1gFnSc4	přilnavost
polštářků	polštářek	k1gInPc2	polštářek
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
suché	suchý	k2eAgFnSc6d1	suchá
a	a	k8xC	a
hladké	hladký	k2eAgFnSc6d1	hladká
podložce	podložka	k1gFnSc6	podložka
<g/>
,	,	kIx,	,
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
tlapky	tlapka	k1gFnSc2	tlapka
pružné	pružný	k2eAgNnSc1d1	pružné
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psa	pes	k1gMnSc2	pes
nemají	mít	k5eNaImIp3nP	mít
potní	potní	k2eAgFnPc4d1	potní
žlázy	žláza	k1gFnPc4	žláza
téměř	téměř	k6eAd1	téměř
žádnou	žádný	k3yNgFnSc4	žádný
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
termoregulaci	termoregulace	k1gFnSc6	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Exteriér	exteriér	k1gInSc1	exteriér
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
psů	pes	k1gMnPc2	pes
víceméně	víceméně	k9	víceméně
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
vnějšími	vnější	k2eAgInPc7d1	vnější
znaky	znak	k1gInPc7	znak
se	se	k3xPyFc4	se
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
plemena	plemeno	k1gNnPc1	plemeno
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
exteriéru	exteriér	k1gInSc2	exteriér
ideálního	ideální	k2eAgMnSc2d1	ideální
jedince	jedinec	k1gMnSc2	jedinec
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
plemene	plemeno	k1gNnSc2	plemeno
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
standardu	standard	k1gInSc6	standard
plemene	plemeno	k1gNnSc2	plemeno
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
průkazem	průkaz	k1gInSc7	průkaz
původu	původ	k1gInSc2	původ
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
především	především	k9	především
podle	podle	k7c2	podle
standardů	standard	k1gInPc2	standard
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kynologické	kynologický	k2eAgFnSc2d1	kynologická
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
FCI	FCI	kA	FCI
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
exteriéru	exteriér	k1gInSc2	exteriér
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
prvotní	prvotní	k2eAgFnSc7d1	prvotní
informací	informace	k1gFnSc7	informace
výška	výška	k1gFnSc1	výška
psa	pes	k1gMnSc2	pes
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
pod	pod	k7c4	pod
35	[number]	k4	35
cm	cm	kA	cm
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
malých	malý	k2eAgMnPc6d1	malý
psech	pes	k1gMnPc6	pes
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
35	[number]	k4	35
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
psy	pes	k1gMnPc4	pes
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
kohoutkovou	kohoutkový	k2eAgFnSc7d1	kohoutková
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
cm	cm	kA	cm
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
a	a	k8xC	a
nad	nad	k7c7	nad
80	[number]	k4	80
cm	cm	kA	cm
kohoutkové	kohoutkový	k2eAgFnSc2d1	kohoutková
výšky	výška	k1gFnSc2	výška
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
obří	obří	k2eAgNnPc4d1	obří
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
exteriérovým	exteriérový	k2eAgInSc7d1	exteriérový
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
tělesný	tělesný	k2eAgInSc4d1	tělesný
rámec	rámec	k1gInSc4	rámec
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
délkou	délka	k1gFnSc7	délka
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
kohoutkovou	kohoutkový	k2eAgFnSc7d1	kohoutková
výškou	výška	k1gFnSc7	výška
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
nebo	nebo	k8xC	nebo
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
celky	celek	k1gInPc4	celek
<g/>
:	:	kIx,	:
mozkovnu	mozkovna	k1gFnSc4	mozkovna
a	a	k8xC	a
čenichovou	čenichová	k1gFnSc4	čenichová
partii	partie	k1gFnSc4	partie
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
mozkovnou	mozkovna	k1gFnSc7	mozkovna
a	a	k8xC	a
čenichem	čenich	k1gInSc7	čenich
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stop	stop	k1gInSc1	stop
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poměrů	poměr	k1gInPc2	poměr
délek	délka	k1gFnPc2	délka
mozkovky	mozkovka	k1gFnSc2	mozkovka
a	a	k8xC	a
nosu	nos	k1gInSc2	nos
se	se	k3xPyFc4	se
psi	pes	k1gMnPc1	pes
dají	dát	k5eAaPmIp3nP	dát
obecně	obecně	k6eAd1	obecně
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
brachycefalická	brachycefalický	k2eAgNnPc1d1	brachycefalický
plemena	plemeno	k1gNnPc1	plemeno
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
krátkohlavá	krátkohlavý	k2eAgFnSc1d1	krátkohlavý
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
psi	pes	k1gMnPc1	pes
se	s	k7c7	s
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
čenichovou	čenichová	k1gFnSc7	čenichová
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
s	s	k7c7	s
dopředu	dopředu	k6eAd1	dopředu
směřujícíma	směřující	k2eAgNnPc7d1	směřující
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
předkus	předkus	k1gInSc1	předkus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
čelist	čelist	k1gFnSc1	čelist
spodní	spodní	k2eAgFnSc1d1	spodní
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
krátkolebého	krátkolebý	k2eAgNnSc2d1	krátkolebé
plemene	plemeno	k1gNnSc2	plemeno
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
buldok	buldok	k1gMnSc1	buldok
<g/>
,	,	kIx,	,
mops	mops	k1gMnSc1	mops
či	či	k8xC	či
boxer	boxer	k1gMnSc1	boxer
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácení	zkrácení	k1gNnSc1	zkrácení
čelistí	čelist	k1gFnPc2	čelist
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
také	také	k9	také
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
způsobené	způsobený	k2eAgFnSc2d1	způsobená
zúžením	zúžení	k1gNnSc7	zúžení
nozder	nozdra	k1gFnPc2	nozdra
či	či	k8xC	či
relativně	relativně	k6eAd1	relativně
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
měkkým	měkký	k2eAgNnSc7d1	měkké
patrem	patro	k1gNnSc7	patro
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
brachycefalický	brachycefalický	k2eAgInSc1d1	brachycefalický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
mezocefalická	mezocefalický	k2eAgNnPc1d1	mezocefalický
plemena	plemeno	k1gNnPc1	plemeno
jsou	být	k5eAaImIp3nP	být
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
lebky	lebka	k1gFnSc2	lebka
od	od	k7c2	od
týlu	týl	k1gInSc2	týl
ke	k	k7c3	k
stopu	stop	k1gInSc3	stop
víceméně	víceméně	k9	víceméně
rovná	rovnat	k5eAaImIp3nS	rovnat
délce	délka	k1gFnSc3	délka
čenichové	čenichové	k2eAgFnPc4d1	čenichové
partie	partie	k1gFnPc4	partie
od	od	k7c2	od
stopu	stop	k1gInSc2	stop
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
čenichu	čenich	k1gInSc2	čenich
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
mozkovny	mozkovna	k1gFnSc2	mozkovna
je	být	k5eAaImIp3nS	být
též	též	k9	též
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
většina	většina	k1gFnSc1	většina
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
dolichocefalická	dolichocefalický	k2eAgNnPc1d1	dolichocefalický
plemena	plemeno	k1gNnPc1	plemeno
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
dlouhohlavá	dlouhohlavý	k2eAgFnSc1d1	dlouhohlavý
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
a	a	k8xC	a
plochou	plochý	k2eAgFnSc7d1	plochá
mozkovnou	mozkovna	k1gFnSc7	mozkovna
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
vystupující	vystupující	k2eAgFnSc4d1	vystupující
týlní	týlní	k2eAgFnSc4d1	týlní
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
hlavy	hlava	k1gFnSc2	hlava
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
její	její	k3xOp3gFnSc4	její
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Čenichová	Čenichový	k2eAgFnSc1d1	Čenichová
partie	partie	k1gFnSc1	partie
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
mozkovnou	mozkovna	k1gFnSc7	mozkovna
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dolicocefalickými	Dolicocefalický	k2eAgNnPc7d1	Dolicocefalický
plemeny	plemeno	k1gNnPc7	plemeno
jsou	být	k5eAaImIp3nP	být
chrti	chrt	k1gMnPc1	chrt
<g/>
,	,	kIx,	,
kolie	kolie	k1gFnPc1	kolie
a	a	k8xC	a
jezevčíci	jezevčík	k1gMnPc1	jezevčík
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
predispozice	predispozice	k1gFnSc1	predispozice
k	k	k7c3	k
podkusu	podkus	k1gInSc3	podkus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
dolní	dolní	k2eAgFnSc4d1	dolní
čelist	čelist	k1gFnSc4	čelist
kratší	krátký	k2eAgFnSc4d2	kratší
než	než	k8xS	než
čelist	čelist	k1gFnSc4	čelist
horní	horní	k2eAgFnSc4d1	horní
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
oronazálních	oronazální	k2eAgFnPc2d1	oronazální
fistul	fistula	k1gFnPc2	fistula
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
stopu	stop	k1gInSc2	stop
jsou	být	k5eAaImIp3nP	být
umístěné	umístěný	k2eAgFnPc4d1	umístěná
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
kulatou	kulatý	k2eAgFnSc4d1	kulatá
zornici	zornice	k1gFnSc4	zornice
a	a	k8xC	a
duhovku	duhovka	k1gFnSc4	duhovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
tmavohnědá	tmavohnědý	k2eAgFnSc1d1	tmavohnědá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
plemen	plemeno	k1gNnPc2	plemeno
je	být	k5eAaImIp3nS	být
povolená	povolený	k2eAgFnSc1d1	povolená
barva	barva	k1gFnSc1	barva
světle	světle	k6eAd1	světle
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
,	,	kIx,	,
jantarově	jantarově	k6eAd1	jantarově
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
nestejně	stejně	k6eNd1	stejně
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
koutku	koutek	k1gInSc6	koutek
oka	oko	k1gNnSc2	oko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
třetí	třetí	k4xOgNnSc4	třetí
oční	oční	k2eAgNnSc4d1	oční
víčko	víčko	k1gNnSc4	víčko
<g/>
,	,	kIx,	,
spojivkou	spojivka	k1gFnSc7	spojivka
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
chrupavčitá	chrupavčitý	k2eAgFnSc1d1	chrupavčitá
ploténka	ploténka	k1gFnSc1	ploténka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
oko	oko	k1gNnSc4	oko
před	před	k7c7	před
případnými	případný	k2eAgNnPc7d1	případné
cizími	cizí	k2eAgNnPc7d1	cizí
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
prach	prach	k1gInSc4	prach
nebo	nebo	k8xC	nebo
písek	písek	k1gInSc4	písek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdravého	zdravý	k2eAgNnSc2d1	zdravé
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
víčko	víčko	k1gNnSc4	víčko
nenápadné	nápadný	k2eNgNnSc4d1	nenápadné
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
tvarem	tvar	k1gInSc7	tvar
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
klínovitá	klínovitý	k2eAgFnSc1d1	klínovitá
<g/>
,	,	kIx,	,
též	též	k9	též
vlčí	vlčet	k5eAaImIp3nS	vlčet
<g/>
.	.	kIx.	.
</s>
<s>
Čenichová	Čenichový	k2eAgFnSc1d1	Čenichová
partie	partie	k1gFnSc1	partie
postupně	postupně	k6eAd1	postupně
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
čenichu	čenich	k1gInSc3	čenich
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
německé	německý	k2eAgMnPc4d1	německý
ovčáky	ovčák	k1gMnPc4	ovčák
<g/>
,	,	kIx,	,
špice	špic	k1gMnPc4	špic
<g/>
,	,	kIx,	,
šiperku	šiperka	k1gFnSc4	šiperka
<g/>
,	,	kIx,	,
kolie	kolie	k1gFnPc4	kolie
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
hranatá	hranatý	k2eAgFnSc1d1	hranatá
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
kratší	krátký	k2eAgFnSc7d2	kratší
čenichovou	čenichův	k2eAgFnSc7d1	čenichův
partií	partie	k1gFnSc7	partie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nezužuje	zužovat	k5eNaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
<g/>
,	,	kIx,	,
s	s	k7c7	s
patrnými	patrný	k2eAgFnPc7d1	patrná
lícními	lícní	k2eAgFnPc7d1	lícní
kostmi	kost	k1gFnPc7	kost
<g/>
.	.	kIx.	.
</s>
<s>
Hranatou	hranatý	k2eAgFnSc4d1	hranatá
hlavu	hlava	k1gFnSc4	hlava
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
molossoidní	molossoidní	k2eAgNnPc1d1	molossoidní
plemena	plemeno	k1gNnPc1	plemeno
nebo	nebo	k8xC	nebo
americký	americký	k2eAgMnSc1d1	americký
pitbulteriér	pitbulteriér	k1gMnSc1	pitbulteriér
<g/>
.	.	kIx.	.
</s>
<s>
Kulatou	kulatý	k2eAgFnSc4d1	kulatá
hlavu	hlava	k1gFnSc4	hlava
mají	mít	k5eAaImIp3nP	mít
malá	malý	k2eAgNnPc4d1	malé
společenská	společenský	k2eAgNnPc4d1	společenské
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
opičí	opičí	k2eAgMnSc1d1	opičí
pinč	pinč	k1gMnSc1	pinč
nebo	nebo	k8xC	nebo
shih-tzu	shihza	k1gFnSc4	shih-tza
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
u	u	k7c2	u
čivavy	čivava	k1gFnSc2	čivava
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vypouklé	vypouklý	k2eAgNnSc1d1	vypouklé
čelo	čelo	k1gNnSc1	čelo
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
tzv.	tzv.	kA	tzv.
jablkovitá	jablkovitý	k2eAgFnSc1d1	jablkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
anglického	anglický	k2eAgMnSc2d1	anglický
bulteriéra	bulteriér	k1gMnSc2	bulteriér
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
ušního	ušní	k2eAgInSc2d1	ušní
boltce	boltec	k1gInSc2	boltec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
postavení	postavení	k1gNnSc1	postavení
velice	velice	k6eAd1	velice
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celkový	celkový	k2eAgInSc1d1	celkový
výraz	výraz	k1gInSc1	výraz
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
jsou	být	k5eAaImIp3nP	být
uši	ucho	k1gNnPc4	ucho
vzpřímené	vzpřímený	k2eAgFnSc2d1	vzpřímená
<g/>
,	,	kIx,	,
prošlechtěná	prošlechtěný	k2eAgNnPc1d1	prošlechtěné
plemena	plemeno	k1gNnPc1	plemeno
mají	mít	k5eAaImIp3nP	mít
uši	ucho	k1gNnPc4	ucho
s	s	k7c7	s
měkkou	měkký	k2eAgFnSc7d1	měkká
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
visí	viset	k5eAaImIp3nP	viset
podle	podle	k7c2	podle
lící	líce	k1gNnPc2	líce
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Klopené	klopený	k2eAgNnSc1d1	klopené
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
uši	ucho	k1gNnPc4	ucho
vzpřímené	vzpřímený	k2eAgFnSc2d1	vzpřímená
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přibližně	přibližně	k6eAd1	přibližně
horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
přehnutá	přehnutý	k2eAgFnSc1d1	přehnutá
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
ústí	ústí	k1gNnSc4	ústí
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Poloklopené	Poloklopený	k2eAgNnSc1d1	Poloklopený
uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
vzpřímenější	vzpřímený	k2eAgFnPc1d2	vzpřímenější
a	a	k8xC	a
lehčí	lehký	k2eAgFnPc1d2	lehčí
než	než	k8xS	než
klopené	klopený	k2eAgFnPc1d1	klopená
<g/>
,	,	kIx,	,
dopředu	dopředu	k6eAd1	dopředu
je	být	k5eAaImIp3nS	být
ohnutá	ohnutý	k2eAgFnSc1d1	ohnutá
jen	jen	k9	jen
špička	špička	k1gFnSc1	špička
ucha	ucho	k1gNnSc2	ucho
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
zvukovodu	zvukovod	k1gInSc2	zvukovod
je	být	k5eAaImIp3nS	být
volné	volný	k2eAgNnSc1d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Ucho	ucho	k1gNnSc1	ucho
tvaru	tvar	k1gInSc2	tvar
růžového	růžový	k2eAgInSc2d1	růžový
lístku	lístek	k1gInSc2	lístek
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
vysoko	vysoko	k6eAd1	vysoko
nasazené	nasazený	k2eAgNnSc1d1	nasazené
klopené	klopený	k2eAgNnSc1d1	klopené
ucho	ucho	k1gNnSc1	ucho
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
nazad	nazad	k6eAd1	nazad
a	a	k8xC	a
odkrývající	odkrývající	k2eAgInSc1d1	odkrývající
vnitřek	vnitřek	k1gInSc1	vnitřek
boltce	boltec	k1gInSc2	boltec
i	i	k8xC	i
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Ušní	ušní	k2eAgInSc1d1	ušní
boltec	boltec	k1gInSc1	boltec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nasazený	nasazený	k2eAgMnSc1d1	nasazený
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
vysoko	vysoko	k6eAd1	vysoko
nebo	nebo	k8xC	nebo
vysoko	vysoko	k6eAd1	vysoko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hodnocení	hodnocení	k1gNnSc1	hodnocení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
nízko	nízko	k6eAd1	nízko
a	a	k8xC	a
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
vysoko	vysoko	k6eAd1	vysoko
zpravidla	zpravidla	k6eAd1	zpravidla
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
standardu	standard	k1gInSc6	standard
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
mají	mít	k5eAaImIp3nP	mít
vysoko	vysoko	k6eAd1	vysoko
nasazené	nasazený	k2eAgNnSc4d1	nasazené
uši	ucho	k1gNnPc4	ucho
–	–	k?	–
například	například	k6eAd1	například
špicové	špic	k1gMnPc1	špic
<g/>
,	,	kIx,	,
novofundlandský	novofundlandský	k2eAgMnSc1d1	novofundlandský
nebo	nebo	k8xC	nebo
bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
nízce	nízce	k6eAd1	nízce
nasazené	nasazený	k2eAgNnSc4d1	nasazené
uši	ucho	k1gNnPc4	ucho
má	mít	k5eAaImIp3nS	mít
bígl	bígl	k1gMnSc1	bígl
nebo	nebo	k8xC	nebo
anglický	anglický	k2eAgMnSc1d1	anglický
kokršpaněl	kokršpaněl	k1gMnSc1	kokršpaněl
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
ušního	ušní	k2eAgInSc2d1	ušní
boltce	boltec	k1gInSc2	boltec
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgInPc1d1	značný
meziplemenné	meziplemenný	k2eAgInPc1d1	meziplemenný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
uši	ucho	k1gNnPc4	ucho
německý	německý	k2eAgMnSc1d1	německý
špic	špic	k1gMnSc1	špic
nebo	nebo	k8xC	nebo
šarpej	šarpat	k5eAaPmRp2nS	šarpat
<g/>
,	,	kIx,	,
brazilská	brazilský	k2eAgNnPc4d1	brazilské
fila	filum	k1gNnPc4	filum
nebo	nebo	k8xC	nebo
tosa-inu	tosana	k1gFnSc4	tosa-ina
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nápadně	nápadně	k6eAd1	nápadně
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
mají	mít	k5eAaImIp3nP	mít
honiči	honič	k1gMnPc1	honič
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
lalokovité	lalokovitý	k2eAgFnPc1d1	lalokovitý
uši	ucho	k1gNnPc4	ucho
honičů	honič	k1gMnPc2	honič
totiž	totiž	k9	totiž
dobře	dobře	k6eAd1	dobře
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
pachy	pach	k1gInPc4	pach
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
psovi	psův	k2eAgMnPc1d1	psův
držet	držet	k5eAaImF	držet
stopu	stopa	k1gFnSc4	stopa
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
byl	být	k5eAaImAgMnS	být
psem	pes	k1gMnSc7	pes
s	s	k7c7	s
vůbec	vůbec	k9	vůbec
nejdelšíma	dlouhý	k2eAgNnPc7d3	nejdelší
ušima	ucho	k1gNnPc7	ucho
bloodhound	bloodhound	k1gMnSc1	bloodhound
Tigger	Tigger	k1gMnSc1	Tigger
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
pravé	pravý	k2eAgNnSc1d1	pravé
ucho	ucho	k1gNnSc1	ucho
měřilo	měřit	k5eAaImAgNnS	měřit
34,9	[number]	k4	34,9
cm	cm	kA	cm
a	a	k8xC	a
levé	levý	k2eAgNnSc1d1	levé
ucho	ucho	k1gNnSc1	ucho
34,2	[number]	k4	34,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
držitelem	držitel	k1gMnSc7	držitel
rekordu	rekord	k1gInSc2	rekord
je	být	k5eAaImIp3nS	být
coonhound	coonhound	k1gInSc1	coonhound
<g/>
,	,	kIx,	,
s	s	k7c7	s
ušima	ucho	k1gNnPc7	ucho
dlouhýma	dlouhý	k2eAgNnPc7d1	dlouhé
31	[number]	k4	31
a	a	k8xC	a
34	[number]	k4	34
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
u	u	k7c2	u
mnohých	mnohé	k1gNnPc2	mnohé
plemen	plemeno	k1gNnPc2	plemeno
provádělo	provádět	k5eAaImAgNnS	provádět
kupírování	kupírování	k1gNnSc6	kupírování
uší	ucho	k1gNnPc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
kupírovala	kupírovat	k5eAaImAgNnP	kupírovat
pracovní	pracovní	k2eAgNnPc1d1	pracovní
plemena	plemeno	k1gNnPc1	plemeno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
hlídání	hlídání	k1gNnSc3	hlídání
stád	stádo	k1gNnPc2	stádo
i	i	k8xC	i
domů	dům	k1gInPc2	dům
před	před	k7c7	před
vlky	vlk	k1gMnPc7	vlk
a	a	k8xC	a
medvědy	medvěd	k1gMnPc7	medvěd
–	–	k?	–
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
odstraňováno	odstraňován	k2eAgNnSc1d1	odstraňován
"	"	kIx"	"
<g/>
slabé	slabý	k2eAgNnSc1d1	slabé
místo	místo	k1gNnSc1	místo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
šelma	šelma	k1gFnSc1	šelma
zakousnout	zakousnout	k5eAaPmF	zakousnout
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
kupírovaly	kupírovat	k5eAaImAgFnP	kupírovat
uši	ucho	k1gNnPc4	ucho
psům	pes	k1gMnPc3	pes
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
psí	psí	k2eAgInPc4d1	psí
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
některá	některý	k3yIgNnPc1	některý
plemena	plemeno	k1gNnPc1	plemeno
kupírovala	kupírovat	k5eAaImAgNnP	kupírovat
čistě	čistě	k6eAd1	čistě
z	z	k7c2	z
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
formátu	formát	k1gInSc2	formát
psa	pes	k1gMnSc2	pes
či	či	k8xC	či
linií	linie	k1gFnSc7	linie
jeho	jeho	k3xOp3gFnSc2	jeho
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
kupírování	kupírování	k1gNnSc1	kupírování
uší	ucho	k1gNnPc2	ucho
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
ČMKU	ČMKU	kA	ČMKU
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
vstup	vstup	k1gInSc1	vstup
kupírovaným	kupírovaný	k2eAgMnPc3d1	kupírovaný
jedincům	jedinec	k1gMnPc3	jedinec
na	na	k7c4	na
kynologické	kynologický	k2eAgFnPc4d1	kynologická
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
krk	krk	k1gInSc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
Spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
hřbetem	hřbet	k1gInSc7	hřbet
tvoří	tvořit	k5eAaImIp3nS	tvořit
kohoutek	kohoutek	k1gInSc1	kohoutek
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
lopatkovým	lopatkový	k2eAgInSc7d1	lopatkový
okrajem	okraj	k1gInSc7	okraj
převyšujícím	převyšující	k2eAgInSc7d1	převyšující
trnové	trnový	k2eAgFnPc4d1	Trnová
výběžky	výběžek	k1gInPc1	výběžek
prvních	první	k4xOgNnPc6	první
3-4	[number]	k4	3-4
hrudních	hrudní	k2eAgInPc2d1	hrudní
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Lopatka	lopatka	k1gFnSc1	lopatka
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
šikmá	šikmý	k2eAgFnSc1d1	šikmá
a	a	k8xC	a
s	s	k7c7	s
ramenní	ramenní	k2eAgFnSc7d1	ramenní
kostí	kost	k1gFnSc7	kost
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
svírat	svírat	k5eAaImF	svírat
úhel	úhel	k1gInSc4	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
-	-	kIx~	-
100	[number]	k4	100
<g/>
°	°	k?	°
a	a	k8xC	a
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
asi	asi	k9	asi
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ramenní	ramenní	k2eAgFnSc7d1	ramenní
a	a	k8xC	a
hrudní	hrudní	k2eAgFnSc7d1	hrudní
kostí	kost	k1gFnSc7	kost
tvoří	tvořit	k5eAaImIp3nS	tvořit
podklad	podklad	k1gInSc1	podklad
předhrudí	předhrudit	k5eAaPmIp3nS	předhrudit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
zaúhlení	zaúhlení	k1gNnSc6	zaúhlení
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
pažní	pažní	k2eAgFnPc1d1	pažní
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
hrudní	hrudní	k2eAgFnPc1d1	hrudní
končetiny	končetina	k1gFnPc1	končetina
umístěny	umístit	k5eAaPmNgFnP	umístit
na	na	k7c6	na
nejnižším	nízký	k2eAgInSc6d3	nejnižší
bodě	bod	k1gInSc6	bod
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
kolmice	kolmice	k1gFnSc1	kolmice
spuštěná	spuštěný	k2eAgFnSc1d1	spuštěná
od	od	k7c2	od
kohoutku	kohoutek	k1gInSc2	kohoutek
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
prochází	procházet	k5eAaImIp3nS	procházet
loktem	loket	k1gInSc7	loket
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnSc1	zakřivení
ramenní	ramenní	k2eAgFnSc2d1	ramenní
kosti	kost	k1gFnSc2	kost
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
klenutí	klenutí	k1gNnSc1	klenutí
žeber	žebro	k1gNnPc2	žebro
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgFnPc1d1	hrudní
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
umístěné	umístěný	k2eAgFnPc1d1	umístěná
rovně	roveň	k1gFnPc1	roveň
pod	pod	k7c7	pod
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nadprstí	Nadprstý	k2eAgMnPc1d1	Nadprstý
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
pružné	pružný	k2eAgNnSc1d1	pružné
a	a	k8xC	a
šikmé	šikmý	k2eAgNnSc1d1	šikmé
<g/>
,	,	kIx,	,
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
předloktím	předloktí	k1gNnSc7	předloktí
a	a	k8xC	a
nadprstím	nadprstí	k1gNnSc7	nadprstí
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
mezi	mezi	k7c7	mezi
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slabém	slabý	k2eAgInSc6d1	slabý
nadprstí	nadprstý	k2eAgMnPc1d1	nadprstý
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vytáčení	vytáčení	k1gNnSc3	vytáčení
tlapek	tlapka	k1gFnPc2	tlapka
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
strmé	strmý	k2eAgInPc1d1	strmý
nadprstí	nadprstý	k2eAgMnPc1d1	nadprstý
nedostatečně	dostatečně	k6eNd1	dostatečně
tlumí	tlumit	k5eAaImIp3nP	tlumit
nárazy	náraz	k1gInPc1	náraz
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
zátěži	zátěž	k1gFnSc3	zátěž
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrudních	hrudní	k2eAgFnPc6d1	hrudní
končetinách	končetina	k1gFnPc6	končetina
má	mít	k5eAaImIp3nS	mít
pes	pes	k1gMnSc1	pes
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
na	na	k7c4	na
pánevních	pánevní	k2eAgInPc2d1	pánevní
čtyři	čtyři	k4xCgInPc1	čtyři
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgInPc1d1	zakončený
tupými	tupý	k2eAgInPc7d1	tupý
drápy	dráp	k1gInPc7	dráp
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
jako	jako	k8xC	jako
tretry	tretra	k1gFnPc4	tretra
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
má	mít	k5eAaImIp3nS	mít
pes	pes	k1gMnSc1	pes
i	i	k9	i
na	na	k7c6	na
pánevních	pánevní	k2eAgFnPc6d1	pánevní
končetinách	končetina	k1gFnPc6	končetina
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
první	první	k4xOgInPc1	první
prsty	prst	k1gInPc1	prst
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
paspárky	paspárek	k1gInPc1	paspárek
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
vlčí	vlčí	k2eAgInPc1d1	vlčí
drápy	dráp	k1gInPc1	dráp
<g/>
,	,	kIx,	,
označené	označený	k2eAgInPc1d1	označený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vlci	vlk	k1gMnPc1	vlk
je	on	k3xPp3gMnPc4	on
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
plemen	plemeno	k1gNnPc2	plemeno
(	(	kIx(	(
<g/>
briard	briard	k1gMnSc1	briard
<g/>
,	,	kIx,	,
pyrenejský	pyrenejský	k2eAgMnSc1d1	pyrenejský
horský	horský	k2eAgMnSc1d1	horský
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
vyžadované	vyžadovaný	k2eAgInPc1d1	vyžadovaný
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
dvojité	dvojitý	k2eAgInPc1d1	dvojitý
paspárky	paspárek	k1gInPc1	paspárek
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
tlapky	tlapka	k1gFnSc2	tlapka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
využití	využití	k1gNnSc4	využití
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Kočičí	kočičí	k2eAgFnSc1d1	kočičí
tlapka	tlapka	k1gFnSc1	tlapka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
psovi	pes	k1gMnSc3	pes
větší	veliký	k2eAgFnSc1d2	veliký
vytrvalost	vytrvalost	k1gFnSc1	vytrvalost
<g/>
,	,	kIx,	,
zaječí	zaječí	k2eAgFnSc1d1	zaječí
tlapka	tlapka	k1gFnSc1	tlapka
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc4	rychlost
psa	pes	k1gMnSc2	pes
a	a	k8xC	a
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
kličkování	kličkování	k1gNnSc4	kličkování
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
bývá	bývat	k5eAaImIp3nS	bývat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgFnSc1d1	sahající
k	k	k7c3	k
loktům	loket	k1gInPc3	loket
–	–	k?	–
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
mělký	mělký	k2eAgInSc1d1	mělký
<g/>
,	,	kIx,	,
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
tolik	tolik	k6eAd1	tolik
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
plíce	plíce	k1gFnPc1	plíce
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
hlubším	hluboký	k2eAgInSc7d2	hlubší
hrudníkem	hrudník	k1gInSc7	hrudník
působí	působit	k5eAaImIp3nS	působit
těžkopádně	těžkopádně	k6eAd1	těžkopádně
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
lehce	lehko	k6eAd1	lehko
vtažené	vtažený	k2eAgNnSc1d1	vtažené
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
vtažené	vtažený	k2eAgNnSc1d1	vtažené
břicho	břicho	k1gNnSc1	břicho
se	se	k3xPyFc4	se
požaduje	požadovat	k5eAaImIp3nS	požadovat
u	u	k7c2	u
chrtů	chrt	k1gMnPc2	chrt
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
novofundlandského	novofundlandský	k2eAgMnSc2d1	novofundlandský
psa	pes	k1gMnSc2	pes
či	či	k8xC	či
výmarského	výmarský	k2eAgMnSc4d1	výmarský
ohaře	ohař	k1gMnSc4	ohař
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
břicho	břicho	k1gNnSc4	břicho
rovné	rovný	k2eAgNnSc4d1	rovné
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
rovný	rovný	k2eAgInSc4d1	rovný
a	a	k8xC	a
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
žádoucí	žádoucí	k2eAgNnPc1d1	žádoucí
jsou	být	k5eAaImIp3nP	být
spíš	spíš	k9	spíš
kratší	krátký	k2eAgNnPc4d2	kratší
bedra	bedra	k1gNnPc4	bedra
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dobře	dobře	k6eAd1	dobře
přenáší	přenášet	k5eAaImIp3nP	přenášet
sílu	síla	k1gFnSc4	síla
pohybu	pohyb	k1gInSc2	pohyb
ze	z	k7c2	z
zadních	zadní	k2eAgFnPc2d1	zadní
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
zaúhlení	zaúhlení	k1gNnSc6	zaúhlení
pánevních	pánevní	k2eAgFnPc2d1	pánevní
končetin	končetina	k1gFnPc2	končetina
by	by	kYmCp3nS	by
pánev	pánev	k1gFnSc1	pánev
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
skloněná	skloněný	k2eAgFnSc1d1	skloněná
oproti	oproti	k7c3	oproti
horizontální	horizontální	k2eAgFnSc3d1	horizontální
linii	linie	k1gFnSc3	linie
asi	asi	k9	asi
o	o	k7c6	o
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
v	v	k7c6	v
kolenním	kolenní	k2eAgInSc6d1	kolenní
kloubu	kloub	k1gInSc6	kloub
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
úhel	úhel	k1gInSc1	úhel
125	[number]	k4	125
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
správný	správný	k2eAgInSc1d1	správný
úhel	úhel	k1gInSc1	úhel
v	v	k7c6	v
hlezenním	hlezenní	k2eAgInSc6d1	hlezenní
kloubu	kloub	k1gInSc6	kloub
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
130	[number]	k4	130
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Hlezno	hlezno	k1gNnSc1	hlezno
je	být	k5eAaImIp3nS	být
postavené	postavený	k2eAgNnSc1d1	postavené
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
svislé	svislý	k2eAgFnPc1d1	svislá
osy	osa	k1gFnPc1	osa
končetin	končetina	k1gFnPc2	končetina
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obecné	obecný	k2eAgFnPc1d1	obecná
zásady	zásada	k1gFnPc1	zásada
však	však	k9	však
neplatí	platit	k5eNaImIp3nP	platit
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
špicové	špic	k1gMnPc1	špic
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
pánevních	pánevní	k2eAgFnPc6d1	pánevní
končetinách	končetina	k1gFnPc6	končetina
strmé	strmý	k2eAgNnSc1d1	strmé
úhlení	úhlení	k1gNnSc1	úhlení
–	–	k?	–
úhly	úhel	k1gInPc7	úhel
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
popisované	popisovaný	k2eAgInPc1d1	popisovaný
jako	jako	k8xS	jako
ideál	ideál	k1gInSc1	ideál
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přeúhlený	přeúhlený	k2eAgMnSc1d1	přeúhlený
<g/>
,	,	kIx,	,
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
v	v	k7c6	v
kolenním	kolenní	k2eAgInSc6d1	kolenní
kloubu	kloub	k1gInSc6	kloub
blízkému	blízký	k2eAgInSc3d1	blízký
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgFnPc1d1	Krátké
končetiny	končetina	k1gFnPc1	končetina
jezevčíků	jezevčík	k1gMnPc2	jezevčík
či	či	k8xC	či
baseta	baseta	k1gFnSc1	baseta
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
achondroplazií	achondroplazie	k1gFnSc7	achondroplazie
<g/>
,	,	kIx,	,
poruchou	porucha	k1gFnSc7	porucha
osifikace	osifikace	k1gFnSc2	osifikace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
trpasličí	trpasličí	k2eAgInSc4d1	trpasličí
vzrůst	vzrůst	k1gInSc4	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnSc1d1	nosní
houba	houba	k1gFnSc1	houba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
pigmentovaná	pigmentovaný	k2eAgFnSc1d1	pigmentovaná
a	a	k8xC	a
nozdry	nozdra	k1gFnPc1	nozdra
dostatečně	dostatečně	k6eAd1	dostatečně
široké	široký	k2eAgFnPc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnSc1d1	nosní
houba	houba	k1gFnSc1	houba
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
chladná	chladný	k2eAgFnSc1d1	chladná
a	a	k8xC	a
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
,	,	kIx,	,
zvlhčována	zvlhčován	k2eAgFnSc1d1	zvlhčována
sekretem	sekret	k1gInSc7	sekret
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
pach	pach	k1gInSc4	pach
nesoucí	nesoucí	k2eAgFnSc2d1	nesoucí
molekuly	molekula	k1gFnSc2	molekula
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k9	tak
jejich	jejich	k3xOp3gNnSc3	jejich
zachycení	zachycení	k1gNnSc3	zachycení
a	a	k8xC	a
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
psího	psí	k2eAgInSc2d1	psí
nosu	nos	k1gInSc2	nos
jsou	být	k5eAaImIp3nP	být
stočené	stočený	k2eAgFnPc4d1	stočená
nosní	nosní	k2eAgFnPc4d1	nosní
skořepy	skořepa	k1gFnPc4	skořepa
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
čichovou	čichový	k2eAgFnSc7d1	čichová
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
plocha	plocha	k1gFnSc1	plocha
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
70	[number]	k4	70
do	do	k7c2	do
200	[number]	k4	200
cm2	cm2	k4	cm2
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
plocha	plocha	k1gFnSc1	plocha
čichové	čichový	k2eAgFnSc2d1	čichová
sliznice	sliznice	k1gFnSc2	sliznice
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
s	s	k7c7	s
delší	dlouhý	k2eAgFnSc7d2	delší
čenichovou	čenichová	k1gFnSc7	čenichová
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
u	u	k7c2	u
krátkolebých	krátkolebý	k2eAgNnPc2d1	krátkolebé
plemen	plemeno	k1gNnPc2	plemeno
je	být	k5eAaImIp3nS	být
menšího	malý	k2eAgInSc2d2	menší
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Sliznice	sliznice	k1gFnSc1	sliznice
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
0,10	[number]	k4	0,10
<g/>
–	–	k?	–
<g/>
0,12	[number]	k4	0,12
mm	mm	kA	mm
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
220	[number]	k4	220
miliónů	milión	k4xCgInPc2	milión
čichových	čichový	k2eAgFnPc2d1	čichová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
pouhých	pouhý	k2eAgInPc2d1	pouhý
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
čichových	čichový	k2eAgInPc2d1	čichový
receptorů	receptor	k1gInPc2	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Čichové	čichový	k2eAgInPc1d1	čichový
laloky	lalok	k1gInPc1	lalok
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
část	část	k1gFnSc1	část
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zpracovávány	zpracováván	k2eAgInPc4d1	zpracováván
čichové	čichový	k2eAgInPc4d1	čichový
vjemy	vjem	k1gInPc4	vjem
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
60	[number]	k4	60
g	g	kA	g
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
4	[number]	k4	4
<g/>
×	×	k?	×
méně	málo	k6eAd2	málo
–	–	k?	–
přitom	přitom	k6eAd1	přitom
mozek	mozek	k1gInSc1	mozek
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
10	[number]	k4	10
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
psí	psí	k2eAgInSc4d1	psí
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
je	být	k5eAaImIp3nS	být
nejvyvinutější	vyvinutý	k2eAgInSc4d3	nejvyvinutější
a	a	k8xC	a
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
smysl	smysl	k1gInSc4	smysl
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
citlivější	citlivý	k2eAgInSc1d2	citlivější
než	než	k8xS	než
čich	čich	k1gInSc1	čich
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plemen	plemeno	k1gNnPc2	plemeno
s	s	k7c7	s
nejvyvinutějším	vyvinutý	k2eAgInSc7d3	nejvyvinutější
čichem	čich	k1gInSc7	čich
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
bloodhoundi	bloodhound	k1gMnPc1	bloodhound
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
hledat	hledat	k5eAaImF	hledat
podle	podle	k7c2	podle
pachové	pachový	k2eAgFnSc2d1	pachová
stopy	stopa	k1gFnSc2	stopa
staré	starý	k2eAgNnSc1d1	staré
i	i	k9	i
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Čenich	čenich	k1gInSc1	čenich
a	a	k8xC	a
nos	nos	k1gInSc1	nos
pes	pes	k1gMnSc1	pes
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
i	i	k9	i
při	při	k7c6	při
vyhrabávání	vyhrabávání	k1gNnSc6	vyhrabávání
možné	možný	k2eAgFnSc2d1	možná
kořisti	kořist	k1gFnSc2	kořist
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zahrnování	zahrnování	k1gNnSc1	zahrnování
zeminy	zemina	k1gFnSc2	zemina
při	při	k7c6	při
zahrabávání	zahrabávání	k1gNnSc6	zahrabávání
kostí	kost	k1gFnPc2	kost
nebo	nebo	k8xC	nebo
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnPc4d1	psí
oči	oko	k1gNnPc4	oko
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
očím	oko	k1gNnPc3	oko
lidským	lidský	k2eAgNnPc3d1	lidské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
plošší	plochý	k2eAgNnSc4d2	plošší
než	než	k8xS	než
lidské	lidský	k2eAgNnSc4d1	lidské
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
čočka	čočka	k1gFnSc1	čočka
nemůže	moct	k5eNaImIp3nS	moct
zaostřovat	zaostřovat	k5eAaImF	zaostřovat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
jako	jako	k8xS	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
netvoří	tvořit	k5eNaImIp3nS	tvořit
tak	tak	k6eAd1	tak
ostrý	ostrý	k2eAgInSc1d1	ostrý
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
dalekozrací	dalekozraký	k2eAgMnPc1d1	dalekozraký
<g/>
,	,	kIx,	,
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
nevidí	vidět	k5eNaImIp3nS	vidět
ostře	ostro	k6eAd1	ostro
a	a	k8xC	a
k	k	k7c3	k
prozkoumání	prozkoumání	k1gNnSc3	prozkoumání
blízkých	blízký	k2eAgInPc2d1	blízký
předmětů	předmět	k1gInPc2	předmět
používají	používat	k5eAaImIp3nP	používat
hlavně	hlavně	k9	hlavně
čich	čich	k1gInSc4	čich
a	a	k8xC	a
hmat	hmat	k1gInSc4	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mají	mít	k5eAaImIp3nP	mít
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
šelmy	šelma	k1gFnPc1	šelma
<g/>
,	,	kIx,	,
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
za	za	k7c7	za
sítnicí	sítnice	k1gFnSc7	sítnice
tapetum	tapetum	k1gNnSc1	tapetum
lucidum	lucidum	k1gInSc4	lucidum
<g/>
,	,	kIx,	,
odrazivou	odrazivý	k2eAgFnSc4d1	odrazivá
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dobré	dobrý	k2eAgNnSc4d1	dobré
vidění	vidění	k1gNnSc4	vidění
i	i	k9	i
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
<g/>
,	,	kIx,	,
psí	psí	k2eAgNnPc1d1	psí
oči	oko	k1gNnPc1	oko
(	(	kIx(	(
<g/>
a	a	k8xC	a
odpovídající	odpovídající	k2eAgNnSc1d1	odpovídající
zrakové	zrakový	k2eAgNnSc1d1	zrakové
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
citlivější	citlivý	k2eAgInPc1d2	citlivější
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgFnSc1d1	psí
sítnice	sítnice	k1gFnSc1	sítnice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
světločivných	světločivný	k2eAgFnPc2d1	světločivná
buněk	buňka	k1gFnPc2	buňka
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vnímání	vnímání	k1gNnSc4	vnímání
kontrastu	kontrast	k1gInSc2	kontrast
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
zorničku	zornička	k1gFnSc4	zornička
a	a	k8xC	a
oči	oko	k1gNnPc4	oko
položené	položená	k1gFnSc2	položená
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
více	hodně	k6eAd2	hodně
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgNnSc4d2	lepší
periferní	periferní	k2eAgNnSc4d1	periferní
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
vidět	vidět	k5eAaImF	vidět
žádné	žádný	k3yNgFnPc4	žádný
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	on	k3xPp3gInPc4	on
vnímají	vnímat	k5eAaImIp3nP	vnímat
jako	jako	k9	jako
odstíny	odstín	k1gInPc1	odstín
žluté	žlutý	k2eAgInPc1d1	žlutý
<g/>
,	,	kIx,	,
červenou	červená	k1gFnSc4	červená
pak	pak	k6eAd1	pak
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
hnědošedou	hnědošedý	k2eAgFnSc4d1	hnědošedá
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc4d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Vnímají	vnímat	k5eAaImIp3nP	vnímat
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerozliší	rozlišit	k5eNaPmIp3nP	rozlišit
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
fialové	fialový	k2eAgFnSc2d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
vada	vada	k1gFnSc1	vada
vidění	vidění	k1gNnSc2	vidění
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
deuteranopie	deuteranopie	k1gFnSc1	deuteranopie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
sítnici	sítnice	k1gFnSc6	sítnice
oka	oko	k1gNnSc2	oko
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
světlocitlivých	světlocitlivý	k2eAgMnPc2d1	světlocitlivý
receptorů	receptor	k1gMnPc2	receptor
–	–	k?	–
čípků	čípek	k1gInPc2	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
u	u	k7c2	u
primátů	primát	k1gMnPc2	primát
a	a	k8xC	a
který	který	k3yQgMnSc1	který
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
rozeznávání	rozeznávání	k1gNnSc3	rozeznávání
zralých	zralý	k2eAgInPc2d1	zralý
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
psi	pes	k1gMnPc1	pes
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
sítnice	sítnice	k1gFnSc1	sítnice
psího	psí	k2eAgNnSc2d1	psí
oka	oko	k1gNnSc2	oko
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
druhého	druhý	k4xOgInSc2	druhý
typu	typ	k1gInSc2	typ
fotoreceptorů	fotoreceptor	k1gMnPc2	fotoreceptor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sice	sice	k8xC	sice
téměř	téměř	k6eAd1	téměř
nerozeznávají	rozeznávat	k5eNaImIp3nP	rozeznávat
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
citlivější	citlivý	k2eAgFnPc1d2	citlivější
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
tak	tak	k9	tak
dokáží	dokázat	k5eAaPmIp3nP	dokázat
rozeznat	rozeznat	k5eAaPmF	rozeznat
více	hodně	k6eAd2	hodně
stupňů	stupeň	k1gInPc2	stupeň
šedi	šeď	k1gFnSc2	šeď
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
složení	složení	k1gNnSc1	složení
fotoreceptorů	fotoreceptor	k1gInPc2	fotoreceptor
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
psi	pes	k1gMnPc1	pes
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
se	se	k3xPyFc4	se
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
citlivější	citlivý	k2eAgInSc4d2	citlivější
zrak	zrak	k1gInSc4	zrak
i	i	k9	i
při	při	k7c6	při
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
slyšení	slyšení	k1gNnSc2	slyšení
zdravého	zdravý	k2eAgMnSc4d1	zdravý
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
frekvenční	frekvenční	k2eAgInSc4d1	frekvenční
rozsah	rozsah	k1gInSc4	rozsah
který	který	k3yQgMnSc1	který
pes	pes	k1gMnSc1	pes
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
vnímají	vnímat	k5eAaImIp3nP	vnímat
zvuk	zvuk	k1gInSc4	zvuk
o	o	k7c6	o
kmitočtech	kmitočet	k1gInPc6	kmitočet
40	[number]	k4	40
Hz	Hz	kA	Hz
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
nižší	nízký	k2eAgFnPc4d2	nižší
frekvence	frekvence	k1gFnPc4	frekvence
vnímají	vnímat	k5eAaImIp3nP	vnímat
psi	pes	k1gMnPc1	pes
citlivěji	citlivě	k6eAd2	citlivě
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sluchové	sluchový	k2eAgFnPc1d1	sluchová
schopnosti	schopnost	k1gFnPc1	schopnost
psů	pes	k1gMnPc2	pes
dále	daleko	k6eAd2	daleko
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
jejich	jejich	k3xOp3gInSc1	jejich
ušní	ušní	k2eAgInSc1d1	ušní
boltec	boltec	k1gInSc1	boltec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
směrovat	směrovat	k5eAaImF	směrovat
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlny	vlna	k1gFnPc4	vlna
do	do	k7c2	do
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnPc1d1	psí
plemena	plemeno	k1gNnPc1	plemeno
se	s	k7c7	s
vztyčenýma	vztyčený	k2eAgNnPc7d1	vztyčené
ušima	ucho	k1gNnPc7	ucho
dokáží	dokázat	k5eAaPmIp3nP	dokázat
podvědomě	podvědomě	k6eAd1	podvědomě
napnout	napnout	k5eAaPmF	napnout
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
otočit	otočit	k5eAaPmF	otočit
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
ušních	ušní	k2eAgInPc2d1	ušní
boltců	boltec	k1gInPc2	boltec
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
šíření	šíření	k1gNnSc3	šíření
poslouchaného	poslouchaný	k2eAgInSc2d1	poslouchaný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Srst	srst	k1gFnSc4	srst
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
psí	psí	k2eAgFnSc4d1	psí
srst	srst	k1gFnSc4	srst
do	do	k7c2	do
několika	několik	k4yIc2	několik
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
srst	srst	k1gFnSc1	srst
s	s	k7c7	s
podsadou	podsada	k1gFnSc7	podsada
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hustou	hustý	k2eAgFnSc7d1	hustá
kratší	krátký	k2eAgFnSc7d2	kratší
podsadou	podsada	k1gFnSc7	podsada
a	a	k8xC	a
pevnými	pevný	k2eAgInPc7d1	pevný
pesíky	pesík	k1gInPc7	pesík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
<g/>
.	.	kIx.	.
</s>
<s>
Hladká	hladký	k2eAgFnSc1d1	hladká
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
krátkými	krátký	k2eAgInPc7d1	krátký
a	a	k8xC	a
přiléhajícími	přiléhající	k2eAgInPc7d1	přiléhající
chlupy	chlup	k1gInPc7	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
jemné	jemný	k2eAgInPc4d1	jemný
pesíky	pesík	k1gInPc4	pesík
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
podsady	podsada	k1gFnSc2	podsada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
jemnou	jemný	k2eAgFnSc7d1	jemná
podsadou	podsada	k1gFnSc7	podsada
<g/>
.	.	kIx.	.
</s>
<s>
Náročná	náročný	k2eAgNnPc1d1	náročné
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
plstnatí	plstnatý	k2eAgMnPc1d1	plstnatý
<g/>
.	.	kIx.	.
</s>
<s>
Kudrnatá	kudrnatý	k2eAgFnSc1d1	kudrnatá
nelínající	línající	k2eNgFnSc1d1	nelínající
srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
kudrnaté	kudrnatý	k2eAgInPc4d1	kudrnatý
jemné	jemný	k2eAgInPc4d1	jemný
pesíky	pesík	k1gInPc4	pesík
bez	bez	k7c2	bez
podsady	podsada	k1gFnSc2	podsada
<g/>
.	.	kIx.	.
</s>
<s>
Upravuje	upravovat	k5eAaImIp3nS	upravovat
se	se	k3xPyFc4	se
stříháním	stříhání	k1gNnSc7	stříhání
<g/>
.	.	kIx.	.
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hrubými	hrubý	k2eAgInPc7d1	hrubý
silnými	silný	k2eAgInPc7d1	silný
pesíky	pesík	k1gInPc7	pesík
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hustou	hustý	k2eAgFnSc7d1	hustá
podsadou	podsada	k1gFnSc7	podsada
<g/>
.	.	kIx.	.
</s>
<s>
Upravuje	upravovat	k5eAaImIp3nS	upravovat
se	se	k3xPyFc4	se
trimováním	trimování	k1gNnSc7	trimování
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
druh	druh	k1gInSc1	druh
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
plstnatět	plstnatět	k5eAaImF	plstnatět
a	a	k8xC	a
formuje	formovat	k5eAaImIp3nS	formovat
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
provazců	provazec	k1gInPc2	provazec
nebo	nebo	k8xC	nebo
ploten	plotna	k1gFnPc2	plotna
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
psi	pes	k1gMnPc1	pes
línají	línat	k5eAaImIp3nP	línat
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kudrnaté	kudrnatý	k2eAgFnSc2d1	kudrnatá
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc2d1	hrubá
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
ale	ale	k9	ale
odumřelé	odumřelý	k2eAgInPc1d1	odumřelý
chlupy	chlup	k1gInPc1	chlup
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
chlupech	chlup	k1gInPc6	chlup
a	a	k8xC	a
samy	sám	k3xTgFnPc1	sám
nevypadávají	vypadávat	k5eNaImIp3nP	vypadávat
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
pigment	pigment	k1gInSc1	pigment
melanin	melanin	k1gInSc1	melanin
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
jeho	jeho	k3xOp3gFnPc1	jeho
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
eumelanin	eumelanin	k2eAgInSc1d1	eumelanin
a	a	k8xC	a
hnědý	hnědý	k2eAgInSc1d1	hnědý
feomelanin	feomelanin	k2eAgInSc1d1	feomelanin
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
fenotypový	fenotypový	k2eAgInSc1d1	fenotypový
projev	projev	k1gInSc1	projev
účinku	účinek	k1gInSc2	účinek
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
řídí	řídit	k5eAaImIp3nS	řídit
druh	druh	k1gInSc1	druh
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
pigmentu	pigment	k1gInSc2	pigment
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
pigmentových	pigmentový	k2eAgFnPc2d1	pigmentová
granulí	granule	k1gFnPc2	granule
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
granulí	granule	k1gFnPc2	granule
v	v	k7c6	v
chlupu	chlup	k1gInSc6	chlup
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
rozvrstvení	rozvrstvení	k1gNnSc4	rozvrstvení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
asi	asi	k9	asi
12	[number]	k4	12
geny	gen	k1gInPc7	gen
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
lokusů	lokus	k1gInPc2	lokus
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
těchto	tento	k3xDgInPc2	tento
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stejný	stejný	k2eAgInSc1d1	stejný
fenotypový	fenotypový	k2eAgInSc1d1	fenotypový
projev	projev	k1gInSc1	projev
nemusí	muset	k5eNaImIp3nS	muset
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
stejného	stejný	k2eAgNnSc2d1	stejné
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
podobného	podobný	k2eAgInSc2d1	podobný
genotypu	genotyp	k1gInSc2	genotyp
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
černě	černě	k6eAd1	černě
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
černý	černý	k2eAgInSc4d1	černý
působením	působení	k1gNnSc7	působení
tzv.	tzv.	kA	tzv.
dominantní	dominantní	k2eAgNnSc1d1	dominantní
černé	černé	k1gNnSc1	černé
na	na	k7c6	na
lokusu	lokus	k1gInSc6	lokus
K	K	kA	K
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dominantní	dominantní	k2eAgFnSc4d1	dominantní
alelu	alela	k1gFnSc4	alela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
vznik	vznik	k1gInSc4	vznik
feomelaninu	feomelanina	k1gFnSc4	feomelanina
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
s	s	k7c7	s
genotypem	genotyp	k1gInSc7	genotyp
Kk	Kk	k1gFnSc2	Kk
a	a	k8xC	a
KK	KK	kA	KK
bude	být	k5eAaImBp3nS	být
černý	černý	k2eAgMnSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
stejnou	stejný	k2eAgFnSc4d1	stejná
barvu	barva	k1gFnSc4	barva
srsti	srst	k1gFnSc2	srst
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jedinec	jedinec	k1gMnSc1	jedinec
kk	kk	k?	kk
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
zároveň	zároveň	k6eAd1	zároveň
recesivním	recesivní	k2eAgInSc7d1	recesivní
homozygotem	homozygot	k1gInSc7	homozygot
v	v	k7c4	v
aguti-sérii	agutiérie	k1gFnSc4	aguti-série
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
aa	aa	k?	aa
kk	kk	k?	kk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
žluté	žlutý	k2eAgNnSc1d1	žluté
zbarvení	zbarvení	k1gNnSc1	zbarvení
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dáno	dát	k5eAaPmNgNnS	dát
působením	působení	k1gNnSc7	působení
genu	gen	k1gInSc2	gen
v	v	k7c6	v
aguti-sérii	agutiérie	k1gFnSc6	aguti-série
nebo	nebo	k8xC	nebo
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Extension	Extension	k1gInSc4	Extension
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zbarvení	zbarvení	k1gNnSc1	zbarvení
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
Genetika	genetik	k1gMnSc2	genetik
zbarvení	zbarvení	k1gNnSc2	zbarvení
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
dechů	dech	k1gInPc2	dech
u	u	k7c2	u
malého	malý	k2eAgMnSc2d1	malý
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
velkého	velký	k2eAgMnSc2d1	velký
psa	pes	k1gMnSc2	pes
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
psa	pes	k1gMnSc2	pes
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
10	[number]	k4	10
<g/>
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
psí	psí	k2eAgNnSc1d1	psí
srdce	srdce	k1gNnSc1	srdce
pak	pak	k6eAd1	pak
skoro	skoro	k6eAd1	skoro
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
savci	savec	k1gMnPc7	savec
<g/>
.	.	kIx.	.
</s>
<s>
Tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
u	u	k7c2	u
malých	malý	k2eAgNnPc2d1	malé
plemen	plemeno	k1gNnPc2	plemeno
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
90	[number]	k4	90
–	–	k?	–
120	[number]	k4	120
pulsy	puls	k1gInPc7	puls
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
plemen	plemeno	k1gNnPc2	plemeno
60	[number]	k4	60
–	–	k?	–
90	[number]	k4	90
pulsů	puls	k1gInPc2	puls
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
štěňat	štěně	k1gNnPc2	štěně
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgFnSc1d1	normální
tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
i	i	k9	i
200	[number]	k4	200
pulsů	puls	k1gInPc2	puls
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
teplokrevný	teplokrevný	k2eAgMnSc1d1	teplokrevný
živočich	živočich	k1gMnSc1	živočich
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
mezi	mezi	k7c7	mezi
37,5	[number]	k4	37,5
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
°	°	k?	°
<g/>
C.	C.	kA	C.
U	u	k7c2	u
štěňat	štěně	k1gNnPc2	štěně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
teplota	teplota	k1gFnSc1	teplota
až	až	k9	až
do	do	k7c2	do
39,5	[number]	k4	39,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Obecně	obecně	k6eAd1	obecně
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důsledek	důsledek	k1gInSc4	důsledek
stresu	stres	k1gInSc2	stres
<g/>
,	,	kIx,	,
strachu	strach	k1gInSc2	strach
nebo	nebo	k8xC	nebo
fyzické	fyzický	k2eAgFnSc2d1	fyzická
námahy	námaha	k1gFnSc2	námaha
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
pomocí	pomocí	k7c2	pomocí
teploměru	teploměr	k1gInSc2	teploměr
zasunutého	zasunutý	k2eAgInSc2d1	zasunutý
do	do	k7c2	do
konečníku	konečník	k1gInSc2	konečník
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
hodnocení	hodnocení	k1gNnSc1	hodnocení
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
či	či	k8xC	či
vlhkosti	vlhkost	k1gFnSc2	vlhkost
nosní	nosní	k2eAgFnSc2d1	nosní
houby	houba	k1gFnSc2	houba
není	být	k5eNaImIp3nS	být
směrodatné	směrodatný	k2eAgNnSc1d1	směrodatné
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc3	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
veku	veka	k1gFnSc4	veka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
rozmnožovaní	rozmnožovaný	k2eAgMnPc1d1	rozmnožovaný
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
krytí	krytí	k1gNnSc1	krytí
se	se	k3xPyFc4	se
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
hárání	hárání	k1gNnSc6	hárání
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
z	z	k7c2	z
vícerých	vícerý	k4xRyIgInPc2	vícerý
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
vývoj	vývoj	k1gInSc4	vývoj
kostry	kostra	k1gFnSc2	kostra
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgInSc1d1	související
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
je	být	k5eAaImIp3nS	být
pozastavení	pozastavení	k1gNnSc1	pozastavení
tělesného	tělesný	k2eAgInSc2d1	tělesný
vývoje	vývoj	k1gInSc2	vývoj
feny	fena	k1gFnSc2	fena
při	při	k7c6	při
březosti	březost	k1gFnSc6	březost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
hárání	hárání	k1gNnSc6	hárání
fena	fena	k1gFnSc1	fena
nemůže	moct	k5eNaImIp3nS	moct
zabřeznout	zabřeznout	k5eAaPmF	zabřeznout
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mylný	mylný	k2eAgInSc4d1	mylný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
hárání	hárání	k1gNnSc1	hárání
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jiný	jiný	k2eAgInSc4d1	jiný
průběh	průběh	k1gInSc4	průběh
než	než	k8xS	než
pozdější	pozdní	k2eAgNnSc4d2	pozdější
hárání	hárání	k1gNnSc4	hárání
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
byť	byť	k8xS	byť
i	i	k9	i
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
fena	fena	k1gFnSc1	fena
je	být	k5eAaImIp3nS	být
však	však	k9	však
schopná	schopný	k2eAgFnSc1d1	schopná
počít	počít	k5eAaPmF	počít
mladé	mladý	k2eAgNnSc4d1	mladé
<g/>
.	.	kIx.	.
</s>
<s>
Hárání	hárání	k1gNnPc1	hárání
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
proestrus	proestrus	k1gInSc1	proestrus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
ve	v	k7c4	v
vaječníkách	vaječníkách	k?	vaječníkách
folikuly	folikul	k1gInPc4	folikul
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zduření	zduření	k1gNnSc3	zduření
pochvy	pochva	k1gFnSc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
si	se	k3xPyFc3	se
zvyknou	zvyknout	k5eAaPmIp3nP	zvyknout
častěji	často	k6eAd2	často
močit	močit	k5eAaImF	močit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
přítulnější	přítulný	k2eAgFnPc1d2	přítulnější
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
světlečervený	světlečervený	k2eAgInSc1d1	světlečervený
krvavý	krvavý	k2eAgInSc1d1	krvavý
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
pochvy	pochva	k1gFnSc2	pochva
způsobený	způsobený	k2eAgInSc1d1	způsobený
praskáním	praskání	k1gNnSc7	praskání
cév	céva	k1gFnPc2	céva
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
další	další	k2eAgNnPc1d1	další
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
estrus	estrus	k1gInSc1	estrus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
období	období	k1gNnSc4	období
vlastní	vlastní	k2eAgFnSc2d1	vlastní
říje	říje	k1gFnSc2	říje
<g/>
.	.	kIx.	.
</s>
<s>
Praskají	praskat	k5eAaImIp3nP	praskat
folikuly	folikul	k1gInPc4	folikul
<g/>
,	,	kIx,	,
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
se	se	k3xPyFc4	se
vajíčka	vajíčko	k1gNnPc1	vajíčko
a	a	k8xC	a
výtok	výtok	k1gInSc1	výtok
se	se	k3xPyFc4	se
z	z	k7c2	z
červené	červená	k1gFnSc2	červená
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
estra	estra	k6eAd1	estra
je	být	k5eAaImIp3nS	být
fena	fena	k1gFnSc1	fena
ochotná	ochotný	k2eAgFnSc1d1	ochotná
kopulovat	kopulovat	k5eAaImF	kopulovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
feny	fena	k1gFnPc1	fena
si	se	k3xPyFc3	se
zvyknou	zvyknout	k5eAaPmIp3nP	zvyknout
být	být	k5eAaImF	být
neposlušné	poslušný	k2eNgInPc1d1	neposlušný
<g/>
,	,	kIx,	,
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
psí	psí	k2eAgFnSc4d1	psí
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
metestrus	metestrus	k1gInSc4	metestrus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
období	období	k1gNnSc1	období
konce	konec	k1gInSc2	konec
říje	říje	k1gFnSc2	říje
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vaječnících	vaječník	k1gInPc6	vaječník
vzniká	vznikat	k5eAaImIp3nS	vznikat
žluté	žlutý	k2eAgNnSc1d1	žluté
tělísko	tělísko	k1gNnSc1	tělísko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
hormonálně	hormonálně	k6eAd1	hormonálně
udržuje	udržovat	k5eAaImIp3nS	udržovat
březivost	březivost	k1gFnSc1	březivost
a	a	k8xC	a
sliznice	sliznice	k1gFnSc1	sliznice
dělohy	děloha	k1gFnSc2	děloha
je	být	k5eAaImIp3nS	být
připravená	připravený	k2eAgFnSc1d1	připravená
přijmout	přijmout	k5eAaPmF	přijmout
oplodněná	oplodněný	k2eAgNnPc4d1	oplodněné
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
krytí	krytí	k1gNnSc6	krytí
přechází	přecházet	k5eAaImIp3nS	přecházet
říje	říje	k1gFnSc1	říje
do	do	k7c2	do
březosti	březost	k1gFnSc2	březost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
krytí	krytí	k1gNnSc1	krytí
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
<g/>
,	,	kIx,	,
přejde	přejít	k5eAaPmIp3nS	přejít
fena	fena	k1gFnSc1	fena
do	do	k7c2	do
stádia	stádium	k1gNnSc2	stádium
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
anestrus	anestrus	k1gInSc1	anestrus
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
období	období	k1gNnSc1	období
říje	říje	k1gFnSc2	říje
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
tří	tři	k4xCgFnPc2	tři
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
diestrický	diestrický	k2eAgInSc4d1	diestrický
cyklus	cyklus	k1gInSc4	cyklus
–	–	k?	–
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
říje	říje	k1gFnSc1	říje
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
hárají	hárat	k5eAaImIp3nP	hárat
zpravidla	zpravidla	k6eAd1	zpravidla
začátkem	začátkem	k7c2	začátkem
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
(	(	kIx(	(
<g/>
krytí	krytí	k1gNnSc1	krytí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
reflexní	reflexní	k2eAgInSc1d1	reflexní
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
podmíněných	podmíněný	k2eAgInPc2d1	podmíněný
a	a	k8xC	a
nepodmíněných	podmíněný	k2eNgInPc2d1	nepodmíněný
reflexů	reflex	k1gInPc2	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
páření	páření	k1gNnSc6	páření
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zasunutí	zasunutí	k1gNnSc3	zasunutí
ztopořeného	ztopořený	k2eAgInSc2d1	ztopořený
údu	úd	k1gInSc2	úd
psa	pes	k1gMnSc2	pes
do	do	k7c2	do
pochvy	pochva	k1gFnSc2	pochva
feny	fena	k1gFnSc2	fena
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
páření	páření	k1gNnPc1	páření
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
předehra	předehra	k1gFnSc1	předehra
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
akt	akt	k1gInSc1	akt
páření	páření	k1gNnSc2	páření
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
svázání	svázání	k1gNnSc6	svázání
<g/>
.	.	kIx.	.
</s>
<s>
Předehra	předehra	k1gFnSc1	předehra
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
seznámení	seznámení	k1gNnSc3	seznámení
se	se	k3xPyFc4	se
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
pobíhají	pobíhat	k5eAaImIp3nP	pobíhat
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
si	se	k3xPyFc3	se
a	a	k8xC	a
fena	fena	k1gFnSc1	fena
hravě	hravě	k6eAd1	hravě
psovi	pes	k1gMnSc3	pes
uniká	unikat	k5eAaImIp3nS	unikat
při	při	k7c6	při
jeho	jeho	k3xOp3gInPc6	jeho
pokusech	pokus	k1gInPc6	pokus
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
páření	páření	k1gNnSc1	páření
začíná	začínat	k5eAaImIp3nS	začínat
plynulým	plynulý	k2eAgInSc7d1	plynulý
přechodem	přechod	k1gInSc7	přechod
z	z	k7c2	z
předešlé	předešlý	k2eAgFnSc2d1	předešlá
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Fena	fena	k1gFnSc1	fena
je	být	k5eAaImIp3nS	být
připravená	připravený	k2eAgFnSc1d1	připravená
přijmout	přijmout	k5eAaPmF	přijmout
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
rozkročí	rozkročit	k5eAaPmIp3nS	rozkročit
zadní	zadní	k2eAgFnPc4d1	zadní
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
ohne	ohnout	k5eAaPmIp3nS	ohnout
ocas	ocas	k1gInSc4	ocas
na	na	k7c4	na
bok	bok	k1gInSc4	bok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zasunutí	zasunutí	k1gNnSc3	zasunutí
částečně	částečně	k6eAd1	částečně
ztopořeného	ztopořený	k2eAgInSc2d1	ztopořený
penisu	penis	k1gInSc2	penis
do	do	k7c2	do
pochvy	pochva	k1gFnSc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Táto	táta	k1gMnSc5	táta
fáze	fáze	k1gFnPc4	fáze
trvá	trvat	k5eAaImIp3nS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
svázání	svázání	k1gNnSc2	svázání
<g/>
.	.	kIx.	.
</s>
<s>
Svázání	svázání	k1gNnSc1	svázání
je	být	k5eAaImIp3nS	být
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
enormnímu	enormní	k2eAgNnSc3d1	enormní
zvětšení	zvětšení	k1gNnSc3	zvětšení
penisu	penis	k1gInSc2	penis
(	(	kIx(	(
<g/>
především	především	k9	především
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
nazývající	nazývající	k2eAgMnSc1d1	nazývající
se	se	k3xPyFc4	se
uzel	uzel	k1gInSc1	uzel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zabezpečí	zabezpečit	k5eAaPmIp3nS	zabezpečit
přechod	přechod	k1gInSc4	přechod
dávky	dávka	k1gFnSc2	dávka
semene	semeno	k1gNnSc2	semeno
do	do	k7c2	do
pochvy	pochva	k1gFnSc2	pochva
a	a	k8xC	a
následně	následně	k6eAd1	následně
dělohy	děloha	k1gFnSc2	děloha
feny	fena	k1gFnSc2	fena
<g/>
.	.	kIx.	.
</s>
<s>
Ejakulace	ejakulace	k1gFnSc1	ejakulace
semene	semeno	k1gNnSc2	semeno
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
psa	pes	k1gMnSc2	pes
po	po	k7c6	po
kapkách	kapka	k1gFnPc6	kapka
v	v	k7c6	v
sekundových	sekundový	k2eAgInPc6d1	sekundový
až	až	k8xS	až
třísekundových	třísekundový	k2eAgInPc6d1	třísekundový
intervalech	interval	k1gInPc6	interval
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
průběhu	průběh	k1gInSc2	průběh
fáze	fáze	k1gFnSc2	fáze
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
frakcí	frakce	k1gFnPc2	frakce
–	–	k?	–
prostatické	prostatický	k2eAgFnSc2d1	prostatická
<g/>
,	,	kIx,	,
spermatické	spermatický	k2eAgFnSc2d1	spermatická
a	a	k8xC	a
opět	opět	k6eAd1	opět
prostatické	prostatický	k2eAgNnSc1d1	prostatický
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svázání	svázání	k1gNnSc2	svázání
přehodí	přehodit	k5eAaPmIp3nS	přehodit
pes	pes	k1gMnSc1	pes
zadní	zadní	k2eAgFnSc4d1	zadní
nohu	noha	k1gFnSc4	noha
přes	přes	k7c4	přes
hřbet	hřbet	k1gInSc4	hřbet
feny	fena	k1gFnSc2	fena
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
stát	stát	k5eAaImF	stát
se	s	k7c7	s
zadky	zadek	k1gInPc7	zadek
přitisknutými	přitisknutý	k2eAgInPc7d1	přitisknutý
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Fáze	fáze	k1gFnSc1	fáze
svázání	svázání	k1gNnSc2	svázání
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k6eAd1	až
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc1	ten
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
feny	fena	k1gFnSc2	fena
trvá	trvat	k5eAaImIp3nS	trvat
průměrně	průměrně	k6eAd1	průměrně
63	[number]	k4	63
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
feny	fena	k1gFnPc1	fena
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
březosti	březost	k1gFnSc2	březost
mění	měnit	k5eAaImIp3nS	měnit
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
mazlivější	mazlivý	k2eAgMnPc1d2	mazlivější
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
jedí	jíst	k5eAaImIp3nP	jíst
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
se	se	k3xPyFc4	se
zase	zase	k9	zase
projevuje	projevovat	k5eAaImIp3nS	projevovat
nechutenství	nechutenství	k1gNnSc4	nechutenství
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvětšení	zvětšení	k1gNnSc3	zvětšení
mléčných	mléčný	k2eAgFnPc2d1	mléčná
žláz	žláza	k1gFnPc2	žláza
a	a	k8xC	a
ke	k	k7c3	k
zduření	zduření	k1gNnSc3	zduření
pochvy	pochva	k1gFnSc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
si	se	k3xPyFc3	se
zvyknou	zvyknout	k5eAaPmIp3nP	zvyknout
omezit	omezit	k5eAaPmF	omezit
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
přijímají	přijímat	k5eAaImIp3nP	přijímat
méně	málo	k6eAd2	málo
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
v	v	k7c6	v
připravené	připravený	k2eAgFnSc6d1	připravená
porodní	porodní	k2eAgFnSc6d1	porodní
bedně	bedna	k1gFnSc6	bedna
si	se	k3xPyFc3	se
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
brloh	brloh	k1gInSc4	brloh
<g/>
.	.	kIx.	.
</s>
<s>
Porod	porod	k1gInSc1	porod
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
fázích	fág	k1gInPc6	fág
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
porodu	porod	k1gInSc2	porod
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nepokojem	nepokoj	k1gInSc7	nepokoj
feny	fena	k1gFnSc2	fena
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
míra	míra	k1gFnSc1	míra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
krčku	krček	k1gInSc2	krček
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
fáze	fáze	k1gFnSc1	fáze
porodu	porod	k1gInSc2	porod
začíná	začínat	k5eAaImIp3nS	začínat
vypuzením	vypuzení	k1gNnSc7	vypuzení
plodu	plod	k1gInSc2	plod
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
porodem	porod	k1gInSc7	porod
placenty	placenta	k1gFnSc2	placenta
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
placentách	placenta	k1gFnPc6	placenta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc4	první
plodový	plodový	k2eAgInSc4d1	plodový
obal	obal	k1gInSc4	obal
praská	praskat	k5eAaImIp3nS	praskat
už	už	k6eAd1	už
v	v	k7c6	v
porodních	porodní	k2eAgFnPc6d1	porodní
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
placenty	placenta	k1gFnSc2	placenta
vysvobozuje	vysvobozovat	k5eAaImIp3nS	vysvobozovat
matka	matka	k1gFnSc1	matka
štěně	štěně	k1gNnSc4	štěně
lízáním	lízání	k1gNnSc7	lízání
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
překousnutím	překousnutí	k1gNnSc7	překousnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pupeční	pupeční	k2eAgFnSc4d1	pupeční
šňůru	šňůra	k1gFnSc4	šňůra
taktéž	taktéž	k?	taktéž
fena	fena	k1gFnSc1	fena
přehryzne	přehryznout	k5eAaPmIp3nS	přehryznout
<g/>
.	.	kIx.	.
</s>
<s>
Štěňata	štěně	k1gNnPc1	štěně
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
hluchá	hluchý	k2eAgFnSc1d1	hluchá
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
sací	sací	k2eAgInSc4d1	sací
reflex	reflex	k1gInSc4	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
přesouvat	přesouvat	k5eAaImF	přesouvat
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
lezením	lezení	k1gNnSc7	lezení
<g/>
,	,	kIx,	,
neumí	umět	k5eNaImIp3nS	umět
se	se	k3xPyFc4	se
však	však	k9	však
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
12	[number]	k4	12
dnů	den	k1gInPc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
otevírají	otevírat	k5eAaImIp3nP	otevírat
oči	oko	k1gNnPc1	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
.	.	kIx.	.
dni	den	k1gInSc3	den
života	život	k1gInSc2	život
začínají	začínat	k5eAaImIp3nP	začínat
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
zvukové	zvukový	k2eAgInPc4d1	zvukový
a	a	k8xC	a
čichové	čichový	k2eAgInPc4d1	čichový
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
třetím	třetí	k4xOgInSc7	třetí
a	a	k8xC	a
dvanáctým	dvanáctý	k4xOgInSc7	dvanáctý
týdnem	týden	k1gInSc7	týden
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zdokonalují	zdokonalovat	k5eAaImIp3nP	zdokonalovat
smysly	smysl	k1gInPc1	smysl
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
byť	byť	k8xS	byť
sociálně	sociálně	k6eAd1	sociálně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
socializace	socializace	k1gFnSc2	socializace
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
navykání	navykání	k1gNnSc4	navykání
štěňat	štěně	k1gNnPc2	štěně
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
živočišné	živočišný	k2eAgInPc4d1	živočišný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
týdnem	týden	k1gInSc7	týden
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc4d1	vhodné
období	období	k1gNnSc4	období
na	na	k7c4	na
odchod	odchod	k1gInSc4	odchod
štěňat	štěně	k1gNnPc2	štěně
k	k	k7c3	k
novým	nový	k2eAgMnPc3d1	nový
majitelům	majitel	k1gMnPc3	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
až	až	k8xS	až
šestý	šestý	k4xOgInSc1	šestý
měsíc	měsíc	k1gInSc1	měsíc
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
ochotné	ochotný	k2eAgNnSc1d1	ochotné
a	a	k8xC	a
schopné	schopný	k2eAgNnSc1d1	schopné
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
šestým	šestý	k4xOgInSc7	šestý
měsícem	měsíc	k1gInSc7	měsíc
a	a	k8xC	a
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
přichází	přicházet	k5eAaImIp3nS	přicházet
psí	psí	k2eAgFnSc1d1	psí
puberta	puberta	k1gFnSc1	puberta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pro	pro	k7c4	pro
chovatele	chovatel	k1gMnPc4	chovatel
-	-	kIx~	-
začátečníky	začátečník	k1gMnPc4	začátečník
představuje	představovat	k5eAaImIp3nS	představovat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejhůře	zle	k6eAd3	zle
zvladatelných	zvladatelný	k2eAgNnPc2d1	zvladatelné
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pes	pes	k1gMnSc1	pes
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
propracovat	propracovat	k5eAaPmF	propracovat
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Ustalování	ustalování	k1gNnSc1	ustalování
charakterových	charakterový	k2eAgFnPc2d1	charakterová
vlastností	vlastnost	k1gFnPc2	vlastnost
psa	pes	k1gMnSc4	pes
probíhá	probíhat	k5eAaImIp3nS	probíhat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
třetího	třetí	k4xOgInSc2	třetí
roku	rok	k1gInSc2	rok
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
dospělého	dospělý	k2eAgMnSc2d1	dospělý
psa	pes	k1gMnSc2	pes
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
dáno	dán	k2eAgNnSc1d1	dáno
jeho	jeho	k3xOp3gNnSc4	jeho
geneticky	geneticky	k6eAd1	geneticky
danými	daný	k2eAgFnPc7d1	daná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
temperament	temperament	k1gInSc4	temperament
a	a	k8xC	a
instinkty	instinkt	k1gInPc4	instinkt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
modifikovány	modifikovat	k5eAaBmNgFnP	modifikovat
během	během	k7c2	během
socializace	socializace	k1gFnSc2	socializace
a	a	k8xC	a
učením	učení	k1gNnSc7	učení
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
mentálně	mentálně	k6eAd1	mentálně
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
dvou	dva	k4xCgMnPc2	dva
až	až	k9	až
dvouapůlletého	dvouapůlletý	k2eAgNnSc2d1	dvouapůlleté
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
racionálním	racionální	k2eAgInSc7d1	racionální
plánem	plán	k1gInSc7	plán
nezávislým	závislý	k2eNgInSc7d1	nezávislý
na	na	k7c6	na
vrozených	vrozený	k2eAgInPc6d1	vrozený
vzorcích	vzorec	k1gInPc6	vzorec
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
znaky	znak	k1gInPc1	znak
elementární	elementární	k2eAgFnSc2d1	elementární
rozumové	rozumový	k2eAgFnSc2d1	rozumová
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
se	se	k3xPyFc4	se
domácí	domácí	k2eAgMnSc1d1	domácí
pes	pes	k1gMnSc1	pes
nejvíce	nejvíce	k6eAd1	nejvíce
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
domestikace	domestikace	k1gFnSc2	domestikace
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojenou	spojený	k2eAgFnSc4d1	spojená
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
selekci	selekce	k1gFnSc4	selekce
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
silné	silný	k2eAgInPc1d1	silný
znaky	znak	k1gInPc1	znak
neotenie	neotenie	k1gFnSc2	neotenie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzorce	vzorec	k1gInPc1	vzorec
chování	chování	k1gNnSc2	chování
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vlčat	vlče	k1gNnPc2	vlče
<g/>
,	,	kIx,	,
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
až	až	k9	až
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
opouštět	opouštět	k5eAaImF	opouštět
svou	svůj	k3xOyFgFnSc4	svůj
smečku	smečka	k1gFnSc4	smečka
ani	ani	k8xC	ani
přebírat	přebírat	k5eAaImF	přebírat
vedení	vedení	k1gNnSc4	vedení
smečky	smečka	k1gFnSc2	smečka
stávající	stávající	k2eAgInSc1d1	stávající
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
dominantní	dominantní	k2eAgNnSc1d1	dominantní
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
lovecké	lovecký	k2eAgInPc1d1	lovecký
pudy	pud	k1gInPc1	pud
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
potlačeny	potlačen	k2eAgMnPc4d1	potlačen
a	a	k8xC	a
modifikovány	modifikován	k2eAgMnPc4d1	modifikován
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
vrozený	vrozený	k2eAgInSc4d1	vrozený
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
vyšší	vysoký	k2eAgFnSc2d2	vyšší
nervové	nervový	k2eAgFnSc2d1	nervová
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
temperament	temperament	k1gInSc4	temperament
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
projev	projev	k1gInSc4	projev
podmíněných	podmíněný	k2eAgInPc2d1	podmíněný
i	i	k8xC	i
nepodmíněných	podmíněný	k2eNgInPc2d1	nepodmíněný
reflexů	reflex	k1gInPc2	reflex
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
míru	míra	k1gFnSc4	míra
vzrušivosti	vzrušivost	k1gFnSc2	vzrušivost
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc4	způsob
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
trvání	trvání	k1gNnSc2	trvání
této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
ochotu	ochota	k1gFnSc4	ochota
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
přizpůsobivost	přizpůsobivost	k1gFnSc1	přizpůsobivost
k	k	k7c3	k
podmínkám	podmínka	k1gFnPc3	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
i	i	k8xC	i
emoce	emoce	k1gFnSc2	emoce
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
vyšší	vysoký	k2eAgFnSc2d2	vyšší
nervové	nervový	k2eAgFnSc2d1	nervová
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
základní	základní	k2eAgFnSc6d1	základní
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
řady	řada	k1gFnSc2	řada
mezitypů	mezityp	k1gInPc2	mezityp
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
sangvinik	sangvinik	k1gMnSc1	sangvinik
je	být	k5eAaImIp3nS	být
ideálním	ideální	k2eAgMnSc7d1	ideální
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
vyrovnané	vyrovnaný	k2eAgMnPc4d1	vyrovnaný
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zbrklý	zbrklý	k2eAgMnSc1d1	zbrklý
<g/>
.	.	kIx.	.
</s>
<s>
Nenechá	nechat	k5eNaPmIp3nS	nechat
se	se	k3xPyFc4	se
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
k	k	k7c3	k
extrémním	extrémní	k2eAgNnPc3d1	extrémní
agresivním	agresivní	k2eAgNnPc3d1	agresivní
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
obranným	obranný	k2eAgFnPc3d1	obranná
reakcím	reakce	k1gFnPc3	reakce
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
radostně	radostně	k6eAd1	radostně
a	a	k8xC	a
ochotně	ochotně	k6eAd1	ochotně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přizpůsobivý	přizpůsobivý	k2eAgMnSc1d1	přizpůsobivý
<g/>
,	,	kIx,	,
nebojácný	bojácný	k2eNgMnSc1d1	nebojácný
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
se	se	k3xPyFc4	se
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
a	a	k8xC	a
cvičí	cvičit	k5eAaImIp3nS	cvičit
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
a	a	k8xC	a
pomalý	pomalý	k2eAgInSc1d1	pomalý
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
flegmatik	flegmatik	k1gMnSc1	flegmatik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
a	a	k8xC	a
těžkopádnější	těžkopádný	k2eAgMnSc1d2	těžkopádnější
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
výcvik	výcvik	k1gInSc1	výcvik
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
reflexy	reflex	k1gInPc1	reflex
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgNnSc1d2	pomalejší
střídání	střídání	k1gNnSc1	střídání
vzruchu	vzruch	k1gInSc2	vzruch
a	a	k8xC	a
útlumu	útlum	k1gInSc3	útlum
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přátelské	přátelský	k2eAgInPc4d1	přátelský
a	a	k8xC	a
nekonfliktní	konfliktní	k2eNgMnPc4d1	nekonfliktní
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
flegmatik	flegmatik	k1gMnSc1	flegmatik
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgMnSc1d1	ideální
společník	společník	k1gMnSc1	společník
do	do	k7c2	do
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Nevyrovnaný	vyrovnaný	k2eNgMnSc1d1	nevyrovnaný
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
dráždivý	dráždivý	k2eAgMnSc1d1	dráždivý
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
cholerik	cholerik	k1gMnSc1	cholerik
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
převažují	převažovat	k5eAaImIp3nP	převažovat
reflexy	reflex	k1gInPc4	reflex
vzruchu	vzruch	k1gInSc2	vzruch
nad	nad	k7c7	nad
reflexy	reflex	k1gInPc7	reflex
útlumu	útlum	k1gInSc2	útlum
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
jako	jako	k9	jako
společník	společník	k1gMnSc1	společník
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodný	vhodný	k2eAgInSc1d1	vhodný
<g/>
.	.	kIx.	.
</s>
<s>
Vyniká	vynikat	k5eAaImIp3nS	vynikat
hbitostí	hbitost	k1gFnSc7	hbitost
a	a	k8xC	a
neúnavností	neúnavnost	k1gFnSc7	neúnavnost
a	a	k8xC	a
typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
aktivní	aktivní	k2eAgFnSc1d1	aktivní
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
agresivitě	agresivita	k1gFnSc3	agresivita
a	a	k8xC	a
dominantnímu	dominantní	k2eAgNnSc3d1	dominantní
chování	chování	k1gNnSc3	chování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
psovoda	psovod	k1gMnSc2	psovod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
pracovním	pracovní	k2eAgMnSc7d1	pracovní
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgInPc1d1	vynikající
v	v	k7c6	v
dynamických	dynamický	k2eAgInPc6d1	dynamický
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
obrana	obrana	k1gFnSc1	obrana
nebo	nebo	k8xC	nebo
agility	agilita	k1gFnPc1	agilita
<g/>
,	,	kIx,	,
při	při	k7c6	při
špatném	špatný	k2eAgNnSc6d1	špatné
vedení	vedení	k1gNnSc6	vedení
se	se	k3xPyFc4	se
z	z	k7c2	z
takového	takový	k3xDgMnSc2	takový
jedince	jedinec	k1gMnSc2	jedinec
snadno	snadno	k6eAd1	snadno
stane	stanout	k5eAaPmIp3nS	stanout
nezvládnutelný	zvládnutelný	k2eNgMnSc1d1	nezvládnutelný
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
melancholik	melancholik	k1gMnSc1	melancholik
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
typem	typ	k1gInSc7	typ
nervové	nervový	k2eAgFnSc2d1	nervová
činností	činnost	k1gFnSc7	činnost
je	být	k5eAaImIp3nS	být
plachý	plachý	k2eAgInSc1d1	plachý
až	až	k6eAd1	až
bázlivý	bázlivý	k2eAgInSc1d1	bázlivý
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
se	se	k3xPyFc4	se
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
silným	silný	k2eAgInPc3d1	silný
podnětům	podnět	k1gInPc3	podnět
a	a	k8xC	a
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
situacích	situace	k1gFnPc6	situace
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
nedostatečný	dostatečný	k2eNgInSc1d1	nedostatečný
útlum	útlum	k1gInSc1	útlum
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
až	až	k9	až
hystericky	hystericky	k6eAd1	hystericky
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
labilní	labilní	k2eAgMnPc1d1	labilní
<g/>
,	,	kIx,	,
lekaví	lekavý	k2eAgMnPc1d1	lekavý
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
pasivní	pasivní	k2eAgFnSc1d1	pasivní
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
to	ten	k3xDgNnSc4	ten
bývají	bývat	k5eAaImIp3nP	bývat
uštěkaní	uštěkaný	k2eAgMnPc1d1	uštěkaný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
tiší	tichý	k2eAgMnPc1d1	tichý
a	a	k8xC	a
plaší	plachý	k2eAgMnPc1d1	plachý
psi	pes	k1gMnPc1	pes
vykazující	vykazující	k2eAgFnSc2d1	vykazující
známky	známka	k1gFnSc2	známka
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
žárlivé	žárlivý	k2eAgFnSc3d1	žárlivá
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
majiteli	majitel	k1gMnSc6	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
důkladně	důkladně	k6eAd1	důkladně
socializaci	socializace	k1gFnSc4	socializace
během	během	k7c2	během
štěněcího	štěněcí	k2eAgInSc2d1	štěněcí
věku	věk	k1gInSc2	věk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
melancholik	melancholik	k1gMnSc1	melancholik
dobrým	dobrý	k2eAgMnSc7d1	dobrý
společníkem	společník	k1gMnSc7	společník
v	v	k7c6	v
tiché	tichý	k2eAgFnSc6d1	tichá
rodině	rodina	k1gFnSc6	rodina
nebo	nebo	k8xC	nebo
u	u	k7c2	u
osaměle	osaměle	k6eAd1	osaměle
žijícího	žijící	k2eAgMnSc2d1	žijící
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
se	se	k3xPyFc4	se
ale	ale	k9	ale
nehodí	hodit	k5eNaPmIp3nS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Etogram	Etogram	k1gInSc1	Etogram
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
popis	popis	k1gInSc1	popis
inventáře	inventář	k1gInSc2	inventář
vrozeného	vrozený	k2eAgNnSc2d1	vrozené
chování	chování	k1gNnSc2	chování
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
souhrn	souhrn	k1gInSc4	souhrn
funkčních	funkční	k2eAgInPc2d1	funkční
okruhů	okruh	k1gInPc2	okruh
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
má	mít	k5eAaImIp3nS	mít
pes	pes	k1gMnSc1	pes
vrozené	vrozený	k2eAgFnSc2d1	vrozená
<g/>
,	,	kIx,	,
instinktivní	instinktivní	k2eAgMnSc1d1	instinktivní
<g/>
.	.	kIx.	.
</s>
<s>
Lovecké	lovecký	k2eAgNnSc1d1	lovecké
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
funkční	funkční	k2eAgInSc4d1	funkční
okruh	okruh	k1gInSc4	okruh
chování	chování	k1gNnSc2	chování
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
nejvíc	hodně	k6eAd3	hodně
změněn	změnit	k5eAaPmNgInS	změnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
domestikace	domestikace	k1gFnSc2	domestikace
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
základ	základ	k1gInSc1	základ
loveckého	lovecký	k2eAgNnSc2d1	lovecké
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
dědičný	dědičný	k2eAgMnSc1d1	dědičný
<g/>
,	,	kIx,	,
učením	učení	k1gNnSc7	učení
se	se	k3xPyFc4	se
jen	jen	k9	jen
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
nebo	nebo	k8xC	nebo
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
tlumí	tlumit	k5eAaImIp3nS	tlumit
<g/>
.	.	kIx.	.
</s>
<s>
Dědičné	dědičný	k2eAgFnPc1d1	dědičná
vlohy	vloha	k1gFnPc1	vloha
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
formě	forma	k1gFnSc3	forma
loveckého	lovecký	k2eAgNnSc2d1	lovecké
chování	chování	k1gNnSc2	chování
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
selekce	selekce	k1gFnSc2	selekce
psa	pes	k1gMnSc2	pes
k	k	k7c3	k
vykonávání	vykonávání	k1gNnSc3	vykonávání
určité	určitý	k2eAgFnSc2d1	určitá
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vlků	vlk	k1gMnPc2	vlk
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
probíhá	probíhat	k5eAaImIp3nS	probíhat
celý	celý	k2eAgInSc1d1	celý
řetězec	řetězec	k1gInSc1	řetězec
navzájem	navzájem	k6eAd1	navzájem
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navazujícího	navazující	k2eAgNnSc2d1	navazující
chování	chování	k1gNnSc2	chování
<g/>
:	:	kIx,	:
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
malé	malý	k2eAgFnSc2d1	malá
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
sekvence	sekvence	k1gFnSc1	sekvence
tvořena	tvořit	k5eAaImNgFnS	tvořit
fixováním	fixování	k1gNnSc7	fixování
nalezené	nalezený	k2eAgFnSc2d1	nalezená
kořisti	kořist	k1gFnSc2	kořist
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
plížením	plížení	k1gNnSc7	plížení
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kořisti	kořist	k1gFnSc3	kořist
<g/>
,	,	kIx,	,
rychlým	rychlý	k2eAgInSc7d1	rychlý
výpadem	výpad	k1gInSc7	výpad
a	a	k8xC	a
smrtícím	smrtící	k2eAgInSc7d1	smrtící
zákusem	zákus	k1gInSc7	zákus
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ještě	ještě	k9	ještě
se	s	k7c7	s
zatřepáním	zatřepání	k1gNnSc7	zatřepání
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nP	lovit
<g/>
-li	i	k?	-li
vlci	vlk	k1gMnPc1	vlk
velkou	velký	k2eAgFnSc4d1	velká
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
pořadí	pořadí	k1gNnSc4	pořadí
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
stopování	stopování	k1gNnSc1	stopování
kořisti	kořist	k1gFnSc2	kořist
–	–	k?	–
štvaní	štvaní	k1gNnSc1	štvaní
–	–	k?	–
stržení	stržení	k1gNnSc1	stržení
kořisti	kořist	k1gFnSc2	kořist
–	–	k?	–
usmrcení	usmrcení	k1gNnSc1	usmrcení
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
u	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
psů	pes	k1gMnPc2	pes
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
například	například	k6eAd1	například
u	u	k7c2	u
basenjiho	basenji	k1gMnSc2	basenji
<g/>
,	,	kIx,	,
u	u	k7c2	u
severských	severský	k2eAgMnPc2d1	severský
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
teriérů	teriér	k1gMnPc2	teriér
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
psy	pes	k1gMnPc4	pes
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
vlčí	vlčí	k2eAgFnSc7d1	vlčí
fyziognomií	fyziognomie	k1gFnSc7	fyziognomie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	s	k7c7	s
vzpřímenými	vzpřímený	k2eAgInPc7d1	vzpřímený
ušními	ušní	k2eAgInPc7d1	ušní
boltci	boltec	k1gInPc7	boltec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
dominantní	dominantní	k2eAgMnPc1d1	dominantní
a	a	k8xC	a
hůře	zle	k6eAd2	zle
cvičitelní	cvičitelný	k2eAgMnPc1d1	cvičitelný
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
neotenie	neotenie	k1gFnSc2	neotenie
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
totiž	totiž	k9	totiž
vyjádřené	vyjádřený	k2eAgNnSc1d1	vyjádřené
méně	málo	k6eAd2	málo
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
chrtů	chrt	k1gMnPc2	chrt
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
lovecké	lovecký	k2eAgNnSc1d1	lovecké
chování	chování	k1gNnSc1	chování
rovněž	rovněž	k9	rovněž
upevněno	upevnit	k5eAaPmNgNnS	upevnit
<g/>
,	,	kIx,	,
chrti	chrt	k1gMnPc1	chrt
ochotně	ochotně	k6eAd1	ochotně
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
štvou	štvát	k5eAaImIp3nP	štvát
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řetězec	řetězec	k1gInSc1	řetězec
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
nedokončen	dokončen	k2eNgMnSc1d1	nedokončen
<g/>
,	,	kIx,	,
u	u	k7c2	u
chrtů	chrt	k1gMnPc2	chrt
chybí	chybět	k5eAaImIp3nS	chybět
chování	chování	k1gNnSc4	chování
vedoucí	vedoucí	k1gFnSc2	vedoucí
ke	k	k7c3	k
stržení	stržení	k1gNnSc3	stržení
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
zabití	zabití	k1gNnSc3	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Ohaři	ohař	k1gMnPc1	ohař
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
velký	velký	k2eAgInSc4d1	velký
lovecký	lovecký	k2eAgInSc4d1	lovecký
pud	pud	k1gInSc4	pud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
výrazněji	výrazně	k6eAd2	výrazně
modifikován	modifikován	k2eAgInSc1d1	modifikován
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namísto	namísto	k7c2	namísto
jejího	její	k3xOp3gNnSc2	její
štvaní	štvaní	k1gNnSc2	štvaní
jí	jíst	k5eAaImIp3nS	jíst
tzv.	tzv.	kA	tzv.
vystaví	vystavit	k5eAaPmIp3nS	vystavit
–	–	k?	–
strne	strnout	k5eAaPmIp3nS	strnout
v	v	k7c6	v
charakteristické	charakteristický	k2eAgFnSc6d1	charakteristická
pozici	pozice	k1gFnSc6	pozice
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
zvěři	zvěř	k1gFnSc3	zvěř
a	a	k8xC	a
tak	tak	k6eAd1	tak
vůdce	vůdce	k1gMnSc4	vůdce
upozorní	upozornit	k5eAaPmIp3nP	upozornit
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Upozornění	upozornění	k1gNnSc1	upozornění
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
úkol	úkol	k1gInSc1	úkol
mladých	mladý	k2eAgMnPc2d1	mladý
vlků	vlk	k1gMnPc2	vlk
–	–	k?	–
u	u	k7c2	u
ohařů	ohař	k1gMnPc2	ohař
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
umělým	umělý	k2eAgInSc7d1	umělý
výběrem	výběr	k1gInSc7	výběr
posíleno	posílen	k2eAgNnSc4d1	posíleno
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
vystavování	vystavování	k1gNnSc2	vystavování
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ohařů	ohař	k1gMnPc2	ohař
<g/>
,	,	kIx,	,
setrů	setr	k1gMnPc2	setr
a	a	k8xC	a
pointrů	pointr	k1gMnPc2	pointr
se	se	k3xPyFc4	se
selekce	selekce	k1gFnSc1	selekce
na	na	k7c4	na
juvenilní	juvenilní	k2eAgNnSc4d1	juvenilní
chování	chování	k1gNnSc4	chování
během	během	k7c2	během
lovu	lov	k1gInSc2	lov
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
projevuje	projevovat	k5eAaImIp3nS	projevovat
středně	středně	k6eAd1	středně
vyjádřenou	vyjádřený	k2eAgFnSc7d1	vyjádřená
neotenií	neotenie	k1gFnSc7	neotenie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
dospělými	dospělý	k2eAgFnPc7d1	dospělá
proporcemi	proporce	k1gFnPc7	proporce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
uši	ucho	k1gNnPc1	ucho
bývají	bývat	k5eAaImIp3nP	bývat
klopené	klopený	k2eAgFnPc1d1	klopená
<g/>
.	.	kIx.	.
</s>
<s>
Instinktivní	instinktivní	k2eAgInSc1d1	instinktivní
program	program	k1gInSc1	program
lovu	lov	k1gInSc2	lov
malé	malý	k2eAgFnSc2d1	malá
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
přinášení	přinášení	k1gNnSc2	přinášení
<g/>
.	.	kIx.	.
</s>
<s>
Ochota	ochota	k1gFnSc1	ochota
aportovat	aportovat	k5eAaImF	aportovat
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
vlastnost	vlastnost	k1gFnSc1	vlastnost
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
během	během	k7c2	během
výcviku	výcvik	k1gInSc2	výcvik
se	se	k3xPyFc4	se
instinkt	instinkt	k1gInSc1	instinkt
jen	jen	k9	jen
dále	daleko	k6eAd2	daleko
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozenými	rozený	k2eAgMnPc7d1	rozený
přinašeči	přinašeč	k1gMnPc7	přinašeč
jsou	být	k5eAaImIp3nP	být
retrívři	retrívr	k1gMnPc1	retrívr
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
plemena	plemeno	k1gNnPc1	plemeno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
lovecké	lovecký	k2eAgInPc1d1	lovecký
pudy	pud	k1gInPc1	pud
nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
aportovat	aportovat	k5eAaImF	aportovat
hůře	zle	k6eAd2	zle
až	až	k9	až
těžko	těžko	k6eAd1	těžko
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
lovu	lov	k1gInSc2	lov
velké	velký	k2eAgFnSc2d1	velká
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
využíván	využívat	k5eAaPmNgInS	využívat
při	při	k7c6	při
nácviku	nácvik	k1gInSc6	nácvik
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
instinkt	instinkt	k1gInSc1	instinkt
stržení	stržení	k1gNnSc2	stržení
kořisti	kořist	k1gFnSc2	kořist
jako	jako	k8xC	jako
pevný	pevný	k2eAgInSc4d1	pevný
zákus	zákus	k1gInSc4	zákus
do	do	k7c2	do
rukávu	rukáv	k1gInSc2	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Ovčáci	ovčák	k1gMnPc1	ovčák
mají	mít	k5eAaImIp3nP	mít
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vlohy	vloha	k1gFnPc4	vloha
k	k	k7c3	k
pasení	pasení	k1gNnSc3	pasení
–	–	k?	–
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
silně	silně	k6eAd1	silně
pozměněné	pozměněný	k2eAgNnSc1d1	pozměněné
lovecké	lovecký	k2eAgNnSc1d1	lovecké
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
pes	pes	k1gMnSc1	pes
využívá	využívat	k5eAaPmIp3nS	využívat
svou	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
fixovat	fixovat	k5eAaImF	fixovat
dobytek	dobytek	k1gInSc4	dobytek
očima	oko	k1gNnPc7	oko
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
eye	eye	k?	eye
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomalé	pomalý	k2eAgNnSc1d1	pomalé
plížení	plížení	k1gNnSc1	plížení
a	a	k8xC	a
rychlé	rychlý	k2eAgInPc1d1	rychlý
výpady	výpad	k1gInPc1	výpad
ke	k	k7c3	k
shánění	shánění	k1gNnSc3	shánění
stáda	stádo	k1gNnSc2	stádo
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
přehánění	přehánění	k1gNnSc1	přehánění
určitým	určitý	k2eAgInSc7d1	určitý
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Ovčácká	ovčácký	k2eAgNnPc1d1	ovčácké
plemena	plemeno	k1gNnPc1	plemeno
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
vzpřímené	vzpřímený	k2eAgNnSc4d1	vzpřímené
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
se	s	k7c7	s
stádem	stádo	k1gNnSc7	stádo
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc4	ten
totiž	totiž	k9	totiž
dává	dávat	k5eAaImIp3nS	dávat
výhodu	výhoda	k1gFnSc4	výhoda
–	–	k?	–
silueta	silueta	k1gFnSc1	silueta
šelmy	šelma	k1gFnSc2	šelma
se	s	k7c7	s
vzpřímenýma	vzpřímený	k2eAgNnPc7d1	vzpřímené
ušima	ucho	k1gNnPc7	ucho
jim	on	k3xPp3gMnPc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lépe	dobře	k6eAd2	dobře
stádo	stádo	k1gNnSc4	stádo
ovládat	ovládat	k5eAaImF	ovládat
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc1	dobytek
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnSc1d1	psí
plemena	plemeno	k1gNnPc1	plemeno
vyšlechtěná	vyšlechtěný	k2eAgNnPc1d1	vyšlechtěné
ke	k	k7c3	k
střežení	střežení	k1gNnSc3	střežení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pastevečtí	pastevecký	k2eAgMnPc1d1	pastevecký
psi	pes	k1gMnPc1	pes
nebo	nebo	k8xC	nebo
špicové	špic	k1gMnPc1	špic
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
lovecké	lovecký	k2eAgInPc1d1	lovecký
pudy	pud	k1gInPc1	pud
výrazně	výrazně	k6eAd1	výrazně
potlačeny	potlačen	k2eAgInPc1d1	potlačen
<g/>
.	.	kIx.	.
</s>
<s>
Neotenické	neotenický	k2eAgInPc1d1	neotenický
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
nich	on	k3xPp3gMnPc6	on
vyjádřeny	vyjádřen	k2eAgMnPc4d1	vyjádřen
silně	silně	k6eAd1	silně
a	a	k8xC	a
vzhledem	vzhled	k1gInSc7	vzhled
jsou	být	k5eAaImIp3nP	být
podobní	podobný	k2eAgMnPc1d1	podobný
vlčatům	vlče	k1gNnPc3	vlče
–	–	k?	–
většinou	většina	k1gFnSc7	většina
mají	mít	k5eAaImIp3nP	mít
klopené	klopený	k2eAgNnSc4d1	klopené
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnSc4d2	kratší
čenichovou	čenichová	k1gFnSc4	čenichová
partii	partie	k1gFnSc4	partie
<g/>
,	,	kIx,	,
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
větší	veliký	k2eAgFnSc2d2	veliký
a	a	k8xC	a
kulatou	kulatý	k2eAgFnSc4d1	kulatá
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vetřelce	vetřelec	k1gMnPc4	vetřelec
označují	označovat	k5eAaImIp3nP	označovat
štěkáním	štěkání	k1gNnSc7	štěkání
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
přivolávají	přivolávat	k5eAaImIp3nP	přivolávat
nadřazené	nadřazený	k2eAgInPc4d1	nadřazený
členy	člen	k1gInPc4	člen
smečky	smečka	k1gFnSc2	smečka
–	–	k?	–
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malých	malý	k2eAgMnPc2d1	malý
společenských	společenský	k2eAgMnPc2d1	společenský
psů	pes	k1gMnPc2	pes
také	také	k9	také
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
loveckého	lovecký	k2eAgNnSc2d1	lovecké
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
–	–	k?	–
společníci	společník	k1gMnPc1	společník
člověka	člověk	k1gMnSc2	člověk
šlechtěni	šlechtit	k5eAaImNgMnP	šlechtit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
roztomilost	roztomilost	k1gFnSc4	roztomilost
<g/>
"	"	kIx"	"
začali	začít	k5eAaPmAgMnP	začít
vykazovat	vykazovat	k5eAaImF	vykazovat
výraznější	výrazný	k2eAgInPc4d2	výraznější
znaky	znak	k1gInPc4	znak
neotenie	neotenie	k1gFnSc2	neotenie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
stočený	stočený	k2eAgInSc4d1	stočený
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
nos	nos	k1gInSc4	nos
a	a	k8xC	a
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
dopředu	dopředu	k6eAd1	dopředu
směřující	směřující	k2eAgNnPc1d1	směřující
uši	ucho	k1gNnPc1	ucho
<g/>
,	,	kIx,	,
a	a	k8xC	a
potlačení	potlačení	k1gNnSc1	potlačení
loveckých	lovecký	k2eAgInPc2d1	lovecký
pudů	pud	k1gInPc2	pud
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
selekce	selekce	k1gFnSc2	selekce
na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
skutečností	skutečnost	k1gFnPc2	skutečnost
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
z	z	k7c2	z
obranného	obranný	k2eAgNnSc2d1	obranné
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
útěková	útěkový	k2eAgFnSc1d1	útěková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
–	–	k?	–
individuálně	individuálně	k6eAd1	individuálně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
pociťované	pociťovaný	k2eAgFnSc2d1	pociťovaná
hrozby	hrozba	k1gFnSc2	hrozba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
pes	pes	k1gMnSc1	pes
bude	být	k5eAaImBp3nS	být
snažit	snažit	k5eAaImF	snažit
zachovat	zachovat	k5eAaPmF	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
strach	strach	k1gInSc1	strach
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
je	být	k5eAaImIp3nS	být
útěková	útěkový	k2eAgFnSc1d1	útěková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
útěku	útěk	k1gInSc2	útěk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
přiblížení	přiblížení	k1gNnSc3	přiblížení
podnětu	podnět	k1gInSc2	podnět
na	na	k7c4	na
kritickou	kritický	k2eAgFnSc4d1	kritická
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
u	u	k7c2	u
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
obranné	obranný	k2eAgFnSc2d1	obranná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
aktivním	aktivní	k2eAgNnSc7d1	aktivní
snažením	snažení	k1gNnSc7	snažení
psa	pes	k1gMnSc2	pes
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
hrozbě	hrozba	k1gFnSc3	hrozba
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
uskočení	uskočení	k1gNnSc4	uskočení
<g/>
,	,	kIx,	,
či	či	k8xC	či
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nemá	mít	k5eNaImIp3nS	mít
<g/>
-li	i	k?	-li
pes	pes	k1gMnSc1	pes
možnost	možnost	k1gFnSc4	možnost
útěku	útěk	k1gInSc2	útěk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
obranný	obranný	k2eAgInSc4d1	obranný
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pasivní	pasivní	k2eAgFnSc6d1	pasivní
obranné	obranný	k2eAgFnSc6d1	obranná
reakci	reakce	k1gFnSc6	reakce
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
doslova	doslova	k6eAd1	doslova
paralyzován	paralyzovat	k5eAaBmNgMnS	paralyzovat
strachem	strach	k1gInSc7	strach
<g/>
.	.	kIx.	.
</s>
<s>
Uhýbá	uhýbat	k5eAaImIp3nS	uhýbat
pohledem	pohled	k1gInSc7	pohled
<g/>
,	,	kIx,	,
přitiskne	přitisknout	k5eAaPmIp3nS	přitisknout
se	se	k3xPyFc4	se
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
či	či	k8xC	či
se	se	k3xPyFc4	se
schovává	schovávat	k5eAaImIp3nS	schovávat
za	za	k7c2	za
psovoda	psovod	k1gMnSc2	psovod
<g/>
,	,	kIx,	,
výrazná	výrazný	k2eAgFnSc1d1	výrazná
je	být	k5eAaImIp3nS	být
strachová	strachový	k2eAgFnSc1d1	strachová
mimika	mimika	k1gFnSc1	mimika
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pes	pes	k1gMnSc1	pes
pomočit	pomočit	k5eAaPmF	pomočit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vystupňování	vystupňování	k1gNnSc6	vystupňování
situace	situace	k1gFnSc2	situace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
pseudoagresivní	pseudoagresivní	k2eAgNnSc4d1	pseudoagresivní
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pes	pes	k1gMnSc1	pes
kouše	kousat	k5eAaImIp3nS	kousat
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgFnSc1d1	pasivní
obranná	obranný	k2eAgFnSc1d1	obranná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
slabé	slabý	k2eAgFnSc2d1	slabá
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
melancholik	melancholik	k1gMnSc1	melancholik
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
přirozená	přirozený	k2eAgFnSc1d1	přirozená
a	a	k8xC	a
instinktivní	instinktivní	k2eAgFnSc1d1	instinktivní
potřeba	potřeba	k1gFnSc1	potřeba
hájit	hájit	k5eAaImF	hájit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgNnSc4d1	vlastní
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
pes	pes	k1gMnSc1	pes
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
teritoriálního	teritoriální	k2eAgNnSc2d1	teritoriální
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
agrese	agrese	k1gFnSc1	agrese
–	–	k?	–
pes	pes	k1gMnSc1	pes
aktivně	aktivně	k6eAd1	aktivně
brání	bránit	k5eAaImIp3nS	bránit
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
před	před	k7c7	před
cizími	cizí	k2eAgMnPc7d1	cizí
jedinci	jedinec	k1gMnPc7	jedinec
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
před	před	k7c7	před
cizími	cizí	k2eAgMnPc7d1	cizí
lidmi	člověk	k1gMnPc7	člověk
–	–	k?	–
a	a	k8xC	a
označování	označování	k1gNnSc1	označování
teritoria	teritorium	k1gNnSc2	teritorium
močí	moč	k1gFnPc2	moč
<g/>
,	,	kIx,	,
trusem	trus	k1gInSc7	trus
a	a	k8xC	a
hrabáním	hrabání	k1gNnSc7	hrabání
<g/>
.	.	kIx.	.
</s>
<s>
Teritoriálního	teritoriální	k2eAgNnSc2d1	teritoriální
chování	chování	k1gNnSc2	chování
psa	pes	k1gMnSc4	pes
využívá	využívat	k5eAaPmIp3nS	využívat
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
–	–	k?	–
určitá	určitý	k2eAgNnPc1d1	určité
plemena	plemeno	k1gNnPc1	plemeno
psů	pes	k1gMnPc2	pes
jsou	být	k5eAaImIp3nP	být
vyšlechtěna	vyšlechtěn	k2eAgMnSc4d1	vyšlechtěn
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
teritoriální	teritoriální	k2eAgFnSc2d1	teritoriální
agrese	agrese	k1gFnSc2	agrese
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
zvlášť	zvlášť	k9	zvlášť
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
hlídají	hlídat	k5eAaImIp3nP	hlídat
instinktivně	instinktivně	k6eAd1	instinktivně
a	a	k8xC	a
bez	bez	k7c2	bez
výcviku	výcvik	k1gInSc2	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
pastevečtí	pastevecký	k2eAgMnPc1d1	pastevecký
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
rotvajler	rotvajler	k1gInSc1	rotvajler
<g/>
,	,	kIx,	,
hovawart	hovawart	k1gInSc1	hovawart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teritoriálním	teritoriální	k2eAgNnSc6d1	teritoriální
chování	chování	k1gNnSc6	chování
existují	existovat	k5eAaImIp3nP	existovat
jisté	jistý	k2eAgInPc1d1	jistý
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
označují	označovat	k5eAaImIp3nP	označovat
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
močí	moč	k1gFnPc2	moč
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
obvykle	obvykle	k6eAd1	obvykle
značkují	značkovat	k5eAaImIp3nP	značkovat
jen	jen	k9	jen
při	při	k7c6	při
hárání	hárání	k1gNnSc6	hárání
jako	jako	k8xC	jako
projev	projev	k1gInSc1	projev
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
přijímá	přijímat	k5eAaImIp3nS	přijímat
potravu	potrava	k1gFnSc4	potrava
hltavě	hltavě	k6eAd1	hltavě
<g/>
,	,	kIx,	,
po	po	k7c6	po
velkých	velký	k2eAgInPc6d1	velký
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
žaludek	žaludek	k1gInSc1	žaludek
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
schopností	schopnost	k1gFnSc7	schopnost
se	se	k3xPyFc4	se
roztáhnout	roztáhnout	k5eAaPmF	roztáhnout
a	a	k8xC	a
velice	velice	k6eAd1	velice
snadno	snadno	k6eAd1	snadno
zvrací	zvracet	k5eAaImIp3nS	zvracet
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
přenášejí	přenášet	k5eAaImIp3nP	přenášet
potravu	potrava	k1gFnSc4	potrava
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
<g/>
,	,	kIx,	,
zahrabat	zahrabat	k5eAaPmF	zahrabat
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc3	on
přinášejí	přinášet	k5eAaImIp3nP	přinášet
vlčatům	vlče	k1gNnPc3	vlče
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
část	část	k1gFnSc1	část
tohoto	tento	k3xDgNnSc2	tento
chování	chování	k1gNnSc2	chování
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
jako	jako	k8xS	jako
atavismus	atavismus	k1gInSc1	atavismus
–	–	k?	–
zahrabávání	zahrabávání	k1gNnSc1	zahrabávání
potravy	potrava	k1gFnSc2	potrava
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
nouze	nouze	k1gFnSc2	nouze
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc2	zvracení
potravy	potrava	k1gFnSc2	potrava
štěňatům	štěně	k1gNnPc3	štěně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
požírání	požírání	k1gNnSc1	požírání
zdechlin	zdechlina	k1gFnPc2	zdechlina
<g/>
,	,	kIx,	,
u	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
psů	pes	k1gMnPc2	pes
obvykle	obvykle	k6eAd1	obvykle
nežádoucí	žádoucí	k2eNgNnSc4d1	nežádoucí
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgNnSc7d1	běžné
potravním	potravní	k2eAgNnSc7d1	potravní
chováním	chování	k1gNnSc7	chování
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
rádi	rád	k2eAgMnPc1d1	rád
až	až	k9	až
do	do	k7c2	do
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
–	–	k?	–
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
projev	projev	k1gInSc4	projev
neotenie	neotenie	k1gFnSc2	neotenie
<g/>
.	.	kIx.	.
</s>
<s>
Solitární	solitární	k2eAgFnSc1d1	solitární
hra	hra	k1gFnSc1	hra
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
sociálně	sociálně	k6eAd1	sociálně
žijící	žijící	k2eAgNnSc4d1	žijící
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
typická	typický	k2eAgFnSc1d1	typická
–	–	k?	–
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
spíš	spíš	k9	spíš
u	u	k7c2	u
štěňat	štěně	k1gNnPc2	štěně
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
příznakem	příznak	k1gInSc7	příznak
osamělosti	osamělost	k1gFnSc2	osamělost
a	a	k8xC	a
nudy	nuda	k1gFnSc2	nuda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
si	se	k3xPyFc3	se
pes	pes	k1gMnSc1	pes
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
představujícím	představující	k2eAgInSc7d1	představující
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
částí	část	k1gFnSc7	část
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hrabání	hrabání	k1gNnSc1	hrabání
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
kameny	kámen	k1gInPc7	kámen
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
příkladem	příklad	k1gInSc7	příklad
solitární	solitární	k2eAgFnSc2d1	solitární
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
chybění	chybění	k1gNnSc6	chybění
sociálního	sociální	k2eAgMnSc2d1	sociální
partnera	partner	k1gMnSc2	partner
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
vybití	vybití	k1gNnSc2	vybití
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ze	z	k7c2	z
solitární	solitární	k2eAgFnSc2d1	solitární
hry	hra	k1gFnSc2	hra
vyvinout	vyvinout	k5eAaPmF	vyvinout
stereotypní	stereotypní	k2eAgNnSc4d1	stereotypní
chování	chování	k1gNnSc4	chování
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
olizováním	olizování	k1gNnSc7	olizování
pacek	packa	k1gFnPc2	packa
<g/>
,	,	kIx,	,
honěním	honění	k1gNnSc7	honění
vlastního	vlastní	k2eAgInSc2d1	vlastní
ocasu	ocas	k1gInSc2	ocas
nebo	nebo	k8xC	nebo
ničením	ničení	k1gNnSc7	ničení
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
pes	pes	k1gMnSc1	pes
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
přirozenější	přirozený	k2eAgFnSc1d2	přirozenější
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
sociálních	sociální	k2eAgFnPc2d1	sociální
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
už	už	k6eAd1	už
u	u	k7c2	u
jeden	jeden	k4xCgMnSc1	jeden
měsíc	měsíc	k1gInSc4	měsíc
starých	starý	k2eAgNnPc2d1	staré
štěňat	štěně	k1gNnPc2	štěně
<g/>
,	,	kIx,	,
nejintenzivněji	intenzivně	k6eAd3	intenzivně
si	se	k3xPyFc3	se
hrají	hrát	k5eAaImIp3nP	hrát
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
stará	starý	k2eAgNnPc4d1	staré
štěňata	štěně	k1gNnPc4	štěně
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
hra	hra	k1gFnSc1	hra
psů	pes	k1gMnPc2	pes
má	mít	k5eAaImIp3nS	mít
svá	svůj	k3xOyFgNnPc4	svůj
pravidla	pravidlo	k1gNnPc4	pravidlo
–	–	k?	–
začíná	začínat	k5eAaImIp3nS	začínat
výzvou	výzva	k1gFnSc7	výzva
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
typicky	typicky	k6eAd1	typicky
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
hravé	hravý	k2eAgFnSc2d1	hravá
poklony	poklona	k1gFnSc2	poklona
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pes	pes	k1gMnSc1	pes
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
přední	přední	k2eAgFnSc7d1	přední
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zadek	zadek	k1gInSc1	zadek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výzvám	výzva	k1gFnPc3	výzva
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
hopsavé	hopsavý	k2eAgInPc4d1	hopsavý
úskoky	úskok	k1gInPc4	úskok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
partnerovi	partner	k1gMnSc3	partner
<g/>
,	,	kIx,	,
zvedání	zvedání	k1gNnSc4	zvedání
packy	packa	k1gFnSc2	packa
<g/>
,	,	kIx,	,
přehnané	přehnaný	k2eAgInPc1d1	přehnaný
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Výzvy	výzev	k1gInPc1	výzev
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
samotné	samotný	k2eAgFnSc2d1	samotná
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
psi	pes	k1gMnPc1	pes
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
typy	typ	k1gInPc7	typ
sociálních	sociální	k2eAgFnPc2d1	sociální
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
hravé	hravý	k2eAgNnSc4d1	hravé
kousání	kousání	k1gNnSc4	kousání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
psi	pes	k1gMnPc1	pes
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
zákus	zákus	k1gInSc4	zákus
do	do	k7c2	do
krku	krk	k1gInSc2	krk
či	či	k8xC	či
plecí	plec	k1gFnPc2	plec
<g/>
,	,	kIx,	,
vycenění	vycenění	k1gNnSc1	vycenění
zubů	zub	k1gInPc2	zub
či	či	k8xC	či
vrčení	vrčení	k1gNnSc4	vrčení
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
hravé	hravý	k2eAgNnSc4d1	hravé
zápolení	zápolení	k1gNnSc4	zápolení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
srazit	srazit	k5eAaPmF	srazit
partnera	partner	k1gMnSc4	partner
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
hravé	hravý	k2eAgFnPc4d1	hravá
honičky	honička	k1gFnPc4	honička
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
sexuální	sexuální	k2eAgFnPc1d1	sexuální
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
psi	pes	k1gMnPc1	pes
naskakují	naskakovat	k5eAaImIp3nP	naskakovat
a	a	k8xC	a
předvádějí	předvádět	k5eAaImIp3nP	předvádět
kopulační	kopulační	k2eAgInPc1d1	kopulační
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
znají	znát	k5eAaImIp3nP	znát
rovněž	rovněž	k9	rovněž
hlasové	hlasový	k2eAgFnPc4d1	hlasová
komunikační	komunikační	k2eAgFnPc4d1	komunikační
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
štěkáním	štěkání	k1gNnSc7	štěkání
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
tónině	tónina	k1gFnSc6	tónina
navzájem	navzájem	k6eAd1	navzájem
provokují	provokovat	k5eAaImIp3nP	provokovat
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
sociální	sociální	k2eAgMnSc1d1	sociální
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vybavený	vybavený	k2eAgMnSc1d1	vybavený
k	k	k7c3	k
životu	život	k1gInSc3	život
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
–	–	k?	–
smečce	smečka	k1gFnSc6	smečka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jedinci	jedinec	k1gMnPc7	jedinec
svého	své	k1gNnSc2	své
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
psi	pes	k1gMnPc1	pes
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
širokých	široký	k2eAgInPc2d1	široký
výrazových	výrazový	k2eAgInPc2d1	výrazový
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
řečí	řeč	k1gFnSc7	řeč
těla	tělo	k1gNnSc2	tělo
vyjádřenou	vyjádřený	k2eAgFnSc7d1	vyjádřená
polohami	poloha	k1gFnPc7	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
uší	ucho	k1gNnPc2	ucho
či	či	k8xC	či
pysků	pysk	k1gInPc2	pysk
<g/>
,	,	kIx,	,
pohledem	pohled	k1gInSc7	pohled
a	a	k8xC	a
ritualizovanými	ritualizovaný	k2eAgInPc7d1	ritualizovaný
gesty	gest	k1gInPc7	gest
<g/>
,	,	kIx,	,
zvukovými	zvukový	k2eAgInPc7d1	zvukový
a	a	k8xC	a
pachovými	pachový	k2eAgInPc7d1	pachový
signály	signál	k1gInPc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Sdělení	sdělení	k1gNnSc1	sdělení
není	být	k5eNaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
–	–	k?	–
například	například	k6eAd1	například
vrtění	vrtění	k1gNnSc1	vrtění
ocasu	ocas	k1gInSc2	ocas
obecně	obecně	k6eAd1	obecně
znamená	znamenat	k5eAaImIp3nS	znamenat
jen	jen	k9	jen
vzrušení	vzrušení	k1gNnSc1	vzrušení
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
pes	pes	k1gMnSc1	pes
přátelské	přátelský	k2eAgInPc4d1	přátelský
úmysly	úmysl	k1gInPc4	úmysl
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
držením	držení	k1gNnSc7	držení
ocasu	ocas	k1gInSc2	ocas
–	–	k?	–
nízko	nízko	k6eAd1	nízko
nebo	nebo	k8xC	nebo
vysoko	vysoko	k6eAd1	vysoko
<g/>
,	,	kIx,	,
polohou	poloha	k1gFnSc7	poloha
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
pysků	pysk	k1gInPc2	pysk
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
přirozeně	přirozeně	k6eAd1	přirozeně
naladěni	naladit	k5eAaPmNgMnP	naladit
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
–	–	k?	–
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
problémů	problém	k1gInPc2	problém
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
už	už	k6eAd1	už
štěňata	štěně	k1gNnPc1	štěně
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
rozumět	rozumět	k5eAaImF	rozumět
lidským	lidský	k2eAgInPc3d1	lidský
gestům	gest	k1gInPc3	gest
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
naznačování	naznačování	k1gNnSc1	naznačování
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
ukazování	ukazování	k1gNnSc4	ukazování
prstem	prst	k1gInSc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
přizpůsobením	přizpůsobení	k1gNnSc7	přizpůsobení
se	se	k3xPyFc4	se
životu	život	k1gInSc2	život
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
je	být	k5eAaImIp3nS	být
štěkot	štěkot	k1gInSc4	štěkot
–	–	k?	–
dospělí	dospělý	k2eAgMnPc1d1	dospělý
vlci	vlk	k1gMnPc1	vlk
neštěkají	štěkat	k5eNaImIp3nP	štěkat
<g/>
,	,	kIx,	,
štěkání	štěkání	k1gNnSc4	štěkání
není	být	k5eNaImIp3nS	být
vlastní	vlastní	k2eAgFnSc1d1	vlastní
ani	ani	k9	ani
primitivním	primitivní	k2eAgMnPc3d1	primitivní
psům	pes	k1gMnPc3	pes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prošlechtění	prošlechtěný	k2eAgMnPc1d1	prošlechtěný
psi	pes	k1gMnPc1	pes
používají	používat	k5eAaImIp3nP	používat
štěkání	štěkání	k1gNnSc4	štěkání
zcela	zcela	k6eAd1	zcela
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
ke	k	k7c3	k
sdělování	sdělování	k1gNnSc3	sdělování
informací	informace	k1gFnPc2	informace
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
respektovat	respektovat	k5eAaImF	respektovat
člověka	člověk	k1gMnSc4	člověk
jako	jako	k8xC	jako
vůdce	vůdce	k1gMnSc4	vůdce
–	–	k?	–
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
dominantních	dominantní	k2eAgFnPc2d1	dominantní
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
sami	sám	k3xTgMnPc1	sám
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nedůsledného	důsledný	k2eNgNnSc2d1	nedůsledné
<g/>
,	,	kIx,	,
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
při	při	k7c6	při
rozmazlení	rozmazlení	k1gNnSc6	rozmazlení
psa	pes	k1gMnSc4	pes
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
polidšťování	polidšťování	k1gNnSc4	polidšťování
ale	ale	k8xC	ale
pes	pes	k1gMnSc1	pes
může	moct	k5eAaImIp3nS	moct
cítit	cítit	k5eAaImF	cítit
absenci	absence	k1gFnSc4	absence
vůdce	vůdce	k1gMnSc2	vůdce
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
postaví	postavit	k5eAaPmIp3nS	postavit
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgNnSc1d1	získané
chování	chování	k1gNnSc1	chování
psa	pes	k1gMnSc2	pes
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
až	až	k9	až
během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
na	na	k7c6	na
základě	základ	k1gInSc6	základ
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
pes	pes	k1gMnSc1	pes
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
naučit	naučit	k5eAaPmF	naučit
165	[number]	k4	165
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
psi	pes	k1gMnPc1	pes
až	až	k9	až
250	[number]	k4	250
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
dokážou	dokázat	k5eAaPmIp3nP	dokázat
počítat	počítat	k5eAaImF	počítat
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
nebo	nebo	k8xC	nebo
pěti	pět	k4xCc3	pět
a	a	k8xC	a
registrují	registrovat	k5eAaBmIp3nP	registrovat
chybný	chybný	k2eAgInSc4d1	chybný
výsledek	výsledek	k1gInSc4	výsledek
při	při	k7c6	při
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
počtářských	počtářský	k2eAgFnPc6d1	počtářská
úlohách	úloha	k1gFnPc6	úloha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
vtištění	vtištění	k1gNnSc1	vtištění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
štěněte	štěně	k1gNnSc2	štěně
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
raná	raný	k2eAgFnSc1d1	raná
socializace	socializace	k1gFnSc1	socializace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
štěně	štěně	k1gNnSc1	štěně
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
životě	život	k1gInSc6	život
bude	být	k5eAaImBp3nS	být
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
běžnou	běžný	k2eAgFnSc4d1	běžná
součást	součást	k1gFnSc4	součást
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
štěně	štěně	k1gNnSc1	štěně
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
v	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
přítulné	přítulný	k2eAgNnSc1d1	přítulné
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
štěně	štěně	k1gNnSc1	štěně
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
člověka	člověk	k1gMnSc4	člověk
vtiskne	vtisknout	k5eAaPmIp3nS	vtisknout
jako	jako	k9	jako
příslušníka	příslušník	k1gMnSc4	příslušník
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
předpoklady	předpoklad	k1gInPc4	předpoklad
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
dobrým	dobrý	k2eAgMnSc7d1	dobrý
společníkem	společník	k1gMnSc7	společník
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
nezanedbaná	zanedbaný	k2eNgFnSc1d1	zanedbaný
socializace	socializace	k1gFnSc1	socializace
může	moct	k5eAaImIp3nS	moct
příznivě	příznivě	k6eAd1	příznivě
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
negativní	negativní	k2eAgFnPc4d1	negativní
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vlastnosti	vlastnost	k1gFnPc4	vlastnost
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
slabý	slabý	k2eAgInSc4d1	slabý
nervový	nervový	k2eAgInSc4d1	nervový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
rané	raný	k2eAgFnSc2d1	raná
socializace	socializace	k1gFnSc2	socializace
plynule	plynule	k6eAd1	plynule
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
socializačního	socializační	k2eAgNnSc2d1	socializační
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
osvojování	osvojování	k1gNnSc3	osvojování
sociálních	sociální	k2eAgInPc2d1	sociální
návyků	návyk	k1gInPc2	návyk
a	a	k8xC	a
upevňování	upevňování	k1gNnPc2	upevňování
sociálního	sociální	k2eAgNnSc2d1	sociální
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
štěně	štěně	k1gNnSc1	štěně
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
poznává	poznávat	k5eAaImIp3nS	poznávat
schopností	schopnost	k1gFnSc7	schopnost
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
i	i	k9	i
vlastnosti	vlastnost	k1gFnPc1	vlastnost
okolních	okolní	k2eAgInPc2d1	okolní
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
rovněž	rovněž	k9	rovněž
návyk	návyk	k1gInSc1	návyk
na	na	k7c4	na
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
nebudou	být	k5eNaImBp3nP	být
u	u	k7c2	u
psa	pes	k1gMnSc2	pes
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
zkušenost	zkušenost	k1gFnSc1	zkušenost
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
naopak	naopak	k6eAd1	naopak
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
strachu	strach	k1gInSc3	strach
trvalému	trvalý	k2eAgInSc3d1	trvalý
<g/>
.	.	kIx.	.
</s>
<s>
Učení	učení	k1gNnSc1	učení
podmiňováním	podmiňování	k1gNnSc7	podmiňování
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
hlavním	hlavní	k2eAgInSc7d1	hlavní
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
získaného	získaný	k2eAgNnSc2d1	získané
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
podmiňování	podmiňování	k1gNnSc1	podmiňování
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
přímo	přímo	k6eAd1	přímo
spjato	spjat	k2eAgNnSc1d1	spjato
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
výzkumům	výzkum	k1gInPc3	výzkum
ruského	ruský	k2eAgMnSc4d1	ruský
fyziologa	fyziolog	k1gMnSc4	fyziolog
a	a	k8xC	a
nositele	nositel	k1gMnSc4	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgInSc2d1	Pavlův
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
zcela	zcela	k6eAd1	zcela
neutrální	neutrální	k2eAgInSc4d1	neutrální
podnět	podnět	k1gInSc4	podnět
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zazvonění	zazvonění	k1gNnSc2	zazvonění
zvonku	zvonek	k1gInSc2	zvonek
nebo	nebo	k8xC	nebo
rozsvícení	rozsvícení	k1gNnSc2	rozsvícení
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
podáváním	podávání	k1gNnSc7	podávání
potravy	potrava	k1gFnSc2	potrava
mění	měnit	k5eAaImIp3nS	měnit
ve	v	k7c4	v
spouštěč	spouštěč	k1gInSc4	spouštěč
chování	chování	k1gNnSc2	chování
–	–	k?	–
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
podnět	podnět	k1gInSc4	podnět
podmíněný	podmíněný	k2eAgInSc4d1	podmíněný
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
po	po	k7c6	po
vystavení	vystavení	k1gNnSc2	vystavení
podmíněnému	podmíněný	k2eAgInSc3d1	podmíněný
podnětu	podnět	k1gInSc3	podnět
sliní	slinit	k5eAaImIp3nS	slinit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
potravu	potrava	k1gFnSc4	potrava
nedostanou	dostat	k5eNaPmIp3nP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
získává	získávat	k5eAaImIp3nS	získávat
během	během	k7c2	během
života	život	k1gInSc2	život
určité	určitý	k2eAgFnSc2d1	určitá
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
silně	silně	k6eAd1	silně
vnímány	vnímat	k5eAaImNgInP	vnímat
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
těchto	tento	k3xDgInPc2	tento
podmíněných	podmíněný	k2eAgInPc2d1	podmíněný
reflexů	reflex	k1gInPc2	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cíleném	cílený	k2eAgNnSc6d1	cílené
vytváření	vytváření	k1gNnSc6	vytváření
podmíněných	podmíněný	k2eAgInPc2d1	podmíněný
reflexů	reflex	k1gInPc2	reflex
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
klasická	klasický	k2eAgFnSc1d1	klasická
teorie	teorie	k1gFnSc1	teorie
výcviku	výcvik	k1gInSc2	výcvik
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
učení	učení	k1gNnSc1	učení
povelu	povel	k1gInSc2	povel
sedni	sednout	k5eAaPmRp2nS	sednout
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
se	se	k3xPyFc4	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
povelu	povel	k1gInSc2	povel
uvede	uvést	k5eAaPmIp3nS	uvést
do	do	k7c2	do
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
zatlačením	zatlačení	k1gNnSc7	zatlačení
na	na	k7c4	na
záď	záď	k1gFnSc4	záď
<g/>
,	,	kIx,	,
trhnutí	trhnutí	k1gNnSc1	trhnutí
vodítkem	vodítko	k1gNnSc7	vodítko
vzhůru	vzhůru	k6eAd1	vzhůru
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
zaujetí	zaujetí	k1gNnSc6	zaujetí
správné	správný	k2eAgFnSc2d1	správná
pozice	pozice	k1gFnSc2	pozice
je	být	k5eAaImIp3nS	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
pamlskem	pamlsek	k1gInSc7	pamlsek
<g/>
.	.	kIx.	.
</s>
<s>
Opakováním	opakování	k1gNnSc7	opakování
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
podmíněný	podmíněný	k2eAgInSc1d1	podmíněný
reflex	reflex	k1gInSc1	reflex
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
pes	pes	k1gMnSc1	pes
po	po	k7c6	po
uslyšení	uslyšení	k1gNnSc6	uslyšení
povelu	povel	k1gInSc2	povel
"	"	kIx"	"
<g/>
sedni	sednout	k5eAaPmRp2nS	sednout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
sedá	sedat	k5eAaImIp3nS	sedat
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
reflexního	reflexní	k2eAgInSc2d1	reflexní
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
načasování	načasování	k1gNnSc1	načasování
povelu	povel	k1gInSc2	povel
a	a	k8xC	a
odměn	odměna	k1gFnPc2	odměna
–	–	k?	–
odměna	odměna	k1gFnSc1	odměna
musí	muset	k5eAaImIp3nS	muset
přijít	přijít	k5eAaPmF	přijít
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
2	[number]	k4	2
s	s	k7c7	s
od	od	k7c2	od
splnění	splnění	k1gNnSc2	splnění
povelu	povel	k1gInSc2	povel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
žádoucímu	žádoucí	k2eAgNnSc3d1	žádoucí
upevnění	upevnění	k1gNnSc3	upevnění
reflexu	reflex	k1gInSc2	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
trest	trest	k1gInSc1	trest
je	být	k5eAaImIp3nS	být
nesmyslný	smyslný	k2eNgInSc1d1	nesmyslný
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
-li	i	k?	-li
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Vyšším	vysoký	k2eAgInSc7d2	vyšší
stupněm	stupeň	k1gInSc7	stupeň
učení	učení	k1gNnSc2	učení
je	být	k5eAaImIp3nS	být
operantní	operantní	k2eAgNnSc1d1	operantní
podmiňování	podmiňování	k1gNnSc1	podmiňování
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
metodou	metoda	k1gFnSc7	metoda
pokus	pokus	k1gInSc1	pokus
–	–	k?	–
omyl	omyl	k1gInSc1	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
má	mít	k5eAaImIp3nS	mít
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
tendenci	tendence	k1gFnSc4	tendence
opakovat	opakovat	k5eAaImF	opakovat
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
následuje	následovat	k5eAaImIp3nS	následovat
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Operantního	Operantní	k2eAgNnSc2d1	Operantní
podmiňování	podmiňování	k1gNnSc2	podmiňování
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
některých	některý	k3yIgInPc6	některý
způsobech	způsob	k1gInPc6	způsob
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
clicker	clicker	k1gInSc1	clicker
training	training	k1gInSc1	training
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
psa	pes	k1gMnSc2	pes
zákusu	zákus	k1gInSc2	zákus
do	do	k7c2	do
ochranného	ochranný	k2eAgInSc2d1	ochranný
rukávu	rukáv	k1gInSc2	rukáv
<g/>
,	,	kIx,	,
překonávání	překonávání	k1gNnSc4	překonávání
žebříku	žebřík	k1gInSc2	žebřík
<g/>
,	,	kIx,	,
při	při	k7c6	při
rozlišování	rozlišování	k1gNnSc6	rozlišování
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podkladem	podklad	k1gInSc7	podklad
naučeného	naučený	k2eAgNnSc2d1	naučené
chování	chování	k1gNnSc2	chování
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
otevírání	otevírání	k1gNnSc1	otevírání
dveří	dveře	k1gFnPc2	dveře
–	–	k?	–
pes	pes	k1gMnSc1	pes
skáče	skákat	k5eAaImIp3nS	skákat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
náhodně	náhodně	k6eAd1	náhodně
podaří	podařit	k5eAaPmIp3nS	podařit
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
otevření	otevření	k1gNnSc4	otevření
už	už	k6eAd1	už
bude	být	k5eAaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
pokusů	pokus	k1gInPc2	pokus
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
otevírání	otevírání	k1gNnSc1	otevírání
dveří	dveře	k1gFnPc2	dveře
na	na	k7c4	na
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
profesora	profesor	k1gMnSc2	profesor
S.	S.	kA	S.
Corena	Coren	k1gMnSc2	Coren
se	se	k3xPyFc4	se
celková	celkový	k2eAgFnSc1d1	celková
inteligence	inteligence	k1gFnSc1	inteligence
psa	pes	k1gMnSc2	pes
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
instinktivní	instinktivní	k2eAgFnSc2d1	instinktivní
<g/>
,	,	kIx,	,
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
a	a	k8xC	a
pracovní	pracovní	k2eAgFnSc2d1	pracovní
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Instinktivní	instinktivní	k2eAgFnSc1d1	instinktivní
inteligence	inteligence	k1gFnSc1	inteligence
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
geneticky	geneticky	k6eAd1	geneticky
zakódovaných	zakódovaný	k2eAgFnPc2d1	zakódovaná
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
upevněných	upevněný	k2eAgInPc2d1	upevněný
cíleným	cílený	k2eAgInSc7d1	cílený
výběrem	výběr	k1gInSc7	výběr
a	a	k8xC	a
šlechtěním	šlechtění	k1gNnSc7	šlechtění
daných	daný	k2eAgNnPc2d1	dané
plemen	plemeno	k1gNnPc2	plemeno
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
inteligence	inteligence	k1gFnSc1	inteligence
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
tvořena	tvořit	k5eAaImNgFnS	tvořit
schopností	schopnost	k1gFnSc7	schopnost
učit	učit	k5eAaImF	učit
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
samostatně	samostatně	k6eAd1	samostatně
řešit	řešit	k5eAaImF	řešit
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
prošlechtěných	prošlechtěný	k2eAgNnPc2d1	prošlechtěné
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
horší	horšit	k5eAaImIp3nP	horšit
než	než	k8xS	než
u	u	k7c2	u
vlků	vlk	k1gMnPc2	vlk
nebo	nebo	k8xC	nebo
u	u	k7c2	u
dingů	dingo	k1gMnPc2	dingo
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
obracejí	obracet	k5eAaImIp3nP	obracet
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samostatně	samostatně	k6eAd1	samostatně
problémy	problém	k1gInPc4	problém
řešit	řešit	k5eAaImF	řešit
spíš	spíš	k9	spíš
neumějí	umět	k5eNaImIp3nP	umět
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
inteligence	inteligence	k1gFnSc1	inteligence
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
schopností	schopnost	k1gFnSc7	schopnost
osvojit	osvojit	k5eAaPmF	osvojit
si	se	k3xPyFc3	se
povel	povel	k1gInSc4	povel
při	při	k7c6	při
co	co	k9	co
nejmenším	malý	k2eAgInSc6d3	nejmenší
počtu	počet	k1gInSc6	počet
opakování	opakování	k1gNnSc2	opakování
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
uposlechnutí	uposlechnutí	k1gNnSc1	uposlechnutí
povelu	povel	k1gInSc2	povel
na	na	k7c4	na
co	co	k3yRnSc4	co
nejmenší	malý	k2eAgInSc1d3	nejmenší
počet	počet	k1gInSc1	počet
opakování	opakování	k1gNnSc2	opakování
povelu	povel	k1gInSc2	povel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obecně	obecně	k6eAd1	obecně
vynikají	vynikat	k5eAaImIp3nP	vynikat
adaptivní	adaptivní	k2eAgFnSc7d1	adaptivní
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
dobrman	dobrman	k1gMnSc1	dobrman
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
losí	losí	k2eAgMnSc1d1	losí
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
pudl	pudl	k1gMnSc1	pudl
<g/>
,	,	kIx,	,
puli	pul	k1gMnPc1	pul
a	a	k8xC	a
šeltie	šeltie	k1gFnPc1	šeltie
<g/>
.	.	kIx.	.
</s>
<s>
Teriéři	teriér	k1gMnPc1	teriér
<g/>
,	,	kIx,	,
malamut	malamut	k1gMnSc1	malamut
<g/>
,	,	kIx,	,
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
husky	husky	k6eAd1	husky
<g/>
,	,	kIx,	,
samojed	samojed	k1gMnSc1	samojed
<g/>
,	,	kIx,	,
basenji	basent	k5eAaPmIp1nS	basent
<g/>
,	,	kIx,	,
čivava	čivava	k1gFnSc1	čivava
<g/>
,	,	kIx,	,
knírač	knírač	k1gMnSc1	knírač
a	a	k8xC	a
šiperka	šiperka	k1gFnSc1	šiperka
mají	mít	k5eAaImIp3nP	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
schopnost	schopnost	k1gFnSc4	schopnost
řešit	řešit	k5eAaImF	řešit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hůře	zle	k6eAd2	zle
a	a	k8xC	a
pomaleji	pomale	k6eAd2	pomale
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
zlatý	zlatý	k2eAgInSc1d1	zlatý
a	a	k8xC	a
labradorský	labradorský	k2eAgMnSc1d1	labradorský
retrívr	retrívr	k1gMnSc1	retrívr
a	a	k8xC	a
belgický	belgický	k2eAgMnSc1d1	belgický
ovčák	ovčák	k1gMnSc1	ovčák
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
samostatná	samostatný	k2eAgFnSc1d1	samostatná
schopnost	schopnost	k1gFnSc1	schopnost
řešení	řešení	k1gNnSc2	řešení
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
pracovní	pracovní	k2eAgFnSc2d1	pracovní
inteligence	inteligence	k1gFnSc2	inteligence
se	se	k3xPyFc4	se
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
border	border	k1gInSc1	border
kolie	kolie	k1gFnSc2	kolie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
pudl	pudl	k1gMnSc1	pudl
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgMnSc1d1	zlatý
retrívr	retrívr	k1gMnSc1	retrívr
<g/>
,	,	kIx,	,
dobrman	dobrman	k1gMnSc1	dobrman
<g/>
,	,	kIx,	,
šeltie	šeltie	k1gFnSc1	šeltie
<g/>
,	,	kIx,	,
labrador	labrador	k1gInSc1	labrador
<g/>
,	,	kIx,	,
papilon	papilon	k1gInSc1	papilon
<g/>
,	,	kIx,	,
rotvajler	rotvajler	k1gMnSc1	rotvajler
a	a	k8xC	a
australský	australský	k2eAgMnSc1d1	australský
honácký	honácký	k2eAgMnSc1d1	honácký
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledních	poslední	k2eAgFnPc6d1	poslední
příčkách	příčka	k1gFnPc6	příčka
je	být	k5eAaImIp3nS	být
afgánský	afgánský	k2eAgMnSc1d1	afgánský
chrt	chrt	k1gMnSc1	chrt
<g/>
,	,	kIx,	,
basenji	basenje	k1gFnSc4	basenje
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
buldok	buldok	k1gMnSc1	buldok
<g/>
,	,	kIx,	,
čau-čau	čau-čaus	k1gInSc2	čau-čaus
<g/>
,	,	kIx,	,
barzoj	barzoj	k1gMnSc1	barzoj
<g/>
,	,	kIx,	,
pekingský	pekingský	k2eAgMnSc1d1	pekingský
palácový	palácový	k2eAgMnSc1d1	palácový
psík	psík	k1gMnSc1	psík
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
mastif	mastif	k1gMnSc1	mastif
<g/>
,	,	kIx,	,
baset	baset	k1gMnSc1	baset
a	a	k8xC	a
shih-tzu	shihza	k1gFnSc4	shih-tza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
krmiv	krmivo	k1gNnPc2	krmivo
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
granule	granule	k1gFnSc1	granule
<g/>
,	,	kIx,	,
BARF	BARF	kA	BARF
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zbytky	zbytek	k1gInPc1	zbytek
po	po	k7c6	po
lidské	lidský	k2eAgFnSc6d1	lidská
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
stravu	strava	k1gFnSc4	strava
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
patří	patřit	k5eAaImIp3nS	patřit
čokoláda	čokoláda	k1gFnSc1	čokoláda
(	(	kIx(	(
<g/>
theobromin	theobromin	k1gInSc1	theobromin
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
vysoce	vysoce	k6eAd1	vysoce
toxický	toxický	k2eAgMnSc1d1	toxický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc4	hrozen
a	a	k8xC	a
hrozinky	hrozinka	k1gFnPc4	hrozinka
<g/>
,	,	kIx,	,
avokádo	avokádo	k1gNnSc4	avokádo
<g/>
,	,	kIx,	,
vařené	vařený	k2eAgFnPc4d1	vařená
kuřecí	kuřecí	k2eAgFnPc4d1	kuřecí
kosti	kost	k1gFnPc4	kost
(	(	kIx(	(
<g/>
štěpí	štěpit	k5eAaImIp3nS	štěpit
se	se	k3xPyFc4	se
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
poškodit	poškodit	k5eAaPmF	poškodit
žaludek	žaludek	k1gInSc4	žaludek
či	či	k8xC	či
střeva	střevo	k1gNnSc2	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syrová	syrový	k2eAgNnPc1d1	syrové
játra	játra	k1gNnPc1	játra
(	(	kIx(	(
<g/>
možná	možný	k2eAgFnSc1d1	možná
toxicita	toxicita	k1gFnSc1	toxicita
<g />
.	.	kIx.	.
</s>
<s>
vitamínem	vitamín	k1gInSc7	vitamín
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česnek	česnek	k1gInSc4	česnek
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnPc4	cibule
<g/>
,	,	kIx,	,
sladkosti	sladkost	k1gFnPc4	sladkost
obsahující	obsahující	k2eAgInSc1d1	obsahující
xylitol	xylitol	k1gInSc1	xylitol
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
hladinu	hladina	k1gFnSc4	hladina
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnPc4	mandle
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc4	ořech
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
obsahující	obsahující	k2eAgInSc1d1	obsahující
kofein	kofein	k1gInSc1	kofein
(	(	kIx(	(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
poruchy	porucha	k1gFnSc2	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc4	rajče
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc4	brambora
<g/>
,	,	kIx,	,
droždí	droždí	k1gNnSc4	droždí
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc4	sušenka
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
syrové	syrový	k2eAgInPc4d1	syrový
vaječné	vaječný	k2eAgInPc4d1	vaječný
bílky	bílek	k1gInPc4	bílek
<g/>
,	,	kIx,	,
tučné	tučný	k2eAgNnSc4d1	tučné
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Granule	granule	k1gFnSc1	granule
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc4d1	obecný
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
krmiva	krmivo	k1gNnPc4	krmivo
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
extruzí	extruze	k1gFnSc7	extruze
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
krmení	krmení	k1gNnSc6	krmení
granulí	granule	k1gFnPc2	granule
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
granule	granule	k1gFnPc1	granule
nebo	nebo	k8xC	nebo
granule	granule	k1gFnPc1	granule
s	s	k7c7	s
nevhodným	vhodný	k2eNgNnSc7d1	nevhodné
složením	složení	k1gNnSc7	složení
mohou	moct	k5eAaImIp3nP	moct
poškozovat	poškozovat	k5eAaImF	poškozovat
psí	psí	k2eAgNnSc4d1	psí
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
problémy	problém	k1gInPc4	problém
patří	patřit	k5eAaImIp3nS	patřit
oslabení	oslabení	k1gNnSc1	oslabení
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
kožní	kožní	k2eAgInPc1d1	kožní
problémy	problém	k1gInPc1	problém
nebo	nebo	k8xC	nebo
rakovina	rakovina	k1gFnSc1	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
granulí	granule	k1gFnPc2	granule
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
klíčové	klíčový	k2eAgNnSc1d1	klíčové
vybírat	vybírat	k5eAaImF	vybírat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Granulí	granule	k1gFnSc7	granule
s	s	k7c7	s
lepším	dobrý	k2eAgNnSc7d2	lepší
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
podávat	podávat	k5eAaImF	podávat
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
výživnější	výživný	k2eAgFnPc1d2	výživnější
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
granule	granule	k1gFnSc1	granule
střídat	střídat	k5eAaImF	střídat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
krmiv	krmivo	k1gNnPc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturách	kultura	k1gFnPc6	kultura
vnímán	vnímat	k5eAaImNgInS	vnímat
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
někde	někde	k6eAd1	někde
je	být	k5eAaImIp3nS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
zatracován	zatracován	k2eAgMnSc1d1	zatracován
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zvířecím	zvířecí	k2eAgInSc7d1	zvířecí
symbolem	symbol	k1gInSc7	symbol
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Inkové	Ink	k1gMnPc1	Ink
<g/>
,	,	kIx,	,
Peršané	Peršan	k1gMnPc1	Peršan
či	či	k8xC	či
Indové	Ind	k1gMnPc1	Ind
uctívali	uctívat	k5eAaImAgMnP	uctívat
psa	pes	k1gMnSc4	pes
jako	jako	k8xC	jako
strážci	strážce	k1gMnPc1	strážce
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
průvodce	průvodce	k1gMnSc4	průvodce
do	do	k7c2	do
království	království	k1gNnSc2	království
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
velké	velký	k2eAgNnSc1d1	velké
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Egypťané	Egypťan	k1gMnPc1	Egypťan
povyšovali	povyšovat	k5eAaImAgMnP	povyšovat
kult	kult	k1gInSc4	kult
smrti	smrt	k1gFnSc2	smrt
nad	nad	k7c4	nad
vše	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
bůh	bůh	k1gMnSc1	bůh
Anubis	Anubis	k1gInSc4	Anubis
měl	mít	k5eAaImAgMnS	mít
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
psí	psí	k2eAgFnSc4d1	psí
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
si	se	k3xPyFc3	se
psů	pes	k1gMnPc2	pes
vážili	vážit	k5eAaImAgMnP	vážit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
pes	pes	k1gMnSc1	pes
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
zabalzamovali	zabalzamovat	k5eAaBmAgMnP	zabalzamovat
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
uložili	uložit	k5eAaPmAgMnP	uložit
jeho	jeho	k3xOp3gInPc4	jeho
ostatky	ostatek	k1gInPc4	ostatek
na	na	k7c4	na
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
zobrazeným	zobrazený	k2eAgMnSc7d1	zobrazený
psem	pes	k1gMnSc7	pes
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
za	za	k7c2	za
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešních	dnešní	k2eAgMnPc2d1	dnešní
chrtů	chrt	k1gMnPc2	chrt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nacházíme	nacházet	k5eAaImIp1nP	nacházet
často	často	k6eAd1	často
zobrazení	zobrazení	k1gNnSc1	zobrazení
ohařů	ohař	k1gMnPc2	ohař
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
zajíců	zajíc	k1gMnPc2	zajíc
a	a	k8xC	a
gazel	gazela	k1gFnPc2	gazela
<g/>
.	.	kIx.	.
</s>
<s>
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
bohyně	bohyně	k1gFnSc1	bohyně
Gula	gula	k1gFnSc1	gula
byla	být	k5eAaImAgFnS	být
patronkou	patronka	k1gFnSc7	patronka
psů	pes	k1gMnPc2	pes
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jí	on	k3xPp3gFnSc3	on
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
nosili	nosit	k5eAaImAgMnP	nosit
psy	pes	k1gMnPc7	pes
vymodelované	vymodelovaný	k2eAgFnSc2d1	vymodelovaná
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sumerských	sumerský	k2eAgFnPc6d1	sumerská
pečetích	pečeť	k1gFnPc6	pečeť
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zobrazení	zobrazený	k2eAgMnPc1d1	zobrazený
psi	pes	k1gMnPc1	pes
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
podobní	podobný	k2eAgMnPc1d1	podobný
dnešním	dnešní	k2eAgMnPc3d1	dnešní
mastifům	mastif	k1gMnPc3	mastif
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
objevili	objevit	k5eAaPmAgMnP	objevit
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
chrti	chrt	k1gMnPc1	chrt
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
sloužili	sloužit	k5eAaImAgMnP	sloužit
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
lvů	lev	k1gMnPc2	lev
a	a	k8xC	a
divokých	divoký	k2eAgNnPc2d1	divoké
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
bojoví	bojový	k2eAgMnPc1d1	bojový
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
k	k	k7c3	k
promíchání	promíchání	k1gNnSc3	promíchání
krve	krev	k1gFnSc2	krev
tibetských	tibetský	k2eAgMnPc2d1	tibetský
dogovitých	dogovitý	k2eAgMnPc2d1	dogovitý
psů	pes	k1gMnPc2	pes
do	do	k7c2	do
místních	místní	k2eAgNnPc2d1	místní
molosoidních	molosoidní	k2eAgNnPc2d1	molosoidní
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
psi	pes	k1gMnPc1	pes
se	se	k3xPyFc4	se
vyznačovali	vyznačovat	k5eAaImAgMnP	vyznačovat
nízkým	nízký	k2eAgInSc7d1	nízký
prahem	práh	k1gInSc7	práh
agresivity	agresivita	k1gFnSc2	agresivita
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
váleční	váleční	k2eAgMnPc1d1	váleční
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Kroniky	kronika	k1gFnPc1	kronika
dokládají	dokládat	k5eAaImIp3nP	dokládat
cílený	cílený	k2eAgInSc4d1	cílený
chov	chov	k1gInSc4	chov
molosoidních	molosoidní	k2eAgMnPc2d1	molosoidní
psů	pes	k1gMnPc2	pes
na	na	k7c6	na
psích	psí	k2eAgFnPc6d1	psí
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
si	se	k3xPyFc3	se
psů	pes	k1gMnPc2	pes
též	též	k6eAd1	též
vážili	vážit	k5eAaImAgMnP	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Chovali	chovat	k5eAaImAgMnP	chovat
ve	v	k7c6	v
svatyních	svatyně	k1gFnPc6	svatyně
boha	bůh	k1gMnSc2	bůh
Asklépia	Asklépia	k1gFnSc1	Asklépia
posvátné	posvátný	k2eAgMnPc4d1	posvátný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
uzdravovat	uzdravovat	k5eAaImF	uzdravovat
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Lovec	lovec	k1gMnSc1	lovec
Orion	orion	k1gInSc4	orion
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgMnPc4	dva
psy	pes	k1gMnPc4	pes
–	–	k?	–
Procyona	Procyona	k1gFnSc1	Procyona
a	a	k8xC	a
Siria	Siria	k1gFnSc1	Siria
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
nesou	nést	k5eAaImIp3nP	nést
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
–	–	k?	–
Sirius	Sirius	k1gInSc4	Sirius
a	a	k8xC	a
Prokyon	Prokyon	k1gInSc4	Prokyon
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
strážce	strážce	k1gMnSc1	strážce
podsvětí	podsvětí	k1gNnSc2	podsvětí
Kerberos	Kerberos	k1gMnSc1	Kerberos
měl	mít	k5eAaImAgMnS	mít
3	[number]	k4	3
psí	psí	k2eAgFnSc2d1	psí
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
Íkaros	Íkarosa	k1gFnPc2	Íkarosa
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
psa	pes	k1gMnSc4	pes
jménem	jméno	k1gNnSc7	jméno
Maira	Maira	k1gMnSc1	Maira
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dovedl	dovést	k5eAaPmAgMnS	dovést
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
mrtvému	mrtvý	k2eAgNnSc3d1	mrtvé
tělu	tělo	k1gNnSc3	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
chovali	chovat	k5eAaImAgMnP	chovat
především	především	k9	především
velké	velký	k2eAgMnPc4d1	velký
psy	pes	k1gMnPc4	pes
na	na	k7c4	na
boj	boj	k1gInSc4	boj
a	a	k8xC	a
malé	malý	k2eAgMnPc4d1	malý
psy	pes	k1gMnPc4	pes
pro	pro	k7c4	pro
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
širých	širý	k2eAgFnPc6d1	širá
pláních	pláň	k1gFnPc6	pláň
používali	používat	k5eAaImAgMnP	používat
Řekové	Řek	k1gMnPc1	Řek
k	k	k7c3	k
hlídání	hlídání	k1gNnSc4	hlídání
stád	stádo	k1gNnPc2	stádo
ovcí	ovce	k1gFnPc2	ovce
před	před	k7c7	před
vlky	vlk	k1gMnPc7	vlk
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešních	dnešní	k2eAgMnPc2d1	dnešní
pastýřských	pastýřský	k2eAgMnPc2d1	pastýřský
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
psů	pes	k1gMnPc2	pes
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
plemena	plemeno	k1gNnPc1	plemeno
jako	jako	k9	jako
komondor	komondor	k1gMnSc1	komondor
<g/>
,	,	kIx,	,
kuvasz	kuvasz	k1gMnSc1	kuvasz
a	a	k8xC	a
kangal	kangal	k1gMnSc1	kangal
<g/>
.	.	kIx.	.
</s>
<s>
Římskou	římský	k2eAgFnSc4d1	římská
bohyni	bohyně	k1gFnSc4	bohyně
lovu	lov	k1gInSc2	lov
Dianu	Diana	k1gFnSc4	Diana
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
boha	bůh	k1gMnSc2	bůh
obchodu	obchod	k1gInSc2	obchod
Merkura	Merkur	k1gMnSc2	Merkur
<g/>
,	,	kIx,	,
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
byli	být	k5eAaImAgMnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgMnPc1	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
systematickým	systematický	k2eAgInSc7d1	systematický
chovem	chov	k1gInSc7	chov
psa	pes	k1gMnSc2	pes
-	-	kIx~	-
plemenitbou	plemenitba	k1gFnSc7	plemenitba
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnit	vlastnit	k5eAaImF	vlastnit
psa	pes	k1gMnSc4	pes
bylo	být	k5eAaImAgNnS	být
stejně	stejně	k9	stejně
módní	módní	k2eAgNnSc1d1	módní
jako	jako	k8xS	jako
vlastnit	vlastnit	k5eAaImF	vlastnit
otroka	otrok	k1gMnSc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
plemena	plemeno	k1gNnPc4	plemeno
patřili	patřit	k5eAaImAgMnP	patřit
zejména	zejména	k9	zejména
větší	veliký	k2eAgMnPc1d2	veliký
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sloužili	sloužit	k5eAaImAgMnP	sloužit
jako	jako	k9	jako
hlídači	hlídač	k1gMnPc1	hlídač
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
varovné	varovný	k2eAgInPc4d1	varovný
nápisy	nápis	k1gInPc4	nápis
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
archeologové	archeolog	k1gMnPc1	archeolog
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
tabulky	tabulka	k1gFnPc1	tabulka
"	"	kIx"	"
<g/>
Pozor	pozor	k1gInSc1	pozor
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
mají	mít	k5eAaImIp3nP	mít
tedy	tedy	k9	tedy
historii	historie	k1gFnSc4	historie
sahající	sahající	k2eAgFnSc4d1	sahající
až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
byl	být	k5eAaImAgInS	být
i	i	k9	i
místem	místo	k1gNnSc7	místo
psích	psí	k2eAgInPc2d1	psí
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
byla	být	k5eAaImAgNnP	být
šlechtěna	šlechtěn	k2eAgNnPc1d1	šlechtěno
speciální	speciální	k2eAgNnPc1d1	speciální
plemena	plemeno	k1gNnPc1	plemeno
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
i	i	k9	i
lovecká	lovecký	k2eAgNnPc4d1	lovecké
a	a	k8xC	a
společenská	společenský	k2eAgNnPc4d1	společenské
plemena	plemeno	k1gNnPc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgInP	být
nejpopulárnější	populární	k2eAgMnPc1d3	nejpopulárnější
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
vznikala	vznikat	k5eAaImAgFnS	vznikat
řada	řada	k1gFnSc1	řada
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
předky	předek	k1gInPc4	předek
dnešních	dnešní	k2eAgMnPc2d1	dnešní
psů	pes	k1gMnPc2	pes
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
přežila	přežít	k5eAaPmAgFnS	přežít
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Společenští	společenský	k2eAgMnPc1d1	společenský
psi	pes	k1gMnPc1	pes
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
vzácností	vzácnost	k1gFnSc7	vzácnost
a	a	k8xC	a
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
křesťanství	křesťanství	k1gNnSc2	křesťanství
jakožto	jakožto	k8xS	jakožto
náboženství	náboženství	k1gNnSc1	náboženství
ke	k	k7c3	k
psům	pes	k1gMnPc3	pes
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
vyhraněný	vyhraněný	k2eAgInSc1d1	vyhraněný
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pes	pes	k1gMnSc1	pes
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
věrným	věrný	k2eAgMnSc7d1	věrný
průvodcem	průvodce	k1gMnSc7	průvodce
Tobiáše	Tobiáš	k1gMnSc2	Tobiáš
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
lidmi	člověk	k1gMnPc7	člověk
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
uctíván	uctívat	k5eAaImNgMnS	uctívat
i	i	k9	i
přes	přes	k7c4	přes
zákaz	zákaz	k1gInSc4	zákaz
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
svatý	svatý	k2eAgInSc1d1	svatý
Guinefort	Guinefort	k1gInSc1	Guinefort
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
chrt	chrt	k1gMnSc1	chrt
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
ochránce	ochránce	k1gMnSc1	ochránce
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
islámu	islám	k1gInSc6	islám
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nečisté	čistý	k2eNgNnSc4d1	nečisté
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
muslimů	muslim	k1gMnPc2	muslim
psy	pes	k1gMnPc4	pes
nechová	chovat	k5eNaImIp3nS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
zákaz	zákaz	k1gInSc1	zákaz
(	(	kIx(	(
<g/>
haraam	haraam	k1gInSc1	haraam
<g/>
)	)	kIx)	)
chovu	chov	k1gInSc6	chov
psů	pes	k1gMnPc2	pes
ale	ale	k8xC	ale
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
neplatí	platit	k5eNaImIp3nS	platit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
mnoho	mnoho	k6eAd1	mnoho
muslimů	muslim	k1gMnPc2	muslim
myslí	mysl	k1gFnPc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
dotýkat	dotýkat	k5eAaImF	dotýkat
se	se	k3xPyFc4	se
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gFnPc2	jejich
slin	slina	k1gFnPc2	slina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
toto	tento	k3xDgNnSc4	tento
pravidlo	pravidlo	k1gNnSc4	pravidlo
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
saluki	saluk	k1gInPc7	saluk
<g/>
.	.	kIx.	.
</s>
<s>
Saluky	Saluk	k1gInPc1	Saluk
byly	být	k5eAaImAgInP	být
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgInPc1d1	ceněný
a	a	k8xC	a
majitelé	majitel	k1gMnPc1	majitel
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
nedali	dát	k5eNaPmAgMnP	dát
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
chov	chov	k1gInSc1	chov
jak	jak	k6eAd1	jak
loveckých	lovecký	k2eAgInPc2d1	lovecký
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
společenských	společenský	k2eAgMnPc2d1	společenský
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
konat	konat	k5eAaImF	konat
psí	psí	k2eAgFnSc2d1	psí
výstavy	výstava	k1gFnSc2	výstava
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
exteriér	exteriér	k1gInSc4	exteriér
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejich	jejich	k3xOp3gNnSc4	jejich
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
psi	pes	k1gMnPc1	pes
velmi	velmi	k6eAd1	velmi
oblíbení	oblíbený	k2eAgMnPc1d1	oblíbený
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
<g/>
,	,	kIx,	,
chovaní	chovaný	k2eAgMnPc1d1	chovaný
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Psa	pes	k1gMnSc2	pes
chová	chovat	k5eAaImIp3nS	chovat
či	či	k8xC	či
chovalo	chovat	k5eAaImAgNnS	chovat
mnoho	mnoho	k4c1	mnoho
význačných	význačný	k2eAgFnPc2d1	význačná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
(	(	kIx(	(
<g/>
Blondie	Blondie	k1gFnSc1	Blondie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
několik	několik	k4yIc1	několik
portugalských	portugalský	k2eAgMnPc2d1	portugalský
vodních	vodní	k2eAgMnPc2d1	vodní
psů	pes	k1gMnPc2	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
psů	pes	k1gMnPc2	pes
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
nebo	nebo	k8xC	nebo
zakázán	zakázán	k2eAgInSc1d1	zakázán
chov	chov	k1gInSc1	chov
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
nebezpečná	bezpečný	k2eNgNnPc4d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žádné	žádný	k3yNgNnSc1	žádný
plemeno	plemeno	k1gNnSc1	plemeno
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
omezující	omezující	k2eAgInPc1d1	omezující
zákony	zákon	k1gInPc1	zákon
platí	platit	k5eAaImIp3nP	platit
například	například	k6eAd1	například
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zakázanými	zakázaný	k2eAgNnPc7d1	zakázané
plemeny	plemeno	k1gNnPc7	plemeno
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
plemena	plemeno	k1gNnPc1	plemeno
jako	jako	k8xC	jako
americký	americký	k2eAgMnSc1d1	americký
pitbulteriér	pitbulteriér	k1gMnSc1	pitbulteriér
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
bulteriér	bulteriér	k1gMnSc1	bulteriér
<g/>
,	,	kIx,	,
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
doga	doga	k1gFnSc1	doga
<g/>
,	,	kIx,	,
brazilská	brazilský	k2eAgFnSc1d1	brazilská
fila	fila	k1gFnSc1	fila
<g/>
,	,	kIx,	,
bandog	bandog	k1gInSc1	bandog
nebo	nebo	k8xC	nebo
tosa	tosa	k6eAd1	tosa
inu	inu	k9	inu
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
psů	pes	k1gMnPc2	pes
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
majitelé	majitel	k1gMnPc1	majitel
domů	domů	k6eAd1	domů
kvůli	kvůli	k7c3	kvůli
možnému	možný	k2eAgInSc3d1	možný
hluku	hluk	k1gInSc3	hluk
nebo	nebo	k8xC	nebo
znečišťování	znečišťování	k1gNnSc6	znečišťování
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psi	pes	k1gMnPc1	pes
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psí	psí	k2eAgNnSc1d1	psí
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgNnSc1d1	psí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
dodnes	dodnes	k6eAd1	dodnes
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
konzumováno	konzumován	k2eAgNnSc1d1	konzumováno
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
asi	asi	k9	asi
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
se	se	k3xPyFc4	se
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
ročně	ročně	k6eAd1	ročně
zabijí	zabít	k5eAaPmIp3nP	zabít
až	až	k9	až
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
na	na	k7c6	na
psí	psí	k2eAgNnSc4d1	psí
maso	maso	k1gNnSc4	maso
chováno	chovat	k5eAaImNgNnS	chovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
ročně	ročně	k6eAd1	ročně
zabito	zabít	k5eAaPmNgNnS	zabít
až	až	k9	až
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c7	za
tabu	tabu	k1gNnSc7	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
jedlo	jíst	k5eAaImAgNnS	jíst
jen	jen	k9	jen
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
a	a	k8xC	a
islám	islám	k1gInSc1	islám
konzumaci	konzumace	k1gFnSc4	konzumace
psího	psí	k2eAgNnSc2d1	psí
masa	maso	k1gNnSc2	maso
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Bývalo	bývat	k5eAaImAgNnS	bývat
rozšíření	rozšíření	k1gNnSc1	rozšíření
i	i	k9	i
mezi	mezi	k7c7	mezi
Indiány	Indián	k1gMnPc7	Indián
(	(	kIx(	(
<g/>
Apačové	Apač	k1gMnPc1	Apač
<g/>
,	,	kIx,	,
Siouxové	Siouxový	k2eAgFnPc1d1	Siouxový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
hepatitida	hepatitida	k1gFnSc1	hepatitida
–	–	k?	–
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
všech	všecek	k3xTgFnPc2	všecek
kategorií	kategorie	k1gFnPc2	kategorie
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
končí	končit	k5eAaImIp3nS	končit
úhynem	úhyn	k1gInSc7	úhyn
jedince	jedinec	k1gMnSc2	jedinec
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
selhání	selhání	k1gNnSc2	selhání
všech	všecek	k3xTgInPc2	všecek
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
postižená	postižený	k2eAgNnPc1d1	postižené
jsou	být	k5eAaImIp3nP	být
játra	játra	k1gNnPc4	játra
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
spolehlivě	spolehlivě	k6eAd1	spolehlivě
brání	bránit	k5eAaImIp3nS	bránit
nákaze	nákaza	k1gFnSc3	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Leptospiróza	Leptospiróza	k1gFnSc1	Leptospiróza
–	–	k?	–
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobující	způsobující	k2eAgMnSc1d1	způsobující
druh	druh	k1gMnSc1	druh
Leptospira	Leptospiro	k1gNnSc2	Leptospiro
spp	spp	k?	spp
<g/>
.	.	kIx.	.
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
spirochéty	spirochéta	k1gFnPc4	spirochéta
<g/>
,	,	kIx,	,
přenosné	přenosný	k2eAgNnSc1d1	přenosné
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
selháním	selhání	k1gNnSc7	selhání
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
postižené	postižený	k2eAgFnPc1d1	postižená
jsou	být	k5eAaImIp3nP	být
játra	játra	k1gNnPc4	játra
a	a	k8xC	a
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Přírodním	přírodní	k2eAgInSc7d1	přírodní
rezervoárem	rezervoár	k1gInSc7	rezervoár
jsou	být	k5eAaImIp3nP	být
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
obrana	obrana	k1gFnSc1	obrana
je	být	k5eAaImIp3nS	být
vakcinace	vakcinace	k1gFnSc1	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Parainfluenza	Parainfluenza	k1gFnSc1	Parainfluenza
–	–	k?	–
virová	virový	k2eAgFnSc1d1	virová
infekční	infekční	k2eAgFnSc1d1	infekční
laryngotracheitida	laryngotracheitida	k1gFnSc1	laryngotracheitida
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
psincový	psincový	k2eAgInSc1d1	psincový
kašel	kašel	k1gInSc1	kašel
<g/>
"	"	kIx"	"
způsobující	způsobující	k2eAgInSc1d1	způsobující
záchvatovitý	záchvatovitý	k2eAgInSc1d1	záchvatovitý
silný	silný	k2eAgInSc1d1	silný
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
lze	lze	k6eAd1	lze
předcházet	předcházet	k5eAaImF	předcházet
vakcinací	vakcinace	k1gFnSc7	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Parvoviróza	Parvoviróza	k1gFnSc1	Parvoviróza
–	–	k?	–
akutní	akutní	k2eAgFnSc1d1	akutní
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
virové	virový	k2eAgNnSc4d1	virové
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
končí	končit	k5eAaImIp3nS	končit
jejich	jejich	k3xOp3gFnSc7	jejich
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dehydratace	dehydratace	k1gFnSc2	dehydratace
a	a	k8xC	a
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
sepse	sepse	k1gFnSc2	sepse
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
postihuje	postihovat	k5eAaImIp3nS	postihovat
střevo	střevo	k1gNnSc4	střevo
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc4d1	kostní
dřeň	dřeň	k1gFnSc4	dřeň
<g/>
,	,	kIx,	,
lymfoidní	lymfoidní	k2eAgFnSc4d1	lymfoidní
tkáň	tkáň	k1gFnSc4	tkáň
a	a	k8xC	a
svalovinu	svalovina	k1gFnSc4	svalovina
srdce	srdce	k1gNnSc2	srdce
myokard	myokard	k1gInSc1	myokard
<g/>
.	.	kIx.	.
</s>
<s>
Nákaze	nákaza	k1gFnSc3	nákaza
se	se	k3xPyFc4	se
bráníme	bránit	k5eAaImIp1nP	bránit
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
vakcinací	vakcinace	k1gFnSc7	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
nejvíc	nejvíc	k6eAd1	nejvíc
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
pro	pro	k7c4	pro
štěně	štěně	k1gNnSc4	štěně
<g/>
.	.	kIx.	.
</s>
<s>
Psinka	psinka	k1gFnSc1	psinka
–	–	k?	–
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
tlumené	tlumený	k2eAgFnPc1d1	tlumená
vakcinací	vakcinace	k1gFnSc7	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
i	i	k9	i
transplacentárně	transplacentárně	k6eAd1	transplacentárně
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
zánětlivá	zánětlivý	k2eAgFnSc1d1	zánětlivá
a	a	k8xC	a
nezánětlivá	zánětlivý	k2eNgFnSc1d1	nezánětlivá
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Postiženi	postižen	k2eAgMnPc1d1	postižen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
všichni	všechen	k3xTgMnPc1	všechen
masožravci	masožravec	k1gMnPc1	masožravec
<g/>
.	.	kIx.	.
</s>
<s>
Vzteklina	vzteklina	k1gMnSc1	vzteklina
–	–	k?	–
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
akutní	akutní	k2eAgNnSc1d1	akutní
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
přenosné	přenosný	k2eAgNnSc1d1	přenosné
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
psů	pes	k1gMnPc2	pes
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
povinnost	povinnost	k1gFnSc4	povinnost
u	u	k7c2	u
nich	on	k3xPp3gNnPc2	on
provést	provést	k5eAaPmF	provést
vakcinaci	vakcinace	k1gFnSc4	vakcinace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Prvoci	prvok	k1gMnPc1	prvok
<g/>
:	:	kIx,	:
Lamblie	Lamblie	k1gFnSc1	Lamblie
střevní	střevní	k2eAgFnSc1d1	střevní
<g/>
,	,	kIx,	,
Cryptosporidium	Cryptosporidium	k1gNnSc1	Cryptosporidium
parvum	parvum	k1gInSc1	parvum
<g/>
,	,	kIx,	,
Neospora	Neospora	k1gFnSc1	Neospora
caninum	caninum	k1gInSc1	caninum
<g/>
,	,	kIx,	,
Babesia	Babesia	k1gFnSc1	Babesia
canis	canis	k1gFnSc2	canis
Helminti	Helminť	k1gFnSc2	Helminť
<g/>
:	:	kIx,	:
Škrkavka	škrkavka	k1gFnSc1	škrkavka
psí	psí	k2eAgFnSc1d1	psí
<g/>
,	,	kIx,	,
Škrkavka	škrkavka	k1gMnSc1	škrkavka
šelmí	šelmí	k2eAgFnSc2d1	šelmí
<g/>
,	,	kIx,	,
Tasemnice	tasemnice	k1gFnSc2	tasemnice
psí	psí	k2eAgFnSc2d1	psí
<g/>
,	,	kIx,	,
Měchožil	měchožil	k1gMnSc1	měchožil
zhoubný	zhoubný	k2eAgMnSc1d1	zhoubný
<g/>
,	,	kIx,	,
Ancylostoma	Ancylostoma	k1gFnSc1	Ancylostoma
caninum	caninum	k1gInSc1	caninum
<g/>
,	,	kIx,	,
Uncinaria	Uncinarium	k1gNnPc1	Uncinarium
stenocephala	stenocephal	k1gMnSc2	stenocephal
<g/>
,	,	kIx,	,
Trichuris	Trichuris	k1gFnSc3	Trichuris
vulpis	vulpis	k1gFnPc2	vulpis
Roztoči	roztoč	k1gMnPc1	roztoč
<g/>
:	:	kIx,	:
Demodex	Demodex	k1gInSc1	Demodex
canis	canis	k1gInSc1	canis
<g/>
,	,	kIx,	,
Sarcoptes	Sarcoptes	k1gInSc1	Sarcoptes
canis	canis	k1gFnSc1	canis
<g/>
,	,	kIx,	,
Cheyletiella	Cheyletiella	k1gFnSc1	Cheyletiella
yasguri	yasguri	k1gNnSc2	yasguri
<g/>
,	,	kIx,	,
Trombicula	Trombiculum	k1gNnSc2	Trombiculum
autumnalis	autumnalis	k1gFnSc2	autumnalis
Hmyz	hmyz	k1gInSc1	hmyz
<g/>
:	:	kIx,	:
Ctenocephalides	Ctenocephalides	k1gInSc1	Ctenocephalides
canis	canis	k1gFnSc2	canis
<g/>
,	,	kIx,	,
Linognatus	Linognatus	k1gMnSc1	Linognatus
setosus	setosus	k1gMnSc1	setosus
Ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
:	:	kIx,	:
Klíště	klíště	k1gNnSc1	klíště
obecné	obecný	k2eAgFnSc2d1	obecná
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kynologie	kynologie	k1gFnSc2	kynologie
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kynologie	kynologie	k1gFnSc1	kynologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
unií	unie	k1gFnSc7	unie
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
pobočku	pobočka	k1gFnSc4	pobočka
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
kynologické	kynologický	k2eAgFnSc2d1	kynologická
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Psí	psí	k2eAgMnPc4d1	psí
sporty	sport	k1gInPc4	sport
jsou	být	k5eAaImIp3nP	být
speciální	speciální	k2eAgInPc4d1	speciální
sporty	sport	k1gInPc4	sport
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
s	s	k7c7	s
majiteli	majitel	k1gMnPc7	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
účelnost	účelnost	k1gFnSc4	účelnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
budování	budování	k1gNnSc4	budování
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
psovodem	psovod	k1gMnSc7	psovod
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Pachová	pachový	k2eAgFnSc1d1	pachová
poslušnost	poslušnost	k1gFnSc1	poslušnost
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Poslušnost	poslušnost	k1gFnSc1	poslušnost
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Obrana	obrana	k1gFnSc1	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
psí	psí	k2eAgInPc4d1	psí
sporty	sport	k1gInPc4	sport
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
agility	agilita	k1gFnSc2	agilita
<g/>
,	,	kIx,	,
coursing	coursing	k1gInSc1	coursing
<g/>
,	,	kIx,	,
flyball	flyball	k1gInSc1	flyball
<g/>
,	,	kIx,	,
obedience	obedience	k1gFnSc1	obedience
nebo	nebo	k8xC	nebo
tanec	tanec	k1gInSc1	tanec
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Plemena	plemeno	k1gNnSc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
soužití	soužití	k1gNnSc2	soužití
psa	pes	k1gMnSc2	pes
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
nejrůznější	různý	k2eAgNnPc4d3	nejrůznější
plemena	plemeno	k1gNnPc4	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
obrazem	obraz	k1gInSc7	obraz
lidských	lidský	k2eAgFnPc2d1	lidská
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
máme	mít	k5eAaImIp1nP	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Plemena	plemeno	k1gNnPc1	plemeno
ovčácká	ovčácký	k2eAgNnPc1d1	ovčácké
<g/>
,	,	kIx,	,
pastevecká	pastevecký	k2eAgNnPc1d1	pastevecké
a	a	k8xC	a
honácká	honácký	k2eAgNnPc1d1	honácké
Pinčové	pinč	k1gMnPc5	pinč
<g/>
,	,	kIx,	,
knírači	knírač	k1gMnPc5	knírač
<g/>
,	,	kIx,	,
plemena	plemeno	k1gNnPc4	plemeno
molossoidní	molossoidní	k2eAgNnPc4d1	molossoidní
a	a	k8xC	a
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
salašničtí	salašnický	k2eAgMnPc1d1	salašnický
psi	pes	k1gMnPc1	pes
Teriéři	teriér	k1gMnPc1	teriér
Jezevčíci	jezevčík	k1gMnPc1	jezevčík
Špicové	špic	k1gMnPc1	špic
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
primitivní	primitivní	k2eAgMnSc1d1	primitivní
plemena	plemeno	k1gNnPc1	plemeno
Honiči	honič	k1gMnPc1	honič
a	a	k8xC	a
barváři	barvář	k1gMnSc6	barvář
Ohaři	ohař	k1gMnSc6	ohař
Slídiči	slídič	k1gMnSc6	slídič
<g/>
,	,	kIx,	,
retrívři	retrívr	k1gMnPc1	retrívr
a	a	k8xC	a
vodní	vodní	k2eAgMnPc1d1	vodní
psi	pes	k1gMnPc1	pes
Plemena	plemeno	k1gNnSc2	plemeno
společenská	společenský	k2eAgNnPc4d1	společenské
Chrti	chrt	k1gMnPc1	chrt
FCI	FCI	kA	FCI
neuznaná	uznaný	k2eNgNnPc4d1	neuznané
plemena	plemeno	k1gNnPc4	plemeno
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
United	United	k1gMnSc1	United
Kennel	Kennel	k1gMnSc1	Kennel
Clubs	Clubsa	k1gFnPc2	Clubsa
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
</s>
<s>
Honiči	honič	k1gMnPc1	honič
Plemena	plemeno	k1gNnSc2	plemeno
lovecká	lovecký	k2eAgFnSc1d1	lovecká
Teriéři	teriér	k1gMnPc1	teriér
Plemena	plemeno	k1gNnPc4	plemeno
užitková	užitkový	k2eAgNnPc4d1	užitkové
Plemena	plemeno	k1gNnPc4	plemeno
pracovní	pracovní	k2eAgNnPc4d1	pracovní
Plemena	plemeno	k1gNnPc4	plemeno
společenská	společenský	k2eAgNnPc4d1	společenské
Chov	chov	k1gInSc4	chov
psů	pes	k1gMnPc2	pes
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
okolí	okolí	k1gNnSc4	okolí
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
znečišťování	znečišťování	k1gNnSc1	znečišťování
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
výkaly	výkal	k1gInPc7	výkal
a	a	k8xC	a
močí	moč	k1gFnSc7	moč
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
český	český	k2eAgInSc1d1	český
vynález	vynález	k1gInSc1	vynález
sáčku	sáček	k1gInSc2	sáček
na	na	k7c4	na
psí	psí	k2eAgInPc4d1	psí
exkrementy	exkrement	k1gInPc4	exkrement
<g/>
)	)	kIx)	)
obtěžování	obtěžování	k1gNnSc1	obtěžování
hlukem	hluk	k1gInSc7	hluk
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
štěkot	štěkot	k1gInSc1	štěkot
<g/>
,	,	kIx,	,
vytí	vytí	k1gNnSc1	vytí
<g/>
,	,	kIx,	,
kvílení	kvílení	k1gNnSc1	kvílení
<g/>
)	)	kIx)	)
napadání	napadání	k1gNnSc1	napadání
lidí	člověk	k1gMnPc2	člověk
psem	pes	k1gMnSc7	pes
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
se	s	k7c7	s
smrtelnými	smrtelný	k2eAgInPc7d1	smrtelný
následky	následek	k1gInPc7	následek
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
Bojová	bojový	k2eAgFnSc1d1	bojová
plemena	plemeno	k1gNnPc4	plemeno
psů	pes	k1gMnPc2	pes
nebo	nebo	k8xC	nebo
Zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
psů	pes	k1gMnPc2	pes
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plašení	plašení	k1gNnSc2	plašení
nebo	nebo	k8xC	nebo
zabíjení	zabíjení	k1gNnSc2	zabíjení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglofonním	anglofonní	k2eAgNnSc6d1	anglofonní
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
dichotmii	dichotmie	k1gFnSc4	dichotmie
cat	cat	k?	cat
people	people	k6eAd1	people
x	x	k?	x
dog	doga	k1gFnPc2	doga
people	people	k6eAd1	people
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
kočičí	kočičí	k2eAgMnPc1d1	kočičí
lidé	člověk	k1gMnPc1	člověk
vs	vs	k?	vs
<g/>
.	.	kIx.	.
psí	psí	k2eAgMnPc1d1	psí
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
afinity	afinita	k1gFnSc2	afinita
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
ze	z	k7c2	z
zmiňovaných	zmiňovaný	k2eAgInPc2d1	zmiňovaný
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Cat	Cat	k1gMnSc1	Cat
people	people	k6eAd1	people
and	and	k?	and
dog	doga	k1gFnPc2	doga
people	people	k6eAd1	people
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doris	Doris	k1gFnSc1	Doris
Baumannová	Baumannová	k1gFnSc1	Baumannová
<g/>
:	:	kIx,	:
Výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
výcvik	výcvik	k1gInSc1	výcvik
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
Aktuell	Aktuella	k1gFnPc2	Aktuella
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88733	[number]	k4	88733
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Eberhard	Eberhard	k1gInSc4	Eberhard
Trumler	Trumler	k1gInSc4	Trumler
<g/>
:	:	kIx,	:
Tykáme	tykat	k5eAaImIp1nP	tykat
si	se	k3xPyFc3	se
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
Dona	Don	k1gMnSc4	Don
<g/>
,	,	kIx,	,
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86136	[number]	k4	86136
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
Fogle	Fogle	k1gFnSc2	Fogle
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
psů	pes	k1gMnPc2	pes
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
,	,	kIx,	,
Cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7181	[number]	k4	7181
<g/>
-	-	kIx~	-
<g/>
456	[number]	k4	456
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Bojová	bojový	k2eAgNnPc1d1	bojové
plemena	plemeno	k1gNnPc1	plemeno
psů	pes	k1gMnPc2	pes
Dingo	dingo	k1gMnSc1	dingo
Kynologie	kynologie	k1gFnSc2	kynologie
Plemena	plemeno	k1gNnSc2	plemeno
psů	pes	k1gMnPc2	pes
Pracovní	pracovní	k2eAgFnSc2d1	pracovní
pes	peso	k1gNnPc2	peso
Psí	psí	k2eAgFnSc1d1	psí
bouda	bouda	k1gFnSc1	bouda
Psík	psík	k1gMnSc1	psík
mývalovitý	mývalovitý	k2eAgInSc1d1	mývalovitý
Psinec	psinec	k1gInSc1	psinec
(	(	kIx(	(
<g/>
psírna	psírna	k1gFnSc1	psírna
<g/>
)	)	kIx)	)
Psohlavci	psohlavec	k1gMnPc1	psohlavec
Psovod	psovod	k1gMnSc1	psovod
Štěně	štěně	k1gNnSc1	štěně
Vlk	Vlk	k1gMnSc1	Vlk
Zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
psů	pes	k1gMnPc2	pes
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pes	pes	k1gMnSc1	pes
domácí	domácí	k2eAgMnSc1d1	domácí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
pes	pes	k1gMnSc1	pes
domácí	domácí	k2eAgMnSc1d1	domácí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pes	pes	k1gMnSc1	pes
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc4	lupus
familiaris	familiaris	k1gFnSc2	familiaris
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
kynologická	kynologický	k2eAgFnSc1d1	kynologická
unie	unie	k1gFnSc1	unie
</s>
