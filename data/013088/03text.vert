<p>
<s>
Heidi	Heid	k1gMnPc1	Heid
Rakels	Rakelsa	k1gFnPc2	Rakelsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
Lovaň	Lovaň	k1gFnPc2	Lovaň
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
Belgie	Belgie	k1gFnSc2	Belgie
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
majitelkou	majitelka	k1gFnSc7	majitelka
bronzové	bronzový	k2eAgFnSc2d1	bronzová
olympijské	olympijský	k2eAgFnSc2d1	olympijská
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
judem	judo	k1gNnSc7	judo
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
15	[number]	k4	15
letech	let	k1gInPc6	let
v	v	k7c4	v
Maasmechelenu	Maasmechelen	k2eAgFnSc4d1	Maasmechelen
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
připravovala	připravovat	k5eAaImAgFnS	připravovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Eddy	Edda	k1gFnSc2	Edda
Vinckiera	Vinckiero	k1gNnSc2	Vinckiero
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
nejvíce	hodně	k6eAd3	hodně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
váha	váha	k1gFnSc1	váha
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
váhová	váhový	k2eAgFnSc1d1	váhová
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vládla	vládnout	k5eAaImAgFnS	vládnout
její	její	k3xOp3gFnSc1	její
krajanka	krajanka	k1gFnSc1	krajanka
Ulla	Ull	k2eAgFnSc1d1	Ulla
Werbroucková	Werbroucková	k1gFnSc1	Werbroucková
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
jak	jak	k6eAd1	jak
startovat	startovat	k5eAaBmF	startovat
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
akci	akce	k1gFnSc6	akce
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgNnP	dokázat
zmáčknout	zmáčknout	k5eAaPmF	zmáčknout
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
váhy	váha	k1gFnSc2	váha
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medaile	medaile	k1gFnSc1	medaile
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
stejného	stejný	k2eAgNnSc2d1	stejné
rizika	riziko	k1gNnSc2	riziko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tentokráte	tentokráte	k?	tentokráte
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
nevydařila	vydařit	k5eNaPmAgFnS	vydařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spásným	spásný	k2eAgInSc7d1	spásný
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc6	on
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
váhové	váhový	k2eAgFnPc1d1	váhová
limity	limita	k1gFnPc1	limita
<g/>
.	.	kIx.	.
</s>
<s>
Ulla	Ull	k2eAgFnSc1d1	Ulla
Werbroucková	Werbroucková	k1gFnSc1	Werbroucková
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
váhy	váha	k1gFnSc2	váha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ji	on	k3xPp3gFnSc4	on
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
a	a	k8xC	a
sahala	sahat	k5eAaImAgFnS	sahat
po	po	k7c6	po
bronzové	bronzový	k2eAgFnSc6d1	bronzová
medaili	medaile	k1gFnSc6	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Obsadila	obsadit	k5eAaPmAgFnS	obsadit
nakonec	nakonec	k6eAd1	nakonec
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
vah	váha	k1gFnPc2	váha
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Heidi	Heid	k1gMnPc1	Heid
Rakelsové	Rakelsová	k1gFnPc4	Rakelsová
na	na	k7c4	na
Judoinside	Judoinsid	k1gInSc5	Judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
