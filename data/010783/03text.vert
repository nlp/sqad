<p>
<s>
Mozzarella	Mozzarella	k6eAd1	Mozzarella
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
sýr	sýr	k1gInSc1	sýr
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozzarella	Mozzarella	k1gFnSc1	Mozzarella
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
oválu	ovál	k1gInSc2	ovál
a	a	k8xC	a
uskladňuje	uskladňovat	k5eAaImIp3nS	uskladňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
miskách	miska	k1gFnPc6	miska
nebo	nebo	k8xC	nebo
pevně	pevně	k6eAd1	pevně
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
obalech	obal	k1gInPc6	obal
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
syrovátku	syrovátka	k1gFnSc4	syrovátka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sněhobílou	sněhobílý	k2eAgFnSc4d1	sněhobílá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc4d1	tenká
lesklou	lesklý	k2eAgFnSc4d1	lesklá
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
mladého	mladý	k2eAgInSc2d1	mladý
sýra	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
pružná	pružný	k2eAgFnSc1d1	pružná
a	a	k8xC	a
poddajná	poddajný	k2eAgFnSc1d1	poddajná
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
lehce	lehko	k6eAd1	lehko
krájet	krájet	k5eAaImF	krájet
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
sýr	sýr	k1gInSc1	sýr
zraje	zrát	k5eAaImIp3nS	zrát
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
měkčím	měkčit	k5eAaImIp1nS	měkčit
a	a	k8xC	a
zintenzivňuje	zintenzivňovat	k5eAaImIp3nS	zintenzivňovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
gastronomické	gastronomický	k2eAgInPc4d1	gastronomický
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvaruje	tvarovat	k5eAaImIp3nS	tvarovat
do	do	k7c2	do
kvádrů	kvádr	k1gInPc2	kvádr
<g/>
,	,	kIx,	,
chuťově	chuťově	k6eAd1	chuťově
se	se	k3xPyFc4	se
nikterak	nikterak	k6eAd1	nikterak
neodlišuje	odlišovat	k5eNaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgFnSc1d1	ideální
na	na	k7c4	na
smažení	smažení	k1gNnSc4	smažení
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
trojobalu	trojobal	k1gInSc6	trojobal
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
než	než	k8xS	než
klasický	klasický	k2eAgInSc4d1	klasický
Eidam	eidam	k1gInSc4	eidam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
mozzare	mozzar	k1gMnSc5	mozzar
–	–	k?	–
odříznout	odříznout	k5eAaPmF	odříznout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
formován	formovat	k5eAaImNgInS	formovat
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozzarella	Mozzarella	k6eAd1	Mozzarella
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc1	sýr
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
z	z	k7c2	z
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
točeného	točený	k2eAgInSc2d1	točený
tvarohu	tvaroh	k1gInSc2	tvaroh
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mléko	mléko	k1gNnSc1	mléko
srazilo	srazit	k5eAaPmAgNnS	srazit
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
iniciační	iniciační	k2eAgFnPc1d1	iniciační
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
syřidlo	syřidlo	k1gNnSc1	syřidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tvaroh	tvaroh	k1gInSc1	tvaroh
se	se	k3xPyFc4	se
nakrájí	nakrájet	k5eAaPmIp3nS	nakrájet
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc4d1	malý
kousky	kousek	k1gInPc4	kousek
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
ze	z	k7c2	z
syrovátky	syrovátka	k1gFnSc2	syrovátka
a	a	k8xC	a
míchá	míchat	k5eAaImIp3nS	míchat
se	se	k3xPyFc4	se
ve	v	k7c6	v
vařící	vařící	k2eAgFnSc6d1	vařící
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
nevytvoří	vytvořit	k5eNaPmIp3nP	vytvořit
hladká	hladký	k2eAgFnSc1d1	hladká
a	a	k8xC	a
lesklá	lesklý	k2eAgFnSc1d1	lesklá
masa	masa	k1gFnSc1	masa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
odkrajují	odkrajovat	k5eAaImIp3nP	odkrajovat
malé	malý	k2eAgInPc1d1	malý
kousky	kousek	k1gInPc1	kousek
<g/>
,	,	kIx,	,
formují	formovat	k5eAaImIp3nP	formovat
se	se	k3xPyFc4	se
do	do	k7c2	do
oválů	ovál	k1gInPc2	ovál
a	a	k8xC	a
namáčejí	namáčet	k5eAaImIp3nP	namáčet
do	do	k7c2	do
slaného	slaný	k2eAgInSc2d1	slaný
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgFnSc1d1	originální
mozzarella	mozzarella	k1gFnSc1	mozzarella
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
buvolího	buvolí	k2eAgNnSc2d1	buvolí
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
"	"	kIx"	"
<g/>
Mozzarella	Mozzarella	k1gFnSc1	Mozzarella
di	di	k?	di
Bufalla	Bufalla	k1gFnSc1	Bufalla
Campana	Campana	k1gFnSc1	Campana
<g/>
"	"	kIx"	"
D.O.C	D.O.C	k1gFnSc1	D.O.C
z	z	k7c2	z
buvolího	buvolí	k2eAgNnSc2d1	buvolí
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
mozzarella	mozzarello	k1gNnSc2	mozzarello
z	z	k7c2	z
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
správně	správně	k6eAd1	správně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
Fior	Fior	k1gMnSc1	Fior
di	di	k?	di
latte	latte	k5eAaPmIp2nP	latte
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
mléčný	mléčný	k2eAgInSc4d1	mléčný
květ	květ	k1gInSc4	květ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
i	i	k9	i
mozzarella	mozzarella	k1gFnSc1	mozzarella
smíchaná	smíchaný	k2eAgFnSc1d1	smíchaná
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
typu	typ	k1gInSc2	typ
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
také	také	k9	také
mozzarella	mozzarella	k6eAd1	mozzarella
uzená	uzený	k2eAgFnSc1d1	uzená
"	"	kIx"	"
<g/>
affumicata	affumicata	k1gFnSc1	affumicata
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
affumicato	affumicato	k6eAd1	affumicato
=	=	kIx~	=
uzený	uzený	k2eAgMnSc1d1	uzený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Provola	Provola	k1gFnSc1	Provola
<g/>
,	,	kIx,	,
s	s	k7c7	s
nazlátle	nazlátle	k6eAd1	nazlátle
hnědou	hnědý	k2eAgFnSc7d1	hnědá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
výraznější	výrazný	k2eAgFnSc7d2	výraznější
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
uzená	uzený	k2eAgFnSc1d1	uzená
nad	nad	k7c7	nad
ohněm	oheň	k1gInSc7	oheň
z	z	k7c2	z
místních	místní	k2eAgInPc2d1	místní
druhů	druh	k1gInPc2	druh
dřev	dřevo	k1gNnPc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozzarella	Mozzarella	k6eAd1	Mozzarella
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
velikostech	velikost	k1gFnPc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
formy	forma	k1gFnPc1	forma
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
kromě	kromě	k7c2	kromě
klasické	klasický	k2eAgFnSc2d1	klasická
koule	koule	k1gFnSc2	koule
také	také	k9	také
"	"	kIx"	"
<g/>
ciliegine	cilieginout	k5eAaPmIp3nS	cilieginout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
třešničky	třešnička	k1gFnPc1	třešnička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nodini	nodin	k2eAgMnPc1d1	nodin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
uzlíky	uzlík	k1gInPc1	uzlík
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
desítky	desítka	k1gFnPc4	desítka
různých	různý	k2eAgInPc2d1	různý
dalších	další	k2eAgInPc2d1	další
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mozzarella	Mozzarella	k6eAd1	Mozzarella
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
přidává	přidávat	k5eAaImIp3nS	přidávat
na	na	k7c4	na
pizzu	pizza	k1gFnSc4	pizza
<g/>
,	,	kIx,	,
těstoviny	těstovina	k1gFnPc1	těstovina
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
předkrmem	předkrm	k1gInSc7	předkrm
je	být	k5eAaImIp3nS	být
salát	salát	k1gInSc4	salát
Caprese	Caprese	k1gFnSc2	Caprese
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typicky	typicky	k6eAd1	typicky
letní	letní	k2eAgInSc1d1	letní
salát	salát	k1gInSc1	salát
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejsnadnějších	snadný	k2eAgInPc2d3	nejsnazší
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgNnSc1	jaký
může	moct	k5eAaImIp3nS	moct
italská	italský	k2eAgFnSc1d1	italská
kuchyně	kuchyně	k1gFnSc1	kuchyně
nabídnout	nabídnout	k5eAaPmF	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
talíř	talíř	k1gInSc4	talíř
se	se	k3xPyFc4	se
střídavě	střídavě	k6eAd1	střídavě
pokládají	pokládat	k5eAaImIp3nP	pokládat
plátky	plátek	k1gInPc4	plátek
rajčat	rajče	k1gNnPc2	rajče
a	a	k8xC	a
mozzarelly	mozzarella	k1gFnSc2	mozzarella
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
posype	posypat	k5eAaPmIp3nS	posypat
lístečky	lísteček	k1gInPc4	lísteček
čerstvě	čerstvě	k6eAd1	čerstvě
utržené	utržený	k2eAgFnPc4d1	utržená
bazalky	bazalka	k1gFnPc4	bazalka
<g/>
,	,	kIx,	,
osolí	osolit	k5eAaPmIp3nS	osolit
a	a	k8xC	a
opepří	opepřit	k5eAaPmIp3nS	opepřit
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
podáváním	podávání	k1gNnSc7	podávání
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
zalije	zalít	k5eAaPmIp3nS	zalít
panenským	panenský	k2eAgInSc7d1	panenský
olivovým	olivový	k2eAgInSc7d1	olivový
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Originální	originální	k2eAgFnSc4d1	originální
mozzarellu	mozzarella	k1gFnSc4	mozzarella
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
buvolů	buvol	k1gMnPc2	buvol
chovaných	chovaný	k2eAgMnPc2d1	chovaný
v	v	k7c6	v
Kampánii	Kampánie	k1gFnSc6	Kampánie
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
údajně	údajně	k6eAd1	údajně
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
sýrařů	sýrař	k1gMnPc2	sýrař
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
Dubaji	Dubaj	k1gInSc6	Dubaj
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mozzarella	mozzarello	k1gNnSc2	mozzarello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
mozzarelly	mozzarella	k1gFnSc2	mozzarella
<g/>
,	,	kIx,	,
speciality	specialita	k1gFnPc4	specialita
</s>
</p>
<p>
<s>
Buvolí	buvolí	k2eAgMnSc1d1	buvolí
mozzarella	mozzarella	k1gMnSc1	mozzarella
-	-	kIx~	-
Sýromil	Sýromil	k1gMnSc1	Sýromil
</s>
</p>
