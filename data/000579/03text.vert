<s>
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
prvního	první	k4xOgInSc2	první
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
svazků	svazek	k1gInPc2	svazek
románu	román	k1gInSc2	román
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
hobit	hobit	k1gMnSc1	hobit
Frodo	Frodo	k1gNnSc1	Frodo
<g/>
,	,	kIx,	,
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
dědic	dědic	k1gMnSc1	dědic
Bilba	Bilb	k1gMnSc2	Bilb
Pytlíka	pytlík	k1gMnSc2	pytlík
<g/>
,	,	kIx,	,
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
čaroděje	čaroděj	k1gMnSc2	čaroděj
Gandalfa	Gandalf	k1gMnSc2	Gandalf
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc4	jeho
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zdědil	zdědit	k5eAaPmAgInS	zdědit
po	po	k7c6	po
Bilbovi	Bilba	k1gMnSc6	Bilba
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gInSc4	on
nabyl	nabýt	k5eAaPmAgInS	nabýt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
části	část	k1gFnSc6	část
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hobit	hobit	k1gMnSc1	hobit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
jím	jíst	k5eAaImIp1nS	jíst
za	za	k7c4	za
víceméně	víceméně	k9	víceméně
hračku	hračka	k1gFnSc4	hračka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
kdysi	kdysi	k6eAd1	kdysi
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
Temný	temný	k2eAgMnSc1d1	temný
pán	pán	k1gMnSc1	pán
Sauron	Sauron	k1gMnSc1	Sauron
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
do	do	k7c2	do
prstenu	prsten	k1gInSc2	prsten
přejít	přejít	k5eAaPmF	přejít
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
podrobil	podrobit	k5eAaPmAgMnS	podrobit
obyvatele	obyvatel	k1gMnPc4	obyvatel
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
především	především	k9	především
elfy	elf	k1gMnPc4	elf
<g/>
.	.	kIx.	.
</s>
<s>
Dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sauron	Sauron	k1gInSc1	Sauron
byl	být	k5eAaImAgInS	být
kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
poražen	poražen	k2eAgInSc4d1	poražen
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
odňat	odňat	k2eAgMnSc1d1	odňat
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
opět	opět	k6eAd1	opět
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
Jeden	jeden	k4xCgInSc4	jeden
prsten	prsten	k1gInSc4	prsten
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k9	již
Sauron	Sauron	k1gMnSc1	Sauron
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
Bilbovi	Bilba	k1gMnSc6	Bilba
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
Frodo	Frodo	k1gNnSc4	Frodo
opustit	opustit	k5eAaPmF	opustit
Kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Prstenu	prsten	k1gInSc2	prsten
Sauron	Sauron	k1gInSc1	Sauron
nezmocnil	zmocnit	k5eNaPmAgMnS	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zahradníkem	zahradník	k1gMnSc7	zahradník
Samvědem	Samvěd	k1gMnSc7	Samvěd
Křepelkou	křepelka	k1gFnSc7	křepelka
(	(	kIx(	(
<g/>
Samem	Samos	k1gInSc7	Samos
<g/>
)	)	kIx)	)
a	a	k8xC	a
přáteli	přítel	k1gMnPc7	přítel
Smělmírem	Smělmír	k1gMnSc7	Smělmír
Brandorádem	Brandorád	k1gMnSc7	Brandorád
(	(	kIx(	(
<g/>
Smíškem	smíšek	k1gInSc7	smíšek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Peregrinem	Peregrin	k1gInSc7	Peregrin
Bralem	Bralo	k1gNnSc7	Bralo
(	(	kIx(	(
<g/>
Pipinem	Pipino	k1gNnSc7	Pipino
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
Gandalfovu	Gandalfův	k2eAgFnSc4d1	Gandalfova
radu	rada	k1gFnSc4	rada
do	do	k7c2	do
elfského	elfské	k1gNnSc2	elfské
Posledního	poslední	k2eAgInSc2d1	poslední
Domáckého	domácký	k2eAgInSc2d1	domácký
domu	dům	k1gInSc2	dům
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Sauronova	Sauronův	k2eAgFnSc1d1	Sauronova
moc	moc	k1gFnSc1	moc
nesahá	sahat	k5eNaImIp3nS	sahat
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
je	on	k3xPp3gInPc4	on
odhalí	odhalit	k5eAaPmIp3nP	odhalit
Černí	černý	k2eAgMnPc1d1	černý
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
Prstenové	prstenový	k2eAgInPc1d1	prstenový
přízraky	přízrak	k1gInPc1	přízrak
<g/>
,	,	kIx,	,
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
služebníci	služebník	k1gMnPc1	služebník
<g/>
,	,	kIx,	,
a	a	k8xC	a
hobiti	hobit	k1gMnPc1	hobit
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
prchat	prchat	k5eAaImF	prchat
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
pronásledováním	pronásledování	k1gNnSc7	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
další	další	k2eAgNnSc4d1	další
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
(	(	kIx(	(
<g/>
málem	málem	k6eAd1	málem
podlehnou	podlehnout	k5eAaPmIp3nP	podlehnout
zlému	zlý	k2eAgNnSc3d1	zlé
duchu	duch	k1gMnSc6	duch
mohyly	mohyla	k1gFnSc2	mohyla
v	v	k7c6	v
Mohylových	mohylový	k2eAgFnPc6d1	Mohylová
vrších	vrš	k1gFnPc6	vrš
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Hůrky	hůrka	k1gFnSc2	hůrka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
Aragornem	Aragorn	k1gInSc7	Aragorn
<g/>
,	,	kIx,	,
dědicem	dědic	k1gMnSc7	dědic
starobylé	starobylý	k2eAgFnSc2d1	starobylá
lidské	lidský	k2eAgFnSc2d1	lidská
říše	říš	k1gFnSc2	říš
Gondor	Gondora	k1gFnPc2	Gondora
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
však	však	k9	však
hobitům	hobit	k1gMnPc3	hobit
představí	představit	k5eAaPmIp3nS	představit
jako	jako	k9	jako
hraničář	hraničář	k1gMnSc1	hraničář
Chodec	chodec	k1gMnSc1	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Aragorn	Aragorn	k1gNnSc1	Aragorn
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
hobitům	hobit	k1gMnPc3	hobit
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Roklinky	roklinka	k1gFnSc2	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
skutečně	skutečně	k6eAd1	skutečně
dorazí	dorazit	k5eAaPmIp3nP	dorazit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
cestou	cesta	k1gFnSc7	cesta
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Větrově	větrově	k6eAd1	větrově
téměř	téměř	k6eAd1	téměř
polapeni	polapit	k5eAaPmNgMnP	polapit
Prstenovými	prstenový	k2eAgInPc7d1	prstenový
přízraky	přízrak	k1gInPc7	přízrak
<g/>
;	;	kIx,	;
Frodo	Frodo	k1gNnSc1	Frodo
je	být	k5eAaImIp3nS	být
těžce	těžce	k6eAd1	těžce
zraněn	zraněn	k2eAgMnSc1d1	zraněn
(	(	kIx(	(
<g/>
v	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
uzdraven	uzdraven	k2eAgMnSc1d1	uzdraven
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
zcela	zcela	k6eAd1	zcela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
pán	pán	k1gMnSc1	pán
Elrond	Elrond	k1gMnSc1	Elrond
poradu	porada	k1gFnSc4	porada
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
Frodo	Frodo	k1gNnSc4	Frodo
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
hobitími	hobití	k1gNnPc7	hobití
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
.	.	kIx.	.
</s>
<s>
Elrond	Elrond	k1gInSc1	Elrond
přítomným	přítomný	k1gMnPc3	přítomný
vypoví	vypovědět	k5eAaPmIp3nS	vypovědět
příběh	příběh	k1gInSc4	příběh
Prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
debatě	debata	k1gFnSc6	debata
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
naložit	naložit	k5eAaPmF	naložit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zamezit	zamezit	k5eAaPmF	zamezit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zničit	zničit	k5eAaPmF	zničit
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Sauronovy	Sauronův	k2eAgFnSc2d1	Sauronova
říše	říš	k1gFnSc2	říš
Mordoru	Mordor	k1gInSc2	Mordor
v	v	k7c6	v
Puklinách	puklina	k1gFnPc6	puklina
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
průrvě	průrvě	k?	průrvě
ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
hory	hora	k1gFnSc2	hora
Orodruiny	Orodruina	k1gFnSc2	Orodruina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nikdo	nikdo	k3yNnSc1	nikdo
nechce	chtít	k5eNaImIp3nS	chtít
dobrovolně	dobrovolně	k6eAd1	dobrovolně
do	do	k7c2	do
tak	tak	k6eAd1	tak
nebezpečného	bezpečný	k2eNgNnSc2d1	nebezpečné
místa	místo	k1gNnSc2	místo
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Frodo	Frodo	k1gNnSc1	Frodo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nabídka	nabídka	k1gFnSc1	nabídka
je	být	k5eAaImIp3nS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
ustanoveni	ustanoven	k2eAgMnPc1d1	ustanoven
společníci	společník	k1gMnPc1	společník
mající	mající	k2eAgMnPc1d1	mající
mu	on	k3xPp3gMnSc3	on
pomáhat	pomáhat	k5eAaImF	pomáhat
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
:	:	kIx,	:
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
hobití	hobitý	k2eAgMnPc1d1	hobitý
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
Aragorn	Aragorn	k1gMnSc1	Aragorn
<g/>
,	,	kIx,	,
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
elf	elf	k1gMnSc1	elf
Legolas	Legolas	k1gMnSc1	Legolas
<g/>
,	,	kIx,	,
trpaslík	trpaslík	k1gMnSc1	trpaslík
Gimli	Gimle	k1gFnSc4	Gimle
a	a	k8xC	a
Boromir	Boromir	k1gMnSc1	Boromir
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
vládnoucího	vládnoucí	k2eAgMnSc2d1	vládnoucí
správce	správce	k1gMnSc2	správce
Gondoru	Gondor	k1gInSc2	Gondor
<g/>
.	.	kIx.	.
</s>
<s>
Družinu	družina	k1gFnSc4	družina
(	(	kIx(	(
<g/>
Společenstvo	společenstvo	k1gNnSc4	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
)	)	kIx)	)
zavede	zavést	k5eAaPmIp3nS	zavést
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
starobylé	starobylý	k2eAgFnSc2d1	starobylá
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
opuštěné	opuštěný	k2eAgFnSc2d1	opuštěná
podzemní	podzemní	k2eAgFnSc2d1	podzemní
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
říše	říš	k1gFnSc2	říš
Morie	Morie	k1gFnSc2	Morie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probudí	probudit	k5eAaPmIp3nS	probudit
démona	démon	k1gMnSc4	démon
z	z	k7c2	z
dávných	dávný	k2eAgInPc2d1	dávný
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
balroga	balroga	k1gFnSc1	balroga
(	(	kIx(	(
<g/>
původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgMnSc4	tento
démona	démon	k1gMnSc4	démon
je	být	k5eAaImIp3nS	být
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
v	v	k7c6	v
Silmarillionu	Silmarillion	k1gInSc6	Silmarillion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
vůdce	vůdce	k1gMnSc1	vůdce
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
balrogem	balrog	k1gInSc7	balrog
zřítí	zřítit	k5eAaPmIp3nP	zřítit
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
<g/>
,	,	kIx,	,
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
družiny	družina	k1gFnSc2	družina
uniknou	uniknout	k5eAaPmIp3nP	uniknout
do	do	k7c2	do
elfské	elfský	k2eAgFnSc2d1	elfská
lesní	lesní	k2eAgFnSc2d1	lesní
říše	říš	k1gFnSc2	říš
Lothlórien	Lothlórina	k1gFnPc2	Lothlórina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vládce	vládce	k1gMnSc1	vládce
Celeborn	Celeborn	k1gMnSc1	Celeborn
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
Galadriel	Galadriela	k1gFnPc2	Galadriela
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
výpravě	výprava	k1gFnSc6	výprava
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
družina	družina	k1gFnSc1	družina
dále	daleko	k6eAd2	daleko
putuje	putovat	k5eAaImIp3nS	putovat
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
řece	řeka	k1gFnSc6	řeka
Anduině	Anduin	k2eAgFnSc6d1	Anduin
divočinou	divočina	k1gFnSc7	divočina
na	na	k7c4	na
jih	jih	k1gInSc4	jih
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
gondorské	gondorský	k2eAgFnSc3d1	gondorská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Rauroským	Rauroský	k2eAgInSc7d1	Rauroský
vodopádem	vodopád	k1gInSc7	vodopád
jsou	být	k5eAaImIp3nP	být
poutníci	poutník	k1gMnPc1	poutník
nuceni	nutit	k5eAaImNgMnP	nutit
opustit	opustit	k5eAaPmF	opustit
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
Boromir	Boromir	k1gMnSc1	Boromir
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
Frodovi	Froda	k1gMnSc3	Froda
sebrat	sebrat	k5eAaPmF	sebrat
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
je	být	k5eAaImIp3nS	být
družina	družina	k1gFnSc1	družina
napadena	napadnout	k5eAaPmNgFnS	napadnout
skřety	skřet	k1gMnPc7	skřet
<g/>
,	,	kIx,	,
plemenem	plemeno	k1gNnSc7	plemeno
ohavných	ohavný	k2eAgMnPc2d1	ohavný
služebníků	služebník	k1gMnPc2	služebník
Saurona	Saurona	k1gFnSc1	Saurona
(	(	kIx(	(
<g/>
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
původu	původ	k1gInSc6	původ
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tito	tento	k3xDgMnPc1	tento
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
čaroděje	čaroděj	k1gMnSc2	čaroděj
Sarumana	Saruman	k1gMnSc2	Saruman
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Frodo	Frodo	k1gNnSc1	Frodo
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
po	po	k7c4	po
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
Boromirem	Boromir	k1gInSc7	Boromir
jít	jít	k5eAaImF	jít
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
se	s	k7c7	s
Samem	Samos	k1gInSc7	Samos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Jeden	jeden	k4xCgInSc1	jeden
prsten	prsten	k1gInSc1	prsten
nepokoušel	pokoušet	k5eNaImAgInS	pokoušet
i	i	k9	i
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
