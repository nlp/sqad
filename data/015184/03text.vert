<s>
Řád	řád	k1gInSc1
klarisek-kapucínek	klarisek-kapucínka	k1gFnPc2
</s>
<s>
Řád	řád	k1gInSc1
klarisek-kapucínek	klarisek-kapucínek	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
OSCCap	OSCCap	k1gMnSc1
<g/>
/	/	kIx~
<g/>
OSClCap	OSClCap	k1gMnSc1
<g/>
,	,	kIx,
lat.	lat.	k?
Ordo	orda	k1gFnSc5
Santae	Santa	k1gInPc1
Clarae	Clarae	k1gFnSc7
Capuccinarum	Capuccinarum	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
římskokatolický	římskokatolický	k2eAgInSc1d1
ženský	ženský	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
větví	větev	k1gFnPc2
řádu	řád	k1gInSc2
františkánů	františkán	k1gMnPc2
a	a	k8xC
řádu	řád	k1gInSc2
klarisek	klariska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeptišky	jeptiška	k1gFnPc1
žijí	žít	k5eAaImIp3nP
podle	podle	k7c2
regulí	regule	k1gFnPc2
sv.	sv.	kA
Kláry	Klára	k1gFnSc2
a	a	k8xC
mají	mít	k5eAaImIp3nP
stejný	stejný	k2eAgInSc4d1
řádový	řádový	k2eAgInSc4d1
oděv	oděv	k1gInSc4
jako	jako	k8xC,k8xS
řád	řád	k1gInSc4
kapucínů	kapucín	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žijí	žít	k5eAaImIp3nP
v	v	k7c6
přísné	přísný	k2eAgFnSc6d1
klauzuře	klauzura	k1gFnSc6
a	a	k8xC
kontemplaci	kontemplace	k1gFnSc6
v	v	k7c6
naprostém	naprostý	k2eAgNnSc6d1
mlčení	mlčení	k1gNnSc6
a	a	k8xC
osobní	osobní	k2eAgFnSc6d1
chudobě	chudoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
oddělil	oddělit	k5eAaPmAgInS
kapucínský	kapucínský	k2eAgInSc1d1
řád	řád	k1gInSc1
od	od	k7c2
františkánského	františkánský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
reformnímu	reformní	k2eAgNnSc3d1
hnutí	hnutí	k1gNnSc3
též	též	k9
u	u	k7c2
řádu	řád	k1gInSc2
klarisek	klariska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1538	#num#	k4
založila	založit	k5eAaPmAgFnS
Neapoli	Neapol	k1gFnSc6
Maria	Mario	k1gMnSc2
Lorenza	Lorenza	k?
Longo	Longo	k1gMnSc1
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
-	-	kIx~
<g/>
1542	#num#	k4
<g/>
)	)	kIx)
reformní	reformní	k2eAgInSc1d1
klášter	klášter	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
řídil	řídit	k5eAaImAgInS
původní	původní	k2eAgFnSc7d1
regulí	regule	k1gFnSc7
sv.	sv.	kA
Kláry	Klára	k1gFnSc2
a	a	k8xC
regulí	regule	k1gFnPc2
jako	jako	k8xC,k8xS
řád	řád	k1gInSc1
kapucínů	kapucín	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
dvou	dva	k4xCgNnPc2
století	století	k1gNnPc2
se	se	k3xPyFc4
klarisky-kapucínky	klarisky-kapucínka	k1gFnPc1
rozšířily	rozšířit	k5eAaPmAgFnP
i	i	k9
do	do	k7c2
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
založily	založit	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
klášter	klášter	k1gInSc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
roku	rok	k1gInSc2
1603	#num#	k4
(	(	kIx(
<g/>
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dal	dát	k5eAaPmAgInS
název	název	k1gInSc4
Boulevardu	Boulevard	k1gInSc2
des	des	k1gNnSc2
Capucines	Capucinesa	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
druhý	druhý	k4xOgMnSc1
roku	rok	k1gInSc2
1626	#num#	k4
v	v	k7c6
Marseille	Marseille	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
se	se	k3xPyFc4
od	od	k7c2
řádu	řád	k1gInSc2
oddělila	oddělit	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
větev	větev	k1gFnSc1
–	–	k?
Řád	řád	k1gInSc1
klarisek-kapucínek	klarisek-kapucínka	k1gFnPc2
věčného	věčný	k2eAgNnSc2d1
Zbožňování	zbožňování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Clarisses	Clarisses	k1gMnSc1
capucines	capucines	k1gMnSc1
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Kapuzinerinnen	Kapuzinerinnen	k2eAgInSc4d1
von	von	k1gInSc4
der	drát	k5eAaImRp2nS
Ewigen	Ewigen	k1gInSc1
Anbetung	Anbetung	k1gInSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Františkáni	františkán	k1gMnPc1
Svatí	svatý	k2eAgMnPc1d1
</s>
<s>
Sv.	sv.	kA
František	František	k1gMnSc1
z	z	k7c2
Assisi	Assise	k1gFnSc4
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Klára	Klára	k1gFnSc1
z	z	k7c2
Assisi	Assise	k1gFnSc4
První	první	k4xOgInSc4
řád	řád	k1gInSc4
(	(	kIx(
<g/>
mužský	mužský	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
menších	malý	k2eAgMnPc2d2
bratří	bratr	k1gMnPc2
(	(	kIx(
<g/>
též	též	k6eAd1
bosí	bosý	k2eAgMnPc1d1
františkání	františkání	k1gNnSc2
<g/>
,	,	kIx,
observanté	observanta	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Řád	řád	k1gInSc4
menších	malý	k2eAgMnPc2d2
bratří	bratr	k1gMnPc2
kapucínů	kapucín	k1gMnPc2
•	•	k?
Řád	řád	k1gInSc4
menších	malý	k2eAgMnPc2d2
bratří	bratr	k1gMnPc2
konventuálů	konventuál	k1gMnPc2
(	(	kIx(
<g/>
minorité	minorita	k1gMnPc1
<g/>
)	)	kIx)
Klarisky	klariska	k1gFnPc4
(	(	kIx(
<g/>
ženský	ženský	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Chudé	Chudé	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
svaté	svatý	k2eAgFnSc2d1
Kláry	Klára	k1gFnSc2
•	•	k?
Řád	řád	k1gInSc1
klarisek-kapucínek	klarisek-kapucínek	k1gInSc1
Třetí	třetí	k4xOgInSc1
řád	řád	k1gInSc1
(	(	kIx(
<g/>
terciáři	terciář	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Sekulární	sekulární	k2eAgInSc1d1
františkánský	františkánský	k2eAgInSc1d1
řád	řád	k1gInSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
františkánských	františkánský	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
•	•	k?
Seznam	seznam	k1gInSc1
kapucínských	kapucínský	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
16278573-2	16278573-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
133008048	#num#	k4
</s>
