<s>
Operace	operace	k1gFnSc1
Nachšon	Nachšona	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
מ	מ	k?
נ	נ	k?
<g/>
,	,	kIx,
Mivca	Mivca	k1gMnSc1
Nachšon	Nachšon	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vojenská	vojenský	k2eAgFnSc1d1
akce	akce	k1gFnSc1
provedená	provedený	k2eAgFnSc1d1
v	v	k7c6
dubnu	duben	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
konce	konec	k1gInSc2
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
nad	nad	k7c7
Palestinou	Palestina	k1gFnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
ještě	ještě	k9
před	před	k7c7
vznikem	vznik	k1gInSc7
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
židovskými	židovský	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
Hagana	Hagan	k1gMnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
dočasné	dočasný	k2eAgNnSc1d1
ovládnutí	ovládnutí	k1gNnSc1
přístupového	přístupový	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
z	z	k7c2
pobřežní	pobřežní	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>