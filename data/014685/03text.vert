<s>
Operace	operace	k1gFnSc1
Nachšon	Nachšona	k1gFnPc2
</s>
<s>
Boje	boj	k1gInPc1
o	o	k7c6
vesnici	vesnice	k1gFnSc6
al-Kastal	al-Kastat	k5eAaPmAgMnS,k5eAaImAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
Operace	operace	k1gFnSc1
Nachšon	Nachšon	k1gInSc1
</s>
<s>
Operace	operace	k1gFnSc1
Nachšon	Nachšona	k1gFnPc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
מ	מ	k?
נ	נ	k?
<g/>
,	,	kIx,
Mivca	Mivca	k1gMnSc1
Nachšon	Nachšon	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vojenská	vojenský	k2eAgFnSc1d1
akce	akce	k1gFnSc1
provedená	provedený	k2eAgFnSc1d1
v	v	k7c6
dubnu	duben	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
konce	konec	k1gInSc2
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
nad	nad	k7c7
Palestinou	Palestina	k1gFnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
ještě	ještě	k9
před	před	k7c7
vznikem	vznik	k1gInSc7
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
židovskými	židovský	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
Hagana	Hagan	k1gMnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
dočasné	dočasný	k2eAgNnSc1d1
ovládnutí	ovládnutí	k1gNnSc1
přístupového	přístupový	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
z	z	k7c2
pobřežní	pobřežní	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgFnSc4
židovskou	židovský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
během	během	k7c2
první	první	k4xOgFnSc2
arabsko-izraelské	arabsko-izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
charakter	charakter	k1gInSc4
konvenční	konvenční	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenována	pojmenován	k2eAgMnSc4d1
je	být	k5eAaImIp3nS
podle	podle	k7c2
biblické	biblický	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
Nachšona	Nachšona	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobové	dobový	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1947	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
OSN	OSN	kA
plán	plán	k1gInSc4
na	na	k7c6
rozdělení	rozdělení	k1gNnSc6
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
britský	britský	k2eAgInSc1d1
mandát	mandát	k1gInSc1
nad	nad	k7c7
Palestinou	Palestina	k1gFnSc7
nahrazen	nahradit	k5eAaPmNgInS
dvěma	dva	k4xCgInPc7
samostatnými	samostatný	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
:	:	kIx,
židovským	židovský	k2eAgInSc7d1
a	a	k8xC
arabským	arabský	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
emocí	emoce	k1gFnPc2
předcházejících	předcházející	k2eAgFnPc6d1
konci	konec	k1gInSc3
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
Palestině	Palestina	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
rozpoutala	rozpoutat	k5eAaPmAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Židy	Žid	k1gMnPc7
a	a	k8xC
Araby	Arab	k1gMnPc7
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
v	v	k7c6
měsících	měsíc	k1gInPc6
a	a	k8xC
týdnech	týden	k1gInPc6
před	před	k7c7
koncem	konec	k1gInSc7
mandátu	mandát	k1gInSc2
zostřovala	zostřovat	k5eAaImAgFnS
a	a	k8xC
kromě	kromě	k7c2
izolovaného	izolovaný	k2eAgNnSc2d1
násilí	násilí	k1gNnSc2
a	a	k8xC
teroristických	teroristický	k2eAgInPc2d1
útoků	útok	k1gInPc2
nabývala	nabývat	k5eAaImAgFnS
ráz	ráz	k1gInSc4
konvenčního	konvenční	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
této	tento	k3xDgFnSc2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
i	i	k9
obléhání	obléhání	k1gNnSc1
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
židovská	židovský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
více	hodně	k6eAd2
odříznuta	odříznout	k5eAaPmNgFnS
od	od	k7c2
zásobování	zásobování	k1gNnSc2
z	z	k7c2
pobřežní	pobřežní	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
kvůli	kvůli	k7c3
arabským	arabský	k2eAgInPc3d1
útokům	útok	k1gInPc3
ve	v	k7c6
vesnicích	vesnice	k1gFnPc6
v	v	k7c6
Judských	judský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
strategické	strategický	k2eAgFnSc6d1
soutěsce	soutěska	k1gFnSc6
Ša	Ša	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ar	ar	k1gInSc1
ha-Gaj	ha-Gaj	k1gInSc1
(	(	kIx(
<g/>
Báb	Báb	k1gFnSc1
al-Vád	al-Váda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
soutěskou	soutěska	k1gFnSc7
také	také	k9
vedl	vést	k5eAaImAgMnS
vodovod	vodovod	k1gInSc4
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
operace	operace	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Ben	Ben	k1gInSc4
Gurion	Gurion	k1gInSc1
trval	trvat	k5eAaImAgInS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
mnoha	mnoho	k4c2
členů	člen	k1gInPc2
vojenského	vojenský	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
Hagany	Hagana	k1gFnSc2
na	na	k7c6
silném	silný	k2eAgInSc6d1
a	a	k8xC
koncentrovaném	koncentrovaný	k2eAgInSc6d1
útoku	útok	k1gInSc6
v	v	k7c6
síle	síla	k1gFnSc6
jedné	jeden	k4xCgFnSc2
brigády	brigáda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
prolomil	prolomit	k5eAaPmAgInS
obležení	obležení	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
soustředit	soustředit	k5eAaPmF
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
síly	síla	k1gFnSc2
a	a	k8xC
vybavení	vybavení	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
na	na	k7c4
úkor	úkor	k1gInSc4
jiných	jiný	k2eAgInPc2d1
regionů	region	k1gInPc2
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
operace	operace	k1gFnSc2
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
koridor	koridor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
v	v	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
měl	mít	k5eAaImAgInS
šířku	šířka	k1gFnSc4
cca	cca	kA
6	#num#	k4
mil	míle	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
Judských	judský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
cca	cca	kA
2	#num#	k4
míle	míle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
obsazení	obsazení	k1gNnSc2
strategických	strategický	k2eAgFnPc2d1
návrší	návrš	k1gFnPc2
a	a	k8xC
sousedních	sousední	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
umožněno	umožněn	k2eAgNnSc1d1
zásobování	zásobování	k1gNnSc1
Jeruzaléma	Jeruzalém	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
velkých	velký	k2eAgInPc2d1
ozbrojených	ozbrojený	k2eAgInPc2d1
konvojů	konvoj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
byla	být	k5eAaImAgFnS
zorganizována	zorganizován	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
čítající	čítající	k2eAgFnSc2d1
cca	cca	kA
1500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
rozdělená	rozdělený	k2eAgFnSc1d1
do	do	k7c2
tří	tři	k4xCgInPc2
praporů	prapor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
prapor	prapor	k1gInSc1
měl	mít	k5eAaImAgInS
zajistit	zajistit	k5eAaPmF
území	území	k1gNnSc4
v	v	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
mezi	mezi	k7c7
arabskou	arabský	k2eAgFnSc7d1
vesnicí	vesnice	k1gFnSc7
Khulda	Khuldo	k1gNnSc2
a	a	k8xC
výšinou	výšina	k1gFnSc7
Latrun	Latruna	k1gFnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
prapor	prapor	k1gInSc1
mezi	mezi	k7c7
Latrunem	Latrun	k1gInSc7
a	a	k8xC
židovskou	židovský	k2eAgFnSc7d1
vesnicí	vesnice	k1gFnSc7
Kirjat	Kirjat	k2eAgInSc1d1
Anavim	Anavim	k1gInSc1
nedaleko	nedaleko	k7c2
západního	západní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc4
prapor	prapor	k1gInSc4
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
připraven	připravit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
záloha	záloha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedení	vedení	k1gNnSc1
akce	akce	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Šim	Šim	k1gMnSc1
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
Avidan	Avidan	k1gMnSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
Brigády	brigáda	k1gFnSc2
Giv	Giv	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ati	ati	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
před	před	k7c7
spuštěním	spuštění	k1gNnSc7
operace	operace	k1gFnSc2
se	se	k3xPyFc4
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
podařilo	podařit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
první	první	k4xOgFnSc4
zásilku	zásilka	k1gFnSc4
zbraní	zbraň	k1gFnPc2
z	z	k7c2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
Operace	operace	k1gFnSc1
Chasida	chasid	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
pušky	puška	k1gFnPc4
a	a	k8xC
lehké	lehký	k2eAgInPc4d1
kulomety	kulomet	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Startu	start	k1gInSc2
operace	operace	k1gFnPc1
předcházely	předcházet	k5eAaImAgFnP
některé	některý	k3yIgFnPc4
drobnější	drobný	k2eAgFnPc4d2
akce	akce	k1gFnPc4
jako	jako	k8xC,k8xS
útok	útok	k1gInSc4
na	na	k7c4
velitelské	velitelský	k2eAgNnSc4d1
stanoviště	stanoviště	k1gNnSc4
Arabů	Arab	k1gMnPc2
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Ramla	Ramlo	k1gNnSc2
a	a	k8xC
také	také	k9
dobytí	dobytí	k1gNnSc4
arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
al-Kastal	al-Kastat	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
horách	hora	k1gFnPc6
u	u	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
Nachšon	Nachšona	k1gFnPc2
byla	být	k5eAaImAgFnS
spuštěna	spuštěn	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovské	židovský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
obklíčily	obklíčit	k5eAaPmAgFnP
sedm	sedm	k4xCc4
arabských	arabský	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
a	a	k8xC
dobyly	dobýt	k5eAaPmAgFnP
vesnice	vesnice	k1gFnSc1
Khulda	Khulda	k1gFnSc1
a	a	k8xC
Dejr	Dejr	k1gMnSc1
Muchejsin	Muchejsin	k1gMnSc1
v	v	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elitní	elitní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
Palmach	Palmacha	k1gFnPc2
zároveň	zároveň	k6eAd1
obsadily	obsadit	k5eAaPmAgFnP
vesnici	vesnice	k1gFnSc6
Bajt	bajt	k1gInSc1
Machsir	Machsira	k1gFnPc2
poblíž	poblíž	k7c2
soutěsky	soutěska	k1gFnSc2
Ša	Ša	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ar	ar	k1gInSc1
ha-Gaj	ha-Gaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
půlnoci	půlnoc	k1gFnSc6
pak	pak	k6eAd1
z	z	k7c2
židovské	židovský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Chulda	Chulda	k1gMnSc1
vyrazil	vyrazit	k5eAaPmAgMnS
velký	velký	k2eAgInSc4d1
konvoj	konvoj	k1gInSc4
civilních	civilní	k2eAgMnPc2d1
i	i	k8xC
vojenských	vojenský	k2eAgMnPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
díky	díky	k7c3
krytí	krytí	k1gNnSc3
Hagany	Hagana	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
po	po	k7c6
10	#num#	k4
hodinách	hodina	k1gFnPc6
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
přísunu	přísun	k1gInSc3
zásob	zásoba	k1gFnPc2
mohla	moct	k5eAaImAgFnS
židovská	židovská	k1gFnSc1
populace	populace	k1gFnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
vydržet	vydržet	k5eAaPmF
další	další	k2eAgInPc4d1
týdny	týden	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1948	#num#	k4
se	se	k3xPyFc4
Arabové	Arab	k1gMnPc1
pokusili	pokusit	k5eAaPmAgMnP
o	o	k7c4
protiútok	protiútok	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
vesnice	vesnice	k1gFnSc2
al-Kastal	al-Kastat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pak	pak	k6eAd1
odehrávalo	odehrávat	k5eAaImAgNnS
několik	několik	k4yIc4
vln	vlna	k1gFnPc2
vojenských	vojenský	k2eAgFnPc2d1
konfrontací	konfrontace	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
arabský	arabský	k2eAgMnSc1d1
vojenský	vojenský	k2eAgMnSc1d1
předák	předák	k1gMnSc1
Abd	Abd	k1gMnSc1
al-Kadir	al-Kadir	k1gMnSc1
al-Husajni	al-Husajň	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
Nachšon	Nachšona	k1gFnPc2
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
vojenskou	vojenský	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
Hagany	Hagana	k1gFnSc2
s	s	k7c7
rysy	rys	k1gInPc7
konvenční	konvenční	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
standardním	standardní	k2eAgNnSc7d1
operačním	operační	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
útvarů	útvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c6
první	první	k4xOgFnSc6
operaci	operace	k1gFnSc6
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
dobýt	dobýt	k5eAaPmF
arabské	arabský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktický	praktický	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
ale	ale	k9
byl	být	k5eAaImAgInS
dočasný	dočasný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
projely	projet	k5eAaPmAgInP
ještě	ještě	k6eAd1
dva	dva	k4xCgInPc4
další	další	k2eAgInPc4d1
konvoje	konvoj	k1gInPc4
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1948	#num#	k4
byla	být	k5eAaImAgFnS
soutěska	soutěska	k1gFnSc1
Ša	Ša	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ar	ar	k1gInSc4
ha-Gaj	ha-Gaj	k1gFnSc4
opět	opět	k6eAd1
uzavřena	uzavřen	k2eAgFnSc1d1
arabskými	arabský	k2eAgMnPc7d1
ostřelovači	ostřelovač	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
s	s	k7c7
Operací	operace	k1gFnSc7
Nachšon	Nachšona	k1gFnPc2
proběhl	proběhnout	k5eAaPmAgInS
také	také	k9
kontroverzní	kontroverzní	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
arabskou	arabský	k2eAgFnSc4d1
vesnici	vesnice	k1gFnSc4
Dejr	Dejr	k1gMnSc1
Jásin	Jásin	k1gMnSc1
<g/>
,	,	kIx,
takzvaný	takzvaný	k2eAgInSc4d1
masakr	masakr	k1gInSc4
v	v	k7c4
Dejr	Dejr	k1gInSc4
Jásin	Jásina	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
vojenské	vojenský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
Irgunu	Irgun	k1gInSc2
a	a	k8xC
Lechi	Lech	k1gFnSc2
(	(	kIx(
<g/>
nepodléhající	podléhající	k2eNgFnSc2d1
Haganě	Hagaň	k1gFnSc2
<g/>
)	)	kIx)
zabily	zabít	k5eAaPmAgFnP
velké	velká	k1gFnPc1
množství	množství	k1gNnSc2
arabských	arabský	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HERZOG	HERZOG	kA
<g/>
,	,	kIx,
Chaim	Chaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arab-Israeli	Arab-Israel	k1gInSc6
Wars	Wars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Vintage	Vintage	k1gInSc1
Books	Books	k1gInSc1
<g/>
/	/	kIx~
<g/>
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
394	#num#	k4
<g/>
-	-	kIx~
<g/>
71746	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Arab-Israeli	Arab-Israel	k1gInSc6
Wars	Warsa	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Arab-Israeli	Arab-Israel	k1gInSc6
Wars	Warsa	k1gFnPc2
<g/>
.	.	kIx.
s.	s.	k?
291	#num#	k4
2	#num#	k4
Arab-Israeli	Arab-Israel	k1gInSc6
Wars	Warsa	k1gFnPc2
<g/>
.	.	kIx.
s.	s.	k?
30	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
↑	↑	k?
NAOR	NAOR	kA
<g/>
,	,	kIx,
Mordecai	Mordecai	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
in	in	k?
Eretz	Eretz	k1gMnSc1
Israel	Israel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolín	Kolín	k1gInSc1
n.	n.	k?
<g/>
Rýnem	Rýn	k1gInSc7
<g/>
:	:	kIx,
Könemann	Könemann	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
89508	#num#	k4
<g/>
-	-	kIx~
<g/>
595	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
258	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
The	The	k1gFnSc1
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
in	in	k?
Eretz	Eretz	k1gMnSc1
Israel	Israel	k1gMnSc1
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
in	in	k?
Eretz	Eretz	k1gMnSc1
Israel	Israel	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
258	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Latrun	Latrun	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Operace	operace	k1gFnSc1
izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
před	před	k7c7
vznikem	vznik	k1gInSc7
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
</s>
<s>
Šmu	Šmu	k?
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
·	·	k?
Hašmed	Hašmed	k1gInSc1
·	·	k?
Chasida	chasid	k1gMnSc2
·	·	k?
Nachšon	Nachšon	k1gMnSc1
·	·	k?
Har	Har	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
·	·	k?
Jevusi	Jevuse	k1gFnSc3
·	·	k?
Plán	plán	k1gInSc1
Dalet	Dalet	k1gMnSc1
·	·	k?
Chamec	Chamec	k1gMnSc1
·	·	k?
Jiftach	Jiftach	k1gMnSc1
·	·	k?
Matate	Matat	k1gInSc5
·	·	k?
Makabi	Makab	k1gMnSc3
·	·	k?
Kitur	Kitur	k1gMnSc1
·	·	k?
Jovel	Jovel	k1gMnSc1
·	·	k?
Barak	Barak	k1gMnSc1
·	·	k?
Gide	Gide	k1gInSc1
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
·	·	k?
Ben	Ben	k1gInSc1
Ami	Ami	k1gFnSc1
·	·	k?
Medina	Medina	k1gFnSc1
po	po	k7c6
vzniku	vznik	k1gInSc6
státu	stát	k1gInSc2
Izrael	Izrael	k1gMnSc1
</s>
<s>
Kilšon	Kilšon	k1gInSc1
·	·	k?
Tinok	Tinok	k1gInSc1
·	·	k?
Balak	Balak	k1gInSc1
·	·	k?
Namal	Namal	k1gInSc1
·	·	k?
Bitvy	bitva	k1gFnSc2
o	o	k7c4
Latrun	Latrun	k1gNnSc4
·	·	k?
Bin	Bin	k1gMnSc1
Nun	Nun	k1gMnSc1
·	·	k?
Bin	Bin	k1gMnSc1
Nun	Nun	k1gMnSc1
Bet	Bet	k1gMnSc1
·	·	k?
Erez	Erez	k1gMnSc1
·	·	k?
Plešet	Plešet	k1gMnSc1
·	·	k?
Jicchak	Jicchak	k1gMnSc1
·	·	k?
Joram	Joram	k1gInSc1
·	·	k?
Bitvy	bitva	k1gFnPc1
deseti	deset	k4xCc2
dnů	den	k1gInPc2
·	·	k?
An-Far	An-Far	k1gInSc1
·	·	k?
Broš	Broš	k1gInSc1
·	·	k?
Dekel	Dekel	k1gInSc1
·	·	k?
Danny	Danna	k1gFnSc2
·	·	k?
Betek	Betek	k1gInSc1
·	·	k?
Kedem	Kedem	k1gInSc1
·	·	k?
Mavet	Mavet	k1gInSc1
la-poleš	la-polat	k5eAaBmIp2nS,k5eAaPmIp2nS
·	·	k?
Šoter	Šoter	k1gInSc1
·	·	k?
GJS	GJS	kA
·	·	k?
Avak	Avak	k1gInSc1
·	·	k?
Velveta	Velveta	k1gFnSc1
·	·	k?
Jo	jo	k9
<g/>
'	'	kIx"
<g/>
av	av	k?
·	·	k?
Moše	mocha	k1gFnSc3
·	·	k?
Egrof	Egrof	k1gMnSc1
·	·	k?
ha-Har	ha-Har	k1gMnSc1
·	·	k?
Jekev	Jekev	k1gFnSc1
·	·	k?
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
·	·	k?
Chiram	Chiram	k1gInSc1
·	·	k?
Lot	Lot	k1gMnSc1
·	·	k?
Hatchala	Hatchala	k1gMnSc1
·	·	k?
Asaf	Asaf	k1gMnSc1
·	·	k?
Chorev	Chorev	k1gFnSc1
·	·	k?
Chisul	Chisula	k1gFnPc2
·	·	k?
Oz	Oz	k1gMnSc1
·	·	k?
Jicuv	Jicuv	k1gMnSc1
·	·	k?
Uvda	Uvda	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
