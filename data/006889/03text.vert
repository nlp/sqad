<s>
Klášter	klášter	k1gInSc1	klášter
boromejek	boromejka	k1gFnPc2	boromejka
v	v	k7c6	v
Zákupech	zákup	k1gInPc6	zákup
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
Dům	dům	k1gInSc4	dům
řeholních	řeholní	k2eAgFnPc2d1	řeholní
sester	sestra	k1gFnPc2	sestra
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Borromejského	Borromejský	k2eAgMnSc2d1	Borromejský
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
)	)	kIx)	)
městečka	městečko	k1gNnSc2	městečko
Zákupy	zákup	k1gInPc1	zákup
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
školou	škola	k1gFnSc7	škola
i	i	k9	i
výchovným	výchovný	k2eAgInSc7d1	výchovný
ústavem	ústav	k1gInSc7	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
umožnil	umožnit	k5eAaPmAgMnS	umožnit
excísař	excísař	k1gMnSc1	excísař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
V.	V.	kA	V.
svou	svůj	k3xOyFgFnSc7	svůj
nadací	nadace	k1gFnSc7	nadace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc1	objekt
používán	používat	k5eAaImNgInS	používat
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xC	jako
pošta	pošta	k1gFnSc1	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
V.	V.	kA	V.
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
využívali	využívat	k5eAaPmAgMnP	využívat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Zákupech	zákup	k1gInPc6	zákup
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
svých	svůj	k3xOyFgNnPc2	svůj
letních	letní	k2eAgNnPc2d1	letní
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Městečku	městečko	k1gNnSc6	městečko
několikrát	několikrát	k6eAd1	několikrát
pomohli	pomoct	k5eAaPmAgMnP	pomoct
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rozvoji	rozvoj	k1gInSc6	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
peněz	peníze	k1gInPc2	peníze
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
řeholních	řeholní	k2eAgFnPc2d1	řeholní
sester	sestra	k1gFnPc2	sestra
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Borromejského	Borromejský	k2eAgMnSc2d1	Borromejský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dvoutřídní	dvoutřídní	k2eAgFnSc1d1	dvoutřídní
obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Boromejky	boromejka	k1gFnPc1	boromejka
vyučovaly	vyučovat	k5eAaImAgFnP	vyučovat
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
dívčím	dívčí	k2eAgInSc7d1	dívčí
ústavem	ústav	k1gInSc7	ústav
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byl	být	k5eAaImAgInS	být
penzionát	penzionát	k1gInSc1	penzionát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
školy	škola	k1gFnSc2	škola
přešly	přejít	k5eAaPmAgFnP	přejít
dívky	dívka	k1gFnPc1	dívka
ze	z	k7c2	z
smíšené	smíšený	k2eAgFnSc2d1	smíšená
zákupské	zákupský	k2eAgFnSc2d1	zákupská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Dospívající	dospívající	k2eAgFnPc1d1	dospívající
dívky	dívka	k1gFnPc1	dívka
dívčího	dívčí	k2eAgInSc2d1	dívčí
ústavu	ústav	k1gInSc2	ústav
zpravidla	zpravidla	k6eAd1	zpravidla
bydlely	bydlet	k5eAaImAgFnP	bydlet
v	v	k7c6	v
zákupských	zákupský	k2eAgFnPc6d1	zákupská
rodinách	rodina	k1gFnPc6	rodina
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
i	i	k9	i
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
vyučování	vyučování	k1gNnSc4	vyučování
docházely	docházet	k5eAaImAgInP	docházet
či	či	k8xC	či
dojížděly	dojíždět	k5eAaImAgInP	dojíždět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohlednicích	pohlednice	k1gFnPc6	pohlednice
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1900	[number]	k4	1900
-	-	kIx~	-
1925	[number]	k4	1925
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
nazýván	nazýván	k2eAgInSc4d1	nazýván
Nonnenkloster	Nonnenkloster	k1gInSc4	Nonnenkloster
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
klášter	klášter	k1gInSc4	klášter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Klosterschule	Klosterschule	k1gFnSc1	Klosterschule
(	(	kIx(	(
<g/>
klášterní	klášterní	k2eAgFnSc1d1	klášterní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
provozovány	provozován	k2eAgInPc1d1	provozován
páté	pátá	k1gFnPc4	pátá
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
třídy	třída	k1gFnPc4	třída
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
jednotné	jednotný	k2eAgFnSc2d1	jednotná
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
boromejek	boromejka	k1gFnPc2	boromejka
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
osmiletou	osmiletý	k2eAgFnSc7d1	osmiletá
střední	střední	k2eAgFnSc7d1	střední
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
základní	základní	k2eAgFnSc4d1	základní
devítiletou	devítiletý	k2eAgFnSc4d1	devítiletá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
zdejší	zdejší	k2eAgFnPc1d1	zdejší
prostory	prostora	k1gFnPc1	prostora
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
žáky	žák	k1gMnPc7	žák
opuštěny	opuštěn	k2eAgInPc1d1	opuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zákupech	zákup	k1gInPc6	zákup
existoval	existovat	k5eAaImAgInS	existovat
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc1	jeden
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
kapucínů	kapucín	k1gMnPc2	kapucín
<g/>
.	.	kIx.	.
</s>
<s>
Postaven	postaven	k2eAgMnSc1d1	postaven
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1679	[number]	k4	1679
až	až	k9	až
1685	[number]	k4	1685
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zdevastován	zdevastovat	k5eAaPmNgMnS	zdevastovat
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
v	v	k7c6	v
dezolátním	dezolátní	k2eAgInSc6d1	dezolátní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
dvoupatrovou	dvoupatrový	k2eAgFnSc4d1	dvoupatrová
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
slohy	sloha	k1gFnSc2	sloha
gotické	gotický	k2eAgFnPc4d1	gotická
s	s	k7c7	s
pozdně	pozdně	k6eAd1	pozdně
klasicistními	klasicistní	k2eAgFnPc7d1	klasicistní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
sanktusová	sanktusový	k2eAgFnSc1d1	sanktusová
vížka	vížka	k1gFnSc1	vížka
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
