<s>
Postaven	postaven	k2eAgMnSc1d1	postaven
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1679	[number]	k4	1679
až	až	k9	až
1685	[number]	k4	1685
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zdevastován	zdevastovat	k5eAaPmNgMnS	zdevastovat
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
v	v	k7c6	v
dezolátním	dezolátní	k2eAgInSc6d1	dezolátní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
