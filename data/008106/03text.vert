<s>
Vasektomie	vasektomie	k1gFnSc1	vasektomie
(	(	kIx(	(
<g/>
také	také	k9	také
vazektomie	vazektomie	k1gFnSc1	vazektomie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zákrok	zákrok	k1gInSc1	zákrok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
chámovodů	chámovod	k1gInPc2	chámovod
muže	muž	k1gMnPc4	muž
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jeho	jeho	k3xOp3gFnSc2	jeho
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
mužské	mužský	k2eAgFnSc2d1	mužská
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Vazektomie	Vazektomie	k1gFnSc1	Vazektomie
je	být	k5eAaImIp3nS	být
nejspolehlivější	spolehlivý	k2eAgFnSc7d3	nejspolehlivější
metodou	metoda	k1gFnSc7	metoda
mužské	mužský	k2eAgFnSc2d1	mužská
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
malá	malý	k2eAgFnSc1d1	malá
invazivita	invazivita	k1gFnSc1	invazivita
a	a	k8xC	a
nízké	nízký	k2eAgNnSc1d1	nízké
riziko	riziko	k1gNnSc1	riziko
komplikací	komplikace	k1gFnPc2	komplikace
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
spolehlivosti	spolehlivost	k1gFnSc6	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
upravila	upravit	k5eAaPmAgFnS	upravit
právní	právní	k2eAgFnSc1d1	právní
legislativa	legislativa	k1gFnSc1	legislativa
omezující	omezující	k2eAgNnSc1d1	omezující
využití	využití	k1gNnSc1	využití
vazektomie	vazektomie	k1gFnSc2	vazektomie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
změně	změna	k1gFnSc3	změna
se	se	k3xPyFc4	se
vazektomie	vazektomie	k1gFnSc1	vazektomie
stala	stát	k5eAaPmAgFnS	stát
běžně	běžně	k6eAd1	běžně
dostupnou	dostupný	k2eAgFnSc7d1	dostupná
metodou	metoda	k1gFnSc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
vazektomie	vazektomie	k1gFnSc2	vazektomie
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
erekce	erekce	k1gFnSc2	erekce
a	a	k8xC	a
orgasmu	orgasmus	k1gInSc2	orgasmus
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
ejakulátu	ejakulát	k1gInSc2	ejakulát
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
jen	jen	k9	jen
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zákrok	zákrok	k1gInSc4	zákrok
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
v	v	k7c6	v
lokální	lokální	k2eAgFnSc6d1	lokální
anestezii	anestezie	k1gFnSc6	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákroku	zákrok	k1gInSc6	zákrok
nebývá	bývat	k5eNaImIp3nS	bývat
nutná	nutný	k2eAgFnSc1d1	nutná
hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
ani	ani	k8xC	ani
pracovní	pracovní	k2eAgFnSc1d1	pracovní
neschopnost	neschopnost	k1gFnSc1	neschopnost
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
komplikací	komplikace	k1gFnPc2	komplikace
u	u	k7c2	u
bezskalpelové	bezskalpelový	k2eAgFnSc2d1	bezskalpelový
techniky	technika	k1gFnSc2	technika
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
oproti	oproti	k7c3	oproti
klasické	klasický	k2eAgFnSc3d1	klasická
technice	technika	k1gFnSc3	technika
vazektomie	vazektomie	k1gFnSc2	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Vazektomie	Vazektomie	k1gFnSc1	Vazektomie
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsán	k2eAgFnSc1d1	popsána
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
sirem	sir	k1gMnSc7	sir
Ashley	Ashle	k2eAgFnPc1d1	Ashle
Cooperem	Coopero	k1gNnSc7	Coopero
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prováděl	provádět	k5eAaImAgMnS	provádět
experimenty	experiment	k1gInPc4	experiment
na	na	k7c6	na
vazektomovaných	vazektomovaný	k2eAgMnPc6d1	vazektomovaný
psech	pes	k1gMnPc6	pes
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
R.	R.	kA	R.
Harrison	Harrison	k1gMnSc1	Harrison
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgFnSc4	první
mužskou	mužský	k2eAgFnSc4d1	mužská
vazektomii	vazektomie	k1gFnSc4	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc1	operace
nebyla	být	k5eNaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
atrofie	atrofie	k1gFnSc2	atrofie
prostaty	prostata	k1gFnSc2	prostata
<g/>
.	.	kIx.	.
</s>
<s>
Vazektomie	Vazektomie	k1gFnSc1	Vazektomie
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
program	program	k1gInSc1	program
vazektomií	vazektomie	k1gFnPc2	vazektomie
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Bezskalpelová	Bezskalpelový	k2eAgFnSc1d1	Bezskalpelový
vazektomie	vazektomie	k1gFnSc1	vazektomie
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
ve	v	k7c6	v
Vědecko-výzkumném	vědeckoýzkumný	k2eAgInSc6d1	vědecko-výzkumný
institutu	institut	k1gInSc6	institut
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
rodičovství	rodičovství	k1gNnSc2	rodičovství
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Sečuán	Sečuána	k1gFnPc2	Sečuána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
technika	technika	k1gFnSc1	technika
bezskalpelové	bezskalpelová	k1gFnSc2	bezskalpelová
vazektomie	vazektomie	k1gFnSc1	vazektomie
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
týmu	tým	k1gInSc3	tým
expertů	expert	k1gMnPc2	expert
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
následně	následně	k6eAd1	následně
nabyli	nabýt	k5eAaPmAgMnP	nabýt
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
standardem	standard	k1gInSc7	standard
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
vazektomii	vazektomie	k1gFnSc3	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
Američanů	Američan	k1gMnPc2	Američan
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
vazektomii	vazektomie	k1gFnSc4	vazektomie
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
cca	cca	kA	cca
11	[number]	k4	11
%	%	kIx~	%
manželských	manželský	k2eAgInPc2d1	manželský
párů	pár	k1gInPc2	pár
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
využívá	využívat	k5eAaPmIp3nS	využívat
vazektomii	vazektomie	k1gFnSc4	vazektomie
cca	cca	kA	cca
18	[number]	k4	18
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
bezskalpelové	bezskalpelový	k2eAgFnSc2d1	bezskalpelový
vazektomie	vazektomie	k1gFnSc2	vazektomie
<g/>
,	,	kIx,	,
podstoupilo	podstoupit	k5eAaPmAgNnS	podstoupit
tento	tento	k3xDgInSc4	tento
výkon	výkon	k1gInSc4	výkon
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byla	být	k5eAaImAgFnS	být
vazektomie	vazektomie	k1gFnSc1	vazektomie
donedávna	donedávna	k6eAd1	donedávna
regulována	regulovat	k5eAaImNgFnS	regulovat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
mohl	moct	k5eAaImAgMnS	moct
podstoupit	podstoupit	k5eAaPmF	podstoupit
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sterilizace	sterilizace	k1gFnSc2	sterilizace
pouze	pouze	k6eAd1	pouze
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
sterilizační	sterilizační	k2eAgFnSc7d1	sterilizační
komisí	komise	k1gFnSc7	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
sterilizace	sterilizace	k1gFnSc1	sterilizace
muže	muž	k1gMnSc2	muž
upravena	upravit	k5eAaPmNgFnS	upravit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
373	[number]	k4	373
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
specifických	specifický	k2eAgFnPc6d1	specifická
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
službách	služba	k1gFnPc6	služba
ze	z	k7c2	z
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Sterilizaci	sterilizace	k1gFnSc4	sterilizace
z	z	k7c2	z
nezdravotních	zdravotní	k2eNgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
pacientovi	pacient	k1gMnSc3	pacient
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dovrší	dovršit	k5eAaPmIp3nS	dovršit
věk	věk	k1gInSc4	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
písemné	písemný	k2eAgFnSc2d1	písemná
žádosti	žádost	k1gFnSc2	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kompletním	kompletní	k2eAgNnSc6d1	kompletní
poučení	poučení	k1gNnSc6	poučení
před	před	k7c7	před
svědkem	svědek	k1gMnSc7	svědek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pacientovi	pacient	k1gMnSc3	pacient
dána	dát	k5eAaPmNgNnP	dát
alespoň	alespoň	k9	alespoň
14	[number]	k4	14
<g/>
denní	denní	k2eAgFnSc1d1	denní
lhůta	lhůta	k1gFnSc1	lhůta
do	do	k7c2	do
udělení	udělení	k1gNnSc2	udělení
písemného	písemný	k2eAgInSc2d1	písemný
souhlasu	souhlas	k1gInSc2	souhlas
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
objektivnímu	objektivní	k2eAgNnSc3d1	objektivní
zhodnocení	zhodnocení	k1gNnSc3	zhodnocení
a	a	k8xC	a
porovnání	porovnání	k1gNnSc3	porovnání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
Pearlův	Pearlův	k2eAgInSc1d1	Pearlův
index	index	k1gInSc1	index
Výsledky	výsledek	k1gInPc1	výsledek
Pearlova	Pearlův	k2eAgInSc2d1	Pearlův
indexu	index	k1gInSc2	index
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
striktním	striktní	k2eAgNnSc7d1	striktní
a	a	k8xC	a
volným	volný	k2eAgNnSc7d1	volné
dodržováním	dodržování	k1gNnSc7	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
daného	daný	k2eAgInSc2d1	daný
systému	systém	k1gInSc2	systém
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
různá	různý	k2eAgFnSc1d1	různá
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
užívání	užívání	k1gNnSc6	užívání
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
striktním	striktní	k2eAgNnSc7d1	striktní
dodržováním	dodržování	k1gNnSc7	dodržování
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
typické	typický	k2eAgNnSc1d1	typické
užívání	užívání	k1gNnSc1	užívání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
chyby	chyba	k1gFnPc4	chyba
lidského	lidský	k2eAgInSc2d1	lidský
faktoru	faktor	k1gInSc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
typického	typický	k2eAgNnSc2d1	typické
užívání	užívání	k1gNnSc2	užívání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
vychází	vycházet	k5eAaImIp3nS	vycházet
vazektomie	vazektomie	k1gFnSc1	vazektomie
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejspolehlivějších	spolehlivý	k2eAgFnPc2d3	nejspolehlivější
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
vazektomie	vazektomie	k1gFnSc2	vazektomie
v	v	k7c6	v
paletě	paleta	k1gFnSc6	paleta
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
metod	metoda	k1gFnPc2	metoda
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
sterilizačními	sterilizační	k2eAgFnPc7d1	sterilizační
technikami	technika	k1gFnPc7	technika
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
u	u	k7c2	u
vazektomie	vazektomie	k1gFnSc2	vazektomie
prakticky	prakticky	k6eAd1	prakticky
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
závažné	závažný	k2eAgFnPc1d1	závažná
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
rizika	riziko	k1gNnPc1	riziko
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
sterilizace	sterilizace	k1gFnSc2	sterilizace
žen	žena	k1gFnPc2	žena
dány	dán	k2eAgMnPc4d1	dán
nitrobřišním	nitrobřišní	k2eAgInSc7d1	nitrobřišní
přístupem	přístup	k1gInSc7	přístup
a	a	k8xC	a
rozsahem	rozsah	k1gInSc7	rozsah
anestezie	anestezie	k1gFnSc2	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
sterilizace	sterilizace	k1gFnSc1	sterilizace
obvykle	obvykle	k6eAd1	obvykle
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
denní	denní	k2eAgFnSc4d1	denní
hospitalizaci	hospitalizace	k1gFnSc4	hospitalizace
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
týdenní	týdenní	k2eAgInSc4d1	týdenní
klidový	klidový	k2eAgInSc4d1	klidový
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
nitroděložního	nitroděložní	k2eAgNnSc2d1	nitroděložní
těhotenstní	těhotenstnit	k5eAaPmIp3nS	těhotenstnit
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
těhotenství	těhotenství	k1gNnSc3	těhotenství
mimoděložní	mimoděložní	k2eAgInSc4d1	mimoděložní
<g/>
.	.	kIx.	.
</s>
<s>
Vazektomie	Vazektomie	k1gFnSc1	Vazektomie
je	být	k5eAaImIp3nS	být
uskutečnitelná	uskutečnitelný	k2eAgFnSc1d1	uskutečnitelná
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
znecitlivěni	znecitlivěn	k2eAgMnPc1d1	znecitlivěn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
0,2	[number]	k4	0,2
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
hematomu	hematom	k1gInSc2	hematom
v	v	k7c4	v
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
%	%	kIx~	%
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
v	v	k7c6	v
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
,	,	kIx,	,
granulomu	granulom	k1gInSc2	granulom
v	v	k7c4	v
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bezskalpelová	Bezskalpelový	k2eAgFnSc1d1	Bezskalpelový
vazektomie	vazektomie	k1gFnSc1	vazektomie
je	být	k5eAaImIp3nS	být
standardem	standard	k1gInSc7	standard
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
aplikaci	aplikace	k1gFnSc6	aplikace
lokálního	lokální	k2eAgNnSc2d1	lokální
anestetika	anestetikum	k1gNnSc2	anestetikum
se	se	k3xPyFc4	se
transkutánně	transkutánně	k6eAd1	transkutánně
fixuje	fixovat	k5eAaImIp3nS	fixovat
chámovod	chámovod	k1gInSc1	chámovod
speciální	speciální	k2eAgInSc1d1	speciální
prstencovou	prstencový	k2eAgFnSc7d1	prstencová
svorkou	svorka	k1gFnSc7	svorka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propíchnutí	propíchnutí	k1gNnSc6	propíchnutí
kůže	kůže	k1gFnSc2	kůže
speciálním	speciální	k2eAgInSc7d1	speciální
nástrojem	nástroj	k1gInSc7	nástroj
–	–	k?	–
disektorem	disektor	k1gInSc7	disektor
–	–	k?	–
se	se	k3xPyFc4	se
chámovod	chámovod	k1gInSc1	chámovod
izoluje	izolovat	k5eAaBmIp3nS	izolovat
<g/>
,	,	kIx,	,
podváže	podvázat	k5eAaPmIp3nS	podvázat
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Konce	konec	k1gInPc1	konec
se	se	k3xPyFc4	se
zataví	zatavit	k5eAaPmIp3nP	zatavit
elektrokoagulací	elektrokoagulace	k1gFnSc7	elektrokoagulace
<g/>
.	.	kIx.	.
</s>
<s>
Rána	Rána	k1gFnSc1	Rána
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
do	do	k7c2	do
5	[number]	k4	5
mm	mm	kA	mm
a	a	k8xC	a
nešije	šít	k5eNaImIp3nS	šít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
výkonu	výkon	k1gInSc2	výkon
obvykle	obvykle	k6eAd1	obvykle
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hospitalizace	hospitalizace	k1gFnSc1	hospitalizace
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
přináší	přinášet	k5eAaImIp3nS	přinášet
méně	málo	k6eAd2	málo
komplikací	komplikace	k1gFnSc7	komplikace
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
perioperační	perioperační	k2eAgFnPc1d1	perioperační
a	a	k8xC	a
pooperační	pooperační	k2eAgFnPc1d1	pooperační
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
časné	časný	k2eAgNnSc1d1	časné
zotavení	zotavení	k1gNnSc1	zotavení
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnSc4d2	kratší
operační	operační	k2eAgFnSc4d1	operační
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
efektivní	efektivní	k2eAgFnSc1d1	efektivní
jako	jako	k8xS	jako
vazektomie	vazektomie	k1gFnSc1	vazektomie
ze	z	k7c2	z
standardního	standardní	k2eAgInSc2d1	standardní
řezu	řez	k1gInSc2	řez
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
vazektomie	vazektomie	k1gFnSc1	vazektomie
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
výkonu	výkon	k1gInSc6	výkon
sterilitu	sterilita	k1gFnSc4	sterilita
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
funkčních	funkční	k2eAgFnPc2d1	funkční
spermií	spermie	k1gFnPc2	spermie
schopných	schopný	k2eAgNnPc2d1	schopné
oplodnění	oplodnění	k1gNnPc2	oplodnění
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
ejakulátu	ejakulát	k1gInSc6	ejakulát
ještě	ještě	k9	ještě
po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
týdnů	týden	k1gInPc2	týden
od	od	k7c2	od
výkonů	výkon	k1gInPc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
ejakulátu	ejakulát	k1gInSc2	ejakulát
prostého	prostý	k2eAgInSc2d1	prostý
spermií	spermie	k1gFnSc7	spermie
je	on	k3xPp3gNnPc4	on
obvykle	obvykle	k6eAd1	obvykle
třeba	třeba	k6eAd1	třeba
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
"	"	kIx"	"
<g/>
očistných	očistný	k2eAgFnPc2d1	očistná
<g/>
"	"	kIx"	"
ejakulací	ejakulace	k1gFnPc2	ejakulace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
proveden	proveden	k2eAgInSc1d1	proveden
spermiogram	spermiogram	k1gInSc1	spermiogram
–	–	k?	–
vyšetření	vyšetření	k1gNnSc1	vyšetření
ejakulátu	ejakulát	k1gInSc2	ejakulát
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
obecných	obecný	k2eAgNnPc2d1	obecné
ustanovení	ustanovení	k1gNnPc2	ustanovení
můžeme	moct	k5eAaImIp1nP	moct
ejakulát	ejakulát	k1gInSc4	ejakulát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc4d1	čistý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
spermie	spermie	k1gFnPc4	spermie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
u	u	k7c2	u
80	[number]	k4	80
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
mužů	muž	k1gMnPc2	muž
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
ještě	ještě	k9	ještě
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
nepohyblivých	pohyblivý	k2eNgFnPc2d1	nepohyblivá
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
ejakulát	ejakulát	k1gInSc1	ejakulát
za	za	k7c4	za
čistý	čistý	k2eAgInSc4d1	čistý
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
mililitru	mililitr	k1gInSc6	mililitr
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
nepohyblivých	pohyblivý	k2eNgFnPc2d1	nepohyblivá
spermií	spermie	k1gFnPc2	spermie
po	po	k7c6	po
3	[number]	k4	3
měsících	měsíc	k1gInPc6	měsíc
od	od	k7c2	od
vazektomie	vazektomie	k1gFnSc2	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Pakliže	pakliže	k8xS	pakliže
po	po	k7c6	po
6	[number]	k4	6
měsících	měsíc	k1gInPc6	měsíc
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
ejakulátu	ejakulát	k1gInSc6	ejakulát
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
spermie	spermie	k1gFnSc2	spermie
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
provedena	proveden	k2eAgFnSc1d1	provedena
vazektomie	vazektomie	k1gFnSc1	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Sterilita	sterilita	k1gFnSc1	sterilita
po	po	k7c6	po
vazektomii	vazektomie	k1gFnSc6	vazektomie
není	být	k5eNaImIp3nS	být
nevratná	vratný	k2eNgFnSc1d1	nevratná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Současné	současný	k2eAgFnPc1d1	současná
mikrochirurgické	mikrochirurgický	k2eAgFnPc1d1	mikrochirurgická
metody	metoda	k1gFnPc1	metoda
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
obnovení	obnovení	k1gNnSc4	obnovení
průchodnosti	průchodnost	k1gFnSc2	průchodnost
chámovodu	chámovod	k1gInSc2	chámovod
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
procentu	procent	k1gInSc6	procent
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
těchto	tento	k3xDgFnPc2	tento
technik	technika	k1gFnPc2	technika
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
době	doba	k1gFnSc6	doba
od	od	k7c2	od
vazektomie	vazektomie	k1gFnSc2	vazektomie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
metodami	metoda	k1gFnPc7	metoda
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
spermií	spermie	k1gFnPc2	spermie
po	po	k7c6	po
vazektomii	vazektomie	k1gFnSc6	vazektomie
jsou	být	k5eAaImIp3nP	být
přímé	přímý	k2eAgInPc1d1	přímý
odběry	odběr	k1gInPc1	odběr
tekutiny	tekutina	k1gFnSc2	tekutina
z	z	k7c2	z
tubulů	tubulus	k1gInPc2	tubulus
nadvarlete	nadvarle	k1gNnSc2	nadvarle
MESA	MESA	kA	MESA
(	(	kIx(	(
<g/>
Microsurgical	Microsurgical	k1gFnSc1	Microsurgical
Epididymal	Epididymal	k1gInSc4	Epididymal
Sperm	Sperm	k1gInSc1	Sperm
Aspiration	Aspiration	k1gInSc1	Aspiration
<g/>
)	)	kIx)	)
či	či	k8xC	či
tkáně	tkáň	k1gFnSc2	tkáň
varlat	varle	k1gNnPc2	varle
s	s	k7c7	s
pohlavními	pohlavní	k2eAgFnPc7d1	pohlavní
buňkami	buňka	k1gFnPc7	buňka
TESE	tes	k1gInSc5	tes
(	(	kIx(	(
<g/>
Testicular	Testicular	k1gInSc1	Testicular
Sperm	Sperm	k1gInSc1	Sperm
Extraction	Extraction	k1gInSc1	Extraction
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
provedení	provedení	k1gNnSc6	provedení
vazektomie	vazektomie	k1gFnSc2	vazektomie
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
není	být	k5eNaImIp3nS	být
hrazené	hrazený	k2eAgNnSc1d1	hrazené
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Úhrada	úhrada	k1gFnSc1	úhrada
pojišťovnou	pojišťovna	k1gFnSc7	pojišťovna
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
jen	jen	k9	jen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
revizním	revizní	k2eAgMnSc7d1	revizní
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
vazektomie	vazektomie	k1gFnSc2	vazektomie
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
do	do	k7c2	do
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
dle	dle	k7c2	dle
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
výkon	výkon	k1gInSc1	výkon
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
lokální	lokální	k2eAgFnSc6d1	lokální
anestezii	anestezie	k1gFnSc6	anestezie
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
hospitalizace	hospitalizace	k1gFnSc2	hospitalizace
<g/>
.	.	kIx.	.
</s>
