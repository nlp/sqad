<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Janků	Janků	k1gMnSc1	Janků
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
profesionální	profesionální	k2eAgMnSc1d1	profesionální
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
útočníka	útočník	k1gMnSc2	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
drobná	drobný	k2eAgFnSc1d1	drobná
postava	postava	k1gFnSc1	postava
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hrát	hrát	k5eAaImF	hrát
především	především	k9	především
technický	technický	k2eAgInSc4d1	technický
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezonách	sezona	k1gFnPc6	sezona
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
trenéra	trenér	k1gMnSc2	trenér
u	u	k7c2	u
týmu	tým	k1gInSc2	tým
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc7	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
byl	být	k5eAaImAgMnS	být
však	však	k9	však
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tým	tým	k1gInSc1	tým
skončil	skončit	k5eAaPmAgInS	skončit
celkově	celkově	k6eAd1	celkově
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
překvapivě	překvapivě	k6eAd1	překvapivě
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
procestoval	procestovat	k5eAaPmAgMnS	procestovat
mnoho	mnoho	k4c4	mnoho
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c4	v
nich	on	k3xPp3gNnPc2	on
několik	několik	k4yIc4	několik
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
následně	následně	k6eAd1	následně
šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
Třince	Třinec	k1gInSc2	Třinec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
7	[number]	k4	7
odehraných	odehraný	k2eAgFnPc2d1	odehraná
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
7	[number]	k4	7
sezónách	sezóna	k1gFnPc6	sezóna
mu	on	k3xPp3gMnSc3	on
Třinec	Třinec	k1gInSc1	Třinec
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezapadá	zapadat	k5eNaPmIp3nS	zapadat
do	do	k7c2	do
koncepce	koncepce	k1gFnSc2	koncepce
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jít	jít	k5eAaImF	jít
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
4	[number]	k4	4
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkusil	zkusit	k5eAaPmAgMnS	zkusit
sezóny	sezóna	k1gFnPc4	sezóna
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
<g/>
:	:	kIx,	:
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
a	a	k8xC	a
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dovršil	dovršit	k5eAaPmAgInS	dovršit
41	[number]	k4	41
let	léto	k1gNnPc2	léto
a	a	k8xC	a
sám	sám	k3xTgInSc4	sám
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
sezónu	sezóna	k1gFnSc4	sezóna
si	se	k3xPyFc3	se
rád	rád	k6eAd1	rád
zahraje	zahrát	k5eAaPmIp3nS	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
extraligovou	extraligový	k2eAgFnSc4d1	extraligová
zkušenost	zkušenost	k1gFnSc4	zkušenost
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
obdiv	obdiv	k1gInSc4	obdiv
především	především	k9	především
díky	díky	k7c3	díky
čichu	čich	k1gInSc2	čich
na	na	k7c4	na
góly	gól	k1gInPc4	gól
(	(	kIx(	(
<g/>
287	[number]	k4	287
<g/>
)	)	kIx)	)
a	a	k8xC	a
parádním	parádní	k2eAgFnPc3d1	parádní
přihrávkám	přihrávka	k1gFnPc3	přihrávka
(	(	kIx(	(
<g/>
255	[number]	k4	255
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
756	[number]	k4	756
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
ho	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
také	také	k9	také
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInPc3	jeho
fair-play	fairla	k2eAgFnPc1d1	fair-pla
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
756	[number]	k4	756
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
extralize	extraliga	k1gFnSc6	extraliga
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
pouhých	pouhý	k2eAgFnPc2d1	pouhá
174	[number]	k4	174
trestných	trestný	k2eAgFnPc2d1	trestná
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
odehrál	odehrát	k5eAaPmAgMnS	odehrát
jen	jen	k9	jen
8	[number]	k4	8
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
vsítil	vsítit	k5eAaPmAgMnS	vsítit
branku	branka	k1gFnSc4	branka
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
hrál	hrát	k5eAaImAgMnS	hrát
poslední	poslední	k2eAgFnSc4d1	poslední
sezónu	sezóna	k1gFnSc4	sezóna
s	s	k7c7	s
Alinčem	Alinč	k1gInSc7	Alinč
a	a	k8xC	a
Pazourkem	pazourek	k1gInSc7	pazourek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
formace	formace	k1gFnSc1	formace
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
věk	věk	k1gInSc4	věk
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
113	[number]	k4	113
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Pazourkem	pazourek	k1gInSc7	pazourek
se	se	k3xPyFc4	se
Janků	Janků	k1gFnSc1	Janků
zná	znát	k5eAaImIp3nS	znát
už	už	k6eAd1	už
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
sehraný	sehraný	k2eAgMnSc1d1	sehraný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
Alinčem	Alinč	k1gInSc7	Alinč
hrál	hrát	k5eAaImAgMnS	hrát
první	první	k4xOgFnSc4	první
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
si	se	k3xPyFc3	se
ale	ale	k9	ale
padli	padnout	k5eAaPmAgMnP	padnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
pomohli	pomoct	k5eAaPmAgMnP	pomoct
Ústí	ústí	k1gNnSc4	ústí
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
místu	místo	k1gNnSc3	místo
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
stříbrnému	stříbrný	k2eAgInSc3d1	stříbrný
úspěchu	úspěch	k1gInSc3	úspěch
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentace	reprezentace	k1gFnSc2	reprezentace
==	==	k?	==
</s>
</p>
<p>
<s>
Statistiky	statistika	k1gFnPc1	statistika
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
1989-90	[number]	k4	1989-90
HC	HC	kA	HC
Dukla	Dukla	k1gFnSc1	Dukla
Trenčín	Trenčín	k1gInSc1	Trenčín
</s>
</p>
<p>
<s>
1991-92	[number]	k4	1991-92
Zetor	zetor	k1gInSc1	zetor
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
1992-93	[number]	k4	1992-93
KP	KP	kA	KP
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993-94	[number]	k4	1993-94
AC	AC	kA	AC
ZPS	ZPS	kA	ZPS
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
1994-95	[number]	k4	1994-95
AC	AC	kA	AC
ZPS	ZPS	kA	ZPS
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
1995-96	[number]	k4	1995-96
AC	AC	kA	AC
ZPS	ZPS	kA	ZPS
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
na	na	k7c6	na
play	play	k0	play
off	off	k?	off
</s>
</p>
<p>
<s>
1996-97	[number]	k4	1996-97
AC	AC	kA	AC
ZPS	ZPS	kA	ZPS
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
1997-98	[number]	k4	1997-98
AC	AC	kA	AC
ZPS	ZPS	kA	ZPS
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
1998-99	[number]	k4	1998-99
HC	HC	kA	HC
Becherovka	becherovka	k1gFnSc1	becherovka
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Vary	Vary	k1gInPc1	Vary
</s>
</p>
<p>
<s>
1999-00	[number]	k4	1999-00
HC	HC	kA	HC
Becherovka	becherovka	k1gFnSc1	becherovka
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2000-01	[number]	k4	2000-01
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2001-02	[number]	k4	2001-02
HC	HC	kA	HC
Oceláři	ocelář	k1gMnSc3	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2002-03	[number]	k4	2002-03
HC	HC	kA	HC
Oceláři	ocelář	k1gMnSc3	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2003-04	[number]	k4	2003-04
HC	HC	kA	HC
Oceláři	ocelář	k1gMnSc6	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2004-05	[number]	k4	2004-05
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc7	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
2005-06	[number]	k4	2005-06
Vsetínská	vsetínský	k2eAgFnSc1d1	vsetínská
hokejová	hokejový	k2eAgFnSc1d1	hokejová
<g/>
,	,	kIx,	,
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
</s>
</p>
<p>
<s>
2006-07	[number]	k4	2006-07
HC	HC	kA	HC
Slovan	Slovan	k1gInSc4	Slovan
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2007-08	[number]	k4	2007-08
HC	HC	kA	HC
Slovan	Slovan	k1gInSc4	Slovan
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008-09	[number]	k4	2008-09
HC	HC	kA	HC
Slovan	Slovan	k1gInSc4	Slovan
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009-10	[number]	k4	2009-10
HC	HC	kA	HC
Slovan	Slovan	k1gInSc4	Slovan
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010-11	[number]	k4	2010-11
HC	HC	kA	HC
Slovan	Slovan	k1gInSc4	Slovan
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
konec	konec	k1gInSc1	konec
hokejové	hokejový	k2eAgFnSc2d1	hokejová
kariéry	kariéra	k1gFnSc2	kariéra
Ano	ano	k9	ano
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
HC	HC	kA	HC
Slovan	Slovany	k1gInPc2	Slovany
Ústečtí	ústecký	k2eAgMnPc1d1	ústecký
Lvi	lev	k1gMnPc1	lev
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
hokejportal	hokejportat	k5eAaPmAgInS	hokejportat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
