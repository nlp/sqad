<s>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnPc2	Purple
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
anglické	anglický	k2eAgFnSc2d1	anglická
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
deska	deska	k1gFnSc1	deska
první	první	k4xOgFnSc2	první
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
propadlo	propadnout	k5eAaPmAgNnS	propadnout
na	na	k7c4	na
162	[number]	k4	162
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Evans	Evansa	k1gFnPc2	Evansa
a	a	k8xC	a
Simper	Simpra	k1gFnPc2	Simpra
opustili	opustit	k5eAaPmAgMnP	opustit
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
tak	tak	k6eAd1	tak
logicky	logicky	k6eAd1	logicky
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
časového	časový	k2eAgNnSc2d1	časové
hlediska	hledisko	k1gNnSc2	hledisko
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
etapu	etapa	k1gFnSc4	etapa
kapely	kapela	k1gFnSc2	kapela
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgInPc2	šedesátý
a	a	k8xC	a
otvírá	otvírat	k5eAaImIp3nS	otvírat
dveře	dveře	k1gFnPc4	dveře
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
charakteristickému	charakteristický	k2eAgInSc3d1	charakteristický
hardrocku	hardrock	k1gInSc3	hardrock
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
americké	americký	k2eAgNnSc1d1	americké
vydání	vydání	k1gNnSc1	vydání
je	být	k5eAaImIp3nS	být
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
Tetragrammaton	Tetragrammaton	k1gInSc1	Tetragrammaton
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydávání	vydávání	k1gNnSc2	vydávání
alba	album	k1gNnSc2	album
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
přerodu	přerod	k1gInSc2	přerod
skupiny	skupina	k1gFnSc2	skupina
z	z	k7c2	z
rockové	rockový	k2eAgFnSc2d1	rocková
v	v	k7c4	v
heavymetalovou	heavymetalový	k2eAgFnSc4d1	heavymetalová
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
nástupu	nástup	k1gInSc2	nástup
nového	nový	k2eAgInSc2d1	nový
stylu	styl	k1gInSc2	styl
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Painter	Painter	k1gMnSc1	Painter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Why	Why	k1gMnSc1	Why
Didn	Didn	k1gMnSc1	Didn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Rosemary	Rosemara	k1gFnSc2	Rosemara
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
především	především	k9	především
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Bird	Bird	k1gInSc1	Bird
Has	hasit	k5eAaImRp2nS	hasit
Blown	Blown	k1gNnSc4	Blown
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
vokálního	vokální	k2eAgInSc2d1	vokální
projevu	projev	k1gInSc2	projev
Roda	Roda	k1gFnSc1	Roda
Evanse	Evanse	k1gFnSc1	Evanse
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevují	projevovat	k5eAaImIp3nP	projevovat
Lordovy	lordův	k2eAgFnPc4d1	Lordova
tenze	tenze	k1gFnPc4	tenze
ke	k	k7c3	k
klasičtějším	klasický	k2eAgInPc3d2	klasičtější
tvarům	tvar	k1gInPc3	tvar
a	a	k8xC	a
modernímu	moderní	k2eAgInSc3d1	moderní
jazzu	jazz	k1gInSc3	jazz
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
orchestrální	orchestrální	k2eAgFnSc4d1	orchestrální
suitu	suita	k1gFnSc4	suita
"	"	kIx"	"
<g/>
April	April	k1gInSc4	April
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
varhanní	varhanní	k2eAgFnSc4d1	varhanní
vložku	vložka	k1gFnSc4	vložka
v	v	k7c6	v
předělávce	předělávka	k1gFnSc6	předělávka
Donovanovy	Donovanův	k2eAgFnSc2d1	Donovanova
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Lalena	Lalena	k1gFnSc1	Lalena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaImF	zdát
spojení	spojení	k1gNnSc1	spojení
art	art	k?	art
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
jazzových	jazzový	k2eAgMnPc2d1	jazzový
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
jako	jako	k8xS	jako
disfunkční	disfunkční	k2eAgNnSc1d1	disfunkční
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
tato	tento	k3xDgFnSc1	tento
kombinace	kombinace	k1gFnSc1	kombinace
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
desce	deska	k1gFnSc6	deska
velmi	velmi	k6eAd1	velmi
přirozeně	přirozeně	k6eAd1	přirozeně
a	a	k8xC	a
soudržně	soudržně	k6eAd1	soudržně
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pozdější	pozdní	k2eAgFnSc2d2	pozdější
tvrdší	tvrdý	k2eAgFnSc2d2	tvrdší
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
textům	text	k1gInPc3	text
daří	dařit	k5eAaImIp3nS	dařit
udržovat	udržovat	k5eAaImF	udržovat
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
hloubku	hloubka	k1gFnSc4	hloubka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
skvosty	skvost	k1gInPc4	skvost
rockových	rockový	k2eAgFnPc2d1	rocková
nahrávek	nahrávka	k1gFnPc2	nahrávka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nepřálo	přát	k5eNaImAgNnS	přát
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
;	;	kIx,	;
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
zkrachovalo	zkrachovat	k5eAaPmAgNnS	zkrachovat
americké	americký	k2eAgNnSc1d1	americké
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Tetragrammaton	Tetragrammaton	k1gInSc1	Tetragrammaton
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
většímu	veliký	k2eAgNnSc3d2	veliký
uvedení	uvedení	k1gNnSc3	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
zabránily	zabránit	k5eAaPmAgInP	zabránit
také	také	k9	také
protesty	protest	k1gInPc1	protest
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
církevních	církevní	k2eAgInPc2d1	církevní
kruhů	kruh	k1gInPc2	kruh
kvůli	kvůli	k7c3	kvůli
obrazu	obraz	k1gInSc3	obraz
Hieronyma	Hieronymus	k1gMnSc4	Hieronymus
Bosche	Bosche	k1gFnSc4	Bosche
Zahrada	zahrada	k1gFnSc1	zahrada
pozemských	pozemský	k2eAgFnPc2d1	pozemská
rozkoší	rozkoš	k1gFnPc2	rozkoš
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
obalu	obal	k1gInSc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Rod	rod	k1gInSc1	rod
Evans	Evans	k1gInSc1	Evans
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Nick	Nick	k1gMnSc1	Nick
Simper	Simper	k1gMnSc1	Simper
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
-	-	kIx~	-
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Ian	Ian	k1gMnSc1	Ian
Paice	Paice	k1gMnSc1	Paice
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
</s>
