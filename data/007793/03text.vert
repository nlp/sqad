<s>
Kinematograf	kinematograf	k1gInSc1	kinematograf
je	být	k5eAaImIp3nS	být
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
původní	původní	k2eAgFnSc4d1	původní
konstrukci	konstrukce	k1gFnSc4	konstrukce
prvních	první	k4xOgFnPc2	první
filmových	filmový	k2eAgFnPc2d1	filmová
promítaček	promítačka	k1gFnPc2	promítačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
také	také	k9	také
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInSc4d1	dnešní
termín	termín	k1gInSc4	termín
kino	kino	k1gNnSc1	kino
či	či	k8xC	či
biograf	biograf	k1gInSc1	biograf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
velice	velice	k6eAd1	velice
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
tak	tak	k6eAd1	tak
jedině	jedině	k6eAd1	jedině
jakožto	jakožto	k8xS	jakožto
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
přenosné	přenosný	k2eAgNnSc4d1	přenosné
či	či	k8xC	či
pojízdné	pojízdný	k2eAgNnSc4d1	pojízdné
kino	kino	k1gNnSc4	kino
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Kinematograf	kinematograf	k1gInSc1	kinematograf
bratří	bratr	k1gMnPc2	bratr
Čadíků	Čadík	k1gInPc2	Čadík
<g/>
.	.	kIx.	.
</s>
<s>
Kinematograf	kinematograf	k1gInSc4	kinematograf
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
Auguste	August	k1gMnSc5	August
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Lumiè	Lumiè	k1gFnSc2	Lumiè
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
