<p>
<s>
František	František	k1gMnSc1	František
Bogataj	Bogataj	k1gMnSc1	Bogataj
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
Uherský	uherský	k2eAgInSc1d1	uherský
Ostroh	Ostroh	k1gInSc1	Ostroh
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
Saint	Saint	k1gMnSc1	Saint
German	German	k1gMnSc1	German
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
výsadku	výsadek	k1gInSc2	výsadek
Carbon	Carbon	k1gMnSc1	Carbon
a	a	k8xC	a
účastník	účastník	k1gMnSc1	účastník
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Ostrohu	Ostroh	k1gInSc6	Ostroh
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Jan	Jan	k1gMnSc1	Jan
byl	být	k5eAaImAgMnS	být
tesařem	tesař	k1gMnSc7	tesař
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Zálešáková	Zálešáková	k1gFnSc1	Zálešáková
byla	být	k5eAaImAgFnS	být
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
10	[number]	k4	10
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
obci	obec	k1gFnSc6	obec
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
obecnou	obecná	k1gFnSc4	obecná
a	a	k8xC	a
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Obchodní	obchodní	k2eAgFnSc4d1	obchodní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1933	[number]	k4	1933
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
k	k	k7c3	k
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
důstojníky	důstojník	k1gMnPc4	důstojník
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
povyšován	povyšovat	k5eAaImNgInS	povyšovat
až	až	k6eAd1	až
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
četaře	četař	k1gMnSc2	četař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
poručíka	poručík	k1gMnSc2	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
Chebu	Cheb	k1gInSc2	Cheb
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
velitele	velitel	k1gMnSc2	velitel
roty	rota	k1gFnSc2	rota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
mobilizaci	mobilizace	k1gFnSc6	mobilizace
v	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
posádkového	posádkový	k2eAgMnSc2d1	posádkový
velitele	velitel	k1gMnSc2	velitel
v	v	k7c6	v
Ostrově	ostrov	k1gInSc6	ostrov
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
zapojil	zapojit	k5eAaPmAgMnS	zapojit
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
Obrany	obrana	k1gFnSc2	obrana
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
ON	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
hrozícímu	hrozící	k2eAgNnSc3d1	hrozící
zatčení	zatčení	k1gNnSc3	zatčení
opustil	opustit	k5eAaPmAgInS	opustit
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1939	[number]	k4	1939
protektorát	protektorát	k1gInSc4	protektorát
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
ON	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
exilu	exil	k1gInSc6	exil
==	==	k?	==
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
<g/>
,	,	kIx,	,
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
a	a	k8xC	a
Bejrút	Bejrút	k1gInSc1	Bejrút
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Agde	Agde	k1gFnSc6	Agde
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
do	do	k7c2	do
československé	československý	k2eAgFnSc2d1	Československá
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Francii	Francie	k1gFnSc4	Francie
se	se	k3xPyFc4	se
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
evakuován	evakuovat	k5eAaBmNgMnS	evakuovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
kulometné	kulometný	k2eAgFnSc2d1	kulometná
roty	rota	k1gFnSc2	rota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
nadporučíka	nadporučík	k1gMnSc2	nadporučík
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
do	do	k7c2	do
výcviku	výcvik	k1gInSc2	výcvik
pro	pro	k7c4	pro
plnění	plnění	k1gNnSc4	plnění
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1943	[number]	k4	1943
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
sabotážní	sabotážní	k2eAgInSc1d1	sabotážní
kurz	kurz	k1gInSc1	kurz
<g/>
,	,	kIx,	,
parakurz	parakurz	k1gInSc1	parakurz
<g/>
,	,	kIx,	,
kurz	kurz	k1gInSc1	kurz
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
sabotáže	sabotáž	k1gFnSc2	sabotáž
a	a	k8xC	a
konspirační	konspirační	k2eAgInSc1d1	konspirační
kurz	kurz	k1gInSc1	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
velitel	velitel	k1gMnSc1	velitel
skupiny	skupina	k1gFnSc2	skupina
Carbon	Carbon	k1gMnSc1	Carbon
společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
prodělal	prodělat	k5eAaPmAgMnS	prodělat
dokončovací	dokončovací	k2eAgInSc4d1	dokončovací
výcvik	výcvik	k1gInSc4	výcvik
a	a	k8xC	a
taktické	taktický	k2eAgNnSc4d1	taktické
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
výcvik	výcvik	k1gInSc1	výcvik
v	v	k7c6	v
civilním	civilní	k2eAgNnSc6d1	civilní
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
<g/>
,	,	kIx,	,
konspirační	konspirační	k2eAgNnPc1d1	konspirační
cvičení	cvičení	k1gNnPc1	cvičení
<g/>
,	,	kIx,	,
výcvik	výcvik	k1gInSc1	výcvik
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
a	a	k8xC	a
sabotážích	sabotáž	k1gFnPc6	sabotáž
a	a	k8xC	a
kurz	kurz	k1gInSc4	kurz
přijímání	přijímání	k1gNnSc2	přijímání
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
byl	být	k5eAaImAgInS	být
zakončen	zakončen	k2eAgInSc1d1	zakončen
dokončovacím	dokončovací	k2eAgNnSc7d1	dokončovací
cvičením	cvičení	k1gNnSc7	cvičení
a	a	k8xC	a
spojovacím	spojovací	k2eAgNnSc7d1	spojovací
cvičením	cvičení	k1gNnSc7	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
skupinou	skupina	k1gFnSc7	skupina
přepraven	přepravit	k5eAaPmNgInS	přepravit
do	do	k7c2	do
Alžíru	Alžír	k1gInSc2	Alžír
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c4	na
vyčkávací	vyčkávací	k2eAgFnSc4d1	vyčkávací
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
vysazen	vysazen	k2eAgMnSc1d1	vysazen
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Ratíškovice	Ratíškovice	k1gFnSc2	Ratíškovice
a	a	k8xC	a
Vacenovice	Vacenovice	k1gFnSc2	Vacenovice
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
po	po	k7c6	po
seskoku	seskok	k1gInSc6	seskok
nesešla	sejít	k5eNaPmAgFnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
Šperlem	Šperl	k1gMnSc7	Šperl
<g/>
.	.	kIx.	.
</s>
<s>
Navázali	navázat	k5eAaPmAgMnP	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
odbojem	odboj	k1gInSc7	odboj
a	a	k8xC	a
navázali	navázat	k5eAaPmAgMnP	navázat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Vytyčili	vytyčit	k5eAaPmAgMnP	vytyčit
23	[number]	k4	23
doskokových	doskokový	k2eAgFnPc2d1	doskoková
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
sabotáži	sabotáž	k1gFnSc6	sabotáž
proti	proti	k7c3	proti
železnici	železnice	k1gFnSc3	železnice
a	a	k8xC	a
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
vojenské	vojenský	k2eAgFnPc4d1	vojenská
transporty	transporta	k1gFnPc4	transporta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
zatčení	zatčení	k1gNnSc4	zatčení
gestapem	gestapo	k1gNnSc7	gestapo
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
osvobozování	osvobozování	k1gNnSc4	osvobozování
Popovic	Popovice	k1gFnPc2	Popovice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
zpravodajským	zpravodajský	k2eAgMnSc7d1	zpravodajský
důstojníkům	důstojník	k1gMnPc3	důstojník
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
k	k	k7c3	k
MNO	MNO	kA	MNO
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
k	k	k7c3	k
33	[number]	k4	33
<g/>
.	.	kIx.	.
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
jako	jako	k8xC	jako
osvětový	osvětový	k2eAgMnSc1d1	osvětový
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
štábního	štábní	k2eAgMnSc4d1	štábní
kapitána	kapitán	k1gMnSc4	kapitán
a	a	k8xC	a
týž	týž	k3xTgInSc1	týž
den	den	k1gInSc1	den
na	na	k7c4	na
majora	major	k1gMnSc4	major
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
válečné	válečný	k2eAgFnSc6d1	válečná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ukončení	ukončení	k1gNnSc6	ukončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
sloužil	sloužit	k5eAaImAgInS	sloužit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Přáslavicích	Přáslavice	k1gFnPc6	Přáslavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
do	do	k7c2	do
zálohy	záloha	k1gFnSc2	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
upozornění	upozornění	k1gNnSc3	upozornění
příslušníkem	příslušník	k1gMnSc7	příslušník
SNB	SNB	kA	SNB
na	na	k7c6	na
hrozící	hrozící	k2eAgNnSc4d1	hrozící
zatčení	zatčení	k1gNnSc4	zatčení
opustil	opustit	k5eAaPmAgInS	opustit
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
zakládajícím	zakládající	k2eAgMnPc3d1	zakládající
členům	člen	k1gMnPc3	člen
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
CIC	cic	k1gInSc1	cic
působila	působit	k5eAaImAgFnS	působit
při	při	k7c6	při
výcviku	výcvik	k1gInSc6	výcvik
a	a	k8xC	a
vysílání	vysílání	k1gNnSc1	vysílání
kurýrů	kurýr	k1gMnPc2	kurýr
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
velice	velice	k6eAd1	velice
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
kurýrů	kurýr	k1gMnPc2	kurýr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgMnPc3	který
Bogataj	Bogataj	k1gInSc1	Bogataj
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Štěpán	Štěpán	k1gMnSc1	Štěpán
Gavenda	Gavend	k1gMnSc2	Gavend
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postupného	postupný	k2eAgNnSc2d1	postupné
ukončování	ukončování	k1gNnSc2	ukončování
činnosti	činnost	k1gFnSc2	činnost
exilových	exilový	k2eAgFnPc2d1	exilová
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
skupin	skupina	k1gFnPc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
bankovním	bankovní	k2eAgInSc6d1	bankovní
sektoru	sektor	k1gInSc6	sektor
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
až	až	k9	až
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Americké	americký	k2eAgFnSc2d1	americká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Frank	Frank	k1gMnSc1	Frank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generálmajora	generálmajor	k1gMnSc2	generálmajor
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Urna	urna	k1gFnSc1	urna
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
popelem	popel	k1gInSc7	popel
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Ostrohu	Ostroh	k1gInSc6	Ostroh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
1944	[number]	k4	1944
–	–	k?	–
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	s	k7c7	s
štítky	štítek	k1gInPc7	štítek
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
–	–	k?	–
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
medaile	medaile	k1gFnSc1	medaile
Za	za	k7c4	za
chrabrost	chrabrost	k1gFnSc4	chrabrost
před	před	k7c7	před
nepřítelem	nepřítel	k1gMnSc7	nepřítel
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
medaile	medaile	k1gFnSc1	medaile
za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
–	–	k?	–
druhý	druhý	k4xOgMnSc1	druhý
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
–	–	k?	–
třetí	třetí	k4xOgMnSc1	třetí
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnSc2	Victorium
medal	medat	k5eAaBmAgMnS	medat
(	(	kIx(	(
<g/>
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Defence	Defence	k1gFnSc1	Defence
medal	medal	k1gInSc1	medal
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Řád	řád	k1gInSc1	řád
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
REICHL	REICHL	kA	REICHL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86808	[number]	k4	86808
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LÁNÍK	Láník	k1gMnSc1	Láník
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgFnPc1d1	vojenská
osobnosti	osobnost	k1gFnPc1	osobnost
československého	československý	k2eAgInSc2d1	československý
odboje	odboj	k1gInSc2	odboj
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
ČR-AVIS	ČR-AVIS	k1gFnSc2	ČR-AVIS
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7278	[number]	k4	7278
<g/>
-	-	kIx~	-
<g/>
233	[number]	k4	233
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Bratři	bratr	k1gMnPc1	bratr
František	František	k1gMnSc1	František
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Bogatajovi	Bogatajův	k2eAgMnPc1d1	Bogatajův
<g/>
:	:	kIx,	:
Protikomunistický	protikomunistický	k2eAgInSc4d1	protikomunistický
odboj	odboj	k1gInSc4	odboj
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
</s>
</p>
<p>
<s>
Weblog	Weblog	k1gMnSc1	Weblog
J.	J.	kA	J.
Šnýdla	Šnýdla	k1gMnSc1	Šnýdla
</s>
</p>
<p>
<s>
Kmenový	kmenový	k2eAgInSc1d1	kmenový
list	list	k1gInSc1	list
</s>
</p>
