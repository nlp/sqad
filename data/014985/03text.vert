<s>
Janičáři	janičár	k1gMnPc1
</s>
<s>
Sedící	sedící	k2eAgMnSc1d1
janičár	janičár	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perokresba	perokresba	k1gFnSc1
Gentile	Gentil	k1gMnSc5
Belliniho	Bellini	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1479	#num#	k4
<g/>
–	–	k?
<g/>
81	#num#	k4
</s>
<s>
Janičáři	janičár	k1gMnPc1
(	(	kIx(
<g/>
jedn	jedn	k1gMnSc1
<g/>
.	.	kIx.
č.	č.	k?
<g/>
:	:	kIx,
janičár	janičár	k1gMnSc1
vyslov	vyslovit	k5eAaPmRp2nS
[	[	kIx(
<g/>
jaňičár	jaňičár	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jen	jen	k9
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
pádu	pád	k1gInSc2
mn	mn	k?
<g/>
.	.	kIx.
č.	č.	k?
tvar	tvar	k1gInSc1
janičáři	janičár	k1gMnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
adj.	adj.	k?
janičárský	janičárský	k2eAgInSc4d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
tureckého	turecký	k2eAgMnSc2d1
ي	ي	k?
<g/>
,	,	kIx,
yeni	yeni	k6eAd1
çeri	çeri	k1gNnSc1
janyčari	janyčar	k1gFnSc2
–	–	k?
nové	nový	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
elitní	elitní	k2eAgFnSc2d1
pěchotní	pěchotní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
osmanské	osmanský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
janičárů	janičár	k1gMnPc2
(	(	kIx(
<g/>
existující	existující	k2eAgInSc1d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1330	#num#	k4
<g/>
–	–	k?
<g/>
1826	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rekrutovaných	rekrutovaný	k2eAgFnPc2d1
z	z	k7c2
nemuslimského	muslimský	k2eNgNnSc2d1
(	(	kIx(
<g/>
křesťanského	křesťanský	k2eAgNnSc2d1
<g/>
)	)	kIx)
obyvatelstva	obyvatelstvo	k1gNnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
řad	řada	k1gFnPc2
Slovanů	Slovan	k1gMnPc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Janičáři	janičár	k1gMnPc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
stálé	stálý	k2eAgNnSc4d1
<g/>
,	,	kIx,
profesionální	profesionální	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jádro	jádro	k1gNnSc1
osmanské	osmanský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesné	přesný	k2eAgNnSc4d1
datum	datum	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
vzniku	vznik	k1gInSc2
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
historikové	historik	k1gMnPc1
přisuzují	přisuzovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gInSc1
vznik	vznik	k1gInSc1
Orhanovi	Orhan	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
vládl	vládnout	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1362	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
se	se	k3xPyFc4
spíše	spíše	k9
kloní	klonit	k5eAaImIp3nS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
jejich	jejich	k3xOp3gNnSc7
založením	založení	k1gNnSc7
a	a	k8xC
hlavně	hlavně	k6eAd1
rozvojem	rozvoj	k1gInSc7
stojí	stát	k5eAaImIp3nS
až	až	k9
Orhanův	Orhanův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Murad	Murad	k1gInSc4
I.	I.	kA
Původně	původně	k6eAd1
byly	být	k5eAaImAgFnP
jejich	jejich	k3xOp3gFnPc1
jednotky	jednotka	k1gFnPc1
doplňovány	doplňovat	k5eAaImNgFnP
prostřednictvím	prostřednictvím	k7c2
tzv.	tzv.	kA
„	„	k?
<g/>
daně	daň	k1gFnSc2
krve	krev	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
devširme	devširm	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procházeli	procházet	k5eAaImAgMnP
důkladným	důkladný	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
výcvikem	výcvik	k1gInSc7
a	a	k8xC
požívali	požívat	k5eAaImAgMnP
řady	řada	k1gFnPc4
výhod	výhoda	k1gFnPc2
a	a	k8xC
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formování	formování	k1gNnPc2
janičárů	janičár	k1gMnPc2
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
skvělý	skvělý	k2eAgInSc4d1
strategický	strategický	k2eAgInSc4d1
tah	tah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
často	často	k6eAd1
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
dosti	dosti	k6eAd1
chudých	chudý	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
na	na	k7c6
okupovaných	okupovaný	k2eAgNnPc6d1
územích	území	k1gNnPc6
a	a	k8xC
jako	jako	k8xC,k8xS
janičáři	janičár	k1gMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
elitními	elitní	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
i	i	k9
na	na	k7c6
správě	správa	k1gFnSc6
okupovaných	okupovaný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgMnSc7
získali	získat	k5eAaPmAgMnP
Osmané	Osman	k1gMnPc1
důležité	důležitý	k2eAgFnSc2d1
spojence	spojenec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
znali	znát	k5eAaImAgMnP
místní	místní	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
a	a	k8xC
místní	místní	k2eAgInPc4d1
poměry	poměr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Standardními	standardní	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
janičárů	janičár	k1gMnPc2
byly	být	k5eAaImAgFnP
zpočátku	zpočátku	k6eAd1
luky	luk	k1gInPc1
a	a	k8xC
šavle	šavle	k1gFnPc1
<g/>
,	,	kIx,
popř.	popř.	kA
kyje	kyj	k1gInPc1
<g/>
;	;	kIx,
později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
výzbroj	výzbroj	k1gFnSc1
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
palné	palný	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
boje	boj	k1gInSc2
se	se	k3xPyFc4
vrhali	vrhat	k5eAaImAgMnP
bez	bez	k7c2
ptaní	ptaní	k1gNnSc2
<g/>
,	,	kIx,
poslušně	poslušně	k6eAd1
prováděli	provádět	k5eAaImAgMnP
<g/>
,	,	kIx,
co	co	k8xS
jim	on	k3xPp3gMnPc3
bylo	být	k5eAaImAgNnS
přikázáno	přikázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Vyznamenali	vyznamenat	k5eAaPmAgMnP
se	se	k3xPyFc4
kupříkladu	kupříkladu	k6eAd1
proti	proti	k7c3
Srbům	Srb	k1gMnPc3
v	v	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Kosově	Kosův	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Konstantinopole	Konstantinopol	k1gInSc2
či	či	k8xC
při	při	k7c6
prvním	první	k4xOgNnSc6
obléhání	obléhání	k1gNnSc6
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
jsou	být	k5eAaImIp3nP
termínem	termín	k1gInSc7
janičáři	janičár	k1gMnPc1
označováni	označován	k2eAgMnPc1d1
synové	syn	k1gMnPc1
významných	významný	k2eAgInPc2d1
rodů	rod	k1gInPc2
vzatí	vzatý	k2eAgMnPc1d1
jako	jako	k9
rukojmí	rukojmí	k1gMnPc1
a	a	k8xC
vycvičeni	vycvičit	k5eAaPmNgMnP
k	k	k7c3
věrnosti	věrnost	k1gFnSc3
svým	svůj	k3xOyFgMnPc3
věznitelům	věznitel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
zaručit	zaručit	k5eAaPmF
nejen	nejen	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gMnPc1
otcové	otec	k1gMnPc1
nevzbouří	vzbouřit	k5eNaPmIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
že	že	k8xS
janičáři	janičár	k1gMnPc1
budou	být	k5eAaImBp3nP
věrně	věrně	k6eAd1
následovat	následovat	k5eAaImF
onoho	onen	k3xDgMnSc4
panovníka	panovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
významu	význam	k1gInSc6
užil	užít	k5eAaPmAgMnS
termín	termín	k1gInSc4
janičár	janičár	k1gMnSc1
kupříkladu	kupříkladu	k6eAd1
David	David	k1gMnSc1
Gemmell	Gemmell	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Smrtonoš	smrtonoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1
zánik	zánik	k1gInSc1
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
však	však	k9
janičáři	janičár	k1gMnPc1
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
více	hodně	k6eAd2
samostatní	samostatný	k2eAgMnPc1d1
na	na	k7c6
osmanských	osmanský	k2eAgMnPc6d1
sultánech	sultán	k1gMnPc6
a	a	k8xC
více	hodně	k6eAd2
se	se	k3xPyFc4
zajímali	zajímat	k5eAaImAgMnP
o	o	k7c4
zachování	zachování	k1gNnSc4
svých	svůj	k3xOyFgFnPc2
výhod	výhoda	k1gFnPc2
a	a	k8xC
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
přestali	přestat	k5eAaPmAgMnP
vyvíjet	vyvíjet	k5eAaImF
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgFnPc2d1
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
evropských	evropský	k2eAgFnPc2d1
<g/>
)	)	kIx)
armád	armáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spíše	spíše	k9
naopak	naopak	k6eAd1
mnohým	mnohý	k2eAgFnPc3d1
reformám	reforma	k1gFnPc3
<g/>
,	,	kIx,
s	s	k7c7
cílem	cíl	k1gInSc7
větší	veliký	k2eAgFnSc2d2
modernizace	modernizace	k1gFnSc2
<g/>
,	,	kIx,
sami	sám	k3xTgMnPc1
urputně	urputně	k6eAd1
bránili	bránit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
po	po	k7c6
zrušení	zrušení	k1gNnSc6
devširme	devširm	k1gInSc5
roku	rok	k1gInSc2
1683	#num#	k4
<g/>
)	)	kIx)
již	již	k6eAd1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
nižší	nízký	k2eAgFnSc3d2
a	a	k8xC
střední	střední	k2eAgFnSc3d1
vrstvě	vrstva	k1gFnSc3
tureckého	turecký	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
často	často	k6eAd1
řemeslníci	řemeslník	k1gMnPc1
nebo	nebo	k8xC
kupci	kupec	k1gMnPc1
přilákaní	přilákaný	k2eAgMnPc1d1
výhodami	výhoda	k1gFnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
více	hodně	k6eAd2
než	než	k8xS
vojenské	vojenský	k2eAgFnSc3d1
službě	služba	k1gFnSc3
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
své	svůj	k3xOyFgFnSc6
civilní	civilní	k2eAgFnSc6d1
profesi	profes	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platilo	platit	k5eAaImAgNnS
prý	prý	k9
pravidlo	pravidlo	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
syn	syn	k1gMnSc1
janičáře	janičář	k1gInSc2
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
janičárem	janičár	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Janičáři	janičár	k1gMnPc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stali	stát	k5eAaPmAgMnP
vlivnou	vlivný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
odstranit	odstranit	k5eAaPmF
jakéhokoli	jakýkoli	k3yIgMnSc4
sultána	sultán	k1gMnSc4
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
nesouhlasila	souhlasit	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Postupně	postupně	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
jezdeckou	jezdecký	k2eAgFnSc7d1
částí	část	k1gFnSc7
sipahí	sipahí	k1gFnSc2
<g/>
)	)	kIx)
ovládli	ovládnout	k5eAaPmAgMnP
nakonec	nakonec	k6eAd1
celou	celý	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
však	však	k9
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
pouze	pouze	k6eAd1
k	k	k7c3
osobnímu	osobní	k2eAgNnSc3d1
obohacení	obohacení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakýkoliv	jakýkoliv	k3yIgMnSc1
pokus	pokus	k1gInSc4
o	o	k7c4
snížení	snížení	k1gNnSc4
svých	svůj	k3xOyFgFnPc2
pravomocí	pravomoc	k1gFnPc2
zastavili	zastavit	k5eAaPmAgMnP
hned	hned	k6eAd1
v	v	k7c6
počátku	počátek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1622	#num#	k4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
konfliktů	konflikt	k1gInPc2
s	s	k7c7
mladým	mladý	k1gMnSc7
sultánem	sultán	k1gMnSc7
Osmanem	Osman	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
chtěl	chtít	k5eAaImAgMnS
nahradit	nahradit	k5eAaPmF
novou	nový	k2eAgFnSc7d1
modernější	moderní	k2eAgFnSc7d2
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
posléze	posléze	k6eAd1
ho	on	k3xPp3gMnSc4
zavřeli	zavřít	k5eAaPmAgMnP
a	a	k8xC
zavraždili	zavraždit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
relativně	relativně	k6eAd1
prosperující	prosperující	k2eAgMnSc1d1
tzv.	tzv.	kA
období	období	k1gNnSc1
tulipánů	tulipán	k1gInPc2
za	za	k7c4
sultána	sultán	k1gMnSc4
Ahmeda	Ahmed	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
ukončili	ukončit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1730	#num#	k4
a	a	k8xC
donutili	donutit	k5eAaPmAgMnP
sultána	sultán	k1gMnSc4
k	k	k7c3
abdikaci	abdikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
svrhli	svrhnout	k5eAaPmAgMnP
a	a	k8xC
zavraždili	zavraždit	k5eAaPmAgMnP
dalšího	další	k2eAgMnSc4d1
sultána	sultán	k1gMnSc4
Selima	Selim	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
je	on	k3xPp3gFnPc4
pokusil	pokusit	k5eAaPmAgMnS
zrušit	zrušit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Janičáři	janičár	k1gMnPc1
byli	být	k5eAaImAgMnP
zrušeni	zrušit	k5eAaPmNgMnP
po	po	k7c6
své	svůj	k3xOyFgFnSc6
vzpouře	vzpoura	k1gFnSc6
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
dekretem	dekret	k1gInSc7
sultána	sultán	k1gMnSc2
Mahmuta	Mahmut	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1826	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Frazeologie	frazeologie	k1gFnSc1
</s>
<s>
Od	od	k7c2
janičárů	janičár	k1gMnPc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
úsloví	úsloví	k1gNnSc3
poturčenec	poturčenec	k1gMnSc1
horší	zlý	k2eAgMnSc1d2
Turka	turek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Měkká	měkký	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
zde	zde	k6eAd1
není	být	k5eNaImIp3nS
samozřejmá	samozřejmý	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
slabiky	slabika	k1gFnPc1
-di-	-di-	k?
<g/>
,	,	kIx,
-ti-	-ti-	k?
<g/>
,	,	kIx,
-ni-	-ni-	k?
ve	v	k7c6
slovech	slovo	k1gNnPc6
cizího	cizí	k2eAgInSc2d1
původu	původ	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
obvykle	obvykle	k6eAd1
vyslovují	vyslovovat	k5eAaImIp3nP
tvrdě	tvrdě	k6eAd1
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
slovo	slovo	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
už	už	k6eAd1
zdomácnělé	zdomácnělý	k2eAgFnPc1d1
a	a	k8xC
proto	proto	k8xC
má	mít	k5eAaImIp3nS
i	i	k9
českou	český	k2eAgFnSc4d1
měkkou	měkký	k2eAgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
KLIMEŠ	Klimeš	k1gMnSc1
<g/>
,	,	kIx,
Lumír	Lumír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
cizích	cizí	k2eAgFnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26059	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
)	)	kIx)
<g/>
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.	v.	k?
v.	v.	k?
i	i	k9
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
janičáři	janičár	k1gMnPc1
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgInSc1d1
akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
janičár	janičár	k1gMnSc1
<g/>
↑	↑	k?
Slovník	slovník	k1gInSc1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
janičár	janičár	k1gMnSc1
<g/>
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
LYER	LYER	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručný	stručný	k2eAgInSc1d1
etymologický	etymologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
se	se	k3xPyFc4
zvláštním	zvláštní	k2eAgInSc7d1
zřetelem	zřetel	k1gInSc7
k	k	k7c3
slovům	slovo	k1gNnPc3
kulturním	kulturní	k2eAgNnPc3d1
a	a	k8xC
cizím	cizí	k2eAgNnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23715	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
206	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Duden	Duden	k1gInSc1
-	-	kIx~
Deutsches	Deutsches	k1gMnSc1
Universalwörterbuch	Universalwörterbuch	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aufl	Aufla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mannheim	Mannheim	k1gInSc1
2003	#num#	k4
[	[	kIx(
<g/>
CD-ROM	CD-ROM	k1gFnSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Ja	Ja	k?
<g/>
|	|	kIx~
<g/>
nit	nit	k1gFnSc1
<g/>
|	|	kIx~
<g/>
schar	schar	k1gMnSc1
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
<g/>
;	;	kIx,
-en	-en	k?
<g/>
,	,	kIx,
-en	-en	k?
[	[	kIx(
<g/>
türk	türk	k1gMnSc1
<g/>
.	.	kIx.
yeniceri	yenicere	k1gFnSc4
<g/>
,	,	kIx,
eigtl	eigtnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
=	=	kIx~
neue	uat	k5eNaPmIp3nS
Streitmacht	Streitmacht	k1gMnSc1
<g/>
;	;	kIx,
urspr	urspr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
=	=	kIx~
Bez	bez	k1gInSc4
<g/>
.	.	kIx.
der	drát	k5eAaImRp2nS
bevorrechtigten	bevorrechtigtno	k1gNnPc2
Kriegerklasse	Kriegerklass	k1gMnSc2
im	im	k?
Osman	Osman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reich	Reich	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Soldat	Soldat	k1gMnSc1
einer	einer	k1gMnSc1
Kerntruppe	Kerntrupp	k1gInSc5
des	des	k1gNnSc7
osmanischen	osmanischen	k2eAgInSc1d1
Sultans	Sultans	k1gInSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Jh	Jh	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.1	.1	k4
2	#num#	k4
Velká	velký	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Я	Я	k?
<g/>
́	́	k?
<g/>
Р	Р	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
časopis	časopis	k1gInSc1
Akta	akta	k1gNnPc1
History	Histor	k1gInPc1
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
stránka	stránka	k1gFnSc1
58	#num#	k4
<g/>
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.	v.	k?
v.	v.	k?
i	i	k9
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
poturčenec	poturčenec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mahmut	Mahmut	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4162701-5	4162701-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85069346	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85069346	#num#	k4
</s>
