<p>
<s>
Semmering	Semmering	k1gInSc1	Semmering
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
lyžařské	lyžařský	k2eAgNnSc4d1	lyžařské
středisko	středisko	k1gNnSc4	středisko
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
střediska	středisko	k1gNnPc1	středisko
nacházející	nacházející	k2eAgNnPc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
Stuhleck	Stuhlecka	k1gFnPc2	Stuhlecka
a	a	k8xC	a
Hirschenkogel	Hirschenkogela	k1gFnPc2	Hirschenkogela
<g/>
.	.	kIx.	.
</s>
<s>
Středisko	středisko	k1gNnSc1	středisko
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
s	s	k7c7	s
celoročním	celoroční	k2eAgInSc7d1	celoroční
provozem	provoz	k1gInSc7	provoz
s	s	k7c7	s
lyžařskými	lyžařský	k2eAgFnPc7d1	lyžařská
a	a	k8xC	a
saňovými	saňový	k2eAgFnPc7d1	saňová
trasami	trasa	k1gFnPc7	trasa
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
s	s	k7c7	s
parkem	park	k1gInSc7	park
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
a	a	k8xC	a
s	s	k7c7	s
pěšími	pěší	k2eAgFnPc7d1	pěší
trasami	trasa	k1gFnPc7	trasa
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Semmeringu	Semmering	k1gInSc6	Semmering
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
konal	konat	k5eAaImAgInS	konat
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
v	v	k7c6	v
alpském	alpský	k2eAgNnSc6d1	alpské
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Semmering	Semmering	k1gInSc1	Semmering
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Dolního	dolní	k2eAgNnSc2d1	dolní
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obklopen	obklopen	k2eAgInSc4d1	obklopen
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Semmering	Semmering	k1gInSc1	Semmering
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Neunkirchen	Neunkirchna	k1gFnPc2	Neunkirchna
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
autem	aut	k1gInSc7	aut
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vlakem	vlak	k1gInSc7	vlak
po	po	k7c6	po
rakouské	rakouský	k2eAgFnSc6d1	rakouská
jižní	jižní	k2eAgFnSc6d1	jižní
dráze	dráha	k1gFnSc6	dráha
trvá	trvat	k5eAaImIp3nS	trvat
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
čtvrt	čtvrt	k1gFnSc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
mezi	mezi	k7c7	mezi
rakouským	rakouský	k2eAgNnSc7d1	rakouské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
lyžařským	lyžařský	k2eAgNnSc7d1	lyžařské
střediskem	středisko	k1gNnSc7	středisko
jezdí	jezdit	k5eAaImIp3nS	jezdit
asi	asi	k9	asi
10	[number]	k4	10
přímých	přímý	k2eAgInPc2d1	přímý
vlakových	vlakový	k2eAgInPc2d1	vlakový
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Semmering	Semmering	k1gInSc1	Semmering
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prochází	procházet	k5eAaImIp3nS	procházet
obcí	obec	k1gFnSc7	obec
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
objektem	objekt	k1gInSc7	objekt
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
horskou	horský	k2eAgFnSc4d1	horská
železnici	železnice	k1gFnSc4	železnice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
zde	zde	k6eAd1	zde
překonává	překonávat	k5eAaImIp3nS	překonávat
mnoho	mnoho	k4c1	mnoho
viaduktů	viadukt	k1gInPc2	viadukt
a	a	k8xC	a
projíždí	projíždět	k5eAaImIp3nS	projíždět
množstvím	množství	k1gNnSc7	množství
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
Schwechat	Schwechat	k1gMnSc1	Schwechat
(	(	kIx(	(
<g/>
106	[number]	k4	106
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zimní	zimní	k2eAgFnSc1d1	zimní
sezóna	sezóna	k1gFnSc1	sezóna
===	===	k?	===
</s>
</p>
<p>
<s>
Lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
sezóna	sezóna	k1gFnSc1	sezóna
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
Semmering	Semmering	k1gInSc1	Semmering
je	být	k5eAaImIp3nS	být
lyžařským	lyžařský	k2eAgNnSc7d1	lyžařské
místem	místo	k1gNnSc7	místo
s	s	k7c7	s
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
14	[number]	k4	14
km	km	kA	km
tras	tras	k1gInSc1	tras
pro	pro	k7c4	pro
celodenní	celodenní	k2eAgNnSc4d1	celodenní
lyžování	lyžování	k1gNnSc4	lyžování
(	(	kIx(	(
<g/>
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
3	[number]	k4	3
km	km	kA	km
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
-	-	kIx~	-
střední	střední	k2eAgFnSc6d1	střední
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
1	[number]	k4	1
km	km	kA	km
–	–	k?	–
obtížná	obtížný	k2eAgFnSc1d1	obtížná
trasa	trasa	k1gFnSc1	trasa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
13	[number]	k4	13
km	km	kA	km
tras	tras	k1gInSc1	tras
pro	pro	k7c4	pro
noční	noční	k2eAgNnSc4d1	noční
lyžování	lyžování	k1gNnSc4	lyžování
</s>
</p>
<p>
<s>
12	[number]	k4	12
km	km	kA	km
tras	tras	k1gInSc4	tras
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
běžecké	běžecký	k2eAgNnSc4d1	běžecké
lyžování	lyžování	k1gNnSc4	lyžování
</s>
</p>
<p>
<s>
3	[number]	k4	3
kilometrová	kilometrový	k2eAgFnSc1d1	kilometrová
saňová	saňový	k2eAgFnSc1d1	saňová
skluzavka	skluzavka	k1gFnSc1	skluzavka
s	s	k7c7	s
nočním	noční	k2eAgNnSc7d1	noční
osvětlením	osvětlení	k1gNnSc7	osvětlení
</s>
</p>
<p>
<s>
snowboard	snowboard	k1gInSc1	snowboard
park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
halfpipe	halfpipat	k5eAaPmIp3nS	halfpipat
<g/>
,	,	kIx,	,
quarter-pipe	quarteripat	k5eAaPmIp3nS	quarter-pipat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
vlekyRozdíl	vlekyRozdínout	k5eAaPmAgMnS	vlekyRozdínout
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
do	do	k7c2	do
400	[number]	k4	400
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Letní	letní	k2eAgFnSc1d1	letní
sezóna	sezóna	k1gFnSc1	sezóna
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
sportovní	sportovní	k2eAgNnSc4d1	sportovní
centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
konání	konání	k1gNnSc2	konání
extrémních	extrémní	k2eAgFnPc2d1	extrémní
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
akcí	akce	k1gFnPc2	akce
-	-	kIx~	-
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
cyklistických	cyklistický	k2eAgInPc2d1	cyklistický
parků	park	k1gInPc2	park
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
celodenní	celodenní	k2eAgInSc1d1	celodenní
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
"	"	kIx"	"
<g/>
Race	Race	k1gInSc1	Race
the	the	k?	the
night	night	k1gInSc1	night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Semmeringu	Semmering	k1gInSc6	Semmering
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Split-park	Splitark	k1gInSc1	Split-park
s	s	k7c7	s
16	[number]	k4	16
figury	figura	k1gFnPc1	figura
</s>
</p>
<p>
<s>
7	[number]	k4	7
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
zón	zóna	k1gFnPc2	zóna
různých	různý	k2eAgFnPc2d1	různá
úrovní	úroveň	k1gFnPc2	úroveň
složitosti	složitost	k1gFnSc2	složitost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dětské	dětský	k2eAgFnSc2d1	dětská
zóny	zóna	k1gFnSc2	zóna
</s>
</p>
<p>
<s>
185	[number]	k4	185
km	km	kA	km
dobře	dobře	k6eAd1	dobře
vybavených	vybavený	k2eAgFnPc2d1	vybavená
pěších	pěší	k2eAgFnPc2d1	pěší
tras	trasa	k1gFnPc2	trasa
</s>
</p>
<p>
<s>
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
</s>
</p>
<p>
<s>
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
</s>
</p>
<p>
<s>
SPA	SPA	kA	SPA
a	a	k8xC	a
wellness	wellness	k6eAd1	wellness
programy	program	k1gInPc4	program
</s>
</p>
<p>
<s>
===	===	k?	===
Ubytování	ubytování	k1gNnSc4	ubytování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Semmeringu	Semmering	k1gInSc6	Semmering
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
je	být	k5eAaImIp3nS	být
Hotel	hotel	k1gInSc4	hotel
Panhans	Panhans	k1gInSc1	Panhans
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
hostil	hostit	k5eAaImAgInS	hostit
mnoho	mnoho	k6eAd1	mnoho
slavných	slavný	k2eAgMnPc2d1	slavný
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
Arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Karel	Karel	k1gMnSc1	Karel
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc1	Loos
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Altenberg	Altenberg	k1gMnSc1	Altenberg
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Schnitzler	Schnitzler	k1gMnSc1	Schnitzler
<g/>
,	,	kIx,	,
Gerhart	Gerhart	k1gInSc1	Gerhart
Hauptmann	Hauptmann	k1gMnSc1	Hauptmann
nebo	nebo	k8xC	nebo
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Semmering	Semmering	k1gInSc1	Semmering
(	(	kIx(	(
<g/>
ski	ski	k1gFnSc1	ski
resort	resort	k1gInSc1	resort
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
