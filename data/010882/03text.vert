<p>
<s>
Koláč	koláč	k1gInSc1	koláč
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
pečením	pečeně	k1gFnPc3	pečeně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
připravovaném	připravovaný	k2eAgInSc6d1	připravovaný
druhu	druh	k1gInSc6	druh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Koláčem	koláč	k1gInSc7	koláč
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
buď	buď	k8xC	buď
menší	malý	k2eAgInPc4d2	menší
drobné	drobný	k2eAgInPc4d1	drobný
kousky	kousek	k1gInPc4	kousek
pečiva	pečivo	k1gNnSc2	pečivo
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
či	či	k8xC	či
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velký	velký	k2eAgInSc1d1	velký
plát	plát	k1gInSc1	plát
těsta	těsto	k1gNnSc2	těsto
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sladká	sladký	k2eAgFnSc1d1	sladká
nebo	nebo	k8xC	nebo
slaná	slaný	k2eAgFnSc1d1	slaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
jako	jako	k8xC	jako
koláč	koláč	k1gInSc1	koláč
označuje	označovat	k5eAaImIp3nS	označovat
menší	malý	k2eAgNnSc4d2	menší
kruhové	kruhový	k2eAgNnSc4d1	kruhové
pečivo	pečivo	k1gNnSc4	pečivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
středu	střed	k1gInSc6	střed
sladkou	sladký	k2eAgFnSc4d1	sladká
náplň	náplň	k1gFnSc4	náplň
(	(	kIx(	(
<g/>
makovou	makový	k2eAgFnSc4d1	maková
<g/>
,	,	kIx,	,
tvarohovou	tvarohový	k2eAgFnSc4d1	tvarohová
<g/>
,	,	kIx,	,
ořechovou	ořechový	k2eAgFnSc4d1	ořechová
<g/>
,	,	kIx,	,
marmeládovou	marmeládový	k2eAgFnSc4d1	marmeládová
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Tradiční	tradiční	k2eAgInPc1d1	tradiční
české	český	k2eAgInPc1d1	český
koláčky	koláček	k1gInPc1	koláček
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
během	během	k7c2	během
posvícení	posvícení	k1gNnSc2	posvícení
jako	jako	k8xS	jako
sváteční	sváteční	k2eAgNnSc4d1	sváteční
pohoštění	pohoštění	k1gNnSc4	pohoštění
a	a	k8xC	a
nebo	nebo	k8xC	nebo
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
událostech	událost	k1gFnPc6	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
malé	malý	k2eAgInPc1d1	malý
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
nepřesahující	přesahující	k2eNgFnSc2d1	nepřesahující
8	[number]	k4	8
cm	cm	kA	cm
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
náplně	náplň	k1gFnSc2	náplň
a	a	k8xC	a
posypané	posypaný	k2eAgInPc1d1	posypaný
sladkou	sladký	k2eAgFnSc7d1	sladká
drobenkou	drobenka	k1gFnSc7	drobenka
<g/>
,	,	kIx,	,
či	či	k8xC	či
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Pečou	péct	k5eAaImIp3nP	péct
se	se	k3xPyFc4	se
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
podávají	podávat	k5eAaImIp3nP	podávat
velké	velký	k2eAgInPc1d1	velký
kruhové	kruhový	k2eAgInPc1d1	kruhový
koláče	koláč	k1gInPc1	koláč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
mají	mít	k5eAaImIp3nP	mít
krajové	krajový	k2eAgInPc1d1	krajový
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
Valašsku	Valašsko	k1gNnSc6	Valašsko
se	se	k3xPyFc4	se
pečou	péct	k5eAaImIp3nP	péct
tzv.	tzv.	kA	tzv.
frgály	frgála	k1gFnSc2	frgála
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
centimetrů	centimetr	k1gInPc2	centimetr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
povidly	povidla	k1gNnPc7	povidla
z	z	k7c2	z
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
hrušek	hruška	k1gFnPc2	hruška
či	či	k8xC	či
švestek	švestka	k1gFnPc2	švestka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
a	a	k8xC	a
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
Chodsku	Chodsko	k1gNnSc6	Chodsko
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
koláče	koláč	k1gInPc1	koláč
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
průměr	průměr	k1gInSc4	průměr
a	a	k8xC	a
zdobí	zdobit	k5eAaImIp3nS	zdobit
se	se	k3xPyFc4	se
kontrastními	kontrastní	k2eAgInPc7d1	kontrastní
ornamenty	ornament	k1gInPc7	ornament
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
povidel	povidla	k1gNnPc2	povidla
<g/>
,	,	kIx,	,
máku	mák	k1gInSc2	mák
a	a	k8xC	a
tvarohu	tvaroh	k1gInSc2	tvaroh
<g/>
.	.	kIx.	.
</s>
<s>
Podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
nakrájené	nakrájený	k2eAgInPc4d1	nakrájený
na	na	k7c4	na
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
pizza	pizza	k1gFnSc1	pizza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
svatební	svatební	k2eAgInSc4d1	svatební
koláč	koláč	k1gInSc4	koláč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc1d2	veliký
a	a	k8xC	a
který	který	k3yRgInSc4	který
současně	současně	k6eAd1	současně
pojídají	pojídat	k5eAaImIp3nP	pojídat
oba	dva	k4xCgMnPc1	dva
novomanželé	novomanžel	k1gMnPc1	novomanžel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Linecký	linecký	k2eAgInSc1d1	linecký
koláč	koláč	k1gInSc1	koláč
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koláč	koláč	k1gInSc1	koláč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koláč	koláč	k1gInSc1	koláč
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
