<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
Štýrský	štýrský	k2eAgMnSc1d1	štýrský
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1578	[number]	k4	1578
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1637	[number]	k4	1637
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
v	v	k7c6	v
letech	let	k1gInPc6	let
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
rovněž	rovněž	k9	rovněž
vévoda	vévoda	k1gMnSc1	vévoda
štýrský	štýrský	k2eAgMnSc1d1	štýrský
<g/>
.	.	kIx.	.
</s>
