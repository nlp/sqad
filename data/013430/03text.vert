<p>
<s>
Misplaced	Misplaced	k1gInSc1	Misplaced
Childhood	Childhooda	k1gFnPc2	Childhooda
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
progresivní	progresivní	k2eAgFnSc2d1	progresivní
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
Marillion	Marillion	k1gInSc1	Marillion
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
jako	jako	k8xC	jako
úplně	úplně	k6eAd1	úplně
první	první	k4xOgNnSc4	první
koncepční	koncepční	k2eAgNnSc4d1	koncepční
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
proslavily	proslavit	k5eAaPmAgInP	proslavit
dva	dva	k4xCgInPc1	dva
megahitové	megahitový	k2eAgInPc1d1	megahitový
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Kayleigh	Kayleigh	k1gInSc1	Kayleigh
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lavender	Lavender	k1gInSc1	Lavender
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Remaster	Remaster	k1gInSc1	Remaster
alba	album	k1gNnSc2	album
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
2	[number]	k4	2
CD	CD	kA	CD
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
originální	originální	k2eAgFnSc1d1	originální
CD	CD	kA	CD
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Strana	strana	k1gFnSc1	strana
první	první	k4xOgFnSc1	první
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pseudo	Pseuda	k1gFnSc5	Pseuda
Silk	silk	k1gInSc1	silk
Kimono	kimono	k1gNnSc1	kimono
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kayleigh	Kayleigha	k1gFnPc2	Kayleigha
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lavender	Lavendero	k1gNnPc2	Lavendero
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bitter	Bittra	k1gFnPc2	Bittra
Suite	Suit	k1gInSc5	Suit
<g/>
"	"	kIx"	"
–	–	k?	–
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Heart	Heart	k1gInSc1	Heart
Of	Of	k1gMnSc1	Of
Lothian	Lothian	k1gMnSc1	Lothian
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Strana	strana	k1gFnSc1	strana
druhá	druhý	k4xOgFnSc1	druhý
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Waterhole	Waterhola	k1gFnSc6	Waterhola
(	(	kIx(	(
<g/>
Expresso	Expressa	k1gFnSc5	Expressa
Bongo	bongo	k1gNnSc4	bongo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lords	Lords	k1gInSc1	Lords
Of	Of	k1gMnSc2	Of
The	The	k1gMnSc2	The
Backstage	Backstag	k1gMnSc2	Backstag
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blind	Blind	k1gMnSc1	Blind
Curve	Curev	k1gFnSc2	Curev
<g/>
"	"	kIx"	"
–	–	k?	–
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Childhood	Childhooda	k1gFnPc2	Childhooda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Feather	Feathra	k1gFnPc2	Feathra
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
stop	stop	k2eAgInSc4d1	stop
(	(	kIx(	(
<g/>
remaster	remaster	k1gInSc4	remaster
2	[number]	k4	2
CD	CD	kA	CD
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
CD	CD	kA	CD
1	[number]	k4	1
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pseudo	Pseuda	k1gFnSc5	Pseuda
Silk	silk	k1gInSc1	silk
Kimono	kimono	k1gNnSc1	kimono
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kayleigh	Kayleigha	k1gFnPc2	Kayleigha
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lavender	Lavendero	k1gNnPc2	Lavendero
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bitter	Bittra	k1gFnPc2	Bittra
Suite	Suit	k1gInSc5	Suit
<g/>
"	"	kIx"	"
–	–	k?	–
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Heart	Heart	k1gInSc1	Heart
Of	Of	k1gMnSc1	Of
Lothian	Lothian	k1gMnSc1	Lothian
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Waterhole	Waterhola	k1gFnSc6	Waterhola
(	(	kIx(	(
<g/>
Expresso	Expressa	k1gFnSc5	Expressa
Bongo	bongo	k1gNnSc4	bongo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lords	Lords	k1gInSc1	Lords
Of	Of	k1gMnSc2	Of
The	The	k1gMnSc2	The
Backstage	Backstag	k1gMnSc2	Backstag
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
52	[number]	k4	52
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blind	Blind	k1gMnSc1	Blind
Curve	Curev	k1gFnSc2	Curev
<g/>
"	"	kIx"	"
–	–	k?	–
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Childhood	Childhooda	k1gFnPc2	Childhooda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
33	[number]	k4	33
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Feather	Feathra	k1gFnPc2	Feathra
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
===	===	k?	===
CD	CD	kA	CD
2	[number]	k4	2
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lady	lady	k1gFnSc1	lady
Nina	Nina	k1gFnSc1	Nina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
12	[number]	k4	12
<g/>
"	"	kIx"	"
Verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Freaks	Freaks	k1gInSc4	Freaks
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Singlová	singlový	k2eAgFnSc1d1	singlová
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kayleigh	Kayleigh	k1gInSc4	Kayleigh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Alternative	Alternativ	k1gInSc5	Alternativ
Mix	mix	k1gInSc1	mix
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lavender	Lavender	k1gInSc1	Lavender
Blue	Blu	k1gInSc2	Blu
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Lavender	Lavender	k1gInSc1	Lavender
Remix	Remix	k1gInSc1	Remix
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Heart	Heart	k1gInSc1	Heart
Of	Of	k1gMnSc1	Of
Lothian	Lothian	k1gMnSc1	Lothian
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
Mix	mix	k1gInSc1	mix
<g/>
)	)	kIx)	)
–	–	k?	–
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pseudo	Pseuda	k1gFnSc5	Pseuda
Silk	silk	k1gInSc1	silk
Kimono	kimono	k1gNnSc1	kimono
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kayleigh	Kayleigh	k1gInSc4	Kayleigh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lavender	Lavender	k1gInSc4	Lavender
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
37	[number]	k4	37
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bitter	Bittra	k1gFnPc2	Bittra
Suite	Suit	k1gInSc5	Suit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lords	Lords	k1gInSc1	Lords
Of	Of	k1gMnSc2	Of
The	The	k1gMnSc2	The
Backstage	Backstag	k1gMnSc2	Backstag
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blue	Blue	k1gFnSc1	Blue
Angel	angel	k1gMnSc1	angel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Misplaced	Misplaced	k1gMnSc1	Misplaced
Randezvous	Randezvous	k1gMnSc1	Randezvous
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Heart	Heart	k1gInSc1	Heart
Of	Of	k1gMnSc1	Of
Lothian	Lothian	k1gMnSc1	Lothian
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Waterhole	Waterhola	k1gFnSc6	Waterhola
(	(	kIx(	(
<g/>
Expresso	Expressa	k1gFnSc5	Expressa
Bongo	bongo	k1gNnSc4	bongo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Passing	Passing	k1gInSc1	Passing
Strangers	Strangers	k1gInSc1	Strangers
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Childhood	Childhooda	k1gFnPc2	Childhooda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Feather	Feathra	k1gFnPc2	Feathra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Demo	demo	k2eAgMnSc2d1	demo
<g/>
)	)	kIx)	)
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Fish	Fish	k1gInSc1	Fish
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Rothery	Rothera	k1gFnSc2	Rothera
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Kelly	Kella	k1gFnSc2	Kella
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Pete	Pete	k1gInSc1	Pete
Trewavas	Trewavas	k1gInSc1	Trewavas
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Ian	Ian	k?	Ian
Mosley	Moslea	k1gFnPc1	Moslea
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
</s>
</p>
