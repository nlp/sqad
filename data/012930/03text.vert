<p>
<s>
"	"	kIx"	"
<g/>
November	November	k1gMnSc1	November
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
americké	americký	k2eAgFnSc2d1	americká
skupiny	skupina	k1gFnSc2	skupina
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
zpěvák	zpěvák	k1gMnSc1	zpěvák
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
;	;	kIx,	;
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
Use	usus	k1gInSc5	usus
Your	Your	k1gInSc4	Your
Illusion	Illusion	k1gInSc1	Illusion
I.	I.	kA	I.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejžádanějších	žádaný	k2eAgInPc2d3	nejžádanější
na	na	k7c6	na
MTV	MTV	kA	MTV
a	a	k8xC	a
také	také	k6eAd1	také
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Music	k1gMnSc1	Music
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
13	[number]	k4	13
<g/>
.	.	kIx.	.
nejdražší	drahý	k2eAgInSc4d3	nejdražší
klip	klip	k1gInSc4	klip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
rockové	rockový	k2eAgFnPc4d1	rocková
balady	balada	k1gFnPc4	balada
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Child	Child	k1gInSc1	Child
in	in	k?	in
Time	Time	k1gInSc1	Time
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Stairway	Stairwaa	k1gMnSc2	Stairwaa
to	ten	k3xDgNnSc1	ten
Heaven	Heaven	k2eAgMnSc1d1	Heaven
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Free	Free	k1gNnSc1	Free
Bird	Birda	k1gFnPc2	Birda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnPc3	svůj
orchestálním	orchestální	k2eAgNnPc3d1	orchestální
aranžmá	aranžmá	k1gNnPc3	aranžmá
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
hraje	hrát	k5eAaImIp3nS	hrát
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
však	však	k9	však
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
živých	živý	k2eAgFnPc2d1	živá
vystoupeních	vystoupení	k1gNnPc6	vystoupení
chybělo	chybět	k5eAaImAgNnS	chybět
(	(	kIx(	(
<g/>
Axl	Axl	k1gMnSc4	Axl
používal	používat	k5eAaImAgMnS	používat
piano	piano	k6eAd1	piano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
časopisu	časopis	k1gInSc2	časopis
Guitar	Guitar	k1gMnSc1	Guitar
World	Worldo	k1gNnPc2	Worldo
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Slashovo	Slashův	k2eAgNnSc1d1	Slashův
sólo	sólo	k1gNnSc1	sólo
ze	z	k7c2	z
skladby	skladba	k1gFnSc2	skladba
šesté	šestý	k4xOgFnSc2	šestý
příčky	příčka	k1gFnSc2	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
nahrána	nahrát	k5eAaPmNgFnS	nahrát
kapelou	kapela	k1gFnSc7	kapela
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
písničky	písnička	k1gFnSc2	písnička
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
slyšet	slyšet	k5eAaImF	slyšet
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
sólech	sólo	k1gNnPc6	sólo
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
G	G	kA	G
N	N	kA	N
<g/>
'	'	kIx"	'
R	R	kA	R
-	-	kIx~	-
November	November	k1gMnSc1	November
Rain	Rain	k1gMnSc1	Rain
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
