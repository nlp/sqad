<s>
Elysejský	elysejský	k2eAgInSc1d1	elysejský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Palais	Palais	k1gInSc1	Palais
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Élysée	Élysée	k1gInSc1	Élysée
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc4	palác
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Avenue	avenue	k1gFnSc2	avenue
des	des	k1gNnSc2	des
Champs-Élysées	Champs-Élyséesa	k1gFnPc2	Champs-Élyséesa
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
