<s>
Elysejský	elysejský	k2eAgInSc1d1	elysejský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Palais	Palais	k1gInSc1	Palais
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Élysée	Élysée	k1gInSc1	Élysée
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc4	palác
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Avenue	avenue	k1gFnSc2	avenue
des	des	k1gNnSc2	des
Champs-Élysées	Champs-Élyséesa	k1gFnPc2	Champs-Élyséesa
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
rezidence	rezidence	k1gFnSc1	rezidence
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
křídle	křídlo	k1gNnSc6	křídlo
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
prezident	prezident	k1gMnSc1	prezident
François	François	k1gFnSc2	François
Hollande	Holland	k1gInSc5	Holland
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1718-1722	[number]	k4	1718-1722
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
architekta	architekt	k1gMnSc2	architekt
Armanda	Armanda	k1gFnSc1	Armanda
Clauda	Clauda	k1gFnSc1	Clauda
Molleta	Molleta	k1gFnSc1	Molleta
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
koupil	koupit	k5eAaPmAgMnS	koupit
pozemek	pozemek	k1gInSc4	pozemek
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Evreux	Evreux	k1gInSc1	Evreux
<g/>
,	,	kIx,	,
Henri-Louis	Henri-Louis	k1gFnSc1	Henri-Louis
de	de	k?	de
la	la	k1gNnSc1	la
Tour	Tour	k1gMnSc1	Tour
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Auvergne	Auvergn	k1gMnSc5	Auvergn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
získala	získat	k5eAaPmAgFnS	získat
palác	palác	k1gInSc4	palác
od	od	k7c2	od
krále	král	k1gMnSc2	král
darem	dar	k1gInSc7	dar
Jeanne-Antoinette	Jeanne-Antoinett	k1gInSc5	Jeanne-Antoinett
Poisson	Poisson	k1gNnSc1	Poisson
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
madame	madame	k1gFnSc1	madame
de	de	k?	de
Pompadour	Pompadoura	k1gFnPc2	Pompadoura
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
jeho	jeho	k3xOp3gInPc4	jeho
interiéry	interiér	k1gInPc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
zvětšena	zvětšen	k2eAgFnSc1d1	zvětšena
<g/>
,	,	kIx,	,
vybavena	vybaven	k2eAgFnSc1d1	vybavena
sloupovím	sloupoví	k1gNnSc7	sloupoví
a	a	k8xC	a
podloubím	podloubí	k1gNnSc7	podloubí
a	a	k8xC	a
labyrintem	labyrint	k1gInSc7	labyrint
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
Jeanne-Antoinette	Jeanne-Antoinett	k1gInSc5	Jeanne-Antoinett
Poisson	Poisson	k1gNnSc1	Poisson
již	již	k9	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
oficiální	oficiální	k2eAgFnSc2d1	oficiální
milenkou	milenka	k1gFnSc7	milenka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
svou	svůj	k3xOyFgFnSc4	svůj
rezidenci	rezidence	k1gFnSc4	rezidence
odkázala	odkázat	k5eAaPmAgFnS	odkázat
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Bourbonů	bourbon	k1gInPc2	bourbon
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgMnS	sloužit
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
vyslanců	vyslanec	k1gMnPc2	vyslanec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
výstavní	výstavní	k2eAgInSc4d1	výstavní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
král	král	k1gMnSc1	král
prodal	prodat	k5eAaPmAgMnS	prodat
palác	palác	k1gInSc4	palác
jednomu	jeden	k4xCgNnSc3	jeden
bankéři	bankéř	k1gMnPc1	bankéř
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
jej	on	k3xPp3gNnSc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
získal	získat	k5eAaPmAgInS	získat
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
přenechal	přenechat	k5eAaPmAgMnS	přenechat
ho	on	k3xPp3gNnSc4	on
své	své	k1gNnSc4	své
sestřenici	sestřenice	k1gFnSc3	sestřenice
<g/>
,	,	kIx,	,
Bathildě	Bathilda	k1gFnSc3	Bathilda
Orléanské	Orléanský	k2eAgFnSc2d1	Orléanská
<g/>
,	,	kIx,	,
vévodkyni	vévodkyně	k1gFnSc3	vévodkyně
Bourbonské	bourbonský	k2eAgFnPc1d1	Bourbonská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
byla	být	k5eAaImAgFnS	být
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1793	[number]	k4	1793
uvězněna	uvězněn	k2eAgFnSc1d1	uvězněna
a	a	k8xC	a
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
palác	palác	k1gInSc1	palác
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
sklad	sklad	k1gInSc4	sklad
nábytku	nábytek	k1gInSc2	nábytek
zabaveného	zabavený	k2eAgInSc2d1	zabavený
emigrantům	emigrant	k1gMnPc3	emigrant
a	a	k8xC	a
zatčeným	zatčený	k1gMnPc3	zatčený
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
získala	získat	k5eAaPmAgFnS	získat
palác	palác	k1gInSc4	palác
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
odešla	odejít	k5eAaPmAgFnS	odejít
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
palác	palác	k1gInSc1	palác
získal	získat	k5eAaPmAgInS	získat
Joachim	Joachim	k1gInSc4	Joachim
Murat	Murat	k2eAgMnSc1d1	Murat
<g/>
,	,	kIx,	,
švagr	švagr	k1gMnSc1	švagr
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
přestavět	přestavět	k5eAaPmF	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
plesový	plesový	k2eAgInSc1d1	plesový
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
prezidenta	prezident	k1gMnSc2	prezident
Georgese	Georgese	k1gFnSc2	Georgese
Pompidoua	Pompidoua	k1gFnSc1	Pompidoua
k	k	k7c3	k
zasedání	zasedání	k1gNnSc3	zasedání
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
králem	král	k1gMnSc7	král
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
palác	palác	k1gInSc1	palác
opět	opět	k6eAd1	opět
Napoleon	napoleon	k1gInSc1	napoleon
I.	I.	kA	I.
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Joséphine	Joséphin	k1gMnSc5	Joséphin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
byl	být	k5eAaImAgInS	být
palác	palác	k1gInSc1	palác
navrácen	navrátit	k5eAaPmNgInS	navrátit
Bourbonům	bourbon	k1gInPc3	bourbon
a	a	k8xC	a
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
jej	on	k3xPp3gInSc4	on
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
s	s	k7c7	s
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
za	za	k7c4	za
hôtel	hôtel	k1gInSc4	hôtel
Matignon	Matignona	k1gFnPc2	Matignona
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
předal	předat	k5eAaPmAgInS	předat
Elysejský	elysejský	k2eAgInSc1d1	elysejský
palác	palác	k1gInSc1	palác
svému	svůj	k1gMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
Karlu	Karel	k1gMnSc3	Karel
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
<g/>
,	,	kIx,	,
vévodovi	vévoda	k1gMnSc3	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Marii	Maria	k1gFnSc3	Maria
Karolíně	Karolína	k1gFnSc3	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vévoda	vévoda	k1gMnSc1	vévoda
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
palác	palác	k1gInSc4	palác
opět	opět	k6eAd1	opět
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
palác	palác	k1gInSc4	palác
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
za	za	k7c4	za
sídlo	sídlo	k1gNnSc4	sídlo
budoucího	budoucí	k2eAgMnSc2d1	budoucí
prezidenta	prezident	k1gMnSc2	prezident
Druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sídlil	sídlit	k5eAaImAgMnS	sídlit
zde	zde	k6eAd1	zde
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
její	její	k3xOp3gMnSc1	její
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Adolphe	Adolph	k1gFnSc2	Adolph
Thiers	Thiers	k1gInSc1	Thiers
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
ještě	ještě	k6eAd1	ještě
trvale	trvale	k6eAd1	trvale
nesídlil	sídlit	k5eNaImAgMnS	sídlit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c2	za
jeho	on	k3xPp3gMnSc2	on
nástupce	nástupce	k1gMnSc2	nástupce
Mac-Mahona	Mac-Mahon	k1gMnSc2	Mac-Mahon
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
sídlem	sídlo	k1gNnSc7	sídlo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
François	François	k1gFnSc1	François
Hollande	Holland	k1gInSc5	Holland
Seznam	seznam	k1gInSc4	seznam
prezidentů	prezident	k1gMnPc2	prezident
Francie	Francie	k1gFnSc2	Francie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elysejský	elysejský	k2eAgInSc4d1	elysejský
palác	palác	k1gInSc4	palác
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
francouzského	francouzský	k2eAgMnSc2d1	francouzský
prezidenta	prezident	k1gMnSc2	prezident
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Palác	palác	k1gInSc1	palác
na	na	k7c4	na
Structurae	Structurae	k1gFnSc4	Structurae
</s>
