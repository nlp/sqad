<s>
Kerry	Kerra	k1gMnSc2	Kerra
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
americké	americký	k2eAgFnSc2d1	americká
thrash	thrash	k1gInSc4	thrash
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Slayer	Slayra	k1gFnPc2	Slayra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
letecký	letecký	k2eAgMnSc1d1	letecký
inspektor	inspektor	k1gMnSc1	inspektor
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
pracovala	pracovat	k5eAaImAgFnS	pracovat
u	u	k7c2	u
telefonní	telefonní	k2eAgFnSc2d1	telefonní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Kerry	Kerra	k1gFnPc1	Kerra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
hledal	hledat	k5eAaImAgMnS	hledat
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
místo	místo	k6eAd1	místo
jako	jako	k8xC	jako
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Jeffem	Jeff	k1gMnSc7	Jeff
Hannemanem	Hanneman	k1gMnSc7	Hanneman
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
hrát	hrát	k5eAaImF	hrát
skladby	skladba	k1gFnPc4	skladba
od	od	k7c2	od
Iron	iron	k1gInSc4	iron
Maiden	Maidno	k1gNnPc2	Maidno
a	a	k8xC	a
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Kerry	Kerr	k1gMnPc4	Kerr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Why	Why	k1gMnSc1	Why
don	don	k1gMnSc1	don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
we	we	k?	we
start	start	k1gInSc1	start
our	our	k?	our
OWN	OWN	kA	OWN
band	band	k1gInSc1	band
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Fuck	Fuck	k1gMnSc1	Fuck
yeah	yeah	k1gMnSc1	yeah
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
metalových	metalový	k2eAgMnPc2d1	metalový
muzikantů	muzikant	k1gMnPc2	muzikant
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
Kerry	Kerra	k1gFnPc1	Kerra
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
začaly	začít	k5eAaPmAgFnP	začít
padat	padat	k5eAaImF	padat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ostříhal	ostříhat	k5eAaImAgMnS	ostříhat
do	do	k7c2	do
hola	hola	k1gFnSc1	hola
<g/>
;	;	kIx,	;
svou	svůj	k3xOyFgFnSc4	svůj
hlavu	hlava	k1gFnSc4	hlava
pak	pak	k6eAd1	pak
doplnil	doplnit	k5eAaPmAgMnS	doplnit
tetováním	tetování	k1gNnSc7	tetování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přezdívka	přezdívka	k1gFnSc1	přezdívka
zní	znět	k5eAaImIp3nS	znět
KFK	KFK	kA	KFK
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Kerry	Kerra	k1gFnPc1	Kerra
Fucking	Fucking	k1gInSc4	Fucking
King	King	k1gMnSc1	King
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zabývají	zabývat	k5eAaImIp3nP	zabývat
satanismem	satanismus	k1gInSc7	satanismus
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc7	jeho
zálibou	záliba	k1gFnSc7	záliba
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
horory	horor	k1gInPc1	horor
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kerry	Kerra	k1gFnSc2	Kerra
King	King	k1gInSc1	King
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
muzikus	muzikus	k1gMnSc1	muzikus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
