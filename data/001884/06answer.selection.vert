<s>
Většina	většina	k1gFnSc1	většina
bakterií	bakterie	k1gFnPc2	bakterie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jediný	jediný	k2eAgInSc1d1	jediný
nukleoid	nukleoid	k1gInSc1	nukleoid
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
chromozom	chromozom	k1gInSc1	chromozom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
