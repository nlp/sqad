<s>
Epica	Epica	k1gFnSc1	Epica
je	být	k5eAaImIp3nS	být
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
symfonicmetalová	symfonicmetalový	k2eAgFnSc1d1	symfonicmetalový
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
operní	operní	k2eAgInPc4d1	operní
prvky	prvek	k1gInPc4	prvek
kombinované	kombinovaný	k2eAgInPc4d1	kombinovaný
s	s	k7c7	s
typickým	typický	k2eAgInSc7d1	typický
death	death	k1gInSc4	death
metalovým	metalový	k2eAgInSc7d1	metalový
chrapotem	chrapot	k1gInSc7	chrapot
(	(	kIx(	(
<g/>
growling	growling	k1gInSc1	growling
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
je	být	k5eAaImIp3nS	být
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Mark	Mark	k1gMnSc1	Mark
Jansen	Jansen	k2eAgMnSc1d1	Jansen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kapelu	kapela	k1gFnSc4	kapela
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
After	Aftra	k1gFnPc2	Aftra
Forever	Forevra	k1gFnPc2	Forevra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
opustil	opustit	k5eAaPmAgMnS	opustit
Mark	Mark	k1gMnSc1	Mark
Jansen	Jansna	k1gFnPc2	Jansna
skupinu	skupina	k1gFnSc4	skupina
After	Aftra	k1gFnPc2	Aftra
Forever	Forevra	k1gFnPc2	Forevra
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
hledat	hledat	k5eAaImF	hledat
muzikanty	muzikant	k1gMnPc4	muzikant
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
nového	nový	k2eAgInSc2d1	nový
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Sahara	Sahara	k1gFnSc1	Sahara
Dust	Dust	k1gInSc1	Dust
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
počítala	počítat	k5eAaImAgFnS	počítat
kapela	kapela	k1gFnSc1	kapela
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Michaelsenovou	Michaelsenová	k1gFnSc7	Michaelsenová
(	(	kIx(	(
<g/>
z	z	k7c2	z
Trail	Traila	k1gFnPc2	Traila
of	of	k?	of
Tears	Tearsa	k1gFnPc2	Tearsa
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
svou	svůj	k3xOyFgFnSc7	svůj
frontwomankou	frontwomanka	k1gFnSc7	frontwomanka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
vystřídaná	vystřídaný	k2eAgNnPc1d1	vystřídané
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neznámou	neznámá	k1gFnSc7	neznámá
mezzo-sopranistkou	mezzoopranistka	k1gFnSc7	mezzo-sopranistka
Simonou	Simona	k1gFnSc7	Simona
Simonsovou	Simonsová	k1gFnSc7	Simonsová
<g/>
,	,	kIx,	,
Jansenovou	Jansenový	k2eAgFnSc7d1	Jansenový
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
Kapelu	kapela	k1gFnSc4	kapela
doplnili	doplnit	k5eAaPmAgMnP	doplnit
kytarista	kytarista	k1gMnSc1	kytarista
Ad	ad	k7c4	ad
Sluijter	Sluijter	k1gInSc4	Sluijter
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Jeroen	Jerona	k1gFnPc2	Jerona
Simons	Simons	k1gInSc1	Simons
<g/>
,	,	kIx,	,
basista	basista	k1gMnSc1	basista
Yves	Yvesa	k1gFnPc2	Yvesa
Huts	Huts	k1gInSc1	Huts
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Coen	Coen	k1gMnSc1	Coen
Janssen	Janssno	k1gNnPc2	Janssno
<g/>
.	.	kIx.	.
</s>
<s>
Změnili	změnit	k5eAaPmAgMnP	změnit
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
Epica	Epica	k1gMnSc1	Epica
<g/>
,	,	kIx,	,
inspirováni	inspirován	k2eAgMnPc1d1	inspirován
albem	album	k1gNnSc7	album
Epica	Epica	k1gMnSc1	Epica
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
formace	formace	k1gFnSc2	formace
Kamelot	kamelot	k1gMnSc1	kamelot
<g/>
.	.	kIx.	.
</s>
<s>
Epica	Epica	k1gFnSc1	Epica
poté	poté	k6eAd1	poté
sestavila	sestavit	k5eAaPmAgFnS	sestavit
sbor	sbor	k1gInSc4	sbor
-	-	kIx~	-
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
šesti	šest	k4xCc2	šest
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
šesti	šest	k4xCc2	šest
žen	žena	k1gFnPc2	žena
-	-	kIx~	-
a	a	k8xC	a
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
orchestr	orchestr	k1gInSc1	orchestr
-	-	kIx~	-
troje	troje	k4xRgFnPc1	troje
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
violy	viola	k1gFnPc1	viola
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
violoncella	violoncello	k1gNnPc4	violoncello
a	a	k8xC	a
kontrabas	kontrabas	k1gInSc4	kontrabas
-	-	kIx~	-
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
jejich	jejich	k3xOp3gNnSc2	jejich
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Phantom	Phantom	k1gInSc1	Phantom
Agony	agon	k1gInPc1	agon
byl	být	k5eAaImAgMnS	být
Sascha	Sascha	k1gMnSc1	Sascha
Paeth	Paeth	k1gMnSc1	Paeth
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
producentem	producent	k1gMnSc7	producent
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
Angra	Angro	k1gNnSc2	Angro
<g/>
,	,	kIx,	,
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
<g/>
,	,	kIx,	,
Kamelot	kamelot	k1gInSc1	kamelot
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaBmNgNnS	nazvat
Consign	Consign	k1gNnSc1	Consign
to	ten	k3xDgNnSc4	ten
Oblivion	Oblivion	k1gInSc1	Oblivion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
songu	song	k1gInSc6	song
"	"	kIx"	"
<g/>
Trois	Trois	k1gFnSc1	Trois
Viegres	Viegresa	k1gFnPc2	Viegresa
<g/>
"	"	kIx"	"
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
hosta	host	k1gMnSc4	host
-	-	kIx~	-
Roye	Roy	k1gMnSc4	Roy
Khana	Khan	k1gMnSc4	Khan
(	(	kIx(	(
<g/>
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Kamelot	kamelot	k1gInSc1	kamelot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Epica	Epica	k6eAd1	Epica
se	se	k3xPyFc4	se
také	také	k9	také
přidala	přidat	k5eAaPmAgFnS	přidat
ke	k	k7c3	k
Kamelotu	kamelot	k1gInSc2	kamelot
jako	jako	k8xC	jako
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
turné	turné	k1gNnSc4	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
The	The	k1gFnSc2	The
Black	Blacka	k1gFnPc2	Blacka
Halo	halo	k1gNnSc1	halo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c4	v
Rockefeller	Rockefeller	k1gInSc4	Rockefeller
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
živém	živé	k1gNnSc6	živé
DVD	DVD	kA	DVD
Kamelotu	kamelot	k1gInSc2	kamelot
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vydáno	vydat	k5eAaPmNgNnS	vydat
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Score	Scor	k1gMnSc5	Scor
–	–	k?	–
An	An	k1gFnSc1	An
Epic	Epic	k1gFnSc4	Epic
Journey	Journea	k1gFnSc2	Journea
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
holandskému	holandský	k2eAgInSc3d1	holandský
filmu	film	k1gInSc3	film
Joyride	Joyrid	k1gInSc5	Joyrid
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k9	jako
jejich	jejich	k3xOp3gNnSc1	jejich
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
řekl	říct	k5eAaPmAgMnS	říct
kytarista	kytarista	k1gMnSc1	kytarista
Mark	Mark	k1gMnSc1	Mark
Jansen	Jansen	k2eAgMnSc1d1	Jansen
-	-	kIx~	-
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
typická	typický	k2eAgFnSc1d1	typická
Epica	Epica	k1gFnSc1	Epica
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
beze	beze	k7c2	beze
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
bassovky	bassovka	k1gFnSc2	bassovka
a	a	k8xC	a
bubnů	buben	k1gInPc2	buben
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
jela	jet	k5eAaImAgFnS	jet
Epica	Epica	k1gFnSc1	Epica
společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Kamelot	kamelot	k1gInSc4	kamelot
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgInSc4	první
tour	tour	k1gInSc4	tour
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tour	toura	k1gFnPc2	toura
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
její	její	k3xOp3gMnSc1	její
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
Jeroen	Jeroen	k2eAgInSc1d1	Jeroen
Simons	Simons	k1gInSc1	Simons
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
jako	jako	k8xS	jako
nového	nový	k2eAgMnSc4d1	nový
bubeníka	bubeník	k1gMnSc4	bubeník
Ariëna	Ariën	k1gInSc2	Ariën
van	van	k1gInSc1	van
Weesenbeeka	Weesenbeeek	k1gInSc2	Weesenbeeek
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
natočili	natočit	k5eAaBmAgMnP	natočit
album	album	k1gNnSc4	album
Divine	Divin	k1gInSc5	Divin
Conspiracy	Conspiracum	k1gNnPc7	Conspiracum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sklidilo	sklidit	k5eAaPmAgNnS	sklidit
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
Epica	Epica	k1gFnSc1	Epica
mohla	moct	k5eAaImAgFnS	moct
rozjet	rozjet	k5eAaPmF	rozjet
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měli	mít	k5eAaImAgMnP	mít
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
Miskolcu	Miskolcum	k1gNnSc6	Miskolcum
na	na	k7c6	na
Opera	opera	k1gFnSc1	opera
Rock	rock	k1gInSc1	rock
Show	show	k1gFnSc1	show
<g/>
,	,	kIx,	,
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
40	[number]	k4	40
<g/>
členného	členný	k2eAgInSc2d1	členný
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
30	[number]	k4	30
<g/>
členného	členný	k2eAgInSc2d1	členný
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
natočil	natočit	k5eAaBmAgInS	natočit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
CD	CD	kA	CD
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
Classical	Classical	k1gMnSc2	Classical
Conspiracy	Conspiraca	k1gMnSc2	Conspiraca
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Design	design	k1gInSc1	design
Your	Youra	k1gFnPc2	Youra
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
než	než	k8xS	než
Divine	Divin	k1gInSc5	Divin
Conspiracy	Conspiraca	k1gFnPc1	Conspiraca
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pocit	pocit	k1gInSc1	pocit
"	"	kIx"	"
<g/>
epičnosti	epičnost	k1gFnPc1	epičnost
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yQnSc1	což
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
jménu	jméno	k1gNnSc3	jméno
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
studiové	studiový	k2eAgNnSc1d1	studiové
albu	album	k1gNnSc3	album
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
"	"	kIx"	"
<g/>
Requiem	Requium	k1gNnSc7	Requium
for	forum	k1gNnPc2	forum
the	the	k?	the
Indifferent	Indifferent	k1gMnSc1	Indifferent
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
Epica	Epica	k1gFnSc1	Epica
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
různými	různý	k2eAgNnPc7d1	různé
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
,	,	kIx,	,
kulturami	kultura	k1gFnPc7	kultura
<g/>
,	,	kIx,	,
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
přírodními	přírodní	k2eAgFnPc7d1	přírodní
katastrofami	katastrofa	k1gFnPc7	katastrofa
a	a	k8xC	a
finanční	finanční	k2eAgFnSc7d1	finanční
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
a	a	k8xC	a
díky	dík	k1gInPc1	dík
svojí	svůj	k3xOyFgFnSc2	svůj
vyváženosti	vyváženost	k1gFnSc2	vyváženost
začalo	začít	k5eAaPmAgNnS	začít
získávát	získávát	k5eAaPmF	získávát
jedno	jeden	k4xCgNnSc1	jeden
vysoké	vysoký	k2eAgNnSc1d1	vysoké
hodnocení	hodnocení	k1gNnSc1	hodnocení
za	za	k7c7	za
druhým	druhý	k4xOgInSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ubylo	ubýt	k5eAaPmAgNnS	ubýt
řevů	řev	k1gInPc2	řev
<g/>
,	,	kIx,	,
gruntů	grunt	k1gInPc2	grunt
a	a	k8xC	a
více	hodně	k6eAd2	hodně
prostoru	prostora	k1gFnSc4	prostora
dostal	dostat	k5eAaPmAgInS	dostat
hlas	hlas	k1gInSc1	hlas
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Simone	Simon	k1gMnSc5	Simon
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
víc	hodně	k6eAd2	hodně
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
a	a	k8xC	a
progresivních	progresivní	k2eAgInPc2d1	progresivní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
novou	nova	k1gFnSc7	nova
turné	turné	k1gNnSc2	turné
Epica	Epic	k1gInSc2	Epic
opět	opět	k6eAd1	opět
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Basák	Basák	k1gInSc1	Basák
Yves	Yves	k1gInSc1	Yves
Huts	Huts	k1gInSc1	Huts
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
kariérním	kariérní	k2eAgFnPc3d1	kariérní
možnostem	možnost	k1gFnPc3	možnost
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
oblasti	oblast	k1gFnSc6	oblast
opustil	opustit	k5eAaPmAgMnS	opustit
Epicu	Epica	k1gFnSc4	Epica
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
ho	on	k3xPp3gMnSc4	on
Rob	roba	k1gFnPc2	roba
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Loo	Loo	k1gFnSc2	Loo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
Delain	Delain	k1gInSc1	Delain
a	a	k8xC	a
Sun	Sun	kA	Sun
Caged	Caged	k1gInSc1	Caged
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
skupina	skupina	k1gFnSc1	skupina
koncert	koncert	k1gInSc1	koncert
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
10	[number]	k4	10
let	léto	k1gNnPc2	léto
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
o	o	k7c4	o
několik	několik	k4yIc4	několik
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
například	například	k6eAd1	například
Floor	Floor	k1gInSc1	Floor
Jansen	Jansen	k1gInSc1	Jansen
(	(	kIx(	(
<g/>
ex-After	ex-After	k1gMnSc1	ex-After
forever	forever	k1gMnSc1	forever
<g/>
,	,	kIx,	,
ReVamp	ReVamp	k1gMnSc1	ReVamp
<g/>
,	,	kIx,	,
Nightwish	Nightwish	k1gMnSc1	Nightwish
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
-	-	kIx~	-
Ad	ad	k7c4	ad
Sluijter	Sluijter	k1gInSc4	Sluijter
<g/>
,	,	kIx,	,
Yves	Yves	k1gInSc4	Yves
Huts	Hutsa	k1gFnPc2	Hutsa
a	a	k8xC	a
Jeroen	Jeroen	k2eAgInSc4d1	Jeroen
Simons	Simons	k1gInSc4	Simons
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jako	jako	k9	jako
2DVD	[number]	k4	2DVD
a	a	k8xC	a
3CD	[number]	k4	3CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Retrospect	Retrospecta	k1gFnPc2	Retrospecta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
oznámila	oznámit	k5eAaPmAgFnS	oznámit
skupina	skupina	k1gFnSc1	skupina
vydání	vydání	k1gNnSc1	vydání
šestého	šestý	k4xOgNnSc2	šestý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Quantum	Quantum	k1gNnSc4	Quantum
Enigma	enigma	k1gNnSc4	enigma
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
přes	přes	k7c4	přes
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
kapela	kapela	k1gFnSc1	kapela
početně	početně	k6eAd1	početně
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
hudebních	hudební	k2eAgInPc6d1	hudební
festivalech	festival	k1gInPc6	festival
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
poté	poté	k6eAd1	poté
i	i	k8xC	i
samostatně	samostatně	k6eAd1	samostatně
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2015	[number]	k4	2015
Epica	Epic	k1gInSc2	Epic
zahájila	zahájit	k5eAaPmAgFnS	zahájit
druhou	druhý	k4xOgFnSc7	druhý
částí	část	k1gFnSc7	část
evropského	evropský	k2eAgNnSc2d1	Evropské
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgMnSc2	který
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
sešli	sejít	k5eAaPmAgMnP	sejít
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
sedmé	sedmý	k4xOgNnSc4	sedmý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
The	The	k1gMnSc2	The
Holographic	Holographice	k1gFnPc2	Holographice
Principle	Principle	k1gFnSc2	Principle
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
využila	využít	k5eAaPmAgFnS	využít
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Epica	Epica	k6eAd1	Epica
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
nahrála	nahrát	k5eAaBmAgFnS	nahrát
celkem	celkem	k6eAd1	celkem
osmnáct	osmnáct	k4xCc4	osmnáct
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
niž	jenž	k3xRgFnSc4	jenž
jich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
album	album	k1gNnSc4	album
vybrala	vybrat	k5eAaPmAgFnS	vybrat
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgFnPc2d1	zbylá
šest	šest	k4xCc1	šest
skladeb	skladba	k1gFnPc2	skladba
vydala	vydat	k5eAaPmAgFnS	vydat
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
extended	extended	k1gInSc4	extended
playe	playe	k1gFnSc4	playe
The	The	k1gFnSc2	The
Solace	Solace	k1gFnSc2	Solace
System	Syst	k1gInSc7	Syst
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
Simons	Simons	k1gInSc1	Simons
–	–	k?	–
mezzo-sopránový	mezzoopránový	k2eAgInSc1d1	mezzo-sopránový
zpěv	zpěv	k1gInSc1	zpěv
Mark	Mark	k1gMnSc1	Mark
Jansen	Jansen	k1gInSc1	Jansen
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
chrapot	chrapot	k1gInSc1	chrapot
(	(	kIx(	(
<g/>
growling	growling	k1gInSc1	growling
<g/>
,	,	kIx,	,
screaming	screaming	k1gInSc1	screaming
<g/>
)	)	kIx)	)
Isaac	Isaac	k1gInSc1	Isaac
Delahaye	Delahay	k1gFnSc2	Delahay
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
Rob	roba	k1gFnPc2	roba
Van	vana	k1gFnPc2	vana
Der	drát	k5eAaImRp2nS	drát
Loo	Loo	k1gFnSc2	Loo
–	–	k?	–
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Coen	Coena	k1gFnPc2	Coena
Janssen	Janssen	k2eAgInSc1d1	Janssen
–	–	k?	–
syntetizér	syntetizér	k1gInSc1	syntetizér
Ariën	Ariën	k1gMnSc1	Ariën
van	vana	k1gFnPc2	vana
Weesenbeek	Weesenbeek	k1gMnSc1	Weesenbeek
–	–	k?	–
bicí	bicí	k2eAgInSc4d1	bicí
Ad	ad	k7c4	ad
Sluijter	Sluijter	k1gInSc4	Sluijter
Helena	Helena	k1gFnSc1	Helena
Michaelsen	Michaelsna	k1gFnPc2	Michaelsna
Jeroen	Jeroen	k2eAgInSc1d1	Jeroen
Simons	Simons	k1gInSc1	Simons
Yves	Yvesa	k1gFnPc2	Yvesa
Huts	Huts	k1gInSc4	Huts
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Epicy	Epica	k1gFnSc2	Epica
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Phantom	Phantom	k1gInSc1	Phantom
Agony	agon	k1gInPc1	agon
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Consign	Consign	k1gMnSc1	Consign
to	ten	k3xDgNnSc4	ten
Oblivion	Oblivion	k1gInSc1	Oblivion
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
The	The	k1gMnSc5	The
Divine	Divin	k1gMnSc5	Divin
Conspiracy	Conspiraca	k1gMnSc2	Conspiraca
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Design	design	k1gInSc1	design
Your	Your	k1gInSc1	Your
Universe	Universe	k1gFnSc1	Universe
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Requiem	Requium	k1gNnSc7	Requium
for	forum	k1gNnPc2	forum
the	the	k?	the
Indifferent	Indifferent	k1gMnSc1	Indifferent
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Quantum	Quantum	k1gNnSc4	Quantum
Enigma	enigma	k1gNnSc1	enigma
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Holographic	Holographic	k1gMnSc1	Holographic
Principle	Principle	k1gMnSc1	Principle
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
