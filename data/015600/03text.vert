<s>
Mahmut	Mahmut	k1gMnSc1
Fadilpašić	Fadilpašić	k1gMnSc1
</s>
<s>
Mahmut	Mahmut	k1gMnSc1
Fadilpašić	Fadilpašić	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1853	#num#	k4
Sarajevo	Sarajevo	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
Sarajevo	Sarajevo	k1gNnSc4
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mahmut-beg	Mahmut-beg	k1gMnSc1
Asaf	Asaf	k1gMnSc1
(	(	kIx(
<g/>
event.	event.	k?
Mahmud-beg	Mahmud-beg	k1gMnSc1
<g/>
)	)	kIx)
Fadilpašić	Fadilpašić	k1gMnSc1
(	(	kIx(
<g/>
1853	#num#	k4
<g/>
/	/	kIx~
<g/>
1854	#num#	k4
Sarajevo	Sarajevo	k1gNnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
Sarajevo	Sarajevo	k1gNnSc4
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
bosenskohercegoský	bosenskohercegoský	k1gMnSc1
statkář	statkář	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
bosňáckého	bosňácký	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobových	dobový	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
je	být	k5eAaImIp3nS
příjmení	příjmení	k1gNnSc1
uváděno	uvádět	k5eAaImNgNnS
i	i	k8xC
jako	jako	k9
Fadil	Fadil	k1gMnSc1
Pašić	Pašić	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
do	do	k7c2
vlivné	vlivný	k2eAgFnSc2d1
a	a	k8xC
zámožné	zámožný	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
osmanského	osmanský	k2eAgMnSc4d1
úředníka	úředník	k1gMnSc4
a	a	k8xC
velkostatkáře	velkostatkář	k1gMnSc4
Fadil-paši	Fadil-paše	k1gFnSc4
Šerifoviće	Šerifoviće	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fadil-paša	Fadil-paš	k1gInSc2
se	se	k3xPyFc4
roku	rok	k1gInSc2
1878	#num#	k4
přišel	přijít	k5eAaPmAgMnS
poklonit	poklonit	k5eAaPmF
přicházejícím	přicházející	k2eAgFnPc3d1
rakousko-uherským	rakousko-uherský	k2eAgFnPc3d1
okupačním	okupační	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
kvůli	kvůli	k7c3
nevybíravému	vybíravý	k2eNgNnSc3d1
jednání	jednání	k1gNnSc3
jejich	jejich	k3xOp3gMnSc2
velitele	velitel	k1gMnSc2
Josipa	Josip	k1gMnSc2
Filipoviće	Filipović	k1gMnSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
emigrovat	emigrovat	k5eAaBmF
do	do	k7c2
osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1882	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Cařihradu	Cařihrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dlouhá	dlouhý	k2eAgNnPc4d1
desetiletí	desetiletí	k1gNnPc4
Mahmut-beg	Mahmut-beg	k1gMnSc1
působil	působit	k5eAaImAgMnS
v	v	k7c6
sarajevském	sarajevský	k2eAgNnSc6d1
zastupitelstvu	zastupitelstvo	k1gNnSc6
(	(	kIx(
<g/>
roku	rok	k1gInSc2
1887	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Zemskou	zemský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
poté	poté	k6eAd1
získal	získat	k5eAaPmAgMnS
mandát	mandát	k1gInSc4
na	na	k7c6
základě	základ	k1gInSc6
voleb	volba	k1gFnPc2
v	v	k7c6
letech	léto	k1gNnPc6
1890	#num#	k4
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
,	,	kIx,
1896	#num#	k4
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
,	,	kIx,
1902	#num#	k4
<g/>
,	,	kIx,
1905	#num#	k4
a	a	k8xC
1910	#num#	k4
<g/>
,	,	kIx,
1908	#num#	k4
nebyl	být	k5eNaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politicky	politicky	k6eAd1
činní	činný	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
i	i	k9
jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
<g/>
:	:	kIx,
Mastafa-beg	Mastafa-beg	k1gMnSc1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
/	/	kIx~
<g/>
1836	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
sarajevským	sarajevský	k2eAgMnSc7d1
starostou	starosta	k1gMnSc7
(	(	kIx(
<g/>
1878	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Omer-beg	Omer-beg	k1gMnSc1
(	(	kIx(
<g/>
1838	#num#	k4
<g/>
/	/	kIx~
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
sarajevským	sarajevský	k2eAgMnSc7d1
zastupitelem	zastupitel	k1gMnSc7
(	(	kIx(
<g/>
po	po	k7c6
volbách	volba	k1gFnPc6
1884	#num#	k4
a	a	k8xC
1887	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
činným	činný	k2eAgMnSc7d1
podnikatelem	podnikatel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
na	na	k7c6
vzniku	vznik	k1gInSc6
Islámské	islámský	k2eAgFnSc2d1
akciové	akciový	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
(	(	kIx(
<g/>
Islamska	Islamska	k1gFnSc1
dionička	dionička	k1gFnSc1
štamparija	štamparija	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jistý	jistý	k2eAgInSc4d1
čas	čas	k1gInSc4
byl	být	k5eAaImAgMnS
jejím	její	k3xOp3gMnSc7
ředitelem	ředitel	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
Muslimské	muslimský	k2eAgFnSc2d1
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
(	(	kIx(
<g/>
Muslimanska	Muslimansko	k1gNnSc2
centralna	centralno	k1gNnSc2
banka	banka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
boje	boj	k1gInSc2
muslimů	muslim	k1gMnPc2
o	o	k7c4
náboženskou	náboženský	k2eAgFnSc4d1
a	a	k8xC
školskou	školský	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1906	#num#	k4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
vzniku	vznik	k1gInSc6
Muslimské	muslimský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
a	a	k8xC
působil	působit	k5eAaImAgMnS
v	v	k7c6
jejím	její	k3xOp3gInSc6
výkonném	výkonný	k2eAgInSc6d1
výboru	výbor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1910	#num#	k4
za	za	k7c4
stranu	strana	k1gFnSc4
kandidoval	kandidovat	k5eAaImAgMnS
v	v	k7c6
prvních	první	k4xOgFnPc6
volbách	volba	k1gFnPc6
do	do	k7c2
bosenského	bosenský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
,	,	kIx,
saboru	sabor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandát	mandát	k1gInSc1
získal	získat	k5eAaPmAgInS
ve	v	k7c6
třetí	třetí	k4xOgFnSc3
<g/>
,	,	kIx,
venkovské	venkovský	k2eAgFnSc3d1
kurii	kurie	k1gFnSc3
za	za	k7c4
Sarajevský	sarajevský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslancem	poslanec	k1gMnSc7
zůstal	zůstat	k5eAaPmAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fadilpašić	Fadilpašić	k?
od	od	k7c2
osmanského	osmanský	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
obdržel	obdržet	k5eAaPmAgInS
Řád	řád	k1gInSc1
Medžidije	Medžidije	k1gFnSc2
(	(	kIx(
<g/>
Mecidiye	Mecidiye	k1gFnSc1
Nişanı	Nişanı	k1gMnSc1
<g/>
)	)	kIx)
IV	IV	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mahmut-beg	Mahmut-beg	k1gMnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1886	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
Rasemou	Rasemý	k2eAgFnSc7d1
Uzunić	Uzunić	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
bohatého	bohatý	k2eAgMnSc2d1
sarajevského	sarajevský	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
Asima	Asim	k1gMnSc2
Uzuniće	Uzunić	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
svazku	svazek	k1gInSc6
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
a	a	k8xC
dcera	dcera	k1gFnSc1
(	(	kIx(
<g/>
vdala	vdát	k5eAaPmAgFnS
se	se	k3xPyFc4
za	za	k7c4
Bećir-bega	Bećir-beg	k1gMnSc4
Gradaščeviće	Gradaščević	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syn	syn	k1gMnSc1
Salahudin	Salahudin	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1933	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
dcerou	dcera	k1gFnSc7
Halid-bega	Halid-beg	k1gMnSc2
Hrasnici	Hrasnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
v	v	k7c6
Zemské	zemský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
převezen	převézt	k5eAaPmNgMnS
do	do	k7c2
Gazi	Gaz	k1gFnSc2
Husrev-begovy	Husrev-begův	k2eAgFnSc2d1
mešity	mešita	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
zádušní	zádušní	k2eAgFnSc1d1
mše	mše	k1gFnSc1
a	a	k8xC
uložení	uložení	k1gNnSc1
ostatků	ostatek	k1gInPc2
v	v	k7c6
blízkém	blízký	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KAMBEROVIĆ	KAMBEROVIĆ	kA
<g/>
,	,	kIx,
Husnija	Husnijum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemljišni	Zemljišen	k2eAgMnPc1d1
posjedi	posjed	k1gMnPc1
obitelji	obitelje	k1gFnSc4
Fadilpašić	Fadilpašić	k1gFnSc3
–	–	k?
Prilog	Prilog	k1gInSc1
historiji	historít	k5eAaPmIp1nS
bosanskih	bosanskih	k1gInSc4
begova	begův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radovi	Rada	k1gMnSc3
<g/>
:	:	kIx,
Radovi	Rada	k1gMnSc3
Zavoda	Zavoda	k1gFnSc1
za	za	k7c2
hrvatsku	hrvatsek	k1gInSc2
povijest	povijest	k5eAaPmF
Filozofskoga	Filozofskog	k1gMnSc2
fakulteta	fakultet	k1gMnSc2
Sveučilišta	Sveučilišt	k1gMnSc2
u	u	k7c2
Zagrebu	Zagreb	k1gInSc2
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
175	#num#	k4
<g/>
–	–	k?
<g/>
183	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mahmudbeg	Mahmudbeg	k1gMnSc1
Fadilpašić	Fadilpašić	k1gMnSc1
umro	umro	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sarajevski	Sarajevski	k1gNnSc1
list	lista	k1gFnPc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXXV	XXXV	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
226	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Gjenaza	Gjenaza	k1gFnSc1
Mahmudbega	Mahmudbega	k1gFnSc1
Fadilpašića	Fadilpašića	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sarajevski	Sarajevski	k1gNnSc1
list	lista	k1gFnPc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXXV	XXXV	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
227	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
