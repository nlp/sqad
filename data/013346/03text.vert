<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
absolutno	absolutno	k1gNnSc4	absolutno
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
antiutopistický	antiutopistický	k2eAgInSc1d1	antiutopistický
román	román	k1gInSc1	román
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
předmluvu	předmluva	k1gFnSc4	předmluva
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
však	však	k9	však
dílo	dílo	k1gNnSc1	dílo
vycházelo	vycházet	k5eAaImAgNnS	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
fejetonů	fejeton	k1gInPc2	fejeton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
líčí	líčit	k5eAaImIp3nS	líčit
události	událost	k1gFnPc4	událost
kolem	kolem	k7c2	kolem
vynálezu	vynález	k1gInSc2	vynález
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
karburátoru	karburátor	k1gInSc2	karburátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
spalováním	spalování	k1gNnSc7	spalování
hmoty	hmota	k1gFnSc2	hmota
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
nejen	nejen	k6eAd1	nejen
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tajemné	tajemný	k2eAgNnSc1d1	tajemné
všeovládající	všeovládající	k2eAgNnSc1d1	všeovládající
absolutno	absolutno	k1gNnSc1	absolutno
–	–	k?	–
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
mělo	mít	k5eAaImAgNnS	mít
mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
autorovi	autorův	k2eAgMnPc1d1	autorův
vyčítali	vyčítat	k5eAaImAgMnP	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
román	román	k1gInSc4	román
se	s	k7c7	s
souvislým	souvislý	k2eAgInSc7d1	souvislý
dějem	děj	k1gInSc7	děj
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kapitoly	kapitola	k1gFnPc1	kapitola
jsou	být	k5eAaImIp3nP	být
viditelně	viditelně	k6eAd1	viditelně
protahované	protahovaný	k2eAgFnPc1d1	protahovaná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c4	na
gradaci	gradace	k1gFnSc4	gradace
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
ke	k	k7c3	k
druhému	druhý	k4xOgNnSc3	druhý
vydání	vydání	k1gNnSc3	vydání
Čapek	Čapek	k1gMnSc1	Čapek
kritiku	kritika	k1gFnSc4	kritika
přijímá	přijímat	k5eAaImIp3nS	přijímat
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
napsat	napsat	k5eAaBmF	napsat
fejeton	fejeton	k1gInSc4	fejeton
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
na	na	k7c4	na
šest	šest	k4xCc4	šest
fejetonů	fejeton	k1gInPc2	fejeton
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
odevzdání	odevzdání	k1gNnSc6	odevzdání
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
otištění	otištění	k1gNnSc4	otištění
postupně	postupně	k6eAd1	postupně
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
kapitoly	kapitola	k1gFnPc4	kapitola
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInPc4d1	vycházející
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Čapek	Čapek	k1gMnSc1	Čapek
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fejetonový	fejetonový	k2eAgInSc1d1	fejetonový
seriál	seriál	k1gInSc1	seriál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Čapkovo	Čapkův	k2eAgNnSc4d1	Čapkovo
dílo	dílo	k1gNnSc4	dílo
napsal	napsat	k5eAaBmAgMnS	napsat
Jiří	Jiří	k1gMnSc1	Jiří
Haussmann	Haussmann	k1gMnSc1	Haussmann
román	román	k1gInSc4	román
Velkovýroba	velkovýroba	k1gFnSc1	velkovýroba
ctnosti	ctnost	k1gFnPc1	ctnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Inženýr	inženýr	k1gMnSc1	inženýr
Marek	Marek	k1gMnSc1	Marek
vynalezne	vynaleznout	k5eAaPmIp3nS	vynaleznout
zázračný	zázračný	k2eAgInSc4d1	zázračný
stroj	stroj	k1gInSc4	stroj
"	"	kIx"	"
<g/>
Karburátor	karburátor	k1gInSc1	karburátor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
rozkládáním	rozkládání	k1gNnSc7	rozkládání
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
Absolutna	absolutno	k1gNnSc2	absolutno
<g/>
,	,	kIx,	,
esence	esence	k1gFnSc2	esence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
vlivem	vlivem	k7c2	vlivem
účinku	účinek	k1gInSc2	účinek
této	tento	k3xDgFnSc2	tento
božské	božský	k2eAgFnSc2d1	božská
síly	síla	k1gFnSc2	síla
obracejí	obracet	k5eAaImIp3nP	obracet
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
konají	konat	k5eAaImIp3nP	konat
zázraky	zázrak	k1gInPc1	zázrak
<g/>
,	,	kIx,	,
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
ušlechtile	ušlechtile	k6eAd1	ušlechtile
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
šťastní	šťastný	k2eAgMnPc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stroj	stroj	k1gInSc1	stroj
začne	začít	k5eAaPmIp3nS	začít
šířit	šířit	k5eAaImF	šířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nastává	nastávat	k5eAaImIp3nS	nastávat
ohromný	ohromný	k2eAgInSc4d1	ohromný
nárůst	nárůst	k1gInSc4	nárůst
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
ale	ale	k9	ale
vše	všechen	k3xTgNnSc1	všechen
obrátí	obrátit	k5eAaPmIp3nS	obrátit
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nadbytku	nadbytek	k1gInSc3	nadbytek
výrobků	výrobek	k1gInPc2	výrobek
se	se	k3xPyFc4	se
hroutí	hroutit	k5eAaImIp3nS	hroutit
světový	světový	k2eAgInSc1d1	světový
trh	trh	k1gInSc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
lidí	člověk	k1gMnPc2	člověk
zarputile	zarputile	k6eAd1	zarputile
prosazovat	prosazovat	k5eAaImF	prosazovat
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
skončí	skončit	k5eAaPmIp3nP	skončit
krvavými	krvavý	k2eAgFnPc7d1	krvavá
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
válkami	válka	k1gFnPc7	válka
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
světový	světový	k2eAgInSc1d1	světový
chaos	chaos	k1gInSc1	chaos
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgFnSc1d3	veliký
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
zapojen	zapojen	k2eAgInSc1d1	zapojen
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
snaží	snažit	k5eAaImIp3nS	snažit
svět	svět	k1gInSc1	svět
od	od	k7c2	od
záhadných	záhadný	k2eAgInPc2d1	záhadný
karburátorů	karburátor	k1gInPc2	karburátor
očistit	očistit	k5eAaPmF	očistit
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
jasného	jasný	k2eAgMnSc4d1	jasný
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
absolutno	absolutno	k1gNnSc4	absolutno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
absolutno	absolutno	k1gNnSc4	absolutno
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Referáty-seminárky	Referátyeminárka	k1gFnPc1	Referáty-seminárka
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
www.capek.misto.cz	www.capek.misto.cz	k1gMnSc1	www.capek.misto.cz
</s>
</p>
