<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1607	[number]	k4	1607
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1677	[number]	k4	1677
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
latinizovanou	latinizovaný	k2eAgFnSc7d1	latinizovaná
formou	forma	k1gFnSc7	forma
jména	jméno	k1gNnSc2	jméno
Wenceslaus	Wenceslaus	k1gMnSc1	Wenceslaus
Hollar	Hollar	k1gMnSc1	Hollar
Bohemus	Bohemus	k1gMnSc1	Bohemus
a	a	k8xC	a
poněmčenou	poněmčený	k2eAgFnSc4d1	poněmčená
Wenzel	Wenzel	k1gFnSc4	Wenzel
Hollar	Hollara	k1gFnPc2	Hollara
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
barokní	barokní	k2eAgMnSc1d1	barokní
rytec	rytec	k1gMnSc1	rytec
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
dílnách	dílna	k1gFnPc6	dílna
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
u	u	k7c2	u
Matthäuse	Matthäuse	k1gFnSc2	Matthäuse
Meriana	Meriana	k1gFnSc1	Meriana
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
a	a	k8xC	a
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
hraběte	hrabě	k1gMnSc2	hrabě
Thomase	Thomas	k1gMnSc2	Thomas
Howarda	Howard	k1gMnSc2	Howard
z	z	k7c2	z
Arundelu	Arundel	k1gInSc2	Arundel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
morové	morový	k2eAgFnSc6d1	morová
epidemii	epidemie	k1gFnSc6	epidemie
<g/>
,	,	kIx,	,
dotkl	dotknout	k5eAaPmAgMnS	dotknout
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
velký	velký	k2eAgInSc4d1	velký
požár	požár	k1gInSc4	požár
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
Čechem	Čech	k1gMnSc7	Čech
a	a	k8xC	a
dával	dávat	k5eAaImAgInS	dávat
to	ten	k3xDgNnSc1	ten
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
hrobu	hrob	k1gInSc2	hrob
pro	pro	k7c4	pro
zvláště	zvláště	k6eAd1	zvláště
nemajetnou	majetný	k2eNgFnSc4d1	nemajetná
chudinu	chudina	k1gFnSc4	chudina
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
hřbitově	hřbitov	k1gInSc6	hřbitov
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
je	být	k5eAaImIp3nS	být
autor	autor	k1gMnSc1	autor
realistických	realistický	k2eAgMnPc2d1	realistický
barokních	barokní	k2eAgMnPc2d1	barokní
leptů	lept	k1gInPc2	lept
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
časté	častý	k2eAgInPc1d1	častý
náměty	námět	k1gInPc1	námět
patří	patřit	k5eAaImIp3nP	patřit
přírodní	přírodní	k2eAgInPc1d1	přírodní
motivy	motiv	k1gInPc1	motiv
<g/>
,	,	kIx,	,
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
veduty	veduta	k1gFnPc1	veduta
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
městské	městský	k2eAgInPc1d1	městský
prospekty	prospekt	k1gInPc1	prospekt
<g/>
,	,	kIx,	,
kroje	kroj	k1gInPc1	kroj
a	a	k8xC	a
zátiší	zátiší	k1gNnSc1	zátiší
na	na	k7c6	na
volných	volný	k2eAgInPc6d1	volný
listech	list	k1gInPc6	list
i	i	k8xC	i
v	v	k7c6	v
souborech	soubor	k1gInPc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
kreseb	kresba	k1gFnPc2	kresba
z	z	k7c2	z
krátké	krátký	k2eAgFnSc2d1	krátká
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
významné	významný	k2eAgFnSc2d1	významná
a	a	k8xC	a
majetné	majetný	k2eAgFnSc2d1	majetná
pražské	pražský	k2eAgFnSc2d1	Pražská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
mládí	mládí	k1gNnSc2	mládí
Václava	Václav	k1gMnSc2	Václav
Hollara	Hollar	k1gMnSc2	Hollar
spadají	spadat	k5eAaPmIp3nP	spadat
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Jan	Jan	k1gMnSc1	Jan
Hollar	Hollar	k1gMnSc1	Hollar
byl	být	k5eAaImAgMnS	být
depozitorem	depozitor	k1gMnSc7	depozitor
českých	český	k2eAgFnPc2d1	Česká
zemských	zemský	k2eAgFnPc2d1	zemská
desek	deska	k1gFnPc2	deska
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
nižší	nízký	k2eAgFnSc2d2	nižší
zemské	zemský	k2eAgFnSc2d1	zemská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
mohl	moct	k5eAaImAgMnS	moct
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
používat	používat	k5eAaImF	používat
erb	erb	k1gInSc4	erb
a	a	k8xC	a
přídomek	přídomek	k1gInSc4	přídomek
z	z	k7c2	z
Práchně	Prácheň	k1gFnSc2	Prácheň
(	(	kIx(	(
<g/>
von	von	k1gInSc1	von
Prachenberg	Prachenberg	k1gInSc1	Prachenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Markéta	Markéta	k1gFnSc1	Markéta
Löw	Löw	k1gFnSc1	Löw
byla	být	k5eAaImAgFnS	být
rodem	rod	k1gInSc7	rod
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
rytířské	rytířský	k2eAgFnSc2d1	rytířská
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Löwengrünu	Löwengrün	k1gInSc2	Löwengrün
a	a	k8xC	a
Bareytu	Bareyt	k1gInSc2	Bareyt
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
Falce	Falc	k1gFnSc2	Falc
<g/>
.	.	kIx.	.
</s>
<s>
Švagr	švagr	k1gMnSc1	švagr
jeho	jeho	k3xOp3gMnPc4	jeho
strýce	strýc	k1gMnPc4	strýc
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Aretin	Aretin	k1gMnSc1	Aretin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
kartografem	kartograf	k1gMnSc7	kartograf
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
třetí	třetí	k4xOgFnPc1	třetí
nejstarší	starý	k2eAgFnPc1d3	nejstarší
mapy	mapa	k1gFnPc1	mapa
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvními	první	k4xOgInPc7	první
uměleckými	umělecký	k2eAgInPc7d1	umělecký
pokusy	pokus	k1gInPc7	pokus
začínal	začínat	k5eAaImAgInS	začínat
Hollar	Hollar	k1gInSc1	Hollar
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rytí	rytí	k1gNnSc1	rytí
začal	začít	k5eAaPmAgInS	začít
praktikovat	praktikovat	k5eAaImF	praktikovat
až	až	k9	až
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmnácti	osmnáct	k4xCc6	osmnáct
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Aegidia	Aegidium	k1gNnSc2	Aegidium
Sadelera	Sadeler	k1gMnSc2	Sadeler
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
narozeného	narozený	k2eAgMnSc2d1	narozený
grafika	grafik	k1gMnSc2	grafik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
rozkvětu	rozkvět	k1gInSc2	rozkvět
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Hollar	Hollar	k1gMnSc1	Hollar
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
Rudolfovou	Rudolfův	k2eAgFnSc7d1	Rudolfova
kunstkomorou	kunstkomora	k1gFnSc7	kunstkomora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k9	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
rozprodána	rozprodat	k5eAaPmNgFnS	rozprodat
nebo	nebo	k8xC	nebo
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
jeho	jeho	k3xOp3gNnSc1	jeho
kopie	kopie	k1gFnPc1	kopie
podle	podle	k7c2	podle
Dürera	Dürero	k1gNnSc2	Dürero
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
Hollar	Hollar	k1gInSc1	Hollar
velmi	velmi	k6eAd1	velmi
vážil	vážit	k5eAaImAgInS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Hollarův	Hollarův	k2eAgMnSc1d1	Hollarův
otec	otec	k1gMnSc1	otec
jeho	jeho	k3xOp3gNnSc4	jeho
umělecké	umělecký	k2eAgNnSc4d1	umělecké
snažení	snažení	k1gNnSc4	snažení
nepodporoval	podporovat	k5eNaImAgMnS	podporovat
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
syn	syn	k1gMnSc1	syn
stal	stát	k5eAaPmAgMnS	stát
právníkem	právník	k1gMnSc7	právník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
kvůli	kvůli	k7c3	kvůli
Obnovenému	obnovený	k2eAgNnSc3d1	obnovené
zřízení	zřízení	k1gNnSc3	zřízení
zemskému	zemský	k2eAgNnSc3d1	zemské
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
odebral	odebrat	k5eAaPmAgMnS	odebrat
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
nejspíše	nejspíše	k9	nejspíše
neodešel	odejít	k5eNaPmAgMnS	odejít
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
politických	politický	k2eAgInPc2d1	politický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
z	z	k7c2	z
existenciálních	existenciální	k2eAgInPc2d1	existenciální
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
zahraničí	zahraničí	k1gNnSc1	zahraničí
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
nabídnout	nabídnout	k5eAaPmF	nabídnout
lepší	dobrý	k2eAgNnSc4d2	lepší
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
snad	snad	k9	snad
ani	ani	k8xC	ani
nehrály	hrát	k5eNaImAgInP	hrát
náboženské	náboženský	k2eAgInPc1d1	náboženský
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
snad	snad	k9	snad
i	i	k9	i
přes	přes	k7c4	přes
informace	informace	k1gFnPc4	informace
naznačující	naznačující	k2eAgFnPc4d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollar	Hollar	k1gMnSc1	Hollar
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
českobratské	českobratský	k2eAgFnSc2d1	českobratský
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
níže	níže	k1gFnSc1	níže
sekce	sekce	k1gFnSc2	sekce
Hollarovo	Hollarův	k2eAgNnSc4d1	Hollarovo
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
)	)	kIx)	)
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
žil	žít	k5eAaImAgInS	žít
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
<g/>
-	-	kIx~	-
<g/>
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
pro	pro	k7c4	pro
nakladatele	nakladatel	k1gMnSc4	nakladatel
Heydena	Heyden	k2eAgMnSc4d1	Heyden
a	a	k8xC	a
kolínského	kolínský	k2eAgMnSc4d1	kolínský
nakladatele	nakladatel	k1gMnSc4	nakladatel
Hogenberga	Hogenberg	k1gMnSc4	Hogenberg
(	(	kIx(	(
<g/>
1629	[number]	k4	1629
<g/>
-	-	kIx~	-
<g/>
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1629-1630	[number]	k4	1629-1630
podnikl	podniknout	k5eAaPmAgMnS	podniknout
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Rýně	Rýn	k1gInSc6	Rýn
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
usadil	usadit	k5eAaPmAgMnS	usadit
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
u	u	k7c2	u
nejvýznamnějšího	významný	k2eAgInSc2d3	nejvýznamnější
mědirytce	mědirytec	k1gMnPc4	mědirytec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Matthäuse	Matthäuse	k1gFnSc1	Matthäuse
Meriana	Meriana	k1gFnSc1	Meriana
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
na	na	k7c6	na
Merianově	Merianův	k2eAgInSc6d1	Merianův
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
díle	díl	k1gInSc6	díl
Topographia	Topographius	k1gMnSc2	Topographius
Germaniae	Germania	k1gMnSc2	Germania
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
soustavně	soustavně	k6eAd1	soustavně
zachycovat	zachycovat	k5eAaImF	zachycovat
krajinu	krajina	k1gFnSc4	krajina
středního	střední	k2eAgNnSc2d1	střední
Porýní	Porýní	k1gNnSc2	Porýní
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
předchůdcem	předchůdce	k1gMnSc7	předchůdce
rýnského	rýnský	k2eAgInSc2d1	rýnský
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1633	[number]	k4	1633
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
tam	tam	k6eAd1	tam
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
rytin	rytina	k1gFnPc2	rytina
u	u	k7c2	u
Abrahama	Abraham	k1gMnSc2	Abraham
Hogenberga	Hogenberg	k1gMnSc2	Hogenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
důležitému	důležitý	k2eAgNnSc3d1	důležité
setkání	setkání	k1gNnSc3	setkání
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Thomasem	Thomas	k1gMnSc7	Thomas
Howardem	Howard	k1gMnSc7	Howard
z	z	k7c2	z
Arundelu	Arundel	k1gInSc2	Arundel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cestoval	cestovat	k5eAaImAgMnS	cestovat
jako	jako	k8xS	jako
anglický	anglický	k2eAgMnSc1d1	anglický
vyslanec	vyslanec	k1gMnSc1	vyslanec
k	k	k7c3	k
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
problém	problém	k1gInSc1	problém
s	s	k7c7	s
majetkovým	majetkový	k2eAgNnSc7d1	majetkové
vyrovnáním	vyrovnání	k1gNnSc7	vyrovnání
vdovy	vdova	k1gFnSc2	vdova
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Falce	Falc	k1gFnSc2	Falc
<g/>
.	.	kIx.	.
</s>
<s>
Arundel	Arundlo	k1gNnPc2	Arundlo
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
sběratel	sběratel	k1gMnSc1	sběratel
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Hollara	Hollar	k1gMnSc4	Hollar
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
Abrahama	Abraham	k1gMnSc4	Abraham
Hogenberga	Hogenberg	k1gMnSc2	Hogenberg
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
zastavili	zastavit	k5eAaPmAgMnP	zastavit
například	například	k6eAd1	například
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Arundel	Arundlo	k1gNnPc2	Arundlo
získal	získat	k5eAaPmAgMnS	získat
dalšího	další	k2eAgMnSc4d1	další
umělce	umělec	k1gMnSc4	umělec
<g/>
,	,	kIx,	,
Henrika	Henrik	k1gMnSc4	Henrik
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Borcht	Borcht	k1gMnSc1	Borcht
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
blízkým	blízký	k2eAgMnSc7d1	blízký
Hollarovým	Hollarův	k2eAgMnSc7d1	Hollarův
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc4	Vídeň
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
procestovali	procestovat	k5eAaPmAgMnP	procestovat
celou	celý	k2eAgFnSc4d1	celá
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
ještě	ještě	k9	ještě
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
dvora	dvůr	k1gInSc2	dvůr
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Arundela	Arundel	k1gMnSc2	Arundel
Hollarovi	Hollar	k1gMnSc3	Hollar
polepšen	polepšen	k2eAgInSc4d1	polepšen
erb	erb	k1gInSc4	erb
a	a	k8xC	a
predikát	predikát	k1gInSc4	predikát
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
Hollar	Hollar	k1gInSc1	Hollar
Prachenberger	Prachenbergra	k1gFnPc2	Prachenbergra
von	von	k1gInSc1	von
Löwengrün	Löwengrün	k1gMnSc1	Löwengrün
und	und	k?	und
Bareyt	Bareyt	k1gInSc1	Bareyt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
sponzor	sponzor	k1gMnSc1	sponzor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1637	[number]	k4	1637
vracel	vracet	k5eAaImAgMnS	vracet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
Hollar	Hollar	k1gInSc1	Hollar
ho	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
tam	tam	k6eAd1	tam
strávil	strávit	k5eAaPmAgMnS	strávit
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
žil	žít	k5eAaImAgMnS	žít
Hollar	Hollar	k1gInSc4	Hollar
v	v	k7c6	v
Arundlově	Arundlův	k2eAgFnSc6d1	Arundlův
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
postupně	postupně	k6eAd1	postupně
seznamovat	seznamovat	k5eAaImF	seznamovat
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
již	již	k6eAd1	již
jednou	jeden	k4xCgFnSc7	jeden
musel	muset	k5eAaImAgInS	muset
odcestovat	odcestovat	k5eAaPmF	odcestovat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
domoviny	domovina	k1gFnSc2	domovina
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
stále	stále	k6eAd1	stále
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
jeho	jeho	k3xOp3gInSc3	jeho
domovu	domov	k1gInSc3	domov
podobné	podobný	k2eAgFnPc4d1	podobná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
příchod	příchod	k1gInSc4	příchod
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
převratnou	převratný	k2eAgFnSc7d1	převratná
změnou	změna	k1gFnSc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Arundela	Arundel	k1gMnSc2	Arundel
pracoval	pracovat	k5eAaImAgMnS	pracovat
především	především	k6eAd1	především
na	na	k7c4	na
zdokumentování	zdokumentování	k1gNnSc4	zdokumentování
jeho	jeho	k3xOp3gFnSc2	jeho
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
umělecké	umělecký	k2eAgFnSc2d1	umělecká
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
být	být	k5eAaImF	být
finančně	finančně	k6eAd1	finančně
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
nebyl	být	k5eNaImAgMnS	být
Arundelovým	Arundelův	k2eAgMnSc7d1	Arundelův
služebníkem	služebník	k1gMnSc7	služebník
či	či	k8xC	či
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
autoportrétech	autoportrét	k1gInPc6	autoportrét
nápadně	nápadně	k6eAd1	nápadně
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
rodinný	rodinný	k2eAgInSc4d1	rodinný
erb	erb	k1gInSc4	erb
aby	aby	kYmCp3nS	aby
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
urozeného	urozený	k2eAgMnSc2d1	urozený
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
díla	dílo	k1gNnSc2	dílo
současníků	současník	k1gMnPc2	současník
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Daniel	Daniel	k1gMnSc1	Daniel
Mytens	Mytens	k1gInSc4	Mytens
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc1	Rubens
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Lievens	Lievensa	k1gFnPc2	Lievensa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Anthonis	Anthonis	k1gInSc1	Anthonis
van	vana	k1gFnPc2	vana
Dyck	Dyck	k1gInSc1	Dyck
a	a	k8xC	a
Hollar	Hollar	k1gInSc1	Hollar
zde	zde	k6eAd1	zde
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
i	i	k9	i
díla	dílo	k1gNnSc2	dílo
starších	starý	k2eAgMnPc2d2	starší
mistrů	mistr	k1gMnPc2	mistr
jako	jako	k9	jako
byl	být	k5eAaImAgMnS	být
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
Raffael	Raffael	k1gInSc1	Raffael
<g/>
,	,	kIx,	,
Parmigianino	Parmigianina	k1gFnSc5	Parmigianina
<g/>
,	,	kIx,	,
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Holbein	Holbein	k1gMnSc1	Holbein
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Elsheimer	Elsheimer	k1gMnSc1	Elsheimer
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
tím	ten	k3xDgInSc7	ten
větší	veliký	k2eAgFnPc4d2	veliký
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
originálů	originál	k1gInPc2	originál
zmizely	zmizet	k5eAaPmAgFnP	zmizet
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jen	jen	k9	jen
z	z	k7c2	z
Hollarových	Hollarův	k2eAgFnPc2d1	Hollarova
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
učil	učit	k5eAaImAgInS	učit
anglicky	anglicky	k6eAd1	anglicky
od	od	k7c2	od
Margaret	Margareta	k1gFnPc2	Margareta
Tracy	Traca	k1gFnSc2	Traca
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
nechávala	nechávat	k5eAaImAgFnS	nechávat
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
v	v	k7c6	v
kreslení	kreslení	k1gNnSc6	kreslení
a	a	k8xC	a
základech	základ	k1gInPc6	základ
rytí	rytí	k1gNnSc2	rytí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
komornou	komorný	k2eAgFnSc4d1	komorná
lady	lady	k1gFnSc4	lady
Alethee	Alethe	k1gFnSc2	Alethe
Howardové	Howardový	k2eAgFnSc2d1	Howardový
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
Hollarovým	Hollarův	k2eAgInSc7d1	Hollarův
zájmem	zájem	k1gInSc7	zájem
bylo	být	k5eAaImAgNnS	být
proniknout	proniknout	k5eAaPmF	proniknout
ke	k	k7c3	k
královskému	královský	k2eAgInSc3d1	královský
dvoru	dvůr	k1gInSc3	dvůr
jako	jako	k9	jako
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
nemalé	malý	k2eNgInPc4d1	nemalý
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ohledně	ohledně	k7c2	ohledně
umění	umění	k1gNnSc2	umění
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
hlavní	hlavní	k2eAgNnSc4d1	hlavní
slovo	slovo	k1gNnSc4	slovo
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
vlámsky	vlámsky	k6eAd1	vlámsky
malíř	malíř	k1gMnSc1	malíř
Anthonis	Anthonis	k1gFnSc2	Anthonis
van	vana	k1gFnPc2	vana
Dyck	Dyck	k1gMnSc1	Dyck
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
Hollarovi	Hollar	k1gMnSc3	Hollar
nakloněn	nakloněn	k2eAgInSc4d1	nakloněn
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
van	van	k1gInSc1	van
Dyck	Dyck	k1gMnSc1	Dyck
jakožto	jakožto	k8xS	jakožto
malíř	malíř	k1gMnSc1	malíř
oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
na	na	k7c4	na
rytectví	rytectví	k1gNnSc4	rytectví
odlišné	odlišný	k2eAgFnSc2d1	odlišná
hodnoty	hodnota	k1gFnSc2	hodnota
než	než	k8xS	než
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
především	především	k6eAd1	především
rytec	rytec	k1gMnSc1	rytec
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
)	)	kIx)	)
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
vyhrocovala	vyhrocovat	k5eAaImAgFnS	vyhrocovat
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
vznikaly	vznikat	k5eAaImAgInP	vznikat
dva	dva	k4xCgInPc1	dva
tábory	tábor	k1gInPc1	tábor
-	-	kIx~	-
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
podporovali	podporovat	k5eAaImAgMnP	podporovat
krále	král	k1gMnSc4	král
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
co	co	k9	co
reptali	reptat	k5eAaImAgMnP	reptat
proti	proti	k7c3	proti
špatnému	špatný	k2eAgNnSc3d1	špatné
královskému	královský	k2eAgNnSc3d1	královské
vedení	vedení	k1gNnSc3	vedení
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
být	být	k5eAaImF	být
neutrální	neutrální	k2eAgMnSc1d1	neutrální
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
Arundelovu	Arundelův	k2eAgInSc3d1	Arundelův
domu	dům	k1gInSc3	dům
přiřazovala	přiřazovat	k5eAaImAgFnS	přiřazovat
k	k	k7c3	k
royalistům	royalista	k1gMnPc3	royalista
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
portrétů	portrét	k1gInPc2	portrét
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgMnS	patřit
spisovateli	spisovatel	k1gMnSc3	spisovatel
a	a	k8xC	a
královu	králův	k2eAgMnSc3d1	králův
odpůrci	odpůrce	k1gMnSc3	odpůrce
Johnu	John	k1gMnSc3	John
Miltonovi	Milton	k1gMnSc3	Milton
<g/>
,	,	kIx,	,
dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
královských	královský	k2eAgMnPc2d1	královský
odpůrců	odpůrce	k1gMnPc2	odpůrce
byl	být	k5eAaImAgMnS	být
Hollarem	Hollar	k1gInSc7	Hollar
portrétovaný	portrétovaný	k2eAgMnSc1d1	portrétovaný
politik	politik	k1gMnSc1	politik
John	John	k1gMnSc1	John
Pym	Pym	k1gMnSc1	Pym
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrocenost	vyhrocenost	k1gFnSc1	vyhrocenost
situace	situace	k1gFnSc2	situace
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
pádu	pád	k1gInSc6	pád
králova	králův	k2eAgMnSc2d1	králův
oblíbence	oblíbenec	k1gMnSc2	oblíbenec
Thomase	Thomas	k1gMnSc2	Thomas
Wentwortha	Wentworth	k1gMnSc2	Wentworth
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc4	první
hraběte	hrabě	k1gMnSc4	hrabě
ze	z	k7c2	z
Straffordu	Strafford	k1gInSc2	Strafford
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Hollar	Hollar	k1gMnSc1	Hollar
poprvé	poprvé	k6eAd1	poprvé
portrétoval	portrétovat	k5eAaImAgMnS	portrétovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1640	[number]	k4	1640
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
irského	irský	k2eAgMnSc4d1	irský
místokrále	místokrál	k1gMnSc4	místokrál
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
zdokumentoval	zdokumentovat	k5eAaPmAgInS	zdokumentovat
proces	proces	k1gInSc1	proces
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1641	[number]	k4	1641
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
komornou	komorná	k1gFnSc7	komorná
Margaret	Margareta	k1gFnPc2	Margareta
Tracy	Traca	k1gFnSc2	Traca
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
staršího	starší	k1gMnSc2	starší
Jamese	Jamese	k1gFnSc2	Jamese
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Amosem	Amos	k1gMnSc7	Amos
Komenským	Komenský	k1gMnSc7	Komenský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
anglická	anglický	k2eAgFnSc1d1	anglická
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Arundel	Arundlo	k1gNnPc2	Arundlo
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
učil	učít	k5eAaPmAgMnS	učít
kreslit	kreslit	k5eAaImF	kreslit
waleského	waleský	k2eAgMnSc4d1	waleský
prince	princ	k1gMnSc4	princ
a	a	k8xC	a
pozdějšího	pozdní	k2eAgMnSc4d2	pozdější
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k9	což
první	první	k4xOgFnSc7	první
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
již	již	k6eAd1	již
Vertue	Vertue	k1gFnSc4	Vertue
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
krátká	krátký	k2eAgFnSc1d1	krátká
Hollarova	Hollarův	k2eAgFnSc1d1	Hollarova
biografie	biografie	k1gFnSc1	biografie
vyšlá	vyšlý	k2eAgFnSc1d1	vyšlá
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1649	[number]	k4	1649
toto	tento	k3xDgNnSc1	tento
zcela	zcela	k6eAd1	zcela
nepotvrzuje	potvrzovat	k5eNaImIp3nS	potvrzovat
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Karlova	Karlův	k2eAgMnSc4d1	Karlův
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Jakuba	Jakub	k1gMnSc2	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Hollarův	Hollarův	k2eAgMnSc1d1	Hollarův
životopisec	životopisec	k1gMnSc1	životopisec
a	a	k8xC	a
rytec	rytec	k1gMnSc1	rytec
George	Georg	k1gMnSc2	Georg
Vertue	Vertue	k1gNnSc2	Vertue
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollar	Hollar	k1gMnSc1	Hollar
bránil	bránit	k5eAaImAgMnS	bránit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
royalisty	royalista	k1gMnPc7	royalista
Basing	Basing	k1gInSc4	Basing
House	house	k1gNnSc1	house
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
vojáky	voják	k1gMnPc4	voják
parlamentu	parlament	k1gInSc2	parlament
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollar	Hollar	k1gInSc1	Hollar
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
a	a	k8xC	a
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
royalistické	royalistický	k2eAgFnSc2d1	royalistická
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Arundelem	Arundel	k1gMnSc7	Arundel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
cestách	cesta	k1gFnPc6	cesta
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
Vlámsku	Vlámsek	k1gInSc6	Vlámsek
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
Hollar	Hollar	k1gMnSc1	Hollar
dokumentaci	dokumentace	k1gFnSc3	dokumentace
Arundelových	Arundelův	k2eAgFnPc2d1	Arundelův
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
té	ten	k3xDgFnSc2	ten
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mecenáš	mecenáš	k1gMnSc1	mecenáš
stihl	stihnout	k5eAaPmAgMnS	stihnout
přesunout	přesunout	k5eAaPmF	přesunout
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
nakonec	nakonec	k6eAd1	nakonec
vzešly	vzejít	k5eAaPmAgFnP	vzejít
i	i	k9	i
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgInPc1d1	ceněný
tisky	tisk	k1gInPc1	tisk
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
mušlí	mušle	k1gFnPc2	mušle
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Arundel	Arundlo	k1gNnPc2	Arundlo
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zajistit	zajistit	k5eAaPmF	zajistit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
antverpských	antverpský	k2eAgMnPc2d1	antverpský
nakladatelů	nakladatel	k1gMnPc2	nakladatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
pozdější	pozdní	k2eAgMnSc1d2	pozdější
přítel	přítel	k1gMnSc1	přítel
Jan	Jan	k1gMnSc1	Jan
Meyssens	Meyssens	k1gInSc4	Meyssens
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
tvrzení	tvrzení	k1gNnSc4	tvrzení
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
van	van	k1gInSc1	van
Dycka	Dycek	k1gMnSc2	Dycek
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollarovo	Hollarův	k2eAgNnSc1d1	Hollarovo
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
překonané	překonaný	k2eAgNnSc1d1	překonané
Rubensovou	Rubensův	k2eAgFnSc7d1	Rubensova
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
proto	proto	k8xC	proto
řadu	řada	k1gFnSc4	řada
kopií	kopie	k1gFnPc2	kopie
obrazů	obraz	k1gInPc2	obraz
tohoto	tento	k3xDgMnSc2	tento
talentovaného	talentovaný	k2eAgMnSc2d1	talentovaný
Vláma	Vlám	k1gMnSc2	Vlám
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
nebyla	být	k5eNaImAgFnS	být
marná	marný	k2eAgFnSc1d1	marná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
svědčit	svědčit	k5eAaImF	svědčit
výrok	výrok	k1gInSc4	výrok
Vertua	Vertuus	k1gMnSc2	Vertuus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Hollara	Hollar	k1gMnSc4	Hollar
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
van	van	k1gInSc4	van
Dyckova	Dyckův	k2eAgMnSc2d1	Dyckův
interpreta	interpret	k1gMnSc2	interpret
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
také	také	k9	také
mohl	moct	k5eAaImAgMnS	moct
blíže	blízce	k6eAd2	blízce
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
díly	díl	k1gInPc7	díl
Hanse	Hans	k1gMnSc2	Hans
Memlinga	Memling	k1gMnSc2	Memling
<g/>
,	,	kIx,	,
Huga	Hugo	k1gMnSc2	Hugo
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Goese	Goes	k1gMnSc4	Goes
a	a	k8xC	a
Rogiera	Rogier	k1gMnSc4	Rogier
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Weydena	Weyden	k2eAgNnPc4d1	Weyden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1647	[number]	k4	1647
publikoval	publikovat	k5eAaBmAgInS	publikovat
s	s	k7c7	s
nakladatelem	nakladatel	k1gMnSc7	nakladatel
C.	C.	kA	C.
Danckertsem	Danckerts	k1gInSc7	Danckerts
celkový	celkový	k2eAgInSc4d1	celkový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1649	[number]	k4	1649
vyryl	vyrýt	k5eAaPmAgInS	vyrýt
velký	velký	k2eAgInSc1d1	velký
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
(	(	kIx(	(
<g/>
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
Merianově	Merianův	k2eAgInSc6d1	Merianův
Topographia	Topographium	k1gNnSc2	Topographium
Bohemiae	Bohemia	k1gInPc4	Bohemia
<g/>
,	,	kIx,	,
Moraviae	Moravia	k1gInPc4	Moravia
et	et	k?	et
Silesiae	Silesia	k1gFnSc2	Silesia
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc4	dílo
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
také	také	k9	také
určitý	určitý	k2eAgInSc4d1	určitý
stesk	stesk	k1gInSc4	stesk
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
další	další	k2eAgFnSc1d1	další
známá	známý	k2eAgFnSc1d1	známá
rytina	rytina	k1gFnSc1	rytina
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
si	se	k3xPyFc3	se
Hollar	Hollar	k1gMnSc1	Hollar
zřejmě	zřejmě	k6eAd1	zřejmě
udělal	udělat	k5eAaPmAgMnS	udělat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
český	český	k2eAgInSc1d1	český
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
těžko	těžko	k6eAd1	těžko
mohl	moct	k5eAaImAgMnS	moct
někdo	někdo	k3yInSc1	někdo
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
rozumět	rozumět	k5eAaImF	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
tvořil	tvořit	k5eAaImAgInS	tvořit
sérii	série	k1gFnSc4	série
holandských	holandský	k2eAgFnPc2d1	holandská
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
<g/>
,	,	kIx,	,
také	také	k9	také
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
módu	móda	k1gFnSc4	móda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
rukávníky	rukávník	k1gInPc1	rukávník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dobře	dobře	k6eAd1	dobře
na	na	k7c6	na
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
kožešiny	kožešina	k1gFnSc2	kožešina
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
jeho	jeho	k3xOp3gInSc4	jeho
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
detail	detail	k1gInSc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1652	[number]	k4	1652
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
překvapivě	překvapivě	k6eAd1	překvapivě
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
sílícího	sílící	k2eAgNnSc2d1	sílící
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
a	a	k8xC	a
Cromwellovou	Cromwellův	k2eAgFnSc7d1	Cromwellova
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
důvody	důvod	k1gInPc1	důvod
návratu	návrat	k1gInSc2	návrat
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Urzidil	Urzidit	k5eAaImAgMnS	Urzidit
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
zastesklo	zastesknout	k5eAaPmAgNnS	zastesknout
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
přemlouvali	přemlouvat	k5eAaImAgMnP	přemlouvat
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
:	:	kIx,	:
spisovatel	spisovatel	k1gMnSc1	spisovatel
John	John	k1gMnSc1	John
Aubrey	Aubrea	k1gFnSc2	Aubrea
<g/>
,	,	kIx,	,
sběratel	sběratel	k1gMnSc1	sběratel
umění	umění	k1gNnSc2	umění
Elias	Elias	k1gMnSc1	Elias
Ashmole	Ashmole	k1gFnSc2	Ashmole
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Dugdale	Dugdal	k1gMnSc5	Dugdal
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
museli	muset	k5eAaImAgMnP	muset
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
Calais	Calais	k1gNnSc2	Calais
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Dover	Dovra	k1gFnPc2	Dovra
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
problémy	problém	k1gInPc1	problém
neskončily	skončit	k5eNaPmAgInP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
byl	být	k5eAaImAgMnS	být
Hollar	Hollar	k1gMnSc1	Hollar
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přesné	přesný	k2eAgNnSc1d1	přesné
obvinění	obvinění	k1gNnSc1	obvinění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijel	přijet	k5eAaPmAgMnS	přijet
z	z	k7c2	z
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
mnoho	mnoho	k4c1	mnoho
royalistických	royalistický	k2eAgMnPc2d1	royalistický
odpůrců	odpůrce	k1gMnPc2	odpůrce
Cromwellova	Cromwellův	k2eAgInSc2d1	Cromwellův
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
Hollar	Hollar	k1gInSc1	Hollar
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
táborem	tábor	k1gInSc7	tábor
již	již	k6eAd1	již
před	před	k7c7	před
útěkem	útěk	k1gInSc7	útěk
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
uvěznění	uvěznění	k1gNnSc1	uvěznění
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
pro	pro	k7c4	pro
Hollara	Hollar	k1gMnSc4	Hollar
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc3	jeho
přátelům	přítel	k1gMnPc3	přítel
podařilo	podařit	k5eAaPmAgNnS	podařit
domluvit	domluvit	k5eAaPmF	domluvit
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
pro	pro	k7c4	pro
umělce	umělec	k1gMnSc4	umělec
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
puritánské	puritánský	k2eAgFnSc6d1	puritánská
Anglii	Anglie	k1gFnSc6	Anglie
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
v	v	k7c6	v
době	doba	k1gFnSc6	doba
královské	královský	k2eAgFnSc2d1	královská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Hollarových	Hollarův	k2eAgNnPc6d1	Hollarovo
očích	oko	k1gNnPc6	oko
byla	být	k5eAaImAgFnS	být
královská	královský	k2eAgFnSc1d1	královská
Anglie	Anglie	k1gFnSc1	Anglie
zemí	zem	k1gFnPc2	zem
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
šťastní	šťastný	k2eAgMnPc1d1	šťastný
<g/>
,	,	kIx,	,
v	v	k7c6	v
puritánské	puritánský	k2eAgFnSc6d1	puritánská
Anglii	Anglie	k1gFnSc6	Anglie
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
žili	žít	k5eAaImAgMnP	žít
pod	pod	k7c7	pod
nějakou	nějaký	k3yIgFnSc7	nějaký
zlou	zlý	k2eAgFnSc7d1	zlá
kletbou	kletba	k1gFnSc7	kletba
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
bydlel	bydlet	k5eAaImAgInS	bydlet
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
žáka	žák	k1gMnSc2	žák
Williama	William	k1gMnSc2	William
Faithorna	Faithorna	k1gFnSc1	Faithorna
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
poslední	poslední	k2eAgNnSc1d1	poslední
období	období	k1gNnSc1	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Johna	John	k1gMnSc4	John
Ogilbyho	Ogilby	k1gMnSc4	Ogilby
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterého	který	k3yIgMnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ilustrace	ilustrace	k1gFnSc1	ilustrace
knih	kniha	k1gFnPc2	kniha
klasických	klasický	k2eAgInPc2d1	klasický
autorů	autor	k1gMnPc2	autor
Homéra	Homér	k1gMnSc2	Homér
<g/>
,	,	kIx,	,
Vergilia	Vergilius	k1gMnSc2	Vergilius
a	a	k8xC	a
Juvenalise	Juvenalise	k1gFnPc1	Juvenalise
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ale	ale	k9	ale
neměly	mít	k5eNaImAgFnP	mít
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
téhož	týž	k3xTgMnSc4	týž
nakladatele	nakladatel	k1gMnSc4	nakladatel
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
i	i	k9	i
Ezopovy	Ezopův	k2eAgFnPc4d1	Ezopova
bajky	bajka	k1gFnPc4	bajka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
také	také	k9	také
pocházejí	pocházet	k5eAaImIp3nP	pocházet
díla	dílo	k1gNnPc1	dílo
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sirem	sir	k1gMnSc7	sir
Williamem	William	k1gInSc7	William
Dugdalem	Dugdal	k1gMnSc7	Dugdal
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Monasticon	Monasticon	k1gMnSc1	Monasticon
Anglicanum	Anglicanum	k1gNnSc1	Anglicanum
<g/>
,	,	kIx,	,
monumentální	monumentální	k2eAgNnSc1d1	monumentální
trojsvazkové	trojsvazkový	k2eAgNnSc1d1	trojsvazkové
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
anglických	anglický	k2eAgInPc6d1	anglický
kostelích	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
History	Histor	k1gInPc1	Histor
of	of	k?	of
St.	st.	kA	st.
Paul	Paul	k1gMnSc1	Paul
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cathedral	Cathedral	k1gFnSc7	Cathedral
o	o	k7c6	o
staré	starý	k2eAgFnSc6d1	stará
londýnské	londýnský	k2eAgFnSc6d1	londýnská
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Předzvěst	předzvěst	k1gFnSc1	předzvěst
obratu	obrat	k1gInSc2	obrat
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
přišla	přijít	k5eAaPmAgNnP	přijít
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
Olivera	Oliver	k1gMnSc2	Oliver
Cromwella	Cromwell	k1gMnSc2	Cromwell
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
<g/>
.	.	kIx.	.
</s>
<s>
Oliverův	Oliverův	k2eAgMnSc1d1	Oliverův
syn	syn	k1gMnSc1	syn
Richard	Richard	k1gMnSc1	Richard
Cromwell	Cromwell	k1gMnSc1	Cromwell
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
udržet	udržet	k5eAaPmF	udržet
otcovu	otcův	k2eAgFnSc4d1	otcova
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
restaurace	restaurace	k1gFnSc1	restaurace
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nový	nový	k2eAgMnSc1d1	nový
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
znal	znát	k5eAaImAgMnS	znát
Hollara	Hollara	k1gFnSc1	Hollara
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
kreslení	kreslení	k1gNnSc2	kreslení
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
proniknout	proniknout	k5eAaPmF	proniknout
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
bude	být	k5eAaImBp3nS	být
poněkud	poněkud	k6eAd1	poněkud
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ani	ani	k8xC	ani
král	král	k1gMnSc1	král
neměl	mít	k5eNaImAgMnS	mít
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
výplatu	výplata	k1gFnSc4	výplata
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc6	chvíle
král	král	k1gMnSc1	král
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
doporučení	doporučení	k1gNnSc1	doporučení
starostovi	starosta	k1gMnSc3	starosta
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
podpořil	podpořit	k5eAaPmAgInS	podpořit
při	při	k7c6	při
dokončování	dokončování	k1gNnSc6	dokončování
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
mapového	mapový	k2eAgNnSc2d1	mapové
díla	dílo	k1gNnSc2	dílo
týkajícího	týkající	k2eAgNnSc2d1	týkající
se	se	k3xPyFc4	se
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
doporučení	doporučení	k1gNnPc2	doporučení
ovšem	ovšem	k9	ovšem
nemělo	mít	k5eNaImAgNnS	mít
větší	veliký	k2eAgFnSc4d2	veliký
odezvu	odezva	k1gFnSc4	odezva
<g/>
.	.	kIx.	.
</s>
<s>
K	K	kA	K
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
existenční	existenční	k2eAgFnSc4d1	existenční
situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
také	také	k9	také
přidala	přidat	k5eAaPmAgFnS	přidat
smrt	smrt	k1gFnSc1	smrt
ženy	žena	k1gFnSc2	žena
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
Hollara	Hollara	k1gFnSc1	Hollara
potkalo	potkat	k5eAaPmAgNnS	potkat
další	další	k2eAgNnSc4d1	další
rodinné	rodinný	k2eAgNnSc4d1	rodinné
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
:	:	kIx,	:
při	při	k7c6	při
morové	morový	k2eAgFnSc6d1	morová
epidemii	epidemie	k1gFnSc6	epidemie
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
také	také	k9	také
projevil	projevit	k5eAaPmAgInS	projevit
kreslířský	kreslířský	k2eAgInSc1d1	kreslířský
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
druhé	druhý	k4xOgFnSc6	druhý
ženě	žena	k1gFnSc6	žena
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Honora	honora	k1gFnSc1	honora
a	a	k8xC	a
že	že	k8xS	že
utekla	utéct	k5eAaPmAgFnS	utéct
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
.	.	kIx.	.
</s>
<s>
Hollarova	Hollarův	k2eAgFnSc1d1	Hollarova
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
pocromwellovské	pocromwellovský	k2eAgFnSc6d1	pocromwellovský
Anglii	Anglie	k1gFnSc6	Anglie
určovala	určovat	k5eAaImAgFnS	určovat
od	od	k7c2	od
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
ne	ne	k9	ne
jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
za	za	k7c4	za
kus	kus	k1gInSc4	kus
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
Hollar	Hollar	k1gMnSc1	Hollar
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakladatelé	nakladatel	k1gMnPc1	nakladatel
zneužívali	zneužívat	k5eAaImAgMnP	zneužívat
nedostatku	nedostatek	k1gInSc3	nedostatek
poptávky	poptávka	k1gFnSc2	poptávka
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
odmítali	odmítat	k5eAaImAgMnP	odmítat
platit	platit	k5eAaImF	platit
celé	celý	k2eAgFnPc4d1	celá
nasmlouvané	nasmlouvaný	k2eAgFnPc4d1	nasmlouvaná
částky	částka	k1gFnPc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platilo	platit	k5eAaImAgNnS	platit
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
portrét	portrét	k1gInSc4	portrét
filozofa	filozof	k1gMnSc2	filozof
Thomase	Thomas	k1gMnSc2	Thomas
Hobbese	Hobbese	k1gFnSc1	Hobbese
pro	pro	k7c4	pro
objednavatele	objednavatel	k1gMnSc4	objednavatel
Petera	Peter	k1gMnSc2	Peter
Stenta	Stent	k1gMnSc2	Stent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
celou	celý	k2eAgFnSc4d1	celá
částku	částka	k1gFnSc4	částka
kvůli	kvůli	k7c3	kvůli
údajné	údajný	k2eAgFnSc3d1	údajná
špatné	špatný	k2eAgFnSc3d1	špatná
kvalitě	kvalita	k1gFnSc3	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
shořela	shořet	k5eAaPmAgFnS	shořet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
požár	požár	k1gInSc1	požár
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
Hollara	Hollar	k1gMnSc4	Hollar
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
dopad	dopad	k1gInSc1	dopad
<g/>
:	:	kIx,	:
před	před	k7c7	před
požárem	požár	k1gInSc7	požár
totiž	totiž	k9	totiž
připravil	připravit	k5eAaPmAgInS	připravit
řadu	řada	k1gFnSc4	řada
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přestalo	přestat	k5eAaPmAgNnS	přestat
existovat	existovat	k5eAaImF	existovat
a	a	k8xC	a
takový	takový	k3xDgInSc1	takový
materiál	materiál	k1gInSc1	materiál
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
podoba	podoba	k1gFnSc1	podoba
středověkého	středověký	k2eAgNnSc2d1	středověké
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
podkladů	podklad	k1gInPc2	podklad
vycházelo	vycházet	k5eAaImAgNnS	vycházet
při	při	k7c6	při
obnově	obnova	k1gFnSc6	obnova
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
opět	opět	k6eAd1	opět
připomnělo	připomnět	k5eAaPmAgNnS	připomnět
Hollara	Hollar	k1gMnSc4	Hollar
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gNnSc3	on
za	za	k7c2	za
služby	služba	k1gFnSc2	služba
udělil	udělit	k5eAaPmAgInS	udělit
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
scenographus	scenographus	k1gMnSc1	scenographus
regius	regius	k1gMnSc1	regius
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
Hollarova	Hollarův	k2eAgFnSc1d1	Hollarova
příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
grafiky	grafika	k1gFnPc4	grafika
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
Revised	Revised	k1gMnSc1	Revised
<g/>
:	:	kIx,	:
Wherein	Wherein	k2eAgMnSc1d1	Wherein
You	You	k1gMnSc1	You
Have	Have	k1gNnSc2	Have
Also	Also	k1gMnSc1	Also
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gInSc1	Hollar
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Receipt	Receipt	k1gInSc1	Receipt
for	forum	k1gNnPc2	forum
Etching	Etching	k1gInSc1	Etching
<g/>
,	,	kIx,	,
With	With	k1gMnSc1	With
Instructions	Instructionsa	k1gFnPc2	Instructionsa
How	How	k1gMnSc1	How
to	ten	k3xDgNnSc1	ten
use	usus	k1gInSc5	usus
it	it	k?	it
(	(	kIx(	(
<g/>
Přepracovaný	přepracovaný	k2eAgMnSc1d1	přepracovaný
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
:	:	kIx,	:
kde	kde	k6eAd1	kde
také	také	k9	také
naleznete	naleznout	k5eAaPmIp2nP	naleznout
návod	návod	k1gInSc4	návod
pana	pan	k1gMnSc4	pan
Hollara	Hollar	k1gMnSc4	Hollar
pro	pro	k7c4	pro
lept	lept	k1gInSc4	lept
a	a	k8xC	a
jak	jak	k6eAd1	jak
ho	on	k3xPp3gInSc4	on
užít	užít	k5eAaPmF	užít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Hollarovou	Hollarův	k2eAgFnSc7d1	Hollarova
prací	práce	k1gFnSc7	práce
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
natolik	natolik	k6eAd1	natolik
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1668	[number]	k4	1668
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Tangeru	Tanger	k1gInSc2	Tanger
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
zachytit	zachytit	k5eAaPmF	zachytit
podobu	podoba	k1gFnSc4	podoba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
nově	nově	k6eAd1	nově
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
princeznou	princezna	k1gFnSc7	princezna
Kateřinou	Kateřina	k1gFnSc7	Kateřina
z	z	k7c2	z
Braganzy	Braganza	k1gFnSc2	Braganza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
Mary	Mary	k1gFnSc2	Mary
Rose	Ros	k1gMnSc2	Ros
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
Johna	John	k1gMnSc2	John
Kempthorna	Kempthorna	k1gFnSc1	Kempthorna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
výprava	výprava	k1gFnSc1	výprava
cestovala	cestovat	k5eAaImAgFnS	cestovat
<g/>
,	,	kIx,	,
přepadena	přepaden	k2eAgFnSc1d1	přepadena
piráty	pirát	k1gMnPc7	pirát
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
přepadení	přepadení	k1gNnSc4	přepadení
Hollar	Hollar	k1gInSc4	Hollar
později	pozdě	k6eAd2	pozdě
také	také	k9	také
umělecky	umělecky	k6eAd1	umělecky
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1672	[number]	k4	1672
například	například	k6eAd1	například
cestoval	cestovat	k5eAaImAgMnS	cestovat
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
vydavatele	vydavatel	k1gMnSc2	vydavatel
po	po	k7c6	po
Nottinghamshire	Nottinghamshir	k1gInSc5	Nottinghamshir
<g/>
,	,	kIx,	,
do	do	k7c2	do
Salisbury	Salisbura	k1gFnSc2	Salisbura
a	a	k8xC	a
Lincolnu	Lincoln	k1gMnSc3	Lincoln
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnSc1	ilustrace
potom	potom	k6eAd1	potom
použil	použít	k5eAaPmAgInS	použít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Robert	Robert	k1gMnSc1	Robert
Thoroton	Thoroton	k1gInSc1	Thoroton
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
umělce	umělec	k1gMnSc2	umělec
začalo	začít	k5eAaPmAgNnS	začít
projevovat	projevovat	k5eAaImF	projevovat
stáří	stáří	k1gNnSc1	stáří
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
měl	mít	k5eAaImAgInS	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
levým	levý	k2eAgNnSc7d1	levé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
musel	muset	k5eAaImAgMnS	muset
zakrývat	zakrývat	k5eAaImF	zakrývat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
Nezemřel	zemřít	k5eNaPmAgMnS	zemřít
však	však	k9	však
opuštěn	opustit	k5eAaPmNgMnS	opustit
<g/>
:	:	kIx,	:
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
udržoval	udržovat	k5eAaImAgInS	udržovat
blízký	blízký	k2eAgInSc1d1	blízký
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Johna	John	k1gMnSc2	John
Evelyna	Evelyno	k1gNnSc2	Evelyno
a	a	k8xC	a
Hollarovi	Hollarův	k2eAgMnPc1d1	Hollarův
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
provázeli	provázet	k5eAaImAgMnP	provázet
rakev	rakev	k1gFnSc4	rakev
<g/>
,	,	kIx,	,
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
nezapomněli	zapomenout	k5eNaPmAgMnP	zapomenout
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
podepisovali	podepisovat	k5eAaImAgMnP	podepisovat
"	"	kIx"	"
<g/>
Quondum	Quondum	k1gInSc1	Quondum
discipulus	discipulus	k1gInSc1	discipulus
Hollari	Hollar	k1gFnSc2	Hollar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kdysi	kdysi	k6eAd1	kdysi
žák	žák	k1gMnSc1	žák
Hollarův	Hollarův	k2eAgMnSc1d1	Hollarův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
archivu	archiv	k1gInSc2	archiv
Matouše	Matouš	k1gMnSc4	Matouš
Konečného	Konečný	k1gMnSc4	Konečný
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
informaci	informace	k1gFnSc4	informace
naznačující	naznačující	k2eAgFnSc4d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
hlásící	hlásící	k2eAgFnSc1d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
Jednotě	jednota	k1gFnSc3	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
český	český	k2eAgMnSc1d1	český
bratr	bratr	k1gMnSc1	bratr
jistý	jistý	k2eAgMnSc1d1	jistý
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
Hollar	Hollar	k1gMnSc1	Hollar
z	z	k7c2	z
Horaždějovic	Horaždějovice	k1gFnPc2	Horaždějovice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
Hollarovým	Hollarův	k2eAgMnSc7d1	Hollarův
otcem	otec	k1gMnSc7	otec
Janem	Jan	k1gMnSc7	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Nalezení	nalezení	k1gNnSc4	nalezení
stopy	stopa	k1gFnSc2	stopa
naznačující	naznačující	k2eAgFnSc4d1	naznačující
Hollarovu	Hollarův	k2eAgFnSc4d1	Hollarova
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
českým	český	k2eAgMnPc3d1	český
bratřím	bratr	k1gMnPc3	bratr
předcházela	předcházet	k5eAaImAgNnP	předcházet
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
argumentem	argument	k1gInSc7	argument
pro	pro	k7c4	pro
Hollarovo	Hollarův	k2eAgNnSc4d1	Hollarovo
protestanství	protestanství	k1gNnSc4	protestanství
(	(	kIx(	(
<g/>
nevědělo	vědět	k5eNaImAgNnS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
k	k	k7c3	k
jaké	jaký	k3yIgFnSc3	jaký
Hollar	Hollar	k1gInSc1	Hollar
patřil	patřit	k5eAaImAgInS	patřit
konfesi	konfese	k1gFnSc4	konfese
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Hollarův	Hollarův	k2eAgInSc1d1	Hollarův
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
skupině	skupina	k1gFnSc3	skupina
exulantů	exulant	k1gMnPc2	exulant
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odcházeli	odcházet	k5eAaImAgMnP	odcházet
právě	právě	k9	právě
z	z	k7c2	z
náboženských	náboženský	k2eAgFnPc2d1	náboženská
(	(	kIx(	(
<g/>
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
<g/>
)	)	kIx)	)
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
největší	veliký	k2eAgInSc4d3	veliký
problém	problém	k1gInSc4	problém
tohoto	tento	k3xDgInSc2	tento
důkazu	důkaz	k1gInSc2	důkaz
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvod	důvod	k1gInSc1	důvod
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
nových	nový	k2eAgFnPc6d1	nová
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
možnostech	možnost	k1gFnPc6	možnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gNnSc3	on
umělecky	umělecky	k6eAd1	umělecky
upadající	upadající	k2eAgNnSc1d1	upadající
české	český	k2eAgNnSc1d1	české
prostředí	prostředí	k1gNnSc1	prostředí
nemohlo	moct	k5eNaImAgNnS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
Protestantismus	protestantismus	k1gInSc4	protestantismus
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
také	také	k9	také
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
dobová	dobový	k2eAgNnPc4d1	dobové
svědectví	svědectví	k1gNnPc4	svědectví
<g/>
.	.	kIx.	.
</s>
<s>
Současník	současník	k1gMnSc1	současník
John	John	k1gMnSc1	John
Aubrey	Aubrea	k1gFnSc2	Aubrea
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvod	důvod	k1gInSc1	důvod
Hollarova	Hollarův	k2eAgInSc2d1	Hollarův
odchodu	odchod	k1gInSc2	odchod
z	z	k7c2	z
vlasti	vlast	k1gFnSc2	vlast
bylo	být	k5eAaImAgNnS	být
náboženské	náboženský	k2eAgNnSc1d1	náboženské
vyznání	vyznání	k1gNnSc1	vyznání
a	a	k8xC	a
Hollarův	Hollarův	k2eAgMnSc1d1	Hollarův
přítel	přítel	k1gMnSc1	přítel
John	John	k1gMnSc1	John
Evelyn	Evelyn	k1gMnSc1	Evelyn
dokonce	dokonce	k9	dokonce
přímo	přímo	k6eAd1	přímo
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollar	Hollar	k1gInSc4	Hollar
byl	být	k5eAaImAgMnS	být
protestant	protestant	k1gMnSc1	protestant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
konvertoval	konvertovat	k5eAaBmAgInS	konvertovat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
antverpských	antverpský	k2eAgMnPc2d1	antverpský
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
života	život	k1gInSc2	život
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
anglikánském	anglikánský	k2eAgInSc6d1	anglikánský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
konfesi	konfese	k1gFnSc6	konfese
podávají	podávat	k5eAaImIp3nP	podávat
znaky	znak	k1gInPc1	znak
naznačující	naznačující	k2eAgInPc1d1	naznačující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hollarovi	Hollar	k1gMnSc3	Hollar
byli	být	k5eAaImAgMnP	být
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Pennington	Pennington	k1gInSc1	Pennington
dokonce	dokonce	k9	dokonce
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgInSc7d1	silný
argumentem	argument	k1gInSc7	argument
byla	být	k5eAaImAgFnS	být
především	především	k9	především
poměrně	poměrně	k6eAd1	poměrně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
kariéra	kariéra	k1gFnSc1	kariéra
Jana	Jan	k1gMnSc2	Jan
Hollara	Hollara	k1gFnSc1	Hollara
ve	v	k7c6	v
značně	značně	k6eAd1	značně
konfesně	konfesně	k6eAd1	konfesně
netolerantním	tolerantní	k2eNgNnSc6d1	netolerantní
prostředí	prostředí	k1gNnSc6	prostředí
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
katolík	katolík	k1gMnSc1	katolík
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
mluvit	mluvit	k5eAaImF	mluvit
jeho	jeho	k3xOp3gNnSc1	jeho
kladně	kladně	k6eAd1	kladně
vyřízená	vyřízený	k2eAgFnSc1d1	vyřízená
žádost	žádost	k1gFnSc1	žádost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
o	o	k7c6	o
polepšení	polepšení	k1gNnSc6	polepšení
erbu	erb	k1gInSc2	erb
císařem	císař	k1gMnSc7	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mužem	muž	k1gMnSc7	muž
pevné	pevný	k2eAgFnSc2d1	pevná
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
i	i	k9	i
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
císařovo	císařův	k2eAgNnSc1d1	císařovo
zamítnutí	zamítnutí	k1gNnSc1	zamítnutí
mohlo	moct	k5eAaImAgNnS	moct
zhoršit	zhoršit	k5eAaPmF	zhoršit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Arundelem	Arundel	k1gMnSc7	Arundel
<g/>
,	,	kIx,	,
nekatolíkem	nekatolík	k1gMnSc7	nekatolík
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
ještě	ještě	k6eAd1	ještě
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
konvertovat	konvertovat	k5eAaBmF	konvertovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Arundelova	Arundelův	k2eAgFnSc1d1	Arundelův
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
katolická	katolický	k2eAgFnSc1d1	katolická
a	a	k8xC	a
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
komorná	komorná	k1gFnSc1	komorná
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
katoličkou	katolička	k1gFnSc7	katolička
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
záznam	záznam	k1gInSc1	záznam
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Hollar	Hollar	k1gInSc1	Hollar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1656	[number]	k4	1656
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Cromwella	Cromwella	k1gMnSc1	Cromwella
<g/>
,	,	kIx,	,
zadržen	zadržen	k2eAgMnSc1d1	zadržen
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
vracel	vracet	k5eAaImAgMnS	vracet
z	z	k7c2	z
kaple	kaple	k1gFnSc2	kaple
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejspíše	nejspíše	k9	nejspíše
katolické	katolický	k2eAgFnSc2d1	katolická
kaple	kaple	k1gFnSc2	kaple
zbudované	zbudovaný	k2eAgFnSc2d1	zbudovaná
u	u	k7c2	u
nějakého	nějaký	k3yIgNnSc2	nějaký
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
nebyl	být	k5eNaImAgMnS	být
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejplodnějších	plodný	k2eAgMnPc2d3	nejplodnější
umělců	umělec	k1gMnPc2	umělec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
okolo	okolo	k7c2	okolo
400	[number]	k4	400
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
3	[number]	k4	3
000	[number]	k4	000
leptů	lept	k1gInPc2	lept
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
generální	generální	k2eAgInSc4d1	generální
katalog	katalog	k1gInSc4	katalog
Hollarových	Hollarův	k2eAgInPc2d1	Hollarův
leptů	lept	k1gInPc2	lept
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
Gustav	Gustav	k1gMnSc1	Gustav
Parthey	Parthea	k1gFnSc2	Parthea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
tento	tento	k3xDgMnSc1	tento
katalog	katalog	k1gInSc4	katalog
revidoval	revidovat	k5eAaImAgMnS	revidovat
Richard	Richard	k1gMnSc1	Richard
Pennington	Pennington	k1gInSc4	Pennington
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
200	[number]	k4	200
rytin	rytina	k1gFnPc2	rytina
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
zařadil	zařadit	k5eAaPmAgInS	zařadit
225	[number]	k4	225
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgMnPc6	který
Parthey	Parthey	k1gInPc4	Parthey
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Penningtonův	Penningtonův	k2eAgInSc1d1	Penningtonův
katalog	katalog	k1gInSc1	katalog
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
z	z	k7c2	z
Partheyova	Partheyův	k2eAgInSc2d1	Partheyův
katalogu	katalog	k1gInSc2	katalog
jak	jak	k8xC	jak
dělení	dělení	k1gNnSc2	dělení
rytin	rytina	k1gFnPc2	rytina
podle	podle	k7c2	podle
tématu	téma	k1gNnSc2	téma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
číslování	číslování	k1gNnSc1	číslování
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
733	[number]	k4	733
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
doplněné	doplněný	k2eAgFnPc1d1	doplněná
rytiny	rytina	k1gFnPc1	rytina
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
připojena	připojen	k2eAgNnPc4d1	připojeno
písmena	písmeno	k1gNnPc4	písmeno
A	a	k9	a
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Soupis	soupis	k1gInSc4	soupis
kreseb	kresba	k1gFnPc2	kresba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Franz	Franz	k1gMnSc1	Franz
Sprinzels	Sprinzelsa	k1gFnPc2	Sprinzelsa
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc4	názor
na	na	k7c4	na
Hollarovo	Hollarův	k2eAgNnSc4d1	Hollarovo
dílo	dílo	k1gNnSc4	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
ceněna	ceněn	k2eAgFnSc1d1	ceněna
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úroveň	úroveň	k1gFnSc1	úroveň
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
detailnost	detailnost	k1gFnSc4	detailnost
a	a	k8xC	a
realismus	realismus	k1gInSc4	realismus
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ceněným	ceněný	k2eAgInSc7d1	ceněný
dokumentem	dokument	k1gInSc7	dokument
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Hollarových	Hollarův	k2eAgInPc2d1	Hollarův
uměleckých	umělecký	k2eAgInPc2d1	umělecký
začátků	začátek	k1gInPc2	začátek
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
známého	známý	k2eAgNnSc2d1	známé
vyjádření	vyjádření	k1gNnSc2	vyjádření
pod	pod	k7c7	pod
leptem	lept	k1gInSc7	lept
Hollar	Hollar	k1gInSc4	Hollar
s	s	k7c7	s
rytinou	rytina	k1gFnSc7	rytina
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
od	od	k7c2	od
mala	mal	k1gInSc2	mal
měl	mít	k5eAaImAgInS	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
malbě	malba	k1gFnSc6	malba
miniatur	miniatura	k1gFnPc2	miniatura
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
první	první	k4xOgNnSc4	první
známé	známý	k2eAgNnSc4d1	známé
dílo	dílo	k1gNnSc4	dílo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1626	[number]	k4	1626
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lept	lept	k1gInSc4	lept
Maria	Mario	k1gMnSc2	Mario
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
podle	podle	k7c2	podle
Dürera	Dürero	k1gNnSc2	Dürero
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
leptů	lept	k1gInPc2	lept
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Svatá	svatý	k2eAgFnSc1d1	svatá
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
133	[number]	k4	133
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
Josepha	Joseph	k1gMnSc2	Joseph
Heintze	Heintze	k1gFnSc1	Heintze
a	a	k8xC	a
Krajina	Krajina	k1gFnSc1	Krajina
s	s	k7c7	s
převislým	převislý	k2eAgInSc7d1	převislý
stromem	strom	k1gInSc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gMnSc1	Hollar
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
zabýval	zabývat	k5eAaImAgMnS	zabývat
širokou	široký	k2eAgFnSc7d1	široká
paletou	paleta	k1gFnSc7	paleta
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnPc1	jeho
topografická	topografický	k2eAgNnPc1d1	topografické
díla	dílo	k1gNnPc1	dílo
zachycující	zachycující	k2eAgNnPc1d1	zachycující
krajinu	krajina	k1gFnSc4	krajina
a	a	k8xC	a
města	město	k1gNnPc4	město
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
podoba	podoba	k1gFnSc1	podoba
Londýna	Londýn	k1gInSc2	Londýn
před	před	k7c7	před
velkým	velký	k2eAgInSc7d1	velký
požárem	požár	k1gInSc7	požár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárňoval	ztvárňovat	k5eAaImAgInS	ztvárňovat
také	také	k9	také
portréty	portrét	k1gInPc4	portrét
svatých	svatý	k1gMnPc2	svatý
a	a	k8xC	a
známých	známý	k2eAgMnPc2d1	známý
lidí	člověk	k1gMnPc2	člověk
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
motivy	motiv	k1gInPc4	motiv
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
historické	historický	k2eAgFnSc2d1	historická
scény	scéna	k1gFnSc2	scéna
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
erby	erb	k1gInPc1	erb
<g/>
,	,	kIx,	,
karikatury	karikatura	k1gFnPc1	karikatura
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
zátiší	zátiší	k1gNnPc1	zátiší
<g/>
,	,	kIx,	,
alegorie	alegorie	k1gFnPc1	alegorie
<g/>
,	,	kIx,	,
módu	mód	k1gInSc6	mód
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
cyklus	cyklus	k1gInSc1	cyklus
Aula	aula	k1gFnSc1	aula
veneris	veneris	k1gFnSc1	veneris
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Rytina	rytina	k1gFnSc1	rytina
jeho	jeho	k3xOp3gFnSc2	jeho
kočky	kočka	k1gFnSc2	kočka
s	s	k7c7	s
poučným	poučný	k2eAgInSc7d1	poučný
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
koczka	koczka	k1gFnSc1	koczka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůbec	vůbec	k9	vůbec
jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
samostatným	samostatný	k2eAgInSc7d1	samostatný
obrazem	obraz	k1gInSc7	obraz
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
grafický	grafický	k2eAgInSc1d1	grafický
list	list	k1gInSc1	list
se	s	k7c7	s
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
také	také	k9	také
Pudl	pudl	k1gMnSc1	pudl
-	-	kIx~	-
Boloňský	boloňský	k2eAgMnSc1d1	boloňský
psík	psík	k1gMnSc1	psík
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
2097	[number]	k4	2097
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
velmi	velmi	k6eAd1	velmi
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
dokumentace	dokumentace	k1gFnSc2	dokumentace
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
Arundelovy	Arundelův	k2eAgFnSc2d1	Arundelův
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
rytiny	rytina	k1gFnPc1	rytina
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označeny	označit	k5eAaPmNgInP	označit
ex	ex	k6eAd1	ex
collectione	collection	k1gInSc5	collection
Arundeliana	Arundeliana	k1gFnSc1	Arundeliana
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
velmi	velmi	k6eAd1	velmi
zdobný	zdobný	k2eAgInSc4d1	zdobný
např.	např.	kA	např.
Mantegnův	Mantegnův	k2eAgInSc4d1	Mantegnův
kalich	kalich	k1gInSc4	kalich
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
2643	[number]	k4	2643
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
také	také	k9	také
použil	použít	k5eAaPmAgMnS	použít
na	na	k7c4	na
cyklus	cyklus	k1gInSc4	cyklus
Londýnské	londýnský	k2eAgInPc4d1	londýnský
roční	roční	k2eAgInPc4d1	roční
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
podstatný	podstatný	k2eAgInSc4d1	podstatný
cyklus	cyklus	k1gInSc4	cyklus
lodí	loď	k1gFnPc2	loď
Navium	Navium	k1gNnSc4	Navium
variae	variae	k1gNnSc2	variae
figurae	figura	k1gMnSc2	figura
et	et	k?	et
formae	forma	k1gMnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
rytin	rytina	k1gFnPc2	rytina
je	být	k5eAaImIp3nS	být
Hollarova	Hollarův	k2eAgFnSc1d1	Hollarova
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
označováno	označován	k2eAgNnSc4d1	označováno
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
hrobu	hrob	k1gInSc2	hrob
Eduarda	Eduard	k1gMnSc2	Eduard
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
2282	[number]	k4	2282
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
sbírky	sbírka	k1gFnPc1	sbírka
Hollarových	Hollarův	k2eAgNnPc2d1	Hollarovo
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
Windsorském	windsorský	k2eAgInSc6d1	windsorský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Hollarova	Hollarův	k2eAgNnSc2d1	Hollarovo
díla	dílo	k1gNnSc2	dílo
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
Hollareum	Hollareum	k1gNnSc4	Hollareum
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
ve	v	k7c6	v
Fisher	Fishra	k1gFnPc2	Fishra
Library	Librara	k1gFnSc2	Librara
patřící	patřící	k2eAgFnSc2d1	patřící
k	k	k7c3	k
University	universita	k1gFnPc4	universita
of	of	k?	of
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Nemalé	malý	k2eNgFnPc1d1	nemalá
sbírky	sbírka	k1gFnPc1	sbírka
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
Albertině	Albertin	k2eAgInSc6d1	Albertin
a	a	k8xC	a
kabinetě	kabinet	k1gInSc6	kabinet
rytin	rytina	k1gFnPc2	rytina
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
prospekt	prospekt	k1gInSc1	prospekt
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
Petřína	Petřín	k1gInSc2	Petřín
Antverpská	antverpský	k2eAgFnSc1d1	Antverpská
katedrála	katedrála	k1gFnSc1	katedrála
Velký	velký	k2eAgInSc1d1	velký
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Mohuč	Mohuč	k1gFnSc4	Mohuč
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
kočka	kočka	k1gFnSc1	kočka
která	který	k3yIgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
K	k	k7c3	k
Hollarovu	Hollarův	k2eAgInSc3d1	Hollarův
odkazu	odkaz	k1gInSc3	odkaz
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
T.	T.	kA	T.
F.	F.	kA	F.
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
,	,	kIx,	,
když	když	k8xS	když
založil	založit	k5eAaPmAgInS	založit
spolek	spolek	k1gInSc1	spolek
grafiků	grafik	k1gMnPc2	grafik
SČUG	SČUG	kA	SČUG
Hollar	Hollar	k1gInSc4	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zaměřující	zaměřující	k2eAgFnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
design	design	k1gInSc4	design
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
si	se	k3xPyFc3	se
do	do	k7c2	do
názvu	název	k1gInSc2	název
zvolila	zvolit	k5eAaPmAgFnS	zvolit
jméno	jméno	k1gNnSc4	jméno
Hollara	Hollar	k1gMnSc2	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Hollarovu	Hollarův	k2eAgFnSc4d1	Hollarova
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
kočku	kočka	k1gFnSc4	kočka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
navázal	navázat	k5eAaPmAgInS	navázat
Miloš	Miloš	k1gMnSc1	Miloš
Václav	Václav	k1gMnSc1	Václav
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
název	název	k1gInSc1	název
historického	historický	k2eAgInSc2d1	historický
románu	román	k1gInSc2	román
o	o	k7c6	o
Hollarově	Hollarův	k2eAgInSc6d1	Hollarův
životě	život	k1gInSc6	život
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
Lucie	Lucie	k1gFnSc1	Lucie
vydala	vydat	k5eAaPmAgFnS	vydat
desku	deska	k1gFnSc4	deska
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
kočzka	kočzka	k1gFnSc1	kočzka
která	který	k3yIgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
jako	jako	k9	jako
reminiscenci	reminiscence	k1gFnSc4	reminiscence
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
ke	k	k7c3	k
kulatému	kulatý	k2eAgNnSc3d1	kulaté
výročí	výročí	k1gNnSc3	výročí
umělcova	umělcův	k2eAgNnSc2d1	umělcovo
narození	narození	k1gNnSc2	narození
vydala	vydat	k5eAaPmAgFnS	vydat
česká	český	k2eAgFnSc1d1	Česká
pošta	pošta	k1gFnSc1	pošta
aršík	aršík	k1gInSc1	aršík
Světový	světový	k2eAgMnSc1d1	světový
grafik	grafik	k1gMnSc1	grafik
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
v	v	k7c6	v
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
35	[number]	k4	35
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Lepty	lept	k1gInPc1	lept
z	z	k7c2	z
Hollarova	Hollarův	k2eAgNnSc2d1	Hollarovo
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
aršík	aršík	k1gInSc4	aršík
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
a	a	k8xC	a
v	v	k7c6	v
ocelorytu	oceloryt	k1gInSc6	oceloryt
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
Bedřich	Bedřich	k1gMnSc1	Bedřich
Housa	Hous	k1gMnSc2	Hous
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Jaro	jaro	k6eAd1	jaro
a	a	k8xC	a
Zima	zima	k6eAd1	zima
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
roční	roční	k2eAgFnSc2d1	roční
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
i	i	k9	i
Stanislav	Stanislav	k1gMnSc1	Stanislav
Richter	Richter	k1gMnSc1	Richter
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
Hollarova	Hollarův	k2eAgNnSc2d1	Hollarovo
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
Hollarův	Hollarův	k2eAgInSc1d1	Hollarův
autoportrét	autoportrét	k1gInSc1	autoportrét
podle	podle	k7c2	podle
Meyssense	Meyssense	k1gFnSc2	Meyssense
a	a	k8xC	a
pod	pod	k7c7	pod
známkami	známka	k1gFnPc7	známka
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gFnSc2	jeho
kresby	kresba	k1gFnSc2	kresba
<g/>
.	.	kIx.	.
</s>
<s>
TINDALL	TINDALL	kA	TINDALL
<g/>
,	,	kIx,	,
Gillian	Gillian	k1gInSc1	Gillian
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Londýn	Londýn	k1gInSc4	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1710	[number]	k4	1710
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
1607	[number]	k4	1607
<g/>
-	-	kIx~	-
<g/>
1677	[number]	k4	1677
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
mezi	mezi	k7c7	mezi
životem	život	k1gInSc7	život
a	a	k8xC	a
zmarem	zmar	k1gInSc7	zmar
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Alena	Alena	k1gFnSc1	Alena
Volrábová	Volrábová	k1gFnSc1	Volrábová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7035	[number]	k4	7035
<g/>
-	-	kIx~	-
<g/>
361	[number]	k4	361
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
GODFREY	GODFREY	kA	GODFREY
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
T.	T.	kA	T.
Wenceslaus	Wenceslaus	k1gMnSc1	Wenceslaus
Hollar	Hollar	k1gMnSc1	Hollar
:	:	kIx,	:
A	A	kA	A
Bohemian	bohemian	k1gInSc1	bohemian
Artists	Artists	k1gInSc1	Artists
in	in	k?	in
England	England	k1gInSc1	England
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
;	;	kIx,	;
London	London	k1gMnSc1	London
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6166	[number]	k4	6166
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Kotalík	Kotalík	k1gMnSc1	Kotalík
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Kesnerová	Kesnerová	k1gFnSc1	Kesnerová
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc1	anton
Griffiths	Griffiths	k1gInSc1	Griffiths
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnPc1	kresba
a	a	k8xC	a
grafické	grafický	k2eAgInPc1d1	grafický
listy	list	k1gInPc1	list
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Britského	britský	k2eAgNnSc2d1	Britské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1983	[number]	k4	1983
FELDKAMP	FELDKAMP	kA	FELDKAMP
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
F.	F.	kA	F.
<g/>
.	.	kIx.	.
</s>
<s>
Anmerkungen	Anmerkungen	k1gInSc1	Anmerkungen
zum	zum	k?	zum
Stadtplan	Stadtplan	k1gInSc1	Stadtplan
Osnabrücks	Osnabrücksa	k1gFnPc2	Osnabrücksa
von	von	k1gInSc1	von
Wenzel	Wenzel	k1gMnSc1	Wenzel
Hollar	Hollar	k1gMnSc1	Hollar
aus	aus	k?	aus
dem	dem	k?	dem
Jahre	Jahr	k1gInSc5	Jahr
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
</s>
<s>
Osnabrück	Osnabrück	k1gInSc1	Osnabrück
:	:	kIx,	:
Osnabrücker	Osnabrücker	k1gInSc1	Osnabrücker
Mitteilungen	Mitteilungen	k1gInSc1	Mitteilungen
88	[number]	k4	88
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S.	S.	kA	S.
230	[number]	k4	230
<g/>
-	-	kIx~	-
<g/>
233	[number]	k4	233
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
němčina	němčina	k1gFnSc1	němčina
<g/>
)	)	kIx)	)
PENNINGTON	PENNINGTON	kA	PENNINGTON
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Descriptive	Descriptiv	k1gInSc5	Descriptiv
Catalogue	Catalogue	k1gNnSc2	Catalogue
of	of	k?	of
the	the	k?	the
Etched	Etched	k1gMnSc1	Etched
Work	Work	k1gMnSc1	Work
of	of	k?	of
Wenceslaus	Wenceslaus	k1gMnSc1	Wenceslaus
Hollar	Hollar	k1gMnSc1	Hollar
1607	[number]	k4	1607
<g/>
-	-	kIx~	-
<g/>
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
52122408	[number]	k4	52122408
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Denkstein	Denkstein	k1gMnSc1	Denkstein
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
RICHTER	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
:	:	kIx,	:
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
URZIDIL	URZIDIL	kA	URZIDIL
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Hollar	Hollar	k1gInSc1	Hollar
:	:	kIx,	:
A	a	k9	a
Czech	Czech	k1gMnSc1	Czech
Emigré	Emigrý	k2eAgFnSc2d1	Emigrý
in	in	k?	in
England	Englanda	k1gFnPc2	Englanda
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
The	The	k1gMnSc1	The
Czechoslovak	Czechoslovak	k1gMnSc1	Czechoslovak
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
SPRINZELS	SPRINZELS	kA	SPRINZELS
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
<g/>
.	.	kIx.	.
</s>
<s>
Hollars	Hollars	k6eAd1	Hollars
Handzeichnungen	Handzeichnungen	k1gInSc1	Handzeichnungen
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
:	:	kIx,	:
Passer	Passer	k1gMnSc1	Passer
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
URZIDIL	URZIDIL	kA	URZIDIL
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
:	:	kIx,	:
Umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
světoobčan	světoobčan	k1gMnSc1	světoobčan
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
URZIDIL	URZIDIL	kA	URZIDIL
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Wenceslaus	Wenceslaus	k1gInSc1	Wenceslaus
Hollar	Hollara	k1gFnPc2	Hollara
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Kupferstecher	Kupferstechra	k1gFnPc2	Kupferstechra
des	des	k1gNnSc1	des
Barock	Barock	k1gMnSc1	Barock
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
;	;	kIx,	;
Leipzig	Leipzig	k1gMnSc1	Leipzig
:	:	kIx,	:
Rolf	Rolf	k1gMnSc1	Rolf
Passer	Passer	k1gMnSc1	Passer
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
němčina	němčina	k1gFnSc1	němčina
<g/>
)	)	kIx)	)
DOSTÁL	Dostál	k1gMnSc1	Dostál
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Štenc	Štenc	k1gFnSc1	Štenc
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
HIND	hind	k1gMnSc1	hind
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
M.	M.	kA	M.
Wenceslaus	Wenceslaus	k1gMnSc1	Wenceslaus
Hollar	Hollar	k1gMnSc1	Hollar
and	and	k?	and
his	his	k1gNnSc2	his
views	viewsa	k1gFnPc2	viewsa
of	of	k?	of
London	London	k1gMnSc1	London
and	and	k?	and
Windsor	Windsor	k1gInSc1	Windsor
in	in	k?	in
the	the	k?	the
seventteenth	seventteenth	k1gInSc4	seventteenth
century	centura	k1gFnSc2	centura
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
John	John	k1gMnSc1	John
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
DOLENSKÝ	DOLENSKÝ	kA	DOLENSKÝ
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Veraikon	veraikon	k1gInSc1	veraikon
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
PARTHEY	PARTHEY	kA	PARTHEY
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Wenzel	Wenzet	k5eAaPmAgMnS	Wenzet
Hollar	Hollar	k1gMnSc1	Hollar
:	:	kIx,	:
beschreibendes	beschreibendes	k1gMnSc1	beschreibendes
Verzeichniss	Verzeichnissa	k1gFnPc2	Verzeichnissa
seiner	seiner	k1gMnSc1	seiner
Kupferstiche	Kupferstich	k1gFnSc2	Kupferstich
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
:	:	kIx,	:
Nicolaische	Nicolaische	k1gInSc1	Nicolaische
Buchhandlung	Buchhandlunga	k1gFnPc2	Buchhandlunga
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
VERTUE	VERTUE	kA	VERTUE
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Wenceslaus	Wenceslaus	k1gMnSc1	Wenceslaus
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1756	[number]	k4	1756
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
škola	škola	k1gFnSc1	škola
Václava	Václav	k1gMnSc2	Václav
Hollara	Hollar	k1gMnSc2	Hollar
Galerie	galerie	k1gFnSc1	galerie
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollara	k1gFnPc2	Hollara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Osoba	osoba	k1gFnSc1	osoba
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
Digitální	digitální	k2eAgFnSc2d1	digitální
galerie	galerie	k1gFnSc2	galerie
děl	dělo	k1gNnPc2	dělo
Václava	Václav	k1gMnSc2	Václav
Hollara	Hollar	k1gMnSc2	Hollar
na	na	k7c6	na
serveru	server	k1gInSc6	server
Torontské	torontský	k2eAgFnSc2d1	Torontská
univerzity	univerzita	k1gFnSc2	univerzita
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
-	-	kIx~	-
článek	článek	k1gInSc1	článek
v	v	k7c6	v
Reflexu	reflex	k1gInSc6	reflex
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
umělcovy	umělcův	k2eAgFnSc2d1	umělcova
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
†	†	k?	†
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemlsá	mlsat	k5eNaImIp3nS	mlsat
-	-	kIx~	-
Toulky	toulka	k1gFnSc2	toulka
českou	český	k2eAgFnSc7d1	Česká
minulostí	minulost	k1gFnSc7	minulost
473	[number]	k4	473
<g/>
.	.	kIx.	.
schůzka	schůzka	k1gFnSc1	schůzka
Výstava	výstava	k1gFnSc1	výstava
grafického	grafický	k2eAgNnSc2d1	grafické
díla	dílo	k1gNnSc2	dílo
Václava	Václav	k1gMnSc2	Václav
Hollara	Hollar	k1gMnSc2	Hollar
-	-	kIx~	-
Galerie	galerie	k1gFnSc1	galerie
ART	ART	kA	ART
Chrudim	Chrudim	k1gFnSc1	Chrudim
-	-	kIx~	-
http://www.galerieart.cz/hollar_vystava_2015.htm	[url]	k1gInSc1	http://www.galerieart.cz/hollar_vystava_2015.htm
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
v	v	k7c6	v
Deutsche	Deutsche	k1gFnSc6	Deutsche
Biographie	Biographie	k1gFnSc2	Biographie
</s>
