<s>
Nejmenším	malý	k2eAgMnSc7d3	nejmenší
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
korela	korela	k1gFnSc1	korela
chocholatá	chocholatý	k2eAgFnSc1d1	chocholatá
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
kakadu	kakadu	k1gMnPc3	kakadu
je	být	k5eAaImIp3nS	být
kakadu	kakadu	k1gMnSc1	kakadu
arový	arový	k2eAgMnSc1d1	arový
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hmotnosti	hmotnost	k1gFnSc3	hmotnost
až	až	k9	až
1	[number]	k4	1
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
