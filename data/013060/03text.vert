<p>
<s>
Matúš	Matúš	k1gMnSc1	Matúš
Dulla	Dulla	k1gMnSc1	Dulla
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
architekt	architekt	k1gMnSc1	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dějin	dějiny	k1gFnPc2	dějiny
architektury	architektura	k1gFnSc2	architektura
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kritiky	kritika	k1gFnSc2	kritika
současné	současný	k2eAgFnSc2d1	současná
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
metod	metoda	k1gFnPc2	metoda
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Architekturu	architektura	k1gFnSc4	architektura
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
na	na	k7c6	na
SVŠT	SVŠT	kA	SVŠT
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
SAV	SAV	kA	SAV
obhájil	obhájit	k5eAaPmAgInS	obhájit
kandidátskou	kandidátský	k2eAgFnSc4d1	kandidátská
disertaci	disertace	k1gFnSc4	disertace
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
STU	sto	k4xCgNnSc6	sto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
architektury	architektura	k1gFnSc2	architektura
STU	sto	k4xCgNnSc3	sto
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
na	na	k7c4	na
docenta	docent	k1gMnSc4	docent
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesorem	profesor	k1gMnSc7	profesor
STU	sto	k4xCgNnSc3	sto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
Katedru	katedra	k1gFnSc4	katedra
dějin	dějiny	k1gFnPc2	dějiny
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Ústav	ústav	k1gInSc1	ústav
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
architektury	architektura	k1gFnSc2	architektura
FA	fa	k1gNnSc6	fa
STU	sto	k4xCgNnSc6	sto
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
i	i	k9	i
na	na	k7c6	na
VŠVU	VŠVU	kA	VŠVU
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
TU	tu	k6eAd1	tu
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
SAV	SAV	kA	SAV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Publikoval	publikovat	k5eAaBmAgInS	publikovat
řadu	řada	k1gFnSc4	řada
knižních	knižní	k2eAgFnPc2d1	knižní
monografií	monografie	k1gFnPc2	monografie
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
studií	studie	k1gFnPc2	studie
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
dějiny	dějiny	k1gFnPc4	dějiny
architektury	architektura	k1gFnSc2	architektura
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
řady	řada	k1gFnSc2	řada
domácích	domácí	k2eAgFnPc2d1	domácí
a	a	k8xC	a
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
výstav	výstava	k1gFnPc2	výstava
slovenské	slovenský	k2eAgFnSc2d1	slovenská
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Autorsky	autorsky	k6eAd1	autorsky
se	se	k3xPyFc4	se
spolupodílel	spolupodílet	k5eAaImAgInS	spolupodílet
na	na	k7c6	na
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
o	o	k7c6	o
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
časopise	časopis	k1gInSc6	časopis
SAV	SAV	kA	SAV
Architektura	architektura	k1gFnSc1	architektura
&	&	k?	&
amp	amp	k?	amp
<g/>
;	;	kIx,	;
Urbanismus	urbanismus	k1gInSc1	urbanismus
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
redakčních	redakční	k2eAgFnPc2d1	redakční
rad	rada	k1gFnPc2	rada
odborných	odborný	k2eAgInPc2d1	odborný
časopisů	časopis	k1gInPc2	časopis
Arch	arch	k1gInSc1	arch
a	a	k8xC	a
Alfa	alfa	k1gFnSc1	alfa
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
rad	rada	k1gFnPc2	rada
(	(	kIx(	(
<g/>
FA	fa	kA	fa
STU	sto	k4xCgNnSc3	sto
<g/>
,	,	kIx,	,
ÚSTARCH	ÚSTARCH	kA	ÚSTARCH
SAV	SAV	kA	SAV
<g/>
,	,	kIx,	,
VŠVU	VŠVU	kA	VŠVU
<g/>
)	)	kIx)	)
a	a	k8xC	a
grantových	grantový	k2eAgFnPc2d1	Grantová
komisí	komise	k1gFnPc2	komise
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
kolegií	kolegium	k1gNnPc2	kolegium
SAV	SAV	kA	SAV
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Ceny	cena	k1gFnSc2	cena
Martina	Martina	k1gFnSc1	Martina
Kusého	kusý	k2eAgInSc2d1	kusý
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc1	cena
Dušana	Dušan	k1gMnSc2	Dušan
Jurkoviče	Jurkovič	k1gMnSc2	Jurkovič
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc1	cena
SAV	SAV	kA	SAV
za	za	k7c4	za
vědecko-popularizační	vědeckoopularizační	k2eAgFnSc4d1	vědecko-popularizační
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ceny	cena	k1gFnPc1	cena
literárního	literární	k2eAgInSc2d1	literární
fondu	fond	k1gInSc2	fond
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Medaile	medaile	k1gFnSc1	medaile
Chatama	Chatama	k?	Chatama
Sofera	Sofera	k1gFnSc1	Sofera
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
Ústavu	ústav	k1gInSc2	ústav
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
dějin	dějiny	k1gFnPc2	dějiny
architektury	architektura	k1gFnSc2	architektura
na	na	k7c4	na
FA	fa	k1gNnSc4	fa
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
KnihyDULLA	KnihyDULLA	k?	KnihyDULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
architektúra	architektúra	k1gFnSc1	architektúra
od	od	k7c2	od
Jurkoviča	Jurkovič	k1gInSc2	Jurkovič
po	po	k7c4	po
dnešok	dnešok	k1gInSc4	dnešok
<g/>
.	.	kIx.	.
</s>
<s>
Perfekt	perfektum	k1gNnPc2	perfektum
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
196	[number]	k4	196
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MORAVČÍKOVÁ	MORAVČÍKOVÁ	kA	MORAVČÍKOVÁ
<g/>
,	,	kIx,	,
H.	H.	kA	H.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Architektúra	Architektúra	k1gFnSc1	Architektúra
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
–	–	k?	–
stručné	stručný	k2eAgInPc1d1	stručný
dejiny	dejin	k1gInPc1	dejin
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovart	Slovart	k1gInSc1	Slovart
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
181	[number]	k4	181
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-8085-079-8	[number]	k4	80-8085-079-8
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Majstri	Majstri	k1gNnSc1	Majstri
architektúry	architektúra	k1gFnSc2	architektúra
<g/>
.	.	kIx.	.
</s>
<s>
Perfekt	perfektum	k1gNnPc2	perfektum
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
8046	[number]	k4	8046
<g/>
-	-	kIx~	-
<g/>
312	[number]	k4	312
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
MORAVČÍKOVÁ	MORAVČÍKOVÁ	kA	MORAVČÍKOVÁ
<g/>
,	,	kIx,	,
H.	H.	kA	H.
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Architektúra	Architektúr	k1gInSc2	Architektúr
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c4	v
dvadsiatom	dvadsiatom	k1gInSc4	dvadsiatom
storočí	storočí	k1gNnSc2	storočí
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovart	Slovart	k1gInSc1	Slovart
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
514	[number]	k4	514
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
spolupráca	spoluprácus	k1gMnSc2	spoluprácus
H.	H.	kA	H.
H.	H.	kA	H.
Moravčíková	Moravčíková	k1gFnSc1	Moravčíková
a	a	k8xC	a
B.	B.	kA	B.
Hochel	Hochel	k1gInSc1	Hochel
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vojenské	vojenský	k2eAgInPc1d1	vojenský
cintoríny	cintorín	k1gInPc1	cintorín
v	v	k7c6	v
západnej	západnat	k5eAaPmRp2nS	západnat
Haliči	halič	k1gMnSc6	halič
<g/>
.	.	kIx.	.
</s>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
1916	[number]	k4	1916
<g/>
/	/	kIx~	/
<g/>
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Sprievodca	Sprievodca	k6eAd1	Sprievodca
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
VŠVU	VŠVU	kA	VŠVU
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
139	[number]	k4	139
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekt	architekt	k1gMnSc1	architekt
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Milučký	Milučký	k2eAgMnSc1d1	Milučký
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekti	architekt	k1gMnPc1	architekt
Jozef	Jozef	k1gMnSc1	Jozef
Ondriaš	Ondriaš	k1gMnSc1	Ondriaš
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Závodný	Závodný	k2eAgMnSc1d1	Závodný
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Meritum	meritum	k1gNnSc1	meritum
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekt	architekt	k1gMnSc1	architekt
Peter	Peter	k1gMnSc1	Peter
Pásztor	Pásztor	k1gMnSc1	Pásztor
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Meritum	meritum	k1gNnSc1	meritum
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
MORAVČÍKOVÁ	MORAVČÍKOVÁ	kA	MORAVČÍKOVÁ
<g/>
,	,	kIx,	,
H.	H.	kA	H.
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Kto	Kto	k1gMnSc2	Kto
je	být	k5eAaImIp3nS	být
kto	kto	k?	kto
v	v	k7c6	v
architektúre	architektúr	k1gInSc5	architektúr
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc3	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Meritum	meritum	k1gNnSc1	meritum
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
189	[number]	k4	189
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekt	architekt	k1gMnSc1	architekt
Ivan	Ivan	k1gMnSc1	Ivan
Marko	Marko	k1gMnSc1	Marko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Meritum	meritum	k1gNnSc1	meritum
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekt	architekt	k1gMnSc1	architekt
Ján	Ján	k1gMnSc1	Ján
Bahna	bahno	k1gNnSc2	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Meritum	meritum	k1gNnSc1	meritum
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architektúra	Architektúra	k1gMnSc1	Architektúra
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
architektúra	architektúra	k1gFnSc1	architektúra
80	[number]	k4	80
<g/>
.	.	kIx.	.
rokov	rokov	k1gInSc1	rokov
a	a	k8xC	a
jej	on	k3xPp3gInSc4	on
súvislosti	súvislost	k1gFnPc4	súvislost
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Pallas	Pallas	k1gMnSc1	Pallas
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
224	[number]	k4	224
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZALČÍK	ZALČÍK	kA	ZALČÍK
<g/>
,	,	kIx,	,
T.	T.	kA	T.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
architektúra	architektúra	k1gFnSc1	architektúra
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Veda	vést	k5eAaImSgInS	vést
<g/>
,	,	kIx,	,
vydavateľstvo	vydavateľstvo	k1gNnSc1	vydavateľstvo
Slovenskej	Slovenskej	k?	Slovenskej
akadémie	akadémie	k1gFnSc1	akadémie
vied	viedo	k1gNnPc2	viedo
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
192	[number]	k4	192
<g/>
.	.	kIx.	.
<g/>
KatalogyDULLA	KatalogyDULLA	k1gFnSc2	KatalogyDULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architektur	architektura	k1gFnPc2	architektura
der	drát	k5eAaImRp2nS	drát
Slowakei	Slowakei	k1gNnSc4	Slowakei
heute	heuit	k5eAaImRp2nP	heuit
<g/>
.	.	kIx.	.
</s>
<s>
Architektúra	Architektúra	k1gFnSc1	Architektúra
Slovenska	Slovensko	k1gNnSc2	Slovensko
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bratislava	Bratislava	k1gFnSc1	Bratislava
-	-	kIx~	-
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
<g/>
,	,	kIx,	,
Slowakisches	Slowakisches	k1gMnSc1	Slowakisches
Institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Milučký	Milučký	k2eAgMnSc1d1	Milučký
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgNnSc4d1	architektonické
dielo	dielo	k1gNnSc4	dielo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOŘUTOVÁ	BOŘUTOVÁ	kA	BOŘUTOVÁ
<g/>
,	,	kIx,	,
D.	D.	kA	D.
-	-	kIx~	-
ZAJKOVÁ	ZAJKOVÁ	kA	ZAJKOVÁ
<g/>
,	,	kIx,	,
A.	A.	kA	A.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
.	.	kIx.	.
</s>
<s>
Súborná	Súborný	k2eAgFnSc1d1	Súborný
výstava	výstava	k1gFnSc1	výstava
architektonického	architektonický	k2eAgNnSc2d1	architektonické
diela	dielo	k1gNnSc2	dielo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
245	[number]	k4	245
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
Belluš	Belluš	k1gMnSc1	Belluš
<g/>
.	.	kIx.	.
</s>
<s>
Regionálna	Regionálna	k1gFnSc1	Regionálna
moderna	moderna	k1gFnSc1	moderna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
slovenská	slovenský	k2eAgFnSc1d1	slovenská
architektúra	architektúra	k1gFnSc1	architektúra
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
SAS	Sas	k1gMnSc1	Sas
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
84	[number]	k4	84
<g/>
.	.	kIx.	.
<g/>
VýstavyDULLA	VýstavyDULLA	k1gFnSc2	VýstavyDULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Milučký	Milučký	k2eAgMnSc1d1	Milučký
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
inštitút	inštitút	k1gInSc1	inštitút
<g/>
,	,	kIx,	,
Viedeň	Viedeň	k1gFnSc1	Viedeň
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
spolupráca	spoluprác	k2eAgFnSc1d1	spoluprác
Moravčíková	Moravčíková	k1gFnSc1	Moravčíková
<g/>
,	,	kIx,	,
H.	H.	kA	H.
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Something	Something	k1gInSc1	Something
-	-	kIx~	-
new	new	k?	new
-	-	kIx~	-
architecture	architectur	k1gMnSc5	architectur
-	-	kIx~	-
capital	capital	k1gMnSc1	capital
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
-	-	kIx~	-
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
.	.	kIx.	.
</s>
<s>
UIA	UIA	kA	UIA
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Bořutová	Bořutový	k2eAgFnSc1d1	Bořutová
<g/>
,	,	kIx,	,
D.	D.	kA	D.
-	-	kIx~	-
Zajková	Zajková	k1gFnSc1	Zajková
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
-	-	kIx~	-
súborná	súborný	k2eAgFnSc1d1	súborný
výstava	výstava	k1gFnSc1	výstava
architektonického	architektonický	k2eAgNnSc2d1	architektonické
diela	dielo	k1gNnSc2	dielo
<g/>
.	.	kIx.	.
</s>
<s>
SNG	SNG	kA	SNG
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
1993	[number]	k4	1993
-	-	kIx~	-
leden	leden	k1gInSc4	leden
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Milučký	Milučký	k2eAgMnSc1d1	Milučký
<g/>
,	,	kIx,	,
F.	F.	kA	F.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Milučký	Milučký	k2eAgMnSc1d1	Milučký
-	-	kIx~	-
architektonické	architektonický	k2eAgNnSc1d1	architektonické
dielo	dielo	k1gNnSc1	dielo
<g/>
.	.	kIx.	.
</s>
<s>
Slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
nár	nár	k?	nár
<g/>
.	.	kIx.	.
múzeum	múzeum	k1gNnSc1	múzeum
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
-	-	kIx~	-
srpen	srpen	k1gInSc1	srpen
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Emil	Emil	k1gMnSc1	Emil
Belluš	Belluš	k1gMnSc1	Belluš
-	-	kIx~	-
regionálna	regionálna	k1gFnSc1	regionálna
moderna	moderna	k1gFnSc1	moderna
<g/>
.	.	kIx.	.
</s>
<s>
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
,	,	kIx,	,
září	zářit	k5eAaImIp3nS	zářit
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Talaš	Talaš	k1gMnSc1	Talaš
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
:	:	kIx,	:
Desať	Desať	k1gMnSc2	Desať
krokov	krokov	k1gInSc1	krokov
slovenskej	slovenskej	k?	slovenskej
architektúry	architektúra	k1gFnSc2	architektúra
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
československá	československý	k2eAgFnSc1d1	Československá
výstava	výstava	k1gFnSc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architektúra	Architektúra	k1gFnSc1	Architektúra
Slovenska	Slovensko	k1gNnSc2	Slovensko
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
UIA	UIA	kA	UIA
Montreal	Montreal	k1gInSc1	Montreal
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
a	a	k8xC	a
i.	i.	k?	i.
<g/>
:	:	kIx,	:
Výstava	výstava	k1gFnSc1	výstava
tvorby	tvorba	k1gFnSc2	tvorba
mladých	mladý	k2eAgNnPc2d1	mladé
architektov	architektovo	k1gNnPc2	architektovo
<g/>
.	.	kIx.	.
</s>
<s>
FA	fa	kA	fa
SVŠT	SVŠT	kA	SVŠT
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
<g/>
FilmyMančuška	FilmyMančuška	k1gMnSc1	FilmyMančuška
<g/>
,	,	kIx,	,
D.	D.	kA	D.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Bořutová	Bořutový	k2eAgFnSc1d1	Bořutová
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Výšiny	výšina	k1gFnSc2	výšina
<g/>
.	.	kIx.	.
</s>
<s>
Dielo	Dielo	k1gNnSc1	Dielo
D.	D.	kA	D.
Jurkoviča	Jurkovič	k1gInSc2	Jurkovič
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architecture	Architectur	k1gMnSc5	Architectur
in	in	k?	in
Forum	forum	k1gNnSc1	forum
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
FA	fa	k1gNnSc1	fa
STU	sto	k4xCgNnSc3	sto
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Architekt	architekt	k1gMnSc1	architekt
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Milučký	Milučký	k2eAgMnSc1d1	Milučký
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
televízia	televízia	k1gFnSc1	televízia
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bahna	bahno	k1gNnPc1	bahno
<g/>
,	,	kIx,	,
J.	J.	kA	J.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
:	:	kIx,	:
Dialógy	Dialóga	k1gFnSc2	Dialóga
architektúry	architektúra	k1gFnSc2	architektúra
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
televízia	televízia	k1gFnSc1	televízia
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zalčík	Zalčík	k1gMnSc1	Zalčík
<g/>
,	,	kIx,	,
T.	T.	kA	T.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
architektúra	architektúra	k1gFnSc1	architektúra
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
televízia	televízia	k1gFnSc1	televízia
Bratislava	Bratislava	k1gFnSc1	Bratislava
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zalčík	Zalčík	k1gMnSc1	Zalčík
<g/>
,	,	kIx,	,
T.	T.	kA	T.
-	-	kIx~	-
DULLA	DULLA	kA	DULLA
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Obraz	obraz	k1gInSc1	obraz
mesta	mest	k1gInSc2	mest
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Matúš	Matúš	k1gMnSc1	Matúš
Dulla	Dulla	k1gMnSc1	Dulla
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Matúš	Matúš	k1gMnSc1	Matúš
Dulla	Dulla	k1gMnSc1	Dulla
</s>
</p>
