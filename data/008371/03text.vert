<p>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
souvislá	souvislý	k2eAgFnSc1d1	souvislá
pevnina	pevnina	k1gFnSc1	pevnina
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
patrných	patrný	k2eAgFnPc2d1	patrná
částí	část	k1gFnPc2	část
–	–	k?	–
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
kontinentu	kontinent	k1gInSc2	kontinent
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Amerika	Amerika	k1gFnSc1	Amerika
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
souvislou	souvislý	k2eAgFnSc7d1	souvislá
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
masou	masa	k1gFnSc7	masa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
litosférických	litosférický	k2eAgFnPc6d1	litosférická
deskách	deska	k1gFnPc6	deska
–	–	k?	–
Severoamerické	severoamerický	k2eAgNnSc4d1	severoamerické
<g/>
,	,	kIx,	,
Karibské	karibský	k2eAgNnSc4d1	Karibské
(	(	kIx(	(
<g/>
Středoamerické	středoamerický	k2eAgNnSc4d1	středoamerické
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihoamerické	jihoamerický	k2eAgFnPc1d1	jihoamerická
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
Ameriky	Amerika	k1gFnSc2	Amerika
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
i	i	k8xC	i
severní	severní	k2eAgFnSc6d1	severní
zemské	zemský	k2eAgFnSc6d1	zemská
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgNnSc7d1	tradiční
datem	datum	k1gNnSc7	datum
objevení	objevení	k1gNnSc2	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
Evropany	Evropan	k1gMnPc4	Evropan
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc1	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
tohoto	tento	k3xDgInSc2	tento
světadílu	světadíl	k1gInSc2	světadíl
pod	pod	k7c7	pod
španělskými	španělský	k2eAgFnPc7d1	španělská
vlajkami	vlajka	k1gFnPc7	vlajka
přirazila	přirazit	k5eAaPmAgFnS	přirazit
flotila	flotila	k1gFnSc1	flotila
vedená	vedený	k2eAgFnSc1d1	vedená
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
však	však	k9	však
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
půdu	půda	k1gFnSc4	půda
jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
Vikingové	Viking	k1gMnPc1	Viking
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
o	o	k7c4	o
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
fyzicko-geografického	fyzickoeografický	k2eAgNnSc2d1	fyzicko-geografický
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
od	od	k7c2	od
sebe	se	k3xPyFc2	se
dělí	dělit	k5eAaImIp3nP	dělit
Tehuantepecká	Tehuantepecká	k1gFnSc1	Tehuantepecká
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Panamská	panamský	k2eAgFnSc1d1	Panamská
šíje	šíje	k1gFnSc1	šíje
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Ameriky	Amerika	k1gFnSc2	Amerika
omývá	omývat	k5eAaImIp3nS	omývat
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
východní	východní	k2eAgInSc1d1	východní
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
světadíl	světadíl	k1gInSc1	světadíl
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
též	též	k9	též
do	do	k7c2	do
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
8,3	[number]	k4	8,3
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
28,4	[number]	k4	28,4
%	%	kIx~	%
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
42,5	[number]	k4	42,5
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
okolo	okolo	k7c2	okolo
13	[number]	k4	13
%	%	kIx~	%
lidské	lidský	k2eAgFnSc2d1	lidská
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
1002	[number]	k4	1002
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
Ameriky	Amerika	k1gFnSc2	Amerika
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
nalezenému	nalezený	k2eAgInSc3d1	nalezený
světadílu	světadíl	k1gInSc3	světadíl
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
italského	italský	k2eAgMnSc2d1	italský
obchodníka	obchodník	k1gMnSc2	obchodník
a	a	k8xC	a
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Ameriga	Amerig	k1gMnSc2	Amerig
Vespucciho	Vespucci	k1gMnSc2	Vespucci
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
který	který	k3yIgMnSc1	který
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1499	[number]	k4	1499
a	a	k8xC	a
1502	[number]	k4	1502
podnikl	podniknout	k5eAaPmAgMnS	podniknout
dvě	dva	k4xCgFnPc4	dva
plavby	plavba	k1gFnPc4	plavba
podél	podél	k7c2	podél
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
kartograf	kartograf	k1gMnSc1	kartograf
Martin	Martin	k1gMnSc1	Martin
Waldseemüller	Waldseemüller	k1gMnSc1	Waldseemüller
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1507	[number]	k4	1507
mapu	mapa	k1gFnSc4	mapa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nově	nova	k1gFnSc6	nova
objevený	objevený	k2eAgInSc4d1	objevený
kontinent	kontinent	k1gInSc4	kontinent
nazval	nazvat	k5eAaBmAgMnS	nazvat
podle	podle	k7c2	podle
Vespucciho	Vespucci	k1gMnSc2	Vespucci
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
–	–	k?	–
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc1	akt
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
protesty	protest	k1gInPc4	protest
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Vespucci	Vespucce	k1gMnPc1	Vespucce
snaží	snažit	k5eAaImIp3nP	snažit
uzurpovat	uzurpovat	k5eAaBmF	uzurpovat
Kolumbovi	Kolumbův	k2eAgMnPc1d1	Kolumbův
jeho	jeho	k3xOp3gNnSc7	jeho
prvenství	prvenství	k1gNnSc4	prvenství
<g/>
.	.	kIx.	.
</s>
<s>
Vespucci	Vespucce	k1gFnSc4	Vespucce
však	však	k9	však
–	–	k?	–
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
z	z	k7c2	z
dobové	dobový	k2eAgFnSc2d1	dobová
korespondence	korespondence	k1gFnSc2	korespondence
–	–	k?	–
označení	označení	k1gNnSc1	označení
Amerika	Amerika	k1gFnSc1	Amerika
nijak	nijak	k6eAd1	nijak
neprosazoval	prosazovat	k5eNaImAgInS	prosazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
Ameriky	Amerika	k1gFnSc2	Amerika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozsah	rozsah	k1gInSc1	rozsah
===	===	k?	===
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
Kaffeklubben	Kaffeklubben	k2eAgInSc1d1	Kaffeklubben
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgInPc1d3	nejjižnější
body	bod	k1gInPc1	bod
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Southern	Southerna	k1gFnPc2	Southerna
Thule	Thule	k1gNnSc2	Thule
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
část	část	k1gFnSc4	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
Nordostrundingen	Nordostrundingen	k1gInSc1	Nordostrundingen
a	a	k8xC	a
nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
Attu	Attus	k1gInSc2	Attus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
protažena	protáhnout	k5eAaPmNgFnS	protáhnout
poledníkovým	poledníkový	k2eAgInSc7d1	poledníkový
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
přes	přes	k7c4	přes
obě	dva	k4xCgFnPc4	dva
polokoule	polokoule	k1gFnPc4	polokoule
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
14	[number]	k4	14
500	[number]	k4	500
km	km	kA	km
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
5	[number]	k4	5
950	[number]	k4	950
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
Amerika	Amerika	k1gFnSc1	Amerika
obklopena	obklopit	k5eAaPmNgFnS	obklopit
Severním	severní	k2eAgInSc7d1	severní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Tichým	Tichý	k1gMnSc7	Tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
světadílům	světadíl	k1gInPc3	světadíl
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
<g/>
:	:	kIx,	:
k	k	k7c3	k
Asii	Asie	k1gFnSc3	Asie
nejtěsněji	těsně	k6eAd3	těsně
(	(	kIx(	(
<g/>
75	[number]	k4	75
km	km	kA	km
<g/>
)	)	kIx)	)
v	v	k7c6	v
Beringově	Beringův	k2eAgFnSc6d1	Beringova
úžině	úžina	k1gFnSc6	úžina
<g/>
,	,	kIx,	,
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
má	mít	k5eAaImIp3nS	mít
nejblíže	blízce	k6eAd3	blízce
přes	přes	k7c4	přes
Grónsko	Grónsko	k1gNnSc4	Grónsko
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Islandu	Island	k1gInSc3	Island
<g/>
,	,	kIx,	,
k	k	k7c3	k
Antarktidě	Antarktida	k1gFnSc3	Antarktida
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
řadou	řada	k1gFnSc7	řada
souostroví	souostroví	k1gNnSc2	souostroví
mezi	mezi	k7c7	mezi
Ohňovou	ohňový	k2eAgFnSc7d1	ohňová
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Antarktickým	antarktický	k2eAgInSc7d1	antarktický
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Topografie	topografie	k1gFnSc2	topografie
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světadílu	světadíl	k1gInSc2	světadíl
je	být	k5eAaImIp3nS	být
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
(	(	kIx(	(
<g/>
6959	[number]	k4	6959
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Denali	Denali	k1gFnSc2	Denali
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Mount	Mount	k1gMnSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
(	(	kIx(	(
<g/>
6168	[number]	k4	6168
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Ameriky	Amerika	k1gFnSc2	Amerika
zabírají	zabírat	k5eAaImIp3nP	zabírat
Kordillery	Kordillery	k1gFnPc1	Kordillery
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc7d1	západní
částí	část	k1gFnSc7	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
Andy	Anda	k1gFnPc1	Anda
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
částí	část	k1gFnSc7	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
rozléhají	rozléhat	k5eAaImIp3nP	rozléhat
Skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
2300	[number]	k4	2300
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
Appalačské	Appalačský	k2eAgNnSc4d1	Appalačské
pohoří	pohoří	k1gNnSc4	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
od	od	k7c2	od
Alabamy	Alabam	k1gInPc4	Alabam
k	k	k7c3	k
Newfoundlandu	Newfoundlando	k1gNnSc3	Newfoundlando
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Apalačského	Apalačský	k2eAgNnSc2d1	Apalačské
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
Arktické	arktický	k2eAgFnPc4d1	arktická
Kordillery	Kordillery	k1gFnPc4	Kordillery
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kaskádovém	kaskádový	k2eAgNnSc6d1	kaskádové
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kordillerách	Kordillery	k1gFnPc6	Kordillery
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
stratovulkán	stratovulkán	k1gInSc1	stratovulkán
Mount	Mount	k1gInSc1	Mount
St.	st.	kA	st.
Helens	Helens	k1gInSc1	Helens
(	(	kIx(	(
<g/>
2549	[number]	k4	2549
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hydrologie	hydrologie	k1gFnSc2	hydrologie
===	===	k?	===
</s>
</p>
<p>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
řek	řeka	k1gFnPc2	řeka
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
povodím	povodí	k1gNnSc7	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
průtok	průtok	k1gInSc1	průtok
z	z	k7c2	z
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
povodí	povodí	k1gNnPc1	povodí
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
povodí	povodí	k1gNnSc1	povodí
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
2,5	[number]	k4	2,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
Paraná	Paraná	k1gFnSc1	Paraná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vodopád	vodopád	k1gInSc1	vodopád
Salto	salto	k1gNnSc1	salto
Angel	Angela	k1gFnPc2	Angela
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnPc1d3	nejznámější
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
kanadsko-americká	kanadskomerický	k2eAgNnPc1d1	kanadsko-americký
<g/>
:	:	kIx,	:
Hořejší	Hořejší	k2eAgNnSc4d1	Hořejší
<g/>
,	,	kIx,	,
Huronské	Huronský	k2eAgNnSc4d1	Huronské
<g/>
,	,	kIx,	,
Michigenské	Michigenský	k2eAgNnSc4d1	Michigenský
<g/>
,	,	kIx,	,
Erijské	Erijský	k2eAgNnSc4d1	Erijské
a	a	k8xC	a
Ontario	Ontario	k1gNnSc4	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
položené	položený	k2eAgNnSc1d1	položené
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Titicaca	Titicaca	k1gFnSc1	Titicaca
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
3812	[number]	k4	3812
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
protáhlému	protáhlý	k2eAgInSc3d1	protáhlý
tvaru	tvar	k1gInSc3	tvar
kontinentu	kontinent	k1gInSc2	kontinent
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
všechna	všechen	k3xTgNnPc4	všechen
podnebná	podnebný	k2eAgNnPc4d1	podnebné
pásma	pásmo	k1gNnPc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
území	území	k1gNnSc1	území
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
pásu	pás	k1gInSc2	pás
arktického	arktický	k2eAgInSc2d1	arktický
(	(	kIx(	(
<g/>
polárního	polární	k2eAgInSc2d1	polární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
subarktický	subarktický	k2eAgInSc1d1	subarktický
pás	pás	k1gInSc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
mírném	mírný	k2eAgInSc6d1	mírný
<g/>
,	,	kIx,	,
jih	jih	k1gInSc4	jih
potom	potom	k6eAd1	potom
v	v	k7c6	v
subtropickém	subtropický	k2eAgNnSc6d1	subtropické
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgFnPc1d3	nejjižnější
oblasti	oblast	k1gFnPc1	oblast
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
nepatrně	patrně	k6eNd1	patrně
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
tropického	tropický	k2eAgNnSc2d1	tropické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
leží	ležet	k5eAaImIp3nS	ležet
celá	celý	k2eAgFnSc1d1	celá
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
Ameriky	Amerika	k1gFnSc2	Amerika
Jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
po	po	k7c6	po
40	[number]	k4	40
<g/>
°	°	k?	°
j.š.	j.š.	k?	j.š.
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
opět	opět	k6eAd1	opět
pásmo	pásmo	k1gNnSc1	pásmo
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
mírné	mírný	k2eAgNnSc1d1	mírné
(	(	kIx(	(
<g/>
Patagonie	Patagonie	k1gFnSc1	Patagonie
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejjižnější	jižní	k2eAgFnPc1d3	nejjižnější
části	část	k1gFnPc1	část
Patagonie	Patagonie	k1gFnSc1	Patagonie
a	a	k8xC	a
Ohňová	ohňový	k2eAgFnSc1d1	ohňová
země	země	k1gFnSc1	země
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
subarktickém	subarktický	k2eAgNnSc6d1	subarktické
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
drsné	drsný	k2eAgFnSc2d1	drsná
a	a	k8xC	a
studené	studený	k2eAgFnSc2d1	studená
(	(	kIx(	(
<g/>
proniká	pronikat	k5eAaImIp3nS	pronikat
sem	sem	k6eAd1	sem
vliv	vliv	k1gInSc4	vliv
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
001	[number]	k4	001
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
495	[number]	k4	495
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
579	[number]	k4	579
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Střední	střední	k2eAgFnSc4d1	střední
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
352	[number]	k4	352
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
422,5	[number]	k4	422,5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
===	===	k?	===
Etnologie	etnologie	k1gFnSc2	etnologie
===	===	k?	===
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
potomků	potomek	k1gMnPc2	potomek
osmi	osm	k4xCc2	osm
velkých	velká	k1gFnPc2	velká
etnik	etnikum	k1gNnPc2	etnikum
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc3	jejich
kombinováním	kombinování	k1gNnPc3	kombinování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Indiáni	Indián	k1gMnPc1	Indián
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Inuité	Inuitý	k2eAgNnSc1d1	Inuité
a	a	k8xC	a
Aleuté	Aleutý	k2eAgNnSc1d1	Aleutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
evropským	evropský	k2eAgInSc7d1	evropský
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
a	a	k8xC	a
Dáni	Dán	k1gMnPc1	Dán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mestici	mestic	k1gMnPc1	mestic
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
smíšený	smíšený	k2eAgInSc4d1	smíšený
evropský	evropský	k2eAgInSc4d1	evropský
a	a	k8xC	a
americký	americký	k2eAgInSc4d1	americký
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
původem	původ	k1gInSc7	původ
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mulati	mulat	k1gMnPc1	mulat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
smíšený	smíšený	k2eAgInSc4d1	smíšený
černošský	černošský	k2eAgInSc4d1	černošský
a	a	k8xC	a
evropský	evropský	k2eAgInSc4d1	evropský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zambové	zambo	k1gMnPc1	zambo
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cafuzové	Cafuzový	k2eAgFnPc1d1	Cafuzový
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
smíšený	smíšený	k2eAgInSc4d1	smíšený
černošský	černošský	k2eAgInSc4d1	černošský
a	a	k8xC	a
indiánský	indiánský	k2eAgInSc4d1	indiánský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
nebo	nebo	k8xC	nebo
Jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
smíšený	smíšený	k2eAgInSc4d1	smíšený
asijský	asijský	k2eAgInSc4d1	asijský
a	a	k8xC	a
americký	americký	k2eAgInSc4d1	americký
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Převažující	převažující	k2eAgNnSc1d1	převažující
náboženství	náboženství	k1gNnSc1	náboženství
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgNnPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
85	[number]	k4	85
%	%	kIx~	%
<g/>
;	;	kIx,	;
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
:	:	kIx,	:
93	[number]	k4	93
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Římské	římský	k2eAgNnSc1d1	římské
katolictví	katolictví	k1gNnSc1	katolictví
(	(	kIx(	(
<g/>
vyznávané	vyznávaný	k2eAgNnSc1d1	vyznávané
85	[number]	k4	85
%	%	kIx~	%
Mexickým	mexický	k2eAgNnSc7d1	mexické
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
24	[number]	k4	24
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protestantství	protestantství	k1gNnSc1	protestantství
(	(	kIx(	(
<g/>
vyznávané	vyznávaný	k2eAgNnSc1d1	vyznávané
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
protestanti	protestant	k1gMnPc1	protestant
tvoří	tvořit	k5eAaImIp3nP	tvořit
polovinu	polovina	k1gFnSc4	polovina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nS	tvořit
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravoslaví	pravoslaví	k1gNnSc1	pravoslaví
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
asi	asi	k9	asi
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgFnSc1d1	rozšiřující
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
ho	on	k3xPp3gMnSc4	on
tam	tam	k6eAd1	tam
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
křesťanství	křesťanství	k1gNnPc4	křesťanství
a	a	k8xC	a
necírkevní	církevní	k2eNgNnSc4d1	necírkevní
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
asi	asi	k9	asi
1000	[number]	k4	1000
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
vyznání	vyznání	k1gNnPc2	vyznání
a	a	k8xC	a
sekt	sekta	k1gFnPc2	sekta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ateismus	ateismus	k1gInSc1	ateismus
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
–	–	k?	–
ateisté	ateista	k1gMnPc1	ateista
tvoří	tvořit	k5eAaImIp3nP	tvořit
16	[number]	k4	16
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
USA	USA	kA	USA
a	a	k8xC	a
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
;	;	kIx,	;
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
4	[number]	k4	4
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
ateisté	ateista	k1gMnPc1	ateista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
(	(	kIx(	(
<g/>
vyznávaný	vyznávaný	k2eAgInSc1d1	vyznávaný
2	[number]	k4	2
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
–	–	k?	–
asi	asi	k9	asi
2,5	[number]	k4	2,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
a	a	k8xC	a
1,2	[number]	k4	1,2
%	%	kIx~	%
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
<g/>
;	;	kIx,	;
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
0,23	[number]	k4	0,23
%	%	kIx~	%
–	–	k?	–
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
1,9	[number]	k4	1,9
%	%	kIx~	%
(	(	kIx(	(
<g/>
600	[number]	k4	600
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
<g/>
,	,	kIx,	,
0,6	[number]	k4	0,6
%	%	kIx~	%
(	(	kIx(	(
<g/>
1	[number]	k4	1
820	[number]	k4	820
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
a	a	k8xC	a
0,2	[number]	k4	0,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
250	[number]	k4	250
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Mexičanů	Mexičan	k1gMnPc2	Mexičan
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
islám	islám	k1gInSc1	islám
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
asi	asi	k9	asi
0,5	[number]	k4	0,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
0,3	[number]	k4	0,3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
města	město	k1gNnPc4	město
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
víry	víra	k1gFnPc4	víra
patří	patřit	k5eAaImIp3nS	patřit
sikhismus	sikhismus	k1gInSc1	sikhismus
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
a	a	k8xC	a
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
počtech	počet	k1gInPc6	počet
bahá	bahat	k5eAaBmIp3nS	bahat
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
a	a	k8xC	a
animismus	animismus	k1gInSc1	animismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
mnoha	mnoho	k4c2	mnoho
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
evropský	evropský	k2eAgInSc4d1	evropský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
některými	některý	k3yIgMnPc7	některý
mluví	mluvit	k5eAaImIp3nP	mluvit
domorodci	domorodec	k1gMnPc1	domorodec
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
cizími	cizí	k2eAgInPc7d1	cizí
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Převažujícím	převažující	k2eAgInSc7d1	převažující
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
státu	stát	k1gInSc6	stát
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
regiony	region	k1gInPc1	region
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
nizozemsky	nizozemsky	k6eAd1	nizozemsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc1	Surinam
a	a	k8xC	a
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
převažuje	převažovat	k5eAaImIp3nS	převažovat
haitská	haitský	k2eAgFnSc1d1	Haitská
kreolština	kreolština	k1gFnSc1	kreolština
<g/>
.	.	kIx.	.
</s>
<s>
Indiánskými	indiánský	k2eAgInPc7d1	indiánský
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
než	než	k8xS	než
v	v	k7c6	v
Angloamerice	Angloamerika	k1gFnSc6	Angloamerika
a	a	k8xC	a
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgInPc4d3	nejčastější
patří	patřit	k5eAaImIp3nS	patřit
nahuatl	nahuatnout	k5eAaPmAgMnS	nahuatnout
<g/>
,	,	kIx,	,
kečuánština	kečuánština	k1gFnSc1	kečuánština
<g/>
,	,	kIx,	,
ajmarština	ajmarština	k1gFnSc1	ajmarština
a	a	k8xC	a
guaraní	guaraní	k1gNnSc1	guaraní
<g/>
.	.	kIx.	.
</s>
<s>
Několika	několik	k4yIc7	několik
dalšími	další	k2eAgInPc7d1	další
indiánskými	indiánský	k2eAgInPc7d1	indiánský
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
i	i	k8xC	i
Angloamerice	Angloamerika	k1gFnSc6	Angloamerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
dalšími	další	k2eAgInPc7d1	další
kreolskými	kreolský	k2eAgInPc7d1	kreolský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
už	už	k9	už
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
v	v	k7c6	v
Angloamerice	Angloamerika	k1gFnSc6	Angloamerika
převládá	převládat	k5eAaImIp3nS	převládat
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
převládá	převládat	k5eAaImIp3nS	převládat
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitým	důležitý	k2eAgInSc7d1	důležitý
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Španělsky	španělsky	k6eAd1	španělsky
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
mluví	mluvit	k5eAaImIp3nS	mluvit
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
mluví	mluvit	k5eAaImIp3nS	mluvit
španělsky	španělsky	k6eAd1	španělsky
po	po	k7c6	po
celých	celá	k1gFnPc6	celá
Spojený	spojený	k2eAgInSc1d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Imigrace	imigrace	k1gFnSc1	imigrace
všeobecně	všeobecně	k6eAd1	všeobecně
do	do	k7c2	do
Angloameriky	Angloamerika	k1gFnSc2	Angloamerika
přinesla	přinést	k5eAaPmAgFnS	přinést
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
diverzitu	diverzita	k1gFnSc4	diverzita
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
v	v	k7c6	v
USA	USA	kA	USA
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
300	[number]	k4	300
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
malými	malý	k2eAgFnPc7d1	malá
skupinami	skupina	k1gFnPc7	skupina
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Surinam	Surinam	k1gInSc1	Surinam
a	a	k8xC	a
Belize	Belize	k1gFnSc1	Belize
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nepočítají	počítat	k5eNaImIp3nP	počítat
ani	ani	k8xC	ani
do	do	k7c2	do
Angloameriky	Angloamerika	k1gFnSc2	Angloamerika
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
do	do	k7c2	do
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
jazykově	jazykově	k6eAd1	jazykově
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
geograficky	geograficky	k6eAd1	geograficky
mimo	mimo	k7c4	mimo
Angloameriku	Angloamerika	k1gFnSc4	Angloamerika
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
obou	dva	k4xCgInPc2	dva
těchto	tento	k3xDgInPc2	tento
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Guayaně	Guayana	k1gFnSc6	Guayana
a	a	k8xC	a
Belize	Beliza	k1gFnSc6	Beliza
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
nizozemština	nizozemština	k1gFnSc1	nizozemština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělština	španělština	k1gFnSc1	španělština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
310	[number]	k4	310
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Portugalština	portugalština	k1gFnSc1	portugalština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
185	[number]	k4	185
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Kečuánština	Kečuánština	k1gFnSc1	Kečuánština
<g/>
:	:	kIx,	:
10-13	[number]	k4	10-13
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Guaraní	Guaranit	k5eAaPmIp3nP	Guaranit
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Čínština	čínština	k1gFnSc1	čínština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Italština	italština	k1gFnSc1	italština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Němčina	němčina	k1gFnSc1	němčina
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
2,2	[number]	k4	2,2
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
2,2	[number]	k4	2,2
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Mayské	mayský	k2eAgInPc1d1	mayský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
1,9	[number]	k4	1,9
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Nahuatl	Nahuatnout	k5eAaPmAgMnS	Nahuatnout
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Tagalog	Tagalog	k1gInSc1	Tagalog
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
1,5	[number]	k4	1,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
Vietnamština	vietnamština	k1gFnSc1	vietnamština
<g/>
:	:	kIx,	:
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
lidí	člověk	k1gMnPc2	člověk
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
42	[number]	k4	42
549	[number]	k4	549
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
(	(	kIx(	(
<g/>
6	[number]	k4	6
960	[number]	k4	960
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
<g/>
:	:	kIx,	:
Amazonka	Amazonka	k1gFnSc1	Amazonka
(	(	kIx(	(
<g/>
7025	[number]	k4	7025
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Hořejší	Hořejší	k1gFnSc1	Hořejší
<g/>
(	(	kIx(	(
<g/>
82	[number]	k4	82
100	[number]	k4	100
km2	km2	k4	km2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
1	[number]	k4	1
001	[number]	k4	001
500	[number]	k4	500
000	[number]	k4	000
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
<g/>
:	:	kIx,	:
Grónsko	Grónsko	k1gNnSc4	Grónsko
(	(	kIx(	(
<g/>
2	[number]	k4	2
166	[number]	k4	166
086	[number]	k4	086
km2	km2	k4	km2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
9	[number]	k4	9
984	[number]	k4	984
670	[number]	k4	670
km2	km2	k4	km2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kolokviální	Kolokviální	k2eAgNnSc1d1	Kolokviální
užití	užití	k1gNnSc1	užití
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
Amerika	Amerika	k1gFnSc1	Amerika
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
v	v	k7c6	v
neformálním	formální	k2eNgInSc6d1	neformální
projevu	projev	k1gInSc6	projev
obecně	obecně	k6eAd1	obecně
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Američan	Američan	k1gMnSc1	Američan
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc4d1	běžné
označení	označení	k1gNnSc4	označení
jak	jak	k8xC	jak
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnSc4	obyvatel
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
občana	občan	k1gMnSc4	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Regionální	regionální	k2eAgFnSc1d1	regionální
spolupráce	spolupráce	k1gFnSc1	spolupráce
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
působí	působit	k5eAaImIp3nS	působit
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
apod.	apod.	kA	apod.
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
uskupení	uskupení	k1gNnSc1	uskupení
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
např.	např.	kA	např.
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
integraci	integrace	k1gFnSc4	integrace
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnPc4d1	obchodní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
odstraňování	odstraňování	k1gNnSc4	odstraňování
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
terorismem	terorismus	k1gInSc7	terorismus
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
OAS	oasa	k1gFnPc2	oasa
-	-	kIx~	-
Organizace	organizace	k1gFnSc1	organizace
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
</s>
</p>
<p>
<s>
NAFTA	nafta	k1gFnSc1	nafta
-	-	kIx~	-
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
</s>
</p>
<p>
<s>
DR-CAFTA	DR-CAFTA	k?	DR-CAFTA
-	-	kIx~	-
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
mezi	mezi	k7c7	mezi
Střední	střední	k2eAgFnSc7d1	střední
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
,	,	kIx,	,
Dominikánskou	dominikánský	k2eAgFnSc7d1	Dominikánská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
</s>
</p>
<p>
<s>
SICA	SICA	kA	SICA
-	-	kIx~	-
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
integrační	integrační	k2eAgInSc1d1	integrační
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
CARICOM	CARICOM	kA	CARICOM
-	-	kIx~	-
Karibské	karibský	k2eAgNnSc1d1	Karibské
společenství	společenství	k1gNnSc1	společenství
</s>
</p>
<p>
<s>
OECS	OECS	kA	OECS
-	-	kIx~	-
Organizace	organizace	k1gFnSc1	organizace
východokaribských	východokaribský	k2eAgInPc2d1	východokaribský
států	stát	k1gInPc2	stát
</s>
</p>
<p>
<s>
MERCOSUR	MERCOSUR	kA	MERCOSUR
-	-	kIx~	-
Společný	společný	k2eAgInSc1d1	společný
trh	trh	k1gInSc1	trh
jihu	jih	k1gInSc2	jih
</s>
</p>
<p>
<s>
CAN	CAN	kA	CAN
-	-	kIx~	-
Andské	andský	k2eAgNnSc1d1	Andské
společenství	společenství	k1gNnSc1	společenství
</s>
</p>
<p>
<s>
UNASUR	UNASUR	kA	UNASUR
-	-	kIx~	-
Unie	unie	k1gFnSc1	unie
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
národů	národ	k1gInPc2	národ
</s>
</p>
<p>
<s>
ALBA	alba	k1gFnSc1	alba
-	-	kIx~	-
Bolívarská	Bolívarský	k2eAgFnSc1d1	Bolívarská
aliance	aliance	k1gFnSc1	aliance
pro	pro	k7c4	pro
Ameriku	Amerika	k1gFnSc4	Amerika
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Americas	Americasa	k1gFnPc2	Americasa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
Kontinent	kontinent	k1gInSc1	kontinent
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
kontinentů	kontinent	k1gInPc2	kontinent
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Amerika	Amerika	k1gFnSc1	Amerika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
