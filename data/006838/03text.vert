<s>
Athénská	athénský	k2eAgFnSc1d1	Athénská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
La	la	k1gNnSc4	la
scuola	scuola	k1gFnSc1	scuola
di	di	k?	di
Atene	Aten	k1gMnSc5	Aten
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgNnPc2d3	nejslavnější
děl	dělo	k1gNnPc2	dělo
Raffaela	Raffael	k1gMnSc2	Raffael
Santiho	Santi	k1gMnSc2	Santi
<g/>
,	,	kIx,	,
předního	přední	k2eAgMnSc2d1	přední
malíře	malíř	k1gMnSc2	malíř
italské	italský	k2eAgFnSc2d1	italská
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
papež	papež	k1gMnSc1	papež
Julius	Julius	k1gMnSc1	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgMnS	nechat
přebudovat	přebudovat	k5eAaPmF	přebudovat
čtyři	čtyři	k4xCgFnPc4	čtyři
staré	starý	k2eAgFnPc4d1	stará
stanze	stanze	k1gFnPc4	stanze
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
komnaty	komnata	k1gFnPc4	komnata
<g/>
,	,	kIx,	,
síně	síň	k1gFnPc4	síň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
křídle	křídlo	k1gNnSc6	křídlo
Vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
papežského	papežský	k2eAgInSc2d1	papežský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
malířské	malířský	k2eAgFnSc3d1	malířská
výzdobě	výzdoba	k1gFnSc3	výzdoba
přizván	přizván	k2eAgMnSc1d1	přizván
–	–	k?	–
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
Bramanteho	Bramante	k1gMnSc2	Bramante
–	–	k?	–
tehdy	tehdy	k6eAd1	tehdy
začínající	začínající	k2eAgMnSc1d1	začínající
Raffael	Raffael	k1gMnSc1	Raffael
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
čase	čas	k1gInSc6	čas
pracoval	pracovat	k5eAaImAgMnS	pracovat
Michelangelo	Michelangela	k1gFnSc5	Michelangela
na	na	k7c6	na
výmalbě	výmalba	k1gFnSc6	výmalba
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
Sixtinské	sixtinský	k2eAgFnSc2d1	Sixtinská
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Michelangelo	Michelanget	k5eAaBmAgNnS	Michelanget
se	se	k3xPyFc4	se
však	však	k9	však
řadil	řadit	k5eAaImAgMnS	řadit
do	do	k7c2	do
tábora	tábor	k1gMnSc2	tábor
Bramanteho	Bramante	k1gMnSc2	Bramante
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
komnat	komnata	k1gFnPc2	komnata
také	také	k9	také
skutečně	skutečně	k6eAd1	skutečně
vymaloval	vymalovat	k5eAaPmAgInS	vymalovat
(	(	kIx(	(
<g/>
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
už	už	k6eAd1	už
dokončili	dokončit	k5eAaPmAgMnP	dokončit
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
<g/>
)	)	kIx)	)
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
freska	freska	k1gFnSc1	freska
lunetového	lunetový	k2eAgInSc2d1	lunetový
(	(	kIx(	(
<g/>
půlkruhového	půlkruhový	k2eAgInSc2d1	půlkruhový
<g/>
)	)	kIx)	)
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
rozměru	rozměr	k1gInSc6	rozměr
770	[number]	k4	770
<g/>
×	×	k?	×
<g/>
500	[number]	k4	500
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
maleb	malba	k1gFnPc2	malba
ve	v	k7c4	v
Stanza	Stanz	k1gMnSc4	Stanz
della	delnout	k5eAaImAgFnS	delnout
Segnatura	Segnatura	k1gFnSc1	Segnatura
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Síň	síň	k1gFnSc1	síň
podpisů	podpis	k1gInPc2	podpis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vůbec	vůbec	k9	vůbec
nejznámějším	známý	k2eAgInPc3d3	nejznámější
dílům	díl	k1gInPc3	díl
<g/>
,	,	kIx,	,
chovaným	chovaný	k2eAgMnSc7d1	chovaný
ve	v	k7c6	v
Vatikánských	vatikánský	k2eAgNnPc6d1	Vatikánské
muzeích	muzeum	k1gNnPc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pravdu	pravda	k1gFnSc4	pravda
rozumovou	rozumový	k2eAgFnSc4d1	rozumová
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
hlavním	hlavní	k2eAgInSc7d1	hlavní
obrazem	obraz	k1gInSc7	obraz
je	být	k5eAaImIp3nS	být
Disputace	disputace	k1gFnSc1	disputace
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Disputa	Disput	k1gMnSc2	Disput
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravda	pravda	k1gFnSc1	pravda
zjevená	zjevený	k2eAgFnSc1d1	zjevená
<g/>
,	,	kIx,	,
výzdobu	výzdoba	k1gFnSc4	výzdoba
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
dvě	dva	k4xCgFnPc1	dva
menší	malý	k2eAgFnPc1d2	menší
alegorie	alegorie	k1gFnPc1	alegorie
krásy	krása	k1gFnSc2	krása
(	(	kIx(	(
<g/>
Parnas	Parnas	k1gInSc1	Parnas
-	-	kIx~	-
Il	Il	k1gFnSc1	Il
Parnaso	Parnasa	k1gFnSc5	Parnasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
práva	právo	k1gNnSc2	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stropě	strop	k1gInSc6	strop
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lunetách	luneta	k1gFnPc6	luneta
alegorie	alegorie	k1gFnSc2	alegorie
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ctností	ctnost	k1gFnPc2	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Architekturu	architektura	k1gFnSc4	architektura
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
prý	prý	k9	prý
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
sám	sám	k3xTgMnSc1	sám
Bramante	Bramant	k1gMnSc5	Bramant
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
interiérem	interiér	k1gInSc7	interiér
baziliky	bazilika	k1gFnSc2	bazilika
Sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dodávat	dodávat	k5eAaImF	dodávat
svou	svůj	k3xOyFgFnSc7	svůj
monumentalitou	monumentalita	k1gFnSc7	monumentalita
postavám	postava	k1gFnPc3	postava
na	na	k7c6	na
významnosti	významnost	k1gFnSc6	významnost
a	a	k8xC	a
současně	současně	k6eAd1	současně
tvořit	tvořit	k5eAaImF	tvořit
svým	svůj	k3xOyFgInSc7	svůj
klidem	klid	k1gInSc7	klid
kontrast	kontrast	k1gInSc1	kontrast
k	k	k7c3	k
živé	živý	k2eAgFnSc3d1	živá
kompozici	kompozice	k1gFnSc3	kompozice
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
freska	freska	k1gFnSc1	freska
je	být	k5eAaImIp3nS	být
namalována	namalovat	k5eAaPmNgFnS	namalovat
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgFnSc3d1	známá
středové	středový	k2eAgFnSc3d1	středová
(	(	kIx(	(
<g/>
jednobodové	jednobodový	k2eAgFnSc3d1	jednobodová
<g/>
)	)	kIx)	)
perspektivě	perspektiva	k1gFnSc3	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
sedí	sedit	k5eAaImIp3nS	sedit
Hérakleitos	Hérakleitos	k1gInSc1	Hérakleitos
z	z	k7c2	z
Efesu	Efes	k1gInSc2	Efes
(	(	kIx(	(
<g/>
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
osamocená	osamocený	k2eAgFnSc1d1	osamocená
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
v	v	k7c6	v
úběžníkové	úběžníkový	k2eAgFnSc6d1	úběžníkový
(	(	kIx(	(
<g/>
dvoubodové	dvoubodový	k2eAgFnSc6d1	dvoubodová
<g/>
)	)	kIx)	)
perspektivě	perspektiva	k1gFnSc6	perspektiva
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
neznámá	známý	k2eNgFnSc1d1	neznámá
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
kvádru	kvádr	k1gInSc2	kvádr
není	být	k5eNaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
z	z	k7c2	z
průmětnou	průmětna	k1gFnSc7	průmětna
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
však	však	k9	však
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
chybně	chybně	k6eAd1	chybně
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nabourává	nabourávat	k5eAaImIp3nS	nabourávat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
do	do	k7c2	do
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přípravném	přípravný	k2eAgInSc6d1	přípravný
kartónu	kartón	k1gInSc6	kartón
(	(	kIx(	(
<g/>
kresbě	kresba	k1gFnSc6	kresba
<g/>
)	)	kIx)	)
postava	postava	k1gFnSc1	postava
Herakleita	Herakleita	k1gFnSc1	Herakleita
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
fresky	freska	k1gFnSc2	freska
spolu	spolu	k6eAd1	spolu
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
realista	realista	k1gMnSc1	realista
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dlaní	dlaň	k1gFnSc7	dlaň
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
idealista	idealista	k1gMnSc1	idealista
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zdvíhá	zdvíhat	k5eAaImIp3nS	zdvíhat
prst	prst	k1gInSc4	prst
k	k	k7c3	k
obloze	obloha	k1gFnSc3	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schodech	schod	k1gInPc6	schod
leží	ležet	k5eAaImIp3nS	ležet
Diogenés	Diogenés	k1gInSc1	Diogenés
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
spojuje	spojovat	k5eAaImIp3nS	spojovat
přírodní	přírodní	k2eAgMnPc4d1	přírodní
filozofy	filozof	k1gMnPc4	filozof
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
s	s	k7c7	s
metafyziky	metafyzik	k1gMnPc7	metafyzik
v	v	k7c6	v
části	část	k1gFnSc6	část
horní	horní	k2eAgFnSc6d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Raffael	Raffael	k1gInSc1	Raffael
do	do	k7c2	do
kompozice	kompozice	k1gFnSc2	kompozice
také	také	k9	také
propašoval	propašovat	k5eAaPmAgMnS	propašovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
(	(	kIx(	(
<g/>
úplně	úplně	k6eAd1	úplně
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
malíře	malíř	k1gMnSc2	malíř
starověku	starověk	k1gInSc2	starověk
Apella	Apell	k1gMnSc2	Apell
<g/>
;	;	kIx,	;
svou	svůj	k3xOyFgFnSc4	svůj
signaturu	signatura	k1gFnSc4	signatura
pak	pak	k6eAd1	pak
nenápadně	nápadně	k6eNd1	nápadně
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
límec	límec	k1gInSc4	límec
Eukleidova	Eukleidův	k2eAgInSc2d1	Eukleidův
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
