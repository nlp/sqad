<s>
Athénská	athénský	k2eAgFnSc1d1	Athénská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
La	la	k1gNnSc4	la
scuola	scuola	k1gFnSc1	scuola
di	di	k?	di
Atene	Aten	k1gMnSc5	Aten
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgNnPc2d3	nejslavnější
děl	dělo	k1gNnPc2	dělo
Raffaela	Raffael	k1gMnSc2	Raffael
Santiho	Santi	k1gMnSc2	Santi
<g/>
,	,	kIx,	,
předního	přední	k2eAgMnSc2d1	přední
malíře	malíř	k1gMnSc2	malíř
italské	italský	k2eAgFnSc2d1	italská
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
