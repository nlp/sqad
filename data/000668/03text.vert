<s>
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
federálního	federální	k2eAgInSc2d1	federální
distriktu	distrikt	k1gInSc2	distrikt
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
D.C.	D.C.	k1gMnSc2	D.C.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihoatlantské	Jihoatlantský	k2eAgFnSc6d1	Jihoatlantský
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
regionu	region	k1gInSc2	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Marylandem	Maryland	k1gInSc7	Maryland
<g/>
,	,	kIx,	,
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
jihozápadní	jihozápadní	k2eAgFnSc4d1	jihozápadní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Virginií	Virginie	k1gFnSc7	Virginie
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Potomac	Potomac	k1gFnSc1	Potomac
<g/>
.	.	kIx.	.
</s>
<s>
Založeno	založen	k2eAgNnSc1d1	založeno
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
pocta	pocta	k1gFnSc1	pocta
prvnímu	první	k4xOgNnSc3	první
prezidentovi	prezident	k1gMnSc3	prezident
USA	USA	kA	USA
Georgi	Georg	k1gMnSc3	Georg
Washingtonovi	Washington	k1gMnSc3	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
mělo	mít	k5eAaImAgNnS	mít
obdobné	obdobný	k2eAgNnSc4d1	obdobné
postavení	postavení	k1gNnSc4	postavení
osm	osm	k4xCc1	osm
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
Washington	Washington	k1gInSc1	Washington
24	[number]	k4	24
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Washingtonu	Washington	k1gInSc2	Washington
nestojí	stát	k5eNaImIp3nS	stát
žádné	žádný	k3yNgInPc4	žádný
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
USA	USA	kA	USA
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
tu	tu	k6eAd1	tu
budovy	budova	k1gFnPc4	budova
veřejných	veřejný	k2eAgFnPc2d1	veřejná
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
budovy	budova	k1gFnPc1	budova
administrativní	administrativní	k2eAgFnPc1d1	administrativní
<g/>
.	.	kIx.	.
</s>
<s>
Zástavba	zástavba	k1gFnSc1	zástavba
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k6eAd1	až
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
Potomac	Potomac	k1gFnSc1	Potomac
ve	v	k7c6	v
virginském	virginský	k2eAgInSc6d1	virginský
Arlingtonu	Arlington	k1gInSc6	Arlington
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
nese	nést	k5eAaImIp3nS	nést
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
prvního	první	k4xOgMnSc2	první
prezidenta	prezident	k1gMnSc2	prezident
George	Georg	k1gMnSc4	Georg
Washingtona	Washington	k1gMnSc4	Washington
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
také	také	k6eAd1	také
vybral	vybrat	k5eAaPmAgMnS	vybrat
jeho	jeho	k3xOp3gNnSc4	jeho
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
význam	význam	k1gInSc1	význam
zkratky	zkratka	k1gFnSc2	zkratka
D.C.	D.C.	k1gFnSc2	D.C.
neboli	neboli	k8xC	neboli
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
District	District	k1gInSc1	District
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
oblast	oblast	k1gFnSc1	oblast
okresu	okres	k1gInSc2	okres
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
dobové	dobový	k2eAgNnSc4d1	dobové
poetické	poetický	k2eAgNnSc4d1	poetické
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
District	District	k1gInSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
tedy	tedy	k8xC	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
Oblast	oblast	k1gFnSc1	oblast
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sídlili	sídlit	k5eAaImAgMnP	sídlit
na	na	k7c6	na
území	území	k1gNnSc6	území
Washingtonu	Washington	k1gInSc2	Washington
Algonkinové	Algonkinová	k1gFnSc2	Algonkinová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
osady	osada	k1gFnPc1	osada
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
1749	[number]	k4	1749
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
město	město	k1gNnSc1	město
Alexandria	Alexandrium	k1gNnSc2	Alexandrium
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
1790	[number]	k4	1790
George	George	k1gNnPc2	George
Washington	Washington	k1gInSc1	Washington
vybral	vybrat	k5eAaPmAgInS	vybrat
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
budoucího	budoucí	k2eAgNnSc2d1	budoucí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přesunula	přesunout	k5eAaPmAgFnS	přesunout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
Kapitol	Kapitol	k1gInSc1	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
Britsko-americké	britskomerický	k2eAgFnSc2d1	britsko-americká
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
24	[number]	k4	24
srpna	srpen	k1gInSc2	srpen
1814	[number]	k4	1814
Washington	Washington	k1gInSc1	Washington
těžce	těžce	k6eAd1	těžce
poničen	poničit	k5eAaPmNgInS	poničit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
prezident	prezident	k1gMnSc1	prezident
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
ale	ale	k8xC	ale
během	během	k7c2	během
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ubráněno	ubránit	k5eAaPmNgNnS	ubránit
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
provoz	provoz	k1gInSc4	provoz
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaImAgInS	Nationat
Mall	Mall	k1gInSc1	Mall
-	-	kIx~	-
park	park	k1gInSc1	park
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
1	[number]	k4	1
míli	míle	k1gFnSc6	míle
<g/>
;	;	kIx,	;
spojuje	spojovat	k5eAaImIp3nS	spojovat
Kapitol	Kapitol	k1gInSc1	Kapitol
a	a	k8xC	a
Washingtonův	Washingtonův	k2eAgInSc1d1	Washingtonův
monument	monument	k1gInSc1	monument
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
lemován	lemovat	k5eAaImNgInS	lemovat
muzei	muzeum	k1gNnPc7	muzeum
Smithsonian	Smithsonian	k1gInSc4	Smithsonian
Institution	Institution	k1gInSc1	Institution
Kapitol	Kapitol	k1gInSc1	Kapitol
-	-	kIx~	-
sídlo	sídlo	k1gNnSc1	sídlo
amerického	americký	k2eAgInSc2d1	americký
Kongresu	kongres	k1gInSc2	kongres
<g/>
;	;	kIx,	;
stavěl	stavět	k5eAaImAgMnS	stavět
se	s	k7c7	s
1793	[number]	k4	1793
až	až	k9	až
1800	[number]	k4	1800
<g/>
;	;	kIx,	;
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
několik	několik	k4yIc1	několik
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
-	-	kIx~	-
sídlo	sídlo	k1gNnSc1	sídlo
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
;	;	kIx,	;
dokončen	dokončen	k2eAgInSc1d1	dokončen
1811	[number]	k4	1811
Washingtonův	Washingtonův	k2eAgInSc1d1	Washingtonův
monument	monument	k1gInSc1	monument
-	-	kIx~	-
169	[number]	k4	169
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
obelisk	obelisk	k1gInSc4	obelisk
na	na	k7c4	na
Mallu	Malla	k1gFnSc4	Malla
<g/>
;	;	kIx,	;
stavěl	stavět	k5eAaImAgMnS	stavět
se	se	k3xPyFc4	se
1848	[number]	k4	1848
až	až	k8xS	až
1888	[number]	k4	1888
Lincolnův	Lincolnův	k2eAgInSc4d1	Lincolnův
památník	památník	k1gInSc4	památník
Jeffersonův	Jeffersonův	k2eAgInSc1d1	Jeffersonův
památník	památník	k1gInSc1	památník
Pentagon	Pentagon	k1gInSc1	Pentagon
-	-	kIx~	-
sídlo	sídlo	k1gNnSc1	sídlo
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
;	;	kIx,	;
dokončen	dokončen	k2eAgInSc1d1	dokončen
1943	[number]	k4	1943
Smithsonův	Smithsonův	k2eAgInSc1d1	Smithsonův
institut	institut	k1gInSc1	institut
-	-	kIx~	-
komplex	komplex	k1gInSc1	komplex
muzeí	muzeum	k1gNnPc2	muzeum
nejrůznějšího	různý	k2eAgNnSc2d3	nejrůznější
zaměření	zaměření	k1gNnSc2	zaměření
Arlingtonský	Arlingtonský	k2eAgInSc4d1	Arlingtonský
národní	národní	k2eAgInSc4d1	národní
hřbitov	hřbitov	k1gInSc4	hřbitov
s	s	k7c7	s
hrobem	hrob	k1gInSc7	hrob
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
Knihovna	knihovna	k1gFnSc1	knihovna
Kongresu	kongres	k1gInSc2	kongres
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
601	[number]	k4	601
723	[number]	k4	723
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
tvořili	tvořit	k5eAaImAgMnP	tvořit
38,5	[number]	k4	38,5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
50,7	[number]	k4	50,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,1	[number]	k4	4,1
%	%	kIx~	%
a	a	k8xC	a
dvě	dva	k4xCgNnPc1	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
2,9	[number]	k4	2,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
9,1	[number]	k4	9,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bělošských	bělošský	k2eAgMnPc2d1	bělošský
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
britského	britský	k2eAgInSc2d1	britský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
Hispánců	Hispánec	k1gMnPc2	Hispánec
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
Salvadoru	Salvador	k1gMnSc6	Salvador
<g/>
.	.	kIx.	.
83,42	[number]	k4	83,42
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
mluví	mluvit	k5eAaImIp3nS	mluvit
doma	doma	k6eAd1	doma
pouze	pouze	k6eAd1	pouze
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
9,18	[number]	k4	9,18
%	%	kIx~	%
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
72	[number]	k4	72
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
následovaní	následovaný	k2eAgMnPc1d1	následovaný
buddhisty	buddhista	k1gMnSc2	buddhista
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muslimy	muslim	k1gMnPc7	muslim
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
židy	žid	k1gMnPc7	žid
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnSc1	obyvatel
měl	mít	k5eAaImAgMnS	mít
Washington	Washington	k1gInSc4	Washington
D.C.	D.C.	k1gFnSc2	D.C.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bílého	bílý	k2eAgInSc2d1	bílý
útěku	útěk	k1gInSc2	útěk
začali	začít	k5eAaPmAgMnP	začít
běloši	běloch	k1gMnPc1	běloch
stěhovat	stěhovat	k5eAaImF	stěhovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gMnPc2	D.C.
si	se	k3xPyFc3	se
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
nelichotivou	lichotivý	k2eNgFnSc4d1	nelichotivá
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
murder	murder	k1gMnSc1	murder
capital	capital	k1gMnSc1	capital
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
vražedné	vražedný	k2eAgNnSc1d1	vražedné
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
počtů	počet	k1gInPc2	počet
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
spáchána	spáchat	k5eAaPmNgFnS	spáchat
černochy	černoch	k1gMnPc7	černoch
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
zločinu	zločin	k1gInSc2	zločin
koncentrována	koncentrovat	k5eAaBmNgFnS	koncentrovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
černošských	černošský	k2eAgFnPc2d1	černošská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
situace	situace	k1gFnSc1	situace
postupně	postupně	k6eAd1	postupně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
gentrifikace	gentrifikace	k1gFnSc1	gentrifikace
a	a	k8xC	a
do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
běloši	běloch	k1gMnPc1	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgFnSc1d1	zasahující
i	i	k9	i
do	do	k7c2	do
států	stát	k1gInPc2	stát
Maryland	Marylanda	k1gFnPc2	Marylanda
a	a	k8xC	a
Virginia	Virginium	k1gNnSc2	Virginium
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
5,8	[number]	k4	5,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metro	metro	k1gNnSc4	metro
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Washington	Washington	k1gInSc1	Washington
Metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Páteřní	páteřní	k2eAgInSc1d1	páteřní
systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
pět	pět	k4xCc4	pět
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
Metrorail	Metrorail	k1gInSc1	Metrorail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
autobusovými	autobusový	k2eAgFnPc7d1	autobusová
linkami	linka	k1gFnPc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
-	-	kIx~	-
Union	union	k1gInSc1	union
Station	station	k1gInSc1	station
je	být	k5eAaImIp3nS	být
nedaleko	nedaleko	k7c2	nedaleko
Kapitolu	Kapitol	k1gInSc2	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Operují	operovat	k5eAaImIp3nP	operovat
zde	zde	k6eAd1	zde
Amtrak	Amtrak	k1gInSc4	Amtrak
(	(	kIx(	(
<g/>
dálkové	dálkový	k2eAgInPc4d1	dálkový
spoje	spoj	k1gInPc4	spoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MARC	MARC	kA	MARC
(	(	kIx(	(
<g/>
příměstské	příměstský	k2eAgInPc1d1	příměstský
spoje	spoj	k1gInPc1	spoj
-	-	kIx~	-
směr	směr	k1gInSc1	směr
Maryland	Maryland	k1gInSc1	Maryland
<g/>
)	)	kIx)	)
a	a	k8xC	a
VRE	VRE	kA	VRE
(	(	kIx(	(
<g/>
příměstské	příměstský	k2eAgInPc1d1	příměstský
spoje	spoj	k1gInPc1	spoj
-	-	kIx~	-
směr	směr	k1gInSc1	směr
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
je	být	k5eAaImIp3nS	být
napojeno	napojit	k5eAaPmNgNnS	napojit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Washington	Washington	k1gInSc4	Washington
National	National	k1gFnSc2	National
Airport	Airport	k1gInSc1	Airport
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
Dulles	Dullesa	k1gFnPc2	Dullesa
International	International	k1gFnPc2	International
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
Baltimore	Baltimore	k1gInSc1	Baltimore
<g/>
/	/	kIx~	/
<g/>
Washington	Washington	k1gInSc1	Washington
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
BWI	BWI	kA	BWI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
nachází	nacházet	k5eAaImIp3nS	nacházet
speciální	speciální	k2eAgInSc4d1	speciální
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
P	P	kA	P
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tzv.	tzv.	kA	tzv.
identifikační	identifikační	k2eAgFnSc4d1	identifikační
zónu	zóna	k1gFnSc4	zóna
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
ní	on	k3xPp3gFnSc7	on
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
zónu	zóna	k1gFnSc4	zóna
(	(	kIx(	(
<g/>
zapovězenou	zapovězený	k2eAgFnSc4d1	zapovězená
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
civilní	civilní	k2eAgInPc4d1	civilní
letouny	letoun	k1gInPc4	letoun
<g/>
)	)	kIx)	)
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
17	[number]	k4	17
mil	míle	k1gFnPc2	míle
okolo	okolo	k7c2	okolo
Washingtonova	Washingtonův	k2eAgInSc2d1	Washingtonův
monumentu	monument	k1gInSc2	monument
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
zónu	zóna	k1gFnSc4	zóna
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
3	[number]	k4	3
míle	míle	k1gFnSc1	míle
kolem	kolem	k7c2	kolem
Kapitolu	Kapitol	k1gInSc2	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
brání	bránit	k5eAaImIp3nP	bránit
stíhačky	stíhačka	k1gFnPc1	stíhačka
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Andrews	Andrewsa	k1gFnPc2	Andrewsa
<g/>
,	,	kIx,	,
cca	cca	kA	cca
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Accra	Accra	k1gFnSc1	Accra
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Bangkok	Bangkok	k1gInSc1	Bangkok
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Brusel	Brusel	k1gInSc1	Brusel
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
Dakar	Dakar	k1gInSc1	Dakar
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Pretoria	Pretorium	k1gNnSc2	Pretorium
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Sunderland	Sunderland	k1gInSc1	Sunderland
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
