<s>
Kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgFnSc2d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgMnSc1d1
Kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgInSc4d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
psovití	psovití	k1gMnPc1
(	(	kIx(
<g/>
Canidae	Canidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
Canis	Canis	k1gFnPc1
Binomické	binomický	k2eAgFnPc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Canis	Canis	k1gFnSc1
latransSay	latransSaa	k1gFnSc2
<g/>
,	,	kIx,
1823	#num#	k4
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kojota	kojot	k1gMnSc2
prérijního	prérijní	k2eAgMnSc2d1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kojota	kojot	k1gMnSc2
prérijního	prérijní	k2eAgMnSc2d1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Kojot	kojot	k1gMnSc1
prérijový	prérijový	k2eAgMnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Canis	Canis	k1gInSc1
latrans	latrans	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
severoamerická	severoamerický	k2eAgFnSc1d1
šelma	šelma	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
psovitých	psovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
aztéckého	aztécký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
coyō	coyō	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Výška	výška	k1gFnSc1
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
<g/>
:	:	kIx,
60	#num#	k4
cm	cm	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
100	#num#	k4
až	až	k9
135	#num#	k4
cm	cm	kA
</s>
<s>
Délka	délka	k1gFnSc1
ocasu	ocas	k1gInSc2
<g/>
:	:	kIx,
40	#num#	k4
cm	cm	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc4
<g/>
:	:	kIx,
samci	samec	k1gInPc7
8	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
výjimečně	výjimečně	k6eAd1
až	až	k9
34	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
samice	samice	k1gFnSc1
7	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
kg	kg	kA
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Kojoti	kojot	k1gMnPc1
původně	původně	k6eAd1
žili	žít	k5eAaImAgMnP
na	na	k7c6
skalnatých	skalnatý	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
postupně	postupně	k6eAd1
pronikali	pronikat	k5eAaImAgMnP
dále	daleko	k6eAd2
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
pouští	poušť	k1gFnPc2
a	a	k8xC
polopouští	polopoušť	k1gFnPc2
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
až	až	k9
po	po	k7c4
Aljašku	Aljaška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Kojot	kojot	k1gMnSc1
je	být	k5eAaImIp3nS
masožravec	masožravec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loví	lovit	k5eAaImIp3nS
hlodavce	hlodavec	k1gMnSc4
<g/>
,	,	kIx,
malé	malý	k2eAgMnPc4d1
ptáky	pták	k1gMnPc4
<g/>
,	,	kIx,
různé	různý	k2eAgMnPc4d1
vodní	vodní	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
vidlorohy	vidloroh	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokáže	dokázat	k5eAaPmIp3nS
vyvinout	vyvinout	k5eAaPmF
poměrně	poměrně	k6eAd1
velkou	velký	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
1,5	1,5	k4
km	km	kA
může	moct	k5eAaImIp3nS
běžet	běžet	k5eAaImF
rychlostí	rychlost	k1gFnSc7
48	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
na	na	k7c4
kratší	krátký	k2eAgFnSc4d2
vzdálenost	vzdálenost	k1gFnSc4
i	i	k9
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Živí	živit	k5eAaImIp3nS
se	se	k3xPyFc4
nicméně	nicméně	k8xC
také	také	k9
zdechlinami	zdechlina	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
lidských	lidský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
požírá	požírat	k5eAaImIp3nS
odpadky	odpadek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nouzi	nouze	k1gFnSc6
se	se	k3xPyFc4
dokáže	dokázat	k5eAaPmIp3nS
spokojit	spokojit	k5eAaPmF
jen	jen	k9
s	s	k7c7
trávou	tráva	k1gFnSc7
a	a	k8xC
ovocem	ovoce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
napadá	napadat	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
i	i	k8xC
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
útoky	útok	k1gInPc1
málokdy	málokdy	k6eAd1
končí	končit	k5eAaImIp3nP
fatálně	fatálně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Kojotí	Kojotit	k5eAaImIp3nS,k5eAaPmIp3nS
říje	říje	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
od	od	k7c2
února	únor	k1gInSc2
do	do	k7c2
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	samec	k1gMnPc7
sice	sice	k8xC
mezi	mezi	k7c7
sebou	se	k3xPyFc7
bojují	bojovat	k5eAaImIp3nP
<g/>
,	,	kIx,
konečná	konečný	k2eAgFnSc1d1
volba	volba	k1gFnSc1
však	však	k9
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
feně	fena	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
vybrat	vybrat	k5eAaPmF
i	i	k9
poraženého	poražený	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
vzniklé	vzniklý	k2eAgInPc1d1
páry	pár	k1gInPc1
spolu	spolu	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
porodem	porod	k1gInSc7
si	se	k3xPyFc3
samice	samice	k1gFnPc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
svého	svůj	k3xOyFgMnSc2
partnera	partner	k1gMnSc2
vybuduje	vybudovat	k5eAaPmIp3nS
doupě	doupě	k1gNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
útočištěm	útočiště	k1gNnSc7
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
a	a	k8xC
pro	pro	k7c4
štěňata	štěně	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
kojoti	kojot	k1gMnPc1
obsadí	obsadit	k5eAaPmIp3nP
jezevčí	jezevčí	k2eAgFnSc4d1
nebo	nebo	k8xC
liščí	liščí	k2eAgFnSc4d1
noru	nora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
devítitýdenní	devítitýdenní	k2eAgFnSc6d1
březosti	březost	k1gFnSc6
vrhne	vrhnout	k5eAaPmIp3nS,k5eAaImIp3nS
fena	fena	k1gFnSc1
čtyři	čtyři	k4xCgNnPc4
až	až	k9
deset	deset	k4xCc4
mláďat	mládě	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
kojí	kojit	k5eAaImIp3nS
7	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stáří	stáří	k1gNnSc6
tří	tři	k4xCgInPc2
až	až	k8xS
čtyř	čtyři	k4xCgInPc2
týdnů	týden	k1gInPc2
jsou	být	k5eAaImIp3nP
štěňata	štěně	k1gNnPc1
schopna	schopen	k2eAgNnPc1d1
přijímat	přijímat	k5eAaImF
pevnou	pevný	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samec	samec	k1gMnSc1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
přísun	přísun	k1gInSc4
potravy	potrava	k1gFnSc2
a	a	k8xC
hlídá	hlídat	k5eAaImIp3nS
okolí	okolí	k1gNnSc4
brlohu	brloh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovnitř	dovnitř	k6eAd1
však	však	k9
již	již	k6eAd1
nevstupuje	vstupovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dosažení	dosažení	k1gNnSc2
úplné	úplný	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
do	do	k7c2
věku	věk	k1gInSc2
20	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
doprovázejí	doprovázet	k5eAaImIp3nP
mladí	mladý	k2eAgMnPc1d1
kojoti	kojot	k1gMnPc1
své	svůj	k3xOyFgMnPc4
rodiče	rodič	k1gMnPc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
osamostatňují	osamostatňovat	k5eAaImIp3nP
a	a	k8xC
zakládají	zakládat	k5eAaImIp3nP
si	se	k3xPyFc3
vlastní	vlastní	k2eAgFnPc4d1
rodiny	rodina	k1gFnPc4
nebo	nebo	k8xC
smečky	smečka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Způsob	způsob	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgMnSc1d1
v	v	k7c6
Národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Grand	grand	k1gMnSc1
Teton	Teton	k1gMnSc1
</s>
<s>
Kojoti	kojot	k1gMnPc1
žijí	žít	k5eAaImIp3nP
v	v	k7c6
párech	pár	k1gInPc6
nebo	nebo	k8xC
v	v	k7c6
rodinných	rodinný	k2eAgFnPc6d1
smečkách	smečka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečlivě	pečlivě	k6eAd1
hlídají	hlídat	k5eAaImIp3nP
hranice	hranice	k1gFnPc1
svého	svůj	k3xOyFgNnSc2
teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
si	se	k3xPyFc3
značkují	značkovat	k5eAaImIp3nP
močí	moč	k1gFnSc7
a	a	k8xC
trusem	trus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Uctívané	uctívaný	k2eAgNnSc1d1
i	i	k8xC
zatracované	zatracovaný	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
</s>
<s>
Kojot	kojot	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
mytologii	mytologie	k1gFnSc6
severoamerických	severoamerický	k2eAgMnPc2d1
Indiánů	Indián	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
často	často	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
jako	jako	k9
šprýmař	šprýmař	k1gMnSc1
<g/>
,	,	kIx,
potměšilý	potměšilý	k2eAgMnSc1d1
šibal	šibal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
ostatním	ostatní	k2eAgMnPc3d1
tvorům	tvor	k1gMnPc3
pomáhá	pomáhat	k5eAaImIp3nS
<g/>
,	,	kIx,
ovšem	ovšem	k9
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
si	se	k3xPyFc3
z	z	k7c2
nich	on	k3xPp3gMnPc2
tropí	tropit	k5eAaImIp3nP
nejapné	nejapné	k?
žerty	žert	k1gInPc1
nebo	nebo	k8xC
se	se	k3xPyFc4
je	on	k3xPp3gMnPc4
snaží	snažit	k5eAaImIp3nP
všemožně	všemožně	k6eAd1
přelstít	přelstít	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
získal	získat	k5eAaPmAgMnS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
chce	chtít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
vždy	vždy	k6eAd1
mu	on	k3xPp3gInSc3
ale	ale	k9
jeho	jeho	k3xOp3gInPc1
plány	plán	k1gInPc1
vyjdou	vyjít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
kmeny	kmen	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
Vraní	vraní	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
<g/>
,	,	kIx,
Juteové	Juteus	k1gMnPc1
<g/>
,	,	kIx,
Pajutové	Pajut	k1gMnPc1
nebo	nebo	k8xC
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Kalifornie	Kalifornie	k1gFnSc2
ho	on	k3xPp3gMnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
stvořitele	stvořitel	k1gMnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komančové	Komanč	k1gMnPc1
věŕí	věŕit	k5eAaBmIp3nP,k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
naučil	naučit	k5eAaPmAgMnS
lidi	člověk	k1gMnPc4
rozdělávat	rozdělávat	k5eAaImF
oheň	oheň	k1gInSc1
a	a	k8xC
lovit	lovit	k5eAaImF
bizony	bizon	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Navahů	Navah	k1gMnPc2
kojot	kojot	k1gMnSc1
přinesl	přinést	k5eAaPmAgMnS
na	na	k7c4
svět	svět	k1gInSc4
smrt	smrt	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránil	zabránit	k5eAaPmAgInS
přelidnění	přelidnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
příbězích	příběh	k1gInPc6
Apačů	Apač	k1gMnPc2
<g/>
,	,	kIx,
Čejenů	Čejen	k1gMnPc2
nebo	nebo	k8xC
Lakotů	lakota	k1gMnPc2
je	být	k5eAaImIp3nS
kojot	kojot	k1gMnSc1
zobrazen	zobrazit	k5eAaPmNgMnS
především	především	k9
jako	jako	k9
drzý	drzý	k2eAgMnSc1d1
a	a	k8xC
zlomyslný	zlomyslný	k2eAgMnSc1d1
šibal	šibal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
ostatní	ostatní	k2eAgFnSc1d1
bytosti	bytost	k1gFnSc3
přelstít	přelstít	k5eAaPmF
a	a	k8xC
podvést	podvést	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
většinou	většinou	k6eAd1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
mnohdy	mnohdy	k6eAd1
potom	potom	k6eAd1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
chtivost	chtivost	k1gFnSc4
a	a	k8xC
zlomyslnost	zlomyslnost	k1gFnSc4
doplatí	doplatit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kojota	kojot	k1gMnSc2
uctívali	uctívat	k5eAaImAgMnP
také	také	k9
Aztékové	Azték	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
božstva	božstvo	k1gNnSc2
hudby	hudba	k1gFnSc2
a	a	k8xC
tance	tanec	k1gInSc2
jménem	jméno	k1gNnSc7
Huehuecoyotl	Huehuecoyotl	k1gMnSc1
„	„	k?
<g/>
Stařík	stařík	k1gMnSc1
kojot	kojot	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
kojot	kojot	k1gMnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
aztéckého	aztécký	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
nahuatl	nahuatnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
coyotl	coyotnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
aztéckém	aztécký	k2eAgNnSc6d1
umění	umění	k1gNnSc6
se	se	k3xPyFc4
často	často	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
motiv	motiv	k1gInSc1
kojota	kojot	k1gMnSc2
na	na	k7c6
péřových	péřový	k2eAgFnPc6d1
mozaikách	mozaika	k1gFnPc6
<g/>
,	,	kIx,
sochách	socha	k1gFnPc6
<g/>
,	,	kIx,
reliéfech	reliéf	k1gInPc6
či	či	k8xC
ilustracích	ilustrace	k1gFnPc6
v	v	k7c6
kodexech	kodex	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indiánskými	indiánský	k2eAgFnPc7d1
bajkami	bajka	k1gFnPc7
a	a	k8xC
pohádkami	pohádka	k1gFnPc7
o	o	k7c6
kojotových	kojotův	k2eAgFnPc6d1
šibalských	šibalský	k2eAgFnPc6d1
koucícch	koucíc	k1gFnPc6
byla	být	k5eAaImAgFnS
inspirována	inspirován	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
animovaná	animovaný	k2eAgFnSc1d1
groteska	groteska	k1gFnSc1
Wile	Wil	k1gFnSc2
E.	E.	kA
Coyote	Coyot	k1gInSc5
and	and	k?
Road	Road	k1gMnSc1
Runner	Runner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1
farmáři	farmář	k1gMnPc1
však	však	k9
mají	mít	k5eAaImIp3nP
na	na	k7c4
kojoty	kojot	k1gMnPc4
jiný	jiný	k2eAgInSc4d1
názor	názor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
přesvědčeno	přesvědčit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgNnPc1
zvířata	zvíře	k1gNnPc1
jsou	být	k5eAaImIp3nP
úhlavními	úhlavní	k2eAgMnPc7d1
nepřáteli	nepřítel	k1gMnPc7
jejich	jejich	k3xOp3gNnPc2
stád	stádo	k1gNnPc2
dobytka	dobytek	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	on	k3xPp3gInPc4
nemilosrdně	milosrdně	k6eNd1
střílejí	střílet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
byli	být	k5eAaImAgMnP
kojoti	kojot	k1gMnPc1
rovněž	rovněž	k9
chytáni	chytán	k2eAgMnPc1d1
do	do	k7c2
želez	železo	k1gNnPc2
<g/>
,	,	kIx,
tráveni	tráven	k2eAgMnPc1d1
kyanidem	kyanid	k1gInSc7
a	a	k8xC
strychninem	strychnin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročně	ročně	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k9
v	v	k7c6
USA	USA	kA
zabito	zabít	k5eAaPmNgNnS
na	na	k7c4
90	#num#	k4
000	#num#	k4
kojotů	kojot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
kojoti	kojot	k1gMnPc1
napadají	napadat	k5eAaPmIp3nP,k5eAaImIp3nP,k5eAaBmIp3nP
ovce	ovce	k1gFnPc4
a	a	k8xC
telata	tele	k1gNnPc4
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
v	v	k7c6
oblasti	oblast	k1gFnSc6
dostatek	dostatek	k1gInSc4
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kojoti	kojot	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
Michiganu	Michigan	k1gInSc6
velmi	velmi	k6eAd1
často	často	k6eAd1
odváží	odvážit	k5eAaPmIp3nS,k5eAaImIp3nS
až	až	k9
na	na	k7c4
terasu	terasa	k1gFnSc4
lidských	lidský	k2eAgNnPc2d1
obydlí	obydlí	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
rádi	rád	k2eAgMnPc1d1
olizují	olizovat	k5eAaImIp3nP
tuk	tuk	k1gInSc1
z	z	k7c2
grilů	gril	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
BRYL	BRYL	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
;	;	kIx,
MATYÁŠTÍK	MATYÁŠTÍK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
savců	savec	k1gMnPc2
-	-	kIx~
Savci	savec	k1gMnSc6
<g/>
,	,	kIx,
internetová	internetový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Palackého	Palackého	k2eAgMnPc2d1
<g/>
,	,	kIx,
upol	upolit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
COYOTE	COYOTE	kA
ATTACKS	ATTACKS	kA
<g/>
:	:	kIx,
AN	AN	kA
INCREASING	INCREASING	kA
SUBURBAN	SUBURBAN	kA
PROBLEM	PROBLEM	kA
<g/>
*	*	kIx~
<g/>
.	.	kIx.
www.co.san-diego.ca.us	www.co.san-diego.ca.us	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Canis	Canis	k1gFnSc2
latrans	latransa	k1gFnPc2
ve	v	k7c6
Wikidruzích	Wikidruh	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kojot	kojot	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kojot	kojot	k1gMnSc1
prériový	prériový	k2eAgMnSc1d1
na	na	k7c6
BioLibu	BioLib	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Psovití	psovití	k1gMnPc1
</s>
<s>
dhoul	dhoout	k5eAaPmAgMnS
•	•	k?
dingo	dingo	k1gMnSc1
•	•	k?
fenek	fenek	k1gMnSc1
•	•	k?
kojot	kojot	k1gMnSc1
prérijní	prérijní	k2eAgMnSc1d1
•	•	k?
korsak	korsak	k1gMnSc1
•	•	k?
liška	liška	k1gFnSc1
džunglová	džunglový	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
horská	horský	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
chama	chama	k1gFnSc1
•	•	k?
liška	liška	k1gFnSc1
kana	kanout	k5eAaImSgInS
•	•	k?
liška	liška	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
ostrovní	ostrovní	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
písečná	písečný	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
polární	polární	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
pouštní	pouštní	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
šedá	šedý	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
šedohnědá	šedohnědý	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
velkouchá	velkouchat	k5eAaPmIp3nS
•	•	k?
maikong	maikong	k1gMnSc1
•	•	k?
pes	pes	k1gMnSc1
argentinský	argentinský	k2eAgMnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
pes	pes	k1gMnSc1
bojovný	bojovný	k2eAgMnSc1d1
<g/>
†	†	k?
•	•	k?
pes	pes	k1gMnSc1
hřivnatý	hřivnatý	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
horský	horský	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
hyenový	hyenový	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
krátkouchý	krátkouchý	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
pampový	pampový	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
pouštní	pouštní	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
pralesní	pralesní	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
šedý	šedý	k2eAgMnSc1d1
•	•	k?
pes	pes	k1gMnSc1
ušatý	ušatý	k2eAgMnSc1d1
•	•	k?
psík	psík	k1gMnSc1
mývalovitý	mývalovitý	k2eAgMnSc1d1
•	•	k?
šakal	šakal	k1gMnSc1
čabrakový	čabrakový	k2eAgMnSc1d1
•	•	k?
šakal	šakal	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
•	•	k?
šakal	šakal	k1gMnSc1
pruhovaný	pruhovaný	k2eAgMnSc1d1
•	•	k?
vlček	vlček	k1gMnSc1
etiopský	etiopský	k2eAgMnSc1d1
•	•	k?
vlk	vlk	k1gMnSc1
africký	africký	k2eAgMnSc1d1
•	•	k?
vlk	vlk	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
•	•	k?
vlk	vlk	k1gMnSc1
rudohnědý	rudohnědý	k2eAgMnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4175548-0	4175548-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85033677	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85033677	#num#	k4
</s>
