<s desamb="1">
Dokáže	dokázat	k5eAaPmIp3nS
vyvinout	vyvinout	k5eAaPmF
poměrně	poměrně	k6eAd1
velkou	velký	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
1,5	1,5	k4
km	km	kA
může	moct	k5eAaImIp3nS
běžet	běžet	k5eAaImF
rychlostí	rychlost	k1gFnSc7
48	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
na	na	k7c4
kratší	krátký	k2eAgFnSc4d2
vzdálenost	vzdálenost	k1gFnSc4
i	i	k9
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Živí	živit	k5eAaImIp3nS
se	se	k3xPyFc4
nicméně	nicméně	k8xC
také	také	k9
zdechlinami	zdechlina	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
lidských	lidský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
požírá	požírat	k5eAaImIp3nS
odpadky	odpadek	k1gInPc7
<g/>
.	.	kIx.
</s>