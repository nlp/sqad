<p>
<s>
cd	cd	kA	cd
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
change	change	k1gFnSc1	change
directory	director	k1gInPc4	director
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
příkaz	příkaz	k1gInSc4	příkaz
v	v	k7c6	v
UNIXu	Unix	k1gInSc6	Unix
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
unixového	unixový	k2eAgInSc2d1	unixový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
změní	změnit	k5eAaPmIp3nS	změnit
aktuální	aktuální	k2eAgInSc4d1	aktuální
pracovní	pracovní	k2eAgInSc4d1	pracovní
adresář	adresář	k1gInSc4	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
ve	v	k7c6	v
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
DOSu	DOSum	k1gNnSc6	DOSum
či	či	k8xC	či
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zápis	zápis	k1gInSc1	zápis
==	==	k?	==
</s>
</p>
<p>
<s>
$	$	kIx~	$
cd	cd	kA	cd
[	[	kIx(	[
<g/>
-L	-L	k?	-L
<g/>
|	|	kIx~	|
<g/>
-P	-P	k?	-P
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
dir	dir	k?	dir
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
cd	cd	kA	cd
–	–	k?	–
dokumentace	dokumentace	k1gFnSc2	dokumentace
k	k	k7c3	k
programu	program	k1gInSc3	program
v	v	k7c6	v
Single	singl	k1gInSc5	singl
UNIX	UNIX	kA	UNIX
Specification	Specification	k1gInSc4	Specification
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
