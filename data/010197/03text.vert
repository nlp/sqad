<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pohotovost	pohotovost	k1gFnSc1	pohotovost
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
připravenost	připravenost	k1gFnSc4	připravenost
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
k	k	k7c3	k
boji	boj	k1gInSc3	boj
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k9	už
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
složek	složka	k1gFnPc2	složka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
armáda	armáda	k1gFnSc1	armáda
světa	svět	k1gInSc2	svět
udržuje	udržovat	k5eAaImIp3nS	udržovat
systém	systém	k1gInSc4	systém
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
bojové	bojový	k2eAgFnSc2d1	bojová
pohotovosti	pohotovost	k1gFnSc2	pohotovost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
aplikován	aplikovat	k5eAaBmNgInS	aplikovat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
situacích	situace	k1gFnPc6	situace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
od	od	k7c2	od
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičení	cvičení	k1gNnSc2	cvičení
až	až	k9	až
po	po	k7c4	po
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
ohrožení	ohrožení	k1gNnSc4	ohrožení
státu	stát	k1gInSc2	stát
cizí	cizí	k2eAgFnSc7d1	cizí
mocností	mocnost	k1gFnSc7	mocnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
bojové	bojový	k2eAgFnSc2d1	bojová
pohotovosti	pohotovost	k1gFnSc2	pohotovost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ohrožení	ohrožení	k1gNnSc6	ohrožení
teroristickým	teroristický	k2eAgInSc7d1	teroristický
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
konání	konání	k1gNnSc6	konání
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
akce	akce	k1gFnSc2	akce
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Významný	významný	k2eAgInSc1d1	významný
dopad	dopad	k1gInSc1	dopad
na	na	k7c4	na
účinek	účinek	k1gInSc4	účinek
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pohotovosti	pohotovost	k1gFnSc2	pohotovost
má	mít	k5eAaImIp3nS	mít
morálka	morálka	k1gFnSc1	morálka
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
připravenost	připravenost	k1gFnSc1	připravenost
<g/>
,	,	kIx,	,
výbava	výbava	k1gFnSc1	výbava
a	a	k8xC	a
výcvik	výcvik	k1gInSc1	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tyto	tento	k3xDgInPc4	tento
faktory	faktor	k1gInPc4	faktor
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vojsko	vojsko	k1gNnSc1	vojsko
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
určitého	určitý	k2eAgInSc2d1	určitý
stupně	stupeň	k1gInSc2	stupeň
pohotovosti	pohotovost	k1gFnSc2	pohotovost
značnou	značný	k2eAgFnSc4d1	značná
taktickou	taktický	k2eAgFnSc4d1	taktická
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
např.	např.	kA	např.
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pohotovost	pohotovost	k1gFnSc4	pohotovost
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
protikomunistickým	protikomunistický	k2eAgNnSc7d1	protikomunistické
povstáním	povstání	k1gNnSc7	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
připravení	připravení	k1gNnSc6	připravení
armády	armáda	k1gFnSc2	armáda
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
v	v	k7c6	v
nepřehledném	přehledný	k2eNgInSc6d1	nepřehledný
vývoji	vývoj	k1gInSc6	vývoj
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
DEFCON	DEFCON	kA	DEFCON
</s>
</p>
