<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pohotovost	pohotovost	k1gFnSc1	pohotovost
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
připravenost	připravenost	k1gFnSc4	připravenost
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
k	k	k7c3	k
boji	boj	k1gInSc3	boj
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k9	už
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
složek	složka	k1gFnPc2	složka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
