<p>
<s>
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsovy	Waalsův	k2eAgFnSc2d1	Waalsova
síly	síla	k1gFnSc2	síla
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
fyzikovi	fyzik	k1gMnSc3	fyzik
Johannesi	Johannese	k1gFnSc4	Johannese
Dideriku	Diderika	k1gFnSc4	Diderika
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waalsovi	Waalsův	k2eAgMnPc1d1	Waalsův
jsou	být	k5eAaImIp3nP	být
přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
a	a	k8xC	a
odpudivé	odpudivý	k2eAgFnPc1d1	odpudivá
interakce	interakce	k1gFnPc1	interakce
(	(	kIx(	(
<g/>
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
molekulami	molekula	k1gFnPc7	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
odpudivou	odpudivý	k2eAgFnSc7d1	odpudivá
silou	síla	k1gFnSc7	síla
je	být	k5eAaImIp3nS	být
elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
repulze	repulze	k1gFnSc1	repulze
elektronových	elektronový	k2eAgInPc2d1	elektronový
oblaků	oblak	k1gInPc2	oblak
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
těsném	těsný	k2eAgNnSc6d1	těsné
přiblížení	přiblížení	k1gNnSc6	přiblížení
molekul	molekula	k1gFnPc2	molekula
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
přitažlivých	přitažlivý	k2eAgFnPc2d1	přitažlivá
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waalsových	Waalsových	k2eAgMnPc1d1	Waalsových
sil	síla	k1gFnPc2	síla
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Coulombická	Coulombický	k2eAgFnSc1d1	Coulombický
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
polaritou	polarita	k1gFnSc7	polarita
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
elektrostatické	elektrostatický	k2eAgNnSc4d1	elektrostatické
přitahování	přitahování	k1gNnSc4	přitahování
opačných	opačný	k2eAgInPc2d1	opačný
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
nebo	nebo	k8xC	nebo
nabitých	nabitý	k2eAgFnPc6d1	nabitá
molekulách	molekula	k1gFnPc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Coulombická	Coulombický	k2eAgFnSc1d1	Coulombický
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
například	například	k6eAd1	například
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
atomu	atom	k1gInSc6	atom
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jeho	jeho	k3xOp3gFnSc2	jeho
vysoké	vysoký	k2eAgFnSc2d1	vysoká
elektronegativity	elektronegativita	k1gFnSc2	elektronegativita
částečně	částečně	k6eAd1	částečně
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
vodících	vodík	k1gInPc6	vodík
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
dipól	dipól	k1gInSc1	dipól
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
interaguje	interagovat	k5eAaBmIp3nS	interagovat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dipóly	dipól	k1gInPc7	dipól
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
vodíkovým	vodíkový	k2eAgInSc7d1	vodíkový
můstkem	můstek	k1gInSc7	můstek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Coulombické	Coulombický	k2eAgFnPc1d1	Coulombický
síly	síla	k1gFnPc1	síla
nemusí	muset	k5eNaImIp3nP	muset
nutně	nutně	k6eAd1	nutně
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
můstky	můstek	k1gInPc4	můstek
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
teplotě	teplota	k1gFnSc6	teplota
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
posunuta	posunout	k5eAaPmNgFnS	posunout
až	až	k9	až
na	na	k7c4	na
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Indukční	indukční	k2eAgFnSc1d1	indukční
síla	síla	k1gFnSc1	síla
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
vzniku	vznik	k1gInSc3	vznik
trvale	trvale	k6eAd1	trvale
polarizovanou	polarizovaný	k2eAgFnSc4d1	polarizovaná
molekulu	molekula	k1gFnSc4	molekula
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
polarizuje	polarizovat	k5eAaImIp3nS	polarizovat
ostatní	ostatní	k1gNnSc4	ostatní
(	(	kIx(	(
<g/>
polární	polární	k2eAgFnSc2d1	polární
i	i	k8xC	i
nepolární	polární	k2eNgFnSc2d1	nepolární
<g/>
)	)	kIx)	)
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Disperzní	disperzní	k2eAgFnSc1d1	disperzní
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
z	z	k7c2	z
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waalsových	Waalsův	k2eAgFnPc2d1	Waalsova
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
kovalentní	kovalentní	k2eAgFnSc1d1	kovalentní
<g/>
,	,	kIx,	,
koordinační	koordinační	k2eAgFnSc1d1	koordinační
vazba	vazba	k1gFnSc1	vazba
a	a	k8xC	a
vodíkový	vodíkový	k2eAgInSc1d1	vodíkový
můstek	můstek	k1gInSc1	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
v	v	k7c6	v
nepolárních	polární	k2eNgFnPc6d1	nepolární
molekulách	molekula	k1gFnPc6	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
stálé	stálý	k2eAgInPc1d1	stálý
dipóly	dipól	k1gInPc1	dipól
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vazby	vazba	k1gFnPc1	vazba
nejsou	být	k5eNaImIp3nP	být
polarizované	polarizovaný	k2eAgFnPc1d1	polarizovaná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
nepolárních	polární	k2eNgFnPc6d1	nepolární
molekulách	molekula	k1gFnPc6	molekula
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
pohybu	pohyb	k1gInSc3	pohyb
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
slabých	slabý	k2eAgFnPc2d1	slabá
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
oscilujících	oscilující	k2eAgInPc2d1	oscilující
(	(	kIx(	(
<g/>
kmitajících	kmitající	k2eAgInPc2d1	kmitající
<g/>
)	)	kIx)	)
dipólů	dipól	k1gInPc2	dipól
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
elektronová	elektronový	k2eAgFnSc1d1	elektronová
hustota	hustota	k1gFnSc1	hustota
v	v	k7c6	v
nepolární	polární	k2eNgFnSc6d1	nepolární
molekule	molekula	k1gFnSc6	molekula
rozložena	rozložit	k5eAaPmNgFnS	rozložit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
dvě	dva	k4xCgFnPc4	dva
molekuly	molekula	k1gFnPc4	molekula
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
tato	tento	k3xDgFnSc1	tento
oscilace	oscilace	k1gFnSc1	oscilace
synchronizována	synchronizovat	k5eAaBmNgFnS	synchronizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
oscilující	oscilující	k2eAgInPc1d1	oscilující
dipóly	dipól	k1gInPc1	dipól
budou	být	k5eAaImBp3nP	být
přitahovat	přitahovat	k5eAaImF	přitahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
