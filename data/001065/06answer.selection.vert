<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2013	[number]	k4	2013
poslanec	poslanec	k1gMnSc1	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
