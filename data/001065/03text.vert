<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1952	[number]	k4	1952
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2013	[number]	k4	2013
poslanec	poslanec	k1gMnSc1	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
Stanislava	Stanislav	k1gMnSc2	Stanislav
Grosse	Gross	k1gMnSc2	Gross
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strana	strana	k1gFnSc1	strana
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
prarodičů	prarodič	k1gMnPc2	prarodič
do	do	k7c2	do
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
za	za	k7c7	za
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tam	tam	k6eAd1	tam
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc4	Paroubek
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
výrobní	výrobní	k2eAgMnSc1d1	výrobní
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
letňanské	letňanský	k2eAgFnSc6d1	Letňanská
Avii	Avia	k1gFnSc6	Avia
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Věra	Věra	k1gFnSc1	Věra
Paroubková	Paroubkový	k2eAgFnSc1d1	Paroubková
byla	být	k5eAaImAgFnS	být
vedoucí	vedoucí	k1gFnSc1	vedoucí
květinářství	květinářství	k1gNnSc2	květinářství
a	a	k8xC	a
květiny	květina	k1gFnSc2	květina
i	i	k9	i
aranžovala	aranžovat	k5eAaImAgFnS	aranžovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Ostrovní	ostrovní	k2eAgFnSc6d1	ostrovní
ulici	ulice	k1gFnSc6	ulice
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Jana	Jan	k1gMnSc2	Jan
Nerudy	Neruda	k1gMnSc2	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
Obchodní	obchodní	k2eAgFnSc4d1	obchodní
fakultu	fakulta	k1gFnSc4	fakulta
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
až	až	k9	až
1990	[number]	k4	1990
pracoval	pracovat	k5eAaImAgInS	pracovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
ekonom	ekonom	k1gMnSc1	ekonom
podniku	podnik	k1gInSc2	podnik
Restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
jídelny	jídelna	k1gFnSc2	jídelna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oficiálním	oficiální	k2eAgInSc6d1	oficiální
životopise	životopis	k1gInSc6	životopis
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
vrcholných	vrcholný	k2eAgFnPc6d1	vrcholná
manažerských	manažerský	k2eAgFnPc6d1	manažerská
funkcích	funkce	k1gFnPc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
až	až	k9	až
2003	[number]	k4	2003
podnikal	podnikat	k5eAaImAgMnS	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
StB	StB	k1gMnSc1	StB
jako	jako	k8xS	jako
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
kandidát	kandidát	k1gMnSc1	kandidát
tajné	tajný	k2eAgFnSc2d1	tajná
spolupráce	spolupráce	k1gFnSc2	spolupráce
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Roko	Roko	k1gNnSc1	Roko
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
jeho	jeho	k3xOp3gMnSc2	jeho
papouška	papoušek	k1gMnSc2	papoušek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
komise	komise	k1gFnSc2	komise
při	při	k7c6	při
Federálním	federální	k2eAgNnSc6d1	federální
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
vědomým	vědomý	k2eAgMnSc7d1	vědomý
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
Paroubek	Paroubek	k1gInSc1	Paroubek
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
socialistické	socialistický	k2eAgFnSc2d1	socialistická
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
různé	různý	k2eAgFnPc4d1	různá
stranické	stranický	k2eAgFnPc4d1	stranická
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
ČSS	ČSS	kA	ČSS
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
politické	politický	k2eAgFnSc6d1	politická
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
ČSS	ČSS	kA	ČSS
se	se	k3xPyFc4	se
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
životopisech	životopis	k1gInPc6	životopis
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Československé	československý	k2eAgFnSc2d1	Československá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
obnovovacím	obnovovací	k2eAgInSc6d1	obnovovací
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ústředního	ústřední	k2eAgMnSc2d1	ústřední
tajemníka	tajemník	k1gMnSc2	tajemník
a	a	k8xC	a
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
ústředního	ústřední	k2eAgInSc2d1	ústřední
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
šéfem	šéf	k1gMnSc7	šéf
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stal	stát	k5eAaPmAgMnS	stát
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1993	[number]	k4	1993
a	a	k8xC	a
1995	[number]	k4	1995
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
regionálním	regionální	k2eAgInSc6d1	regionální
výkonném	výkonný	k2eAgInSc6d1	výkonný
výboru	výbor	k1gInSc6	výbor
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
pražské	pražský	k2eAgFnSc2d1	Pražská
stranické	stranický	k2eAgFnSc2d1	stranická
organizace	organizace	k1gFnSc2	organizace
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
ODS	ODS	kA	ODS
zvolen	zvolen	k2eAgMnSc1d1	zvolen
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
náměstka	náměstek	k1gMnSc2	náměstek
primátora	primátor	k1gMnSc2	primátor
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
finanční	finanční	k2eAgFnSc2d1	finanční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2000	[number]	k4	2000
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
zastupitelem	zastupitel	k1gMnSc7	zastupitel
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
zastupitelů	zastupitel	k1gMnPc2	zastupitel
zvolených	zvolený	k2eAgMnPc2d1	zvolený
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
získal	získat	k5eAaPmAgInS	získat
druhý	druhý	k4xOgInSc1	druhý
nejnižší	nízký	k2eAgInSc1d3	nejnižší
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
neuspěl	uspět	k5eNaPmAgMnS	uspět
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
předsedy	předseda	k1gMnSc2	předseda
pražské	pražský	k2eAgFnSc2d1	Pražská
organizace	organizace	k1gFnSc2	organizace
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vláda	vláda	k1gFnSc1	vláda
Jiřího	Jiří	k1gMnSc4	Jiří
Paroubka	Paroubek	k1gMnSc4	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Grossově	Grossův	k2eAgFnSc6d1	Grossova
vládě	vláda	k1gFnSc6	vláda
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
zastával	zastávat	k5eAaImAgInS	zastávat
post	post	k1gInSc1	post
ministra	ministr	k1gMnSc2	ministr
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
sestavil	sestavit	k5eAaPmAgInS	sestavit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Paroubkův	Paroubkův	k2eAgInSc4d1	Paroubkův
návrh	návrh	k1gInSc4	návrh
vláda	vláda	k1gFnSc1	vláda
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
uznala	uznat	k5eAaPmAgFnS	uznat
zásluhy	zásluha	k1gFnPc4	zásluha
německých	německý	k2eAgMnPc2d1	německý
antifašistů	antifašista	k1gMnPc2	antifašista
a	a	k8xC	a
omluvila	omluvit	k5eAaPmAgFnS	omluvit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
způsobená	způsobený	k2eAgNnPc4d1	způsobené
příkoří	příkoří	k1gNnPc4	příkoří
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
sehrál	sehrát	k5eAaPmAgMnS	sehrát
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povodní	povodeň	k1gFnPc2	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Paroubek	Paroubek	k1gInSc1	Paroubek
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
činnost	činnost	k1gFnSc4	činnost
šéfa	šéf	k1gMnSc2	šéf
Povodí	povodí	k1gNnSc2	povodí
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
následně	následně	k6eAd1	následně
ministr	ministr	k1gMnSc1	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
Jan	Jan	k1gMnSc1	Jan
Mládek	Mládek	k1gMnSc1	Mládek
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
předvolebním	předvolební	k2eAgInSc6d1	předvolební
sjezdu	sjezd	k1gInSc6	sjezd
ČSSD	ČSSD	kA	ČSSD
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
ČSSD	ČSSD	kA	ČSSD
umístila	umístit	k5eAaPmAgFnS	umístit
za	za	k7c4	za
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
podala	podat	k5eAaPmAgFnS	podat
celá	celý	k2eAgFnSc1d1	celá
vláda	vláda	k1gFnSc1	vláda
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
však	však	k9	však
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
funkce	funkce	k1gFnPc4	funkce
až	až	k6eAd1	až
do	do	k7c2	do
jmenování	jmenování	k1gNnSc2	jmenování
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
patřil	patřit	k5eAaImAgInS	patřit
J.	J.	kA	J.
Paroubek	Paroubek	k1gInSc1	Paroubek
mezi	mezi	k7c4	mezi
čelné	čelný	k2eAgMnPc4d1	čelný
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
premiéra	premiér	k1gMnSc4	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jeho	jeho	k3xOp3gMnSc4	jeho
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Sobotku	Sobotka	k1gFnSc4	Sobotka
na	na	k7c6	na
předvolebním	předvolební	k2eAgInSc6d1	předvolební
shromážděním	shromáždění	k1gNnSc7	shromáždění
napadl	napadnout	k5eAaPmAgMnS	napadnout
opilý	opilý	k2eAgMnSc1d1	opilý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestává	přestávat	k5eAaImIp3nS	přestávat
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
vybranými	vybraný	k2eAgInPc7d1	vybraný
sdělovacími	sdělovací	k2eAgInPc7d1	sdělovací
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Respekt	respekt	k1gInSc1	respekt
a	a	k8xC	a
Reflex	reflex	k1gInSc1	reflex
<g/>
)	)	kIx)	)
které	který	k3yIgNnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
pravicové	pravicový	k2eAgFnSc2d1	pravicová
a	a	k8xC	a
vyvolávající	vyvolávající	k2eAgFnSc4d1	vyvolávající
atmosféru	atmosféra	k1gFnSc4	atmosféra
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ČSSD	ČSSD	kA	ČSSD
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
sestavit	sestavit	k5eAaPmF	sestavit
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odstoupit	odstoupit	k5eAaPmF	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
místopředseda	místopředseda	k1gMnSc1	místopředseda
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
frakcí	frakce	k1gFnPc2	frakce
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
odsuzovaly	odsuzovat	k5eAaImAgFnP	odsuzovat
Paroubkovu	Paroubkův	k2eAgFnSc4d1	Paroubkova
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
Paroubka	Paroubek	k1gMnSc2	Paroubek
zase	zase	k9	zase
hájili	hájit	k5eAaImAgMnP	hájit
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Rath	Rath	k1gMnSc1	Rath
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Davidu	David	k1gMnSc3	David
Rathovi	Rath	k1gMnSc3	Rath
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc4	Paroubek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
sepsání	sepsání	k1gNnSc4	sepsání
nejméně	málo	k6eAd3	málo
tří	tři	k4xCgFnPc2	tři
knih	kniha	k1gFnPc2	kniha
pamětí	paměť	k1gFnPc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
pojednávat	pojednávat	k5eAaImF	pojednávat
o	o	k7c6	o
Paroubkově	Paroubkův	k2eAgNnSc6d1	Paroubkovo
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
o	o	k7c6	o
ČSSD	ČSSD	kA	ČSSD
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
vydanou	vydaný	k2eAgFnSc7d1	vydaná
knihou	kniha	k1gFnSc7	kniha
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
celoživotní	celoživotní	k2eAgFnSc2d1	celoživotní
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
-	-	kIx~	-
Levice	levice	k1gFnSc1	levice
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
<g/>
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
zkratkou	zkratka	k1gFnSc7	zkratka
LEV	Lev	k1gMnSc1	Lev
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
jejího	její	k3xOp3gNnSc2	její
sloučení	sloučení	k1gNnSc2	sloučení
do	do	k7c2	do
konce	konec	k1gInSc2	konec
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
již	již	k6eAd1	již
existujícím	existující	k2eAgInSc7d1	existující
subjektem	subjekt	k1gInSc7	subjekt
Českou	český	k2eAgFnSc7d1	Česká
stranou	strana	k1gFnSc7	strana
národně	národně	k6eAd1	národně
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
(	(	kIx(	(
<g/>
ČSNS	ČSNS	kA	ČSNS
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
ustavujícího	ustavující	k2eAgInSc2d1	ustavující
sjezdu	sjezd	k1gInSc2	sjezd
nové	nový	k2eAgFnPc4d1	nová
strany	strana	k1gFnPc4	strana
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
-	-	kIx~	-
levice	levice	k1gFnSc1	levice
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
však	však	k9	však
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc4	Paroubek
sloučení	sloučení	k1gNnSc2	sloučení
s	s	k7c7	s
ČSNS	ČSNS	kA	ČSNS
2005	[number]	k4	2005
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
o	o	k7c6	o
slučování	slučování	k1gNnSc6	slučování
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
stranou	strana	k1gFnSc7	strana
neuvažujeme	uvažovat	k5eNaImIp1nP	uvažovat
<g/>
.	.	kIx.	.
</s>
<s>
Neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
ani	ani	k9	ani
sloučení	sloučení	k1gNnSc4	sloučení
se	s	k7c7	s
stranou	strana	k1gFnSc7	strana
ČSNS	ČSNS	kA	ČSNS
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
přišli	přijít	k5eAaPmAgMnP	přijít
k	k	k7c3	k
nám.	nám.	k?	nám.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
sjezdu	sjezd	k1gInSc6	sjezd
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
233	[number]	k4	233
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
se	se	k3xPyFc4	se
zdrželi	zdržet	k5eAaPmAgMnP	zdržet
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
místopředsedy	místopředseda	k1gMnPc4	místopředseda
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastního	vlastní	k2eAgNnSc2d1	vlastní
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
(	(	kIx(	(
<g/>
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
získal	získat	k5eAaPmAgInS	získat
pouze	pouze	k6eAd1	pouze
0,46	[number]	k4	0,46
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
do	do	k7c2	do
EP	EP	kA	EP
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
LEV	Lev	k1gMnSc1	Lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodnil	odůvodnit	k5eAaPmAgInS	odůvodnit
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
podnikatelským	podnikatelský	k2eAgFnPc3d1	podnikatelská
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
akademické	akademický	k2eAgFnSc3d1	akademická
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
veřejně-prospěšným	veřejněrospěšný	k2eAgFnPc3d1	veřejně-prospěšný
činnostem	činnost	k1gFnPc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc4	Paroubek
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
ženatý	ženatý	k2eAgMnSc1d1	ženatý
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Paroubkovou	Paroubkový	k2eAgFnSc7d1	Paroubková
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Zajíčkovou	Zajíčková	k1gFnSc7	Zajíčková
<g/>
,	,	kIx,	,
překladatelkou	překladatelka	k1gFnSc7	překladatelka
a	a	k8xC	a
učitelkou	učitelka	k1gFnSc7	učitelka
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
byli	být	k5eAaImAgMnP	být
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
manželé	manžel	k1gMnPc1	manžel
spolu	spolu	k6eAd1	spolu
mají	mít	k5eAaImIp3nP	mít
syna	syn	k1gMnSc4	syn
Jiřího	Jiří	k1gMnSc4	Jiří
narozeného	narozený	k2eAgMnSc4d1	narozený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
se	se	k3xPyFc4	se
Paroubek	Paroubek	k1gInSc1	Paroubek
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
tlumočnicí	tlumočnice	k1gFnSc7	tlumočnice
Petrou	Petra	k1gFnSc7	Petra
Kováčovou	Kováčová	k1gFnSc7	Kováčová
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
přijala	přijmout	k5eAaPmAgFnS	přijmout
příjmení	příjmení	k1gNnSc4	příjmení
Paroubková	Paroubkový	k2eAgFnSc1d1	Paroubková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
navázal	navázat	k5eAaPmAgInS	navázat
vztah	vztah	k1gInSc1	vztah
již	již	k6eAd1	již
během	během	k7c2	během
trvání	trvání	k1gNnSc2	trvání
svého	svůj	k3xOyFgNnSc2	svůj
manželství	manželství	k1gNnSc2	manželství
se	s	k7c7	s
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Paroubkovou	Paroubkový	k2eAgFnSc7d1	Paroubková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
údajně	údajně	k6eAd1	údajně
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
manželce	manželka	k1gFnSc3	manželka
oznámil	oznámit	k5eAaPmAgMnS	oznámit
přání	přání	k1gNnSc3	přání
rozvodu	rozvod	k1gInSc2	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Chuchli	Chuchle	k1gFnSc6	Chuchle
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpisu	výpis	k1gInSc2	výpis
z	z	k7c2	z
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
:	:	kIx,	:
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
zástupce	zástupce	k1gMnSc1	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
Restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
jídelny	jídelna	k1gFnSc2	jídelna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
do	do	k7c2	do
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
člen	člen	k1gInSc1	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
člen	člen	k1gInSc4	člen
a	a	k8xC	a
od	od	k7c2	od
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
předseda	předseda	k1gMnSc1	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Kongresové	kongresový	k2eAgNnSc4d1	Kongresové
centrum	centrum	k1gNnSc4	centrum
Praha	Praha	k1gFnSc1	Praha
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
společník	společník	k1gMnSc1	společník
Megaplay	Megaplaa	k1gFnSc2	Megaplaa
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
a.s.	a.s.	k?	a.s.
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
předseda	předseda	k1gMnSc1	předseda
dozorčí	dozorčí	k1gMnSc1	dozorčí
rady	rada	k1gFnSc2	rada
První	první	k4xOgFnSc1	první
městská	městský	k2eAgFnSc1d1	městská
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
působila	působit	k5eAaImAgFnS	působit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Royal	Royal	k1gMnSc1	Royal
banka	banka	k1gFnSc1	banka
CS	CS	kA	CS
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
PPF	PPF	kA	PPF
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
Funkci	funkce	k1gFnSc4	funkce
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Koukalovi	Koukal	k1gMnSc6	Koukal
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
finanční	finanční	k2eAgFnSc1d1	finanční
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
o.p.s.	o.p.s.	k?	o.p.s.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Antonín	Antonín	k1gMnSc1	Antonín
<g />
.	.	kIx.	.
</s>
<s>
Koláček	koláček	k1gInSc1	koláček
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Husitské	husitský	k2eAgNnSc1d1	husitské
centrum	centrum	k1gNnSc1	centrum
o.p.s.	o.p.s.	k?	o.p.s.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Czech	Czech	k1gMnSc1	Czech
-	-	kIx~	-
Australian	Australian	k1gMnSc1	Australian
Group	Group	k1gMnSc1	Group
o.p.s.	o.p.s.	k?	o.p.s.
(	(	kIx(	(
<g/>
CZECH	CZECH	kA	CZECH
-	-	kIx~	-
PACIFIC	PACIFIC	kA	PACIFIC
AGENCY	AGENCY	kA	AGENCY
o.p.s.	o.p.s.	k?	o.p.s.
v	v	k7c6	v
likvidaci	likvidace	k1gFnSc6	likvidace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
vymazáno	vymazat	k5eAaPmNgNnS	vymazat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Horská	horský	k2eAgFnSc1d1	horská
služba	služba	k1gFnSc1	služba
ČR	ČR	kA	ČR
o.p.s.	o.p.s.	k?	o.p.s.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
Paroubek	Paroubka	k1gFnPc2	Paroubka
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
Společnosti	společnost	k1gFnSc2	společnost
Willyho	Willy	k1gMnSc2	Willy
Brandta	Brandt	k1gMnSc2	Brandt
a	a	k8xC	a
Bruna	Bruno	k1gMnSc2	Bruno
Kreiskyho	Kreisky	k1gMnSc2	Kreisky
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
časopisu	časopis	k1gInSc2	časopis
ČSSD	ČSSD	kA	ČSSD
Trend	trend	k1gInSc1	trend
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
desítky	desítka	k1gFnPc4	desítka
vlastních	vlastní	k2eAgMnPc2d1	vlastní
článků	článek	k1gInPc2	článek
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
i	i	k8xC	i
zahraničních	zahraniční	k2eAgNnPc6d1	zahraniční
periodikách	periodikum	k1gNnPc6	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
též	též	k9	též
iniciátorem	iniciátor	k1gMnSc7	iniciátor
názorového	názorový	k2eAgInSc2d1	názorový
webu	web	k1gInSc2	web
Vaše	váš	k3xOp2gNnSc1	váš
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
psaní	psaní	k1gNnSc4	psaní
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
kniha	kniha	k1gFnSc1	kniha
Minuty	minuta	k1gFnSc2	minuta
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
..	..	k?	..
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
mu	on	k3xPp3gNnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
očima	oko	k1gNnPc7	oko
sociálního	sociální	k2eAgMnSc2d1	sociální
demokrata	demokrat	k1gMnSc2	demokrat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
Ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
též	též	k9	též
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
navrátil	navrátit	k5eAaPmAgInS	navrátit
policii	policie	k1gFnSc4	policie
na	na	k7c4	na
fotbalová	fotbalový	k2eAgNnPc4d1	fotbalové
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
výtržnosti	výtržnost	k1gFnSc6	výtržnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
při	při	k7c6	při
zápase	zápas	k1gInSc6	zápas
Baníku	Baník	k1gInSc2	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
a	a	k8xC	a
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Částí	část	k1gFnSc7	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
populistu	populista	k1gMnSc4	populista
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
sbližování	sbližování	k1gNnSc4	sbližování
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Politickými	politický	k2eAgMnPc7d1	politický
protivníky	protivník	k1gMnPc7	protivník
bývá	bývat	k5eAaImIp3nS	bývat
obviňován	obviňován	k2eAgInSc1d1	obviňován
z	z	k7c2	z
afér	aféra	k1gFnPc2	aféra
kolem	kolem	k7c2	kolem
prodělku	prodělek	k1gInSc2	prodělek
První	první	k4xOgFnSc2	první
městské	městský	k2eAgFnSc2d1	městská
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
zavádění	zavádění	k1gNnSc1	zavádění
zabezpečovače	zabezpečovač	k1gInSc2	zabezpečovač
Matra	Matra	k1gFnSc1	Matra
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
zakázek	zakázka	k1gFnPc2	zakázka
po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
i	i	k9	i
nákup	nákup	k1gInSc4	nákup
72	[number]	k4	72
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
zakázka	zakázka	k1gFnSc1	zakázka
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
skleníku	skleník	k1gInSc2	skleník
Fata	fatum	k1gNnSc2	fatum
Morgána	morgána	k1gFnSc1	morgána
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
v	v	k7c4	v
Troji	troje	k4xRgFnSc4	troje
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
i	i	k9	i
na	na	k7c4	na
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
zadlužení	zadlužení	k1gNnSc4	zadlužení
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
politický	politický	k2eAgMnSc1d1	politický
karikaturista	karikaturista	k1gMnSc1	karikaturista
Štěpán	Štěpán	k1gMnSc1	Štěpán
Mareš	Mareš	k1gMnSc1	Mareš
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
Metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
uveřejněného	uveřejněný	k2eAgInSc2d1	uveřejněný
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedení	vedení	k1gNnSc4	vedení
Reflexu	reflex	k1gInSc2	reflex
Mareše	Mareš	k1gMnSc2	Mareš
propustilo	propustit	k5eAaPmAgNnS	propustit
a	a	k8xC	a
že	že	k8xS	že
nejvíc	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
snažil	snažit	k5eAaImAgMnS	snažit
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
CzechTek	CzechTko	k1gNnPc2	CzechTko
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
byl	být	k5eAaImAgInS	být
také	také	k9	také
obviňován	obviňován	k2eAgInSc1d1	obviňován
<g/>
,	,	kIx,	,
že	že	k8xS	že
festival	festival	k1gInSc1	festival
CzechTek	CzechTek	k1gInSc1	CzechTek
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
násilně	násilně	k6eAd1	násilně
rozehnán	rozehnat	k5eAaPmNgInS	rozehnat
policií	policie	k1gFnSc7	policie
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
politickou	politický	k2eAgFnSc4d1	politická
objednávku	objednávka	k1gFnSc4	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc4	Paroubek
posléze	posléze	k6eAd1	posléze
nařídil	nařídit	k5eAaPmAgMnS	nařídit
ministrovi	ministr	k1gMnSc3	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Bublanovi	Bublan	k1gMnSc3	Bublan
prošetření	prošetření	k1gNnSc2	prošetření
incidentu	incident	k1gInSc6	incident
<g/>
,	,	kIx,	,
po	po	k7c4	po
prošetření	prošetření	k1gNnSc4	prošetření
však	však	k9	však
žádný	žádný	k3yNgMnSc1	žádný
policista	policista	k1gMnSc1	policista
nebyl	být	k5eNaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgFnPc7	dva
účastníkům	účastník	k1gMnPc3	účastník
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
policejní	policejní	k2eAgInSc4d1	policejní
zákrok	zákrok	k1gInSc4	zákrok
přiznáno	přiznán	k2eAgNnSc4d1	přiznáno
odškodné	odškodné	k1gNnSc4	odškodné
a	a	k8xC	a
omluva	omluva	k1gFnSc1	omluva
(	(	kIx(	(
<g/>
definitivní	definitivní	k2eAgNnSc1d1	definitivní
soudní	soudní	k2eAgNnSc1d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
padlo	padnout	k5eAaPmAgNnS	padnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
zveřejněn	zveřejněn	k2eAgInSc4d1	zveřejněn
článek	článek	k1gInSc4	článek
z	z	k7c2	z
bulletinu	bulletin	k1gInSc2	bulletin
ČSS	ČSS	kA	ČSS
Socialistický	socialistický	k2eAgInSc4d1	socialistický
směr	směr	k1gInSc4	směr
r.	r.	kA	r.
1978	[number]	k4	1978
se	s	k7c7	s
záznamem	záznam	k1gInSc7	záznam
stranické	stranický	k2eAgFnSc2d1	stranická
besedy	beseda	k1gFnSc2	beseda
o	o	k7c6	o
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
Paroubek	Paroubek	k1gInSc1	Paroubek
účastnil	účastnit	k5eAaImAgInS	účastnit
a	a	k8xC	a
ideologickými	ideologický	k2eAgFnPc7d1	ideologická
frázemi	fráze	k1gFnPc7	fráze
chválil	chválit	k5eAaImAgMnS	chválit
"	"	kIx"	"
<g/>
likvidaci	likvidace	k1gFnSc4	likvidace
pozic	pozice	k1gFnPc2	pozice
exponentů	exponent	k1gInPc2	exponent
buržoazie	buržoazie	k1gFnSc1	buržoazie
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
ap.	ap.	kA	ap.
Za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
se	se	k3xPyFc4	se
Paroubek	Paroubek	k1gInSc4	Paroubek
neomluvil	omluvit	k5eNaPmAgInS	omluvit
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
nebyl	být	k5eNaImAgInS	být
autorizován	autorizován	k2eAgInSc1d1	autorizován
a	a	k8xC	a
novináři	novinář	k1gMnPc1	novinář
ho	on	k3xPp3gMnSc4	on
zkreslují	zkreslovat	k5eAaImIp3nP	zkreslovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kubiceho	Kubice	k1gMnSc2	Kubice
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
šéf	šéf	k1gMnSc1	šéf
Útvaru	útvar	k1gInSc2	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
Jan	Jan	k1gMnSc1	Jan
Kubice	kubika	k1gFnSc6	kubika
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
policejní	policejní	k2eAgNnSc1d1	policejní
prezidium	prezidium	k1gNnSc1	prezidium
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
závažných	závažný	k2eAgInPc2d1	závažný
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
prorůstání	prorůstání	k1gNnSc4	prorůstání
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
do	do	k7c2	do
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
míst	místo	k1gNnPc2	místo
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
zprávě	zpráva	k1gFnSc6	zpráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
unikla	uniknout	k5eAaPmAgFnS	uniknout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
citováno	citován	k2eAgNnSc1d1	citováno
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Paroubek	Paroubek	k1gInSc1	Paroubek
pohlavně	pohlavně	k6eAd1	pohlavně
zneužíval	zneužívat	k5eAaImAgInS	zneužívat
nezletilou	zletilý	k2eNgFnSc4d1	nezletilá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
podnikatele	podnikatel	k1gMnSc2	podnikatel
Bohumíra	Bohumír	k1gMnSc2	Bohumír
Ďurička	Ďurička	k1gFnSc1	Ďurička
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
oba	dva	k4xCgMnPc1	dva
naprosto	naprosto	k6eAd1	naprosto
popřeli	popřít	k5eAaPmAgMnP	popřít
a	a	k8xC	a
na	na	k7c4	na
Kubiceho	Kubice	k1gMnSc4	Kubice
podal	podat	k5eAaPmAgInS	podat
Paroubek	Paroubek	k1gInSc1	Paroubek
trestní	trestní	k2eAgFnSc2d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
pro	pro	k7c4	pro
pomluvu	pomluva	k1gFnSc4	pomluva
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
od	od	k7c2	od
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
tzv.	tzv.	kA	tzv.
Kubiceho	Kubice	k1gMnSc2	Kubice
zprávy	zpráva	k1gFnSc2	zpráva
se	se	k3xPyFc4	se
obvinění	obvinění	k1gNnSc1	obvinění
ohledně	ohledně	k7c2	ohledně
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
obsah	obsah	k1gInSc4	obsah
této	tento	k3xDgFnSc2	tento
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
večer	večer	k6eAd1	večer
Paroubek	Paroubek	k1gInSc1	Paroubek
obvinil	obvinit	k5eAaPmAgInS	obvinit
ODS	ODS	kA	ODS
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
média	médium	k1gNnPc4	médium
z	z	k7c2	z
neférově	férově	k6eNd1	férově
vedené	vedený	k2eAgFnSc2d1	vedená
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
demokracie	demokracie	k1gFnSc1	demokracie
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
zásah	zásah	k1gInSc4	zásah
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
snad	snad	k9	snad
jen	jen	k9	jen
s	s	k7c7	s
únorem	únor	k1gInSc7	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSSD	ČSSD	kA	ČSSD
hodlá	hodlat	k5eAaImIp3nS	hodlat
přezkoumat	přezkoumat	k5eAaPmF	přezkoumat
možnost	možnost	k1gFnSc4	možnost
podat	podat	k5eAaPmF	podat
žalobu	žaloba	k1gFnSc4	žaloba
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
na	na	k7c4	na
neplatnost	neplatnost	k1gFnSc4	neplatnost
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
zvedlo	zvednout	k5eAaPmAgNnS	zvednout
vlnu	vlna	k1gFnSc4	vlna
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
Paroubek	Paroubek	k1gInSc1	Paroubek
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
odvolal	odvolat	k5eAaPmAgInS	odvolat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Povolební	povolební	k2eAgInSc1d1	povolební
projev	projev	k1gInSc1	projev
J.	J.	kA	J.
Paroubka	Paroubka	k1gFnSc1	Paroubka
také	také	k6eAd1	také
podnítil	podnítit	k5eAaPmAgMnS	podnítit
protikomunistického	protikomunistický	k2eAgMnSc4d1	protikomunistický
aktivistu	aktivista	k1gMnSc4	aktivista
Jana	Jan	k1gMnSc4	Jan
Šinágla	Šinágl	k1gMnSc4	Šinágl
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
petice	petice	k1gFnSc2	petice
požadující	požadující	k2eAgNnPc1d1	požadující
odstoupení	odstoupení	k1gNnPc1	odstoupení
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
z	z	k7c2	z
politických	politický	k2eAgFnPc2d1	politická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
peticí	petice	k1gFnSc7	petice
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
pěti	pět	k4xCc2	pět
dní	den	k1gInPc2	den
přibylo	přibýt	k5eAaPmAgNnS	přibýt
přes	přes	k7c4	přes
62	[number]	k4	62
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
J.	J.	kA	J.
Paroubek	Paroubek	k1gInSc1	Paroubek
obvinil	obvinit	k5eAaPmAgMnS	obvinit
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
ze	z	k7c2	z
"	"	kIx"	"
<g/>
skandálního	skandální	k2eAgInSc2d1	skandální
pokusu	pokus	k1gInSc2	pokus
o	o	k7c6	o
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
veřejným	veřejný	k2eAgNnSc7d1	veřejné
míněním	mínění	k1gNnSc7	mínění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
přitom	přitom	k6eAd1	přitom
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSSD	ČSSD	kA	ČSSD
sama	sám	k3xTgFnSc1	sám
organizuje	organizovat	k5eAaBmIp3nS	organizovat
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
během	během	k7c2	během
diskusních	diskusní	k2eAgInPc2d1	diskusní
pořadů	pořad	k1gInPc2	pořad
účastnili	účastnit	k5eAaImAgMnP	účastnit
SMS	SMS	kA	SMS
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
také	také	k9	také
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
stížnost	stížnost	k1gFnSc4	stížnost
šéfa	šéf	k1gMnSc2	šéf
úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
Ivana	Ivan	k1gMnSc2	Ivan
Přikryla	Přikryl	k1gMnSc2	Přikryl
na	na	k7c4	na
pořad	pořad	k1gInSc4	pořad
Bez	bez	k7c2	bez
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
hanlivě	hanlivě	k6eAd1	hanlivě
se	se	k3xPyFc4	se
zmiňoval	zmiňovat	k5eAaImAgInS	zmiňovat
také	také	k9	také
o	o	k7c6	o
pořadu	pořad	k1gInSc6	pořad
Budování	budování	k1gNnSc2	budování
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
také	také	k9	také
zpřísnit	zpřísnit	k5eAaPmF	zpřísnit
český	český	k2eAgInSc4d1	český
tiskový	tiskový	k2eAgInSc4d1	tiskový
zákon	zákon	k1gInSc4	zákon
podle	podle	k7c2	podle
rakouského	rakouský	k2eAgInSc2d1	rakouský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
pořady	pořad	k1gInPc1	pořad
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
zmizely	zmizet	k5eAaPmAgFnP	zmizet
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Paroubkova	Paroubkův	k2eAgInSc2d1	Paroubkův
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Moderátor	moderátor	k1gMnSc1	moderátor
Aleš	Aleš	k1gMnSc1	Aleš
Cibulka	Cibulka	k1gMnSc1	Cibulka
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
odvolán	odvolat	k5eAaPmNgInS	odvolat
z	z	k7c2	z
moderování	moderování	k1gNnSc2	moderování
pořadu	pořad	k1gInSc2	pořad
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
Yvonne	Yvonn	k1gInSc5	Yvonn
Přenosilová	Přenosilová	k1gFnSc1	Přenosilová
označila	označit	k5eAaPmAgFnS	označit
chování	chování	k1gNnSc4	chování
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
za	za	k7c4	za
podobné	podobný	k2eAgFnPc4d1	podobná
stylu	styl	k1gInSc3	styl
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
a	a	k8xC	a
Zelené	Zelené	k2eAgFnSc2d1	Zelené
za	za	k7c4	za
impotentní	impotentní	k2eAgFnSc4d1	impotentní
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Moderátor	moderátor	k1gMnSc1	moderátor
David	David	k1gMnSc1	David
Borek	Borek	k1gMnSc1	Borek
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
odvolán	odvolat	k5eAaPmNgInS	odvolat
z	z	k7c2	z
moderování	moderování	k1gNnSc2	moderování
pořadu	pořad	k1gInSc2	pořad
Události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
vedoucího	vedoucí	k1gMnSc2	vedoucí
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Šámala	Šámal	k1gMnSc2	Šámal
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Paroubkem	Paroubek	k1gInSc7	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
během	během	k7c2	během
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
střet	střet	k1gInSc4	střet
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
povinnost	povinnost	k1gFnSc4	povinnost
rezignovat	rezignovat	k5eAaBmF	rezignovat
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
funkce	funkce	k1gFnPc4	funkce
při	při	k7c6	při
jmenování	jmenování	k1gNnSc6	jmenování
Davida	David	k1gMnSc2	David
Ratha	Rath	k1gMnSc2	Rath
ministrem	ministr	k1gMnSc7	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Paroubka	Paroubek	k1gMnSc2	Paroubek
obvinil	obvinit	k5eAaPmAgInS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
vědomě	vědomě	k6eAd1	vědomě
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c4	v
omyl	omyl	k1gInSc4	omyl
<g/>
:	:	kIx,	:
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
písemném	písemný	k2eAgNnSc6d1	písemné
čestném	čestný	k2eAgNnSc6d1	čestné
prohlášení	prohlášení	k1gNnSc6	prohlášení
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
svého	svůj	k3xOyFgNnSc2	svůj
jmenování	jmenování	k1gNnSc2	jmenování
nevykonává	vykonávat	k5eNaImIp3nS	vykonávat
žádnou	žádný	k3yNgFnSc4	žádný
činnost	činnost	k1gFnSc4	činnost
nebo	nebo	k8xC	nebo
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
reagoval	reagovat	k5eAaBmAgInS	reagovat
citací	citace	k1gFnSc7	citace
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vzdát	vzdát	k5eAaPmF	vzdát
vyjmenovaných	vyjmenovaný	k2eAgFnPc2d1	vyjmenovaná
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
době	doba	k1gFnSc6	doba
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc4	Paroubek
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
paragrafu	paragraf	k1gInSc6	paragraf
pět	pět	k4xCc4	pět
odstavci	odstavec	k1gInSc6	odstavec
dvě	dva	k4xCgFnPc4	dva
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
předkládá	předkládat	k5eAaImIp3nS	předkládat
čestné	čestný	k2eAgNnSc1d1	čestné
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
do	do	k7c2	do
30	[number]	k4	30
dnů	den	k1gInPc2	den
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
výkonu	výkon	k1gInSc2	výkon
veřejné	veřejný	k2eAgFnSc2d1	veřejná
funkce	funkce	k1gFnSc2	funkce
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
informoval	informovat	k5eAaBmAgInS	informovat
internetový	internetový	k2eAgInSc1d1	internetový
deník	deník	k1gInSc1	deník
iDnes	iDnesa	k1gFnPc2	iDnesa
o	o	k7c6	o
neprůhledném	průhledný	k2eNgNnSc6d1	neprůhledné
financování	financování	k1gNnSc6	financování
časopisu	časopis	k1gInSc2	časopis
Trend	trend	k1gInSc1	trend
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vydávání	vydávání	k1gNnSc4	vydávání
Paroubek	Paroubka	k1gFnPc2	Paroubka
řídí	řídit	k5eAaImIp3nP	řídit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
předsedy	předseda	k1gMnSc2	předseda
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
odpovědi	odpověď	k1gFnSc2	odpověď
Paroubka	Paroubek	k1gMnSc2	Paroubek
je	být	k5eAaImIp3nS	být
hrazeno	hrazen	k2eAgNnSc1d1	hrazeno
vydávání	vydávání	k1gNnSc1	vydávání
časopisu	časopis	k1gInSc2	časopis
z	z	k7c2	z
inzerce	inzerce	k1gFnSc2	inzerce
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
iDnes	iDnesa	k1gFnPc2	iDnesa
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
vydaných	vydaný	k2eAgNnPc2d1	vydané
čísel	číslo	k1gNnPc2	číslo
žádná	žádný	k3yNgFnSc1	žádný
inzerce	inzerce	k1gFnSc1	inzerce
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Paroubek	Paroubek	k1gInSc1	Paroubek
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
část	část	k1gFnSc4	část
údajů	údaj	k1gInPc2	údaj
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
časopisu	časopis	k1gInSc2	časopis
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
inzerentech	inzerent	k1gMnPc6	inzerent
a	a	k8xC	a
sponzorech	sponzor	k1gMnPc6	sponzor
za	za	k7c2	za
léta	léto	k1gNnSc2	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Václav	Václav	k1gMnSc1	Václav
Kočka	kočka	k1gFnSc1	kočka
mladší	mladý	k2eAgMnPc1d2	mladší
a	a	k8xC	a
Bohumír	Bohumír	k1gMnSc1	Bohumír
Ďuričko	Ďurička	k1gFnSc5	Ďurička
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
křest	křest	k1gInSc1	křest
Paroubkovy	Paroubkův	k2eAgFnSc2d1	Paroubkova
knihy	kniha	k1gFnSc2	kniha
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
svět	svět	k1gInSc1	svět
očima	oko	k1gNnPc7	oko
sociálního	sociální	k2eAgMnSc2d1	sociální
demokrata	demokrat	k1gMnSc2	demokrat
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
restauraci	restaurace	k1gFnSc6	restaurace
Monarch	Monarcha	k1gFnPc2	Monarcha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
akce	akce	k1gFnSc2	akce
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
pozvanými	pozvaný	k2eAgMnPc7d1	pozvaný
hosty	host	k1gMnPc7	host
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
mezi	mezi	k7c7	mezi
podnikatelem	podnikatel	k1gMnSc7	podnikatel
Bohumírem	Bohumír	k1gMnSc7	Bohumír
Ďuričkem	Ďuriček	k1gMnSc7	Ďuriček
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
provozovatele	provozovatel	k1gMnSc2	provozovatel
Matějské	matějský	k2eAgFnSc2d1	Matějská
pouti	pouť	k1gFnSc2	pouť
Václavem	Václav	k1gMnSc7	Václav
Kočkou	kočka	k1gFnSc7	kočka
ml.	ml.	kA	ml.
Výsledkem	výsledek	k1gInSc7	výsledek
incidentu	incident	k1gInSc2	incident
byla	být	k5eAaImAgFnS	být
střelba	střelba	k1gFnSc1	střelba
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
druhého	druhý	k4xOgMnSc2	druhý
jmenovaného	jmenovaný	k1gMnSc2	jmenovaný
<g/>
.	.	kIx.	.
</s>
<s>
Ďuričko	Ďuričko	k6eAd1	Ďuričko
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
12,5	[number]	k4	12,5
roku	rok	k1gInSc2	rok
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
od	od	k7c2	od
incidentu	incident	k1gInSc2	incident
distancoval	distancovat	k5eAaBmAgMnS	distancovat
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bohumíra	Bohumír	k1gMnSc4	Bohumír
Ďuričku	Ďurička	k1gMnSc4	Ďurička
vlastně	vlastně	k9	vlastně
nezná	znát	k5eNaImIp3nS	znát
a	a	k8xC	a
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
křest	křest	k1gInSc4	křest
knihy	kniha	k1gFnSc2	kniha
ho	on	k3xPp3gMnSc4	on
nezval	zvát	k5eNaImAgMnS	zvát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
známosti	známost	k1gFnSc6	známost
Paroubka	Paroubka	k1gFnSc1	Paroubka
s	s	k7c7	s
Ďuričkem	Ďuriček	k1gInSc7	Ďuriček
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
psalo	psát	k5eAaImAgNnS	psát
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
měl	mít	k5eAaImAgInS	mít
Paroubek	Paroubek	k1gInSc4	Paroubek
strávit	strávit	k5eAaPmF	strávit
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
;	;	kIx,	;
zrušil	zrušit	k5eAaPmAgInS	zrušit
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ďuričko	Ďurička	k1gFnSc5	Ďurička
byl	být	k5eAaImAgMnS	být
agentem	agent	k1gMnSc7	agent
StB	StB	k1gFnSc2	StB
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
Paroubkovi	Paroubek	k1gMnSc3	Paroubek
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
Paroubkovi	Paroubek	k1gMnSc3	Paroubek
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
zdvihly	zdvihnout	k5eAaPmAgInP	zdvihnout
veřejné	veřejný	k2eAgInPc1d1	veřejný
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
vrhání	vrhání	k1gNnSc1	vrhání
vajec	vejce	k1gNnPc2	vejce
na	na	k7c4	na
pódia	pódium	k1gNnPc4	pódium
během	během	k7c2	během
předvolebních	předvolební	k2eAgInPc2d1	předvolební
mítinků	mítink	k1gInPc2	mítink
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
vygradovaly	vygradovat	k5eAaPmAgInP	vygradovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c4	na
Andělu	Anděla	k1gFnSc4	Anděla
setkaly	setkat	k5eAaPmAgFnP	setkat
desítky	desítka	k1gFnPc1	desítka
odpůrců	odpůrce	k1gMnPc2	odpůrce
politiky	politika	k1gFnSc2	politika
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
a	a	k8xC	a
zasypaly	zasypat	k5eAaPmAgFnP	zasypat
předsednictvo	předsednictvo	k1gNnSc4	předsednictvo
ČSSD	ČSSD	kA	ČSSD
stovkami	stovka	k1gFnPc7	stovka
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
útoku	útok	k1gInSc2	útok
obvinila	obvinit	k5eAaPmAgFnS	obvinit
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
to	ten	k3xDgNnSc4	ten
popřela	popřít	k5eAaPmAgFnS	popřít
a	a	k8xC	a
označila	označit	k5eAaPmAgFnS	označit
akci	akce	k1gFnSc4	akce
jako	jako	k8xS	jako
řízenou	řízený	k2eAgFnSc4d1	řízená
provokaci	provokace	k1gFnSc4	provokace
Lidového	lidový	k2eAgInSc2d1	lidový
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k1gFnSc1	Google
bomba	bomba	k1gFnSc1	bomba
namyšlenej	namyšlenej	k?	namyšlenej
buran	buran	k1gInSc4	buran
Petra	Petr	k1gMnSc2	Petr
Paroubková	Paroubkový	k2eAgFnSc1d1	Paroubková
Protesty	protest	k1gInPc7	protest
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
Paroubkovi	Paroubek	k1gMnSc3	Paroubek
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
kampani	kampaň	k1gFnSc6	kampaň
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc1	Paroubek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubka	k1gFnPc2	Paroubka
Osobní	osobní	k2eAgFnSc2d1	osobní
stránky	stránka	k1gFnSc2	stránka
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc1	Paroubek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Blog	Blog	k1gMnSc1	Blog
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
na	na	k7c6	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Blog	Blog	k1gInSc1	Blog
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubka	k1gFnSc1	Paroubka
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jiří	Jiří	k1gMnSc1	Jiří
Paroubek	Paroubek	k1gInSc1	Paroubek
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Impulsy	impuls	k1gInPc4	impuls
Václava	Václav	k1gMnSc4	Václav
Moravce	Moravec	k1gMnSc4	Moravec
</s>
