<p>
<s>
Allsvenskan	Allsvenskan	k1gMnSc1	Allsvenskan
resp.	resp.	kA	resp.
Fotbollsallsvenskan	Fotbollsallsvenskan	k1gMnSc1	Fotbollsallsvenskan
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
ligová	ligový	k2eAgFnSc1d1	ligová
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
soutěž	soutěž	k1gFnSc1	soutěž
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
hraje	hrát	k5eAaImIp3nS	hrát
16	[number]	k4	16
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
lize	liga	k1gFnSc6	liga
Superettan	Superettan	k1gInSc4	Superettan
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
až	až	k6eAd1	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
hraje	hrát	k5eAaImIp3nS	hrát
30	[number]	k4	30
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Liga	liga	k1gFnSc1	liga
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
soutěž	soutěž	k1gFnSc1	soutěž
Svenska	Svensko	k1gNnSc2	Svensko
Serien	Serina	k1gFnPc2	Serina
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
a	a	k8xC	a
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
obdrží	obdržet	k5eAaPmIp3nS	obdržet
trofej	trofej	k1gFnSc4	trofej
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Lennart	Lennarta	k1gFnPc2	Lennarta
Johanssons	Johanssons	k1gInSc1	Johanssons
Pokal	pokalit	k5eAaPmRp2nS	pokalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgMnSc1d1	současný
vítěz	vítěz	k1gMnSc1	vítěz
ligy	liga	k1gFnSc2	liga
je	být	k5eAaImIp3nS	být
tým	tým	k1gInSc1	tým
Malmö	Malmö	k1gFnSc2	Malmö
FF	ff	kA	ff
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
vítězů	vítěz	k1gMnPc2	vítěz
==	==	k?	==
</s>
</p>
<p>
<s>
VysvětlivkaPozn	VysvětlivkaPozn	k1gNnSc1	VysvětlivkaPozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
číslo	číslo	k1gNnSc1	číslo
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
znamená	znamenat	k5eAaImIp3nS	znamenat
počet	počet	k1gInSc1	počet
získaných	získaný	k2eAgInPc2d1	získaný
titulů	titul	k1gInPc2	titul
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
roku	rok	k1gInSc3	rok
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
a	a	k8xC	a
města	město	k1gNnPc4	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Allsvenskan	Allsvenskana	k1gFnPc2	Allsvenskana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Guldbollen	Guldbollen	k1gInSc1	Guldbollen
</s>
</p>
<p>
<s>
Svenska	Svensko	k1gNnPc1	Svensko
Cupen	Cupno	k1gNnPc2	Cupno
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Allsvenskan	Allsvenskana	k1gFnPc2	Allsvenskana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
