<s>
Rozmarné	rozmarný	k2eAgNnSc1d1	Rozmarné
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Menzela	Menzela	k1gMnSc2	Menzela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
deštivé	deštivý	k2eAgNnSc1d1	deštivé
letní	letní	k2eAgNnSc1d1	letní
odpoledne	odpoledne	k1gNnSc1	odpoledne
se	se	k3xPyFc4	se
na	na	k7c6	na
prázdné	prázdný	k2eAgFnSc6d1	prázdná
staré	starý	k2eAgFnSc6d1	stará
plovárně	plovárna	k1gFnSc6	plovárna
malého	malý	k2eAgNnSc2d1	malé
města	město	k1gNnSc2	město
sejdou	sejít	k5eAaPmIp3nP	sejít
major	major	k1gMnSc1	major
Hugo	Hugo	k1gMnSc1	Hugo
(	(	kIx(	(
<g/>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abbé	abbé	k1gMnSc1	abbé
Roch	roch	k0	roch
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Řehák	Řehák	k1gMnSc1	Řehák
<g/>
)	)	kIx)	)
a	a	k8xC	a
majitel	majitel	k1gMnSc1	majitel
těchto	tento	k3xDgFnPc2	tento
říčních	říční	k2eAgFnPc2d1	říční
lázní	lázeň	k1gFnPc2	lázeň
Antonín	Antonín	k1gMnSc1	Antonín
Důra	důra	k1gFnSc1	důra
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pánové	pán	k1gMnPc1	pán
probírají	probírat	k5eAaImIp3nP	probírat
vznešeným	vznešený	k2eAgInSc7d1	vznešený
<g/>
,	,	kIx,	,
vzdělaným	vzdělaný	k2eAgInSc7d1	vzdělaný
jazykem	jazyk	k1gInSc7	jazyk
naprosté	naprostý	k2eAgFnSc2d1	naprostá
životní	životní	k2eAgFnSc2d1	životní
nicotnosti	nicotnost	k1gFnSc2	nicotnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
nečinné	činný	k2eNgNnSc1d1	nečinné
klábosení	klábosení	k1gNnSc1	klábosení
přerušeno	přerušen	k2eAgNnSc1d1	přerušeno
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
vytrženi	vytrhnout	k5eAaPmNgMnP	vytrhnout
z	z	k7c2	z
letargie	letargie	k1gFnSc2	letargie
příjezdem	příjezd	k1gInSc7	příjezd
pouťových	pouťový	k2eAgMnPc2d1	pouťový
komediantů	komediant	k1gMnPc2	komediant
<g/>
,	,	kIx,	,
kouzelníka	kouzelník	k1gMnSc2	kouzelník
Arnoštka	Arnoštek	k1gMnSc2	Arnoštek
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
společnice	společnice	k1gFnSc1	společnice
Anny	Anna	k1gFnSc2	Anna
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
snaží	snažit	k5eAaImIp3nP	snažit
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
slečnou	slečna	k1gFnSc7	slečna
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
barevný	barevný	k2eAgInSc1d1	barevný
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
Igora	Igor	k1gMnSc2	Igor
Bauersimu	Bauersim	k1gInSc2	Bauersim
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Launischer	Launischra	k1gFnPc2	Launischra
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
inscenoval	inscenovat	k5eAaBmAgMnS	inscenovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
düsseldorfském	düsseldorfský	k2eAgNnSc6d1	düsseldorfské
divadle	divadlo	k1gNnSc6	divadlo
Düsseldorfer	Düsseldorfer	k1gMnSc1	Düsseldorfer
Schauspielhaus	Schauspielhaus	k1gMnSc1	Schauspielhaus
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
soutěž	soutěž	k1gFnSc4	soutěž
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
filmový	filmový	k2eAgInSc4d1	filmový
festival	festival	k1gInSc4	festival
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
Křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
Antonína	Antonín	k1gMnSc2	Antonín
Důry	důra	k1gFnSc2	důra
pochází	pocházet	k5eAaImIp3nS	pocházet
proslulý	proslulý	k2eAgInSc1d1	proslulý
výrok	výrok	k1gInSc1	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
léta	léto	k1gNnSc2	léto
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
poněkud	poněkud	k6eAd1	poněkud
nešťastným	šťastný	k2eNgMnPc3d1	nešťastný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rozmarné	rozmarný	k2eAgNnSc1d1	Rozmarné
léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
