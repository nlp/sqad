<p>
<s>
Algologie	algologie	k1gFnSc1	algologie
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
také	také	k9	také
fykologie	fykologie	k1gFnSc1	fykologie
<g/>
,	,	kIx,	,
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
od	od	k7c2	od
algae	alga	k1gFnSc2	alga
=	=	kIx~	=
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
řas	řasa	k1gFnPc2	řasa
a	a	k8xC	a
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Algologie	algologie	k1gFnSc1	algologie
jako	jako	k8xC	jako
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
má	mít	k5eAaImIp3nS	mít
široký	široký	k2eAgInSc1d1	široký
rozptyl	rozptyl	k1gInSc1	rozptyl
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
systematikou	systematika	k1gFnSc7	systematika
<g/>
,	,	kIx,	,
fylogenezí	fylogeneze	k1gFnSc7	fylogeneze
a	a	k8xC	a
ekologií	ekologie	k1gFnSc7	ekologie
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
však	však	k9	však
také	také	k9	také
<g/>
:	:	kIx,	:
fyziologii	fyziologie	k1gFnSc4	fyziologie
<g/>
,	,	kIx,	,
biochemii	biochemie	k1gFnSc4	biochemie
<g/>
,	,	kIx,	,
genetiku	genetika	k1gFnSc4	genetika
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
např.	např.	kA	např.
řeší	řešit	k5eAaImIp3nP	řešit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
sinice	sinice	k1gFnSc1	sinice
využít	využít	k5eAaPmF	využít
v	v	k7c6	v
biotechnologiích	biotechnologie	k1gFnPc6	biotechnologie
<g/>
,	,	kIx,	,
či	či	k8xC	či
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
algologie	algologie	k1gFnSc2	algologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
algologie	algologie	k1gFnSc2	algologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
