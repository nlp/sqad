<s>
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
též	též	k9	též
Baťovy	Baťův	k2eAgInPc4d1	Baťův
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
velké	velký	k2eAgFnSc2d1	velká
české	český	k2eAgFnSc2d1	Česká
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založili	založit	k5eAaPmAgMnP	založit
sourozenci	sourozenec	k1gMnPc1	sourozenec
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťové	Baťová	k1gFnSc2	Baťová
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
v	v	k7c6	v
září	září	k1gNnSc6	září
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
nesla	nést	k5eAaImAgFnS	nést
až	až	k9	až
do	do	k7c2	do
změny	změna	k1gFnSc2	změna
právní	právní	k2eAgFnSc2d1	právní
subjektivity	subjektivita	k1gFnSc2	subjektivita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
jména	jméno	k1gNnSc2	jméno
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
slovenské	slovenský	k2eAgInPc1d1	slovenský
závody	závod	k1gInPc1	závod
vyčleněny	vyčlenit	k5eAaPmNgInP	vyčlenit
pod	pod	k7c4	pod
"	"	kIx"	"
<g/>
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
účastinná	účastinný	k2eAgFnSc1d1	účastinný
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
<g/>
"	"	kIx"	"
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Šimonovanech	Šimonovan	k1gMnPc6	Šimonovan
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
koncern	koncern	k1gInSc1	koncern
Baťa	Baťa	k1gMnSc1	Baťa
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
svého	svůj	k3xOyFgNnSc2	svůj
znárodnění	znárodnění	k1gNnSc2	znárodnění
do	do	k7c2	do
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
výrobních	výrobní	k2eAgNnPc2d1	výrobní
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1949	[number]	k4	1949
byly	být	k5eAaImAgFnP	být
zlínské	zlínský	k2eAgInPc4d1	zlínský
závody	závod	k1gInPc4	závod
Baťa	Baťa	k1gMnSc1	Baťa
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
Svit	svit	k1gInSc4	svit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slovenské	slovenský	k2eAgFnSc2d1	slovenská
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
obuvi	obuv	k1gFnSc2	obuv
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
Závody	závod	k1gInPc7	závod
29	[number]	k4	29
<g/>
.	.	kIx.	.
augusta	august	k1gMnSc4	august
(	(	kIx(	(
<g/>
ZDA	zda	k8xS	zda
<g/>
)	)	kIx)	)
v	v	k7c6	v
Partizánském	partizánský	k2eAgNnSc6d1	Partizánské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
Baťovy	Baťův	k2eAgFnPc1d1	Baťova
továrny	továrna	k1gFnPc1	továrna
v	v	k7c6	v
socialistických	socialistický	k2eAgInPc6d1	socialistický
státech	stát	k1gInPc6	stát
znárodněny	znárodněn	k2eAgInPc1d1	znárodněn
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Pobočky	pobočka	k1gFnPc1	pobočka
v	v	k7c6	v
kapitalistických	kapitalistický	k2eAgInPc6d1	kapitalistický
státech	stát	k1gInPc6	stát
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
majetku	majetek	k1gInSc6	majetek
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
řízeny	řídit	k5eAaImNgFnP	řídit
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
evropská	evropský	k2eAgFnSc1d1	Evropská
centrála	centrála	k1gFnSc1	centrála
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
Centrála	centrála	k1gFnSc1	centrála
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Guinnessova	Guinnessův	k2eAgFnSc1d1	Guinnessova
kniha	kniha	k1gFnSc1	kniha
rekordů	rekord	k1gInPc2	rekord
firmu	firma	k1gFnSc4	firma
Baťa	Baťa	k1gMnSc1	Baťa
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
Největšího	veliký	k2eAgMnSc2d3	veliký
prodejce	prodejce	k1gMnSc2	prodejce
a	a	k8xC	a
výrobce	výrobce	k1gMnSc2	výrobce
bot	bota	k1gFnPc2	bota
<g/>
"	"	kIx"	"
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
firma	firma	k1gFnSc1	firma
prodala	prodat	k5eAaPmAgFnS	prodat
již	již	k6eAd1	již
přes	přes	k7c4	přes
14	[number]	k4	14
miliard	miliarda	k4xCgFnPc2	miliarda
párů	pár	k1gInPc2	pár
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
koncernu	koncern	k1gInSc2	koncern
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
zahájili	zahájit	k5eAaPmAgMnP	zahájit
tři	tři	k4xCgMnPc1	tři
sourozenci	sourozenec	k1gMnPc1	sourozenec
Baťové	Baťová	k1gFnSc2	Baťová
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
-	-	kIx~	-
obuvnické	obuvnický	k2eAgNnSc1d1	Obuvnické
podnikání	podnikání	k1gNnSc1	podnikání
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
jménem	jméno	k1gNnSc7	jméno
A.	A.	kA	A.
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
měla	mít	k5eAaImAgFnS	mít
firma	firma	k1gFnSc1	firma
právní	právní	k2eAgFnSc4d1	právní
podobu	podoba	k1gFnSc4	podoba
veřejné	veřejný	k2eAgFnSc2d1	veřejná
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
120	[number]	k4	120
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
majitelem	majitel	k1gMnSc7	majitel
firmy	firma	k1gFnSc2	firma
T.	T.	kA	T.
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Tomáši	Tomáš	k1gMnSc3	Tomáš
Baťovi	Baťa	k1gMnSc3	Baťa
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
zakázku	zakázka	k1gFnSc4	zakázka
od	od	k7c2	od
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
měla	mít	k5eAaImAgFnS	mít
z	z	k7c2	z
válečných	válečný	k2eAgFnPc2d1	válečná
dob	doba	k1gFnPc2	doba
víc	hodně	k6eAd2	hodně
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k9	kolik
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
odbyt	odbyt	k1gInSc4	odbyt
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
Baťa	Baťa	k1gMnSc1	Baťa
pokusil	pokusit	k5eAaPmAgMnS	pokusit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
úvěrovou	úvěrový	k2eAgFnSc7d1	úvěrová
zadlužeností	zadluženost	k1gFnSc7	zadluženost
firmy	firma	k1gFnSc2	firma
překonat	překonat	k5eAaPmF	překonat
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
úvěrovým	úvěrový	k2eAgInSc7d1	úvěrový
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
zřízením	zřízení	k1gNnSc7	zřízení
podnikové	podnikový	k2eAgFnSc2d1	podniková
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
situace	situace	k1gFnSc2	situace
přinesla	přinést	k5eAaPmAgFnS	přinést
akce	akce	k1gFnSc1	akce
Baťa	Baťa	k1gMnSc1	Baťa
drtí	drtit	k5eAaImIp3nS	drtit
drahotu	drahota	k1gFnSc4	drahota
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
firma	firma	k1gFnSc1	firma
snížila	snížit	k5eAaPmAgFnS	snížit
cenu	cena	k1gFnSc4	cena
obuvi	obuv	k1gFnSc2	obuv
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1922	[number]	k4	1922
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
cen	cena	k1gFnPc2	cena
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
spoluúčast	spoluúčast	k1gFnSc1	spoluúčast
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
a	a	k8xC	a
ztrátě	ztráta	k1gFnSc3	ztráta
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výstavba	výstavba	k1gFnSc1	výstavba
moderních	moderní	k2eAgFnPc2d1	moderní
továrních	tovární	k2eAgFnPc2d1	tovární
budov	budova	k1gFnPc2	budova
s	s	k7c7	s
železobetonovým	železobetonový	k2eAgInSc7d1	železobetonový
skeletem	skelet	k1gInSc7	skelet
<g/>
.	.	kIx.	.
</s>
<s>
Pásová	pásový	k2eAgFnSc1d1	pásová
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Henryho	Henry	k1gMnSc2	Henry
Forda	ford	k1gMnSc2	ford
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
a	a	k8xC	a
již	již	k6eAd1	již
zanedlouho	zanedlouho	k6eAd1	zanedlouho
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zvýšit	zvýšit	k5eAaPmF	zvýšit
výrobu	výroba	k1gFnSc4	výroba
o	o	k7c4	o
58	[number]	k4	58
%	%	kIx~	%
procent	procent	k1gInSc4	procent
při	při	k7c6	při
zvýšení	zvýšení	k1gNnSc6	zvýšení
počtu	počet	k1gInSc2	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
o	o	k7c4	o
35	[number]	k4	35
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1931	[number]	k4	1931
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
veřejná	veřejný	k2eAgFnSc1d1	veřejná
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
T.	T.	kA	T.
&	&	k?	&
A.	A.	kA	A.
Baťa	Baťa	k1gMnSc1	Baťa
na	na	k7c6	na
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1932	[number]	k4	1932
koncern	koncern	k1gInSc1	koncern
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jeho	jeho	k3xOp3gMnSc2	jeho
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
expandoval	expandovat	k5eAaImAgInS	expandovat
<g/>
,	,	kIx,	,
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
výrobní	výrobní	k2eAgInPc4d1	výrobní
obory	obor	k1gInPc4	obor
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
technické	technický	k2eAgFnSc2d1	technická
pryže	pryž	k1gFnSc2	pryž
<g/>
,	,	kIx,	,
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
hraček	hračka	k1gFnPc2	hračka
<g/>
,	,	kIx,	,
kovoobráběcích	kovoobráběcí	k2eAgMnPc2d1	kovoobráběcí
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
pletacích	pletací	k2eAgInPc2d1	pletací
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
koncernu	koncern	k1gInSc3	koncern
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
stavební	stavební	k2eAgFnSc1d1	stavební
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
uhelné	uhelný	k2eAgInPc1d1	uhelný
doly	dol	k1gInPc1	dol
<g/>
,	,	kIx,	,
63	[number]	k4	63
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
-	-	kIx~	-
<g/>
Vizovice	Vizovice	k1gFnPc1	Vizovice
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc1d1	cestovní
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Otrokovicích	Otrokovice	k1gFnPc6	Otrokovice
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInPc1d1	obchodní
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
prodejny	prodejna	k1gFnPc1	prodejna
-	-	kIx~	-
cca	cca	kA	cca
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
prodejen	prodejna	k1gFnPc2	prodejna
v	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
bytový	bytový	k2eAgInSc1d1	bytový
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnPc4d1	vlastní
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Koncern	koncern	k1gInSc1	koncern
zaměstnával	zaměstnávat	k5eAaImAgInS	zaměstnávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
67	[number]	k4	67
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
000	[number]	k4	000
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Protektorátu	protektorát	k1gInSc2	protektorát
opustil	opustit	k5eAaPmAgMnS	opustit
zemi	zem	k1gFnSc4	zem
šéf	šéf	k1gMnSc1	šéf
koncernu	koncern	k1gInSc2	koncern
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
budoval	budovat	k5eAaImAgMnS	budovat
závod	závod	k1gInSc4	závod
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
synovec	synovec	k1gMnSc1	synovec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jan	Jan	k1gMnSc1	Jan
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
podniku	podnik	k1gInSc2	podnik
tak	tak	k6eAd1	tak
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
tvořili	tvořit	k5eAaImAgMnP	tvořit
Dominik	Dominik	k1gMnSc1	Dominik
Čipera	Čipera	k1gMnSc1	Čipera
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Vavrečka	Vavrečka	k1gFnSc1	Vavrečka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Malota	Malot	k1gMnSc2	Malot
a	a	k8xC	a
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tragické	tragický	k2eAgFnSc2d1	tragická
smrti	smrt	k1gFnSc2	smrt
i	i	k8xC	i
Josef	Josef	k1gMnSc1	Josef
Hlavnička	hlavnička	k1gFnSc1	hlavnička
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
podniku	podnik	k1gInSc2	podnik
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
centrem	centr	k1gMnSc7	centr
Baťovy	Baťův	k2eAgFnSc2d1	Baťova
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
Baťa	Baťa	k1gMnSc1	Baťa
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
byl	být	k5eAaImAgInS	být
znárodněn	znárodnit	k5eAaPmNgInS	znárodnit
zestátněním	zestátnění	k1gNnSc7	zestátnění
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
Svit	svit	k2eAgInSc1d1	svit
<g/>
,	,	kIx,	,
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Svit	svit	k1gInSc1	svit
dodnes	dodnes	k6eAd1	dodnes
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
budoval	budovat	k5eAaImAgMnS	budovat
nejen	nejen	k6eAd1	nejen
továrnu	továrna	k1gFnSc4	továrna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
veškeré	veškerý	k3xTgNnSc4	veškerý
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
i	i	k9	i
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Suroviny	surovina	k1gFnPc1	surovina
sám	sám	k3xTgMnSc1	sám
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
a	a	k8xC	a
bez	bez	k7c2	bez
prostředníků	prostředník	k1gMnPc2	prostředník
prodával	prodávat	k5eAaImAgMnS	prodávat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
šetřil	šetřit	k5eAaImAgMnS	šetřit
na	na	k7c6	na
nákladech	náklad	k1gInPc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
tedy	tedy	k9	tedy
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koželužnu	koželužna	k1gFnSc4	koželužna
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
pole	pole	k1gNnPc4	pole
i	i	k8xC	i
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
budoval	budovat	k5eAaImAgMnS	budovat
Baťovy	Baťův	k2eAgInPc4d1	Baťův
domky	domek	k1gInPc4	domek
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
luxusní	luxusní	k2eAgNnSc4d1	luxusní
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
)	)	kIx)	)
a	a	k8xC	a
internáty	internát	k1gInPc1	internát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
platy	plat	k1gInPc7	plat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
přílivu	příliv	k1gInSc2	příliv
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
expanze	expanze	k1gFnSc2	expanze
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Nechyběly	chybět	k5eNaImAgInP	chybět
ani	ani	k9	ani
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgMnPc4d1	budoucí
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
školky	školka	k1gFnSc2	školka
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
Zlín	Zlín	k1gInSc1	Zlín
byl	být	k5eAaImAgInS	být
budován	budovat	k5eAaImNgInS	budovat
podle	podle	k7c2	podle
jednotného	jednotný	k2eAgInSc2d1	jednotný
funkcionalistického	funkcionalistický	k2eAgInSc2d1	funkcionalistický
plánu	plán	k1gInSc2	plán
architekta	architekt	k1gMnSc2	architekt
Františka	František	k1gMnSc2	František
Lydie	Lydie	k1gFnSc2	Lydie
Gahury	Gahura	k1gFnSc2	Gahura
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zahradní	zahradní	k2eAgNnSc1d1	zahradní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
Zlína	Zlín	k1gInSc2	Zlín
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
:	:	kIx,	:
za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
a	a	k8xC	a
hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
Studijní	studijní	k2eAgInPc1d1	studijní
ústavy	ústav	k1gInPc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
a	a	k8xC	a
Svit	svit	k1gInSc1	svit
(	(	kIx(	(
<g/>
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
:	:	kIx,	:
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
správa	správa	k1gFnSc1	správa
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Ivan	Ivan	k1gMnSc1	Ivan
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Pištěk	Pištěk	k1gMnSc1	Pištěk
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Černoch	Černoch	k1gMnSc1	Černoch
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1970	[number]	k4	1970
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hruška	Hruška	k1gMnSc1	Hruška
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
RSDr.	RSDr.	kA	RSDr.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Josef	Josef	k1gMnSc1	Josef
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Vodák	Vodák	k1gMnSc1	Vodák
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
<g />
.	.	kIx.	.
</s>
<s>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lukavský	Lukavský	k2eAgMnSc1d1	Lukavský
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Mgr.	Mgr.	kA	Mgr.
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
řídící	řídící	k2eAgFnSc2d1	řídící
struktura	struktura	k1gFnSc1	struktura
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
nepřehledná	přehledný	k2eNgFnSc1d1	nepřehledná
Bata	Bat	k2eAgFnSc1d1	Bata
Shoe	Shoe	k1gFnSc1	Shoe
Organization	Organization	k1gInSc1	Organization
(	(	kIx(	(
<g/>
od	od	k7c2	od
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jan	Jan	k1gMnSc1	Jan
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
CEO	CEO	kA	CEO
od	od	k7c2	od
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
George	Georg	k1gFnSc2	Georg
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
CEO	CEO	kA	CEO
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
kanadská	kanadský	k2eAgFnSc1d1	kanadská
<g/>
)	)	kIx)	)
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
měla	mít	k5eAaImAgFnS	mít
Bata	Bat	k2eAgFnSc1d1	Bata
Shoe	Shoe	k1gFnSc1	Shoe
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
firma	firma	k1gFnSc1	firma
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
,	,	kIx,	,
padesát	padesát	k4xCc4	padesát
výrobních	výrobní	k2eAgInPc2d1	výrobní
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
koželužen	koželužna	k1gFnPc2	koželužna
<g/>
,	,	kIx,	,
4	[number]	k4	4
743	[number]	k4	743
prodejen	prodejna	k1gFnPc2	prodejna
<g/>
,	,	kIx,	,
49	[number]	k4	49
324	[number]	k4	324
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
68	[number]	k4	68
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
čtyři	čtyři	k4xCgFnPc1	čtyři
obchodní	obchodní	k2eAgFnPc1d1	obchodní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
Bata	Bat	k2eAgFnSc1d1	Bata
Europe	Europ	k1gInSc5	Europ
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
,	,	kIx,	,
Bata	Bat	k2eAgFnSc1d1	Bata
Asia	Asi	k2eAgFnSc1d1	Asia
Pacific-Africa	Pacific-Africa	k1gFnSc1	Pacific-Africa
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
Bata	Bata	k1gMnSc1	Bata
Latin	Latin	k1gMnSc1	Latin
America	America	k1gMnSc1	America
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
a	a	k8xC	a
Bata	Bata	k1gMnSc1	Bata
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Torontu	Toronto	k1gNnSc6	Toronto
Vlivem	vlivem	k7c2	vlivem
celních	celní	k2eAgFnPc2d1	celní
překážek	překážka	k1gFnPc2	překážka
koncern	koncern	k1gInSc1	koncern
Baťa	Baťa	k1gMnSc1	Baťa
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
budoval	budovat	k5eAaImAgMnS	budovat
své	svůj	k3xOyFgFnPc4	svůj
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
továrny	továrna	k1gFnPc4	továrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nechyběly	chybět	k5eNaImAgFnP	chybět
ani	ani	k9	ani
typické	typický	k2eAgInPc4d1	typický
Baťovy	Baťův	k2eAgInPc4d1	Baťův
domky	domek	k1gInPc4	domek
a	a	k8xC	a
budovy	budova	k1gFnPc4	budova
občanské	občanský	k2eAgFnSc2d1	občanská
vybavenosti	vybavenost	k1gFnSc2	vybavenost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
satelitů	satelit	k1gInPc2	satelit
byl	být	k5eAaImAgInS	být
budován	budovat	k5eAaImNgInS	budovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Möhlinu	Möhlin	k1gInSc6	Möhlin
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
otevření	otevření	k1gNnSc6	otevření
této	tento	k3xDgFnSc2	tento
továrny	továrna	k1gFnSc2	továrna
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
letěl	letět	k5eAaImAgMnS	letět
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
již	již	k6eAd1	již
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Jana	Jan	k1gMnSc2	Jan
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
baťovských	baťovský	k2eAgInPc2d1	baťovský
satelitů	satelit	k1gInPc2	satelit
Česko	Česko	k1gNnSc1	Česko
<g/>
:	:	kIx,	:
Dolní	dolní	k2eAgNnSc1d1	dolní
Němčí	němčit	k5eAaImIp3nS	němčit
<g/>
,	,	kIx,	,
Napajedla	Napajedla	k1gNnPc1	Napajedla
<g/>
,	,	kIx,	,
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
(	(	kIx(	(
<g/>
Baťov	Baťov	k1gInSc1	Baťov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sezimovo	Sezimův	k2eAgNnSc1d1	Sezimovo
Ústí	ústí	k1gNnSc1	ústí
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Dvůr	Dvůr	k1gInSc1	Dvůr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
(	(	kIx(	(
<g/>
Borovina	borovina	k1gFnSc1	borovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zruč	Zruč	k1gInSc1	Zruč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
:	:	kIx,	:
Partizánske	Partizánske	k1gFnSc4	Partizánske
(	(	kIx(	(
<g/>
Baťovany	Baťovan	k1gMnPc4	Baťovan
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svit	svit	k1gInSc1	svit
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Zámky	zámek	k1gInPc1	zámek
<g/>
,	,	kIx,	,
Bošany	Bošan	k1gInPc1	Bošan
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
:	:	kIx,	:
Seneffre	Seneffr	k1gInSc5	Seneffr
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
:	:	kIx,	:
Anaurilândia	Anaurilândium	k1gNnSc2	Anaurilândium
<g/>
,	,	kIx,	,
Batatuba	Batatuba	k1gMnSc1	Batatuba
<g/>
,	,	kIx,	,
Batayporã	Batayporã	k1gMnSc1	Batayporã
<g/>
,	,	kIx,	,
Bataguassu	Bataguass	k1gInSc2	Bataguass
<g/>
,	,	kIx,	,
Mariapolis	Mariapolis	k1gFnSc1	Mariapolis
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
SP	SP	kA	SP
Francie	Francie	k1gFnSc1	Francie
<g/>
:	:	kIx,	:
Hellcourt	Hellcourt	k1gInSc1	Hellcourt
(	(	kIx(	(
<g/>
Bataville	Bataville	k1gInSc1	Bataville
<g/>
)	)	kIx)	)
Chile	Chile	k1gNnSc2	Chile
<g/>
:	:	kIx,	:
Peñ	Peñ	k1gFnSc1	Peñ
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Vukovar	Vukovar	k1gInSc1	Vukovar
(	(	kIx(	(
<g/>
Borovo	Borovo	k1gNnSc1	Borovo
Naselje	Naselje	k1gFnSc1	Naselje
<g/>
)	)	kIx)	)
Indie	Indie	k1gFnSc1	Indie
<g/>
:	:	kIx,	:
Kalkata	Kalkata	k1gFnSc1	Kalkata
(	(	kIx(	(
<g/>
Batanagar	Batanagar	k1gInSc1	Batanagar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bataganj	Bataganj	k1gFnSc1	Bataganj
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
:	:	kIx,	:
Veca	Vec	k2eAgFnSc1d1	Vec
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
:	:	kIx,	:
Batawa	Bataw	k1gInSc2	Bataw
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
:	:	kIx,	:
Tiszaföldvár	Tiszaföldvár	k1gInSc1	Tiszaföldvár
(	(	kIx(	(
<g/>
Martfü	Martfü	k1gFnSc1	Martfü
<g/>
)	)	kIx)	)
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
:	:	kIx,	:
Best	Best	k1gMnSc1	Best
(	(	kIx(	(
<g/>
Batadorp	Batadorp	k1gMnSc1	Batadorp
<g/>
)	)	kIx)	)
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
:	:	kIx,	:
Batapur	Batapur	k1gMnSc1	Batapur
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
:	:	kIx,	:
Chełmek	Chełmek	k1gInSc1	Chełmek
<g/>
,	,	kIx,	,
Krapkowice	Krapkowice	k1gFnSc1	Krapkowice
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Ottmuth	Ottmuth	k1gInSc1	Ottmuth
)	)	kIx)	)
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
:	:	kIx,	:
Möhlin	Möhlin	k1gInSc1	Möhlin
USA	USA	kA	USA
<g/>
:	:	kIx,	:
Belcamp	Belcamp	k1gMnSc1	Belcamp
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
:	:	kIx,	:
Tilbury	Tilbura	k1gFnPc1	Tilbura
(	(	kIx(	(
<g/>
East	East	k1gInSc1	East
Tilbury	Tilbura	k1gFnSc2	Tilbura
<g/>
)	)	kIx)	)
</s>
