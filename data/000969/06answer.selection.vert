<s>
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
též	též	k9	též
Baťovy	Baťův	k2eAgInPc4d1	Baťův
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
velké	velký	k2eAgFnSc2d1	velká
české	český	k2eAgFnSc2d1	Česká
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založili	založit	k5eAaPmAgMnP	založit
sourozenci	sourozenec	k1gMnPc1	sourozenec
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťové	Baťová	k1gFnSc2	Baťová
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
v	v	k7c6	v
září	září	k1gNnSc6	září
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
