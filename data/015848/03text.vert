<s>
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
(	(	kIx(
<g/>
programátor	programátor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1978	#num#	k4
(	(	kIx(
<g/>
42	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Povolání	povolání	k1gNnSc3
</s>
<s>
programátor	programátor	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1978	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
programátor	programátor	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známým	známá	k1gFnPc3
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
především	především	k9
díky	díky	k7c3
nástroji	nástroj	k1gInSc3
Adminer	Adminra	k1gFnPc2
pro	pro	k7c4
správu	správa	k1gFnSc4
obsahu	obsah	k1gInSc2
databází	databáze	k1gFnPc2
pomocí	pomocí	k7c2
webového	webový	k2eAgInSc2d1
prohlížeče	prohlížeč	k1gInSc2
<g/>
,	,	kIx,
weblogu	weblog	k1gInSc2
o	o	k7c4
programování	programování	k1gNnSc4
v	v	k7c6
PHP	PHP	kA
či	či	k8xC
knize	kniha	k1gFnSc6
1001	#num#	k4
tipů	tip	k1gInPc2
a	a	k8xC
triků	trik	k1gInPc2
pro	pro	k7c4
PHP	PHP	kA
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
především	především	k9
vývojem	vývoj	k1gInSc7
webových	webový	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
zejména	zejména	k9
PHP	PHP	kA
<g/>
,	,	kIx,
Nette	Nett	k1gInSc5
Framework	Framework	k1gInSc1
<g/>
,	,	kIx,
Javascript	Javascript	k1gInSc1
<g/>
,	,	kIx,
jQuery	jQuer	k1gInPc1
<g/>
,	,	kIx,
HTML	HTML	kA
<g/>
,	,	kIx,
CSS	CSS	kA
<g/>
,	,	kIx,
SQL	SQL	kA
ale	ale	k8xC
i	i	k9
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
,	,	kIx,
sekci	sekce	k1gFnSc6
informatiky	informatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
získal	získat	k5eAaPmAgInS
bakalářský	bakalářský	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
obhájil	obhájit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
diplomovou	diplomový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c4
téma	téma	k1gNnSc4
<g/>
:	:	kIx,
Obnovení	obnovení	k1gNnSc1
diakritiky	diakritika	k1gFnSc2
v	v	k7c6
českém	český	k2eAgInSc6d1
textu	text	k1gInSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
úspěšně	úspěšně	k6eAd1
tak	tak	k6eAd1
dokončil	dokončit	k5eAaPmAgMnS
i	i	k9
magisterské	magisterský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
se	s	k7c7
specializací	specializace	k1gFnSc7
databázové	databázový	k2eAgInPc1d1
systémy	systém	k1gInPc1
a	a	k8xC
lingvistika	lingvistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1
projekty	projekt	k1gInPc1
</s>
<s>
Čtyřka	čtyřka	k1gFnSc1
</s>
<s>
Čtyřka	čtyřka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
prvních	první	k4xOgInPc2
úspěšných	úspěšný	k2eAgInPc2d1
projektů	projekt	k1gInPc2
od	od	k7c2
Jakuba	Jakub	k1gMnSc2
Vrány	Vrána	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1998	#num#	k4
až	až	k8xS
2000	#num#	k4
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hru	hra	k1gFnSc4
Mariáš	mariáš	k1gInSc4
pro	pro	k7c4
čtyři	čtyři	k4xCgMnPc4
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Matfyz	Matfyz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Matfyz	Matfyz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
je	být	k5eAaImIp3nS
server	server	k1gInSc1
na	na	k7c4
e-mailové	e-mailové	k2eAgInPc4d1
a	a	k8xC
webové	webový	k2eAgInPc4d1
aliasy	alias	k1gInPc4
vytvořený	vytvořený	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
studenty	student	k1gMnPc4
MFF	MFF	kA
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
špatného	špatný	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
e-mailových	e-mailův	k2eAgInPc2d1
a	a	k8xC
webových	webový	k2eAgInPc2d1
aliasů	alias	k1gInPc2
ze	z	k7c2
strany	strana	k1gFnSc2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
PHP	PHP	kA
triky	trik	k1gInPc1
</s>
<s>
Weblog	Weblog	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
o	o	k7c4
programování	programování	k1gNnSc4
v	v	k7c6
PHP	PHP	kA
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
mnohdy	mnohdy	k6eAd1
neočekávaných	očekávaný	k2eNgInPc6d1
problémech	problém	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
programovacím	programovací	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
mohou	moct	k5eAaImIp3nP
nastat	nastat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
především	především	k9
o	o	k7c4
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
hraje	hrát	k5eAaImIp3nS
na	na	k7c6
serverových	serverový	k2eAgInPc6d1
skriptech	skript	k1gInPc6
zcela	zcela	k6eAd1
zásadní	zásadní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
blog	blog	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
v	v	k7c6
letech	léto	k1gNnPc6
2008	#num#	k4
a	a	k8xC
2009	#num#	k4
vítězem	vítěz	k1gMnSc7
ankety	anketa	k1gFnSc2
Czech	Czech	k1gMnSc1
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Blog	Bloga	k1gFnPc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Adminer	Adminer	k1gMnSc1
</s>
<s>
Adminer	Adminer	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
phpMinAdmin	phpMinAdmin	k2eAgInSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nástroj	nástroj	k1gInSc4
napsaný	napsaný	k2eAgInSc4d1
v	v	k7c6
jazyce	jazyk	k1gInSc6
PHP	PHP	kA
umožňující	umožňující	k2eAgFnSc4d1
jednoduchou	jednoduchý	k2eAgFnSc4d1
správu	správa	k1gFnSc4
obsahu	obsah	k1gInSc2
databáze	databáze	k1gFnSc2
MySQL	MySQL	k1gFnPc2
<g/>
,	,	kIx,
PostgreSQL	PostgreSQL	k1gFnPc2
<g/>
,	,	kIx,
SQLite	SQLit	k1gMnSc5
<g/>
,	,	kIx,
MS	MS	kA
SQL	SQL	kA
a	a	k8xC
Oracle	Oracle	k1gFnSc1
prostřednictvím	prostřednictvím	k7c2
webového	webový	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
napsán	napsat	k5eAaBmNgInS,k5eAaPmNgInS
jako	jako	k8xS,k8xC
lehčí	lehký	k2eAgFnSc1d2
alternativa	alternativa	k1gFnSc1
phpMyAdminu	phpMyAdmin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
šířený	šířený	k2eAgInSc1d1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgInSc1d1
zdrojový	zdrojový	k2eAgInSc1d1
skript	skript	k1gInSc1
pod	pod	k7c7
licencí	licence	k1gFnSc7
Apache	Apach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Adminer	Adminer	k1gMnSc1
pomalu	pomalu	k6eAd1
začínají	začínat	k5eAaImIp3nP
nabízet	nabízet	k5eAaImF
první	první	k4xOgFnPc4
webhostingové	webhostingový	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
jako	jako	k8xC,k8xS
alternativu	alternativa	k1gFnSc4
k	k	k7c3
phpMyAdminu	phpMyAdmin	k1gInSc3
(	(	kIx(
<g/>
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
čítá	čítat	k5eAaImIp3nS
667	#num#	k4
souborů	soubor	k1gInPc2
a	a	k8xC
zabírá	zabírat	k5eAaImIp3nS
přes	přes	k7c4
11	#num#	k4
MB	MB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adminer	Adminer	k1gInSc1
je	být	k5eAaImIp3nS
vůči	vůči	k7c3
phpMyAdminu	phpMyAdmin	k1gInSc3
při	při	k7c6
běžných	běžný	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
v	v	k7c6
průměru	průměr	k1gInSc6
zhruba	zhruba	k6eAd1
2,5	2,5	k4
<g/>
×	×	k?
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
při	při	k7c6
vzdáleném	vzdálený	k2eAgNnSc6d1
spojení	spojení	k1gNnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozdíl	rozdíl	k1gInSc4
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
pro	pro	k7c4
velikost	velikost	k1gFnSc4
přenášených	přenášený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
NotORM	NotORM	k?
</s>
<s>
NotORM	NotORM	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
knihovna	knihovna	k1gFnSc1
PHP	PHP	kA
pro	pro	k7c4
snadnou	snadný	k2eAgFnSc4d1
a	a	k8xC
efektivní	efektivní	k2eAgFnSc4d1
práci	práce	k1gFnSc4
s	s	k7c7
daty	datum	k1gNnPc7
v	v	k7c6
databázi	databáze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
Knihovna	knihovna	k1gFnSc1
za	za	k7c2
nás	my	k3xPp1nPc2
řeší	řešit	k5eAaImIp3nS
efektivitu	efektivita	k1gFnSc4
vytvořených	vytvořený	k2eAgInPc2d1
příkazů	příkaz	k1gInPc2
na	na	k7c4
databázi	databáze	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
oceníme	ocenit	k5eAaPmIp1nP
například	například	k6eAd1
při	při	k7c6
spojování	spojování	k1gNnSc6
tabulek	tabulka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otevřené	otevřený	k2eAgInPc1d1
projekty	projekt	k1gInPc1
</s>
<s>
Kromě	kromě	k7c2
vlastních	vlastní	k2eAgInPc2d1
projektů	projekt	k1gInPc2
se	se	k3xPyFc4
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
podílí	podílet	k5eAaImIp3nS
i	i	k9
na	na	k7c6
otevřených	otevřený	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
autorů	autor	k1gMnPc2
anglické	anglický	k2eAgFnSc2d1
PHP	PHP	kA
dokumentace	dokumentace	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
té	ten	k3xDgFnSc3
české	český	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
načítání	načítání	k1gNnSc2
formátu	formát	k1gInSc2
v	v	k7c6
PHPExcel	PHPExcel	k1gFnSc6
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
–	–	k?
knihovny	knihovna	k1gFnSc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
formátem	formát	k1gInSc7
Open	Opena	k1gFnPc2
XML	XML	kA
aplikace	aplikace	k1gFnSc2
MS	MS	kA
Excel	Excel	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
účastnil	účastnit	k5eAaImAgInS
se	se	k3xPyFc4
i	i	k9
vývoje	vývoj	k1gInPc1
dalších	další	k2eAgInPc2d1
projektů	projekt	k1gInPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
HTMLtmpl	HTMLtmpl	k1gFnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
i	i	k9
některých	některý	k3yIgMnPc2
dalších	další	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Praxe	praxe	k1gFnSc1
a	a	k8xC
zaměstnání	zaměstnání	k1gNnSc1
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
začal	začít	k5eAaPmAgInS
vyučovat	vyučovat	k5eAaImF
na	na	k7c6
Karlově	Karlův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
působil	působit	k5eAaImAgMnS
na	na	k7c6
konferencích	konference	k1gFnPc6
web_expo	web_expa	k1gFnSc5
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
kromě	kromě	k7c2
přednášek	přednáška	k1gFnPc2
vedených	vedený	k2eAgFnPc2d1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
prezentoval	prezentovat	k5eAaBmAgMnS
např.	např.	kA
na	na	k7c4
osidays	osidays	k1gInSc4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
jazyce	jazyk	k1gInSc6
anglickém	anglický	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s>
Školení	školení	k1gNnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
do	do	k7c2
2011	#num#	k4
provozoval	provozovat	k5eAaImAgMnS
např.	např.	kA
školení	školení	k1gNnSc4
<g/>
:	:	kIx,
Bezpečnost	bezpečnost	k1gFnSc1
PHP	PHP	kA
aplikací	aplikace	k1gFnSc7
<g/>
,	,	kIx,
Návrh	návrh	k1gInSc1
a	a	k8xC
používání	používání	k1gNnSc1
MySQL	MySQL	k1gFnSc2
databáze	databáze	k1gFnSc2
<g/>
,	,	kIx,
JavaScript	JavaScript	k1gInSc4
a	a	k8xC
AJAX	AJAX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
školení	školení	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yIgFnPc6,k3yRgFnPc6,k3yQgFnPc6
vyučoval	vyučovat	k5eAaImAgMnS
<g/>
,	,	kIx,
si	se	k3xPyFc3
sám	sám	k3xTgMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
a	a	k8xC
soukromě	soukromě	k6eAd1
vyučoval	vyučovat	k5eAaImAgMnS
<g/>
,	,	kIx,
další	další	k2eAgFnSc1d1
mu	on	k3xPp3gMnSc3
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
přidělena	přidělen	k2eAgFnSc1d1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Akademií	akademie	k1gFnSc7
Root	Roota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Facebook	Facebook	k1gInSc1
</s>
<s>
Ve	v	k7c6
Facebooku	Facebook	k1gInSc6
působil	působit	k5eAaImAgMnS
od	od	k7c2
října	říjen	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
kam	kam	k6eAd1
ho	on	k3xPp3gNnSc4
vedení	vedení	k1gNnSc4
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
přivedlo	přivést	k5eAaPmAgNnS
po	po	k7c6
vyjednávání	vyjednávání	k1gNnSc6
od	od	k7c2
února	únor	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
programátorem	programátor	k1gMnSc7
z	z	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ve	v	k7c6
Facebooku	Facebook	k1gInSc6
pracoval	pracovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
Palo	Pala	k1gMnSc5
Alto	Alto	k1gNnSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
firmy	firma	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gigwalk	Gigwalk	k6eAd1
</s>
<s>
Po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
Facebooku	Facebook	k1gInSc2
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
pro	pro	k7c4
společnost	společnost	k1gFnSc4
Gigwalk	Gigwalka	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
programátor	programátor	k1gMnSc1
v	v	k7c6
PHP	PHP	kA
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
července	červenec	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Google	Google	k6eAd1
</s>
<s>
Do	do	k7c2
společnosti	společnost	k1gFnSc2
Google	Googl	k1gMnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
srpnu	srpen	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
https://www.vrana.cz/vrana.html	https://www.vrana.cz/vrana.html	k1gMnSc1
<g/>
↑	↑	k?
https://jakub.vrana.cz/texty/diplomka.pdf	https://jakub.vrana.cz/texty/diplomka.pdf	k1gMnSc1
<g/>
↑	↑	k?
https://ctyrka.vrana.cz/	https://ctyrka.vrana.cz/	k?
<g/>
↑	↑	k?
https://php.vrana.cz/	https://php.vrana.cz/	k?
<g/>
↑	↑	k?
Srovnání	srovnání	k1gNnSc1
rychlosti	rychlost	k1gFnSc2
Admineru	Adminer	k1gMnSc3
a	a	k8xC
phpMyAdminu	phpMyAdmin	k2eAgFnSc4d1
<g/>
↑	↑	k?
https://www.notorm.com/	https://www.notorm.com/	k?
<g/>
↑	↑	k?
https://php.vrana.cz/notorm.php	https://php.vrana.cz/notorm.php	k1gInSc1
<g/>
↑	↑	k?
http://www.php.net/manual/en/	http://www.php.net/manual/en/	k?
<g/>
↑	↑	k?
http://phpexcel.codeplex.com/	http://phpexcel.codeplex.com/	k?
<g/>
↑	↑	k?
http://htmltmpl.sourceforge.net/(kompilátor%5B%5D	http://htmltmpl.sourceforge.net/(kompilátor%5B%5D	k4
existujícího	existující	k2eAgInSc2d1
šablonovacího	šablonovací	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
do	do	k7c2
PHP	PHP	kA
kódu	kód	k1gInSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.webexpo.cz	https://www.webexpo.cz	k1gInSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
opensourceindia	opensourceindium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
in	in	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.root.cz/	https://www.root.cz/	k?
<g/>
↑	↑	k?
https://php.vrana.cz/jak-me-zamestnali-ve-facebooku.php?new=11753	https://php.vrana.cz/jak-me-zamestnali-ve-facebooku.php?new=11753	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Osoba	osoba	k1gFnSc1
Jakub	Jakub	k1gMnSc1
Vrána	Vrána	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
web	web	k1gInSc1
Jakuba	Jakub	k1gMnSc2
Vrány	Vrána	k1gMnSc2
</s>
<s>
Weblog	Weblog	k1gInSc1
o	o	k7c4
programování	programování	k1gNnSc4
v	v	k7c6
php	php	k?
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
o	o	k7c6
novém	nový	k2eAgNnSc6d1
zaměstnání	zaměstnání	k1gNnSc6
ve	v	k7c6
Facebooku	Facebook	k1gInSc6
</s>
