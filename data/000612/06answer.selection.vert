<s>
In	In	k?	In
nomine	nominout	k5eAaPmIp3nS	nominout
Domini	Domin	k2eAgMnPc1d1	Domin
je	být	k5eAaImIp3nS	být
papežský	papežský	k2eAgInSc4d1	papežský
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
papež	papež	k1gMnSc1	papež
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
způsob	způsob	k1gInSc1	způsob
papežské	papežský	k2eAgFnSc2d1	Papežská
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
