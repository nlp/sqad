<s>
In	In	k?	In
nomine	nominout	k5eAaPmIp3nS	nominout
Domini	Domin	k2eAgMnPc1d1	Domin
je	být	k5eAaImIp3nS	být
papežský	papežský	k2eAgInSc4d1	papežský
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
papež	papež	k1gMnSc1	papež
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
způsob	způsob	k1gInSc1	způsob
papežské	papežský	k2eAgFnSc2d1	Papežská
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
cézaropapismu	cézaropapismus	k1gInSc2	cézaropapismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
císař	císař	k1gMnSc1	císař
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
fakticky	fakticky	k6eAd1	fakticky
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
papežský	papežský	k2eAgInSc4d1	papežský
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
římská	římský	k2eAgFnSc1d1	římská
kurie	kurie	k1gFnSc1	kurie
snažila	snažit	k5eAaImAgFnS	snažit
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
vlivu	vliv	k1gInSc2	vliv
světských	světský	k2eAgMnPc2d1	světský
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
papež	papež	k1gMnSc1	papež
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
jediný	jediný	k2eAgInSc1d1	jediný
možný	možný	k2eAgInSc1d1	možný
způsob	způsob	k1gInSc1	způsob
papežské	papežský	k2eAgFnSc2d1	Papežská
volby	volba	k1gFnSc2	volba
-	-	kIx~	-
napříště	napříště	k6eAd1	napříště
bude	být	k5eAaImBp3nS	být
platná	platný	k2eAgFnSc1d1	platná
jen	jen	k9	jen
ta	ten	k3xDgFnSc1	ten
volba	volba	k1gFnSc1	volba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
světské	světský	k2eAgFnPc4d1	světská
moci	moc	k1gFnPc4	moc
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
přesně	přesně	k6eAd1	přesně
určeným	určený	k2eAgInSc7d1	určený
sborem	sbor	k1gInSc7	sbor
volitelů	volitel	k1gMnPc2	volitel
-	-	kIx~	-
sborem	sbor	k1gInSc7	sbor
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Bernhard	Bernhard	k1gMnSc1	Bernhard
Hülsebusch	Hülsebusch	k1gMnSc1	Hülsebusch
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Stellvertreter	Stellvertreter	k1gInSc4	Stellvertreter
Jesu	Jesa	k1gMnSc4	Jesa
-	-	kIx~	-
Geheimnis	Geheimnis	k1gInSc1	Geheimnis
und	und	k?	und
Wahrheit	Wahrheit	k1gInSc1	Wahrheit
der	drát	k5eAaImRp2nS	drát
Papstwahl	Papstwahl	k1gFnSc2	Papstwahl
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Benno	Benno	k1gNnSc1	Benno
Buch-	Buch-	k1gMnSc1	Buch-
und	und	k?	und
Zeitschriftenverlagsgesellschaft	Zeitschriftenverlagsgesellschaft	k1gMnSc1	Zeitschriftenverlagsgesellschaft
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7462-1501-3	[number]	k4	3-7462-1501-3
Detlef	Detlef	k1gMnSc1	Detlef
Jasper	Jasper	k1gMnSc1	Jasper
<g/>
,	,	kIx,	,
Das	Das	k1gMnSc1	Das
Papstwahldekret	Papstwahldekret	k1gMnSc1	Papstwahldekret
von	von	k1gInSc4	von
1059	[number]	k4	1059
<g/>
.	.	kIx.	.
</s>
<s>
Überlieferung	Überlieferung	k1gInSc1	Überlieferung
und	und	k?	und
Textgestalt	Textgestalt	k1gInSc1	Textgestalt
<g/>
,	,	kIx,	,
Sigmaringen	Sigmaringen	k1gInSc1	Sigmaringen
<g/>
:	:	kIx,	:
Thorbecke	Thorbecke	k1gInSc1	Thorbecke
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
Beiträge	Beiträg	k1gInSc2	Beiträg
zur	zur	k?	zur
Geschichte	Geschicht	k1gInSc5	Geschicht
und	und	k?	und
Quellenkunde	Quellenkund	k1gInSc5	Quellenkund
des	des	k1gNnPc4	des
Mittelalters	Mittelaltersa	k1gFnPc2	Mittelaltersa
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
3-7995-5712-1	[number]	k4	3-7995-5712-1
Hans-Georg	Hans-Georg	k1gInSc1	Hans-Georg
Krause	Kraus	k1gMnSc4	Kraus
<g/>
,	,	kIx,	,
Das	Das	k1gMnSc1	Das
Papstwahldekret	Papstwahldekret	k1gMnSc1	Papstwahldekret
von	von	k1gInSc4	von
1059	[number]	k4	1059
und	und	k?	und
seine	seinout	k5eAaPmIp3nS	seinout
Rolle	Rolle	k1gFnSc1	Rolle
im	im	k?	im
Investiturstreit	Investiturstreit	k1gInSc1	Investiturstreit
<g/>
.	.	kIx.	.
</s>
<s>
Rom	Rom	k1gMnSc1	Rom
1960	[number]	k4	1960
</s>
