<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
moravským	moravský	k2eAgMnSc7d1	moravský
markrabětem	markrabě	k1gMnSc7	markrabě
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
)	)	kIx)	)
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
procházel	procházet	k5eAaImAgMnS	procházet
mnoha	mnoho	k4c2	mnoho
výraznými	výrazný	k2eAgFnPc7d1	výrazná
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
