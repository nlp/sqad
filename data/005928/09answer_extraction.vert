nihilistický	nihilistický	k2eAgInSc1d1	nihilistický
nádech	nádech	k1gInSc1	nádech
s	s	k7c7	s
dotekem	dotek	k1gInSc7	dotek
japonské	japonský	k2eAgFnSc2d1	japonská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
všudypřítomné	všudypřítomný	k2eAgFnPc4d1	všudypřítomná
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
s	s	k7c7	s
cynickým	cynický	k2eAgInSc7d1	cynický
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
kyberimplantáty	kyberimplantát	k1gInPc7	kyberimplantát
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
ponořený	ponořený	k2eAgInSc4d1	ponořený
v	v	k7c6	v
kyberprostoru	kyberprostor	k1gInSc6	kyberprostor
