<p>
<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Écume	Écum	k1gInSc5	Écum
des	des	k1gNnPc6	des
jours	jours	k1gInSc4	jours
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Borise	Boris	k1gMnSc2	Boris
Viana	Vian	k1gMnSc2	Vian
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
-	-	kIx~	-
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
prvky	prvek	k1gInPc1	prvek
absurdity	absurdita	k1gFnSc2	absurdita
a	a	k8xC	a
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zastavila	zastavit	k5eAaPmAgFnS	zastavit
na	na	k7c4	na
půli	půle	k1gFnSc4	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
groteskním	groteskní	k2eAgNnSc7d1	groteskní
a	a	k8xC	a
bezstarostným	bezstarostný	k2eAgNnSc7d1	bezstarostné
vyprávěním	vyprávění	k1gNnSc7	vyprávění
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
příběhem	příběh	k1gInSc7	příběh
o	o	k7c6	o
obavách	obava	k1gFnPc6	obava
z	z	k7c2	z
kruté	krutý	k2eAgFnSc2d1	krutá
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
lidského	lidský	k2eAgNnSc2d1	lidské
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
nemožnost	nemožnost	k1gFnSc4	nemožnost
uniknout	uniknout	k5eAaPmF	uniknout
osudovým	osudový	k2eAgFnPc3d1	osudová
ranám	rána	k1gFnPc3	rána
a	a	k8xC	a
společenskému	společenský	k2eAgInSc3d1	společenský
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
bohatým	bohatý	k2eAgInSc7d1	bohatý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
plným	plný	k2eAgNnSc7d1	plné
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
symboliky	symbolika	k1gFnSc2	symbolika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
větší	veliký	k2eAgFnPc4d2	veliký
proslulosti	proslulost	k1gFnPc4	proslulost
až	až	k9	až
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
uvedena	uveden	k2eAgFnSc1d1	uvedena
i	i	k8xC	i
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Roubala	Roubal	k1gMnSc2	Roubal
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
i	i	k8xC	i
pražský	pražský	k2eAgInSc1d1	pražský
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
muzikál	muzikál	k1gInSc1	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jiřího	Jiří	k1gMnSc2	Jiří
Havelky	Havelka	k1gMnSc2	Havelka
<g/>
,	,	kIx,	,
v	v	k7c4	v
pražské	pražský	k2eAgNnSc4d1	Pražské
La	la	k1gNnSc4	la
Fabrice	fabrika	k1gFnSc3	fabrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
surreálné	surreálný	k2eAgFnSc6d1	surreálná
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
protagonisty	protagonista	k1gMnPc4	protagonista
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
partnerské	partnerský	k2eAgFnPc4d1	partnerská
dvojice	dvojice	k1gFnPc4	dvojice
-	-	kIx~	-
Chloé	Chloé	k1gNnSc4	Chloé
s	s	k7c7	s
Colinem	Colino	k1gNnSc7	Colino
a	a	k8xC	a
Alise	Alise	k1gFnSc1	Alise
s	s	k7c7	s
Chickem	Chick	k1gInSc7	Chick
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gMnSc1	Colin
je	být	k5eAaImIp3nS	být
nezměrně	změrně	k6eNd1	změrně
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
pohledný	pohledný	k2eAgMnSc1d1	pohledný
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
užívá	užívat	k5eAaImIp3nS	užívat
života	život	k1gInSc2	život
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Chloé	Chloá	k1gFnSc2	Chloá
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc3	jejich
štěstí	štěstí	k1gNnSc4	štěstí
nic	nic	k3yNnSc1	nic
nestojí	stát	k5eNaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Colinův	Colinův	k2eAgMnSc1d1	Colinův
přítel	přítel	k1gMnSc1	přítel
Chick	Chick	k1gMnSc1	Chick
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
chudší	chudý	k2eAgMnPc1d2	chudší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
musí	muset	k5eAaImIp3nS	muset
občas	občas	k6eAd1	občas
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
práci	práce	k1gFnSc4	práce
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
–	–	k?	–
je	být	k5eAaImIp3nS	být
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
podřízení	podřízený	k2eAgMnPc1d1	podřízený
dělníci	dělník	k1gMnPc1	dělník
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
skrytě	skrytě	k6eAd1	skrytě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vysmívají	vysmívat	k5eAaImIp3nP	vysmívat
<g/>
.	.	kIx.	.
</s>
<s>
Chick	Chick	k1gMnSc1	Chick
je	být	k5eAaImIp3nS	být
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
sběratelem	sběratel	k1gMnSc7	sběratel
díla	dílo	k1gNnSc2	dílo
filozofa	filozof	k1gMnSc2	filozof
Jeana-Sol	Jeana-Sol	k1gInSc4	Jeana-Sol
Partra	Partrum	k1gNnSc2	Partrum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
stojí	stát	k5eAaImIp3nS	stát
spoustu	spousta	k1gFnSc4	spousta
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gMnSc1	Colin
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
světlém	světlý	k2eAgInSc6d1	světlý
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
vaří	vařit	k5eAaImIp3nP	vařit
vybrané	vybraný	k2eAgFnPc4d1	vybraná
speciality	specialita	k1gFnPc4	specialita
vždy	vždy	k6eAd1	vždy
úslužný	úslužný	k2eAgMnSc1d1	úslužný
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
naladěný	naladěný	k2eAgMnSc1d1	naladěný
kuchař	kuchař	k1gMnSc1	kuchař
Nicolas	Nicolas	k1gMnSc1	Nicolas
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
Chickovy	Chickův	k2eAgFnSc2d1	Chickův
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Alise	Alise	k1gFnSc2	Alise
<g/>
.	.	kIx.	.
</s>
<s>
Idylický	idylický	k2eAgInSc1d1	idylický
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
však	však	k9	však
začne	začít	k5eAaPmIp3nS	začít
náhle	náhle	k6eAd1	náhle
hroutit	hroutit	k5eAaImF	hroutit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Colin	Colin	k1gMnSc1	Colin
s	s	k7c7	s
Chloé	Chloé	k1gNnSc7	Chloé
ožení	oženit	k5eAaPmIp3nP	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Chloé	Chloé	k6eAd1	Chloé
vážně	vážně	k6eAd1	vážně
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
–	–	k?	–
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
pravé	pravý	k2eAgFnSc6d1	pravá
plíci	plíce	k1gFnSc6	plíce
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
leknín	leknín	k1gInSc4	leknín
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
–	–	k?	–
Chloé	Chloé	k1gNnPc1	Chloé
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
spoustu	spoustu	k6eAd1	spoustu
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
pít	pít	k5eAaImF	pít
jen	jen	k6eAd1	jen
dvě	dva	k4xCgFnPc1	dva
lžičky	lžička	k1gFnPc1	lžička
vody	voda	k1gFnSc2	voda
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Colina	Colin	k2eAgFnSc1d1	Colina
nákladná	nákladný	k2eAgFnSc1d1	nákladná
léčba	léčba	k1gFnSc1	léčba
Chloé	Chloá	k1gFnSc2	Chloá
postupně	postupně	k6eAd1	postupně
ruinuje	ruinovat	k5eAaBmIp3nS	ruinovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
byt	byt	k1gInSc1	byt
se	se	k3xPyFc4	se
z	z	k7c2	z
nevysvětlitelné	vysvětlitelný	k2eNgFnSc2d1	nevysvětlitelná
příčiny	příčina	k1gFnSc2	příčina
stále	stále	k6eAd1	stále
scvrkává	scvrkávat	k5eAaImIp3nS	scvrkávat
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
čím	čí	k3xOyRgNnSc7	čí
dál	daleko	k6eAd2	daleko
menším	malý	k2eAgNnSc7d2	menší
<g/>
,	,	kIx,	,
temnějším	temný	k2eAgNnSc7d2	temnější
a	a	k8xC	a
špinavějším	špinavý	k2eAgNnSc7d2	špinavější
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
dokonce	dokonce	k9	dokonce
musí	muset	k5eAaImIp3nP	muset
propustit	propustit	k5eAaPmF	propustit
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Chloé	Chloé	k6eAd1	Chloé
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nakrátko	nakrátko	k6eAd1	nakrátko
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnPc4	její
plíce	plíce	k1gFnPc4	plíce
je	být	k5eAaImIp3nS	být
nefunkční	funkční	k2eNgMnSc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
leknín	leknín	k1gInSc1	leknín
i	i	k9	i
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
plíci	plíce	k1gFnSc6	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
vztah	vztah	k1gInSc1	vztah
Alise	Alise	k1gFnSc2	Alise
s	s	k7c7	s
Chickem	Chick	k1gInSc7	Chick
má	mít	k5eAaImIp3nS	mít
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
Chickovým	Chickův	k2eAgInSc7d1	Chickův
nesmírným	smírný	k2eNgInSc7d1	nesmírný
obdivem	obdiv	k1gInSc7	obdiv
k	k	k7c3	k
Partrovi	Partr	k1gMnSc3	Partr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
k	k	k7c3	k
fanatismu	fanatismus	k1gInSc3	fanatismus
<g/>
.	.	kIx.	.
</s>
<s>
Chick	Chick	k6eAd1	Chick
sbírá	sbírat	k5eAaImIp3nS	sbírat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nějak	nějak	k6eAd1	nějak
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
Partrem	Partr	k1gInSc7	Partr
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
využívají	využívat	k5eAaImIp3nP	využívat
knihkupci	knihkupec	k1gMnPc1	knihkupec
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
Chick	Chicka	k1gFnPc2	Chicka
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
Partrova	Partrův	k2eAgNnPc4d1	Partrův
díla	dílo	k1gNnPc4	dílo
získává	získávat	k5eAaImIp3nS	získávat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
peněz	peníze	k1gInPc2	peníze
ale	ale	k8xC	ale
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Colina	Colin	k1gMnSc2	Colin
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
s	s	k7c7	s
Alise	Alis	k1gInSc6	Alis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zklamaná	zklamaný	k2eAgFnSc1d1	zklamaná
Alise	Alise	k1gFnSc1	Alise
<g/>
,	,	kIx,	,
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chick	Chick	k1gInSc1	Chick
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c4	na
zaplacení	zaplacení	k1gNnSc4	zaplacení
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
kavárny	kavárna	k1gFnSc2	kavárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejprve	nejprve	k6eAd1	nejprve
Partra	Partra	k1gFnSc1	Partra
přemlouvá	přemlouvat	k5eAaImIp3nS	přemlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k9	už
nic	nic	k3yNnSc1	nic
nepsal	psát	k5eNaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
přemluvit	přemluvit	k5eAaPmF	přemluvit
nedá	dát	k5eNaPmIp3nS	dát
<g/>
,	,	kIx,	,
Alise	Alise	k1gFnSc1	Alise
jej	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
Chickovým	Chickův	k2eAgInSc7d1	Chickův
srdcerváčem	srdcerváč	k1gInSc7	srdcerváč
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
zabije	zabít	k5eAaPmIp3nS	zabít
všechny	všechen	k3xTgMnPc4	všechen
knihkupce	knihkupec	k1gMnPc4	knihkupec
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
zapálí	zapálit	k5eAaPmIp3nP	zapálit
všechna	všechen	k3xTgNnPc1	všechen
knihkupectví	knihkupectví	k1gNnPc1	knihkupectví
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
uhoří	uhořet	k5eAaPmIp3nS	uhořet
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
požárů	požár	k1gInPc2	požár
nedaleko	nedaleko	k7c2	nedaleko
Chickova	Chickův	k2eAgInSc2d1	Chickův
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sama	sám	k3xTgFnSc1	sám
založila	založit	k5eAaPmAgFnS	založit
krabičkou	krabička	k1gFnSc7	krabička
sirek	sirka	k1gFnPc2	sirka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vzala	vzít	k5eAaPmAgFnS	vzít
Partrovi	Partr	k1gMnSc3	Partr
z	z	k7c2	z
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Chicka	Chicka	k1gFnSc1	Chicka
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
vymahači	vymahač	k1gMnPc1	vymahač
nezaplacených	zaplacený	k2eNgFnPc2d1	nezaplacená
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
srdcerváč	srdcerváč	k1gInSc4	srdcerváč
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
sbírka	sbírka	k1gFnSc1	sbírka
Partrova	Partrův	k2eAgNnSc2d1	Partrův
díla	dílo	k1gNnSc2	dílo
shoří	shořet	k5eAaPmIp3nS	shořet
od	od	k7c2	od
zapáleného	zapálený	k2eAgNnSc2d1	zapálené
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chloé	Chloé	k6eAd1	Chloé
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gInSc1	Colin
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
peníze	peníz	k1gInPc4	peníz
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
chudinský	chudinský	k2eAgInSc1d1	chudinský
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c6	na
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
lávce	lávka	k1gFnSc6	lávka
u	u	k7c2	u
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
když	když	k8xS	když
leknín	leknín	k1gInSc1	leknín
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
Colin	Colin	k1gInSc1	Colin
ho	on	k3xPp3gMnSc4	on
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
na	na	k7c4	na
fotku	fotka	k1gFnSc4	fotka
Chloé	Chloá	k1gFnSc2	Chloá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
brzy	brzy	k6eAd1	brzy
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přestává	přestávat	k5eAaImIp3nS	přestávat
jíst	jíst	k5eAaImF	jíst
a	a	k8xC	a
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
se	se	k3xPyFc4	se
ničemu	nic	k3yNnSc3	nic
jinému	jiný	k2eAgNnSc3d1	jiné
než	než	k8xS	než
těmto	tento	k3xDgFnPc3	tento
činnostem	činnost	k1gFnPc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
myška	myška	k1gFnSc1	myška
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
domácnosti	domácnost	k1gFnSc2	domácnost
mu	on	k3xPp3gMnSc3	on
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nemůže	moct	k5eNaImIp3nS	moct
dále	daleko	k6eAd2	daleko
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
kočku	kočka	k1gFnSc4	kočka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
mohla	moct	k5eAaImAgFnS	moct
dát	dát	k5eAaPmF	dát
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
tlamy	tlama	k1gFnSc2	tlama
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
-	-	kIx~	-
až	až	k9	až
jí	on	k3xPp3gFnSc7	on
někdo	někdo	k3yInSc1	někdo
šlápne	šlápnout	k5eAaPmIp3nS	šlápnout
na	na	k7c4	na
ocas	ocas	k1gInSc4	ocas
-	-	kIx~	-
jí	jíst	k5eAaImIp3nS	jíst
hlavu	hlava	k1gFnSc4	hlava
ukousne	ukousnout	k5eAaPmIp3nS	ukousnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Motivy	motiv	k1gInPc1	motiv
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Autobiografické	autobiografický	k2eAgInPc1d1	autobiografický
prvky	prvek	k1gInPc1	prvek
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
některé	některý	k3yIgInPc4	některý
motivy	motiv	k1gInPc4	motiv
ze	z	k7c2	z
skutečného	skutečný	k2eAgInSc2d1	skutečný
Vianova	Vianův	k2eAgInSc2d1	Vianův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vian	Vian	k1gInSc1	Vian
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Colin	Colin	k1gMnSc1	Colin
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zbankrotovala	zbankrotovat	k5eAaPmAgFnS	zbankrotovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
snažili	snažit	k5eAaImAgMnP	snažit
žít	žít	k5eAaImF	žít
stylem	styl	k1gInSc7	styl
bohatých	bohatý	k2eAgMnPc2d1	bohatý
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Nepřehlédnutelná	přehlédnutelný	k2eNgFnSc1d1	nepřehlédnutelná
je	být	k5eAaImIp3nS	být
také	také	k9	také
Vianova	Vianův	k2eAgFnSc1d1	Vianova
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jazzu	jazz	k1gInSc3	jazz
(	(	kIx(	(
<g/>
pianoctail	pianoctail	k1gInSc1	pianoctail
<g/>
,	,	kIx,	,
míchající	míchající	k2eAgInPc1d1	míchající
koktejly	koktejl	k1gInPc1	koktejl
podle	podle	k7c2	podle
zahrané	zahraný	k2eAgFnSc2d1	zahraná
melodie	melodie	k1gFnSc2	melodie
<g/>
;	;	kIx,	;
melodické	melodický	k2eAgInPc1d1	melodický
zvuky	zvuk	k1gInPc1	zvuk
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Vianův	Vianův	k2eAgMnSc1d1	Vianův
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
jazzman	jazzman	k1gMnSc1	jazzman
Duke	Duk	k1gFnSc2	Duk
Ellington	Ellington	k1gInSc1	Ellington
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
románu	román	k1gInSc6	román
několikrát	několikrát	k6eAd1	několikrát
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jean-Sol	Jean-Sol	k1gInSc1	Jean-Sol
Partre	Partr	k1gMnSc5	Partr
===	===	k?	===
</s>
</p>
<p>
<s>
Jean-Sol	Jean-Solit	k5eAaPmRp2nS	Jean-Solit
Partre	Partr	k1gMnSc5	Partr
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
filozof	filozof	k1gMnSc1	filozof
Vianova	Vianův	k2eAgInSc2d1	Vianův
imaginárního	imaginární	k2eAgInSc2d1	imaginární
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Colinův	Colinův	k2eAgMnSc1d1	Colinův
přítel	přítel	k1gMnSc1	přítel
Chick	Chick	k1gMnSc1	Chick
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
sběratelem	sběratel	k1gMnSc7	sběratel
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nějak	nějak	k6eAd1	nějak
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
koníček	koníček	k1gInSc1	koníček
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
tak	tak	k6eAd1	tak
nákladný	nákladný	k2eAgMnSc1d1	nákladný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
přivede	přivést	k5eAaPmIp3nS	přivést
zcela	zcela	k6eAd1	zcela
na	na	k7c4	na
mizinu	mizina	k1gFnSc4	mizina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Pěně	pěna	k1gFnSc6	pěna
dní	den	k1gInPc2	den
neobjevují	objevovat	k5eNaImIp3nP	objevovat
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgFnPc4	žádný
Partrovy	Partrův	k2eAgFnPc4d1	Partrův
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Chickovo	Chickův	k2eAgNnSc1d1	Chickův
jednání	jednání	k1gNnSc1	jednání
karikaturou	karikatura	k1gFnSc7	karikatura
obsesivního	obsesivní	k2eAgMnSc2d1	obsesivní
<g/>
,	,	kIx,	,
nesmyslného	smyslný	k2eNgInSc2d1	nesmyslný
fanouškovství	fanouškovství	k?	fanouškovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jean	Jean	k1gMnSc1	Jean
Sol-Partre	Sol-Partr	k1gInSc5	Sol-Partr
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgFnSc1d1	slovní
hříčka	hříčka	k1gFnSc1	hříčka
na	na	k7c4	na
filozofa	filozof	k1gMnSc4	filozof
Jeana-Paula	Jeana-Paul	k1gMnSc4	Jeana-Paul
Sartra	Sartr	k1gMnSc4	Sartr
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
názvy	název	k1gInPc1	název
Partrových	Partrův	k2eAgFnPc2d1	Partrův
knih	kniha	k1gFnPc2	kniha
v	v	k7c6	v
Pěně	pěna	k1gFnSc6	pěna
dní	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
skutečné	skutečný	k2eAgFnPc4d1	skutečná
Sartrovy	Sartrův	k2eAgFnPc4d1	Sartrova
knihy	kniha	k1gFnPc4	kniha
–	–	k?	–
např.	např.	kA	např.
Zvracení	zvracení	k1gNnPc2	zvracení
místo	místo	k7c2	místo
Nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
Písmo	písmo	k1gNnSc1	písmo
a	a	k8xC	a
neón	neón	k1gInSc1	neón
(	(	kIx(	(
<g/>
La	la	k0	la
Lettre	Lettr	k1gMnSc5	Lettr
et	et	k?	et
le	le	k?	le
Néon	Néon	k1gInSc1	Néon
<g/>
)	)	kIx)	)
místo	místo	k1gNnSc1	místo
Bytí	bytí	k1gNnSc1	bytí
a	a	k8xC	a
nicota	nicota	k1gFnSc1	nicota
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Ê	Ê	k?	Ê
et	et	k?	et
le	le	k?	le
Néant	Néant	k1gInSc1	Néant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Colinova	Colinův	k2eAgNnSc2d1	Colinovo
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Colin	Colin	k1gMnSc1	Colin
zchudne	zchudnout	k5eAaPmIp3nS	zchudnout
<g/>
,	,	kIx,	,
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
několik	několik	k4yIc4	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydělal	vydělat	k5eAaPmAgInS	vydělat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
květiny	květina	k1gFnPc4	květina
pro	pro	k7c4	pro
Chloé	Chloé	k1gNnSc4	Chloé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
pušek	puška	k1gFnPc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
povinností	povinnost	k1gFnSc7	povinnost
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgNnSc7	svůj
nahým	nahý	k2eAgNnSc7d1	nahé
tělem	tělo	k1gNnSc7	tělo
zahřívat	zahřívat	k5eAaImF	zahřívat
hlavně	hlavně	k9	hlavně
pušek	puška	k1gFnPc2	puška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rostly	růst	k5eAaImAgFnP	růst
rovně	rovně	k6eAd1	rovně
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
neustále	neustále	k6eAd1	neustále
kroutí	kroutit	k5eAaImIp3nS	kroutit
<g/>
,	,	kIx,	,
a	a	k8xC	a
Colin	Colin	k1gMnSc1	Colin
je	být	k5eAaImIp3nS	být
brzy	brzy	k6eAd1	brzy
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potom	potom	k6eAd1	potom
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Colin	Colin	k1gMnSc1	Colin
musí	muset	k5eAaImIp3nS	muset
chodit	chodit	k5eAaImF	chodit
kolem	kolem	k7c2	kolem
sloupoví	sloupoví	k1gNnSc2	sloupoví
a	a	k8xC	a
křičet	křičet	k5eAaImF	křičet
<g/>
,	,	kIx,	,
kdykoli	kdykoli	k6eAd1	kdykoli
uvidí	uvidět	k5eAaPmIp3nS	uvidět
zloděje	zloděj	k1gMnPc4	zloděj
<g/>
.	.	kIx.	.
</s>
<s>
Správný	správný	k2eAgMnSc1d1	správný
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
stihne	stihnout	k5eAaPmIp3nS	stihnout
svižným	svižný	k2eAgInSc7d1	svižný
krokem	krok	k1gInSc7	krok
obejít	obejít	k5eAaPmF	obejít
kolem	kolem	k7c2	kolem
celého	celý	k2eAgNnSc2d1	celé
sloupoví	sloupoví	k1gNnSc2	sloupoví
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
směnu	směna	k1gFnSc4	směna
a	a	k8xC	a
protože	protože	k8xS	protože
zloději	zloděj	k1gMnPc1	zloděj
přicházejí	přicházet	k5eAaImIp3nP	přicházet
vždy	vždy	k6eAd1	vždy
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
křik	křik	k1gInSc4	křik
je	on	k3xPp3gMnPc4	on
zažene	zahnat	k5eAaPmIp3nS	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Colin	Colin	k1gMnSc1	Colin
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
unaven	unavit	k5eAaPmNgMnS	unavit
<g/>
,	,	kIx,	,
zpožďuje	zpožďovat	k5eAaImIp3nS	zpožďovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
zloději	zloděj	k1gMnPc1	zloděj
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
proniknout	proniknout	k5eAaPmF	proniknout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Colin	Colin	k1gMnSc1	Colin
rovněž	rovněž	k9	rovněž
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
získá	získat	k5eAaPmIp3nS	získat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
stačí	stačit	k5eAaBmIp3nP	stačit
–	–	k?	–
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
nosič	nosič	k1gInSc1	nosič
špatných	špatný	k2eAgFnPc2d1	špatná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Obchází	obcházet	k5eAaImIp3nS	obcházet
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
někdo	někdo	k3yInSc1	někdo
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
onemocnět	onemocnět	k5eAaPmF	onemocnět
<g/>
,	,	kIx,	,
ztratit	ztratit	k5eAaPmF	ztratit
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
apod.	apod.	kA	apod.
Lidé	člověk	k1gMnPc1	člověk
ho	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
nenávidí	návidět	k5eNaImIp3nP	návidět
a	a	k8xC	a
házejí	házet	k5eAaImIp3nP	házet
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
různé	různý	k2eAgFnPc4d1	různá
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
najde	najít	k5eAaPmIp3nS	najít
v	v	k7c6	v
pořadníku	pořadník	k1gInSc6	pořadník
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chloé	Chloé	k1gNnSc1	Chloé
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
SOUKAL	soukat	k5eAaImAgMnS	soukat
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
gymnázií	gymnázium	k1gNnPc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VIAN	VIAN	kA	VIAN
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
<g/>
.	.	kIx.	.
</s>
<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	mond	k1gInSc5	mond
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Pěna	pěn	k2eAgFnSc1d1	pěna
dní	den	k1gInPc2	den
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
