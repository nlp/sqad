<s>
Pěna	pěna	k1gFnSc1	pěna
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Écume	Écum	k1gInSc5	Écum
des	des	k1gNnPc6	des
jours	jours	k1gInSc4	jours
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Borise	Boris	k1gMnSc2	Boris
Viana	Vian	k1gMnSc2	Vian
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
-	-	kIx~	-
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
