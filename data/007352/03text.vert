<s>
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
synchronos	synchronos	k1gInSc1	synchronos
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psycholog	psycholog	k1gMnSc1	psycholog
C.	C.	kA	C.
G.	G.	kA	G.
Jung	Jung	k1gMnSc1	Jung
jako	jako	k8xC	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
příčinně	příčinně	k6eAd1	příčinně
nevysvětlitelné	vysvětlitelný	k2eNgNnSc4d1	nevysvětlitelné
(	(	kIx(	(
<g/>
akauzální	akauzální	k2eAgNnSc4d1	akauzální
<g/>
)	)	kIx)	)
setkání	setkání	k1gNnSc4	setkání
dvou	dva	k4xCgInPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tím	ten	k3xDgNnSc7	ten
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
prožívání	prožívání	k1gNnSc2	prožívání
subjektu	subjekt	k1gInSc2	subjekt
získávají	získávat	k5eAaImIp3nP	získávat
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
synchronicity	synchronicita	k1gFnSc2	synchronicita
je	být	k5eAaImIp3nS	být
však	však	k9	však
spojován	spojovat	k5eAaImNgInS	spojovat
také	také	k6eAd1	také
s	s	k7c7	s
objektivním	objektivní	k2eAgNnSc7d1	objektivní
nahromaděním	nahromadění	k1gNnSc7	nahromadění
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
žádají	žádat	k5eAaImIp3nP	žádat
společnou	společný	k2eAgFnSc4d1	společná
interpretaci	interpretace	k1gFnSc4	interpretace
<g/>
,	,	kIx,	,
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nebo	nebo	k8xC	nebo
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
není	být	k5eNaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
kauzální	kauzální	k2eAgFnSc1d1	kauzální
souvislost	souvislost	k1gFnSc1	souvislost
<g/>
,	,	kIx,	,
a	a	k8xC	a
vymykají	vymykat	k5eAaImIp3nP	vymykat
se	se	k3xPyFc4	se
novověké	novověký	k2eAgFnSc3d1	novověká
představě	představa	k1gFnSc3	představa
o	o	k7c6	o
náhodě	náhoda	k1gFnSc6	náhoda
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
souběžný	souběžný	k2eAgInSc4d1	souběžný
vzestup	vzestup	k1gInSc4	vzestup
nacismu	nacismus	k1gInSc2	nacismus
i	i	k8xC	i
komunismu	komunismus	k1gInSc2	komunismus
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
anebo	anebo	k8xC	anebo
kulturní	kulturní	k2eAgFnSc2d1	kulturní
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
zasáhla	zasáhnout	k5eAaPmAgNnP	zasáhnout
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
i	i	k8xC	i
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
zavedli	zavést	k5eAaPmAgMnP	zavést
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psycholog	psycholog	k1gMnSc1	psycholog
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
a	a	k8xC	a
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paule	k1gFnSc4	Paule
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
Paulli	Paulle	k1gFnSc4	Paulle
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
hodnotu	hodnota	k1gFnSc4	hodnota
137	[number]	k4	137
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
konstantě	konstanta	k1gFnSc6	konstanta
jemné	jemný	k2eAgFnSc2d1	jemná
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
například	například	k6eAd1	například
pojem	pojem	k1gInSc1	pojem
zákon	zákon	k1gInSc1	zákon
sérií	série	k1gFnPc2	série
(	(	kIx(	(
<g/>
Seriengesetz	Seriengesetz	k1gInSc1	Seriengesetz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zavedl	zavést	k5eAaPmAgMnS	zavést
Paul	Paul	k1gMnSc1	Paul
Kammerer	Kammerer	k1gMnSc1	Kammerer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
podobný	podobný	k2eAgInSc4d1	podobný
pojem	pojem	k1gInSc4	pojem
Arthur	Arthur	k1gMnSc1	Arthur
Koestler	Koestler	k1gMnSc1	Koestler
nazval	nazvat	k5eAaBmAgMnS	nazvat
koincidence	koincidence	k1gFnPc4	koincidence
(	(	kIx(	(
<g/>
coincidence	coincidence	k1gFnPc4	coincidence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Komárek	Komárek	k1gMnSc1	Komárek
vedle	vedle	k7c2	vedle
pojmu	pojem	k1gInSc2	pojem
synchronicita	synchronicita	k1gFnSc1	synchronicita
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
pojem	pojem	k1gInSc4	pojem
syntopicita	syntopicita	k1gFnSc1	syntopicita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ke	k	k7c3	k
koincidenci	koincidence	k1gFnSc3	koincidence
dochází	docházet	k5eAaImIp3nS	docházet
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
synmorficita	synmorficita	k1gFnSc1	synmorficita
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
mimetické	mimetický	k2eAgInPc4d1	mimetický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podobnosti	podobnost	k1gFnPc1	podobnost
navzájem	navzájem	k6eAd1	navzájem
nepříbuzných	příbuzný	k2eNgInPc2d1	nepříbuzný
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
dokáží	dokázat	k5eAaPmIp3nP	dokázat
kauzalistické	kauzalistický	k2eAgFnPc1d1	kauzalistický
metody	metoda	k1gFnPc1	metoda
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jen	jen	k6eAd1	jen
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgNnPc1	tento
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
spíše	spíše	k9	spíše
etiologickými	etiologický	k2eAgInPc7d1	etiologický
mýty	mýtus	k1gInPc7	mýtus
<g/>
.	.	kIx.	.
atd.	atd.	kA	atd.
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paul	k1gMnSc3	Paul
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
synchronicitou	synchronicita	k1gFnSc7	synchronicita
v	v	k7c4	v
souvislosti	souvislost	k1gFnPc4	souvislost
s	s	k7c7	s
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
fyzikou	fyzika	k1gFnSc7	fyzika
a	a	k8xC	a
principem	princip	k1gInSc7	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
klasické	klasický	k2eAgNnSc4d1	klasické
pojetí	pojetí	k1gNnSc4	pojetí
kauzality	kauzalita	k1gFnSc2	kauzalita
selhává	selhávat	k5eAaImIp3nS	selhávat
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
evropské	evropský	k2eAgNnSc1d1	Evropské
pojetí	pojetí	k1gNnSc1	pojetí
kauzality	kauzalita	k1gFnSc2	kauzalita
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Aristotelovo	Aristotelův	k2eAgNnSc4d1	Aristotelovo
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
středověké	středověký	k2eAgNnSc4d1	středověké
arabské	arabský	k2eAgNnSc4d1	arabské
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
synchronicitu	synchronicita	k1gFnSc4	synchronicita
převažoval	převažovat	k5eAaImAgMnS	převažovat
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
projevil	projevit	k5eAaPmAgMnS	projevit
se	se	k3xPyFc4	se
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
renesančním	renesanční	k2eAgNnSc6d1	renesanční
myšlení	myšlení	k1gNnSc6	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stanislava	Stanislav	k1gMnSc2	Stanislav
Komárka	Komárek	k1gMnSc2	Komárek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cituje	citovat	k5eAaBmIp3nS	citovat
Batesona	Bateson	k1gMnSc4	Bateson
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dva	dva	k4xCgInPc4	dva
popisy	popis	k1gInPc4	popis
jsou	být	k5eAaImIp3nP	být
lepší	dobrý	k2eAgMnPc1d2	lepší
než	než	k8xS	než
jeden	jeden	k4xCgMnSc1	jeden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
myšlení	myšlení	k1gNnSc2	myšlení
"	"	kIx"	"
<g/>
jaksi	jaksi	k6eAd1	jaksi
komplementární	komplementární	k2eAgInPc4d1	komplementární
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmé	kolmý	k2eAgFnPc1d1	kolmá
či	či	k8xC	či
mimoběžné	mimoběžný	k2eAgFnPc1d1	mimoběžná
<g/>
,	,	kIx,	,
a	a	k8xC	a
nelze	lze	k6eNd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc1d1	správný
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
mylný	mylný	k2eAgInSc1d1	mylný
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přímo	přímo	k6eAd1	přímo
volají	volat	k5eAaImIp3nP	volat
po	po	k7c6	po
kauzalistním	kauzalistní	k2eAgInSc6d1	kauzalistní
popisu	popis	k1gInSc6	popis
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
zas	zas	k6eAd1	zas
po	po	k7c6	po
synchronicistním	synchronicistní	k2eAgMnSc6d1	synchronicistní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
novověká	novověký	k2eAgFnSc1d1	novověká
přírodověda	přírodověda	k1gFnSc1	přírodověda
neuznává	uznávat	k5eNaImIp3nS	uznávat
synchronicistní	synchronicistní	k2eAgInSc4d1	synchronicistní
popis	popis	k1gInSc4	popis
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
<g/>
"	"	kIx"	"
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kauzalita	kauzalita	k1gFnSc1	kauzalita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
synchronicity	synchronicita	k1gFnSc2	synchronicita
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
náhody	náhoda	k1gFnSc2	náhoda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
uměle	uměle	k6eAd1	uměle
rozdělující	rozdělující	k2eAgInPc1d1	rozdělující
jevy	jev	k1gInPc1	jev
na	na	k7c6	na
"	"	kIx"	"
<g/>
nutné	nutný	k2eAgFnSc3d1	nutná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
náhodné	náhodný	k2eAgNnSc1d1	náhodné
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
statistika	statistika	k1gFnSc1	statistika
jako	jako	k8xC	jako
metody	metoda	k1gFnPc1	metoda
kauzalistní	kauzalistní	k2eAgFnPc1d1	kauzalistní
vědy	věda	k1gFnPc1	věda
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
odstínění	odstínění	k1gNnSc4	odstínění
či	či	k8xC	či
popření	popření	k1gNnSc4	popření
(	(	kIx(	(
<g/>
jiných	jiný	k2eAgFnPc2d1	jiná
<g/>
)	)	kIx)	)
synchronicit	synchronicita	k1gFnPc2	synchronicita
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
starořecké	starořecký	k2eAgNnSc1d1	starořecké
slovo	slovo	k1gNnSc1	slovo
symbolon	symbolon	k1gInSc1	symbolon
(	(	kIx(	(
<g/>
znamení	znamení	k1gNnSc1	znamení
<g/>
)	)	kIx)	)
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
zástupný	zástupný	k2eAgInSc4d1	zástupný
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
koincidenci	koincidence	k1gFnSc4	koincidence
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
synchronicity	synchronicita	k1gFnSc2	synchronicita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výkladu	výklad	k1gInSc6	výklad
o	o	k7c6	o
synchronicitě	synchronicita	k1gFnSc6	synchronicita
uvádí	uvádět	k5eAaImIp3nS	uvádět
Jung	Jung	k1gMnSc1	Jung
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zkušenost	zkušenost	k1gFnSc4	zkušenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
mnohokrát	mnohokrát	k6eAd1	mnohokrát
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
podobou	podoba	k1gFnSc7	podoba
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
sen	sen	k1gInSc1	sen
pacientky	pacientka	k1gFnSc2	pacientka
<g/>
,	,	kIx,	,
oběd	oběd	k1gInSc4	oběd
či	či	k8xC	či
skutečná	skutečný	k2eAgFnSc1d1	skutečná
ryba	ryba	k1gFnSc1	ryba
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
jezerním	jezerní	k2eAgInSc6d1	jezerní
molu	mol	k1gInSc6	mol
<g/>
.	.	kIx.	.
</s>
<s>
Souběh	souběh	k1gInSc1	souběh
událostí	událost	k1gFnPc2	událost
nezůstal	zůstat	k5eNaPmAgInS	zůstat
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
náhodná	náhodný	k2eAgFnSc1d1	náhodná
koincidence	koincidence	k1gFnSc1	koincidence
získala	získat	k5eAaPmAgFnS	získat
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
synchronistní	synchronistní	k2eAgInPc4d1	synchronistní
fenomény	fenomén	k1gInPc4	fenomén
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
i	i	k9	i
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pak	pak	k6eAd1	pak
najdou	najít	k5eAaPmIp3nP	najít
korespondenci	korespondence	k1gFnSc4	korespondence
s	s	k7c7	s
nastalou	nastalý	k2eAgFnSc7d1	nastalá
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
déjà	déjà	k?	déjà
vu	vu	k?	vu
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
již	již	k6eAd1	již
zažitého	zažitý	k2eAgNnSc2d1	zažité
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tyto	tento	k3xDgInPc4	tento
jevy	jev	k1gInPc4	jev
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
psychické	psychický	k2eAgFnSc2d1	psychická
představy	představa	k1gFnSc2	představa
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
nevysvětlitelné	vysvětlitelný	k2eNgInPc1d1	nevysvětlitelný
pouhou	pouhý	k2eAgFnSc7d1	pouhá
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
se	se	k3xPyFc4	se
ve	v	k7c6	v
zkoumání	zkoumání	k1gNnSc6	zkoumání
synchronicity	synchronicita	k1gFnSc2	synchronicita
opřel	opřít	k5eAaPmAgMnS	opřít
o	o	k7c4	o
Rhineovy	Rhineův	k2eAgInPc4d1	Rhineův
pokusy	pokus	k1gInPc4	pokus
se	s	k7c7	s
zenerovými	zenerův	k2eAgFnPc7d1	zenerova
kartami	karta	k1gFnPc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
procento	procento	k1gNnSc4	procento
uhádnutí	uhádnutí	k1gNnSc2	uhádnutí
losované	losovaný	k2eAgFnSc2d1	losovaná
karty	karta	k1gFnSc2	karta
pokusnou	pokusný	k2eAgFnSc7d1	pokusná
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
statistickému	statistický	k2eAgNnSc3d1	statistické
rozložení	rozložení	k1gNnSc3	rozložení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Stejných	stejný	k2eAgInPc2d1	stejný
výsledků	výsledek	k1gInPc2	výsledek
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pokusná	pokusný	k2eAgFnSc1d1	pokusná
osoba	osoba	k1gFnSc1	osoba
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
dokonce	dokonce	k9	dokonce
i	i	k9	i
k	k	k7c3	k
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
prekognici	prekognice	k1gFnSc3	prekognice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
losování	losování	k1gNnSc2	losování
uběhl	uběhnout	k5eAaPmAgInS	uběhnout
delší	dlouhý	k2eAgInSc1d2	delší
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
Junga	Jung	k1gMnSc4	Jung
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
afektivní	afektivní	k2eAgInSc1d1	afektivní
stav	stav	k1gInSc1	stav
lidské	lidský	k2eAgFnSc2d1	lidská
psýchy	psýcha	k1gFnSc2	psýcha
odráží	odrážet	k5eAaImIp3nS	odrážet
tytéž	týž	k3xTgInPc4	týž
vzorce	vzorec	k1gInPc4	vzorec
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
je	být	k5eAaImIp3nS	být
utvářena	utvářen	k2eAgFnSc1d1	utvářena
realita	realita	k1gFnSc1	realita
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
odvodil	odvodit	k5eAaPmAgMnS	odvodit
myšlenku	myšlenka	k1gFnSc4	myšlenka
nezávislosti	nezávislost	k1gFnSc2	nezávislost
psychických	psychický	k2eAgInPc2d1	psychický
dějů	děj	k1gInPc2	děj
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
koncepce	koncepce	k1gFnSc1	koncepce
synchronicity	synchronicita	k1gFnPc4	synchronicita
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
předmětem	předmět	k1gInSc7	předmět
kritického	kritický	k2eAgNnSc2d1	kritické
hodnocení	hodnocení	k1gNnSc2	hodnocení
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
srovnávána	srovnávat	k5eAaImNgFnS	srovnávat
s	s	k7c7	s
typem	typ	k1gInSc7	typ
magického	magický	k2eAgNnSc2d1	magické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
nepřestával	přestávat	k5eNaImAgMnS	přestávat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
metodologie	metodologie	k1gFnSc1	metodologie
evropské	evropský	k2eAgFnSc2d1	Evropská
vědy	věda	k1gFnSc2	věda
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
chápání	chápání	k1gNnSc3	chápání
jednoty	jednota	k1gFnSc2	jednota
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
unus	unusit	k5eAaPmRp2nS	unusit
mundus	mundus	k1gInSc1	mundus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
svázána	svázán	k2eAgFnSc1d1	svázána
se	s	k7c7	s
subjekt-objektovým	subjektbjektový	k2eAgNnSc7d1	subjekt-objektový
rozdělením	rozdělení	k1gNnSc7	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
se	s	k7c7	s
Zenerovými	Zenerův	k2eAgFnPc7d1	Zenerova
kartami	karta	k1gFnPc7	karta
však	však	k9	však
byly	být	k5eAaImAgInP	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k8xC	jako
zdiskreditované	zdiskreditovaný	k2eAgInPc1d1	zdiskreditovaný
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
pochybné	pochybný	k2eAgFnSc2d1	pochybná
metodologie	metodologie	k1gFnSc2	metodologie
a	a	k8xC	a
případného	případný	k2eAgNnSc2d1	případné
podvádění	podvádění	k1gNnSc2	podvádění
<g/>
.	.	kIx.	.
</s>
<s>
Analogii	analogie	k1gFnSc4	analogie
svého	svůj	k3xOyFgNnSc2	svůj
stanoviska	stanovisko	k1gNnSc2	stanovisko
našel	najít	k5eAaPmAgMnS	najít
Jung	Jung	k1gMnSc1	Jung
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
Knize	kniha	k1gFnSc6	kniha
proměn	proměna	k1gFnPc2	proměna
(	(	kIx(	(
<g/>
I-ťing	I-ťing	k1gInSc1	I-ťing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
německý	německý	k2eAgInSc1d1	německý
překlad	překlad	k1gInSc1	překlad
pořízený	pořízený	k2eAgInSc1d1	pořízený
Richardem	Richard	k1gMnSc7	Richard
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
opatřil	opatřit	k5eAaPmAgMnS	opatřit
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
.	.	kIx.	.
</s>
<s>
Hexagram	hexagram	k1gInSc1	hexagram
sestavený	sestavený	k2eAgInSc1d1	sestavený
v	v	k7c4	v
určitou	určitý	k2eAgFnSc4d1	určitá
chvíli	chvíle	k1gFnSc4	chvíle
náhodným	náhodný	k2eAgInSc7d1	náhodný
losem	los	k1gInSc7	los
se	se	k3xPyFc4	se
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
chvílí	chvíle	k1gFnSc7	chvíle
shoduje	shodovat	k5eAaImIp3nS	shodovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Hexagram	hexagram	k1gInSc1	hexagram
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Junga	Jung	k1gMnSc2	Jung
exponentem	exponent	k1gMnSc7	exponent
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
indikuje	indikovat	k5eAaBmIp3nS	indikovat
základní	základní	k2eAgFnSc4d1	základní
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převažovala	převažovat	k5eAaImAgFnS	převažovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
je	být	k5eAaImIp3nS	být
koincidence	koincidence	k1gFnSc1	koincidence
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1	fyzické
i	i	k9	i
psychické	psychický	k2eAgFnPc4d1	psychická
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
exponenty	exponent	k1gMnPc7	exponent
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
téže	týž	k3xTgFnSc2	týž
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Orákulum	orákulum	k1gNnSc1	orákulum
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pro	pro	k7c4	pro
náhodně	náhodně	k6eAd1	náhodně
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
hexagramy	hexagram	k1gInPc4	hexagram
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
korespondují	korespondovat	k5eAaImIp3nP	korespondovat
s	s	k7c7	s
psychologickým	psychologický	k2eAgNnSc7d1	psychologické
nastavením	nastavení	k1gNnSc7	nastavení
tazatele	tazatel	k1gMnSc2	tazatel
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
smysluplnost	smysluplnost	k1gFnSc1	smysluplnost
je	být	k5eAaImIp3nS	být
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
odpovědi	odpověď	k1gFnSc2	odpověď
je	být	k5eAaImIp3nS	být
vtažena	vtažen	k2eAgFnSc1d1	vtažena
celá	celý	k2eAgFnSc1d1	celá
tazatelova	tazatelův	k2eAgFnSc1d1	tazatelova
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
synchronistický	synchronistický	k2eAgInSc4d1	synchronistický
koncept	koncept	k1gInSc4	koncept
promýšlel	promýšlet	k5eAaImAgMnS	promýšlet
Jung	Jung	k1gMnSc1	Jung
s	s	k7c7	s
teoretickým	teoretický	k2eAgMnSc7d1	teoretický
fyzikem	fyzik	k1gMnSc7	fyzik
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
Paulim	Paulima	k1gFnPc2	Paulima
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
konceptu	koncept	k1gInSc2	koncept
se	se	k3xPyFc4	se
na	na	k7c6	na
kvantové	kvantový	k2eAgFnSc6d1	kvantová
úrovni	úroveň	k1gFnSc6	úroveň
spíše	spíše	k9	spíše
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	s	k7c7	s
situacemi	situace	k1gFnPc7	situace
než	než	k8xS	než
experimentálně	experimentálně	k6eAd1	experimentálně
opakovatelnými	opakovatelný	k2eAgFnPc7d1	opakovatelná
kauzalitami	kauzalita	k1gFnPc7	kauzalita
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
nečekané	čekaný	k2eNgFnSc2d1	nečekaná
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
koincidence	koincidence	k1gFnSc2	koincidence
mění	měnit	k5eAaImIp3nS	měnit
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
přísně	přísně	k6eAd1	přísně
konsekventní	konsekventní	k2eAgNnSc1d1	konsekventní
chápání	chápání	k1gNnSc1	chápání
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
elementy	element	k1gInPc7	element
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
látku	látka	k1gFnSc4	látka
skutečnosti	skutečnost	k1gFnSc2	skutečnost
(	(	kIx(	(
<g/>
hexagramy	hexagram	k1gInPc1	hexagram
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
binárních	binární	k2eAgFnPc2d1	binární
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
stavy	stav	k1gInPc1	stav
<g/>
)	)	kIx)	)
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
přednášce	přednáška	k1gFnSc6	přednáška
o	o	k7c6	o
synchronicitě	synchronicita	k1gFnSc6	synchronicita
uvedl	uvést	k5eAaPmAgMnS	uvést
Jung	Jung	k1gMnSc1	Jung
příklad	příklad	k1gInSc4	příklad
nepravděpodobné	pravděpodobný	k2eNgFnSc2d1	nepravděpodobná
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
smysluplné	smysluplný	k2eAgFnSc2d1	smysluplná
koincidence	koincidence	k1gFnSc2	koincidence
akauzální	akauzální	k2eAgFnSc2d1	akauzální
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pacientku	pacientka	k1gFnSc4	pacientka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trpěla	trpět	k5eAaImAgFnS	trpět
"	"	kIx"	"
<g/>
hořem	hoře	k1gNnSc7	hoře
z	z	k7c2	z
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgFnPc1d1	extrémní
formou	forma	k1gFnSc7	forma
racionalismu	racionalismus	k1gInSc2	racionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
případu	případ	k1gInSc6	případ
říká	říkat	k5eAaImIp3nS	říkat
(	(	kIx(	(
<g/>
citace	citace	k1gFnSc1	citace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
neplodných	plodný	k2eNgInPc6d1	neplodný
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
zmírnění	zmírnění	k1gNnSc4	zmírnění
jejího	její	k3xOp3gInSc2	její
racionalismu	racionalismus	k1gInSc2	racionalismus
poněkud	poněkud	k6eAd1	poněkud
humánnějším	humánní	k2eAgNnSc7d2	humánnější
uvažováním	uvažování	k1gNnSc7	uvažování
–	–	k?	–
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
omezit	omezit	k5eAaPmF	omezit
na	na	k7c4	na
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
snad	snad	k9	snad
přihodí	přihodit	k5eAaPmIp3nP	přihodit
něco	něco	k3yInSc4	něco
nečekaného	čekaný	k2eNgNnSc2d1	nečekané
a	a	k8xC	a
iracionálního	iracionální	k2eAgNnSc2d1	iracionální
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
rozbít	rozbít	k5eAaPmF	rozbít
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
retortu	retorta	k1gFnSc4	retorta
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
<g/>
:	:	kIx,	:
Nejdřív	dříve	k6eAd3	dříve
měla	mít	k5eAaImAgFnS	mít
působivý	působivý	k2eAgInSc4d1	působivý
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jí	on	k3xPp3gFnSc7	on
někdo	někdo	k3yInSc1	někdo
daroval	darovat	k5eAaPmAgMnS	darovat
drahocenný	drahocenný	k2eAgInSc4d1	drahocenný
šperk	šperk	k1gInSc4	šperk
–	–	k?	–
zlatého	zlatý	k2eAgMnSc4d1	zlatý
skarabea	skarabeus	k1gMnSc4	skarabeus
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
měl	mít	k5eAaImAgMnS	mít
Jung	Jung	k1gMnSc1	Jung
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
klientkou	klientka	k1gFnSc7	klientka
sezení	sezení	k1gNnSc1	sezení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
něco	něco	k3yInSc1	něco
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zatímco	zatímco	k8xS	zatímco
mi	já	k3xPp1nSc3	já
tento	tento	k3xDgInSc4	tento
sen	sen	k1gInSc4	sen
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
<g/>
,	,	kIx,	,
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
za	za	k7c7	za
mnou	já	k3xPp1nSc7	já
něco	něco	k6eAd1	něco
tiše	tiš	k1gFnSc2	tiš
klepe	klepat	k5eAaImIp3nS	klepat
na	na	k7c4	na
okno	okno	k1gNnSc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Otočil	otočit	k5eAaPmAgMnS	otočit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
a	a	k8xC	a
spatřil	spatřit	k5eAaPmAgMnS	spatřit
jsem	být	k5eAaImIp1nS	být
dost	dost	k6eAd1	dost
velký	velký	k2eAgInSc1d1	velký
létající	létající	k2eAgInSc1d1	létající
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvenku	zvenku	k6eAd1	zvenku
narážel	narážet	k5eAaPmAgInS	narážet
do	do	k7c2	do
okenic	okenice	k1gFnPc2	okenice
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
temné	temný	k2eAgFnSc2d1	temná
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdálo	zdát	k5eAaImAgNnS	zdát
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
jsem	být	k5eAaImIp1nS	být
okno	okno	k1gNnSc4	okno
otevřel	otevřít	k5eAaPmAgInS	otevřít
a	a	k8xC	a
přilétající	přilétající	k2eAgInSc4d1	přilétající
hmyz	hmyz	k1gInSc4	hmyz
jsem	být	k5eAaImIp1nS	být
chytil	chytit	k5eAaPmAgMnS	chytit
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
Cetonia	Cetonium	k1gNnPc1	Cetonium
aurata	aure	k1gNnPc1	aure
<g/>
,	,	kIx,	,
Scarabaeidae	Scarabaeida	k1gInPc1	Scarabaeida
<g/>
,	,	kIx,	,
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
zlatohlávek	zlatohlávek	k1gMnSc1	zlatohlávek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
zelenozlaté	zelenozlatý	k2eAgFnSc3d1	zelenozlatá
barvě	barva	k1gFnSc3	barva
přibližně	přibližně	k6eAd1	přibližně
podobá	podobat	k5eAaImIp3nS	podobat
nejspíše	nejspíše	k9	nejspíše
skarabeovi	skarabeův	k2eAgMnPc1d1	skarabeův
<g/>
.	.	kIx.	.
</s>
<s>
Podal	podat	k5eAaPmAgMnS	podat
jsem	být	k5eAaImIp1nS	být
brouka	brouk	k1gMnSc4	brouk
své	svůj	k3xOyFgFnSc6	svůj
pacientce	pacientka	k1gFnSc6	pacientka
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Tady	tady	k6eAd1	tady
máte	mít	k5eAaImIp2nP	mít
svého	svůj	k3xOyFgMnSc4	svůj
skarabea	skarabeus	k1gMnSc4	skarabeus
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
prorazila	prorazit	k5eAaPmAgFnS	prorazit
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
racionalismu	racionalismus	k1gInSc2	racionalismus
žádoucí	žádoucí	k2eAgFnSc4d1	žádoucí
díru	díra	k1gFnSc4	díra
a	a	k8xC	a
led	led	k1gInSc4	led
jejího	její	k3xOp3gInSc2	její
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
odporu	odpor	k1gInSc2	odpor
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
prolomil	prolomit	k5eAaPmAgMnS	prolomit
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
mohla	moct	k5eAaImAgFnS	moct
nyní	nyní	k6eAd1	nyní
úspěšně	úspěšně	k6eAd1	úspěšně
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kritika	kritika	k1gFnSc1	kritika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
běžná	běžný	k2eAgFnSc1d1	běžná
věda	věda	k1gFnSc1	věda
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
synchronicitu	synchronicita	k1gFnSc4	synchronicita
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
je	být	k5eAaImIp3nS	být
založeno	založen	k2eAgNnSc1d1	založeno
buď	buď	k8xC	buď
na	na	k7c6	na
pravděpodobnostním	pravděpodobnostní	k2eAgInSc6d1	pravděpodobnostní
počtu	počet	k1gInSc6	počet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
občasný	občasný	k2eAgInSc1d1	občasný
současný	současný	k2eAgInSc1d1	současný
výskyt	výskyt	k1gInSc1	výskyt
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
synchronicitních	synchronicitní	k2eAgFnPc2d1	synchronicitní
situací	situace	k1gFnPc2	situace
může	moct	k5eAaImIp3nS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
náhodě	náhoda	k1gFnSc3	náhoda
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
jejich	jejich	k3xOp3gFnPc4	jejich
společné	společný	k2eAgFnPc4d1	společná
"	"	kIx"	"
<g/>
skryté	skrytý	k2eAgFnPc4d1	skrytá
<g/>
"	"	kIx"	"
příčiny	příčina	k1gFnPc4	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
pak	pak	k6eAd1	pak
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
náchylný	náchylný	k2eAgMnSc1d1	náchylný
všímat	všímat	k5eAaImF	všímat
si	se	k3xPyFc3	se
různých	různý	k2eAgInPc2d1	různý
jevů	jev	k1gInPc2	jev
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
Konfirmační	konfirmační	k2eAgInSc1d1	konfirmační
zkreslení	zkreslení	k1gNnSc2	zkreslení
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
heuristik	heuristika	k1gFnPc2	heuristika
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
synchronicita	synchronicita	k1gFnSc1	synchronicita
proto	proto	k8xC	proto
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
popisuje	popisovat	k5eAaImIp3nS	popisovat
spíše	spíše	k9	spíše
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
pocit	pocit	k1gInSc4	pocit
nevysvětlitelné	vysvětlitelný	k2eNgFnSc2d1	nevysvětlitelná
koincidence	koincidence	k1gFnSc2	koincidence
situací	situace	k1gFnPc2	situace
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
BEGG	BEGG	kA	BEGG
<g/>
,	,	kIx,	,
Deike	Deike	k1gNnSc2	Deike
<g/>
.	.	kIx.	.
</s>
<s>
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
<g/>
:	:	kIx,	:
náhody	náhoda	k1gFnPc1	náhoda
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
nedochází	docházet	k5eNaImIp3nS	docházet
náhodou	náhodou	k6eAd1	náhodou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Metafora	metafora	k1gFnSc1	metafora
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86518	[number]	k4	86518
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
BEGG	BEGG	kA	BEGG
<g/>
,	,	kIx,	,
Deike	Deike	k1gNnSc2	Deike
<g/>
.	.	kIx.	.
</s>
<s>
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
<g/>
:	:	kIx,	:
moderní	moderní	k2eAgNnSc1d1	moderní
pojetí	pojetí	k1gNnSc1	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Emitos	Emitos	k1gMnSc1	Emitos
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87171	[number]	k4	87171
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FRANZ	FRANZ	kA	FRANZ
<g/>
,	,	kIx,	,
Marie-Louise	Marie-Louise	k1gFnSc1	Marie-Louise
von	von	k1gInSc1	von
<g/>
.	.	kIx.	.
</s>
<s>
Věštění	věštění	k1gNnSc1	věštění
a	a	k8xC	a
synchronicita	synchronicita	k1gFnSc1	synchronicita
<g/>
:	:	kIx,	:
psychologie	psychologie	k1gFnSc1	psychologie
smyslupné	smyslupný	k2eAgFnSc2d1	smyslupná
náhody	náhoda	k1gFnSc2	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
opr	opr	k?	opr
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Tomáše	Tomáš	k1gMnSc2	Tomáš
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85880	[number]	k4	85880
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
GILL	GILL	kA	GILL
<g/>
,	,	kIx,	,
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
.	.	kIx.	.
</s>
<s>
Meta	meta	k1gFnSc1	meta
Secret	Secreta	k1gFnPc2	Secreta
<g/>
:	:	kIx,	:
všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7451	[number]	k4	7451
<g/>
-	-	kIx~	-
<g/>
115	[number]	k4	115
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
HOPCKE	HOPCKE	kA	HOPCKE
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
H.	H.	kA	H.
Nic	nic	k6eAd1	nic
není	být	k5eNaImIp3nS	být
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
:	:	kIx,	:
příběhy	příběh	k1gInPc7	příběh
našich	náš	k3xOp1gInPc2	náš
životů	život	k1gInPc2	život
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
neobyčejných	obyčejný	k2eNgFnPc2d1	neobyčejná
shod	shoda	k1gFnPc2	shoda
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7202	[number]	k4	7202
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
JUNG	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Archetypy	archetyp	k1gInPc4	archetyp
a	a	k8xC	a
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Tomáše	Tomáš	k1gMnSc2	Tomáš
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85880	[number]	k4	85880
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.356	.356	k4	.356
<g/>
-	-	kIx~	-
<g/>
372	[number]	k4	372
<g/>
.	.	kIx.	.
</s>
<s>
KMUNÍČEK	KMUNÍČEK	kA	KMUNÍČEK
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
filozofický	filozofický	k2eAgInSc4d1	filozofický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
upravené	upravený	k2eAgNnSc1d1	upravené
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Bílovice	Bílovice	k1gInPc1	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
:	:	kIx,	:
Text	text	k1gInSc1	text
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
8752	[number]	k4	8752
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
KOMÁREK	Komárek	k1gMnSc1	Komárek
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Spasení	spasení	k1gNnSc1	spasení
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
psychosomatika	psychosomatika	k1gFnSc1	psychosomatika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1287	[number]	k4	1287
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
MOSS	MOSS	kA	MOSS
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc4	tři
"	"	kIx"	"
<g/>
pouhé	pouhý	k2eAgMnPc4d1	pouhý
<g/>
"	"	kIx"	"
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
okusit	okusit	k5eAaPmF	okusit
sílu	síla	k1gFnSc4	síla
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
náhody	náhoda	k1gFnSc2	náhoda
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
CEDR	cedr	k1gInSc1	cedr
-	-	kIx~	-
Cesty	cesta	k1gFnSc2	cesta
duchovního	duchovní	k2eAgInSc2d1	duchovní
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7387	[number]	k4	7387
<g/>
-	-	kIx~	-
<g/>
359	[number]	k4	359
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
PÁLEŠ	PÁLEŠ	kA	PÁLEŠ
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Angelologie	Angelologie	k1gFnSc1	Angelologie
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
synchronicita	synchronicita	k1gFnSc1	synchronicita
a	a	k8xC	a
periodicita	periodicita	k1gFnSc1	periodicita
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Sophia	Sophia	k1gFnSc1	Sophia
<g/>
,	,	kIx,	,
2004-	[number]	k4	2004-
.	.	kIx.	.
sv.	sv.	kA	sv.
Základy	základ	k1gInPc4	základ
duchovní	duchovní	k2eAgFnSc2d1	duchovní
vědy	věda	k1gFnSc2	věda
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
RANDLES	RANDLES	kA	RANDLES
<g/>
,	,	kIx,	,
Jenny	Jenna	k1gFnSc2	Jenna
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
A.	A.	kA	A.
HOUGH	HOUGH	kA	HOUGH
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
nevysvětleného	vysvětlený	k2eNgInSc2d1	nevysvětlený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
:	:	kIx,	:
Mustang	mustang	k1gMnSc1	mustang
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7191	[number]	k4	7191
<g/>
-	-	kIx~	-
<g/>
163	[number]	k4	163
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
RICHO	RICHO	kA	RICHO
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
<g/>
:	:	kIx,	:
skrytá	skrytý	k2eAgFnSc1d1	skrytá
moc	moc	k1gFnSc1	moc
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
série	série	k1gFnSc2	série
náhod	náhoda	k1gFnPc2	náhoda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
světy	svět	k1gInPc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7387	[number]	k4	7387
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
WILCOCK	WILCOCK	kA	WILCOCK
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
zvaný	zvaný	k2eAgInSc1d1	zvaný
synchronicita	synchronicita	k1gFnSc1	synchronicita
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
nám	my	k3xPp1nPc3	my
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
skrytá	skrytý	k2eAgFnSc1d1	skrytá
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7306	[number]	k4	7306
<g/>
-	-	kIx~	-
<g/>
584	[number]	k4	584
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Carl	Carl	k1gMnSc1	Carl
Jung	Jung	k1gMnSc1	Jung
and	and	k?	and
Synchronicity	Synchronicita	k1gFnSc2	Synchronicita
</s>
