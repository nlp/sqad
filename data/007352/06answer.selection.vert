<s>
Synchronicita	Synchronicita	k1gFnSc1	Synchronicita
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
synchronos	synchronos	k1gInSc1	synchronos
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psycholog	psycholog	k1gMnSc1	psycholog
C.	C.	kA	C.
G.	G.	kA	G.
Jung	Jung	k1gMnSc1	Jung
jako	jako	k8xC	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
příčinně	příčinně	k6eAd1	příčinně
nevysvětlitelné	vysvětlitelný	k2eNgNnSc4d1	nevysvětlitelné
(	(	kIx(	(
<g/>
akauzální	akauzální	k2eAgNnSc4d1	akauzální
<g/>
)	)	kIx)	)
setkání	setkání	k1gNnSc4	setkání
dvou	dva	k4xCgInPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tím	ten	k3xDgNnSc7	ten
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
prožívání	prožívání	k1gNnSc2	prožívání
subjektu	subjekt	k1gInSc2	subjekt
získávají	získávat	k5eAaImIp3nP	získávat
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
