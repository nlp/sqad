<s>
Zámek	zámek	k1gInSc1
Chambord	Chamborda	k1gFnPc2
</s>
<s>
Zámek	zámek	k1gInSc1
Chambord	Chamborda	k1gFnPc2
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sloh	sloha	k1gFnPc2
</s>
<s>
renesanční	renesanční	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Architekti	architekt	k1gMnPc1
</s>
<s>
Jules	Jules	k1gInSc1
Hardouin-Mansart	Hardouin-Mansarta	k1gFnPc2
<g/>
,	,	kIx,
Domenico	Domenico	k6eAd1
da	da	k?
Cortona	Cortona	k1gFnSc1
a	a	k8xC
Leonardo	Leonardo	k1gMnSc1
da	da	k?
Vinci	Vinca	k1gMnPc7
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1608	#num#	k4
Další	další	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
1519	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
<g/>
První	první	k4xOgFnSc1
Francouzská	francouzský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1804	#num#	k4
<g/>
)	)	kIx)
<g/>
První	první	k4xOgNnSc1
Francouzské	francouzský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1809	#num#	k4
<g/>
)	)	kIx)
<g/>
Louis	Louis	k1gMnSc1
Berthier	Berthier	k1gMnSc1
(	(	kIx(
<g/>
1809	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
Jindřich	Jindřich	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Chambord	Chambord	k1gMnSc1
(	(	kIx(
<g/>
1821	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
Bourbonsko-parmská	bourbonsko-parmský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1930	#num#	k4
<g/>
)	)	kIx)
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Chambord	Chambord	k6eAd1
<g/>
,	,	kIx,
Muides-sur-Loire	Muides-sur-Loir	k1gMnSc5
<g/>
,	,	kIx,
Neuvy	Neuv	k1gMnPc4
<g/>
,	,	kIx,
Saint-Dyé-sur-Loire	Saint-Dyé-sur-Loir	k1gMnSc5
<g/>
,	,	kIx,
Thoury	Thour	k1gMnPc4
<g/>
,	,	kIx,
Tour-en-Sologne	Tour-en-Sologn	k1gMnSc5
<g/>
,	,	kIx,
Maslives	Maslives	k1gMnSc1
<g/>
,	,	kIx,
Huisseau-sur-Cosson	Huisseau-sur-Cosson	k1gMnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
PA	Pa	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
98405	#num#	k4
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
41000006	#num#	k4
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
41000007	#num#	k4
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
41000009	#num#	k4
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
41000011	#num#	k4
<g/>
,	,	kIx,
PA	Pa	kA
<g/>
41000012	#num#	k4
<g/>
,	,	kIx,
PA41000004	PA41000004	k1gFnSc1
a	a	k8xC
PA41000003	PA41000003	k1gFnSc1
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zámek	zámek	k1gInSc1
Chambord	Chambord	k1gInSc1
[	[	kIx(
<g/>
šambór	šambór	k1gInSc1
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgMnSc1d3
ze	z	k7c2
zámků	zámek	k1gInPc2
na	na	k7c6
Loiře	Loira	k1gFnSc6
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1519	#num#	k4
až	až	k9
1547	#num#	k4
na	na	k7c4
objednávku	objednávka	k1gFnSc4
Františka	František	k1gMnSc2
I.	I.	kA
v	v	k7c6
zátoce	zátoka	k1gFnSc6
řeky	řeka	k1gFnSc2
Cosson	Cossona	k1gFnPc2
<g/>
,	,	kIx,
levého	levý	k2eAgInSc2d1
přítoku	přítok	k1gInSc2
Loiry	Loira	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
leží	ležet	k5eAaImIp3nS
asi	asi	k9
14	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Blois	Blois	k1gFnSc2
v	v	k7c6
departementu	departement	k1gInSc6
Loir-et-Cher	Loir-et-Chra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
Půdorys	půdorys	k1gInSc1
zámku	zámek	k1gInSc2
</s>
<s>
Chambord	Chambord	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
vrcholným	vrcholný	k2eAgFnPc3d1
stavbám	stavba	k1gFnPc3
renesanční	renesanční	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
a	a	k8xC
k	k	k7c3
nejznámějším	známý	k2eAgInPc3d3
zámkům	zámek	k1gInPc3
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
fasáda	fasáda	k1gFnSc1
má	mít	k5eAaImIp3nS
šířku	šířka	k1gFnSc4
128	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
zámku	zámek	k1gInSc6
je	být	k5eAaImIp3nS
440	#num#	k4
místností	místnost	k1gFnPc2
<g/>
,	,	kIx,
80	#num#	k4
schodišť	schodiště	k1gNnPc2
<g/>
,	,	kIx,
365	#num#	k4
komínů	komín	k1gInPc2
a	a	k8xC
800	#num#	k4
tesaných	tesaný	k2eAgFnPc2d1
hlavic	hlavice	k1gFnPc2
sloupů	sloup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
architekta	architekt	k1gMnSc2
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
projektoval	projektovat	k5eAaBmAgMnS
stavbu	stavba	k1gFnSc4
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
analýzy	analýza	k1gFnPc1
však	však	k9
ukazují	ukazovat	k5eAaImIp3nP
že	že	k8xS
na	na	k7c4
plány	plán	k1gInPc4
zapůsobil	zapůsobit	k5eAaPmAgInS
vliv	vliv	k1gInSc1
Leonarda	Leonardo	k1gMnSc2
da	da	k?
Vinciho	Vinci	k1gMnSc2
jenž	jenž	k3xRgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k9
architekt	architekt	k1gMnSc1
na	na	k7c6
dvoře	dvůr	k1gInSc6
Františka	František	k1gMnSc2
I.	I.	kA
avšak	avšak	k8xC
zemřel	zemřít	k5eAaPmAgInS
krátce	krátce	k6eAd1
před	před	k7c7
započetím	započetí	k1gNnSc7
stavebních	stavební	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1519	#num#	k4
<g/>
;	;	kIx,
podle	podle	k7c2
dochovaných	dochovaný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
na	na	k7c6
ní	on	k3xPp3gFnSc6
pracovalo	pracovat	k5eAaImAgNnS
1800	#num#	k4
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovní	pracovní	k2eAgFnSc2d1
podmínky	podmínka	k1gFnSc2
ztěžovalo	ztěžovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
zvoleném	zvolený	k2eAgNnSc6d1
místě	místo	k1gNnSc6
se	se	k3xPyFc4
rozkládaly	rozkládat	k5eAaImAgInP
mokřady	mokřad	k1gInPc1
a	a	k8xC
dělníci	dělník	k1gMnPc1
proto	proto	k8xC
museli	muset	k5eAaImAgMnP
do	do	k7c2
měkké	měkký	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
zatloukat	zatloukat	k5eAaImF
dubové	dubový	k2eAgInPc4d1
kůly	kůl	k1gInPc4
do	do	k7c2
hloubky	hloubka	k1gFnSc2
až	až	k9
12	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
stavba	stavba	k1gFnSc1
stála	stát	k5eAaImAgFnS
na	na	k7c6
pevných	pevný	k2eAgInPc6d1
základech	základ	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
stavbě	stavba	k1gFnSc3
byl	být	k5eAaImAgInS
použit	použit	k2eAgInSc1d1
vápenec	vápenec	k1gInSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Tuffeau	Tuffeaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
podle	podle	k7c2
vzoru	vzor	k1gInSc2
středověkých	středověký	k2eAgFnPc2d1
tvrzí	tvrz	k1gFnPc2
okolo	okolo	k7c2
centrální	centrální	k2eAgFnSc2d1
části	část	k1gFnSc2
zvané	zvaný	k2eAgInPc1d1
donjon	donjon	k1gInSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
zde	zde	k6eAd1
neplnil	plnit	k5eNaImAgMnS
obrannou	obranný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Donjon	donjon	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
pět	pět	k4xCc4
obyvatelných	obyvatelný	k2eAgNnPc2d1
pater	patro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgNnSc6
patře	patro	k1gNnSc6
zámku	zámek	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
čtvercové	čtvercový	k2eAgInPc4d1
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
kruhové	kruhový	k2eAgInPc4d1
byty	byt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc1
chodby	chodba	k1gFnPc4
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
světových	světový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
se	se	k3xPyFc4
stýkají	stýkat	k5eAaImIp3nP
ve	v	k7c6
středu	střed	k1gInSc6
zámku	zámek	k1gInSc2
jemuž	jenž	k3xRgMnSc3
dominuje	dominovat	k5eAaImIp3nS
schodiště	schodiště	k1gNnSc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
dvojité	dvojitý	k2eAgFnSc2d1
šroubovice	šroubovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zřejmě	zřejmě	k6eAd1
navrženo	navrhnout	k5eAaPmNgNnS
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Leonarda	Leonardo	k1gMnSc2
da	da	k?
Vinciho	Vinci	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
František	František	k1gMnSc1
I.	I.	kA
zámek	zámek	k1gInSc1
nechal	nechat	k5eAaPmAgInS
rozšířit	rozšířit	k5eAaPmF
a	a	k8xC
usídlil	usídlit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
rozsáhlejších	rozsáhlý	k2eAgFnPc6d2
místnostech	místnost	k1gFnPc6
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západním	západní	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
byla	být	k5eAaImAgFnS
započata	započat	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
až	až	k9
později	pozdě	k6eAd2
za	za	k7c2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
..	..	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Roku	rok	k1gInSc2
1392	#num#	k4
odkoupil	odkoupit	k5eAaPmAgInS
rod	rod	k1gInSc1
knížat	kníže	k1gMnPc2wR
z	z	k7c2
Orléans	Orléansa	k1gFnPc2
lesnatou	lesnatý	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Chambordu	Chambord	k1gInSc2
od	od	k7c2
hrabat	hrabě	k1gNnPc2
z	z	k7c2
Blois	Blois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1498	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Ludvík	Ludvík	k1gMnSc1
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orleánský	orleánský	k2eAgMnSc1d1
a	a	k8xC
pozemky	pozemek	k1gInPc1
tak	tak	k6eAd1
přešly	přejít	k5eAaPmAgInP
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1516	#num#	k4
se	se	k3xPyFc4
František	františek	k1gInSc1
I.	I.	kA
vrátil	vrátit	k5eAaPmAgInS
s	s	k7c7
Leonardem	Leonardo	k1gMnSc7
da	da	k?
Vinci	Vinca	k1gMnSc2
z	z	k7c2
Itálie	Itálie	k1gFnSc2
kde	kde	k6eAd1
se	se	k3xPyFc4
inspiroval	inspirovat	k5eAaBmAgMnS
italskou	italský	k2eAgFnSc7d1
renesanční	renesanční	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1519	#num#	k4
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
stavební	stavební	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
však	však	k9
postupovaly	postupovat	k5eAaImAgFnP
pomalu	pomalu	k6eAd1
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1521	#num#	k4
až	až	k9
1526	#num#	k4
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
přerušila	přerušit	k5eAaPmAgFnS
italská	italský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
pak	pak	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
od	od	k7c2
září	září	k1gNnSc2
roku	rok	k1gInSc2
1526	#num#	k4
<g/>
;	;	kIx,
účastnilo	účastnit	k5eAaImAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
asi	asi	k9
1800	#num#	k4
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
byly	být	k5eAaImAgFnP
dokončeny	dokončit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
úmrtí	úmrtí	k1gNnSc2
Františka	František	k1gMnSc2
I.	I.	kA
v	v	k7c6
roce	rok	k1gInSc6
1547	#num#	k4
<g/>
,	,	kIx,
dostavbou	dostavba	k1gFnSc7
královských	královský	k2eAgNnPc2d1
apartmá	apartmá	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
sám	sám	k3xTgMnSc1
na	na	k7c6
zámku	zámek	k1gInSc6
během	během	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
velmi	velmi	k6eAd1
málo	málo	k4c4
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
králové	král	k1gMnPc1
zámek	zámek	k1gInSc4
nechali	nechat	k5eAaPmAgMnP
opuštěný	opuštěný	k2eAgInSc1d1
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
postupnému	postupný	k2eAgNnSc3d1
chátrání	chátrání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1725	#num#	k4
až	až	k9
1733	#num#	k4
zámek	zámek	k1gInSc1
obýval	obývat	k5eAaImAgInS
Stanisław	Stanisław	k1gFnSc4
Leszczyński	Leszczyński	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
sesazený	sesazený	k2eAgMnSc1d1
polský	polský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
tchán	tchán	k1gMnSc1
Ludvíka	Ludvík	k1gMnSc2
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1745	#num#	k4
do	do	k7c2
r.	r.	kA
1750	#num#	k4
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
kasárna	kasárna	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1792	#num#	k4
nechala	nechat	k5eAaPmAgFnS
revoluční	revoluční	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
prodat	prodat	k5eAaPmF
mobiliář	mobiliář	k1gInSc4
zámku	zámek	k1gInSc2
a	a	k8xC
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
zámek	zámek	k1gInSc1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
maršálu	maršál	k1gMnSc3
L.	L.	kA
A.	A.	kA
Berthierovi	Berthierův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
francouzsko-pruské	francouzsko-pruský	k2eAgFnSc2d1
války	válka	k1gFnSc2
roku	rok	k1gInSc2
1870	#num#	k4
sloužil	sloužit	k5eAaImAgInS
zámek	zámek	k1gInSc1
jako	jako	k8xC,k8xS
polní	polní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
majetkem	majetek	k1gInSc7
bývalého	bývalý	k2eAgInSc2d1
vládnoucího	vládnoucí	k2eAgInSc2d1
rodu	rod	k1gInSc2
Parmského	parmský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
je	být	k5eAaImIp3nS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zde	zde	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
byly	být	k5eAaImAgFnP
uskladněny	uskladněn	k2eAgFnPc1d1
umělecké	umělecký	k2eAgFnPc1d1
sbírky	sbírka	k1gFnPc1
Pařížského	pařížský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c4
Louvre	Louvre	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zámek	zámek	k1gInSc1
Chambord	Chamborda	k1gFnPc2
</s>
<s>
Střecha	střecha	k1gFnSc1
zámku	zámek	k1gInSc2
s	s	k7c7
mnoha	mnoho	k4c7
komíny	komín	k1gInPc7
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
zámků	zámek	k1gInPc2
na	na	k7c6
Loiře	Loira	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zámek	zámek	k1gInSc1
Chambord	Chambordo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
zámku	zámek	k1gInSc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Zámek	zámek	k1gInSc1
Chambord	Chambord	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4113210-5	4113210-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85022740	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
172374209	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85022740	#num#	k4
</s>
