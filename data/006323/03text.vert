<s>
Daniel	Daniel	k1gMnSc1	Daniel
Louis	Louis	k1gMnSc1	Louis
Castellaneta	Castellaneta	k1gFnSc1	Castellaneta
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
dabér	dabér	k1gMnSc1	dabér
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
známý	známý	k1gMnSc1	známý
především	především	k9	především
dabováním	dabování	k1gNnSc7	dabování
Homera	Homer	k1gMnSc2	Homer
Simpsona	Simpson	k1gMnSc2	Simpson
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
postavami	postava	k1gFnPc7	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
v	v	k7c6	v
Simpsonových	Simpsonová	k1gFnPc6	Simpsonová
dabuje	dabovat	k5eAaBmIp3nS	dabovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Abe	Abe	k1gMnSc1	Abe
Simpson	Simpson	k1gMnSc1	Simpson
<g/>
,	,	kIx,	,
Barney	Barnea	k1gMnSc2	Barnea
Gumble	Gumble	k1gMnSc2	Gumble
<g/>
,	,	kIx,	,
Šáša	Šášus	k1gMnSc2	Šášus
Krusty	krusta	k1gFnSc2	krusta
<g/>
,	,	kIx,	,
Školník	školník	k1gMnSc1	školník
Willie	Willie	k1gFnSc2	Willie
<g/>
,	,	kIx,	,
Starosta	Starosta	k1gMnSc1	Starosta
Quimby	Quimba	k1gFnSc2	Quimba
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Moleman	Moleman	k1gMnSc1	Moleman
<g/>
,	,	kIx,	,
Itchy	Itcha	k1gFnPc1	Itcha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Poochie	Poochie	k1gFnSc1	Poochie
<g/>
.	.	kIx.	.
</s>
<s>
Cash	cash	k1gFnSc1	cash
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Simpsons	Simpsons	k1gInSc1	Simpsons
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Pursuit	Pursuit	k1gMnSc1	Pursuit
of	of	k?	of
Happyness	Happyness	k1gInSc1	Happyness
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Casper	Casper	k1gInSc1	Casper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Scare	Scar	k1gInSc5	Scar
School	Schoola	k1gFnPc2	Schoola
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
Jimmy	Jimma	k1gFnSc2	Jimma
Timmy	Timma	k1gFnSc2	Timma
Power	Power	k1gMnSc1	Power
Hour	Hour	k1gMnSc1	Hour
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Cat	Cat	k1gFnSc2	Cat
in	in	k?	in
the	the	k?	the
Hat	hat	k0	hat
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Kim	Kim	k1gMnSc1	Kim
Possible	Possible	k1gMnSc1	Possible
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Secret	Secret	k1gMnSc1	Secret
Files	Files	k1gMnSc1	Files
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Buttleman	Buttleman	k1gMnSc1	Buttleman
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Hey	Hey	k1gMnSc1	Hey
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
The	The	k1gFnSc3	The
Movie	Movie	k1gFnSc2	Movie
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Return	Return	k1gMnSc1	Return
to	ten	k3xDgNnSc4	ten
Never	Never	k1gMnSc1	Never
Land	Land	k1gMnSc1	Land
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Neil	Neil	k1gMnSc1	Neil
Simon	Simon	k1gMnSc1	Simon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Laughter	Laughter	k1gInSc1	Laughter
On	on	k3xPp3gMnSc1	on
The	The	k1gFnSc7	The
23	[number]	k4	23
<g/>
rd	rd	k?	rd
Floor	Floor	k1gMnSc1	Floor
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
:	:	kIx,	:
King	King	k1gMnSc1	King
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Space	Spaec	k1gInSc2	Spaec
Jam	jam	k1gInSc1	jam
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Toonstruck	Toonstrucka	k1gFnPc2	Toonstrucka
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Love	lov	k1gInSc5	lov
Affair	Affair	k1gMnSc1	Affair
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Client	Client	k1gMnSc1	Client
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Return	Return	k1gMnSc1	Return
of	of	k?	of
Jafar	Jafar	k1gMnSc1	Jafar
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Super	super	k2eAgMnSc1d1	super
Mario	Mario	k1gMnSc1	Mario
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Tell	Tell	k1gMnSc1	Tell
Mom	Mom	k1gMnSc1	Mom
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Babysitter	Babysitter	k1gInSc1	Babysitter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dead	Deada	k1gFnPc2	Deada
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Válka	válka	k1gFnSc1	válka
Roseových	Roseová	k1gFnPc2	Roseová
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
K-9	K-9	k1gFnSc2	K-9
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
The	The	k1gFnPc1	The
Thing	Thing	k1gInSc4	Thing
that	that	k5eAaPmF	that
Ate	Ate	k1gFnSc4	Ate
Everybody	Everyboda	k1gFnSc2	Everyboda
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Say	Say	k1gFnSc1	Say
Anything	Anything	k1gInSc1	Anything
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Nothing	Nothing	k1gInSc1	Nothing
in	in	k?	in
Common	Common	k1gInSc1	Common
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
That	Thatum	k1gNnPc2	Thatum
'	'	kIx"	'
<g/>
70	[number]	k4	70
<g/>
s	s	k7c7	s
Show	show	k1gFnSc7	show
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Veronica	Veronic	k1gInSc2	Veronic
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Legion	legion	k1gInSc1	legion
of	of	k?	of
Super	super	k2eAgInSc1d1	super
Heroes	Heroes	k1gInSc1	Heroes
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
Boris	Boris	k1gMnSc1	Boris
the	the	k?	the
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
Captain	Captain	k1gMnSc1	Captain
Howdy	Howda	k1gFnSc2	Howda
Complete	Comple	k1gNnSc2	Comple
Savages	Savagesa	k1gFnPc2	Savagesa
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Epizoda	epizoda	k1gFnSc1	epizoda
–	–	k?	–
Free	Fre	k1gInPc1	Fre
Lily	lít	k5eAaImAgInP	lít
<g/>
)	)	kIx)	)
–	–	k?	–
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Horner	Horner	k1gInSc1	Horner
<g/>
/	/	kIx~	/
<g/>
Franklin	Franklin	k1gInSc1	Franklin
the	the	k?	the
Frog	Frog	k1gInSc1	Frog
Stargate	Stargat	k1gInSc5	Stargat
SG-1	SG-1	k1gMnSc6	SG-1
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Sci-fi	scii	k1gFnSc1	sci-fi
seriál	seriál	k1gInSc1	seriál
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stargate	Stargat	k1gMnSc5	Stargat
SG-1	SG-1	k1gMnSc5	SG-1
-	-	kIx~	-
Citizen	Citizen	kA	Citizen
Joe	Joe	k1gFnSc1	Joe
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Everybody	Everyboda	k1gFnSc2	Everyboda
Loves	Loves	k1gMnSc1	Loves
Raymond	Raymond	k1gMnSc1	Raymond
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
Trenér	trenér	k1gMnSc1	trenér
Bryan	Bryan	k1gMnSc1	Bryan
Arrested	Arrested	k1gMnSc1	Arrested
Development	Development	k1gMnSc1	Development
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
Doktor	doktor	k1gMnSc1	doktor
Stein	Stein	k1gMnSc1	Stein
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
–	–	k?	–
zjizvená	zjizvený	k2eAgFnSc1d1	zjizvená
tvář	tvář	k1gFnSc1	tvář
Behind	Behinda	k1gFnPc2	Behinda
the	the	k?	the
Camera	Camer	k1gMnSc2	Camer
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Unauthorized	Unauthorized	k1gMnSc1	Unauthorized
Story	story	k1gFnSc2	story
of	of	k?	of
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Angels	Angelsa	k1gFnPc2	Angelsa
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Disney	Disnea	k1gFnSc2	Disnea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
House	house	k1gNnSc1	house
of	of	k?	of
Mouse	Mous	k1gMnSc2	Mous
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Recess	Recess	k1gInSc1	Recess
<g/>
:	:	kIx,	:
School	School	k1gInSc1	School
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Out	Out	k1gFnSc7	Out
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Olive	Oliev	k1gFnSc2	Oliev
<g/>
,	,	kIx,	,
the	the	k?	the
Other	Other	k1gMnSc1	Other
Reindeer	Reindeer	k1gMnSc1	Reindeer
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Cow	Cow	k1gFnSc1	Cow
and	and	k?	and
Chicken	Chicken	k1gInSc1	Chicken
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Hey	Hey	k1gMnSc1	Hey
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
–	–	k?	–
Děda	děda	k1gMnSc1	děda
Friends	Friends	k1gInSc1	Friends
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
Ošetřovatel	ošetřovatel	k1gMnSc1	ošetřovatel
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
After	After	k1gMnSc1	After
the	the	k?	the
Superbowl	Superbowl	k1gInSc1	Superbowl
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
One	One	k1gFnPc2	One
<g/>
"	"	kIx"	"
Aladdin	Aladdina	k1gFnPc2	Aladdina
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Earthworm	Earthworm	k1gInSc1	Earthworm
Jim	on	k3xPp3gMnPc3	on
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
–	–	k?	–
Jim	on	k3xPp3gMnPc3	on
Sonic	Sonic	k1gMnSc1	Sonic
the	the	k?	the
Hedgehog	Hedgehog	k1gMnSc1	Hedgehog
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
Lazar	Lazar	k1gMnSc1	Lazar
the	the	k?	the
Wizard	Wizard	k1gMnSc1	Wizard
v	v	k7c6	v
Super	super	k1gInSc6	super
Sonic	Sonic	k1gMnSc1	Sonic
Back	Back	k1gMnSc1	Back
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Animated	Animated	k1gMnSc1	Animated
Series	Series	k1gMnSc1	Series
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
Doktor	doktor	k1gMnSc1	doktor
Emmett	Emmett	k1gMnSc1	Emmett
L.	L.	kA	L.
Brown	Brown	k1gNnSc1	Brown
Darkwing	Darkwing	k1gInSc1	Darkwing
Duck	Duck	k1gMnSc1	Duck
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Megavolt	megavolt	k1gInSc1	megavolt
Taz-Mania	Taz-Manium	k1gNnSc2	Taz-Manium
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Fievel	Fievel	k1gInSc1	Fievel
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
American	American	k1gInSc1	American
Tails	Tails	k1gInSc1	Tails
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
Chula	Chula	k1gFnSc1	Chula
the	the	k?	the
Tarantula	tarantula	k1gFnSc1	tarantula
Dynamo	dynamo	k1gNnSc1	dynamo
Duck	Duck	k1gInSc1	Duck
(	(	kIx(	(
<g/>
199	[number]	k4	199
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
–	–	k?	–
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Tale	Tal	k1gInSc2	Tal
Spin	spina	k1gFnPc2	spina
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Televizní	televizní	k2eAgFnSc1d1	televizní
série	série	k1gFnSc1	série
Married	Married	k1gMnSc1	Married
<g/>
...	...	k?	...
<g/>
With	With	k1gMnSc1	With
Children	Childrna	k1gFnPc2	Childrna
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
Pete	Pete	k1gInSc1	Pete
The	The	k1gMnSc1	The
Simpsons	Simpsons	k1gInSc1	Simpsons
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
present	present	k1gMnSc1	present
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Tracey	Tracea	k1gFnSc2	Tracea
Ullman	Ullman	k1gMnSc1	Ullman
Show	show	k1gFnSc1	show
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Magic	Magic	k1gMnSc1	Magic
Door	Door	k1gMnSc1	Door
(	(	kIx(	(
<g/>
circa	circa	k1gFnSc1	circa
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
–	–	k?	–
Detektiv	detektiv	k1gMnSc1	detektiv
Farblonget	Farblonget	k1gMnSc1	Farblonget
Yes	Yes	k1gMnSc1	Yes
<g/>
,	,	kIx,	,
Dear	Dear	k1gMnSc1	Dear
–	–	k?	–
Walt	Walt	k1gMnSc1	Walt
<g />
.	.	kIx.	.
</s>
<s>
Futurama	Futurama	k?	Futurama
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnPc4d1	různá
epizody	epizoda	k1gFnPc4	epizoda
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1999	[number]	k4	1999
and	and	k?	and
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
Robo-Ďábel	Robo-Ďábel	k1gMnSc1	Robo-Ďábel
Frasier	Frasier	k1gMnSc1	Frasier
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
–	–	k?	–
Brad	brada	k1gFnPc2	brada
The	The	k1gFnSc1	The
Tick	Tick	k1gInSc1	Tick
Kingdom	Kingdom	k1gInSc1	Kingdom
Hearts	Hearts	k1gInSc4	Hearts
II	II	kA	II
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Džin	džin	k1gInSc1	džin
Kingdom	Kingdom	k1gInSc1	Kingdom
Hearts	Hearts	k1gInSc1	Hearts
<g/>
:	:	kIx,	:
Chain	Chain	k1gInSc1	Chain
of	of	k?	of
Memories	Memories	k1gInSc1	Memories
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Džin	džin	k1gMnSc1	džin
I	i	k8xC	i
Am	Am	k1gMnSc1	Am
Not	nota	k1gFnPc2	nota
Homer	Homer	k1gMnSc1	Homer
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
Kingdom	Kingdom	k1gInSc1	Kingdom
Hearts	Hearts	k1gInSc1	Hearts
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Džin	džin	k1gInSc1	džin
Two	Two	k1gMnSc1	Two
Lips	Lips	k1gInSc1	Lips
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
Planescape	Planescap	k1gInSc5	Planescap
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Torment	Torment	k1gMnSc1	Torment
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Nordom	Nordom	k1gInSc1	Nordom
Earthworm	Earthworm	k1gInSc1	Earthworm
Jim	on	k3xPp3gMnPc3	on
3D	[number]	k4	3D
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Earthworm	Earthworm	k1gInSc1	Earthworm
Jim	on	k3xPp3gMnPc3	on
ClayFighter	ClayFighter	k1gMnSc1	ClayFighter
63	[number]	k4	63
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Earthworm	Earthworm	k1gInSc1	Earthworm
Jim	on	k3xPp3gMnPc3	on
Toonstruck	Toonstruck	k1gMnSc1	Toonstruck
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Videohra	videohra	k1gFnSc1	videohra
<g/>
)	)	kIx)	)
–	–	k?	–
Flux	flux	k1gInSc1	flux
Wildly	Wildly	k1gMnSc2	Wildly
Castellaneta	Castellanet	k1gMnSc2	Castellanet
hrál	hrát	k5eAaImAgMnS	hrát
Harveyho	Harvey	k1gMnSc4	Harvey
Pekara	pekar	k1gMnSc4	pekar
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
adaptaci	adaptace	k1gFnSc6	adaptace
Pekarova	pekarův	k2eAgInSc2d1	pekarův
autobiografického	autobiografický	k2eAgInSc2d1	autobiografický
komiksu	komiks	k1gInSc2	komiks
American	American	k1gMnSc1	American
Splendor	Splendor	k1gMnSc1	Splendor
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dan	Dana	k1gFnPc2	Dana
Castellaneta	Castellanet	k5eAaPmNgFnS	Castellanet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
