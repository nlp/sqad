<p>
<s>
Johnsonovo	Johnsonův	k2eAgNnSc1d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
Space	Spaec	k1gInSc2	Spaec
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středisko	středisko	k1gNnSc1	středisko
Národního	národní	k2eAgInSc2d1	národní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
Houstonu	Houston	k1gInSc6	Houston
<g/>
,	,	kIx,	,
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
středisko	středisko	k1gNnSc1	středisko
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
texaského	texaský	k2eAgMnSc2d1	texaský
rodáka	rodák	k1gMnSc2	rodák
Lyndona	Lyndon	k1gMnSc2	Lyndon
B.	B.	kA	B.
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
připravující	připravující	k2eAgInSc4d1	připravující
program	program	k1gInSc4	program
letů	let	k1gInPc2	let
Apollo	Apollo	k1gMnSc1	Apollo
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Langley	Langley	k1gInPc1	Langley
svou	svůj	k3xOyFgFnSc4	svůj
Skupinu	skupina	k1gFnSc4	skupina
pro	pro	k7c4	pro
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
lety	let	k1gInPc4	let
(	(	kIx(	(
<g/>
Space	Space	k1gMnSc1	Space
Task	Task	k1gMnSc1	Task
Group	Group	k1gMnSc1	Group
–	–	k?	–
STG	STG	kA	STG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
vedl	vést	k5eAaImAgMnS	vést
Robert	Robert	k1gMnSc1	Robert
Gilhurt	Gilhurta	k1gFnPc2	Gilhurta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
přetvořena	přetvořit	k5eAaPmNgFnS	přetvořit
na	na	k7c4	na
Středisko	středisko	k1gNnSc4	středisko
pilotovaných	pilotovaný	k2eAgFnPc2d1	pilotovaná
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
Manned	Manned	k1gMnSc1	Manned
Spacecraft	Spacecraft	k1gMnSc1	Spacecraft
Center	centrum	k1gNnPc2	centrum
–	–	k?	–
MSC	MSC	kA	MSC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
středisko	středisko	k1gNnSc1	středisko
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
přetvořeno	přetvořit	k5eAaPmNgNnS	přetvořit
a	a	k8xC	a
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
Space	Space	k1gMnSc1	Space
Center	centrum	k1gNnPc2	centrum
–	–	k?	–
JSC	JSC	kA	JSC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc1	činnost
střediska	středisko	k1gNnSc2	středisko
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
oddílu	oddíl	k1gInSc2	oddíl
astronautů	astronaut	k1gMnPc2	astronaut
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
je	on	k3xPp3gMnPc4	on
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
výcvik	výcvik	k1gInSc1	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Johnsonově	Johnsonův	k2eAgNnSc6d1	Johnsonovo
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
středisku	středisko	k1gNnSc6	středisko
sídlí	sídlet	k5eAaImIp3nS	sídlet
Středisko	středisko	k1gNnSc4	středisko
řízení	řízení	k1gNnSc2	řízení
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
všechny	všechen	k3xTgInPc4	všechen
pilotované	pilotovaný	k2eAgInPc4d1	pilotovaný
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
lety	let	k1gInPc4	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
činnost	činnost	k1gFnSc4	činnost
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kosmické	kosmický	k2eAgFnSc2d1	kosmická
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
programů	program	k1gInPc2	program
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
Skylab	Skylab	k1gMnSc1	Skylab
<g/>
.	.	kIx.	.
</s>
<s>
Johnsonovo	Johnsonův	k2eAgNnSc1d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS	být
také	také	k9	také
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
řízení	řízení	k1gNnSc4	řízení
operací	operace	k1gFnPc2	operace
na	na	k7c4	na
White	Whit	k1gInSc5	Whit
Sands	Sands	k1gInSc4	Sands
Test	test	k1gInSc1	test
Facility	Facilita	k1gFnSc2	Facilita
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
záložní	záložní	k2eAgNnSc4d1	záložní
přistávací	přistávací	k2eAgNnSc4d1	přistávací
místo	místo	k1gNnSc4	místo
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
programu	program	k1gInSc2	program
Constellation	Constellation	k1gInSc1	Constellation
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nahradí	nahradit	k5eAaPmIp3nS	nahradit
raketoplány	raketoplán	k1gInPc4	raketoplán
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
Space	Space	k1gFnSc2	Space
Center	centrum	k1gNnPc2	centrum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Johnsonovo	Johnsonův	k2eAgNnSc4d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
středisko	středisko	k1gNnSc4	středisko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
Space	Space	k1gMnSc1	Space
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
