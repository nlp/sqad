<s>
Hnutí	hnutí	k1gNnSc1
basmačů	basmač	k1gInPc2
neboli	neboli	k8xC
Basmačské	Basmačský	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Б	Б	k?
[	[	kIx(
<g/>
Basmačestvo	Basmačestvo	k1gNnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
Turkestánské	turkestánský	k2eAgNnSc1d1
osvobozenecké	osvobozenecký	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
vojensko-politické	vojensko-politický	k2eAgNnSc1d1
a	a	k8xC
náboženské	náboženský	k2eAgNnSc1d1
partyzánské	partyzánský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
muslimských	muslimský	k2eAgInPc2d1
národů	národ	k1gInPc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
proti	proti	k7c3
carské	carský	k2eAgFnSc3d1
a	a	k8xC
sovětské	sovětský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>