<s>
Hnutí	hnutí	k1gNnSc1
basmačů	basmač	k1gInPc2
</s>
<s>
Hnutí	hnutí	k1gNnSc1
basmačů	basmač	k1gInPc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
první	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
ruská	ruský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Hořící	hořící	k2eAgFnSc1d1
Buchara	Buchara	k1gFnSc1
v	v	k7c6
obklíčení	obklíčení	k1gNnSc6
jednotek	jednotka	k1gFnPc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1920	#num#	k4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1916	#num#	k4
–	–	k?
1934	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Turkestánská	turkestánský	k2eAgFnSc1d1
gubernie	gubernie	k1gFnSc1
(	(	kIx(
<g/>
západní	západní	k2eAgInSc1d1
Turkestán	Turkestán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
islámské	islámský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
>	>	kIx)
<g/>
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
)	)	kIx)
Ruská	ruský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Turkestánská	turkestánský	k2eAgFnSc1d1
ASSR	ASSR	kA
</s>
<s>
Kyrgyzská	kyrgyzský	k2eAgFnSc1d1
ASSR	ASSR	kA
</s>
<s>
Chórezmská	Chórezmský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Bucharská	bucharský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
>	>	kIx)
<g/>
(	(	kIx(
<g/>
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Basmači	Basmač	k1gMnPc1
</s>
<s>
Chivský	Chivský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alašská	Alašský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
</s>
<s>
Bucharský	bucharský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
<g/>
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
podporou	podpora	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
poloviny	polovina	k1gFnSc2
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Michail	Michail	k1gMnSc1
Frunze	Frunze	k1gNnSc2
Grigorij	Grigorij	k1gFnSc2
Sokolnikov	Sokolnikov	k1gInSc1
Vladimir	Vladimir	k1gMnSc1
Lazarevič	Lazarevič	k1gMnSc1
Vasilij	Vasilij	k1gMnSc1
Šorin	Šorin	k1gMnSc1
August	August	k1gMnSc1
Kork	Kork	k1gMnSc1
Semjon	Semjona	k1gFnPc2
Pugačov	Pugačov	k1gInSc1
Michail	Michail	k1gMnSc1
Levandovskij	Levandovskij	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
Avksentevskij	Avksentevskij	k1gMnSc1
Filipp	Filippy	k1gInPc2
Gološčokin	Gološčokin	k1gMnSc1
Magaza	Magaz	k1gMnSc2
Masanč	Masanč	k1gMnSc1
Fayzulla	Fayzull	k1gMnSc2
Chodžajev	Chodžajev	k1gFnSc2
Mirza	Mirza	k1gFnSc1
Davud	Davud	k1gMnSc1
Huseynov	Huseynov	k1gInSc1
</s>
<s>
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
Ibrahim	Ibrahim	k1gMnSc1
bek	bek	k1gMnSc1
Irgaš	Irgaš	k1gMnSc1
bej	bej	k1gMnSc1
Madamin	Madamin	k2eAgMnSc1d1
bej	bej	k1gMnSc1
Džunajd	Džunajd	k1gMnSc1
chán	chán	k1gMnSc1
Sajd	sajda	k1gFnPc2
Alim	Alim	k1gMnSc1
chán	chán	k1gMnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Monstrov	Monstrov	k1gInSc4
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
Turkestánský	turkestánský	k2eAgInSc1d1
front	front	k1gInSc1
<g/>
120,000	120,000	k4
<g/>
–	–	k?
<g/>
160,000	160,000	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
při	při	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
stavu	stav	k1gInSc6
asi	asi	k9
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
přes	přes	k7c4
20	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
koncem	koncem	k7c2
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
9338	#num#	k4
zabitých	zabitý	k2eAgMnPc2d1
či	či	k8xC
zemřelých	zemřelý	k2eAgMnPc2d1
na	na	k7c6
nemoci	nemoc	k1gFnSc6
<g/>
29	#num#	k4
617	#num#	k4
zraněných	zraněný	k1gMnPc2
či	či	k8xC
nemocných	nemocný	k1gMnPc2
(	(	kIx(
<g/>
leden	leden	k1gInSc1
1921	#num#	k4
–	–	k?
červen	červen	k1gInSc1
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
516	#num#	k4
zabitých	zabitý	k1gMnPc2
<g/>
867	#num#	k4
zraněných	zraněný	k1gMnPc2
či	či	k8xC
nemocných	nemocný	k1gMnPc2
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
1922	#num#	k4
–	–	k?
červen	červen	k1gInSc1
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
40	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
9854	#num#	k4
<g/>
+	+	kIx~
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
30	#num#	k4
484	#num#	k4
<g/>
+	+	kIx~
zraněných	zraněný	k1gMnPc2
či	či	k8xC
nemocných	nemocný	k1gMnPc2
</s>
<s>
nejsou	být	k5eNaImIp3nP
známy	znám	k2eAgInPc1d1
</s>
<s>
Byly	být	k5eAaImAgFnP
zabity	zabít	k5eAaPmNgFnP
desítky	desítka	k1gFnPc1
tisíc	tisíc	k4xCgInSc1
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Několik	několik	k4yIc4
set	sto	k4xCgNnPc2
tisíc	tisíc	k4xCgInPc2
Kazachů	Kazach	k1gMnPc2
a	a	k8xC
Kyrgyzů	Kyrgyz	k1gMnPc2
bylo	být	k5eAaImAgNnS
buď	buď	k8xC
zabito	zabít	k5eAaPmNgNnS
<g/>
,	,	kIx,
nebo	nebo	k8xC
násilně	násilně	k6eAd1
vystěhováno	vystěhován	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Neznámé	známý	k2eNgNnSc1d1
množství	množství	k1gNnSc1
lidí	člověk	k1gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
hladem	hlad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Hnutí	hnutí	k1gNnSc1
basmačů	basmač	k1gInPc2
neboli	neboli	k8xC
Basmačské	Basmačský	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Б	Б	k?
[	[	kIx(
<g/>
Basmačestvo	Basmačestvo	k1gNnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
Turkestánské	turkestánský	k2eAgNnSc1d1
osvobozenecké	osvobozenecký	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
vojensko-politické	vojensko-politický	k2eAgNnSc1d1
a	a	k8xC
náboženské	náboženský	k2eAgNnSc1d1
partyzánské	partyzánský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
muslimských	muslimský	k2eAgInPc2d1
národů	národ	k1gInPc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
proti	proti	k7c3
carské	carský	k2eAgFnSc3d1
a	a	k8xC
sovětské	sovětský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Kořeny	kořen	k1gInPc1
tohoto	tento	k3xDgNnSc2
hnutí	hnutí	k1gNnSc2
lze	lze	k6eAd1
hledat	hledat	k5eAaImF
ve	v	k7c6
vzpouře	vzpoura	k1gFnSc6
proti	proti	k7c3
odvodům	odvod	k1gInPc3
ve	v	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vypukla	vypuknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
carská	carský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
počala	počnout	k5eAaPmAgFnS
odvádět	odvádět	k5eAaImF
muslimy	muslim	k1gMnPc4
pro	pro	k7c4
boje	boj	k1gInPc4
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
měsících	měsíc	k1gInPc6
po	po	k7c6
Říjnové	říjnový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
se	se	k3xPyFc4
bolševici	bolševik	k1gMnPc1
v	v	k7c6
mnoha	mnoho	k4c6
částech	část	k1gFnPc6
Ruska	Rusko	k1gNnSc2
chopili	chopit	k5eAaPmAgMnP
moci	moct	k5eAaImF
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
začala	začít	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politická	politický	k2eAgFnSc1d1
hnutí	hnutí	k1gNnPc1
turkestánských	turkestánský	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
se	se	k3xPyFc4
pokoušela	pokoušet	k5eAaImAgFnS
vytvořit	vytvořit	k5eAaPmF
ve	v	k7c6
městě	město	k1gNnSc6
Kokandu	Kokand	k1gInSc2
ve	v	k7c6
Ferganské	Ferganský	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
nezávislou	závislý	k2eNgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1918	#num#	k4
však	však	k9
bolševici	bolševik	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
Kokand	Kokand	k1gInSc4
a	a	k8xC
zmasakrovali	zmasakrovat	k5eAaPmAgMnP
přitom	přitom	k6eAd1
až	až	k9
25	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Masakr	masakr	k1gInSc1
vyvolal	vyvolat	k5eAaPmAgInS
podporu	podpora	k1gFnSc4
basmačského	basmačský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basmači	Basmač	k1gInSc3
–	–	k?
název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
turkického	turkický	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
pro	pro	k7c4
nájezdníky	nájezdník	k1gMnPc4
–	–	k?
vedli	vést	k5eAaImAgMnP
jak	jak	k6eAd1
partyzánskou	partyzánský	k2eAgFnSc4d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
konvenční	konvenční	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zachvátila	zachvátit	k5eAaPmAgFnS
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
Ferganské	Ferganský	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
a	a	k8xC
většinu	většina	k1gFnSc4
Turkestánu	Turkestán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
slavilo	slavit	k5eAaImAgNnS
toto	tento	k3xDgNnSc4
decentralizované	decentralizovaný	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
kolísavé	kolísavý	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
tažení	tažení	k1gNnPc4
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
basmačům	basmač	k1gMnPc3
do	do	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
řadu	řada	k1gFnSc4
porážek	porážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozsáhlých	rozsáhlý	k2eAgFnPc6d1
operacích	operace	k1gFnPc6
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
ústupcích	ústupek	k1gInPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
zavedla	zavést	k5eAaPmAgFnS
sovětská	sovětský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
týkaly	týkat	k5eAaImAgInP
hospodářského	hospodářský	k2eAgInSc2d1
a	a	k8xC
náboženského	náboženský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vojenské	vojenský	k2eAgNnSc1d1
štěstí	štěstí	k1gNnSc1
a	a	k8xC
veřejná	veřejný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
začaly	začít	k5eAaPmAgFnP
od	od	k7c2
basmačského	basmačský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
odvracet	odvracet	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Odpor	odpor	k1gInSc1
vůči	vůči	k7c3
ruské	ruský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
a	a	k8xC
sovětskému	sovětský	k2eAgNnSc3d1
vedení	vedení	k1gNnSc3
se	se	k3xPyFc4
opět	opět	k6eAd1
více	hodně	k6eAd2
rozhořel	rozhořet	k5eAaPmAgMnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
kolektivizační	kolektivizační	k2eAgFnPc4d1
kampaně	kampaň	k1gFnPc4
počátkem	počátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Před	před	k7c7
1	#num#	k4
<g/>
.	.	kIx.
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
byla	být	k5eAaImAgFnS
ruská	ruský	k2eAgFnSc1d1
Turkestánská	turkestánský	k2eAgFnSc1d1
gubernie	gubernie	k1gFnSc1
řízena	řízen	k2eAgFnSc1d1
z	z	k7c2
Taškentu	Taškent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferganská	Ferganský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
etnicky	etnicky	k6eAd1
různorodá	různorodý	k2eAgNnPc1d1
<g/>
,	,	kIx,
hustě	hustě	k6eAd1
osídlená	osídlený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
dělili	dělit	k5eAaImAgMnP
usedlí	usedlý	k2eAgMnPc1d1
rolníci	rolník	k1gMnPc1
nazývaní	nazývaný	k2eAgMnPc1d1
též	též	k9
sartové	sart	k1gMnPc1
a	a	k8xC
kočovníci	kočovník	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byli	být	k5eAaImAgMnP
převážně	převážně	k6eAd1
Kyrgyzové	Kyrgyzové	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
ruské	ruský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
se	se	k3xPyFc4
zní	znět	k5eAaImIp3nS
stala	stát	k5eAaPmAgFnS
významná	významný	k2eAgFnSc1d1
bavlnářská	bavlnářský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
ekonomickému	ekonomický	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
s	s	k7c7
tím	ten	k3xDgNnSc7
souvisel	souviset	k5eAaImAgMnS
<g/>
,	,	kIx,
zde	zde	k6eAd1
začala	začít	k5eAaPmAgFnS
vznikat	vznikat	k5eAaImF
průmyslová	průmyslový	k2eAgFnSc1d1
malovýroba	malovýroba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměstnanci	zaměstnanec	k1gMnPc1
dílen	dílna	k1gFnPc2
z	z	k7c2
řad	řada	k1gFnPc2
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
však	však	k9
na	na	k7c6
tom	ten	k3xDgNnSc6
byli	být	k5eAaImAgMnP
hůř	zle	k6eAd2
než	než	k8xS
jejich	jejich	k3xOp3gMnPc4
ruské	ruský	k2eAgMnPc4d1
protějšky	protějšek	k1gInPc4
a	a	k8xC
podobně	podobně	k6eAd1
nerovné	rovný	k2eNgFnSc2d1
bylo	být	k5eAaImAgNnS
i	i	k9
rozdělování	rozdělování	k1gNnSc4
zisků	zisk	k1gInPc2
plynoucích	plynoucí	k2eAgInPc2d1
z	z	k7c2
bavlny	bavlna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životní	životní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
se	se	k3xPyFc4
celkově	celkově	k6eAd1
nezlepšila	zlepšit	k5eNaPmAgNnP
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
rolníci	rolník	k1gMnPc1
se	se	k3xPyFc4
zadlužili	zadlužit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátcích	počátek	k1gInPc6
basmačského	basmačský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
ve	v	k7c6
Ferganské	Ferganský	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
mnohdy	mnohdy	k6eAd1
tvořily	tvořit	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnSc4
základnu	základna	k1gFnSc4
organizované	organizovaný	k2eAgInPc4d1
zločinecké	zločinecký	k2eAgInPc4d1
bandy	band	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stanovení	stanovení	k1gNnSc1
pevných	pevný	k2eAgFnPc2d1
cen	cena	k1gFnPc2
bavlny	bavlna	k1gFnSc2
během	během	k7c2
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
celkovou	celkový	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
ještě	ještě	k6eAd1
zhoršilo	zhoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
začala	začít	k5eAaPmAgFnS
vznikat	vznikat	k5eAaImF
početná	početný	k2eAgFnSc1d1
třída	třída	k1gFnSc1
zproletarizovaných	zproletarizovaný	k2eAgMnPc2d1
venkovských	venkovský	k2eAgMnPc2d1
bezzemků	bezzemek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimské	muslimský	k2eAgNnSc4d1
duchovenstvo	duchovenstvo	k1gNnSc4
brojilo	brojit	k5eAaImAgNnS
proti	proti	k7c3
hazardním	hazardní	k2eAgFnPc3d1
hrám	hra	k1gFnPc3
a	a	k8xC
alkoholismu	alkoholismus	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
velice	velice	k6eAd1
rozmohly	rozmoct	k5eAaPmAgFnP
a	a	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
běžným	běžný	k2eAgInSc7d1
zjevem	zjev	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značně	značně	k6eAd1
se	se	k3xPyFc4
rozrůstala	rozrůstat	k5eAaImAgFnS
i	i	k9
zločinnost	zločinnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Větší	veliký	k2eAgFnPc1d2
násilnosti	násilnost	k1gFnPc1
propukly	propuknout	k5eAaPmAgFnP
v	v	k7c6
Turkestánské	turkestánský	k2eAgFnSc6d1
gubernii	gubernie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
carská	carský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
ukončila	ukončit	k5eAaPmAgFnS
platnost	platnost	k1gFnSc4
výjimky	výjimka	k1gFnSc2
zprošťující	zprošťující	k2eAgMnPc4d1
muslimy	muslim	k1gMnPc4
povinnosti	povinnost	k1gFnSc2
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
středoasijské	středoasijský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
se	s	k7c7
střediskem	středisko	k1gNnSc7
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
Kazachstánu	Kazachstán	k1gInSc6
a	a	k8xC
Uzbekistánu	Uzbekistán	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
potlačeno	potlačit	k5eAaPmNgNnS
pomocí	pomocí	k7c2
stanného	stanný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
obyvatelstvem	obyvatelstvo	k1gNnSc7
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
kazašskými	kazašský	k2eAgMnPc7d1
a	a	k8xC
ruskými	ruský	k2eAgMnPc7d1
usedlíky	usedlík	k1gMnPc7
<g/>
,	,	kIx,
vedlo	vést	k5eAaImAgNnS
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
k	k	k7c3
rozsáhlým	rozsáhlý	k2eAgInPc3d1
masakrům	masakr	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřely	zemřít	k5eAaPmAgInP
tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
a	a	k8xC
další	další	k2eAgInPc4d1
statisíce	statisíce	k1gInPc4
jich	on	k3xPp3gMnPc2
uprchly	uprchnout	k5eAaPmAgInP
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
do	do	k7c2
sousední	sousední	k2eAgFnSc2d1
Čínské	čínský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Středoasijské	středoasijský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1916	#num#	k4
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
masová	masový	k2eAgFnSc1d1
protiruská	protiruský	k2eAgFnSc1d1
vzpoura	vzpoura	k1gFnSc1
ve	v	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
připravilo	připravit	k5eAaPmAgNnS
scénu	scéna	k1gFnSc4
pro	pro	k7c4
domorodý	domorodý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
po	po	k7c6
pádu	pád	k1gInSc6
cara	car	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potlačení	potlačení	k1gNnSc1
vzpoury	vzpoura	k1gFnSc2
bylo	být	k5eAaImAgNnS
záměrným	záměrný	k2eAgNnSc7d1
vyhlazovacím	vyhlazovací	k2eAgNnSc7d1
tažením	tažení	k1gNnSc7
proti	proti	k7c3
kazašskému	kazašský	k2eAgNnSc3d1
a	a	k8xC
kyrgyzskému	kyrgyzský	k2eAgNnSc3d1
etniku	etnikum	k1gNnSc3
ze	z	k7c2
strany	strana	k1gFnSc2
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
usedlíků	usedlík	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
při	při	k7c6
tom	ten	k3xDgNnSc6
povražděny	povražděn	k2eAgInPc1d1
nebo	nebo	k8xC
vypuzeny	vypuzen	k2eAgInPc1d1
statisíce	statisíce	k1gInPc1
Kazachů	Kazach	k1gMnPc2
a	a	k8xC
Kyrgyzů	Kyrgyz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
etnická	etnický	k2eAgFnSc1d1
čistka	čistka	k1gFnSc1
měla	mít	k5eAaImAgFnS
své	svůj	k3xOyFgInPc4
kořeny	kořen	k1gInPc4
v	v	k7c6
politice	politika	k1gFnSc6
etnické	etnický	k2eAgFnSc2d1
stejnorodosti	stejnorodost	k1gFnSc2
prováděné	prováděný	k2eAgNnSc1d1
carskou	carský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kokandská	Kokandský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
a	a	k8xC
počátek	počátek	k1gInSc4
nepřátelství	nepřátelství	k1gNnSc2
</s>
<s>
Budova	budova	k1gFnSc1
taškentského	taškentský	k2eAgInSc2d1
sovětu	sovět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
</s>
<s>
Po	po	k7c6
únorové	únorový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
muslimské	muslimský	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
síly	síla	k1gFnPc1
organizovat	organizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernistické	modernistický	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
muslimské	muslimský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
vytvořilo	vytvořit	k5eAaPmAgNnS
Islámskou	islámský	k2eAgFnSc4d1
radu	rada	k1gFnSc4
(	(	kIx(
<g/>
Šurá-i	Šurá-	k1gFnSc2
Islam	Islam	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
usilovala	usilovat	k5eAaImAgFnS
o	o	k7c4
federativní	federativní	k2eAgInSc4d1
demokratiský	demokratiský	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
by	by	kYmCp3nS
měli	mít	k5eAaImAgMnP
muslimové	muslim	k1gMnPc1
autonomii	autonomie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Konzervativnější	konzervativní	k2eAgMnPc1d2
náboženští	náboženský	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
zformovali	zformovat	k5eAaPmAgMnP
Radu	rada	k1gFnSc4
duchovenstva	duchovenstvo	k1gNnSc2
(	(	kIx(
<g/>
Šurá-i	Šurá-	k1gFnSc2
Ulema	ulema	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
záleželo	záležet	k5eAaImAgNnS
víc	hodně	k6eAd2
na	na	k7c4
uchování	uchování	k1gNnSc4
islámských	islámský	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
a	a	k8xC
náboženského	náboženský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
šaría	šarí	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
pak	pak	k6eAd1
tito	tento	k3xDgMnPc1
muslimští	muslimský	k2eAgMnPc1d1
nacionalisté	nacionalista	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
koalici	koalice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
Říjnové	říjnový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
modernisté	modernista	k1gMnPc1
podpořili	podpořit	k5eAaPmAgMnP
bolševiky	bolševik	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zmocnili	zmocnit	k5eAaPmAgMnP
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taškentský	taškentský	k2eAgInSc1d1
sovět	sovět	k1gInSc1
ovládaný	ovládaný	k2eAgInSc1d1
ruskými	ruský	k2eAgMnPc7d1
železničáři	železničář	k1gMnPc7
a	a	k8xC
proletáři	proletář	k1gMnPc7
z	z	k7c2
řad	řada	k1gFnPc2
ruských	ruský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
odmítl	odmítnout	k5eAaPmAgInS
účast	účast	k1gFnSc4
muslimů	muslim	k1gMnPc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
cítila	cítit	k5eAaImAgFnS
podvedená	podvedený	k2eAgFnSc1d1
tímto	tento	k3xDgInSc7
zjevným	zjevný	k2eAgInSc7d1
příklonem	příklon	k1gInSc7
k	k	k7c3
předchozímu	předchozí	k2eAgInSc3d1
ruskému	ruský	k2eAgInSc3d1
koloniálnímu	koloniální	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
opětovně	opětovně	k6eAd1
spojila	spojit	k5eAaPmAgNnP
s	s	k7c7
Radou	rada	k1gFnSc7
duchovenstva	duchovenstvo	k1gNnSc2
a	a	k8xC
společně	společně	k6eAd1
vytvořily	vytvořit	k5eAaPmAgFnP
Kokandskou	Kokandský	k2eAgFnSc4d1
autonomní	autonomní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
základ	základ	k1gInSc1
nezávislého	závislý	k2eNgInSc2d1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
státu	stát	k1gInSc2
v	v	k7c6
Turkestánu	Turkestán	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
řídil	řídit	k5eAaImAgInS
podle	podle	k7c2
islámského	islámský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
šaría	šarí	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taškentský	taškentský	k2eAgInSc1d1
sovět	sovět	k1gInSc1
zpočátku	zpočátku	k6eAd1
Kokandskou	Kokandský	k2eAgFnSc4d1
autonomní	autonomní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
uznal	uznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
omezil	omezit	k5eAaPmAgInS
její	její	k3xOp3gFnSc4
pravomoc	pravomoc	k1gFnSc4
na	na	k7c4
muslimskou	muslimský	k2eAgFnSc4d1
část	část	k1gFnSc4
starého	starý	k2eAgInSc2d1
Taškentu	Taškent	k1gInSc2
a	a	k8xC
navíc	navíc	k6eAd1
trval	trvat	k5eAaImAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
mít	mít	k5eAaImF
v	v	k7c6
záležitostech	záležitost	k1gFnPc6
regionálního	regionální	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
poslední	poslední	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bouřlivých	bouřlivý	k2eAgInPc6d1
nepokojích	nepokoj	k1gInPc6
v	v	k7c6
Taškentu	Taškent	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
a	a	k8xC
přes	přes	k7c4
levicové	levicový	k2eAgNnSc4d1
smýšlení	smýšlení	k1gNnSc4
řady	řada	k1gFnSc2
členů	člen	k1gMnPc2
se	se	k3xPyFc4
vláda	vláda	k1gFnSc1
přiklonila	přiklonit	k5eAaPmAgFnS
k	k	k7c3
Bílému	bílý	k2eAgNnSc3d1
hnutí	hnutí	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Politicky	politicky	k6eAd1
a	a	k8xC
vojensky	vojensky	k6eAd1
slabá	slabý	k2eAgFnSc1d1
muslimská	muslimský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
začala	začít	k5eAaPmAgFnS
hledat	hledat	k5eAaImF
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgFnS
omilostněna	omilostněn	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
bývalých	bývalý	k2eAgMnPc2d1
ozbrojených	ozbrojený	k2eAgMnPc2d1
zlodějů	zloděj	k1gMnPc2
vedená	vedený	k2eAgFnSc1d1
Ergašem	Ergaš	k1gMnSc7
bejem	bej	k1gMnSc7
a	a	k8xC
naverbována	naverbován	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
bránili	bránit	k5eAaImAgMnP
Kokand	Kokand	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
jednotka	jednotka	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
s	s	k7c7
to	ten	k3xDgNnSc4
odolat	odolat	k5eAaPmF
útoku	útok	k1gInSc3
sil	síla	k1gFnPc2
taškentského	taškentský	k2eAgInSc2d1
sovětu	sovět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1918	#num#	k4
vydrancovali	vydrancovat	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
arménští	arménský	k2eAgMnPc1d1
dašnaci	dašnace	k1gFnSc3
Kokand	Kokand	k1gInSc4
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
popsán	popsat	k5eAaPmNgInS
jako	jako	k9
pogrom	pogrom	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Zahynulo	zahynout	k5eAaPmAgNnS
při	při	k7c6
něm	on	k3xPp3gNnSc6
až	až	k9
25	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
masakr	masakr	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
popravou	poprava	k1gFnSc7
mnoha	mnoho	k4c2
ferganských	ferganský	k2eAgMnPc2d1
rolníků	rolník	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
bolševici	bolševik	k1gMnPc1
obvinili	obvinit	k5eAaPmAgMnP
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
skrývají	skrývat	k5eAaImIp3nP
zásoby	zásoba	k1gFnPc4
jídla	jídlo	k1gNnSc2
a	a	k8xC
bavlny	bavlna	k1gFnSc2
<g/>
,	,	kIx,
rozlítily	rozlítit	k5eAaPmAgFnP
muslimské	muslimský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ergaš	Ergaš	k1gMnSc1
bej	bej	k1gMnSc1
pozdvihl	pozdvihnout	k5eAaPmAgMnS
zbraně	zbraň	k1gFnPc4
proti	proti	k7c3
silám	síla	k1gFnPc3
sovětu	sovět	k1gInSc2
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
se	se	k3xPyFc4
za	za	k7c4
„	„	k?
<g/>
vrchního	vrchní	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
islámské	islámský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
byl	být	k5eAaImAgInS
počátek	počátek	k1gInSc4
Basmačského	Basmačský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rudoarmějci	rudoarmějec	k1gMnPc1
mezitím	mezitím	k6eAd1
sesadili	sesadit	k5eAaPmAgMnP
bucharského	bucharský	k2eAgMnSc2d1
emíra	emír	k1gMnSc2
Sajda	sajda	k1gFnSc1
Mohammada	Mohammada	k1gFnSc1
Alima	Alim	k1gMnSc4
chána	chán	k1gMnSc4
a	a	k8xC
dosadili	dosadit	k5eAaPmAgMnP
místo	místo	k7c2
něj	on	k3xPp3gNnSc2
skupinu	skupina	k1gFnSc4
levičáckých	levičácký	k2eAgMnPc2d1
mladobucharců	mladobucharce	k1gMnPc2
vedenou	vedený	k2eAgFnSc4d1
Fajzullou	Fajzullá	k1gFnSc4
Chodžajevem	Chodžajev	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
Buchary	buchar	k1gInPc1
však	však	k8xC
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
po	po	k7c6
čase	čas	k1gInSc6
drancování	drancování	k1gNnSc2
vyhnali	vyhnat	k5eAaPmAgMnP
a	a	k8xC
emír	emír	k1gMnSc1
získal	získat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
trůn	trůn	k1gInSc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Chivském	Chivský	k2eAgInSc6d1
chanátu	chanát	k1gInSc6
svrhl	svrhnout	k5eAaPmAgMnS
basmačský	basmačský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Džunajd	Džunajd	k1gMnSc1
chán	chán	k1gMnSc1
ruskou	ruský	k2eAgFnSc4d1
loutku	loutka	k1gFnSc4
a	a	k8xC
potlačil	potlačit	k5eAaPmAgMnS
modernizační	modernizační	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
levičáckých	levičácký	k2eAgInPc2d1
mladochivanců	mladochivanec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
fáze	fáze	k1gFnSc1
revolty	revolta	k1gFnSc2
ve	v	k7c6
Ferganské	Ferganský	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
</s>
<s>
Duchovenstvo	duchovenstvo	k1gNnSc1
z	z	k7c2
Ferganské	Ferganský	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
uznalo	uznat	k5eAaPmAgNnS
Ergaš	Ergaš	k1gInSc4
bejovy	bejův	k2eAgInPc1d1
nároky	nárok	k1gInPc1
na	na	k7c6
velení	velení	k1gNnSc6
armádě	armáda	k1gFnSc3
věřících	věřící	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
stál	stát	k5eAaImAgInS
záhy	záhy	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
značně	značně	k6eAd1
velkého	velký	k2eAgNnSc2d1
bojového	bojový	k2eAgNnSc2d1
uskupení	uskupení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
znárodňovací	znárodňovací	k2eAgFnSc1d1
tažení	tažení	k1gNnSc1
vedená	vedený	k2eAgFnSc1d1
z	z	k7c2
Taškentu	Taškent	k1gInSc2
způsobila	způsobit	k5eAaPmAgFnS
zhroucení	zhroucení	k1gNnSc4
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
ve	v	k7c6
Ferganské	Ferganský	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
zavládl	zavládnout	k5eAaPmAgInS
vzhledem	vzhledem	k7c3
k	k	k7c3
neexistujícímu	existující	k2eNgInSc3d1
dovozu	dovoz	k1gInSc3
obilí	obilí	k1gNnSc2
hladomor	hladomor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
vedlo	vést	k5eAaImAgNnS
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
přidávali	přidávat	k5eAaImAgMnP
k	k	k7c3
basmačům	basmač	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taškentský	taškentský	k2eAgInSc1d1
sovět	sovět	k1gInSc1
nedokázal	dokázat	k5eNaPmAgInS
povstání	povstání	k1gNnSc4
potlačit	potlačit	k5eAaPmF
<g/>
,	,	kIx,
takže	takže	k8xS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
ovládly	ovládnout	k5eAaPmAgFnP
decentralizované	decentralizovaný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
bojovníků	bojovník	k1gMnPc2
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
asi	asi	k9
20	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
Ferganu	Fergan	k1gInSc2
a	a	k8xC
oblast	oblast	k1gFnSc4
kolem	kolem	k7c2
Taškentu	Taškent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Ergaš	Ergaš	k1gMnSc1
bejem	bej	k1gMnSc7
soupeřili	soupeřit	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Madamin	Madamin	k2eAgMnSc1d1
bej	bej	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
podporovaly	podporovat	k5eAaImAgFnP
umírněnější	umírněný	k2eAgFnPc1d2
muslimské	muslimský	k2eAgFnPc1d1
frakce	frakce	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
Ergaš	Ergaš	k1gMnSc1
bej	bej	k1gMnSc1
si	se	k3xPyFc3
na	na	k7c6
shromáždění	shromáždění	k1gNnSc6
v	v	k7c6
březnu	březen	k1gInSc6
1919	#num#	k4
podržel	podržet	k5eAaPmAgMnS
formální	formální	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
ožehavé	ožehavý	k2eAgFnSc3d1
vojenské	vojenský	k2eAgFnSc3d1
pozici	pozice	k1gFnSc3
Taškentského	taškentský	k2eAgInSc2d1
sovětu	sovět	k1gInSc2
nechali	nechat	k5eAaPmAgMnP
bolševici	bolševik	k1gMnPc1
ruské	ruský	k2eAgMnPc4d1
osadníky	osadník	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
zorganizovali	zorganizovat	k5eAaPmAgMnP
vlastní	vlastní	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
vytvořením	vytvoření	k1gNnSc7
takzvané	takzvaný	k2eAgFnSc2d1
Ferganské	Ferganský	k2eAgFnSc2d1
rolnické	rolnický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
basmačské	basmačský	k2eAgInPc1d1
útoky	útok	k1gInPc1
často	často	k6eAd1
vyvolávaly	vyvolávat	k5eAaImAgFnP
brutální	brutální	k2eAgFnPc1d1
odvetné	odvetný	k2eAgFnPc1d1
akce	akce	k1gFnPc1
spojených	spojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Rudé	rudý	k2eAgFnSc2d1
a	a	k8xC
rolnické	rolnický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tvrdá	tvrdý	k2eAgFnSc1d1
politika	politika	k1gFnSc1
válečného	válečný	k2eAgInSc2d1
komunismu	komunismus	k1gInSc2
však	však	k9
způsobila	způsobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
rolníci	rolník	k1gMnPc1
na	na	k7c4
taškentský	taškentský	k2eAgInSc4d1
sovět	sovět	k1gInSc4
nevražili	nevražit	k5eAaImAgMnP,k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1919	#num#	k4
zformoval	zformovat	k5eAaPmAgMnS
Madamin	Madamin	k2eAgMnSc1d1
bej	bej	k1gMnSc1
koalici	koalice	k1gFnSc4
s	s	k7c7
osadníky	osadník	k1gMnPc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
o	o	k7c6
neútočení	neútočení	k1gNnSc6
a	a	k8xC
koaliční	koaliční	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noví	nový	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
plánovali	plánovat	k5eAaImAgMnP
založit	založit	k5eAaPmF
rusko-muslimský	rusko-muslimský	k2eAgInSc4d1
stát	stát	k1gInSc4
s	s	k7c7
dohodami	dohoda	k1gFnPc7
o	o	k7c6
společné	společný	k2eAgFnSc6d1
účasti	účast	k1gFnSc6
na	na	k7c6
vládě	vláda	k1gFnSc6
a	a	k8xC
o	o	k7c6
kulturních	kulturní	k2eAgNnPc6d1
právech	právo	k1gNnPc6
pro	pro	k7c4
obě	dva	k4xCgFnPc4
skupiny	skupina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Spory	spora	k1gFnSc2
ohledně	ohledně	k7c2
islámské	islámský	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
basmačů	basmač	k1gInPc2
vedly	vést	k5eAaImAgFnP
nicméně	nicméně	k8xC
k	k	k7c3
rozpadu	rozpad	k1gInSc3
koalice	koalice	k1gFnSc2
a	a	k8xC
jak	jak	k6eAd1
Madamin	Madamin	k2eAgMnSc1d1
bej	bej	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
tak	tak	k6eAd1
osadníci	osadník	k1gMnPc1
utrpěli	utrpět	k5eAaPmAgMnP
porážky	porážka	k1gFnPc4
od	od	k7c2
rudé	rudý	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
muslimských	muslimský	k2eAgMnPc2d1
Povolžských	povolžský	k2eAgMnPc2d1
Tatarů	Tatar	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Krutá	krutý	k2eAgFnSc1d1
zima	zima	k1gFnSc1
1919	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
obyvatelstvo	obyvatelstvo	k1gNnSc4
Ferganské	Ferganský	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
značně	značně	k6eAd1
vyčerpala	vyčerpat	k5eAaPmAgFnS
a	a	k8xC
Madamin	Madamin	k2eAgMnSc1d1
bej	bej	k1gMnSc1
přeběhl	přeběhnout	k5eAaPmAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
na	na	k7c4
sovětskou	sovětský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Mezitím	mezitím	k6eAd1
začal	začít	k5eAaPmAgInS
díky	díky	k7c3
liberálnější	liberální	k2eAgFnSc3d2
Nové	Nové	k2eAgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
politice	politika	k1gFnSc3
(	(	kIx(
<g/>
NEP	Nep	k1gInSc1
<g/>
)	)	kIx)
hladomor	hladomor	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ustupovat	ustupovat	k5eAaImF
a	a	k8xC
pozemková	pozemkový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
amnestií	amnestie	k1gFnSc7
uklidnily	uklidnit	k5eAaPmAgFnP
obyvatelstvo	obyvatelstvo	k1gNnSc4
kotliny	kotlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
basmači	basmač	k1gMnPc1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
nejzalidněnějšími	zalidněný	k2eAgFnPc7d3
oblastmi	oblast	k1gFnPc7
a	a	k8xC
stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
bezpečnější	bezpečný	k2eAgNnSc4d2
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Klid	klid	k1gInSc1
a	a	k8xC
pořádek	pořádek	k1gInSc1
v	v	k7c6
kotlině	kotlina	k1gFnSc6
však	však	k9
netrvaly	trvat	k5eNaImAgInP
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
léta	léto	k1gNnSc2
1920	#num#	k4
se	s	k7c7
sověty	sovět	k1gInPc7
cítily	cítit	k5eAaImAgFnP
již	již	k9
natolik	natolik	k6eAd1
bezpečně	bezpečně	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
začaly	začít	k5eAaPmAgFnP
opětovně	opětovně	k6eAd1
rekvírovat	rekvírovat	k5eAaBmF
potraviny	potravina	k1gFnPc4
a	a	k8xC
odvádět	odvádět	k5eAaImF
muslimské	muslimský	k2eAgFnSc3d1
brance	branka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovu	znovu	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
rozhořelo	rozhořet	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
a	a	k8xC
vyrojily	vyrojit	k5eAaPmAgFnP
se	se	k3xPyFc4
nové	nový	k2eAgFnPc1d1
basmačské	basmačský	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
bojující	bojující	k2eAgFnPc1d1
pod	pod	k7c7
praporem	prapor	k1gInSc7
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Obnovený	obnovený	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
měl	mít	k5eAaImAgInS
za	za	k7c4
následek	následek	k1gInSc4
rozšíření	rozšíření	k1gNnSc2
basmačského	basmačský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
Turkestánu	Turkestán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Basmači	Basmač	k1gMnPc1
v	v	k7c6
Chivě	Chiva	k1gFnSc6
a	a	k8xC
v	v	k7c4
Buchaře	Buchař	k1gMnPc4
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1920	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Chivu	Chiv	k1gInSc2
a	a	k8xC
nastolila	nastolit	k5eAaPmAgFnS
provizorní	provizorní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
mladochivanců	mladochivanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Džunajd	Džunajd	k1gMnSc1
chán	chán	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
následovníky	následovník	k1gMnPc7
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
pustiny	pustina	k1gFnSc2
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
zrodilo	zrodit	k5eAaPmAgNnS
basmačské	basmačský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
v	v	k7c6
Chórezmské	Chórezmský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
sesadili	sesadit	k5eAaPmAgMnP
rudí	rudý	k1gMnPc1
vládu	vláda	k1gFnSc4
mladochivanců	mladochivanec	k1gMnPc2
a	a	k8xC
muslimští	muslimský	k2eAgMnPc1d1
nacionalisté	nacionalista	k1gMnPc1
uprchli	uprchnout	k5eAaPmAgMnP
k	k	k7c3
Džunajd	Džunajd	k1gInSc1
chánovi	chán	k1gMnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
značně	značně	k6eAd1
posílili	posílit	k5eAaPmAgMnP
řady	řada	k1gFnPc4
jeho	jeho	k3xOp3gMnPc2
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgMnS
bucharský	bucharský	k2eAgMnSc1d1
emír	emír	k1gMnSc1
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
sesazen	sesadit	k5eAaPmNgMnS
<g/>
,	,	kIx,
když	když	k8xS
jednotky	jednotka	k1gFnPc1
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
dobyly	dobýt	k5eAaPmAgFnP
Bucharu	buchar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
svého	svůj	k3xOyFgInSc2
afghánského	afghánský	k2eAgInSc2d1
exilu	exil	k1gInSc2
řídil	řídit	k5eAaImAgMnS
pak	pak	k6eAd1
emír	emír	k1gMnSc1
basmačské	basmačský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
podporované	podporovaný	k2eAgNnSc1d1
rozhořčeným	rozhořčený	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
a	a	k8xC
duchovními	duchovní	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basmačům	Basmač	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
bojovali	bojovat	k5eAaImAgMnP
v	v	k7c6
emírově	emírův	k2eAgInSc6d1
zájmu	zájem	k1gInSc6
<g/>
,	,	kIx,
velel	velet	k5eAaImAgMnS
kmenový	kmenový	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Ibrahim	Ibrahim	k1gMnSc1
bek	bek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Basmačské	Basmačský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
vedly	vést	k5eAaImAgFnP
svůj	svůj	k3xOyFgInSc4
boj	boj	k1gInSc4
v	v	k7c6
Chivě	Chiva	k1gFnSc6
a	a	k8xC
Buchaře	Buchař	k1gMnPc4
po	po	k7c4
delší	dlouhý	k2eAgInSc4d2
čas	čas	k1gInSc4
tak	tak	k6eAd1
úspěšně	úspěšně	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
povstání	povstání	k1gNnSc2
začalo	začít	k5eAaPmAgNnS
šířit	šířit	k5eAaImF
i	i	k9
do	do	k7c2
Kazachstánu	Kazachstán	k1gInSc2
a	a	k8xC
na	na	k7c6
území	území	k1gNnSc6
obývaná	obývaný	k2eAgFnSc1d1
Tádžiky	Tádžik	k1gMnPc4
a	a	k8xC
Turkmeny	Turkmen	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
a	a	k8xC
vrchol	vrchol	k1gInSc1
basmačského	basmačský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Jednání	jednání	k1gNnSc1
s	s	k7c7
basmači	basmač	k1gMnPc7
<g/>
,	,	kIx,
Fergana	Fergana	k1gFnSc1
1921	#num#	k4
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1921	#num#	k4
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Buchary	buchar	k1gInPc7
generál	generál	k1gMnSc1
İ	İ	k1gMnSc1
Enver	Enver	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
osmanský	osmanský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
války	válka	k1gFnSc2
a	a	k8xC
spolu	spolu	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spoluvládci	spoluvládce	k1gMnPc7
<g/>
,	,	kIx,
pašou	paša	k1gMnSc7
Talatem	Talat	k1gMnSc7
a	a	k8xC
Džamalem	Džamal	k1gMnSc7
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
strůjce	strůjce	k1gMnSc1
arménské	arménský	k2eAgFnSc2d1
genocidy	genocida	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pomohl	pomoct	k5eAaPmAgMnS
válečnému	válečný	k2eAgNnSc3d1
úsilí	úsilí	k1gNnSc3
sovětů	sovět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
však	však	k9
uprchl	uprchnout	k5eAaPmAgMnS
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
basmačských	basmačský	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centralizoval	centralizovat	k5eAaBmAgMnS
a	a	k8xC
znovuoživil	znovuoživit	k5eAaImAgMnS,k5eAaPmAgMnS
celé	celý	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
hodlal	hodlat	k5eAaImAgMnS
vytvořit	vytvořit	k5eAaPmF
všeturkickou	všeturkický	k2eAgFnSc4d1
konfederaci	konfederace	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
zahrnovala	zahrnovat	k5eAaImAgFnS
celou	celá	k1gFnSc4
Střední	střední	k2eAgFnSc3d1
Asii	Asie	k1gFnSc3
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
i	i	k9
Anatólii	Anatólie	k1gFnSc4
a	a	k8xC
čínská	čínský	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gNnSc1
volání	volání	k1gNnSc1
po	po	k7c6
džihádu	džihád	k1gInSc6
vyvolalo	vyvolat	k5eAaPmAgNnS
značnou	značný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
mu	on	k3xPp3gMnSc3
umožnila	umožnit	k5eAaPmAgFnS
přeměnit	přeměnit	k5eAaPmF
basmačské	basmačský	k2eAgInPc4d1
partyzánské	partyzánský	k2eAgInPc4d1
bojové	bojový	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
v	v	k7c4
armádu	armáda	k1gFnSc4
čítající	čítající	k2eAgFnSc4d1
16	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
kontrolovali	kontrolovat	k5eAaImAgMnP
již	jenž	k3xRgMnPc1
basmači	basmač	k1gMnPc1
významnou	významný	k2eAgFnSc4d1
část	část	k1gFnSc4
Bucharské	bucharský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
včetně	včetně	k7c2
Samarkandu	Samarkand	k1gInSc2
a	a	k8xC
Dušanbe	Dušanb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dunganský	Dunganský	k2eAgInSc1d1
muslim	muslim	k1gMnSc1
Magazy	Magaz	k1gInPc7
Masanči	Masanči	k1gMnSc1
mezitím	mezitím	k6eAd1
dával	dávat	k5eAaImAgMnS
dohromady	dohromady	k6eAd1
dunganský	dunganský	k2eAgInSc4d1
jízdní	jízdní	k2eAgInSc4d1
pluk	pluk	k1gInSc4
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
basmačům	basmač	k1gInPc3
na	na	k7c6
straně	strana	k1gFnSc6
sovětů	sovět	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Porážka	porážka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1
Střední	střední	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
</s>
<s>
V	v	k7c6
obavách	obava	k1gFnPc6
o	o	k7c4
úplnou	úplný	k2eAgFnSc4d1
ztrátu	ztráta	k1gFnSc4
Turkestánu	Turkestán	k1gInSc2
použily	použít	k5eAaPmAgInP
sovětské	sovětský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
k	k	k7c3
rozdrcení	rozdrcení	k1gNnSc3
povstání	povstání	k1gNnSc2
opět	opět	k6eAd1
dvojí	dvojí	k4xRgFnSc4
strategii	strategie	k1gFnSc4
<g/>
:	:	kIx,
politickou	politický	k2eAgFnSc4d1
smířlivost	smířlivost	k1gFnSc4
a	a	k8xC
kulturní	kulturní	k2eAgInPc4d1
ústupky	ústupek	k1gInPc4
spolu	spolu	k6eAd1
s	s	k7c7
ohromnou	ohromný	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
kulturním	kulturní	k2eAgMnPc3d1
ústupkům	ústupek	k1gInPc3
patřilo	patřit	k5eAaImAgNnS
znovuzavedení	znovuzavedení	k1gNnSc1
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
šaría	šaríum	k1gNnSc2
a	a	k8xC
obnovení	obnovení	k1gNnSc2
medres	medresa	k1gFnPc2
<g/>
,	,	kIx,
škol	škola	k1gFnPc2
koránu	korán	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
území	území	k1gNnSc1
vakf	vakf	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Moskva	Moskva	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
využít	využít	k5eAaPmF
pro	pro	k7c4
boj	boj	k1gInSc4
více	hodně	k6eAd2
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
vytvářením	vytváření	k1gNnSc7
dobrovolnických	dobrovolnický	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
zeměbrany	zeměbrana	k1gFnSc2
složených	složený	k2eAgFnPc2d1
z	z	k7c2
muslimských	muslimský	k2eAgMnPc2d1
rolníků	rolník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odhadů	odhad	k1gInPc2
tvořili	tvořit	k5eAaImAgMnP
muslimové	muslim	k1gMnPc1
15	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
%	%	kIx~
bojovníků	bojovník	k1gMnPc2
sovětských	sovětský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Sověti	Sovět	k1gMnPc1
se	se	k3xPyFc4
spoléhali	spoléhat	k5eAaImAgMnP
především	především	k9
na	na	k7c4
tisíce	tisíc	k4xCgInPc4
rudoarmějců	rudoarmějec	k1gMnPc2
–	–	k?
veteránů	veterán	k1gMnPc2
z	z	k7c2
ruské	ruský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nyní	nyní	k6eAd1
měli	mít	k5eAaImAgMnP
i	i	k9
leteckou	letecký	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategie	strategie	k1gFnSc1
ústupků	ústupek	k1gInPc2
a	a	k8xC
náletů	nálet	k1gInPc2
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
jako	jako	k9
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
květnu	květen	k1gInSc6
1922	#num#	k4
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
nabídku	nabídka	k1gFnSc4
míru	míra	k1gFnSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
ultimátum	ultimátum	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
vyžadoval	vyžadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
veškeré	veškerý	k3xTgFnPc1
jednotky	jednotka	k1gFnPc1
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
stáhly	stáhnout	k5eAaPmAgInP
do	do	k7c2
15	#num#	k4
dní	den	k1gInPc2
z	z	k7c2
Turkestánu	Turkestán	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
Moskva	Moskva	k1gFnSc1
na	na	k7c6
konfrontaci	konfrontace	k1gFnSc6
dobře	dobře	k6eAd1
připravená	připravený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1922	#num#	k4
porazily	porazit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgFnPc3
velel	velet	k5eAaImAgInS
plukovník	plukovník	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Kakurin	Kakurin	k1gInSc4
<g/>
,	,	kIx,
basmačské	basmačský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kafrunu	Kafrun	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
začala	začít	k5eAaPmAgFnS
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vytlačovat	vytlačovat	k5eAaImF
povstalce	povstalec	k1gMnSc2
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
přitom	přitom	k6eAd1
získat	získat	k5eAaPmF
zpět	zpět	k6eAd1
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enver	Enver	k1gMnSc1
Paša	paša	k1gMnSc1
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
při	při	k7c6
posledním	poslední	k2eAgInSc6d1
zoufalém	zoufalý	k2eAgInSc6d1
výpadu	výpad	k1gInSc6
jízdy	jízda	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1922	#num#	k4
poblíž	poblíž	k7c2
Balčuanu	Balčuan	k1gInSc2
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
Tádžikistánu	Tádžikistán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Selim	Selim	k?
Paša	paša	k1gMnSc1
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
nucen	nutit	k5eAaImNgMnS
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
červenec	červenec	k1gInSc1
<g/>
–	–	k?
<g/>
srpen	srpen	k1gInSc4
1923	#num#	k4
vytlačily	vytlačit	k5eAaPmAgFnP
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
mohutné	mohutný	k2eAgFnSc2d1
ofenzivy	ofenziva	k1gFnSc2
basmače	basmač	k1gInSc2
z	z	k7c2
tádžického	tádžický	k2eAgInSc2d1
Garmu	Garm	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Ferganské	Ferganský	k2eAgFnSc6d1
kotlině	kotlina	k1gFnSc6
zůstali	zůstat	k5eAaPmAgMnP
basmačští	basmačský	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgMnPc3
bojovníkům	bojovník	k1gMnPc3
velel	velet	k5eAaImAgInS
vůdce	vůdce	k1gMnPc4
Kurširmat	Kurširmat	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
obnovil	obnovit	k5eAaPmAgInS
povstání	povstání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
údajů	údaj	k1gInPc2
britské	britský	k2eAgFnSc2d1
výzvědné	výzvědný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
se	se	k3xPyFc4
počet	počet	k1gInSc1
bojovníků	bojovník	k1gMnPc2
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
Kurširmat	Kurširmat	k1gInSc1
velel	velet	k5eAaImAgInS
<g/>
,	,	kIx,
pohyboval	pohybovat	k5eAaImAgInS
mezi	mezi	k7c4
5000	#num#	k4
až	až	k9
6000	#num#	k4
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
letech	let	k1gInPc6
bojů	boj	k1gInPc2
nicméně	nicméně	k8xC
podpora	podpora	k1gFnSc1
basmačského	basmačský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
mezi	mezi	k7c7
lidem	lid	k1gInSc7
skomírala	skomírat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolníci	rolník	k1gMnPc1
se	se	k3xPyFc4
chtěli	chtít	k5eAaImAgMnP
vrátit	vrátit	k5eAaPmF
k	k	k7c3
práci	práce	k1gFnSc3
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
díky	díky	k7c3
sovětské	sovětský	k2eAgFnSc3d1
politice	politika	k1gFnSc3
stal	stát	k5eAaPmAgInS
život	život	k1gInSc1
v	v	k7c6
Turkestánu	Turkestán	k1gInSc6
opět	opět	k6eAd1
snesitelný	snesitelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
Kurširmatových	Kurširmatův	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
se	se	k3xPyFc4
scvrkl	scvrknout	k5eAaPmAgInS
na	na	k7c4
pouhých	pouhý	k2eAgNnPc2d1
asi	asi	k9
2000	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
mnozí	mnohý	k2eAgMnPc1d1
se	se	k3xPyFc4
uchýlili	uchýlit	k5eAaPmAgMnP
k	k	k7c3
banditství	banditství	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgInSc1
Kurširmat	Kurširmat	k1gInSc1
záhy	záhy	k6eAd1
uprchl	uprchnout	k5eAaPmAgInS
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Turkestán	Turkestán	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vyčerpán	vyčerpán	k2eAgInSc4d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tádžických	tádžický	k2eAgNnPc2d1
území	území	k1gNnPc2
uprchlo	uprchnout	k5eAaPmAgNnS
na	na	k7c4
200	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
zanechalo	zanechat	k5eAaPmAgNnS
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
orné	orný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
opuštěné	opuštěný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Ferganě	Fergan	k1gInSc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
menší	malý	k2eAgFnPc4d2
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přeshraniční	přeshraniční	k2eAgFnPc1d1
operace	operace	k1gFnPc1
v	v	k7c6
severním	severní	k2eAgInSc6d1
Afghánistánu	Afghánistán	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1929	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
během	během	k7c2
afghánské	afghánský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
moci	moc	k1gFnSc3
<g/>
,	,	kIx,
povolil	povolit	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
afghánský	afghánský	k2eAgMnSc1d1
král	král	k1gMnSc1
Habibullā	Habibullā	k1gMnSc1
Kalakā	Kalakā	k1gFnSc6
basmačským	basmačský	k2eAgMnPc3d1
povstalcům	povstalec	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vedli	vést	k5eAaImAgMnP
svůj	svůj	k3xOyFgInSc4
boj	boj	k1gInSc4
ze	z	k7c2
severního	severní	k2eAgInSc2d1
Afghánistánu	Afghánistán	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
usazovali	usazovat	k5eAaImAgMnP
v	v	k7c6
městech	město	k1gNnPc6
jako	jako	k8xC,k8xS
Chánabád	Chánabáda	k1gFnPc2
<g/>
,	,	kIx,
Talikán	Talikán	k1gMnSc1
a	a	k8xC
Fajzabád	Fajzabáda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
března	březen	k1gInSc2
1929	#num#	k4
provedli	provést	k5eAaPmAgMnP
tito	tento	k3xDgMnPc1
bojovníci	bojovník	k1gMnPc1
dva	dva	k4xCgInPc4
nájezdy	nájezd	k1gInPc4
na	na	k7c4
sovětské	sovětský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
do	do	k7c2
oblasti	oblast	k1gFnSc2
kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
Amudarja	Amudarjum	k1gNnSc2
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Kulobu	Kuloba	k1gFnSc4
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
vedl	vést	k5eAaImAgMnS
Kerim	Kerim	k1gMnSc1
Berdoi	Berdo	k1gFnSc2
a	a	k8xC
zúčastnilo	zúčastnit	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc4
100	#num#	k4
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
nájezdy	nájezd	k1gInPc1
byly	být	k5eAaImAgInP
odraženy	odrazit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
se	se	k3xPyFc4
vedlo	vést	k5eAaImAgNnS
i	i	k9
nájezdům	nájezd	k1gInPc3
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
však	však	k9
basmačtí	basmacký	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
úspěšně	úspěšně	k6eAd1
překročili	překročit	k5eAaPmAgMnP
řeku	řeka	k1gFnSc4
Pandž	Pandž	k1gFnSc4
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
město	město	k1gNnSc4
Togmaj	Togmaj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
Dzafr	Dzafr	k1gInSc4
a	a	k8xC
Kevron	Kevron	k1gInSc4
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
pak	pak	k6eAd1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
Kalaj-Chumb	Kalaj-Chumb	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
Gašion	Gašion	k1gInSc1
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
také	také	k9
Vanč	Vanč	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
sověti	sovět	k5eAaImF,k5eAaPmF
příštího	příští	k2eAgInSc2d1
dne	den	k1gInSc2
získali	získat	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
basmači	basmač	k1gMnPc1
Kalaj-Liabob	Kalaj-Liaboba	k1gFnPc2
a	a	k8xC
následujícího	následující	k2eAgInSc2d1
dne	den	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
zuřivé	zuřivý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
,	,	kIx,
Nimči	Nimč	k1gFnSc6
<g/>
,	,	kIx,
35	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Garmu	Garm	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
dubnem	duben	k1gInSc7
překročily	překročit	k5eAaPmAgFnP
sovětské	sovětský	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
další	další	k2eAgInPc4d1
basmačské	basmačský	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
skupina	skupina	k1gFnSc1
postoupila	postoupit	k5eAaPmAgFnS
až	až	k9
k	k	k7c3
Tavildaře	Tavildara	k1gFnSc3
<g/>
,	,	kIx,
načež	načež	k6eAd1
byla	být	k5eAaImAgFnS
tamějšími	tamější	k2eAgInPc7d1
strážními	strážní	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
zatlačena	zatlačen	k2eAgFnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
basmači	basmač	k1gMnPc1
Garm	Garmo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
sověti	sovět	k5eAaImF,k5eAaPmF
ještě	ještě	k9
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
nebo	nebo	k8xC
dne	den	k1gInSc2
následujícího	následující	k2eAgInSc2d1
dobyli	dobýt	k5eAaPmAgMnP
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
začala	začít	k5eAaPmAgFnS
mohutná	mohutný	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
protiofenziva	protiofenziva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
bylo	být	k5eAaImAgNnS
osvobozeno	osvobodit	k5eAaPmNgNnS
město	město	k1gNnSc4
Kalaj-Liabob	Kalaj-Liaboba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
květnu	květno	k1gNnSc6
ustoupily	ustoupit	k5eAaPmAgFnP
poslední	poslední	k2eAgFnPc1d1
basmačské	basmačský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
zpět	zpět	k6eAd1
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
období	období	k1gNnSc6
květen	květena	k1gFnPc2
<g/>
–	–	k?
<g/>
červen	červen	k1gInSc1
1929	#num#	k4
vtrhly	vtrhnout	k5eAaPmAgFnP
sovětské	sovětský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
do	do	k7c2
Afghánistánu	Afghánistán	k1gInSc2
a	a	k8xC
podstatně	podstatně	k6eAd1
přitom	přitom	k6eAd1
snížily	snížit	k5eAaPmAgFnP
schopnost	schopnost	k1gFnSc4
basmačských	basmačský	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
opětovně	opětovně	k6eAd1
zaútočit	zaútočit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakter	charakter	k1gInSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Profesor	profesor	k1gMnSc1
Michael	Michael	k1gMnSc1
Rywkin	Rywkin	k1gMnSc1
charakterizoval	charakterizovat	k5eAaBmAgMnS
basmačské	basmačský	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
jako	jako	k8xC,k8xS
národně	národně	k6eAd1
osvobozenecký	osvobozenecký	k2eAgInSc4d1
boj	boj	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
ukončit	ukončit	k5eAaPmF
cizí	cizí	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
středoasijskou	středoasijský	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
tehdy	tehdy	k6eAd1
známou	známá	k1gFnSc4
jako	jako	k8xS,k8xC
Turkestán	Turkestán	k1gInSc4
a	a	k8xC
současně	současně	k6eAd1
nad	nad	k7c7
chivským	chivský	k2eAgInSc7d1
a	a	k8xC
bucharským	bucharský	k2eAgInSc7d1
protektorátem	protektorát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
basmacı	basmacı	k?
se	se	k3xPyFc4
v	v	k7c6
turkických	turkický	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
údajně	údajně	k6eAd1
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
banditům	bandita	k1gMnPc3
či	či	k8xC
záškodníkům	záškodník	k1gMnPc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byly	být	k5eAaImAgInP
bandy	band	k1gInPc1
zlodějů	zloděj	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ve	v	k7c6
zdejších	zdejší	k2eAgFnPc6d1
končinách	končina	k1gFnPc6
přepadávaly	přepadávat	k5eAaImAgFnP
karavany	karavana	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
bývalo	bývat	k5eAaImAgNnS
často	často	k6eAd1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
hanlivý	hanlivý	k2eAgInSc4d1
význam	význam	k1gInSc4
používáno	používán	k2eAgNnSc1d1
v	v	k7c6
sovětských	sovětský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sověti	Sovět	k1gMnPc1
popisovali	popisovat	k5eAaImAgMnP
hnutí	hnutí	k1gNnSc4
jako	jako	k8xC,k8xS
lupiče	lupič	k1gMnPc4
podnícené	podnícený	k2eAgMnPc4d1
islámským	islámský	k2eAgInSc7d1
fundamentalismem	fundamentalismus	k1gInSc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
vedou	vést	k5eAaImIp3nP
kontrarevoluční	kontrarevoluční	k2eAgFnSc4d1
válku	válka	k1gFnSc4
za	za	k7c2
podpory	podpora	k1gFnSc2
britských	britský	k2eAgMnPc2d1
agentů	agent	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tvořili	tvořit	k5eAaImAgMnP
basmači	basmač	k1gMnPc1
velmi	velmi	k6eAd1
rozmanitou	rozmanitý	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
se	se	k3xPyFc4
dostávalo	dostávat	k5eAaImAgNnS
nevelké	velký	k2eNgFnSc3d1
zahraniční	zahraniční	k2eAgFnSc3d1
pomoci	pomoc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
nepohlížely	pohlížet	k5eNaImAgFnP
na	na	k7c4
basmače	basmač	k1gMnPc4
příliš	příliš	k6eAd1
příznivě	příznivě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
panturkistickým	panturkistický	k2eAgInPc3d1
a	a	k8xC
panislamistickým	panislamistický	k2eAgInPc3d1
názorům	názor	k1gInPc3
připisovaným	připisovaný	k2eAgInPc3d1
některým	některý	k3yIgMnPc3
z	z	k7c2
jejich	jejich	k3xOp3gMnPc2
vůdců	vůdce	k1gMnPc2
v	v	k7c6
nich	on	k3xPp3gFnPc6
viděly	vidět	k5eAaImAgInP
potenciální	potenciální	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
basmačské	basmačský	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
však	však	k9
dostávaly	dostávat	k5eAaImAgFnP
podporu	podpora	k1gFnSc4
od	od	k7c2
britské	britský	k2eAgFnSc2d1
a	a	k8xC
turecké	turecký	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sověti	Sovět	k1gMnPc1
proto	proto	k8xC
vytvářeli	vytvářet	k5eAaImAgMnP
speciální	speciální	k2eAgFnPc4d1
maskované	maskovaný	k2eAgFnPc4d1
vojenské	vojenský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
těmto	tento	k3xDgFnPc3
dodávkám	dodávka	k1gFnPc3
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
řada	řada	k1gFnSc1
povstaleckých	povstalecký	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
byla	být	k5eAaImAgFnS
motivována	motivovat	k5eAaBmNgFnS
voláním	volání	k1gNnSc7
po	po	k7c6
džihádu	džihád	k1gInSc6
<g/>
,	,	kIx,
dostávalo	dostávat	k5eAaImAgNnS
se	se	k3xPyFc4
basmačům	basmač	k1gMnPc3
podpory	podpora	k1gFnSc2
z	z	k7c2
rozličných	rozličný	k2eAgInPc2d1
názorových	názorový	k2eAgInPc2d1
táborů	tábor	k1gInPc2
a	a	k8xC
od	od	k7c2
různých	různý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
tak	tak	k6eAd1
přitáhli	přitáhnout	k5eAaPmAgMnP
pozornost	pozornost	k1gFnSc4
umírněných	umírněný	k2eAgMnPc2d1
sunnitských	sunnitský	k2eAgMnPc2d1
džadidistů	džadidista	k1gMnPc2
<g/>
,	,	kIx,
panturkických	panturkický	k2eAgMnPc2d1
ideologů	ideolog	k1gMnPc2
či	či	k8xC
levicových	levicový	k2eAgMnPc2d1
turkestánských	turkestánský	k2eAgMnPc2d1
nacionalistů	nacionalista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Zemědělci	zemědělec	k1gMnPc1
a	a	k8xC
kočovníci	kočovník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
dlouho	dlouho	k6eAd1
čelili	čelit	k5eAaImAgMnP
ruské	ruský	k2eAgFnSc3d1
koloniální	koloniální	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
,	,	kIx,
přijímali	přijímat	k5eAaImAgMnP
s	s	k7c7
nevraživostí	nevraživost	k1gFnSc7
sovětskou	sovětský	k2eAgFnSc4d1
protiislámskou	protiislámský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
rekvírování	rekvírování	k?
potravin	potravina	k1gFnPc2
a	a	k8xC
hospodářského	hospodářský	k2eAgNnSc2d1
zvířectva	zvířectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
hlavními	hlavní	k2eAgMnPc7d1
nositeli	nositel	k1gMnPc7
bolševických	bolševický	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
v	v	k7c6
Turkestánu	Turkestán	k1gInSc6
byli	být	k5eAaImAgMnP
především	především	k9
ruští	ruský	k2eAgMnPc1d1
kolonisté	kolonista	k1gMnPc1
v	v	k7c6
Taškentu	Taškent	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
způsobovala	způsobovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
carská	carský	k2eAgFnSc1d1
a	a	k8xC
sovětská	sovětský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
připadaly	připadat	k5eAaImAgInP,k5eAaPmAgInP
zdejším	zdejší	k2eAgMnPc3d1
obyvatelům	obyvatel	k1gMnPc3
totožné	totožný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řadách	řada	k1gFnPc6
basmačů	basmač	k1gMnPc2
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k6eAd1
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zůstali	zůstat	k5eAaPmAgMnP
díky	díky	k7c3
špatným	špatný	k2eAgFnPc3d1
ekonomickým	ekonomický	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
bez	bez	k7c2
práce	práce	k1gFnSc2
a	a	k8xC
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
pociťovali	pociťovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
ohrožen	ohrozit	k5eAaPmNgInS
jejich	jejich	k3xOp3gInSc1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgMnPc1
basmačtí	basmacký	k2eAgMnPc1d1
bojovníci	bojovník	k1gMnPc1
byli	být	k5eAaImAgMnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
napovídá	napovídat	k5eAaBmIp3nS
jejich	jejich	k3xOp3gInSc1
název	název	k1gInSc1
<g/>
,	,	kIx,
bandité	bandita	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
hnutí	hnutí	k1gNnSc1
posléze	posléze	k6eAd1
začalo	začít	k5eAaPmAgNnS
rozpadat	rozpadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
k	k	k7c3
lupičství	lupičství	k1gNnSc3
se	se	k3xPyFc4
opět	opět	k6eAd1
vraceli	vracet	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
bylo	být	k5eAaImAgNnS
hnutí	hnutí	k1gNnSc4
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
momentech	moment	k1gInPc6
relativně	relativně	k6eAd1
jednotné	jednotný	k2eAgFnPc1d1
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
trpělo	trpět	k5eAaImAgNnS
značnou	značný	k2eAgFnSc7d1
rozdrobeností	rozdrobenost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupeření	soupeření	k1gNnSc1
mezi	mezi	k7c7
různými	různý	k2eAgMnPc7d1
vůdci	vůdce	k1gMnPc7
s	s	k7c7
sebou	se	k3xPyFc7
přinášelo	přinášet	k5eAaImAgNnS
řadu	řada	k1gFnSc4
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
vážnější	vážní	k2eAgInPc1d2
rozpory	rozpor	k1gInPc1
vznikaly	vznikat	k5eAaImAgInP
z	z	k7c2
etnických	etnický	k2eAgInPc2d1
sporů	spor	k1gInPc2
mezi	mezi	k7c4
Kyrgyzy	Kyrgyza	k1gFnPc4
a	a	k8xC
Uzbeky	Uzbek	k1gMnPc4
nebo	nebo	k8xC
Turkmeny	Turkmen	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Následky	následek	k1gInPc1
</s>
<s>
Místní	místní	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
začali	začít	k5eAaPmAgMnP
spolupracovat	spolupracovat	k5eAaImF
se	s	k7c7
sovětskou	sovětský	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
a	a	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
Středoasijců	Středoasijce	k1gMnPc2
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
řad	řada	k1gFnPc2
sovětské	sovětský	k2eAgFnSc2d1
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
uplatňovala	uplatňovat	k5eAaImAgFnS
leninskou	leninský	k2eAgFnSc4d1
a	a	k8xC
stalinskou	stalinský	k2eAgFnSc4d1
národnostní	národnostní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
zaujali	zaujmout	k5eAaPmAgMnP
vysoká	vysoký	k2eAgNnPc4d1
místa	místo	k1gNnPc4
ve	v	k7c6
vládách	vláda	k1gFnPc6
Uzbecké	uzbecký	k2eAgFnSc2d1
<g/>
,	,	kIx,
Tádžické	tádžický	k2eAgFnSc2d1
<g/>
,	,	kIx,
Turkmenské	turkmenský	k2eAgFnSc2d1
a	a	k8xC
Kyrgyzské	kyrgyzský	k2eAgFnSc2d1
a	a	k8xC
Kazašské	kazašský	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
SSR	SSR	kA
<g/>
)	)	kIx)
a	a	k8xC
autonomní	autonomní	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
ASSR	ASSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
sovětizace	sovětizace	k1gFnSc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
se	se	k3xPyFc4
islám	islám	k1gInSc1
stal	stát	k5eAaPmAgInS
terčem	terč	k1gInSc7
protináboženských	protináboženský	k2eAgFnPc2d1
kampaní	kampaň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
většinu	většina	k1gFnSc4
mešit	mešita	k1gFnPc2
<g/>
,	,	kIx,
pronásledovala	pronásledovat	k5eAaImAgFnS
islámské	islámský	k2eAgInPc4d1
duchovní	duchovní	k2eAgInPc4d1
a	a	k8xC
zaměřovala	zaměřovat	k5eAaImAgFnS
se	se	k3xPyFc4
proti	proti	k7c3
symbolům	symbol	k1gInPc3
islámské	islámský	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
zahalování	zahalování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Uzbekové	Uzbek	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
nevzdali	vzdát	k5eNaPmAgMnP
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
označováni	označován	k2eAgMnPc1d1
za	za	k7c4
nacionalisty	nacionalista	k1gMnPc4
a	a	k8xC
často	často	k6eAd1
bývali	bývat	k5eAaImAgMnP
vězněni	vězněn	k2eAgMnPc1d1
a	a	k8xC
popravováni	popravován	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalinistická	stalinistický	k2eAgFnSc1d1
kolektivizace	kolektivizace	k1gFnSc1
a	a	k8xC
industrializace	industrializace	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
kdekoli	kdekoli	k6eAd1
jinde	jinde	k6eAd1
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Působení	působení	k1gNnSc1
basmačů	basmač	k1gInPc2
po	po	k7c6
sovětském	sovětský	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
basmačské	basmačský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
jako	jako	k8xC,k8xS
politická	politický	k2eAgFnSc1d1
a	a	k8xC
vojenská	vojenský	k2eAgFnSc1d1
síla	síla	k1gFnSc1
zlikvidováno	zlikvidovat	k5eAaPmNgNnS
<g/>
,	,	kIx,
pokračovali	pokračovat	k5eAaImAgMnP
bojovníci	bojovník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
skrývali	skrývat	k5eAaImAgMnP
v	v	k7c6
hornatých	hornatý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
partyzánské	partyzánský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basmačské	Basmačský	k2eAgFnPc4d1
povstání	povstání	k1gNnSc2
zaniklo	zaniknout	k5eAaPmAgNnS
ve	v	k7c6
většině	většina	k1gFnSc6
oblastí	oblast	k1gFnPc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
šarvátky	šarvátka	k1gFnPc1
a	a	k8xC
příležitostné	příležitostný	k2eAgFnPc1d1
potyčky	potyčka	k1gFnPc1
podél	podél	k7c2
hranic	hranice	k1gFnPc2
s	s	k7c7
Afghánistánem	Afghánistán	k1gInSc7
pokračovaly	pokračovat	k5eAaImAgInP
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Džunajd	Džunajd	k1gMnSc1
chán	chán	k1gMnSc1
ohrožoval	ohrožovat	k5eAaImAgMnS
roku	rok	k1gInSc2
1926	#num#	k4
Chivu	Chiv	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
zatlačen	zatlačit	k5eAaPmNgInS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
zahnán	zahnat	k5eAaPmNgInS
do	do	k7c2
exilu	exil	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgMnPc1
přední	přední	k2eAgMnPc1d1
basmačtí	basmacký	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
<g/>
,	,	kIx,
Fajsal	Fajsal	k1gMnSc1
Maksum	Maksum	k1gInSc1
a	a	k8xC
Ibrahim	Ibrahim	k1gInSc1
bek	bek	k1gMnSc1
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
boji	boj	k1gInSc6
ze	z	k7c2
severního	severní	k2eAgInSc2d1
Afghánistánu	Afghánistán	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
provedli	provést	k5eAaPmAgMnP
řadu	řada	k1gFnSc4
nájezdů	nájezd	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
Tádžické	tádžický	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ibrahim	Ibrahim	k1gMnSc1
Bek	bek	k1gMnSc1
nakrátko	nakrátko	k6eAd1
obrodil	obrodit	k5eAaPmAgMnS
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
zdvihl	zdvihnout	k5eAaPmAgInS
odpor	odpor	k1gInSc1
způsobený	způsobený	k2eAgInSc1d1
násilnou	násilný	k2eAgFnSc7d1
kolektivizací	kolektivizace	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zabrzdit	zabrzdit	k5eAaPmF
v	v	k7c6
Turkmenistánu	Turkmenistán	k1gInSc2
tyto	tento	k3xDgFnPc1
snahy	snaha	k1gFnPc1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
však	však	k9
byl	být	k5eAaImAgInS
polapen	polapen	k2eAgInSc1d1
a	a	k8xC
popraven	popraven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
poté	poté	k6eAd1
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
odeznělo	odeznět	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
poslední	poslední	k2eAgFnSc3d1
velké	velký	k2eAgFnSc3d1
basmačské	basmačský	k2eAgFnSc3d1
akci	akce	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1933	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
poušti	poušť	k1gFnSc6
Karakum	Karakum	k1gInSc4
rozdrceny	rozdrcen	k2eAgFnPc1d1
Džunajd	Džunajd	k1gMnSc1
chánovy	chánův	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basmačské	Basmačský	k2eAgNnSc4d1
hnutí	hnutí	k1gNnPc4
tak	tak	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1934	#num#	k4
zcela	zcela	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Basmačské	Basmačský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
bylo	být	k5eAaImAgNnS
námětem	námět	k1gInSc7
několika	několik	k4yIc7
tzv.	tzv.	kA
„	„	k?
<g/>
rudých	rudý	k2eAgInPc2d1
westernů	western	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byly	být	k5eAaImAgInP
filmy	film	k1gInPc1
Věrný	věrný	k2eAgInSc1d1
Džulbars	Džulbars	k1gInSc4
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bílé	bílý	k2eAgNnSc1d1
slunce	slunce	k1gNnSc1
pouště	poušť	k1gFnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sedmá	sedmý	k4xOgFnSc1
kulka	kulka	k1gFnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Červené	Červené	k2eAgInPc1d1
máky	mák	k1gInPc1
Issyk-Kulu	Issyk-Kul	k1gInSc2
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
v	v	k7c6
Kokandu	Kokando	k1gNnSc6
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nepřemožitelný	přemožitelný	k2eNgMnSc1d1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
či	či	k8xC
TV	TV	kA
seriál	seriál	k1gInSc1
Státní	státní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Basmachi	Basmach	k1gFnSc2
movement	movement	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Б	Б	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
MUḤ	MUḤ	k1gFnPc2
<g/>
,	,	kIx,
Fayz	Fayza	k1gFnPc2
<g/>
̤	̤	k?
<g/>
;	;	kIx,
HAZĀ	HAZĀ	k1gMnSc1
<g/>
,	,	kIx,
Fayz	Fayz	k1gMnSc1
<g/>
̤	̤	k?
Muḥ	Muḥ	k1gInSc1
Kā	Kā	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabul	Kabul	k1gInSc1
Under	Under	k1gInSc4
Siege	Sieg	k1gFnSc2
<g/>
:	:	kIx,
Fayz	Fayz	k1gInSc1
Muhammad	Muhammad	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Account	Account	k1gInSc1
of	of	k?
the	the	k?
1929	#num#	k4
Uprising	Uprising	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Markus	Markus	k1gInSc1
Wiener	Wienra	k1gFnPc2
Publishers	Publishersa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781558761551	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Michael	Michael	k1gMnSc1
Rywkin	Rywkin	k1gMnSc1
<g/>
,	,	kIx,
Moscow	Moscow	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
s	s	k7c7
Muslim	muslim	k1gMnSc1
Challenge	Challenge	k1gInSc1
<g/>
:	:	kIx,
Soviet	Soviet	k1gMnSc1
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
(	(	kIx(
<g/>
Armonk	Armonk	k1gMnSc1
<g/>
:	:	kIx,
M.	M.	kA
E.	E.	kA
Sharpe	sharp	k1gInSc5
<g/>
,	,	kIx,
Inc	Inc	k1gMnPc7
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
41	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Soviet	Soviet	k1gInSc1
Disunion	Disunion	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Nationalities	Nationalitiesa	k1gFnPc2
Problem	Probl	k1gMnSc7
in	in	k?
the	the	k?
USSR	USSR	kA
<g/>
,	,	kIx,
By	by	k9
Bohdan	Bohdan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Nahaylo	Nahaylo	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Victor	Victor	k1gMnSc1
Swoboda	Swoboda	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
40	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Krivosheev	Krivosheev	k1gFnSc1
<g/>
,	,	kIx,
Grigori	Grigori	k1gNnSc1
(	(	kIx(
<g/>
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Soviet	Soviet	k1gMnSc1
Casualties	Casualties	k1gMnSc1
and	and	k?
Combat	Combat	k1gMnSc1
Losses	Losses	k1gMnSc1
in	in	k?
the	the	k?
Twentieth	Twentieth	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
,	,	kIx,
p.	p.	k?
43	#num#	k4
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
:	:	kIx,
Greenhill	Greenhill	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
General-Lieutenant	General-Lieutenant	k1gInSc1
G.	G.	kA
<g/>
F.	F.	kA
<g/>
Krivosheyev	Krivosheyva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gMnSc1
Armed	Armed	k1gMnSc1
Forces	Forces	k1gMnSc1
Losses	Losses	k1gMnSc1
In	In	k1gMnSc1
Wars	Warsa	k1gFnPc2
<g/>
,	,	kIx,
<g/>
Combat	Combat	k1gFnPc2
Operations	Operationsa	k1gFnPc2
Military	Militara	k1gFnSc2
Conflicts	Conflicts	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moscow	Moscow	k1gFnSc1
Military	Militara	k1gFnPc4
Publishing	Publishing	k1gInSc4
House	house	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Uzbekistan	Uzbekistan	k1gInSc1
<g/>
,	,	kIx,
By	by	k9
Thomas	Thomas	k1gMnSc1
R	R	kA
McCray	McCraa	k1gFnPc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
F	F	kA
Gritzner	Gritzner	k1gMnSc1
<g/>
,	,	kIx,
pg	pg	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
30	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1438105517.1	1438105517.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
Martha	Marth	k1gMnSc2
B.	B.	kA
Olcott	Olcott	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Basmachi	Basmach	k1gFnSc2
or	or	k?
Freemen	Freemen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Revolt	revolta	k1gFnPc2
in	in	k?
Turkestan	Turkestan	k1gInSc1
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
24.1	24.1	k4
2	#num#	k4
Baberowski	Baberowski	k1gNnPc2
<g/>
,	,	kIx,
Jörg	Jörg	k1gMnSc1
;	;	kIx,
Doering-Manteuffel	Doering-Manteuffel	k1gMnSc1
<g/>
,	,	kIx,
Anselm	Anselm	k1gMnSc1
<g/>
:	:	kIx,
Ordnung	Ordnung	k1gInSc1
durch	durch	k1gInSc1
Terror	Terror	k1gInSc1
:	:	kIx,
Gewaltexzesse	Gewaltexzesse	k1gFnSc1
und	und	k?
Vernichtung	Vernichtung	k1gInSc1
im	im	k?
nationalsozialistischen	nationalsozialistischen	k1gInSc1
und	und	k?
im	im	k?
stalinschen	stalinschen	k1gInSc1
Imperium	Imperium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bonn	Bonn	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
pp	pp	k?
<g/>
=	=	kIx~
<g/>
201	#num#	k4
<g/>
-	-	kIx~
<g/>
2021	#num#	k4
2	#num#	k4
Victor	Victor	k1gMnSc1
Spolnikov	Spolnikov	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Impact	Impact	k1gInSc1
of	of	k?
Afghanistan	Afghanistan	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
War	War	k1gFnSc7
on	on	k3xPp3gMnSc1
the	the	k?
Former	Former	k1gInSc1
Soviet	Soviet	k1gMnSc1
Republics	Republicsa	k1gFnPc2
of	of	k?
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
in	in	k?
Hafeez	Hafeez	k1gMnSc1
Malik	Malik	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
:	:	kIx,
Its	Its	k1gMnSc1
Strategic	Strategic	k1gMnSc1
Importance	importance	k1gFnSc2
and	and	k?
Future	Futur	k1gMnSc5
Prospects	Prospects	k1gInSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
101.1	101.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Richard	Richard	k1gMnSc1
Lorenz	Lorenz	k1gMnSc1
<g/>
,	,	kIx,
Economic	Economic	k1gMnSc1
Bases	Bases	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
of	of	k?
the	the	k?
Basmachi	Basmachi	k1gNnPc2
Movement	Movement	k1gInSc1
in	in	k?
the	the	k?
Ferghana	Ferghan	k1gMnSc2
Valley	Vallea	k1gMnSc2
<g/>
,	,	kIx,
in	in	k?
"	"	kIx"
<g/>
Muslim	muslim	k1gMnSc1
Communities	Communities	k1gMnSc1
Reemerge	Reemerge	k1gFnSc1
<g/>
:	:	kIx,
Historical	Historical	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
on	on	k3xPp3gMnSc1
Nationality	Nationalita	k1gFnPc1
<g/>
,	,	kIx,
Politics	Politics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Opposition	Opposition	k1gInSc1
in	in	k?
the	the	k?
Former	Former	k1gInSc1
Soviet	Soviet	k1gMnSc1
Union	union	k1gInSc1
and	and	k?
Yugoslavia	Yugoslavia	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Editors	Editors	k1gInSc1
<g/>
:	:	kIx,
Andreas	Andreas	k1gMnSc1
Kappeler	Kappeler	k1gMnSc1
<g/>
,	,	kIx,
Gerhard	Gerhard	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Gerog	Gerog	k1gMnSc1
Brunner	Brunner	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Catherin	Catherin	k1gInSc1
Evtuhov	Evtuhov	k1gInSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Stites	Stites	k1gMnSc1
<g/>
,	,	kIx,
A	a	k9
History	Histor	k1gMnPc4
of	of	k?
Russia	Russia	k1gFnSc1
<g/>
:	:	kIx,
Peoples	Peoples	k1gInSc1
<g/>
,	,	kIx,
Legends	Legends	k1gInSc1
<g/>
,	,	kIx,
Events	Events	k1gInSc1
<g/>
,	,	kIx,
Forces	Forces	k1gInSc1
(	(	kIx(
<g/>
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
Houghton	Houghton	k1gInSc1
Mifflin	Mifflina	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
265	#num#	k4
<g/>
↑	↑	k?
Fazal-Ur-Rahim	Fazal-Ur-Rahim	k1gMnSc1
Khan	Khan	k1gMnSc1
Marwat	Marwat	k2eAgMnSc1d1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Basmachi	Basmach	k1gFnSc2
Movement	Movement	k1gMnSc1
in	in	k?
Soviet	Soviet	k1gMnSc1
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
,	,	kIx,
160	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Joseph	Josepha	k1gFnPc2
L.	L.	kA
Wieczynski	Wieczynsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Modern	Modern	k1gMnSc1
encyclopedia	encyclopedium	k1gNnSc2
of	of	k?
Russian	Russian	k1gMnSc1
and	and	k?
Soviet	Soviet	k1gMnSc1
history	histor	k1gInPc4
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
21	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
International	International	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87569	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
125	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Ritter	Rittra	k1gFnPc2
<g/>
,	,	kIx,
William	William	k1gInSc1
S	s	k7c7
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Revolt	revolta	k1gFnPc2
in	in	k?
the	the	k?
Mountains	Mountains	k1gInSc1
<g/>
:	:	kIx,
Fuzail	Fuzail	k1gInSc1
Maksum	Maksum	k1gInSc1
and	and	k?
the	the	k?
Occupation	Occupation	k1gInSc1
of	of	k?
Garm	Garm	k1gInSc1
<g/>
,	,	kIx,
Spring	Spring	k1gInSc1
1929	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Contemporary	Contemporara	k1gFnSc2
History	Histor	k1gInPc1
25	#num#	k4
<g/>
:	:	kIx,
547.1	547.1	k4
2	#num#	k4
Yı	Yı	k1gInSc1
Şuhnaz	Şuhnaz	k1gInSc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
An	An	k1gMnSc1
Ottoman	Ottoman	k1gMnSc1
Warrior	Warrior	k1gMnSc1
Abroad	Abroad	k1gInSc1
<g/>
:	:	kIx,
Enver	Enver	k1gInSc1
Paşa	Paş	k1gInSc2
as	as	k9
an	an	k?
Expatriate	Expatriat	k1gInSc5
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Middle	Middle	k1gMnSc1
Eastern	Eastern	k1gMnSc1
Studies	Studies	k1gInSc4
35	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
4	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
47	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
↑	↑	k?
Basmachis	Basmachis	k1gInSc1
-	-	kIx~
Oxford	Oxford	k1gInSc1
Islamic	Islamic	k1gMnSc1
Studies	Studies	k1gMnSc1
Online	Onlin	k1gInSc5
<g/>
↑	↑	k?
Fazal-Ur-Rahim	Fazal-Ur-Rahim	k1gInSc1
Khan	Khan	k1gMnSc1
Marwat	Marwat	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Basmachi	Basmach	k1gFnSc2
Movement	Movement	k1gMnSc1
in	in	k?
Soviet	Soviet	k1gMnSc1
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
(	(	kIx(
<g/>
A	a	k8xC
Study	stud	k1gInPc1
in	in	k?
Political	Political	k1gMnSc4
Development	Development	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Peshawar	Peshawar	k1gInSc1
<g/>
,	,	kIx,
Emjay	Emjay	k1gInPc1
Books	Booksa	k1gFnPc2
International	International	k1gFnPc2
<g/>
:	:	kIx,
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
151	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PIPES	PIPES	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslims	Muslims	k1gInSc1
of	of	k?
Soviet	Soviet	k1gInSc1
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
:	:	kIx,
Trends	Trends	k1gInSc1
and	and	k?
Prospects	Prospects	k1gInSc1
(	(	kIx(
<g/>
Part	part	k1gInSc1
I	I	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
1955	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
<g/>
–	–	k?
<g/>
150	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ritter	Ritter	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
S	s	k7c7
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gFnSc1
Final	Final	k1gInSc1
Phase	Phase	k1gFnSc1
in	in	k?
the	the	k?
Liquidation	Liquidation	k1gInSc1
of	of	k?
Anti-Soviet	Anti-Soviet	k1gInSc1
Resistance	Resistance	k1gFnSc1
in	in	k?
Tadzhikistan	Tadzhikistan	k1gInSc1
<g/>
:	:	kIx,
Ibrahim	Ibrahim	k1gMnSc1
Bek	bek	k1gMnSc1
and	and	k?
the	the	k?
Basmachi	Basmach	k1gFnSc2
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soviet	Soviet	k1gMnSc1
Studies	Studies	k1gMnSc1
37	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TUCKER	TUCKER	kA
<g/>
,	,	kIx,
Spencer	Spencer	k1gMnSc1
C.	C.	kA
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Insurgency	Insurgenca	k1gMnSc2
and	and	k?
Counterinsurgency	Counterinsurgenca	k1gMnSc2
<g/>
:	:	kIx,
A	A	kA
New	New	k1gMnSc1
Era	Era	k1gMnSc1
of	of	k?
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
<g/>
:	:	kIx,
A	a	k9
New	New	k1gMnSc1
Era	Era	k1gMnSc1
of	of	k?
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781610692809	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
61	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
