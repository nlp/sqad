<p>
<s>
Břehouš	břehouš	k1gMnSc1	břehouš
černoocasý	černoocasý	k2eAgMnSc1d1	černoocasý
(	(	kIx(	(
<g/>
Limosa	Limosa	k1gFnSc1	Limosa
limosa	limosa	k1gFnSc1	limosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
druhem	druh	k1gInSc7	druh
bahňáka	bahňák	k1gMnSc2	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slukovitých	slukovitý	k2eAgFnPc2d1	slukovitý
(	(	kIx(	(
<g/>
Scolopacidae	Scolopacidae	k1gFnPc2	Scolopacidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
82	[number]	k4	82
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
160	[number]	k4	160
<g/>
–	–	k?	–
<g/>
440	[number]	k4	440
g	g	kA	g
<g/>
,	,	kIx,	,
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
mezi	mezi	k7c7	mezi
244	[number]	k4	244
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
g.	g.	k?	g.
Samci	samec	k1gInPc7	samec
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
kratší	krátký	k2eAgInSc4d2	kratší
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
šatu	šat	k1gInSc6	šat
má	mít	k5eAaImIp3nS	mít
oranžově	oranžově	k6eAd1	oranžově
rezavý	rezavý	k2eAgInSc4d1	rezavý
krk	krk	k1gInSc4	krk
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
(	(	kIx(	(
<g/>
intenzivnější	intenzivní	k2eAgInPc1d2	intenzivnější
u	u	k7c2	u
samce	samec	k1gInSc2	samec
<g/>
)	)	kIx)	)
a	a	k8xC	a
různě	různě	k6eAd1	různě
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pruhovaní	pruhovaný	k2eAgMnPc1d1	pruhovaný
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
jinak	jinak	k6eAd1	jinak
bílého	bílý	k2eAgNnSc2d1	bílé
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatu	šat	k1gInSc6	šat
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
–	–	k?	–
šedohnědě	šedohnědě	k6eAd1	šedohnědě
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
žlutavě	žlutavě	k6eAd1	žlutavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
hruď	hruď	k1gFnSc4	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
letu	let	k1gInSc6	let
je	být	k5eAaImIp3nS	být
nápadné	nápadný	k2eAgNnSc1d1	nápadné
černobílé	černobílý	k2eAgNnSc1d1	černobílé
zbarvení	zbarvení	k1gNnSc1	zbarvení
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
letek	letka	k1gFnPc2	letka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
ostrým	ostrý	k2eAgInSc7d1	ostrý
"	"	kIx"	"
<g/>
grutjo	grutjo	k1gMnSc1	grutjo
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
také	také	k9	také
nosové	nosový	k2eAgFnSc3d1	nosová
"	"	kIx"	"
<g/>
vitovitovito	vitovitovita	k1gFnSc5	vitovitovita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čejčí	čejčí	k2eAgInSc1d1	čejčí
"	"	kIx"	"
<g/>
kvuíh	kvuíh	k1gInSc1	kvuíh
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
svižné	svižný	k2eAgFnSc6d1	svižná
"	"	kIx"	"
<g/>
vivivi	viviev	k1gFnSc6	viviev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
Islandu	Island	k1gInSc2	Island
východně	východně	k6eAd1	východně
po	po	k7c4	po
východní	východní	k2eAgFnSc4d1	východní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Australasii	Australasie	k1gFnSc6	Australasie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Globální	globální	k2eAgFnSc1d1	globální
populace	populace	k1gFnSc1	populace
břehouše	břehouš	k1gMnSc2	břehouš
čenoocasého	čenoocasý	k2eAgInSc2d1	čenoocasý
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
;	;	kIx,	;
jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
úbytek	úbytek	k1gInSc1	úbytek
za	za	k7c4	za
předchozích	předchozí	k2eAgNnPc2d1	předchozí
15	[number]	k4	15
let	léto	k1gNnPc2	léto
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c6	na
celých	celý	k2eAgInPc6d1	celý
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
ztráta	ztráta	k1gFnSc1	ztráta
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
zapříčiněná	zapříčiněný	k2eAgNnPc1d1	zapříčiněné
především	především	k6eAd1	především
odvodňováním	odvodňování	k1gNnSc7	odvodňování
mokřadů	mokřad	k1gInPc2	mokřad
a	a	k8xC	a
intenzifikací	intenzifikace	k1gFnSc7	intenzifikace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
také	také	k9	také
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
státech	stát	k1gInPc6	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vyjma	vyjma	k7c2	vyjma
Francie	Francie	k1gFnSc2	Francie
zakázán	zakázán	k2eAgMnSc1d1	zakázán
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ČR	ČR	kA	ČR
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xS	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
větších	veliký	k2eAgFnPc6d2	veliký
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
v	v	k7c6	v
travnatých	travnatý	k2eAgFnPc6d1	travnatá
bažinách	bažina	k1gFnPc6	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
především	především	k9	především
v	v	k7c6	v
ústích	ústí	k1gNnPc6	ústí
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
korýši	korýš	k1gMnPc7	korýš
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc7	měkkýš
a	a	k8xC	a
červy	červ	k1gMnPc7	červ
<g/>
,	,	kIx,	,
za	za	k7c2	za
tahu	tah	k1gInSc2	tah
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
také	také	k9	také
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kořisti	kořist	k1gFnSc6	kořist
pátrá	pátrat	k5eAaImIp3nS	pátrat
hlavně	hlavně	k9	hlavně
píchaním	píchaní	k2eAgInSc7d1	píchaní
svým	svůj	k3xOyFgInSc7	svůj
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
zobákem	zobák	k1gInSc7	zobák
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
citlivým	citlivý	k2eAgInSc7d1	citlivý
koncem	konec	k1gInSc7	konec
do	do	k7c2	do
bahna	bahno	k1gNnSc2	bahno
nebo	nebo	k8xC	nebo
měkké	měkký	k2eAgFnSc2d1	měkká
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
volných	volný	k2eAgFnPc6d1	volná
koloniích	kolonie	k1gFnPc6	kolonie
o	o	k7c4	o
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zkoumající	zkoumající	k2eAgFnSc2d1	zkoumající
islandské	islandský	k2eAgFnSc2d1	islandská
populace	populace	k1gFnSc2	populace
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
páry	pár	k1gInPc1	pár
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
vrací	vracet	k5eAaImIp3nS	vracet
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pouhé	pouhý	k2eAgInPc4d1	pouhý
3	[number]	k4	3
dny	den	k1gInPc4	den
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
trávili	trávit	k5eAaImAgMnP	trávit
zimu	zima	k1gFnSc4	zima
zcela	zcela	k6eAd1	zcela
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
opozdí	opozdit	k5eAaPmIp3nS	opozdit
<g/>
,	,	kIx,	,
pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
teritoriu	teritorium	k1gNnSc6	teritorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
m	m	kA	m
okolo	okolo	k7c2	okolo
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
hloubí	hloubit	k5eAaImIp3nS	hloubit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
vegetaci	vegetace	k1gFnSc6	vegetace
několik	několik	k4yIc4	několik
mělkých	mělký	k2eAgInPc2d1	mělký
důlků	důlek	k1gInPc2	důlek
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vystlaných	vystlaný	k2eAgInPc2d1	vystlaný
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
samice	samice	k1gFnPc1	samice
sama	sám	k3xTgFnSc1	sám
jedno	jeden	k4xCgNnSc1	jeden
vybere	vybrat	k5eAaPmIp3nS	vybrat
a	a	k8xC	a
naklade	naklást	k5eAaPmIp3nS	naklást
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
zelenavá	zelenavý	k2eAgNnPc1d1	zelenavé
<g/>
,	,	kIx,	,
hnědě	hnědě	k6eAd1	hnědě
skvrnitá	skvrnitý	k2eAgNnPc4d1	skvrnité
vejce	vejce	k1gNnPc4	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
55,1	[number]	k4	55,1
x	x	k?	x
37,8	[number]	k4	37,8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
inkubaci	inkubace	k1gFnSc6	inkubace
trvající	trvající	k2eAgFnSc1d1	trvající
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nP	podílet
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
společně	společně	k6eAd1	společně
také	také	k9	také
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
aktivně	aktivně	k6eAd1	aktivně
brání	bránit	k5eAaImIp3nS	bránit
okolí	okolí	k1gNnSc4	okolí
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
predátora	predátor	k1gMnSc4	predátor
často	často	k6eAd1	často
útočí	útočit	k5eAaImIp3nS	útočit
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
hlasitě	hlasitě	k6eAd1	hlasitě
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
ozývají	ozývat	k5eAaImIp3nP	ozývat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
prekociální	prekociální	k2eAgFnPc1d1	prekociální
a	a	k8xC	a
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
oběma	dva	k4xCgMnPc7	dva
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
opeří	opeřit	k5eAaPmIp3nS	opeřit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
společně	společně	k6eAd1	společně
putovat	putovat	k5eAaImF	putovat
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
nabídkou	nabídka	k1gFnSc7	nabídka
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
studie	studie	k1gFnSc2	studie
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
činí	činit	k5eAaImIp3nS	činit
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
druhu	druh	k1gInSc2	druh
37,6	[number]	k4	37,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
32	[number]	k4	32
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
36,9	[number]	k4	36,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Polytypický	Polytypický	k2eAgInSc1d1	Polytypický
druh	druh	k1gInSc1	druh
popsaný	popsaný	k2eAgInSc1d1	popsaný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Scolopax	Scolopax	k1gInSc1	Scolopax
limosa	limosa	k1gFnSc1	limosa
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
limus	limus	k1gInSc1	limus
=	=	kIx~	=
bláto	bláto	k1gNnSc1	bláto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
3	[number]	k4	3
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
L.	L.	kA	L.
limosa	limosa	k1gFnSc1	limosa
limosa	limosa	k1gFnSc1	limosa
–	–	k?	–
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c4	po
střední	střední	k2eAgFnSc4d1	střední
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
asijskou	asijský	k2eAgFnSc4d1	asijská
část	část	k1gFnSc4	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
.	.	kIx.	.
</s>
<s>
Zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgNnSc6d1	blízké
východně	východně	k6eAd1	východně
až	až	k6eAd1	až
po	po	k7c4	po
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L.	L.	kA	L.
limosa	limosa	k1gFnSc1	limosa
islandica	islandica	k1gFnSc1	islandica
–	–	k?	–
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
Shetlandech	Shetland	k1gInPc6	Shetland
a	a	k8xC	a
Lofotech	Lofot	k1gInPc6	Lofot
<g/>
.	.	kIx.	.
</s>
<s>
Zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L.	L.	kA	L.
limosa	limosa	k1gFnSc1	limosa
melanuroides	melanuroides	k1gInSc1	melanuroides
–	–	k?	–
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
až	až	k9	až
po	po	k7c4	po
samotný	samotný	k2eAgInSc4d1	samotný
východ	východ	k1gInSc4	východ
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
,	,	kIx,	,
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc3	Indonésie
<g/>
,	,	kIx,	,
Papuy	Papuy	k1gInPc1	Papuy
<g/>
–	–	k?	–
<g/>
Nové	Nové	k2eAgFnSc2d1	Nové
Guiney	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
břehouš	břehouš	k1gMnSc1	břehouš
černoocasý	černoocasý	k2eAgMnSc1d1	černoocasý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
břehouš	břehouš	k1gMnSc1	břehouš
černoocasý	černoocasý	k2eAgMnSc1d1	černoocasý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Limosa	Limosa	k1gFnSc1	Limosa
limosa	limosa	k1gFnSc1	limosa
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
