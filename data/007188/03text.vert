<s>
Kružítko	kružítko	k1gNnSc1	kružítko
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
Kružidlo	kružidlo	k1gNnSc4	kružidlo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
rýsování	rýsování	k1gNnSc3	rýsování
kružnic	kružnice	k1gFnPc2	kružnice
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Kružítko	kružítko	k1gNnSc1	kružítko
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
nožiček	nožička	k1gFnPc2	nožička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pohyblivě	pohyblivě	k6eAd1	pohyblivě
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
nožička	nožička	k1gFnSc1	nožička
je	být	k5eAaImIp3nS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
hrotem	hrot	k1gInSc7	hrot
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
<g/>
)	)	kIx)	)
tuhou	tuhý	k2eAgFnSc4d1	tuhá
<g/>
.	.	kIx.	.
</s>
<s>
Pohyblivé	pohyblivý	k2eAgNnSc1d1	pohyblivé
spojení	spojení	k1gNnSc1	spojení
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
měnit	měnit	k5eAaImF	měnit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
konců	konec	k1gInPc2	konec
kružítka	kružítko	k1gNnSc2	kružítko
a	a	k8xC	a
tak	tak	k6eAd1	tak
rýsovat	rýsovat	k5eAaImF	rýsovat
kružnice	kružnice	k1gFnSc1	kružnice
různého	různý	k2eAgInSc2d1	různý
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Rýsování	rýsování	k1gNnSc1	rýsování
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jehla	jehla	k1gFnSc1	jehla
se	se	k3xPyFc4	se
umístí	umístit	k5eAaPmIp3nS	umístit
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
označujícího	označující	k2eAgInSc2d1	označující
střed	střed	k1gInSc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
a	a	k8xC	a
točením	točení	k1gNnSc7	točení
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
v	v	k7c6	v
prstech	prst	k1gInPc6	prst
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
narýsování	narýsování	k1gNnSc3	narýsování
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
trochu	trocha	k1gFnSc4	trocha
cviku	cvik	k1gInSc2	cvik
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnPc1d3	nejjednodušší
kružítka	kružítko	k1gNnPc1	kružítko
jsou	být	k5eAaImIp3nP	být
vyrobena	vyroben	k2eAgNnPc1d1	vyrobeno
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
jehla	jehla	k1gFnSc1	jehla
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tuha	tuha	k1gFnSc1	tuha
nejdou	jít	k5eNaImIp3nP	jít
naklonit	naklonit	k5eAaPmF	naklonit
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
k	k	k7c3	k
rýsování	rýsování	k1gNnSc3	rýsování
kružnic	kružnice	k1gFnPc2	kružnice
velkých	velký	k2eAgFnPc2d1	velká
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
malých	malý	k2eAgInPc2d1	malý
<g/>
,	,	kIx,	,
poloměrů	poloměr	k1gInPc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
spojení	spojení	k1gNnSc1	spojení
nožiček	nožička	k1gFnPc2	nožička
nezaručuje	zaručovat	k5eNaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
větším	veliký	k2eAgInSc6d2	veliký
tlaku	tlak	k1gInSc6	tlak
na	na	k7c4	na
kružíto	kružíto	k1gNnSc4	kružíto
nedojde	dojít	k5eNaPmIp3nS	dojít
během	během	k7c2	během
rýsování	rýsování	k1gNnSc2	rýsování
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
konců	konec	k1gInPc2	konec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
narýsování	narýsování	k1gNnSc3	narýsování
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
kružítka	kružítko	k1gNnPc1	kružítko
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
výbavy	výbava	k1gFnSc2	výbava
žáka	žák	k1gMnSc2	žák
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rýsování	rýsování	k1gNnSc4	rýsování
v	v	k7c6	v
deskriptivní	deskriptivní	k2eAgFnSc6d1	deskriptivní
geometrii	geometrie	k1gFnSc6	geometrie
nebo	nebo	k8xC	nebo
v	v	k7c6	v
technickém	technický	k2eAgNnSc6d1	technické
kreslení	kreslení	k1gNnSc6	kreslení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kružítka	kružítko	k1gNnPc1	kružítko
vyrobená	vyrobený	k2eAgNnPc1d1	vyrobené
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
konstrukce	konstrukce	k1gFnSc1	konstrukce
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přesné	přesný	k2eAgNnSc4d1	přesné
nastavení	nastavení	k1gNnSc4	nastavení
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
rozevření	rozevření	k1gNnSc1	rozevření
kružítka	kružítko	k1gNnSc2	kružítko
při	při	k7c6	při
rýsování	rýsování	k1gNnSc6	rýsování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
šroubu	šroub	k1gInSc2	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Nožička	nožička	k1gFnSc1	nožička
s	s	k7c7	s
tuhou	tuha	k1gFnSc7	tuha
je	být	k5eAaImIp3nS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
kloubem	kloub	k1gInSc7	kloub
<g/>
,	,	kIx,	,
držák	držák	k1gInSc1	držák
tuhy	tuha	k1gFnSc2	tuha
lze	lze	k6eAd1	lze
vyměnit	vyměnit	k5eAaPmF	vyměnit
např.	např.	kA	např.
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
hrot	hrot	k1gInSc4	hrot
<g/>
,	,	kIx,	,
držák	držák	k1gInSc4	držák
rýsovacího	rýsovací	k2eAgNnSc2d1	rýsovací
pera	pero	k1gNnSc2	pero
nebo	nebo	k8xC	nebo
lze	lze	k6eAd1	lze
vsunout	vsunout	k5eAaPmF	vsunout
prodlužovací	prodlužovací	k2eAgInSc4d1	prodlužovací
nástavec	nástavec	k1gInSc4	nástavec
pro	pro	k7c4	pro
kružnice	kružnice	k1gFnPc4	kružnice
velkých	velký	k2eAgInPc2d1	velký
poloměrů	poloměr	k1gInPc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rýsování	rýsování	k1gNnSc3	rýsování
na	na	k7c4	na
kovové	kovový	k2eAgInPc4d1	kovový
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c4	na
plech	plech	k1gInSc4	plech
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
kružítko	kružítko	k1gNnSc1	kružítko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kružítko	kružítko	k1gNnSc1	kružítko
má	mít	k5eAaImIp3nS	mít
oba	dva	k4xCgInPc4	dva
hroty	hrot	k1gInPc4	hrot
z	z	k7c2	z
tvrzeného	tvrzený	k2eAgInSc2d1	tvrzený
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Hroty	hrot	k1gInPc1	hrot
jsou	být	k5eAaImIp3nP	být
broušeny	brousit	k5eAaImNgInP	brousit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgInPc2d1	malý
průměrů	průměr	k1gInPc2	průměr
(	(	kIx(	(
<g/>
od	od	k7c2	od
2	[number]	k4	2
<g/>
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
pružným	pružný	k2eAgInSc7d1	pružný
plátem	plát	k1gInSc7	plát
se	s	k7c7	s
stopkou	stopka	k1gFnSc7	stopka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
zvoleného	zvolený	k2eAgInSc2d1	zvolený
průměru	průměr	k1gInSc2	průměr
slouží	sloužit	k5eAaImIp3nS	sloužit
šroubovice	šroubovice	k1gFnSc1	šroubovice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
volně	volně	k6eAd1	volně
stavitelná	stavitelný	k2eAgFnSc1d1	stavitelná
svěrnou	svěrný	k2eAgFnSc7d1	svěrná
maticí	matice	k1gFnSc7	matice
<g/>
.	.	kIx.	.
</s>
<s>
Strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
kružítko	kružítko	k1gNnSc1	kružítko
lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
odpíchnutí	odpíchnutí	k1gNnSc4	odpíchnutí
rozteče	rozteč	k1gFnSc2	rozteč
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
k	k	k7c3	k
rýsování	rýsování	k1gNnSc3	rýsování
na	na	k7c4	na
plast	plast	k1gInSc4	plast
i	i	k8xC	i
jiné	jiný	k2eAgFnPc4d1	jiná
slitiny	slitina	k1gFnPc4	slitina
různých	různý	k2eAgInPc2d1	různý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nulátko	nulátko	k1gNnSc1	nulátko
je	být	k5eAaImIp3nS	být
kružítko	kružítko	k1gNnSc4	kružítko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
rýsování	rýsování	k1gNnSc4	rýsování
kružnic	kružnice	k1gFnPc2	kružnice
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
5	[number]	k4	5
mm	mm	kA	mm
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tak	tak	k6eAd1	tak
malých	malý	k2eAgInPc6d1	malý
poloměrech	poloměr	k1gInPc6	poloměr
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgFnSc1d1	speciální
šablona	šablona	k1gFnSc1	šablona
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bublina	bublina	k1gFnSc1	bublina
<g/>
.	.	kIx.	.
</s>
<s>
Odpichovátko	odpichovátko	k1gNnSc1	odpichovátko
je	být	k5eAaImIp3nS	být
kružítko	kružítko	k1gNnSc4	kružítko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
hroty	hrot	k1gInPc4	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
hrotů	hrot	k1gInPc2	hrot
se	se	k3xPyFc4	se
nastaví	nastavět	k5eAaBmIp3nP	nastavět
podle	podle	k7c2	podle
měřítka	měřítko	k1gNnSc2	měřítko
mapy	mapa	k1gFnSc2	mapa
a	a	k8xC	a
"	"	kIx"	"
<g/>
kráčením	kráčení	k1gNnSc7	kráčení
<g/>
"	"	kIx"	"
po	po	k7c6	po
mapě	mapa	k1gFnSc6	mapa
se	se	k3xPyFc4	se
změří	změřit	k5eAaPmIp3nS	změřit
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
a	a	k8xC	a
letecké	letecký	k2eAgFnSc6d1	letecká
navigaci	navigace	k1gFnSc6	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
odpichovátko	odpichovátko	k1gNnSc4	odpichovátko
dostatečně	dostatečně	k6eAd1	dostatečně
zvětšíme	zvětšit	k5eAaPmIp1nP	zvětšit
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
pomůcku	pomůcka	k1gFnSc4	pomůcka
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
přenášení	přenášení	k1gNnSc4	přenášení
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
užívalo	užívat	k5eAaImAgNnS	užívat
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
jeho	jeho	k3xOp3gInSc7	jeho
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kladivem	kladivo	k1gNnSc7	kladivo
i	i	k9	i
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc2d1	neexistující
<g/>
)	)	kIx)	)
Německé	německý	k2eAgFnSc2d1	německá
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Logaritmické	logaritmický	k2eAgNnSc1d1	logaritmické
pravítko	pravítko	k1gNnSc1	pravítko
Pravítko	pravítko	k1gNnSc4	pravítko
Příložník	příložník	k1gInSc4	příložník
Rýsovací	rýsovací	k2eAgNnSc1d1	rýsovací
prkno	prkno	k1gNnSc1	prkno
Trojúhelník	trojúhelník	k1gInSc1	trojúhelník
Úhloměr	úhloměr	k1gInSc4	úhloměr
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kružítko	kružítko	k1gNnSc4	kružítko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kružítko	kružítko	k1gNnSc4	kružítko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
kružítko	kružítko	k1gNnSc4	kružítko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
