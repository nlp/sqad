<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
kolibříkovití	kolibříkovitý	k2eAgMnPc1d1	kolibříkovitý
(	(	kIx(	(
<g/>
Trochilidae	Trochilidae	k1gNnSc7	Trochilidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
létající	létající	k2eAgInPc1d1	létající
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
endemické	endemický	k2eAgMnPc4d1	endemický
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
ptačí	ptačí	k2eAgInSc1d1	ptačí
druh	druh	k1gInSc1	druh
snadno	snadno	k6eAd1	snadno
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
za	za	k7c2	za
dlouhozobku	dlouhozobek	k1gInSc2	dlouhozobek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
motýl	motýl	k1gMnSc1	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lišajovitých	lišajovití	k1gMnPc2	lišajovití
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Kolibříci	kolibřík	k1gMnPc1	kolibřík
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zdatnými	zdatný	k2eAgMnPc7d1	zdatný
letci	letec	k1gMnPc7	letec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
mohou	moct	k5eAaImIp3nP	moct
mávnout	mávnout	k5eAaPmF	mávnout
křídly	křídlo	k1gNnPc7	křídlo
12	[number]	k4	12
<g/>
×	×	k?	×
až	až	k9	až
90	[number]	k4	90
<g/>
×	×	k?	×
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
doslova	doslova	k6eAd1	doslova
zastavit	zastavit	k5eAaPmF	zastavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
také	také	k9	také
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
15	[number]	k4	15
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
54	[number]	k4	54
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
zástupci	zástupce	k1gMnPc1	zástupce
ptačí	ptačí	k2eAgFnSc2d1	ptačí
říše	říš	k1gFnSc2	říš
dokážou	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
.	.	kIx.	.
</s>

