<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
či	či	k8xC	či
videohra	videohra	k1gFnSc1	videohra
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hry	hra	k1gFnSc2	hra
zprostředkovaný	zprostředkovaný	k2eAgInSc4d1	zprostředkovaný
interaktivním	interaktivní	k2eAgInSc7d1	interaktivní
softwarem	software	k1gInSc7	software
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
elektronických	elektronický	k2eAgFnPc6d1	elektronická
platformách	platforma	k1gFnPc6	platforma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
počítačích	počítač	k1gInPc6	počítač
PC	PC	kA	PC
a	a	k8xC	a
Macintosh	Macintosh	kA	Macintosh
<g/>
,	,	kIx,	,
na	na	k7c6	na
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
herních	herní	k2eAgFnPc6d1	herní
konzolích	konzole	k1gFnPc6	konzole
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
multimediální	multimediální	k2eAgFnSc4d1	multimediální
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
hry	hra	k1gFnSc2	hra
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
uživatelem	uživatel	k1gMnSc7	uživatel
pomocí	pomocí	k7c2	pomocí
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
ovládací	ovládací	k2eAgNnPc4d1	ovládací
zařízení	zařízení	k1gNnPc4	zařízení
či	či	k8xC	či
výkonný	výkonný	k2eAgInSc4d1	výkonný
hardware	hardware	k1gInSc4	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
serious	serious	k1gInSc4	serious
games	gamesa	k1gFnPc2	gamesa
ale	ale	k8xC	ale
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
výuce	výuka	k1gFnSc3	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
videoher	videohra	k1gFnPc2	videohra
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
vývoje	vývoj	k1gInSc2	vývoj
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
virtuální	virtuální	k2eAgInSc4d1	virtuální
svět	svět	k1gInSc4	svět
nebo	nebo	k8xC	nebo
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
pomocí	pomocí	k7c2	pomocí
komponentů	komponent	k1gInPc2	komponent
připojených	připojený	k2eAgInPc2d1	připojený
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
(	(	kIx(	(
<g/>
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
joystick	joystick	k1gInSc1	joystick
<g/>
,	,	kIx,	,
gamepad	gamepad	k1gInSc1	gamepad
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
)	)	kIx)	)
vstoupit	vstoupit	k5eAaPmF	vstoupit
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
dění	dění	k1gNnSc4	dění
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
interakci	interakce	k1gFnSc3	interakce
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zásahu	zásah	k1gInSc2	zásah
hráče	hráč	k1gMnSc2	hráč
do	do	k7c2	do
spuštěného	spuštěný	k2eAgInSc2d1	spuštěný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
zásah	zásah	k1gInSc4	zásah
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
musí	muset	k5eAaImIp3nS	muset
hráč	hráč	k1gMnSc1	hráč
splnit	splnit	k5eAaPmF	splnit
za	za	k7c2	za
daných	daný	k2eAgFnPc2d1	daná
podmínek	podmínka	k1gFnPc2	podmínka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
limitu	limit	k1gInSc6	limit
<g/>
,	,	kIx,	,
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
simulaci	simulace	k1gFnSc6	simulace
sportu	sport	k1gInSc2	sport
či	či	k8xC	či
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jiného	jiný	k2eAgInSc2d1	jiný
cíle	cíl	k1gInSc2	cíl
odvislého	odvislý	k2eAgInSc2d1	odvislý
od	od	k7c2	od
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
žánru	žánr	k1gInSc2	žánr
hry	hra	k1gFnSc2	hra
i	i	k8xC	i
záměru	záměr	k1gInSc2	záměr
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
tzv.	tzv.	kA	tzv.
sandboxové	sandboxový	k2eAgFnPc4d1	sandboxová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
naprosto	naprosto	k6eAd1	naprosto
cokoliv	cokoliv	k3yInSc4	cokoliv
–	–	k?	–
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
reálie	reálie	k1gFnPc1	reálie
a	a	k8xC	a
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
detektivní	detektivní	k2eAgNnSc1d1	detektivní
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
<g/>
,	,	kIx,	,
fantastické	fantastický	k2eAgFnPc1d1	fantastická
bitvy	bitva	k1gFnPc1	bitva
mezi	mezi	k7c7	mezi
smyšlenými	smyšlený	k2eAgInPc7d1	smyšlený
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
deskové	deskový	k2eAgFnPc1d1	desková
hry	hra	k1gFnPc1	hra
atd.	atd.	kA	atd.
Herní	herní	k2eAgInPc1d1	herní
žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nejobecněji	obecně	k6eAd3	obecně
kategorizují	kategorizovat	k5eAaBmIp3nP	kategorizovat
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
žánrových	žánrový	k2eAgFnPc2d1	žánrová
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
titulů	titul	k1gInPc2	titul
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prolínání	prolínání	k1gNnSc3	prolínání
žánrů	žánr	k1gInPc2	žánr
nebo	nebo	k8xC	nebo
vytváření	vytváření	k1gNnSc2	vytváření
specifických	specifický	k2eAgInPc2d1	specifický
podžánrů	podžánr	k1gInPc2	podžánr
<g/>
.	.	kIx.	.
</s>
<s>
Žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
způsobem	způsob	k1gInSc7	způsob
zásahu	zásah	k1gInSc2	zásah
hráče	hráč	k1gMnSc2	hráč
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
nabízenými	nabízený	k2eAgInPc7d1	nabízený
prostředky	prostředek	k1gInPc7	prostředek
programu	program	k1gInSc2	program
k	k	k7c3	k
interakci	interakce	k1gFnSc3	interakce
<g/>
.	.	kIx.	.
akční	akční	k2eAgFnSc1d1	akční
bojovka	bojovka	k1gFnSc1	bojovka
(	(	kIx(	(
<g/>
Beat	beat	k1gInSc1	beat
'	'	kIx"	'
<g/>
em	em	k?	em
up	up	k?	up
<g/>
,	,	kIx,	,
Hack	Hack	k1gMnSc1	Hack
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
slash	slash	k1gInSc1	slash
<g/>
)	)	kIx)	)
plošinovka	plošinovka	k1gFnSc1	plošinovka
MOBA	MOBA	kA	MOBA
akční	akční	k2eAgFnSc1d1	akční
adventura	adventura	k1gFnSc1	adventura
stealth	stealtha	k1gFnPc2	stealtha
survival	survivat	k5eAaPmAgInS	survivat
horor	horor	k1gInSc1	horor
adventura	adventura	k1gFnSc1	adventura
textová	textový	k2eAgFnSc1d1	textová
adventura	adventura	k1gFnSc1	adventura
grafická	grafický	k2eAgFnSc1d1	grafická
textovka	textovka	k1gFnSc1	textovka
grafická	grafický	k2eAgFnSc1d1	grafická
point	pointa	k1gFnPc2	pointa
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
click	click	k1gInSc1	click
adventura	adventura	k1gFnSc1	adventura
3D	[number]	k4	3D
adventura	adventura	k1gFnSc1	adventura
Vizuální	vizuální	k2eAgFnSc2d1	vizuální
román	román	k1gInSc4	román
Interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
film	film	k1gInSc1	film
střílečka	střílečka	k1gFnSc1	střílečka
FPS	FPS	kA	FPS
(	(	kIx(	(
<g/>
střílečka	střílečka	k1gFnSc1	střílečka
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
TPS	TPS	kA	TPS
(	(	kIx(	(
<g/>
střílečka	střílečka	k1gFnSc1	střílečka
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
MMOFPS	MMOFPS	kA	MMOFPS
Shoot	Shoot	k1gInSc1	Shoot
'	'	kIx"	'
<g/>
em	em	k?	em
up	up	k?	up
Taktická	taktický	k2eAgFnSc1d1	taktická
střílečka	střílečka	k1gFnSc1	střílečka
strategie	strategie	k1gFnSc2	strategie
tahová	tahový	k2eAgFnSc1d1	tahová
strategie	strategie	k1gFnSc1	strategie
(	(	kIx(	(
<g/>
TBS	TBS	kA	TBS
<g/>
)	)	kIx)	)
realtimová	realtimový	k2eAgFnSc1d1	realtimová
strategie	strategie	k1gFnSc1	strategie
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
RTS	RTS	kA	RTS
<g/>
)	)	kIx)	)
budovatelská	budovatelský	k2eAgFnSc1d1	budovatelská
strategie	strategie	k1gFnSc1	strategie
tower	tower	k1gInSc1	tower
defense	defense	k1gFnSc1	defense
(	(	kIx(	(
<g/>
TD	TD	kA	TD
<g/>
)	)	kIx)	)
simulátor	simulátor	k1gInSc4	simulátor
simulátor	simulátor	k1gInSc1	simulátor
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Sims	Sims	k1gInSc1	Sims
<g/>
,	,	kIx,	,
Second	Second	k1gMnSc1	Second
Life	Lif	k1gFnSc2	Lif
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
dopravní	dopravní	k2eAgInSc1d1	dopravní
simulátor	simulátor	k1gInSc1	simulátor
(	(	kIx(	(
<g/>
letecký	letecký	k2eAgInSc1d1	letecký
<g/>
,	,	kIx,	,
vlakový	vlakový	k2eAgInSc1d1	vlakový
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
virtuální	virtuální	k2eAgNnSc4d1	virtuální
bojiště	bojiště	k1gNnSc4	bojiště
(	(	kIx(	(
<g/>
simuluje	simulovat	k5eAaImIp3nS	simulovat
bojiště	bojiště	k1gNnSc4	bojiště
složené	složený	k2eAgNnSc4d1	složené
nejméně	málo	k6eAd3	málo
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
<g />
.	.	kIx.	.
</s>
<s>
armád	armáda	k1gFnPc2	armáda
–	–	k?	–
pozemní	pozemní	k2eAgFnSc2d1	pozemní
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgFnSc2d1	letecká
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgMnSc1d1	námořní
<g/>
)	)	kIx)	)
sportovní	sportovní	k2eAgMnSc1d1	sportovní
závodní	závodní	k1gMnSc1	závodní
týmové	týmový	k2eAgFnSc2d1	týmová
kompetitivní	kompetitivní	k2eAgFnSc2d1	kompetitivní
sportovně	sportovně	k6eAd1	sportovně
založené	založený	k2eAgInPc4d1	založený
souboje	souboj	k1gInPc4	souboj
(	(	kIx(	(
<g/>
box	box	k1gInSc4	box
<g/>
,	,	kIx,	,
wrestling	wrestling	k1gInSc4	wrestling
<g/>
)	)	kIx)	)
sportovní	sportovní	k2eAgInSc4d1	sportovní
manažer	manažer	k1gInSc4	manažer
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
(	(	kIx(	(
<g/>
RPG	RPG	kA	RPG
<g/>
)	)	kIx)	)
akční	akční	k2eAgMnSc1d1	akční
RPG	RPG	kA	RPG
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
diablovky	diablovka	k1gFnPc4	diablovka
<g/>
"	"	kIx"	"
-	-	kIx~	-
akce	akce	k1gFnSc1	akce
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
postavy	postava	k1gFnSc2	postava
<g />
.	.	kIx.	.
</s>
<s>
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
příběhem	příběh	k1gInSc7	příběh
epické	epický	k2eAgFnSc2d1	epická
RPG	RPG	kA	RPG
-	-	kIx~	-
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
silnou	silný	k2eAgFnSc7d1	silná
stránkou	stránka	k1gFnSc7	stránka
hry	hra	k1gFnSc2	hra
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
Massive	Massiev	k1gFnSc2	Massiev
multiplayer	multiplayer	k1gInSc1	multiplayer
online	onlinout	k5eAaPmIp3nS	onlinout
role	role	k1gFnPc4	role
play	play	k0	play
game	game	k1gInSc1	game
–	–	k?	–
masivně	masivně	k6eAd1	masivně
multiplayerová	multiplayerový	k2eAgFnSc1d1	multiplayerová
online	onlinout	k5eAaPmIp3nS	onlinout
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
)	)	kIx)	)
ostatní	ostatní	k2eAgFnSc1d1	ostatní
arkáda	arkáda	k1gFnSc1	arkáda
logické	logický	k2eAgFnSc2d1	logická
hudební	hudební	k2eAgFnSc2d1	hudební
deskové	deskový	k2eAgFnSc2d1	desková
/	/	kIx~	/
karetní	karetní	k2eAgFnSc2d1	karetní
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
počítači	počítač	k1gInSc3	počítač
nebo	nebo	k8xC	nebo
online	onlinout	k5eAaPmIp3nS	onlinout
po	po	k7c6	po
síti	síť	k1gFnSc6	síť
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
příležitostné	příležitostný	k2eAgMnPc4d1	příležitostný
hráče	hráč	k1gMnPc4	hráč
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
sesíťování	sesíťování	k1gNnSc2	sesíťování
<g/>
"	"	kIx"	"
všech	všecek	k3xTgMnPc2	všecek
svých	svůj	k3xOyFgMnPc2	svůj
počítačů	počítač	k1gMnPc2	počítač
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
internetu	internet	k1gInSc2	internet
<g/>
)	)	kIx)	)
a	a	k8xC	a
napojením	napojení	k1gNnSc7	napojení
<g/>
/	/	kIx~	/
<g/>
přihlášením	přihlášení	k1gNnSc7	přihlášení
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
v	v	k7c6	v
programu	program	k1gInSc6	program
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
tytéž	týž	k3xTgFnPc1	týž
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
kooperativního	kooperativní	k2eAgNnSc2d1	kooperativní
jednání	jednání	k1gNnSc2	jednání
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
jako	jako	k9	jako
protivníci	protivník	k1gMnPc1	protivník
či	či	k8xC	či
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
hrách	hra	k1gFnPc6	hra
i	i	k9	i
rozličné	rozličný	k2eAgInPc4d1	rozličný
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
nebo	nebo	k8xC	nebo
spřátelené	spřátelený	k2eAgInPc4d1	spřátelený
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
o	o	k7c4	o
více	hodně	k6eAd2	hodně
hráčích	hráč	k1gMnPc6	hráč
(	(	kIx(	(
<g/>
Multiplayer	Multiplayer	k1gInSc1	Multiplayer
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInPc1d1	populární
mezi	mezi	k7c7	mezi
počítačovými	počítačový	k2eAgMnPc7d1	počítačový
hráči	hráč	k1gMnPc7	hráč
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgMnSc1d1	lidský
protivník	protivník	k1gMnSc1	protivník
či	či	k8xC	či
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
atraktivnější	atraktivní	k2eAgFnSc7d2	atraktivnější
volbou	volba	k1gFnSc7	volba
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
počítačem	počítač	k1gInSc7	počítač
ovládaný	ovládaný	k2eAgInSc4d1	ovládaný
subjekt	subjekt	k1gInSc4	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
nabízí	nabízet	k5eAaImIp3nP	nabízet
spektrum	spektrum	k1gNnSc4	spektrum
nápadů	nápad	k1gInPc2	nápad
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
využití	využití	k1gNnSc2	využití
herního	herní	k2eAgNnSc2d1	herní
prostředí	prostředí	k1gNnSc2	prostředí
hráčům	hráč	k1gMnPc3	hráč
<g/>
,	,	kIx,	,
odvíjející	odvíjející	k2eAgNnSc4d1	odvíjející
se	se	k3xPyFc4	se
od	od	k7c2	od
žánru	žánr	k1gInSc2	žánr
i	i	k8xC	i
technologického	technologický	k2eAgNnSc2d1	Technologické
zpracování	zpracování	k1gNnSc2	zpracování
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
každá	každý	k3xTgFnSc1	každý
komerčně	komerčně	k6eAd1	komerčně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
hra	hra	k1gFnSc1	hra
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
hry	hra	k1gFnSc2	hra
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Online	Onlin	k1gInSc5	Onlin
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hraní	hraní	k1gNnSc4	hraní
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
specificky	specificky	k6eAd1	specificky
pro	pro	k7c4	pro
takovou	takový	k3xDgFnSc4	takový
hru	hra	k1gFnSc4	hra
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nabízí	nabízet	k5eAaImIp3nS	nabízet
online	onlinout	k5eAaPmIp3nS	onlinout
hru	hra	k1gFnSc4	hra
jako	jako	k9	jako
druhou	druhý	k4xOgFnSc4	druhý
možnost	možnost	k1gFnSc4	možnost
vedle	vedle	k7c2	vedle
hry	hra	k1gFnSc2	hra
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
kategorii	kategorie	k1gFnSc3	kategorie
<g/>
:	:	kIx,	:
MOBA	MOBA	kA	MOBA
(	(	kIx(	(
<g/>
Multiplayer	Multiplayer	k1gMnSc1	Multiplayer
online	onlinout	k5eAaPmIp3nS	onlinout
battle	battle	k6eAd1	battle
arena	arena	k1gFnSc1	arena
<g/>
)	)	kIx)	)
MUD	MUD	kA	MUD
(	(	kIx(	(
<g/>
Multi-User	Multi-User	k1gMnSc1	Multi-User
Dungeon	Dungeon	k1gMnSc1	Dungeon
<g/>
)	)	kIx)	)
MMOG	MMOG	kA	MMOG
(	(	kIx(	(
<g/>
Massively-Multiplayer	Massively-Multiplayer	k1gMnSc1	Massively-Multiplayer
Online	Onlin	k1gInSc5	Onlin
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
Massively-Multiplayer	Massively-Multiplayer	k1gMnSc1	Massively-Multiplayer
Online	Onlin	k1gInSc5	Onlin
Role	role	k1gFnSc1	role
Playing	Playing	k1gInSc4	Playing
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
MMORTS	MMORTS	kA	MMORTS
(	(	kIx(	(
<g/>
Massively-Multiplayer	Massively-Multiplayer	k1gMnSc1	Massively-Multiplayer
Online	Onlin	k1gInSc5	Onlin
Real-Time	Real-Tim	k1gInSc5	Real-Tim
Strategy	Strateg	k1gInPc7	Strateg
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
MMOFPS	MMOFPS	kA	MMOFPS
(	(	kIx(	(
<g/>
Massively-Multiplayer	Massively-Multiplayer	k1gMnSc1	Massively-Multiplayer
Online	Onlin	k1gInSc5	Onlin
First	First	k1gMnSc1	First
Person	persona	k1gFnPc2	persona
Shooter	Shooter	k1gMnSc1	Shooter
<g/>
)	)	kIx)	)
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
může	moct	k5eAaImIp3nS	moct
až	až	k9	až
několik	několik	k4yIc1	několik
hráčů	hráč	k1gMnPc2	hráč
najednou	najednou	k6eAd1	najednou
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
až	až	k9	až
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
limitován	limitovat	k5eAaBmNgInS	limitovat
samotnou	samotný	k2eAgFnSc7d1	samotná
hrou	hra	k1gFnSc7	hra
nebo	nebo	k8xC	nebo
množstvím	množství	k1gNnSc7	množství
ovládacích	ovládací	k2eAgNnPc2d1	ovládací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc7	jejich
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
počítači	počítač	k1gInSc3	počítač
připojit	připojit	k5eAaPmF	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
hraní	hranit	k5eAaImIp3nS	hranit
už	už	k6eAd1	už
však	však	k9	však
dnes	dnes	k6eAd1	dnes
u	u	k7c2	u
komerčních	komerční	k2eAgInPc2d1	komerční
titulů	titul	k1gInPc2	titul
nebývá	bývat	k5eNaImIp3nS	bývat
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
značné	značný	k2eAgNnSc4d1	značné
nepohodlí	nepohodlí	k1gNnSc4	nepohodlí
několika	několik	k4yIc2	několik
hráčů	hráč	k1gMnPc2	hráč
tísnících	tísnící	k2eAgMnPc2d1	tísnící
se	se	k3xPyFc4	se
u	u	k7c2	u
jedné	jeden	k4xCgFnSc2	jeden
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
doménou	doména	k1gFnSc7	doména
hlavně	hlavně	k6eAd1	hlavně
starších	starý	k2eAgFnPc2d2	starší
akčních	akční	k2eAgFnPc2d1	akční
arkádových	arkádový	k2eAgFnPc2d1	arkádová
her	hra	k1gFnPc2	hra
např.	např.	kA	např.
typu	typa	k1gFnSc4	typa
Dyna	dyna	k1gFnSc1	dyna
Blaster	Blastra	k1gFnPc2	Blastra
nebo	nebo	k8xC	nebo
závodů	závod	k1gInPc2	závod
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lotus	Lotus	kA	Lotus
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
či	či	k8xC	či
mód	mód	k1gInSc4	mód
hraní	hraní	k1gNnSc2	hraní
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
hráčích	hráč	k1gMnPc6	hráč
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
využíván	využívat	k5eAaImNgInS	využívat
zejména	zejména	k9	zejména
u	u	k7c2	u
her	hra	k1gFnPc2	hra
hraných	hraný	k2eAgFnPc2d1	hraná
po	po	k7c6	po
kolech	kolo	k1gNnPc6	kolo
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
strategických	strategický	k2eAgFnPc2d1	strategická
her	hra	k1gFnPc2	hra
jako	jako	k8xC	jako
Heroes	Heroesa	k1gFnPc2	Heroesa
of	of	k?	of
Might	Might	k1gMnSc1	Might
and	and	k?	and
Magic	Magic	k1gMnSc1	Magic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výhoda	výhoda	k1gFnSc1	výhoda
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
hraní	hraní	k1gNnSc2	hraní
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
neúměrně	úměrně	k6eNd1	úměrně
protahovat	protahovat	k5eAaImF	protahovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
ošetřeno	ošetřen	k2eAgNnSc4d1	ošetřeno
např.	např.	kA	např.
časovým	časový	k2eAgInSc7d1	časový
limitem	limit	k1gInSc7	limit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
sledují	sledovat	k5eAaImIp3nP	sledovat
tahy	tah	k1gInPc1	tah
protivníka	protivník	k1gMnSc2	protivník
či	či	k8xC	či
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Split	Split	k1gInSc1	Split
screen	screen	k1gInSc1	screen
je	být	k5eAaImIp3nS	být
multiplayerový	multiplayerový	k2eAgInSc1d1	multiplayerový
mód	mód	k1gInSc1	mód
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ještě	ještě	k6eAd1	ještě
nepodporovaly	podporovat	k5eNaImAgFnP	podporovat
hraní	hraň	k1gFnSc7	hraň
po	po	k7c6	po
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
rozdělení	rozdělení	k1gNnSc6	rozdělení
obrazovky	obrazovka	k1gFnSc2	obrazovka
do	do	k7c2	do
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
pak	pak	k6eAd1	pak
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
svou	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Obrazovka	obrazovka	k1gFnSc1	obrazovka
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
her	hra	k1gFnPc2	hra
ani	ani	k8xC	ani
dělit	dělit	k5eAaImF	dělit
nemusí	muset	k5eNaImIp3nS	muset
<g/>
,	,	kIx,	,
sdílí	sdílet	k5eAaImIp3nS	sdílet
<g/>
-li	i	k?	-li
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc7	hráč
stejný	stejný	k2eAgInSc4d1	stejný
pohled	pohled	k1gInSc4	pohled
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dyna	dyna	k1gFnSc1	dyna
blaster	blaster	k1gMnSc1	blaster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrají	hrát	k5eAaImIp3nP	hrát
všichni	všechen	k3xTgMnPc1	všechen
najednou	najednou	k6eAd1	najednou
<g/>
:	:	kIx,	:
nestřídají	střídat	k5eNaImIp3nP	střídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Bot	bota	k1gFnPc2	bota
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
z	z	k7c2	z
"	"	kIx"	"
<g/>
robot	robot	k1gMnSc1	robot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
programový	programový	k2eAgMnSc1d1	programový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počítačem	počítač	k1gInSc7	počítač
simulovaný	simulovaný	k2eAgMnSc1d1	simulovaný
hráč	hráč	k1gMnSc1	hráč
užívaný	užívaný	k2eAgMnSc1d1	užívaný
k	k	k7c3	k
tréninku	trénink	k1gInSc3	trénink
nebo	nebo	k8xC	nebo
i	i	k9	i
k	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
<g/>
,	,	kIx,	,
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
možnost	možnost	k1gFnSc4	možnost
živých	živý	k2eAgMnPc2d1	živý
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
AI	AI	kA	AI
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
primitivní	primitivní	k2eAgInPc4d1	primitivní
(	(	kIx(	(
<g/>
a	a	k8xC	a
jako	jako	k9	jako
protihráči	protihráč	k1gMnPc1	protihráč
jen	jen	k6eAd1	jen
slabí	slabit	k5eAaImIp3nP	slabit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
hráči	hráč	k1gMnSc3	hráč
(	(	kIx(	(
<g/>
human	human	k1gMnSc1	human
player	player	k1gMnSc1	player
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výhodě	výhoda	k1gFnSc6	výhoda
<g/>
:	:	kIx,	:
rychlostí	rychlost	k1gFnPc2	rychlost
zpracování	zpracování	k1gNnSc2	zpracování
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgMnPc3	který
hráč	hráč	k1gMnSc1	hráč
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
uplatněním	uplatnění	k1gNnSc7	uplatnění
pravidel	pravidlo	k1gNnPc2	pravidlo
platným	platný	k2eAgMnSc7d1	platný
jen	jen	k9	jen
pro	pro	k7c4	pro
počítačem	počítač	k1gInSc7	počítač
řízené	řízený	k2eAgFnSc2d1	řízená
postavy	postava	k1gFnSc2	postava
<g/>
...	...	k?	...
Mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
mít	mít	k5eAaImF	mít
nastavitelné	nastavitelný	k2eAgInPc4d1	nastavitelný
atributy	atribut	k1gInPc4	atribut
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
pak	pak	k6eAd1	pak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
vyváženost	vyváženost	k1gFnSc4	vyváženost
obtížnosti	obtížnost	k1gFnSc2	obtížnost
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
hratelnost	hratelnost	k1gFnSc1	hratelnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
a	a	k8xC	a
bot	bota	k1gFnPc2	bota
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
hrané	hraný	k2eAgFnPc1d1	hraná
po	po	k7c6	po
místní	místní	k2eAgFnSc6d1	místní
síti	síť	k1gFnSc6	síť
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
hráči	hráč	k1gMnPc1	hráč
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
(	(	kIx(	(
<g/>
PvP	PvP	k1gFnSc1	PvP
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
spolu	spolu	k6eAd1	spolu
proti	proti	k7c3	proti
postavám	postava	k1gFnPc3	postava
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
možných	možný	k2eAgMnPc2d1	možný
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
enginem	engino	k1gNnSc7	engino
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
protokol	protokol	k1gInSc4	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
zakládá	zakládat	k5eAaImIp3nS	zakládat
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
hostitelem	hostitel	k1gMnSc7	hostitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojují	připojovat	k5eAaImIp3nP	připojovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
klienti	klient	k1gMnPc1	klient
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hostitel	hostitel	k1gMnSc1	hostitel
přednastavuje	přednastavovat	k5eAaImIp3nS	přednastavovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hry	hra	k1gFnSc2	hra
buď	buď	k8xC	buď
v	v	k7c6	v
menu	menu	k1gNnSc6	menu
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
konzoli	konzole	k1gFnSc4	konzole
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc4d1	moderní
hry	hra	k1gFnPc4	hra
umožňující	umožňující	k2eAgInSc1d1	umožňující
multiplayer	multiplayer	k1gInSc1	multiplayer
po	po	k7c6	po
LAN	lano	k1gNnPc2	lano
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
často	často	k6eAd1	často
mívají	mívat	k5eAaImIp3nP	mívat
implementovanou	implementovaný	k2eAgFnSc4d1	implementovaná
možnost	možnost	k1gFnSc4	možnost
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
samostatného	samostatný	k2eAgInSc2d1	samostatný
serveru	server	k1gInSc2	server
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
herního	herní	k2eAgNnSc2d1	herní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Dedicated	Dedicated	k1gInSc1	Dedicated
server	server	k1gInSc1	server
v	v	k7c6	v
Counter-Strike	Counter-Strike	k1gFnSc6	Counter-Strike
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Local	Local	k1gInSc1	Local
Area	Ares	k1gMnSc4	Ares
Network	network	k1gInSc2	network
<g/>
.	.	kIx.	.
</s>
<s>
Hráčův	hráčův	k2eAgInSc1d1	hráčův
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
snímá	snímat	k5eAaImIp3nS	snímat
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
statická	statický	k2eAgFnSc1d1	statická
nebo	nebo	k8xC	nebo
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
typu	typ	k1gInSc6	typ
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc6	nastavení
nebo	nebo	k8xC	nebo
grafickém	grafický	k2eAgNnSc6d1	grafické
enginu	engino	k1gNnSc6	engino
<g/>
.	.	kIx.	.
</s>
<s>
Záběry	záběr	k1gInPc1	záběr
kamery	kamera	k1gFnSc2	kamera
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
přenášeny	přenášet	k5eAaImNgInP	přenášet
na	na	k7c4	na
hráčův	hráčův	k2eAgInSc4d1	hráčův
monitor	monitor	k1gInSc4	monitor
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
dění	dění	k1gNnSc2	dění
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
statické	statický	k2eAgFnSc2d1	statická
kamery	kamera	k1gFnSc2	kamera
jsou	být	k5eAaImIp3nP	být
přednastaveny	přednastavit	k5eAaPmNgFnP	přednastavit
její	její	k3xOp3gFnPc1	její
polohy	poloha	k1gFnPc1	poloha
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
přepínat	přepínat	k5eAaImF	přepínat
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
pohledy	pohled	k1gInPc7	pohled
například	například	k6eAd1	například
klávesou	klávesa	k1gFnSc7	klávesa
a	a	k8xC	a
zvolit	zvolit	k5eAaPmF	zvolit
si	se	k3xPyFc3	se
tak	tak	k9	tak
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
ideální	ideální	k2eAgInSc1d1	ideální
pohled	pohled	k1gInSc1	pohled
(	(	kIx(	(
<g/>
statické	statický	k2eAgFnSc2d1	statická
kamery	kamera	k1gFnSc2	kamera
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
zejména	zejména	k9	zejména
u	u	k7c2	u
závodních	závodní	k2eAgFnPc2d1	závodní
her	hra	k1gFnPc2	hra
-	-	kIx~	-
pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
nárazníku	nárazník	k1gInSc2	nárazník
<g/>
,	,	kIx,	,
z	z	k7c2	z
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
zpoza	zpoza	k7c2	zpoza
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
kamera	kamera	k1gFnSc1	kamera
je	být	k5eAaImIp3nS	být
ovládána	ovládán	k2eAgFnSc1d1	ovládána
hráčem	hráč	k1gMnSc7	hráč
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
určuje	určovat	k5eAaImIp3nS	určovat
její	její	k3xOp3gFnSc4	její
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
úhel	úhel	k1gInSc4	úhel
natočení	natočení	k1gNnSc2	natočení
většinou	většinou	k6eAd1	většinou
pomocí	pomocí	k7c2	pomocí
pohybu	pohyb	k1gInSc2	pohyb
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
zejména	zejména	k9	zejména
u	u	k7c2	u
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
hráč	hráč	k1gMnSc1	hráč
velkou	velký	k2eAgFnSc4d1	velká
volnost	volnost	k1gFnSc4	volnost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
MMORPG	MMORPG	kA	MMORPG
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
kombinací	kombinace	k1gFnSc7	kombinace
statické	statický	k2eAgFnSc2d1	statická
a	a	k8xC	a
plovoucí	plovoucí	k2eAgFnSc2d1	plovoucí
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
dán	dát	k5eAaPmNgInS	dát
úhel	úhel	k1gInSc1	úhel
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kameru	kamera	k1gFnSc4	kamera
lze	lze	k6eAd1	lze
přibližovat	přibližovat	k5eAaImF	přibližovat
a	a	k8xC	a
oddalovat	oddalovat	k5eAaImF	oddalovat
nebo	nebo	k8xC	nebo
nelze	lze	k6eNd1	lze
zoomovat	zoomovat	k5eAaImF	zoomovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
rotovat	rotovat	k5eAaImF	rotovat
pod	pod	k7c7	pod
pevným	pevný	k2eAgInSc7d1	pevný
úhlem	úhel	k1gInSc7	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
pro	pro	k7c4	pro
typ	typ	k1gInSc4	typ
kamery	kamera	k1gFnSc2	kamera
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
First	First	k1gFnSc1	First
person	persona	k1gFnPc2	persona
-	-	kIx~	-
pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
hrané	hraný	k2eAgFnSc2d1	hraná
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dojem	dojem	k1gInSc4	dojem
jako	jako	k8xC	jako
byste	by	kYmCp2nP	by
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Third	Third	k1gInSc1	Third
person	persona	k1gFnPc2	persona
-	-	kIx~	-
pohled	pohled	k1gInSc1	pohled
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
vidíte	vidět	k5eAaImIp2nP	vidět
ovládanou	ovládaný	k2eAgFnSc4d1	ovládaná
postavu	postava	k1gFnSc4	postava
před	před	k7c7	před
<g/>
/	/	kIx~	/
<g/>
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
větší	veliký	k2eAgInSc4d2	veliký
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
děje	dít	k5eAaImIp3nS	dít
kolem	kolem	k7c2	kolem
Vás	vy	k3xPp2nPc2	vy
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgNnSc1d1	grafické
zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
určující	určující	k2eAgFnSc7d1	určující
součástí	součást	k1gFnSc7	součást
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
její	její	k3xOp3gInSc4	její
obsah	obsah	k1gInSc4	obsah
z	z	k7c2	z
vizuálního	vizuální	k2eAgNnSc2d1	vizuální
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
ryze	ryze	k6eAd1	ryze
textové	textový	k2eAgFnSc6d1	textová
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
grafické	grafický	k2eAgNnSc4d1	grafické
znázornění	znázornění	k1gNnSc4	znázornění
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
moderní	moderní	k2eAgFnPc1d1	moderní
hry	hra	k1gFnPc1	hra
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
díky	díky	k7c3	díky
množství	množství	k1gNnSc3	množství
efektů	efekt	k1gInPc2	efekt
vysoké	vysoký	k2eAgInPc4d1	vysoký
nároky	nárok	k1gInPc4	nárok
zejména	zejména	k9	zejména
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
grafické	grafický	k2eAgFnSc2d1	grafická
karty	karta	k1gFnSc2	karta
<g/>
.	.	kIx.	.
</s>
<s>
Zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
2D	[number]	k4	2D
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
D.	D.	kA	D.
jeden	jeden	k4xCgInSc4	jeden
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
a	a	k8xC	a
detailní	detailní	k2eAgInSc4d1	detailní
svět	svět	k1gInSc4	svět
–	–	k?	–
RPG	RPG	kA	RPG
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
adventury	adventura	k1gFnSc2	adventura
<g/>
,	,	kIx,	,
akční	akční	k2eAgFnSc2d1	akční
hry	hra	k1gFnSc2	hra
okruhy	okruh	k1gInPc1	okruh
<g/>
,	,	kIx,	,
stadiony	stadion	k1gInPc1	stadion
atp.	atp.	kA	atp.
–	–	k?	–
sportovní	sportovní	k2eAgFnSc2d1	sportovní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
závodní	závodní	k2eAgFnPc1d1	závodní
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
simulátory	simulátor	k1gInPc1	simulátor
početné	početný	k2eAgFnSc2d1	početná
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
lokace	lokace	k1gFnSc2	lokace
–	–	k?	–
simulátory	simulátor	k1gInPc1	simulátor
<g/>
,	,	kIx,	,
adventury	adventura	k1gFnPc1	adventura
<g/>
,	,	kIx,	,
arkády	arkáda	k1gFnPc1	arkáda
<g/>
,	,	kIx,	,
akční	akční	k2eAgFnPc1d1	akční
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
logické	logický	k2eAgFnPc1d1	logická
hry	hra	k1gFnPc1	hra
jedno	jeden	k4xCgNnSc4	jeden
obměňované	obměňovaný	k2eAgNnSc4d1	obměňované
herní	herní	k2eAgNnSc4d1	herní
prostředí	prostředí	k1gNnSc4	prostředí
–	–	k?	–
arkády	arkáda	k1gFnSc2	arkáda
<g/>
,	,	kIx,	,
logické	logický	k2eAgFnSc2d1	logická
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc2d1	sportovní
hry	hra	k1gFnSc2	hra
Rozlehlými	rozlehlý	k2eAgInPc7d1	rozlehlý
světy	svět	k1gInPc7	svět
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
RPG	RPG	kA	RPG
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Arkády	arkáda	k1gFnPc1	arkáda
a	a	k8xC	a
logické	logický	k2eAgFnPc1d1	logická
hry	hra	k1gFnPc1	hra
si	se	k3xPyFc3	se
většinou	většinou	k6eAd1	většinou
vystačí	vystačit	k5eAaBmIp3nP	vystačit
s	s	k7c7	s
neměnným	neměnný	k2eAgNnSc7d1	neměnné
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
obměněným	obměněný	k2eAgMnPc3d1	obměněný
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
např.	např.	kA	např.
na	na	k7c4	na
kola	kolo	k1gNnPc4	kolo
nebo	nebo	k8xC	nebo
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
určitého	určitý	k2eAgInSc2d1	určitý
počtu	počet	k1gInSc2	počet
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arkádách	arkáda	k1gFnPc6	arkáda
<g/>
,	,	kIx,	,
logických	logický	k2eAgFnPc6d1	logická
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
simulacích	simulace	k1gFnPc6	simulace
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
Game	game	k1gInSc1	game
Over	Overa	k1gFnPc2	Overa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
neúspěch	neúspěch	k1gInSc4	neúspěch
–	–	k?	–
Konec	konec	k1gInSc1	konec
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Adventury	adventura	k1gFnPc1	adventura
mívají	mívat	k5eAaImIp3nP	mívat
početné	početný	k2eAgFnPc4d1	početná
lokace	lokace	k1gFnPc4	lokace
k	k	k7c3	k
prozkoumávání	prozkoumávání	k1gNnSc3	prozkoumávání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
RPG	RPG	kA	RPG
hry	hra	k1gFnPc4	hra
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyznačovat	vyznačovat	k5eAaImF	vyznačovat
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Závodní	závodní	k2eAgFnPc1d1	závodní
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
lokacích	lokace	k1gFnPc6	lokace
prostoupených	prostoupený	k2eAgFnPc6d1	prostoupená
dráhou	dráha	k1gFnSc7	dráha
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
vozidla	vozidlo	k1gNnSc2	vozidlo
nebo	nebo	k8xC	nebo
několika	několik	k4yIc7	několik
okruhy	okruh	k1gInPc7	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgInPc1d1	letecký
simulátory	simulátor	k1gInPc1	simulátor
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
logicky	logicky	k6eAd1	logicky
vymezeny	vymezen	k2eAgMnPc4d1	vymezen
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
prostorem	prostor	k1gInSc7	prostor
nad	nad	k7c7	nad
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
lokací	lokace	k1gFnSc7	lokace
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
simulátory	simulátor	k1gInPc1	simulátor
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
lokaci	lokace	k1gFnSc6	lokace
s	s	k7c7	s
detailním	detailní	k2eAgNnSc7d1	detailní
a	a	k8xC	a
uceleným	ucelený	k2eAgNnSc7d1	ucelené
prostranstvím	prostranství	k1gNnSc7	prostranství
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
sportovních	sportovní	k2eAgFnPc2d1	sportovní
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
příznačná	příznačný	k2eAgFnSc1d1	příznačná
–	–	k?	–
stadiony	stadion	k1gInPc4	stadion
<g/>
,	,	kIx,	,
okruhy	okruh	k1gInPc1	okruh
atd.	atd.	kA	atd.
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
popálenin	popálenina	k1gFnPc2	popálenina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
bolestivé	bolestivý	k2eAgFnSc2d1	bolestivá
terapie	terapie	k1gFnSc2	terapie
při	při	k7c6	při
hojení	hojení	k1gNnSc6	hojení
ran	rána	k1gFnPc2	rána
vedle	vedle	k7c2	vedle
nasazení	nasazení	k1gNnSc2	nasazení
opiátů	opiát	k1gInPc2	opiát
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
též	též	k6eAd1	též
hrát	hrát	k5eAaImF	hrát
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnPc4	sloužící
k	k	k7c3	k
odvedení	odvedení	k1gNnSc3	odvedení
jeho	jeho	k3xOp3gFnSc2	jeho
pozornosti	pozornost	k1gFnSc2	pozornost
od	od	k7c2	od
bolesti	bolest	k1gFnSc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
program	program	k1gInSc1	program
Ice	Ice	k1gMnSc1	Ice
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
ledu	led	k1gInSc2	led
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacienti	pacient	k1gMnPc1	pacient
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
připomíná	připomínat	k5eAaImIp3nS	připomínat
zážitek	zážitek	k1gInSc1	zážitek
popálení	popálení	k1gNnSc2	popálení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
program	program	k1gInSc1	program
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
Load	Load	k1gInSc1	Load
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Načíst	načíst	k5eAaPmF	načíst
uloženou	uložený	k2eAgFnSc4d1	uložená
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
Save	Sav	k1gInPc4	Sav
game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
Uložit	uložit	k5eAaPmF	uložit
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
Quick	Quick	k1gInSc1	Quick
save	sav	k1gInSc2	sav
<g/>
/	/	kIx~	/
<g/>
load	load	k6eAd1	load
(	(	kIx(	(
<g/>
Rychlé	Rychlé	k2eAgNnSc1d1	Rychlé
uložení	uložení	k1gNnSc1	uložení
<g/>
/	/	kIx~	/
<g/>
načtení	načtení	k1gNnSc1	načtení
<g/>
)	)	kIx)	)
Single	singl	k1gInSc5	singl
player	player	k1gInSc1	player
(	(	kIx(	(
<g/>
Hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Multiplayer	Multiplayer	k1gInSc1	Multiplayer
(	(	kIx(	(
<g/>
Hra	hra	k1gFnSc1	hra
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
Game	game	k1gInSc1	game
Over	Over	k1gInSc1	Over
(	(	kIx(	(
<g/>
Konec	konec	k1gInSc1	konec
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
Level	level	k1gInSc4	level
(	(	kIx(	(
<g/>
Úroveň	úroveň	k1gFnSc4	úroveň
<g/>
)	)	kIx)	)
Score	Scor	k1gMnSc5	Scor
(	(	kIx(	(
<g/>
Body	bod	k1gInPc4	bod
<g/>
)	)	kIx)	)
Respawn	Respawn	k1gInSc4	Respawn
(	(	kIx(	(
<g/>
obnovení	obnovení	k1gNnSc4	obnovení
postavy	postava	k1gFnSc2	postava
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
a	a	k8xC	a
videoher	videohra	k1gFnPc2	videohra
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgFnPc1d1	počítačová
hry	hra	k1gFnPc1	hra
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
množství	množství	k1gNnSc1	množství
volnosti	volnost	k1gFnSc2	volnost
a	a	k8xC	a
kreativity	kreativita	k1gFnSc2	kreativita
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
,	,	kIx,	,
vytvářením	vytváření	k1gNnSc7	vytváření
umělých	umělý	k2eAgMnPc2d1	umělý
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
takovou	takový	k3xDgFnSc4	takový
etiku	etika	k1gFnSc4	etika
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
definuje	definovat	k5eAaBmIp3nS	definovat
tvůrce	tvůrce	k1gMnSc1	tvůrce
v	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
a	a	k8xC	a
takovou	takový	k3xDgFnSc4	takový
morálku	morálka	k1gFnSc4	morálka
jakou	jaký	k3yRgFnSc4	jaký
zvolí	zvolit	k5eAaPmIp3nS	zvolit
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
typů	typ	k1gInPc2	typ
čistě	čistě	k6eAd1	čistě
akčních	akční	k2eAgFnPc2d1	akční
her	hra	k1gFnPc2	hra
jde	jít	k5eAaImIp3nS	jít
prvořadě	prvořadě	k6eAd1	prvořadě
o	o	k7c4	o
účelové	účelový	k2eAgNnSc4d1	účelové
násilí	násilí	k1gNnSc4	násilí
nebo	nebo	k8xC	nebo
zabíjení	zabíjení	k1gNnSc4	zabíjení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
možnostmi	možnost	k1gFnPc7	možnost
technologií	technologie	k1gFnPc2	technologie
se	se	k3xPyFc4	se
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
realistické	realistický	k2eAgNnSc1d1	realistické
ztvárnění	ztvárnění	k1gNnSc1	ztvárnění
smrti	smrt	k1gFnSc2	smrt
adekvátní	adekvátní	k2eAgFnSc2d1	adekvátní
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
zabití	zabití	k1gNnSc2	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
takových	takový	k3xDgFnPc2	takový
her	hra	k1gFnPc2	hra
pak	pak	k6eAd1	pak
otupí	otupit	k5eAaPmIp3nS	otupit
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Metaanalýza	Metaanalýza	k1gFnSc1	Metaanalýza
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraní	hraní	k1gNnSc4	hraní
násilných	násilný	k2eAgFnPc2d1	násilná
her	hra	k1gFnPc2	hra
působuje	působovat	k5eAaImIp3nS	působovat
zvýšení	zvýšení	k1gNnSc1	zvýšení
agresivity	agresivita	k1gFnSc2	agresivita
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
empatie	empatie	k1gFnSc2	empatie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
a	a	k8xC	a
zpřístupněním	zpřístupnění	k1gNnSc7	zpřístupnění
internetu	internet	k1gInSc2	internet
masám	masa	k1gFnPc3	masa
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
online	onlinout	k5eAaPmIp3nS	onlinout
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
MMORPG	MMORPG	kA	MMORPG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hráčům	hráč	k1gMnPc3	hráč
žít	žít	k5eAaImF	žít
život	život	k1gInSc4	život
virtuálního	virtuální	k2eAgInSc2d1	virtuální
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
avatara	avatara	k1gFnSc1	avatara
<g/>
)	)	kIx)	)
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
kooperace	kooperace	k1gFnSc2	kooperace
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
napojením	napojení	k1gNnPc3	napojení
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnSc7	součást
virtuálního	virtuální	k2eAgInSc2d1	virtuální
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vesměs	vesměs	k6eAd1	vesměs
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
morálku	morálka	k1gFnSc4	morálka
si	se	k3xPyFc3	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
avataři	avatař	k1gMnPc1	avatař
sami	sám	k3xTgMnPc1	sám
a	a	k8xC	a
často	často	k6eAd1	často
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
<g/>
,	,	kIx,	,
než	než	k8xS	než
znají	znát	k5eAaImIp3nP	znát
ze	z	k7c2	z
skutečného	skutečný	k2eAgInSc2d1	skutečný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
spojeným	spojený	k2eAgMnSc7d1	spojený
s	s	k7c7	s
MMORPG	MMORPG	kA	MMORPG
je	být	k5eAaImIp3nS	být
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gFnSc7	jejich
časovou	časový	k2eAgFnSc7d1	časová
náročností	náročnost	k1gFnSc7	náročnost
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
počítačových	počítačový	k2eAgFnPc6d1	počítačová
hrách	hra	k1gFnPc6	hra
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
kondici	kondice	k1gFnSc4	kondice
<g/>
.	.	kIx.	.
</s>
