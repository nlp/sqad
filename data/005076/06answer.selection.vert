<s>
Hry	hra	k1gFnPc1	hra
o	o	k7c4	o
více	hodně	k6eAd2	hodně
hráčích	hráč	k1gMnPc6	hráč
(	(	kIx(	(
<g/>
Multiplayer	Multiplayer	k1gInSc1	Multiplayer
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInPc1d1	populární
mezi	mezi	k7c7	mezi
počítačovými	počítačový	k2eAgMnPc7d1	počítačový
hráči	hráč	k1gMnPc7	hráč
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgMnSc1d1	lidský
protivník	protivník	k1gMnSc1	protivník
či	či	k8xC	či
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
atraktivnější	atraktivní	k2eAgFnSc7d2	atraktivnější
volbou	volba	k1gFnSc7	volba
ve	v	k7c6	v
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
počítačem	počítač	k1gInSc7	počítač
ovládaný	ovládaný	k2eAgInSc4d1	ovládaný
subjekt	subjekt	k1gInSc4	subjekt
<g/>
.	.	kIx.	.
</s>
