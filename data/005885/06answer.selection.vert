<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
nebo	nebo	k8xC	nebo
zastarale	zastarale	k6eAd1	zastarale
stálice	stálice	k1gFnSc1	stálice
je	být	k5eAaImIp3nS	být
plazmové	plazmový	k2eAgNnSc1d1	plazmové
(	(	kIx(	(
<g/>
plynné	plynný	k2eAgNnSc1d1	plynné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
kulovité	kulovitý	k2eAgNnSc1d1	kulovité
těleso	těleso	k1gNnSc1	těleso
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
zdroj	zdroj	k1gInSc4	zdroj
viditelného	viditelný	k2eAgNnSc2d1	viditelné
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
ho	on	k3xPp3gMnSc4	on
pohromadě	pohromadě	k6eAd1	pohromadě
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
0,08	[number]	k4	0,08
až	až	k8xS	až
300	[number]	k4	300
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
