<s>
Public	publicum	k1gNnPc2	publicum
Enemy	Enem	k1gInPc7	Enem
nebo	nebo	k8xC	nebo
také	také	k9	také
PE	PE	kA	PE
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hiphopová	hiphopový	k2eAgFnSc1d1	hiphopová
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
formace	formace	k1gFnSc1	formace
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
hiphopové	hiphopový	k2eAgFnPc4d1	hiphopová
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnSc4	jejich
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
veřejný	veřejný	k2eAgMnSc1d1	veřejný
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
mají	mít	k5eAaImIp3nP	mít
člena	člen	k1gMnSc2	člen
strany	strana	k1gFnSc2	strana
Černých	Černých	k2eAgMnPc2d1	Černých
Panterů	panter	k1gMnPc2	panter
v	v	k7c6	v
hledáčku	hledáček	k1gInSc6	hledáček
zaměřovače	zaměřovač	k1gInSc2	zaměřovač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
magazín	magazín	k1gInSc1	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
zařadil	zařadit	k5eAaPmAgInS	zařadit
na	na	k7c4	na
44	[number]	k4	44
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
skupin	skupina	k1gFnPc2	skupina
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
acclaimedmusic	acclaimedmusice	k1gInPc2	acclaimedmusice
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
jim	on	k3xPp3gInPc3	on
přisoudil	přisoudit	k5eAaPmAgInS	přisoudit
29	[number]	k4	29
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
hudebníci	hudebník	k1gMnPc1	hudebník
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
hiphopovou	hiphopový	k2eAgFnSc7d1	hiphopová
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
do	do	k7c2	do
Long	Longa	k1gFnPc2	Longa
Islandské	islandský	k2eAgFnSc2d1	islandská
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
Long	Longa	k1gFnPc2	Longa
Island	Island	k1gInSc1	Island
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Produkční	produkční	k2eAgFnSc4d1	produkční
sekci	sekce	k1gFnSc4	sekce
Bomb	bomba	k1gFnPc2	bomba
Squad	Squad	k1gInSc4	Squad
tvoří	tvořit	k5eAaImIp3nP	tvořit
bratři	bratr	k1gMnPc1	bratr
Keith	Keith	k1gMnSc1	Keith
a	a	k8xC	a
Hank	Hank	k1gMnSc1	Hank
Shockley	Shocklea	k1gFnSc2	Shocklea
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Sadler	Sadler	k1gMnSc1	Sadler
a	a	k8xC	a
Chuck	Chuck	k1gMnSc1	Chuck
D	D	kA	D
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc4d1	taneční
skupinu	skupina	k1gFnSc4	skupina
S1W	S1W	k1gFnSc2	S1W
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Security	Securita	k1gFnSc2	Securita
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
First	First	k1gMnSc1	First
World	World	k1gMnSc1	World
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
zpěvák	zpěvák	k1gMnSc1	zpěvák
Professor	Professor	k1gMnSc1	Professor
Griff	Griff	k1gMnSc1	Griff
<g/>
.	.	kIx.	.
</s>
<s>
Rapperské	Rapperský	k2eAgNnSc1d1	Rapperský
jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
Chuck	Chuck	k1gInSc4	Chuck
D	D	kA	D
<g/>
,	,	kIx,	,
Professor	Professor	k1gMnSc1	Professor
Griff	Griff	k1gMnSc1	Griff
a	a	k8xC	a
Flavor	Flavor	k1gMnSc1	Flavor
Flav	Flav	k1gMnSc1	Flav
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
studoval	studovat	k5eAaImAgMnS	studovat
Chuck	Chuck	k1gInSc4	Chuck
D	D	kA	D
grafický	grafický	k2eAgInSc4d1	grafický
design	design	k1gInSc4	design
na	na	k7c4	na
Adelphi	Adelphi	k1gNnSc4	Adelphi
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
studentském	studentský	k2eAgInSc6d1	studentský
rádiu	rádius	k1gInSc6	rádius
WBAU	WBAU	kA	WBAU
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
DJ	DJ	kA	DJ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Hankem	Hanek	k1gMnSc7	Hanek
Shockleem	Shockleus	k1gMnSc7	Shockleus
a	a	k8xC	a
Billem	Bill	k1gMnSc7	Bill
Stephenym	Stephenym	k1gInSc4	Stephenym
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
zálibě	záliba	k1gFnSc3	záliba
v	v	k7c6	v
hip-hopu	hipop	k1gInSc6	hip-hop
a	a	k8xC	a
stejnému	stejný	k2eAgInSc3d1	stejný
politickému	politický	k2eAgInSc3d1	politický
smýšlení	smýšlení	k1gNnSc4	smýšlení
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
stali	stát	k5eAaPmAgMnP	stát
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
produkovali	produkovat	k5eAaImAgMnP	produkovat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1983	[number]	k4	1983
kolekci	kolekce	k1gFnSc4	kolekce
agresivních	agresivní	k2eAgFnPc2d1	agresivní
rap	rapa	k1gFnPc2	rapa
<g/>
/	/	kIx~	/
<g/>
hip-hopových	hipopový	k2eAgFnPc2d1	hip-hopová
skladeb	skladba	k1gFnPc2	skladba
Super	super	k2eAgInSc1d1	super
Special	Special	k1gInSc1	Special
Mix	mix	k1gInSc1	mix
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přibyl	přibýt	k5eAaPmAgInS	přibýt
Chuckův	Chuckův	k2eAgMnSc1d1	Chuckův
přítel	přítel	k1gMnSc1	přítel
Flavor	Flavor	k1gMnSc1	Flavor
Flav	Flav	k1gMnSc1	Flav
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hostitelů	hostitel	k1gMnPc2	hostitel
na	na	k7c6	na
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
pořadu	pořad	k1gInSc6	pořad
WBAU	WBAU	kA	WBAU
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
začali	začít	k5eAaPmAgMnP	začít
Chuck	Chuck	k1gInSc4	Chuck
D	D	kA	D
s	s	k7c7	s
Hankem	Hanek	k1gMnSc7	Hanek
Shockleem	Shockleus	k1gMnSc7	Shockleus
mixovat	mixovat	k5eAaImF	mixovat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
demonahrávky	demonahrávka	k1gFnPc4	demonahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
Public	publicum	k1gNnPc2	publicum
Enemy	Enem	k1gInPc4	Enem
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
dostala	dostat	k5eAaPmAgFnS	dostat
kapela	kapela	k1gFnSc1	kapela
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
slyšel	slyšet	k5eAaImAgMnS	slyšet
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
získat	získat	k5eAaPmF	získat
Chucka	Chucko	k1gNnPc4	Chucko
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
label	label	k1gInSc4	label
Def	Def	k1gFnSc2	Def
Jam	jáma	k1gFnPc2	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Chuck	Chuck	k1gMnSc1	Chuck
D	D	kA	D
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
dlouho	dlouho	k6eAd1	dlouho
váhal	váhat	k5eAaImAgInS	váhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pojetí	pojetí	k1gNnSc4	pojetí
doslova	doslova	k6eAd1	doslova
revoluční	revoluční	k2eAgFnPc1d1	revoluční
hip-hopové	hipopový	k2eAgFnPc1d1	hip-hopová
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
extrémní	extrémní	k2eAgFnSc6d1	extrémní
zvukové	zvukový	k2eAgFnSc6d1	zvuková
produkci	produkce	k1gFnSc6	produkce
a	a	k8xC	a
prezentaci	prezentace	k1gFnSc6	prezentace
sociopolitických	sociopolitický	k2eAgInPc2d1	sociopolitický
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Podporován	podporován	k2eAgInSc1d1	podporován
Shockleem	Shockleus	k1gMnSc7	Shockleus
(	(	kIx(	(
<g/>
coby	coby	k?	coby
šéfproducentem	šéfproducent	k1gMnSc7	šéfproducent
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stephenym	Stephenym	k1gInSc1	Stephenym
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
publicistou	publicista	k1gMnSc7	publicista
<g/>
)	)	kIx)	)
zformoval	zformovat	k5eAaPmAgInS	zformovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Chuck	Chuck	k1gInSc1	Chuck
D	D	kA	D
novou	nový	k2eAgFnSc4d1	nová
sestavu	sestava	k1gFnSc4	sestava
souboru	soubor	k1gInSc2	soubor
-	-	kIx~	-
přibyli	přibýt	k5eAaPmAgMnP	přibýt
DJ	DJ	kA	DJ
Terminator	Terminator	k1gInSc4	Terminator
X	X	kA	X
a	a	k8xC	a
Professor	Professor	k1gMnSc1	Professor
Griff	Griff	k1gMnSc1	Griff
<g/>
,	,	kIx,	,
choreograf	choreograf	k1gMnSc1	choreograf
taneční	taneční	k2eAgFnSc2d1	taneční
pódiové	pódiový	k2eAgFnSc2d1	pódiová
formace	formace	k1gFnSc2	formace
the	the	k?	the
Security	Securita	k1gFnSc2	Securita
of	of	k?	of
the	the	k?	the
First	First	k1gMnSc1	First
World	World	k1gMnSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Flavor	Flavor	k1gMnSc1	Flavor
Flav	Flav	k1gMnSc1	Flav
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
rapperem	rapper	k1gMnSc7	rapper
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
humorný	humorný	k2eAgInSc1d1	humorný
doprovod	doprovod	k1gInSc1	doprovod
kontrastoval	kontrastovat	k5eAaImAgInS	kontrastovat
s	s	k7c7	s
Chuckovým	Chuckův	k2eAgInSc7d1	Chuckův
autoritativním	autoritativní	k2eAgInSc7d1	autoritativní
barytonem	baryton	k1gInSc7	baryton
<g/>
,	,	kIx,	,
do	do	k7c2	do
projevu	projev	k1gInSc2	projev
kapely	kapela	k1gFnSc2	kapela
přinesl	přinést	k5eAaPmAgMnS	přinést
hravost	hravost	k1gFnSc4	hravost
a	a	k8xC	a
lehkost	lehkost	k1gFnSc4	lehkost
<g/>
.	.	kIx.	.
</s>
<s>
Public	publicum	k1gNnPc2	publicum
Enemy	Enema	k1gFnSc2	Enema
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Výstavišti	výstaviště	k1gNnSc6	výstaviště
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
31.10	[number]	k4	31.10
2010	[number]	k4	2010
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
Pražském	pražský	k2eAgInSc6d1	pražský
klubu	klub	k1gInSc6	klub
SaSaZu	SaSaZus	k1gInSc2	SaSaZus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Chuck	Chuck	k1gMnSc1	Chuck
D	D	kA	D
(	(	kIx(	(
<g/>
Carlton	Carlton	k1gInSc1	Carlton
Douglas	Douglas	k1gMnSc1	Douglas
Ridenhour	Ridenhour	k1gMnSc1	Ridenhour
<g/>
)	)	kIx)	)
-	-	kIx~	-
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Flavor	Flavor	k1gMnSc1	Flavor
Flav	Flav	k1gMnSc1	Flav
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Jonathan	Jonathan	k1gMnSc1	Jonathan
Drayton	Drayton	k1gInSc1	Drayton
<g/>
)	)	kIx)	)
-	-	kIx~	-
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Professor	Professor	k1gMnSc1	Professor
Griff	Griff	k1gMnSc1	Griff
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Griffin	Griffin	k1gMnSc1	Griffin
<g/>
)	)	kIx)	)
-	-	kIx~	-
vede	vést	k5eAaImIp3nS	vést
taneční	taneční	k2eAgFnSc4d1	taneční
skupinu	skupina	k1gFnSc4	skupina
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
W	W	kA	W
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
manažer	manažer	k1gMnSc1	manažer
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
sedí	sedit	k5eAaImIp3nP	sedit
za	za	k7c7	za
bicími	bicí	k2eAgInPc7d1	bicí
DJ	DJ	kA	DJ
Terminator	Terminator	k1gInSc4	Terminator
X	X	kA	X
-	-	kIx~	-
DJ	DJ	kA	DJ
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
DJ	DJ	kA	DJ
Lord	lord	k1gMnSc1	lord
-	-	kIx~	-
DJ	DJ	kA	DJ
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Poison	Poison	k1gMnSc1	Poison
Goin	Goin	k1gMnSc1	Goin
<g/>
'	'	kIx"	'
On	on	k3xPp3gMnSc1	on
2002	[number]	k4	2002
-	-	kIx~	-
Revolverlution	Revolverlution	k1gInSc1	Revolverlution
2005	[number]	k4	2005
-	-	kIx~	-
New	New	k1gMnSc1	New
Whirl	Whirl	k1gMnSc1	Whirl
Odor	Odor	k1gMnSc1	Odor
2006	[number]	k4	2006
-	-	kIx~	-
Rebirth	Rebirtha	k1gFnPc2	Rebirtha
of	of	k?	of
a	a	k8xC	a
Nation	Nation	k1gInSc1	Nation
2007	[number]	k4	2007
-	-	kIx~	-
How	How	k1gMnSc1	How
You	You	k1gFnSc2	You
Sell	Sell	k1gMnSc1	Sell
Soul	Soul	k1gInSc1	Soul
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
Soulless	Soulless	k1gInSc1	Soulless
People	People	k1gFnSc2	People
Who	Who	k1gFnPc2	Who
Sold	sold	k1gInSc1	sold
Their	Their	k1gMnSc1	Their
Soul	Soul	k1gInSc1	Soul
<g/>
?	?	kIx.	?
</s>
<s>
1992	[number]	k4	1992
-	-	kIx~	-
Greatest	Greatest	k1gFnSc1	Greatest
Misses	Misses	k1gInSc1	Misses
2005	[number]	k4	2005
-	-	kIx~	-
Power	Power	k1gMnSc1	Power
to	ten	k3xDgNnSc4	ten
the	the	k?	the
People	People	k1gMnSc3	People
and	and	k?	and
the	the	k?	the
Beats	Beats	k1gInSc1	Beats
<g/>
:	:	kIx,	:
Public	publicum	k1gNnPc2	publicum
Enemy	Enema	k1gFnSc2	Enema
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc1	Hits
1988	[number]	k4	1988
-	-	kIx~	-
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Believe	Belieev	k1gFnSc2	Belieev
the	the	k?	the
Hype	Hyp	k1gFnSc2	Hyp
<g/>
"	"	kIx"	"
1989	[number]	k4	1989
-	-	kIx~	-
"	"	kIx"	"
<g/>
Fight	Fight	k1gInSc1	Fight
the	the	k?	the
Power	Power	k1gInSc1	Power
<g/>
"	"	kIx"	"
1990	[number]	k4	1990
-	-	kIx~	-
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Terrordome	Terrordom	k1gInSc5	Terrordom
<g/>
"	"	kIx"	"
1990	[number]	k4	1990
-	-	kIx~	-
"	"	kIx"	"
<g/>
911	[number]	k4	911
Is	Is	k1gFnPc2	Is
a	a	k8xC	a
Joke	Joke	k1gFnPc2	Joke
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
-	-	kIx~	-
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Truss	Truss	k1gInSc1	Truss
It	It	k1gFnSc1	It
<g/>
"	"	kIx"	"
1991	[number]	k4	1991
-	-	kIx~	-
"	"	kIx"	"
<g/>
Shut	Shut	k1gMnSc1	Shut
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
Down	Down	k1gInSc1	Down
<g/>
"	"	kIx"	"
1994	[number]	k4	1994
-	-	kIx~	-
"	"	kIx"	"
<g/>
Give	Give	k1gInSc1	Give
It	It	k1gMnSc2	It
Up	Up	k1gMnSc2	Up
<g/>
"	"	kIx"	"
1998	[number]	k4	1998
-	-	kIx~	-
"	"	kIx"	"
<g/>
He	he	k0	he
Got	Got	k1gFnPc3	Got
Game	game	k1gInSc4	game
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Stills	Stills	k1gInSc1	Stills
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Public	publicum	k1gNnPc2	publicum
Enemy	Enema	k1gFnSc2	Enema
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
MySpace	MySpace	k1gFnSc1	MySpace
profil	profil	k1gInSc4	profil
</s>
