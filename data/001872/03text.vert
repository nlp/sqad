<s>
Lamarckismus	Lamarckismus	k1gInSc1	Lamarckismus
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Jean	Jean	k1gMnSc1	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Lamarck	Lamarcko	k1gNnPc2	Lamarcko
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
obecně	obecně	k6eAd1	obecně
přijímána	přijímán	k2eAgFnSc1d1	přijímána
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
organismy	organismus	k1gInPc4	organismus
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
prostředí	prostředí	k1gNnSc2	prostředí
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
stále	stále	k6eAd1	stále
složitějších	složitý	k2eAgFnPc2d2	složitější
a	a	k8xC	a
dokonalejších	dokonalý	k2eAgFnPc2d2	dokonalejší
adaptivních	adaptivní	k2eAgFnPc2d1	adaptivní
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Lamarckismus	Lamarckismus	k1gInSc1	Lamarckismus
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
znaky	znak	k1gInPc1	znak
získané	získaný	k2eAgInPc1d1	získaný
během	během	k7c2	během
života	život	k1gInSc2	život
organismu	organismus	k1gInSc2	organismus
jsou	být	k5eAaImIp3nP	být
dědičné	dědičný	k2eAgInPc1d1	dědičný
a	a	k8xC	a
přenášejí	přenášet	k5eAaImIp3nP	přenášet
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
myšlence	myšlenka	k1gFnSc6	myšlenka
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
široce	široko	k6eAd1	široko
akceptovaného	akceptovaný	k2eAgInSc2d1	akceptovaný
darwinismu	darwinismus	k1gInSc2	darwinismus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
neodarwinismu	neodarwinismus	k1gInSc2	neodarwinismus
<g/>
.	.	kIx.	.
</s>
<s>
Lamarck	Lamarck	k6eAd1	Lamarck
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vůli	vůle	k1gFnSc6	vůle
k	k	k7c3	k
pokroku	pokrok	k1gInSc3	pokrok
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
nutností	nutnost	k1gFnSc7	nutnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
se	se	k3xPyFc4	se
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vědci	vědec	k1gMnPc7	vědec
panuje	panovat	k5eAaImIp3nS	panovat
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
lamarkistické	lamarkistický	k2eAgFnSc3d1	lamarkistický
evoluci	evoluce	k1gFnSc3	evoluce
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jen	jen	k9	jen
v	v	k7c6	v
minimální	minimální	k2eAgFnSc6d1	minimální
míře	míra	k1gFnSc6	míra
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Znovuvzkříšení	znovuvzkříšení	k1gNnSc1	znovuvzkříšení
zažívá	zažívat	k5eAaImIp3nS	zažívat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
podobě	podoba	k1gFnSc6	podoba
též	též	k9	též
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
memů	mem	k1gInPc2	mem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
organismy	organismus	k1gInPc1	organismus
dokáží	dokázat	k5eAaPmIp3nP	dokázat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
cíleně	cíleně	k6eAd1	cíleně
mutovat	mutovat	k5eAaImF	mutovat
(	(	kIx(	(
<g/>
trypanosomy	trypanosoma	k1gFnPc4	trypanosoma
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgInPc4d1	lidský
B-lymfocyty	Bymfocyt	k1gInPc4	B-lymfocyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
myšlenek	myšlenka	k1gFnPc2	myšlenka
lamarckismu	lamarckismus	k1gInSc2	lamarckismus
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
evoluční	evoluční	k2eAgInSc1d1	evoluční
mechanismus	mechanismus	k1gInSc1	mechanismus
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
evoluci	evoluce	k1gFnSc6	evoluce
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Brání	bránit	k5eAaImIp3nP	bránit
mu	on	k3xPp3gMnSc3	on
několik	několik	k4yIc4	několik
překážek	překážka	k1gFnPc2	překážka
<g/>
:	:	kIx,	:
Nemožnost	nemožnost	k1gFnSc4	nemožnost
přepsat	přepsat	k5eAaPmF	přepsat
informaci	informace	k1gFnSc4	informace
z	z	k7c2	z
proteinů	protein	k1gInPc2	protein
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
-	-	kIx~	-
Centrální	centrální	k2eAgNnSc1d1	centrální
dogma	dogma	k1gNnSc1	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
popisuje	popisovat	k5eAaImIp3nS	popisovat
přenos	přenos	k1gInSc1	přenos
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
DNA	DNA	kA	DNA
do	do	k7c2	do
RNA	RNA	kA	RNA
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
organismu	organismus	k1gInSc6	organismus
se	s	k7c7	s
transkripcí	transkripce	k1gFnSc7	transkripce
přepíše	přepsat	k5eAaPmIp3nS	přepsat
do	do	k7c2	do
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc4d1	možný
i	i	k9	i
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
-	-	kIx~	-
pomocí	pomocí	k7c2	pomocí
reverzní	reverzní	k2eAgFnSc2d1	reverzní
transkriptázy	transkriptáza	k1gFnSc2	transkriptáza
lze	lze	k6eAd1	lze
informaci	informace	k1gFnSc4	informace
z	z	k7c2	z
RNA	RNA	kA	RNA
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Translací	translace	k1gFnSc7	translace
se	se	k3xPyFc4	se
z	z	k7c2	z
RNA	RNA	kA	RNA
překládá	překládat	k5eAaImIp3nS	překládat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
jazyku	jazyk	k1gInSc3	jazyk
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
do	do	k7c2	do
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
nemůže	moct	k5eNaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
změny	změna	k1gFnPc1	změna
získané	získaný	k2eAgFnPc1d1	získaná
během	během	k7c2	během
života	život	k1gInSc2	život
nemohou	moct	k5eNaImIp3nP	moct
uložit	uložit	k5eAaPmF	uložit
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Weismannovská	Weismannovský	k2eAgFnSc1d1	Weismannovský
bariéra	bariéra	k1gFnSc1	bariéra
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
přenosu	přenos	k1gInSc2	přenos
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
tělních	tělní	k2eAgFnPc2d1	tělní
buněk	buňka	k1gFnPc2	buňka
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
genech	gen	k1gInPc6	gen
tělních	tělní	k2eAgFnPc2d1	tělní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
generace	generace	k1gFnSc2	generace
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
slouží	sloužit	k5eAaImIp3nS	sloužit
speciální	speciální	k2eAgFnSc1d1	speciální
linie	linie	k1gFnSc1	linie
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
změny	změna	k1gFnPc1	změna
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
projevit	projevit	k5eAaPmF	projevit
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
lamarkistické	lamarkistický	k2eAgFnSc2d1	lamarkistický
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Epigenetické	epigenetický	k2eAgInPc1d1	epigenetický
procesy	proces	k1gInPc1	proces
-	-	kIx~	-
U	u	k7c2	u
složitějších	složitý	k2eAgInPc2d2	složitější
organismů	organismus	k1gInPc2	organismus
geny	gen	k1gInPc4	gen
neurčují	určovat	k5eNaImIp3nP	určovat
přímo	přímo	k6eAd1	přímo
vznikající	vznikající	k2eAgFnSc4d1	vznikající
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
její	její	k3xOp3gInSc4	její
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
plán	plán	k1gInSc4	plán
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zpětně	zpětně	k6eAd1	zpětně
zapsat	zapsat	k5eAaPmF	zapsat
provedené	provedený	k2eAgFnPc4d1	provedená
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
evoluce	evoluce	k1gFnSc1	evoluce
postupovat	postupovat	k5eAaImF	postupovat
lamarkistickou	lamarkistický	k2eAgFnSc7d1	lamarkistický
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
neolamarckismu	neolamarckismus	k1gInSc2	neolamarckismus
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemusí	muset	k5eNaImIp3nS	muset
platit	platit	k5eAaImF	platit
darwinismus	darwinismus	k1gInSc4	darwinismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lamarkismus	lamarkismus	k1gInSc1	lamarkismus
<g/>
.	.	kIx.	.
absence	absence	k1gFnSc1	absence
weismannovské	weismannovský	k2eAgFnSc2d1	weismannovský
bariéry	bariéra	k1gFnSc2	bariéra
-	-	kIx~	-
některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
(	(	kIx(	(
<g/>
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozmnoženy	rozmnožit	k5eAaPmNgInP	rozmnožit
z	z	k7c2	z
tělních	tělní	k2eAgFnPc2d1	tělní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
existence	existence	k1gFnSc1	existence
cílených	cílený	k2eAgFnPc2d1	cílená
mutací	mutace	k1gFnPc2	mutace
-	-	kIx~	-
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
organismy	organismus	k1gInPc1	organismus
cíleně	cíleně	k6eAd1	cíleně
měnit	měnit	k5eAaImF	měnit
svou	svůj	k3xOyFgFnSc4	svůj
mutační	mutační	k2eAgFnSc4d1	mutační
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
i	i	k8xC	i
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
mutační	mutační	k2eAgNnPc4d1	mutační
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
darwinismem	darwinismus	k1gInSc7	darwinismus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
SOS	sos	k1gInSc1	sos
reakce	reakce	k1gFnSc2	reakce
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
)	)	kIx)	)
Jednobuněčné	jednobuněčný	k2eAgInPc1d1	jednobuněčný
organismy	organismus	k1gInPc1	organismus
-	-	kIx~	-
neexistuje	existovat	k5eNaImIp3nS	existovat
u	u	k7c2	u
nich	on	k3xPp3gMnPc6	on
weismannovská	weismannovský	k2eAgFnSc1d1	weismannovský
bariéra	bariéra	k1gFnSc1	bariéra
a	a	k8xC	a
získané	získaný	k2eAgFnPc1d1	získaná
změny	změna	k1gFnPc1	změna
přechází	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c4	na
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
horizontální	horizontální	k2eAgFnSc1d1	horizontální
přenos	přenos	k1gInSc1	přenos
-	-	kIx~	-
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
viru	vir	k1gInSc2	vir
přenést	přenést	k5eAaPmF	přenést
část	část	k1gFnSc4	část
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
tělní	tělní	k2eAgFnSc2d1	tělní
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
předtím	předtím	k6eAd1	předtím
změněna	změnit	k5eAaPmNgNnP	změnit
<g/>
)	)	kIx)	)
a	a	k8xC	a
vložena	vložit	k5eAaPmNgFnS	vložit
do	do	k7c2	do
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
selekce	selekce	k1gFnSc1	selekce
somatických	somatický	k2eAgFnPc2d1	somatická
buněk	buňka	k1gFnPc2	buňka
-	-	kIx~	-
pokud	pokud	k8xS	pokud
probíhá	probíhat	k5eAaImIp3nS	probíhat
selekce	selekce	k1gFnSc1	selekce
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
lišit	lišit	k5eAaImF	lišit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
weismannovské	weismannovský	k2eAgFnSc2d1	weismannovský
bariéry	bariéra	k1gFnSc2	bariéra
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
získaná	získaný	k2eAgFnSc1d1	získaná
vlastnost	vlastnost	k1gFnSc1	vlastnost
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
generace	generace	k1gFnSc2	generace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
částech	část	k1gFnPc6	část
rostliny	rostlina	k1gFnSc2	rostlina
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
selekci	selekce	k1gFnSc3	selekce
na	na	k7c4	na
odolání	odolání	k1gNnSc4	odolání
přehřátí	přehřátí	k1gNnSc2	přehřátí
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
na	na	k7c6	na
selekci	selekce	k1gFnSc6	selekce
odolání	odolání	k1gNnSc2	odolání
chladu	chlad	k1gInSc2	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
nový	nový	k2eAgMnSc1d1	nový
jedinec	jedinec	k1gMnSc1	jedinec
z	z	k7c2	z
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
tělní	tělní	k2eAgFnSc2d1	tělní
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jedinec	jedinec	k1gMnSc1	jedinec
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
bude	být	k5eAaImBp3nS	být
lépe	dobře	k6eAd2	dobře
adaptován	adaptovat	k5eAaBmNgInS	adaptovat
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
jedinec	jedinec	k1gMnSc1	jedinec
vyrostlý	vyrostlý	k2eAgMnSc1d1	vyrostlý
z	z	k7c2	z
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
bude	být	k5eAaImBp3nS	být
lépe	dobře	k6eAd2	dobře
snášet	snášet	k5eAaImF	snášet
chlad	chlad	k1gInSc4	chlad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ribozymy	ribozym	k1gInPc1	ribozym
-	-	kIx~	-
existují	existovat	k5eAaImIp3nP	existovat
molekuly	molekula	k1gFnPc1	molekula
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
přepsány	přepsat	k5eAaPmNgInP	přepsat
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
pak	pak	k6eAd1	pak
šířeny	šířen	k2eAgInPc1d1	šířen
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
body	bod	k1gInPc1	bod
ovšem	ovšem	k9	ovšem
platí	platit	k5eAaImIp3nP	platit
jen	jen	k9	jen
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
situacích	situace	k1gFnPc6	situace
pro	pro	k7c4	pro
určité	určitý	k2eAgInPc4d1	určitý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Neznamenají	znamenat	k5eNaImIp3nP	znamenat
popření	popření	k1gNnSc4	popření
darwinismu	darwinismus	k1gInSc2	darwinismus
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
jeho	jeho	k3xOp3gFnPc4	jeho
specifické	specifický	k2eAgFnPc4d1	specifická
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
žirafy	žirafa	k1gFnSc2	žirafa
-	-	kIx~	-
podle	podle	k7c2	podle
lamarkismu	lamarkismus	k1gInSc2	lamarkismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
krk	krk	k1gInSc1	krk
žiraf	žirafa	k1gFnPc2	žirafa
postupným	postupný	k2eAgNnSc7d1	postupné
natahováním	natahování	k1gNnSc7	natahování
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
spíše	spíše	k9	spíše
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
stránka	stránka	k1gFnSc1	stránka
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
Kováři	kovář	k1gMnPc1	kovář
-	-	kIx~	-
pozorováním	pozorování	k1gNnSc7	pozorování
bylo	být	k5eAaImAgNnS	být
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
synové	syn	k1gMnPc1	syn
kovářů	kovář	k1gMnPc2	kovář
bývají	bývat	k5eAaImIp3nP	bývat
svalnatější	svalnatý	k2eAgFnPc1d2	svalnatější
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
lamarkistického	lamarkistický	k2eAgNnSc2d1	lamarkistický
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
prací	práce	k1gFnSc7	práce
kovářů	kovář	k1gMnPc2	kovář
sílily	sílit	k5eAaImAgInP	sílit
jejich	jejich	k3xOp3gInPc1	jejich
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
posílení	posílení	k1gNnSc1	posílení
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ale	ale	k9	ale
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
výchovou	výchova	k1gFnSc7	výchova
synů	syn	k1gMnPc2	syn
(	(	kIx(	(
<g/>
tvrdší	tvrdý	k2eAgFnSc2d2	tvrdší
práce	práce	k1gFnSc2	práce
už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
)	)	kIx)	)
i	i	k9	i
genetickými	genetický	k2eAgFnPc7d1	genetická
dispozicemi	dispozice	k1gFnPc7	dispozice
(	(	kIx(	(
<g/>
kovářem	kovář	k1gMnSc7	kovář
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
geny	gen	k1gInPc7	gen
podporujícími	podporující	k2eAgInPc7d1	podporující
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
výdrž	výdrž	k1gFnSc4	výdrž
-	-	kIx~	-
tyto	tento	k3xDgFnPc1	tento
predispozice	predispozice	k1gFnPc1	predispozice
se	se	k3xPyFc4	se
předávaly	předávat	k5eAaImAgFnP	předávat
dalším	další	k2eAgFnPc3d1	další
generacím	generace	k1gFnPc3	generace
<g/>
)	)	kIx)	)
</s>
