<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Budkov	Budkov	k1gInSc1	Budkov
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Budkov	Budkov	k1gInSc4	Budkov
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Třebíč	Třebíč	k1gFnSc4	Třebíč
<g/>
,	,	kIx,	,
asi	asi	k9	asi
11	[number]	k4	11
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zámku	zámek	k1gInSc3	zámek
předcházela	předcházet	k5eAaImAgFnS	předcházet
vodní	vodní	k2eAgFnSc1d1	vodní
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
roku	rok	k1gInSc2	rok
1353	[number]	k4	1353
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Budkova	Budkov	k1gInSc2	Budkov
prodal	prodat	k5eAaPmAgInS	prodat
Jeníku	Jeník	k1gMnSc3	Jeník
mladšímu	mladý	k2eAgInSc3d2	mladší
a	a	k8xC	a
Jeníku	Jeník	k1gMnSc3	Jeník
staršímu	starší	k1gMnSc3	starší
z	z	k7c2	z
Mezimostí	Mezimost	k1gFnPc2	Mezimost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
přestavbě	přestavba	k1gFnSc3	přestavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
pozdní	pozdní	k2eAgFnSc2d1	pozdní
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1545-1555	[number]	k4	1545-1555
za	za	k7c2	za
Hrubčických	Hrubčická	k1gFnPc2	Hrubčická
z	z	k7c2	z
Čechtína	Čechtín	k1gInSc2	Čechtín
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
Berchtolda	Berchtold	k1gMnSc2	Berchtold
z	z	k7c2	z
Uherčic	Uherčice	k1gFnPc2	Uherčice
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
drobným	drobný	k2eAgFnPc3d1	drobná
barokním	barokní	k2eAgFnPc3d1	barokní
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
promítly	promítnout	k5eAaPmAgInP	promítnout
spíše	spíše	k9	spíše
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
nachází	nacházet	k5eAaImIp3nS	nacházet
dětský	dětský	k2eAgInSc4d1	dětský
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Nejnápadnějším	nápadní	k2eAgInSc7d3	nápadní
prvkem	prvek	k1gInSc7	prvek
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
vstupní	vstupní	k2eAgFnSc1d1	vstupní
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
vodní	vodní	k2eAgFnSc2d1	vodní
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
patrovou	patrový	k2eAgFnSc7d1	patrová
pětikřídlou	pětikřídlý	k2eAgFnSc7d1	pětikřídlý
budovou	budova	k1gFnSc7	budova
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
nepravidelné	pravidelný	k2eNgNnSc4d1	nepravidelné
nádvoří	nádvoří	k1gNnSc4	nádvoří
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
a	a	k8xC	a
toskánským	toskánský	k2eAgNnSc7d1	toskánské
sloupovím	sloupoví	k1gNnSc7	sloupoví
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vodní	vodní	k2eAgFnSc2d1	vodní
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
anglického	anglický	k2eAgInSc2d1	anglický
parku	park	k1gInSc2	park
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,3	[number]	k4	3,3
ha	ha	kA	ha
se	se	k3xPyFc4	se
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zámek	zámek	k1gInSc1	zámek
Budkov	Budkov	k1gInSc4	Budkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
na	na	k7c4	na
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
dětského	dětský	k2eAgInSc2d1	dětský
domova	domov	k1gInSc2	domov
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
zámku	zámek	k1gInSc2	zámek
</s>
</p>
