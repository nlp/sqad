<p>
<s>
Enrico	Enrico	k6eAd1	Enrico
Martino	Martin	k2eAgNnSc1d1	Martino
(	(	kIx(	(
<g/>
*	*	kIx~	*
Turín	Turín	k1gInSc1	Turín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Ligurie	Ligurie	k1gFnSc1	Ligurie
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Mondadori	Mondadori	k1gNnSc1	Mondadori
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Duše	duše	k1gFnSc1	duše
Indové	Ind	k1gMnPc1	Ind
(	(	kIx(	(
<g/>
EGA	EGA	kA	EGA
Editore	editor	k1gInSc5	editor
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
volání	volání	k1gNnSc2	volání
Torino	Torino	k1gNnSc4	Torino
(	(	kIx(	(
<g/>
EGA	EGA	kA	EGA
Editore	editor	k1gInSc5	editor
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexico	Mexico	k1gNnSc1	Mexico
(	(	kIx(	(
<g/>
Idealibri	Idealibri	k1gNnSc1	Idealibri
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Burgundy	Burgundy	k1gInPc1	Burgundy
kamene	kámen	k1gInSc2	kámen
(	(	kIx(	(
<g/>
Idealibri	Idealibri	k1gNnSc1	Idealibri
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Italie	Italie	k1gFnSc1	Italie
(	(	kIx(	(
<g/>
vilo	vít	k5eAaImAgNnS	vít
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexico	Mexico	k1gNnSc1	Mexico
(	(	kIx(	(
<g/>
Giunti	Giunti	k1gNnSc1	Giunti
<g/>
,	,	kIx,	,
odchozí	odchozí	k2eAgFnSc1d1	odchozí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavy	výstava	k1gFnPc1	výstava
==	==	k?	==
</s>
</p>
<p>
<s>
Reporter	Reporter	k1gInSc1	Reporter
<g/>
'	'	kIx"	'
<g/>
70	[number]	k4	70
(	(	kIx(	(
<g/>
Torino	Torino	k1gNnSc1	Torino
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
35	[number]	k4	35
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
a	a	k8xC	a
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chiapas	Chiapas	k1gInSc1	Chiapas
(	(	kIx(	(
<g/>
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
,	,	kIx,	,
Messina	Messina	k1gFnSc1	Messina
<g/>
,	,	kIx,	,
Catania	Catanium	k1gNnPc1	Catanium
<g/>
,	,	kIx,	,
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Baja	Baja	k1gFnSc1	Baja
California	Californium	k1gNnSc2	Californium
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Queretaro	Queretara	k1gFnSc5	Queretara
<g/>
,	,	kIx,	,
Acapulco	Acapulca	k1gFnSc5	Acapulca
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Miláno	Milán	k2eAgNnSc1d1	Miláno
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gente	Gente	k5eAaPmIp2nP	Gente
di	di	k?	di
Torino	Torino	k1gNnSc1	Torino
(	(	kIx(	(
<g/>
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Migrantes	Migrantes	k1gMnSc1	Migrantes
<g/>
,	,	kIx,	,
domorodé	domorodý	k2eAgFnSc6d1	domorodá
do	do	k7c2	do
Mexico	Mexico	k1gNnSc4	Mexico
City	City	k1gFnPc1	City
(	(	kIx(	(
<g/>
Chicago	Chicago	k1gNnSc1	Chicago
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Boloňa	Boloňa	k1gFnSc1	Boloňa
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Enrico	Enrico	k6eAd1	Enrico
Martino	Martin	k2eAgNnSc1d1	Martino
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
</s>
</p>
