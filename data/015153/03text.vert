<s>
HistoryLink	HistoryLink	k1gInSc1
</s>
<s>
HistoryLink	HistoryLink	k1gInSc1
je	být	k5eAaImIp3nS
internetová	internetový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
historii	historie	k1gFnSc6
amerického	americký	k2eAgInSc2d1
státu	stát	k1gInSc2
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
únoru	únor	k1gInSc3
2016	#num#	k4
měla	mít	k5eAaImAgFnS
více	hodně	k6eAd2
než	než	k8xS
sedm	sedm	k4xCc1
tisíc	tisíc	k4xCgInPc2
článků	článek	k1gInPc2
a	a	k8xC
v	v	k7c6
průměru	průměr	k1gInSc6
ji	on	k3xPp3gFnSc4
navštívilo	navštívit	k5eAaPmAgNnS
pět	pět	k4xCc1
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
S	s	k7c7
původní	původní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
založení	založení	k1gNnSc2
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
doplnila	doplnit	k5eAaPmAgFnS
třísvazkovou	třísvazkový	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
History	Histor	k1gInPc4
of	of	k?
King	King	k1gInSc4
County	Counta	k1gFnSc2
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
od	od	k7c2
Clarence	Clarenec	k1gMnSc2
Bagleye	Bagley	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
<g/>
,	,	kIx,
přišli	přijít	k5eAaPmAgMnP
historici	historik	k1gMnPc1
Walt	Walta	k1gFnPc2
Crowley	Crowlea	k1gFnSc2
a	a	k8xC
Paul	Paul	k1gMnSc1
Dorpat	Dorpat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
si	se	k3xPyFc3
novou	nový	k2eAgFnSc4d1
encyklopedii	encyklopedie	k1gFnSc4
představovali	představovat	k5eAaImAgMnP
jako	jako	k8xC,k8xS
tištěnou	tištěný	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
<g/>
,	,	kIx,
Crowleyova	Crowleyův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
ale	ale	k9
navrhla	navrhnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
kvůli	kvůli	k7c3
lepší	dobrý	k2eAgFnSc3d2
dostupnosti	dostupnost	k1gFnSc3
a	a	k8xC
možnosti	možnost	k1gFnSc3
dalšího	další	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc2
zpřístupněna	zpřístupněn	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
Crowley	Crowle	k2eAgInPc1d1
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
založil	založit	k5eAaPmAgMnS
History	Histor	k1gMnPc4
Ink	Ink	k1gMnSc1
<g/>
,	,	kIx,
neziskovou	ziskový	k2eNgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
HistoryLink	HistoryLink	k1gInSc4
dosud	dosud	k6eAd1
zastřešuje	zastřešovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
encyklopedie	encyklopedie	k1gFnSc2
byly	být	k5eAaImAgFnP
za	za	k7c7
účelem	účel	k1gInSc7
demonstrace	demonstrace	k1gFnSc2
spuštěny	spuštěn	k2eAgInPc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1998	#num#	k4
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
pak	pak	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
počáteční	počáteční	k2eAgFnSc4d1
financovaní	financovaný	k2eAgMnPc1d1
zajišťovaly	zajišťovat	k5eAaImAgInP
peněžní	peněžní	k2eAgInPc4d1
dary	dar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byl	být	k5eAaImAgInS
okruh	okruh	k1gInSc1
článků	článek	k1gInPc2
rozšířen	rozšířen	k2eAgInSc1d1
o	o	k7c6
historii	historie	k1gFnSc6
celého	celý	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
oproti	oproti	k7c3
prvotnímu	prvotní	k2eAgNnSc3d1
zaměření	zaměření	k1gNnSc3
na	na	k7c4
město	město	k1gNnSc4
Seattle	Seattle	k1gFnSc2
a	a	k8xC
okres	okres	k1gInSc1
King	Kinga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
About	About	k1gInSc1
HistoryLink	HistoryLink	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MACINTOSH	Macintosh	kA
<g/>
,	,	kIx,
Heather	Heathra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
—	—	k?
A	a	k8xC
Slideshow	Slideshow	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
First	First	k1gInSc1
Years	Years	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
Priscilla	Priscillo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crowley	Crowle	k2eAgInPc1d1
<g/>
,	,	kIx,
Walt	Walt	k1gInSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
STEIN	Stein	k1gMnSc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
is	is	k?
launched	launched	k1gInSc1
on	on	k3xPp3gMnSc1
January	Januar	k1gInPc1
17	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HistoryLink	HistoryLink	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
historylink	historylink	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
–	–	k?
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
encyklopedie	encyklopedie	k1gFnSc2
</s>
<s>
What	What	k2eAgInSc1d1
is	is	k?
HistoryLink	HistoryLink	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
propagační	propagační	k2eAgNnSc4d1
video	video	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Washington	Washington	k1gInSc1
</s>
