<s>
S	s	k7c7
původní	původní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
založení	založení	k1gNnSc2
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
doplnila	doplnit	k5eAaPmAgFnS
třísvazkovou	třísvazkový	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
History	Histor	k1gInPc4
of	of	k?
King	King	k1gInSc4
County	Counta	k1gFnSc2
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
od	od	k7c2
Clarence	Clarenec	k1gMnSc2
Bagleye	Bagley	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
<g/>
,	,	kIx,
přišli	přijít	k5eAaPmAgMnP
historici	historik	k1gMnPc1
Walt	Walta	k1gFnPc2
Crowley	Crowlea	k1gFnSc2
a	a	k8xC
Paul	Paul	k1gMnSc1
Dorpat	Dorpat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
HistoryLink	HistoryLink	k1gInSc1
je	být	k5eAaImIp3nS
internetová	internetový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
historii	historie	k1gFnSc6
amerického	americký	k2eAgInSc2d1
státu	stát	k1gInSc2
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>