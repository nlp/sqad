<s desamb="1">
Oba	dva	k4xCgInPc1
si	se	k3xPyFc3
novou	nový	k2eAgFnSc4d1
encyklopedii	encyklopedie	k1gFnSc4
představovali	představovat	k5eAaImAgMnP
jako	jako	k8xC,k8xS
tištěnou	tištěný	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
<g/>
,	,	kIx,
Crowleyova	Crowleyův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
ale	ale	k9
navrhla	navrhnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
kvůli	kvůli	k7c3
lepší	dobrý	k2eAgFnSc3d2
dostupnosti	dostupnost	k1gFnSc3
a	a	k8xC
možnosti	možnost	k1gFnSc3
dalšího	další	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc2
zpřístupněna	zpřístupněn	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
Crowley	Crowle	k2eAgInPc1d1
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
založil	založit	k5eAaPmAgMnS
History	Histor	k1gMnPc4
Ink	Ink	k1gMnSc1
<g/>
,	,	kIx,
neziskovou	ziskový	k2eNgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
HistoryLink	HistoryLink	k1gInSc4
dosud	dosud	k6eAd1
zastřešuje	zastřešovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>