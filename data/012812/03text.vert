<p>
<s>
Aerologie	aerologie	k1gFnSc1	aerologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
meteorologie	meteorologie	k1gFnSc2	meteorologie
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
atmosférou	atmosféra	k1gFnSc7	atmosféra
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
vertikálním	vertikální	k2eAgInSc6d1	vertikální
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgInPc4d1	speciální
nosiče	nosič	k1gInPc4	nosič
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
měřicí	měřicí	k2eAgFnSc2d1	měřicí
techniky	technika	k1gFnSc2	technika
<g/>
:	:	kIx,	:
rakety	raketa	k1gFnPc1	raketa
<g/>
,	,	kIx,	,
balóny	balón	k1gInPc1	balón
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Radiosondy	radiosonda	k1gFnPc1	radiosonda
obvykle	obvykle	k6eAd1	obvykle
přímo	přímo	k6eAd1	přímo
měří	měřit	k5eAaImIp3nS	měřit
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
PTU	PTU	kA	PTU
(	(	kIx(	(
<g/>
Pressure	Pressur	k1gMnSc5	Pressur
<g/>
,	,	kIx,	,
Temperature	Temperatur	k1gMnSc5	Temperatur
<g/>
,	,	kIx,	,
hUmidity	humidita	k1gFnPc1	humidita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
změny	změna	k1gFnSc2	změna
polohy	poloha	k1gFnSc2	poloha
radiosondy	radiosonda	k1gFnSc2	radiosonda
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výstupu	výstup	k1gInSc2	výstup
sondy	sonda	k1gFnSc2	sonda
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tlakových	tlakový	k2eAgFnPc6d1	tlaková
hladinách	hladina	k1gFnPc6	hladina
<g/>
.	.	kIx.	.
</s>
</p>
