<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
ozbrojených	ozbrojený	k2eAgFnPc6d1
silách	síla	k1gFnPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Československá	československý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
aerolinieAirbus	aerolinieAirbus	k1gInSc1
A330-300	A330-300	k1gFnSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
</s>
<s>
IATAOK	IATAOK	kA
</s>
<s>
ICAOCSA	ICAOCSA	kA
</s>
<s>
CALLSIGNCSA-LINES	CALLSIGNCSA-LINES	k?
</s>
<s>
Zahájení	zahájení	k1gNnSc1
činnosti	činnost	k1gFnSc2
<g/>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
<g/>
SídloEvropská	SídloEvropský	k2eAgFnSc1d1
846	#num#	k4
<g/>
/	/	kIx~
<g/>
176	#num#	k4
<g/>
a	a	k8xC
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
ČeskoHlavní	českohlavní	k2eAgNnSc1d1
základnaLetiště	základnaLetiště	k1gNnSc1
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
PrahaDestinace	PrahaDestinace	k1gFnSc2
<g/>
4	#num#	k4
(	(	kIx(
<g/>
březen	březen	k1gInSc1
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
Velikost	velikost	k1gFnSc1
flotily	flotila	k1gFnSc2
<g/>
2	#num#	k4
(	(	kIx(
<g/>
březen	březen	k1gInSc1
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
Přepravené	přepravený	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
▲	▲	k?
2,7	2,7	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Věrnostní	věrnostní	k2eAgInSc1d1
programOK	programOK	k?
PlusČlen	PlusČlen	k2eAgInSc1d1
alianceSkyTeamOficiální	alianceSkyTeamOficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
www.csa.cz	www.csa.cza	k1gFnPc2
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
<g/>
,	,	kIx,
a.s.	a.s.	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Czech	Czech	k2eAgFnPc1d1
Airlines	Airlines	k2gFnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
na	na	k7c6
letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Navazují	navazovat	k5eAaImIp3nP
na	na	k7c4
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
identitu	identita	k1gFnSc4
a	a	k8xC
původně	původně	k6eAd1
i	i	k9
značku	značka	k1gFnSc4
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
1992	#num#	k4
transformována	transformovat	k5eAaBmNgFnS
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
a	a	k8xC
se	s	k7c7
zánikem	zánik	k1gInSc7
ČSFR	ČSFR	kA
majetkově	majetkově	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a	a	k8xC
slovenskou	slovenský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Slov-Air	Slov-Aira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
změně	změna	k1gFnSc3
názvu	název	k1gInSc2
na	na	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a.s.	a.s.	k?
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
březnu	březen	k1gInSc6
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pátou	pátá	k1gFnSc4
nejstarší	starý	k2eAgFnSc4d3
dosud	dosud	k6eAd1
fungující	fungující	k2eAgFnSc4d1
aerolinii	aerolinie	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
;	;	kIx,
starší	starší	k1gMnPc1
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
KLM	KLM	kA
<g/>
,	,	kIx,
Avianca	Avianca	k1gFnSc1
<g/>
,	,	kIx,
Qantas	Qantas	k1gInSc1
a	a	k8xC
Aeroflot	aeroflot	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
se	se	k3xPyFc4
k	k	k7c3
7	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
2021	#num#	k4
skládala	skládat	k5eAaImAgFnS
ze	z	k7c2
2	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
jedno	jeden	k4xCgNnSc1
letadlo	letadlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2017	#num#	k4
provozovaly	provozovat	k5eAaImAgInP
ČSA	ČSA	kA
pravidelné	pravidelný	k2eAgFnPc4d1
linky	linka	k1gFnPc4
do	do	k7c2
50	#num#	k4
destinací	destinace	k1gFnPc2
v	v	k7c6
25	#num#	k4
zemích	zem	k1gFnPc6
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
přepravila	přepravit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
2,7	2,7	k4
milionu	milion	k4xCgInSc2
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
představovalo	představovat	k5eAaImAgNnS
jen	jen	k9
minimální	minimální	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
oproti	oproti	k7c3
předchozímu	předchozí	k2eAgInSc3d1
roku	rok	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2019	#num#	k4
pracovalo	pracovat	k5eAaImAgNnS
u	u	k7c2
ČSA	ČSA	kA
587	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
také	také	k9
jako	jako	k9
dopravce	dopravce	k1gMnSc1
nákladu	náklad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
března	březen	k1gInSc2
2001	#num#	k4
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
aliance	aliance	k1gFnSc2
leteckých	letecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
SkyTeam	SkyTeam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slogan	slogan	k1gInSc1
zní	znět	k5eAaImIp3nS
„	„	k?
<g/>
V	v	k7c6
oblacích	oblak	k1gInPc6
jako	jako	k8xC,k8xS
doma	doma	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
odkoupila	odkoupit	k5eAaPmAgFnS
podíl	podíl	k1gInSc4
státu	stát	k1gInSc2
a	a	k8xC
tehdejší	tehdejší	k2eAgInSc4d1
podíl	podíl	k1gInSc4
Korean	Koreana	k1gFnPc2
Air	Air	k1gFnPc2
konkurenční	konkurenční	k2eAgFnSc1d1
soukromá	soukromý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Smartwings	Smartwingsa	k1gFnPc2
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
zakladatelem	zakladatel	k1gMnSc7
je	být	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Šimáně	Šimána	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smartwings	Smartwings	k1gInSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
holdingu	holding	k1gInSc2
Unimex	Unimex	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ovládá	ovládat	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Šimáně	Šimán	k1gInSc6
a	a	k8xC
Jaromír	Jaromír	k1gMnSc1
Šmejkal	Šmejkal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smartwings	Smartwings	k1gInSc1
je	být	k5eAaImIp3nS
majoritním	majoritní	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
ČSA	ČSA	kA
s	s	k7c7
téměř	téměř	k6eAd1
98	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
zbylé	zbylý	k2eAgFnSc2d1
2,3	2,3	k4
%	%	kIx~
drží	držet	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
si	se	k3xPyFc3
Smartwings	Smartwings	k1gInSc4
dle	dle	k7c2
výroční	výroční	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
vypůjčil	vypůjčit	k5eAaPmAgInS
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
Kč	Kč	kA
z	z	k7c2
ČSA	ČSA	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
250	#num#	k4
000	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
celosvětové	celosvětový	k2eAgFnSc2d1
krize	krize	k1gFnSc2
způsobené	způsobený	k2eAgFnSc2d1
od	od	k7c2
března	březen	k1gInSc2
2020	#num#	k4
nemocí	nemoc	k1gFnPc2
covid-	covid-	k?
<g/>
19	#num#	k4
se	se	k3xPyFc4
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
hlubokém	hluboký	k2eAgInSc6d1
útlumu	útlum	k1gInSc6
svých	svůj	k3xOyFgFnPc2
aktivit	aktivita	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
března	březen	k1gInSc2
2021	#num#	k4
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
v	v	k7c6
úpadku	úpadek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Založení	založení	k1gNnSc1
ČSA	ČSA	kA
a	a	k8xC
období	období	k1gNnSc2
1923	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
</s>
<s>
Letoun	letoun	k1gMnSc1
Farman	Farman	k1gMnSc1
F.	F.	kA
<g/>
60	#num#	k4
Goliath	Goliath	k1gInSc1
Československých	československý	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
byly	být	k5eAaImAgFnP
československou	československý	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
založeny	založit	k5eAaPmNgFnP
Československé	československý	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
(	(	kIx(
<g/>
ČSA	ČSA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
dosavadním	dosavadní	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
československého	československý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
majorem	major	k1gMnSc7
Karlem	Karel	k1gMnSc7
Huppnerem	Huppner	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
ředitelem	ředitel	k1gMnSc7
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc7
domovským	domovský	k2eAgNnSc7d1
letištěm	letiště	k1gNnSc7
bylo	být	k5eAaImAgNnS
Letiště	letiště	k1gNnSc1
Praha-Kbely	Praha-Kbela	k1gFnSc2
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
první	první	k4xOgInSc1
dálkový	dálkový	k2eAgInSc1d1
let	let	k1gInSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
320	#num#	k4
kilometrů	kilometr	k1gInPc2
tehdy	tehdy	k6eAd1
překonal	překonat	k5eAaPmAgMnS
šéfpilot	šéfpilot	k1gMnSc1
Karel	Karel	k1gMnSc1
Brabenec	Brabenec	k1gMnSc1
s	s	k7c7
jedním	jeden	k4xCgMnSc7
cestujícím	cestující	k1gMnSc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
byl	být	k5eAaImAgMnS
redaktor	redaktor	k1gMnSc1
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
Václav	Václav	k1gMnSc1
König	König	k1gMnSc1
<g/>
;	;	kIx,
letěli	letět	k5eAaImAgMnP
s	s	k7c7
upraveným	upravený	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
strojem	stroj	k1gInSc7
Aero	aero	k1gNnSc4
A-	A-	k1gFnPc2
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
byla	být	k5eAaImAgFnS
trasa	trasa	k1gFnSc1
spojení	spojení	k1gNnSc2
prodloužena	prodloužit	k5eAaPmNgFnS
až	až	k6eAd1
na	na	k7c4
Letiště	letiště	k1gNnSc4
Košice	Košice	k1gInPc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
cesta	cesta	k1gFnSc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
trvala	trvat	k5eAaImAgFnS
asi	asi	k9
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
celkem	celkem	k6eAd1
649	#num#	k4
letů	let	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
bylo	být	k5eAaImAgNnS
přepraveno	přepravit	k5eAaPmNgNnS
426	#num#	k4
osob	osoba	k1gFnPc2
a	a	k8xC
také	také	k9
2148	#num#	k4
kg	kg	kA
letecké	letecký	k2eAgFnSc2d1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Lety	let	k1gInPc1
zajišťovaly	zajišťovat	k5eAaImAgInP
československé	československý	k2eAgInPc1d1
stroje	stroj	k1gInPc1
Aero	aero	k1gNnSc4
A-14	A-14	k1gFnSc2
a	a	k8xC
Aero	aero	k1gNnSc4
A-	A-	k1gFnPc2
<g/>
10	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
postupně	postupně	k6eAd1
nahrazovány	nahrazován	k2eAgInPc1d1
britskými	britský	k2eAgInPc7d1
modernějšími	moderní	k2eAgInPc7d2
typy	typ	k1gInPc7
De	De	k?
Havilland	Havillanda	k1gFnPc2
DH	DH	kA
<g/>
.50	.50	k4
a	a	k8xC
Farman	Farman	k1gMnSc1
F-60	F-60	k1gMnSc1
Goliath	Goliath	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
ČSA	ČSA	kA
byly	být	k5eAaImAgFnP
nástrojem	nástroj	k1gInSc7
národní	národní	k2eAgFnSc2d1
prestiže	prestiž	k1gFnSc2
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
byly	být	k5eAaImAgFnP
dotovány	dotovat	k5eAaBmNgFnP
Ministerstvem	ministerstvo	k1gNnSc7
veřejných	veřejný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1923	#num#	k4
a	a	k8xC
1924	#num#	k4
nedosahovaly	dosahovat	k5eNaImAgFnP
tržby	tržba	k1gFnPc1
výše	výše	k1gFnSc1
ani	ani	k8xC
jednoho	jeden	k4xCgNnSc2
procenta	procento	k1gNnSc2
finančních	finanční	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Období	období	k1gNnSc1
1929	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
</s>
<s>
Douglas	Douglas	k1gMnSc1
DC-3	DC-3	k1gMnSc1
(	(	kIx(
<g/>
OK-XDM	OK-XDM	k1gMnSc1
<g/>
)	)	kIx)
ČSA	ČSA	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
vystaven	vystavit	k5eAaPmNgInS
na	na	k7c6
ruzyňském	ruzyňský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
stala	stát	k5eAaPmAgFnS
členem	člen	k1gInSc7
Mezinárodní	mezinárodní	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
leteckých	letecký	k2eAgMnPc2d1
dopravců	dopravce	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
předchůdce	předchůdce	k1gMnSc1
dnešní	dnešní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
IATA	IATA	kA
založené	založený	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
dosavadní	dosavadní	k2eAgFnSc2d1
poznávací	poznávací	k2eAgFnSc2d1
značky	značka	k1gFnSc2
L	L	kA
přijala	přijmout	k5eAaPmAgFnS
kód	kód	k1gInSc4
OK	oko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
ČSA	ČSA	kA
létaly	létat	k5eAaImAgFnP
jen	jen	k9
na	na	k7c6
vnitrostátních	vnitrostátní	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
a	a	k8xC
první	první	k4xOgFnSc1
linka	linka	k1gFnSc1
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
předtím	předtím	k6eAd1
z	z	k7c2
Česka	Česko	k1gNnSc2
mezinárodně	mezinárodně	k6eAd1
létal	létat	k5eAaImAgMnS
konkurent	konkurent	k1gMnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
Československá	československý	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ovšem	ovšem	k9
definitivně	definitivně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
činnost	činnost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
linka	linka	k1gFnSc1
ČSA	ČSA	kA
vedla	vést	k5eAaImAgFnS
z	z	k7c2
Bratislavy	Bratislava	k1gFnSc2
do	do	k7c2
Záhřebu	Záhřeb	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byly	být	k5eAaImAgInP
lety	let	k1gInPc1
prodlouženy	prodloužit	k5eAaPmNgInP
i	i	k9
do	do	k7c2
Rijeky	Rijeka	k1gFnSc2
a	a	k8xC
Dubrovníku	Dubrovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1933	#num#	k4
byla	být	k5eAaImAgFnS
otevřená	otevřený	k2eAgFnSc1d1
pravidelná	pravidelný	k2eAgFnSc1d1
linka	linka	k1gFnSc1
ve	v	k7c6
směru	směr	k1gInSc6
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Bukurešť	Bukurešť	k1gFnSc1
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
1266	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1936	#num#	k4
spojily	spojit	k5eAaPmAgFnP
ČSA	ČSA	kA
Prahu	Praha	k1gFnSc4
s	s	k7c7
letištěm	letiště	k1gNnSc7
Tušino	Tušino	k1gNnSc4
u	u	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
o	o	k7c6
délce	délka	k1gFnSc6
2440	#num#	k4
km	km	kA
trval	trvat	k5eAaImAgInS
deset	deset	k4xCc4
hodin	hodina	k1gFnPc2
a	a	k8xC
měl	mít	k5eAaImAgMnS
pět	pět	k4xCc4
mezipřistání	mezipřistání	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1937	#num#	k4
byla	být	k5eAaImAgFnS
základna	základna	k1gFnSc1
ČSA	ČSA	kA
přesunuta	přesunout	k5eAaPmNgFnS
z	z	k7c2
kbelského	kbelský	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
na	na	k7c4
nově	nově	k6eAd1
otevřené	otevřený	k2eAgNnSc4d1
Ruzyňské	ruzyňský	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
oceněna	ocenit	k5eAaPmNgFnS
zlatou	zlatý	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
umění	umění	k1gNnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
palubách	paluba	k1gFnPc6
letounů	letoun	k1gInPc2
Savoia-Marchetti	Savoia-Marchetti	k1gNnSc2
SM	SM	kA
<g/>
.73	.73	k4
začaly	začít	k5eAaPmAgInP
objevovat	objevovat	k5eAaImF
stevardky	stevardka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
začaly	začít	k5eAaPmAgInP
fungovat	fungovat	k5eAaImF
pravidelné	pravidelný	k2eAgInPc1d1
spoje	spoj	k1gInPc1
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Budapešti	Budapešť	k1gFnSc6
a	a	k8xC
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
civilní	civilní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ČSA	ČSA	kA
pozastavena	pozastavit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
letovou	letový	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
zabavila	zabavit	k5eAaPmAgFnS
německá	německý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Lufthansa	Lufthansa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgFnPc1d1
proměny	proměna	k1gFnPc1
právní	právní	k2eAgFnSc2d1
formy	forma	k1gFnSc2
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
převzetí	převzetí	k1gNnSc2
moci	moc	k1gFnSc2
Komunistickou	komunistický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Československa	Československo	k1gNnSc2
na	na	k7c6
základě	základ	k1gInSc6
únorového	únorový	k2eAgInSc2d1
převratu	převrat	k1gInSc2
roku	rok	k1gInSc2
1948	#num#	k4
byly	být	k5eAaImAgFnP
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
zapsány	zapsat	k5eAaPmNgInP
v	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
jako	jako	k9
národní	národní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
<g/>
,	,	kIx,
zřízený	zřízený	k2eAgInSc1d1
ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1949	#num#	k4
zákonem	zákon	k1gInSc7
č.	č.	k?
311	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
vyhláškou	vyhláška	k1gFnSc7
č.	č.	k?
122	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vládním	vládní	k2eAgNnSc7d1
nařízením	nařízení	k1gNnSc7
č.	č.	k?
128	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
a	a	k8xC
zákonem	zákon	k1gInSc7
č.	č.	k?
148	#num#	k4
<g/>
/	/	kIx~
<g/>
1950	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
K	K	kA
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
1956	#num#	k4
byly	být	k5eAaImAgFnP
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
zapsány	zapsán	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
státní	státní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
<g/>
,	,	kIx,
zřízený	zřízený	k2eAgInSc1d1
listinou	listina	k1gFnSc7
ministra	ministr	k1gMnSc2
dopravy	doprava	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
k	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc6
1989	#num#	k4
vymazán	vymazat	k5eAaPmNgMnS
pro	pro	k7c4
zrušení	zrušení	k1gNnSc4
bez	bez	k7c2
likvidace	likvidace	k1gFnSc2
podle	podle	k7c2
rozhodnutí	rozhodnutí	k1gNnSc2
ministra	ministr	k1gMnSc2
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
ČSSR	ČSSR	kA
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
1989	#num#	k4
byl	být	k5eAaImAgInS
zakládací	zakládací	k2eAgFnSc7d1
listinou	listina	k1gFnSc7
vydanou	vydaný	k2eAgFnSc7d1
rozhodnutím	rozhodnutí	k1gNnSc7
ministra	ministr	k1gMnSc2
dopravy	doprava	k1gFnSc2
a	a	k8xC
spojů	spoj	k1gInPc2
ČSSR	ČSSR	kA
čj.	čj.	k?
12271	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
<g/>
-	-	kIx~
<g/>
350	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1989	#num#	k4
založen	založen	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
1992	#num#	k4
byly	být	k5eAaImAgInP
ČSA	ČSA	kA
transformovány	transformovat	k5eAaBmNgInP
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
se	s	k7c7
základním	základní	k2eAgInSc7d1
kapitálem	kapitál	k1gInSc7
2,7	2,7	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
zahájily	zahájit	k5eAaPmAgInP
ČSA	ČSA	kA
opět	opět	k5eAaPmF
provoz	provoz	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
letecký	letecký	k2eAgMnSc1d1
dopravce	dopravce	k1gMnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnovily	obnovit	k5eAaPmAgInP
všechny	všechen	k3xTgFnPc4
vnitrostátní	vnitrostátní	k2eAgFnPc4d1
linky	linka	k1gFnPc4
a	a	k8xC
postupně	postupně	k6eAd1
otvíraly	otvírat	k5eAaImAgFnP
i	i	k9
mezinárodní	mezinárodní	k2eAgFnPc1d1
linky	linka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
ČSA	ČSA	kA
poprvé	poprvé	k6eAd1
vstoupily	vstoupit	k5eAaPmAgInP
do	do	k7c2
mezikontinentální	mezikontinentální	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c6
linkách	linka	k1gFnPc6
do	do	k7c2
Káhiry	Káhira	k1gFnSc2
a	a	k8xC
Ankary	Ankara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
ČSA	ČSA	kA
zakoupily	zakoupit	k5eAaPmAgInP
z	z	k7c2
válečných	válečný	k2eAgInPc2d1
přebytků	přebytek	k1gInPc2
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
letadla	letadlo	k1gNnSc2
Douglas	Douglas	k1gInSc1
DC-	DC-	k1gFnSc4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
továrnách	továrna	k1gFnPc6
Avia	Avia	k1gFnSc1
postupně	postupně	k6eAd1
rekonstruovány	rekonstruovat	k5eAaBmNgInP
na	na	k7c4
civilní	civilní	k2eAgFnSc4d1
dopravní	dopravní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
pro	pro	k7c4
21	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc1
<g/>
12	#num#	k4
ČSA	ČSA	kA
na	na	k7c6
Letišti	letiště	k1gNnSc6
Orly	Orel	k1gMnPc4
u	u	k7c2
Paříže	Paříž	k1gFnSc2
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1957	#num#	k4
přistálo	přistát	k5eAaImAgNnS,k5eAaPmAgNnS
na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Ruzyni	Ruzyně	k1gFnSc6
první	první	k4xOgNnSc4
proudové	proudový	k2eAgNnSc4d1
dopravní	dopravní	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
Tupolev	Tupolev	k1gFnSc2
Tu-	Tu-	k1gFnSc2
<g/>
104	#num#	k4
imatrikulace	imatrikulace	k1gFnSc1
OK-LDA	OK-LDA	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvody	důvod	k1gInPc1
koupě	koupě	k1gFnSc2
ruského	ruský	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
byly	být	k5eAaImAgFnP
kromě	kromě	k7c2
technické	technický	k2eAgFnSc2d1
pokročilosti	pokročilost	k1gFnSc2
také	také	k9
politické	politický	k2eAgFnPc1d1
<g/>
,	,	kIx,
ruská	ruský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
tento	tento	k3xDgInSc4
letoun	letoun	k1gInSc4
propagovat	propagovat	k5eAaImF
i	i	k9
v	v	k7c6
západním	západní	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
ČSA	ČSA	kA
létaly	létat	k5eAaImAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
ČSA	ČSA	kA
byly	být	k5eAaImAgFnP
po	po	k7c6
Aeroflotu	aeroflot	k1gInSc6
druhou	druhý	k4xOgFnSc7
leteckou	letecký	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
pravidelně	pravidelně	k6eAd1
provozující	provozující	k2eAgInPc4d1
proudové	proudový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
na	na	k7c6
svých	svůj	k3xOyFgFnPc6
linkách	linka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
začala	začít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
používat	používat	k5eAaImF
letadla	letadlo	k1gNnSc2
značky	značka	k1gFnSc2
Iljušin	iljušin	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
kapacitu	kapacita	k1gFnSc4
až	až	k9
110	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1962	#num#	k4
zahájily	zahájit	k5eAaPmAgInP
ČSA	ČSA	kA
provoz	provoz	k1gInSc4
na	na	k7c6
první	první	k4xOgFnSc6
transatlantické	transatlantický	k2eAgFnSc6d1
lince	linka	k1gFnSc6
směřující	směřující	k2eAgFnSc6d1
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Havany	Havana	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
létalo	létat	k5eAaImAgNnS
čtyřmotorové	čtyřmotorový	k2eAgNnSc1d1
turbovrtulové	turbovrtulový	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
Bristol	Bristol	k1gInSc1
Britannia	Britannium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
pronajato	pronajmout	k5eAaPmNgNnS
od	od	k7c2
kubánské	kubánský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Cubana	Cuban	k1gMnSc2
de	de	k?
Aviación	Aviación	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gMnSc2
<g/>
104	#num#	k4
<g/>
A	A	kA
(	(	kIx(
<g/>
OK-NDD	OK-NDD	k1gMnSc6
<g/>
)	)	kIx)
společnosti	společnost	k1gFnSc2
ČSA	ČSA	kA
na	na	k7c6
letišti	letiště	k1gNnSc6
Lipsko	Lipsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Halle	Halle	k1gNnSc1
v	v	k7c6
NDR	NDR	kA
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
listopadu	listopad	k1gInSc2
1962	#num#	k4
začaly	začít	k5eAaPmAgFnP
ČSA	ČSA	kA
provozovat	provozovat	k5eAaImF
lety	let	k1gInPc4
se	s	k7c7
stroji	stroj	k1gInPc7
sovětské	sovětský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
Tupolev	Tupolev	k1gMnSc7
Tu-	Tu-	k1gMnSc7
<g/>
124	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
ve	v	k7c6
flotile	flotila	k1gFnSc6
zůstaly	zůstat	k5eAaPmAgInP
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
do	do	k7c2
flotily	flotila	k1gFnSc2
přibyl	přibýt	k5eAaPmAgInS
letoun	letoun	k1gInSc1
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc1
<g/>
62	#num#	k4
<g/>
,	,	kIx,
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
dálkové	dálkový	k2eAgInPc4d1
lety	let	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
umožnil	umožnit	k5eAaPmAgInS
otevření	otevření	k1gNnSc4
linek	linka	k1gFnPc2
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
<g/>
,	,	kIx,
Střední	střední	k2eAgInSc4d1
a	a	k8xC
Dálný	dálný	k2eAgInSc4d1
východ	východ	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
a	a	k8xC
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Tu-	Tu-	k?
<g/>
134	#num#	k4
<g/>
A	a	k8xC
na	na	k7c6
letišti	letiště	k1gNnSc6
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
byly	být	k5eAaImAgFnP
ČSA	ČSA	kA
podle	podle	k7c2
statistiky	statistika	k1gFnSc2
IATA	IATA	kA
jednou	jednou	k6eAd1
ze	z	k7c2
34	#num#	k4
leteckých	letecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
přepravila	přepravit	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc4
cestujících	cestující	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujímaly	zaujímat	k5eAaImAgFnP
29	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
světového	světový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
mezi	mezi	k7c7
evropskými	evropský	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
;	;	kIx,
ve	v	k7c6
vytěžování	vytěžování	k1gNnSc6
letadel	letadlo	k1gNnPc2
jim	on	k3xPp3gMnPc3
patřilo	patřit	k5eAaImAgNnS
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
činilo	činit	k5eAaImAgNnS
63,3	63,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
byly	být	k5eAaImAgInP
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
flotile	flotila	k1gFnSc6
měly	mít	k5eAaImAgInP
celkem	celkem	k6eAd1
75	#num#	k4
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
nejvíce	nejvíce	k6eAd1,k6eAd3
v	v	k7c6
historii	historie	k1gFnSc6
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
62	#num#	k4
společnosti	společnost	k1gFnSc2
ČSA	ČSA	kA
před	před	k7c7
odletem	odlet	k1gInSc7
z	z	k7c2
Letiště	letiště	k1gNnSc2
London	London	k1gMnSc1
Heathrow	Heathrow	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
byly	být	k5eAaImAgInP
díky	díky	k7c3
Il-	Il-	k1gFnSc3
<g/>
62	#num#	k4
otevřeny	otevřen	k2eAgFnPc4d1
linky	linka	k1gFnPc4
ve	v	k7c6
směrech	směr	k1gInPc6
Praha	Praha	k1gFnSc1
–	–	k?
Montréal	Montréal	k1gMnSc1
–	–	k?
New	New	k1gMnSc1
York-JFK	York-JFK	k1gMnSc1
a	a	k8xC
Bratislava	Bratislava	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
–	–	k?
Amsterdam	Amsterdam	k1gInSc1
–	–	k?
New	New	k1gMnSc2
York-JFK	York-JFK	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
začaly	začít	k5eAaPmAgInP
fungovat	fungovat	k5eAaImF
lety	let	k1gInPc4
se	s	k7c7
stroji	stroj	k1gInPc7
Tu-	Tu-	k1gFnSc2
<g/>
134	#num#	k4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
měly	mít	k5eAaImAgFnP
76	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
přibylo	přibýt	k5eAaPmAgNnS
do	do	k7c2
flotily	flotila	k1gFnSc2
pět	pět	k4xCc4
letounů	letoun	k1gInPc2
Jak-	Jak-	k1gFnSc2
<g/>
40	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
ve	v	k7c6
vnitrostátní	vnitrostátní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
nahradily	nahradit	k5eAaPmAgInP
letouny	letoun	k1gInPc1
Il-	Il-	k1gFnSc2
<g/>
14	#num#	k4
(	(	kIx(
<g/>
později	pozdě	k6eAd2
byly	být	k5eAaImAgFnP
postupně	postupně	k6eAd1
vyřazovány	vyřazovat	k5eAaImNgFnP
díky	díky	k7c3
prudkému	prudký	k2eAgInSc3d1
růstu	růst	k1gInSc3
ceny	cena	k1gFnSc2
ropy	ropa	k1gFnSc2
a	a	k8xC
velké	velký	k2eAgFnSc3d1
spotřebě	spotřeba	k1gFnSc3
paliva	palivo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1977	#num#	k4
až	až	k8xS
1987	#num#	k4
vznikly	vzniknout	k5eAaPmAgFnP
pravidelné	pravidelný	k2eAgFnPc1d1
linky	linka	k1gFnPc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
Bratislavy	Bratislava	k1gFnSc2
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gMnSc1
<g/>
,	,	kIx,
Dubaje	Dubaj	k1gInPc1
<g/>
,	,	kIx,
Valletty	Vallett	k1gInPc1
(	(	kIx(
<g/>
Malta	Malta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jerevanu	Jerevan	k1gMnSc6
<g/>
,	,	kIx,
Taškentu	Taškent	k1gInSc2
a	a	k8xC
Hanoje	Hanoj	k1gFnSc2
a	a	k8xC
Ho	on	k3xPp3gNnSc2
Či	či	k8xC
Minova	Minův	k2eAgNnSc2d1
Města	město	k1gNnSc2
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
byly	být	k5eAaImAgInP
zakoupeny	zakoupen	k2eAgInPc1d1
nové	nový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Tu-	Tu-	k1gFnSc2
<g/>
154	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
1990	#num#	k4
otevřely	otevřít	k5eAaPmAgFnP
ČSA	ČSA	kA
linky	linka	k1gFnSc2
Praha	Praha	k1gFnSc1
–	–	k?
Havana	Havana	k1gFnSc1
–	–	k?
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
a	a	k8xC
Praha	Praha	k1gFnSc1
–	–	k?
Hamburk	Hamburk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
ČSA	ČSA	kA
koupily	koupit	k5eAaPmAgInP
první	první	k4xOgMnSc1
západoevropský	západoevropský	k2eAgInSc1d1
širokotrupý	širokotrupý	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Airbus	airbus	k1gInSc4
A310-300	A310-300	k1gFnSc2
určený	určený	k2eAgInSc4d1
pro	pro	k7c4
dálkové	dálkový	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
koupě	koupě	k1gFnSc2
se	se	k3xPyFc4
zamýšlela	zamýšlet	k5eAaImAgFnS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
kvůli	kvůli	k7c3
již	již	k6eAd1
nedostačujícím	dostačující	k2eNgNnPc3d1
sovětským	sovětský	k2eAgNnPc3d1
letadlům	letadlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
byla	být	k5eAaImAgFnS
také	také	k9
otevřena	otevřít	k5eAaPmNgFnS
linka	linka	k1gFnSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
na	na	k7c4
Ben	Ben	k1gInSc4
Gurionovo	Gurionův	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
u	u	k7c2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
a	a	k8xC
do	do	k7c2
Istanbulu	Istanbul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1992	#num#	k4
přibyla	přibýt	k5eAaPmAgFnS
do	do	k7c2
flotily	flotila	k1gFnSc2
ČSA	ČSA	kA
čtyři	čtyři	k4xCgNnPc1
turbovrtulová	turbovrtulový	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
ATR	ATR	kA
72	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
stejného	stejný	k2eAgInSc2d1
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
služby	služba	k1gFnSc2
zavedeno	zavést	k5eAaPmNgNnS
prvních	první	k4xOgNnPc6
pět	pět	k4xCc4
letadel	letadlo	k1gNnPc2
typu	typ	k1gInSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
<g/>
-	-	kIx~
<g/>
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Letadlo	letadlo	k1gNnSc1
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc1
<g/>
18	#num#	k4
<g/>
D	D	kA
Československých	československý	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
na	na	k7c6
Letišti	letiště	k1gNnSc6
Ruzyně	Ruzyně	k1gFnSc2
v	v	k7c6
září	září	k1gNnSc6
1988	#num#	k4
</s>
<s>
Aerotaxi	aerotaxi	k1gNnSc1
a	a	k8xC
Agrolet	Agrolet	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Agrolet	Agrolet	k1gInSc4
a	a	k8xC
Slov-Air	Slov-Air	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
provozovaly	provozovat	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
také	také	k9
aerotaxi	aerotaxi	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
Svitlet	Svitlet	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
znárodněná	znárodněný	k2eAgFnSc1d1
Baťova	Baťův	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
včleněna	včleněn	k2eAgFnSc1d1
do	do	k7c2
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vznikla	vzniknout	k5eAaPmAgFnS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Agrolet	Agrolet	k1gInSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
si	se	k3xPyFc3
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
vyhradily	vyhradit	k5eAaPmAgFnP
pro	pro	k7c4
aerotaxi	aerotaxi	k1gNnSc4
a	a	k8xC
například	například	k6eAd1
letecké	letecký	k2eAgFnSc2d1
zemědělské	zemědělský	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dobu	doba	k1gFnSc4
fungování	fungování	k1gNnSc2
aerotaxi	aerotaxi	k1gNnSc2
se	se	k3xPyFc4
vystřídalo	vystřídat	k5eAaPmAgNnS
47	#num#	k4
letadel	letadlo	k1gNnPc2
Aero	aero	k1gNnSc4
Ae-	Ae-	k1gFnPc2
<g/>
45	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
strojů	stroj	k1gInPc2
Aero	aero	k1gNnSc4
Ae-	Ae-	k1gFnSc2
<g/>
45	#num#	k4
<g/>
S	s	k7c7
<g/>
,	,	kIx,
5	#num#	k4
strojů	stroj	k1gInPc2
Let	let	k1gInSc4
L-	L-	k1gMnPc2
<g/>
200	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
strojů	stroj	k1gInPc2
L-200A	L-200A	k1gFnPc2
a	a	k8xC
30	#num#	k4
letadel	letadlo	k1gNnPc2
L-	L-	k1gFnSc1
<g/>
200	#num#	k4
<g/>
D.	D.	kA
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
se	se	k3xPyFc4
podnik	podnik	k1gInSc1
Agrolet	Agrolet	k1gInSc1
spadající	spadající	k2eAgInSc1d1
pod	pod	k7c7
ČSA	ČSA	kA
změnil	změnit	k5eAaPmAgInS
ve	v	k7c4
slovenský	slovenský	k2eAgInSc4d1
podnik	podnik	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Slov-Air	Slov-Aira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
kvůli	kvůli	k7c3
požadavku	požadavek	k1gInSc2
slovenských	slovenský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
chtěli	chtít	k5eAaImAgMnP
po	po	k7c6
federalizaci	federalizace	k1gFnSc6
Československa	Československo	k1gNnSc2
rozdělit	rozdělit	k5eAaPmF
leteckou	letecký	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
mezi	mezi	k7c4
obě	dva	k4xCgFnPc4
republiky	republika	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozování	provozování	k1gNnSc1
aerotaxi	aerotaxi	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
Československu	Československo	k1gNnSc6
postupně	postupně	k6eAd1
stávalo	stávat	k5eAaImAgNnS
ztrátovým	ztrátový	k2eAgMnSc7d1
a	a	k8xC
jak	jak	k8xS,k8xC
ČSA	ČSA	kA
tak	tak	k8xS,k8xC
Slov-air	Slov-air	k1gInSc4
byly	být	k5eAaImAgFnP
nuceny	nucen	k2eAgFnPc1d1
prodávat	prodávat	k5eAaImF
flotilu	flotila	k1gFnSc4
menších	malý	k2eAgNnPc2d2
letadel	letadlo	k1gNnPc2
různým	různý	k2eAgInPc3d1
aeroklubům	aeroklub	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
mezi	mezi	k7c7
Českem	Česko	k1gNnSc7
a	a	k8xC
Slovenskem	Slovensko	k1gNnSc7
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gMnPc7
<g/>
134	#num#	k4
<g/>
A	a	k8xC
Československých	československý	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začalo	začít	k5eAaPmAgNnS
dělení	dělení	k1gNnSc1
společnosti	společnost	k1gFnSc2
mezi	mezi	k7c7
Českem	Česko	k1gNnSc7
a	a	k8xC
Slovenskem	Slovensko	k1gNnSc7
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
majetku	majetek	k1gInSc2
Československých	československý	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
mezi	mezi	k7c7
Českem	Česko	k1gNnSc7
a	a	k8xC
Slovenskem	Slovensko	k1gNnSc7
nezačalo	začít	k5eNaPmAgNnS
až	až	k9
se	s	k7c7
zánikem	zánik	k1gInSc7
federace	federace	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
Československo	Československo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
reálně	reálně	k6eAd1
federalizováno	federalizován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majetek	majetek	k1gInSc1
ČSA	ČSA	kA
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
podle	podle	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
zákona	zákon	k1gInSc2
platného	platný	k2eAgNnSc2d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
se	se	k3xPyFc4
rozdělily	rozdělit	k5eAaPmAgFnP
na	na	k7c4
dvě	dva	k4xCgFnPc4
společnosti	společnost	k1gFnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c4
transformovanou	transformovaný	k2eAgFnSc4d1
českou	český	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
a	a	k8xC
Slov-Air	Slov-Aira	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
předtím	předtím	k6eAd1
formálně	formálně	k6eAd1
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
však	však	k9
osamostatněná	osamostatněný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protokol	protokol	k1gInSc1
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
majetku	majetek	k1gInSc2
mezi	mezi	k7c4
transformované	transformovaný	k2eAgFnPc4d1
Československé	československý	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a	a	k8xC
Slov-Air	Slov-Air	k1gInSc4
připravilo	připravit	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
hospodářství	hospodářství	k1gNnSc2
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
za	za	k7c2
první	první	k4xOgFnSc2
vlády	vláda	k1gFnSc2
Vladimíra	Vladimír	k1gMnSc2
Mečiara	Mečiar	k1gMnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
Ministerstva	ministerstvo	k1gNnSc2
dopravy	doprava	k1gFnSc2
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
stál	stát	k5eAaImAgMnS
Roman	Roman	k1gMnSc1
Hofbauer	Hofbauer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
byly	být	k5eAaImAgFnP
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
transformovány	transformován	k2eAgInPc1d1
v	v	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
a.s.	a.s.	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1992	#num#	k4
až	až	k9
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
už	už	k6eAd1
jako	jako	k8xC,k8xS
český	český	k2eAgInSc1d1
podnik	podnik	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jim	on	k3xPp3gFnPc3
zůstaly	zůstat	k5eAaPmAgFnP
původní	původní	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
,	,	kIx,
obchodní	obchodní	k2eAgFnSc1d1
značka	značka	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
mezinárodní	mezinárodní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
OK	oka	k1gFnPc2
i	i	k8xC
předčíslí	předčíslí	k1gNnSc2
0	#num#	k4
<g/>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Slovenský	slovenský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
Roman	Roman	k1gMnSc1
Hofbauer	Hofbauer	k1gMnSc1
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
spustil	spustit	k5eAaPmAgInS
útočnou	útočný	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
proti	proti	k7c3
ČSA	ČSA	kA
a	a	k8xC
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadoval	požadovat	k5eAaImAgMnS
nejméně	málo	k6eAd3
25	#num#	k4
%	%	kIx~
podíl	podíl	k1gInSc1
Slovenska	Slovensko	k1gNnSc2
v	v	k7c6
ČSA	ČSA	kA
a	a	k8xC
hrozil	hrozit	k5eAaImAgInS
požadavkem	požadavek	k1gInSc7
na	na	k7c4
zrušení	zrušení	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
názvu	název	k1gInSc2
a	a	k8xC
symbolu	symbol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
se	s	k7c7
zástupci	zástupce	k1gMnPc7
slovenského	slovenský	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
nezúčastnili	zúčastnit	k5eNaPmAgMnP
oslav	oslava	k1gFnPc2
70	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
založení	založení	k1gNnSc2
ČSA	ČSA	kA
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1993	#num#	k4
a	a	k8xC
poslali	poslat	k5eAaPmAgMnP
ultimativní	ultimativní	k2eAgInSc4d1
fax	fax	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
zopakovali	zopakovat	k5eAaPmAgMnP
požadavek	požadavek	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ČSA	ČSA	kA
vypustily	vypustit	k5eAaPmAgInP
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
názvu	název	k1gInSc2
Slovensko	Slovensko	k1gNnSc1
a	a	k8xC
přejmenovaly	přejmenovat	k5eAaPmAgFnP
se	se	k3xPyFc4
například	například	k6eAd1
na	na	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensku	Slovensko	k1gNnSc3
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
chyběla	chybět	k5eAaImAgFnS
koncepční	koncepční	k2eAgFnSc1d1
práce	práce	k1gFnSc1
v	v	k7c6
oboru	obor	k1gInSc6
letectví	letectví	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
(	(	kIx(
<g/>
MDSVP	MDSVP	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
od	od	k7c2
osamostatnění	osamostatnění	k1gNnSc2
Slovenska	Slovensko	k1gNnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
několik	několik	k4yIc1
ředitelů	ředitel	k1gMnPc2
Odboru	odbor	k1gInSc2
civilního	civilní	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
1994	#num#	k4
zakázaly	zakázat	k5eAaPmAgInP
slovenské	slovenský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
ČSA	ČSA	kA
létat	létat	k5eAaImF
z	z	k7c2
letišť	letiště	k1gNnPc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
Popradu	Poprad	k1gInSc6
a	a	k8xC
Košicích	Košice	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
sama	sám	k3xTgFnSc1
nebyla	být	k5eNaImAgFnS
schopna	schopen	k2eAgFnSc1d1
tyto	tento	k3xDgInPc4
lety	let	k1gInPc4
dopravně	dopravně	k6eAd1
nahradit	nahradit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Jan	Jan	k1gMnSc1
Stráský	Stráský	k1gMnSc1
v	v	k7c6
knize	kniha	k1gFnSc6
rozhovorů	rozhovor	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
připustil	připustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
dělení	dělení	k1gNnSc6
majetku	majetek	k1gInSc2
ČSA	ČSA	kA
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
podle	podle	k7c2
územního	územní	k2eAgInSc2d1
principu	princip	k1gInSc2
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
na	na	k7c6
české	český	k2eAgFnSc6d1
straně	strana	k1gFnSc6
majetku	majetek	k1gInSc2
víc	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenská	slovenský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
později	pozdě	k6eAd2
argumentovala	argumentovat	k5eAaImAgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
dělení	dělení	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
nevědělo	vědět	k5eNaImAgNnS
o	o	k7c6
zániku	zánik	k1gInSc6
společného	společný	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
na	na	k7c4
takové	takový	k3xDgNnSc4
dělení	dělení	k1gNnSc4
neměli	mít	k5eNaImAgMnP
přistoupit	přistoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stráský	Stráský	k1gMnSc1
argumentoval	argumentovat	k5eAaImAgMnS
zákonem	zákon	k1gInSc7
o	o	k7c6
dělení	dělení	k1gNnSc6
majetku	majetek	k1gInSc2
federace	federace	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
jsou	být	k5eAaImIp3nP
ČSA	ČSA	kA
uvedeny	uveden	k2eAgInPc1d1
jako	jako	k8xC,k8xS
podnik	podnik	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
znovu	znovu	k6eAd1
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
dávno	dávno	k6eAd1
před	před	k7c7
zánikem	zánik	k1gInSc7
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
zdráhala	zdráhat	k5eAaImAgFnS
na	na	k7c4
slovenské	slovenský	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
přistoupit	přistoupit	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
ČSA	ČSA	kA
mezitím	mezitím	k6eAd1
získala	získat	k5eAaPmAgFnS
Air	Air	k1gFnSc1
France	Franc	k1gMnSc2
38	#num#	k4
%	%	kIx~
podíl	podíl	k1gInSc4
<g/>
;	;	kIx,
převedením	převedení	k1gNnSc7
dalšího	další	k2eAgInSc2d1
podílu	podíl	k1gInSc2
na	na	k7c4
Slovensko	Slovensko	k1gNnSc4
by	by	kYmCp3nS
český	český	k2eAgInSc1d1
stát	stát	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
majoritu	majorita	k1gFnSc4
a	a	k8xC
Slovensko	Slovensko	k1gNnSc1
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
nezískalo	získat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
Slovensku	Slovensko	k1gNnSc3
ochotna	ochoten	k2eAgFnSc1d1
postoupit	postoupit	k5eAaPmF
nejvýše	vysoce	k6eAd3,k6eAd1
6	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
s	s	k7c7
čímž	což	k3yQnSc7,k3yRnSc7
však	však	k9
slovenská	slovenský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
nesouhlasila	souhlasit	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Stráský	Stráský	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
vyjádřil	vyjádřit	k5eAaPmAgMnS
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
dohodě	dohoda	k1gFnSc3
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
majetkového	majetkový	k2eAgNnSc2d1
vypořádání	vypořádání	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Slovenska	Slovensko	k1gNnSc2
nedojde	dojít	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
První	první	k4xOgInSc1
západoevropský	západoevropský	k2eAgInSc1d1
letoun	letoun	k1gInSc1
ČSA	ČSA	kA
po	po	k7c6
době	doba	k1gFnSc6
ruských	ruský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
–	–	k?
Airbus	airbus	k1gInSc1
A310-300	A310-300	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Letadlo	letadlo	k1gNnSc1
ATR	ATR	kA
72	#num#	k4
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
na	na	k7c6
Letišti	letiště	k1gNnSc6
Zürich	Züricha	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1992	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
proměně	proměna	k1gFnSc3
Československých	československý	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
19,1	19,1	k4
%	%	kIx~
akcií	akcie	k1gFnPc2
odkoupila	odkoupit	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Air	Air	k1gMnSc2
France	Franc	k1gMnSc2
<g/>
,	,	kIx,
stejný	stejný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
získala	získat	k5eAaPmAgFnS
Evropská	evropský	k2eAgFnSc1d1
banka	banka	k1gFnSc1
pro	pro	k7c4
obnovu	obnova	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgInSc1d1
Československý	československý	k2eAgInSc1d1
fond	fond	k1gInSc1
národního	národní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
si	se	k3xPyFc3
ponechal	ponechat	k5eAaPmAgInS
49,3	49,3	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
získala	získat	k5eAaPmAgFnS
4,5	4,5	k4
%	%	kIx~
akcií	akcie	k1gFnPc2
<g/>
,	,	kIx,
necelých	celý	k2eNgInPc2d1
3,5	3,5	k4
%	%	kIx~
<g/>
připadlo	připadnout	k5eAaPmAgNnS
hla	hla	k?
vnímu	vním	k1gInSc2
městu	město	k1gNnSc3
Praha	Praha	k1gFnSc1
a	a	k8xC
2,3	2,3	k4
%	%	kIx~
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
společně	společně	k6eAd1
majetkem	majetek	k1gInSc7
měst	město	k1gNnPc2
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
a	a	k8xC
Poprad	Poprad	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
ČSA	ČSA	kA
propadly	propadnout	k5eAaPmAgInP
do	do	k7c2
velké	velký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
1,2	1,2	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
byly	být	k5eAaImAgFnP
způsobeny	způsobit	k5eAaPmNgInP
nevýhodnými	výhodný	k2eNgInPc7d1
leasingovým	leasingový	k2eAgFnPc3d1
smlouvami	smlouva	k1gFnPc7
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
si	se	k3xPyFc3
společnost	společnost	k1gFnSc1
pronajala	pronajmout	k5eAaPmAgFnS
nová	nový	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc4
západní	západní	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
vzniklé	vzniklý	k2eAgFnSc3d1
ztrátě	ztráta	k1gFnSc3
požadovali	požadovat	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
Air	Air	k1gFnSc2
France	Franc	k1gMnSc2
odstoupení	odstoupení	k1gNnSc2
od	od	k7c2
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
vrácení	vrácení	k1gNnSc2
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
do	do	k7c2
ČSA	ČSA	kA
vložili	vložit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
dohodě	dohoda	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
únoru	únor	k1gInSc6
1994	#num#	k4
a	a	k8xC
francouzský	francouzský	k2eAgInSc1d1
podíl	podíl	k1gInSc1
v	v	k7c6
ČSA	ČSA	kA
odkoupila	odkoupit	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
Konsolidační	konsolidační	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1995	#num#	k4
byly	být	k5eAaImAgFnP
Československé	československý	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
a.s.	a.s.	k?
přejmenovány	přejmenovat	k5eAaPmNgInP
na	na	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a.s.	a.s.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
1994	#num#	k4
byla	být	k5eAaImAgFnS
do	do	k7c2
flotily	flotila	k1gFnSc2
nasazena	nasazen	k2eAgNnPc4d1
první	první	k4xOgNnPc4
dvě	dva	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
typu	typ	k1gInSc2
ATR	ATR	kA
42	#num#	k4
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
pak	pak	k6eAd1
přibyly	přibýt	k5eAaPmAgInP
první	první	k4xOgNnSc4
2	#num#	k4
letouny	letoun	k1gInPc4
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
-	-	kIx~
<g/>
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
byla	být	k5eAaImAgFnS
již	již	k6eAd1
používána	používán	k2eAgFnSc1d1
a	a	k8xC
nosila	nosit	k5eAaImAgFnS
registrace	registrace	k1gFnSc1
OK-WGF	OK-WGF	k1gFnSc1
a	a	k8xC
WGG	WGG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
byly	být	k5eAaImAgFnP
otevřeny	otevřen	k2eAgFnPc1d1
linky	linka	k1gFnPc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Hannoveru	Hannover	k1gInSc2
a	a	k8xC
Stuttgartu	Stuttgart	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
(	(	kIx(
<g/>
již	již	k6eAd1
po	po	k7c6
přejmenování	přejmenování	k1gNnSc6
na	na	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
<g/>
)	)	kIx)
začaly	začít	k5eAaPmAgFnP
ČSA	ČSA	kA
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
americkou	americký	k2eAgFnSc7d1
leteckou	letecký	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Continental	Continental	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
byla	být	k5eAaImAgFnS
do	do	k7c2
flotily	flotila	k1gFnSc2
dodána	dodán	k2eAgNnPc4d1
tři	tři	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
typu	typ	k1gInSc2
Boeing	boeing	k1gInSc4
737-500	737-500	k4
a	a	k8xC
zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
z	z	k7c2
pravidelných	pravidelný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
vyřazena	vyřadit	k5eAaPmNgFnS
poslední	poslední	k2eAgNnPc4d1
tři	tři	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
sovětské	sovětský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
Tu-	Tu-	k1gFnSc2
<g/>
134	#num#	k4
<g/>
A	A	kA
a	a	k8xC
letadla	letadlo	k1gNnSc2
Tu-	Tu-	k1gFnSc2
<g/>
154	#num#	k4
<g/>
M.	M.	kA
Tyto	tento	k3xDgInPc4
stroje	stroj	k1gInPc4
byly	být	k5eAaImAgInP
pak	pak	k6eAd1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
používány	používat	k5eAaImNgFnP
pro	pro	k7c4
charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
nebo	nebo	k8xC
na	na	k7c6
nepravidelných	pravidelný	k2eNgFnPc6d1
linkách	linka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
byly	být	k5eAaImAgFnP
otevřeny	otevřen	k2eAgFnPc1d1
linky	linka	k1gFnPc1
do	do	k7c2
Osla	Oslo	k1gNnSc2
<g/>
,	,	kIx,
Nice	Nice	k1gFnSc2
a	a	k8xC
Boloně	Boloňa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
SkyTeam	SkyTeam	k1gInSc1
</s>
<s>
Letoun	letoun	k1gMnSc1
ATR-42-500	ATR-42-500	k1gFnSc2
ČSA	ČSA	kA
v	v	k7c6
barvách	barva	k1gFnPc6
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
ČSA	ČSA	kA
zahájily	zahájit	k5eAaPmAgInP
přímý	přímý	k2eAgInSc4d1
prodej	prodej	k1gInSc4
letenek	letenka	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
internetu	internet	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
ukončena	ukončen	k2eAgFnSc1d1
přeprava	přeprava	k1gFnSc1
cestujících	cestující	k1gMnPc2
pomocí	pomocí	k7c2
letadel	letadlo	k1gNnPc2
sovětské	sovětský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2001	#num#	k4
vstoupily	vstoupit	k5eAaPmAgFnP
ČSA	ČSA	kA
do	do	k7c2
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
také	také	k9
linka	linka	k1gFnSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Brna	Brno	k1gNnSc2
a	a	k8xC
Mošnova	Mošnov	k1gInSc2
u	u	k7c2
Ostravy	Ostrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
linka	linka	k1gFnSc1
na	na	k7c4
brněnské	brněnský	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
zrušena	zrušen	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
kvůli	kvůli	k7c3
konkurenci	konkurence	k1gFnSc3
znovu	znovu	k6eAd1
obnovena	obnoven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
ČSA	ČSA	kA
rozhodly	rozhodnout	k5eAaPmAgInP
o	o	k7c4
koupi	koupě	k1gFnSc4
osmi	osm	k4xCc2
německých	německý	k2eAgInPc2d1
plánovaných	plánovaný	k2eAgInPc2d1
letounech	letoun	k1gInPc6
Fairchild	Fairchild	k1gInSc4
Dornier	Dornira	k1gFnPc2
728	#num#	k4
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
přibližně	přibližně	k6eAd1
50-100	50-100	k4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
projekt	projekt	k1gInSc4
Dornieru	Dornier	k1gInSc2
nakonec	nakonec	k6eAd1
zkrachoval	zkrachovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
Airbus	airbus	k1gInSc1
A321	A321	k1gFnSc2
ČSA	ČSA	kA
ve	v	k7c6
starém	starý	k2eAgNnSc6d1
zbarvení	zbarvení	k1gNnSc6
<g/>
,	,	kIx,
2008V	2008V	k4
roce	rok	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
do	do	k7c2
flotily	flotila	k1gFnSc2
zařazena	zařazen	k2eAgMnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
první	první	k4xOgInPc1
dvě	dva	k4xCgFnPc4
letadla	letadlo	k1gNnSc2
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
320	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
,	,	kIx,
registrací	registrace	k1gFnSc7
OK-GEA	OK-GEA	k1gFnSc2
a	a	k8xC
OK-GEB	OK-GEB	k1gFnSc2
<g/>
,	,	kIx,
jména	jméno	k1gNnSc2
"	"	kIx"
<g/>
Rožnov	Rožnov	k1gInSc1
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
Strakonice	Strakonice	k1gFnPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
na	na	k7c4
krátké	krátký	k2eAgFnPc4d1
až	až	k8xS
střední	střední	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
a	a	k8xC
dvě	dva	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
321	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
,	,	kIx,
registrací	registrace	k1gFnSc7
OK-CEC	OK-CEC	k1gFnSc2
a	a	k8xC
OK-CED	OK-CED	k1gFnSc2
na	na	k7c4
charterovou	charterový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
ČSA	ČSA	kA
přepravily	přepravit	k5eAaPmAgInP
rekordních	rekordní	k2eAgInPc2d1
5,5	5,5	k4
milionů	milion	k4xCgInPc2
cestujících	cestující	k1gMnPc2
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
obdržely	obdržet	k5eAaPmAgFnP
3	#num#	k4
nové	nový	k2eAgFnSc6d1
A320	A320	k1gFnSc6
registrací	registrace	k1gFnPc2
OK-LEE	OK-LEE	k1gFnSc1
<g/>
,	,	kIx,
LEF	Lef	k1gInSc1
a	a	k8xC
LEG	lego	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
2007	#num#	k4
ČSA	ČSA	kA
obdržely	obdržet	k5eAaPmAgFnP
první	první	k4xOgNnPc4
dvě	dva	k4xCgNnPc4
nová	nový	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
typu	typ	k1gInSc2
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
ČSA	ČSA	kA
zavedly	zavést	k5eAaPmAgInP
novinku	novinka	k1gFnSc4
–	–	k?
internetové	internetový	k2eAgNnSc1d1
odbavení	odbavení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
rychlejší	rychlý	k2eAgNnSc1d2
než	než	k8xS
standardní	standardní	k2eAgNnSc1d1
odbavení	odbavení	k1gNnSc1
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
obdržely	obdržet	k5eAaPmAgFnP
ČSA	ČSA	kA
postupně	postupně	k6eAd1
další	další	k2eAgNnSc4d1
čtyři	čtyři	k4xCgNnPc4
nová	nový	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
319	#num#	k4
<g/>
-	-	kIx~
<g/>
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1
situace	situace	k1gFnSc1
společnosti	společnost	k1gFnSc2
se	se	k3xPyFc4
k	k	k7c3
roku	rok	k1gInSc3
2008	#num#	k4
prudce	prudko	k6eAd1
zhoršila	zhoršit	k5eAaPmAgFnS
<g/>
,	,	kIx,
i	i	k8xC
přes	přes	k7c4
účetní	účetní	k2eAgInSc4d1
zisk	zisk	k1gInSc4
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
z	z	k7c2
prodeje	prodej	k1gInSc2
nemovitostí	nemovitost	k1gFnPc2
v	v	k7c6
areálu	areál	k1gInSc6
Jih	jih	k1gInSc1
se	se	k3xPyFc4
vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
propadl	propadnout	k5eAaPmAgInS
o	o	k7c4
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
na	na	k7c4
pouhých	pouhý	k2eAgInPc2d1
102	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
vypsáno	vypsat	k5eAaPmNgNnS
výběrové	výběrový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
na	na	k7c4
prodej	prodej	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
ČSA	ČSA	kA
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
vláda	vláda	k1gFnSc1
po	po	k7c6
dohodě	dohoda	k1gFnSc6
s	s	k7c7
odbory	odbor	k1gInPc7
od	od	k7c2
prodeje	prodej	k1gInSc2
odstoupila	odstoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2009	#num#	k4
vláda	vláda	k1gFnSc1
v	v	k7c6
demisi	demise	k1gFnSc6
schválila	schválit	k5eAaPmAgFnS
příspěvek	příspěvek	k1gInSc1
27,2	27,2	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
na	na	k7c4
odstranění	odstranění	k1gNnSc4
ekologických	ekologický	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
svou	svůj	k3xOyFgFnSc4
činností	činnost	k1gFnPc2
způsobily	způsobit	k5eAaPmAgFnP
<g />
.	.	kIx.
</s>
<s hack="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
na	na	k7c6
ruzyňském	ruzyňský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
zjištěna	zjištěn	k2eAgFnSc1d1
kontaminace	kontaminace	k1gFnSc1
pozemků	pozemek	k1gInPc2
chlorovanými	chlorovaný	k2eAgInPc7d1
uhlovodíky	uhlovodík	k1gInPc7
a	a	k8xC
ropnými	ropný	k2eAgInPc7d1
uhlovodíky	uhlovodík	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
dle	dle	k7c2
analýzy	analýza	k1gFnSc2
Arthur	Arthur	k1gMnSc1
D	D	kA
Little	Little	k1gFnPc1
dosahovaly	dosahovat	k5eAaImAgFnP
platy	plat	k1gInPc4
některých	některý	k3yIgMnPc2
pilotů	pilot	k1gMnPc2
přes	přes	k7c4
350	#num#	k4
tis	tis	k1gInSc4
Kč	Kč	kA
a	a	k8xC
odbory	odbor	k1gInPc1
pilotů	pilot	k1gMnPc2
na	na	k7c6
základě	základ	k1gInSc6
kolektivní	kolektivní	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
odmítaly	odmítat	k5eAaImAgFnP
jejich	jejich	k3xOp3gNnSc4
snížení	snížení	k1gNnSc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
ztráta	ztráta	k1gFnSc1
firmy	firma	k1gFnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
roce	rok	k1gInSc6
dosahovala	dosahovat	k5eAaImAgFnS
3,5	3,5	k4
miliardy	miliarda	k4xCgFnSc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolektivní	kolektivní	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
podepsaná	podepsaný	k2eAgFnSc1d1
pod	pod	k7c7
Tvrdíkovým	Tvrdíkův	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
zároveň	zároveň	k6eAd1
pilotům	pilot	k1gInPc3
garantovala	garantovat	k5eAaBmAgFnS
každoroční	každoroční	k2eAgInSc4d1
patnáctiprocentní	patnáctiprocentní	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2007	#num#	k4
společnost	společnost	k1gFnSc1
představila	představit	k5eAaPmAgFnS
internetový	internetový	k2eAgInSc4d1
prodejní	prodejní	k2eAgInSc4d1
systém	systém	k1gInSc4
Click	Clicka	k1gFnPc2
<g/>
4	#num#	k4
<g/>
Sky	Sky	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
konkurovat	konkurovat	k5eAaImF
nízkonákladovým	nízkonákladový	k2eAgFnPc3d1
leteckým	letecký	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
<g/>
,	,	kIx,
provozovatelem	provozovatel	k1gMnSc7
byla	být	k5eAaImAgFnS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ClickforSky	ClickforSka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
nabízela	nabízet	k5eAaImAgFnS
lety	let	k1gInPc4
do	do	k7c2
převážně	převážně	k6eAd1
evropských	evropský	k2eAgFnPc2d1
destinací	destinace	k1gFnPc2
za	za	k7c4
jednotnou	jednotný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
včetně	včetně	k7c2
poplatků	poplatek	k1gInPc2
a	a	k8xC
tax	taxa	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
přeprodávala	přeprodávat	k5eAaImAgFnS,k5eAaPmAgFnS
tak	tak	k9
volné	volný	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
letů	let	k1gInPc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc4
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
provozu	provoz	k1gInSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
2009	#num#	k4
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stránka	stránka	k1gFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
skončení	skončení	k1gNnSc6
dále	daleko	k6eAd2
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
zobrazovaly	zobrazovat	k5eAaImAgInP
pouze	pouze	k6eAd1
vybrané	vybraný	k2eAgInPc4d1
levné	levný	k2eAgInPc4d1
spoje	spoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
kritiků	kritik	k1gMnPc2
ČSA	ČSA	kA
za	za	k7c4
propagaci	propagace	k1gFnSc4
této	tento	k3xDgFnSc2
značky	značka	k1gFnSc2
prodělávaly	prodělávat	k5eAaImAgInP
pouze	pouze	k6eAd1
peníze	peníz	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2009	#num#	k4
vlastnila	vlastnit	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
51	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Historicky	historicky	k6eAd1
nejdelší	dlouhý	k2eAgInSc1d3
let	let	k1gInSc1
společnosti	společnost	k1gFnSc2
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
trval	trvat	k5eAaImAgInS
11	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
Airbus	airbus	k1gInSc4
A310	A310	k1gFnSc2
letěl	letět	k5eAaImAgMnS
bez	bez	k7c2
mezipřistání	mezipřistání	k1gNnSc2
z	z	k7c2
jihoamerického	jihoamerický	k2eAgMnSc2d1
San	San	k1gMnSc2
Salvadoru	Salvador	k1gInSc2
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
za	za	k7c7
hranicí	hranice	k1gFnSc7
maximální	maximální	k2eAgFnSc2d1
nominální	nominální	k2eAgFnSc2d1
délky	délka	k1gFnSc2
doletu	dolet	k1gInSc2
letounu	letoun	k1gInSc2
A	a	k9
<g/>
310	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdále	daleko	k6eAd3
od	od	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
letoun	letoun	k1gInSc1
ČSA	ČSA	kA
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
chilském	chilský	k2eAgNnSc6d1
Letišti	letiště	k1gNnSc6
Punta	punto	k1gNnSc2
Arenas	Arenasa	k1gFnPc2
na	na	k7c6
Ohňové	ohňový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letěl	letět	k5eAaImAgMnS
tam	tam	k6eAd1
se	s	k7c7
dvěma	dva	k4xCgNnPc7
mezipřistáními	mezipřistání	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Budova	budova	k1gFnSc1
APC	APC	kA
<g/>
,	,	kIx,
bývalé	bývalý	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
ČSA	ČSA	kA
na	na	k7c6
ruzyňském	ruzyňský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
společnost	společnost	k1gFnSc1
zaměstnávala	zaměstnávat	k5eAaImAgFnS
415	#num#	k4
pilotů	pilot	k1gMnPc2
a	a	k8xC
752	#num#	k4
palubních	palubní	k2eAgFnPc2d1
průvodčích	průvodčí	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
celkem	celkem	k6eAd1
292	#num#	k4
pilotů	pilot	k1gMnPc2
a	a	k8xC
613	#num#	k4
letušek	letuška	k1gFnPc2
a	a	k8xC
stevardů	stevard	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
srpnu	srpen	k1gInSc6
2014	#num#	k4
přes	přes	k7c4
230	#num#	k4
pilotů	pilot	k1gMnPc2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
400	#num#	k4
palubních	palubní	k2eAgFnPc2d1
průvodčích	průvodčí	k1gFnPc2
a	a	k8xC
270	#num#	k4
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
administrativě	administrativa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
koncem	koncem	k7c2
září	září	k1gNnSc2
2014	#num#	k4
ČSA	ČSA	kA
oznámily	oznámit	k5eAaPmAgInP
úřadu	úřad	k1gInSc3
práce	práce	k1gFnSc2
propuštění	propuštění	k1gNnSc2
77	#num#	k4
pilotů	pilot	k1gMnPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
stevardů	stevard	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
odbory	odbor	k1gInPc1
vyhlásily	vyhlásit	k5eAaPmAgInP
stávkovou	stávkový	k2eAgFnSc4d1
pohotovost	pohotovost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
ČSA	ČSA	kA
odvrátily	odvrátit	k5eAaPmAgInP
redukcí	redukce	k1gFnSc7
útlumového	útlumový	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjmy	příjem	k1gInPc1
zaměstnanců	zaměstnanec	k1gMnPc2
však	však	k8xC
klesaly	klesat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
75	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
ukončily	ukončit	k5eAaPmAgInP
ČSA	ČSA	kA
pravidelné	pravidelný	k2eAgFnPc4d1
linky	linka	k1gFnPc4
do	do	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
vnitrostátní	vnitrostátní	k2eAgFnSc4d1
linku	linka	k1gFnSc4
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
po	po	k7c6
nich	on	k3xPp3gFnPc6
převzaly	převzít	k5eAaPmAgInP
Central	Central	k1gMnPc3
Connect	Connect	k1gInSc4
Airlines	Airlinesa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
ji	on	k3xPp3gFnSc4
pro	pro	k7c4
ČSA	ČSA	kA
už	už	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
předtím	předtím	k6eAd1
zajišťovaly	zajišťovat	k5eAaImAgInP
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
Boeing	boeing	k1gInSc4
737-500	737-500	k4
ČSA	ČSA	kA
ve	v	k7c6
Frankfurtu	Frankfurt	k1gInSc6
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
modernizována	modernizován	k2eAgFnSc1d1
s	s	k7c7
novými	nový	k2eAgInPc7d1
Airbusy	airbus	k1gInPc7
A319	A319	k1gFnSc2
a	a	k8xC
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zapříčinilo	zapříčinit	k5eAaPmAgNnS
úplné	úplný	k2eAgNnSc1d1
vyřazení	vyřazení	k1gNnSc1
všech	všecek	k3xTgInPc2
Boeingů	boeing	k1gInPc2
737	#num#	k4
verze	verze	k1gFnSc1
400	#num#	k4
z	z	k7c2
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc4d1
let	let	k1gInSc4
Boeingu	boeing	k1gInSc2
737-400	737-400	k4
u	u	k7c2
ČSA	ČSA	kA
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
(	(	kIx(
<g/>
OK-FGS	OK-FGS	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
postupně	postupně	k6eAd1
kvůli	kvůli	k7c3
úsporným	úsporný	k2eAgNnPc3d1
opatřením	opatření	k1gNnPc3
odprodaly	odprodat	k5eAaPmAgFnP
řadu	řada	k1gFnSc4
dceřiných	dceřin	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
:	:	kIx,
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
Training	Training	k1gInSc4
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
Air	Air	k1gMnSc7
Czech	Czecha	k1gFnPc2
Catering	Catering	k1gInSc1
<g/>
,	,	kIx,
Czech	Czech	k1gInSc1
Airlines	Airlines	k1gInSc1
Handling	Handling	k1gInSc1
(	(	kIx(
<g/>
CSA	CSA	kA
Services	Services	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
nákladový	nákladový	k2eAgInSc1d1
terminál	terminál	k1gInSc1
na	na	k7c6
ruzyňském	ruzyňský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
společnost	společnost	k1gFnSc1
vykázala	vykázat	k5eAaPmAgFnS
ztrátu	ztráta	k1gFnSc4
241	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
při	při	k7c6
tržbách	tržba	k1gFnPc6
14	#num#	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
skončila	skončit	k5eAaPmAgFnS
s	s	k7c7
účetním	účetní	k2eAgInSc7d1
ziskem	zisk	k1gInSc7
849	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
při	při	k7c6
tržbách	tržba	k1gFnPc6
13,7	13,7	k4
miliardy	miliarda	k4xCgFnPc4
Kč	Kč	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
ČSA	ČSA	kA
skončily	skončit	k5eAaPmAgInP
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
922	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
při	při	k7c6
tržbách	tržba	k1gFnPc6
13,3	13,3	k4
miliardy	miliarda	k4xCgFnPc4
Kč	Kč	kA
<g/>
,	,	kIx,
ztrátovou	ztrátový	k2eAgFnSc4d1
zimní	zimní	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
musel	muset	k5eAaImAgInS
Český	český	k2eAgInSc1d1
Aeroholding	Aeroholding	k1gInSc4
sanovat	sanovat	k5eAaBmF
podporou	podpora	k1gFnSc7
152	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2012	#num#	k4
se	s	k7c7
ČSA	ČSA	kA
staly	stát	k5eAaPmAgFnP
dceřinou	dceřin	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
státního	státní	k2eAgInSc2d1
Českého	český	k2eAgInSc2d1
Aeroholdingu	Aeroholding	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
postupně	postupně	k6eAd1
sdružoval	sdružovat	k5eAaImAgInS
i	i	k8xC
letiště	letiště	k1gNnSc4
Praha	Praha	k1gFnSc1
a	a	k8xC
servisní	servisní	k2eAgFnPc1d1
letištní	letištní	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
Czech	Czecha	k1gFnPc2
Airlines	Airlinesa	k1gFnPc2
Handling	Handling	k1gInSc1
a	a	k8xC
Czech	Czech	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
Technics	Technics	kA
spolu	spolu	k6eAd1
s	s	k7c7
nízkonákladovou	nízkonákladový	k2eAgFnSc7d1
aerolinií	aerolinie	k1gFnSc7
Holidays	Holidaysa	k1gFnPc2
Czech	Czecha	k1gFnPc2
Airlines	Airlines	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
společnost	společnost	k1gFnSc1
oslavovala	oslavovat	k5eAaImAgFnS
90	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
od	od	k7c2
vzniku	vznik	k1gInSc2
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
opustilo	opustit	k5eAaPmAgNnS
flotilu	flotila	k1gFnSc4
ČSA	ČSA	kA
posledních	poslední	k2eAgInPc2d1
pět	pět	k4xCc4
Boeingů	boeing	k1gInPc2
737-500	737-500	k4
(	(	kIx(
<g/>
registrace	registrace	k1gFnSc2
OK-XGA	OK-XGA	k1gFnSc2
<g/>
,	,	kIx,
XGB	XGB	kA
<g/>
,	,	kIx,
XGC	XGC	kA
<g/>
,	,	kIx,
XGD	XGD	kA
a	a	k8xC
XGE	XGE	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
letadla	letadlo	k1gNnPc4
vydržela	vydržet	k5eAaPmAgFnS
u	u	k7c2
ČSA	ČSA	kA
nejdéle	dlouho	k6eAd3
<g/>
,	,	kIx,
všechna	všechen	k3xTgFnSc1
byla	být	k5eAaImAgFnS
dodaná	dodaný	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
skládala	skládat	k5eAaImAgFnS
výlučně	výlučně	k6eAd1
z	z	k7c2
Airbusů	airbus	k1gInPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
typu	typ	k1gInSc2
ATR	ATR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
společnost	společnost	k1gFnSc1
Korean	Koreana	k1gFnPc2
Air	Air	k1gFnSc1
odkoupila	odkoupit	k5eAaPmAgFnS
od	od	k7c2
Českého	český	k2eAgInSc2d1
Aeroholdingu	Aeroholding	k1gInSc2
část	část	k1gFnSc1
státního	státní	k2eAgInSc2d1
podílu	podíl	k1gInSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
44	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Travel	Travel	k1gInSc4
Service	Service	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
dokončila	dokončit	k5eAaPmAgFnS
odkup	odkup	k1gInSc4
34	#num#	k4
%	%	kIx~
podílu	podíl	k1gInSc2
od	od	k7c2
Českého	český	k2eAgInSc2d1
Aeroholdingu	Aeroholding	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
opce	opce	k1gFnSc2
od	od	k7c2
Korean	Koreana	k1gFnPc2
Air	Air	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
2014	#num#	k4
schválila	schválit	k5eAaPmAgFnS
tyto	tento	k3xDgFnPc4
transakce	transakce	k1gFnPc4
Evropská	evropský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
příslušné	příslušný	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
soutěžní	soutěžní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgInSc1d1
Český	český	k2eAgInSc1d1
Aeroholding	Aeroholding	k1gInSc1
nadále	nadále	k6eAd1
držel	držet	k5eAaImAgInS
v	v	k7c6
ČSA	ČSA	kA
už	už	k6eAd1
jen	jen	k9
cca	cca	kA
pětinový	pětinový	k2eAgInSc4d1
podíl	podíl	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ČSA	ČSA	kA
měla	mít	k5eAaImAgFnS
od	od	k7c2
května	květen	k1gInSc2
2013	#num#	k4
pronajatý	pronajatý	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
A330	A330	k1gFnSc2
od	od	k7c2
partnerské	partnerský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Korean	Korean	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
proběhly	proběhnout	k5eAaPmAgInP
pokusy	pokus	k1gInPc1
o	o	k7c4
využití	využití	k1gNnSc4
tohoto	tento	k3xDgNnSc2
letadla	letadlo	k1gNnSc2
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
kapacity	kapacita	k1gFnSc2
letů	let	k1gInPc2
do	do	k7c2
destinací	destinace	k1gFnPc2
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Tel	tel	kA
Aviv	Aviv	k1gInSc1
a	a	k8xC
Almaty	Almata	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
ovšem	ovšem	k9
neprojevily	projevit	k5eNaPmAgInP
jako	jako	k9
ekonomicky	ekonomicky	k6eAd1
přínosné	přínosný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
Airbus	airbus	k1gInSc4
A330	A330	k1gMnPc2
obsluhoval	obsluhovat	k5eAaImAgMnS
pravidelné	pravidelný	k2eAgInPc4d1
lety	let	k1gInPc4
do	do	k7c2
Soulu	Soul	k1gInSc2
a	a	k8xC
příležitostně	příležitostně	k6eAd1
létal	létat	k5eAaImAgInS
na	na	k7c4
vytíženější	vytížený	k2eAgFnPc4d2
destinace	destinace	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
například	například	k6eAd1
při	při	k7c6
sobotních	sobotní	k2eAgFnPc6d1
rotacích	rotace	k1gFnPc6
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
dopravce	dopravce	k1gMnSc1
oslavil	oslavit	k5eAaPmAgMnS
90	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc4
od	od	k7c2
vzniku	vznik	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
oslavoval	oslavovat	k5eAaImAgInS
také	také	k9
nátěr	nátěr	k1gInSc1
na	na	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
ČSA	ČSA	kA
obnovily	obnovit	k5eAaPmAgInP
po	po	k7c4
dvouroční	dvouroční	k2eAgNnSc4d1
pauze	pauza	k1gFnSc3
lety	let	k1gInPc7
z	z	k7c2
bratislavského	bratislavský	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
do	do	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
Košic	Košice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
těchto	tento	k3xDgFnPc6
trasách	trasa	k1gFnPc6
létaly	létat	k5eAaImAgFnP
turbovrtulové	turbovrtulový	k2eAgInPc4d1
stroje	stroj	k1gInPc4
ATR	ATR	kA
42	#num#	k4
nebo	nebo	k8xC
ATR	ATR	kA
72	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byly	být	k5eAaImAgFnP
rozprodány	rozprodat	k5eAaPmNgFnP
všechny	všechen	k3xTgFnPc1
letouny	letoun	k1gInPc4
Airbus	airbus	k1gInSc1
A320	A320	k1gMnPc2
kvůli	kvůli	k7c3
neúnosné	únosný	k2eNgFnSc3d1
ekonomické	ekonomický	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
–	–	k?
málokdy	málokdy	k6eAd1
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
podařilo	podařit	k5eAaPmAgNnS
naplnit	naplnit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
palubě	paluba	k1gFnSc6
Airbusu	airbus	k1gInSc2
A319	A319	k1gFnSc1
registace	registace	k1gFnSc1
OK-NEP	OK-NEP	k1gFnSc1
během	během	k7c2
letu	let	k1gInSc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Jerevanu	Jerevan	k1gMnSc3
k	k	k7c3
vážnému	vážný	k2eAgInSc3d1
incidentu	incident	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
Černým	černý	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
náhlému	náhlý	k2eAgMnSc3d1
a	a	k8xC
významnému	významný	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
tlaku	tlak	k1gInSc2
v	v	k7c6
kabině	kabina	k1gFnSc6
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
nouzové	nouzový	k2eAgNnSc1d1
klesání	klesání	k1gNnSc1
a	a	k8xC
přistání	přistání	k1gNnSc1
na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Burgasu	Burgas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedošlo	dojít	k5eNaPmAgNnS
ke	k	k7c3
zranění	zranění	k1gNnSc3
žádného	žádný	k1gMnSc2
cestujícího	cestující	k1gMnSc2
ani	ani	k8xC
člena	člen	k1gMnSc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
2014	#num#	k4
byl	být	k5eAaImAgInS
Airbus	airbus	k1gInSc1
A319	A319	k1gMnSc2
OK-NEP	OK-NEP	k1gMnSc2
opatřen	opatřit	k5eAaPmNgInS
nátěrem	nátěr	k1gInSc7
propagujícím	propagující	k2eAgInSc7d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
trupu	trup	k1gInSc6
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
panorama	panorama	k1gNnSc1
Hradčan	Hradčany	k1gInPc2
a	a	k8xC
nápis	nápis	k1gInSc1
„	„	k?
<g/>
Fly	Fly	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
City	city	k1gNnSc1
of	of	k?
Magic	Magice	k1gInPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letní	letní	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
2015	#num#	k4
společnost	společnost	k1gFnSc1
provozovala	provozovat	k5eAaImAgFnS
celkem	celkem	k6eAd1
17	#num#	k4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flotilu	flotila	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
devět	devět	k4xCc4
letadel	letadlo	k1gNnPc2
Airbus	airbus	k1gInSc4
A	a	k8xC
<g/>
319	#num#	k4
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
dálkový	dálkový	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A330	A330	k1gFnPc2
a	a	k8xC
sedm	sedm	k4xCc1
vrtulových	vrtulový	k2eAgInPc2d1
ATR	ATR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
společnost	společnost	k1gFnSc1
přepravila	přepravit	k5eAaPmAgFnS
2,26	2,26	k4
milionů	milion	k4xCgInPc2
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představuje	představovat	k5eAaImIp3nS
nárůst	nárůst	k1gInSc4
o	o	k7c4
13	#num#	k4
procent	procento	k1gNnPc2
oproti	oproti	k7c3
roku	rok	k1gInSc3
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Průměrná	průměrný	k2eAgFnSc1d1
obsazenost	obsazenost	k1gFnSc1
na	na	k7c6
pravidelných	pravidelný	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
se	se	k3xPyFc4
zvýšila	zvýšit	k5eAaPmAgFnS
o	o	k7c4
10	#num#	k4
%	%	kIx~
oproti	oproti	k7c3
roku	rok	k1gInSc3
2015	#num#	k4
na	na	k7c4
75,4	75,4	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvýšil	zvýšit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
počet	počet	k1gInSc1
letů	let	k1gInPc2
(	(	kIx(
<g/>
o	o	k7c4
9	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zapříčilo	zapříčit	k5eAaPmAgNnS
růst	růst	k5eAaImF
nabízené	nabízený	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
v	v	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
zvedla	zvednout	k5eAaPmAgFnS
o	o	k7c4
3,5	3,5	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
společnost	společnost	k1gFnSc1
přesídlila	přesídlit	k5eAaPmAgFnS
z	z	k7c2
letiště	letiště	k1gNnSc2
na	na	k7c4
Evropskou	evropský	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
,	,	kIx,
také	také	k9
kvůli	kvůli	k7c3
nižším	nízký	k2eAgInPc3d2
nájmům	nájem	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2016	#num#	k4
společnost	společnost	k1gFnSc1
objednala	objednat	k5eAaPmAgFnS
sedm	sedm	k4xCc4
letadel	letadlo	k1gNnPc2
úspornější	úsporný	k2eAgFnSc2d2
nové	nový	k2eAgFnSc2d1
řady	řada	k1gFnSc2
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
obdržet	obdržet	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
jejich	jejich	k3xOp3gFnSc4
objednávku	objednávka	k1gFnSc4
snížila	snížit	k5eAaPmAgFnS
na	na	k7c4
pouhé	pouhý	k2eAgInPc4d1
tři	tři	k4xCgInPc4
kusy	kus	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
objednávka	objednávka	k1gFnSc1
konvertována	konvertován	k2eAgFnSc1d1
na	na	k7c4
3	#num#	k4
Airbusy	airbus	k1gInPc4
A	a	k8xC
<g/>
321	#num#	k4
<g/>
XLR	XLR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
s	s	k7c7
nimi	on	k3xPp3gMnPc7
plánuje	plánovat	k5eAaImIp3nS
návrat	návrat	k1gInSc1
na	na	k7c4
trans-atlantické	trans-atlantický	k2eAgFnPc4d1
linky	linka	k1gFnPc4
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoje	stoj	k1gInSc2
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
dodány	dodat	k5eAaPmNgInP
v	v	k7c6
letech	léto	k1gNnPc6
2023	#num#	k4
a	a	k8xC
2024	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2017	#num#	k4
létalo	létat	k5eAaImAgNnS
pro	pro	k7c4
společnost	společnost	k1gFnSc4
18	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
po	po	k7c6
letech	let	k1gInPc6
společnost	společnost	k1gFnSc1
nabrala	nabrat	k5eAaPmAgFnS
nové	nový	k2eAgMnPc4d1
zaměstnance	zaměstnanec	k1gMnPc4
–	–	k?
15	#num#	k4
pilotů	pilot	k1gInPc2
<g/>
,	,	kIx,
30	#num#	k4
sezónních	sezónní	k2eAgFnPc2d1
letušek	letuška	k1gFnPc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
IT	IT	kA
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
právníků	právník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smartwings	Smartwings	k1gInSc1
a	a	k8xC
léta	léto	k1gNnSc2
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
majoritním	majoritní	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
stala	stát	k5eAaPmAgFnS
společnost	společnost	k1gFnSc4
Smartwings	Smartwingsa	k1gFnPc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
získala	získat	k5eAaPmAgFnS
podíl	podíl	k1gInSc4
97,74	97,74	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Odkoupila	odkoupit	k5eAaPmAgFnS
totiž	totiž	k9
44	#num#	k4
%	%	kIx~
akcií	akcie	k1gFnPc2
od	od	k7c2
společnosti	společnost	k1gFnSc2
Korean	Koreany	k1gInPc2
Air	Air	k1gFnSc2
a	a	k8xC
20	#num#	k4
%	%	kIx~
od	od	k7c2
státního	státní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
Prisko	Prisko	k1gNnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
34	#num#	k4
%	%	kIx~
již	již	k6eAd1
předtím	předtím	k6eAd1
vlastnila	vlastnit	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
2,26	2,26	k4
%	%	kIx~
zůstalo	zůstat	k5eAaPmAgNnS
České	český	k2eAgFnSc3d1
pojišťovně	pojišťovna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
krátkodobému	krátkodobý	k2eAgInSc3d1
návratu	návrat	k1gInSc3
letadel	letadlo	k1gNnPc2
Airbus	airbus	k1gInSc4
A321	A321	k1gMnPc1
do	do	k7c2
flotily	flotila	k1gFnSc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadlo	letadlo	k1gNnSc1
registrace	registrace	k1gFnSc2
OY-RUU	OY-RUU	k1gFnPc2
bylo	být	k5eAaImAgNnS
pronajato	pronajmout	k5eAaPmNgNnS
od	od	k7c2
dánské	dánský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Danish	Danisha	k1gFnPc2
Air	Air	k1gFnSc7
Transport	transporta	k1gFnPc2
a	a	k8xC
u	u	k7c2
společnosti	společnost	k1gFnSc2
létalo	létat	k5eAaImAgNnS
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
řijna	řijn	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c4
odkoupení	odkoupení	k1gNnSc4
společností	společnost	k1gFnPc2
Travel	Travela	k1gFnPc2
Service	Service	k1gFnSc2
byly	být	k5eAaImAgInP
ČSA	ČSA	kA
přiřazeny	přiřadit	k5eAaPmNgInP
do	do	k7c2
koncernu	koncern	k1gInSc2
Smartwings	Smartwingsa	k1gFnPc2
Group	Group	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
hodlal	hodlat	k5eAaImAgMnS
ČSA	ČSA	kA
zbavit	zbavit	k5eAaPmF
flotily	flotila	k1gFnPc4
Airbusů	airbus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
měly	mít	k5eAaImAgInP
nahradit	nahradit	k5eAaPmF
unifikované	unifikovaný	k2eAgInPc1d1
Boeingy	boeing	k1gInPc1
737	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
uzemnění	uzemnění	k1gNnSc6
Boeingů	boeing	k1gInPc2
737	#num#	k4
MAX	Max	k1gMnSc1
kvůli	kvůli	k7c3
technickým	technický	k2eAgInPc3d1
nedostatkům	nedostatek	k1gInPc3
a	a	k8xC
leteckým	letecký	k2eAgNnSc7d1
neštěstím	neštěstí	k1gNnSc7
jiných	jiný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
však	však	k9
k	k	k7c3
přehodnocení	přehodnocení	k1gNnSc3
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
měly	mít	k5eAaImAgInP
dále	daleko	k6eAd2
létat	létat	k5eAaImF
s	s	k7c7
Airbusy	airbus	k1gInPc7
a	a	k8xC
Smartwings	Smartwings	k1gInSc1
si	se	k3xPyFc3
měl	mít	k5eAaImAgInS
ponechat	ponechat	k5eAaPmF
Boeingy	boeing	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
2018	#num#	k4
a	a	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
2019	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rušení	rušení	k1gNnSc3
mnoha	mnoho	k4c2
linek	linka	k1gFnPc2
ČSA	ČSA	kA
<g/>
,	,	kIx,
například	například	k6eAd1
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2
<g/>
,	,	kIx,
Ostravy	Ostrava	k1gFnSc2
a	a	k8xC
z	z	k7c2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
společnost	společnost	k1gFnSc1
létala	létat	k5eAaImAgFnS
již	již	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
ukončily	ukončit	k5eAaPmAgInP
ČSA	ČSA	kA
provozování	provozování	k1gNnSc3
výnosných	výnosný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
do	do	k7c2
vybraných	vybraný	k2eAgNnPc2d1
ruských	ruský	k2eAgNnPc2d1
měst	město	k1gNnPc2
–	–	k?
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
Petrohrad	Petrohrad	k1gInSc4
<g/>
,	,	kIx,
Kazaň	Kazaň	k1gFnSc1
<g/>
,	,	kIx,
Samara	Samara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
tyto	tento	k3xDgFnPc4
linky	linka	k1gFnPc4
ihned	ihned	k6eAd1
převzala	převzít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Smartwings	Smartwingsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
dubna	duben	k1gInSc2
2019	#num#	k4
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
redukována	redukován	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
Airbusů	airbus	k1gInPc2
A	a	k9
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
opustily	opustit	k5eAaPmAgFnP
flotilu	flotila	k1gFnSc4
stroje	stroj	k1gInSc2
s	s	k7c7
registrací	registrace	k1gFnSc7
OK-MEK	OK-MEK	k1gFnPc2
<g/>
,	,	kIx,
MEL	mlít	k5eAaImRp2nS
a	a	k8xC
PET	PET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
jmenovaný	jmenovaný	k2eAgInSc4d1
stroj	stroj	k1gInSc4
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
flotily	flotila	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
létě	léto	k1gNnSc6
2019	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
jediné	jediný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
typu	typ	k1gInSc2
A319	A319	k1gFnSc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
létalo	létat	k5eAaImAgNnS
z	z	k7c2
Letiště	letiště	k1gNnSc2
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
ledna	leden	k1gInSc2
2020	#num#	k4
byla	být	k5eAaImAgFnS
oznámena	oznámit	k5eAaPmNgFnS
plánovaná	plánovaný	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
přepravní	přepravní	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
flotily	flotila	k1gFnSc2
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
zařazeny	zařadit	k5eAaPmNgInP
až	až	k9
čtyři	čtyři	k4xCgInPc1
Airbusy	airbus	k1gInPc1
A	a	k9
<g/>
320	#num#	k4
<g/>
,	,	kIx,
Airbusy	airbus	k1gInPc1
A319	A319	k1gFnPc2
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
létající	létající	k2eAgFnSc2d1
v	v	k7c6
leasingu	leasing	k1gInSc6
pro	pro	k7c4
německou	německý	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Eurowings	Eurowingsa	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
zpět	zpět	k6eAd1
a	a	k8xC
pronájem	pronájem	k1gInSc4
letadla	letadlo	k1gNnSc2
Airbus	airbus	k1gInSc1
A330	A330	k1gFnSc4
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
dále	daleko	k6eAd2
prodloužen	prodloužen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
flotily	flotila	k1gFnSc2
ČSA	ČSA	kA
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
skutečně	skutečně	k6eAd1
vrátil	vrátit	k5eAaPmAgInS
jeden	jeden	k4xCgInSc1
letoun	letoun	k1gInSc1
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-HEU	OK-HEU	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Plánované	plánovaný	k2eAgNnSc1d1
zařazení	zařazení	k1gNnSc1
dalších	další	k2eAgNnPc2d1
tří	tři	k4xCgNnPc2
letadel	letadlo	k1gNnPc2
stejného	stejný	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
měly	mít	k5eAaImAgFnP
nést	nést	k5eAaImF
registrace	registrace	k1gFnPc1
OK-REV	OK-REV	k1gMnSc1
<g/>
,	,	kIx,
REW	REW	kA
a	a	k8xC
TEX	TEX	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
posléze	posléze	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
zrušeno	zrušit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
světové	světový	k2eAgFnSc3d1
pandemii	pandemie	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
konci	konec	k1gInSc6
února	únor	k1gInSc2
k	k	k7c3
přerušení	přerušení	k1gNnSc3
linky	linka	k1gFnSc2
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
jihokorejského	jihokorejský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Soulu	Soul	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
nařízení	nařízení	k1gNnSc2
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
přerušily	přerušit	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
spolu	spolu	k6eAd1
se	se	k3xPyFc4
Smartwings	Smartwings	k1gInSc1
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
covidu-	covidu-	k?
<g/>
19	#num#	k4
veškerý	veškerý	k3xTgInSc4
provoz	provoz	k1gInSc4
svých	svůj	k3xOyFgInPc2
letů	let	k1gInPc2
z	z	k7c2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vyřazení	vyřazení	k1gNnSc3
dvou	dva	k4xCgNnPc2
Airbusů	airbus	k1gInPc2
A319	A319	k1gFnSc2
(	(	kIx(
<g/>
OK-MEL	OK-MEL	k1gMnSc1
a	a	k8xC
OK-NEN	OK-NEN	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2020	#num#	k4
obnovily	obnovit	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
linky	linka	k1gFnSc2
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
Amsterdamu	Amsterdam	k1gInSc2
a	a	k8xC
Frankfurtu	Frankfurt	k1gInSc2
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
obnovy	obnova	k1gFnPc1
dočkaly	dočkat	k5eAaPmAgFnP
linky	linka	k1gFnPc4
do	do	k7c2
Stockholmu	Stockholm	k1gInSc2
a	a	k8xC
Bukurešti	Bukurešť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
června	červen	k1gInSc2
byly	být	k5eAaImAgFnP
postupně	postupně	k6eAd1
obnoveny	obnoven	k2eAgFnPc1d1
linky	linka	k1gFnPc1
do	do	k7c2
Košic	Košice	k1gInPc2
<g/>
,	,	kIx,
Budapešti	Budapešť	k1gFnSc2
<g/>
,	,	kIx,
Keflavíku	Keflavík	k1gInSc2
a	a	k8xC
Kyjeva	Kyjev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
byla	být	k5eAaImAgFnS
téměř	téměř	k6eAd1
po	po	k7c6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
otevřena	otevřít	k5eAaPmNgFnS
linka	linka	k1gFnSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
na	na	k7c4
Letiště	letiště	k1gNnSc4
London	London	k1gMnSc1
Heathrow	Heathrow	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
začala	začít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
znovu	znovu	k6eAd1
létat	létat	k5eAaImF
do	do	k7c2
Oděsy	Oděsa	k1gFnSc2
<g/>
,	,	kIx,
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Barcelony	Barcelona	k1gFnSc2
<g/>
,	,	kIx,
Göteborgu	Göteborg	k1gInSc2
<g/>
,	,	kIx,
Madridu	Madrid	k1gInSc2
a	a	k8xC
Helsinek	Helsinky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
se	se	k3xPyFc4
obnovení	obnovení	k1gNnSc2
dočkala	dočkat	k5eAaPmAgFnS
linka	linka	k1gFnSc1
do	do	k7c2
Kodaně	Kodaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
ten	ten	k3xDgInSc4
samý	samý	k3xTgInSc4
den	den	k1gInSc4
byl	být	k5eAaImAgInS
oznámen	oznámen	k2eAgInSc1d1
přechod	přechod	k1gInSc1
celé	celý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Smartwings	Smartwingsa	k1gFnPc2
Group	Group	k1gInSc4
od	od	k7c2
handlingové	handlingový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Menzies	Menzies	k1gMnSc1
k	k	k7c3
Czech	Czech	k1gInSc1
Airlines	Airlines	k1gMnSc1
Handling	Handling	k1gInSc1
(	(	kIx(
<g/>
CSAH	CSAH	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
července	červenec	k1gInSc2
2020	#num#	k4
byl	být	k5eAaImAgInS
z	z	k7c2
flotily	flotila	k1gFnSc2
vyřazen	vyřazen	k2eAgInSc4d1
jediný	jediný	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A330	A330	k1gFnSc2
a	a	k8xC
vrácen	vrátit	k5eAaPmNgInS
majiteli	majitel	k1gMnSc3
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc3
Korean	Koreany	k1gInPc2
Air	Air	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
dálková	dálkový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
ČSA	ČSA	kA
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Soulu	Soul	k1gInSc2
už	už	k9
nebude	být	k5eNaImBp3nS
obnovena	obnoven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
má	mít	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
propuštění	propuštění	k1gNnSc3
více	hodně	k6eAd2
než	než	k8xS
poloviny	polovina	k1gFnSc2
personálu	personál	k1gInSc2
(	(	kIx(
<g/>
snížení	snížení	k1gNnSc1
stavu	stav	k1gInSc2
z	z	k7c2
cca	cca	kA
700	#num#	k4
na	na	k7c4
300	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
zeštíhlení	zeštíhlení	k1gNnSc1
flotily	flotila	k1gFnSc2
na	na	k7c4
pouhých	pouhý	k2eAgNnPc2d1
pět	pět	k4xCc4
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
ATR-	ATR-	k1gFnPc2
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
×	×	k?
A320	A320	k1gMnPc2
a	a	k8xC
1	#num#	k4
<g/>
×	×	k?
B	B	kA
<g/>
737	#num#	k4
<g/>
-	-	kIx~
<g/>
800	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
akcionáře	akcionář	k1gMnSc2
(	(	kIx(
<g/>
společnosti	společnost	k1gFnSc2
Smartwings	Smartwings	k1gInSc1
<g/>
)	)	kIx)
není	být	k5eNaImIp3nS
vize	vize	k1gFnSc1
na	na	k7c6
tomto	tento	k3xDgInSc6
plánu	plán	k1gInSc3
cokoliv	cokoliv	k3yInSc4
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
požádaly	požádat	k5eAaPmAgFnP
letecké	letecký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
vlastněné	vlastněný	k2eAgFnPc4d1
Smartwings	Smartwings	k1gInSc1
Group	Group	k1gInSc1
–	–	k?
Smartwings	Smartwings	k1gInSc1
a.s.	a.s.	k?
a	a	k8xC
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
u	u	k7c2
Městského	městský	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
o	o	k7c6
vyhlášení	vyhlášení	k1gNnSc6
moratoria	moratorium	k1gNnSc2
umožňujícího	umožňující	k2eAgNnSc2d1
odklad	odklad	k1gInSc4
splatnosti	splatnost	k1gFnSc2
dluhů	dluh	k1gInPc2
dle	dle	k7c2
tzv.	tzv.	kA
Lex	Lex	k1gFnPc2
covid	covida	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
Městský	městský	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
schválil	schválit	k5eAaPmAgInS
žádost	žádost	k1gFnSc4
Smartwings	Smartwingsa	k1gFnPc2
a	a	k8xC
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
a	a	k8xC
odsouhlasil	odsouhlasit	k5eAaPmAgMnS
mimořádné	mimořádný	k2eAgNnSc4d1
moratorium	moratorium	k1gNnSc4
před	před	k7c7
zahájením	zahájení	k1gNnSc7
insolvenčního	insolvenční	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
na	na	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
těchto	tento	k3xDgInPc2
3	#num#	k4
měsíců	měsíc	k1gInPc2
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
vyhlášen	vyhlásit	k5eAaPmNgInS
úpadek	úpadek	k1gInSc1
společností	společnost	k1gFnPc2
Smartwings	Smartwingsa	k1gFnPc2
a	a	k8xC
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
moratoriu	moratorium	k1gNnSc3
nebudou	být	k5eNaImBp3nP
vypláceny	vyplácet	k5eAaImNgFnP
refundace	refundace	k1gFnPc1
cestujícím	cestující	k1gMnSc7
za	za	k7c4
zrušené	zrušený	k2eAgInPc4d1
lety	let	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
2020	#num#	k4
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
další	další	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A319	A319	k1gFnSc2
(	(	kIx(
<g/>
OK-NEP	OK-NEP	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
zhoršování	zhoršování	k1gNnSc2
situace	situace	k1gFnSc2
s	s	k7c7
nemocí	nemoc	k1gFnSc7
covid-	covid-	k?
<g/>
19	#num#	k4
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
přijímaným	přijímaný	k2eAgNnSc7d1
opatřením	opatření	k1gNnSc7
vlády	vláda	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
září	září	k1gNnSc2
2020	#num#	k4
k	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
přerušení	přerušení	k1gNnSc3
linek	linka	k1gFnPc2
do	do	k7c2
řady	řada	k1gFnSc2
destinací	destinace	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
Amsterdam	Amsterdam	k1gInSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
a	a	k8xC
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
října	říjen	k1gInSc2
2020	#num#	k4
létaly	létat	k5eAaImAgFnP
ČSA	ČSA	kA
ještě	ještě	k9
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
Kodaně	Kodaň	k1gFnSc2
<g/>
,	,	kIx,
Stockholmu	Stockholm	k1gInSc2
<g/>
,	,	kIx,
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Milána	Milán	k1gInSc2
<g/>
,	,	kIx,
Kyjeva	Kyjev	k1gInSc2
<g/>
,	,	kIx,
Oděsy	Oděsa	k1gFnSc2
a	a	k8xC
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
datu	datum	k1gNnSc3
provozovaly	provozovat	k5eAaImAgInP
ČSA	ČSA	kA
jen	jen	k9
čtyři	čtyři	k4xCgNnPc1
letadla	letadlo	k1gNnPc1
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc1
ATR-	ATR-	k1gFnPc1
<g/>
72	#num#	k4
<g/>
:	:	kIx,
OK-NFU	OK-NFU	k1gFnSc6
a	a	k8xC
OK-NFV	OK-NFV	k1gFnSc6
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
319	#num#	k4
<g/>
:	:	kIx,
OK-REQ	OK-REQ	k1gFnSc6
a	a	k8xC
jeden	jeden	k4xCgInSc1
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
320	#num#	k4
<g/>
:	:	kIx,
OK-HEU	OK-HEU	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc4
linky	linka	k1gFnPc4
ČSA	ČSA	kA
operovala	operovat	k5eAaImAgFnS
svými	svůj	k3xOyFgInPc7
Boeingy	boeing	k1gInPc7
737	#num#	k4
společnost	společnost	k1gFnSc4
Smartwings	Smartwingsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
další	další	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A319	A319	k1gFnSc2
(	(	kIx(
<g/>
OK-NEM	OK-NEM	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
zbývající	zbývající	k2eAgFnPc1d1
A319	A319	k1gFnPc1
byly	být	k5eAaImAgFnP
uskladněné	uskladněný	k2eAgFnPc1d1
na	na	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
aktivně	aktivně	k6eAd1
létaly	létat	k5eAaImAgFnP
jen	jen	k9
A320	A320	k1gFnPc1
a	a	k8xC
dva	dva	k4xCgInPc4
stroje	stroj	k1gInPc4
ATR-72	ATR-72	k1gFnSc1
(	(	kIx(
<g/>
OK-NFV	OK-NFV	k1gFnSc1
a	a	k8xC
NFU	NFU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2020	#num#	k4
obsluhovala	obsluhovat	k5eAaImAgFnS
ČSA	ČSA	kA
již	již	k6eAd1
jen	jen	k9
pět	pět	k4xCc4
destinací	destinace	k1gFnPc2
<g/>
:	:	kIx,
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Kodaň	Kodaň	k1gFnSc1
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
a	a	k8xC
Kyjev	Kyjev	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
požádaly	požádat	k5eAaPmAgFnP
ČSA	ČSA	kA
i	i	k8xC
Smartwings	Smartwings	k1gInSc1
u	u	k7c2
Městského	městský	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
o	o	k7c4
prodloužení	prodloužení	k1gNnSc4
moratoria	moratorium	k1gNnSc2
o	o	k7c4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
přechodně	přechodně	k6eAd1
lepší	dobrý	k2eAgFnSc3d2
situaci	situace	k1gFnSc3
s	s	k7c7
pandemií	pandemie	k1gFnSc7
covidu-	covidu-	k?
<g/>
19	#num#	k4
začaly	začít	k5eAaPmAgFnP
ČSA	ČSA	kA
v	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
obnovovat	obnovovat	k5eAaImF
některé	některý	k3yIgFnPc4
linky	linka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
kvůli	kvůli	k7c3
zhoršující	zhoršující	k2eAgFnSc3d1
se	se	k3xPyFc4
situaci	situace	k1gFnSc3
na	na	k7c4
podzim	podzim	k1gInSc4
přerušeny	přerušen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
linky	linka	k1gFnPc4
do	do	k7c2
Amsterdamu	Amsterdam	k1gInSc2
<g/>
,	,	kIx,
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
Říma	Řím	k1gInSc2
a	a	k8xC
Oděsy	Oděsa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
opustil	opustit	k5eAaPmAgMnS
flotilu	flotila	k1gFnSc4
další	další	k2eAgInSc4d1
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
Airbus	airbus	k1gInSc4
A319	A319	k1gFnSc2
registrace	registrace	k1gFnSc1
OK-NEO	OK-NEO	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
z	z	k7c2
původních	původní	k2eAgNnPc2d1
9	#num#	k4
letadel	letadlo	k1gNnPc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
zbylo	zbýt	k5eAaPmAgNnS
poslední	poslední	k2eAgNnSc1d1
<g/>
,	,	kIx,
A319	A319	k1gFnSc1
registrace	registrace	k1gFnSc1
OK-REQ	OK-REQ	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Airbusem	airbus	k1gInSc7
A320	A320	k1gMnSc1
(	(	kIx(
<g/>
OK-HEU	OK-HEU	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Boeingem	boeing	k1gInSc7
737-800	737-800	k4
(	(	kIx(
<g/>
OK-TST	OK-TST	k1gFnSc1
v	v	k7c6
barvách	barva	k1gFnPc6
Smartwings	Smartwings	k1gInSc1
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
nyní	nyní	k6eAd1
tato	tento	k3xDgFnSc1
3	#num#	k4
letadla	letadlo	k1gNnSc2
jedinou	jediný	k2eAgFnSc4d1
tryskovou	tryskový	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
společnosti	společnost	k1gFnPc4
zbyla	zbýt	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
5	#num#	k4
ATR	ATR	kA
72	#num#	k4
létala	létat	k5eAaImAgFnS
aktivně	aktivně	k6eAd1
2	#num#	k4
<g/>
,	,	kIx,
registrací	registrace	k1gFnSc7
OK-NFU	OK-NFU	k1gFnSc2
a	a	k8xC
OK-NFV	OK-NFV	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2021	#num#	k4
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
pokračující	pokračující	k2eAgFnSc2d1
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
musela	muset	k5eAaImAgFnS
ČSA	ČSA	kA
opět	opět	k6eAd1
upravit	upravit	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
síť	síť	k1gFnSc4
linek	linka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
lednu	leden	k1gInSc6
2021	#num#	k4
zahrnovala	zahrnovat	k5eAaImAgFnS
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Amsterdam	Amsterdam	k1gInSc1
<g/>
,	,	kIx,
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1
a	a	k8xC
Moskvu	Moskva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
zažádal	zažádat	k5eAaPmAgMnS
vlastník	vlastník	k1gMnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
Smartwings	Smartwingsa	k1gFnPc2
<g/>
,	,	kIx,
českou	český	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
o	o	k7c4
nevratnou	vratný	k2eNgFnSc4d1
státní	státní	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomoc	pomoc	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
primárně	primárně	k6eAd1
poskytnuta	poskytnout	k5eAaPmNgFnS
ČSA	ČSA	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
dle	dle	k7c2
majitelů	majitel	k1gMnPc2
ohroženy	ohrozit	k5eAaPmNgFnP
insolvenčním	insolvenční	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
po	po	k7c6
konci	konec	k1gInSc6
mimořádného	mimořádný	k2eAgNnSc2d1
moratoria	moratorium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyprší	vypršet	k5eAaPmIp3nS
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
jejíchž	jejíž	k3xOyRp3gInPc2
účtů	účet	k1gInPc2
si	se	k3xPyFc3
v	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
2018	#num#	k4
a	a	k8xC
2019	#num#	k4
Smartwings	Smartwingsa	k1gFnPc2
půjčovaly	půjčovat	k5eAaImAgFnP
více	hodně	k6eAd2
než	než	k8xS
1,25	1,25	k4
miliardy	miliarda	k4xCgFnSc2
Kč	Kč	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
Smartwings	Smartwings	k1gInSc4
podaly	podat	k5eAaPmAgInP
za	za	k7c4
obě	dva	k4xCgFnPc4
společnosti	společnost	k1gFnPc4
žádost	žádost	k1gFnSc4
o	o	k7c4
tzv.	tzv.	kA
sedačkovné	sedačkovný	k2eAgNnSc1d1
za	za	k7c4
období	období	k1gNnSc4
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
do	do	k7c2
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2020	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
přerušen	přerušit	k5eAaPmNgInS
veškerý	veškerý	k3xTgInSc1
letecký	letecký	k2eAgInSc1d1
provoz	provoz	k1gInSc1
do	do	k7c2
<g/>
/	/	kIx~
<g/>
z	z	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
Covid-	Covid-	k1gFnSc1
<g/>
19	#num#	k4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
měly	mít	k5eAaImAgInP
Smartwings	Smartwings	k1gInSc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
uzemněna	uzemněn	k2eAgFnSc1d1
všechna	všechen	k3xTgNnPc4
letadla	letadlo	k1gNnSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
MAX	max	kA
a	a	k8xC
omezen	omezit	k5eAaPmNgInS
tak	tak	k9
provoz	provoz	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
vykonaly	vykonat	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
poslední	poslední	k2eAgInSc4d1
let	let	k1gInSc4
s	s	k7c7
cestujícími	cestující	k1gFnPc7
ATR	ATR	kA
72	#num#	k4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
kariéra	kariéra	k1gFnSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
po	po	k7c6
29	#num#	k4
letech	léto	k1gNnPc6
u	u	k7c2
ČSA	ČSA	kA
uzavřela	uzavřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
nahrazeny	nahrazen	k2eAgInPc4d1
Airbusy	airbus	k1gInPc4
A	a	k9
<g/>
220	#num#	k4
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
se	se	k3xPyFc4
zmenšila	zmenšit	k5eAaPmAgFnS
na	na	k7c4
3	#num#	k4
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
(	(	kIx(
<g/>
OK-REQ	OK-REQ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
(	(	kIx(
<g/>
OK-HEU	OK-HEU	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
jeden	jeden	k4xCgInSc1
Boeing	boeing	k1gInSc1
737-800	737-800	k4
(	(	kIx(
<g/>
OK-TST	OK-TST	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
oznámily	oznámit	k5eAaPmAgFnP
ČSA	ČSA	kA
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
důvodu	důvod	k1gInSc2
dluhů	dluh	k1gInPc2
a	a	k8xC
platební	platební	k2eAgFnSc2d1
neschopnosti	neschopnost	k1gFnSc2
a	a	k8xC
nemožnosti	nemožnost	k1gFnSc2
státní	státní	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
propustí	propust	k1gFnPc2
všech	všecek	k3xTgMnPc2
zbývajících	zbývající	k2eAgMnPc2d1
430	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
zpráva	zpráva	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
dementována	dementovat	k5eAaBmNgFnS
v	v	k7c6
insolvenčním	insolvenční	k2eAgInSc6d1
návrhu	návrh	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
ČSA	ČSA	kA
<g />
.	.	kIx.
</s>
<s hack="1">
nadále	nadále	k6eAd1
operovala	operovat	k5eAaImAgFnS
své	svůj	k3xOyFgInPc4
lety	let	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
situaci	situace	k1gFnSc4
v	v	k7c6
maximálně	maximálně	k6eAd1
úsporném	úsporný	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
na	na	k7c4
sebe	sebe	k3xPyFc4
ČSA	ČSA	kA
podala	podat	k5eAaPmAgFnS
insolvenční	insolvenční	k2eAgInSc4d1
návrh	návrh	k1gInSc4
spojený	spojený	k2eAgInSc4d1
s	s	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c4
povolení	povolení	k1gNnSc4
reogranizace	reogranizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
10	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
Městský	městský	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
úpadek	úpadek	k1gInSc4
a	a	k8xC
dosadil	dosadit	k5eAaPmAgMnS
do	do	k7c2
vedení	vedení	k1gNnSc2
firmy	firma	k1gFnSc2
insolvenčního	insolvenční	k2eAgMnSc2d1
správce	správce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
insolvenčního	insolvenční	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
přes	přes	k7c4
230	#num#	k4
tisíc	tisíc	k4xCgInPc2
věřitelů	věřitel	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
dluží	dlužit	k5eAaImIp3nP
přes	přes	k7c4
1,8	1,8	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hospodaření	hospodaření	k1gNnSc1
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1923	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
846	#num#	k4
<g/>
/	/	kIx~
<g/>
176	#num#	k4
<g/>
a	a	k8xC
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
160	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc4
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Petr	Petr	k1gMnSc1
Kudela	Kudel	k1gMnSc2
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
Obrat	obrat	k1gInSc1
</s>
<s>
8,5	8,5	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
8,5	8,5	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
93,4	93,4	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
255	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
12,5	12,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
287,2	287,2	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgNnPc1d1
aktiva	aktivum	k1gNnPc1
</s>
<s>
4,2	4,2	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
4	#num#	k4
mld.	mld.	k?
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
916,8	916,8	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
839,7	839,7	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
646	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Smartwings	Smartwings	k6eAd1
Majitelé	majitel	k1gMnPc1
</s>
<s>
Smartwings	Smartwings	k1gInSc1
(	(	kIx(
<g/>
97,7	97,7	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pojišťovna	pojišťovna	k1gFnSc1
(	(	kIx(
<g/>
2,3	2,3	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
Dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Holidays	Holidays	k1gInSc1
Czech	Czech	k1gInSc1
Airlines	Airlines	k1gInSc4
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInPc4d1
web	web	k1gInSc4
</s>
<s>
www.csa.cz	www.csa.cz	k1gMnSc1
IČO	IČO	kA
</s>
<s>
45795908	#num#	k4
LEI	lei	k1gInSc2
</s>
<s>
315700DWVMDKYOLEJM60	315700DWVMDKYOLEJM60	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
transformace	transformace	k1gFnSc2
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
v	v	k7c6
srpnu	srpen	k1gInSc6
1992	#num#	k4
ČSA	ČSA	kA
nikdy	nikdy	k6eAd1
nevyplácely	vyplácet	k5eNaImAgFnP
dividendy	dividenda	k1gFnPc4
<g/>
,	,	kIx,
po	po	k7c6
rekordní	rekordní	k2eAgFnSc6d1
ztrátě	ztráta	k1gFnSc6
3,8	3,8	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
musel	muset	k5eAaImAgInS
stát	stát	k1gInSc1
upsat	upsat	k5eAaPmF
nové	nový	k2eAgFnPc4d1
akcie	akcie	k1gFnPc4
za	za	k7c4
2,5	2,5	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Prodej	prodej	k1gInSc1
části	část	k1gFnSc2
společnosti	společnost	k1gFnSc2
francouzské	francouzský	k2eAgFnSc2d1
Air	Air	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
France	Franc	k1gMnPc4
skončil	skončit	k5eAaPmAgInS
neúspěchem	neúspěch	k1gInSc7
<g/>
,	,	kIx,
následně	následně	k6eAd1
se	se	k3xPyFc4
Antonínu	Antonín	k1gMnSc3
Jakubšemu	Jakubšem	k1gMnSc3
a	a	k8xC
Miroslavu	Miroslav	k1gMnSc3
Kůlovi	Kůla	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
společnost	společnost	k1gFnSc4
stabilizovat	stabilizovat	k5eAaBmF
a	a	k8xC
postupně	postupně	k6eAd1
navyšovat	navyšovat	k5eAaImF
přepravní	přepravní	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2003	#num#	k4
Miroslava	Miroslava	k1gFnSc1
Kůlu	kůl	k1gInSc2
nahradil	nahradit	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Tvrdík	Tvrdík	k1gMnSc1
<g/>
,	,	kIx,
dohodl	dohodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
odbory	odbor	k1gInPc7
na	na	k7c4
navýšení	navýšení	k1gNnSc4
mezd	mzda	k1gFnPc2
zhruba	zhruba	k6eAd1
o	o	k7c4
třetinu	třetina	k1gFnSc4
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
„	„	k?
<g/>
bezprecedentní	bezprecedentní	k2eAgInSc4d1
<g/>
“	“	k?
nárůst	nárůst	k1gInSc4
přepravní	přepravní	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
hospodaření	hospodaření	k1gNnSc2
společnosti	společnost	k1gFnSc2
prudce	prudko	k6eAd1
zhoršilo	zhoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
zisk	zisk	k1gInSc4
198	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
za	za	k7c4
prodej	prodej	k1gInSc4
dvou	dva	k4xCgInPc2
letounů	letoun	k1gInPc2
ATR	ATR	kA
skončilo	skončit	k5eAaPmAgNnS
hospodaření	hospodaření	k1gNnSc1
ČSA	ČSA	kA
za	za	k7c4
rok	rok	k1gInSc4
2005	#num#	k4
provozní	provozní	k2eAgFnSc7d1
ztrátou	ztráta	k1gFnSc7
téměř	téměř	k6eAd1
půl	půl	k1xP
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
a	a	k8xC
Paroubkova	Paroubkův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c4
nahrazení	nahrazení	k1gNnSc4
Jaroslava	Jaroslav	k1gMnSc2
Tvrdíka	Tvrdík	k1gMnSc2
Radomírem	Radomír	k1gMnSc7
Lašákem	Lašák	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
nadále	nadále	k6eAd1
generovala	generovat	k5eAaImAgFnS
provozní	provozní	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
,	,	kIx,
nepomohl	pomoct	k5eNaPmAgInS
ani	ani	k8xC
zisk	zisk	k1gInSc1
2,1	2,1	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
z	z	k7c2
prodeje	prodej	k1gInSc2
téměř	téměř	k6eAd1
veškerého	veškerý	k3xTgInSc2
nemovitého	movitý	k2eNgInSc2d1
majetku	majetek	k1gInSc2
a	a	k8xC
zisk	zisk	k1gInSc1
1,2	1,2	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
z	z	k7c2
prodeje	prodej	k1gInSc2
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
vykázaly	vykázat	k5eAaPmAgFnP
ČSA	ČSA	kA
za	za	k7c2
období	období	k1gNnSc2
2005	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
provozní	provozní	k2eAgFnSc4d1
ztrátu	ztráta	k1gFnSc4
3,4	3,4	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
zahrnutí	zahrnutí	k1gNnSc2
zisku	zisk	k1gInSc2
z	z	k7c2
prodeje	prodej	k1gInSc2
dlouhodobého	dlouhodobý	k2eAgInSc2d1
majetku	majetek	k1gInSc2
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
provozní	provozní	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
dvojnásobná	dvojnásobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrubá	Hrubá	k1gFnSc1
marže	marže	k1gFnSc1
(	(	kIx(
<g/>
přidaná	přidaný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
)	)	kIx)
nestačila	stačit	k5eNaBmAgFnS
ani	ani	k9
na	na	k7c4
pokrytí	pokrytí	k1gNnSc4
osobních	osobní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2015	#num#	k4
se	se	k3xPyFc4
společnosti	společnost	k1gFnPc4
podařilo	podařit	k5eAaPmAgNnS
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
době	doba	k1gFnSc6
dosáhnout	dosáhnout	k5eAaPmF
zisku	zisk	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
12,9	12,9	k4
milionu	milion	k4xCgInSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hospodaření	hospodaření	k1gNnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
a.	a.	k?
s.	s.	k?
v	v	k7c6
letech	let	k1gInPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
součtové	součtový	k2eAgInPc4d1
řádky	řádek	k1gInPc4
tučně	tučně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Miliardy	miliarda	k4xCgFnPc1
korun	koruna	k1gFnPc2
<g/>
20052006200720082009201020112012201320142005	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
</s>
<s>
Tržby	tržba	k1gFnPc1
</s>
<s>
21,524,024,023,220,416,914,813,710,29	21,524,024,023,220,416,914,813,710,29	k4
<g/>
,5178	,5178	k4
<g/>
,2	,2	k4
</s>
<s>
Výkonová	výkonový	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
18,3	18,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
18,6	18,6	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
18,7	18,7	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
18,7	18,7	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
18,1	18,1	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
14,3	14,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
12,8	12,8	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
12,1	12,1	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
10,0	10,0	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
8,8	8,8	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
150,4	150,4	k4
<g/>
)	)	kIx)
</s>
<s>
Hrubá	hrubý	k2eAgFnSc1d1
marže	marže	k1gFnSc1
</s>
<s>
3,25	3,25	k4
<g/>
,45	,45	k4
<g/>
,44	,44	k4
<g/>
,52	,52	k4
<g/>
,22	,22	k4
<g/>
,62	,62	k4
<g/>
,11	,11	k4
<g/>
,50	,50	k4
<g/>
,20	,20	k4
<g/>
,727	,727	k4
<g/>
,8	,8	k4
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
</s>
<s>
(	(	kIx(
<g/>
4,1	4,1	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
4,5	4,5	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
4,8	4,8	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
4,8	4,8	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
4,9	4,9	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
3,9	3,9	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
3,3	3,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
1,5	1,5	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
1,3	1,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
1,2	1,2	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
34,3	34,3	k4
<g/>
)	)	kIx)
</s>
<s>
Prodej	prodej	k1gInSc1
dl.	dl.	k?
majetku	majetek	k1gInSc2
</s>
<s>
0,20	0,20	k4
<g/>
,20	,20	k4
<g/>
,61	,61	k4
<g/>
,40	,40	k4
<g/>
,40	,40	k4
<g/>
,80	,80	k4
<g/>
,3	,3	k4
<g/>
(	(	kIx(
<g/>
0,2	0,2	k4
<g/>
)	)	kIx)
<g/>
0,10	0,10	k4
<g/>
,03	,03	k4
<g/>
,8	,8	k4
</s>
<s>
Rezervy	rezerva	k1gFnPc1
</s>
<s>
0,3	0,3	k4
<g/>
(	(	kIx(
<g/>
0,5	0,5	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,1	0,1	k4
<g/>
)	)	kIx)
<g/>
0,3	0,3	k4
<g/>
(	(	kIx(
<g/>
0,6	0,6	k4
<g/>
)	)	kIx)
<g/>
0,50	0,50	k4
<g/>
,11	,11	k4
<g/>
,10	,10	k4
<g/>
,2	,2	k4
<g/>
(	(	kIx(
<g/>
0,1	0,1	k4
<g/>
)	)	kIx)
<g/>
1,1	1,1	k4
</s>
<s>
Ostatní	ostatní	k2eAgInPc4d1
(	(	kIx(
<g/>
odpisy	odpis	k1gInPc1
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,8	0,8	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,7	0,7	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,6	0,6	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,7	0,7	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,3	0,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,4	0,4	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,1	0,1	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,1	0,1	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
3,7	3,7	k4
<g/>
)	)	kIx)
</s>
<s>
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
0,5	0,5	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,2	0,2	k4
<g/>
)	)	kIx)
<g/>
0,50	0,50	k4
<g/>
,7	,7	k4
<g/>
(	(	kIx(
<g/>
3,5	3,5	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,3	0,3	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
1,1	1,1	k4
<g/>
)	)	kIx)
<g/>
0,8	0,8	k4
<g/>
(	(	kIx(
<g/>
1,0	1,0	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
0,6	0,6	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
5,4	5,4	k4
<g/>
)	)	kIx)
</s>
<s>
Hospodaření	hospodaření	k1gNnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
a.s.	a.s.	k?
v	v	k7c6
letech	léto	k1gNnPc6
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
</s>
<s>
Parametr	parametr	k1gInSc1
<g/>
20152016201720182019	#num#	k4
</s>
<s>
tržby	tržba	k1gFnPc1
</s>
<s>
8,1	8,1	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
8	#num#	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
9,1	9,1	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
8,5	8,5	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
8,5	8,5	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
308,8	308,8	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
285,3	285,3	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
595,3	595,3	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
255	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
93,4	93,4	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
čistý	čistý	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
223,4	223,4	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
241,4	241,4	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
267,9	267,9	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
287,2	287,2	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
12,5	12,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
aktiva	aktivum	k1gNnPc1
</s>
<s>
2,8	2,8	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
3,3	3,3	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
4,2	4,2	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
4	#num#	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
4,2	4,2	k4
mld.	mld.	k?
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
413	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
652,1	652,1	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
839,7	839,7	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
916,8	916,8	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
počet	počet	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
628	#num#	k4
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
649	#num#	k4
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
679	#num#	k4
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
692	#num#	k4
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
646	#num#	k4
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
Tabulka	tabulka	k1gFnSc1
zobrazující	zobrazující	k2eAgFnSc1d1
počet	počet	k1gInSc4
přepravených	přepravený	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
počty	počet	k1gInPc1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
přepravený	přepravený	k2eAgInSc1d1
náklad	náklad	k1gInSc1
a	a	k8xC
počet	počet	k1gInSc1
letadel	letadlo	k1gNnPc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
v	v	k7c6
jednotlivých	jednotlivý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Přepravení	přepravený	k2eAgMnPc1d1
cestující	cestující	k1gMnPc1
</s>
<s>
Cestující	cestující	k1gMnPc1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
Přepravený	přepravený	k2eAgInSc1d1
</s>
<s>
náklad	náklad	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
tuny	tuna	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Náklad	náklad	k1gInSc1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
Flotila	flotila	k1gFnSc1
</s>
<s>
Flotila	flotila	k1gFnSc1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
1924	#num#	k4
(	(	kIx(
<g/>
pro	pro	k7c4
zajímavost	zajímavost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
426	#num#	k4
</s>
<s>
</s>
<s>
1,9	1,9	k4
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
1938	#num#	k4
(	(	kIx(
<g/>
pro	pro	k7c4
zajímavost	zajímavost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
33	#num#	k4
003	#num#	k4
</s>
<s>
</s>
<s>
603	#num#	k4
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
1994	#num#	k4
</s>
<s>
1	#num#	k4
239	#num#	k4
700	#num#	k4
</s>
<s>
</s>
<s>
9	#num#	k4
000	#num#	k4
</s>
<s>
</s>
<s>
3924	#num#	k4
</s>
<s>
</s>
<s>
</s>
<s>
1995	#num#	k4
</s>
<s>
1	#num#	k4
488	#num#	k4
300	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
10	#num#	k4
600	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
3916	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
1996	#num#	k4
</s>
<s>
1	#num#	k4
612	#num#	k4
100	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
10	#num#	k4
000	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
3976	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
1997	#num#	k4
</s>
<s>
1	#num#	k4
733	#num#	k4
700	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
10	#num#	k4
400	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
3912	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
1998	#num#	k4
</s>
<s>
1	#num#	k4
801	#num#	k4
800	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
11	#num#	k4
900	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
3795	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
1999	#num#	k4
</s>
<s>
2	#num#	k4
064	#num#	k4
100	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
13	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
3876	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2000	#num#	k4
</s>
<s>
2	#num#	k4
460	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
16	#num#	k4
600	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
3990	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2001	#num#	k4
</s>
<s>
2	#num#	k4
870	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
16	#num#	k4
200	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
4422	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2002	#num#	k4
</s>
<s>
3	#num#	k4
060	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+7	+7	k4
%	%	kIx~
</s>
<s>
17	#num#	k4
900	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
4455	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2003	#num#	k4
</s>
<s>
3	#num#	k4
590	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+17	+17	k4
%	%	kIx~
</s>
<s>
21	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
4543	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2004	#num#	k4
</s>
<s>
4	#num#	k4
340	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+21	+21	k4
%	%	kIx~
</s>
<s>
22	#num#	k4
700	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
4861	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2005	#num#	k4
</s>
<s>
5	#num#	k4
210	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+20	+20	k4
%	%	kIx~
</s>
<s>
22	#num#	k4
000	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
5303	#num#	k4
▲	▲	k?
</s>
<s>
</s>
<s>
</s>
<s>
2006	#num#	k4
</s>
<s>
5	#num#	k4
460	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+5	+5	k4
%	%	kIx~
</s>
<s>
23	#num#	k4
200	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
5442	#num#	k4
▲	▲	k?
</s>
<s>
50	#num#	k4
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
</s>
<s>
2007	#num#	k4
</s>
<s>
5	#num#	k4
490	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+0,5	+0,5	k4
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
4777	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
2008	#num#	k4
</s>
<s>
5	#num#	k4
620	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+2	+2	k4
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
4642	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
2009	#num#	k4
</s>
<s>
5	#num#	k4
464	#num#	k4
643	#num#	k4
</s>
<s>
▼	▼	k?
−	−	k?
<g/>
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
4172	#num#	k4
▼	▼	k?
</s>
<s>
51	#num#	k4
</s>
<s>
</s>
<s>
2010	#num#	k4
</s>
<s>
5	#num#	k4
061	#num#	k4
756	#num#	k4
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
3300	#num#	k4
▼	▼	k?
</s>
<s>
50	#num#	k4
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
▼	▼	k?
</s>
<s>
2011	#num#	k4
</s>
<s>
4	#num#	k4
200	#num#	k4
000	#num#	k4
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
1783	#num#	k4
▼	▼	k?
</s>
<s>
</s>
<s>
</s>
<s>
2012	#num#	k4
</s>
<s>
3	#num#	k4
306	#num#	k4
403	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
14	#num#	k4
184	#num#	k4
</s>
<s>
</s>
<s>
998	#num#	k4
▼	▼	k?
</s>
<s>
31	#num#	k4
</s>
<s>
</s>
<s>
2013	#num#	k4
</s>
<s>
2	#num#	k4
844	#num#	k4
586	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
11	#num#	k4
168	#num#	k4
</s>
<s>
</s>
<s>
928	#num#	k4
▼	▼	k?
</s>
<s>
26	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
2014	#num#	k4
</s>
<s>
2	#num#	k4
270	#num#	k4
000	#num#	k4
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
</s>
<s>
</s>
<s>
823	#num#	k4
▼	▼	k?
</s>
<s>
17	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
2015	#num#	k4
</s>
<s>
2	#num#	k4
008	#num#	k4
000	#num#	k4
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
10	#num#	k4
192	#num#	k4
</s>
<s>
</s>
<s>
628	#num#	k4
▼	▼	k?
</s>
<s>
17	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
2016	#num#	k4
</s>
<s>
2	#num#	k4
260	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+13	+13	k4
%	%	kIx~
</s>
<s>
8	#num#	k4
813	#num#	k4
</s>
<s>
▼	▼	k?
</s>
<s>
649	#num#	k4
▲	▲	k?
</s>
<s>
17	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
2017	#num#	k4
</s>
<s>
2	#num#	k4
678	#num#	k4
967	#num#	k4
</s>
<s>
▲	▲	k?
+18	+18	k4
%	%	kIx~
</s>
<s>
9	#num#	k4
228	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
679	#num#	k4
▲	▲	k?
</s>
<s>
18	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
2018	#num#	k4
</s>
<s>
2	#num#	k4
700	#num#	k4
000	#num#	k4
</s>
<s>
▲	▲	k?
+0	+0	k4
%	%	kIx~
</s>
<s>
692	#num#	k4
▲	▲	k?
</s>
<s>
20	#num#	k4
</s>
<s>
▲	▲	k?
</s>
<s>
Identita	identita	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Československé	československý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
konci	konec	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Československé	československý	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rozdělení	rozdělení	k1gNnSc6
podniku	podnik	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
společnost	společnost	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
název	název	k1gInSc4
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
používá	používat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
ČSA	ČSA	kA
odvinutá	odvinutý	k2eAgFnSc1d1
od	od	k7c2
názvu	název	k1gInSc2
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
do	do	k7c2
rozdělení	rozdělení	k1gNnSc2
podniku	podnik	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc4
si	se	k3xPyFc3
tuto	tento	k3xDgFnSc4
zkratku	zkratka	k1gFnSc4
ponechala	ponechat	k5eAaPmAgFnS
kvůli	kvůli	k7c3
zachování	zachování	k1gNnSc3
prestiže	prestiž	k1gFnSc2
a	a	k8xC
dobrému	dobrý	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
CSA	CSA	kA
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
kód	kód	k1gInSc4
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
ICAO	ICAO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
má	mít	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
současný	současný	k2eAgInSc4d1
název	název	k1gInSc4
a	a	k8xC
to	ten	k3xDgNnSc1
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
tvořil	tvořit	k5eAaImAgInS
firemní	firemní	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
ČSA	ČSA	kA
logotyp	logotyp	k1gMnSc1
Československé	československý	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
s	s	k7c7
moderně	moderně	k6eAd1
vyhlížejícím	vyhlížející	k2eAgInSc7d1
fontem	font	k1gInSc7
v	v	k7c6
červených	červený	k2eAgFnPc6d1
a	a	k8xC
modrých	modrý	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
československou	československý	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
změnou	změna	k1gFnSc7
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
vzniklo	vzniknout	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
logo	logo	k1gNnSc1
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
toho	ten	k3xDgNnSc2
původního	původní	k2eAgNnSc2d1
<g/>
,	,	kIx,
kombinující	kombinující	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
tří	tři	k4xCgInPc2
tupoúhlých	tupoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
–	–	k?
„	„	k?
<g/>
praporků	praporek	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
vzatých	vzatý	k2eAgNnPc2d1
ze	z	k7c2
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
existovaly	existovat	k5eAaImAgFnP
na	na	k7c6
levých	levý	k2eAgInPc6d1
horních	horní	k2eAgInPc6d1
okrajích	okraj	k1gInPc6
každé	každý	k3xTgFnSc2
skosené	skosený	k2eAgFnSc2d1
kapitálky	kapitálka	k1gFnSc2
původního	původní	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firemní	firemní	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
se	se	k3xPyFc4
odvíjely	odvíjet	k5eAaImAgFnP
z	z	k7c2
barev	barva	k1gFnPc2
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autory	autor	k1gMnPc7
tohoto	tento	k3xDgNnSc2
loga	logo	k1gNnSc2
jsou	být	k5eAaImIp3nP
Rostislav	Rostislav	k1gMnSc1
Vaněk	Vaněk	k1gMnSc1
a	a	k8xC
Radomír	Radomír	k1gMnSc1
Leszczynski	Leszczynsk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
na	na	k7c6
něm	on	k3xPp3gNnSc6
pracovali	pracovat	k5eAaImAgMnP
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
a	a	k8xC
práce	práce	k1gFnSc1
jim	on	k3xPp3gMnPc3
zabrala	zabrat	k5eAaPmAgFnS
14	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Autoři	autor	k1gMnPc1
původního	původní	k2eAgNnSc2d1
loga	logo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
Vaněk	Vaněk	k1gMnSc1
a	a	k8xC
Leszczynski	Leszczynsk	k1gMnPc1
se	se	k3xPyFc4
s	s	k7c7
ČSA	ČSA	kA
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
soudili	soudit	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
používají	používat	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnSc4
logo	logo	k1gNnSc4
protiprávně	protiprávně	k6eAd1
<g/>
,	,	kIx,
bez	bez	k7c2
smlouvy	smlouva	k1gFnSc2
o	o	k7c4
šíření	šíření	k1gNnSc4
(	(	kIx(
<g/>
licenční	licenční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
částka	částka	k1gFnSc1
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
autoři	autor	k1gMnPc1
soudí	soudit	k5eAaImIp3nP
<g/>
,	,	kIx,
vyčíslena	vyčíslit	k5eAaPmNgFnS
na	na	k7c4
53	#num#	k4
780	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
ČSA	ČSA	kA
odmítalo	odmítat	k5eAaImAgNnS
souvislost	souvislost	k1gFnSc4
výměny	výměna	k1gFnSc2
původního	původní	k2eAgNnSc2d1
loga	logo	k1gNnSc2
se	s	k7c7
soudní	soudní	k2eAgFnSc7d1
pří	pře	k1gFnSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Výzkumy	výzkum	k1gInPc1
provedené	provedený	k2eAgInPc1d1
v	v	k7c6
Česku	Česko	k1gNnSc6
i	i	k8xC
zahraničí	zahraničí	k1gNnSc6
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
dosavadní	dosavadní	k2eAgNnSc1d1
logo	logo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vnímáno	vnímat	k5eAaImNgNnS
jako	jako	k9
strnulé	strnulý	k2eAgNnSc4d1
<g/>
,	,	kIx,
nezajímavé	zajímavý	k2eNgNnSc4d1
a	a	k8xC
spojené	spojený	k2eAgNnSc4d1
s	s	k7c7
východoevropským	východoevropský	k2eAgInSc7d1
<g/>
,	,	kIx,
nižším	nízký	k2eAgInSc7d2
standardem	standard	k1gInSc7
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
poskytují	poskytovat	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
“	“	k?
tvrdil	tvrdit	k5eAaImAgMnS
viceprezident	viceprezident	k1gMnSc1
pro	pro	k7c4
marketing	marketing	k1gInSc4
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
Petr	Petr	k1gMnSc1
Pištělák	Pištělák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2007	#num#	k4
ČSA	ČSA	kA
uvedly	uvést	k5eAaPmAgInP
změnu	změna	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
loga	logo	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
pracovaly	pracovat	k5eAaImAgInP
předchozích	předchozí	k2eAgInPc2d1
18	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
nového	nový	k2eAgNnSc2d1
loga	logo	k1gNnSc2
je	být	k5eAaImIp3nS
grafik	grafik	k1gMnSc1
a	a	k8xC
zaměstnanec	zaměstnanec	k1gMnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Kotyza	Kotyz	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
na	na	k7c6
něm	on	k3xPp3gMnSc6
pracoval	pracovat	k5eAaImAgMnS
šest	šest	k4xCc1
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztmavil	ztmavit	k5eAaPmAgMnS
firemní	firemní	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
vyměnil	vyměnit	k5eAaPmAgMnS
písma	písmo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yIgNnPc4,k3yRgNnPc4
zvolil	zvolit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
místo	místo	k1gNnSc1
jednoho	jeden	k4xCgNnSc2
původního	původní	k2eAgNnSc2d1
<g/>
,	,	kIx,
pro	pro	k7c4
logotyp	logotyp	k1gInSc4
i	i	k9
pro	pro	k7c4
logo	logo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
mírně	mírně	k6eAd1
otočeného	otočený	k2eAgInSc2d1
sférického	sférický	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
trsátko	trsátko	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1
letadel	letadlo	k1gNnPc2
</s>
<s>
Prvnímu	první	k4xOgMnSc3
sjednocenému	sjednocený	k2eAgMnSc3d1
zbarvení	zbarvení	k1gNnSc1
letounů	letoun	k1gInPc2
Československých	československý	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
bylo	být	k5eAaImAgNnS
přezdíváno	přezdívat	k5eAaImNgNnS
„	„	k?
<g/>
vousy	vous	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nátěr	nátěr	k1gInSc1
byl	být	k5eAaImAgInS
charakteristický	charakteristický	k2eAgInSc1d1
šedým	šedý	k2eAgNnSc7d1
zbarvením	zbarvení	k1gNnSc7
dolní	dolní	k2eAgFnSc2d1
části	část	k1gFnSc2
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
od	od	k7c2
bílého	bílý	k2eAgInSc2d1
hořejšku	hořejšek	k1gInSc2
odděloval	oddělovat	k5eAaImAgInS
modrý	modrý	k2eAgInSc1d1
<g/>
,	,	kIx,
pruh	pruh	k1gInSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
se	se	k3xPyFc4
na	na	k7c6
přídi	příď	k1gFnSc6
letounu	letoun	k1gInSc2
rozbíhal	rozbíhat	k5eAaImAgInS
a	a	k8xC
symbolizoval	symbolizovat	k5eAaImAgInS
tak	tak	k9
křídla	křídlo	k1gNnSc2
ptáka	pták	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
spolu	spolu	k6eAd1
se	s	k7c7
zahájením	zahájení	k1gNnSc7
provozu	provoz	k1gInSc2
prvního	první	k4xOgInSc2
proudového	proudový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Tupolev	Tupolev	k1gMnSc1
Tu-	Tu-	k1gMnSc1
<g/>
104	#num#	k4
pruh	pruh	k1gInSc1
změnil	změnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
barvu	barva	k1gFnSc4
na	na	k7c4
červenou	červená	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Dodáním	dodání	k1gNnSc7
nového	nový	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc4
<g/>
62	#num#	k4
imatrikulace	imatrikulace	k1gFnSc1
OK-YBA	OK-YBA	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
změnily	změnit	k5eAaPmAgInP
ČSA	ČSA	kA
své	svůj	k3xOyFgNnSc4
zbarvení	zbarvení	k1gNnSc4
flotily	flotila	k1gFnSc2
na	na	k7c4
standardní	standardní	k2eAgInSc4d1
pruh	pruh	k1gInSc4
přes	přes	k7c4
okna	okno	k1gNnPc4
a	a	k8xC
červenou	červený	k2eAgFnSc4d1
směrovku	směrovka	k1gFnSc4
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
OK	oka	k1gFnPc2
JET	jet	k5eAaImF
<g/>
“	“	k?
a	a	k8xC
českou	český	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
písmena	písmeno	k1gNnPc4
„	„	k?
<g/>
JET	jet	k2eAgInSc4d1
<g/>
“	“	k?
za	za	k7c4
„	„	k?
<g/>
OK	oko	k1gNnPc2
<g/>
“	“	k?
změněny	změnit	k5eAaPmNgInP
na	na	k7c4
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1990	#num#	k4
přilétl	přilétnout	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
po	po	k7c6
generální	generální	k2eAgFnSc6d1
opravě	oprava	k1gFnSc6
v	v	k7c6
Taškentu	Taškent	k1gInSc6
první	první	k4xOgInSc1
stroj	stroj	k1gInSc1
v	v	k7c6
novém	nový	k2eAgNnSc6d1
bílém	bílé	k1gNnSc6
nátěru	nátěr	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
trupu	trup	k1gInSc6
bylo	být	k5eAaImAgNnS
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
ČSA	ČSA	kA
a	a	k8xC
na	na	k7c6
nově	nově	k6eAd1
bílé	bílý	k2eAgFnSc3d1
směrovce	směrovka	k1gFnSc3
zůstal	zůstat	k5eAaPmAgInS
pouze	pouze	k6eAd1
nápis	nápis	k1gInSc1
„	„	k?
<g/>
OK	oka	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
prošlo	projít	k5eAaPmAgNnS
zbarvení	zbarvení	k1gNnSc1
letounů	letoun	k1gMnPc2
ČSA	ČSA	kA
změnou	změna	k1gFnSc7
<g/>
,	,	kIx,
směrovka	směrovka	k1gFnSc1
byla	být	k5eAaImAgFnS
zpátky	zpátky	k6eAd1
zbarvena	zbarvit	k5eAaPmNgFnS
<g/>
,	,	kIx,
do	do	k7c2
bílé	bílý	k2eAgFnSc2d1
<g/>
,	,	kIx,
červené	červený	k2eAgFnSc2d1
a	a	k8xC
modré	modrý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
změnou	změna	k1gFnSc7
na	na	k7c4
akciovou	akciový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
vzniklo	vzniknout	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
logo	logo	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
odrazilo	odrazit	k5eAaPmAgNnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1992	#num#	k4
i	i	k8xC
v	v	k7c6
nátěru	nátěr	k1gInSc6
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
trupu	trup	k1gInSc6
zůstal	zůstat	k5eAaPmAgInS
nápis	nápis	k1gInSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
„	„	k?
<g/>
OK	oko	k1gNnPc2
<g/>
“	“	k?
na	na	k7c6
směrovce	směrovka	k1gFnSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
tzv.	tzv.	kA
hokejkové	hokejkový	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
s	s	k7c7
třemi	tři	k4xCgInPc7
tupouhelníky	tupouhelník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zbarvení	zbarvení	k1gNnSc1
se	se	k3xPyFc4
jako	jako	k9
první	první	k4xOgMnSc1
objevilo	objevit	k5eAaPmAgNnS
na	na	k7c6
nově	nova	k1gFnSc6
dodaných	dodaný	k2eAgFnPc2d1
ATR-72	ATR-72	k1gFnPc2
a	a	k8xC
Boeingách	Boeingách	k?
737	#num#	k4
<g/>
-	-	kIx~
<g/>
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
jej	on	k3xPp3gMnSc4
dostaly	dostat	k5eAaPmAgInP
i	i	k8xC
oba	dva	k4xCgInPc1
Airbusy	airbus	k1gInPc1
A310-300	A310-300	k1gFnPc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
pár	pár	k4xCyI
starších	starý	k2eAgNnPc2d2
letadel	letadlo	k1gNnPc2
Tupolev	Tupolev	k1gFnSc1
Tu-	Tu-	k1gFnSc1
<g/>
154	#num#	k4
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
uvedením	uvedení	k1gNnSc7
nového	nový	k2eAgNnSc2d1
loga	logo	k1gNnSc2
změnil	změnit	k5eAaPmAgInS
nátěr	nátěr	k1gInSc1
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc1
se	se	k3xPyFc4
nátěr	nátěr	k1gInSc1
objevil	objevit	k5eAaPmAgInS
na	na	k7c6
Boeingu	boeing	k1gInSc6
737-55S	737-55S	k4
imatrikulace	imatrikulace	k1gFnSc1
OK-DGL	OK-DGL	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Vepředu	vepředu	k6eAd1
na	na	k7c6
trupu	trup	k1gInSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
je	být	k5eAaImIp3nS
napsáno	napsán	k2eAgNnSc4d1
ČSA	ČSA	kA
<g/>
,	,	kIx,
motory	motor	k1gInPc1
jsou	být	k5eAaImIp3nP
zabarvené	zabarvený	k2eAgInPc1d1
do	do	k7c2
červené	červený	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
nad	nad	k7c7
nimi	on	k3xPp3gInPc7
je	být	k5eAaImIp3nS
nad	nad	k7c7
okénky	okénko	k1gNnPc7
modrý	modrý	k2eAgInSc1d1
anglický	anglický	k2eAgInSc1d1
modrý	modrý	k2eAgInSc1d1
nápis	nápis	k1gInSc1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
okénky	okénko	k1gNnPc7
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
trupu	trup	k1gInSc2
<g/>
,	,	kIx,
před	před	k7c7
zadními	zadní	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
webová	webový	k2eAgFnSc1d1
adresa	adresa	k1gFnSc1
www.czechairlines.com	www.czechairlines.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
směrovce	směrovka	k1gFnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
červeném	červený	k2eAgInSc6d1
trojúhelníku	trojúhelník	k1gInSc6
tzv.	tzv.	kA
diamant	diamant	k1gInSc1
<g/>
,	,	kIx,
přezdívaný	přezdívaný	k2eAgMnSc1d1
„	„	k?
<g/>
trsátko	trsátko	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Spodek	spodek	k1gInSc1
letadla	letadlo	k1gNnSc2
je	být	k5eAaImIp3nS
standardně	standardně	k6eAd1
z	z	k7c2
části	část	k1gFnSc2
šedý	šedý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
mělo	mít	k5eAaImAgNnS
nový	nový	k2eAgInSc4d1
nátěr	nátěr	k1gInSc4
16	#num#	k4
z	z	k7c2
50	#num#	k4
letounů	letoun	k1gInPc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nosilo	nosit	k5eAaImAgNnS
staré	starý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Airbus	airbus	k1gInSc1
A319	A319	k1gFnSc2
registrace	registrace	k1gFnSc2
OK-MEL	OK-MEL	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
dočkalo	dočkat	k5eAaPmAgNnS
přelakování	přelakování	k1gNnSc1
na	na	k7c6
konci	konec	k1gInSc6
února	únor	k1gInSc2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
dodaném	dodaný	k2eAgInSc6d1
Airbusu	airbus	k1gInSc6
A	A	kA
<g/>
320	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
OK-HEU	OK-HEU	k1gFnSc1
<g/>
,	,	kIx,
objevily	objevit	k5eAaPmAgFnP
modifikované	modifikovaný	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
je	být	k5eAaImIp3nS
spodek	spodek	k1gInSc1
trupu	trup	k1gInSc2
v	v	k7c6
bílé	bílý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
s	s	k7c7
červeným	červený	k2eAgInSc7d1
nápisem	nápis	k1gInSc7
flyOK	flyOK	k?
<g/>
,	,	kIx,
winglety	wingleta	k1gFnPc1
jsou	být	k5eAaImIp3nP
ponechány	ponechat	k5eAaPmNgFnP
v	v	k7c6
šedé	šedý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
a	a	k8xC
logo	logo	k1gNnSc4
na	na	k7c6
ocase	ocas	k1gInSc6
se	se	k3xPyFc4
zvětšilo	zvětšit	k5eAaPmAgNnS
<g/>
,	,	kIx,
zmizelo	zmizet	k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc1
šedé	šedý	k2eAgNnSc1d1
lemování	lemování	k1gNnSc1
a	a	k8xC
pokrývá	pokrývat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
směrovky	směrovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Webovou	webový	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
nad	nad	k7c7
okénky	okénko	k1gNnPc7
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
trupu	trup	k1gInSc2
nahradil	nahradit	k5eAaPmAgInS
šedý	šedý	k2eAgInSc1d1
nápis	nápis	k1gInSc1
Smartwings	Smartwingsa	k1gFnPc2
Group	Group	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdali	zdali	k8xS
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
zbarvení	zbarvení	k1gNnSc1
objeví	objevit	k5eAaPmIp3nS
i	i	k9
na	na	k7c6
dalších	další	k2eAgNnPc6d1
letadlech	letadlo	k1gNnPc6
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
nejasné	jasný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
</s>
<s>
1957	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
</s>
<s>
Nátěr	nátěr	k1gInSc1
oslavující	oslavující	k2eAgInSc1d1
80	#num#	k4
let	léto	k1gNnPc2
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc4
737-500	737-500	k4
OK-DGL	OK-DGL	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Červený	červený	k2eAgInSc1d1
retrojet	retrojet	k1gInSc1
vycházející	vycházející	k2eAgInSc1d1
z	z	k7c2
barev	barva	k1gFnPc2
ČSA	ČSA	kA
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc4
737-500	737-500	k4
OK-XGC	OK-XGC	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modrý	modrý	k2eAgInSc1d1
retrojet	retrojet	k1gInSc1
vycházející	vycházející	k2eAgInSc1d1
z	z	k7c2
barev	barva	k1gFnPc2
ČSA	ČSA	kA
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc4
737-500	737-500	k4
OK-XGB	OK-XGB	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barvy	barva	k1gFnPc1
SkyTeam	SkyTeam	k1gInSc1
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc1
737-500	737-500	k4
OK-XGE	OK-XGE	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Motiv	motiv	k1gInSc1
propagující	propagující	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Prague	Prague	k1gInSc1
loves	loves	k1gInSc1
you	you	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
Airbus	airbus	k1gInSc1
A320	A320	k1gMnSc2
OK-HCA	OK-HCA	k1gMnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nátěr	nátěr	k1gInSc1
oslavující	oslavující	k2eAgInSc1d1
90	#num#	k4
let	léto	k1gNnPc2
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
většině	většina	k1gFnSc6
letadel	letadlo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Barvy	barva	k1gFnPc1
SkyTeam	SkyTeam	k1gInSc1
(	(	kIx(
<g/>
Airbus	airbus	k1gInSc1
A319	A319	k1gMnSc2
OK-PET	OK-PET	k1gMnSc2
<g/>
,	,	kIx,
OER	OER	kA
<g/>
,	,	kIx,
při	při	k7c6
pronájmu	pronájem	k1gInSc6
u	u	k7c2
Saudie	Saudie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Barvy	barva	k1gFnPc1
SkyTeam	SkyTeam	k1gInSc1
(	(	kIx(
<g/>
ATR	ATR	kA
42	#num#	k4
OK-JFL	OK-JFL	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Motiv	motiv	k1gInSc1
propagující	propagující	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Fly	Fly	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
city	cit	k1gInPc1
of	of	k?
Magic	Magice	k1gInPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Airbus	airbus	k1gInSc1
A319	A319	k1gMnSc2
OK-NEP	OK-NEP	k1gMnSc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Uniforma	uniforma	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
uniforma	uniforma	k1gFnSc1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
stevardů	stevard	k1gMnPc2
a	a	k8xC
stevardek	stevardka	k1gFnPc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
designéry	designér	k1gMnPc7
z	z	k7c2
české	český	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
OP	op	k1gMnSc1
Prostějov	Prostějov	k1gInSc1
Profashion	Profashion	k1gInSc1
při	při	k7c6
změně	změna	k1gFnSc6
loga	logo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převládají	převládat	k5eAaImIp3nP
na	na	k7c6
nich	on	k3xPp3gMnPc6
základní	základní	k2eAgFnSc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
tmavomodrá	tmavomodrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
červená	červený	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
i	i	k9
černá	černý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
detailech	detail	k1gInPc6
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
na	na	k7c6
šátku	šátek	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
knoflících	knoflík	k1gInPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
symbol	symbol	k1gInSc1
značky	značka	k1gFnSc2
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
se	se	k3xPyFc4
někdy	někdy	k6eAd1
přezdívá	přezdívat	k5eAaImIp3nS
„	„	k?
<g/>
trsátko	trsátko	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
uniformní	uniformní	k2eAgFnSc2d1
kázně	kázeň	k1gFnSc2
jsou	být	k5eAaImIp3nP
nejrůznější	různý	k2eAgInPc4d3
doplňky	doplněk	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
právě	právě	k9
šátek	šátek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
uvázaný	uvázaný	k2eAgMnSc1d1
vždy	vždy	k6eAd1
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Časopisy	časopis	k1gInPc1
</s>
<s>
Každé	každý	k3xTgInPc1
tři	tři	k4xCgInPc1
měsíce	měsíc	k1gInPc1
vydávají	vydávat	k5eAaImIp3nP,k5eAaPmIp3nP
ČSA	ČSA	kA
časopis	časopis	k1gInSc1
flyOK	flyOK	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
v	v	k7c6
něm	on	k3xPp3gInSc6
uvedeny	uvést	k5eAaPmNgFnP
destinace	destinace	k1gFnPc1
<g/>
,	,	kIx,
tipy	tip	k1gInPc1
<g/>
,	,	kIx,
uživatelské	uživatelský	k2eAgInPc1d1
informace	informace	k1gFnPc4
a	a	k8xC
novinky	novinka	k1gFnPc4
z	z	k7c2
prostředí	prostředí	k1gNnSc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Předchozí	předchozí	k2eAgInSc1d1
palubní	palubní	k2eAgInSc1d1
časopis	časopis	k1gInSc1
vydávaný	vydávaný	k2eAgInSc1d1
do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Review	Review	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
ČSA	ČSA	kA
v	v	k7c6
minulosti	minulost	k1gFnSc6
vydávalo	vydávat	k5eAaPmAgNnS,k5eAaImAgNnS
také	také	k9
časopis	časopis	k1gInSc1
Runway	runway	k1gInSc1
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Servis	servis	k1gInSc1
</s>
<s>
Občerstvení	občerstvení	k1gNnSc1
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
společnost	společnost	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
na	na	k7c6
palubách	paluba	k1gFnPc6
svých	svůj	k3xOyFgInPc2
letounů	letoun	k1gInPc2
podávat	podávat	k5eAaImF
občerstvení	občerstvení	k1gNnSc4
zdarma	zdarma	k6eAd1
(	(	kIx(
<g/>
kromě	kromě	k7c2
letů	let	k1gInPc2
do	do	k7c2
Almat	Almat	k2eAgInSc4d1
a	a	k8xC
Soulu	Soul	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
přidaly	přidat	k5eAaPmAgFnP
k	k	k7c3
spoustám	spousta	k1gFnPc3
společností	společnost	k1gFnPc2
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
odůvodnily	odůvodnit	k5eAaPmAgInP
své	svůj	k3xOyFgInPc1
kroky	krok	k1gInPc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
lidem	člověk	k1gMnPc3
nechtějí	chtít	k5eNaImIp3nP
vnucovat	vnucovat	k5eAaImF
bezplatné	bezplatný	k2eAgNnSc4d1
neatraktivní	atraktivní	k2eNgNnSc4d1
občerstvení	občerstvení	k1gNnSc4
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
zavedly	zavést	k5eAaPmAgFnP
novou	nový	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
placeného	placený	k2eAgNnSc2d1
občerstvení	občerstvení	k1gNnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
kvalitnější	kvalitní	k2eAgNnSc1d2
a	a	k8xC
má	mít	k5eAaImIp3nS
větší	veliký	k2eAgFnSc4d2
nabídku	nabídka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zavazadla	zavazadlo	k1gNnPc1
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgInS
zvýšen	zvýšit	k5eAaPmNgInS
váhový	váhový	k2eAgInSc1d1
limit	limit	k1gInSc1
na	na	k7c4
zapsaná	zapsaný	k2eAgNnPc4d1
(	(	kIx(
<g/>
odbavovaná	odbavovaný	k2eAgNnPc4d1
<g/>
)	)	kIx)
zavazadla	zavazadlo	k1gNnPc4
–	–	k?
místo	místo	k7c2
15	#num#	k4
kilogramů	kilogram	k1gInPc2
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
brát	brát	k5eAaImF
až	až	k9
23	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
v	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
obchodní	obchodní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
si	se	k3xPyFc3
každý	každý	k3xTgMnSc1
cestující	cestující	k1gMnSc1
může	moct	k5eAaImIp3nS
vzít	vzít	k5eAaPmF
na	na	k7c4
palubu	paluba	k1gFnSc4
až	až	k9
dvě	dva	k4xCgFnPc4
32	#num#	k4
kilová	kilový	k2eAgNnPc4d1
zavazadla	zavazadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestující	cestující	k1gFnPc4
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
brát	brát	k5eAaImF
do	do	k7c2
ekonomické	ekonomický	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
příruční	příruční	k2eAgNnSc4d1
zavazadlo	zavazadlo	k1gNnSc4
do	do	k7c2
váhy	váha	k1gFnSc2
8	#num#	k4
kg	kg	kA
o	o	k7c6
maximální	maximální	k2eAgFnSc6d1
délce	délka	k1gFnSc6
55	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
šířce	šířka	k1gFnSc6
45	#num#	k4
cm	cm	kA
a	a	k8xC
hloubce	hloubka	k1gFnSc6
25	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestující	cestující	k1gFnSc1
obchodní	obchodní	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
na	na	k7c4
palubu	paluba	k1gFnSc4
brát	brát	k5eAaImF
až	až	k9
2	#num#	k4
příruční	příruční	k2eAgFnSc4d1
zavazadla	zavazadlo	k1gNnPc1
s	s	k7c7
hmotností	hmotnost	k1gFnSc7
až	až	k9
8	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Cestovní	cestovní	k2eAgFnPc1d1
třídy	třída	k1gFnPc1
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
třída	třída	k1gFnSc1
(	(	kIx(
<g/>
Economy	Econom	k1gInPc1
class	classa	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejčastější	častý	k2eAgFnSc1d3
a	a	k8xC
nejlevnější	levný	k2eAgFnSc1d3
třída	třída	k1gFnSc1
na	na	k7c6
palubě	paluba	k1gFnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
měli	mít	k5eAaImAgMnP
cestující	cestující	k1gMnPc1
ekonomické	ekonomický	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
osobní	osobní	k2eAgFnSc2d1
zábavní	zábavní	k2eAgFnSc2d1
systém	systém	k1gInSc4
s	s	k7c7
interaktivní	interaktivní	k2eAgFnSc7d1
obrazovkou	obrazovka	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Airbus	airbus	k1gInSc4
A	a	k9
<g/>
330	#num#	k4
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1
třída	třída	k1gFnSc1
(	(	kIx(
<g/>
Business	business	k1gInSc1
class	class	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dražší	drahý	k2eAgFnSc1d2
třída	třída	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
s	s	k7c7
rozšířenými	rozšířený	k2eAgFnPc7d1
službami	služba	k1gFnPc7
<g/>
,	,	kIx,
poskytuje	poskytovat	k5eAaImIp3nS
přístup	přístup	k1gInSc1
do	do	k7c2
letištních	letištní	k2eAgInPc2d1
salónků	salónek	k1gInPc2
a	a	k8xC
rychlejší	rychlý	k2eAgNnSc1d2
odbavení	odbavení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
cestující	cestující	k1gMnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
letecké	letecký	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc1
balíček	balíček	k1gInSc1
různých	různý	k2eAgFnPc2d1
výhod	výhoda	k1gFnPc2
nazvaný	nazvaný	k2eAgInSc4d1
SkyPriority	SkyPriorit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
letadlech	letadlo	k1gNnPc6
výrobce	výrobce	k1gMnPc4
ATR	ATR	kA
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
poskytují	poskytovat	k5eAaImIp3nP
upravená	upravený	k2eAgNnPc1d1
sedadla	sedadlo	k1gNnPc1
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
trupu	trup	k1gInSc2
(	(	kIx(
<g/>
u	u	k7c2
hlavního	hlavní	k2eAgInSc2d1
východu	východ	k1gInSc2
pro	pro	k7c4
rychlý	rychlý	k2eAgInSc4d1
výstup	výstup	k1gInSc4
a	a	k8xC
nástup	nástup	k1gInSc4
<g/>
)	)	kIx)
v	v	k7c6
uskupení	uskupení	k1gNnSc6
místo	místo	k7c2
2-2	2-2	k4
je	být	k5eAaImIp3nS
pro	pro	k7c4
cestující	cestující	k1gMnPc4
v	v	k7c4
business	business	k1gInSc4
class	class	k6eAd1
v	v	k7c6
uskupení	uskupení	k1gNnSc6
1-1	1-1	k4
a	a	k8xC
odděleny	oddělen	k2eAgFnPc1d1
záclonkou	záclonka	k1gFnSc7
od	od	k7c2
economy	econom	k1gInPc4
class	classa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
Airbusů	airbus	k1gInPc2
A319	A319	k1gMnPc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
prostředním	prostřední	k2eAgNnSc6d1
sedadle	sedadlo	k1gNnSc6
stolek	stolek	k1gInSc1
(	(	kIx(
<g/>
sedí	sedit	k5eAaImIp3nS
se	se	k3xPyFc4
2-2	2-2	k4
místo	místo	k7c2
3	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
několika	několik	k4yIc6
letadlech	letadlo	k1gNnPc6
A319	A319	k1gFnSc2
na	na	k7c4
delší	dlouhý	k2eAgFnPc4d2
trasy	trasa	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
OK-OER	OK-OER	k1gMnSc1
<g/>
,	,	kIx,
OK-PET	OK-PET	k1gMnSc1
<g/>
)	)	kIx)
měly	mít	k5eAaImAgFnP
ČSA	ČSA	kA
v	v	k7c6
obchodní	obchodní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
speciální	speciální	k2eAgNnPc4d1
větší	veliký	k2eAgNnPc4d2
sedadla	sedadlo	k1gNnPc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Letoun	letoun	k1gInSc1
Airbus	airbus	k1gInSc1
A330-300	A330-300	k1gFnSc2
měl	mít	k5eAaImAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
ČSA	ČSA	kA
v	v	k7c6
obchodní	obchodní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
sedadla	sedadlo	k1gNnSc2
s	s	k7c7
lůžkovou	lůžkový	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
designu	design	k1gInSc6
společnosti	společnost	k1gFnSc2
Korean	Korean	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
a	a	k8xC
2006	#num#	k4
obdržely	obdržet	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
na	na	k7c6
základě	základ	k1gInSc6
hodnocení	hodnocení	k1gNnSc2
odborné	odborný	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
ocenění	ocenění	k1gNnSc2
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
a	a	k8xC
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byly	být	k5eAaImAgInP
ČSA	ČSA	kA
společností	společnost	k1gFnPc2
Skytrax	Skytrax	k1gInSc1
ohodnoceny	ohodnocen	k2eAgInPc1d1
jako	jako	k8xC,k8xS
tříhvězdičková	tříhvězdičkový	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2019	#num#	k4
byly	být	k5eAaImAgInP
Czech	Czech	k1gInSc4
Airlines	Airlinesa	k1gFnPc2
na	na	k7c6
serveru	server	k1gInSc6
TripAdvisor	TripAdvisora	k1gFnPc2
hodnoceny	hodnocen	k2eAgInPc1d1
třemi	tři	k4xCgFnPc7
hvězdičkami	hvězdička	k1gFnPc7
z	z	k7c2
pěti	pět	k4xCc2
podle	podle	k7c2
zhruba	zhruba	k6eAd1
dvou	dva	k4xCgInPc2
tisíc	tisíc	k4xCgInPc2
recenzí	recenze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
žebříčku	žebříček	k1gInSc2
serveru	server	k1gInSc2
AirHelp	AirHelp	k1gInSc1
se	se	k3xPyFc4
Czech	Czecha	k1gFnPc2
Airlines	Airlines	k1gInSc1
umístily	umístit	k5eAaPmAgInP
na	na	k7c6
59	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
ze	z	k7c2
72	#num#	k4
hodnocených	hodnocený	k2eAgFnPc2d1
aerolinek	aerolinka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bývalé	bývalý	k2eAgFnPc1d1
sesterské	sesterský	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
</s>
<s>
Firma	firma	k1gFnSc1
Air	Air	k1gFnSc2
Cargo	Cargo	k6eAd1
Terminal	Terminal	k1gFnSc1
a.s.	a.s.	k?
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
2007	#num#	k4
Českými	český	k2eAgFnPc7d1
aeroliniemi	aerolinie	k1gFnPc7
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
připravily	připravit	k5eAaPmAgInP
k	k	k7c3
odprodeji	odprodej	k1gInSc3
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Stala	stát	k5eAaPmAgFnS
se	s	k7c7
provozovatelem	provozovatel	k1gMnSc7
nákladního	nákladní	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
na	na	k7c6
Letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
zajišťuje	zajišťovat	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
zacházení	zacházení	k1gNnSc1
s	s	k7c7
nákladem	náklad	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
odkoupena	odkoupit	k5eAaPmNgFnS
firmou	firma	k1gFnSc7
Central	Central	k1gFnSc2
European	European	k1gInSc1
Handling	Handling	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Firma	firma	k1gFnSc1
Air	Air	k1gFnSc2
Czech	Czecha	k1gFnPc2
Catering	Catering	k1gInSc1
a.s.	a.s.	k?
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
2007	#num#	k4
Českými	český	k2eAgFnPc7d1
aeroliniemi	aerolinie	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
převzala	převzít	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
jejich	jejich	k3xOp3gFnPc4
aktivity	aktivita	k1gFnPc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
stravovacích	stravovací	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
(	(	kIx(
<g/>
cateringu	catering	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
připravily	připravit	k5eAaPmAgInP
k	k	k7c3
odprodeji	odprodej	k1gInSc3
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byl	být	k5eAaImAgInS
podnik	podnik	k1gInSc1
odkoupen	odkoupen	k2eAgInSc1d1
firmou	firma	k1gFnSc7
Alpha	Alpha	k1gFnSc1
Overseas	Overseas	k1gInSc4
Holdings	Holdings	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
za	za	k7c4
682	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
Budova	budova	k1gFnSc1
Českého	český	k2eAgInSc2d1
aeroholdingu	aeroholding	k1gInSc2
na	na	k7c6
Letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c4
PrazeClickforSky	PrazeClickforSka	k1gFnPc4
a.s.	a.s.	k?
byla	být	k5eAaImAgFnS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ČSA	ČSA	kA
provozující	provozující	k2eAgInSc1d1
bývalý	bývalý	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
systém	systém	k1gInSc1
Click	Click	k1gInSc1
<g/>
4	#num#	k4
<g/>
sky	sky	k?
prodeje	prodej	k1gInSc2
letenek	letenka	k1gFnPc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
konkurovat	konkurovat	k5eAaImF
nízkonákladovým	nízkonákladový	k2eAgFnPc3d1
leteckým	letecký	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
se	se	k3xPyFc4
společnost	společnost	k1gFnSc1
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
Czech	Czech	k1gInSc4
Charters	Chartersa	k1gFnPc2
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc4
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
provozu	provoz	k1gInSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
2009	#num#	k4
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Czech	Czech	k1gInSc1
Airlines	Airlines	k1gInSc1
Handling	Handling	k1gInSc1
a.s.	a.s.	k?
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
ČSA	ČSA	kA
Support	support	k1gInSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
sesterská	sesterský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
byla	být	k5eAaImAgFnS
prodána	prodat	k5eAaPmNgFnS
skupině	skupina	k1gFnSc3
Český	český	k2eAgInSc1d1
aeroholding	aeroholding	k1gInSc1
za	za	k7c4
přibližně	přibližně	k6eAd1
750	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Zajišťuje	zajišťovat	k5eAaImIp3nS
pozemní	pozemní	k2eAgFnPc4d1
služby	služba	k1gFnPc4
na	na	k7c6
Letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
odbavení	odbavení	k1gNnSc1
cestujících	cestující	k1gMnPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
funguje	fungovat	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
založení	založení	k1gNnSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
se	se	k3xPyFc4
vyčlenily	vyčlenit	k5eAaPmAgFnP
činnosti	činnost	k1gFnPc1
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
úklid	úklid	k1gInSc4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
úklid	úklid	k1gInSc4
objektů	objekt	k1gInPc2
a	a	k8xC
dopravní	dopravní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
do	do	k7c2
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
s	s	k7c7
názvem	název	k1gInSc7
ČSA	ČSA	kA
Support	support	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
převzala	převzít	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
společnost	společnost	k1gFnSc1
služby	služba	k1gFnSc2
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
odbavováním	odbavování	k1gNnSc7
cestujících	cestující	k1gMnPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
a	a	k8xC
o	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
změnila	změnit	k5eAaPmAgFnS
název	název	k1gInSc4
na	na	k7c4
Czech	Czech	k1gInSc4
Airlines	Airlines	k1gInSc1
Handling	Handling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
Technics	Technics	kA
a.s.	a.s.	k?
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
jako	jako	k8xC,k8xS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
předtím	předtím	k6eAd1
působila	působit	k5eAaImAgFnS
jako	jako	k9
součást	součást	k1gFnSc1
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
stala	stát	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
Český	český	k2eAgInSc4d1
aeroholding	aeroholding	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
za	za	k7c4
tuto	tento	k3xDgFnSc4
firmu	firma	k1gFnSc4
zaplatila	zaplatit	k5eAaPmAgFnS
1,1	1,1	k4
miliardy	miliarda	k4xCgFnSc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
údržbu	údržba	k1gFnSc4
letadel	letadlo	k1gNnPc2
pro	pro	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
a	a	k8xC
další	další	k2eAgMnSc1d1
dopravce	dopravce	k1gMnSc1
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
zaměstnávala	zaměstnávat	k5eAaImAgFnS
přes	přes	k7c4
800	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
měla	mít	k5eAaImAgFnS
zisk	zisk	k1gInSc4
4,35	4,35	k4
milion	milion	k4xCgInSc4
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
CSA	CSA	kA
Services	Services	k1gMnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
bylo	být	k5eAaImAgNnS
kontaktní	kontaktní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
dříve	dříve	k6eAd2
spadající	spadající	k2eAgNnSc4d1
pod	pod	k7c4
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
nabízející	nabízející	k2eAgFnSc2d1
personální	personální	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Firma	firma	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
vlastníkem	vlastník	k1gMnSc7
Český	český	k2eAgInSc1d1
aeroholding	aeroholding	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Czech	Czech	k1gInSc1
Aviation	Aviation	k1gInSc1
Training	Training	k1gInSc1
Centre	centr	k1gInSc5
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
CATC	CATC	kA
je	být	k5eAaImIp3nS
výcvikové	výcvikový	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
zajišťující	zajišťující	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
pilotů	pilot	k1gMnPc2
a	a	k8xC
dalšího	další	k2eAgInSc2d1
personálu	personál	k1gInSc2
v	v	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Poskytuje	poskytovat	k5eAaImIp3nS
výcvik	výcvik	k1gInSc1
pilotů	pilot	k1gMnPc2
Český	český	k2eAgMnSc1d1
aerolinií	aerolinie	k1gFnSc7
<g/>
,	,	kIx,
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
tradici	tradice	k1gFnSc4
výcvikového	výcvikový	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
ČSA	ČSA	kA
<g/>
,	,	kIx,
sídli	sídlet	k5eAaImRp2nS
na	na	k7c6
Letišti	letiště	k1gNnSc6
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Holidays	Holidays	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
HCA	HCA	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
charterová	charterový	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
založená	založený	k2eAgFnSc1d1
Českými	český	k2eAgFnPc7d1
aeroliniemi	aerolinie	k1gFnPc7
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovala	provozovat	k5eAaImAgFnS
nepravidelnou	pravidelný	k2eNgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
přepravu	přeprava	k1gFnSc4
flotilou	flotila	k1gFnSc7
Boeingů	boeing	k1gInPc2
737	#num#	k4
a	a	k8xC
Airbusů	airbus	k1gInPc2
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
si	se	k3xPyFc3
ztrátové	ztrátový	k2eAgFnPc1d1
Holidays	Holidays	k1gInSc4
Czech	Czecha	k1gFnPc2
Airlines	Airlinesa	k1gFnPc2
od	od	k7c2
ČSA	ČSA	kA
odkoupil	odkoupit	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
aeroholding	aeroholding	k1gInSc1
za	za	k7c4
víc	hodně	k6eAd2
než	než	k8xS
půl	půl	k1xP
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Rozhodnutím	rozhodnutí	k1gNnSc7
bývalého	bývalý	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
Miroslava	Miroslav	k1gMnSc2
Kalouska	Kalousek	k1gMnSc2
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
utlumení	utlumení	k1gNnSc3
činnosti	činnost	k1gFnSc2
na	na	k7c6
trhu	trh	k1gInSc6
charterové	charterový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
této	tento	k3xDgFnSc2
nejprve	nejprve	k6eAd1
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
ČSA	ČSA	kA
<g/>
,	,	kIx,
poté	poté	k6eAd1
sesterské	sesterský	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
Českého	český	k2eAgInSc2d1
Aeroholdingu	Aeroholding	k1gInSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
soudním	soudní	k2eAgInPc3d1
sporům	spor	k1gInPc3
vedenými	vedený	k2eAgFnPc7d1
konkurenční	konkurenční	k2eAgInSc1d1
společností	společnost	k1gFnSc7
Travel	Travela	k1gFnPc2
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smartwings	Smartwings	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
si	se	k3xPyFc3
poté	poté	k6eAd1
pronajala	pronajmout	k5eAaPmAgFnS
část	část	k1gFnSc1
letadel	letadlo	k1gNnPc2
HCA	HCA	kA
a	a	k8xC
ovládla	ovládnout	k5eAaPmAgFnS
i	i	k9
charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
pro	pro	k7c4
cestovní	cestovní	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2013	#num#	k4
byly	být	k5eAaImAgFnP
pozastaveny	pozastavit	k5eAaPmNgFnP
lety	léto	k1gNnPc7
HCA	HCA	kA
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
vymazána	vymazán	k2eAgFnSc1d1
z	z	k7c2
rejstříku	rejstřík	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
při	při	k7c6
nehodách	nehoda	k1gFnPc6
letadel	letadlo	k1gNnPc2
ČSA	ČSA	kA
zemřelo	zemřít	k5eAaPmAgNnS
545	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
12	#num#	k4
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
v	v	k7c6
letadle	letadlo	k1gNnSc6
při	při	k7c6
havárii	havárie	k1gFnSc6
nebyli	být	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
ČSA	ČSA	kA
nestala	stát	k5eNaPmAgFnS
žádná	žádný	k3yNgFnSc1
smrtelná	smrtelný	k2eAgFnSc1d1
nehoda	nehoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejtragičtější	tragický	k2eAgFnSc1d3
nehoda	nehoda	k1gFnSc1
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
byl	být	k5eAaImAgInS
Let	let	k1gInSc1
ČSA	ČSA	kA
540	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
při	při	k7c6
nepodařeném	podařený	k2eNgInSc6d1
pokusu	pokus	k1gInSc6
o	o	k7c4
přistání	přistání	k1gNnSc4
v	v	k7c6
Damašku	Damašek	k1gInSc6
zemřelo	zemřít	k5eAaPmAgNnS
126	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
přežili	přežít	k5eAaPmAgMnP
pouze	pouze	k6eAd1
dva	dva	k4xCgMnPc1
<g/>
.	.	kIx.
</s>
<s>
Ředitelé	ředitel	k1gMnPc1
</s>
<s>
Od	od	k7c2
rozdělení	rozdělení	k1gNnSc2
ČSA	ČSA	kA
v	v	k7c6
listopadu	listopad	k1gInSc6
1991	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
současnosti	současnost	k1gFnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
jedenáct	jedenáct	k4xCc1
ředitelů	ředitel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1
obdobíJméno	obdobíJméno	k1gNnSc1
</s>
<s>
listopad	listopad	k1gInSc1
1990	#num#	k4
–	–	k?
duben	duben	k1gInSc1
1992	#num#	k4
<g/>
Oldřich	Oldřich	k1gMnSc1
Churain	Churain	k1gMnSc1
</s>
<s>
červen	červen	k1gInSc1
1992	#num#	k4
–	–	k?
prosinec	prosinec	k1gInSc1
1993	#num#	k4
<g/>
Jiří	Jiří	k1gMnSc1
Fiker	Fiker	k1gMnSc1
(	(	kIx(
<g/>
prezident	prezident	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
srpen	srpen	k1gInSc1
1992	#num#	k4
–	–	k?
únor	únor	k1gInSc4
1994	#num#	k4
<g/>
Georges	Georges	k1gInSc1
Vejdovský	Vejdovský	k2eAgInSc1d1
(	(	kIx(
<g/>
výkonný	výkonný	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
prosinec	prosinec	k1gInSc1
1993	#num#	k4
–	–	k?
srpen	srpen	k1gInSc4
1999	#num#	k4
<g/>
Antonín	Antonín	k1gMnSc1
Jakubše	Jakubš	k1gInSc2
</s>
<s>
září	září	k1gNnSc1
1999	#num#	k4
–	–	k?
prosinec	prosinec	k1gInSc1
2003	#num#	k4
<g/>
Miroslav	Miroslav	k1gMnSc1
Kůla	Kůla	k1gMnSc1
</s>
<s>
leden	leden	k1gInSc1
2004	#num#	k4
–	–	k?
leden	leden	k1gInSc4
2006	#num#	k4
<g/>
Jaroslav	Jaroslav	k1gMnSc1
Tvrdík	Tvrdík	k1gMnSc1
</s>
<s>
únor	únor	k1gInSc1
2006	#num#	k4
–	–	k?
říjen	říjen	k1gInSc4
2009	#num#	k4
<g/>
Radomír	Radomír	k1gMnSc1
Lašák	Lašák	k1gMnSc1
</s>
<s>
října	říjen	k1gInSc2
2009	#num#	k4
–	–	k?
říjen	říjen	k1gInSc4
2011	#num#	k4
<g/>
Miroslav	Miroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
</s>
<s>
listopad	listopad	k1gInSc1
2011	#num#	k4
–	–	k?
září	zářit	k5eAaImIp3nP
2014	#num#	k4
<g/>
Philippe	Philipp	k1gMnSc5
Moreels	Moreelsa	k1gFnPc2
</s>
<s>
září	září	k1gNnSc1
2014	#num#	k4
–	–	k?
říjen	říjen	k1gInSc1
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
<g/>
Jozef	Jozef	k1gMnSc1
Sinčák	Sinčák	k1gMnSc1
</s>
<s>
listopad	listopad	k1gInSc1
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
–	–	k?
současnostPetr	současnostPetr	k1gInSc1
Kudela	Kudela	k1gFnSc1
</s>
<s>
Destinace	destinace	k1gFnSc1
</s>
<s>
Destinace	destinace	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
2017	#num#	k4
<g/>
;	;	kIx,
modře	modř	k1gFnSc2
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
<g/>
;	;	kIx,
červeně	červeně	k6eAd1
<g/>
:	:	kIx,
pravidelné	pravidelný	k2eAgFnPc4d1
destinace	destinace	k1gFnPc4
<g/>
;	;	kIx,
žlutě	žlutě	k6eAd1
<g/>
:	:	kIx,
sezónní	sezónní	k2eAgFnPc4d1
destinace	destinace	k1gFnPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Destinace	destinace	k1gFnSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
v	v	k7c4
letní	letní	k2eAgFnSc4d1
sezónně	sezónně	k6eAd1
2017	#num#	k4
létaly	létat	k5eAaImAgFnP
do	do	k7c2
50	#num#	k4
destinací	destinace	k1gFnPc2
v	v	k7c6
25	#num#	k4
zemích	zem	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
(	(	kIx(
<g/>
Soul	Soul	k1gInSc1
<g/>
,	,	kIx,
Rijád	Rijáda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
Provozují	provozovat	k5eAaImIp3nP
pravidelné	pravidelný	k2eAgInPc4d1
lety	let	k1gInPc4
z	z	k7c2
hlavní	hlavní	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Codeshare	Codeshar	k1gMnSc5
</s>
<s>
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
tzv.	tzv.	kA
codeshare	codeshar	k1gMnSc5
dohodu	dohoda	k1gFnSc4
a	a	k8xC
provozují	provozovat	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
společné	společný	k2eAgInPc4d1
lety	let	k1gInPc4
s	s	k7c7
aerolinkami	aerolinka	k1gFnPc7
SkyTeamu	SkyTeam	k1gInSc2
a	a	k8xC
dalšími	další	k2eAgInPc7d1
<g/>
:	:	kIx,
</s>
<s>
Aeroflot	aeroflot	k1gInSc1
</s>
<s>
Aeroméxico	Aeroméxico	k6eAd1
</s>
<s>
Air	Air	k?
Europa	Europa	k1gFnSc1
</s>
<s>
Air	Air	k?
France	Franc	k1gMnSc4
</s>
<s>
Air	Air	k?
Malta	Malta	k1gFnSc1
</s>
<s>
Air	Air	k?
Seychelles	Seychelles	k1gInSc1
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
airBaltic	airBaltice	k1gFnPc2
</s>
<s>
Alitalia	Alitalia	k1gFnSc1
</s>
<s>
Ázerbájdžánské	ázerbájdžánský	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
Belavia	Belavia	k1gFnSc1
</s>
<s>
Bulgaria	Bulgarium	k1gNnPc1
Air	Air	k1gFnSc2
</s>
<s>
China	China	k1gFnSc1
Airlines	Airlinesa	k1gFnPc2
</s>
<s>
China	China	k1gFnSc1
Southern	Southern	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
China	China	k1gFnSc1
Eastern	Eastern	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Delta	delta	k1gFnSc1
Air	Air	k1gFnSc2
Lines	Linesa	k1gFnPc2
</s>
<s>
Etihad	Etihad	k6eAd1
Airways	Airways	k1gInSc1
</s>
<s>
Finnair	Finnair	k1gMnSc1
</s>
<s>
Hainan	Hainan	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Iberia	Iberium	k1gNnPc1
</s>
<s>
KLM	KLM	kA
</s>
<s>
Korean	Korean	k1gMnSc1
Air	Air	k1gMnSc1
</s>
<s>
Middle	Middle	k6eAd1
East	East	k2eAgInSc1d1
Airlines	Airlines	k1gInSc1
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smartwings	Smartwings	k1gInSc1
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
TAROM	TAROM	kA
</s>
<s>
Ukraine	Ukrainout	k5eAaPmIp3nS
International	International	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Ural	Ural	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
</s>
<s>
Vietnam	Vietnam	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
</s>
<s>
Charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
kromě	kromě	k7c2
pravidelných	pravidelný	k2eAgInPc2d1
letů	let	k1gInPc2
provozují	provozovat	k5eAaImIp3nP
také	také	k9
nepravidelné	pravidelný	k2eNgInPc4d1
charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
cestovní	cestovní	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Exim	Exim	k6eAd1
Tours	Tours	k1gInSc1
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1
sesterská	sesterský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
Holidays	Holidays	k1gInSc1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
a	a	k8xC
její	její	k3xOp3gInSc1
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
320	#num#	k4
<g/>
Česká	český	k2eAgFnSc1d1
cestovní	cestovní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
Exim	Exim	k1gMnSc1
Tours	Tours	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
s	s	k7c7
Českými	český	k2eAgFnPc7d1
aeroliniemi	aerolinie	k1gFnPc7
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
charterové	charterový	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
tak	tak	k6eAd1
začala	začít	k5eAaPmAgFnS
využívat	využívat	k5eAaPmF,k5eAaImF
přepravní	přepravní	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
této	tento	k3xDgFnSc2
letecké	letecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
roce	rok	k1gInSc6
zahájení	zahájení	k1gNnSc2
spolupráce	spolupráce	k1gFnSc2
ČSA	ČSA	kA
na	na	k7c6
charterových	charterový	k2eAgNnPc6d1
letech	léto	k1gNnPc6
přepravily	přepravit	k5eAaPmAgFnP
330	#num#	k4
000	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
díky	díky	k7c3
této	tento	k3xDgFnSc3
cestovní	cestovní	k2eAgFnSc3d1
kanceláři	kancelář	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
ČSA	ČSA	kA
podepsaly	podepsat	k5eAaPmAgInP
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Exim	Exi	k1gNnSc7
Tours	Toursa	k1gFnPc2
o	o	k7c4
prodloužení	prodloužení	k1gNnSc4
smlouvy	smlouva	k1gFnSc2
na	na	k7c4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
létaly	létat	k5eAaImAgFnP
s	s	k7c7
klienty	klient	k1gMnPc7
Exim	Exima	k1gFnPc2
Tours	Toursa	k1gFnPc2
například	například	k6eAd1
do	do	k7c2
destinací	destinace	k1gFnPc2
jako	jako	k8xC,k8xS
Varadera	Varadero	k1gNnSc2
na	na	k7c6
Kubě	Kuba	k1gFnSc6
<g/>
,	,	kIx,
Isla	Isl	k2eAgFnSc1d1
de	de	k?
Margarita	Margarita	k1gFnSc1
ve	v	k7c6
Venezuele	Venezuela	k1gFnSc6
<g/>
,	,	kIx,
ostrovy	ostrov	k1gInPc4
San	San	k1gFnSc2
Andres	Andres	k1gInSc1
a	a	k8xC
Providencia	Providencia	k1gFnSc1
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
<g/>
,	,	kIx,
La	la	k1gNnSc4
Romana	Roman	k1gMnSc2
v	v	k7c6
Dominikánské	dominikánský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
nebo	nebo	k8xC
do	do	k7c2
destinací	destinace	k1gFnPc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Salvadoru	Salvador	k1gInSc2
<g/>
,	,	kIx,
Tuniska	Tunisko	k1gNnSc2
a	a	k8xC
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
přepravily	přepravit	k5eAaPmAgFnP
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
na	na	k7c6
charterových	charterový	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
845	#num#	k4
000	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
13	#num#	k4
<g/>
%	%	kIx~
nárůst	nárůst	k1gInSc4
oproti	oproti	k7c3
roku	rok	k1gInSc3
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
skončil	skončit	k5eAaPmAgInS
Českým	český	k2eAgInSc7d1
aeroliniím	aerolinie	k1gFnPc3
leasing	leasing	k1gInSc4
posledního	poslední	k2eAgInSc2d1
dálkového	dálkový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Airbus	airbus	k1gInSc1
A310	A310	k1gFnSc1
umožňující	umožňující	k2eAgNnSc1d1
létat	létat	k5eAaImF
do	do	k7c2
exotických	exotický	k2eAgFnPc2d1
destinací	destinace	k1gFnPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
se	se	k3xPyFc4
největší	veliký	k2eAgFnSc1d3
česká	český	k2eAgFnSc1d1
cestovní	cestovní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
–	–	k?
Exim	Exim	k1gInSc1
Tours	Tours	k1gInSc1
po	po	k7c6
mnoholeté	mnoholetý	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
rozhodla	rozhodnout	k5eAaPmAgFnS
ukončit	ukončit	k5eAaPmF
dlouhodobou	dlouhodobý	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Klienti	klient	k1gMnPc1
Exim	Exima	k1gFnPc2
Tours	Tours	k1gInSc4
od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
začali	začít	k5eAaPmAgMnP
létat	létat	k5eAaImF
s	s	k7c7
českou	český	k2eAgFnSc7d1
leteckou	letecký	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Travel	Travela	k1gFnPc2
Service	Service	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
výběrové	výběrový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
a	a	k8xC
s	s	k7c7
Exim	Exi	k1gNnSc7
Tours	Toursa	k1gFnPc2
spolupracovala	spolupracovat	k5eAaImAgFnS
již	již	k6eAd1
předchozí	předchozí	k2eAgInPc4d1
roky	rok	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
cestovní	cestovní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
Exim	Exima	k1gFnPc2
Tours	Toursa	k1gFnPc2
od	od	k7c2
Travel	Travela	k1gFnPc2
Service	Service	k1gFnSc2
<g/>
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smartwings	Smartwingsa	k1gFnPc2
<g/>
)	)	kIx)
k	k	k7c3
ČSA	ČSA	kA
<g/>
,	,	kIx,
respektive	respektive	k9
k	k	k7c3
dceřiné	dceřiný	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
i	i	k9
na	na	k7c4
přání	přání	k1gNnPc4
klientů	klient	k1gMnPc2
Exim	Exim	k1gInSc1
Tours	Toursa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Holidays	Holidays	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
odchod	odchod	k1gInSc4
Exim	Exim	k1gInSc4
Tours	Toursa	k1gFnPc2
od	od	k7c2
ČSA	ČSA	kA
firma	firma	k1gFnSc1
založila	založit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
novou	nový	k2eAgFnSc4d1
charterovou	charterový	k2eAgFnSc4d1
sesterskou	sesterský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Holidays	Holidays	k1gInSc1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
na	na	k7c4
podzim	podzim	k1gInSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
uzavřela	uzavřít	k5eAaPmAgFnS
s	s	k7c7
cestovní	cestovní	k2eAgFnSc7d1
kanceláří	kancelář	k1gFnSc7
Blue	Blu	k1gFnSc2
Style	styl	k1gInSc5
tříletou	tříletý	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
následovaly	následovat	k5eAaImAgInP
charterové	charterový	k2eAgInPc1d1
lety	let	k1gInPc1
i	i	k9
pro	pro	k7c4
další	další	k2eAgFnPc4d1
cestovní	cestovní	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
si	se	k3xPyFc3
ztrátové	ztrátový	k2eAgFnPc1d1
Holidays	Holidays	k1gInSc4
Czech	Czecha	k1gFnPc2
Airlines	Airlinesa	k1gFnPc2
od	od	k7c2
ČSA	ČSA	kA
odkoupil	odkoupit	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
aeroholding	aeroholding	k1gInSc1
za	za	k7c4
víc	hodně	k6eAd2
než	než	k8xS
půl	půl	k1xP
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
později	pozdě	k6eAd2
začaly	začít	k5eAaPmAgInP
rušit	rušit	k5eAaImF
své	svůj	k3xOyFgMnPc4
chartery	charter	k1gMnPc4
a	a	k8xC
létaly	létat	k5eAaImAgInP
je	on	k3xPp3gFnPc4
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
posádkami	posádka	k1gFnPc7
pro	pro	k7c4
Travel	Travel	k1gInSc4
Service	Service	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6
2013	#num#	k4
byly	být	k5eAaImAgFnP
pozastaveny	pozastavit	k5eAaPmNgFnP
jejich	jejich	k3xOp3gFnPc1
lety	léto	k1gNnPc7
(	(	kIx(
<g/>
také	také	k9
kvůli	kvůli	k7c3
žalobě	žaloba	k1gFnSc3
TVS	TVS	kA
–	–	k?
dnes	dnes	k6eAd1
Smartwings	Smartwingsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
vymazána	vymazán	k2eAgFnSc1d1
z	z	k7c2
rejstříku	rejstřík	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smartwings	Smartwings	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
soudním	soudní	k2eAgInPc3d1
sporům	spor	k1gInPc3
vedenými	vedený	k2eAgFnPc7d1
konkurenční	konkurenční	k2eAgInSc1d1
společností	společnost	k1gFnSc7
Travel	Travela	k1gFnPc2
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smartwings	Smartwingsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
rozhodl	rozhodnout	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Miroslava	Miroslav	k1gMnSc2
Kalouska	Kalousek	k1gMnSc2
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
o	o	k7c6
utlumení	utlumení	k1gNnSc6
činnosti	činnost	k1gFnSc2
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
na	na	k7c6
trhu	trh	k1gInSc6
charterové	charterový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Travel	Travel	k1gInSc4
Service	Service	k1gFnSc2
si	se	k3xPyFc3
poté	poté	k6eAd1
pronajal	pronajmout	k5eAaPmAgMnS
část	část	k1gFnSc4
letadel	letadlo	k1gNnPc2
HCA	HCA	kA
a	a	k8xC
ovládl	ovládnout	k5eAaPmAgInS
i	i	k9
charterové	charterový	k2eAgInPc4d1
lety	let	k1gInPc4
Holidays	Holidaysa	k1gFnPc2
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
pro	pro	k7c4
cestovní	cestovní	k2eAgFnPc4d1
kanceláře	kancelář	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nákladní	nákladní	k2eAgInPc4d1
lety	let	k1gInPc4
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
provozují	provozovat	k5eAaImIp3nP
také	také	k9
svou	svůj	k3xOyFgFnSc4
organizační	organizační	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
Cargo	Cargo	k6eAd1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
Cargo	Cargo	k1gMnSc1
<g/>
)	)	kIx)
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
zajišťující	zajišťující	k2eAgFnSc4d1
přepravu	přeprava	k1gFnSc4
různého	různý	k2eAgInSc2d1
zásilek	zásilka	k1gFnPc2
a	a	k8xC
pošty	pošta	k1gFnSc2
v	v	k7c6
letadlech	letadlo	k1gNnPc6
ČSA	ČSA	kA
do	do	k7c2
přímých	přímý	k2eAgFnPc2d1
destinací	destinace	k1gFnPc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
k	k	k7c3
tomu	ten	k3xDgNnSc3
především	především	k9
nákladní	nákladní	k2eAgFnSc1d1
kapacity	kapacita	k1gFnPc1
na	na	k7c6
palubách	paluba	k1gFnPc6
letadel	letadlo	k1gNnPc2
ČSA	ČSA	kA
nebo	nebo	k8xC
speciální	speciální	k2eAgInPc4d1
nákladní	nákladní	k2eAgInPc4d1
lety	let	k1gInPc4
v	v	k7c6
režimu	režim	k1gInSc6
jednorázových	jednorázový	k2eAgFnPc2d1
i	i	k8xC
pravidelných	pravidelný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
Cargo	Cargo	k6eAd1
jsou	být	k5eAaImIp3nP
členem	člen	k1gMnSc7
letecké	letecký	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc4
Cargo	Cargo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
členy	člen	k1gMnPc4
SkyTeamu	SkyTeam	k1gInSc2
jako	jako	k9
generální	generální	k2eAgMnSc1d1
prodejní	prodejní	k2eAgMnSc1d1
agent	agent	k1gMnSc1
pro	pro	k7c4
Česko	Česko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Flotila	flotila	k1gFnSc1
</s>
<s>
ATR	ATR	kA
42-500	42-500	k4
imatrikulace	imatrikulace	k1gFnSc2
OK-JFK	OK-JFK	k1gFnPc2
</s>
<s>
Airbus	airbus	k1gInSc1
A319-112	A319-112	k1gFnSc2
(	(	kIx(
<g/>
OK-NEM	OK-NEM	k1gMnSc1
<g/>
)	)	kIx)
ČSA	ČSA	kA
na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Současná	současný	k2eAgFnSc1d1
</s>
<s>
Boeing	boeing	k1gInSc1
737-800	737-800	k4
(	(	kIx(
<g/>
OK-TST	OK-TST	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
se	se	k3xPyFc4
k	k	k7c3
7	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
2021	#num#	k4
skládala	skládat	k5eAaImAgFnS
ze	z	k7c2
2	#num#	k4
letadel	letadlo	k1gNnPc2
s	s	k7c7
různým	různý	k2eAgNnSc7d1
vlastnictvím	vlastnictví	k1gNnSc7
<g/>
,	,	kIx,
ČSA	ČSA	kA
vlastní	vlastnit	k5eAaImIp3nS
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc4
letadlo	letadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrné	průměrný	k2eAgInPc1d1
stáří	stáří	k1gNnSc3
letounů	letoun	k1gInPc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
14,2	14,2	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
9,5	9,5	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgInSc7d1
typem	typ	k1gInSc7
ve	v	k7c6
flotile	flotila	k1gFnSc6
je	být	k5eAaImIp3nS
úzkotrupý	úzkotrupý	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A319	A319	k1gFnPc2
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turbuvrtulovými	Turbuvrtulův	k2eAgFnPc7d1
ATR	ATR	kA
72	#num#	k4
létají	létat	k5eAaImIp3nP
ČSA	ČSA	kA
na	na	k7c6
především	především	k6eAd1
krátkých	krátký	k2eAgInPc6d1
<g/>
,	,	kIx,
až	až	k6eAd1
středně	středně	k6eAd1
dlouhých	dlouhý	k2eAgFnPc6d1
trasách	trasa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejdelší	dlouhý	k2eAgFnSc6d3
lince	linka	k1gFnSc6
do	do	k7c2
Soulu	Soul	k1gInSc2
létal	létat	k5eAaImAgInS
širokotrupý	širokotrupý	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
330	#num#	k4
<g/>
-	-	kIx~
<g/>
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vytížené	vytížený	k2eAgFnSc6d1
letní	letní	k2eAgFnSc6d1
sezóny	sezóna	k1gFnSc2
si	se	k3xPyFc3
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
půjčují	půjčovat	k5eAaImIp3nP
letadla	letadlo	k1gNnSc2
od	od	k7c2
společností	společnost	k1gFnPc2
jako	jako	k8xS,k8xC
Smartwings	Smartwingsa	k1gFnPc2
či	či	k8xC
Go	Go	k1gFnPc2
<g/>
2	#num#	k4
<g/>
Sky	Sky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
v	v	k7c6
zimě	zima	k1gFnSc6
při	pře	k1gFnSc4
menším	malý	k2eAgNnSc7d2
vytížením	vytížení	k1gNnSc7
společnost	společnost	k1gFnSc1
pronajímá	pronajímat	k5eAaImIp3nS
svá	svůj	k3xOyFgNnPc4
letadla	letadlo	k1gNnPc4
dalším	další	k2eAgFnPc3d1
leteckým	letecký	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
jako	jako	k8xS,k8xC
například	například	k6eAd1
Eurowings	Eurowingsa	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
Saudia	Saudium	k1gNnSc2
nebo	nebo	k8xC
SpiceJet	SpiceJeta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
zeštíhlila	zeštíhlit	k5eAaPmAgFnS
na	na	k7c4
pouhých	pouhý	k2eAgInPc2d1
5	#num#	k4
letadel	letadlo	k1gNnPc2
–	–	k?
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
×	×	k?
ATR-	ATR-	k1gFnPc2
<g/>
72	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
Airbus	airbus	k1gInSc1
A	A	kA
<g/>
319	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
×	×	k?
Airbus	airbus	k1gInSc1
A320	A320	k1gFnSc2
a	a	k8xC
1	#num#	k4
<g/>
×	×	k?
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
-	-	kIx~
<g/>
800	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2021	#num#	k4
byly	být	k5eAaImAgFnP
z	z	k7c2
provozu	provoz	k1gInSc2
vyřazeny	vyřadit	k5eAaPmNgFnP
všechna	všechen	k3xTgNnPc4
zbývající	zbývající	k2eAgNnSc4d1
ATR	ATR	kA
72	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letadlo	letadlo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
Objednávky	objednávka	k1gFnPc4
</s>
<s>
Cestující	cestující	k1gMnPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Imatrikulace	imatrikulace	k1gFnSc1
</s>
<s>
B	B	kA
</s>
<s>
E	E	kA
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
Airbus	airbus	k1gInSc1
A220-300	A220-300	k1gFnSc2
</s>
<s>
-	-	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
145	#num#	k4
</s>
<s>
4	#num#	k4
letadla	letadlo	k1gNnPc1
budou	být	k5eAaImBp3nP
přímo	přímo	k6eAd1
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
ČSA	ČSA	kA
<g/>
,	,	kIx,
2	#num#	k4
pronajata	pronajmout	k5eAaPmNgFnS
od	od	k7c2
leasingových	leasingový	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mají	mít	k5eAaImIp3nP
nahradit	nahradit	k5eAaPmF
ATR-	ATR-	k1gFnSc7
<g/>
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařazení	zařazení	k1gNnSc1
odloženo	odložit	k5eAaPmNgNnS
na	na	k7c4
rok	rok	k1gInSc4
2022	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Airbus	airbus	k1gInSc1
A319-112	A319-112	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
138	#num#	k4
</s>
<s>
142	#num#	k4
</s>
<s>
Ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-REQ	OK-REQ	k?
</s>
<s>
Airbus	airbus	k1gInSc1
A320-200	A320-200	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
168	#num#	k4
</s>
<s>
180	#num#	k4
</s>
<s>
OK-HEU	OK-HEU	k?
po	po	k7c4
Brussels	Brussels	k1gInSc4
Airlines	Airlinesa	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
HORIZON	HORIZON	kA
AVIATION	AVIATION	kA
1	#num#	k4
LIMITED	limited	k2eAgFnPc2d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nosí	nosit	k5eAaImIp3nP
upravené	upravený	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
–	–	k?
bílý	bílý	k2eAgInSc1d1
spodek	spodek	k1gInSc1
trupu	trup	k1gInSc2
místo	místo	k7c2
šedého	šedý	k2eAgMnSc2d1
a	a	k8xC
upravené	upravený	k2eAgNnSc1d1
logo	logo	k1gNnSc1
na	na	k7c6
ocasní	ocasní	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
OK-HEU	OK-HEU	k?
</s>
<s>
Airbus	airbus	k1gInSc1
A321XLR	A321XLR	k1gFnSc2
</s>
<s>
-	-	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
195	#num#	k4
</s>
<s>
Všechna	všechen	k3xTgNnPc1
letadla	letadlo	k1gNnPc1
budou	být	k5eAaImBp3nP
ve	v	k7c6
vlastnictví	vlastnictví	k1gNnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
plánuje	plánovat	k5eAaImIp3nS
návrat	návrat	k1gInSc1
na	na	k7c4
dálkové	dálkový	k2eAgFnPc4d1
linky	linka	k1gFnPc4
do	do	k7c2
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařazení	zařazení	k1gNnSc1
se	se	k3xPyFc4
plánuje	plánovat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
2	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Historická	historický	k2eAgFnSc1d1
</s>
<s>
Vysloužené	vysloužený	k2eAgInPc4d1
typy	typ	k1gInPc4
letadel	letadlo	k1gNnPc2
provozované	provozovaný	k2eAgInPc1d1
jak	jak	k8xC,k8xS
Českými	český	k2eAgInPc7d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
Československými	československý	k2eAgFnPc7d1
aeroliniemi	aerolinie	k1gFnPc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letadlo	letadlo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
Zařazeno	zařazen	k2eAgNnSc1d1
</s>
<s>
Vyřazeno	vyřazen	k2eAgNnSc1d1
</s>
<s>
Cestujících	cestující	k1gFnPc2
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Imatrikulace	imatrikulace	k1gFnSc1
</s>
<s>
Aero	aero	k1gNnSc1
A-14	A-14	k1gFnSc2
Brandenburg	Brandenburg	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1923	#num#	k4
</s>
<s>
1927	#num#	k4
</s>
<s>
</s>
<s>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
</s>
<s>
Aero	aero	k1gNnSc1
Ae-	Ae-	k1gFnSc2
<g/>
45	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
1951	#num#	k4
</s>
<s>
1969	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Aera	aero	k1gNnSc2
Ae-	Ae-	k1gFnSc2
<g/>
45	#num#	k4
verzí	verze	k1gFnPc2
45	#num#	k4
a	a	k8xC
45S	45S	k4
sloužily	sloužit	k5eAaImAgFnP
pro	pro	k7c4
provozování	provozování	k1gNnSc4
aerotaxi	aerotaxi	k1gNnSc1
pod	pod	k7c7
značkou	značka	k1gFnSc7
Agrolet	Agrolet	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
</s>
<s>
Airbus	airbus	k1gInSc1
A310-300	A310-300	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
201	#num#	k4
<g/>
/	/	kIx~
<g/>
210	#num#	k4
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
chyběl	chybět	k5eAaImAgInS
ČSA	ČSA	kA
dálkový	dálkový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
a	a	k8xC
žádný	žádný	k3yNgInSc4
ze	z	k7c2
sovětské	sovětský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
nebyl	být	k5eNaImAgInS
vhodný	vhodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
létal	létat	k5eAaImAgInS
na	na	k7c6
pravidelných	pravidelný	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
do	do	k7c2
Bangkoku	Bangkok	k1gInSc2
<g/>
,	,	kIx,
Bombaje	Bombaj	k1gFnSc2
<g/>
,	,	kIx,
Colomba	Colomba	k1gFnSc1
<g/>
,	,	kIx,
Dubaje	Dubaj	k1gInPc1
<g/>
,	,	kIx,
Hanoje	Hanoj	k1gFnPc1
<g/>
,	,	kIx,
Havany	Havana	k1gFnPc1
<g/>
,	,	kIx,
Jakarty	Jakarta	k1gFnPc1
<g/>
,	,	kIx,
Kuala	Kuala	k1gFnSc1
Lumpuru	Lumpur	k1gInSc2
<g/>
,	,	kIx,
Montrealu	Montreal	k1gInSc2
<g/>
,	,	kIx,
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Singapuru	Singapur	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
skončil	skončit	k5eAaPmAgInS
leasing	leasing	k1gInSc1
posledních	poslední	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
Airbusů	airbus	k1gInPc2
A310-300	A310-300	k1gFnPc2
na	na	k7c4
dlouhé	dlouhý	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
pro	pro	k7c4
ČSA	ČSA	kA
ztrátové	ztrátový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
OK-WAA	OK-WAA	k?
<g/>
,	,	kIx,
OK-WAB	OK-WAB	k1gMnSc1
<g/>
,	,	kIx,
OK-YAC	OK-YAC	k1gMnSc1
<g/>
,	,	kIx,
OK-YAD	OK-YAD	k1gMnSc1
</s>
<s>
Airbus	airbus	k1gInSc1
A319-100	A319-100	k1gFnSc2
</s>
<s>
8	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
135	#num#	k4
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
získala	získat	k5eAaPmAgFnS
ČSA	ČSA	kA
společnost	společnost	k1gFnSc1
Smartwings	Smartwings	k1gInSc1
<g/>
,	,	kIx,
první	první	k4xOgInPc1
dva	dva	k4xCgInPc1
Airbusy	airbus	k1gInPc1
byly	být	k5eAaImAgInP
sešrotovány	sešrotovat	k5eAaPmNgInP
roku	rok	k1gInSc2
2019	#num#	k4
na	na	k7c6
letišti	letiště	k1gNnSc6
St.	st.	kA
Athan	Athan	k1gMnSc1
<g/>
,	,	kIx,
třetí	třetí	k4xOgInSc1
uložen	uložen	k2eAgInSc1d1
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
přelétnuty	přelétnut	k2eAgInPc4d1
na	na	k7c4
letiště	letiště	k1gNnSc4
Kemble	Kemble	k1gFnSc2
ke	k	k7c3
šrotaci	šrotace	k1gFnSc3
v	v	k7c6
březnu	březen	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
Smartwings	Smartwings	k1gInSc4
deklarovala	deklarovat	k5eAaBmAgFnS
kvůli	kvůli	k7c3
výpadku	výpadek	k1gInSc3
Boeing	boeing	k1gInSc1
Max	Max	k1gMnSc1
nedostatek	nedostatek	k1gInSc1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
OK-PET	OK-PET	k?
nosil	nosit	k5eAaImAgMnS
barvy	barva	k1gFnPc4
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc4
</s>
<s>
OK-NEP	OK-NEP	k?
nosil	nosit	k5eAaImAgInS
speciální	speciální	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
propagující	propagující	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
OK-PET	OK-PET	k?
<g/>
,	,	kIx,
OK-MEK	OK-MEK	k1gMnPc5
<g/>
,	,	kIx,
OK-OER	OK-OER	k1gMnPc5
<g/>
,	,	kIx,
OK-MEL	OK-MEL	k1gMnPc5
<g/>
,	,	kIx,
OK-NEN	OK-NEN	k1gMnPc5
</s>
<s>
OK-NEP	OK-NEP	k?
<g/>
,	,	kIx,
OK-NEM	OK-NEM	k1gMnSc1
<g/>
,	,	kIx,
OK-NEO	OK-NEO	k1gMnSc1
</s>
<s>
Airbus	airbus	k1gInSc1
A320-200	A320-200	k1gFnSc2
</s>
<s>
9	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
180	#num#	k4
</s>
<s>
Setrvání	setrvání	k1gNnSc1
typu	typ	k1gInSc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
flotile	flotila	k1gFnSc6
ekonomicky	ekonomicky	k6eAd1
neúnosné	únosný	k2eNgInPc1d1
–	–	k?
všechna	všechen	k3xTgNnPc1
místa	místo	k1gNnPc1
se	se	k3xPyFc4
málokdy	málokdy	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
naplnit	naplnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2014	#num#	k4
byl	být	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
A320	A320	k1gMnSc1
pronajat	pronajmout	k5eAaPmNgMnS
od	od	k7c2
Air	Air	k1gMnSc2
Asia	Asius	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
létal	létat	k5eAaImAgMnS
v	v	k7c6
designu	design	k1gInSc6
malajské	malajský	k2eAgFnSc2d1
aerolinky	aerolinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
OK-GEA	OK-GEA	k?
<g/>
,	,	kIx,
OK-GEB	OK-GEB	k1gMnPc5
<g/>
,	,	kIx,
OK-LEE	OK-LEE	k1gMnPc5
<g/>
,	,	kIx,
OK-LEF	OK-LEF	k1gMnPc5
<g/>
,	,	kIx,
OK-LEG	OK-LEG	k1gMnPc5
<g/>
,	,	kIx,
OK-MEH	OK-MEH	k1gMnPc5
<g/>
,	,	kIx,
OK-MEI	OK-MEI	k1gMnPc5
<g/>
,	,	kIx,
OK-MEJ	OK-MEJ	k1gMnPc5
<g/>
,	,	kIx,
OK-NES	OK-NES	k1gMnPc5
</s>
<s>
Airbus	airbus	k1gInSc1
A321-200	A321-200	k1gFnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
206	#num#	k4
<g/>
/	/	kIx~
<g/>
212	#num#	k4
</s>
<s>
Setrvání	setrvání	k1gNnSc1
typu	typ	k1gInSc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
flotile	flotila	k1gFnSc6
ekonomicky	ekonomicky	k6eAd1
neúnosné	únosný	k2eNgInPc1d1
–	–	k?
všechna	všechen	k3xTgNnPc1
místa	místo	k1gNnPc1
se	se	k3xPyFc4
málokdy	málokdy	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
naplnit	naplnit	k5eAaPmF
<g/>
,	,	kIx,
létaly	létat	k5eAaImAgFnP
převážně	převážně	k6eAd1
na	na	k7c6
charterech	charter	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
léto	léto	k1gNnSc4
2018	#num#	k4
si	se	k3xPyFc3
ČSA	ČSA	kA
typ	typa	k1gFnPc2
pronajaly	pronajmout	k5eAaPmAgFnP
od	od	k7c2
Danish	Danisha	k1gFnPc2
Air	Air	k1gFnSc2
Transport	transport	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-CED	OK-CED	k?
<g/>
,	,	kIx,
OK-CEC	OK-CEC	k1gMnSc1
<g/>
,	,	kIx,
OY-RUU	OY-RUU	k1gMnSc1
</s>
<s>
Airbus	airbus	k1gInSc1
A330-200	A330-200	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
Pronajatý	pronajatý	k2eAgMnSc1d1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Air	Air	k1gMnSc2
Transat	Transat	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Létal	létat	k5eAaImAgMnS
v	v	k7c6
barvách	barva	k1gFnPc6
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc1
z	z	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
OK-GBB	OK-GBB	k?
</s>
<s>
Airbus	airbus	k1gInSc1
A330-300	A330-300	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
252	#num#	k4
</s>
<s>
Jediný	jediný	k2eAgInSc1d1
Airbus	airbus	k1gInSc1
A330-300	A330-300	k1gFnSc2
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťoval	zajišťovat	k5eAaImAgMnS
linku	linka	k1gFnSc4
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Soulu	Soul	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
pandemie	pandemie	k1gFnSc2
covidu-	covidu-	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadlo	letadlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vráceno	vrátit	k5eAaPmNgNnS
majiteli	majitel	k1gMnPc7
<g/>
,	,	kIx,
společnosti	společnost	k1gFnPc4
Korean	Koreana	k1gFnPc2
Air	Air	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
OK-YBA	OK-YBA	k?
</s>
<s>
Airspeed	Airspeed	k1gInSc1
AS	as	k9
<g/>
.6	.6	k4
Envoy	Envoum	k1gNnPc7
</s>
<s>
4	#num#	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Britský	britský	k2eAgInSc1d1
celodřevěný	celodřevěný	k2eAgInSc1d1
nízkokapacitní	nízkokapacitní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
prvním	první	k4xOgNnSc7
letadlem	letadlo	k1gNnSc7
se	s	k7c7
zatahovacím	zatahovací	k2eAgInSc7d1
podvozkem	podvozek	k1gInSc7
ve	v	k7c6
flotile	flotila	k1gFnSc6
ČSA	ČSA	kA
<g/>
;	;	kIx,
obsluhovaly	obsluhovat	k5eAaImAgInP
hlavně	hlavně	k9
linku	linka	k1gFnSc4
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
OK-BAL	OK-BAL	k?
<g/>
,	,	kIx,
OK-BAM	OK-BAM	k1gMnSc1
<g/>
,	,	kIx,
OK-BAN	OK-BAN	k1gMnSc1
<g/>
,	,	kIx,
OK-BAO	OK-BAO	k1gMnSc1
</s>
<s>
ATR	ATR	kA
42-300	42-300	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
OK-BFG	OK-BFG	k?
<g/>
,	,	kIx,
OK-BFH	OK-BFH	k1gMnPc5
<g/>
,	,	kIx,
OK-TFE	OK-TFE	k1gMnPc5
<g/>
,	,	kIx,
OK-TFF	OK-TFF	k1gMnPc5
<g/>
,	,	kIx,
OK-VFI	OK-VFI	k1gMnPc5
</s>
<s>
ATR	ATR	kA
42-400	42-400	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
</s>
<s>
OK-AFE	OK-AFE	k?
<g/>
,	,	kIx,
OK-AFF	OK-AFF	k1gFnSc1
</s>
<s>
ATR	ATR	kA
42-500	42-500	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2019	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
OK-JFL	OK-JFL	k?
<g/>
,	,	kIx,
OK-JFJ	OK-JFJ	k1gMnPc5
<g/>
,	,	kIx,
OK-JFK	OK-JFK	k1gMnPc5
<g/>
,	,	kIx,
OK-KFO	OK-KFO	k1gMnPc5
<g/>
,	,	kIx,
OK-KFN	OK-KFN	k1gMnPc5
<g/>
,	,	kIx,
OK-KFP	OK-KFP	k1gMnPc5
</s>
<s>
ATR	ATR	kA
72-200	72-200	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
vypůjčení	vypůjčení	k1gNnPc2
na	na	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
od	od	k7c2
Mistral	mistral	k1gInSc4
Air	Air	k1gFnSc4
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-XFA	OK-XFA	k?
<g/>
,	,	kIx,
OK-XFB	OK-XFB	k1gMnPc5
<g/>
,	,	kIx,
OK-XFC	OK-XFC	k1gMnPc5
<g/>
,	,	kIx,
OK-XFD	OK-XFD	k1gMnPc5
<g/>
,	,	kIx,
OK-XFT	OK-XFT	k1gMnPc5
<g/>
,	,	kIx,
</s>
<s>
ATR	ATR	kA
72-500	72-500	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2021	#num#	k4
</s>
<s>
70	#num#	k4
</s>
<s>
Vyřazeny	vyřadit	k5eAaPmNgFnP
v	v	k7c6
březnu	březen	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plánované	plánovaný	k2eAgFnPc4d1
nahrazení	nahrazení	k1gNnPc1
Airbusy	airbus	k1gInPc4
A220-300	A220-300	k1gFnSc7
</s>
<s>
OK-GFR	OK-GFR	k?
nosil	nosit	k5eAaImAgMnS
barvy	barva	k1gFnPc4
aliance	aliance	k1gFnSc2
Sky	Sky	k1gFnPc2
Team	team	k1gInSc1
</s>
<s>
OK-MFT	OK-MFT	k?
<g/>
,	,	kIx,
OK-GFQ	OK-GFQ	k1gMnSc1
<g/>
,	,	kIx,
OK-GFR	OK-GFR	k1gMnSc1
<g/>
,	,	kIx,
OK-GFS	OK-GFS	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
OK-NFU	OK-NFU	k?
<g/>
,	,	kIx,
OK-NFV	OK-NFV	k1gFnSc1
</s>
<s>
Avia	Avia	k1gFnSc1
F-VIIb-	F-VIIb-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
m	m	kA
</s>
<s>
6	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
10-12	10-12	k4
</s>
<s>
Licenční	licenční	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Fokker	Fokkra	k1gFnPc2
F.	F.	kA
<g/>
VII	VII	kA
vyráběné	vyráběný	k2eAgInPc1d1
pražskou	pražský	k2eAgFnSc7d1
Avií	Avia	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
letounem	letoun	k1gInSc7
OK-AFA	OK-AFA	k1gFnPc2
zahájen	zahájit	k5eAaPmNgInS
mezinárodní	mezinárodní	k2eAgInSc1d1
provoz	provoz	k1gInSc1
linkou	linka	k1gFnSc7
do	do	k7c2
Záhřebu	Záhřeb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
nalétaly	nalétat	k5eAaBmAgInP,k5eAaPmAgInP,k5eAaImAgInP
2	#num#	k4
306	#num#	k4
487	#num#	k4
km	km	kA
a	a	k8xC
16	#num#	k4
016	#num#	k4
letových	letový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
</s>
<s>
OK-AFA	OK-AFA	k?
<g/>
,	,	kIx,
OK-AFB	OK-AFB	k1gMnPc5
<g/>
,	,	kIx,
OK-AFC	OK-AFC	k1gMnPc5
<g/>
,	,	kIx,
OK-AFD	OK-AFD	k1gMnPc5
<g/>
,	,	kIx,
OK-AFE	OK-AFE	k1gMnPc5
<g/>
,	,	kIx,
OK-ABU	OK-ABU	k1gMnPc5
</s>
<s>
Boeing	boeing	k1gInSc1
737-400	737-400	k4
</s>
<s>
15	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
162	#num#	k4
</s>
<s>
Na	na	k7c4
letní	letní	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
2016	#num#	k4
a	a	k8xC
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
<g/>
si	se	k3xPyFc3
ČSA	ČSA	kA
pronajaly	pronajmout	k5eAaPmAgFnP
jeden	jeden	k4xCgInSc4
kus	kus	k1gInSc4
od	od	k7c2
Go	Go	k1gFnSc2
<g/>
2	#num#	k4
<g/>
Sky	Sky	k1gFnPc2
</s>
<s>
OK-BGQ	OK-BGQ	k?
<g/>
,	,	kIx,
OK-CGT	OK-CGT	k1gMnPc5
<g/>
,	,	kIx,
OK-DGM	OK-DGM	k1gMnPc5
<g/>
,	,	kIx,
OK-DGN	OK-DGN	k1gMnPc5
<g/>
,	,	kIx,
OK-EGP	OK-EGP	k1gMnPc5
<g/>
,	,	kIx,
OK-FGR	OK-FGR	k1gMnPc5
<g/>
,	,	kIx,
OK-FGS	OK-FGS	k1gMnPc5
<g/>
,	,	kIx,
OK-VGZ	OK-VGZ	k1gMnPc5
<g/>
,	,	kIx,
OK-WGT	OK-WGT	k1gMnPc5
<g/>
,	,	kIx,
OK-WGG	OK-WGG	k1gMnPc5
<g/>
,	,	kIx,
OK-WGX	OK-WGX	k1gMnPc5
<g/>
,	,	kIx,
OK-WGY	OK-WGY	k1gMnPc5
<g/>
,	,	kIx,
OK-YGA	OK-YGA	k1gMnPc5
<g/>
,	,	kIx,
OK-YGU	OK-YGU	k1gMnPc5
<g/>
,	,	kIx,
OM-GTB	OM-GTB	k1gMnPc5
</s>
<s>
Boeing	boeing	k1gInSc1
737-500	737-500	k4
</s>
<s>
15	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
102	#num#	k4
<g/>
/	/	kIx~
<g/>
108	#num#	k4
</s>
<s>
OK-WGD	OK-WGD	k?
<g/>
,	,	kIx,
OK-XGV	OK-XGV	k1gMnPc5
<g/>
,	,	kIx,
OK-XGW	OK-XGW	k1gMnPc5
<g/>
,	,	kIx,
OK-XGA	OK-XGA	k1gMnPc5
<g/>
,	,	kIx,
OK-XGB	OK-XGB	k1gMnPc5
<g/>
,	,	kIx,
OK-XGC	OK-XGC	k1gMnPc5
<g/>
,	,	kIx,
OK-XGD	OK-XGD	k1gMnPc5
<g/>
,	,	kIx,
OK-XGE	OK-XGE	k1gMnPc5
<g/>
,	,	kIx,
OK-CGH	OK-CGH	k1gMnPc5
<g/>
,	,	kIx,
OK-CGJ	OK-CGJ	k1gMnPc5
<g/>
,	,	kIx,
OK-CGK	OK-CGK	k1gMnPc5
<g/>
,	,	kIx,
OK-DGL	OK-DGL	k1gMnPc5
<g/>
,	,	kIx,
OK-EGO	OK-EGO	k1gMnPc5
<g/>
,	,	kIx,
OK-DGB	OK-DGB	k1gMnPc5
<g/>
,	,	kIx,
OK-DGC	OK-DGC	k1gMnPc5
</s>
<s>
Bristol	Bristol	k1gInSc1
Britannia	Britannium	k1gNnSc2
</s>
<s>
2	#num#	k4
</s>
<s>
1962	#num#	k4
</s>
<s>
1969	#num#	k4
</s>
<s>
61-90	61-90	k4
?	?	kIx.
</s>
<s>
Pronajaté	pronajatý	k2eAgNnSc4d1
od	od	k7c2
kubánské	kubánský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Cubana	Cuban	k1gMnSc2
de	de	k?
Aviación	Aviación	k1gInSc1
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-MBA	OK-MBA	k?
<g/>
,	,	kIx,
OK-MBB	OK-MBB	k1gFnSc1
</s>
<s>
Douglas	Douglas	k1gMnSc1
DC-3	DC-3	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
1946	#num#	k4
</s>
<s>
</s>
<s>
21	#num#	k4
</s>
<s>
</s>
<s>
De	De	k?
Havilland	Havilland	k1gInSc1
DH	DH	kA
<g/>
.50	.50	k4
</s>
<s>
8	#num#	k4
</s>
<s>
1925	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
pro	pro	k7c4
provoz	provoz	k1gInSc4
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
,	,	kIx,
u	u	k7c2
společnosti	společnost	k1gFnSc2
nalétaly	nalétat	k5eAaBmAgInP,k5eAaPmAgInP,k5eAaImAgInP
1	#num#	k4
029	#num#	k4
792	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
</s>
<s>
L-BAHG	L-BAHG	k?
<g/>
,	,	kIx,
L-BALA	L-BALA	k1gMnPc5
<g/>
,	,	kIx,
L-BALB	L-BALB	k1gMnPc5
<g/>
,	,	kIx,
L-BALC	L-BALC	k1gMnPc5
<g/>
,	,	kIx,
L-BALD	L-BALD	k1gMnPc5
<g/>
,	,	kIx,
L-BALE	L-BALE	k1gMnPc5
<g/>
,	,	kIx,
L-BALF	L-BALF	k1gMnPc5
<g/>
,	,	kIx,
L-BALG	L-BALG	k1gMnPc5
</s>
<s>
Farman	Farman	k1gMnSc1
F.	F.	kA
<g/>
60	#num#	k4
Goliath	Goliatha	k1gFnPc2
</s>
<s>
6	#num#	k4
</s>
<s>
1924	#num#	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-ALF	OK-ALF	k?
<g/>
,	,	kIx,
OK-AGA	OK-AGA	k1gMnPc5
<g/>
,	,	kIx,
OK-AGC	OK-AGC	k1gMnPc5
<g/>
,	,	kIx,
OK-AGE	OK-AGE	k1gMnPc5
<g/>
,	,	kIx,
OK-AGF	OK-AGF	k1gMnPc5
<g/>
,	,	kIx,
OK-AGH	OK-AGH	k1gMnPc5
</s>
<s>
Ford	ford	k1gInSc1
5-AT-C	5-AT-C	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1929	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
ukončil	ukončit	k5eAaPmAgMnS
stroj	stroj	k1gInSc4
tragicky	tragicky	k6eAd1
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
havaroval	havarovat	k5eAaPmAgMnS
v	v	k7c6
Bedřichově	Bedřichův	k2eAgFnSc6d1
u	u	k7c2
Jihlavy	Jihlava	k1gFnSc2
<g/>
,	,	kIx,
12	#num#	k4
z	z	k7c2
13	#num#	k4
osob	osoba	k1gFnPc2
zahynulo	zahynout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
tento	tento	k3xDgInSc1
stroj	stroj	k1gInSc1
nalétal	nalétat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
15	#num#	k4
800	#num#	k4
km	km	kA
a	a	k8xC
109	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-FOR	OK-FOR	k?
</s>
<s>
Lisunov	Lisunov	k1gInSc1
Li-	Li-	k1gFnSc2
<g/>
2	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Tyto	tento	k3xDgInPc1
stroje	stroj	k1gInPc1
létaly	létat	k5eAaImAgInP
po	po	k7c6
boku	bok	k1gInSc6
Douglasů	Douglas	k1gMnPc2
DC-	DC-	k1gFnSc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
oproti	oproti	k7c3
Lisunovům	Lisunov	k1gInPc3
u	u	k7c2
mechaniků	mechanik	k1gMnPc2
a	a	k8xC
posádky	posádka	k1gFnSc2
oblíbenější	oblíbený	k2eAgFnSc2d2
kvůli	kvůli	k7c3
větší	veliký	k2eAgFnSc3d2
spolehlivosti	spolehlivost	k1gFnSc3
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-GAA	OK-GAA	k?
<g/>
,	,	kIx,
OK-GAB	OK-GAB	k1gMnPc5
<g/>
,	,	kIx,
OK-GAC	OK-GAC	k1gMnPc5
<g/>
,	,	kIx,
OK-GAD	OK-GAD	k1gMnPc5
<g/>
,	,	kIx,
OK-GAE	OK-GAE	k1gMnPc5
<g/>
,	,	kIx,
OK-GAF	OK-GAF	k1gMnPc5
<g/>
,	,	kIx,
OK-GAG	OK-GAG	k1gMnPc5
<g/>
,	,	kIx,
OK-GAH	OK-GAH	k1gMnPc5
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
12	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
</s>
<s>
OK-CBA	OK-CBA	k?
<g/>
,	,	kIx,
OK-CBF	OK-CBF	k1gMnSc4
<g/>
,	,	kIx,
OK-DBB	OK-DBB	k1gMnSc4
<g/>
,	,	kIx,
OK-DBC	OK-DBC	k1gMnSc4
<g/>
,	,	kIx,
OK-DBD	OK-DBD	k1gMnSc4
<g/>
,	,	kIx,
OK-DBG	OK-DBG	k1gMnSc4
<g/>
,	,	kIx,
OK-DBN	OK-DBN	k1gMnSc4
<g/>
,	,	kIx,
OK-DBP	OK-DBP	k1gMnSc4
<g/>
,	,	kIx,
OK-DBU	OK-DBU	k1gMnSc4
a	a	k8xC
OK-DBW	OK-DBW	k1gMnSc4
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
Avia	Avia	k1gFnSc1
14	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
1977	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
OK-LCE	OK-LCE	k?
<g/>
,	,	kIx,
?	?	kIx.
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
18	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
84	#num#	k4
<g/>
/	/	kIx~
<g/>
120	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-NAA	OK-NAA	k?
<g/>
,	,	kIx,
OK-NAB	OK-NAB	k1gMnPc5
<g/>
,	,	kIx,
OK-OAC	OK-OAC	k1gMnPc5
<g/>
,	,	kIx,
OK-OAD	OK-OAD	k1gMnPc5
<g/>
,	,	kIx,
OK-PAE	OK-PAE	k1gMnPc5
<g/>
,	,	kIx,
OK-PAF	OK-PAF	k1gMnPc5
<g/>
,	,	kIx,
OK-PAG	OK-PAG	k1gMnPc5
<g/>
,	,	kIx,
OK-PAH	OK-PAH	k1gMnPc5
<g/>
,	,	kIx,
OK-WAI	OK-WAI	k1gMnPc5
<g/>
,	,	kIx,
OK-WAJ	OK-WAJ	k1gMnPc5
<g/>
,	,	kIx,
OK-PAI	OK-PAI	k1gMnPc5
<g/>
,	,	kIx,
OK-VAF	OK-VAF	k1gMnPc5
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
62	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
1969	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
174	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-YBA	OK-YBA	k?
<g/>
,	,	kIx,
OK-YBB	OK-YBB	k1gMnPc5
<g/>
,	,	kIx,
OK-ZBC	OK-ZBC	k1gMnPc5
<g/>
,	,	kIx,
OK-ABD	OK-ABD	k1gMnPc5
<g/>
,	,	kIx,
OK-DBE	OK-DBE	k1gMnPc5
<g/>
,	,	kIx,
OK-DBF	OK-DBF	k1gMnPc5
<g/>
,	,	kIx,
OK-EGB	OK-EGB	k1gMnPc5
<g/>
,	,	kIx,
OK-GBH	OK-GBH	k1gMnPc5
<g/>
,	,	kIx,
OK-FBF	OK-FBF	k1gMnPc5
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc1
<g/>
62	#num#	k4
<g/>
M	M	kA
</s>
<s>
6	#num#	k4
</s>
<s>
1979	#num#	k4
</s>
<s>
1993	#num#	k4
</s>
<s>
174	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-JBI	OK-JBI	k?
<g/>
,	,	kIx,
OK-JBJ	OK-JBJ	k1gMnPc5
<g/>
,	,	kIx,
OK-KBK	OK-KBK	k1gMnPc5
<g/>
,	,	kIx,
OK-OBL	OK-OBL	k1gMnPc5
<g/>
,	,	kIx,
OK-PBM	OK-PBM	k1gMnPc5
<g/>
,	,	kIx,
OK-KBN	OK-KBN	k1gMnPc5
</s>
<s>
Jakovlev	Jakovlet	k5eAaPmDgInS
Jak-	Jak-	k1gFnPc4
<g/>
40	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-EEA	OK-EEA	k?
<g/>
,	,	kIx,
OK-EEB	OK-EEB	k1gMnPc5
<g/>
,	,	kIx,
OK-EEC	OK-EEC	k1gMnPc5
<g/>
,	,	kIx,
OK-EEG	OK-EEG	k1gMnPc5
<g/>
,	,	kIx,
OK-OFEH	OK-OFEH	k1gMnPc5
</s>
<s>
Junkers	Junkers	k6eAd1
Ju	ju	k0
352A-1	352A-1	k4
</s>
<s>
1	#num#	k4
</s>
<s>
</s>
<s>
1946	#num#	k4
</s>
<s>
</s>
<s>
OK-JUE	OK-JUE	k?
</s>
<s>
Junkers	Junkers	k1gInSc1
Ju-	Ju-	k1gFnSc2
<g/>
52	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
m	m	kA
</s>
<s>
5	#num#	k4
</s>
<s>
1946	#num#	k4
</s>
<s>
1948	#num#	k4
</s>
<s>
</s>
<s>
OK-ODH	OK-ODH	k?
<g/>
,	,	kIx,
OK-PDC	OK-PDC	k1gMnPc5
<g/>
,	,	kIx,
OK-TDI	OK-TDI	k1gMnPc5
<g/>
,	,	kIx,
OK-ZDN	OK-ZDN	k1gMnPc5
<g/>
,	,	kIx,
OK-ZCA	OK-ZCA	k1gMnPc5
</s>
<s>
Saab	Saab	k1gInSc1
340B	340B	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
</s>
<s>
33	#num#	k4
</s>
<s>
Pronajaty	pronajat	k2eAgInPc4d1
od	od	k7c2
CCA	cca	kA
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
od	od	k7c2
Škoda	Škoda	k1gMnSc1
Air	Air	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
)	)	kIx)
</s>
<s>
OK-CCD	OK-CCD	k?
<g/>
,	,	kIx,
CCO	CCO	kA
</s>
<s>
Saro	Saro	k6eAd1
Cloud	Cloud	k1gInSc1
A.	A.	kA
19	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
1939	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Nasazen	nasazen	k2eAgMnSc1d1
na	na	k7c4
první	první	k4xOgFnSc4
mezinárodní	mezinárodní	k2eAgFnSc4d1
linku	linka	k1gFnSc4
ČSA	ČSA	kA
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
(	(	kIx(
<g/>
letní	letní	k2eAgInPc1d1
chartery	charter	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přistávalo	přistávat	k5eAaImAgNnS
na	na	k7c6
moři	moře	k1gNnSc6
u	u	k7c2
resortů	resort	k1gInPc2
</s>
<s>
OK-BAK	OK-BAK	k?
</s>
<s>
Savoia	Savoia	k1gFnSc1
Marchetti	Marchetť	k1gFnSc2
S-73	S-73	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
1937	#num#	k4
</s>
<s>
1940	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
U	u	k7c2
ČSA	ČSA	kA
tyto	tento	k3xDgInPc4
stroje	stroj	k1gInPc4
nalétaly	nalétat	k5eAaBmAgInP,k5eAaPmAgInP,k5eAaImAgInP
5371,07	5371,07	k4
hod	hod	k1gInSc4
a	a	k8xC
1	#num#	k4
233	#num#	k4
302	#num#	k4
km	km	kA
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-BAB	OK-BAB	k?
<g/>
,	,	kIx,
OK-BAC	OK-BAC	k1gMnPc5
<g/>
,	,	kIx,
OK-BAD	OK-BAD	k1gMnPc5
<g/>
,	,	kIx,
OK-BAE	OK-BAE	k1gMnPc5
<g/>
,	,	kIx,
OK-BAF	OK-BAF	k1gMnPc5
<g/>
,	,	kIx,
OK-BAG	OK-BAG	k1gMnPc5
</s>
<s>
Siebel	Siebel	k1gMnSc1
Si	se	k3xPyFc3
204	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
1946	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
</s>
<s>
OK-ZCD	OK-ZCD	k?
<g/>
,	,	kIx,
OK-ACS	OK-ACS	k1gMnSc1
<g/>
,	,	kIx,
OK-ACV	OK-ACV	k1gMnSc1
<g/>
,	,	kIx,
OK-ADF	OK-ADF	k1gMnSc1
a	a	k8xC
OK-ADY	OK-ADY	k1gMnSc1
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gMnPc7
<g/>
104	#num#	k4
<g/>
A	a	k9
</s>
<s>
6	#num#	k4
</s>
<s>
1957	#num#	k4
</s>
<s>
1973	#num#	k4
</s>
<s>
70	#num#	k4
</s>
<s>
Letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
provozovaly	provozovat	k5eAaImAgInP
ČSA	ČSA	kA
pravidelné	pravidelný	k2eAgFnPc4d1
linky	linka	k1gFnPc4
s	s	k7c7
proudovým	proudový	k2eAgInSc7d1
letounem	letoun	k1gInSc7
jako	jako	k9
druzí	druhý	k4xOgMnPc1
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-LDA	OK-LDA	k?
<g/>
,	,	kIx,
OK-LDB	OK-LDB	k1gMnPc5
<g/>
,	,	kIx,
OK-LDC	OK-LDC	k1gMnPc5
<g/>
,	,	kIx,
OK-NDD	OK-NDD	k1gMnPc5
<g/>
,	,	kIx,
OK-MDE	OK-MDE	k1gMnPc5
<g/>
,	,	kIx,
OK-NDF	OK-NDF	k1gMnPc5
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gFnPc1
<g/>
124	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1964	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-TEA	OK-TEA	k?
<g/>
,	,	kIx,
OK-TEB	OK-TEB	k1gMnSc1
<g/>
,	,	kIx,
OK-UEC	OK-UEC	k1gMnSc1
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gMnPc7
<g/>
134	#num#	k4
<g/>
A	a	k9
</s>
<s>
14	#num#	k4
</s>
<s>
1971	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
72	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-AFA	OK-AFA	k?
<g/>
,	,	kIx,
OK-AFB	OK-AFB	k1gMnPc5
<g/>
,	,	kIx,
OK-CFC	OK-CFC	k1gMnPc5
<g/>
,	,	kIx,
OK-CFD	OK-CFD	k1gMnPc5
<g/>
,	,	kIx,
OK-CFE	OK-CFE	k1gMnPc5
<g/>
,	,	kIx,
OK-CFF	OK-CFF	k1gMnPc5
<g/>
,	,	kIx,
OK-CFG	OK-CFG	k1gMnPc5
<g/>
,	,	kIx,
OK-CFH	OK-CFH	k1gMnPc5
<g/>
,	,	kIx,
OK-DFI	OK-DFI	k1gMnPc5
<g/>
,	,	kIx,
OK-EFK	OK-EFK	k1gMnPc5
<g/>
,	,	kIx,
OK-EFJ	OK-EFJ	k1gMnPc5
<g/>
,	,	kIx,
OK-HFL	OK-HFL	k1gMnPc5
<g/>
,	,	kIx,
OK-HFM	OK-HFM	k1gMnPc5
<g/>
,	,	kIx,
OK-IFN	OK-IFN	k1gMnPc5
</s>
<s>
Tupolev	Tupolet	k5eAaPmDgInS
Tu-	Tu-	k1gMnSc2
<g/>
154	#num#	k4
<g/>
M	M	kA
</s>
<s>
7	#num#	k4
</s>
<s>
1988	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
164	#num#	k4
<g/>
/	/	kIx~
<g/>
175	#num#	k4
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
Služba	služba	k1gFnSc1
u	u	k7c2
ČSA	ČSA	kA
<g/>
)	)	kIx)
</s>
<s>
OK-SCA	OK-SCA	k?
<g/>
,	,	kIx,
OK-TCB	OK-TCB	k1gMnPc5
<g/>
,	,	kIx,
OK-TCC	OK-TCC	k1gMnPc5
<g/>
,	,	kIx,
OK-UCE	OK-UCE	k1gMnPc5
<g/>
,	,	kIx,
OK-UCF	OK-UCF	k1gMnPc5
<g/>
,	,	kIx,
OK-VCG	OK-VCG	k1gMnPc5
</s>
<s>
Let	let	k1gInSc1
L-410	L-410	k1gFnSc2
</s>
<s>
12	#num#	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Převedé	Převedý	k2eAgInPc4d1
od	od	k7c2
SlovAiru	SlovAir	k1gInSc2
i	i	k9
s	s	k7c7
posádkami	posádka	k1gFnPc7
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-DDY	OK-DDY	k?
<g/>
,	,	kIx,
OK-DZA	OK-DZA	k1gMnSc1
<g/>
,	,	kIx,
OK-BYF	OK-BYF	k1gMnSc1
<g/>
,	,	kIx,
?	?	kIx.
</s>
<s>
Let	let	k1gInSc1
L-200	L-200	k1gFnSc2
Morava	Morava	k1gFnSc1
</s>
<s>
45	#num#	k4
</s>
<s>
</s>
<s>
</s>
<s>
4	#num#	k4
</s>
<s>
Lety	let	k1gInPc4
L-200	L-200	k1gFnSc2
verzí	verze	k1gFnSc7
200	#num#	k4
<g/>
,	,	kIx,
200A	200A	k4
a	a	k8xC
200D	200D	k4
sloužily	sloužit	k5eAaImAgFnP
pro	pro	k7c4
provozování	provozování	k1gNnSc4
aerotaxi	aerotaxi	k1gNnSc1
pod	pod	k7c7
značkou	značka	k1gFnSc7
Agrolet	Agrolet	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OK-MEA	OK-MEA	k?
<g/>
,	,	kIx,
OK-MEB	OK-MEB	k1gMnPc5
<g/>
,	,	kIx,
OK-MEC	OK-MEC	k1gMnPc5
<g/>
,	,	kIx,
OK-MED	OK-MED	k1gMnPc5
<g/>
,	,	kIx,
OK-MEE	OK-MEE	k1gMnPc5
<g/>
,	,	kIx,
?	?	kIx.
</s>
<s>
Jakolev	Jakolet	k5eAaPmDgInS
Jak-	Jak-	k1gFnSc3
<g/>
42	#num#	k4
</s>
<s>
X	X	kA
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
Objednávky	objednávka	k1gFnPc1
zrušeny	zrušit	k5eAaPmNgFnP
po	po	k7c6
tragické	tragický	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
letu	let	k1gInSc2
Aeroflot	aeroflot	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
způsobenou	způsobený	k2eAgFnSc7d1
konstrukční	konstrukční	k2eAgFnSc7d1
chybou	chyba	k1gFnSc7
</s>
<s>
X	X	kA
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
443	#num#	k4
</s>
<s>
Oprava	oprava	k1gFnSc1
letadel	letadlo	k1gNnPc2
</s>
<s>
Oprava	oprava	k1gFnSc1
letadel	letadlo	k1gNnPc2
Smartwings	Smartwings	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
tedy	tedy	k9
i	i	k9
ČSA	ČSA	kA
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
v	v	k7c6
opravnách	opravna	k1gFnPc6
letadel	letadlo	k1gNnPc2
Avia	Avia	k1gFnSc1
Prime	prim	k1gInSc5
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
hangáry	hangár	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Polsku	Polsko	k1gNnSc6
a	a	k8xC
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
jsou	být	k5eAaImIp3nP
vlastněny	vlastnit	k5eAaImNgFnP
Fondem	fond	k1gInSc7
Hartenberg	Hartenberg	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
majitel	majitel	k1gMnSc1
je	být	k5eAaImIp3nS
svěřenecký	svěřenecký	k2eAgInSc4d1
fond	fond	k1gInSc4
premiéra	premiér	k1gMnSc2
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gMnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Šéf	šéf	k1gMnSc1
ČSA	ČSA	kA
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
placení	placení	k1gNnSc2
jídla	jídlo	k1gNnSc2
přijali	přijmout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letenky	letenka	k1gFnPc4
za	za	k7c4
pár	pár	k1gInSc4
set	set	k1gInSc4
korun	koruna	k1gFnPc2
nechystáme	chystat	k5eNaImIp1nP
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-11	2016-11-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČSA	ČSA	kA
–	–	k?
minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
snad	snad	k9
i	i	k9
budoucnost	budoucnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeroweb	Aerowba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oldest	Oldest	k1gMnSc1
Airlines	Airlines	k1gMnSc1
In	In	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
That	That	k1gMnSc1
Are	ar	k1gInSc5
Still	Still	k1gMnSc1
Operating	Operating	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Worldatlas	Worldatlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
zahájí	zahájit	k5eAaPmIp3nS
v	v	k7c6
nadcházející	nadcházející	k2eAgFnSc6d1
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
provoz	provoz	k1gInSc4
na	na	k7c6
pěti	pět	k4xCc6
nových	nový	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
csa	csa	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smartwings	Smartwings	k1gInSc1
hlásí	hlásit	k5eAaImIp3nS
další	další	k2eAgInSc4d1
růst	růst	k1gInSc4
<g/>
,	,	kIx,
letos	letos	k6eAd1
firma	firma	k1gFnSc1
převezme	převzít	k5eAaPmIp3nS
15	#num#	k4
nových	nový	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-02-01	2019-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
společnosti	společnost	k1gFnSc2
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
a.s.	a.s.	k?
za	za	k7c4
rok	rok	k1gInSc4
2019	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
14	#num#	k4
<g/>
↑	↑	k?
Úvodní	úvodní	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
Cargo	Cargo	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Forbes	forbes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
miliardářů	miliardář	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
↑	↑	k?
Smartwings	Smartwings	k1gInSc1
si	se	k3xPyFc3
půjčily	půjčit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
od	od	k7c2
ČSA	ČSA	kA
přes	přes	k7c4
miliardu	miliarda	k4xCgFnSc4
<g/>
,	,	kIx,
odhalila	odhalit	k5eAaPmAgFnS
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
↑	↑	k?
ČSA	ČSA	kA
jsou	být	k5eAaImIp3nP
v	v	k7c6
úpadku	úpadek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
zadlužené	zadlužený	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
rozhodnou	rozhodnout	k5eAaPmIp3nP
věřitelé	věřitel	k1gMnPc1
<g/>
.	.	kIx.
www.seznamzpravy.cz	www.seznamzpravy.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-10	2021-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Soud	soud	k1gInSc1
poslal	poslat	k5eAaPmAgInS
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
do	do	k7c2
úpadku	úpadek	k1gInSc2
<g/>
,	,	kIx,
správcem	správce	k1gMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Inskol	Inskol	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-10	2021-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
boj	boj	k1gInSc4
se	s	k7c7
škůdcem	škůdce	k1gMnSc7
vyhrávaly	vyhrávat	k5eAaImAgInP
<g/>
,	,	kIx,
teď	teď	k6eAd1
čelí	čelit	k5eAaImIp3nS
úpadku	úpadek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
ČSA	ČSA	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-10-06	2014-10-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Neznámé	známý	k2eNgFnSc2d1
fotografie	fotografia	k1gFnSc2
<g/>
:	:	kIx,
letadlo	letadlo	k1gNnSc4
pomalejší	pomalý	k2eAgNnSc4d2
než	než	k8xS
rychlík	rychlík	k1gInSc4
a	a	k8xC
statečná	statečný	k2eAgFnSc1d1
letuška	letuška	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-11-13	2013-11-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
80	#num#	k4
years	years	k6eAd1
to	ten	k3xDgNnSc4
a	a	k8xC
route	route	k5eAaPmIp2nP
Prague-Moscow	Prague-Moscow	k1gMnPc1
performed	performed	k1gInSc4
by	by	kYmCp3nS
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruaviation	Ruaviation	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Obchodní	obchodní	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
na	na	k7c4
justice	justice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
IČ	IČ	kA
45795908	#num#	k4
<g/>
↑	↑	k?
Revoluce	revoluce	k1gFnSc1
v	v	k7c6
ČSA	ČSA	kA
<g/>
:	:	kIx,
letadlo	letadlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyjíždělo	vyjíždět	k5eAaImAgNnS
z	z	k7c2
dráhy	dráha	k1gFnSc2
a	a	k8xC
přistávalo	přistávat	k5eAaImAgNnS
s	s	k7c7
padákem	padák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-11-02	2017-11-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jak	jak	k8xC,k8xS
Tupolev-	Tupolev-	k1gFnSc1
<g/>
104	#num#	k4
<g/>
A	a	k9
přenesl	přenést	k5eAaPmAgMnS
Československé	československý	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
do	do	k7c2
proudového	proudový	k2eAgInSc2d1
věku	věk	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-02-27	2008-02-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠVEC	Švec	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švící	Švící	k1gFnSc1
-	-	kIx~
historie	historie	k1gFnSc1
ČSA	ČSA	kA
<g/>
.	.	kIx.
svici	svice	k1gMnSc3
<g/>
.	.	kIx.
<g/>
sweb	sweb	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Legendární	legendární	k2eAgNnSc1d1
české	český	k2eAgNnSc1d1
aerotaxi	aerotaxi	k1gNnSc1
vznikalo	vznikat	k5eAaImAgNnS
tajně	tajně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
letadel	letadlo	k1gNnPc2
měli	mít	k5eAaImAgMnP
dělat	dělat	k5eAaImF
hrnce	hrnec	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-10-16	2013-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ako	Ako	k1gMnSc1
sme	sme	k?
prišli	prišle	k1gFnSc4
aj	aj	kA
o	o	k7c6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sme	Sme	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1995-05-03	1995-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠTĚPÁNEK	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělení	dělení	k1gNnSc1
majetku	majetek	k1gInSc2
federace	federace	k1gFnSc2
-	-	kIx~
2	#num#	k4
<g/>
.	.	kIx.
www.petrstepanek.cz	www.petrstepanek.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-07-01	2009-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
ČSA	ČSA	kA
-	-	kIx~
díl	díl	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
1968	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CzechAirliners	CzechAirliners	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
ČSA	ČSA	kA
-	-	kIx~
díl	díl	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CzechAirliners	CzechAirliners	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČSA	ČSA	kA
koupí	koupit	k5eAaPmIp3nS
letadla	letadlo	k1gNnPc4
pro	pro	k7c4
regionální	regionální	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2001-06-18	2001-06-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČSA	ČSA	kA
vloni	vloni	k6eAd1
přepravily	přepravit	k5eAaPmAgFnP
5,2	5,2	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airways	Airways	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
2008	#num#	k4
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Šimáně	Šimán	k1gInSc6
<g/>
:	:	kIx,
Zisk	zisk	k1gInSc4
tvoříme	tvořit	k5eAaImIp1nP
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
trh	trh	k1gInSc1
je	být	k5eAaImIp3nS
deformovaný	deformovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
E	E	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Nabídli	nabídnout	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
luxusní	luxusní	k2eAgFnSc4d1
jednu	jeden	k4xCgFnSc4
miliardu	miliarda	k4xCgFnSc4
při	pře	k1gFnSc4
vyrovnání	vyrovnání	k1gNnSc2
vlastního	vlastní	k2eAgNnSc2d1
jmění	jmění	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
částka	částka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
prohospodařily	prohospodařit	k5eAaPmAgInP
nejméně	málo	k6eAd3
třikrát	třikrát	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
:	:	kIx,
Každý	každý	k3xTgInSc1
další	další	k2eAgInSc1d1
rok	rok	k1gInSc1
fungování	fungování	k1gNnSc2
ČSA	ČSA	kA
je	být	k5eAaImIp3nS
pro	pro	k7c4
Ruzyni	Ruzyně	k1gFnSc4
přínosem	přínos	k1gInSc7
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
E	E	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
titulní	titulní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Důvody	důvod	k1gInPc4
(	(	kIx(
<g/>
pro	pro	k7c4
vznik	vznik	k1gInSc4
Českého	český	k2eAgInSc2d1
aeroholdingu	aeroholding	k1gInSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
v	v	k7c6
zásadě	zásada	k1gFnSc6
tři	tři	k4xCgFnPc4
<g/>
.	.	kIx.
...	...	k?
Za	za	k7c4
druhé	druhý	k4xOgMnPc4
šlo	jít	k5eAaImAgNnS
samozřejmě	samozřejmě	k6eAd1
o	o	k7c4
získání	získání	k1gNnSc4
prostředků	prostředek	k1gInPc2
pro	pro	k7c4
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenkrát	tenkrát	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
mediální	mediální	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
nabídl	nabídnout	k5eAaPmAgInS
za	za	k7c4
ČSA	ČSA	kA
jednu	jeden	k4xCgFnSc4
miliardu	miliarda	k4xCgFnSc4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
státu	stát	k1gInSc2
by	by	kYmCp3nS
ale	ale	k9
byl	být	k5eAaImAgInS
záporný	záporný	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
měl	mít	k5eAaImAgInS
několik	několik	k4yIc4
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
chtěl	chtít	k5eAaImAgMnS
dorovnat	dorovnat	k5eAaPmF
záporný	záporný	k2eAgInSc4d1
vlastní	vlastní	k2eAgInSc4d1
kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
by	by	kYmCp3nS
tak	tak	k9
zaplatil	zaplatit	k5eAaPmAgInS
několik	několik	k4yIc4
miliard	miliarda	k4xCgFnPc2
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
by	by	kYmCp3nS
jednu	jeden	k4xCgFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
↑	↑	k?
Vláda	vláda	k1gFnSc1
dá	dát	k5eAaPmIp3nS
na	na	k7c4
odstranění	odstranění	k1gNnSc4
ekologických	ekologický	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
po	po	k7c6
ČSA	ČSA	kA
27	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
↑	↑	k?
Airbus	airbus	k1gInSc1
A310	A310	k1gMnPc3
uskutečnil	uskutečnit	k5eAaPmAgInS
nejdelší	dlouhý	k2eAgInSc4d3
let	let	k1gInSc4
za	za	k7c2
existence	existence	k1gFnSc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-12-31	2008-12-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Finanční	finanční	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
dokončil	dokončit	k5eAaPmAgMnS
koupi	koupě	k1gFnSc4
34	#num#	k4
procent	procento	k1gNnPc2
akcií	akcie	k1gFnPc2
ČSA	ČSA	kA
<g/>
,	,	kIx,
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
↑	↑	k?
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O.	O.	kA
<g/>
,	,	kIx,
Ozzy	Ozza	k1gFnSc2
<g/>
,	,	kIx,
<g/>
eif	eif	k?
<g/>
,	,	kIx,
<g/>
Avito	Avito	k1gNnSc1
<g/>
.	.	kIx.
planes	planes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Novinka	novinka	k1gFnSc1
-	-	kIx~
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
vyřadily	vyřadit	k5eAaPmAgFnP
poslední	poslední	k2eAgInSc4d1
Boeing	boeing	k1gInSc4
737	#num#	k4
<g/>
-	-	kIx~
<g/>
400	#num#	k4
<g/>
.	.	kIx.
www.planes.cz	www.planes.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
letošních	letošní	k2eAgFnPc6d1
letních	letní	k2eAgFnPc6d1
prázdninách	prázdniny	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
A330	A330	k1gFnSc6
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
www.facebook.com	www.facebook.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MYŠKA	Myška	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
90	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
vzniku	vznik	k1gInSc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
ivao	ivao	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tri	Tri	k1gFnSc1
roky	rok	k1gInPc4
s	s	k7c7
ČSA	ČSA	kA
na	na	k7c6
letisku	letisek	k1gInSc6
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
airtiper	airtiper	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČSA	ČSA	kA
prodá	prodat	k5eAaPmIp3nS
všechny	všechen	k3xTgInPc4
své	svůj	k3xOyFgInPc4
Airbusy	airbus	k1gInPc4
A	a	k9
<g/>
320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc4
estránky	estránka	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-08-05	2013-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vážný	vážný	k2eAgInSc4d1
incident	incident	k1gInSc4
letounu	letoun	k1gInSc2
A	a	k9
319	#num#	k4
–	–	k?
112	#num#	k4
«	«	k?
A	a	k8xC
e	e	k0
r	r	kA
o	o	k7c4
n	n	k0
o	o	k7c4
v	v	k7c6
i	i	k9
n	n	k0
y	y	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČSA	ČSA	kA
vloni	vloni	k6eAd1
vzrostl	vzrůst	k5eAaPmAgInS
meziročně	meziročně	k6eAd1
zisk	zisk	k1gInSc1
o	o	k7c4
8	#num#	k4
procent	procento	k1gNnPc2
na	na	k7c4
241	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2017-03-06	2017-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zisk	zisk	k1gInSc1
ČSA	ČSA	kA
loni	loni	k6eAd1
stoupl	stoupnout	k5eAaPmAgInS
na	na	k7c4
241	#num#	k4
miliónů	milión	k4xCgInPc2
korun	koruna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-03-06	2017-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
škrtly	škrtnout	k5eAaPmAgFnP
objednávku	objednávka	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
nových	nový	k2eAgInPc2d1
airbusů	airbus	k1gInPc2
A	a	k9
<g/>
320	#num#	k4
<g/>
neo	neo	k?
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
koupit	koupit	k5eAaPmF
jen	jen	k9
tři	tři	k4xCgMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
chystají	chystat	k5eAaImIp3nP
návrat	návrat	k1gInSc4
k	k	k7c3
letům	let	k1gInPc3
přes	přes	k7c4
Atlantik	Atlantik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A321XLR	A321XLR	k1gMnSc1
umožní	umožnit	k5eAaPmIp3nS
lety	léto	k1gNnPc7
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-10-24	2019-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
společnosti	společnost	k1gFnSc2
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2017	#num#	k4
<g/>
↑	↑	k?
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
ovládne	ovládnout	k5eAaPmIp3nS
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc2
ovládne	ovládnout	k5eAaPmIp3nS
ČSA	ČSA	kA
<g/>
,	,	kIx,
vykoupí	vykoupit	k5eAaPmIp3nS
podíl	podíl	k1gInSc1
Korean	Koreany	k1gInPc2
Air	Air	k1gFnSc2
i	i	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-06	2017-10-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Státní	státní	k2eAgInPc1d1
podniky	podnik	k1gInPc1
si	se	k3xPyFc3
přehodily	přehodit	k5eAaPmAgInP
pětinový	pětinový	k2eAgInSc4d1
podíl	podíl	k1gInSc4
v	v	k7c6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
novou	nový	k2eAgFnSc4d1
kolektivní	kolektivní	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-11-09	2015-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
končí	končit	k5eAaImIp3nS
s	s	k7c7
A	a	k9
<g/>
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půjčí	půjčit	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
Eurowings	Eurowings	k1gInSc1
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nS
leasingovým	leasingový	k2eAgFnPc3d1
firmám	firma	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradí	nahradit	k5eAaPmIp3nS
je	on	k3xPp3gInPc4
boeingy	boeing	k1gInPc4
737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
2019	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justice	justice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-18	2020-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
znovu	znovu	k6eAd1
A320	A320	k1gFnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
barvách	barva	k1gFnPc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
upravené	upravený	k2eAgNnSc1d1
„	„	k?
<g/>
trsátko	trsátko	k1gNnSc1
<g/>
“	“	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-04-10	2020-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
ruší	rušit	k5eAaImIp3nS
s	s	k7c7
okamžitou	okamžitý	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
lety	let	k1gInPc4
mezi	mezi	k7c7
Prahou	Praha	k1gFnSc7
a	a	k8xC
Soulem	Soul	k1gInSc7
<g/>
,	,	kIx,
Korean	Korean	k1gMnSc1
Air	Air	k1gMnSc1
na	na	k7c6
trase	trasa	k1gFnSc6
zůstává	zůstávat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-02-27	2020-02-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
a	a	k8xC
Smartwings	Smartwings	k1gInSc4
postupně	postupně	k6eAd1
do	do	k7c2
pondělí	pondělí	k1gNnSc2
zastaví	zastavit	k5eAaPmIp3nS
provoz	provoz	k1gInSc1
letů	let	k1gInPc2
z	z	k7c2
Česka	Česko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-03-13	2020-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-25	2020-06-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-31	2020-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Smartwings	Smartwings	k1gInSc1
a	a	k8xC
ČSA	ČSA	kA
požádaly	požádat	k5eAaPmAgFnP
o	o	k7c4
dluhové	dluhový	k2eAgNnSc4d1
moratorium	moratorium	k1gNnSc4
<g/>
↑	↑	k?
Soud	soud	k1gInSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
mimořádné	mimořádný	k2eAgNnSc4d1
moratorium	moratorium	k1gNnSc4
na	na	k7c6
CSA	CSA	kA
i	i	k9
Smartwings	Smartwings	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
https://zdopravy.cz/soud-vyhlasil-mimoradne-moratorium-na-csa-i-smartwings-58862/	https://zdopravy.cz/soud-vyhlasil-mimoradne-moratorium-na-csa-i-smartwings-58862/	k4
<g/>
↑	↑	k?
ČSA	ČSA	kA
a	a	k8xC
Smartwings	Smartwings	k1gInSc1
vyhlásily	vyhlásit	k5eAaPmAgInP
finanční	finanční	k2eAgFnSc4d1
moratorium	moratorium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zrušené	zrušený	k2eAgInPc4d1
lety	let	k1gInPc4
neposkytnou	poskytnout	k5eNaPmIp3nP
refundaci	refundace	k1gFnSc4
<g/>
.	.	kIx.
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-26	2020-11-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Smartwings	Smartwings	k1gInSc1
si	se	k3xPyFc3
půjčily	půjčit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
od	od	k7c2
ČSA	ČSA	kA
přes	přes	k7c4
miliardu	miliarda	k4xCgFnSc4
<g/>
,	,	kIx,
odhalila	odhalit	k5eAaPmAgFnS
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
↑	↑	k?
Škody	škoda	k1gFnSc2
Smartwings	Smartwingsa	k1gFnPc2
narůstají	narůstat	k5eAaImIp3nP
<g/>
,	,	kIx,
Boeing	boeing	k1gInSc1
opět	opět	k6eAd1
odkládá	odkládat	k5eAaImIp3nS
návrat	návrat	k1gInSc1
Maxů	Max	k1gMnPc2
do	do	k7c2
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FAEI	FAEI	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-22	2020-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-08	2021-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-23	2021-02-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Insolvenční	Insolvenční	k2eAgInSc4d1
návrh	návrh	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-24	2021-02-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Detail	detail	k1gInSc1
insolvenčního	insolvenční	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
.	.	kIx.
isir	isir	k1gMnSc1
<g/>
.	.	kIx.
<g/>
justice	justice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
<g/>
↑	↑	k?
Marcela	Marcela	k1gFnSc1
Alföldi	Alföld	k1gMnPc1
Šperkerová	Šperkerový	k2eAgNnPc4d1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Štětka	Štětka	k1gMnSc1
<g/>
,	,	kIx,
Operace	operace	k1gFnSc1
OK	oka	k1gFnPc2
<g/>
:	:	kIx,
pacient	pacient	k1gMnSc1
umírá	umírat	k5eAaImIp3nS
<g/>
,	,	kIx,
EKONOM	ekonom	k1gMnSc1
</s>
<s>
<g/>
<g/>
IHNED	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
2003	#num#	k4
<g/>
,	,	kIx,
úvodní	úvodní	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
předsedy	předseda	k1gMnSc2
představenstva	představenstvo	k1gNnSc2
a	a	k8xC
prezidenta	prezident	k1gMnSc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
Jaroslava	Jaroslav	k1gMnSc2
Tvrdíka	Tvrdík	k1gMnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
"	"	kIx"
<g/>
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
dojde	dojít	k5eAaPmIp3nS
</s>
<s>
k	k	k7c3
bezprecedentnímu	bezprecedentní	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
přepravní	přepravní	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
a	a	k8xC
str	str	kA
<g/>
.	.	kIx.
57	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
listopadu	listopad	k1gInSc6
2003	#num#	k4
podepsány	podepsán	k2eAgFnPc1d1
kolektivní	kolektivní	k2eAgFnPc1d1
smlouvy	smlouva	k1gFnPc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Marek	Marek	k1gMnSc1
Pražák	Pražák	k1gMnSc1
<g/>
,	,	kIx,
ČSA	ČSA	kA
se	se	k3xPyFc4
pouštějí	pouštět	k5eAaImIp3nP
do	do	k7c2
odvážné	odvážný	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
DNES	dnes	k6eAd1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
druhá	druhý	k4xOgFnSc1
strana	strana	k1gFnSc1
sešitu	sešit	k1gInSc2
Ekonomika	ekonomika	k1gFnSc1
(	(	kIx(
<g/>
průměrná	průměrný	k2eAgFnSc1d1
mzda	mzda	k1gFnSc1
v	v	k7c6
ČSA	ČSA	kA
má	mít	k5eAaImIp3nS
z	z	k7c2
loňských	loňský	k2eAgInPc2d1
33	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
měsíčně	měsíčně	k6eAd1
vzrůst	vzrůst	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
na	na	k7c4
45	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Výroční	výroční	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
ČSA	ČSA	kA
za	za	k7c4
kalendářní	kalendářní	k2eAgInPc4d1
roky	rok	k1gInPc4
1997	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
↑	↑	k?
ČSA	ČSA	kA
létají	létat	k5eAaImIp3nP
ziskově	ziskově	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
hospodaření	hospodaření	k1gNnSc2
ještě	ještě	k6eAd1
zlepšily	zlepšit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
zvýšily	zvýšit	k5eAaPmAgFnP
zisk	zisk	k1gInSc4
na	na	k7c4
čtvrt	čtvrt	k1xP,k1gFnSc1,k1gFnSc4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
více	hodně	k6eAd2
obsazená	obsazený	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-03-06	2017-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
2008	#num#	k4
ČSA	ČSA	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
as	as	k9
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
1999	#num#	k4
<g/>
.	.	kIx.
doczz	doczza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČSA	ČSA	kA
přepravily	přepravit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
téměř	téměř	k6eAd1
5,5	5,5	k4
milionu	milion	k4xCgInSc2
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airways	Airways	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
loni	loni	k6eAd1
klesl	klesnout	k5eAaPmAgInS
počet	počet	k1gInSc1
cestujících	cestující	k1gMnPc2
o	o	k7c4
7,5	7,5	k4
procenta	procento	k1gNnSc2
<g/>
,	,	kIx,
letos	letos	k6eAd1
cílí	cílit	k5eAaImIp3nS
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-01-19	2012-01-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
vydělaly	vydělat	k5eAaPmAgFnP
skoro	skoro	k6eAd1
čtvrt	čtvrt	k1xP,k1gFnSc4,k1gFnSc1
miliardy	miliarda	k4xCgFnSc2
díky	díky	k7c3
levné	levný	k2eAgFnSc3d1
ropě	ropa	k1gFnSc3
i	i	k8xC
zeštíhlení	zeštíhlení	k1gNnSc3
flotily	flotila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnSc2
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČSA	ČSA	kA
oslovují	oslovovat	k5eAaImIp3nP
potencionální	potencionální	k2eAgMnPc4d1
zájemce	zájemce	k1gMnPc4
o	o	k7c4
Cargo	Cargo	k1gNnSc4
terminál	terminál	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
,	,	kIx,
2006-10-24	2006-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Babiš	Babiš	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
propuštění	propuštění	k1gNnSc4
v	v	k7c6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
o	o	k7c6
navýšení	navýšení	k1gNnSc6
kapitálu	kapitál	k1gInSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
PETRMAN	PETRMAN	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSA	ČSA	kA
–	–	k?
nejstarší	starý	k2eAgFnSc1d3
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
českého	český	k2eAgNnSc2d1
nebe	nebe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztlak	vztlak	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-06-26	2009-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
-	-	kIx~
význam	význam	k1gInSc4
zkratky	zkratka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkratky	zkratka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
má	mít	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
logo	logo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
starému	starý	k1gMnSc3
žalobu	žaloba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Spor	spor	k1gInSc1
o	o	k7c4
logo	logo	k1gNnSc4
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jak	jak	k6eAd1
se	se	k3xPyFc4
vám	vy	k3xPp2nPc3
líbí	líbit	k5eAaImIp3nP
nové	nový	k2eAgNnSc4d1
logo	logo	k1gNnSc4
ČSA	ČSA	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Design	design	k1gInSc1
portál	portál	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
originální	originální	k2eAgInSc4d1
boeing	boeing	k1gInSc4
v	v	k7c6
retrostylu	retrostyl	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
Ruzyni	Ruzyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MAŤO	MAŤO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
OK-DGM	OK-DGM	k1gMnSc1
-	-	kIx~
B737-45S	B737-45S	k1gMnSc1
-	-	kIx~
Southend	Southend	k1gMnSc1
<g/>
.	.	kIx.
planes	planes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Letadla	letadlo	k1gNnSc2
ČSA	ČSA	kA
mění	měnit	k5eAaImIp3nS
tvář	tvář	k1gFnSc4
<g/>
:	:	kIx,
trojúhelníky	trojúhelník	k1gInPc7
střídá	střídat	k5eAaImIp3nS
diamant	diamant	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-09-14	2007-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
-	-	kIx~
Nové	Nové	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
již	již	k6eAd1
nese	nést	k5eAaImIp3nS
16	#num#	k4
letounů	letoun	k1gInPc2
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-11-21	2008-11-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Čtyři	čtyři	k4xCgFnPc1
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgFnPc1
uniformy	uniforma	k1gFnPc1
letušek	letuška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyberte	vybrat	k5eAaPmRp2nP
tu	tu	k6eAd1
nejstylovější	stylový	k2eAgInSc4d3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-13	2014-11-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Magazín	magazín	k1gInSc1
flyOK	flyOK	k?
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hladolety	hladolet	k1gMnPc7
se	se	k3xPyFc4
rozšiřují	rozšiřovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jídlo	jídlo	k1gNnSc1
a	a	k8xC
pití	pití	k1gNnSc1
zdarma	zdarma	k6eAd1
zruší	zrušit	k5eAaPmIp3nS
také	také	k9
ČSA	ČSA	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-22	2015-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zavazadla	zavazadlo	k1gNnSc2
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VAŠUT	VAŠUT	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OK-PET	OK-PET	k1gFnSc1
-	-	kIx~
A319-112	A319-112	k1gFnSc1
-	-	kIx~
Praha	Praha	k1gFnSc1
-	-	kIx~
Ruzyně	Ruzyně	k1gFnSc1
(	(	kIx(
<g/>
PRG	PRG	kA
<g/>
/	/	kIx~
<g/>
LKPR	LKPR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
planes	planes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
mezi	mezi	k7c7
nejhoršími	zlý	k2eAgMnPc7d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc4d1
žebříček	žebříček	k1gInSc4
aerolinek	aerolinka	k1gFnPc2
dle	dle	k7c2
zákaznického	zákaznický	k2eAgInSc2d1
servisu	servis	k1gInSc2
není	být	k5eNaImIp3nS
lichotivý	lichotivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
.1	.1	k4
2	#num#	k4
ČSA	ČSA	kA
založily	založit	k5eAaPmAgFnP
dceřiné	dceřiný	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
Air	Air	k1gMnSc1
Cargo	Cargo	k1gMnSc1
Terminal	Terminal	k1gMnSc1
a	a	k8xC
Air	Air	k1gMnSc1
Czech	Czech	k1gMnSc1
Catering	Catering	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FinExpert	FinExpert	k1gInSc1
<g/>
.	.	kIx.
<g/>
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČSA	ČSA	kA
založily	založit	k5eAaPmAgFnP
dceřiné	dceřiný	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
Air	Air	k1gMnSc1
Cargo	Cargo	k1gMnSc1
Terminal	Terminal	k1gMnSc1
a	a	k8xC
Air	Air	k1gMnSc1
Czech	Czech	k1gMnSc1
Catering	Catering	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Air	Air	k1gMnSc1
Cargo	Cargo	k1gMnSc1
Terminal	Terminal	k1gMnSc1
a.s.	a.s.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Air	Air	k1gFnSc1
Czech	Czech	k1gMnSc1
Catering	Catering	k1gInSc1
a.s.	a.s.	k?
<g/>
.	.	kIx.
www.bloomberg.com	www.bloomberg.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Za	za	k7c4
firmu	firma	k1gFnSc4
Air	Air	k1gFnSc2
Czech	Czecha	k1gFnPc2
Catering	Catering	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
stravovací	stravovací	k2eAgFnPc4d1
služby	služba	k1gFnPc4
v	v	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
,	,	kIx,
dostanou	dostat	k5eAaPmIp3nP
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
682	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patria	Patrium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Click	Click	k1gInSc1
<g/>
4	#num#	k4
<g/>
Sky	Sky	k1gFnSc2
–	–	k?
hra	hra	k1gFnSc1
na	na	k7c4
lowcost	lowcost	k1gInSc4
–	–	k?
FinExpert	FinExpert	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
FinExpert	FinExpert	k1gInSc1
<g/>
.	.	kIx.
<g/>
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČSA	ČSA	kA
Support	support	k1gInSc1
přejmenována	přejmenován	k2eAgFnSc1d1
<g/>
:	:	kIx,
Czech	Czech	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
Handling	Handling	k1gInSc1
lépe	dobře	k6eAd2
vystihuje	vystihovat	k5eAaImIp3nS
aktivity	aktivita	k1gFnPc4
společnostiČeské	společnostiČeský	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-02-26	2010-02-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
prodaly	prodat	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc4
dceřiné	dceřiný	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
Českému	český	k2eAgInSc3d1
aeroholdingu	aeroholding	k1gInSc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-12-07	2011-12-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c4
nás	my	k3xPp1nPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Airlines	Airlines	k1gInSc4
Technics	Technics	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Aeroholding	Aeroholding	k1gInSc1
převzal	převzít	k5eAaPmAgInS
údržbu	údržba	k1gFnSc4
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
miliardu	miliarda	k4xCgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnSc2
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
CSA	CSA	kA
Services	Services	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linkedin	Linkedina	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
jsme	být	k5eAaImIp1nP
a	a	k8xC
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
děláme	dělat	k5eAaImIp1nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Czech	Czech	k1gInSc1
Aviation	Aviation	k1gInSc1
Training	Training	k1gInSc1
Centre	centr	k1gInSc5
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Další	další	k2eAgInPc1d1
půlmiliarda	půlmiliarda	k4xCgFnSc1
pro	pro	k7c4
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chartery	Charter	k1gInPc1
prodaly	prodat	k5eAaPmAgFnP
aeroholdingu	aeroholding	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-01-30	2012-01-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c4
převzetí	převzetí	k1gNnSc4
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
zájem	zájem	k1gInSc1
Qatar	Qatar	k1gMnSc1
Airways	Airways	k1gInSc1
a	a	k8xC
Korean	Korean	k1gInSc1
Air	Air	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-12-05	2012-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HOLIDAYS	HOLIDAYS	kA
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
.	.	kIx.
penize	penize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sinčák	Sinčák	k1gInSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
ČSA	ČSA	kA
<g/>
,	,	kIx,
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
mu	on	k3xPp3gMnSc3
neprodloužil	prodloužit	k5eNaPmAgMnS
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Veřejný	veřejný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
a	a	k8xC
Sbírka	sbírka	k1gFnSc1
listin	listina	k1gFnPc2
-	-	kIx~
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
or	or	k?
<g/>
.	.	kIx.
<g/>
justice	justice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kam	kam	k6eAd1
létáme	létat	k5eAaImIp1nP
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
a	a	k8xC
Air	Air	k1gFnSc1
Seychelles	Seychellesa	k1gFnPc2
se	se	k3xPyFc4
dohodly	dohodnout	k5eAaPmAgInP
na	na	k7c6
code-sharingu	code-sharing	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Airways	Airways	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vybrané	vybraný	k2eAgInPc4d1
lety	let	k1gInPc4
ČSA	ČSA	kA
budou	být	k5eAaImBp3nP
létat	létat	k5eAaImF
i	i	k9
s	s	k7c7
kódy	kód	k1gInPc7
China	China	k1gFnSc1
Eastern	Eastern	k1gMnSc1
<g/>
.	.	kIx.
planes	planes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-01	2017-05-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Průlom	průlom	k1gInSc1
<g/>
:	:	kIx,
MEA	MEA	kA
/	/	kIx~
CSA	CSA	kA
begins	begins	k6eAd1
codeshare	codeshar	k1gMnSc5
partnership	partnership	k1gInSc1
from	from	k1gInSc1
June	jun	k1gMnSc5
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Routesonline	Routesonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Průlom	průlom	k1gInSc1
<g/>
:	:	kIx,
ČSA	ČSA	kA
a	a	k8xC
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc2
budou	být	k5eAaImBp3nP
poprvé	poprvé	k6eAd1
sdílet	sdílet	k5eAaImF
své	svůj	k3xOyFgInPc4
lety	let	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-06-26	2013-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Exim	Exim	k1gInSc1
Tours	Tours	k1gInSc1
s	s	k7c7
ČSA	ČSA	kA
a	a	k8xC
obratem	obratem	k6eAd1
dvou	dva	k4xCgFnPc2
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
e-vsudybyl	e-vsudybyl	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
loni	loni	k6eAd1
vzrostl	vzrůst	k5eAaPmAgInS
zisk	zisk	k1gInSc1
čtyřnásobně	čtyřnásobně	k6eAd1
324	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
finance	finance	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
budou	být	k5eAaImBp3nP
létat	létat	k5eAaImF
pro	pro	k7c4
Exim	Exim	k1gInSc4
Tours	Tours	k1gInSc4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
www.airways.cz	www.airways.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
:	:	kIx,
Exim	Exim	k1gInSc1
Tours	Toursa	k1gFnPc2
odchází	odcházet	k5eAaImIp3nS
od	od	k7c2
ČSA	ČSA	kA
k	k	k7c3
Travel	Travel	k1gInSc1
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-05-18	2010-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CK	CK	kA
Exim	Exim	k1gInSc1
Tours	Tours	k1gInSc1
obnoví	obnovit	k5eAaPmIp3nS
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
ČSA	ČSA	kA
<g/>
↑	↑	k?
Holidays	Holidays	k1gInSc1
Czech	Czech	k1gMnSc1
Airlines	Airlinesa	k1gFnPc2
budou	být	k5eAaImBp3nP
létat	létat	k5eAaImF
pro	pro	k7c4
Exim	Exim	k1gInSc4
Tours	Toursa	k1gFnPc2
<g/>
↑	↑	k?
Šéf	šéf	k1gMnSc1
ČSA	ČSA	kA
provdal	provdat	k5eAaPmAgInS
„	„	k?
<g/>
dceru	dcera	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
charterová	charterový	k2eAgFnSc1d1
firma	firma	k1gFnSc1
bude	být	k5eAaImBp3nS
létat	létat	k5eAaImF
pro	pro	k7c4
Blue	Blue	k1gFnSc4
Style	styl	k1gInSc5
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-11-18	2010-11-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c4
převzetí	převzetí	k1gNnSc4
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
zájem	zájem	k1gInSc1
Qatar	Qatar	k1gMnSc1
Airways	Airways	k1gInSc1
a	a	k8xC
Korean	Korean	k1gInSc1
Air	Air	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-12-05	2012-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Letecký	letecký	k2eAgInSc1d1
obrat	obrat	k1gInSc1
dokonán	dokonat	k5eAaPmNgInS
<g/>
:	:	kIx,
chartery	charter	k1gInPc7
po	po	k7c6
ČSA	ČSA	kA
létají	létat	k5eAaImIp3nP
v	v	k7c6
barvách	barva	k1gFnPc6
Travel	Travel	k1gMnSc1
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-07	2013-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Profil	profil	k1gInSc1
společnosti	společnost	k1gFnSc2
–	–	k?
O	o	k7c4
nás	my	k3xPp1nPc4
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
CSA	CSA	kA
<g/>
)	)	kIx)
Fleet	Fleet	k1gMnSc1
Details	Detailsa	k1gFnPc2
and	and	k?
History	Histor	k1gInPc4
<g/>
.	.	kIx.
www.planespotters.net	www.planespotters.net	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Letadlová	letadlový	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
pošlou	poslat	k5eAaPmIp3nP
do	do	k7c2
Německa	Německo	k1gNnSc2
další	další	k2eAgInSc4d1
airbus	airbus	k1gInSc4
<g/>
,	,	kIx,
Eurowings	Eurowings	k1gInSc1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
po	po	k7c6
pádu	pád	k1gInSc6
Air	Air	k1gFnSc2
Berlin	berlina	k1gFnPc2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
zdopravy	zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-28	2017-12-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-31	2020-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-08	2021-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Letecký	letecký	k2eAgInSc1d1
rejtřík	rejtřík	k1gInSc1
<g/>
↑	↑	k?
Letecký	letecký	k2eAgInSc1d1
rejtřík	rejtřík	k1gInSc1
<g/>
↑	↑	k?
Letadlový	letadlový	k2eAgInSc1d1
park	park	k1gInSc1
ČSA	ČSA	kA
<g/>
.	.	kIx.
planes	planes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Švící	Švící	k1gMnSc1
–	–	k?
historie	historie	k1gFnSc2
ČSA	ČSA	kA
<g/>
.	.	kIx.
svici	svice	k1gMnSc3
<g/>
.	.	kIx.
<g/>
sweb	sweb	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O.	O.	kA
<g/>
,	,	kIx,
AARON	AARON	kA
GROUP	GROUP	kA
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
5	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
-	-	kIx~
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
rozšířily	rozšířit	k5eAaPmAgFnP
flotilu	flotila	k1gFnSc4
o	o	k7c4
čtvrtý	čtvrtý	k4xOgInSc4
dálkový	dálkový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Airbus	airbus	k1gInSc4
A310	A310	k1gFnPc2
–	–	k?
Tiskové	tiskový	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
archiv	archiv	k1gInSc4
–	–	k?
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
<g/>
.	.	kIx.
www.csa.cz	www.csa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Airbus	airbus	k1gInSc1
A	a	k9
<g/>
310	#num#	k4
<g/>
.	.	kIx.
www.cs-letectvi.cz	www.cs-letectvi.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
posílí	posílit	k5eAaPmIp3nS
kapacitu	kapacita	k1gFnSc4
<g/>
,	,	kIx,
půjčují	půjčovat	k5eAaImIp3nP
si	se	k3xPyFc3
dvacetiletý	dvacetiletý	k2eAgInSc4d1
Airbus	airbus	k1gInSc4
A321	A321	k1gFnSc2
i	i	k8xC
s	s	k7c7
piloty	pilot	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MILHAUS	MILHAUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
I-ADLW	I-ADLW	k1gFnSc1
-	-	kIx~
ATR72-212A	ATR72-212A	k1gFnSc1
-	-	kIx~
Praha	Praha	k1gFnSc1
-	-	kIx~
Ruzyně	Ruzyně	k1gFnSc1
(	(	kIx(
<g/>
PRG	PRG	kA
<g/>
/	/	kIx~
<g/>
LKPR	LKPR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
planes	planes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSA	ČSA	kA
mají	mít	k5eAaImIp3nP
půjčený	půjčený	k2eAgMnSc1d1
ze	z	k7c2
Slovenska	Slovensko	k1gNnSc2
boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yIgNnSc7,k3yRgNnSc7,k3yQgNnSc7
dříve	dříve	k6eAd2
létaly	létat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednání	jednání	k1gNnSc1
o	o	k7c4
ukrajinské	ukrajinský	k2eAgFnPc4d1
A321	A321	k1gFnPc4
padla	padnout	k5eAaPmAgNnP,k5eAaImAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdopravy	Zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tupolev	Tupolev	k1gMnPc4
Tu-	Tu-	k1gFnSc2
<g/>
154	#num#	k4
<g/>
M	M	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Planes	Planes	k1gInSc1
<g/>
.	.	kIx.
<g/>
axlegeeks	axlegeeks	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Legendární	legendární	k2eAgFnPc4d1
L-410	L-410	k1gFnPc4
Turbolet	Turbolet	k1gInSc1
létá	létat	k5eAaImIp3nS
již	již	k6eAd1
45	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilotinfo	Pilotinfo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Babišův	Babišův	k2eAgInSc1d1
fond	fond	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
polovinu	polovina	k1gFnSc4
polských	polský	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
opraven	opravna	k1gFnPc2
Avia	Avia	k1gFnSc1
Prime	prim	k1gInSc5
<g/>
↑	↑	k?
Babišův	Babišův	k2eAgInSc1d1
fond	fond	k1gInSc1
koupil	koupit	k5eAaPmAgInS
polovinu	polovina	k1gFnSc4
leteckých	letecký	k2eAgFnPc2d1
opraven	opravna	k1gFnPc2
Avia	Avia	k1gFnSc1
Prime	prim	k1gInSc5
<g/>
↑	↑	k?
Stát	stát	k1gInSc1
chce	chtít	k5eAaImIp3nS
koupit	koupit	k5eAaPmF
ČSA	ČSA	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
servisuje	servisovat	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
premiérových	premiérový	k2eAgFnPc2d1
firem	firma	k1gFnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Smartwings	Smartwings	k1gInSc1
a.s.	a.s.	k?
–	–	k?
vlastník	vlastník	k1gMnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
</s>
<s>
Destinace	destinace	k1gFnSc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
České	český	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Flotila	flotila	k1gFnSc1
ČSA	ČSA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Fotografie	fotografie	k1gFnSc1
letadel	letadlo	k1gNnPc2
ČSA	ČSA	kA
na	na	k7c6
Planes	Planesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Historické	historický	k2eAgInPc1d1
letové	letový	k2eAgInPc1d1
řády	řád	k1gInPc1
ČSA	ČSA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1
nehody	nehoda	k1gFnPc1
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
</s>
<s>
let	let	k1gInSc1
ČSA	ČSA	kA
001	#num#	k4
</s>
<s>
let	let	k1gInSc1
ČSA	ČSA	kA
511	#num#	k4
</s>
<s>
let	let	k1gInSc1
ČSA	ČSA	kA
523	#num#	k4
</s>
<s>
let	let	k1gInSc1
ČSA	ČSA	kA
540	#num#	k4
</s>
<s>
let	let	k1gInSc1
ČSA	ČSA	kA
584	#num#	k4
Ředitelé	ředitel	k1gMnPc1
</s>
<s>
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Churain	Churain	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Fiker	Fiker	k1gMnSc1
</s>
<s>
Georges	Georges	k1gMnSc1
Vejdovský	Vejdovský	k2eAgMnSc1d1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Jakubše	Jakubše	k1gFnSc2
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Kůla	Kůla	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Tvrdík	Tvrdík	k1gMnSc1
</s>
<s>
Radomír	Radomír	k1gMnSc1
Lašák	Lašák	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
</s>
<s>
Philippe	Philippat	k5eAaPmIp3nS
Moreels	Moreels	k1gInSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Sinčák	Sinčák	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
destinace	destinace	k1gFnPc4
Českých	český	k2eAgFnPc2d1
aerolinií	aerolinie	k1gFnPc2
</s>
<s>
Click	Click	k6eAd1
<g/>
4	#num#	k4
<g/>
Sky	Sky	k1gFnPc2
</s>
<s>
Holidays	Holidays	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Unimex	Unimex	k1gInSc1
Group	Group	k1gInSc1
Sesterské	sesterský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
Smartwings	Smartwings	k1gInSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
flotila	flotila	k1gFnSc1
SmartWings	SmartWingsa	k1gFnPc2
</s>
<s>
České	český	k2eAgFnPc1d1
letecké	letecký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
Současné	současný	k2eAgFnPc1d1
</s>
<s>
Pravidelné	pravidelný	k2eAgFnPc4d1
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
Silver	Silver	k1gMnSc1
Air	Air	k1gMnSc1
</s>
<s>
Smartwings	Smartwings	k6eAd1
</s>
<s>
Van	van	k1gInSc1
Air	Air	k1gFnSc2
Europe	Europ	k1gInSc5
Aerotaxi	aerotaxi	k1gNnPc5
</s>
<s>
ABS	ABS	kA
Jets	Jets	k1gInSc1
</s>
<s>
Aerotaxi	aerotaxi	k1gNnSc1
</s>
<s>
Aeropartner	Aeropartner	k1gMnSc1
</s>
<s>
Air	Air	k?
Bohemia	bohemia	k1gFnSc1
</s>
<s>
Air	Air	k?
Prague	Prague	k1gInSc1
</s>
<s>
Airstream	Airstream	k6eAd1
</s>
<s>
CTR	CTR	kA
</s>
<s>
Eclair	Eclair	k1gInSc1
Aviation	Aviation	k1gInSc1
</s>
<s>
F-Air	F-Air	k1gMnSc1
</s>
<s>
G-JET	G-JET	k?
</s>
<s>
JetBee	JetBee	k1gFnSc1
Czech	Czecha	k1gFnPc2
</s>
<s>
Silesia	Silesia	k1gFnSc1
Air	Air	k1gFnSc2
</s>
<s>
LR	LR	kA
Airlines	Airlines	k1gMnSc1
</s>
<s>
Time	Time	k1gFnSc1
Air	Air	k1gFnSc2
</s>
<s>
Queen	Queen	k2eAgMnSc1d1
Air	Air	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
DSA	DSA	kA
</s>
<s>
air	air	k?
Fischer	Fischer	k1gMnSc1
</s>
<s>
Zaniklé	zaniklý	k2eAgNnSc1d1
</s>
<s>
(	(	kIx(
<g/>
vč.	vč.	k?
československých	československý	k2eAgInPc2d1
<g/>
)	)	kIx)
</s>
<s>
ABA	ABA	kA
Air	Air	k1gFnSc1
</s>
<s>
Alfa-Helicopter	Alfa-Helicopter	k1gMnSc1
</s>
<s>
Air	Air	k?
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Air	Air	k?
Moravia	Moravia	k1gFnSc1
</s>
<s>
Air	Air	k?
Terrex	Terrex	k1gInSc1
</s>
<s>
Air	Air	k?
Vítkovice	Vítkovice	k1gInPc1
</s>
<s>
Baťova	Baťův	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Bemoair	Bemoair	k1gMnSc1
</s>
<s>
Cargo	Cargo	k1gMnSc1
Moravia	Moravium	k1gNnSc2
Airlines	Airlines	k1gMnSc1
</s>
<s>
City	city	k1gNnSc1
Air	Air	k1gMnSc1
Services	Services	k1gMnSc1
</s>
<s>
Czech	Czech	k1gMnSc1
Connect	Connect	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
Central	Centrat	k5eAaPmAgMnS,k5eAaImAgMnS
Connect	Connect	k1gMnSc1
Airlines	Airlines	k1gMnSc1
(	(	kIx(
<g/>
Job	Job	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Československá	československý	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Discovery	Discovera	k1gFnPc1
Link	Linka	k1gFnPc2
</s>
<s>
Egretta	Egretta	k1gFnSc1
BMI	BMI	kA
</s>
<s>
Ensor	Ensor	k1gMnSc1
Air	Air	k1gMnSc1
</s>
<s>
Espe	Espe	k1gFnSc1
Air	Air	k1gFnSc2
</s>
<s>
Fischer	Fischer	k1gMnSc1
Air	Air	k1gMnSc1
</s>
<s>
Georgia	Georgia	k1gFnSc1
Air	Air	k1gFnSc1
Prague	Prague	k1gFnSc1
(	(	kIx(
<g/>
Air	Air	k1gFnSc1
Prague	Prague	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Helicopter	Helicopter	k1gMnSc1
</s>
<s>
Holidays	Holidays	k6eAd1
Czech	Czech	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
IDG	IDG	kA
Technology	technolog	k1gMnPc4
Airlines	Airlines	k1gMnSc1
</s>
<s>
Škoda	Škoda	k1gMnSc1
Air	Air	k1gMnSc1
</s>
<s>
Olimex	Olimex	k1gInSc1
</s>
<s>
Top	topit	k5eAaImRp2nS
Air	Air	k1gFnSc1
</s>
<s>
Členové	člen	k1gMnPc1
aliance	aliance	k1gFnSc2
SkyTeam	SkyTeam	k1gInSc1
Zakládající	zakládající	k2eAgInSc1d1
členové	člen	k1gMnPc1
</s>
<s>
Aeroméxico	Aeroméxico	k6eAd1
</s>
<s>
Air	Air	k?
France	Franc	k1gMnSc4
</s>
<s>
Delta	delta	k1gFnSc1
Air	Air	k1gFnSc2
Lines	Linesa	k1gFnPc2
</s>
<s>
Korean	Korean	k1gInSc1
Air	Air	k1gMnPc2
Ostatní	ostatní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Aeroflot	aeroflot	k1gInSc1
</s>
<s>
Aerolíneas	Aerolíneas	k1gMnSc1
Argentinas	Argentinas	k1gMnSc1
</s>
<s>
Air	Air	k?
Europa	Europa	k1gFnSc1
</s>
<s>
Alitalia	Alitalia	k1gFnSc1
</s>
<s>
China	China	k1gFnSc1
Airlines	Airlinesa	k1gFnPc2
</s>
<s>
China	China	k1gFnSc1
Eastern	Eastern	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
China	China	k1gFnSc1
Southern	Southern	k1gMnSc1
Airlines	Airlines	k1gMnSc1
</s>
<s>
České	český	k2eAgFnPc1d1
aerolinie	aerolinie	k1gFnPc1
</s>
<s>
Garuda	Garuda	k1gFnSc1
Indonesia	Indonesia	k1gFnSc1
</s>
<s>
Kenya	Kenya	k1gFnSc1
Airways	Airwaysa	k1gFnPc2
</s>
<s>
KLM	KLM	kA
</s>
<s>
Middle	Middle	k6eAd1
East	East	k2eAgInSc1d1
Airlines	Airlines	k1gInSc1
</s>
<s>
Saudia	Saudium	k1gNnPc1
</s>
<s>
TAROM	TAROM	kA
</s>
<s>
Vietnam	Vietnam	k1gInSc1
Airlines	Airlinesa	k1gFnPc2
</s>
<s>
Xiamen	Xiamen	k2eAgMnSc1d1
Air	Air	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010725056	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014116801	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
310631186	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014116801	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Letectví	letectví	k1gNnSc1
</s>
