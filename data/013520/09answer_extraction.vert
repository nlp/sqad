pověřeného	pověřený	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
vykonává	vykonávat	k5eAaImIp3nS
službu	služba	k1gFnSc4
charitativního	charitativní	k2eAgInSc2d1
a	a	k8xC
administrativního	administrativní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
bohoslužbách	bohoslužba	k1gFnPc6
<g/>
,	,	kIx,
předčítá	předčítat	k5eAaImIp3nS
posvátné	posvátný	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
vyučuje	vyučovat	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc4
a	a	k8xC
podobně	podobně	k6eAd1
