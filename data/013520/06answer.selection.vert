<s>
Jáhen	jáhen	k1gMnSc1
či	či	k8xC
diakon	diakon	k1gMnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
δ	δ	kA
diakonos	diakonos	k1gInSc1
<g/>
,	,	kIx,
služebník	služebník	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
číšník	číšník	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
křesťanských	křesťanský	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
označení	označení	k1gNnSc4
pro	pro	k7c4
pověřeného	pověřený	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
vykonává	vykonávat	k5eAaImIp3nS
službu	služba	k1gFnSc4
charitativního	charitativní	k2eAgInSc2d1
a	a	k8xC
administrativního	administrativní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
bohoslužbách	bohoslužba	k1gFnPc6
<g/>
,	,	kIx,
předčítá	předčítat	k5eAaImIp3nS
posvátné	posvátný	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
vyučuje	vyučovat	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>