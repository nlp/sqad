<s>
Jáhen	jáhen	k1gMnSc1
</s>
<s>
Pravoslavný	pravoslavný	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
modlí	modlit	k5eAaImIp3nS
ektenii	ektenie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Husitský	husitský	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
při	pře	k1gFnSc4
liturgiiJáhen	liturgiiJáhna	k1gFnPc2
čte	číst	k5eAaImIp3nS
evangelium	evangelium	k1gNnSc1
</s>
<s>
Jáhen	jáhen	k1gMnSc1
či	či	k8xC
diakon	diakon	k1gMnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
δ	δ	kA
diakonos	diakonos	k1gInSc1
<g/>
,	,	kIx,
služebník	služebník	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
číšník	číšník	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
křesťanských	křesťanský	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
označení	označení	k1gNnSc4
pro	pro	k7c4
pověřeného	pověřený	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
vykonává	vykonávat	k5eAaImIp3nS
službu	služba	k1gFnSc4
charitativního	charitativní	k2eAgInSc2d1
a	a	k8xC
administrativního	administrativní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
bohoslužbách	bohoslužba	k1gFnPc6
<g/>
,	,	kIx,
předčítá	předčítat	k5eAaImIp3nS
posvátné	posvátný	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
vyučuje	vyučovat	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
úřad	úřad	k1gInSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vysvěcen	vysvětit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
katolické	katolický	k2eAgFnSc6d1
<g/>
,	,	kIx,
pravoslavné	pravoslavný	k2eAgFnSc3d1
<g/>
,	,	kIx,
anglikánské	anglikánský	k2eAgFnSc3d1
<g/>
,	,	kIx,
starokatolické	starokatolický	k2eAgFnSc3d1
a	a	k8xC
husitské	husitský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
je	být	k5eAaImIp3nS
jáhenství	jáhenství	k1gNnSc1
prvním	první	k4xOgInSc7
stupněm	stupeň	k1gInSc7
svátosti	svátost	k1gFnSc2
svěcení	svěcení	k1gNnSc2
(	(	kIx(
<g/>
jáhen	jáhen	k1gMnSc1
»	»	k?
kněz	kněz	k1gMnSc1
»	»	k?
biskup	biskup	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
jiných	jiný	k2eAgFnPc6d1
tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc6
zastávají	zastávat	k5eAaImIp3nP
laikové	laik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc4d1
výraz	výraz	k1gInSc4
jáhen	jáhen	k1gMnSc1
překvapivě	překvapivě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
původního	původní	k2eAgInSc2d1
řeckého	řecký	k2eAgInSc2d1
diakonos	diakonos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgFnSc1d1
odlišnost	odlišnost	k1gFnSc1
obou	dva	k4xCgNnPc2
slov	slovo	k1gNnPc2
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
češtiny	čeština	k1gFnSc2
výraz	výraz	k1gInSc1
nepronikl	proniknout	k5eNaPmAgInS
přímo	přímo	k6eAd1
z	z	k7c2
řečtiny	řečtina	k1gFnSc2
či	či	k8xC
latiny	latina	k1gFnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
prostřednictvím	prostřednictvím	k7c2
němčiny	němčina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
řecké	řecký	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
diakonos	diakonosa	k1gFnPc2
či	či	k8xC
latinské	latinský	k2eAgNnSc4d1
diaconus	diaconus	k1gMnSc1
upravila	upravit	k5eAaPmAgFnS
na	na	k7c4
jáguno	jáguna	k1gFnSc5
či	či	k8xC
jáchono	jáchona	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
téhož	týž	k3xTgInSc2
původem	původ	k1gInSc7
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
i	i	k9
české	český	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
žák	žák	k1gMnSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
přes	přes	k7c4
němčinu	němčina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
přes	přes	k7c4
italštinu	italština	k1gFnSc4
či	či	k8xC
jiný	jiný	k2eAgInSc4d1
románský	románský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
počáteční	počáteční	k2eAgMnSc1d1
di	di	k?
přeměnilo	přeměnit	k5eAaPmAgNnS
místo	místo	k1gNnSc1
v	v	k7c6
dž	dž	k?
či	či	k8xC
ž.	ž.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
diakonátu	diakonát	k1gInSc2
</s>
<s>
Jáhnové	jáhen	k1gMnPc1
v	v	k7c6
Novém	nový	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
muž	muž	k1gMnSc1
plný	plný	k2eAgInSc4d1
víry	víra	k1gFnSc2
a	a	k8xC
Ducha	duch	k1gMnSc2
svatého	svatý	k1gMnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
,	,	kIx,
Prochor	Prochor	k1gMnSc1
<g/>
,	,	kIx,
Nikanor	Nikanor	k1gMnSc1
<g/>
,	,	kIx,
Timon	Timon	k1gMnSc1
<g/>
,	,	kIx,
Parmén	parména	k1gFnPc2
a	a	k8xC
Mikuláš	Mikuláš	k1gMnSc1
(	(	kIx(
<g/>
proselyta	proselyta	k1gMnSc1
z	z	k7c2
Antiochie	Antiochie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
vatikánský	vatikánský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
</s>
<s>
V	v	k7c6
katolické	katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
prvních	první	k4xOgNnPc6
staletích	staletí	k1gNnPc6
jáhenství	jáhenství	k1gNnSc2
stavem	stav	k1gInSc7
trvalým	trvalý	k2eAgInSc7d1
<g/>
,	,	kIx,
asi	asi	k9
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
jáhenství	jáhenství	k1gNnSc4
jako	jako	k9
předstupeň	předstupeň	k1gInSc1
kněžského	kněžský	k2eAgNnSc2d1
svěcení	svěcení	k1gNnSc2
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
tradice	tradice	k1gFnPc4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
po	po	k7c6
Druhém	druhý	k4xOgInSc6
vatikánském	vatikánský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
43	#num#	k4
000	#num#	k4
trvalých	trvalý	k2eAgMnPc2d1
jáhnů	jáhen	k1gMnPc2
v	v	k7c6
Římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Jáhen	jáhen	k1gMnSc1
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
</s>
<s>
Římskokatolický	římskokatolický	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
oblečený	oblečený	k2eAgMnSc1d1
v	v	k7c6
dalmatice	dalmatika	k1gFnSc6
</s>
<s>
Úkolem	úkol	k1gInSc7
jáhna	jáhen	k1gMnSc2
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
je	být	k5eAaImIp3nS
především	především	k9
„	„	k?
<g/>
hlásat	hlásat	k5eAaImF
Boží	boží	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
např.	např.	kA
předčítat	předčítat	k5eAaImF
z	z	k7c2
evangelia	evangelium	k1gNnSc2
při	při	k7c6
liturgii	liturgie	k1gFnSc6
<g/>
,	,	kIx,
křtít	křtít	k5eAaImF
<g/>
,	,	kIx,
oddávat	oddávat	k5eAaImF
<g/>
,	,	kIx,
pohřbívat	pohřbívat	k5eAaImF
a	a	k8xC
vyučovat	vyučovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
na	na	k7c6
starosti	starost	k1gFnSc6
též	též	k9
charitativní	charitativní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
(	(	kIx(
<g/>
návštěvy	návštěva	k1gFnPc1
nemocných	nemocný	k1gMnPc2
<g/>
)	)	kIx)
či	či	k8xC
administrativu	administrativa	k1gFnSc4
(	(	kIx(
<g/>
správa	správa	k1gFnSc1
farnosti	farnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Liturgickým	liturgický	k2eAgInSc7d1
oděvem	oděv	k1gInSc7
jáhna	jáhen	k1gMnSc2
při	při	k7c6
bohoslužbě	bohoslužba	k1gFnSc6
je	být	k5eAaImIp3nS
dalmatika	dalmatika	k1gFnSc1
<g/>
,	,	kIx,
roucho	roucho	k1gNnSc1
s	s	k7c7
rukávy	rukáv	k1gInPc7
–	–	k?
podobné	podobný	k2eAgInPc4d1
ornátu	ornát	k1gInSc3
–	–	k?
používané	používaný	k2eAgInPc1d1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
ornát	ornát	k1gInSc4
v	v	k7c6
liturgické	liturgický	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jáhen	jáhen	k1gMnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
kněze	kněz	k1gMnSc2
nosí	nosit	k5eAaImIp3nS
na	na	k7c6
albě	alba	k1gFnSc6
či	či	k8xC
rochetě	rocheta	k1gFnSc3
štolu	štola	k1gFnSc4
šikmo	šikmo	k6eAd1
přes	přes	k7c4
levé	levý	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
a	a	k8xC
sepjatou	sepjatý	k2eAgFnSc4d1
na	na	k7c6
boku	bok	k1gInSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
šerpu	šerpa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jáhni	jáhen	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
dočasní	dočasný	k2eAgMnPc1d1
–	–	k?
ti	ten	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
jáhny	jáhen	k1gMnPc4
svěceni	svěcen	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
současně	současně	k6eAd1
kandidáty	kandidát	k1gMnPc7
kněžství	kněžství	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
jáhenskou	jáhenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
má	mít	k5eAaImIp3nS
po	po	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
zastávat	zastávat	k5eAaImF
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
knězem	kněz	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
neaspirují	aspirovat	k5eNaImIp3nP
na	na	k7c4
kněžské	kněžský	k2eAgNnSc4d1
svěcení	svěcení	k1gNnSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
trvalí	trvalý	k2eAgMnPc1d1
jáhnové	jáhen	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
v	v	k7c6
pořádku	pořádek	k1gInSc6
rodinné	rodinný	k2eAgFnSc2d1
záležitosti	záležitost	k1gFnSc2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
dosáhnout	dosáhnout	k5eAaPmF
věku	věk	k1gInSc2
minimálně	minimálně	k6eAd1
35	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
absolvovat	absolvovat	k5eAaPmF
teologické	teologický	k2eAgNnSc4d1
studium	studium	k1gNnSc4
a	a	k8xC
formaci	formace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Jáhen	jáhen	k1gMnSc1
křtí	křtít	k5eAaImIp3nS
</s>
<s>
Kardinál-jáhen	Kardinál-jáhen	k1gInSc1
je	být	k5eAaImIp3nS
čestné	čestný	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
kardinála	kardinál	k1gMnSc4
majícího	mající	k2eAgMnSc4d1
titul	titul	k1gInSc4
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
14	#num#	k4
římských	římský	k2eAgFnPc2d1
chudinských	chudinský	k2eAgFnPc2d1
kurií	kurie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
nemá	mít	k5eNaImIp3nS
s	s	k7c7
jáhenským	jáhenský	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
svěcení	svěcení	k1gNnSc2
přímou	přímý	k2eAgFnSc4d1
souvislost	souvislost	k1gFnSc4
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
kardinálů-jáhnů	kardinálů-jáhn	k1gMnPc2
je	být	k5eAaImIp3nS
biskupy	biskup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Diakonát	diakonát	k1gInSc1
u	u	k7c2
žen	žena	k1gFnPc2
</s>
<s>
Jáhenky	Jáhenka	k1gFnPc1
v	v	k7c6
počátcích	počátek	k1gInPc6
křesťanství	křesťanství	k1gNnSc2
</s>
<s>
Bible	bible	k1gFnSc1
zná	znát	k5eAaImIp3nS
také	také	k6eAd1
jáhenky	jáhenka	k1gFnSc2
–	–	k?
diakonky	diakonka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
praxe	praxe	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
prvotní	prvotní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
obvyklá	obvyklý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
církevní	církevní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
byly	být	k5eAaImAgInP
ustanovovány	ustanovovat	k5eAaImNgInP
od	od	k7c2
biskupa	biskup	k1gInSc2
zvláštním	zvláštní	k2eAgInSc7d1
obřadem	obřad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opodstatnění	opodstatnění	k1gNnSc1
jejich	jejich	k3xOp3gInSc2
úřadu	úřad	k1gInSc2
lze	lze	k6eAd1
hledat	hledat	k5eAaImF
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jedním	jeden	k4xCgInSc7
z	z	k7c2
úkolů	úkol	k1gInPc2
jáhna	jáhen	k1gMnSc2
bylo	být	k5eAaImAgNnS
asistovat	asistovat	k5eAaImF
u	u	k7c2
křtů	křest	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křty	křest	k1gInPc4
v	v	k7c6
prvotní	prvotní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
přijímali	přijímat	k5eAaImAgMnP
v	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
dospělí	dospělí	k1gMnPc1
a	a	k8xC
při	při	k7c6
křtu	křest	k1gInSc6
bývali	bývat	k5eAaImAgMnP
nazí	nahý	k2eAgMnPc1d1
ponořováni	ponořován	k2eAgMnPc1d1
do	do	k7c2
vodní	vodní	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
křtily	křtít	k5eAaImAgFnP
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
probíhalo	probíhat	k5eAaImAgNnS
to	ten	k3xDgNnSc1
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
nevzniklo	vzniknout	k5eNaPmAgNnS
pohoršení	pohoršení	k1gNnSc1
<g/>
,	,	kIx,
asistovaly	asistovat	k5eAaImAgFnP
u	u	k7c2
těchto	tento	k3xDgInPc2
křtů	křest	k1gInPc2
ženy-jáhenky	ženy-jáhenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdější	pozdní	k2eAgFnSc6d2
době	doba	k1gFnSc6
převládla	převládnout	k5eAaPmAgFnS
praxe	praxe	k1gFnSc1
křtít	křtít	k5eAaImF
již	již	k9
nemluvňata	nemluvně	k1gNnPc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
již	již	k6eAd1
jáhenek	jáhenka	k1gFnPc2
nebylo	být	k5eNaImAgNnS
třeba	třeba	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Jáhenky	Jáhenka	k1gFnPc1
však	však	k9
nepřisluhovaly	přisluhovat	k5eNaImAgFnP
jen	jen	k9
při	při	k7c6
křtu	křest	k1gInSc6
a	a	k8xC
katechezi	katecheze	k1gFnSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
vykonávaly	vykonávat	k5eAaImAgInP
i	i	k9
skutky	skutek	k1gInPc1
křesťanské	křesťanský	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
k	k	k7c3
bližnímu	bližní	k1gMnSc3
<g/>
,	,	kIx,
tj.	tj.	kA
posluhovaly	posluhovat	k5eAaImAgFnP
nemocným	nemocný	k1gMnPc3
<g/>
,	,	kIx,
chudým	chudý	k1gMnPc3
a	a	k8xC
poutníkům	poutník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
církvi	církev	k1gFnSc6
bylo	být	k5eAaImAgNnS
odedávna	odedávna	k6eAd1
zvykem	zvyk	k1gInSc7
oddělovat	oddělovat	k5eAaImF
v	v	k7c6
chrámu	chrám	k1gInSc6
muže	muž	k1gMnSc2
od	od	k7c2
žen	žena	k1gFnPc2
a	a	k8xC
vykazovat	vykazovat	k5eAaImF
každému	každý	k3xTgNnSc3
pohlaví	pohlaví	k1gNnSc3
příslušné	příslušný	k2eAgInPc1d1
místo	místo	k1gNnSc4
po	po	k7c6
stranách	strana	k1gFnPc6
chrámové	chrámový	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
pohlaví	pohlaví	k1gNnSc1
mělo	mít	k5eAaImAgNnS
též	též	k9
svou	svůj	k3xOyFgFnSc4
bránu	brána	k1gFnSc4
do	do	k7c2
kostela	kostel	k1gInSc2
<g/>
;	;	kIx,
u	u	k7c2
brány	brána	k1gFnSc2
pro	pro	k7c4
muže	muž	k1gMnPc4
dbal	dbát	k5eAaImAgMnS
o	o	k7c4
pořádek	pořádek	k1gInSc4
ostiář	ostiář	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c4
bránu	brána	k1gFnSc4
pro	pro	k7c4
ženy	žena	k1gFnPc4
se	se	k3xPyFc4
starala	starat	k5eAaImAgFnS
jáhenka	jáhenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
západní	západní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
přestal	přestat	k5eAaPmAgInS
obyčej	obyčej	k1gInSc1
zřizovat	zřizovat	k5eAaImF
jáhenky	jáhenka	k1gFnPc4
ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
z	z	k7c2
části	část	k1gFnSc2
i	i	k9
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
nepřístojnosti	nepřístojnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
zavinili	zavinit	k5eAaPmAgMnP
zvláště	zvláště	k9
bludaři	bludař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
Španělsku	Španělsko	k1gNnSc6
byly	být	k5eAaImAgFnP
jáhenky	jáhenka	k1gFnPc1
odstraněny	odstranit	k5eAaPmNgFnP
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
se	se	k3xPyFc4
zachoval	zachovat	k5eAaPmAgInS
úřad	úřad	k1gInSc1
jáhenek	jáhenka	k1gFnPc2
až	až	k6eAd1
do	do	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozůstatky	pozůstatek	k1gInPc1
tohoto	tento	k3xDgInSc2
úřadu	úřad	k1gInSc2
přetrvávají	přetrvávat	k5eAaImIp3nP
u	u	k7c2
maronitů	maronit	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
abatyše	abatyše	k1gFnPc1
dostávají	dostávat	k5eAaImIp3nP
žehnání	žehnání	k1gNnSc1
jáhenek	jáhenka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jáhenky	Jáhenka	k1gFnPc1
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
</s>
<s>
Otevření	otevření	k1gNnSc1
otázky	otázka	k1gFnSc2
a	a	k8xC
zkoumání	zkoumání	k1gNnSc2
možnosti	možnost	k1gFnSc2
vysvěcení	vysvěcení	k1gNnSc2
žen	žena	k1gFnPc2
za	za	k7c4
jáhenky	jáhenka	k1gFnPc4
</s>
<s>
Dvanáctého	dvanáctý	k4xOgInSc2
května	květen	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
katolický	katolický	k2eAgMnSc1d1
papež	papež	k1gMnSc1
František	František	k1gMnSc1
sešel	sejít	k5eAaPmAgMnS
s	s	k7c7
vedoucími	vedoucí	k2eAgMnPc7d1
představenými	představený	k1gMnPc7
900	#num#	k4
ženských	ženský	k2eAgFnPc2d1
katolických	katolický	k2eAgFnPc2d1
náboženských	náboženský	k2eAgFnPc2d1
kongregací	kongregace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
setkání	setkání	k1gNnSc6
mimo	mimo	k7c4
jiného	jiný	k1gMnSc4
odpovídal	odpovídat	k5eAaImAgMnS
na	na	k7c4
otázku	otázka	k1gFnSc4
ohledně	ohledně	k7c2
ženských	ženský	k2eAgFnPc2d1
jáhenek	jáhenka	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
po	po	k7c6
setkání	setkání	k1gNnSc6
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
papež	papež	k1gMnSc1
František	František	k1gMnSc1
zřídil	zřídit	k5eAaPmAgMnS
dvanáctičlennou	dvanáctičlenný	k2eAgFnSc4d1
studijní	studijní	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
za	za	k7c4
úkol	úkol	k1gInSc4
prozkoumat	prozkoumat	k5eAaPmF
úkol	úkol	k1gInSc4
jáhenek	jáhenka	k1gFnPc2
v	v	k7c6
prvotní	prvotní	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jáhen	jáhen	k1gMnSc1
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
</s>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1
používají	používat	k5eAaImIp3nP
pro	pro	k7c4
jáhny	jáhen	k1gMnPc4
spíše	spíše	k9
označení	označení	k1gNnSc4
diakon	diakon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
čtení	čtení	k1gNnSc2
evangelia	evangelium	k1gNnSc2
a	a	k8xC
pomoci	pomoc	k1gFnSc2
při	při	k7c6
podávání	podávání	k1gNnSc6
svatého	svatý	k2eAgNnSc2d1
přijímání	přijímání	k1gNnSc2
vede	vést	k5eAaImIp3nS
diakon	diakon	k1gMnSc1
litanie	litanie	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
v	v	k7c6
dialogu	dialog	k1gInSc6
anafory	anafora	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
katolických	katolický	k2eAgMnPc2d1
jáhnů	jáhen	k1gMnPc2
(	(	kIx(
<g/>
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mohou	moct	k5eAaImIp3nP
vysluhovat	vysluhovat	k5eAaImF
jen	jen	k9
některé	některý	k3yIgFnPc1
<g/>
)	)	kIx)
nemůže	moct	k5eNaImIp3nS
pravoslavný	pravoslavný	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
východní	východní	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
vysluhovat	vysluhovat	k5eAaImF
svaté	svatý	k2eAgFnSc2d1
tajiny	tajina	k1gFnSc2
(	(	kIx(
<g/>
svátosti	svátost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jáhen	jáhen	k1gMnSc1
však	však	k9
pomáhá	pomáhat	k5eAaImIp3nS
při	při	k7c6
křtu	křest	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
s	s	k7c7
neofytou	neofyta	k1gMnSc7
noří	nořit	k5eAaImIp3nS
do	do	k7c2
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
křest	křest	k1gInSc1
praktikován	praktikovat	k5eAaImNgInS
ponořením	ponoření	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liturgickými	liturgický	k2eAgInPc7d1
oděvy	oděv	k1gInPc7
diakona	diakon	k1gMnSc2
jsou	být	k5eAaImIp3nP
sticharion	sticharion	k1gInSc4
(	(	kIx(
<g/>
dalmatika	dalmatika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
orarion	orarion	k1gInSc1
(	(	kIx(
<g/>
jáhenská	jáhenský	k2eAgFnSc1d1
štóla	štóla	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
epimanikia	epimanikia	k1gFnSc1
(	(	kIx(
<g/>
rukávce	rukávec	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc2d1
jmenované	jmenovaná	k1gFnSc2
se	se	k3xPyFc4
nosí	nosit	k5eAaImIp3nS
pod	pod	k7c4
sticharion	sticharion	k1gInSc4
<g/>
,	,	kIx,
ne	ne	k9
jako	jako	k9
u	u	k7c2
kněží	kněz	k1gMnPc2
či	či	k8xC
biskupů	biskup	k1gMnPc2
nad	nad	k7c4
něj	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
venkovní	venkovní	k2eAgInSc4d1
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
oblékají	oblékat	k5eAaImIp3nP
jáhni	jáhen	k1gMnPc1
podobně	podobně	k6eAd1
jako	jako	k9
pravoslavní	pravoslavný	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
kleriku	klerika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jáhen	jáhen	k1gMnSc1
ve	v	k7c6
starokatolické	starokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
</s>
<s>
Jáhenská	jáhenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
duchovní	duchovní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
i	i	k9
ve	v	k7c6
starokatolických	starokatolický	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
<g/>
,	,	kIx,
Starokatolická	starokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
světí	světit	k5eAaImIp3nP
též	též	k9
jáhenky	jáhenka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
jáhni	jáhen	k1gMnPc1
asistují	asistovat	k5eAaImIp3nP
kněžím	kněz	k1gMnPc3
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pověřováni	pověřován	k2eAgMnPc1d1
různými	různý	k2eAgInPc7d1
pastoračními	pastorační	k2eAgInPc7d1
úkoly	úkol	k1gInPc7
<g/>
,	,	kIx,
vést	vést	k5eAaImF
liturgii	liturgie	k1gFnSc4
bohoslužby	bohoslužba	k1gFnSc2
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
udělovat	udělovat	k5eAaImF
svátost	svátost	k1gFnSc4
křtu	křest	k1gInSc2
či	či	k8xC
posledního	poslední	k2eAgNnSc2d1
pomazání	pomazání	k1gNnSc2
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
liturgii	liturgie	k1gFnSc6
užívají	užívat	k5eAaImIp3nP
standardní	standardní	k2eAgInSc4d1
jáhenský	jáhenský	k2eAgInSc4d1
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k9
jáhenskou	jáhenský	k2eAgFnSc4d1
štolu	štola	k1gFnSc4
přes	přes	k7c4
albu	alba	k1gFnSc4
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
též	též	k9
dalmatiku	dalmatika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jáhen	jáhen	k1gMnSc1
v	v	k7c6
evangelických	evangelický	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
</s>
<s>
V	v	k7c6
evangelických	evangelický	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
jsou	být	k5eAaImIp3nP
jako	jako	k9
jáhni	jáhen	k1gMnPc1
označovány	označovat	k5eAaImNgInP
buďto	buďto	k8xC
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jako	jako	k9
hlavní	hlavní	k2eAgNnPc1d1
povolání	povolání	k1gNnPc1
poskytují	poskytovat	k5eAaImIp3nP
charitativní	charitativní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
ordinací	ordinace	k1gFnSc7
povolány	povolat	k5eAaPmNgInP
ke	k	k7c3
kázání	kázání	k1gNnSc3
Božího	boží	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
a	a	k8xC
vysluhování	vysluhování	k1gNnSc2
svátostí	svátost	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
nemají	mít	k5eNaImIp3nP
takový	takový	k3xDgInSc4
rozsah	rozsah	k1gInSc4
pravomocí	pravomoc	k1gFnPc2
jako	jako	k8xS,k8xC
pastor	pastor	k1gMnSc1
či	či	k8xC
farář	farář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jáhen	jáhen	k1gMnSc1
v	v	k7c6
Církvi	církev	k1gFnSc6
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
</s>
<s>
Jáhenství	jáhenství	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Církvi	církev	k1gFnSc6
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
prvním	první	k4xOgInSc7
stupněm	stupeň	k1gInSc7
svátosti	svátost	k1gFnSc2
svěcení	svěcení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jáhen	jáhen	k1gMnSc1
(	(	kIx(
<g/>
diákon	diákon	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
pomocného	pomocný	k2eAgMnSc4d1
duchovního	duchovní	k1gMnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
mu	on	k3xPp3gNnSc3
však	však	k9
náležet	náležet	k5eAaImF
specifické	specifický	k2eAgInPc4d1
liturgické	liturgický	k2eAgInPc4d1
úkony	úkon	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
obdobné	obdobný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
(	(	kIx(
<g/>
resp.	resp.	kA
pravoslavné	pravoslavný	k2eAgFnSc3d1
<g/>
)	)	kIx)
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
husitů	husita	k1gMnPc2
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
setkat	setkat	k5eAaPmF
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jáhen	jáhen	k1gMnSc1
obléká	oblékat	k5eAaImIp3nS
buď	buď	k8xC
pouze	pouze	k6eAd1
černý	černý	k2eAgInSc4d1
talár	talár	k1gInSc4
s	s	k7c7
kalichem	kalich	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
doplňuje	doplňovat	k5eAaImIp3nS
o	o	k7c4
štólu	štóla	k1gFnSc4
<g/>
,	,	kIx,
umístěnou	umístěný	k2eAgFnSc4d1
šikmo	šikmo	k6eAd1
přes	přes	k7c4
levé	levý	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
a	a	k8xC
sepjatou	sepjatý	k2eAgFnSc4d1
na	na	k7c6
boku	bok	k1gInSc6
<g/>
,	,	kIx,
či	či	k8xC
obléká	oblékat	k5eAaImIp3nS
albu	alba	k1gFnSc4
(	(	kIx(
<g/>
s	s	k7c7
červeným	červený	k2eAgInSc7d1
kalichem	kalich	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
stejně	stejně	k6eAd1
umístěnou	umístěný	k2eAgFnSc7d1
štólou	štóla	k1gFnSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
lze	lze	k6eAd1
štólu	štóla	k1gFnSc4
umístit	umístit	k5eAaPmF
z	z	k7c2
levého	levý	k2eAgNnSc2d1
ramene	rameno	k1gNnSc2
směřujíc	směřovat	k5eAaImSgFnS
dolů	dol	k1gInPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
pravoslavných	pravoslavný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
původní	původní	k2eAgFnSc6d1
slovanské	slovanský	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
klerika	klerika	k1gFnSc1
u	u	k7c2
světských	světský	k2eAgMnPc2d1
kleriků	klerik	k1gMnPc2
šedá	šedat	k5eAaImIp3nS
a	a	k8xC
u	u	k7c2
mnichů	mnich	k1gMnPc2
černá	černý	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jáhnů	jáhen	k1gMnPc2
římskokatolických	římskokatolický	k2eAgMnPc2d1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
většině	většina	k1gFnSc6
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
nechodí	chodit	k5eNaImIp3nP
oděni	odět	k5eAaPmNgMnP
jako	jako	k9
klerici	klerik	k1gMnPc1
<g/>
,	,	kIx,
byť	byť	k8xS
jimi	on	k3xPp3gInPc7
de	de	k?
iure	iur	k1gMnSc4
jsou	být	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
tato	tento	k3xDgFnSc1
praxe	praxe	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
méně	málo	k6eAd2
dodržována	dodržován	k2eAgFnSc1d1
<g/>
;	;	kIx,
kněží	kněz	k1gMnPc1
i	i	k8xC
diakoni	diakon	k1gMnPc1
nosí	nosit	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
černé	černý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
barevné	barevný	k2eAgFnPc1d1
kleriky	klerika	k1gFnPc1
(	(	kIx(
<g/>
tmavě	tmavě	k6eAd1
zelené	zelený	k2eAgInPc1d1
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
modré	modré	k1gNnSc1
<g/>
,	,	kIx,
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠMILAUER	ŠMILAUER	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etymologická	etymologický	k2eAgFnSc1d1
procházka	procházka	k1gFnSc1
po	po	k7c6
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
1944	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
28	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.cirkev.cz/archiv/150417-vatikan-vydal-nove-statistiky-o-katolicke-cirkvi-ve-svete	http://www.cirkev.cz/archiv/150417-vatikan-vydal-nove-statistiky-o-katolicke-cirkvi-ve-svat	k5eAaPmIp2nP
<g/>
↑	↑	k?
Směrnice	směrnice	k1gFnPc1
pro	pro	k7c4
život	život	k1gInSc4
a	a	k8xC
službu	služba	k1gFnSc4
trvalých	trvalý	k2eAgInPc2d1
jáhnů	jáhen	k1gMnPc2
http://www.biskupstvi.cz/storage/dokumenty/smernice_stali_jahni.pdf	http://www.biskupstvi.cz/storage/dokumenty/smernice_stali_jahni.pdf	k1gMnSc1
<g/>
↑	↑	k?
TUMPACH	TUMPACH	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
PODLAHA	podlaha	k1gFnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
slovník	slovník	k1gInSc1
bohovědný	bohovědný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
církevní	církevní	k2eAgInPc4d1
řády	řád	k1gInPc4
–	–	k?
Ezzo	Ezzo	k6eAd1
(	(	kIx(
<g/>
sešity	sešit	k1gInPc7
42	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cyrillo-Methodějská	Cyrillo-Methodějský	k2eAgFnSc1d1
knihtiskárna	knihtiskárna	k1gFnSc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
V.	V.	kA
Kotrba	kotrba	k1gFnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
960	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
heslo	heslo	k1gNnSc4
„	„	k?
<g/>
diakonissa	diakonissa	k1gFnSc1
<g/>
,	,	kIx,
jáhenka	jáhenka	k1gFnSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
490	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://ok21.sk/zeny-diakonky-v-katolickej-cirkvi/	http://ok21.sk/zeny-diakonky-v-katolickej-cirkvi/	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.tkkbs.sk/view.php?cisloclanku=20160802035	https://www.tkkbs.sk/view.php?cisloclanku=20160802035	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
</s>
<s>
Liturgická	liturgický	k2eAgNnPc4d1
oblečení	oblečení	k1gNnPc4
pravoslavných	pravoslavný	k2eAgMnPc2d1
duchovních	duchovní	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KAŇA	KAŇA	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jáhni	jáhen	k1gMnPc1
a	a	k8xC
my	my	k3xPp1nPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TUMPACH	TUMPACH	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
PODLAHA	podlaha	k1gFnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
slovník	slovník	k1gInSc1
bohovědný	bohovědný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
církevní	církevní	k2eAgInPc4d1
řády	řád	k1gInPc4
–	–	k?
Ezzo	Ezzo	k6eAd1
(	(	kIx(
<g/>
sešity	sešit	k1gInPc7
42	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cyrillo-Methodějská	Cyrillo-Methodějský	k2eAgFnSc1d1
knihtiskárna	knihtiskárna	k1gFnSc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
V.	V.	kA
Kotrba	kotrba	k1gFnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
960	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
heslo	heslo	k1gNnSc4
„	„	k?
<g/>
diakonát	diakonát	k1gInSc1
<g/>
,	,	kIx,
jáhenství	jáhenství	k1gNnSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
489	#num#	k4
<g/>
–	–	k?
<g/>
490	#num#	k4
a	a	k8xC
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
diakonissa	diakonissa	k1gFnSc1
<g/>
,	,	kIx,
jáhenka	jáhenka	k1gFnSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
490	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lektor	lektor	k1gMnSc1
</s>
<s>
Akolyta	akolyta	k1gMnSc1
</s>
<s>
Kněz	kněz	k1gMnSc1
</s>
<s>
Arcijáhen	arcijáhen	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
jáhen	jáhen	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
jáhen	jáhen	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4149467-2	4149467-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85036022	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85036022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
