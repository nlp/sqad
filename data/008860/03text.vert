<p>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
Rakouska	Rakousko	k1gNnSc2	Rakousko
tvoří	tvořit	k5eAaImIp3nP	tvořit
list	list	k1gInSc4	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
<g/>
,	,	kIx,	,
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vzor	vzor	k1gInSc1	vzor
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
historickému	historický	k2eAgInSc3d1	historický
štítu	štít	k1gInSc3	štít
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
babenberský	babenberský	k2eAgMnSc1d1	babenberský
vévoda	vévoda	k1gMnSc1	vévoda
Leopold	Leopolda	k1gFnPc2	Leopolda
V.	V.	kA	V.
v	v	k7c6	v
křižáckém	křižácký	k2eAgNnSc6d1	křižácké
tažení	tažení	k1gNnSc6	tažení
roku	rok	k1gInSc2	rok
1191	[number]	k4	1191
v	v	k7c6	v
Akkonu	Akkon	k1gInSc6	Akkon
za	za	k7c4	za
Svatou	svatý	k2eAgFnSc4d1	svatá
zemi	zem	k1gFnSc4	zem
tak	tak	k9	tak
udatně	udatně	k6eAd1	udatně
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
boji	boj	k1gInSc6	boj
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
bílé	bílý	k2eAgNnSc1d1	bílé
bojové	bojový	k2eAgNnSc1d1	bojové
oblečení	oblečení	k1gNnSc1	oblečení
úplně	úplně	k6eAd1	úplně
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
však	však	k9	však
sundal	sundat	k5eAaPmAgInS	sundat
pásek	pásek	k1gInSc1	pásek
<g/>
,	,	kIx,	,
oděv	oděv	k1gInSc1	oděv
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
nebyl	být	k5eNaImAgMnS	být
krví	krvit	k5eAaImIp3nS	krvit
dotknutý	dotknutý	k2eAgMnSc1d1	dotknutý
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naskytl	naskytnout	k5eAaPmAgMnS	naskytnout
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
rozložení	rozložení	k1gNnSc1	rozložení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
červeno-bílo-červená	červenoílo-červený	k2eAgFnSc1d1	červeno-bílo-červený
vlajka	vlajka	k1gFnSc1	vlajka
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc4d1	státní
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
obchodní	obchodní	k2eAgFnSc7d1	obchodní
vlajkou	vlajka	k1gFnSc7	vlajka
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
doplněná	doplněný	k2eAgNnPc4d1	doplněné
štítem	štít	k1gInSc7	štít
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousko-Uherské	rakouskoherský	k2eAgFnSc6d1	rakousko-uherská
monarchii	monarchie	k1gFnSc6	monarchie
byla	být	k5eAaImAgFnS	být
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
je	být	k5eAaImIp3nS	být
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
doplněná	doplněný	k2eAgNnPc4d1	doplněné
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pruhu	pruh	k1gInSc2	pruh
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vlajku	vlajka	k1gFnSc4	vlajka
státní	státní	k2eAgFnSc4d1	státní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motiv	motiv	k1gInSc4	motiv
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
červenobílých	červenobílý	k2eAgInPc2d1	červenobílý
pruhů	pruh	k1gInPc2	pruh
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Polynésie	Polynésie	k1gFnSc2	Polynésie
či	či	k8xC	či
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Georgia	Georgium	k1gNnSc2	Georgium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Rakúska	Rakúska	k1gFnSc1	Rakúska
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Rakouska	Rakousko	k1gNnSc2	Rakousko
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rakouska	Rakousko	k1gNnSc2	Rakousko
</s>
</p>
