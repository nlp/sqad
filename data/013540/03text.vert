<s>
Terezie	Terezie	k1gFnSc1
Dobrovolná	Dobrovolná	k1gFnSc1
</s>
<s>
Mgr.	Mgr.	kA
Terezie	Terezie	k1gFnSc1
DobrovolnáNarození	DobrovolnáNarození	k1gNnPc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
česká	český	k2eAgFnSc1d1
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Miss	miss	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1997	#num#	k4
vítězkaMiss	vítězkaMissa	k1gFnPc2
Tourism	Tourisma	k1gFnPc2
Queen	Queen	k2eAgInSc1d1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
International	International	k1gFnSc1
1994	#num#	k4
II	II	kA
<g/>
.	.	kIx.
vicemissMiss	vicemissMiss	k1gInSc1
Tourism	Tourism	k1gInSc1
International	International	k1gFnSc1
1993	#num#	k4
<g/>
III	III	kA
<g/>
.	.	kIx.
vicemissMiss	vicemissMiss	k6eAd1
Globe	globus	k1gInSc5
International	International	k1gMnSc2
1993	#num#	k4
<g/>
I.	I.	kA
vicemiss	vicemiss	k1gFnPc2
</s>
<s>
Terezie	Terezie	k1gFnSc1
Dobrovolná	Dobrovolná	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1975	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
Miss	miss	k1gFnSc1
ČR	ČR	kA
1997	#num#	k4
a	a	k8xC
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Modelingu	Modeling	k1gInSc3
se	se	k3xPyFc4
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
od	od	k7c2
šestnácti	šestnáct	k4xCc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
předváděla	předvádět	k5eAaImAgFnS
hlavně	hlavně	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vítězkou	vítězka	k1gFnSc7
soutěže	soutěž	k1gFnSc2
krásy	krása	k1gFnSc2
Miss	miss	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
nás	my	k3xPp1nPc4
ještě	ještě	k9
reprezentovala	reprezentovat	k5eAaImAgFnS
na	na	k7c4
Miss	miss	k1gFnSc4
World	Worlda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
další	další	k2eAgNnSc4d1
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
zvolení	zvolení	k1gNnSc6
žije	žít	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
partnerem	partner	k1gMnSc7
miliardářem	miliardář	k1gMnSc7
Davidem	David	k1gMnSc7
Beranem	Beran	k1gMnSc7
na	na	k7c6
samotě	samota	k1gFnSc6
nedaleko	nedaleko	k7c2
Slatiňan	Slatiňany	k1gInPc2
u	u	k7c2
Pardubic	Pardubice	k1gInPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
s	s	k7c7
ním	on	k3xPp3gMnSc7
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
Kristýnu	Kristýna	k1gFnSc4
a	a	k8xC
Kateřinu	Kateřina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žili	žít	k5eAaImAgMnP
spolu	spolu	k6eAd1
i	i	k8xC
krátce	krátce	k6eAd1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Miss	miss	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
Ivana	Ivana	k1gFnSc1
Christová	Christová	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Renáta	Renáta	k1gFnSc1
Gorecká	gorecký	k2eAgFnSc1d1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Michaela	Michaela	k1gFnSc1
Maláčová	Maláčová	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Pavlína	Pavlína	k1gFnSc1
Babůrková	Babůrková	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Silvia	Silvia	k1gFnSc1
Lakatošová	Lakatošová	k1gFnSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1994	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Eva	Eva	k1gFnSc1
Kotulánová	Kotulánová	k1gFnSc1
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Monika	Monika	k1gFnSc1
Žídková	Žídková	k1gFnSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Petra	Petra	k1gFnSc1
Minářová	Minářová	k1gFnSc1
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
Terezie	Terezie	k1gFnSc1
Dobrovolná	Dobrovolná	k1gFnSc1
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Kateřina	Kateřina	k1gFnSc1
Stočesová	Stočesový	k2eAgFnSc1d1
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Helena	Helena	k1gFnSc1
Houdová	Houdová	k1gFnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Michaela	Michaela	k1gFnSc1
Salačová	Salačová	k1gFnSc1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Diana	Diana	k1gFnSc1
Kobzanová	Kobzanová	k1gFnSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Kateřina	Kateřina	k1gFnSc1
Průšová	Průšová	k1gFnSc1
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Lucie	Lucie	k1gFnSc1
Váchová	Váchová	k1gFnSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Jana	Jana	k1gFnSc1
Doleželová	Doleželová	k1gFnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Lucie	Lucie	k1gFnSc1
Králová	Králová	k1gFnSc1
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Taťána	Taťána	k1gFnSc1
Kuchařová	Kuchařová	k1gFnSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Kateřina	Kateřina	k1gFnSc1
Sokolová	Sokolová	k1gFnSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Zuzana	Zuzana	k1gFnSc1
Jandová	Jandová	k1gFnSc1
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Aneta	Aneta	k1gFnSc1
Vignerová	Vignerová	k1gFnSc1
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Iveta	Iveta	k1gFnSc1
Maurerová	Maurerová	k1gFnSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Nikola	Nikola	k1gFnSc1
Kokyová	Kokyová	k1gFnSc1
názvy	název	k1gInPc4
soutěže	soutěž	k1gFnSc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Miss	miss	k1gFnSc2
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
)	)	kIx)
Miss	miss	k1gFnSc2
ČSFR	ČSFR	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
)	)	kIx)
Miss	miss	k1gFnPc3
České	český	k2eAgFnSc2d1
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
99240000167	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5828	#num#	k4
0165	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84105087	#num#	k4
</s>
