<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
označovaná	označovaný	k2eAgFnSc1d1
též	též	k9
zkratkou	zkratka	k1gFnSc7
EMU	EMU	kA
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Electric	electric	k2eAgInSc1d1
multiple	multiple	k2eAgInSc1d1
unit	unit	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
souprava	souprava	k1gFnSc1
železničních	železniční	k2eAgInPc2d1
vozů	vůz	k1gInPc2
schopná	schopný	k2eAgFnSc1d1
vyvíjet	vyvíjet	k5eAaImF
tažnou	tažný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
na	na	k7c6
obvodu	obvod	k1gInSc6
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>