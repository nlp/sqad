<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
440	#num#	k4
(	(	kIx(
<g/>
RegioPanter	RegioPanter	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
680	#num#	k4
(	(	kIx(
<g/>
Pendolino	Pendolin	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
hlavní	hlavní	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
471	#num#	k4
(	(	kIx(
<g/>
CityElefant	CityElefant	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
označovaná	označovaný	k2eAgFnSc1d1
též	též	k9
zkratkou	zkratka	k1gFnSc7
EMU	EMU	kA
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Electric	electric	k2eAgInSc1d1
multiple	multiple	k2eAgInSc1d1
unit	unit	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
souprava	souprava	k1gFnSc1
železničních	železniční	k2eAgInPc2d1
vozů	vůz	k1gInPc2
schopná	schopný	k2eAgFnSc1d1
vyvíjet	vyvíjet	k5eAaImF
tažnou	tažný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
na	na	k7c6
obvodu	obvod	k1gInSc6
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
odlišnost	odlišnost	k1gFnSc1
elektrické	elektrický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
od	od	k7c2
klasické	klasický	k2eAgFnSc2d1
soupravy	souprava	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
vlaku	vlak	k1gInSc2
složené	složený	k2eAgFnSc2d1
z	z	k7c2
lokomotivy	lokomotiva	k1gFnSc2
a	a	k8xC
tažených	tažený	k2eAgInPc2d1
vagonů	vagon	k1gInPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
nedělitelná	dělitelný	k2eNgFnSc1d1
a	a	k8xC
její	její	k3xOp3gInPc4
vozy	vůz	k1gInPc4
lze	lze	k6eAd1
od	od	k7c2
sebe	sebe	k3xPyFc4
odpojit	odpojit	k5eAaPmF
jen	jen	k9
dílensky	dílensky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
konstruována	konstruovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
vratná	vratný	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
obousměrná	obousměrný	k2eAgFnSc1d1
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
řídicí	řídicí	k2eAgFnSc4d1
kabinu	kabina	k1gFnSc4
na	na	k7c6
obou	dva	k4xCgNnPc6
čelech	čelo	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dříve	dříve	k6eAd2
byly	být	k5eAaImAgInP
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
zejména	zejména	k9
v	v	k7c6
příměstské	příměstský	k2eAgFnSc6d1
nebo	nebo	k8xC
regionální	regionální	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
běžné	běžný	k2eAgInPc1d1
i	i	k9
v	v	k7c6
rychlíkové	rychlíkový	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výhody	výhoda	k1gFnPc1
a	a	k8xC
nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
výhody	výhoda	k1gFnPc4
elektrických	elektrický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
oproti	oproti	k7c3
klasickým	klasický	k2eAgFnPc3d1
vlakovým	vlakový	k2eAgFnPc3d1
soupravám	souprava	k1gFnPc3
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
optimalizace	optimalizace	k1gFnSc1
trakčních	trakční	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
(	(	kIx(
<g/>
jednotka	jednotka	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
vlaku	vlak	k1gInSc2
s	s	k7c7
proměnlivým	proměnlivý	k2eAgInSc7d1
počtem	počet	k1gInSc7
vozů	vůz	k1gInPc2
relativní	relativní	k2eAgFnSc4d1
stálou	stálý	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
odpovídá	odpovídat	k5eAaImIp3nS
optimálně	optimálně	k6eAd1
dimenzovaný	dimenzovaný	k2eAgInSc1d1
trakční	trakční	k2eAgInSc1d1
pohon	pohon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
minimalizace	minimalizace	k1gFnSc1
hmotnosti	hmotnost	k1gFnSc2
vlaku	vlak	k1gInSc2
(	(	kIx(
<g/>
lokomotiva	lokomotiva	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
neužitečnou	užitečný	k2eNgFnSc4d1
zátěž	zátěž	k1gFnSc4
zvyšující	zvyšující	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
klasického	klasický	k2eAgInSc2d1
vlaku	vlak	k1gInSc2
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
jsou	být	k5eAaImIp3nP
provozně	provozně	k6eAd1
levnější	levný	k2eAgFnPc1d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
zvýšení	zvýšení	k1gNnSc1
poměrné	poměrný	k2eAgFnSc2d1
adhezní	adhezní	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
</s>
<s>
zvýšení	zvýšení	k1gNnSc1
podílu	podíl	k1gInSc2
rekuperačního	rekuperační	k2eAgNnSc2d1
brzdění	brzdění	k1gNnSc2
(	(	kIx(
<g/>
elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
při	při	k7c6
pohonu	pohon	k1gInSc6
50	#num#	k4
%	%	kIx~
a	a	k8xC
více	hodně	k6eAd2
dvojkolí	dvojkolí	k1gNnPc1
brzděna	brzdit	k5eAaImNgNnP
převážně	převážně	k6eAd1
pomocí	pomocí	k7c2
elektrodynamického	elektrodynamický	k2eAgNnSc2d1
rekuperačního	rekuperační	k2eAgNnSc2d1
brzdění	brzdění	k1gNnSc2
a	a	k8xC
rekuperací	rekuperace	k1gFnPc2
energie	energie	k1gFnSc2
snižovat	snižovat	k5eAaImF
náklady	náklad	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
zvýšení	zvýšení	k1gNnSc1
provozní	provozní	k2eAgFnSc2d1
spolehlivosti	spolehlivost	k1gFnSc2
(	(	kIx(
<g/>
konstrukce	konstrukce	k1gFnSc1
ucelených	ucelený	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
umožňuje	umožňovat	k5eAaImIp3nS
zdvojení	zdvojení	k1gNnSc1
<g/>
,	,	kIx,
či	či	k8xC
dokonce	dokonce	k9
vyšší	vysoký	k2eAgNnSc4d2
zmnožení	zmnožení	k1gNnSc4
<g/>
,	,	kIx,
trakčních	trakční	k2eAgInPc2d1
i	i	k8xC
pomocných	pomocný	k2eAgInPc2d1
agregátů	agregát	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
aerodynamika	aerodynamika	k1gFnSc1
(	(	kIx(
<g/>
ucelená	ucelený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
je	být	k5eAaImIp3nS
konstruována	konstruovat	k5eAaImNgFnS
jako	jako	k9
co	co	k9
nejdokonalejší	dokonalý	k2eAgInSc1d3
aerodynamický	aerodynamický	k2eAgInSc1d1
celek	celek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nevýhodami	nevýhoda	k1gFnPc7
naopak	naopak	k6eAd1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
menší	malý	k2eAgFnSc1d2
míra	míra	k1gFnSc1
operativnosti	operativnost	k1gFnSc2
při	při	k7c6
přizpůsobení	přizpůsobení	k1gNnSc6
kapacity	kapacita	k1gFnSc2
pro	pro	k7c4
očekávanou	očekávaný	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
(	(	kIx(
<g/>
kapacitu	kapacita	k1gFnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
upravovat	upravovat	k5eAaImF
v	v	k7c6
rozsahu	rozsah	k1gInSc6
celých	celý	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
nikoli	nikoli	k9
jen	jen	k9
jednotlivých	jednotlivý	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
při	při	k7c6
spojení	spojení	k1gNnSc6
více	hodně	k6eAd2
jednotek	jednotka	k1gFnPc2
nemožnost	nemožnost	k1gFnSc4
průchodu	průchod	k1gInSc2
celým	celý	k2eAgInSc7d1
vlakem	vlak	k1gInSc7
jako	jako	k9
u	u	k7c2
klasických	klasický	k2eAgInPc2d1
vozů	vůz	k1gInPc2
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
speciálně	speciálně	k6eAd1
konstruovaných	konstruovaný	k2eAgInPc2d1
typů	typ	k1gInPc2
jednotek	jednotka	k1gFnPc2
s	s	k7c7
průchozími	průchozí	k2eAgNnPc7d1
čely	čelo	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
agregáty	agregát	k1gInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
nad	nad	k7c7
nebo	nebo	k8xC
pod	pod	k7c7
prostorem	prostor	k1gInSc7
pro	pro	k7c4
cestující	cestující	k2eAgInSc4d1
(	(	kIx(
<g/>
negativní	negativní	k2eAgInSc4d1
dopad	dopad	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
hluku	hluk	k1gInSc2
a	a	k8xC
vibrací	vibrace	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
v	v	k7c6
případě	případ	k1gInSc6
závady	závada	k1gFnSc2
a	a	k8xC
opravy	oprava	k1gFnPc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
odstavit	odstavit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
(	(	kIx(
<g/>
u	u	k7c2
klasických	klasický	k2eAgFnPc2d1
souprav	souprava	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vyřadit	vyřadit	k5eAaPmF
<g/>
/	/	kIx~
<g/>
nahradit	nahradit	k5eAaPmF
jen	jen	k9
poškozený	poškozený	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
/	/	kIx~
<g/>
lokomotivu	lokomotiva	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
440	#num#	k4
<g/>
,	,	kIx,
451	#num#	k4
<g/>
,	,	kIx,
452	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
460	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
470	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
471	#num#	k4
<g/>
,	,	kIx,
480	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
560	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
640	#num#	k4
<g/>
,	,	kIx,
650	#num#	k4
<g/>
,	,	kIx,
660	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
680	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
AVE	ave	k1gNnSc2
S-	S-	k1gFnSc2
<g/>
100	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Acela	Acela	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Alaris	Alaris	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
EN	EN	kA
<g/>
57	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Eurostar	Eurostar	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
ICE	ICE	kA
1	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
ICE	ICE	kA
2	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
ICE	ICE	kA
3	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Pendolino	Pendolin	k2eAgNnSc1d1
<g/>
,	,	kIx,
</s>
<s>
TGV	TGV	kA
<g/>
,	,	kIx,
</s>
<s>
AGV	AGV	kA
<g/>
,	,	kIx,
</s>
<s>
Talgo	Talgo	k6eAd1
350	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
vůz	vůz	k1gInSc1
</s>
<s>
Motorový	motorový	k2eAgInSc1d1
vůz	vůz	k1gInSc1
</s>
<s>
Motorová	motorový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Ucelený	ucelený	k2eAgInSc1d1
vlak	vlak	k1gInSc1
</s>
<s>
Přezdívky	přezdívka	k1gFnPc1
českých	český	k2eAgMnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgMnPc2d1
elektrických	elektrický	k2eAgMnPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
jednotek	jednotka	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
elektrická	elektrický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
Atlasu	Atlas	k1gInSc6
lokomotiv	lokomotiva	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
ZLINSKÝ	ZLINSKÝ	kA
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrické	elektrický	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
na	na	k7c6
našich	náš	k3xOp1gFnPc6
kolejích	kolej	k1gFnPc6
<g/>
:	:	kIx,
něco	něco	k3yInSc1
málo	málo	k6eAd1
úvodem	úvod	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlaky	vlak	k1gInPc4
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
2009-03-06	2009-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ŠŤÁHLAVSKÝ	ŠŤÁHLAVSKÝ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlíkové	Rychlíková	k1gFnPc4
elektrické	elektrický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
evropský	evropský	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
válcuje	válcovat	k5eAaImIp3nS
klasiku	klasika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železničář	železničář	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČD	ČD	kA
<g/>
,	,	kIx,
2015-11-19	2015-11-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
