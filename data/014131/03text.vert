<s>
The	The	k?
Music	Music	k1gMnSc1
of	of	k?
Smash	Smash	k1gMnSc1
</s>
<s>
The	The	k?
Music	Musice	k1gFnPc2
of	of	k?
SmashInterpretSmashDruh	SmashInterpretSmashDruha	k1gFnPc2
albaSoundtrackVydáno	albaSoundtrackVydat	k5eAaPmNgNnS,k5eAaImNgNnS
<g/>
1	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2012	#num#	k4
<g/>
Nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
<g/>
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
ŽánrPopJazykangličtinaVydavatelstvíColumbia	ŽánrPopJazykangličtinaVydavatelstvíColumbia	k1gFnSc1
RecordsProducentiMarc	RecordsProducentiMarc	k1gFnSc1
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
Wittman	Wittman	k1gMnSc1
<g/>
,	,	kIx,
Ryan	Ryan	k1gInSc4
TedderNěkterá	TedderNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Music	Music	k1gMnSc1
of	of	k?
Smash	Smash	k1gMnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgNnSc4
soundtrackové	soundtrackový	k2eAgNnSc4d1
album	album	k1gNnSc4
z	z	k7c2
amerického	americký	k2eAgInSc2d1
muzikálového	muzikálový	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Smash	Smasha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšlo	vyjít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
přes	přes	k7c4
vydavatelství	vydavatelství	k1gNnSc4
Columbia	Columbia	k1gFnSc1
Records	Recordsa	k1gFnPc2
a	a	k8xC
za	za	k7c4
první	první	k4xOgInSc4
týden	týden	k1gInSc4
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
39	#num#	k4
000	#num#	k4
alb	album	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
16	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
přes	přes	k7c4
59	#num#	k4
000	#num#	k4
kopií	kopie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
albu	album	k1gNnSc6
</s>
<s>
Album	album	k1gNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
ve	v	k7c4
standardní	standardní	k2eAgFnPc4d1
i	i	k8xC
deluxe	deluxe	k1gFnPc4
(	(	kIx(
<g/>
rozšířenou	rozšířený	k2eAgFnSc4d1
<g/>
)	)	kIx)
verzi	verze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardní	standardní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
třináct	třináct	k4xCc4
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
složeny	složit	k5eAaPmNgFnP
z	z	k7c2
šesti	šest	k4xCc2
cover	covra	k1gFnPc2
verzí	verze	k1gFnPc2
a	a	k8xC
sedmi	sedm	k4xCc2
původních	původní	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
(	(	kIx(
<g/>
všechny	všechen	k3xTgFnPc4
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
Marc	Marc	k1gInSc4
Shaiman	Shaiman	k1gMnSc1
a	a	k8xC
Scott	Scott	k1gMnSc1
Wittman	Wittman	k1gMnSc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
písně	píseň	k1gFnSc2
"	"	kIx"
<g/>
Touch	Touch	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
složili	složit	k5eAaPmAgMnP
Ryan	Ryan	k1gMnSc1
Tedder	Tedder	k1gMnSc1
<g/>
,	,	kIx,
Brent	Brent	k?
Kutzle	Kutzle	k1gFnSc1
<g/>
,	,	kIx,
Bonnie	Bonnie	k1gFnSc1
McKee	McKee	k1gFnSc1
a	a	k8xC
Noel	Noel	k1gMnSc1
Zancanella	Zancanella	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deluxe	Deluxe	k1gFnSc1
verze	verze	k1gFnSc1
se	se	k3xPyFc4
prodává	prodávat	k5eAaImIp3nS
na	na	k7c6
internetových	internetový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
společnosti	společnost	k1gFnSc2
Target	Target	k1gInSc1
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
ještě	ještě	k9
pět	pět	k4xCc1
bonusových	bonusový	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
dohromady	dohromady	k6eAd1
osmnáct	osmnáct	k4xCc1
<g/>
.	.	kIx.
</s>
<s>
Tracklist	Tracklist	k1gMnSc1
</s>
<s>
TrackNázevAutořiPůvodní	TrackNázevAutořiPůvodní	k2eAgInSc1d1
interpretV	interpretV	k?
seriálu	seriál	k1gInSc2
zpíváDélka	zpíváDélek	k1gMnSc2
</s>
<s>
1	#num#	k4
<g/>
"	"	kIx"
<g/>
Touch	Touch	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Ryan	Ryan	k1gMnSc1
Tedder	Tedder	k1gMnSc1
<g/>
,	,	kIx,
Brent	Brent	k?
Kutzle	Kutzle	k1gFnSc1
<g/>
,	,	kIx,
Bonnie	Bonnie	k1gFnSc1
McKee	McKee	k1gFnSc1
<g/>
,	,	kIx,
Noel	Noel	k1gInSc1
ZancanellaPůvodní	ZancanellaPůvodní	k2eAgInSc1d1
skladbaKatharine	skladbaKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
"	"	kIx"
<g/>
Stand	Standa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
Michael	Michael	k1gMnSc1
More	mor	k1gInSc5
<g/>
,	,	kIx,
Frederick	Frederick	k1gMnSc1
B.	B.	kA
WilliamsDonnie	WilliamsDonnie	k1gFnSc1
McClurkinKatharine	McClurkinKatharin	k1gInSc5
McPhee	McPhee	k1gFnSc1
a	a	k8xC
Leslie	Leslie	k1gFnSc1
Odom	Odomo	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
"	"	kIx"
<g/>
Who	Who	k1gMnSc1
You	You	k1gMnSc1
Are	ar	k1gInSc5
<g/>
"	"	kIx"
<g/>
Jessica	Jessicum	k1gNnPc1
Cornish	Cornisha	k1gFnPc2
<g/>
,	,	kIx,
Toby	Tob	k2eAgFnPc4d1
Gad	Gad	k1gFnPc4
<g/>
,	,	kIx,
Shelly	Shella	k1gFnPc4
PeikenJessie	PeikenJessie	k1gFnSc2
JMegan	JMegany	k1gInPc2
Hilty	Hilta	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
"	"	kIx"
<g/>
Crazy	Craza	k1gFnSc2
Dreams	Dreams	k1gInSc1
<g/>
"	"	kIx"
<g/>
Carrie	Carrie	k1gFnSc1
Underwood	underwood	k1gInSc1
<g/>
,	,	kIx,
George	George	k1gInSc1
Barry	Barra	k1gFnSc2
Dean	Deana	k1gFnPc2
<g/>
,	,	kIx,
Troy	Tro	k2eAgFnPc1d1
VergesCarrie	VergesCarrie	k1gFnPc1
UnderwoodMegan	UnderwoodMegan	k1gInSc1
Hilty	Hilta	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
"	"	kIx"
<g/>
Beautiful	Beautiful	k1gInSc1
<g/>
"	"	kIx"
<g/>
Linda	Linda	k1gFnSc1
PerryChristina	PerryChristina	k1gFnSc1
AguileraKatharine	AguileraKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
"	"	kIx"
<g/>
Haven	Havna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
t	t	k?
Met	met	k1gInSc1
You	You	k1gMnSc1
Yet	Yet	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Michael	Michael	k1gMnSc1
Bublé	Bubl	k1gMnPc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
Chang	Chang	k1gMnSc1
<g/>
,	,	kIx,
Amy	Amy	k1gMnSc1
Foster-GillesMichael	Foster-GillesMichael	k1gMnSc1
BubléNick	BubléNick	k1gMnSc1
Jonas	Jonas	k1gMnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
"	"	kIx"
<g/>
Shake	Shak	k1gFnSc2
It	It	k1gMnSc4
Out	Out	k1gMnSc4
<g/>
"	"	kIx"
<g/>
Florence	Florenc	k1gFnSc2
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
EpworthFlorence	EpworthFlorence	k1gFnSc2
and	and	k?
the	the	k?
MachineKatharine	MachineKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
"	"	kIx"
<g/>
Brighter	Brighter	k1gMnSc1
Than	Than	k1gMnSc1
the	the	k?
Sun	Sun	kA
<g/>
"	"	kIx"
<g/>
Colbie	Colbie	k1gFnPc1
Caillat	Caille	k1gNnPc2
<g/>
,	,	kIx,
TedderColbie	TedderColbie	k1gFnSc1
CaillatKatharine	CaillatKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
"	"	kIx"
<g/>
Let	léto	k1gNnPc2
Me	Me	k1gFnPc2
Be	Be	k1gFnSc1
Your	Your	k1gInSc1
Star	Star	kA
<g/>
"	"	kIx"
<g/>
Marc	Marc	k1gFnSc1
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
WittmanPůvodní	WittmanPůvodní	k2eAgFnSc2d1
skladbaKatharine	skladbaKatharin	k1gInSc5
McPhee	McPhee	k1gInSc1
a	a	k8xC
Megan	Megan	k1gInSc1
Hilty	Hilta	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
"	"	kIx"
<g/>
The	The	k1gFnSc1
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
Fox	fox	k1gInSc1
Mambo	Mamba	k1gFnSc5
<g/>
"	"	kIx"
<g/>
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
WittmanPůvodní	WittmanPůvodní	k2eAgMnSc1d1
skladbaKatharine	skladbaKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
"	"	kIx"
<g/>
Mr	Mr	k1gFnSc1
<g/>
.	.	kIx.
&	&	k?
Mrs	Mrs	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smith	Smith	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
WittmanPůvodní	WittmanPůvodní	k2eAgMnSc1d1
skladbaMegan	skladbaMegan	k1gMnSc1
Hilty	Hilta	k1gFnSc2
a	a	k8xC
Will	Willa	k1gFnPc2
Chase	chasa	k1gFnSc6
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
"	"	kIx"
<g/>
Let	léto	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Be	Be	k1gFnSc7
Bad	Bad	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
WittmanPůvodní	WittmanPůvodní	k2eAgMnSc1d1
skladbaMegan	skladbaMegan	k1gMnSc1
Hilty	Hilta	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
"	"	kIx"
<g/>
History	Histor	k1gMnPc4
Is	Is	k1gFnSc1
Made	Mad	k1gFnSc2
at	at	k?
Night	Night	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Shaiman	Shaiman	k1gMnSc1
<g/>
,	,	kIx,
WittmanPůvodní	WittmanPůvodní	k2eAgMnSc1d1
skladbaMegan	skladbaMegan	k1gMnSc1
Hilty	Hilta	k1gFnSc2
a	a	k8xC
Will	Willa	k1gFnPc2
Chase	chasa	k1gFnSc6
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
</s>
<s>
Target	Target	k1gInSc1
Exclusive	Exclusiev	k1gFnSc2
Tracks	Tracksa	k1gFnPc2
</s>
<s>
TrackNázevAutořiPůvodní	TrackNázevAutořiPůvodní	k2eAgInSc1d1
interpretV	interpretV	k?
seriálu	seriál	k1gInSc2
zpíváDélka	zpíváDélek	k1gMnSc2
</s>
<s>
14	#num#	k4
<g/>
"	"	kIx"
<g/>
September	September	k1gInSc1
Song	song	k1gInSc1
<g/>
"	"	kIx"
<g/>
Kurt	Kurt	k1gMnSc1
Weill	Weill	k1gMnSc1
<g/>
,	,	kIx,
Maxwell	maxwell	k1gInSc1
AndersonKnickerbocker	AndersonKnickerbocker	k1gMnSc1
HolidayAnjelica	HolidayAnjelica	k1gMnSc1
Huston	Huston	k1gInSc4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
"	"	kIx"
<g/>
Our	Our	k1gMnSc1
Day	Day	k1gMnSc2
Will	Will	k1gMnSc1
Come	Com	k1gMnSc2
<g/>
"	"	kIx"
<g/>
Bob	Bob	k1gMnSc1
Hilliard	Hilliard	k1gMnSc1
<g/>
,	,	kIx,
Mort	Mort	k1gMnSc1
GarsonRuby	GarsonRuba	k1gFnSc2
&	&	k?
the	the	k?
RomanticsKatharine	RomanticsKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
"	"	kIx"
<g/>
Everything	Everything	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Coming	Coming	k1gInSc1
Up	Up	k1gMnSc1
Roses	Roses	k1gMnSc1
<g/>
"	"	kIx"
<g/>
Stephen	Stephen	k1gInSc1
Sondheim	Sondheimo	k1gNnPc2
<g/>
,	,	kIx,
Jule	Jula	k1gFnSc3
Stynemuzikál	Stynemuzikála	k1gFnPc2
GypsyBernadette	GypsyBernadett	k1gInSc5
Peters	Peters	k1gInSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
"	"	kIx"
<g/>
Breakaway	Breakawaa	k1gFnSc2
<g/>
"	"	kIx"
<g/>
Avril	Avril	k1gInSc1
Lavigne	Lavign	k1gInSc5
<g/>
,	,	kIx,
Bridget	Bridget	k1gInSc1
Benenate	Benenat	k1gInSc5
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
GerrardKelly	GerrardKella	k1gFnSc2
ClarksonMegan	ClarksonMegan	k1gMnSc1
Hilty	Hilta	k1gFnSc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
"	"	kIx"
<g/>
Run	Runa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
Gary	Gar	k2eAgFnPc1d1
Lightbody	Lightboda	k1gFnPc1
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
Quinn	Quinn	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
McClelland	McClelland	k1gInSc1
<g/>
,	,	kIx,
Nathan	Nathan	k1gMnSc1
Connolly	Connolla	k1gFnSc2
<g/>
,	,	kIx,
Iain	Iain	k1gMnSc1
ArcherSnow	ArcherSnow	k1gMnSc1
PatrolKatharine	PatrolKatharin	k1gInSc5
McPhee	McPhee	k1gNnPc2
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
hitparádách	hitparáda	k1gFnPc6
</s>
<s>
Hitparáda	hitparáda	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejvyšší	vysoký	k2eAgNnSc1d3
umístění	umístění	k1gNnSc1
</s>
<s>
Canadian	Canadian	k1gInSc1
Albums	Albumsa	k1gFnPc2
Chart	charta	k1gFnPc2
</s>
<s>
35	#num#	k4
</s>
<s>
U.	U.	kA
<g/>
S.	S.	kA
Billboard	billboard	k1gInSc4
200	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
U.	U.	kA
<g/>
S.	S.	kA
Billboard	billboard	k1gInSc4
soundtracky	soundtrack	k1gInPc7
</s>
<s>
1	#num#	k4
</s>
<s>
U.	U.	kA
<g/>
S.	S.	kA
Billboard	billboard	k1gInSc4
digitální	digitální	k2eAgInSc1d1
alba	album	k1gNnPc4
</s>
<s>
9	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Smash	Smash	k1gMnSc1
Tvůrci	tvůrce	k1gMnPc1
</s>
<s>
Steven	Steven	k2eAgMnSc1d1
Spielberg	Spielberg	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Greenblatt	Greenblatt	k1gMnSc1
•	•	k?
Theresa	Theresa	k1gFnSc1
Rebeck	Rebeck	k1gInSc1
•	•	k?
Marc	Marc	k1gInSc1
Shaiman	Shaiman	k1gMnSc1
•	•	k?
Scott	Scott	k1gMnSc1
Wittman	Wittman	k1gMnSc1
•	•	k?
Josh	Josh	k1gMnSc1
Safran	Safran	k1gInSc4
Herci	herec	k1gMnSc3
</s>
<s>
Debra	Debra	k6eAd1
Messing	Messing	k1gInSc1
•	•	k?
Jack	Jack	k1gInSc1
Davenport	Davenport	k1gInSc1
•	•	k?
Katharine	Katharin	k1gInSc5
McPhee	McPhee	k1gNnPc7
•	•	k?
Christian	Christian	k1gMnSc1
Borle	Borle	k1gFnSc2
•	•	k?
Megan	Megan	k1gInSc1
Hilty	Hilta	k1gMnSc2
•	•	k?
Raza	Razus	k1gMnSc2
Jaffrey	Jaffrea	k1gMnSc2
•	•	k?
Brian	Brian	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arcy	Arca	k1gFnSc2
James	James	k1gMnSc1
•	•	k?
Anjelica	Anjelica	k1gFnSc1
Huston	Huston	k1gInSc1
•	•	k?
Krysta	Krysta	k1gMnSc1
Rodriguez	Rodriguez	k1gMnSc1
•	•	k?
Jeremy	Jerema	k1gFnSc2
Jordan	Jordan	k1gMnSc1
•	•	k?
Andy	Anda	k1gFnSc2
Mientus	Mientus	k1gMnSc1
•	•	k?
Leslie	Leslie	k1gFnSc2
Odom	Odoma	k1gFnPc2
mladší	mladý	k2eAgMnPc1d2
Hosté	host	k1gMnPc1
</s>
<s>
Will	Wilnout	k5eAaPmAgMnS
Chase	chasa	k1gFnSc3
•	•	k?
Michael	Michael	k1gMnSc1
Cristofer	Cristofer	k1gMnSc1
•	•	k?
Wesley	Weslea	k1gFnSc2
Taylor	Taylor	k1gMnSc1
•	•	k?
Thorsten	Thorsten	k2eAgMnSc1d1
Kaye	Kaye	k1gFnSc7
•	•	k?
Uma	uma	k1gFnSc1
Thurman	Thurman	k1gMnSc1
•	•	k?
Nick	Nick	k1gMnSc1
Jonas	Jonas	k1gMnSc1
•	•	k?
Bernadette	Bernadett	k1gInSc5
Peters	Petersa	k1gFnPc2
•	•	k?
Grace	Grace	k1gMnSc1
Gummer	Gummer	k1gMnSc1
•	•	k?
Nikki	Nikk	k1gFnSc2
Blonsky	Blonsko	k1gNnPc7
•	•	k?
Daniel	Daniel	k1gMnSc1
Sunjata	Sunjat	k1gMnSc2
•	•	k?
Jennifer	Jennifer	k1gMnSc1
Hudson	Hudson	k1gMnSc1
•	•	k?
Sean	Sean	k1gMnSc1
Hayes	Hayes	k1gMnSc1
•	•	k?
Liza	Liza	k1gMnSc1
Minnelli	Minnell	k1gMnSc3
•	•	k?
Neal	Neal	k1gMnSc1
Bledsoe	Bledso	k1gFnSc2
•	•	k?
Ann	Ann	k1gFnSc1
Harada	Harada	k1gFnSc1
•	•	k?
Jesse	Jesse	k1gFnSc2
L.	L.	kA
Martin	Martin	k1gMnSc1
•	•	k?
Daphne	Daphne	k1gMnSc1
Rubin-Vega	Rubin-Veg	k1gMnSc2
•	•	k?
Harvey	Harvea	k1gFnSc2
Fierstein	Fierstein	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gMnSc1
Tedder	Tedder	k1gMnSc1
•	•	k?
Jaime	Jaim	k1gInSc5
Cepero	Cepero	k1gNnSc4
•	•	k?
Mara	Mara	k1gFnSc1
Davi	Dav	k1gFnSc2
Epizody	epizoda	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
Pilot	pilot	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Callback	Callback	k1gMnSc1
•	•	k?
Enter	Enter	k1gMnSc1
Mr	Mr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DiMaggio	DiMaggio	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Cost	Cost	k1gMnSc1
of	of	k?
Art	Art	k1gMnSc1
•	•	k?
Let	léto	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Be	Be	k1gMnSc7
Bad	Bad	k1gMnSc7
•	•	k?
Chemistry	Chemistr	k1gMnPc4
•	•	k?
The	The	k1gFnSc1
Workshop	workshop	k1gInSc1
•	•	k?
The	The	k1gFnSc1
Coup	coup	k1gInSc1
•	•	k?
Hell	Hell	k1gInSc4
on	on	k3xPp3gMnSc1
Earth	Earth	k1gMnSc1
•	•	k?
Understudy	Understuda	k1gFnSc2
•	•	k?
The	The	k1gFnSc2
Movie	Movie	k1gFnSc2
Star	Star	kA
•	•	k?
Publicity	publicita	k1gFnSc2
•	•	k?
Tech	Tech	k?
•	•	k?
Previews	Previews	k1gInSc1
•	•	k?
Bombshell	Bombshell	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
řada	řada	k1gFnSc1
</s>
<s>
On	on	k3xPp3gMnSc1
Broadway	Broadway	k1gInPc1
•	•	k?
The	The	k1gFnSc2
Fallout	Fallout	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Dramaturg	dramaturg	k1gMnSc1
•	•	k?
The	The	k1gFnSc1
Song	song	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Read-Through	Read-Through	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Fringe	Fring	k1gFnSc2
•	•	k?
Musical	musical	k1gInSc1
Chairs	Chairs	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Bells	Bellsa	k1gFnPc2
and	and	k?
Whistles	Whistles	k1gMnSc1
•	•	k?
The	The	k1gFnSc1
Parents	Parents	k1gInSc1
•	•	k?
The	The	k1gFnSc1
Surprise	Surprise	k1gFnSc1
Party	party	k1gFnSc1
•	•	k?
The	The	k1gFnSc2
Dress	Dressa	k1gFnPc2
Rehearsal	Rehearsal	k1gMnSc1
•	•	k?
Opening	Opening	k1gInSc1
Night	Night	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Producers	Producersa	k1gFnPc2
•	•	k?
The	The	k1gMnSc1
Phenomenon	Phenomenon	k1gMnSc1
•	•	k?
The	The	k1gFnSc1
Transfer	transfer	k1gInSc1
•	•	k?
The	The	k1gFnSc1
Nominations	Nominations	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Tonys	Tonysa	k1gFnPc2
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
písní	píseň	k1gFnPc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Smash	Smash	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Music	Music	k1gMnSc1
of	of	k?
Smash	Smash	k1gMnSc1
•	•	k?
Bombshell	Bombshell	k1gMnSc1
•	•	k?
Never	Never	k1gMnSc1
Give	Giv	k1gFnSc2
All	All	k1gMnSc1
the	the	k?
Heart	Heart	k1gInSc1
•	•	k?
The	The	k1gMnSc5
National	National	k1gMnSc5
Pastime	Pastim	k1gMnSc5
•	•	k?
Let	léto	k1gNnPc2
Me	Me	k1gMnPc2
Be	Be	k1gFnSc2
Your	Your	k1gMnSc1
Star	Star	kA
•	•	k?
History	Histor	k1gInPc1
is	is	k?
Made	Mad	k1gInSc2
at	at	k?
Night	Night	k1gMnSc1
•	•	k?
I	i	k9
Never	Never	k1gMnSc1
Met	met	k1gInSc1
a	a	k8xC
Wolf	Wolf	k1gMnSc1
Who	Who	k1gMnSc1
Didn	Didn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Love	lov	k1gInSc5
to	ten	k3xDgNnSc4
Howl	Howl	k1gMnSc1
•	•	k?
Touch	Touch	k1gMnSc1
Me	Me	k1gMnSc1
•	•	k?
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Forget	Forget	k1gMnSc1
Me	Me	k1gMnSc1
•	•	k?
I	i	k9
Heard	Heard	k1gMnSc1
Your	Your	k1gMnSc1
Voice	Voice	k1gMnSc1
In	In	k1gMnSc1
a	a	k8xC
Dream	Dream	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
