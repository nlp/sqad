<s>
Zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
ZZS	ZZS	kA	ZZS
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
odbornou	odborný	k2eAgFnSc4d1	odborná
přednemocniční	přednemocniční	k2eAgFnSc4d1	přednemocniční
neodkladnou	odkladný	k2eNgFnSc4d1	neodkladná
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Tísňová	tísňový	k2eAgFnSc1d1	tísňová
linka	linka	k1gFnSc1	linka
ZZS	ZZS	kA	ZZS
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
číslo	číslo	k1gNnSc1	číslo
155	[number]	k4	155
<g/>
.	.	kIx.	.
</s>
<s>
Přednemocniční	přednemocniční	k2eAgFnSc1d1	přednemocniční
neodkladná	odkladný	k2eNgFnSc1d1	neodkladná
péče	péče	k1gFnSc1	péče
(	(	kIx(	(
<g/>
PNP	PNP	kA	PNP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
postižené	postižený	k1gMnPc4	postižený
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
jejich	jejich	k3xOp3gInSc2	jejich
úrazu	úraz	k1gInSc2	úraz
nebo	nebo	k8xC	nebo
náhlého	náhlý	k2eAgNnSc2d1	náhlé
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jejich	jejich	k3xOp3gInSc2	jejich
transportu	transport	k1gInSc2	transport
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
odbornému	odborný	k2eAgNnSc3d1	odborné
ošetření	ošetření	k1gNnSc3	ošetření
a	a	k8xC	a
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
předání	předání	k1gNnSc6	předání
do	do	k7c2	do
zdravotnického	zdravotnický	k2eAgNnSc2d1	zdravotnické
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
PNP	PNP	kA	PNP
je	být	k5eAaImIp3nS	být
poskytována	poskytován	k2eAgFnSc1d1	poskytována
při	při	k7c6	při
stavech	stav	k1gInPc6	stav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
<g/>
:	:	kIx,	:
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
život	život	k1gInSc4	život
postiženého	postižený	k1gMnSc4	postižený
způsobí	způsobit	k5eAaPmIp3nS	způsobit
bez	bez	k7c2	bez
rychlého	rychlý	k2eAgNnSc2d1	rychlé
poskytnutí	poskytnutí	k1gNnSc2	poskytnutí
odborné	odborný	k2eAgFnSc2d1	odborná
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
trvalé	trvalý	k2eAgInPc1d1	trvalý
následky	následek	k1gInPc1	následek
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
prohlubováním	prohlubování	k1gNnSc7	prohlubování
chorobných	chorobný	k2eAgFnPc2d1	chorobná
změn	změna	k1gFnPc2	změna
k	k	k7c3	k
náhlé	náhlý	k2eAgFnSc3d1	náhlá
smrti	smrt	k1gFnSc3	smrt
působí	působit	k5eAaImIp3nS	působit
náhlé	náhlý	k2eAgNnSc4d1	náhlé
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
působí	působit	k5eAaImIp3nP	působit
změny	změna	k1gFnPc1	změna
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
postiženého	postižený	k1gMnSc4	postižený
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
Přednemocniční	přednemocniční	k2eAgFnSc7d1	přednemocniční
neodkladnou	odkladný	k2eNgFnSc7d1	neodkladná
<g />
.	.	kIx.	.
</s>
<s>
péči	péče	k1gFnSc4	péče
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
výjezdové	výjezdový	k2eAgFnPc1d1	výjezdová
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
rychlá	rychlý	k2eAgFnSc1d1	rychlá
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
pomoc	pomoc	k1gFnSc1	pomoc
(	(	kIx(	(
<g/>
RZP	RZP	kA	RZP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
dvoučlenná	dvoučlenný	k2eAgFnSc1d1	dvoučlenná
posádka	posádka	k1gFnSc1	posádka
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
řidič-záchranář	řidičáchranář	k1gInSc4	řidič-záchranář
a	a	k8xC	a
zdravotnický	zdravotnický	k2eAgMnSc1d1	zdravotnický
záchranář	záchranář	k1gMnSc1	záchranář
rychlá	rychlý	k2eAgFnSc1d1	rychlá
lékařská	lékařský	k2eAgFnSc1d1	lékařská
pomoc	pomoc	k1gFnSc1	pomoc
(	(	kIx(	(
<g/>
RLP	RLP	kA	RLP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
tříčlennou	tříčlenný	k2eAgFnSc7d1	tříčlenná
posádkou	posádka	k1gFnSc7	posádka
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
řidič-záchranář	řidičáchranář	k1gMnSc1	řidič-záchranář
<g/>
,	,	kIx,	,
zdravotnický	zdravotnický	k2eAgMnSc1d1	zdravotnický
záchranář	záchranář	k1gMnSc1	záchranář
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
1	[number]	k4	1
atestace	atestace	k1gFnSc2	atestace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
chirurgie	chirurgie	k1gFnSc2	chirurgie
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
,	,	kIx,	,
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
,	,	kIx,	,
anesteziologie	anesteziologie	k1gFnSc1	anesteziologie
a	a	k8xC	a
resuscitace	resuscitace	k1gFnSc1	resuscitace
nebo	nebo	k8xC	nebo
pediatrie	pediatrie	k1gFnSc1	pediatrie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
atestace	atestace	k1gFnSc1	atestace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
urgentní	urgentní	k2eAgFnSc1d1	urgentní
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
rychlá	rychlý	k2eAgFnSc1d1	rychlá
lékařská	lékařský	k2eAgFnSc1d1	lékařská
pomoc	pomoc	k1gFnSc1	pomoc
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Rendez-Vous	rendezous	k1gNnSc2	rendez-vous
(	(	kIx(	(
<g/>
RV	RV	kA	RV
<g/>
)	)	kIx)	)
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
nejméně	málo	k6eAd3	málo
dvoučlennou	dvoučlenný	k2eAgFnSc7d1	dvoučlenná
posádkou	posádka	k1gFnSc7	posádka
ve	v	k7c4	v
složení	složení	k1gNnSc4	složení
řidič-záchranář	řidičáchranář	k1gMnSc1	řidič-záchranář
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
výjezdovými	výjezdový	k2eAgFnPc7d1	výjezdová
skupinami	skupina	k1gFnPc7	skupina
rychlé	rychlý	k2eAgFnSc2d1	rychlá
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
pomoci	pomoc	k1gFnSc2	pomoc
ve	v	k7c6	v
víceúrovňovém	víceúrovňový	k2eAgInSc6d1	víceúrovňový
setkávacím	setkávací	k2eAgInSc6d1	setkávací
systému	systém	k1gInSc6	systém
letecká	letecký	k2eAgFnSc1d1	letecká
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
LZS	LZS	kA	LZS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
část	část	k1gFnSc1	část
osádky	osádka	k1gFnSc2	osádka
nejméně	málo	k6eAd3	málo
dvoučlenná	dvoučlenný	k2eAgFnSc1d1	dvoučlenná
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
zdravotnický	zdravotnický	k2eAgMnSc1d1	zdravotnický
záchranář	záchranář	k1gMnSc1	záchranář
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
výjezdových	výjezdový	k2eAgFnPc2d1	výjezdová
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
operátora	operátor	k1gMnSc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
reálně	reálně	k6eAd1	reálně
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebo	nebo	k8xC	nebo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
základních	základní	k2eAgFnPc2d1	základní
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
události	událost	k1gFnSc2	událost
vysílána	vysílán	k2eAgFnSc1d1	vysílána
posádka	posádka	k1gFnSc1	posádka
RLP	RLP	kA	RLP
nebo	nebo	k8xC	nebo
LZS	LZS	kA	LZS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pacient	pacient	k1gMnSc1	pacient
kvalifikovaně	kvalifikovaně	k6eAd1	kvalifikovaně
ošetřen	ošetřen	k2eAgInSc4d1	ošetřen
posádkou	posádka	k1gFnSc7	posádka
RZP	RZP	kA	RZP
<g/>
.	.	kIx.	.
</s>
<s>
PNP	PNP	kA	PNP
je	být	k5eAaImIp3nS	být
garantována	garantovat	k5eAaBmNgFnS	garantovat
státem	stát	k1gInSc7	stát
a	a	k8xC	a
hrazena	hradit	k5eAaImNgFnS	hradit
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dislokováno	dislokovat	k5eAaBmNgNnS	dislokovat
celkem	celkem	k6eAd1	celkem
503	[number]	k4	503
výjezdových	výjezdový	k2eAgFnPc2d1	výjezdová
skupin	skupina	k1gFnPc2	skupina
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
rozmístěných	rozmístěný	k2eAgFnPc2d1	rozmístěná
na	na	k7c6	na
280	[number]	k4	280
výjezdových	výjezdový	k2eAgNnPc6d1	výjezdové
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
skupin	skupina	k1gFnPc2	skupina
leteckých	letecký	k2eAgFnPc2d1	letecká
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
Asociace	asociace	k1gFnSc2	asociace
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
záchranných	záchranný	k2eAgFnPc2d1	záchranná
služeb	služba	k1gFnPc2	služba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
provedeno	provést	k5eAaPmNgNnS	provést
celkem	celek	k1gInSc7	celek
851	[number]	k4	851
289	[number]	k4	289
výjezdů	výjezd	k1gInPc2	výjezd
posádek	posádka	k1gFnPc2	posádka
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
záchranných	záchranný	k2eAgFnPc2d1	záchranná
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
výjezdů	výjezd	k1gInPc2	výjezd
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
výjezdů	výjezd	k1gInPc2	výjezd
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
posádky	posádka	k1gFnPc1	posádka
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
záchranných	záchranný	k2eAgFnPc2d1	záchranná
služeb	služba	k1gFnPc2	služba
přibližně	přibližně	k6eAd1	přibližně
788	[number]	k4	788
tisíc	tisíc	k4xCgInPc2	tisíc
výjezdů	výjezd	k1gInPc2	výjezd
<g/>
.	.	kIx.	.
zdravotnická	zdravotnický	k2eAgNnPc1d1	zdravotnické
operační	operační	k2eAgNnPc1d1	operační
střediska	středisko	k1gNnPc1	středisko
–	–	k?	–
kvalifikovaný	kvalifikovaný	k2eAgInSc1d1	kvalifikovaný
příjem	příjem	k1gInSc1	příjem
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
,	,	kIx,	,
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
a	a	k8xC	a
předaní	předanit	k5eAaPmIp3nS	předanit
tísňových	tísňový	k2eAgFnPc2d1	tísňová
výzev	výzva	k1gFnPc2	výzva
doprava	doprava	k1gFnSc1	doprava
raněných	raněný	k1gMnPc2	raněný
<g/>
,	,	kIx,	,
nemocných	nemocný	k1gMnPc2	nemocný
a	a	k8xC	a
rodiček	rodička	k1gFnPc2	rodička
(	(	kIx(	(
<g/>
DRNR	DRNR	kA	DRNR
<g/>
)	)	kIx)	)
doprava	doprava	k1gFnSc1	doprava
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
transplantační	transplantační	k2eAgFnSc7d1	transplantační
činností	činnost	k1gFnSc7	činnost
doprava	doprava	k6eAd1	doprava
materiálů	materiál	k1gInPc2	materiál
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
radiologii	radiologie	k1gFnSc4	radiologie
<g/>
,	,	kIx,	,
vyžadujících	vyžadující	k2eAgMnPc6d1	vyžadující
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
podmínky	podmínka	k1gFnPc4	podmínka
přepravy	přeprava	k1gFnSc2	přeprava
rychlá	rychlý	k2eAgFnSc1d1	rychlá
doprava	doprava	k1gFnSc1	doprava
krve	krev	k1gFnSc2	krev
repatriační	repatriační	k2eAgFnSc2d1	repatriační
transporty	transporta	k1gFnSc2	transporta
raněných	raněný	k1gMnPc2	raněný
a	a	k8xC	a
nemocných	nemocný	k1gMnPc2	nemocný
z	z	k7c2	z
a	a	k8xC	a
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
přednemocniční	přednemocniční	k2eAgFnSc1d1	přednemocniční
neodkladná	odkladný	k2eNgFnSc1d1	neodkladná
péče	péče	k1gFnSc1	péče
při	při	k7c6	při
hromadných	hromadný	k2eAgInPc6d1	hromadný
neštěstích	neštěstí	k1gNnPc6	neštěstí
a	a	k8xC	a
katastrofách	katastrofa	k1gFnPc6	katastrofa
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
složkami	složka	k1gFnPc7	složka
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
IZS	IZS	kA	IZS
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
hasiči	hasič	k1gMnPc7	hasič
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
Horská	horský	k2eAgFnSc1d1	horská
služba	služba	k1gFnSc1	služba
výuková	výukový	k2eAgFnSc1d1	výuková
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
poskytování	poskytování	k1gNnSc6	poskytování
odborné	odborný	k2eAgFnSc2d1	odborná
přednemocniční	přednemocniční	k2eAgFnSc2d1	přednemocniční
péče	péče	k1gFnSc2	péče
Převozová	Převozový	k2eAgFnSc1d1	Převozový
služba	služba	k1gFnSc1	služba
DRNR	DRNR	kA	DRNR
(	(	kIx(	(
<g/>
doprava	doprava	k1gFnSc1	doprava
raněných	raněný	k1gMnPc2	raněný
<g/>
,	,	kIx,	,
nemocných	nemocný	k1gMnPc2	nemocný
a	a	k8xC	a
rodiček	rodička	k1gFnPc2	rodička
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pouze	pouze	k6eAd1	pouze
převozovou	převozový	k2eAgFnSc4d1	převozová
službu	služba	k1gFnSc4	služba
pacientů	pacient	k1gMnPc2	pacient
z	z	k7c2	z
domova	domov	k1gInSc2	domov
do	do	k7c2	do
zdravotnického	zdravotnický	k2eAgNnSc2d1	zdravotnické
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
zdravotnickými	zdravotnický	k2eAgNnPc7d1	zdravotnické
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
služby	služba	k1gFnSc2	služba
DRNR	DRNR	kA	DRNR
bývá	bývat	k5eAaImIp3nS	bývat
užíváno	užíván	k2eAgNnSc1d1	užíváno
sanitních	sanitní	k2eAgFnPc2d1	sanitní
vozidel	vozidlo	k1gNnPc2	vozidlo
třídy	třída	k1gFnSc2	třída
A1	A1	k1gFnSc1	A1
nebo	nebo	k8xC	nebo
A	A	kA	A
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nedisponují	disponovat	k5eNaBmIp3nP	disponovat
téměř	téměř	k6eAd1	téměř
žádným	žádný	k3yNgNnSc7	žádný
zdravotnickým	zdravotnický	k2eAgNnSc7d1	zdravotnické
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
členem	člen	k1gMnSc7	člen
posádky	posádka	k1gFnSc2	posádka
DRNR	DRNR	kA	DRNR
bývá	bývat	k5eAaImIp3nS	bývat
řidič	řidič	k1gMnSc1	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Převozovou	Převozový	k2eAgFnSc4d1	Převozový
službu	služba	k1gFnSc4	služba
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
většinou	většina	k1gFnSc7	většina
soukromé	soukromý	k2eAgFnSc2d1	soukromá
firmy	firma	k1gFnSc2	firma
nebo	nebo	k8xC	nebo
některé	některý	k3yIgFnSc2	některý
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
ji	on	k3xPp3gFnSc4	on
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
i	i	k8xC	i
samotné	samotný	k2eAgFnPc4d1	samotná
záchranné	záchranný	k2eAgFnPc4d1	záchranná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnické	zdravotnický	k2eAgFnPc1d1	zdravotnická
záchranné	záchranný	k2eAgFnPc1d1	záchranná
služby	služba	k1gFnPc1	služba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
provoz	provoz	k1gInSc1	provoz
LPS	LPS	kA	LPS
(	(	kIx(	(
<g/>
lékařské	lékařský	k2eAgFnPc1d1	lékařská
pohotovostní	pohotovostní	k2eAgFnPc1d1	pohotovostní
služby	služba	k1gFnPc1	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
LPS	LPS	kA	LPS
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
záchrannými	záchranný	k2eAgFnPc7d1	záchranná
službami	služba	k1gFnPc7	služba
celoplošně	celoplošně	k6eAd1	celoplošně
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
dělí	dělit	k5eAaImIp3nP	dělit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
regionech	region	k1gInPc6	region
se	s	k7c7	s
spádovými	spádový	k2eAgFnPc7d1	spádová
nemocnicemi	nemocnice	k1gFnPc7	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
LPS	LPS	kA	LPS
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
nezbytném	nezbytný	k2eAgInSc6d1	nezbytný
rozsahu	rozsah	k1gInSc6	rozsah
ambulantní	ambulantní	k2eAgFnSc2d1	ambulantní
péči	péče	k1gFnSc3	péče
občanům	občan	k1gMnPc3	občan
v	v	k7c6	v
případech	případ	k1gInPc6	případ
náhlého	náhlý	k2eAgNnSc2d1	náhlé
onemocnění	onemocnění	k1gNnSc2	onemocnění
nebo	nebo	k8xC	nebo
zhoršení	zhoršení	k1gNnSc2	zhoršení
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mimo	mimo	k7c4	mimo
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
provoz	provoz	k1gInSc4	provoz
ordinací	ordinace	k1gFnPc2	ordinace
praktických	praktický	k2eAgMnPc2d1	praktický
lékařů	lékař	k1gMnPc2	lékař
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
mimopracovních	mimopracovní	k2eAgInPc6d1	mimopracovní
dnech	den	k1gInPc6	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
zpoplatněna	zpoplatnit	k5eAaPmNgFnS	zpoplatnit
částkou	částka	k1gFnSc7	částka
90	[number]	k4	90
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
záchranné	záchranný	k2eAgFnPc1d1	záchranná
služby	služba	k1gFnPc1	služba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
také	také	k9	také
provoz	provoz	k1gInSc4	provoz
protialkoholních	protialkoholní	k2eAgFnPc2d1	protialkoholní
záchytných	záchytný	k2eAgFnPc2d1	záchytná
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Letecká	letecký	k2eAgFnSc1d1	letecká
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
LZS	LZS	kA	LZS
<g/>
)	)	kIx)	)
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
systému	systém	k1gInSc6	systém
poskytování	poskytování	k1gNnSc1	poskytování
přednemocniční	přednemocniční	k2eAgFnSc2d1	přednemocniční
neodkladné	odkladný	k2eNgFnSc2d1	neodkladná
péče	péče	k1gFnSc2	péče
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
sítí	síť	k1gFnSc7	síť
10	[number]	k4	10
stanic	stanice	k1gFnPc2	stanice
letecké	letecký	k2eAgFnSc2d1	letecká
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
stanic	stanice	k1gFnPc2	stanice
provozují	provozovat	k5eAaImIp3nP	provozovat
soukromí	soukromý	k2eAgMnPc1d1	soukromý
provozovatelé	provozovatel	k1gMnPc1	provozovatel
(	(	kIx(	(
<g/>
společnosti	společnost	k1gFnSc2	společnost
DSA	DSA	kA	DSA
a	a	k8xC	a
Alfa	alfa	k1gFnSc1	alfa
Helicopter	Helicoptra	k1gFnPc2	Helicoptra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
stanici	stanice	k1gFnSc4	stanice
provozuje	provozovat	k5eAaImIp3nS	provozovat
Armáda	armáda	k1gFnSc1	armáda
ČR	ČR	kA	ČR
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
Letecká	letecký	k2eAgFnSc1d1	letecká
služba	služba	k1gFnSc1	služba
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Leteckou	letecký	k2eAgFnSc4d1	letecká
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
jako	jako	k8xS	jako
takovou	takový	k3xDgFnSc4	takový
nezajišťují	zajišťovat	k5eNaImIp3nP	zajišťovat
tedy	tedy	k8xC	tedy
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
krajské	krajský	k2eAgFnPc4d1	krajská
příspěvkové	příspěvkový	k2eAgFnPc4d1	příspěvková
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgFnPc1d1	územní
záchranné	záchranný	k2eAgFnPc1d1	záchranná
služby	služba	k1gFnPc1	služba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pouze	pouze	k6eAd1	pouze
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
část	část	k1gFnSc4	část
osádky	osádka	k1gFnSc2	osádka
LZS	LZS	kA	LZS
<g/>
,	,	kIx,	,
piloti	pilot	k1gMnPc1	pilot
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
techničtí	technický	k2eAgMnPc1d1	technický
pracovníci	pracovník	k1gMnPc1	pracovník
jsou	být	k5eAaImIp3nP	být
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
provozovatelů	provozovatel	k1gMnPc2	provozovatel
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
letecká	letecký	k2eAgFnSc1d1	letecká
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
Kryštof	Kryštof	k1gMnSc1	Kryštof
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
část	část	k1gFnSc1	část
osádky	osádka	k1gFnSc2	osádka
součástí	součást	k1gFnPc2	součást
Armády	armáda	k1gFnSc2	armáda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
soukromými	soukromý	k2eAgFnPc7d1	soukromá
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příspěvkovými	příspěvkový	k2eAgFnPc7d1	příspěvková
organizacemi	organizace	k1gFnPc7	organizace
zřizovanými	zřizovaný	k2eAgInPc7d1	zřizovaný
krajskými	krajský	k2eAgInPc7d1	krajský
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
záchranná	záchranný	k2eAgFnSc1d1	záchranná
služba	služba	k1gFnSc1	služba
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
charakter	charakter	k1gInSc4	charakter
služby	služba	k1gFnSc2	služba
garantované	garantovaný	k2eAgFnSc2d1	garantovaná
státem	stát	k1gInSc7	stát
a	a	k8xC	a
spravované	spravovaný	k2eAgInPc1d1	spravovaný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
Česka	Česko	k1gNnSc2	Česko
provozují	provozovat	k5eAaImIp3nP	provozovat
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
i	i	k8xC	i
nestátní	státní	k2eNgFnPc4d1	nestátní
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přednemocniční	přednemocniční	k2eAgFnSc1d1	přednemocniční
neodkladná	odkladný	k2eNgFnSc1d1	neodkladná
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
smluvně	smluvně	k6eAd1	smluvně
s	s	k7c7	s
územní	územní	k2eAgFnSc7d1	územní
záchrannou	záchranný	k2eAgFnSc7d1	záchranná
službou	služba	k1gFnSc7	služba
příslušného	příslušný	k2eAgInSc2d1	příslušný
kraje	kraj	k1gInSc2	kraj
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
výjezdové	výjezdový	k2eAgFnPc1d1	výjezdová
skupiny	skupina	k1gFnPc1	skupina
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
poskytování	poskytování	k1gNnSc1	poskytování
odborné	odborný	k2eAgFnSc2d1	odborná
přednemocniční	přednemocniční	k2eAgFnSc2d1	přednemocniční
neodkladné	odkladný	k2eNgFnSc2d1	neodkladná
péče	péče	k1gFnSc2	péče
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
240	[number]	k4	240
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
krizovém	krizový	k2eAgNnSc6d1	krizové
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
odbornou	odborný	k2eAgFnSc4d1	odborná
přednemocniční	přednemocniční	k2eAgFnSc4d1	přednemocniční
neodkladnou	odkladný	k2eNgFnSc4d1	neodkladná
péči	péče	k1gFnSc4	péče
na	na	k7c6	na
některém	některý	k3yIgNnSc6	některý
z	z	k7c2	z
výjezdových	výjezdový	k2eAgNnPc2d1	výjezdové
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
