<s>
Galaxie	galaxie	k1gFnSc1	galaxie
je	být	k5eAaImIp3nS	být
gravitačně	gravitačně	k6eAd1	gravitačně
vázaný	vázaný	k2eAgInSc1d1	vázaný
systém	systém	k1gInSc1	systém
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
kosmického	kosmický	k2eAgInSc2d1	kosmický
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
galaxie	galaxie	k1gFnSc2	galaxie
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
názvu	název	k1gInSc2	název
naší	náš	k3xOp1gFnSc2	náš
vlastní	vlastní	k2eAgFnSc2d1	vlastní
galaxie	galaxie	k1gFnSc2	galaxie
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
Κ	Κ	k?	Κ
γ	γ	k?	γ
(	(	kIx(	(
<g/>
Κ	Κ	k?	Κ
galaktikos	galaktikos	k1gInSc1	galaktikos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
nazývaných	nazývaný	k2eAgMnPc2d1	nazývaný
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
temnou	temný	k2eAgFnSc7d1	temná
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
drží	držet	k5eAaImIp3nS	držet
pospolu	pospolu	k6eAd1	pospolu
působení	působení	k1gNnSc1	působení
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
komponenty	komponenta	k1gFnPc1	komponenta
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgInSc2d1	společný
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
některých	některý	k3yIgInPc2	některý
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
většiny	většina	k1gFnPc1	většina
galaxií	galaxie	k1gFnPc2	galaxie
nacházejí	nacházet	k5eAaImIp3nP	nacházet
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnPc1	galaxie
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
protogalaxií	protogalaxie	k1gFnPc2	protogalaxie
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
galaxií	galaxie	k1gFnPc2	galaxie
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
podobně	podobně	k6eAd1	podobně
napříč	napříč	k7c7	napříč
historií	historie	k1gFnSc7	historie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozorovatelné	pozorovatelný	k2eAgFnSc6d1	pozorovatelná
části	část	k1gFnSc6	část
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
nachází	nacházet	k5eAaImIp3nS	nacházet
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc4	dva
biliony	bilion	k4xCgInPc4	bilion
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleova	Hubbleův	k2eAgFnSc1d1	Hubbleova
klasifikace	klasifikace	k1gFnSc1	klasifikace
galaxií	galaxie	k1gFnPc2	galaxie
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
:	:	kIx,	:
Od	od	k7c2	od
eliptických	eliptický	k2eAgFnPc2d1	eliptická
přes	přes	k7c4	přes
čočkové	čočkový	k2eAgInPc4d1	čočkový
až	až	k9	až
po	po	k7c4	po
spirální	spirální	k2eAgInPc4d1	spirální
a	a	k8xC	a
spirální	spirální	k2eAgInPc4d1	spirální
s	s	k7c7	s
příčkou	příčka	k1gFnSc7	příčka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
podtypy	podtyp	k1gInPc4	podtyp
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
galaxie	galaxie	k1gFnSc2	galaxie
–	–	k?	–
ultrakompaktní	ultrakompaktní	k2eAgFnSc1d1	ultrakompaktní
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
galaxie	galaxie	k1gFnSc1	galaxie
–	–	k?	–
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Michaelem	Michael	k1gMnSc7	Michael
Drinkwaterem	Drinkwater	k1gMnSc7	Drinkwater
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Queensland	Queensland	k1gInSc1	Queensland
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
spirálních	spirální	k2eAgFnPc6d1	spirální
galaxiích	galaxie	k1gFnPc6	galaxie
mají	mít	k5eAaImIp3nP	mít
ramena	rameno	k1gNnPc4	rameno
přibližně	přibližně	k6eAd1	přibližně
tvar	tvar	k1gInSc4	tvar
logaritmické	logaritmický	k2eAgFnSc2d1	logaritmická
spirály	spirála	k1gFnSc2	spirála
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
vzor	vzor	k1gInSc4	vzor
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
rozrušením	rozrušení	k1gNnSc7	rozrušení
jednotné	jednotný	k2eAgFnSc2d1	jednotná
rotující	rotující	k2eAgFnSc2d1	rotující
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hvězdy	hvězda	k1gFnPc1	hvězda
i	i	k8xC	i
spirální	spirální	k2eAgNnPc1d1	spirální
ramena	rameno	k1gNnPc1	rameno
rotují	rotovat	k5eAaImIp3nP	rotovat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgInSc2d1	společný
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
konstantní	konstantní	k2eAgFnSc7d1	konstantní
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
do	do	k7c2	do
<g/>
/	/	kIx~	/
<g/>
ze	z	k7c2	z
spirálních	spirální	k2eAgNnPc2d1	spirální
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
spirální	spirální	k2eAgNnPc1d1	spirální
ramena	rameno	k1gNnPc1	rameno
jsou	být	k5eAaImIp3nP	být
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
anebo	anebo	k8xC	anebo
vlnami	vlna	k1gFnPc7	vlna
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
ramena	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
zpomalí	zpomalet	k5eAaPmIp3nS	zpomalet
se	se	k3xPyFc4	se
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ještě	ještě	k9	ještě
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
svou	svůj	k3xOyFgFnSc4	svůj
hustotu	hustota	k1gFnSc4	hustota
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vlna	vlna	k1gFnSc1	vlna
<g/>
"	"	kIx"	"
zpomalujících	zpomalující	k2eAgFnPc2d1	zpomalující
se	se	k3xPyFc4	se
aut	auto	k1gNnPc2	auto
na	na	k7c6	na
přeplněné	přeplněný	k2eAgFnSc6d1	přeplněná
dálnici	dálnice	k1gFnSc6	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
galaxií	galaxie	k1gFnSc7	galaxie
v	v	k7c6	v
Místní	místní	k2eAgFnSc6d1	místní
skupině	skupina	k1gFnSc6	skupina
galaxií	galaxie	k1gFnPc2	galaxie
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gFnSc1	náš
vlastní	vlastní	k2eAgFnSc1d1	vlastní
galaxie	galaxie	k1gFnSc1	galaxie
–	–	k?	–
Galaxie	galaxie	k1gFnSc1	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
spirální	spirální	k2eAgFnSc1d1	spirální
galaxie	galaxie	k1gFnSc1	galaxie
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
100	[number]	k4	100
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
a	a	k8xC	a
šířkou	šířka	k1gFnSc7	šířka
3000	[number]	k4	3000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
hala	halo	k1gNnSc2	halo
a	a	k8xC	a
koróny	koróna	k1gFnSc2	koróna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgInPc4	tři
až	až	k6eAd1	až
šest	šest	k4xCc4	šest
bilionů	bilion	k4xCgInPc2	bilion
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
galaxie	galaxie	k1gFnPc1	galaxie
Velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Magellanův	Magellanův	k2eAgInSc1d1	Magellanův
oblak	oblak	k1gInSc1	oblak
a	a	k8xC	a
několik	několik	k4yIc1	několik
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
galaxií	galaxie	k1gFnSc7	galaxie
místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rovněž	rovněž	k9	rovněž
spirální	spirální	k2eAgFnSc1d1	spirální
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
(	(	kIx(	(
<g/>
M	M	kA	M
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
dosud	dosud	k6eAd1	dosud
prozkoumaných	prozkoumaný	k2eAgFnPc2d1	prozkoumaná
galaxií	galaxie	k1gFnPc2	galaxie
existuje	existovat	k5eAaImIp3nS	existovat
osamoceně	osamoceně	k6eAd1	osamoceně
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xC	jako
polní	polní	k2eAgFnPc1d1	polní
galaxie	galaxie	k1gFnPc1	galaxie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
field	field	k1gMnSc1	field
galaxies	galaxies	k1gMnSc1	galaxies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
gravitačně	gravitačně	k6eAd1	gravitačně
nereagovaly	reagovat	k5eNaBmAgInP	reagovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
galaxiemi	galaxie	k1gFnPc7	galaxie
nebo	nebo	k8xC	nebo
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
např.	např.	kA	např.
nenarazily	narazit	k5eNaPmAgFnP	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
osamocené	osamocený	k2eAgFnPc1d1	osamocená
galaxie	galaxie	k1gFnPc1	galaxie
mohou	moct	k5eAaImIp3nP	moct
podle	podle	k7c2	podle
výzkumů	výzkum	k1gInPc2	výzkum
vytvářet	vytvářet	k5eAaImF	vytvářet
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gInPc1	jejich
plyny	plyn	k1gInPc1	plyn
nejsou	být	k5eNaImIp3nP	být
"	"	kIx"	"
<g/>
kradeny	kraden	k2eAgFnPc1d1	kradena
<g/>
"	"	kIx"	"
okolními	okolní	k2eAgFnPc7d1	okolní
galaxiemi	galaxie	k1gFnPc7	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
galaxií	galaxie	k1gFnPc2	galaxie
je	být	k5eAaImIp3nS	být
gravitačně	gravitačně	k6eAd1	gravitačně
vázána	vázat	k5eAaImNgFnS	vázat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
jiných	jiný	k2eAgFnPc2d1	jiná
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
do	do	k7c2	do
50	[number]	k4	50
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
chudé	chudý	k2eAgFnPc1d1	chudá
kupy	kupa	k1gFnPc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
tisíce	tisíc	k4xCgInPc1	tisíc
galaxií	galaxie	k1gFnPc2	galaxie
natlačených	natlačený	k2eAgFnPc2d1	natlačená
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
několika	několik	k4yIc2	několik
megaparseků	megaparsek	k1gInPc2	megaparsek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bohaté	bohatý	k2eAgFnPc1d1	bohatá
kupy	kupa	k1gFnPc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgFnPc1d1	obří
kupy	kupa	k1gFnPc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
jsou	být	k5eAaImIp3nP	být
gigantické	gigantický	k2eAgFnPc1d1	gigantická
množiny	množina	k1gFnPc1	množina
obsahující	obsahující	k2eAgInPc4d1	obsahující
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
galaxií	galaxie	k1gFnPc2	galaxie
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
do	do	k7c2	do
kup	kupa	k1gFnPc2	kupa
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
nebo	nebo	k8xC	nebo
i	i	k9	i
osamoceně	osamoceně	k6eAd1	osamoceně
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
společně	společně	k6eAd1	společně
s	s	k7c7	s
galaxií	galaxie	k1gFnSc7	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
největší	veliký	k2eAgFnSc6d3	veliký
<g/>
;	;	kIx,	;
celkově	celkově	k6eAd1	celkově
naše	náš	k3xOp1gFnSc1	náš
místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
30	[number]	k4	30
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
1	[number]	k4	1
megaparseku	megaparsek	k1gInSc2	megaparsek
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc7d2	veliký
Kupou	kupa	k1gFnSc7	kupa
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
Panně	Panna	k1gFnSc6	Panna
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
kupami	kupa	k1gFnPc7	kupa
součástí	součást	k1gFnSc7	součást
Místní	místní	k2eAgFnSc2d1	místní
nadkupy	nadkupa	k1gFnSc2	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
neustále	neustále	k6eAd1	neustále
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
galaxiemi	galaxie	k1gFnPc7	galaxie
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Hubbleova	Hubbleův	k2eAgFnSc1d1	Hubbleova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
lokálně	lokálně	k6eAd1	lokálně
potlačit	potlačit	k5eAaPmF	potlačit
svým	svůj	k3xOyFgNnSc7	svůj
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
spojením	spojení	k1gNnSc7	spojení
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
galaxií	galaxie	k1gFnPc2	galaxie
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
náležící	náležící	k2eAgFnSc3d1	náležící
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnPc1d3	nejbližší
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spojily	spojit	k5eAaPmAgFnP	spojit
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
kupy	kupa	k1gFnPc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
probíhající	probíhající	k2eAgNnSc1d1	probíhající
spojování	spojování	k1gNnSc1	spojování
společně	společně	k6eAd1	společně
s	s	k7c7	s
nasáváním	nasávání	k1gNnSc7	nasávání
okolních	okolní	k2eAgInPc2d1	okolní
plynů	plyn	k1gInPc2	plyn
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ohřev	ohřev	k1gInSc4	ohřev
mezigalaktických	mezigalaktický	k2eAgInPc2d1	mezigalaktický
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
kupě	kupa	k1gFnSc6	kupa
galaxií	galaxie	k1gFnPc2	galaxie
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc4d1	dosahující
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
K.	K.	kA	K.
Kolem	kolem	k6eAd1	kolem
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
hmoty	hmota	k1gFnSc2	hmota
kupě	kupa	k1gFnSc6	kupa
galaxií	galaxie	k1gFnPc2	galaxie
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horkého	horký	k2eAgNnSc2d1	horké
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
řídkého	řídký	k2eAgInSc2d1	řídký
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
pár	pár	k4xCyI	pár
procent	procento	k1gNnPc2	procento
tvoří	tvořit	k5eAaImIp3nP	tvořit
viditelné	viditelný	k2eAgFnPc4d1	viditelná
galaxie	galaxie	k1gFnPc4	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
použil	použít	k5eAaPmAgInS	použít
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc7	Galile
dalekohled	dalekohled	k1gInSc4	dalekohled
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
světelného	světelný	k2eAgInSc2d1	světelný
pásu	pás	k1gInSc2	pás
noční	noční	k2eAgFnSc2d1	noční
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
jako	jako	k8xC	jako
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
obrovského	obrovský	k2eAgInSc2d1	obrovský
počtu	počet	k1gInSc2	počet
matně	matně	k6eAd1	matně
se	se	k3xPyFc4	se
jevících	jevící	k2eAgFnPc2d1	jevící
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
se	se	k3xPyFc4	se
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
úvaze	úvaha	k1gFnSc6	úvaha
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc6d1	vycházející
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
práce	práce	k1gFnSc2	práce
Thomase	Thomas	k1gMnSc2	Thomas
Wrighta	Wright	k1gMnSc2	Wright
<g/>
,	,	kIx,	,
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
galaxie	galaxie	k1gFnSc1	galaxie
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
rotující	rotující	k2eAgNnSc4d1	rotující
těleso	těleso	k1gNnSc4	těleso
obrovského	obrovský	k2eAgInSc2d1	obrovský
počtu	počet	k1gInSc2	počet
hvězd	hvězda	k1gFnPc2	hvězda
držených	držený	k2eAgFnPc2d1	držená
pohromadě	pohromadě	k6eAd1	pohromadě
gravitačními	gravitační	k2eAgFnPc7d1	gravitační
silami	síla	k1gFnPc7	síla
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Kant	Kant	k1gMnSc1	Kant
se	se	k3xPyFc4	se
též	též	k9	též
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
,	,	kIx,	,
viděných	viděný	k2eAgFnPc2d1	viděná
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
samostatné	samostatný	k2eAgFnPc1d1	samostatná
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Charles	Charles	k1gMnSc1	Charles
Messier	Messier	k1gMnSc1	Messier
Seznam	seznam	k1gInSc4	seznam
Messierových	Messierův	k2eAgInPc2d1	Messierův
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
110	[number]	k4	110
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
mlhovin	mlhovina	k1gFnPc2	mlhovina
a	a	k8xC	a
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
,	,	kIx,	,
zanedlouho	zanedlouho	k6eAd1	zanedlouho
následovaný	následovaný	k2eAgInSc1d1	následovaný
katalogem	katalog	k1gInSc7	katalog
5000	[number]	k4	5000
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
shromážděn	shromáždit	k5eAaPmNgInS	shromáždit
Williamem	William	k1gInSc7	William
Herschelem	Herschel	k1gMnSc7	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
William	William	k1gInSc1	William
Persons	Persons	k1gInSc1	Persons
nový	nový	k2eAgInSc1d1	nový
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yQgNnSc2	který
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
rozlišit	rozlišit	k5eAaPmF	rozlišit
eliptické	eliptický	k2eAgFnPc4d1	eliptická
a	a	k8xC	a
spirální	spirální	k2eAgFnPc4d1	spirální
mlhoviny	mlhovina	k1gFnPc4	mlhovina
(	(	kIx(	(
<g/>
galaxie	galaxie	k1gFnPc4	galaxie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
mlhovinách	mlhovina	k1gFnPc6	mlhovina
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
světelné	světelný	k2eAgInPc4d1	světelný
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Kantovu	Kantův	k2eAgFnSc4d1	Kantova
dřívější	dřívější	k2eAgFnSc4d1	dřívější
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgMnSc3	ten
nebyly	být	k5eNaImAgFnP	být
mlhoviny	mlhovina	k1gFnPc1	mlhovina
uznávány	uznáván	k2eAgFnPc1d1	uznávána
jako	jako	k8xS	jako
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
samostatné	samostatný	k2eAgFnPc1d1	samostatná
galaxie	galaxie	k1gFnPc1	galaxie
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Edwin	Edwin	k1gMnSc1	Edwin
Powell	Powell	k1gMnSc1	Powell
Hubble	Hubble	k1gMnSc1	Hubble
použil	použít	k5eAaPmAgMnS	použít
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
rozlišit	rozlišit	k5eAaPmF	rozlišit
vnější	vnější	k2eAgFnPc4d1	vnější
části	část	k1gFnPc4	část
některých	některý	k3yIgFnPc2	některý
spirálních	spirální	k2eAgFnPc2d1	spirální
mlhovin	mlhovina	k1gFnPc2	mlhovina
jako	jako	k8xS	jako
množiny	množina	k1gFnSc2	množina
samostatných	samostatný	k2eAgFnPc2d1	samostatná
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
též	též	k9	též
umožnil	umožnit	k5eAaPmAgInS	umožnit
odhadnutí	odhadnutí	k1gNnSc4	odhadnutí
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
;	;	kIx,	;
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
Mléčné	mléčný	k2eAgFnPc1d1	mléčná
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
klasifikační	klasifikační	k2eAgInSc1d1	klasifikační
systém	systém	k1gInSc1	systém
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Hubbleovu	Hubbleův	k2eAgFnSc4d1	Hubbleova
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
popsat	popsat	k5eAaPmF	popsat
tvar	tvar	k1gInSc4	tvar
Galaxie	galaxie	k1gFnSc2	galaxie
a	a	k8xC	a
určit	určit	k5eAaPmF	určit
pozici	pozice	k1gFnSc4	pozice
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc1	Herschel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
důkladným	důkladný	k2eAgNnSc7d1	důkladné
spočítáním	spočítání	k1gNnSc7	spočítání
počtu	počet	k1gInSc2	počet
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Použitím	použití	k1gNnSc7	použití
přepracovaného	přepracovaný	k2eAgInSc2d1	přepracovaný
postupu	postup	k1gInSc2	postup
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Jacobus	Jacobus	k1gMnSc1	Jacobus
Kapteyn	Kapteyn	k1gMnSc1	Kapteyn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
k	k	k7c3	k
obrázku	obrázek	k1gInSc3	obrázek
malé	malá	k1gFnSc2	malá
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
15	[number]	k4	15
kiloparseků	kiloparsek	k1gInPc2	kiloparsek
<g/>
)	)	kIx)	)
elipsovité	elipsovitý	k2eAgFnPc1d1	elipsovitá
galaxie	galaxie	k1gFnPc1	galaxie
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
blízko	blízko	k7c2	blízko
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
použil	použít	k5eAaPmAgMnS	použít
Harlow	Harlow	k1gMnSc1	Harlow
Shapley	Shaplea	k1gFnSc2	Shaplea
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
katalogizování	katalogizování	k1gNnSc6	katalogizování
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úplně	úplně	k6eAd1	úplně
odlišnému	odlišný	k2eAgInSc3d1	odlišný
obrázku	obrázek	k1gInSc3	obrázek
<g/>
:	:	kIx,	:
plochý	plochý	k2eAgInSc1d1	plochý
disk	disk	k1gInSc1	disk
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
70	[number]	k4	70
kiloparseků	kiloparsek	k1gInPc2	kiloparsek
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
analýzy	analýza	k1gFnPc1	analýza
však	však	k9	však
selhaly	selhat	k5eAaPmAgFnP	selhat
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebraly	brát	k5eNaImAgFnP	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
absorpci	absorpce	k1gFnSc4	absorpce
světla	světlo	k1gNnSc2	světlo
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
obrázek	obrázek	k1gInSc1	obrázek
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Julius	Julius	k1gMnSc1	Julius
Trumpler	Trumpler	k1gMnSc1	Trumpler
vyčíslil	vyčíslit	k5eAaPmAgMnS	vyčíslit
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
studováním	studování	k1gNnSc7	studování
otevřených	otevřený	k2eAgFnPc2d1	otevřená
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
Hendrik	Hendrik	k1gMnSc1	Hendrik
van	vana	k1gFnPc2	vana
de	de	k?	de
Hulst	Hulst	k1gMnSc1	Hulst
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
21	[number]	k4	21
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
přicházet	přicházet	k5eAaImF	přicházet
z	z	k7c2	z
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
atomového	atomový	k2eAgInSc2d1	atomový
vodíkového	vodíkový	k2eAgInSc2d1	vodíkový
plynu	plyn	k1gInSc2	plyn
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
záření	záření	k1gNnSc4	záření
byly	být	k5eAaImAgFnP	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
přesnější	přesný	k2eAgNnSc4d2	přesnější
studium	studium	k1gNnSc4	studium
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pozorování	pozorování	k1gNnPc1	pozorování
vedla	vést	k5eAaImAgNnP	vést
k	k	k7c3	k
modelu	model	k1gInSc3	model
rotující	rotující	k2eAgFnSc2d1	rotující
pruhové	pruhový	k2eAgFnSc2d1	pruhová
struktury	struktura	k1gFnSc2	struktura
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
vylepšených	vylepšený	k2eAgInPc2d1	vylepšený
dalekohledů	dalekohled	k1gInPc2	dalekohled
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
plyn	plyn	k1gInSc4	plyn
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
galaxiích	galaxie	k1gFnPc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
vědci	vědec	k1gMnPc1	vědec
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgFnSc1	všechen
viditelná	viditelný	k2eAgFnSc1d1	viditelná
hmota	hmota	k1gFnSc1	hmota
galaxií	galaxie	k1gFnPc2	galaxie
patřičně	patřičně	k6eAd1	patřičně
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
rychlosti	rychlost	k1gFnSc2	rychlost
rotujícího	rotující	k2eAgInSc2d1	rotující
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
předpokladu	předpoklad	k1gInSc3	předpoklad
existence	existence	k1gFnSc2	existence
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
tmavá	tmavý	k2eAgFnSc1d1	tmavá
galaxie	galaxie	k1gFnSc1	galaxie
VIRGOHI	VIRGOHI	kA	VIRGOHI
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
tmavost	tmavost	k1gFnSc1	tmavost
byla	být	k5eAaImAgFnS	být
ověřena	ověřit	k5eAaPmNgFnS	ověřit
a	a	k8xC	a
objev	objev	k1gInSc1	objev
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
galaxie	galaxie	k1gFnSc1	galaxie
EGSY	EGSY	kA	EGSY
<g/>
8	[number]	k4	8
<g/>
p	p	k?	p
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejvzdálenější	vzdálený	k2eAgFnSc7d3	nejvzdálenější
galaxií	galaxie	k1gFnSc7	galaxie
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
byla	být	k5eAaImAgFnS	být
člověkem	člověk	k1gMnSc7	člověk
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Astronomie	astronomie	k1gFnSc1	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
galaxií	galaxie	k1gFnPc2	galaxie
mimo	mimo	k7c4	mimo
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
pozorování	pozorování	k1gNnPc1	pozorování
prováděna	provádět	k5eAaImNgNnP	provádět
přirozeně	přirozeně	k6eAd1	přirozeně
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
spočívá	spočívat	k5eAaImIp3nS	spočívat
maximum	maximum	k1gNnSc1	maximum
záření	záření	k1gNnSc2	záření
většiny	většina	k1gFnSc2	většina
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pozorování	pozorování	k1gNnSc1	pozorování
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
utvářejí	utvářet	k5eAaImIp3nP	utvářet
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
optické	optický	k2eAgFnSc2d1	optická
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
se	se	k3xPyFc4	se
také	také	k9	také
dobře	dobře	k6eAd1	dobře
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
ionizované	ionizovaný	k2eAgNnSc4d1	ionizované
HII	HII	kA	HII
oblasti	oblast	k1gFnSc2	oblast
či	či	k8xC	či
rozložení	rozložení	k1gNnSc2	rozložení
prachových	prachový	k2eAgNnPc2d1	prachové
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
z	z	k7c2	z
polarizačních	polarizační	k2eAgNnPc2d1	polarizační
měření	měření	k1gNnPc2	měření
odvozená	odvozený	k2eAgFnSc1d1	odvozená
magnetická	magnetický	k2eAgFnSc1d1	magnetická
pole	pole	k1gFnSc1	pole
galaxií	galaxie	k1gFnPc2	galaxie
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
částečně	částečně	k6eAd1	částečně
i	i	k9	i
jejich	jejich	k3xOp3gFnSc3	jejich
pozorované	pozorovaný	k2eAgFnSc3d1	pozorovaná
struktuře	struktura	k1gFnSc3	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
světlo	světlo	k1gNnSc4	světlo
neprůhledný	průhledný	k2eNgMnSc1d1	neprůhledný
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
<g/>
,	,	kIx,	,
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
pozorování	pozorování	k1gNnSc1	pozorování
vzdálenějších	vzdálený	k2eAgInPc2d2	vzdálenější
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
jím	jíst	k5eAaImIp1nS	jíst
však	však	k9	však
prochází	procházet	k5eAaImIp3nS	procházet
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
detailnímu	detailní	k2eAgInSc3d1	detailní
průzkumu	průzkum	k1gInSc3	průzkum
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
oblastí	oblast	k1gFnPc2	oblast
gigantických	gigantický	k2eAgNnPc2d1	gigantické
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
mračen	mračno	k1gNnPc2	mračno
a	a	k8xC	a
galaktických	galaktický	k2eAgNnPc2d1	Galaktické
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Infračervené	infračervený	k2eAgNnSc1d1	infračervené
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
používáno	používat	k5eAaImNgNnS	používat
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
pozorování	pozorování	k1gNnSc6	pozorování
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
červený	červený	k2eAgInSc1d1	červený
posuv	posuv	k1gInSc1	posuv
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
páry	pára	k1gFnPc1	pára
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
množství	množství	k1gNnPc1	množství
použitelného	použitelný	k2eAgNnSc2d1	použitelné
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pro	pro	k7c4	pro
infračervenou	infračervený	k2eAgFnSc4d1	infračervená
astronomii	astronomie	k1gFnSc4	astronomie
používají	používat	k5eAaImIp3nP	používat
teleskopy	teleskop	k1gInPc1	teleskop
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
vyvýšených	vyvýšený	k2eAgInPc6d1	vyvýšený
místech	místo	k1gNnPc6	místo
či	či	k8xC	či
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
nevizuální	vizuální	k2eNgNnSc1d1	vizuální
studium	studium	k1gNnSc1	studium
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
aktivních	aktivní	k2eAgFnPc2d1	aktivní
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rádiových	rádiový	k2eAgFnPc2d1	rádiová
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
průhledná	průhledný	k2eAgFnSc1d1	průhledná
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rádiovým	rádiový	k2eAgFnPc3d1	rádiová
vlnám	vlna	k1gFnPc3	vlna
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
od	od	k7c2	od
5	[number]	k4	5
MHz	Mhz	kA	Mhz
do	do	k7c2	do
30	[number]	k4	30
GHz	GHz	k1gFnPc2	GHz
(	(	kIx(	(
<g/>
ionosféra	ionosféra	k1gFnSc1	ionosféra
blokuje	blokovat	k5eAaImIp3nS	blokovat
vlny	vlna	k1gFnPc4	vlna
nižších	nízký	k2eAgFnPc2d2	nižší
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
velké	velký	k2eAgInPc1d1	velký
rádiové	rádiový	k2eAgInPc1d1	rádiový
interferometry	interferometr	k1gInPc1	interferometr
k	k	k7c3	k
zmapování	zmapování	k1gNnSc3	zmapování
proudů	proud	k1gInPc2	proud
vyzařovaných	vyzařovaný	k2eAgInPc2d1	vyzařovaný
z	z	k7c2	z
aktivních	aktivní	k2eAgNnPc2d1	aktivní
galaktických	galaktický	k2eAgNnPc2d1	Galaktické
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Radioteleskopy	radioteleskop	k1gInPc1	radioteleskop
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
neutrálního	neutrální	k2eAgInSc2d1	neutrální
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
díky	dík	k1gInPc7	dík
21	[number]	k4	21
<g/>
centimetrovému	centimetrový	k2eAgNnSc3d1	centimetrové
záření	záření	k1gNnSc3	záření
<g/>
)	)	kIx)	)
a	a	k8xC	a
potenciálně	potenciálně	k6eAd1	potenciálně
také	také	k9	také
neionizované	ionizovaný	k2eNgFnSc2d1	neionizovaná
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
později	pozdě	k6eAd2	pozdě
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
a	a	k8xC	a
utvořila	utvořit	k5eAaPmAgFnS	utvořit
galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialové	ultrafialový	k2eAgInPc1d1	ultrafialový
a	a	k8xC	a
rentgenové	rentgenový	k2eAgInPc1d1	rentgenový
teleskopy	teleskop	k1gInPc1	teleskop
mohou	moct	k5eAaImIp3nP	moct
sledovat	sledovat	k5eAaImF	sledovat
vysokoenergetické	vysokoenergetický	k2eAgInPc4d1	vysokoenergetický
galaktické	galaktický	k2eAgInPc4d1	galaktický
úkazy	úkaz	k1gInPc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialová	ultrafialový	k2eAgFnSc1d1	ultrafialová
záře	záře	k1gFnSc1	záře
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
například	například	k6eAd1	například
při	při	k7c6	při
roztrhání	roztrhání	k1gNnSc6	roztrhání
hvězdy	hvězda	k1gFnSc2	hvězda
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
galaxii	galaxie	k1gFnSc6	galaxie
gravitačními	gravitační	k2eAgFnPc7d1	gravitační
silami	síla	k1gFnPc7	síla
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
horkých	horký	k2eAgInPc2d1	horký
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
galaktických	galaktický	k2eAgInPc6d1	galaktický
klastrech	klastr	k1gInPc6	klastr
se	se	k3xPyFc4	se
zase	zase	k9	zase
mapuje	mapovat	k5eAaImIp3nS	mapovat
pomocí	pomocí	k7c2	pomocí
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
díky	díky	k7c3	díky
rentgenové	rentgenový	k2eAgFnSc3d1	rentgenová
astronomii	astronomie	k1gFnSc3	astronomie
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
také	také	k9	také
existence	existence	k1gFnSc1	existence
superhmotných	superhmotný	k2eAgFnPc2d1	superhmotná
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
