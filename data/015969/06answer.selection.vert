<s>
S	s	k7c7
příchodem	příchod	k1gInSc7
normalizace	normalizace	k1gFnSc2
po	po	k7c6
potlačeném	potlačený	k2eAgNnSc6d1
Maďarském	maďarský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tradiční	tradiční	k2eAgInSc1d1
Kossuthův	Kossuthův	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
se	se	k3xPyFc4
během	během	k7c2
povstání	povstání	k1gNnSc2
nahradil	nahradit	k5eAaPmAgMnS
komunistický	komunistický	k2eAgMnSc1d1
Rákosiho	Rákosi	k1gMnSc4
znak	znak	k1gInSc4
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
nadále	nadále	k6eAd1
používán	používat	k5eAaImNgInS
jako	jako	k8xS,k8xC
znak	znak	k1gInSc1
socialistického	socialistický	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>