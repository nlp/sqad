<s>
Kádárův	Kádárův	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Kádárův	Kádárův	k2eAgInSc1d1
znak	znak	k1gInSc1
(	(	kIx(
<g/>
Kádár-címer	Kádár-címer	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kádárův	Kádárův	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Kádár-címer	Kádár-címra	k1gFnPc2
<g/>
,	,	kIx,
je	on	k3xPp3gFnPc4
označení	označení	k1gNnSc1
pro	pro	k7c4
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
Maďarské	maďarský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
platný	platný	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
1957	#num#	k4
–	–	k?
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
podle	podle	k7c2
Jánose	Jánosa	k1gFnSc6
Kádára	Kádár	k1gMnSc2
<g/>
,	,	kIx,
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
MSZMP	MSZMP	kA
a	a	k8xC
největšího	veliký	k2eAgMnSc2d3
komunistického	komunistický	k2eAgMnSc2d1
funkcionáře	funkcionář	k1gMnPc4
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
Kádárova	Kádárův	k2eAgFnSc1d1
éra	éra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
se	s	k7c7
stáním	stání	k1gNnSc7
znakem	znak	k1gInSc7
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
příchodem	příchod	k1gInSc7
normalizace	normalizace	k1gFnSc2
po	po	k7c6
potlačeném	potlačený	k2eAgNnSc6d1
Maďarském	maďarský	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tradiční	tradiční	k2eAgInSc1d1
Kossuthův	Kossuthův	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
se	se	k3xPyFc4
během	během	k7c2
povstání	povstání	k1gNnSc2
nahradil	nahradit	k5eAaPmAgMnS
komunistický	komunistický	k2eAgMnSc1d1
Rákosiho	Rákosi	k1gMnSc4
znak	znak	k1gInSc4
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
nadále	nadále	k6eAd1
používán	používat	k5eAaImNgInS
jako	jako	k8xS,k8xC
znak	znak	k1gInSc1
socialistického	socialistický	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
zákonem	zákon	k1gInSc7
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
.	.	kIx.
évi	évi	k?
II	II	kA
<g/>
.	.	kIx.
törvény	törvéna	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
§	§	k?
<g/>
)	)	kIx)
přijat	přijat	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
opět	opět	k6eAd1
vytvořený	vytvořený	k2eAgInSc1d1
dle	dle	k7c2
sovětského	sovětský	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidově	lidově	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
začalo	začít	k5eAaPmAgNnS
říkat	říkat	k5eAaImF
Kádár-címer	Kádár-címer	k1gInSc4
(	(	kIx(
<g/>
Kádárův	Kádárův	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Rákosiho	Rákosi	k1gMnSc2
znaku	znak	k1gInSc2
však	však	k9
tento	tento	k3xDgInSc4
znak	znak	k1gInSc4
již	již	k6eAd1
nebyl	být	k5eNaImAgMnS
oficiální	oficiální	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
vlajky	vlajka	k1gFnPc1
se	s	k7c7
znakem	znak	k1gInSc7
objevovaly	objevovat	k5eAaImAgFnP
<g/>
,	,	kIx,
používaly	používat	k5eAaImAgInP
se	se	k3xPyFc4
jen	jen	k9
ojediněle	ojediněle	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
a	a	k8xC
vyhlášení	vyhlášení	k1gNnSc2
demokratické	demokratický	k2eAgFnSc2d1
Maďarské	maďarský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
znak	znak	k1gInSc1
nahrazen	nahradit	k5eAaPmNgInS
současným	současný	k2eAgInSc7d1
státním	státní	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
tradici	tradice	k1gFnSc4
maďarského	maďarský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
měl	mít	k5eAaImAgInS
štít	štít	k1gInSc4
byl	být	k5eAaImAgInS
dělený	dělený	k2eAgInSc1d1
na	na	k7c4
tři	tři	k4xCgInPc4
vodorovné	vodorovný	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
s	s	k7c7
barvami	barva	k1gFnPc7
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
štítem	štít	k1gInSc7
byla	být	k5eAaImAgFnS
rudá	rudý	k2eAgFnSc1d1
pěticípá	pěticípý	k2eAgFnSc1d1
zlatě	zlatě	k6eAd1
lemovaná	lemovaný	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
vycházely	vycházet	k5eAaImAgFnP
za	za	k7c4
štít	štít	k1gInSc4
zlaté	zlatý	k2eAgInPc4d1
paprsky	paprsek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k6eAd1
byly	být	k5eAaImAgFnP
zlaté	zlatý	k2eAgFnPc1d1
obilné	obilný	k2eAgFnPc1d1
klasy	klasa	k1gFnPc1
ovinuté	ovinutý	k2eAgFnPc1d1
z	z	k7c2
levé	levý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
stuhou	stuha	k1gFnSc7
v	v	k7c6
národních	národní	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
a	a	k8xC
z	z	k7c2
pravé	pravý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
rudou	rudý	k2eAgFnSc7d1
stuhou	stuha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
symbolizoval	symbolizovat	k5eAaImAgInS
spojení	spojení	k1gNnSc4
pracujících	pracující	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
venkova	venkov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
popis	popis	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc4d1
popis	popis	k1gInSc4
státního	státní	k2eAgInSc2d1
znaku	znak	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
MLR	MLR	kA
z	z	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
A	a	k9
Magyar	Magyar	k1gMnSc1
Népköztársaság	Népköztársaság	k1gMnSc1
címere	címrat	k5eAaPmIp3nS
<g/>
:	:	kIx,
kétoldalt	kétoldalt	k1gMnSc1
búzakoszorúval	búzakoszorúvat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
egybefogott	egybefogott	k1gInSc4
<g/>
,	,	kIx,
világoskék	világoskék	k1gInSc4
mezőben	mezőben	k2eAgInSc4d1
álló	álló	k?
<g/>
,	,	kIx,
ívelt	ívelt	k1gMnSc1
oldalú	oldalú	k?
piros-fehér-zöld	piros-fehér-zöld	k1gMnSc1
színű	színű	k?
pajzs	pajzs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
búzakoszorút	búzakoszorút	k2eAgMnSc1d1
balról	balról	k1gMnSc1
piros-fehér-zöld	piros-fehér-zöld	k1gMnSc1
<g/>
,	,	kIx,
jobbról	jobbról	k1gMnSc1
vörös	vörösa	k1gFnPc2
színű	színű	k?
szalag	szalag	k1gMnSc1
fonja	fonja	k1gMnSc1
át	át	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
pajzs	pajzs	k6eAd1
fölött	fölött	k2eAgInSc1d1
középen	középen	k2eAgInSc1d1
elhelyezett	elhelyezett	k1gInSc1
ötágú	ötágú	k?
vörös	vörös	k1gInSc1
csillag	csillag	k1gInSc1
aranyszínű	aranyszínű	k?
sugarakat	sugarakat	k5eAaImF,k5eAaBmF,k5eAaPmF
bocsát	bocsát	k1gInSc4
a	a	k8xC
mezőre	mezőr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
TOMÁŠEK	Tomášek	k1gMnSc1
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
průvodce	průvodce	k1gMnSc1
olympia	olympius	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Stání	stání	k1gNnSc2
zřízení	zřízení	k1gNnSc2
a	a	k8xC
administrativní	administrativní	k2eAgNnSc4d1
členění	členění	k1gNnSc4
<g/>
,	,	kIx,
s.	s.	k?
43	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kádár-címer	Kádár-címra	k1gFnPc2
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BROŽEK	Brožek	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
vlajek	vlajka	k1gFnPc2
a	a	k8xC
znaků	znak	k1gInPc2
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kartografie	kartografie	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
223	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
776	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MUCHA	Mucha	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajky	vlajka	k1gFnPc1
a	a	k8xC
Znaky	znak	k1gInPc1
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
GKP	GKP	kA
<g/>
,	,	kIx,
1987	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
215	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Maďarska	Maďarsko	k1gNnSc2
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Euroastra	Euroastra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
-	-	kIx~
Kádár	Kádár	k1gMnSc1
János	János	k1gMnSc1
Magyar	Magyar	k1gMnSc1
Népköztársaságának	Népköztársaságának	k1gMnSc1
címere	címrat	k5eAaPmIp3nS
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
DÖMÖTÖRFI	DÖMÖTÖRFI	kA
TIBOR	Tibor	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
magyar	magyar	k1gMnSc1
címer	címer	k1gMnSc1
és	és	k?
zászló	zászló	k?
útja	útja	k6eAd1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
MLR	MLR	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Kossuthův	Kossuthův	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
(	(	kIx(
<g/>
Rákosiho	Rákosi	k1gMnSc2
znak	znak	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1957	#num#	k4
-	-	kIx~
1989	#num#	k4
Kádárův	Kádárův	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Současný	současný	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Maďarska	Maďarsko	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
