<s>
Hvězdokupu	hvězdokupa	k1gFnSc4
objevil	objevit	k5eAaPmAgMnS
Pierre	Pierr	k1gInSc5
Méchain	Méchain	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1780	#num#	k4
a	a	k8xC
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Charles	Charles	k1gMnSc1
Messier	Messier	k1gMnSc1
změřil	změřit	k5eAaPmAgMnS
její	její	k3xOp3gFnSc4
polohu	poloha	k1gFnSc4
a	a	k8xC
přidal	přidat	k5eAaPmAgMnS
ji	ona	k3xPp3gFnSc4
do	do	k7c2
svého	svůj	k3xOyFgInSc2
katalogu	katalog	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
tímto	tento	k3xDgInSc7
popisem	popis	k1gInSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Mlhovina	mlhovina	k1gFnSc1
bez	bez	k7c2
hvězd	hvězda	k1gFnPc2
umístěná	umístěný	k2eAgFnSc1d1
pod	pod	k7c7
Zajícem	Zajíc	k1gMnSc7
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
rovnoběžce	rovnoběžka	k1gFnSc6
s	s	k7c7
hvězdou	hvězda	k1gFnSc7
6	#num#	k4
<g/>
.	.	kIx.
magnitudy	magnituda	k1gFnSc2
<g/>
.	.	kIx.
</s>