<s>
Messier	Messier	k1gInSc1
79	#num#	k4
</s>
<s>
Messier	Messier	k1gInSc1
79	#num#	k4
M79	M79	k1gFnPc2
na	na	k7c6
snímku	snímek	k1gInSc6
z	z	k7c2
Hubbleova	Hubbleův	k2eAgInSc2d1
vesmírného	vesmírný	k2eAgInSc2d1
dalekohleduPozorovací	dalekohleduPozorovací	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
kulová	kulový	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
a	a	k8xC
astrophysical	astrophysicat	k5eAaPmAgMnS
X-ray	X-ra	k2eAgMnPc4d1
source	source	k1gMnPc4
Třída	třída	k1gFnSc1
</s>
<s>
V	v	k7c6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Pierre	Pierr	k1gMnSc5
Méchain	Méchain	k1gMnSc1
Datum	datum	k1gInSc4
objevu	objev	k1gInSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1780	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
h	h	k?
24	#num#	k4
<g/>
m	m	kA
11,09	11,09	k4
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Deklinace	deklinace	k1gFnSc1
</s>
<s>
-24	-24	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
29,0	29,0	k4
<g/>
″	″	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Zajíc	Zajíc	k1gMnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
Lep	lep	k1gInSc1
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
8,16	8,16	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
9,21	9,21	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
5,67	5,67	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
9,006	9,006	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
8,57	8,57	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
8,351	8,351	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
8,178	8,178	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
9,6	9,6	k4
<g/>
'	'	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
0,013	0,013	k4
Mpc	Mpc	k1gMnSc1
Rudý	rudý	k1gMnSc1
posuv	posuv	k1gInSc4
</s>
<s>
205,8	205,8	k4
<g/>
±	±	k?
<g/>
0,4	0,4	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Poloměr	poloměr	k1gInSc1
</s>
<s>
59	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ly	ly	k?
Absolutní	absolutní	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
−	−	k?
Metalicita	Metalicita	k1gFnSc1
[	[	kIx(
<g/>
Fe	Fe	k1gMnSc1
<g/>
/	/	kIx~
<g/>
H	H	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
Odhadované	odhadovaný	k2eAgNnSc4d1
stáří	stáří	k1gNnSc4
</s>
<s>
11,7	11,7	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
Messierův	Messierův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
M	M	kA
79	#num#	k4
New	New	k1gFnSc2
General	General	k1gFnSc1
Catalogue	Catalogue	k1gFnSc1
</s>
<s>
NGC	NGC	kA
1904	#num#	k4
Melottův	Melottův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
Melotte	Melotte	k5eAaPmIp2nP
34	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
M	M	kA
<g/>
79	#num#	k4
<g/>
,	,	kIx,
NGC	NGC	kA
1904	#num#	k4
<g/>
,	,	kIx,
GCl	GCl	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Mel	mlít	k5eAaImRp2nS
34	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Messier	Messier	k1gInSc1
79	#num#	k4
(	(	kIx(
<g/>
také	také	k9
M79	M79	k1gFnSc1
nebo	nebo	k8xC
NGC	NGC	kA
1904	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kulová	kulový	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Zajíce	Zajíc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Pierre	Pierr	k1gInSc5
Méchain	Méchain	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1780	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hvězdokupa	hvězdokupa	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Země	zem	k1gFnSc2
vzdálená	vzdálený	k2eAgFnSc1d1
okolo	okolo	k7c2
42	#num#	k4
100	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
a	a	k8xC
od	od	k7c2
středu	střed	k1gInSc2
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
asi	asi	k9
60	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozorování	pozorování	k1gNnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
M	M	kA
79	#num#	k4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Zajíce	Zajíc	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
M79	M79	k4
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
najít	najít	k5eAaPmF
poměrně	poměrně	k6eAd1
snadno	snadno	k6eAd1
protažením	protažení	k1gNnSc7
spojnice	spojnice	k1gFnSc2
hvězd	hvězda	k1gFnPc2
Arneb	Arnba	k1gFnPc2
(	(	kIx(
<g/>
α	α	k?
Leporis	Leporis	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Nihal	Nihal	k1gInSc1
(	(	kIx(
<g/>
β	β	k?
Leporis	Leporis	k1gFnSc1
<g/>
)	)	kIx)
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
o	o	k7c4
stejnou	stejný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dělí	dělit	k5eAaImIp3nS
tyto	tento	k3xDgFnPc4
hvězdy	hvězda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
spatření	spatření	k1gNnSc4
této	tento	k3xDgFnSc2
hvězdokupy	hvězdokupa	k1gFnSc2
středně	středně	k6eAd1
velkým	velký	k2eAgInSc7d1
triedrem	triedr	k1gInSc7
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
velmi	velmi	k6eAd1
čistá	čistý	k2eAgFnSc1d1
obloha	obloha	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
dalekohled	dalekohled	k1gInSc1
o	o	k7c6
průměru	průměr	k1gInSc6
80	#num#	k4
mm	mm	kA
ji	on	k3xPp3gFnSc4
ukáže	ukázat	k5eAaPmIp3nS
mnohem	mnohem	k6eAd1
snadněji	snadno	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalekohledu	dalekohled	k1gInSc6
o	o	k7c6
průměru	průměr	k1gInSc6
140	#num#	k4
mm	mm	kA
se	se	k3xPyFc4
při	při	k7c6
zvětšení	zvětšení	k1gNnSc6
25	#num#	k4
<g/>
x	x	k?
ukáže	ukázat	k5eAaPmIp3nS
jako	jako	k9
kulatá	kulatý	k2eAgFnSc1d1
skvrna	skvrna	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
skrývá	skrývat	k5eAaImIp3nS
několik	několik	k4yIc1
velmi	velmi	k6eAd1
malých	malý	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc7
počet	počet	k1gInSc1
v	v	k7c6
dalekohledu	dalekohled	k1gInSc6
o	o	k7c6
průměru	průměr	k1gInSc6
200	#num#	k4
mm	mm	kA
naroste	narůst	k5eAaPmIp3nS
na	na	k7c4
padesát	padesát	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
průměru	průměr	k1gInSc6
300	#num#	k4
mm	mm	kA
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
rozeznat	rozeznat	k5eAaPmF
výrazně	výrazně	k6eAd1
více	hodně	k6eAd2
hvězd	hvězda	k1gFnPc2
a	a	k8xC
v	v	k7c6
nejjasnější	jasný	k2eAgFnSc6d3
záři	zář	k1gFnSc6
uprostřed	uprostřed	k7c2
hvězdokupy	hvězdokupa	k1gFnSc2
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
více	hodně	k6eAd2
než	než	k8xS
stovka	stovka	k1gFnSc1
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
M79	M79	k4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
jednoduše	jednoduše	k6eAd1
pozorovat	pozorovat	k5eAaImF
z	z	k7c2
obou	dva	k4xCgFnPc2
zemských	zemský	k2eAgFnPc2d1
polokoulí	polokoule	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pozorovatelé	pozorovatel	k1gMnPc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
zvýhodněni	zvýhodněn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
poloze	poloha	k1gFnSc3
není	být	k5eNaImIp3nS
pozorovatelná	pozorovatelný	k2eAgFnSc1d1
v	v	k7c6
severních	severní	k2eAgFnPc6d1
částech	část	k1gFnPc6
Evropy	Evropa	k1gFnSc2
za	za	k7c7
polárním	polární	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
severního	severní	k2eAgInSc2d1
mírného	mírný	k2eAgInSc2d1
podnebného	podnebný	k2eAgInSc2d1
pásu	pás	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nízko	nízko	k6eAd1
nad	nad	k7c7
obzorem	obzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
vychází	vycházet	k5eAaImIp3nS
hvězdokupa	hvězdokupa	k1gFnSc1
velmi	velmi	k6eAd1
vysoko	vysoko	k6eAd1
na	na	k7c4
oblohu	obloha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Nejvhodnější	vhodný	k2eAgNnPc1d3
období	období	k1gNnPc1
pro	pro	k7c4
její	její	k3xOp3gNnPc4
pozorování	pozorování	k1gNnPc4
na	na	k7c6
večerní	večerní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
je	být	k5eAaImIp3nS
od	od	k7c2
listopadu	listopad	k1gInSc2
do	do	k7c2
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
pozorování	pozorování	k1gNnPc2
</s>
<s>
Hvězdokupu	hvězdokupa	k1gFnSc4
objevil	objevit	k5eAaPmAgMnS
Pierre	Pierr	k1gInSc5
Méchain	Méchain	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1780	#num#	k4
a	a	k8xC
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Charles	Charles	k1gMnSc1
Messier	Messier	k1gMnSc1
změřil	změřit	k5eAaPmAgMnS
její	její	k3xOp3gFnSc4
polohu	poloha	k1gFnSc4
a	a	k8xC
přidal	přidat	k5eAaPmAgMnS
ji	ona	k3xPp3gFnSc4
do	do	k7c2
svého	svůj	k3xOyFgInSc2
katalogu	katalog	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
tímto	tento	k3xDgInSc7
popisem	popis	k1gInSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Mlhovina	mlhovina	k1gFnSc1
bez	bez	k7c2
hvězd	hvězda	k1gFnPc2
umístěná	umístěný	k2eAgFnSc1d1
pod	pod	k7c7
Zajícem	Zajíc	k1gMnSc7
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
rovnoběžce	rovnoběžka	k1gFnSc6
s	s	k7c7
hvězdou	hvězda	k1gFnSc7
6	#num#	k4
<g/>
.	.	kIx.
magnitudy	magnituda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viděl	vidět	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
p.	p.	k?
Méchain	Méchain	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1780	#num#	k4
<g/>
…	…	k?
Tato	tento	k3xDgFnSc1
mlhovina	mlhovina	k1gFnSc1
je	být	k5eAaImIp3nS
pěkná	pěkný	k2eAgFnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
jasný	jasný	k2eAgInSc4d1
střed	střed	k1gInSc4
a	a	k8xC
trochu	trochu	k6eAd1
rozptýlenou	rozptýlený	k2eAgFnSc4d1
mlhavost	mlhavost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
poloha	poloha	k1gFnSc1
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
pomocí	pomocí	k7c2
hvězdy	hvězda	k1gFnSc2
Epsilon	epsilon	k1gNnSc2
Zajíce	Zajíc	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
čtvrté	čtvrtý	k4xOgFnPc1
magnitudy	magnituda	k1gFnPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
hvězdy	hvězda	k1gFnPc4
ji	on	k3xPp3gFnSc4
kolem	kolem	k7c2
roku	rok	k1gInSc2
1784	#num#	k4
svým	svůj	k3xOyFgInSc7
velkým	velký	k2eAgInSc7d1
dalekohledem	dalekohled	k1gInSc7
rozložil	rozložit	k5eAaPmAgMnS
William	William	k1gInSc4
Herschel	Herschel	k1gMnSc1
a	a	k8xC
popsal	popsat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
jako	jako	k8xC,k8xS
velmi	velmi	k6eAd1
bohatou	bohatý	k2eAgFnSc4d1
kulovou	kulový	k2eAgFnSc4d1
hvězdokupu	hvězdokupa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
M79	M79	k4
je	být	k5eAaImIp3nS
od	od	k7c2
Země	zem	k1gFnSc2
vzdálená	vzdálený	k2eAgFnSc1d1
okolo	okolo	k7c2
42	#num#	k4
100	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
a	a	k8xC
od	od	k7c2
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
asi	asi	k9
60	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jejímu	její	k3xOp3gInSc3
úhlovému	úhlový	k2eAgInSc3d1
rozměru	rozměr	k1gInSc3
9,6	9,6	k4
<g/>
'	'	kIx"
v	v	k7c6
této	tento	k3xDgFnSc6
vzdálenosti	vzdálenost	k1gFnSc6
odpovídá	odpovídat	k5eAaImIp3nS
skutečná	skutečný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
přibližně	přibližně	k6eAd1
118	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Má	mít	k5eAaImIp3nS
mírně	mírně	k6eAd1
protažený	protažený	k2eAgInSc4d1
tvar	tvar	k1gInSc4
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
v	v	k7c6
ní	on	k3xPp3gFnSc6
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
pouze	pouze	k6eAd1
12	#num#	k4
nebo	nebo	k8xC
13	#num#	k4
proměnných	proměnný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
Země	zem	k1gFnSc2
se	se	k3xPyFc4
vzdaluje	vzdalovat	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
kolem	kolem	k7c2
206	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Předpokládané	předpokládaný	k2eAgNnSc1d1
stáří	stáří	k1gNnSc1
hvězdokupy	hvězdokupa	k1gFnSc2
je	být	k5eAaImIp3nS
11,7	11,7	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Velkého	velký	k2eAgMnSc2d1
psa	pes	k1gMnSc2
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
výrazné	výrazný	k2eAgNnSc1d1
zhuštění	zhuštění	k1gNnSc1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
prohlášeno	prohlásit	k5eAaPmNgNnS
za	za	k7c4
trpasličí	trpasličí	k2eAgFnSc4d1
galaxii	galaxie	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
trpasličí	trpasličí	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
Velký	velký	k2eAgMnSc1d1
pes	pes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
M79	M79	k1gFnPc2
by	by	kYmCp3nS
tak	tak	k9
mohla	moct	k5eAaImAgFnS
pocházet	pocházet	k5eAaImF
z	z	k7c2
této	tento	k3xDgFnSc2
trpasličí	trpasličí	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
fyzicky	fyzicky	k6eAd1
blízko	blízko	k6eAd1
ní	on	k3xPp3gFnSc7
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
je	být	k5eAaImIp3nS
Messier	Messier	k1gInSc4
54	#num#	k4
součástí	součást	k1gFnPc2
trpasličí	trpasličí	k2eAgFnSc2d1
eliptické	eliptický	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
ve	v	k7c6
Střelci	Střelec	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novější	nový	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
ovšem	ovšem	k9
zpochybnily	zpochybnit	k5eAaPmAgInP
podstatu	podstata	k1gFnSc4
galaxie	galaxie	k1gFnSc2
ve	v	k7c6
Velkém	velký	k2eAgMnSc6d1
psu	pes	k1gMnSc6
a	a	k8xC
pozorované	pozorovaný	k2eAgNnSc1d1
zhuštění	zhuštění	k1gNnSc1
hvězd	hvězda	k1gFnPc2
připisují	připisovat	k5eAaImIp3nP
pouhému	pouhý	k2eAgNnSc3d1
prohnutí	prohnutí	k1gNnSc3
galaktického	galaktický	k2eAgInSc2d1
disku	disk	k1gInSc2
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
M79	M79	k1gFnSc2
(	(	kIx(
<g/>
astronomia	astronomia	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
SEDS	SEDS	kA
Messier	Messier	k1gInSc1
Objects	Objects	k1gInSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Messier	Messier	k1gInSc1
79	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
HARRIS	HARRIS	kA
<g/>
,	,	kIx,
W.	W.	kA
E.	E.	kA
A	A	kA
Catalog	Catalog	k1gMnSc1
of	of	k?
Parameters	Parameters	k1gInSc1
for	forum	k1gNnPc2
Globular	Globulara	k1gFnPc2
Clusters	Clustersa	k1gFnPc2
in	in	k?
the	the	k?
Milky	Milka	k1gFnSc2
Way	Way	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1487	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomical	Astronomical	k1gFnSc1
Journal	Journal	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1996	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
112	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1487	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
118116	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1996	#num#	k4
<g/>
AJ	aj	kA
<g/>
...	...	k?
<g/>
.112	.112	k4
<g/>
.1487	.1487	k4
<g/>
H.	H.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
William	William	k1gInSc1
E.	E.	kA
Harris	Harris	k1gFnSc2
<g/>
:	:	kIx,
A	a	k9
catalog	catalog	k1gMnSc1
of	of	k?
parameters	parameters	k6eAd1
for	forum	k1gNnPc2
globular	globulara	k1gFnPc2
clusters	clustersa	k1gFnPc2
in	in	k?
the	the	k?
Milky	Milka	k1gFnSc2
Way	Way	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Astronomical	Astronomical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
118116	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sangmo	Sangma	k1gFnSc5
Sohn	Sohn	k1gNnSc1
<g/>
,	,	kIx,
Ricardo	Ricardo	k1gNnSc1
P.	P.	kA
Schiavon	Schiavon	k1gNnSc4
<g/>
:	:	kIx,
Ultraviolet	Ultraviolet	k1gInSc1
properties	properties	k1gInSc1
of	of	k?
Galactic	Galactice	k1gFnPc2
globular	globulara	k1gFnPc2
clusters	clusters	k1gInSc1
with	with	k1gMnSc1
GALEX	GALEX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrated	Integrated	k1gInSc1
colors	colors	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Astronomical	Astronomical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.108	10.108	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6256	#num#	k4
<g/>
/	/	kIx~
<g/>
144	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
126	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SIMBAD	SIMBAD	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uniting	Uniting	k1gInSc1
old	old	k?
stellar	stellar	k1gInSc1
systems	systems	k1gInSc1
<g/>
:	:	kIx,
from	from	k1gInSc1
globular	globulara	k1gFnPc2
clusters	clusters	k6eAd1
to	ten	k3xDgNnSc4
giant	giant	k1gInSc1
ellipticals	ellipticals	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Monthly	Monthly	k1gMnSc1
Notices	Notices	k1gMnSc1
of	of	k?
the	the	k?
Royal	Royal	k1gInSc4
Astronomical	Astronomical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
J	J	kA
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2966.2008	2966.2008	k4
<g/>
.13739	.13739	k4
<g/>
.	.	kIx.
<g/>
X	X	kA
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Laura	Laura	k1gFnSc1
Ferrarese	Ferrarese	k1gFnSc1
<g/>
,	,	kIx,
Maarten	Maarten	k2eAgInSc1d1
Baes	Baes	k1gInSc1
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Côté	Côtá	k1gFnSc2
<g/>
:	:	kIx,
G2C2	G2C2	k1gFnSc1
–	–	k?
I.	I.	kA
Homogeneous	Homogeneous	k1gInSc1
photometry	photometr	k1gInPc1
for	forum	k1gNnPc2
Galactic	Galactice	k1gFnPc2
globular	globulara	k1gFnPc2
clusters	clusters	k1gInSc1
in	in	k?
SDSS	SDSS	kA
passbands	passbands	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Monthly	Monthly	k1gMnSc1
Notices	Notices	k1gMnSc1
of	of	k?
the	the	k?
Royal	Royal	k1gInSc4
Astronomical	Astronomical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
MNRAS	MNRAS	kA
<g/>
/	/	kIx~
<g/>
STT	STT	kA
<g/>
2002.1	2002.1	k4
2	#num#	k4
KOLEVA	KOLEVA	kA
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
PRUGNIEL	PRUGNIEL	kA
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
OCVIRK	OCVIRK	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spectroscopic	Spectroscopice	k1gInPc2
ages	ages	k1gInSc4
and	and	k?
metallicities	metallicities	k1gInSc1
of	of	k?
stellar	stellar	k1gInSc1
populations	populations	k1gInSc1
<g/>
:	:	kIx,
validation	validation	k1gInSc1
of	of	k?
full	full	k1gInSc1
spectrum	spectrum	k1gNnSc1
fitting	fitting	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monthly	Monthly	k1gMnSc1
Notices	Notices	k1gMnSc1
of	of	k?
the	the	k?
Royal	Royal	k1gInSc4
Astronomical	Astronomical	k1gMnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
385	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
801.087	801.087	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1365	.1365	k4
<g/>
-	-	kIx~
<g/>
2966.2008	2966.2008	k4
<g/>
.12908	.12908	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2008	#num#	k4
<g/>
MNRAS	MNRAS	kA
<g/>
.385	.385	k4
<g/>
.1998	.1998	k4
<g/>
K.	K.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc3
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
M	M	kA
79	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MELOTTE	MELOTTE	kA
<g/>
,	,	kIx,
P.	P.	kA
J.	J.	kA
A	a	k9
Catalogue	Catalogue	k1gFnSc1
of	of	k?
Star	Star	kA
Clusters	Clusters	k1gInSc1
shown	shown	k1gInSc1
on	on	k3xPp3gMnSc1
Franklin-Adams	Franklin-Adams	k1gInSc1
Chart	charta	k1gFnPc2
Plates	Platesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
175	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memoirs	Memoirs	k1gInSc1
of	of	k?
the	the	k?
Royal	Royal	k1gInSc4
Astronomical	Astronomical	k1gMnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1915	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
60	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
175	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1915	#num#	k4
<g/>
MmRAS	MmRAS	k1gFnPc2
<g/>
.	.	kIx.
<g/>
.60	.60	k4
<g/>
.	.	kIx.
<g/>
.175	.175	k4
<g/>
M.	M.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MANZINI	MANZINI	kA
<g/>
,	,	kIx,
Federico	Federico	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Il	Il	k1gMnSc1
Catalogo	Catalogo	k1gMnSc1
di	di	k?
Messier	Messier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nuovo	Nuovo	k1gNnSc1
Orione	orion	k1gInSc5
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deklinace	deklinace	k1gFnSc1
25	#num#	k4
<g/>
°	°	k?
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
odpovídá	odpovídat	k5eAaImIp3nS
úhlové	úhlový	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
65	#num#	k4
<g/>
°	°	k?
od	od	k7c2
jižního	jižní	k2eAgInSc2d1
nebeského	nebeský	k2eAgInSc2d1
pólu	pól	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
65	#num#	k4
<g/>
°	°	k?
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
tato	tento	k3xDgFnSc1
hvězdokupa	hvězdokupa	k1gFnSc1
cirkumpolární	cirkumpolární	k2eAgFnSc1d1
(	(	kIx(
<g/>
nikdy	nikdy	k6eAd1
nezapadá	zapadat	k5eNaImIp3nS,k5eNaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
severně	severně	k6eAd1
od	od	k7c2
65	#num#	k4
<g/>
°	°	k?
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
objekt	objekt	k1gInSc1
vůbec	vůbec	k9
nevychází	vycházet	k5eNaImIp3nS
nad	nad	k7c4
obzor	obzor	k1gInSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LÓPEZ-CORREDOIRA	LÓPEZ-CORREDOIRA	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
MOMANY	MOMANY	kA
<g/>
,	,	kIx,
Y.	Y.	kA
<g/>
;	;	kIx,
ZAGGIA	ZAGGIA	kA
<g/>
,	,	kIx,
S.	S.	kA
<g/>
;	;	kIx,
CABRERA-LAVERS	CABRERA-LAVERS	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
Re-affirming	Re-affirming	k1gInSc1
the	the	k?
connection	connection	k1gInSc1
between	between	k1gInSc1
the	the	k?
Galactic	Galactice	k1gFnPc2
stellar	stellar	k1gMnSc1
warp	warp	k1gMnSc1
and	and	k?
the	the	k?
Canis	Canis	k1gFnSc7
Major	major	k1gMnSc1
over-density	over-densit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
L	L	kA
<g/>
47	#num#	k4
<g/>
-L	-L	k?
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomy	astronom	k1gMnPc4
and	and	k?
Astrophysics	Astrophysics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-09-04	2007-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
472	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
L	L	kA
<g/>
47	#num#	k4
<g/>
-L	-L	k?
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
707.444	707.444	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6361	#num#	k4
<g/>
:	:	kIx,
<g/>
20077813	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2007	#num#	k4
<g/>
A	A	kA
<g/>
&	&	k?
<g/>
A.	A.	kA
<g/>
.	.	kIx.
<g/>
.472	.472	k4
<g/>
L.	L.	kA
<g/>
.47	.47	k4
<g/>
L.	L.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
O	O	kA
<g/>
'	'	kIx"
<g/>
MEARA	MEARA	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgMnSc1d1
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deep	Deep	k1gInSc1
Sky	Sky	k1gFnSc2
Companions	Companionsa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Messier	Messier	k1gMnSc1
Objects	Objects	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
55332	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mapy	mapa	k1gFnPc1
hvězdné	hvězdný	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
</s>
<s>
Toshimi	Toshi	k1gFnPc7
Taki	Taki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taki	Taki	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
8.5	8.5	k4
Magnitude	Magnitud	k1gInSc5
Star	Star	kA
Atlas	Atlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
-	-	kIx~
Atlas	Atlas	k1gInSc1
hvězdné	hvězdný	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
volně	volně	k6eAd1
stažitelný	stažitelný	k2eAgMnSc1d1
ve	v	k7c6
formátu	formát	k1gInSc6
PDF	PDF	kA
<g/>
.	.	kIx.
</s>
<s>
TIRION	TIRION	kA
<g/>
,	,	kIx,
RAPPAPORT	RAPPAPORT	kA
<g/>
,	,	kIx,
LOVI	LOVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uranometria	Uranometrium	k1gNnSc2
2000.0	2000.0	k4
-	-	kIx~
Volume	volum	k1gInSc5
II	II	kA
-	-	kIx~
The	The	k1gMnSc1
Southern	Southern	k1gMnSc1
Hemisphere	Hemispher	k1gInSc5
to	ten	k3xDgNnSc1
+6	+6	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richmond	Richmonda	k1gFnPc2
<g/>
,	,	kIx,
Virginia	Virginium	k1gNnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Willmann-Bell	Willmann-Bell	k1gMnSc1
<g/>
,	,	kIx,
inc	inc	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
943396	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TIRION	TIRION	kA
<g/>
,	,	kIx,
SINNOTT	SINNOTT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gMnSc1
Atlas	Atlas	k1gMnSc1
2000.0	2000.0	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
933346	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TIRION	TIRION	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc1
Star	star	k1gFnSc1
Atlas	Atlas	k1gInSc1
2000.0	2000.0	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
80084	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Messier	Messier	k1gInSc1
79	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
M	M	kA
79	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SEDS	SEDS	kA
Messier	Messier	k1gInSc1
Objects	Objects	k1gInSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Messier	Messier	k1gInSc1
79	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
POWELL	POWELL	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc1
<g/>
:	:	kIx,
Globular	Globular	k1gInSc1
Clusters	Clusters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KODRIŠ	KODRIŠ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodka	k1gFnSc6
hvězdnou	hvězdný	k2eAgFnSc7d1
oblohou	obloha	k1gFnSc7
<g/>
:	:	kIx,
Zajíc	Zajíc	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
NASA	NASA	kA
-	-	kIx~
APOD.	apod.	kA
Astronomický	astronomický	k2eAgInSc1d1
snímek	snímek	k1gInSc1
dne	den	k1gInSc2
-	-	kIx~
Kometa	kometa	k1gFnSc1
Lovejoy	Lovejoa	k1gFnSc2
před	před	k7c7
kulovou	kulový	k2eAgFnSc7d1
hvězdokupou	hvězdokupa	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
astro	astra	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-12-31	2014-12-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Objekty	objekt	k1gInPc1
v	v	k7c6
Messierově	Messierův	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
</s>
<s>
M	M	kA
1	#num#	k4
</s>
<s>
M	M	kA
2	#num#	k4
</s>
<s>
M	M	kA
3	#num#	k4
</s>
<s>
M	M	kA
4	#num#	k4
</s>
<s>
M	M	kA
5	#num#	k4
</s>
<s>
M	M	kA
6	#num#	k4
</s>
<s>
M	M	kA
7	#num#	k4
</s>
<s>
M	M	kA
8	#num#	k4
</s>
<s>
M	M	kA
9	#num#	k4
</s>
<s>
M	M	kA
10	#num#	k4
</s>
<s>
M	M	kA
11	#num#	k4
</s>
<s>
M	M	kA
12	#num#	k4
</s>
<s>
M	M	kA
13	#num#	k4
</s>
<s>
M	M	kA
14	#num#	k4
</s>
<s>
M	M	kA
15	#num#	k4
</s>
<s>
M	M	kA
16	#num#	k4
</s>
<s>
M	M	kA
17	#num#	k4
</s>
<s>
M	M	kA
18	#num#	k4
</s>
<s>
M	M	kA
19	#num#	k4
</s>
<s>
M	M	kA
20	#num#	k4
</s>
<s>
M	M	kA
21	#num#	k4
</s>
<s>
M	M	kA
22	#num#	k4
</s>
<s>
M	M	kA
23	#num#	k4
</s>
<s>
M	M	kA
24	#num#	k4
</s>
<s>
M	M	kA
25	#num#	k4
</s>
<s>
M	M	kA
26	#num#	k4
</s>
<s>
M	M	kA
27	#num#	k4
</s>
<s>
M	M	kA
28	#num#	k4
</s>
<s>
M	M	kA
29	#num#	k4
</s>
<s>
M	M	kA
30	#num#	k4
</s>
<s>
M	M	kA
31	#num#	k4
</s>
<s>
M	M	kA
32	#num#	k4
</s>
<s>
M	M	kA
33	#num#	k4
</s>
<s>
M	M	kA
34	#num#	k4
</s>
<s>
M	M	kA
35	#num#	k4
</s>
<s>
M	M	kA
36	#num#	k4
</s>
<s>
M	M	kA
37	#num#	k4
</s>
<s>
M	M	kA
38	#num#	k4
</s>
<s>
M	M	kA
39	#num#	k4
</s>
<s>
M	M	kA
40	#num#	k4
</s>
<s>
M	M	kA
41	#num#	k4
</s>
<s>
M	M	kA
42	#num#	k4
</s>
<s>
M	M	kA
43	#num#	k4
</s>
<s>
M	M	kA
44	#num#	k4
</s>
<s>
M	M	kA
45	#num#	k4
</s>
<s>
M	M	kA
46	#num#	k4
</s>
<s>
M	M	kA
47	#num#	k4
</s>
<s>
M	M	kA
48	#num#	k4
</s>
<s>
M	M	kA
49	#num#	k4
</s>
<s>
M	M	kA
50	#num#	k4
</s>
<s>
M	M	kA
51	#num#	k4
</s>
<s>
M	M	kA
52	#num#	k4
</s>
<s>
M	M	kA
53	#num#	k4
</s>
<s>
M	M	kA
54	#num#	k4
</s>
<s>
M	M	kA
55	#num#	k4
</s>
<s>
M	M	kA
56	#num#	k4
</s>
<s>
M	M	kA
57	#num#	k4
</s>
<s>
M	M	kA
58	#num#	k4
</s>
<s>
M	M	kA
59	#num#	k4
</s>
<s>
M	M	kA
60	#num#	k4
</s>
<s>
M	M	kA
61	#num#	k4
</s>
<s>
M	M	kA
62	#num#	k4
</s>
<s>
M	M	kA
63	#num#	k4
</s>
<s>
M	M	kA
64	#num#	k4
</s>
<s>
M	M	kA
65	#num#	k4
</s>
<s>
M	M	kA
66	#num#	k4
</s>
<s>
M	M	kA
67	#num#	k4
</s>
<s>
M	M	kA
68	#num#	k4
</s>
<s>
M	M	kA
69	#num#	k4
</s>
<s>
M	M	kA
70	#num#	k4
</s>
<s>
M	M	kA
71	#num#	k4
</s>
<s>
M	M	kA
72	#num#	k4
</s>
<s>
M	M	kA
73	#num#	k4
</s>
<s>
M	M	kA
74	#num#	k4
</s>
<s>
M	M	kA
75	#num#	k4
</s>
<s>
M	M	kA
76	#num#	k4
</s>
<s>
M	M	kA
77	#num#	k4
</s>
<s>
M	M	kA
78	#num#	k4
</s>
<s>
M	M	kA
79	#num#	k4
</s>
<s>
M	M	kA
80	#num#	k4
</s>
<s>
M	M	kA
81	#num#	k4
</s>
<s>
M	M	kA
82	#num#	k4
</s>
<s>
M	M	kA
83	#num#	k4
</s>
<s>
M	M	kA
84	#num#	k4
</s>
<s>
M	M	kA
85	#num#	k4
</s>
<s>
M	M	kA
86	#num#	k4
</s>
<s>
M	M	kA
87	#num#	k4
</s>
<s>
M	M	kA
88	#num#	k4
</s>
<s>
M	M	kA
89	#num#	k4
</s>
<s>
M	M	kA
90	#num#	k4
</s>
<s>
M	M	kA
91	#num#	k4
</s>
<s>
M	M	kA
92	#num#	k4
</s>
<s>
M	M	kA
93	#num#	k4
</s>
<s>
M	M	kA
94	#num#	k4
</s>
<s>
M	M	kA
95	#num#	k4
</s>
<s>
M	M	kA
96	#num#	k4
</s>
<s>
M	M	kA
97	#num#	k4
</s>
<s>
M	M	kA
98	#num#	k4
</s>
<s>
M	M	kA
99	#num#	k4
</s>
<s>
M	M	kA
100	#num#	k4
</s>
<s>
M	M	kA
101	#num#	k4
</s>
<s>
M	M	kA
102	#num#	k4
</s>
<s>
M	M	kA
103	#num#	k4
</s>
<s>
M	M	kA
104	#num#	k4
</s>
<s>
M	M	kA
105	#num#	k4
</s>
<s>
M	M	kA
106	#num#	k4
</s>
<s>
M	M	kA
107	#num#	k4
</s>
<s>
M	M	kA
108	#num#	k4
</s>
<s>
M	M	kA
109	#num#	k4
</s>
<s>
M	M	kA
110	#num#	k4
</s>
