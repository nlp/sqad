<s>
Pomáda	pomáda	k1gFnSc1	pomáda
(	(	kIx(	(
<g/>
Grease	Greasa	k1gFnSc6	Greasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
muzikální	muzikální	k2eAgInSc1d1	muzikální
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
režisérem	režisér	k1gMnSc7	režisér
Randalem	Randal	k1gMnSc7	Randal
Kleiserem	Kleiser	k1gMnSc7	Kleiser
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Olivia	Olivius	k1gMnSc4	Olivius
Newton-John	Newton-John	k1gMnSc1	Newton-John
a	a	k8xC	a
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
,	,	kIx,	,
Olivia	Olivia	k1gFnSc1	Olivia
Newton-John	Newton-John	k1gMnSc1	Newton-John
<g/>
,	,	kIx,	,
Stockard	Stockard	k1gMnSc1	Stockard
Channing	Channing	k1gInSc1	Channing
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gInSc1	Jeff
Conaway	Conawaa	k1gFnSc2	Conawaa
<g/>
,	,	kIx,	,
Jamie	Jamie	k1gFnSc2	Jamie
Donnelly	Donnella	k1gFnSc2	Donnella
<g/>
,	,	kIx,	,
Eve	Eve	k1gFnSc2	Eve
Arden	Ardeny	k1gFnPc2	Ardeny
<g/>
,	,	kIx,	,
Dinah	Dinah	k1gMnSc1	Dinah
Manoff	Manoff	k1gMnSc1	Manoff
<g/>
,	,	kIx,	,
Frankie	Frankie	k1gFnSc1	Frankie
Avalon	Avalon	k1gInSc1	Avalon
<g/>
,	,	kIx,	,
Joan	Joan	k1gNnSc1	Joan
Blondell	Blondella	k1gFnPc2	Blondella
<g/>
,	,	kIx,	,
Edd	Edda	k1gFnPc2	Edda
Byrnes	Byrnesa	k1gFnPc2	Byrnesa
<g/>
,	,	kIx,	,
Sid	Sid	k1gMnSc1	Sid
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Lamas	Lamas	k1gInSc1	Lamas
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc1	Eddie
Deezen	Deezna	k1gFnPc2	Deezna
<g/>
,	,	kIx,	,
Ellen	Ellen	k1gInSc4	Ellen
Travolta	Travolt	k1gMnSc2	Travolt
<g/>
,	,	kIx,	,
Antonia	Antonio	k1gMnSc2	Antonio
Franceschi	Francesch	k1gFnSc2	Francesch
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnPc1	Anda
Tennant	Tennant	k1gMnSc1	Tennant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Biehn	Biehn	k1gMnSc1	Biehn
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Tucci	Tucce	k1gFnSc4	Tucce
<g/>
,	,	kIx,	,
Didi	Didi	k1gNnSc4	Didi
Conn	Conna	k1gFnPc2	Conna
<g/>
,	,	kIx,	,
Dennis	Dennis	k1gFnSc2	Dennis
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Sean	Sean	k1gMnSc1	Sean
Moran	Morana	k1gFnPc2	Morana
<g/>
,	,	kIx,	,
Fannie	Fannie	k1gFnSc1	Fannie
Flagg	Flagga	k1gFnPc2	Flagga
</s>
