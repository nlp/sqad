<s>
Pomáda	pomáda	k1gFnSc1	pomáda
(	(	kIx(	(
<g/>
Grease	Greasa	k1gFnSc6	Greasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
muzikální	muzikální	k2eAgInSc1d1	muzikální
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
režisérem	režisér	k1gMnSc7	režisér
Randalem	Randal	k1gMnSc7	Randal
Kleiserem	Kleiser	k1gMnSc7	Kleiser
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Olivia	Olivius	k1gMnSc4	Olivius
Newton-John	Newton-John	k1gMnSc1	Newton-John
a	a	k8xC	a
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
.	.	kIx.	.
</s>
