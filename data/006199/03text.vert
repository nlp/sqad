<s>
Argot	argot	k1gInSc1	argot
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
žargon	žargon	k1gInSc1	žargon
užívaný	užívaný	k2eAgInSc1d1	užívaný
společenskou	společenský	k2eAgFnSc7d1	společenská
spodinou	spodina	k1gFnSc7	spodina
(	(	kIx(	(
<g/>
zloději	zloděj	k1gMnPc1	zloděj
<g/>
,	,	kIx,	,
překupníky	překupník	k1gMnPc4	překupník
<g/>
,	,	kIx,	,
narkomany	narkoman	k1gMnPc4	narkoman
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
tvoření	tvoření	k1gNnSc2	tvoření
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
pojmenování	pojmenování	k1gNnPc2	pojmenování
je	být	k5eAaImIp3nS	být
utajit	utajit	k5eAaPmF	utajit
obsah	obsah	k1gInSc4	obsah
sdělení	sdělení	k1gNnSc2	sdělení
před	před	k7c7	před
nezasvěcenými	zasvěcený	k2eNgMnPc7d1	nezasvěcený
posluchači	posluchač	k1gMnPc7	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
argotem	argot	k1gInSc7	argot
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
argotologie	argotologie	k1gFnSc1	argotologie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
výrazy	výraz	k1gInPc1	výraz
z	z	k7c2	z
argotu	argot	k1gInSc2	argot
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
prozrazeny	prozrazen	k2eAgInPc1d1	prozrazen
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přestanou	přestat	k5eAaPmIp3nP	přestat
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
argotu	argot	k1gInSc2	argot
a	a	k8xC	a
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
běžné	běžný	k2eAgFnSc2d1	běžná
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
argotu	argot	k1gInSc2	argot
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
verlan	verlan	k1gInSc1	verlan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
utajení	utajení	k1gNnSc3	utajení
prohazování	prohazování	k1gNnSc2	prohazování
slabik	slabika	k1gFnPc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Kalita	Kalita	k1gFnSc1	Kalita
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
argot	argot	k1gInSc4	argot
jako	jako	k8xC	jako
tajný	tajný	k2eAgInSc4d1	tajný
komunikativní	komunikativní	k2eAgInSc4d1	komunikativní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
prostředkem	prostředek	k1gInSc7	prostředek
omezeného	omezený	k2eAgNnSc2d1	omezené
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
v	v	k7c6	v
dichotomické	dichotomický	k2eAgFnSc6d1	dichotomická
opozici	opozice	k1gFnSc6	opozice
dozorce	dozorce	k1gMnSc1	dozorce
–	–	k?	–
odsouzený	odsouzený	k1gMnSc1	odsouzený
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzený	odsouzený	k1gMnSc1	odsouzený
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
u	u	k7c2	u
známých	známý	k2eAgInPc2d1	známý
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
lexémů	lexém	k1gInPc2	lexém
známky	známka	k1gFnSc2	známka
sekundární	sekundární	k2eAgFnSc1d1	sekundární
sémantiky	sémantika	k1gFnPc1	sémantika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
srozumitelné	srozumitelný	k2eAgFnPc4d1	srozumitelná
dozorci	dozorce	k1gMnPc7	dozorce
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
tvoří	tvořit	k5eAaImIp3nS	tvořit
lexém	lexém	k1gInSc1	lexém
úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc1d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Argot	argot	k1gInSc1	argot
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
teritoriálně	teritoriálně	k6eAd1	teritoriálně
omezeném	omezený	k2eAgInSc6d1	omezený
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
podmíněn	podmíněn	k2eAgMnSc1d1	podmíněn
omezujícím	omezující	k2eAgInSc7d1	omezující
(	(	kIx(	(
<g/>
trestním	trestní	k2eAgInSc7d1	trestní
<g/>
)	)	kIx)	)
rámcem	rámec	k1gInSc7	rámec
–	–	k?	–
omezením	omezení	k1gNnSc7	omezení
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nositelé	nositel	k1gMnPc1	nositel
argotu	argot	k1gInSc2	argot
se	se	k3xPyFc4	se
nesnaží	snažit	k5eNaImIp3nS	snažit
vyčnívat	vyčnívat	k5eAaImF	vyčnívat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kladou	klást	k5eAaImIp3nP	klást
si	se	k3xPyFc3	se
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
izolovat	izolovat	k5eAaBmF	izolovat
své	svůj	k3xOyFgNnSc4	svůj
prostředí	prostředí	k1gNnSc4	prostředí
od	od	k7c2	od
vnějšího	vnější	k2eAgNnSc2d1	vnější
<g/>
.	.	kIx.	.
bahno	bahno	k1gNnSc1	bahno
–	–	k?	–
pivo	pivo	k1gNnSc1	pivo
basa	basa	k1gFnSc1	basa
–	–	k?	–
vězení	vězení	k1gNnSc2	vězení
benga	beng	k1gMnSc2	beng
<g/>
,	,	kIx,	,
fízl	fízl	k?	fízl
<g/>
,	,	kIx,	,
chlupatej	chlupatej	k?	chlupatej
<g/>
,	,	kIx,	,
švestky	švestka	k1gFnPc1	švestka
–	–	k?	–
policista	policista	k1gMnSc1	policista
<g/>
/	/	kIx~	/
<g/>
é	é	k0	é
<g/>
,	,	kIx,	,
strážník	strážník	k1gMnSc1	strážník
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ci	ci	k0	ci
bručet	bručet	k5eAaImF	bručet
(	(	kIx(	(
<g/>
v	v	k7c6	v
base	basa	k1gFnSc6	basa
<g/>
)	)	kIx)	)
–	–	k?	–
sedět	sedět	k5eAaImF	sedět
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
čokl	čokl	k?	čokl
–	–	k?	–
pes	pes	k1gMnSc1	pes
čorkařit	čorkařit	k5eAaPmF	čorkařit
<g/>
,	,	kIx,	,
čórkařit	čórkařit	k5eAaImF	čórkařit
–	–	k?	–
krást	krást	k5eAaImF	krást
čórnout	čórnout	k5eAaImF	čórnout
<g/>
,	,	kIx,	,
lohnout	lohnout	k5eAaPmF	lohnout
–	–	k?	–
ukrást	ukrást	k5eAaPmF	ukrást
dacan	dacan	k?	dacan
–	–	k?	–
vagabund	vagabund	k?	vagabund
<g/>
,	,	kIx,	,
darebák	darebák	k1gMnSc1	darebák
<g/>
,	,	kIx,	,
potvora	potvora	k1gFnSc1	potvora
káča	káča	k1gFnSc1	káča
–	–	k?	–
kasa	kasa	k1gFnSc1	kasa
<g/>
,	,	kIx,	,
trezor	trezor	k1gInSc1	trezor
krochna	krochna	k1gFnSc1	krochna
<g/>
,	,	kIx,	,
stříkačka	stříkačka	k1gFnSc1	stříkačka
–	–	k?	–
pistole	pistol	k1gFnSc2	pistol
love	lov	k1gInSc5	lov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
prachy	prach	k1gInPc4	prach
<g/>
,	,	kIx,	,
škvára	škvára	k1gFnSc1	škvára
–	–	k?	–
peníze	peníz	k1gInPc1	peníz
máčka	máčka	k1gFnSc1	máčka
–	–	k?	–
marlbora	marlbora	k1gFnSc1	marlbora
<g/>
,	,	kIx,	,
cigarety	cigareta	k1gFnPc1	cigareta
značky	značka	k1gFnSc2	značka
Marlboro	Marlboro	k1gNnSc2	Marlboro
oddělat	oddělat	k5eAaPmF	oddělat
<g/>
,	,	kIx,	,
voddělat	voddělat	k5eAaImF	voddělat
<g/>
,	,	kIx,	,
sejmout	sejmout	k5eAaPmF	sejmout
-	-	kIx~	-
zabít	zabít	k5eAaPmF	zabít
odprásknout	odprásknout	k5eAaPmF	odprásknout
<g/>
,	,	kIx,	,
vodprásknout	vodprásknout	k5eAaImF	vodprásknout
–	–	k?	–
zastřelit	zastřelit	k5eAaPmF	zastřelit
puma	puma	k1gFnSc1	puma
–	–	k?	–
vyšetřovací	vyšetřovací	k2eAgMnPc4d1	vyšetřovací
soudce	soudce	k1gMnPc4	soudce
raky	rak	k1gMnPc4	rak
–	–	k?	–
hodinky	hodinka	k1gFnPc4	hodinka
rychna	rychn	k1gInSc2	rychn
–	–	k?	–
zápach	zápach	k1gInSc1	zápach
(	(	kIx(	(
<g/>
brn	brn	k?	brn
<g/>
.	.	kIx.	.
hantec	hantec	k1gMnSc1	hantec
<g/>
)	)	kIx)	)
skéro	skéro	k6eAd1	skéro
–	–	k?	–
marihuana	marihuana	k1gFnSc1	marihuana
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
vybílit	vybílit	k5eAaPmF	vybílit
–	–	k?	–
vykrást	vykrást	k5eAaPmF	vykrást
velryba	velryba	k1gFnSc1	velryba
–	–	k?	–
manželka	manželka	k1gFnSc1	manželka
vodprejskni	vodprejsknout	k5eAaPmRp2nS	vodprejsknout
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
<g/>
:	:	kIx,	:
odprejskni	odprejsknout	k5eAaPmRp2nS	odprejsknout
<g/>
,	,	kIx,	,
emaile	email	k1gInSc5	email
<g/>
)	)	kIx)	)
–	–	k?	–
odejdi	odejít	k5eAaPmRp2nS	odejít
Oberpfalcer	Oberpfalcer	k1gMnSc1	Oberpfalcer
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Slang	slang	k1gInSc1	slang
a	a	k8xC	a
argot	argot	k1gInSc1	argot
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Jazykozpyt	jazykozpyt	k1gInSc1	jazykozpyt
<g/>
.	.	kIx.	.
</s>
<s>
JČF	JČF	kA	JČF
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Oberpfalcer	Oberpfalcer	k1gMnSc1	Oberpfalcer
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Argot	argot	k1gInSc1	argot
a	a	k8xC	a
slangy	slang	k1gInPc1	slang
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Československá	československý	k2eAgFnSc1d1	Československá
vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sfinx	sfinx	k1gFnSc1	sfinx
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Ouředník	Ouředník	k1gMnSc1	Ouředník
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Argot	argot	k1gInSc1	argot
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
–	–	k?	–
Argot	argot	k1gInSc4	argot
v	v	k7c6	v
beletrii	beletrie	k1gFnSc6	beletrie
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Šmírbuch	šmírbuch	k1gInSc1	šmírbuch
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
Paseka	paseka	k1gFnSc1	paseka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
638	[number]	k4	638
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Kalita	Kalita	k1gMnSc1	Kalita
I.	I.	kA	I.
<g/>
:	:	kIx,	:
Obrysy	obrys	k1gInPc1	obrys
a	a	k8xC	a
tvary	tvar	k1gInPc1	tvar
nespisovnosti	nespisovnost	k1gFnSc2	nespisovnost
<g/>
:	:	kIx,	:
ruština	ruština	k1gFnSc1	ruština
vs	vs	k?	vs
<g/>
.	.	kIx.	.
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
slang	slang	k1gInSc1	slang
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7414	[number]	k4	7414
<g/>
-	-	kIx~	-
<g/>
427	[number]	k4	427
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
argot	argot	k1gInSc1	argot
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
PRAVDOVÁ	PRAVDOVÁ	kA	PRAVDOVÁ
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Zaražený	zaražený	k2eAgInSc4d1	zaražený
prdy	prd	k1gInPc4	prd
<g/>
,	,	kIx,	,
křapíček	křapíček	k1gInSc4	křapíček
a	a	k8xC	a
elpíčko	elpíčko	k1gNnSc4	elpíčko
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
mezi	mezi	k7c7	mezi
zločinci	zločinec	k1gMnPc7	zločinec
<g/>
.	.	kIx.	.
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2014-09-01	[number]	k4	2014-09-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
PRAVDOVÁ	PRAVDOVÁ	kA	PRAVDOVÁ
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Termobuk	Termobuk	k1gMnSc1	Termobuk
<g/>
,	,	kIx,	,
pytložvejk	pytložvejk	k1gMnSc1	pytložvejk
<g/>
,	,	kIx,	,
marcela	marcet	k5eAaImAgFnS	marcet
<g/>
.	.	kIx.	.
</s>
<s>
Naučte	naučit	k5eAaPmRp2nP	naučit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k9	co
vás	vy	k3xPp2nPc4	vy
čeká	čekat	k5eAaImIp3nS	čekat
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2014-09-01	[number]	k4	2014-09-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
PRAVDOVÁ	PRAVDOVÁ	kA	PRAVDOVÁ
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
správně	správně	k6eAd1	správně
nadávat	nadávat	k5eAaImF	nadávat
a	a	k8xC	a
vyhrožovat	vyhrožovat	k5eAaImF	vyhrožovat
<g/>
.	.	kIx.	.
</s>
<s>
Naučte	naučit	k5eAaPmRp2nP	naučit
se	se	k3xPyFc4	se
mluvit	mluvit	k5eAaImF	mluvit
sprostě	sprostě	k6eAd1	sprostě
jako	jako	k8xS	jako
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
.	.	kIx.	.
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2014-09-01	[number]	k4	2014-09-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
