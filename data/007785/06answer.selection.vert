<s>
Severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgInPc1d1	střední
druhy	druh	k1gInPc1	druh
želvy	želva	k1gFnSc2	želva
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
s	s	k7c7	s
želvou	želva	k1gFnSc7	želva
hranatou	hranatý	k2eAgFnSc7d1	hranatá
(	(	kIx(	(
<g/>
Pyxidea	Pyxide	k2eAgFnSc1d1	Pyxide
mouhotii	mouhotie	k1gFnSc4	mouhotie
nebo	nebo	k8xC	nebo
Cuora	Cuora	k1gMnSc1	Cuora
mouhotii	mouhotie	k1gFnSc4	mouhotie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
tak	tak	k9	tak
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
želv	želva	k1gFnPc2	želva
Cuora	Cuor	k1gInSc2	Cuor
serrata	serrat	k2eAgFnSc1d1	serrat
<g/>
.	.	kIx.	.
</s>
