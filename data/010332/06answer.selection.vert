<s>
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
APS	APS	kA	APS
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Americká	americký	k2eAgFnSc1d1	americká
filozofická	filozofický	k2eAgFnSc1d1	filozofická
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
a	a	k8xC	a
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
vědecká	vědecký	k2eAgFnSc1d1	vědecká
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
pověstí	pověst	k1gFnSc7	pověst
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
znalosti	znalost	k1gFnPc4	znalost
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
a	a	k8xC	a
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
,	,	kIx,	,
odborných	odborný	k2eAgNnPc2d1	odborné
setkání	setkání	k1gNnPc2	setkání
<g/>
,	,	kIx,	,
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
odborné	odborný	k2eAgFnSc2d1	odborná
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
