<p>
<s>
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
APS	APS	kA	APS
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Americká	americký	k2eAgFnSc1d1	americká
filozofická	filozofický	k2eAgFnSc1d1	filozofická
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
a	a	k8xC	a
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
vědecká	vědecký	k2eAgFnSc1d1	vědecká
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
pověstí	pověst	k1gFnSc7	pověst
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
znalosti	znalost	k1gFnPc4	znalost
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
a	a	k8xC	a
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
bádání	bádání	k1gNnSc2	bádání
<g/>
,	,	kIx,	,
odborných	odborný	k2eAgNnPc2d1	odborné
setkání	setkání	k1gNnPc2	setkání
<g/>
,	,	kIx,	,
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
odborné	odborný	k2eAgFnSc2d1	odborná
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
učenou	učený	k2eAgFnSc4d1	učená
společnost	společnost	k1gFnSc4	společnost
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
kulturním	kulturní	k2eAgInSc6d1	kulturní
a	a	k8xC	a
intelektuálním	intelektuální	k2eAgInSc6d1	intelektuální
životě	život	k1gInSc6	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
270	[number]	k4	270
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
grantů	grant	k1gInPc2	grant
<g/>
,	,	kIx,	,
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
American	Americana	k1gFnPc2	Americana
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnPc1	societa
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
setkání	setkání	k1gNnPc2	setkání
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
i	i	k9	i
nadále	nadále	k6eAd1	nadále
podporuje	podporovat	k5eAaImIp3nS	podporovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
disciplín	disciplína	k1gFnPc2	disciplína
v	v	k7c6	v
humanitních	humanitní	k2eAgFnPc6d1	humanitní
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Philosophical	Philosophicat	k5eAaPmAgInS	Philosophicat
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Independence	Independence	k1gFnSc2	Independence
Hall	Halla	k1gFnPc2	Halla
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Independence	Independence	k1gFnPc4	Independence
National	National	k1gFnSc7	National
Historical	Historical	k1gFnSc2	Historical
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Philosophical	Philosophical	k1gFnSc1	Philosophical
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
založena	založen	k2eAgFnSc1d1	založena
1743	[number]	k4	1743
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Franklinem	Franklin	k1gInSc7	Franklin
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Bartramem	Bartram	k1gInSc7	Bartram
jako	jako	k8xS	jako
odnož	odnož	k1gInSc4	odnož
dřívější	dřívější	k2eAgInSc4d1	dřívější
klubu	klub	k1gInSc2	klub
Junto	junta	k1gFnSc5	junta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
Pensylvánské	pensylvánský	k2eAgFnSc6d1	Pensylvánská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
instituce	instituce	k1gFnPc4	instituce
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
nadále	nadále	k6eAd1	nadále
úzce	úzko	k6eAd1	úzko
svázané	svázaný	k2eAgFnPc4d1	svázaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
společnost	společnost	k1gFnSc1	společnost
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
mozky	mozek	k1gInPc4	mozek
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
byli	být	k5eAaImAgMnP	být
George	George	k1gFnSc4	George
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jeffersona	k1gFnPc2	Jeffersona
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
McHenry	McHenra	k1gFnSc2	McHenra
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Rittenhouse	Rittenhouse	k1gFnSc2	Rittenhouse
<g/>
,	,	kIx,	,
Nicholas	Nicholas	k1gMnSc1	Nicholas
Biddle	Biddle	k1gMnSc1	Biddle
<g/>
,	,	kIx,	,
Owen	Owen	k1gMnSc1	Owen
Biddle	Biddle	k1gMnSc1	Biddle
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Rush	Rush	k1gMnSc1	Rush
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Hillegas	Hillegas	k1gMnSc1	Hillegas
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
,	,	kIx,	,
a	a	k8xC	a
John	John	k1gMnSc1	John
Andrews	Andrewsa	k1gFnPc2	Andrewsa
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k6eAd1	také
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
filozofy	filozof	k1gMnPc4	filozof
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
jako	jako	k8xS	jako
členy	člen	k1gInPc4	člen
(	(	kIx(	(
<g/>
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
<g/>
,	,	kIx,	,
Gilbert	Gilbert	k1gMnSc1	Gilbert
du	du	k?	du
Motier	Motier	k1gMnSc1	Motier
<g/>
,	,	kIx,	,
markýz	markýz	k1gMnSc1	markýz
de	de	k?	de
La	la	k0	la
Fayette	Fayett	k1gMnSc5	Fayett
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Steuben	Steuben	k2eAgInSc4d1	Steuben
<g/>
,	,	kIx,	,
Tadeusz	Tadeusz	k1gInSc1	Tadeusz
Kościuszko	Kościuszka	k1gFnSc5	Kościuszka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
Voroncovová-Daškovová	Voroncovová-Daškovová	k1gFnSc1	Voroncovová-Daškovová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
společnost	společnost	k1gFnSc1	společnost
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
nečinnosti	nečinnost	k1gFnSc2	nečinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1767	[number]	k4	1767
nastalo	nastat	k5eAaPmAgNnS	nastat
oživení	oživení	k1gNnSc1	oživení
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1769	[number]	k4	1769
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgNnP	spojit
s	s	k7c7	s
American	American	k1gMnSc1	American
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
Promoting	Promoting	k1gInSc1	Promoting
Useful	Useful	k1gInSc4	Useful
Knowledge	Knowledg	k1gFnSc2	Knowledg
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
American	Americany	k1gInPc2	Americany
Philosophical	Philosophical	k1gMnSc2	Philosophical
Society	societa	k1gFnSc2	societa
Held	Held	k1gMnSc1	Held
at	at	k?	at
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
for	forum	k1gNnPc2	forum
Promoting	Promoting	k1gInSc1	Promoting
Useful	Useful	k1gInSc4	Useful
Knowledge	Knowledg	k1gFnSc2	Knowledg
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
revoluci	revoluce	k1gFnSc6	revoluce
vedl	vést	k5eAaImAgMnS	vést
společnost	společnost	k1gFnSc4	společnost
Francis	Francis	k1gFnSc1	Francis
Hopkinson	Hopkinson	k1gMnSc1	Hopkinson
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
pozemky	pozemek	k1gInPc4	pozemek
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pozemkem	pozemek	k1gInSc7	pozemek
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
členy	člen	k1gMnPc7	člen
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Frost	Frost	k1gMnSc1	Frost
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Cabot	Cabot	k1gInSc1	Cabot
Agassizová	Agassizová	k1gFnSc1	Agassizová
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
James	James	k1gMnSc1	James
Audubon	Audubon	k1gMnSc1	Audubon
<g/>
,	,	kIx,	,
Linus	Linus	k1gMnSc1	Linus
Pauling	Pauling	k1gInSc1	Pauling
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
Meadová	Meadový	k2eAgFnSc1d1	Meadová
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Mitchellová	Mitchellová	k1gFnSc1	Mitchellová
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Edison	Edison	k1gMnSc1	Edison
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
společnost	společnost	k1gFnSc1	společnost
založila	založit	k5eAaPmAgFnS	založit
Magellanic	Magellanice	k1gFnPc2	Magellanice
Premium	Premium	k1gNnSc1	Premium
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
přínos	přínos	k1gInSc1	přínos
v	v	k7c6	v
"	"	kIx"	"
<g/>
navigaci	navigace	k1gFnSc6	navigace
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc3	astronomie
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgFnSc3d1	přírodní
filozofii	filozofie	k1gFnSc3	filozofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
cenu	cena	k1gFnSc4	cena
udělovanou	udělovaný	k2eAgFnSc7d1	udělovaná
americkou	americký	k2eAgFnSc7d1	americká
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
udělována	udělován	k2eAgFnSc1d1	udělována
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
cenami	cena	k1gFnPc7	cena
jsou	být	k5eAaImIp3nP	být
Barzun	Barzun	k1gInSc4	Barzun
Prize	Prize	k1gFnSc2	Prize
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
Judson	Judson	k1gInSc4	Judson
Daland	Dalanda	k1gFnPc2	Dalanda
Prize	Prize	k1gFnSc2	Prize
za	za	k7c4	za
klinickou	klinický	k2eAgFnSc4d1	klinická
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k2eAgInSc4d1	Franklin
Medal	Medal	k1gInSc4	Medal
<g/>
,	,	kIx,	,
Lashley	Lashley	k1gInPc4	Lashley
Award	Awarda	k1gFnPc2	Awarda
za	za	k7c4	za
neurobiologii	neurobiologie	k1gFnSc4	neurobiologie
<g/>
,	,	kIx,	,
Lewis	Lewis	k1gFnSc1	Lewis
Award	Award	k1gMnSc1	Award
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
Medal	Medal	k1gMnSc1	Medal
za	za	k7c4	za
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
nebo	nebo	k8xC	nebo
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
APS	APS	kA	APS
publikuje	publikovat	k5eAaBmIp3nS	publikovat
Transactions	Transactions	k1gInSc1	Transactions
of	of	k?	of
the	the	k?	the
American	American	k1gInSc4	American
Philosophical	Philosophical	k1gMnSc4	Philosophical
Society	societa	k1gFnSc2	societa
již	již	k9	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1771	[number]	k4	1771
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
vyjde	vyjít	k5eAaPmIp3nS	vyjít
5	[number]	k4	5
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
je	být	k5eAaImIp3nS	být
vydáváno	vydávat	k5eAaPmNgNnS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k6eAd1	také
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
různé	různý	k2eAgInPc4d1	různý
sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k2eAgMnSc1d1	Franklin
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Penn	Penna	k1gFnPc2	Penna
<g/>
,	,	kIx,	,
Meriwether	Meriwethra	k1gFnPc2	Meriwethra
Lewis	Lewis	k1gFnPc2	Lewis
a	a	k8xC	a
William	William	k1gInSc1	William
Clark	Clark	k1gInSc1	Clark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budovy	budova	k1gFnPc4	budova
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Philosophical	Philosophical	k1gMnSc1	Philosophical
Hall	Hall	k1gMnSc1	Hall
===	===	k?	===
</s>
</p>
<p>
<s>
Philosophical	Philosophicat	k5eAaPmAgMnS	Philosophicat
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c4	na
104	[number]	k4	104
South	Southa	k1gFnPc2	Southa
Fifth	Fifth	k1gMnSc1	Fifth
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
a	a	k8xC	a
Walnut	Walnut	k2eAgInSc4d1	Walnut
Streets	Streets	k1gInSc4	Streets
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1785-89	[number]	k4	1785-89
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc4	sídlo
společnosti	společnost	k1gFnSc2	společnost
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Samuela	Samuel	k1gMnSc2	Samuel
Vaughana	Vaughan	k1gMnSc2	Vaughan
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
patro	patro	k1gNnSc1	patro
bylo	být	k5eAaImAgNnS	být
přistavěno	přistavět	k5eAaPmNgNnS	přistavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
pro	pro	k7c4	pro
rozšiřující	rozšiřující	k2eAgFnSc4d1	rozšiřující
se	se	k3xPyFc4	se
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948-50	[number]	k4	1948-50
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
před	před	k7c7	před
zařazením	zařazení	k1gNnSc7	zařazení
do	do	k7c2	do
Independence	Independence	k1gFnSc2	Independence
National	National	k1gFnSc2	National
Historical	Historical	k1gFnSc2	Historical
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
veřejnosti	veřejnost	k1gFnSc3	veřejnost
jako	jako	k8xS	jako
The	The	k1gFnSc3	The
American	Americany	k1gInPc2	Americany
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
Museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Hostí	hostit	k5eAaImIp3nP	hostit
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
křižovatkami	křižovatka	k1gFnPc7	křižovatka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
představuje	představovat	k5eAaImIp3nS	představovat
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgInPc4d1	vědecký
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
originální	originální	k2eAgInPc4d1	originální
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
,	,	kIx,	,
vzácné	vzácný	k2eAgFnPc4d1	vzácná
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInPc4d1	přírodní
vzorky	vzorek	k1gInPc4	vzorek
a	a	k8xC	a
kuriozity	kuriozita	k1gFnPc4	kuriozita
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
sbírek	sbírka	k1gFnPc2	sbírka
APS	APS	kA	APS
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předměty	předmět	k1gInPc4	předmět
zapůjčené	zapůjčený	k2eAgNnSc1d1	zapůjčené
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Library	Librar	k1gInPc4	Librar
Hall	Hall	k1gInSc1	Hall
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1789-90	[number]	k4	1789-90
Library	Librara	k1gFnPc4	Librara
Company	Compana	k1gFnSc2	Compana
of	of	k?	of
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
(	(	kIx(	(
<g/>
LCP	LCP	kA	LCP
<g/>
)	)	kIx)	)
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
přímo	přímo	k6eAd1	přímo
naproti	naproti	k6eAd1	naproti
APS	APS	kA	APS
na	na	k7c4	na
5	[number]	k4	5
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
LCP	LCP	kA	LCP
prodala	prodat	k5eAaPmAgFnS	prodat
svou	svůj	k3xOyFgFnSc4	svůj
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
zbourána	zbourat	k5eAaPmNgFnS	zbourat
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
Drexel	Drexel	k1gMnSc1	Drexel
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
Building	Building	k1gInSc1	Building
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
zbořena	zbořit	k5eAaPmNgFnS	zbořit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
Independence	Independence	k1gFnSc2	Independence
National	National	k1gFnSc2	National
Historical	Historical	k1gFnSc2	Historical
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
APS	APS	kA	APS
postavila	postavit	k5eAaPmAgFnS	postavit
knihovnu	knihovna	k1gFnSc4	knihovna
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
průčelím	průčelí	k1gNnSc7	průčelí
staré	starý	k2eAgFnSc2d1	stará
budovy	budova	k1gFnSc2	budova
LCP	LCP	kA	LCP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
Hall	Hall	k1gMnSc1	Hall
===	===	k?	===
</s>
</p>
<p>
<s>
APS	APS	kA	APS
rekonstruovala	rekonstruovat	k5eAaBmAgFnS	rekonstruovat
původní	původní	k2eAgInSc4d1	původní
Farmers	Farmers	k1gInSc4	Farmers
<g/>
'	'	kIx"	'
&	&	k?	&
Mechanics	Mechanics	k1gInSc1	Mechanics
<g/>
'	'	kIx"	'
Bank	bank	k1gInSc1	bank
building	building	k1gInSc1	building
na	na	k7c4	na
425-29	[number]	k4	425-29
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
Street	Street	k1gInSc4	Street
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
období	období	k1gNnSc6	období
1854-5	[number]	k4	1854-5
a	a	k8xC	a
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
Johnem	John	k1gMnSc7	John
M.	M.	kA	M.
Griesem	Gries	k1gMnSc7	Gries
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Hostí	hostit	k5eAaImIp3nS	hostit
většinu	většina	k1gFnSc4	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
událostí	událost	k1gFnPc2	událost
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Richardson	Richardson	k1gMnSc1	Richardson
Hall	Hall	k1gMnSc1	Hall
===	===	k?	===
</s>
</p>
<p>
<s>
Constance	Constance	k1gFnSc1	Constance
C.	C.	kA	C.
and	and	k?	and
Edgar	Edgar	k1gMnSc1	Edgar
P.	P.	kA	P.
Richardson	Richardson	k1gMnSc1	Richardson
Hall	Hall	k1gMnSc1	Hall
na	na	k7c4	na
431	[number]	k4	431
Chestnut	Chestnut	k2eAgInSc4d1	Chestnut
Street	Street	k1gInSc4	Street
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k2eAgInSc4d1	Franklin
Hall	Hall	k1gInSc4	Hall
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgNnPc4d1	původní
Pennsylvania	Pennsylvanium	k1gNnPc4	Pennsylvanium
Company	Compana	k1gFnSc2	Compana
for	forum	k1gNnPc2	forum
Insurances	Insurances	k1gInSc4	Insurances
on	on	k3xPp3gMnSc1	on
Lives	Lives	k1gMnSc1	Lives
and	and	k?	and
Granting	Granting	k1gInSc1	Granting
Annuities	Annuities	k1gMnSc1	Annuities
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
,	,	kIx,	,
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
Addison	Addison	k1gNnSc4	Addison
Hutton	Hutton	k1gInSc1	Hutton
<g/>
.	.	kIx.	.
<g/>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nP	sídlet
kanceláře	kancelář	k1gFnPc1	kancelář
a	a	k8xC	a
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Area	Ares	k1gMnSc2	Ares
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
History	Histor	k1gMnPc4	Histor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
American	Americana	k1gFnPc2	Americana
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
</s>
</p>
<p>
<s>
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
American	American	k1gInSc1	American
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
publications	publications	k6eAd1	publications
</s>
</p>
