<s>
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
:	:	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
Coast	Coast	k1gMnSc1
Guard	Guard	k1gMnSc1
<g/>
,	,	kIx,
USCG	USCG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
šesti	šest	k4xCc2
složek	složka	k1gFnPc2
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>