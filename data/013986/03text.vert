<s>
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Předchůdce	předchůdce	k1gMnPc4
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
Life-Saving	Life-Saving	k1gInSc4
Service	Service	k1gFnSc2
a	a	k8xC
United	United	k1gMnSc1
States	States	k1gMnSc1
Revenue	Revenue	k1gFnPc2
Cutter	Cutter	k1gMnSc1
Service	Service	k1gFnSc2
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Hamilton	Hamilton	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1790	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Southwest	Southwest	k1gMnSc1
<g/>
,	,	kIx,
205	#num#	k4
93	#num#	k4
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc1d1
Mateřská	mateřský	k2eAgNnPc4d1
organizace	organizace	k1gFnSc2
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
vnitřní	vnitřní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.uscg.mil	www.uscg.mit	k5eAaPmAgMnS
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
:	:	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
Coast	Coast	k1gMnSc1
Guard	Guard	k1gMnSc1
<g/>
,	,	kIx,
USCG	USCG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
šesti	šest	k4xCc2
složek	složka	k1gFnPc2
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
dohlížet	dohlížet	k5eAaImF
na	na	k7c4
dodržování	dodržování	k1gNnSc4
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
ve	v	k7c6
vodách	voda	k1gFnPc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
míru	mír	k1gInSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
USCG	USCG	kA
pod	pod	k7c4
Ministerstvo	ministerstvo	k1gNnSc4
vnitřní	vnitřní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
Námořnictvo	námořnictvo	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
USCG	USCG	kA
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
nejmenší	malý	k2eAgFnSc4d3
ze	z	k7c2
šesti	šest	k4xCc2
složek	složka	k1gFnPc2
Ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
další	další	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
jsou	být	k5eAaImIp3nP
ochrana	ochrana	k1gFnSc1
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ekonomických	ekonomický	k2eAgInPc2d1
a	a	k8xC
bezpečnostních	bezpečnostní	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
kdekoliv	kdekoliv	k6eAd1
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ohroženy	ohrozit	k5eAaPmNgFnP
<g/>
,	,	kIx,
včetně	včetně	k7c2
mezinárodních	mezinárodní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
amerických	americký	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
a	a	k8xC
domácích	domácí	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1915	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
spojením	spojení	k1gNnSc7
United	United	k1gMnSc1
States	States	k1gMnSc1
Revenue	Revenue	k1gFnPc2
Cutter	Cutter	k1gMnSc1
Service	Service	k1gFnSc2
a	a	k8xC
United	United	k1gMnSc1
States	States	k1gMnSc1
Life-Saving	Life-Saving	k1gInSc4
Service	Service	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
ní	on	k3xPp3gFnSc6
slouží	sloužit	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
42	#num#	k4
000	#num#	k4
námořníků	námořník	k1gMnPc2
<g/>
,	,	kIx,
1600	#num#	k4
lodí	loď	k1gFnPc2
a	a	k8xC
192	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kutry	kutr	k1gInPc1
</s>
<s>
Kutr	kutr	k1gInSc1
třídy	třída	k1gFnSc2
Bear	Beara	k1gFnPc2
</s>
<s>
Hlídkové	hlídkový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
Offshore	Offshor	k1gMnSc5
Patrol	Patrol	k?
Cutter	Cutter	k1gMnSc1
(	(	kIx(
<g/>
OPC	OPC	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Heritage	Heritage	k1gFnSc1
(	(	kIx(
<g/>
ve	v	k7c6
stavbě	stavba	k1gFnSc6
<g/>
,	,	kIx,
25	#num#	k4
plánováno	plánovat	k5eAaImNgNnS
<g/>
)	)	kIx)
</s>
<s>
National	Nationat	k5eAaPmAgMnS,k5eAaImAgMnS
Security	Securita	k1gFnPc4
Cutter	Cuttrum	k1gNnPc2
(	(	kIx(
<g/>
NSC	NSC	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Legend	legenda	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
ks	ks	kA
<g/>
,	,	kIx,
další	další	k2eAgMnSc1d1
4	#num#	k4
ks	ks	kA
ve	v	k7c6
stavbě	stavba	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
High	High	k1gMnSc1
endurance	endurance	k1gFnSc2
cutter	cutter	k1gMnSc1
(	(	kIx(
<g/>
WHEC	WHEC	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Hamilton	Hamilton	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Medium	medium	k1gNnSc1
Endurance	Endurance	k1gFnSc2
Cutter	Cuttra	k1gFnPc2
(	(	kIx(
<g/>
WMEC	WMEC	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Bear	Bear	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třída	třída	k1gFnSc1
Reliance	Reliance	k1gFnSc1
(	(	kIx(
<g/>
14	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
a	a	k8xC
USCGC	USCGC	kA
Alex	Alex	k1gMnSc1
Haley	Halea	k1gMnSc2
(	(	kIx(
<g/>
WMEC-	WMEC-	k1gFnSc1
<g/>
39	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fast	Fast	k1gMnSc1
Response	response	k1gFnSc2
Cutter	Cutter	k1gMnSc1
(	(	kIx(
<g/>
WPC	WPC	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Sentinel	sentinel	k1gInSc1
(	(	kIx(
<g/>
33	#num#	k4
ks	ks	kA
<g/>
,	,	kIx,
dalších	další	k2eAgInPc2d1
25	#num#	k4
ks	ks	kA
objednáno	objednán	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Patrol	Patrol	k?
Boats	Boats	k1gInSc1
(	(	kIx(
<g/>
WPB	WPB	kA
<g/>
)	)	kIx)
–	–	k?
třída	třída	k1gFnSc1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
vyřazovány	vyřazován	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třída	třída	k1gFnSc1
Marine	Marin	k1gInSc5
Protector	Protector	k1gMnSc1
(	(	kIx(
<g/>
77	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Ledoborce	ledoborec	k1gInPc1
</s>
<s>
USCGC	USCGC	kA
Healy	Heal	k1gInPc1
(	(	kIx(
<g/>
WAGB-	WAGB-	k1gFnSc1
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Polar	Polara	k1gFnPc2
</s>
<s>
USCGC	USCGC	kA
Polar	Polar	k1gMnSc1
Star	Star	kA
(	(	kIx(
<g/>
WAGB-	WAGB-	k1gFnSc1
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
USCGC	USCGC	kA
Healy	Heal	k1gInPc1
(	(	kIx(
<g/>
WAGB-	WAGB-	k1gFnSc1
<g/>
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
USCGC	USCGC	kA
Mackinaw	Mackinaw	k1gFnSc1
(	(	kIx(
<g/>
WLBB-	WLBB-	k1gFnSc1
<g/>
30	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
Bay	Bay	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
–	–	k?
remorkéry	remorkér	k1gInPc1
</s>
<s>
Nosiče	nosič	k1gInPc1
bójí	bóje	k1gFnPc2
</s>
<s>
USCGC	USCGC	kA
Fir	Fir	k1gFnSc1
(	(	kIx(
<g/>
WLB-	WLB-	k1gFnSc1
<g/>
213	#num#	k4
<g/>
)	)	kIx)
třídy	třída	k1gFnSc2
Juniper	Juniper	k1gMnSc1
</s>
<s>
Seagoing	Seagoing	k1gInSc4
Buoy	Buoa	k1gFnSc2
Tender	tender	k1gInSc1
–	–	k?
třída	třída	k1gFnSc1
Juniper	Juniper	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Coastal	Coastat	k5eAaPmAgInS,k5eAaImAgInS
Buoy	Buoa	k1gFnSc2
Tender	tender	k1gInSc1
–	–	k?
třída	třída	k1gFnSc1
Keeper	Keeper	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
ks	ks	kA
<g/>
)	)	kIx)
</s>
<s>
Cvičné	cvičný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
USCGC	USCGC	kA
Eagle	Eagle	k1gFnSc1
(	(	kIx(
<g/>
WIX-	WIX-	k1gFnSc1
<g/>
327	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Plánované	plánovaný	k2eAgNnSc1d1
</s>
<s>
Polar	Polar	k1gMnSc1
Security	Securita	k1gFnSc2
Cutter	Cutter	k1gMnSc1
(	(	kIx(
<g/>
PSC	PSC	kA
<g/>
)	)	kIx)
–	–	k?
program	program	k1gInSc1
stavby	stavba	k1gFnSc2
těžkých	těžký	k2eAgInPc2d1
ledoborců	ledoborec	k1gInPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Coast	Coast	k1gMnSc1
Guard	Guard	k1gMnSc1
Investigative	Investigativ	k1gInSc5
Service	Service	k1gFnSc1
(	(	kIx(
<g/>
CGIS	CGIS	kA
<g/>
)	)	kIx)
-	-	kIx~
menší	malý	k2eAgFnSc1d2
obdoba	obdoba	k1gFnSc1
námořní	námořní	k2eAgFnSc2d1
NCIS	NCIS	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pobřežní	pobřežní	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Armáda	armáda	k1gFnSc1
•	•	k?
Námořnictvo	námořnictvo	k1gNnSc1
•	•	k?
Letectvo	letectvo	k1gNnSc1
•	•	k?
Námořní	námořní	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
•	•	k?
Vesmírné	vesmírný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
•	•	k?
Pobřežní	pobřežní	k1gMnSc1
stráž	stráž	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
13011-4	13011-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0405	#num#	k4
8787	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80125851	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
154411282	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80125851	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
