<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1085	#num#	k4
král	král	k1gMnSc1
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
freska	freska	k1gFnSc1
ze	z	k7c2
znojemské	znojemský	k2eAgFnSc2d1
rotundy	rotunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1085	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1032	#num#	k4
<g/>
/	/	kIx~
<g/>
1035	#num#	k4
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1092	#num#	k4
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Vyšehrad	Vyšehrad	k1gInSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
I.	I.	kA
manželka	manželka	k1gFnSc1
neznámá	neznámá	k1gFnSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adléta	Adléta	k1gFnSc1
Uherská	uherský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1057	#num#	k4
<g/>
–	–	k?
<g/>
1062	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatava	Svatava	k1gFnSc1
Polská	polský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1062	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
Judita	Judita	k1gFnSc1
PřemyslovnaVratislav	PřemyslovnaVratislav	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
mladý	mladý	k1gMnSc1
<g/>
)	)	kIx)
<g/>
LudmilaBoleslavBořivoj	LudmilaBoleslavBořivoj	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
Judita	Judita	k1gFnSc1
GrojčskáVladislav	GrojčskáVladislav	k1gMnSc1
I.	I.	kA
<g/>
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Jitka	Jitka	k1gFnSc1
ze	z	k7c2
Schweinfurtu	Schweinfurt	k1gInSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
okolo	okolo	k7c2
1033	#num#	k4
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1092	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
od	od	k7c2
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1061	#num#	k4
do	do	k7c2
dubna	duben	k1gInSc2
1085	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
z	z	k7c2
rodu	rod	k1gInSc2
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
titulárním	titulární	k2eAgMnSc7d1
polským	polský	k2eAgMnSc7d1
králem	král	k1gMnSc7
v	v	k7c6
letech	let	k1gInPc6
1085	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
kníže	kníže	k1gMnSc1
byl	být	k5eAaImAgMnS
Vratislavem	Vratislav	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
král	král	k1gMnSc1
Vratislavem	Vratislav	k1gMnSc7
I.	I.	kA
(	(	kIx(
<g/>
Wratislaus	Wratislaus	k1gMnSc1
Primus	primus	k1gMnSc1
Rex	Rex	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
byl	být	k5eAaImAgMnS
druhorozený	druhorozený	k2eAgMnSc1d1
syn	syn	k1gMnSc1
knížete	kníže	k1gNnSc2wR
Břetislava	Břetislav	k1gMnSc2
I.	I.	kA
a	a	k8xC
Jitky	Jitka	k1gFnSc2
ze	z	k7c2
Svinibrodu	Svinibrod	k1gInSc2
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
Spytihněva	Spytihněvo	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Konráda	Konrád	k1gMnSc4
I.	I.	kA
Brněnského	brněnský	k2eAgMnSc2d1
<g/>
,	,	kIx,
Oty	Ota	k1gMnSc2
Olomouckého	olomoucký	k2eAgMnSc2d1
a	a	k8xC
biskupa	biskup	k1gMnSc2
Jaromíra	Jaromír	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vůle	vůle	k1gFnSc2
otce	otec	k1gMnSc2
zdědil	zdědit	k5eAaPmAgInS
olomoucký	olomoucký	k2eAgInSc1d1
úděl	úděl	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
vládl	vládnout	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1055	#num#	k4
<g/>
–	–	k?
<g/>
1056	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
1058	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vratislavovu	Vratislavův	k2eAgFnSc4d1
první	první	k4xOgFnSc4
manželku	manželka	k1gFnSc4
neznáme	znát	k5eNaImIp1nP,k5eAaImIp1nP
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
spekuluje	spekulovat	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
o	o	k7c6
jejím	její	k3xOp3gInSc6
německém	německý	k2eAgInSc6d1
původu	původ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k1gMnSc1
je	být	k5eAaImIp3nS
ale	ale	k9
její	její	k3xOp3gInSc1
příběh	příběh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Vratislav	Vratislav	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
sporu	spor	k1gInSc2
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Spytihněvem	Spytihněv	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
opustit	opustit	k5eAaPmF
Olomouc	Olomouc	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlil	sídlit	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočiště	útočiště	k1gNnSc1
hledal	hledat	k5eAaImAgInS
u	u	k7c2
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ondřeje	Ondřej	k1gMnSc2
I.	I.	kA
Svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
šlechtičnu	šlechtična	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
už	už	k6eAd1
pokročilém	pokročilý	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
těhotenství	těhotenství	k1gNnSc2
<g/>
,	,	kIx,
raději	rád	k6eAd2
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spytihněv	Spytihněv	k1gFnSc1
tuto	tento	k3xDgFnSc4
ženu	žena	k1gFnSc4
ale	ale	k8xC
uvěznil	uvěznit	k5eAaPmAgMnS
a	a	k8xC
ta	ten	k3xDgFnSc1
nakonec	nakonec	k6eAd1
zemřela	zemřít	k5eAaPmAgFnS
při	při	k7c6
předčasném	předčasný	k2eAgInSc6d1
porodu	porod	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
po	po	k7c6
propuštění	propuštění	k1gNnSc6
odjížděla	odjíždět	k5eAaImAgFnS
za	za	k7c7
manželem	manžel	k1gMnSc7
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podruhé	podruhé	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
Vratislav	Vratislav	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Ondřejovou	Ondřejův	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
Adlétou	Adlétý	k2eAgFnSc7d1
<g/>
,	,	kIx,
uherskou	uherský	k2eAgFnSc7d1
princeznou	princezna	k1gFnSc7
z	z	k7c2
rodu	rod	k1gInSc2
Arpádovců	Arpádovec	k1gMnPc2
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1057	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislava	k1gFnPc2
tak	tak	k6eAd1
získal	získat	k5eAaPmAgMnS
silného	silný	k2eAgMnSc4d1
spojence	spojenec	k1gMnSc4
a	a	k8xC
Spytihněv	Spytihněv	k1gMnSc4
II	II	kA
<g/>
.	.	kIx.
mu	on	k3xPp3gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1058	#num#	k4
vrátil	vrátit	k5eAaPmAgInS
olomoucký	olomoucký	k2eAgInSc1d1
úděl	úděl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
Vratislav	Vratislav	k1gMnSc1
stal	stát	k5eAaPmAgMnS
po	po	k7c6
smrti	smrt	k1gFnSc6
staršího	starý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Spytihněva	Spytihněv	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1061	#num#	k4
knížetem	kníže	k1gMnSc7
<g/>
,	,	kIx,
Adléta	Adléta	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
titulu	titul	k1gInSc2
kněžny	kněžna	k1gFnSc2
dlouho	dlouho	k6eAd1
neradovala	radovat	k5eNaImAgFnS
a	a	k8xC
zemřela	zemřít	k5eAaPmAgFnS
už	už	k6eAd1
počátkem	počátek	k1gInSc7
příštího	příští	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
rok	rok	k1gInSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
Adléty	Adléta	k1gMnSc2
se	se	k3xPyFc4
Vratislav	Vratislav	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
potřetí	potřetí	k4xO
<g/>
,	,	kIx,
se	s	k7c7
Svatavou	Svatava	k1gFnSc7
Polskou	Polska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potvrdil	potvrdit	k5eAaPmAgMnS
tak	tak	k9
přátelství	přátelství	k1gNnSc4
s	s	k7c7
polským	polský	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
Boleslavem	Boleslav	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smělým	smělý	k2eAgNnSc7d1
(	(	kIx(
<g/>
se	s	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
ale	ale	k9
později	pozdě	k6eAd2
bojoval	bojovat	k5eAaImAgInS
o	o	k7c4
česko-polské	česko-polský	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
byl	být	k5eAaImAgMnS
především	především	k6eAd1
významným	významný	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
císaře	císař	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
při	při	k7c6
konfliktech	konflikt	k1gInPc6
s	s	k7c7
Poláky	Polák	k1gMnPc7
<g/>
,	,	kIx,
Míšní	Míšeň	k1gFnSc7
a	a	k8xC
Jindřichovým	Jindřichův	k2eAgMnSc7d1
protikrálem	protikrál	k1gMnSc7
Rudolfem	Rudolf	k1gMnSc7
Švábským	švábský	k2eAgMnSc7d1
<g/>
,	,	kIx,
podnikal	podnikat	k5eAaImAgInS
také	také	k9
vpády	vpád	k1gInPc4
do	do	k7c2
Rakous	Rakousy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
polský	polský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
obsadil	obsadit	k5eAaPmAgInS
Kyjev	Kyjev	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
povzbuzen	povzbudit	k5eAaPmNgInS
tímto	tento	k3xDgInSc7
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
zaútočil	zaútočit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1070	#num#	k4
na	na	k7c4
Čechy	Čechy	k1gFnPc4
(	(	kIx(
<g/>
pokoušel	pokoušet	k5eAaImAgMnS
se	se	k3xPyFc4
změnit	změnit	k5eAaPmF
hranice	hranice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gMnSc1
byl	být	k5eAaImAgMnS
ovšem	ovšem	k9
úspěšnější	úspěšný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
obě	dva	k4xCgNnPc4
knížata	kníže	k1gNnPc4
nabádal	nabádat	k5eAaImAgInS,k5eAaBmAgInS
k	k	k7c3
urovnání	urovnání	k1gNnSc3
sporu	spor	k1gInSc2
a	a	k8xC
pohrozil	pohrozit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zaútočí	zaútočit	k5eAaPmIp3nS
na	na	k7c4
toho	ten	k3xDgMnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
příště	příště	k6eAd1
poruší	porušit	k5eAaPmIp3nS
mír	mír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přísahu	přísaha	k1gFnSc4
porušil	porušit	k5eAaPmAgMnS
Boleslav	Boleslav	k1gMnSc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1073	#num#	k4
Jindřich	Jindřich	k1gMnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
Polsku	Polsko	k1gNnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ale	ale	k9
kvůli	kvůli	k7c3
vnitřním	vnitřní	k2eAgInPc3d1
problémům	problém	k1gInPc3
neproběhla	proběhnout	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
stát	stát	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
sílit	sílit	k5eAaImF
a	a	k8xC
Jindřich	Jindřich	k1gMnSc1
Vratislava	Vratislav	k1gMnSc2
potřeboval	potřebovat	k5eAaImAgInS
k	k	k7c3
obraně	obrana	k1gFnSc3
proti	proti	k7c3
jeho	jeho	k3xOp3gMnPc3
vládcům	vládce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
roku	rok	k1gInSc2
1073	#num#	k4
nový	nový	k2eAgMnSc1d1
papež	papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
potvrdil	potvrdit	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
listem	list	k1gInSc7
používání	používání	k1gNnSc2
mitry	mitra	k1gFnSc2
českému	český	k2eAgMnSc3d1
knížeti	kníže	k1gMnSc3
za	za	k7c4
poplatek	poplatek	k1gInSc4
100	#num#	k4
hřiven	hřivna	k1gFnPc2
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zavedeno	zavést	k5eAaPmNgNnS
za	za	k7c4
Vratislavova	Vratislavův	k2eAgMnSc4d1
předchůdce	předchůdce	k1gMnSc4
(	(	kIx(
<g/>
a	a	k8xC
staršího	starý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
<g/>
)	)	kIx)
Spytihněva	Spytihněv	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německý	německý	k2eAgMnSc1d1
král	král	k1gMnSc1
i	i	k8xC
papež	papež	k1gMnSc1
očividně	očividně	k6eAd1
chtěli	chtít	k5eAaImAgMnP
Vratislava	Vratislav	k1gMnSc4
získat	získat	k5eAaPmF
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
do	do	k7c2
pozdějšího	pozdní	k2eAgInSc2d2
boje	boj	k1gInSc2
o	o	k7c4
investituru	investitura	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
zda	zda	k8xS
má	mít	k5eAaImIp3nS
světský	světský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
právo	právo	k1gNnSc4
volit	volit	k5eAaImF
a	a	k8xC
uvádět	uvádět	k5eAaImF
do	do	k7c2
funkce	funkce	k1gFnSc2
církevní	církevní	k2eAgMnPc4d1
hodnostáře	hodnostář	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polsko	Polsko	k1gNnSc1
a	a	k8xC
Uhry	Uhry	k1gFnPc1
stály	stát	k5eAaImAgFnP
při	při	k7c6
papežovi	papež	k1gMnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Vratislav	Vratislav	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
věrný	věrný	k2eAgMnSc1d1
Jindřichovi	Jindřichův	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s>
Spojenectvím	spojenectví	k1gNnSc7
s	s	k7c7
Jindřichem	Jindřich	k1gMnSc7
Vratislav	Vratislava	k1gFnPc2
přinesl	přinést	k5eAaPmAgInS
svému	svůj	k3xOyFgNnSc3
panství	panství	k1gNnSc3
územní	územní	k2eAgInPc1d1
zisky	zisk	k1gInPc1
–	–	k?
od	od	k7c2
Jindřicha	Jindřich	k1gMnSc2
získal	získat	k5eAaPmAgMnS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jen	jen	k9
dočasně	dočasně	k6eAd1
<g/>
,	,	kIx,
tituly	titul	k1gInPc1
markraběte	markrabě	k1gMnSc2
saského	saský	k2eAgMnSc2d1
východní	východní	k2eAgFnSc2d1
marky	marka	k1gFnSc2
(	(	kIx(
<g/>
Lužice	Lužice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Míšeňska	Míšeňska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1081	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
změnila	změnit	k5eAaPmAgFnS
dohoda	dohoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míšeňsko	Míšeňsko	k1gNnSc1
a	a	k8xC
Lužici	Lužice	k1gFnSc6
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
Jindřich	Jindřich	k1gMnSc1
původním	původní	k2eAgMnPc3d1
rodům	rod	k1gInPc3
<g/>
,	,	kIx,
náhradou	náhrada	k1gFnSc7
mu	on	k3xPp3gMnSc3
udělil	udělit	k5eAaPmAgMnS
titul	titul	k1gInSc4
markraběte	markrabě	k1gMnSc2
bavorské	bavorský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
marky	marka	k1gFnSc2
-	-	kIx~
Rakousko	Rakousko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
ale	ale	k9
především	především	k9
o	o	k7c4
politický	politický	k2eAgInSc4d1
tah	tah	k1gInSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
vládli	vládnout	k5eAaImAgMnP
Babenberkové	Babenberek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Královská	královský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
jako	jako	k8xS,k8xC
král	král	k1gMnSc1
(	(	kIx(
<g/>
Vyšehradský	vyšehradský	k2eAgInSc1d1
kodex	kodex	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1085	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
dvorský	dvorský	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
v	v	k7c6
Mohuči	Mohuč	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
významně	významně	k6eAd1
zapsal	zapsat	k5eAaPmAgInS
do	do	k7c2
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
obdržel	obdržet	k5eAaPmAgInS
od	od	k7c2
císaře	císař	k1gMnSc4
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
za	za	k7c4
své	svůj	k3xOyFgFnPc4
věrné	věrný	k2eAgFnPc4d1
služby	služba	k1gFnPc4
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
(	(	kIx(
<g/>
ovšem	ovšem	k9
jen	jen	k9
nedědičnou	dědičný	k2eNgFnSc7d1
-	-	kIx~
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
osobu	osoba	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zbaven	zbavit	k5eAaPmNgMnS
povinných	povinný	k2eAgInPc2d1
poplatků	poplatek	k1gInPc2
a	a	k8xC
povinován	povinován	k2eAgInSc1d1
účastí	účast	k1gFnSc7
českých	český	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
s	s	k7c7
družinou	družina	k1gFnSc7
na	na	k7c6
korunovačních	korunovační	k2eAgFnPc6d1
cestách	cesta	k1gFnPc6
německých	německý	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
uvolňovaly	uvolňovat	k5eAaImAgFnP
vazby	vazba	k1gFnPc4
českých	český	k2eAgMnPc2d1
a	a	k8xC
německých	německý	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1085	#num#	k4
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
trevírský	trevírský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
Egilbert	Egilbert	k1gMnSc1
Vratislava	Vratislav	k1gMnSc4
korunoval	korunovat	k5eAaBmAgMnS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
při	při	k7c6
této	tento	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgMnS
u	u	k7c2
Vratislava	Vratislav	k1gMnSc2
i	i	k8xC
titul	titul	k1gInSc1
krále	král	k1gMnSc2
polského	polský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Mezitím	mezitím	k6eAd1
Egilbert	Egilbert	k1gMnSc1
<g/>
,	,	kIx,
arcibiskup	arcibiskup	k1gMnSc1
trevírský	trevírský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jsa	být	k5eAaImSgMnS
poslušen	poslušen	k2eAgInSc1d1
císařova	císařův	k2eAgInSc2d1
rozkazu	rozkaz	k1gInSc2
<g/>
,	,	kIx,
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
Prahy	Praha	k1gFnSc2
a	a	k8xC
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
při	při	k7c6
slavné	slavný	k2eAgFnSc6d1
mši	mše	k1gFnSc6
svaté	svatá	k1gFnSc2
pomazal	pomazat	k5eAaPmAgMnS
Vratislava	Vratislav	k1gMnSc4
<g/>
,	,	kIx,
oděného	oděný	k2eAgMnSc4d1
královskými	královský	k2eAgInPc7d1
odznaky	odznak	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c4
krále	král	k1gMnPc4
a	a	k8xC
vložil	vložit	k5eAaPmAgMnS
korunu	koruna	k1gFnSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
jeho	jeho	k3xOp3gMnSc3
i	i	k9
na	na	k7c4
hlavu	hlava	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Svatavy	Svatava	k1gFnSc2
<g/>
,	,	kIx,
oblečené	oblečený	k2eAgNnSc1d1
v	v	k7c4
královské	královský	k2eAgNnSc4d1
roucho	roucho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
příležitosti	příležitost	k1gFnSc2
korunovace	korunovace	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
Kodex	kodex	k1gInSc1
vyšehradský	vyšehradský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vnitropolitický	vnitropolitický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Vratislavův	Vratislavův	k2eAgInSc1d1
denár	denár	k1gInSc1
-	-	kIx~
líc	líc	k1gInSc1
</s>
<s>
Vratislavův	Vratislavův	k2eAgInSc1d1
denár	denár	k1gInSc1
-	-	kIx~
rub	rub	k1gInSc1
</s>
<s>
Po	po	k7c6
nástupu	nástup	k1gInSc6
na	na	k7c4
knížecí	knížecí	k2eAgInSc4d1
stolec	stolec	k1gInSc4
obnovil	obnovit	k5eAaPmAgMnS
moravské	moravský	k2eAgInPc4d1
úděly	úděl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
vrátil	vrátit	k5eAaPmAgInS
bratrům	bratr	k1gMnPc3
Konrádovi	Konrád	k1gMnSc3
a	a	k8xC
Otovi	Ota	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmladší	mladý	k2eAgMnSc1d3
Jaromír	Jaromír	k1gMnSc1
byl	být	k5eAaImAgMnS
předurčen	předurčit	k5eAaPmNgMnS
pro	pro	k7c4
duchovní	duchovní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
pražským	pražský	k2eAgInSc7d1
biskupem	biskup	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
velmi	velmi	k6eAd1
prestižní	prestižní	k2eAgInSc4d1
a	a	k8xC
vlivný	vlivný	k2eAgInSc4d1
post	post	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
knížetem	kníže	k1gMnSc7
a	a	k8xC
Jaromírem	Jaromír	k1gMnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
nepřátelství	nepřátelství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	k9
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
omezil	omezit	k5eAaPmAgInS
vliv	vliv	k1gInSc4
pražského	pražský	k2eAgNnSc2d1
biskupství	biskupství	k1gNnSc2
<g/>
,	,	kIx,
rozdělil	rozdělit	k5eAaPmAgInS
roku	rok	k1gInSc2
1063	#num#	k4
českou	český	k2eAgFnSc4d1
diecézi	diecéze	k1gFnSc4
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
–	–	k?
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
biskupství	biskupství	k1gNnSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
podléhající	podléhající	k2eAgNnSc4d1
arcibiskupství	arcibiskupství	k1gNnSc4
v	v	k7c6
Mohuči	Mohuč	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
také	také	k9
povolal	povolat	k5eAaPmAgInS
z	z	k7c2
Uher	Uhry	k1gFnPc2
zpět	zpět	k6eAd1
do	do	k7c2
Sázavského	sázavský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
vyhnané	vyhnaný	k2eAgInPc1d1
mnichy	mnich	k1gInPc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
obnovení	obnovení	k1gNnSc4
slovanské	slovanský	k2eAgFnSc2d1
liturgie	liturgie	k1gFnSc2
papež	papež	k1gMnSc1
nepovolil	povolit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
odmítl	odmítnout	k5eAaPmAgInS
žádost	žádost	k1gFnSc4
krále	král	k1gMnSc2
Vratislava	Vratislav	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1080	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
žádal	žádat	k5eAaImAgMnS
povolení	povolení	k1gNnSc4
liturgie	liturgie	k1gFnSc2
slovanské	slovanský	k2eAgFnSc2d1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Quia	Quia	k1gMnSc1
nobilitas	nobilitas	k1gMnSc1
tua	tua	k?
postulavit	postulavit	k5eAaPmF
<g/>
,	,	kIx,
quo	quo	k?
secundum	secundum	k1gInSc1
Slavonicam	Slavonicam	k1gInSc1
linguam	linguam	k1gInSc1
apud	apuda	k1gFnPc2
Vos	vosa	k1gFnPc2
divinum	divinum	k1gInSc1
annueremus	annueremus	k1gInSc4
celebrari	celebrar	k1gFnSc2
officium	officium	k1gNnSc4
<g/>
,	,	kIx,
scias	scias	k1gInSc1
nos	nos	k1gInSc1
huic	huic	k1gFnSc1
petitioni	petitioň	k1gFnSc3
tuae	tuae	k1gFnSc4
nequaquam	quaquam	k6eNd1
posse	poss	k1gMnSc5
favare	favar	k1gMnSc5
-	-	kIx~
Ježto	jenžto	k3yRgFnPc4
Vznešenosti	vznešenost	k1gFnPc4
tvá	tvůj	k3xOp2gFnSc1
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
dovolili	dovolit	k5eAaPmAgMnP
jazykem	jazyk	k1gInSc7
slovanským	slovanský	k2eAgInSc7d1
u	u	k7c2
Vás	vy	k3xPp2nPc2
sloužiti	sloužit	k5eAaImF
služby	služba	k1gFnSc2
Boží	boží	k2eAgFnSc2d1
<g/>
,	,	kIx,
věz	vědět	k5eAaImRp2nS
<g/>
,	,	kIx,
že	že	k8xS
této	tento	k3xDgFnSc2
žádosti	žádost	k1gFnSc2
Tvé	tvůj	k3xOp2gNnSc4
nijak	nijak	k6eAd1
nemůžeme	moct	k5eNaImIp1nP
vyhověti	vyhovět	k5eAaPmF
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
pražského	pražský	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
Šebíře	Šebíř	k1gInSc2
se	se	k3xPyFc4
konflikt	konflikt	k1gInSc1
vyostřil	vyostřit	k5eAaPmAgInS
a	a	k8xC
dokonce	dokonce	k9
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
přeroste	přerůst	k5eAaPmIp3nS
v	v	k7c4
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaromírovu	Jaromírův	k2eAgFnSc4d1
kandidaturu	kandidatura	k1gFnSc4
totiž	totiž	k9
podpořila	podpořit	k5eAaPmAgFnS
obě	dva	k4xCgNnPc4
moravská	moravský	k2eAgNnPc4d1
údělná	údělný	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gMnSc1
odpověděl	odpovědět	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
založil	založit	k5eAaPmAgMnS
vyšehradskou	vyšehradský	k2eAgFnSc4d1
kapitulu	kapitula	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
podřízena	podřízen	k2eAgFnSc1d1
přímo	přímo	k6eAd1
Římu	Řím	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřátelství	nepřátelství	k1gNnSc1
mezi	mezi	k7c7
Vratislavem	Vratislav	k1gMnSc7
a	a	k8xC
Jaromírem	Jaromír	k1gMnSc7
pokračovalo	pokračovat	k5eAaImAgNnS
a	a	k8xC
postupně	postupně	k6eAd1
do	do	k7c2
něj	on	k3xPp3gMnSc4
vstoupili	vstoupit	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
dva	dva	k4xCgMnPc1
činitelé	činitel	k1gMnPc1
–	–	k?
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
a	a	k8xC
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
v	v	k7c6
knížectví	knížectví	k1gNnSc6
o	o	k7c6
obnovení	obnovení	k1gNnSc6
jediné	jediný	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spory	spora	k1gFnSc2
dočasně	dočasně	k6eAd1
utichly	utichnout	k5eAaPmAgFnP
roku	rok	k1gInSc2
1077	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Jaromír	Jaromír	k1gMnSc1
stal	stát	k5eAaPmAgMnS
kancléřem	kancléř	k1gMnSc7
císaře	císař	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1085	#num#	k4
císař	císař	k1gMnSc1
také	také	k9
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pražské	pražský	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
zahrnovalo	zahrnovat	k5eAaImAgNnS
i	i	k9
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
Vratislav	Vratislav	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
o	o	k7c4
obnovení	obnovení	k1gNnSc4
olomouckého	olomoucký	k2eAgNnSc2d1
biskupství	biskupství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaromír	Jaromír	k1gMnSc1
odjel	odjet	k5eAaPmAgMnS
protestovat	protestovat	k5eAaBmF
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
cestou	cesta	k1gFnSc7
zemřel	zemřít	k5eAaPmAgInS
(	(	kIx(
<g/>
1090	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1
léta	léto	k1gNnPc1
života	život	k1gInSc2
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1
léta	léto	k1gNnPc1
Vratislavovy	Vratislavův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
přinesla	přinést	k5eAaPmAgFnS
konflikty	konflikt	k1gInPc4
v	v	k7c6
přemyslovském	přemyslovský	k2eAgInSc6d1
rodě	rod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Oty	Ota	k1gMnSc2
Olomouckého	olomoucký	k2eAgInSc2d1
(	(	kIx(
<g/>
1086	#num#	k4
<g/>
)	)	kIx)
svěřil	svěřit	k5eAaPmAgMnS
král	král	k1gMnSc1
olomoucký	olomoucký	k2eAgMnSc1d1
úděl	úděl	k1gInSc4
svému	svůj	k3xOyFgMnSc3
synovi	syn	k1gMnSc3
Boleslavovi	Boleslav	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práva	právo	k1gNnPc1
Otových	Otův	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
naopak	naopak	k6eAd1
bránil	bránit	k5eAaImAgMnS
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gMnSc1
vpadl	vpadnout	k5eAaPmAgMnS
na	na	k7c4
Moravu	Morava	k1gFnSc4
(	(	kIx(
<g/>
1091	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
obléhal	obléhat	k5eAaImAgMnS
Brno	Brno	k1gNnSc4
<g/>
,	,	kIx,
během	během	k7c2
obléhání	obléhání	k1gNnSc2
ale	ale	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzpouře	vzpoura	k1gFnSc3
v	v	k7c6
královském	královský	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
vedl	vést	k5eAaImAgMnS
Vratislavův	Vratislavův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplikovaná	komplikovaný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
přiměla	přimět	k5eAaPmAgFnS
krále	král	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podle	podle	k7c2
stařešinského	stařešinský	k2eAgInSc2d1
řádu	řád	k1gInSc2
určil	určit	k5eAaPmAgInS
za	za	k7c2
svého	svůj	k3xOyFgMnSc2
nástupce	nástupce	k1gMnSc2
Konráda	Konrád	k1gMnSc2
Brněnského	brněnský	k2eAgMnSc2d1
a	a	k8xC
oba	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
se	se	k3xPyFc4
smířili	smířit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břetislav	Břetislav	k1gMnSc1
raději	rád	k6eAd2
prchl	prchnout	k5eAaPmAgMnS
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
ledna	leden	k1gInSc2
1092	#num#	k4
král	král	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
zranění	zranění	k1gNnSc4
po	po	k7c6
pádu	pád	k1gInSc6
z	z	k7c2
koně	kůň	k1gMnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
ve	v	k7c6
vyšehradském	vyšehradský	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
hrob	hrob	k1gInSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
dosud	dosud	k6eAd1
nepodařilo	podařit	k5eNaPmAgNnS
najít	najít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
třicetiletá	třicetiletý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
znamení	znamení	k1gNnSc6
růstu	růst	k1gInSc2
prestiže	prestiž	k1gFnSc2
a	a	k8xC
moci	moc	k1gFnSc2
českého	český	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
však	však	k9
zanedlouho	zanedlouho	k6eAd1
změnilo	změnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Poslechnout	poslechnout	k5eAaPmF
si	se	k3xPyFc3
článek	článek	k1gInSc4
·	·	k?
info	info	k6eAd1
</s>
<s>
Tato	tento	k3xDgFnSc1
zvuková	zvukový	k2eAgFnSc1d1
nahrávka	nahrávka	k1gFnSc1
byla	být	k5eAaImAgFnS
pořízena	pořídit	k5eAaPmNgFnS
z	z	k7c2
revize	revize	k1gFnSc2
data	datum	k1gNnSc2
7	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
nereflektuje	reflektovat	k5eNaImIp3nS
změny	změna	k1gFnPc4
po	po	k7c6
tomto	tento	k3xDgNnSc6
datu	datum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Více	hodně	k6eAd2
namluvených	namluvený	k2eAgInPc2d1
článků	článek	k1gInPc2
•	•	k?
Nápověda	nápověda	k1gFnSc1
</s>
<s>
Vratislavova	Vratislavův	k2eAgFnSc1d1
první	první	k4xOgFnSc1
manželka	manželka	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
při	při	k7c6
předčasném	předčasný	k2eAgInSc6d1
porodu	porod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
druhou	druhý	k4xOgFnSc7
manželkou	manželka	k1gFnSc7
se	se	k3xPyFc4
asi	asi	k9
roku	rok	k1gInSc2
1057	#num#	k4
stala	stát	k5eAaPmAgFnS
Adléta	Adléta	k1gFnSc1
Uherská	uherský	k2eAgFnSc1d1
(	(	kIx(
<g/>
kolem	kolem	k7c2
1040	#num#	k4
<g/>
–	–	k?
<g/>
1062	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
†	†	k?
1100	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
</s>
<s>
∞	∞	k?
1094	#num#	k4
Lukarta	Lukarta	k1gFnSc1
z	z	k7c2
Bogenu	Bogen	k1gInSc2
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1062	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Judita	Judita	k1gFnSc1
Přemyslovna	Přemyslovna	k1gFnSc1
(	(	kIx(
<g/>
1056	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
–	–	k?
<g/>
1086	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polská	polský	k2eAgFnSc1d1
kněžna	kněžna	k1gFnSc1
</s>
<s>
∞	∞	k?
1080	#num#	k4
Vladislav	Vladislava	k1gFnPc2
I.	I.	kA
Heřman	Heřman	k1gMnSc1
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
(	(	kIx(
<g/>
†	†	k?
po	po	k7c6
1100	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Asi	asi	k9
rok	rok	k1gInSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
Adléty	Adléta	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1062	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Vratislav	Vratislav	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
potřetí	potřetí	k4xO
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Svatava	Svatava	k1gFnSc1
Polská	polský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1046	#num#	k4
<g/>
/	/	kIx~
<g/>
1048	#num#	k4
-	-	kIx~
1126	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
měl	mít	k5eAaImAgMnS
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1091	#num#	k4
<g/>
)	)	kIx)
,	,	kIx,
olomoucký	olomoucký	k2eAgMnSc1d1
údělník	údělník	k1gMnSc1
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
asi	asi	k9
1064	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
</s>
<s>
∞	∞	k?
1100	#num#	k4
Helbirga	Helbirga	k1gFnSc1
Babenberská	babenberský	k2eAgFnSc5d1
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
†	†	k?
1125	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
</s>
<s>
∞	∞	k?
1111	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Richenza	Richenza	k1gFnSc1
z	z	k7c2
Bergu	Berg	k1gInSc2
</s>
<s>
Judita	Judita	k1gFnSc1
Grojčská	Grojčský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1066	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
1108	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
∞	∞	k?
1086	#num#	k4
Wiprecht	Wiprecht	k2eAgInSc4d1
Grojčský	Grojčský	k2eAgInSc4d1
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
†	†	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
</s>
<s>
∞	∞	k?
1123	#num#	k4
Adleyta	Adleyta	k1gFnSc1
Arpádovna	Arpádovna	k1gFnSc1
</s>
<s>
Rodokmen	rodokmen	k1gInSc1
</s>
<s>
Oldřichzm	Oldřichzm	k1gInSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1034	#num#	k4
</s>
<s>
Božena	Božena	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
zm	zm	k?
<g/>
.	.	kIx.
po	po	k7c4
1052	#num#	k4
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Nordgavskýzm	Nordgavskýzm	k1gMnSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1017	#num#	k4
</s>
<s>
Gerberga	Gerberga	k1gFnSc1
<g/>
(	(	kIx(
<g/>
z	z	k7c2
Grabfeldu	Grabfeld	k1gInSc2
<g/>
)	)	kIx)
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
zm	zm	k?
<g/>
.	.	kIx.
po	po	k7c4
1036	#num#	k4
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
<g/>
zm	zm	k?
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1055	#num#	k4
</s>
<s>
Jitka	Jitka	k1gFnSc1
ze	z	k7c2
Svinibroduzm	Svinibroduzma	k1gFnPc2
<g/>
.	.	kIx.
po	po	k7c4
1052	#num#	k4
</s>
<s>
1	#num#	k4
neznámá	známý	k2eNgFnSc1d1
manželka	manželka	k1gFnSc1
</s>
<s>
2	#num#	k4
Adléta	Adléta	k1gMnSc1
Uherskánar	Uherskánar	k1gMnSc1
<g/>
.	.	kIx.
asi	asi	k9
1040	#num#	k4
<g/>
zm	zm	k?
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1062	#num#	k4
OO	OO	kA
asi	asi	k9
1057	#num#	k4
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
zm	zm	k?
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1092	#num#	k4
</s>
<s>
3	#num#	k4
Svatava	Svatava	k1gFnSc1
Polskánar	Polskánara	k1gFnPc2
<g/>
.	.	kIx.
asi	asi	k9
1046	#num#	k4
<g/>
-	-	kIx~
<g/>
1048	#num#	k4
<g/>
zm	zm	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1126	#num#	k4
OO	OO	kA
1062	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zm	zm	k?
<g/>
.	.	kIx.
1100	#num#	k4
</s>
<s>
Vratislavzm	Vratislavzm	k1gInSc1
<g/>
.	.	kIx.
1061	#num#	k4
</s>
<s>
Judita	Judita	k1gFnSc1
Přemyslovna	Přemyslovna	k1gFnSc1
zm	zm	k?
<g/>
.	.	kIx.
1086	#num#	k4
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
zm	zm	k?
<g/>
.	.	kIx.
po	po	k7c4
1100	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
zm	zm	k?
<g/>
.	.	kIx.
1091	#num#	k4
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1064	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
zm	zm	k?
<g/>
.	.	kIx.
1124	#num#	k4
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
zm	zm	k?
<g/>
.	.	kIx.
1125	#num#	k4
</s>
<s>
Judita	Judita	k1gFnSc1
Grojčskánar	Grojčskánara	k1gFnPc2
<g/>
.	.	kIx.
1066	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
zm	zm	k?
<g/>
.	.	kIx.
1108	#num#	k4
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1075	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
zm	zm	k?
<g/>
.	.	kIx.
1140	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SCHELLE	SCHELLE	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
Zemí	zem	k1gFnSc7
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tematická	tematický	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
,	,	kIx,
Stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
649	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
-	-	kIx~
<g/>
652	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
44	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hájek	Hájek	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
Kronice	kronika	k1gFnSc6
české	český	k2eAgNnSc1d1
(	(	kIx(
<g/>
1541	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gInSc6
Stránský	Stránský	k1gMnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
státě	stát	k1gInSc6
(	(	kIx(
<g/>
1634	#num#	k4
<g/>
)	)	kIx)
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
Arabona	Arabona	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
„	„	k?
<g/>
Dobrý	dobrý	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Dobré	dobrý	k2eAgNnSc1d1
útočiště	útočiště	k1gNnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ovšem	ovšem	k9
nijak	nijak	k6eAd1
nedokládají	dokládat	k5eNaImIp3nP
a	a	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
formě	forma	k1gFnSc3
jména	jméno	k1gNnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
velmi	velmi	k6eAd1
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
;	;	kIx,
stojí	stát	k5eAaImIp3nS
za	za	k7c4
pozornost	pozornost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
latinský	latinský	k2eAgInSc4d1
název	název	k1gInSc4
uherského	uherský	k2eAgNnSc2d1
(	(	kIx(
<g/>
maďarského	maďarský	k2eAgNnSc2d1
<g/>
)	)	kIx)
města	město	k1gNnSc2
Ráb	Ráb	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
↑	↑	k?
VANÍČEK	Vaníček	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
:	:	kIx,
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
evropského	evropský	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
obratu	obrat	k1gInSc2
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
269	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
655	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
127	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
:	:	kIx,
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
↑	↑	k?
KOSMAS	Kosmas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Karel	Karel	k1gMnSc1
Hrdina	Hrdina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
.	.	kIx.
229	#num#	k4
s.	s.	k?
S.	S.	kA
125	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Kosmova	Kosmův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
↑	↑	k?
VANÍČEK	Vaníček	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
První	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgNnPc1
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
655	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
S.	S.	kA
224	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
FROLÍK	FROLÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
I.	I.	kA
Do	do	k7c2
roku	rok	k1gInSc2
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
800	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
265	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
I.	I.	kA
<g/>
/	/	kIx~
<g/>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Břetislava	Břetislav	k1gMnSc2
I.	I.	kA
do	do	k7c2
Přemysla	Přemysl	k1gMnSc2
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Laichter	Laichter	k1gMnSc1
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
1214	#num#	k4
s.	s.	k?
</s>
<s>
REITINGER	REITINGER	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
král	král	k1gMnSc1
Čechů	Čech	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
536	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
2070	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VANÍČEK	Vaníček	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I.	I.	kA
<g/>
)	)	kIx)
:	:	kIx,
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
evropského	evropský	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
obratu	obrat	k1gInSc2
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
269	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
655	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc1d1
906	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
464	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
563	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polská	polský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
českých	český	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
časopis	časopis	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
102	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
721	#num#	k4
<g/>
-	-	kIx~
<g/>
744	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
6111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc4
česká	český	k2eAgNnPc4d1
království	království	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
438	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
278	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vratislav	Vratislav	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
RYANTOVÁ	RYANTOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
VOREL	Vorel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
940	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
45	#num#	k4
<g/>
-	-	kIx~
<g/>
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čechy	Čech	k1gMnPc7
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc1d1
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
712	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
905	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Polská	polský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
“	“	k?
Vratislava	Vratislava	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
aneb	aneb	k?
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
ho	on	k3xPp3gMnSc4
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
mohl	moct	k5eAaImAgMnS
obdařit	obdařit	k5eAaPmF
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glosy	glosa	k1gFnPc4
ke	k	k7c3
středověké	středověký	k2eAgFnSc3d1
korunovační	korunovační	k2eAgFnSc3d1
symbolice	symbolika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
časopis	časopis	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
104	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
46	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
6111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vratislav	Vratislava	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
na	na	k7c4
www.e-stredovek.cz	www.e-stredovek.cz	k1gInSc4
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Mailberku	Mailberk	k1gInSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Spytihněv	Spytihněv	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.1061	.1061	k4
<g/>
–	–	k?
<g/>
1085	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
</s>
<s>
Český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
I	i	k9
<g/>
.1085	.1085	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgMnPc1
mytičtí	mytický	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
</s>
<s>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Krok	krok	k1gInSc1
•	•	k?
Libuše	Libuše	k1gFnSc1
Mytičtí	mytický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Přemysl	Přemysl	k1gMnSc1
Oráč	oráč	k1gMnSc1
•	•	k?
Nezamysl	Nezamysl	k1gMnSc1
•	•	k?
Mnata	Mnata	k1gFnSc1
•	•	k?
Vojen	vojna	k1gFnPc2
•	•	k?
Vnislav	Vnislav	k1gMnSc1
•	•	k?
Křesomysl	Křesomysl	k1gMnSc1
•	•	k?
Neklan	Neklan	k1gMnSc1
•	•	k?
Hostivít	Hostivít	k1gMnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
872	#num#	k4
<g/>
–	–	k?
<g/>
889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
I.	I.	kA
(	(	kIx(
<g/>
894	#num#	k4
<g/>
–	–	k?
<g/>
915	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
915	#num#	k4
<g/>
–	–	k?
<g/>
921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
svatý	svatý	k1gMnSc1
Václav	Václav	k1gMnSc1
(	(	kIx(
<g/>
921	#num#	k4
<g/>
–	–	k?
<g/>
935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
935	#num#	k4
<g/>
–	–	k?
<g/>
972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
972	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
999	#num#	k4
<g/>
–	–	k?
<g/>
1002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Boleslav	Boleslav	k1gMnSc1
Chrabrý¹	Chrabrý¹	k1gMnSc1
(	(	kIx(
<g/>
1003	#num#	k4
<g/>
–	–	k?
<g/>
1004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladivoj	Vladivoj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1004	#num#	k4
<g/>
–	–	k?
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oldřich	Oldřich	k1gMnSc1
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
–	–	k?
<g/>
1033	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaromír	Jaromír	k1gMnSc1
(	(	kIx(
<g/>
1033	#num#	k4
<g/>
–	–	k?
<g/>
1034	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spytihněv	Spytihněv	k1gFnPc4
II	II	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
–	–	k?
<g/>
1061	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1061	#num#	k4
<g/>
–	–	k?
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
I.	I.	kA
Brněnský	brněnský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Břetislav	Břetislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1092	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
(	(	kIx(
<g/>
1100	#num#	k4
<g/>
–	–	k?
<g/>
1107	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Svatopluk	Svatopluk	k1gMnSc1
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1109	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1109	#num#	k4
<g/>
–	–	k?
<g/>
1117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bořivoj	Bořivoj	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1117	#num#	k4
<g/>
–	–	k?
<g/>
1020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
I.	I.	kA
(	(	kIx(
<g/>
1120	#num#	k4
<g/>
–	–	k?
<g/>
1025	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1125	#num#	k4
<g/>
–	–	k?
<g/>
1140	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1140	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1173	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Soběslav	Soběslav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1173	#num#	k4
<g/>
–	–	k?
<g/>
1178	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
1178	#num#	k4
<g/>
–	–	k?
<g/>
1189	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
(	(	kIx(
<g/>
1193	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Vladislav	Vladislav	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1198	#num#	k4
<g/>
–	–	k?
<g/>
1230	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1230	#num#	k4
<g/>
–	–	k?
<g/>
1253	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1253	#num#	k4
<g/>
–	–	k?
<g/>
1278	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1278	#num#	k4
<g/>
–	–	k?
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1306	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Korutanský	korutanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1307	#num#	k4
<g/>
–	–	k?
<g/>
1310	#num#	k4
<g/>
)	)	kIx)
Lucemburkové	Lucemburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
–	–	k?
<g/>
1419	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1419	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
interregnum	interregnum	k1gNnSc4
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1453	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1453	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1457	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1471	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín²	Korvín²	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
Jagellonci	Jagellonec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fridrich	Fridrich	k1gMnSc1
Falcký³	Falcký³	k1gMnSc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1620	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský³	Bavorský³	k1gFnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
Piastovec	Piastovec	k1gInSc1
<g/>
,	,	kIx,
²	²	k?
vládce	vládce	k1gMnSc1
vedlejších	vedlejší	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
,	,	kIx,
³	³	k?
vzdorokrál	vzdorokrál	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000728812	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
102473374	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2004078178	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
200009660	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2004078178	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
