<p>
<s>
Nakanošima	Nakanošima	k1gFnSc1	Nakanošima
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
(	(	kIx(	(
<g/>
9	[number]	k4	9
×	×	k?	×
5,5	[number]	k4	5,5
km	km	kA	km
<g/>
)	)	kIx)	)
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
souostroví	souostroví	k1gNnSc6	souostroví
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
<g/>
,	,	kIx,	,
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Kagošima	Kagošimum	k1gNnSc2	Kagošimum
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Tošimamura	Tošimamura	k1gFnSc1	Tošimamura
(	(	kIx(	(
<g/>
十	十	k?	十
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kagošima	Kagošimum	k1gNnSc2	Kagošimum
(	(	kIx(	(
<g/>
鹿	鹿	k?	鹿
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc7d3	veliký
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2004	[number]	k4	2004
–	–	k?	–
167	[number]	k4	167
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
podřazeném	podřazený	k2eAgNnSc6d1	podřazené
souostroví	souostroví	k1gNnSc6	souostroví
Tokara	Tokara	k1gFnSc1	Tokara
(	(	kIx(	(
<g/>
吐	吐	k?	吐
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnSc1d2	starší
vulkanické	vulkanický	k2eAgNnSc1d1	vulkanické
centrum	centrum	k1gNnSc1	centrum
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
oddělené	oddělený	k2eAgNnSc1d1	oddělené
plochým	plochý	k2eAgNnSc7d1	ploché
plató	plató	k1gNnSc7	plató
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc2d1	aktivní
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
stavby	stavba	k1gFnSc2	stavba
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholový	vrcholový	k2eAgInSc1d1	vrcholový
kráter	kráter	k1gInSc1	kráter
aktivního	aktivní	k2eAgNnSc2d1	aktivní
centra	centrum	k1gNnSc2	centrum
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
dešťů	dešť	k1gInPc2	dešť
zalitý	zalitý	k2eAgInSc4d1	zalitý
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
solfataře	solfatař	k1gMnPc4	solfatař
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
úbočí	úbočí	k1gNnSc6	úbočí
byla	být	k5eAaImAgFnS	být
těžena	těžen	k2eAgFnSc1d1	těžena
síra	síra	k1gFnSc1	síra
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nakanošima	Nakanošimum	k1gNnSc2	Nakanošimum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.volcano.si.edu	www.volcano.si.edu	k6eAd1	www.volcano.si.edu
–	–	k?	–
ostrov	ostrov	k1gInSc4	ostrov
Nakanošima	Nakanošim	k1gMnSc4	Nakanošim
na	na	k7c6	na
Global	globat	k5eAaImAgInS	globat
Volcanism	Volcanis	k1gNnSc7	Volcanis
Program	program	k1gInSc1	program
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nakano-šima	Nakano-šimum	k1gNnSc2	Nakano-šimum
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
