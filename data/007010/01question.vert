<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Richarda	Richard	k1gMnSc2	Richard
Lví	lví	k2eAgNnSc1d1	lví
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
boku	bok	k1gInSc6	bok
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
třetí	třetí	k4xOgFnPc4	třetí
křížové	křížový	k2eAgFnPc4d1	křížová
výpravy	výprava	k1gFnPc4	výprava
<g/>
?	?	kIx.	?
</s>
