<s>
Berengarie	Berengarie	k1gFnSc1	Berengarie
Navarrská	navarrský	k2eAgFnSc1d1	Navarrská
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Bérangè	Bérangè	k1gMnSc2	Bérangè
de	de	k?	de
Navarre	Navarr	k1gMnSc5	Navarr
<g/>
,	,	kIx,	,
katalánsky	katalánsky	k6eAd1	katalánsky
Berenguera	Berenguera	k1gFnSc1	Berenguera
de	de	k?	de
Navarra	Navarra	k1gFnSc1	Navarra
;	;	kIx,	;
1165	[number]	k4	1165
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1230	[number]	k4	1230
Le	Le	k1gFnSc2	Le
Mans	Mansa	k1gFnPc2	Mansa
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Richarda	Richard	k1gMnSc2	Richard
Lví	lví	k2eAgNnSc1d1	lví
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
boku	bok	k1gInSc6	bok
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
třetí	třetí	k4xOgFnPc4	třetí
křížové	křížový	k2eAgFnPc4d1	křížová
výpravy	výprava	k1gFnPc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Berengarie	Berengarie	k1gFnSc1	Berengarie
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
navarrského	navarrský	k2eAgMnSc2d1	navarrský
krále	král	k1gMnSc2	král
Sancha	Sanch	k1gMnSc2	Sanch
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Moudrého	moudrý	k2eAgMnSc2d1	moudrý
a	a	k8xC	a
Sanchi	Sanch	k1gFnSc2	Sanch
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
kastilského	kastilský	k2eAgMnSc4d1	kastilský
krále	král	k1gMnSc4	král
Alfonse	Alfons	k1gMnSc2	Alfons
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Podnět	podnět	k1gInSc1	podnět
k	k	k7c3	k
manželskému	manželský	k2eAgInSc3d1	manželský
svazku	svazek	k1gInSc3	svazek
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Lví	lví	k2eAgFnSc2d1	lví
srdce	srdce	k1gNnPc4	srdce
dala	dát	k5eAaPmAgFnS	dát
Richardova	Richardův	k2eAgFnSc1d1	Richardova
energická	energický	k2eAgFnSc1d1	energická
matka	matka	k1gFnSc1	matka
Eleonora	Eleonora	k1gFnSc1	Eleonora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nechtěla	chtít	k5eNaImAgFnS	chtít
dopustit	dopustit	k5eAaPmF	dopustit
spříznění	spříznění	k1gNnSc4	spříznění
s	s	k7c7	s
Kapetovci	Kapetovec	k1gMnPc7	Kapetovec
<g/>
.	.	kIx.	.
</s>
<s>
Sňatku	sňatek	k1gInSc3	sňatek
předcházelo	předcházet	k5eAaImAgNnS	předcházet
nevěstino	nevěstin	k2eAgNnSc1d1	nevěstino
únavné	únavný	k2eAgNnSc1d1	únavné
putování	putování	k1gNnSc1	putování
za	za	k7c7	za
budoucím	budoucí	k2eAgMnSc7d1	budoucí
chotěm	choť	k1gMnSc7	choť
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
přes	přes	k7c4	přes
Alpy	Alpy	k1gFnPc4	Alpy
a	a	k8xC	a
Lombardii	Lombardie	k1gFnSc4	Lombardie
do	do	k7c2	do
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
plavily	plavit	k5eAaImAgFnP	plavit
obě	dva	k4xCgFnPc1	dva
dámy	dáma	k1gFnPc1	dáma
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Reggiu	Reggium	k1gNnSc6	Reggium
di	di	k?	di
Calabria	Calabrium	k1gNnPc4	Calabrium
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1191	[number]	k4	1191
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
totiž	totiž	k9	totiž
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
vyslyšel	vyslyšet	k5eAaPmAgMnS	vyslyšet
volání	volání	k1gNnSc4	volání
křesťanstva	křesťanstvo	k1gNnSc2	křesťanstvo
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
dvaatřicet	dvaatřicet	k4xCc4	dvaatřicet
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dle	dle	k7c2	dle
současníků	současník	k1gMnPc2	současník
byl	být	k5eAaImAgInS	být
světlovlasý	světlovlasý	k2eAgInSc1d1	světlovlasý
<g/>
,	,	kIx,	,
prchlivý	prchlivý	k2eAgInSc1d1	prchlivý
a	a	k8xC	a
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
věk	věk	k1gInSc4	věk
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
jeho	jeho	k3xOp3gFnSc1	jeho
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
Richardova	Richardův	k2eAgMnSc2d1	Richardův
současníka	současník	k1gMnSc2	současník
<g/>
:	:	kIx,	:
Královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
zanechala	zanechat	k5eAaPmAgFnS	zanechat
Berengarii	Berengarie	k1gFnSc4	Berengarie
v	v	k7c6	v
Mesině	Mesina	k1gFnSc6	Mesina
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
prozatím	prozatím	k6eAd1	prozatím
pro	pro	k7c4	pro
právě	právě	k6eAd1	právě
probíhající	probíhající	k2eAgFnSc4d1	probíhající
postní	postní	k2eAgFnSc4d1	postní
dobu	doba	k1gFnSc4	doba
odložena	odložen	k2eAgFnSc1d1	odložena
a	a	k8xC	a
navarrská	navarrský	k2eAgFnSc1d1	Navarrská
princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
ke	k	k7c3	k
křížové	křížový	k2eAgFnSc3d1	křížová
výpravě	výprava	k1gFnSc3	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
kronikáře	kronikář	k1gMnSc2	kronikář
Richarda	Richard	k1gMnSc2	Richard
z	z	k7c2	z
Devizes	Devizesa	k1gFnPc2	Devizesa
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
odjezdu	odjezd	k1gInSc6	odjezd
z	z	k7c2	z
Messiny	Messina	k1gFnSc2	Messina
zřejmě	zřejmě	k6eAd1	zřejmě
stále	stále	k6eAd1	stále
pannou	panna	k1gFnSc7	panna
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
švagrovou	švagrův	k2eAgFnSc7d1	švagrova
Johanou	Johana	k1gFnSc7	Johana
<g/>
,	,	kIx,	,
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
sicilském	sicilský	k2eAgMnSc6d1	sicilský
králi	král	k1gMnSc6	král
Vilémovi	Vilém	k1gMnSc6	Vilém
se	se	k3xPyFc4	se
během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
prozíravě	prozíravě	k6eAd1	prozíravě
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
hrozícímu	hrozící	k2eAgInSc3d1	hrozící
zajetí	zajetí	k1gNnSc6	zajetí
u	u	k7c2	u
kyperského	kyperský	k2eAgNnSc2d1	kyperské
města	město	k1gNnSc2	město
Amathos	Amathosa	k1gFnPc2	Amathosa
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1191	[number]	k4	1191
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
v	v	k7c6	v
Limassolu	Limassol	k1gInSc6	Limassol
za	za	k7c4	za
Richarda	Richard	k1gMnSc4	Richard
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
byla	být	k5eAaImAgFnS	být
korunována	korunovat	k5eAaBmNgFnS	korunovat
anglickou	anglický	k2eAgFnSc7d1	anglická
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
června	červen	k1gInSc2	červen
1191	[number]	k4	1191
vyplula	vyplout	k5eAaPmAgFnS	vyplout
výprava	výprava	k1gFnSc1	výprava
z	z	k7c2	z
Kypru	Kypr	k1gInSc2	Kypr
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Svaté	svatý	k2eAgFnSc3d1	svatá
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Berengarie	Berengarie	k1gFnSc1	Berengarie
trávila	trávit	k5eAaImAgFnS	trávit
většinu	většina	k1gFnSc4	většina
výpravy	výprava	k1gFnSc2	výprava
po	po	k7c6	po
boku	bok	k1gInSc6	bok
švagrové	švagrová	k1gFnSc2	švagrová
Johany	Johana	k1gFnSc2	Johana
<g/>
.	.	kIx.	.
</s>
<s>
Levantu	Levanta	k1gFnSc4	Levanta
obě	dva	k4xCgFnPc4	dva
ženy	žena	k1gFnPc4	žena
opustily	opustit	k5eAaPmAgFnP	opustit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Damselou	Damsela	k1gFnSc7	Damsela
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
svrženého	svržený	k2eAgMnSc2d1	svržený
kyperského	kyperský	k2eAgMnSc2d1	kyperský
císaře	císař	k1gMnSc2	císař
Izáka	Izák	k1gMnSc2	Izák
Komnena	Komnen	k1gMnSc2	Komnen
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1192	[number]	k4	1192
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypluly	vyplout	k5eAaPmAgFnP	vyplout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poklidná	poklidný	k2eAgFnSc1d1	poklidná
plavba	plavba	k1gFnSc1	plavba
byla	být	k5eAaImAgFnS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
v	v	k7c6	v
Brindisi	Brindisi	k1gNnSc6	Brindisi
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
takové	takový	k3xDgNnSc4	takový
štěstí	štěstí	k1gNnSc4	štěstí
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
cestu	cesta	k1gFnSc4	cesta
mu	on	k3xPp3gMnSc3	on
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
bouře	bouř	k1gFnPc4	bouř
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
přes	přes	k7c4	přes
Korfu	Korfu	k1gNnSc4	Korfu
<g/>
,	,	kIx,	,
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgMnSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Erdberg	Erdberg	k1gInSc1	Erdberg
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
součást	součást	k1gFnSc1	součást
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
převlek	převlek	k1gInSc4	převlek
rozpoznán	rozpoznán	k2eAgInSc1d1	rozpoznán
a	a	k8xC	a
zajat	zajat	k2eAgInSc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
strávil	strávit	k5eAaPmAgInS	strávit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
peripetiích	peripetie	k1gFnPc6	peripetie
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1195	[number]	k4	1195
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Mans	Mansa	k1gFnPc2	Mansa
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Thorée	Thorée	k1gFnSc6	Thorée
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
zemřel	zemřít	k5eAaPmAgMnS	zemřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1199	[number]	k4	1199
za	za	k7c2	za
války	válka	k1gFnSc2	válka
s	s	k7c7	s
místohrabětem	místohrabět	k1gInSc7	místohrabět
z	z	k7c2	z
Limoges	Limogesa	k1gFnPc2	Limogesa
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnPc2	zranění
kuší	kuše	k1gFnSc7	kuše
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
hradu	hrad	k1gInSc2	hrad
Châlus	Châlus	k1gInSc1	Châlus
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zranění	zranění	k1gNnSc3	zranění
došlo	dojít	k5eAaPmAgNnS	dojít
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
po	po	k7c6	po
lékařském	lékařský	k2eAgInSc6d1	lékařský
zákroku	zákrok	k1gInSc6	zákrok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
chirurg	chirurg	k1gMnSc1	chirurg
snažil	snažit	k5eAaImAgMnS	snažit
šíp	šíp	k1gInSc4	šíp
z	z	k7c2	z
páteře	páteř	k1gFnSc2	páteř
vyjmout	vyjmout	k5eAaPmF	vyjmout
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
Richarda	Richarda	k1gFnSc1	Richarda
sužovat	sužovat	k5eAaImF	sužovat
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
infekce	infekce	k1gFnSc1	infekce
zkázu	zkáza	k1gFnSc4	zkáza
dokonala	dokonat	k5eAaPmAgFnS	dokonat
<g/>
.	.	kIx.	.
</s>
<s>
Stačil	stačit	k5eAaBmAgInS	stačit
včas	včas	k6eAd1	včas
vyrozumět	vyrozumět	k5eAaPmF	vyrozumět
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dlela	dlít	k5eAaImAgFnS	dlít
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Fontevrault	Fontevraulta	k1gFnPc2	Fontevraulta
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
náručí	náručí	k1gNnSc6	náručí
<g/>
.	.	kIx.	.
</s>
<s>
Richardovo	Richardův	k2eAgNnSc1d1	Richardovo
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Rouenu	Rouen	k1gInSc6	Rouen
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
v	v	k7c6	v
opatském	opatský	k2eAgInSc6d1	opatský
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Fontevraultu	Fontevrault	k1gInSc6	Fontevrault
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnSc6	vnitřnost
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Chalus	Chalus	k1gInSc1	Chalus
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vdova	vdova	k1gFnSc1	vdova
žila	žít	k5eAaImAgFnS	žít
Berengarie	Berengarie	k1gFnPc4	Berengarie
v	v	k7c6	v
Chinonu	chinon	k1gInSc6	chinon
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Mans	Mansa	k1gFnPc2	Mansa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dodnes	dodnes	k6eAd1	dodnes
stojí	stát	k5eAaImIp3nS	stát
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
údajně	údajně	k6eAd1	údajně
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
přebývala	přebývat	k5eAaImAgFnS	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
snad	snad	k9	snad
i	i	k9	i
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
cisterciáckém	cisterciácký	k2eAgInSc6d1	cisterciácký
klášteře	klášter	k1gInSc6	klášter
Epau	Epaus	k1gInSc2	Epaus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
roku	rok	k1gInSc2	rok
1229	[number]	k4	1229
a	a	k8xC	a
trávila	trávit	k5eAaImAgFnS	trávit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
podzim	podzim	k1gInSc4	podzim
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
