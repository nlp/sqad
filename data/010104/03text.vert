<p>
<s>
Landrase	Landrase	k6eAd1	Landrase
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
plemen	plemeno	k1gNnPc2	plemeno
prasat	prase	k1gNnPc2	prase
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
klopenýma	klopený	k2eAgNnPc7d1	klopené
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc3d1	původní
landrase	landrasa	k1gFnSc3	landrasa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgNnPc4d1	dánské
prasata	prase	k1gNnPc4	prase
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
importována	importovat	k5eAaBmNgFnS	importovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
křížením	křížení	k1gNnSc7	křížení
s	s	k7c7	s
místními	místní	k2eAgNnPc7d1	místní
prasaty	prase	k1gNnPc7	prase
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
landrase	landrase	k6eAd1	landrase
belgická	belgický	k2eAgFnSc1d1	belgická
<g/>
,	,	kIx,	,
holandská	holandský	k2eAgFnSc1d1	holandská
<g/>
,	,	kIx,	,
švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
norská	norský	k2eAgFnSc1d1	norská
<g/>
,	,	kIx,	,
finská	finský	k2eAgFnSc1d1	finská
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
také	také	k9	také
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
česká	český	k2eAgFnSc1d1	Česká
landrase	landrase	k6eAd1	landrase
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
landrase	landrase	k6eAd1	landrase
liší	lišit	k5eAaImIp3nS	lišit
užitkovostí	užitkovost	k1gFnSc7	užitkovost
i	i	k8xC	i
užitkovým	užitkový	k2eAgInSc7d1	užitkový
typem	typ	k1gInSc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Užitkový	užitkový	k2eAgInSc1d1	užitkový
typ	typ	k1gInSc1	typ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
trojího	trojí	k4xRgMnSc2	trojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
bekonový	bekonový	k2eAgInSc1d1	bekonový
<g/>
,	,	kIx,	,
masný	masný	k2eAgInSc1d1	masný
nebo	nebo	k8xC	nebo
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SAMBRAUS	SAMBRAUS	kA	SAMBRAUS
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Hinrich	Hinrich	k1gMnSc1	Hinrich
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc4	Atlas
plemen	plemeno	k1gNnPc2	plemeno
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Brázda	brázda	k1gFnSc1	brázda
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
295	[number]	k4	295
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
209	[number]	k4	209
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
344	[number]	k4	344
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
