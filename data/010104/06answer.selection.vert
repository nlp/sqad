<s>
Landrase	Landrase	k6eAd1	Landrase
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
plemen	plemeno	k1gNnPc2	plemeno
prasat	prase	k1gNnPc2	prase
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
klopenýma	klopený	k2eAgNnPc7d1	klopené
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
