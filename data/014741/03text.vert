<s>
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
</s>
<s>
Norská	norský	k2eAgFnSc1d1
korunaNorsk	korunaNorsk	k1gInSc1
krone	kronout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
<g/>
)	)	kIx)
bankovka	bankovka	k1gFnSc1
100	#num#	k4
korunZemě	korunZemě	k6eAd1
</s>
<s>
Norsko	Norsko	k1gNnSc1
NorskoŠpicberky	NorskoŠpicberka	k1gFnSc2
Špicberky	Špicberky	k1gFnPc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
NOK	nok	k1gInSc1
Inflace	inflace	k1gFnSc2
</s>
<s>
1,9	1,9	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
ø	ø	k1gMnSc5
Mince	mince	k1gFnPc4
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
korun	koruna	k1gFnPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
</s>
<s>
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
skandinávského	skandinávský	k2eAgInSc2d1
státu	stát	k1gInSc2
Norsko	Norsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
kód	kód	k1gInSc4
4217	#num#	k4
koruny	koruna	k1gFnSc2
je	být	k5eAaImIp3nS
NOK	NOK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
norštině	norština	k1gFnSc6
se	se	k3xPyFc4
jednotné	jednotný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
měny	měna	k1gFnSc2
píše	psát	k5eAaImIp3nS
krone	kronout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
množné	množný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
kroner	kronra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
koruny	koruna	k1gFnSc2
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
ø	ø	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
oběhu	oběh	k1gInSc6
se	se	k3xPyFc4
však	však	k9
nevyskytují	vyskytovat	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
mince	mince	k1gFnPc4
nižší	nízký	k2eAgFnSc2d2
nominální	nominální	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
než	než	k8xS
1	#num#	k4
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
používána	používán	k2eAgFnSc1d1
i	i	k9
na	na	k7c6
Špicberkách	Špicberky	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
mezinárodního	mezinárodní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
součástí	součást	k1gFnPc2
Norského	norský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
velice	velice	k6eAd1
vysoký	vysoký	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
autonomie	autonomie	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
mohou	moct	k5eAaImIp3nP
uzavírat	uzavírat	k5eAaPmF,k5eAaImF
některé	některý	k3yIgFnPc4
mezinárodní	mezinárodní	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
nezávisle	závisle	k6eNd1
na	na	k7c6
pevninském	pevninský	k2eAgNnSc6d1
Norsku	Norsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Norsko	Norsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1875	#num#	k4
a	a	k8xC
1914	#num#	k4
členem	člen	k1gMnSc7
Skandinávské	skandinávský	k2eAgFnSc2d1
měnové	měnový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
členy	člen	k1gInPc7
byly	být	k5eAaImAgFnP
Dánsko	Dánsko	k1gNnSc4
a	a	k8xC
Švédsko	Švédsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
státech	stát	k1gInPc6
se	se	k3xPyFc4
společná	společný	k2eAgFnSc1d1
měna	měna	k1gFnSc1
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
koruna	koruna	k1gFnSc1
a	a	k8xC
byla	být	k5eAaImAgFnS
kryta	krýt	k5eAaImNgFnS
zlatem	zlato	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měnová	měnový	k2eAgFnSc1d1
unie	unie	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
na	na	k7c6
začátku	začátek	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
si	se	k3xPyFc3
ponechaly	ponechat	k5eAaPmAgInP
název	název	k1gInSc4
koruna	koruna	k1gFnSc1
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
národní	národní	k2eAgFnPc4d1
měny	měna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
a	a	k8xC
mince	mince	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnPc1
jsou	být	k5eAaImIp3nP
čtyř	čtyři	k4xCgFnPc2
nominálních	nominální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mince	mince	k1gFnSc2
10	#num#	k4
a	a	k8xC
20	#num#	k4
korun	koruna	k1gFnPc2
mají	mít	k5eAaImIp3nP
na	na	k7c6
lícové	lícový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
portrét	portrét	k1gInSc4
norského	norský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Haralda	Haraldo	k1gNnSc2
V.	V.	kA
Na	na	k7c6
10	#num#	k4
korunách	koruna	k1gFnPc6
je	být	k5eAaImIp3nS
napsáno	napsat	k5eAaPmNgNnS,k5eAaBmNgNnS
„	„	k?
<g/>
Alt	alt	k1gInSc4
for	forum	k1gNnPc2
Norge	Norg	k1gMnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
Vše	všechen	k3xTgNnSc1
pro	pro	k7c4
Norsko	Norsko	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
20	#num#	k4
korunách	koruna	k1gFnPc6
„	„	k?
<g/>
Norges	Norges	k1gInSc1
Konge	Konge	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
Norský	norský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
1	#num#	k4
a	a	k8xC
5	#num#	k4
korun	koruna	k1gFnPc2
nesou	nést	k5eAaImIp3nP
vyobrazení	vyobrazení	k1gNnSc4
norských	norský	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
a	a	k8xC
královských	královský	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Norské	norský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tisknuty	tisknout	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInPc1d1
motivy	motiv	k1gInPc1
na	na	k7c6
bankovkách	bankovka	k1gFnPc6
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
50	#num#	k4
korun	koruna	k1gFnPc2
-	-	kIx~
maják	maják	k1gInSc1
v	v	k7c6
Utvæ	Utvæ	k1gInSc6
</s>
<s>
100	#num#	k4
korun	koruna	k1gFnPc2
-	-	kIx~
vikinská	vikinský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Gokstad	Gokstad	k1gInSc1
</s>
<s>
200	#num#	k4
korun	koruna	k1gFnPc2
-	-	kIx~
treska	treska	k1gFnSc1
</s>
<s>
500	#num#	k4
korun	koruna	k1gFnPc2
-	-	kIx~
záchranná	záchranný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
RS	RS	kA
14	#num#	k4
Stavanger	Stavangero	k1gNnPc2
</s>
<s>
1000	#num#	k4
korun	koruna	k1gFnPc2
-	-	kIx~
vlna	vlna	k1gFnSc1
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
moři	moře	k1gNnSc6
</s>
<s>
Směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byl	být	k5eAaImAgInS
směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
mezi	mezi	k7c7
norskou	norský	k2eAgFnSc7d1
a	a	k8xC
českou	český	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
přibližně	přibližně	k6eAd1
3,5	3,5	k4
CZK	CZK	kA
=	=	kIx~
1	#num#	k4
NOK	NOK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
rovna	roven	k2eAgFnSc1d1
asi	asi	k9
3	#num#	k4
Kč	Kč	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
asi	asi	k9
3,3	3,3	k4
Kč	Kč	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
asi	asi	k9
2,9	2,9	k4
Kč	Kč	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
asi	asi	k9
2,35	2,35	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
-	-	kIx~
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Legal	Legal	k1gInSc1
tender	tender	k1gInSc1
notes	notes	k1gInSc1
and	and	k?
coins	coins	k1gInSc1
<g/>
.	.	kIx.
www.norges-bank.no	www.norges-bank.no	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norges	Norges	k1gInSc1
Bank	bank	k1gInSc4
-	-	kIx~
Norská	norský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
