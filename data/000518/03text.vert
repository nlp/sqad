<s>
Hipologie	hipologie	k1gFnSc1	hipologie
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
pravopisem	pravopis	k1gInSc7	pravopis
hippologie	hippologie	k1gFnSc2	hippologie
<g/>
,	,	kIx,	,
z	z	k7c2	z
řec.	řec.	k?	řec.
ι	ι	k?	ι
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
ippos	ippos	k1gMnSc1	ippos
"	"	kIx"	"
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
a	a	k8xC	a
λ	λ	k?	λ
<g/>
,	,	kIx,	,
logos	logos	k1gInSc1	logos
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nauka	nauka	k1gFnSc1	nauka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Vědec	vědec	k1gMnSc1	vědec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hipolog	hipolog	k1gMnSc1	hipolog
(	(	kIx(	(
<g/>
hippolog	hippolog	k1gMnSc1	hippolog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hipologie	hipologie	k1gFnSc1	hipologie
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc4	směs
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
jako	jako	k9	jako
každá	každý	k3xTgFnSc1	každý
nauka	nauka	k1gFnSc1	nauka
se	se	k3xPyFc4	se
i	i	k9	i
hipologie	hipologie	k1gFnSc1	hipologie
časově	časově	k6eAd1	časově
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
oblast	oblast	k1gFnSc1	oblast
medicínsko-veterinární	medicínskoeterinární	k2eAgFnSc1d1	medicínsko-veterinární
(	(	kIx(	(
<g/>
hipiatrie	hipiatrie	k1gFnSc1	hipiatrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zootechnicko	zootechnicka	k1gMnSc5	zootechnicka
-	-	kIx~	-
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
a	a	k8xC	a
jezdeckou	jezdecký	k2eAgFnSc7d1	jezdecká
(	(	kIx(	(
<g/>
ekvitace	ekvitace	k1gFnSc2	ekvitace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
studuje	studovat	k5eAaImIp3nS	studovat
zejména	zejména	k9	zejména
zdraví	zdraví	k1gNnSc1	zdraví
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
chov	chov	k1gInSc4	chov
a	a	k8xC	a
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
koně	kůň	k1gMnSc2	kůň
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
sportovního	sportovní	k2eAgInSc2d1	sportovní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Bílek	Bílek	k1gMnSc1	Bílek
Jaromír	Jaromír	k1gMnSc1	Jaromír
Dušek	Dušek	k1gMnSc1	Dušek
</s>
