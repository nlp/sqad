<s>
Hipologie	hipologie	k1gFnSc1	hipologie
(	(	kIx(	(
<g/>
starším	starý	k2eAgInSc7d2	starší
pravopisem	pravopis	k1gInSc7	pravopis
hippologie	hippologie	k1gFnSc2	hippologie
<g/>
,	,	kIx,	,
z	z	k7c2	z
řec.	řec.	k?	řec.
ι	ι	k?	ι
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
ippos	ippos	k1gMnSc1	ippos
"	"	kIx"	"
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
a	a	k8xC	a
λ	λ	k?	λ
<g/>
,	,	kIx,	,
logos	logos	k1gInSc1	logos
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nauka	nauka	k1gFnSc1	nauka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
