<s>
Charles	Charles	k1gMnSc1	Charles
Edouard	Edouard	k1gMnSc1	Edouard
Guillaume	Guillaum	k1gInSc5	Guillaum
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Fleurier	Fleurier	k1gInSc1	Fleurier
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Sè	Sè	k1gFnSc1	Sè
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzsko-švýcarský	francouzsko-švýcarský	k2eAgMnSc1d1	francouzsko-švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
objev	objev	k1gInSc4	objev
anomálií	anomálie	k1gFnPc2	anomálie
v	v	k7c6	v
niklové	niklový	k2eAgFnSc6d1	niklová
oceli	ocel	k1gFnSc6	ocel
(	(	kIx(	(
<g/>
invar	invar	k1gInSc1	invar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
přesných	přesný	k2eAgFnPc2d1	přesná
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
slitiny	slitina	k1gFnSc2	slitina
invar	invar	k1gInSc1	invar
a	a	k8xC	a
elinvar	elinvar	k1gInSc1	elinvar
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
La	la	k0	la
Température	Températur	k1gMnSc5	Températur
de	de	k?	de
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Espace	Espace	k1gFnSc1	Espace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
La	la	k0	la
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
volume	volum	k1gInSc5	volum
24	[number]	k4	24
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Études	Études	k1gMnSc1	Études
thermométriques	thermométriques	k1gMnSc1	thermométriques
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Traité	Traitý	k2eAgFnSc2d1	Traitý
de	de	k?	de
thermométrie	thermométrie	k1gFnSc2	thermométrie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Unités	Unités	k1gInSc1	Unités
et	et	k?	et
Étalons	Étalons	k1gInSc1	Étalons
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
rayons	rayons	k1gInSc1	rayons
X	X	kA	X
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Recherches	Recherches	k1gInSc1	Recherches
sur	sur	k?	sur
le	le	k?	le
nickel	nickel	k1gInSc1	nickel
et	et	k?	et
ses	ses	k?	ses
alliages	alliages	k1gInSc1	alliages
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
La	la	k1gNnSc2	la
vie	vie	k?	vie
de	de	k?	de
la	la	k1gNnSc2	la
matiè	matiè	k?	matiè
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
La	la	k1gNnSc2	la
Convention	Convention	k1gInSc1	Convention
du	du	k?	du
Mè	Mè	k1gFnSc2	Mè
et	et	k?	et
le	le	k?	le
Bureau	Bureaus	k1gInSc2	Bureaus
international	internationat	k5eAaImAgInS	internationat
des	des	k1gNnSc7	des
Poids	Poids	k1gInSc1	Poids
et	et	k?	et
Mesures	Mesures	k1gInSc1	Mesures
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
applications	applicationsa	k1gFnPc2	applicationsa
des	des	k1gNnSc2	des
aciers	aciers	k6eAd1	aciers
au	au	k0	au
nickel	nicket	k5eAaImAgMnS	nicket
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Des	des	k1gNnPc2	des
états	étatsa	k1gFnPc2	étatsa
de	de	k?	de
la	la	k1gNnSc6	la
matiè	matiè	k?	matiè
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Les	les	k1gInSc1	les
récents	récents	k1gInSc1	récents
progrè	progrè	k?	progrè
du	du	k?	du
systè	systè	k?	systè
métrique	métrique	k1gInSc1	métrique
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Charles-Edouard	Charles-Edouarda	k1gFnPc2	Charles-Edouarda
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Initiation	Initiation	k1gInSc1	Initiation
à	à	k?	à
la	la	k1gNnSc7	la
Mécanique	Mécaniqu	k1gFnSc2	Mécaniqu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Charles	Charles	k1gMnSc1	Charles
Édouard	Édouard	k1gMnSc1	Édouard
Guillaume	Guillaum	k1gInSc5	Guillaum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Charles	Charles	k1gMnSc1	Charles
Edouard	Edouard	k1gInSc1	Edouard
Guillaume	Guillaum	k1gInSc5	Guillaum
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
aldebaran	aldebarana	k1gFnPc2	aldebarana
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Official	Official	k1gInSc1	Official
Nobel	Nobel	k1gMnSc1	Nobel
site	site	k1gFnPc2	site
Nobel	Nobel	k1gMnSc1	Nobel
Lectures	Lectures	k1gMnSc1	Lectures
<g/>
,	,	kIx,	,
Physics	Physics	k1gInSc1	Physics
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
"	"	kIx"	"
Charles-Edouard	Charles-Edouard	k1gInSc1	Charles-Edouard
Guillaume	Guillaum	k1gInSc5	Guillaum
-	-	kIx~	-
Biography	Biograph	k1gInPc7	Biograph
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Elsevier	Elsevier	k1gInSc1	Elsevier
Publishing	Publishing	k1gInSc1	Publishing
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
.	.	kIx.	.
</s>
