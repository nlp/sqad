<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Janoušek	Janoušek	k1gMnSc1	Janoušek
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Přední	přední	k2eAgFnSc1d1	přední
Ždírnice	Ždírnice	k1gFnSc1	Ždírnice
u	u	k7c2	u
Nové	Nové	k2eAgFnSc2d1	Nové
Paky	Paka	k1gFnSc2	Paka
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
UB	UB	kA	UB
12	[number]	k4	12
a	a	k8xC	a
manžel	manžel	k1gMnSc1	manžel
sochařky	sochařka	k1gFnSc2	sochařka
Věry	Věra	k1gFnSc2	Věra
Janouškové	Janoušková	k1gFnSc2	Janoušková
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Janoušek	Janoušek	k1gMnSc1	Janoušek
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
mladém	mladý	k2eAgInSc6d1	mladý
věku	věk	k1gInSc6	věk
zastihly	zastihnout	k5eAaPmAgFnP	zastihnout
válečné	válečný	k2eAgFnPc1d1	válečná
události	událost	k1gFnPc1	událost
–	–	k?	–
odtržení	odtržení	k1gNnSc1	odtržení
Sudet	Sudety	k1gInPc2	Sudety
<g/>
,	,	kIx,	,
stěhování	stěhování	k1gNnSc2	stěhování
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
nucené	nucený	k2eAgNnSc1d1	nucené
pracovní	pracovní	k2eAgNnSc1d1	pracovní
nasazení	nasazení	k1gNnSc1	nasazení
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
započatá	započatý	k2eAgNnPc1d1	započaté
na	na	k7c6	na
gymnasiu	gymnasion	k1gNnSc6	gymnasion
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
kreslení	kreslení	k1gNnSc1	kreslení
Bedřich	Bedřich	k1gMnSc1	Bedřich
Mudroch	mudroch	k1gMnSc1	mudroch
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgInS	dokončit
r.	r.	kA	r.
1940	[number]	k4	1940
v	v	k7c6	v
Úpici	Úpice	k1gFnSc6	Úpice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
abiturientský	abiturientský	k2eAgInSc1d1	abiturientský
kurs	kurs	k1gInSc1	kurs
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
<g/>
,	,	kIx,	,
městečku	městečko	k1gNnSc6	městečko
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
pomohl	pomoct	k5eAaPmAgMnS	pomoct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
mezi	mezi	k7c7	mezi
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
studiem	studio	k1gNnSc7	studio
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
stráveném	strávený	k2eAgInSc6d1	strávený
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
Uměleckoprůmyslovou	uměleckoprůmyslový	k2eAgFnSc4d1	uměleckoprůmyslová
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studium	studium	k1gNnSc1	studium
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
totální	totální	k2eAgNnSc1d1	totální
nasazení	nasazení	k1gNnSc1	nasazení
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
na	na	k7c6	na
pile	pila	k1gFnSc6	pila
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
ateliéru	ateliér	k1gInSc2	ateliér
prof.	prof.	kA	prof.
Josefa	Josef	k1gMnSc2	Josef
Wagnera	Wagner	k1gMnSc2	Wagner
na	na	k7c6	na
VŠUP	VŠUP	kA	VŠUP
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spolužáky	spolužák	k1gMnPc7	spolužák
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Věry	Věra	k1gFnSc2	Věra
Havlové	Havlová	k1gFnSc2	Havlová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
příští	příští	k2eAgFnSc2d1	příští
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
Věra	Věra	k1gFnSc1	Věra
Janoušková	Janoušková	k1gFnSc1	Janoušková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Palcr	Palcr	k1gMnSc1	Palcr
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Chlupáč	chlupáč	k1gMnSc1	chlupáč
<g/>
,	,	kIx,	,
Olbram	Olbram	k1gInSc1	Olbram
Zoubek	zoubek	k1gInSc1	zoubek
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Kmentová	Kmentová	k1gFnSc1	Kmentová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
formování	formování	k1gNnSc6	formování
Janouškova	Janouškův	k2eAgInSc2d1	Janouškův
sochařského	sochařský	k2eAgInSc2d1	sochařský
výrazu	výraz	k1gInSc2	výraz
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
zejména	zejména	k9	zejména
pozdní	pozdní	k2eAgNnSc1d1	pozdní
dílo	dílo	k1gNnSc1	dílo
O.	O.	kA	O.
Gutfreunda	Gutfreund	k1gMnSc2	Gutfreund
a	a	k8xC	a
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1948	[number]	k4	1948
i	i	k8xC	i
pražské	pražský	k2eAgFnSc2d1	Pražská
výstavy	výstava	k1gFnSc2	výstava
západoevropského	západoevropský	k2eAgNnSc2d1	západoevropské
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
H.	H.	kA	H.
Moora	Moor	k1gInSc2	Moor
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
teoretiků	teoretik	k1gMnPc2	teoretik
umění	umění	k1gNnPc2	umění
zejména	zejména	k9	zejména
úvahy	úvaha	k1gFnPc1	úvaha
Bohumila	Bohumil	k1gMnSc2	Bohumil
Kubišty	Kubišta	k1gMnSc2	Kubišta
a	a	k8xC	a
přednášky	přednáška	k1gFnSc2	přednáška
prof.	prof.	kA	prof.
Václava	Václav	k1gMnSc2	Václav
Nebeského	nebeský	k2eAgMnSc2d1	nebeský
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
absolventskou	absolventský	k2eAgFnSc7d1	absolventská
prací	práce	k1gFnSc7	práce
–	–	k?	–
monumentální	monumentální	k2eAgFnSc7d1	monumentální
sochou	socha	k1gFnSc7	socha
Mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
sochařkou	sochařka	k1gFnSc7	sochařka
Věrou	Věra	k1gFnSc7	Věra
Havlovou	Havlová	k1gFnSc7	Havlová
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
strávil	strávit	k5eAaPmAgMnS	strávit
dva	dva	k4xCgInPc4	dva
semestry	semestr	k1gInPc4	semestr
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Svazu	svaz	k1gInSc2	svaz
čs	čs	kA	čs
<g/>
.	.	kIx.	.
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
skupiny	skupina	k1gFnSc2	skupina
UB	UB	kA	UB
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
ateliéru	ateliér	k1gInSc6	ateliér
v	v	k7c6	v
Praze-Košířích	Praze-Košíř	k1gInPc6	Praze-Košíř
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
též	též	k6eAd1	též
ve	v	k7c6	v
Vidonicích	Vidonice	k1gFnPc6	Vidonice
u	u	k7c2	u
Pecky	pecka	k1gFnSc2	pecka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
z	z	k7c2	z
přehledu	přehled	k1gInSc2	přehled
výstav	výstava	k1gFnPc2	výstava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Janoušek	Janoušek	k1gMnSc1	Janoušek
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
a	a	k8xC	a
během	během	k7c2	během
následného	následný	k2eAgNnSc2d1	následné
období	období	k1gNnSc2	období
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
"	"	kIx"	"
ztratil	ztratit	k5eAaPmAgInS	ztratit
možnost	možnost	k1gFnSc4	možnost
vystavovat	vystavovat	k5eAaImF	vystavovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
už	už	k6eAd1	už
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
nemohl	moct	k5eNaImAgMnS	moct
představit	představit	k5eAaPmF	představit
širší	široký	k2eAgFnSc3d2	širší
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
nyní	nyní	k6eAd1	nyní
spravuje	spravovat	k5eAaImIp3nS	spravovat
Nadace	nadace	k1gFnSc1	nadace
Věry	Věra	k1gFnSc2	Věra
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Janouškových	Janoušková	k1gFnPc2	Janoušková
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Janoušek	Janoušek	k1gMnSc1	Janoušek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1986	[number]	k4	1986
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
vedle	vedle	k7c2	vedle
vynikajících	vynikající	k2eAgInPc2d1	vynikající
sochařských	sochařský	k2eAgInPc2d1	sochařský
portrétů	portrét	k1gInPc2	portrét
(	(	kIx(	(
<g/>
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
,	,	kIx,	,
Talich	Talich	k1gMnSc1	Talich
<g/>
)	)	kIx)	)
také	také	k9	také
reliéfy	reliéf	k1gInPc4	reliéf
a	a	k8xC	a
četné	četný	k2eAgFnPc1d1	četná
studie	studie	k1gFnPc1	studie
plastik	plastika	k1gFnPc2	plastika
pro	pro	k7c4	pro
začlenění	začlenění	k1gNnSc4	začlenění
do	do	k7c2	do
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sochař	sochař	k1gMnSc1	sochař
postupně	postupně	k6eAd1	postupně
opouštěl	opouštět	k5eAaImAgMnS	opouštět
tradiční	tradiční	k2eAgInPc4d1	tradiční
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
k	k	k7c3	k
zobrazení	zobrazení	k1gNnSc3	zobrazení
vztahu	vztah	k1gInSc2	vztah
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
plastiky	plastika	k1gFnSc2	plastika
kovové	kovový	k2eAgFnSc2d1	kovová
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
neurčité	určitý	k2eNgInPc1d1	neurčitý
obrysy	obrys	k1gInPc1	obrys
figur	figura	k1gFnPc2	figura
kladl	klást	k5eAaImAgMnS	klást
do	do	k7c2	do
kontrastu	kontrast	k1gInSc2	kontrast
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
interaktivní	interaktivní	k2eAgFnPc4d1	interaktivní
plastiky	plastika	k1gFnPc4	plastika
s	s	k7c7	s
mobilními	mobilní	k2eAgInPc7d1	mobilní
prvky	prvek	k1gInPc7	prvek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kyvadel	kyvadlo	k1gNnPc2	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
fázi	fáze	k1gFnSc6	fáze
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
sochař	sochař	k1gMnSc1	sochař
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
pohyblivé	pohyblivý	k2eAgFnSc3d1	pohyblivá
součásti	součást	k1gFnSc3	součást
prostorovou	prostorový	k2eAgFnSc7d1	prostorová
variabilitou	variabilita	k1gFnSc7	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnPc1d1	složená
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
plátů	plát	k1gInPc2	plát
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
spojené	spojený	k2eAgInPc4d1	spojený
šrouby	šroub	k1gInPc4	šroub
v	v	k7c6	v
paralelních	paralelní	k2eAgFnPc6d1	paralelní
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
upevněné	upevněný	k2eAgInPc1d1	upevněný
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
s	s	k7c7	s
imaginární	imaginární	k2eAgFnSc7d1	imaginární
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
zarámované	zarámovaný	k2eAgInPc1d1	zarámovaný
<g/>
,	,	kIx,	,
nabízejí	nabízet	k5eAaImIp3nP	nabízet
divákovi	divák	k1gMnSc3	divák
vlastní	vlastní	k2eAgFnSc2d1	vlastní
interpretace	interpretace	k1gFnSc2	interpretace
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
sochaře	sochař	k1gMnSc2	sochař
zapojit	zapojit	k5eAaPmF	zapojit
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
tvůrce	tvůrce	k1gMnSc4	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k8xS	jako
předloha	předloha	k1gFnSc1	předloha
pro	pro	k7c4	pro
zamýšlené	zamýšlený	k2eAgInPc4d1	zamýšlený
monumentální	monumentální	k2eAgInPc4d1	monumentální
projekty	projekt	k1gInPc4	projekt
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
díla	dílo	k1gNnSc2	dílo
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Janouška	Janoušek	k1gMnSc2	Janoušek
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gMnSc3	jeho
malby	malba	k1gFnPc4	malba
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
volná	volný	k2eAgFnSc1d1	volná
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
studie	studie	k1gFnPc1	studie
k	k	k7c3	k
sochám	socha	k1gFnPc3	socha
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
polychromovaným	polychromovaný	k2eAgMnPc3d1	polychromovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tušové	tušový	k2eAgFnPc1d1	tušová
kresby	kresba	k1gFnPc1	kresba
černých	černý	k2eAgFnPc2d1	černá
figur	figura	k1gFnPc2	figura
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
autora	autor	k1gMnSc4	autor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
paralelou	paralela	k1gFnSc7	paralela
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
variabilním	variabilní	k2eAgFnPc3d1	variabilní
plastikám	plastika	k1gFnPc3	plastika
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
–	–	k?	–
reliéf	reliéf	k1gInSc1	reliéf
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
módy	móda	k1gFnSc2	móda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1958	[number]	k4	1958
–	–	k?	–
Hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
pavilon	pavilon	k1gInSc1	pavilon
Expo	Expo	k1gNnSc1	Expo
58	[number]	k4	58
Brusel	Brusel	k1gInSc1	Brusel
1964	[number]	k4	1964
–	–	k?	–
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
plavecký	plavecký	k2eAgInSc1d1	plavecký
stadion	stadion	k1gInSc1	stadion
<g />
.	.	kIx.	.
</s>
<s>
Podolí	Podolí	k1gNnSc1	Podolí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
kašna	kašna	k1gFnSc1	kašna
<g/>
,	,	kIx,	,
Jiřský	jiřský	k2eAgInSc1d1	jiřský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1967	[number]	k4	1967
–	–	k?	–
Expo	Expo	k1gNnSc4	Expo
Montreal	Montreal	k1gInSc1	Montreal
1970	[number]	k4	1970
–	–	k?	–
Hrozba	hrozba	k1gFnSc1	hrozba
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Expo	Expo	k1gNnSc1	Expo
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
<g/>
,	,	kIx,	,
symbolický	symbolický	k2eAgInSc4d1	symbolický
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
Československa	Československo	k1gNnSc2	Československo
1971	[number]	k4	1971
–	–	k?	–
Krystal	krystal	k1gInSc1	krystal
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
nádvoří	nádvoří	k1gNnSc2	nádvoří
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Kladno-Kročehlavy	Kladno-Kročehlava	k1gFnSc2	Kladno-Kročehlava
1981	[number]	k4	1981
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgFnSc1d1	mobilní
plastika	plastika	k1gFnSc1	plastika
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
1967	[number]	k4	1967
–	–	k?	–
Kyvadla	kyvadlo	k1gNnSc2	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
–	–	k?	–
Sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
OG	OG	kA	OG
Liberec	Liberec	k1gInSc1	Liberec
(	(	kIx(	(
<g/>
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
–	–	k?	–
Obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Orlová	Orlová	k1gFnSc1	Orlová
1982	[number]	k4	1982
–	–	k?	–
Ústav	ústav	k1gInSc1	ústav
<g />
.	.	kIx.	.
</s>
<s>
makromolek	makromolek	k1gInSc1	makromolek
<g/>
.	.	kIx.	.
chemie	chemie	k1gFnSc2	chemie
AV	AV	kA	AV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
–	–	k?	–
Plastika	plastika	k1gFnSc1	plastika
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
galerie	galerie	k1gFnSc1	galerie
Stavoprojektu	Stavoprojekt	k1gInSc2	Stavoprojekt
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
výstava	výstava	k1gFnSc1	výstava
nepovolena	povolen	k2eNgFnSc1d1	nepovolena
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
–	–	k?	–
Poslední	poslední	k2eAgFnPc1d1	poslední
plastiky	plastika	k1gFnPc1	plastika
<g/>
,	,	kIx,	,
ÚKDŽ	ÚKDŽ	kA	ÚKDŽ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
–	–	k?	–
Kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Opatov	Opatov	k1gInSc1	Opatov
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
–	–	k?	–
GHMP	GHMP	kA	GHMP
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
u	u	k7c2	u
kamenného	kamenný	k2eAgInSc2d1	kamenný
zvonu	zvon	k1gInSc2	zvon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
–	–	k?	–
Poslední	poslední	k2eAgFnSc1d1	poslední
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Hořice	Hořice	k1gFnPc1	Hořice
1994	[number]	k4	1994
–	–	k?	–
Galerie	galerie	k1gFnSc2	galerie
na	na	k7c6	na
Pecce	pecka	k1gFnSc6	pecka
(	(	kIx(	(
<g/>
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
–	–	k?	–
Sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
koláže	koláž	k1gFnPc1	koláž
<g/>
,	,	kIx,	,
Turčianská	Turčianský	k2eAgNnPc1d1	Turčianský
galéria	galérium	k1gNnPc1	galérium
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
–	–	k?	–
Galerie	galerie	k1gFnSc1	galerie
Trigon	trigon	k1gInSc1	trigon
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
s	s	k7c7	s
Věrou	Věra	k1gFnSc7	Věra
Janouškovou	Janoušková	k1gFnSc7	Janoušková
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
–	–	k?	–
V.	V.	kA	V.
Janoušek	Janoušek	k1gMnSc1	Janoušek
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
OGV	OGV	kA	OGV
Jihlava	Jihlava	k1gFnSc1	Jihlava
1999	[number]	k4	1999
–	–	k?	–
Pohyb	pohyb	k1gInSc1	pohyb
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
plastice	plastika	k1gFnSc6	plastika
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Millenium	Millenium	k1gNnSc1	Millenium
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
–	–	k?	–
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
umění	umění	k1gNnSc4	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
1954	[number]	k4	1954
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
Biennale	Biennale	k6eAd1	Biennale
di	di	k?	di
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
1955	[number]	k4	1955
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
přehlídka	přehlídka	k1gFnSc1	přehlídka
Čs	čs	kA	čs
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Biennale	Biennale	k6eAd1	Biennale
internazionale	internazionale	k6eAd1	internazionale
di	di	k?	di
scultura	scultura	k1gFnSc1	scultura
<g/>
,	,	kIx,	,
Carrara	Carrara	k1gFnSc1	Carrara
1959	[number]	k4	1959
–	–	k?	–
Čs	čs	kA	čs
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
Kodaň	Kodaň	k1gFnSc1	Kodaň
1961	[number]	k4	1961
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
exposition	exposition	k1gInSc1	exposition
internationale	internationale	k6eAd1	internationale
de	de	k?	de
sculpture	sculptur	k1gMnSc5	sculptur
contemporaine	contemporain	k1gMnSc5	contemporain
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnPc3	Musée
Rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
1965	[number]	k4	1965
–	–	k?	–
Tschechoslowakische	Tschechoslowakisch	k1gInPc1	Tschechoslowakisch
Kunst	Kunst	k1gFnSc1	Kunst
heute	heute	k5eAaPmIp2nP	heute
<g/>
,	,	kIx,	,
Bochum	Bochum	k1gInSc1	Bochum
1966	[number]	k4	1966
–	–	k?	–
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
tendence	tendence	k1gFnSc1	tendence
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
–	–	k?	–
Tschechoslowakische	Tschechoslowakische	k1gInSc1	Tschechoslowakische
Plastik	plastik	k1gInSc1	plastik
von	von	k1gInSc1	von
1960	[number]	k4	1960
gegenwart	gegenwarta	k1gFnPc2	gegenwarta
<g/>
,	,	kIx,	,
Essen	Essen	k1gInSc1	Essen
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
1967	[number]	k4	1967
–	–	k?	–
Mostra	Mostrum	k1gNnSc2	Mostrum
d	d	k?	d
<g/>
́	́	k?	́
<g/>
arte	art	k1gFnSc2	art
contemporanea	contemporanea	k1gMnSc1	contemporanea
cecoslovaccha	cecoslovaccha	k1gMnSc1	cecoslovaccha
<g/>
,	,	kIx,	,
Turin	Turin	k2eAgMnSc1d1	Turin
1967	[number]	k4	1967
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Biennale	Biennale	k6eAd1	Biennale
<g/>
,	,	kIx,	,
Openluchtmuseum	Openluchtmuseum	k1gInSc1	Openluchtmuseum
<g/>
,	,	kIx,	,
Antwerpy	Antwerp	k1gInPc1	Antwerp
1968	[number]	k4	1968
–	–	k?	–
300	[number]	k4	300
malířů	malíř	k1gMnPc2	malíř
a	a	k8xC	a
sochařů	sochař	k1gMnPc2	sochař
pěti	pět	k4xCc2	pět
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
U	u	k7c2	u
Hybernů	hybern	k1gMnPc2	hybern
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Musée	Musée	k1gNnSc1	Musée
Rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
1969	[number]	k4	1969
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
figurace	figurace	k1gFnSc1	figurace
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
–	–	k?	–
Gallerie	Gallerie	k1gFnSc2	Gallerie
nazionale	nazionale	k6eAd1	nazionale
d	d	k?	d
<g/>
́	́	k?	́
<g/>
arte	artat	k5eAaPmIp3nS	artat
moderna	moderna	k1gFnSc1	moderna
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
1980	[number]	k4	1980
–	–	k?	–
Eleven	Elevna	k1gFnPc2	Elevna
contemporary	contemporar	k1gInPc1	contemporar
artists	artists	k6eAd1	artists
from	from	k6eAd1	from
Prague	Prague	k1gFnPc2	Prague
<g/>
,	,	kIx,	,
Univ	Univa	k1gFnPc2	Univa
<g/>
.	.	kIx.	.
</s>
<s>
NY	NY	kA	NY
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
1984	[number]	k4	1984
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
H	H	kA	H
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
1985	[number]	k4	1985
–	–	k?	–
Barevná	barevný	k2eAgFnSc1d1	barevná
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
H	H	kA	H
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
2017	[number]	k4	2017
GENERATION	GENERATION	kA	GENERATION
ONE	ONE	kA	ONE
:	:	kIx,	:
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
české	český	k2eAgFnSc2d1	Česká
postmoderny	postmoderna	k1gFnSc2	postmoderna
<g/>
,	,	kIx,	,
Zámecký	zámecký	k2eAgInSc1d1	zámecký
pivovar	pivovar	k1gInSc1	pivovar
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
–	–	k?	–
6.7	[number]	k4	6.7
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kurátoři	kurátor	k1gMnPc1	kurátor
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Železný	Železný	k1gMnSc1	Železný
a	a	k8xC	a
Federico	Federico	k1gMnSc1	Federico
Díaz	Díaz	k1gMnSc1	Díaz
<g/>
,	,	kIx,	,
pořáadatel	pořáadatel	k1gMnSc1	pořáadatel
<g/>
:	:	kIx,	:
BOHEMIAN	bohemian	k1gInSc1	bohemian
HERITAGE	HERITAGE	kA	HERITAGE
FUND	fund	k1gInSc1	fund
a	a	k8xC	a
Cermak	Cermak	k1gInSc1	Cermak
Eisenkraft	Eisenkraft	k2eAgInSc1d1	Eisenkraft
1970	[number]	k4	1970
–	–	k?	–
Dictionnaire	Dictionnair	k1gInSc5	Dictionnair
de	de	k?	de
la	la	k0	la
sculpture	sculptur	k1gMnSc5	sculptur
moderne	modernout	k5eAaPmIp3nS	modernout
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Mailard	Mailard	k1gMnSc1	Mailard
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Hazan	Hazan	k1gMnSc1	Hazan
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-8288-6551-7	[number]	k4	978-0-8288-6551-7
1979	[number]	k4	1979
–	–	k?	–
L	L	kA	L
<g/>
́	́	k?	́
<g/>
art	art	k?	art
aujourd	aujourd	k1gInSc1	aujourd
<g/>
́	́	k?	́
<g/>
hui	hui	k?	hui
an	an	k?	an
Tchecoslovaquie	Tchecoslovaquie	k1gFnSc2	Tchecoslovaquie
(	(	kIx(	(
<g/>
Bénamou	Bénama	k1gFnSc7	Bénama
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
,	,	kIx,	,
190	[number]	k4	190
s.	s.	k?	s.
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
78	[number]	k4	78
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Jazzová	jazzový	k2eAgFnSc1d1	jazzová
sekce	sekce	k1gFnSc1	sekce
(	(	kIx(	(
<g/>
sborník	sborník	k1gInSc1	sborník
"	"	kIx"	"
<g/>
Šedá	šedý	k2eAgFnSc1d1	šedá
cihla	cihla	k1gFnSc1	cihla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
322	[number]	k4	322
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samizdat	samizdat	k1gInSc1	samizdat
1986	[number]	k4	1986
–	–	k?	–
Sborník	sborník	k1gInSc4	sborník
k	k	k7c3	k
nedožitým	dožitý	k2eNgFnPc3d1	dožitý
65	[number]	k4	65
<g/>
.	.	kIx.	.
nar	nar	kA	nar
<g/>
.	.	kIx.	.
V.	V.	kA	V.
J.	J.	kA	J.
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kudlička	kudlička	k1gFnSc1	kudlička
<g/>
,	,	kIx,	,
J.	J.	kA	J.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
168	[number]	k4	168
s.	s.	k?	s.
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
–	–	k?	–
Výpovědi	výpověď	k1gFnSc2	výpověď
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
240	[number]	k4	240
s.	s.	k?	s.
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
80-7047-042-9	[number]	k4	80-7047-042-9
1994	[number]	k4	1994
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Chalupecký	Chalupecký	k2eAgMnSc1d1	Chalupecký
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Ars	Ars	k?	Ars
pictura	pictura	k1gFnSc1	pictura
<g/>
,	,	kIx,	,
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
173	[number]	k4	173
<g/>
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85787-81-4	[number]	k4	80-85787-81-4
1994	[number]	k4	1994
–	–	k?	–
Český	český	k2eAgInSc4d1	český
kinetismus	kinetismus	k1gInSc4	kinetismus
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
Hlaváček	hlaváček	k1gInSc4	hlaváček
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
kat	kat	k1gInSc4	kat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
–	–	k?	–
Proč	proč	k6eAd1	proč
to	ten	k3xDgNnSc4	ten
dělám	dělat	k5eAaImIp1nS	dělat
právě	právě	k9	právě
tak	tak	k6eAd1	tak
(	(	kIx(	(
<g/>
Zemina	zemina	k1gFnSc1	zemina
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Janoušek	Janoušek	k1gMnSc1	Janoušek
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Portal	Portal	k1gMnSc1	Portal
<g/>
,	,	kIx,	,
120	[number]	k4	120
s.	s.	k?	s.
<g/>
,	,	kIx,	,
čes.	čes.	k?	čes.
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
–	–	k?	–
České	český	k2eAgNnSc1d1	české
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
Bydžovská	Bydžovská	k1gFnSc1	Bydžovská
<g/>
,	,	kIx,	,
<g/>
L.	L.	kA	L.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
NG	NG	kA	NG
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
349	[number]	k4	349
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7035-095-4	[number]	k4	80-7035-095-4
1995	[number]	k4	1995
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
A-M	A-M	k1gFnSc1	A-M
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Horová	Horová	k1gFnSc1	Horová
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
548	[number]	k4	548
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0521-8	[number]	k4	80-200-0521-8
1996	[number]	k4	1996
–	–	k?	–
Umění	umění	k1gNnSc1	umění
zastaveného	zastavený	k2eAgInSc2d1	zastavený
času	čas	k1gInSc2	čas
/	/	kIx~	/
Art	Art	k1gFnSc1	Art
when	whena	k1gFnPc2	whena
Time	Time	k1gFnPc2	Time
stood	stood	k1gInSc1	stood
still	still	k1gMnSc1	still
(	(	kIx(	(
<g/>
Beran	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kat	kat	k1gMnSc1	kat
<g/>
.	.	kIx.	.
</s>
<s>
ČMVU	ČMVU	kA	ČMVU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
268	[number]	k4	268
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7056-050-9	[number]	k4	80-7056-050-9
1998	[number]	k4	1998
–	–	k?	–
České	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
GHMP	GHMP	kA	GHMP
(	(	kIx(	(
<g/>
Bregantová	Bregantový	k2eAgFnSc1d1	Bregantová
<g/>
,	,	kIx,	,
P.	P.	kA	P.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kat	kat	k1gMnSc1	kat
<g/>
.	.	kIx.	.
</s>
<s>
GHMP	GHMP	kA	GHMP
<g/>
,	,	kIx,	,
280	[number]	k4	280
s.	s.	k?	s.
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
80-7010-057-5	[number]	k4	80-7010-057-5
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
překlad	překlad	k1gInSc1	překlad
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7010	[number]	k4	7010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
65	[number]	k4	65
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
–	–	k?	–
Slovník	slovník	k1gInSc1	slovník
čes.	čes.	k?	čes.
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ch-J	Ch-J	k?	Ch-J
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Výtvar	výtvar	k1gInSc1	výtvar
<g/>
.	.	kIx.	.
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
341	[number]	k4	341
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86171-04-3	[number]	k4	80-86171-04-3
2004	[number]	k4	2004
–	–	k?	–
Šedesátá	šedesátý	k4xOgNnPc1	šedesátý
/	/	kIx~	/
The	The	k1gFnSc1	The
sixties	sixties	k1gMnSc1	sixties
(	(	kIx(	(
<g/>
Juříková	Juříková	k1gFnSc1	Juříková
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
,	,	kIx,	,
gal	gal	k?	gal
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
414	[number]	k4	414
<g/>
.	.	kIx.	.
s	s	k7c7	s
<g/>
,	,	kIx,	,
čes.	čes.	k?	čes.
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
80-239-3406-6	[number]	k4	80-239-3406-6
2006	[number]	k4	2006
–	–	k?	–
UB	UB	kA	UB
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
Studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc1	dokument
(	(	kIx(	(
<g/>
Slavická	Slavická	k1gFnSc1	Slavická
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Šetlík	Šetlík	k1gMnSc1	Šetlík
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Gallery	Galler	k1gInPc1	Galler
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
337	[number]	k4	337
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86990-07-9	[number]	k4	80-86990-07-9
2008	[number]	k4	2008
–	–	k?	–
Nechci	chtít	k5eNaImIp1nS	chtít
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
/	/	kIx~	/
No	no	k9	no
cage	cage	k6eAd1	cage
for	forum	k1gNnPc2	forum
me	me	k?	me
(	(	kIx(	(
<g/>
Bieleszová-Müllerová	Bieleszová-Müllerová	k1gFnSc1	Bieleszová-Müllerová
<g/>
,	,	kIx,	,
Š.	Š.	kA	Š.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kat	kat	k1gMnSc1	kat
<g/>
.	.	kIx.	.
</s>
<s>
MU	MU	kA	MU
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
247	[number]	k4	247
s.	s.	k?	s.
<g/>
,	,	kIx,	,
čes.	čes.	k?	čes.
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87149-04-1	[number]	k4	978-80-87149-04-1
2010	[number]	k4	2010
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
koláž	koláž	k1gFnSc1	koláž
(	(	kIx(	(
<g/>
Machalický	Machalický	k2eAgInSc1d1	Machalický
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
254	[number]	k4	254
<g/>
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Gallery	Galler	k1gInPc1	Galler
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
–	–	k?	–
Via	via	k7c4	via
Artis	Artis	k1gFnSc4	Artis
Via	via	k7c4	via
Vitae	Vitae	k1gFnSc4	Vitae
(	(	kIx(	(
<g/>
bibliografie	bibliografie	k1gFnSc1	bibliografie
J.	J.	kA	J.
Zeminy	zemina	k1gFnPc1	zemina
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Bregantová	Bregantový	k2eAgFnSc1d1	Bregantová
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
997	[number]	k4	997
<g/>
s.	s.	k?	s.
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
978-80-7215-403-6	[number]	k4	978-80-7215-403-6
</s>
