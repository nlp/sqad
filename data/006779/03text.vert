<s>
Paul	Paul	k1gMnSc1	Paul
Kevin	Kevin	k1gMnSc1	Kevin
Jonas	Jonas	k1gMnSc1	Jonas
II	II	kA	II
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1987	[number]	k4	1987
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Kevin	Kevin	k1gMnSc1	Kevin
Jonas	Jonas	k1gMnSc1	Jonas
nebo	nebo	k8xC	nebo
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Denisa	Denisa	k1gFnSc1	Denisa
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothers	k1gInSc1	Brothers
<g/>
,	,	kIx,	,
pop	pop	k1gMnSc1	pop
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
mladšími	mladý	k2eAgMnPc7d2	mladší
bratry	bratr	k1gMnPc7	bratr
Joem	Joemo	k1gNnPc2	Joemo
a	a	k8xC	a
Nickem	Nicek	k1gInSc7	Nicek
<g/>
.	.	kIx.	.
</s>
<s>
Kevin	Kevin	k1gMnSc1	Kevin
Jonas	Jonas	k1gMnSc1	Jonas
původně	původně	k6eAd1	původně
působil	působit	k5eAaImAgMnS	působit
v	v	k7c4	v
Jonas	Jonas	k1gInSc4	Jonas
Brothers	Brothersa	k1gFnPc2	Brothersa
jako	jako	k8xS	jako
sólový	sólový	k2eAgMnSc1d1	sólový
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
jej	on	k3xPp3gInSc4	on
svými	svůj	k3xOyFgInPc7	svůj
vokály	vokál	k1gInPc7	vokál
pouze	pouze	k6eAd1	pouze
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
hudebnímu	hudební	k2eAgMnSc3d1	hudební
producentovi	producent	k1gMnSc3	producent
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
hlasy	hlas	k1gInPc1	hlas
líbily	líbit	k5eAaImAgInP	líbit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
třemi	tři	k4xCgInPc7	tři
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Camp	camp	k1gInSc1	camp
Rock	rock	k1gInSc1	rock
či	či	k8xC	či
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
J.O.	J.O.	k1gFnSc2	J.O.
<g/>
N.	N.	kA	N.
<g/>
A.S.	A.S.	k1gMnSc2	A.S.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
ženu	hnát	k5eAaImIp1nS	hnát
jménem	jméno	k1gNnSc7	jméno
Danielle	Daniell	k1gMnSc2	Daniell
Deleasa	Deleas	k1gMnSc2	Deleas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
koníčky	koníček	k1gInPc4	koníček
patří	patřit	k5eAaImIp3nS	patřit
bowling	bowling	k1gInSc1	bowling
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
je	být	k5eAaImIp3nS	být
evangelický	evangelický	k2eAgMnSc1d1	evangelický
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c6	v
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kevin	Kevin	k1gMnSc1	Kevin
Jonas	Jonas	k1gMnSc1	Jonas
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kevin	Kevin	k2eAgInSc4d1	Kevin
Jonas	Jonas	k1gInSc4	Jonas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothersa	k1gFnPc2	Brothersa
</s>
