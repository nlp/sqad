<s>
Obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
jsem	být	k5eAaImIp1nS	být
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Bohumila	Bohumil	k1gMnSc2	Bohumil
Hrabala	Hrabal	k1gMnSc2	Hrabal
režisérem	režisér	k1gMnSc7	režisér
Jiřím	Jiří	k1gMnSc7	Jiří
Menzelem	Menzel	k1gMnSc7	Menzel
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
ocenění	ocenění	k1gNnSc4	ocenění
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
Státním	státní	k2eAgInSc7d1	státní
fondem	fond	k1gInSc7	fond
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Začínající	začínající	k2eAgMnSc1d1	začínající
číšník	číšník	k1gMnSc1	číšník
Jan	Jan	k1gMnSc1	Jan
Dítě	Dítě	k1gMnSc1	Dítě
má	mít	k5eAaImIp3nS	mít
komplexy	komplex	k1gInPc4	komplex
kvůli	kvůli	k7c3	kvůli
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
prodeje	prodej	k1gInSc2	prodej
párků	párek	k1gInPc2	párek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ale	ale	k9	ale
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
Tichota	tichota	k1gFnSc1	tichota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
významní	významný	k2eAgMnPc1d1	významný
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
milenkami	milenka	k1gFnPc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
vzorem	vzor	k1gInSc7	vzor
vrchní	vrchní	k2eAgMnSc1d1	vrchní
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zná	znát	k5eAaImIp3nS	znát
obdivuhodně	obdivuhodně	k6eAd1	obdivuhodně
psychologii	psychologie	k1gFnSc4	psychologie
a	a	k8xC	a
sociologii	sociologie	k1gFnSc4	sociologie
hostů	host	k1gMnPc2	host
–	–	k?	–
říkal	říkat	k5eAaImAgInS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Janovi	Jan	k1gMnSc3	Jan
se	se	k3xPyFc4	se
poštěstí	poštěstit	k5eAaPmIp3nS	poštěstit
a	a	k8xC	a
jako	jako	k9	jako
záskok	záskok	k1gInSc1	záskok
mohl	moct	k5eAaImAgInS	moct
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
habešského	habešský	k2eAgMnSc4d1	habešský
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgNnSc2	který
dostal	dostat	k5eAaPmAgInS	dostat
habešský	habešský	k2eAgInSc1d1	habešský
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Němku	Němka	k1gFnSc4	Němka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukradla	ukradnout	k5eAaPmAgFnS	ukradnout
deportovaným	deportovaný	k2eAgMnPc3d1	deportovaný
Židům	Žid	k1gMnPc3	Žid
cenné	cenný	k2eAgFnPc4d1	cenná
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
však	však	k9	však
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
známky	známka	k1gFnPc4	známka
zachránit	zachránit	k5eAaPmF	zachránit
z	z	k7c2	z
hořící	hořící	k2eAgFnSc2d1	hořící
budovy	budova	k1gFnSc2	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
hotelu	hotel	k1gInSc2	hotel
Tichota	tichota	k1gFnSc1	tichota
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
únorem	únor	k1gInSc7	únor
1948	[number]	k4	1948
si	se	k3xPyFc3	se
koupil	koupit	k5eAaPmAgMnS	koupit
za	za	k7c4	za
utržené	utržený	k2eAgInPc4d1	utržený
peníze	peníz	k1gInPc4	peníz
za	za	k7c2	za
známky	známka	k1gFnSc2	známka
hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Penězmi	Peněz	k1gFnPc7	Peněz
si	se	k3xPyFc3	se
i	i	k9	i
vytapetoval	vytapetovat	k5eAaPmAgMnS	vytapetovat
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
hotel	hotel	k1gInSc1	hotel
sebrán	sebrán	k2eAgInSc1d1	sebrán
a	a	k8xC	a
milionáři	milionář	k1gMnPc1	milionář
byli	být	k5eAaImAgMnP	být
sváženi	svážet	k5eAaImNgMnP	svážet
do	do	k7c2	do
komunistických	komunistický	k2eAgFnPc2d1	komunistická
vězení	vězení	k1gNnPc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
tam	tam	k6eAd1	tam
však	však	k9	však
odešel	odejít	k5eAaPmAgMnS	odejít
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
společnosti	společnost	k1gFnSc6	společnost
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
i	i	k8xC	i
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kriminále	kriminál	k1gInSc6	kriminál
ale	ale	k8xC	ale
potká	potkat	k5eAaPmIp3nS	potkat
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
dříve	dříve	k6eAd2	dříve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
a	a	k8xC	a
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
ambicím	ambice	k1gFnPc3	ambice
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
otočil	otočit	k5eAaPmAgMnS	otočit
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ho	on	k3xPp3gInSc4	on
teď	teď	k6eAd1	teď
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
nepřijmou	přijmout	k5eNaPmIp3nP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
dostal	dostat	k5eAaPmAgMnS	dostat
Dítě	Dítě	k1gMnSc1	Dítě
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
natvrdo	natvrdo	k6eAd1	natvrdo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
amnestii	amnestie	k1gFnSc3	amnestie
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
po	po	k7c6	po
14	[number]	k4	14
letech	let	k1gInPc6	let
a	a	k8xC	a
9	[number]	k4	9
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
cestář	cestář	k1gMnSc1	cestář
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
v	v	k7c6	v
liduprázdných	liduprázdný	k2eAgInPc6d1	liduprázdný
Sudetech	Sudety	k1gInPc6	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
,	,	kIx,	,
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
i	i	k9	i
za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
podlézal	podlézat	k5eAaImAgMnS	podlézat
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Pochopí	pochopit	k5eAaPmIp3nS	pochopit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
se	se	k3xPyFc4	se
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
se	se	k3xPyFc4	se
smíří	smířit	k5eAaPmIp3nS	smířit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
i	i	k8xC	i
zlatý	zlatý	k2eAgInSc1d1	zlatý
jídelní	jídelní	k2eAgInSc1d1	jídelní
servis	servis	k1gInSc1	servis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostal	dostat	k5eAaPmAgInS	dostat
rakouský	rakouský	k2eAgMnSc1d1	rakouský
kníže	kníže	k1gMnSc1	kníže
Klemens	Klemensa	k1gFnPc2	Klemensa
von	von	k1gInSc4	von
Metternich	Metternich	k1gInSc4	Metternich
darem	dar	k1gInSc7	dar
od	od	k7c2	od
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vyslanec	vyslanec	k1gMnSc1	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
servis	servis	k1gInSc1	servis
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
z	z	k7c2	z
hotelu	hotel	k1gInSc2	hotel
Tichota	tichota	k1gFnSc1	tichota
stala	stát	k5eAaPmAgFnS	stát
farma	farma	k1gFnSc1	farma
na	na	k7c4	na
chov	chov	k1gInSc4	chov
lidí	člověk	k1gMnPc2	člověk
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgFnSc2d1	čistá
rasy	rasa	k1gFnSc2	rasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
farmy	farma	k1gFnPc1	farma
skutečně	skutečně	k6eAd1	skutečně
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
,	,	kIx,	,
zřizoval	zřizovat	k5eAaImAgMnS	zřizovat
je	on	k3xPp3gNnSc4	on
vůdce	vůdce	k1gMnSc1	vůdce
SS	SS	kA	SS
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
.	.	kIx.	.
</s>
<s>
Mimochodem	mimochodem	k9	mimochodem
-	-	kIx~	-
lékař	lékař	k1gMnSc1	lékař
z	z	k7c2	z
farmy	farma	k1gFnSc2	farma
jako	jako	k8xS	jako
Himmler	Himmler	k1gMnSc1	Himmler
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
polidští	polidštit	k5eAaPmIp3nP	polidštit
<g/>
,	,	kIx,	,
když	když	k8xS	když
začíná	začínat	k5eAaImIp3nS	začínat
ztroskotávat	ztroskotávat	k5eAaImF	ztroskotávat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vykolejen	vykolejen	k2eAgMnSc1d1	vykolejen
<g/>
,	,	kIx,	,
zbaven	zbaven	k2eAgMnSc1d1	zbaven
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
pořádku	pořádek	k1gInSc2	pořádek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
-	-	kIx~	-
monolog	monolog	k1gInSc1	monolog
Dítěte	Dítě	k1gMnSc2	Dítě
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
v	v	k7c6	v
Sudetech	Sudety	k1gInPc6	Sudety
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
popis	popis	k1gInSc1	popis
filmu	film	k1gInSc2	film
v	v	k7c6	v
ČFN	ČFN	kA	ČFN
</s>
