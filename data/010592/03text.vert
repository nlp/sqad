<p>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
davu	dav	k1gInSc2	dav
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
knihou	kniha	k1gFnSc7	kniha
francouzského	francouzský	k2eAgMnSc2d1	francouzský
sociologa	sociolog	k1gMnSc2	sociolog
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Gustava	Gustav	k1gMnSc2	Gustav
le	le	k?	le
Bona	bona	k1gFnSc1	bona
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Bon	bon	k1gInSc1	bon
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
popsal	popsat	k5eAaPmAgMnS	popsat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
součástí	součást	k1gFnSc7	součást
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
v	v	k7c6	v
davu	dav	k1gInSc6	dav
se	se	k3xPyFc4	se
neřídí	řídit	k5eNaImIp3nS	řídit
vlastním	vlastní	k2eAgNnSc7d1	vlastní
svědomím	svědomí	k1gNnSc7	svědomí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dělají	dělat	k5eAaImIp3nP	dělat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k9	co
ostatní	ostatní	k2eAgMnSc1d1	ostatní
(	(	kIx(	(
<g/>
novějším	nový	k2eAgInSc7d2	novější
termínem	termín	k1gInSc7	termín
řečeno	říct	k5eAaPmNgNnS	říct
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
konformní	konformní	k2eAgFnSc4d1	konformní
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
mnohem	mnohem	k6eAd1	mnohem
snáze	snadno	k6eAd2	snadno
ovlivnitelní	ovlivnitelný	k2eAgMnPc1d1	ovlivnitelný
nějakým	nějaký	k3yIgMnSc7	nějaký
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
o	o	k7c4	o
několik	několik	k4yIc4	několik
stádií	stádium	k1gNnPc2	stádium
civilizačního	civilizační	k2eAgInSc2d1	civilizační
vývoje	vývoj	k1gInSc2	vývoj
níže	níže	k1gFnSc2	níže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
velice	velice	k6eAd1	velice
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
(	(	kIx(	(
<g/>
na	na	k7c6	na
přebalu	přebal	k1gInSc6	přebal
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
rád	rád	k6eAd1	rád
četl	číst	k5eAaImAgMnS	číst
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xC	jako
překonaná	překonaný	k2eAgFnSc1d1	překonaná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgFnPc1d1	mnohá
moderní	moderní	k2eAgFnPc1d1	moderní
sociálně-psychologické	sociálněsychologický	k2eAgFnPc1d1	sociálně-psychologická
teorie	teorie	k1gFnPc1	teorie
pouze	pouze	k6eAd1	pouze
rozpracovávají	rozpracovávat	k5eAaImIp3nP	rozpracovávat
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
načrtnuté	načrtnutý	k2eAgInPc4d1	načrtnutý
postřehy	postřeh	k1gInPc4	postřeh
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
a	a	k8xC	a
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
le	le	k?	le
Bona	bona	k1gFnSc1	bona
je	být	k5eAaImIp3nS	být
současnost	současnost	k1gFnSc4	současnost
(	(	kIx(	(
<g/>
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
obdobím	období	k1gNnSc7	období
davů	dav	k1gInPc2	dav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
historické	historický	k2eAgInPc4d1	historický
převraty	převrat	k1gInPc4	převrat
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
tkví	tkvět	k5eAaImIp3nS	tkvět
dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
myšlení	myšlení	k1gNnSc2	myšlení
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
významné	významný	k2eAgFnPc1d1	významná
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
jen	jen	k9	jen
viditelnými	viditelný	k2eAgInPc7d1	viditelný
výsledky	výsledek	k1gInPc7	výsledek
takovýchto	takovýto	k3xDgFnPc2	takovýto
revolučních	revoluční	k2eAgFnPc2d1	revoluční
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
myslích	mysl	k1gFnPc6	mysl
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
jako	jako	k8xS	jako
období	období	k1gNnSc4	období
právě	právě	k9	právě
takovýchto	takovýto	k3xDgFnPc2	takovýto
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
dvěma	dva	k4xCgInPc7	dva
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rozvrat	rozvrat	k1gInSc1	rozvrat
náboženských	náboženský	k2eAgFnPc2d1	náboženská
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
či	či	k8xC	či
politických	politický	k2eAgFnPc2d1	politická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgMnPc6	který
stojí	stát	k5eAaImIp3nP	stát
naše	náš	k3xOp1gFnPc4	náš
civilizace	civilizace	k1gFnPc4	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
neméně	málo	k6eNd2	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
důvodem	důvod	k1gInSc7	důvod
jsou	být	k5eAaImIp3nP	být
objektivní	objektivní	k2eAgFnPc1d1	objektivní
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
novými	nový	k2eAgInPc7d1	nový
technologickými	technologický	k2eAgInPc7d1	technologický
objevy	objev	k1gInPc7	objev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
neměl	mít	k5eNaImAgInS	mít
společenský	společenský	k2eAgInSc1d1	společenský
názor	názor	k1gInSc1	názor
zpravidla	zpravidla	k6eAd1	zpravidla
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autorově	autorův	k2eAgFnSc6d1	autorova
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
však	však	k9	však
mínění	mínění	k1gNnSc1	mínění
davů	dav	k1gInPc2	dav
stává	stávat	k5eAaImIp3nS	stávat
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
historického	historický	k2eAgNnSc2d1	historické
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
tradice	tradice	k1gFnPc1	tradice
či	či	k8xC	či
osobní	osobní	k2eAgInPc1d1	osobní
názory	názor	k1gInPc1	názor
vládců	vládce	k1gMnPc2	vládce
znamenají	znamenat	k5eAaImIp3nP	znamenat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
davu	dav	k1gInSc2	dav
nabývá	nabývat	k5eAaImIp3nS	nabývat
převahy	převaha	k1gFnPc4	převaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
davů	dav	k1gInPc2	dav
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
podává	podávat	k5eAaImIp3nS	podávat
autor	autor	k1gMnSc1	autor
obecnou	obecný	k2eAgFnSc4d1	obecná
charakteristiku	charakteristika	k1gFnSc4	charakteristika
davů	dav	k1gInPc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
má	mít	k5eAaImIp3nS	mít
shromáždění	shromáždění	k1gNnSc2	shromáždění
lidí	člověk	k1gMnPc2	člověk
nové	nový	k2eAgFnSc2d1	nová
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
vlastností	vlastnost	k1gFnPc2	vlastnost
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
–	–	k?	–
taková	takový	k3xDgNnPc4	takový
shromáždění	shromáždění	k1gNnSc3	shromáždění
le	le	k?	le
Bon	bon	k1gInSc1	bon
nazývá	nazývat	k5eAaImIp3nS	nazývat
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc1	osobnost
každého	každý	k3xTgMnSc2	každý
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
je	být	k5eAaImIp3nS	být
jakoby	jakoby	k8xS	jakoby
potřena	potřen	k2eAgFnSc1d1	potřena
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
uvědomělé	uvědomělý	k2eAgFnSc2d1	uvědomělá
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
orientace	orientace	k1gFnSc2	orientace
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
citů	cit	k1gInPc2	cit
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
atributy	atribut	k1gInPc7	atribut
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
postřehem	postřeh	k1gInSc7	postřeh
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostorová	prostorový	k2eAgFnSc1d1	prostorová
sounáležitost	sounáležitost	k1gFnSc1	sounáležitost
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
a	a	k8xC	a
ani	ani	k8xC	ani
postačující	postačující	k2eAgMnSc1d1	postačující
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
davu	dav	k1gInSc2	dav
(	(	kIx(	(
<g/>
davem	dav	k1gInSc7	dav
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
čtenáři	čtenář	k1gMnPc1	čtenář
stejných	stejný	k2eAgFnPc2d1	stejná
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
rozhlas	rozhlas	k1gInSc1	rozhlas
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgMnS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kolektivní	kolektivní	k2eAgFnSc6d1	kolektivní
duši	duše	k1gFnSc6	duše
se	se	k3xPyFc4	se
stírají	stírat	k5eAaImIp3nP	stírat
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
individualita	individualita	k1gFnSc1	individualita
<g/>
.	.	kIx.	.
</s>
<s>
Jaké	jaký	k3yRgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
příčiny	příčina	k1gFnPc4	příčina
této	tento	k3xDgFnSc2	tento
proměny	proměna	k1gFnSc2	proměna
jedince	jedinec	k1gMnSc2	jedinec
<g/>
?	?	kIx.	?
</s>
<s>
Vědomé	vědomý	k2eAgInPc1d1	vědomý
činy	čin	k1gInPc1	čin
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
neuvědomělého	uvědomělý	k2eNgInSc2d1	neuvědomělý
podkladu	podklad	k1gInSc2	podklad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
utvářen	utvářit	k5eAaPmNgInS	utvářit
dědičnými	dědičný	k2eAgInPc7d1	dědičný
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podklad	podklad	k1gInSc1	podklad
tvoří	tvořit	k5eAaImIp3nS	tvořit
duši	duše	k1gFnSc4	duše
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Davům	Dav	k1gInPc3	Dav
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
společné	společný	k2eAgFnPc1d1	společná
právě	právě	k9	právě
tyto	tento	k3xDgFnPc1	tento
obecné	obecná	k1gFnPc1	obecná
<g/>
,	,	kIx,	,
vrozené	vrozený	k2eAgFnPc1d1	vrozená
charakterové	charakterový	k2eAgFnPc4d1	charakterová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
řízené	řízený	k2eAgFnPc4d1	řízená
nevědomím	nevědomí	k1gNnSc7	nevědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
nabývá	nabývat	k5eAaImIp3nS	nabývat
již	již	k6eAd1	již
svou	svůj	k3xOyFgFnSc7	svůj
příslušností	příslušnost	k1gFnSc7	příslušnost
k	k	k7c3	k
davu	dav	k1gInSc3	dav
pocitu	pocit	k1gInSc2	pocit
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
povolit	povolit	k5eAaPmF	povolit
pudům	pud	k1gInPc3	pud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
by	by	kYmCp3nS	by
sám	sám	k3xTgMnSc1	sám
musel	muset	k5eAaImAgMnS	muset
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
je	být	k5eAaImIp3nS	být
anonymní	anonymní	k2eAgMnSc1d1	anonymní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nezodpovědný	zodpovědný	k2eNgMnSc1d1	nezodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
Jedinec	jedinec	k1gMnSc1	jedinec
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
davu	dav	k1gInSc6	dav
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
...	...	k?	...
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
civilizace	civilizace	k1gFnSc2	civilizace
o	o	k7c4	o
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
níže	nízce	k6eAd2	nízce
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
pudovou	pudový	k2eAgFnSc7d1	pudová
bytostí	bytost	k1gFnSc7	bytost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
také	také	k9	také
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
nestálost	nestálost	k1gFnSc4	nestálost
davu	dav	k1gInSc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Srovnává	srovnávat	k5eAaImIp3nS	srovnávat
davy	dav	k1gInPc4	dav
s	s	k7c7	s
bytostmi	bytost	k1gFnPc7	bytost
na	na	k7c6	na
nižším	nízký	k2eAgInSc6d2	nižší
stupni	stupeň	k1gInSc6	stupeň
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
s	s	k7c7	s
divochy	divoch	k1gMnPc7	divoch
či	či	k8xC	či
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
textu	text	k1gInSc2	text
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
dosti	dosti	k6eAd1	dosti
nekorektní	korektní	k2eNgNnSc4d1	nekorektní
přirovnání	přirovnání	k1gNnSc4	přirovnání
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
:	:	kIx,	:
"	"	kIx"	"
...	...	k?	...
davy	dav	k1gInPc1	dav
neznají	neznat	k5eAaImIp3nP	neznat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
ani	ani	k8xC	ani
nejistotu	nejistota	k1gFnSc4	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Zacházejí	zacházet	k5eAaImIp3nP	zacházet
hned	hned	k6eAd1	hned
do	do	k7c2	do
krajnosti	krajnost	k1gFnSc2	krajnost
právě	právě	k6eAd1	právě
jako	jako	k8xC	jako
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
vyslovené	vyslovený	k2eAgNnSc1d1	vyslovené
podezření	podezření	k1gNnSc1	podezření
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
okamžitě	okamžitě	k6eAd1	okamžitě
nepopiratelnou	popiratelný	k2eNgFnSc7d1	nepopiratelná
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
počínající	počínající	k2eAgFnSc1d1	počínající
antipatie	antipatie	k1gFnSc1	antipatie
nebo	nebo	k8xC	nebo
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
...	...	k?	...
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
u	u	k7c2	u
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
davu	dav	k1gInSc6	dav
ihned	ihned	k6eAd1	ihned
v	v	k7c4	v
divokou	divoký	k2eAgFnSc4d1	divoká
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Názory	názor	k1gInPc1	názor
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
davů	dav	k1gInPc2	dav
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
knihy	kniha	k1gFnSc2	kniha
autor	autor	k1gMnSc1	autor
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hlavní	hlavní	k2eAgInPc1d1	hlavní
faktory	faktor	k1gInPc1	faktor
působící	působící	k2eAgInPc1d1	působící
nepřímo	přímo	k6eNd1	přímo
na	na	k7c4	na
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
a	a	k8xC	a
názory	názor	k1gInPc4	názor
davů	dav	k1gInPc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
hlavní	hlavní	k2eAgInPc4d1	hlavní
faktory	faktor	k1gInPc4	faktor
považuje	považovat	k5eAaImIp3nS	považovat
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc4	instituce
a	a	k8xC	a
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
zabývá	zabývat	k5eAaImIp3nS	zabývat
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
působí	působit	k5eAaImIp3nP	působit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
názory	názor	k1gInPc4	názor
davů	dav	k1gInPc2	dav
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnPc7	vůdce
davů	dav	k1gInPc2	dav
a	a	k8xC	a
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
používají	používat	k5eAaImIp3nP	používat
a	a	k8xC	a
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
a	a	k8xC	a
proměnlivostí	proměnlivost	k1gFnPc2	proměnlivost
názorů	názor	k1gInPc2	názor
davů	dav	k1gInPc2	dav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Faktory	faktor	k1gInPc1	faktor
působící	působící	k2eAgInPc1d1	působící
nepřímo	přímo	k6eNd1	přímo
na	na	k7c6	na
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
a	a	k8xC	a
názory	názor	k1gInPc1	názor
davů	dav	k1gInPc2	dav
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zapříčiňují	zapříčiňovat	k5eAaImIp3nP	zapříčiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dav	dav	k1gInSc1	dav
může	moct	k5eAaImIp3nS	moct
přijmout	přijmout	k5eAaPmF	přijmout
určité	určitý	k2eAgNnSc4d1	určité
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc4	některý
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nemají	mít	k5eNaImIp3nP	mít
vůbec	vůbec	k9	vůbec
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
výchovu	výchova	k1gFnSc4	výchova
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
nové	nový	k2eAgFnPc4d1	nová
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rasa	rasa	k1gFnSc1	rasa
====	====	k?	====
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Davy	Dav	k1gInPc1	Dav
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgNnSc4d1	odlišné
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
dílčí	dílčí	k2eAgInSc4d1	dílčí
dav	dav	k1gInSc4	dav
určitého	určitý	k2eAgInSc2d1	určitý
národa	národ	k1gInSc2	národ
či	či	k8xC	či
rasy	rasa	k1gFnSc2	rasa
zapůsobí	zapůsobit	k5eAaPmIp3nS	zapůsobit
silněji	silně	k6eAd2	silně
či	či	k8xC	či
slaběji	slabo	k6eAd2	slabo
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tradice	tradice	k1gFnSc1	tradice
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ideje	idea	k1gFnPc4	idea
<g/>
,	,	kIx,	,
potřeby	potřeba	k1gFnPc4	potřeba
a	a	k8xC	a
city	cit	k1gInPc4	cit
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
řídí	řídit	k5eAaImIp3nS	řídit
národy	národ	k1gInPc4	národ
a	a	k8xC	a
bez	bez	k7c2	bez
nich	on	k3xPp3gFnPc2	on
by	by	kYmCp3nS	by
žádná	žádný	k3yNgFnSc1	žádný
civilizace	civilizace	k1gFnSc1	civilizace
nemohla	moct	k5eNaImAgFnS	moct
fungovat	fungovat	k5eAaImF	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
uchovateli	uchovatel	k1gMnPc7	uchovatel
jsou	být	k5eAaImIp3nP	být
davy	dav	k1gInPc1	dav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
snaží	snažit	k5eAaImIp3nP	snažit
staré	starý	k2eAgFnPc1d1	stará
konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
tradice	tradice	k1gFnPc1	tradice
zachovat	zachovat	k5eAaPmF	zachovat
a	a	k8xC	a
neměnit	měnit	k5eNaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
tradic	tradice	k1gFnPc2	tradice
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
po	po	k7c6	po
sobě	se	k3xPyFc3	se
určitý	určitý	k2eAgInSc1d1	určitý
národ	národ	k1gInSc1	národ
nezanechal	zanechat	k5eNaPmAgInS	zanechat
hluboké	hluboký	k2eAgInPc4d1	hluboký
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
časem	časem	k6eAd1	časem
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
odbourání	odbourání	k1gNnSc3	odbourání
starých	starý	k2eAgFnPc2d1	stará
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
vývoji	vývoj	k1gInSc3	vývoj
tradic	tradice	k1gFnPc2	tradice
nových	nový	k2eAgFnPc2d1	nová
či	či	k8xC	či
znovuobnovení	znovuobnovení	k1gNnSc6	znovuobnovení
ztracených	ztracený	k2eAgFnPc2d1	ztracená
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Čas	čas	k1gInSc1	čas
====	====	k?	====
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
hybatel	hybatel	k1gMnSc1	hybatel
a	a	k8xC	a
příčina	příčina	k1gFnSc1	příčina
vývoje	vývoj	k1gInSc2	vývoj
i	i	k8xC	i
konce	konec	k1gInSc2	konec
všech	všecek	k3xTgNnPc2	všecek
přesvědčení	přesvědčení	k1gNnPc2	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
všechny	všechen	k3xTgInPc4	všechen
názory	názor	k1gInPc4	názor
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
z	z	k7c2	z
předchozích	předchozí	k2eAgNnPc2d1	předchozí
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
nové	nový	k2eAgFnPc1d1	nová
ideje	idea	k1gFnPc1	idea
davů	dav	k1gInPc2	dav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
sociální	sociální	k2eAgFnPc1d1	sociální
instituce	instituce	k1gFnPc1	instituce
====	====	k?	====
</s>
</p>
<p>
<s>
Osud	osud	k1gInSc1	osud
a	a	k8xC	a
směr	směr	k1gInSc1	směr
národa	národ	k1gInSc2	národ
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgMnS	založit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
povaze	povaha	k1gFnSc6	povaha
(	(	kIx(	(
<g/>
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
rasy	rasa	k1gFnSc2	rasa
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
vliv	vliv	k1gInSc4	vliv
mohou	moct	k5eAaImIp3nP	moct
instituce	instituce	k1gFnPc1	instituce
mít	mít	k5eAaImF	mít
na	na	k7c4	na
národ	národ	k1gInSc4	národ
a	a	k8xC	a
dílčí	dílčí	k2eAgInPc1d1	dílčí
davy	dav	k1gInPc1	dav
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
potřeba	potřeba	k1gFnSc1	potřeba
zkoumat	zkoumat	k5eAaImF	zkoumat
každý	každý	k3xTgInSc4	každý
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
instituci	instituce	k1gFnSc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
na	na	k7c4	na
dav	dav	k1gInSc4	dav
sice	sice	k8xC	sice
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mohou	moct	k5eAaImIp3nP	moct
vyústit	vyústit	k5eAaPmF	vyústit
ve	v	k7c4	v
velké	velký	k2eAgFnPc4d1	velká
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
výchova	výchova	k1gFnSc1	výchova
====	====	k?	====
</s>
</p>
<p>
<s>
Výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
spolu	spolu	k6eAd1	spolu
přímo	přímo	k6eAd1	přímo
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
řízené	řízený	k2eAgNnSc1d1	řízené
<g/>
,	,	kIx,	,
promyšlené	promyšlený	k2eAgNnSc1d1	promyšlené
vzdělání	vzdělání	k1gNnSc1	vzdělání
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgMnPc4d1	budoucí
plnoprávné	plnoprávný	k2eAgMnPc4d1	plnoprávný
členy	člen	k1gMnPc4	člen
společnosti	společnost	k1gFnSc2	společnost
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
lze	lze	k6eAd1	lze
výchovou	výchova	k1gFnSc7	výchova
a	a	k8xC	a
vzděláním	vzdělání	k1gNnSc7	vzdělání
zlepšit	zlepšit	k5eAaPmF	zlepšit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
systémech	systém	k1gInPc6	systém
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Faktory	faktor	k1gInPc1	faktor
působící	působící	k2eAgInPc1d1	působící
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
názory	názor	k1gInPc4	názor
davů	dav	k1gInPc2	dav
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
Gustave	Gustav	k1gMnSc5	Gustav
le	le	k?	le
Bon	bon	k1gInSc1	bon
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zacházet	zacházet	k5eAaImF	zacházet
s	s	k7c7	s
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
na	na	k7c4	na
dav	dav	k1gInSc4	dav
působí	působit	k5eAaImIp3nS	působit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
všech	všecek	k3xTgInPc2	všecek
jejich	jejich	k3xOp3gInPc2	jejich
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
formule	formule	k1gFnSc1	formule
====	====	k?	====
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
nemáme	mít	k5eNaImIp1nP	mít
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
obrazné	obrazný	k2eAgFnSc2d1	obrazná
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
vyvolat	vyvolat	k5eAaPmF	vyvolat
správným	správný	k2eAgNnSc7d1	správné
užitím	užití	k1gNnSc7	užití
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
formulí	formule	k1gFnPc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpůsobivější	působivý	k2eAgNnPc4d3	nejpůsobivější
slova	slovo	k1gNnPc4	slovo
patří	patřit	k5eAaImIp3nS	patřit
ta	ten	k3xDgNnPc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
nejednoznačný	jednoznačný	k2eNgInSc4d1	nejednoznačný
význam	význam	k1gInSc4	význam
a	a	k8xC	a
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
můžou	můžou	k?	můžou
různé	různý	k2eAgFnPc4d1	různá
skupiny	skupina	k1gFnPc4	skupina
lidí	člověk	k1gMnPc2	člověk
vyložit	vyložit	k5eAaPmF	vyložit
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
slova	slovo	k1gNnPc4	slovo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
rovnost	rovnost	k1gFnSc1	rovnost
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
v	v	k7c6	v
sobě	se	k3xPyFc3	se
totiž	totiž	k9	totiž
shrnují	shrnovat	k5eAaImIp3nP	shrnovat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
přání	přání	k1gNnPc2	přání
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejasnost	nejasnost	k1gFnSc1	nejasnost
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
důležité	důležitý	k2eAgNnSc1d1	důležité
znát	znát	k5eAaImF	znát
současný	současný	k2eAgInSc4d1	současný
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
a	a	k8xC	a
tatáž	týž	k3xTgNnPc1	týž
slova	slovo	k1gNnPc1	slovo
mohla	moct	k5eAaImAgNnP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
stejná	stejný	k2eAgNnPc1d1	stejné
slova	slovo	k1gNnPc1	slovo
mohou	moct	k5eAaImIp3nP	moct
jinak	jinak	k6eAd1	jinak
vykládat	vykládat	k5eAaImF	vykládat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zdůrazněna	zdůraznit	k5eAaPmNgFnS	zdůraznit
důležitá	důležitý	k2eAgFnSc1d1	důležitá
vlastnost	vlastnost	k1gFnSc1	vlastnost
státníků	státník	k1gMnPc2	státník
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
nepopulární	populární	k2eNgFnPc4d1	nepopulární
věci	věc	k1gFnPc4	věc
neutrálními	neutrální	k2eAgInPc7d1	neutrální
nebo	nebo	k8xC	nebo
populárními	populární	k2eAgInPc7d1	populární
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Stačí	stačit	k5eAaBmIp3nS	stačit
tak	tak	k6eAd1	tak
označit	označit	k5eAaPmF	označit
nenáviděné	nenáviděný	k2eAgFnSc2d1	nenáviděná
instituce	instituce	k1gFnSc2	instituce
novými	nový	k2eAgInPc7d1	nový
názvy	název	k1gInPc7	název
a	a	k8xC	a
davy	dav	k1gInPc1	dav
je	on	k3xPp3gMnPc4	on
přijmou	přijmout	k5eAaPmIp3nP	přijmout
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Iluze	iluze	k1gFnPc1	iluze
====	====	k?	====
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
civilizace	civilizace	k1gFnSc2	civilizace
davy	dav	k1gInPc1	dav
podléhaly	podléhat	k5eAaImAgFnP	podléhat
vlivu	vliv	k1gInSc2	vliv
iluzím	iluze	k1gFnPc3	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
národů	národ	k1gInPc2	národ
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
klam	klam	k1gInSc1	klam
<g/>
.	.	kIx.	.
</s>
<s>
Davy	Dav	k1gInPc1	Dav
totiž	totiž	k9	totiž
netouží	toužit	k5eNaImIp3nP	toužit
po	po	k7c6	po
pravdě	pravda	k1gFnSc6	pravda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nelíbí	líbit	k5eNaImIp3nP	líbit
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
raději	rád	k6eAd2	rád
klamy	klam	k1gInPc4	klam
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	on	k3xPp3gMnPc4	on
oslňují	oslňovat	k5eAaImIp3nP	oslňovat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
v	v	k7c6	v
lidech	člověk	k1gMnPc6	člověk
vzbudit	vzbudit	k5eAaPmF	vzbudit
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
ovládne	ovládnout	k5eAaPmIp3nS	ovládnout
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
lidi	člověk	k1gMnPc4	člověk
snaží	snažit	k5eAaImIp3nP	snažit
zbavit	zbavit	k5eAaPmF	zbavit
iluzí	iluze	k1gFnSc7	iluze
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nS	skončit
jako	jako	k9	jako
oběť	oběť	k1gFnSc1	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zkušenost	zkušenost	k1gFnSc1	zkušenost
====	====	k?	====
</s>
</p>
<p>
<s>
Jedině	jedině	k6eAd1	jedině
zkušenost	zkušenost	k1gFnSc1	zkušenost
má	mít	k5eAaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
moci	moct	k5eAaImF	moct
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zničila	zničit	k5eAaPmAgFnS	zničit
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zkušenost	zkušenost	k1gFnSc1	zkušenost
často	často	k6eAd1	často
opakovala	opakovat	k5eAaImAgFnS	opakovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zkušenosti	zkušenost	k1gFnPc4	zkušenost
nabyté	nabytý	k2eAgFnPc4d1	nabytá
jednou	jednou	k6eAd1	jednou
generací	generace	k1gFnSc7	generace
nemají	mít	k5eNaImIp3nP	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
generace	generace	k1gFnPc4	generace
následující	následující	k2eAgFnPc4d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nemohou	moct	k5eNaImIp3nP	moct
historická	historický	k2eAgNnPc1d1	historické
fakta	faktum	k1gNnPc1	faktum
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
důkaz	důkaz	k1gInSc4	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozum	rozum	k1gInSc1	rozum
====	====	k?	====
</s>
</p>
<p>
<s>
Rozum	rozum	k1gInSc1	rozum
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
davy	dav	k1gInPc4	dav
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
negativním	negativní	k2eAgInSc6d1	negativní
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
davy	dav	k1gInPc4	dav
nelze	lze	k6eNd1	lze
působit	působit	k5eAaImF	působit
logickými	logický	k2eAgFnPc7d1	logická
úvahami	úvaha	k1gFnPc7	úvaha
a	a	k8xC	a
proto	proto	k8xC	proto
řečníci	řečník	k1gMnPc1	řečník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
davy	dav	k1gInPc4	dav
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
city	cit	k1gInPc4	cit
ale	ale	k8xC	ale
ne	ne	k9	ne
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
li	li	k8xS	li
davy	dav	k1gInPc1	dav
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
city	cit	k1gInPc4	cit
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
jsou	být	k5eAaImIp3nP	být
momentálně	momentálně	k6eAd1	momentálně
davy	dav	k1gInPc1	dav
ovládány	ovládán	k2eAgInPc1d1	ovládán
a	a	k8xC	a
předstírat	předstírat	k5eAaImF	předstírat
sdílení	sdílení	k1gNnSc4	sdílení
těchto	tento	k3xDgInPc2	tento
citů	cit	k1gInPc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
vědomi	vědom	k2eAgMnPc1d1	vědom
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vzbuzujeme	vzbuzovat	k5eAaImIp1nP	vzbuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Neměli	mít	k5eNaImAgMnP	mít
bychom	by	kYmCp1nP	by
ale	ale	k9	ale
litovat	litovat	k5eAaImF	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
davy	dav	k1gInPc1	dav
nejsou	být	k5eNaImIp3nP	být
řízeny	řídit	k5eAaImNgInP	řídit
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
city	cit	k1gInPc1	cit
až	až	k6eAd1	až
dosud	dosud	k6eAd1	dosud
byly	být	k5eAaImAgFnP	být
největšími	veliký	k2eAgFnPc7d3	veliký
hybnými	hybný	k2eAgFnPc7d1	hybná
silami	síla	k1gFnPc7	síla
všech	všecek	k3xTgFnPc2	všecek
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vůdcové	vůdce	k1gMnPc1	vůdce
davu	dav	k1gInSc2	dav
a	a	k8xC	a
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
přesvědčují	přesvědčovat	k5eAaImIp3nP	přesvědčovat
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vůdcové	vůdce	k1gMnPc1	vůdce
davů	dav	k1gInPc2	dav
====	====	k?	====
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
shromážděn	shromážděn	k2eAgInSc1d1	shromážděn
určitý	určitý	k2eAgInSc1d1	určitý
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
podléhat	podléhat	k5eAaImF	podléhat
autoritě	autorita	k1gFnSc3	autorita
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidských	lidský	k2eAgInPc6d1	lidský
davech	dav	k1gInPc6	dav
hraje	hrát	k5eAaImIp3nS	hrát
vůdce	vůdce	k1gMnSc1	vůdce
značnou	značný	k2eAgFnSc4d1	značná
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcové	vůdce	k1gMnPc1	vůdce
nebývají	bývat	k5eNaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
muži	muž	k1gMnPc1	muž
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
nervózních	nervózní	k2eAgMnPc2d1	nervózní
a	a	k8xC	a
podrážděných	podrážděný	k2eAgMnPc2d1	podrážděný
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
vůle	vůle	k1gFnSc1	vůle
<g/>
,	,	kIx,	,
dav	dav	k1gInSc1	dav
se	se	k3xPyFc4	se
instinktivně	instinktivně	k6eAd1	instinktivně
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
jedni	jeden	k4xCgMnPc1	jeden
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
energičtí	energický	k2eAgMnPc1d1	energický
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
okamžitou	okamžitý	k2eAgFnSc7d1	okamžitá
vůlí	vůle	k1gFnSc7	vůle
<g/>
,	,	kIx,	,
a	a	k8xC	a
druzí	druhý	k4xOgMnPc1	druhý
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
vzácnější	vzácný	k2eAgFnPc1d2	vzácnější
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vůli	vůle	k1gFnSc4	vůle
zároveň	zároveň	k6eAd1	zároveň
pevnou	pevný	k2eAgFnSc4d1	pevná
i	i	k8xC	i
trvalou	trvalý	k2eAgFnSc4d1	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
jsou	být	k5eAaImIp3nP	být
bouřliví	bouřlivý	k2eAgMnPc1d1	bouřlivý
<g/>
,	,	kIx,	,
stateční	statečný	k2eAgMnPc1d1	statečný
a	a	k8xC	a
smělí	smělý	k2eAgMnPc1d1	smělý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Garibaldi	Garibald	k1gMnPc1	Garibald
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
chvilková	chvilkový	k2eAgFnSc1d1	chvilková
a	a	k8xC	a
málokdy	málokdy	k6eAd1	málokdy
přetrvá	přetrvat	k5eAaPmIp3nS	přetrvat
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
kategorie	kategorie	k1gFnSc1	kategorie
vůdců	vůdce	k1gMnPc2	vůdce
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
méně	málo	k6eAd2	málo
okázalé	okázalý	k2eAgFnPc4d1	okázalá
formy	forma	k1gFnPc4	forma
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
vůle	vůle	k1gFnSc1	vůle
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
překoná	překonat	k5eAaPmIp3nS	překonat
všechny	všechen	k3xTgFnPc4	všechen
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
vůdcové	vůdce	k1gMnPc1	vůdce
působí	působit	k5eAaImIp3nP	působit
-	-	kIx~	-
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
<g/>
,	,	kIx,	,
nákaza	nákaza	k1gFnSc1	nákaza
====	====	k?	====
</s>
</p>
<p>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůdce	vůdce	k1gMnSc1	vůdce
šel	jít	k5eAaImAgMnS	jít
sám	sám	k3xTgMnSc1	sám
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
ale	ale	k9	ale
být	být	k5eAaImF	být
metody	metoda	k1gFnPc4	metoda
vůdců	vůdce	k1gMnPc2	vůdce
účinné	účinný	k2eAgInPc1d1	účinný
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
použity	použít	k5eAaPmNgInP	použít
především	především	k9	především
tři	tři	k4xCgInPc1	tři
prostředky	prostředek	k1gInPc1	prostředek
–	–	k?	–
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
a	a	k8xC	a
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgNnSc1d1	čisté
a	a	k8xC	a
prosté	prostý	k2eAgNnSc1d1	prosté
tvrzení	tvrzení	k1gNnSc1	tvrzení
bez	bez	k7c2	bez
uvažování	uvažování	k1gNnSc2	uvažování
a	a	k8xC	a
důkazů	důkaz	k1gInPc2	důkaz
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejjistějších	jistý	k2eAgInPc2d3	nejjistější
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nějaká	nějaký	k3yIgFnSc1	nějaký
idea	idea	k1gFnSc1	idea
uchytit	uchytit	k5eAaPmF	uchytit
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
davů	dav	k1gInPc2	dav
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
tvrzení	tvrzení	k1gNnSc4	tvrzení
stručnější	stručný	k2eAgNnSc1d2	stručnější
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
má	mít	k5eAaImIp3nS	mít
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Nabývá	nabývat	k5eAaImIp3nS	nabývat
však	však	k9	však
skutečného	skutečný	k2eAgInSc2d1	skutečný
vlivu	vliv	k1gInSc2	vliv
jenom	jenom	k9	jenom
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ustavičně	ustavičně	k6eAd1	ustavičně
opakováno	opakován	k2eAgNnSc1d1	opakováno
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pramení	pramenit	k5eAaImIp3nS	pramenit
síla	síla	k1gFnSc1	síla
reklamy	reklama	k1gFnSc2	reklama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tvrzení	tvrzení	k1gNnSc4	tvrzení
často	často	k6eAd1	často
opakováno	opakovat	k5eAaImNgNnS	opakovat
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
působit	působit	k5eAaImF	působit
mohutný	mohutný	k2eAgInSc1d1	mohutný
mechanismus	mechanismus	k1gInSc1	mechanismus
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
současnou	současný	k2eAgFnSc4d1	současná
přítomnost	přítomnost	k1gFnSc4	přítomnost
jedinců	jedinec	k1gMnPc2	jedinec
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
i	i	k9	i
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
(	(	kIx(	(
<g/>
revoluce	revoluce	k1gFnSc1	revoluce
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Prestiž	prestiž	k1gInSc4	prestiž
====	====	k?	====
</s>
</p>
<p>
<s>
Získají	získat	k5eAaPmIp3nP	získat
<g/>
-li	i	k?	-li
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
propagované	propagovaný	k2eAgNnSc1d1	propagované
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
opakováním	opakování	k1gNnSc7	opakování
a	a	k8xC	a
nákazou	nákaza	k1gFnSc7	nákaza
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
získají	získat	k5eAaPmIp3nP	získat
tzv.	tzv.	kA	tzv.
prestiž	prestiž	k1gInSc1	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc4d1	používaný
výraz	výraz	k1gInSc4	výraz
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jakási	jakýsi	k3yIgFnSc1	jakýsi
fascinace	fascinace	k1gFnSc1	fascinace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
člověk	člověk	k1gMnSc1	člověk
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
před	před	k7c7	před
určitým	určitý	k2eAgMnSc7d1	určitý
jedincem	jedinec	k1gMnSc7	jedinec
<g/>
,	,	kIx,	,
dílem	dílo	k1gNnSc7	dílo
nebo	nebo	k8xC	nebo
ideou	idea	k1gFnSc7	idea
<g/>
.	.	kIx.	.
</s>
<s>
Prestiž	prestiž	k1gFnSc1	prestiž
je	být	k5eAaImIp3nS	být
nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
každé	každý	k3xTgFnSc2	každý
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
bohové	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
králové	král	k1gMnPc1	král
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
by	by	kYmCp3nP	by
nemohli	moct	k5eNaImAgMnP	moct
nikdy	nikdy	k6eAd1	nikdy
vládnout	vládnout	k5eAaImF	vládnout
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
–	–	k?	–
prestiž	prestiž	k1gFnSc1	prestiž
získaná	získaný	k2eAgFnSc1d1	získaná
a	a	k8xC	a
prestiž	prestiž	k1gFnSc1	prestiž
osobní	osobní	k2eAgFnSc1d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
,	,	kIx,	,
získaná	získaný	k2eAgFnSc1d1	získaná
prestiž	prestiž	k1gFnSc1	prestiž
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
jmění	jmění	k1gNnSc3	jmění
a	a	k8xC	a
vážnosti	vážnost	k1gFnSc3	vážnost
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
prestiž	prestiž	k1gFnSc1	prestiž
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
něco	něco	k3yInSc1	něco
osobitého	osobitý	k2eAgNnSc2d1	osobité
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
vzácnější	vzácný	k2eAgFnSc1d2	vzácnější
<g/>
.	.	kIx.	.
</s>
<s>
Prestiž	prestiž	k1gInSc1	prestiž
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
brání	bránit	k5eAaImIp3nS	bránit
vidět	vidět	k5eAaImF	vidět
věci	věc	k1gFnPc4	věc
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
a	a	k8xC	a
ochromuje	ochromovat	k5eAaImIp3nS	ochromovat
naše	náš	k3xOp1gInPc4	náš
úsudky	úsudek	k1gInPc4	úsudek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Roztřídění	roztřídění	k1gNnPc1	roztřídění
a	a	k8xC	a
popis	popis	k1gInSc1	popis
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
davů	dav	k1gInPc2	dav
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Roztřídění	roztřídění	k1gNnPc2	roztřídění
davů	dav	k1gInPc2	dav
===	===	k?	===
</s>
</p>
<p>
<s>
Různé	různý	k2eAgFnPc1d1	různá
kategorie	kategorie	k1gFnPc1	kategorie
davů	dav	k1gInPc2	dav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Davy	Dav	k1gInPc1	Dav
různorodé	různorodý	k2eAgFnSc2d1	různorodá
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skupiny	skupina	k1gFnPc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
libovolného	libovolný	k2eAgInSc2d1	libovolný
počtu	počet	k1gInSc2	počet
jedinců	jedinec	k1gMnPc2	jedinec
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
na	na	k7c4	na
davy	dav	k1gInPc4	dav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anonymní	anonymní	k2eAgFnSc1d1	anonymní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
davy	dav	k1gInPc4	dav
pouliční	pouliční	k2eAgInPc4d1	pouliční
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neanonymní	anonymní	k2eNgFnSc1d1	neanonymní
(	(	kIx(	(
<g/>
porota	porota	k1gFnSc1	porota
<g/>
,	,	kIx,	,
parlamenty	parlament	k1gInPc1	parlament
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Davy	Dav	k1gInPc1	Dav
stejnorodé	stejnorodý	k2eAgFnSc2d1	stejnorodá
====	====	k?	====
</s>
</p>
<p>
<s>
Sekty	sekt	k1gInPc1	sekt
<g/>
:	:	kIx,	:
Jedinci	jedinec	k1gMnPc1	jedinec
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
sdílejí	sdílet	k5eAaImIp3nP	sdílet
společné	společný	k2eAgNnSc4d1	společné
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nS	lišit
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
výchovou	výchova	k1gFnSc7	výchova
i	i	k8xC	i
zaměstnáním	zaměstnání	k1gNnSc7	zaměstnání
(	(	kIx(	(
<g/>
sekty	sekta	k1gFnSc2	sekta
politické	politický	k2eAgFnSc2d1	politická
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnSc2d1	náboženská
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kasty	kasta	k1gFnPc1	kasta
<g/>
:	:	kIx,	:
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
jedince	jedinko	k6eAd1	jedinko
stejného	stejný	k2eAgNnSc2d1	stejné
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
jsou	být	k5eAaImIp3nP	být
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
výchova	výchova	k1gFnSc1	výchova
téměř	téměř	k6eAd1	téměř
identické	identický	k2eAgNnSc1d1	identické
(	(	kIx(	(
<g/>
kasta	kasta	k1gFnSc1	kasta
vojenská	vojenský	k2eAgFnSc1d1	vojenská
<g/>
,	,	kIx,	,
kněžská	kněžský	k2eAgFnSc1d1	kněžská
<g/>
,	,	kIx,	,
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
<g/>
:	:	kIx,	:
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prošli	projít	k5eAaPmAgMnP	projít
podobnou	podobný	k2eAgFnSc7d1	podobná
výchovou	výchova	k1gFnSc7	výchova
<g/>
,	,	kIx,	,
sdílejí	sdílet	k5eAaImIp3nP	sdílet
stejné	stejný	k2eAgInPc4d1	stejný
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
životní	životní	k2eAgInPc4d1	životní
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
zde	zde	k6eAd1	zde
na	na	k7c6	na
původu	původ	k1gInSc6	původ
ani	ani	k8xC	ani
na	na	k7c6	na
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
<g/>
,	,	kIx,	,
selská	selský	k2eAgFnSc1d1	selská
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Davy	Dav	k1gInPc1	Dav
tzv.	tzv.	kA	tzv.
zločinné	zločinný	k2eAgInPc1d1	zločinný
===	===	k?	===
</s>
</p>
<p>
<s>
Zločinné	zločinný	k2eAgInPc1d1	zločinný
davy	dav	k1gInPc1	dav
jsou	být	k5eAaImIp3nP	být
poháněny	poháněn	k2eAgInPc1d1	poháněn
city	cit	k1gInPc1	cit
<g/>
,	,	kIx,	,
rozum	rozum	k1gInSc1	rozum
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
zločiny	zločin	k1gInPc4	zločin
konány	konán	k2eAgInPc4d1	konán
z	z	k7c2	z
pocitu	pocit	k1gInSc2	pocit
povinnosti	povinnost	k1gFnSc2	povinnost
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
zločinných	zločinný	k2eAgInPc2d1	zločinný
davů	dav	k1gInPc2	dav
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
důvěřivost	důvěřivost	k1gFnSc1	důvěřivost
<g/>
,	,	kIx,	,
citová	citový	k2eAgFnSc1d1	citová
přehnanost	přehnanost	k1gFnSc1	přehnanost
<g/>
,	,	kIx,	,
přístupnost	přístupnost	k1gFnSc1	přístupnost
k	k	k7c3	k
sugescím	sugesce	k1gFnPc3	sugesce
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Porotní	porotní	k2eAgInPc1d1	porotní
soudy	soud	k1gInPc1	soud
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
neanonymní	anonymní	k2eNgInSc4d1	neanonymní
různorodý	různorodý	k2eAgInSc4d1	různorodý
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc2	který
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc3	role
malá	malý	k2eAgFnSc1d1	malá
schopnost	schopnost	k1gFnSc1	schopnost
uvažování	uvažování	k1gNnSc1	uvažování
<g/>
,	,	kIx,	,
převaha	převaha	k1gFnSc1	převaha
neuvědomělých	uvědomělý	k2eNgInPc2d1	neuvědomělý
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
vůdců	vůdce	k1gMnPc2	vůdce
apod.	apod.	kA	apod.
Kdysi	kdysi	k6eAd1	kdysi
se	se	k3xPyFc4	se
porotní	porotní	k2eAgInPc1d1	porotní
soudy	soud	k1gInPc1	soud
skládaly	skládat	k5eAaImAgInP	skládat
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevují	objevovat	k5eAaImIp3nP	objevovat
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
i	i	k8xC	i
zřízenci	zřízenec	k1gMnPc1	zřízenec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
současnosti	současnost	k1gFnSc2	současnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dle	dle	k7c2	dle
statistik	statistika	k1gFnPc2	statistika
totožné	totožný	k2eAgFnPc1d1	totožná
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
porotci	porotce	k1gMnPc1	porotce
klíčově	klíčově	k6eAd1	klíčově
podléhají	podléhat	k5eAaImIp3nP	podléhat
citům	cit	k1gInPc3	cit
a	a	k8xC	a
málo	málo	k6eAd1	málo
rozumovým	rozumový	k2eAgFnPc3d1	rozumová
úvahám	úvaha	k1gFnPc3	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
každý	každý	k3xTgMnSc1	každý
dílčí	dílčí	k2eAgMnSc1d1	dílčí
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgMnSc1d1	citlivý
na	na	k7c4	na
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
cílem	cíl	k1gInSc7	cíl
advokátů	advokát	k1gMnPc2	advokát
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
porotce	porotce	k1gMnSc4	porotce
zapůsobit	zapůsobit	k5eAaPmF	zapůsobit
a	a	k8xC	a
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jej	on	k3xPp3gMnSc4	on
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
soudce	soudce	k1gMnSc1	soudce
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
ovlivnitelný	ovlivnitelný	k2eAgMnSc1d1	ovlivnitelný
<g/>
,	,	kIx,	,
znát	znát	k5eAaImF	znát
soucit	soucit	k1gInSc4	soucit
<g/>
,	,	kIx,	,
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
porotní	porotní	k2eAgInSc1d1	porotní
soud	soud	k1gInSc1	soud
jako	jako	k8xS	jako
orgán	orgán	k1gInSc1	orgán
fungoval	fungovat	k5eAaImAgInS	fungovat
kompletně	kompletně	k6eAd1	kompletně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
soudce	soudce	k1gMnSc1	soudce
s	s	k7c7	s
porotci	porotce	k1gMnPc7	porotce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Davy	Dav	k1gInPc1	Dav
voličské	voličský	k2eAgFnSc2d1	voličská
===	===	k?	===
</s>
</p>
<p>
<s>
Voličské	voličský	k2eAgInPc1d1	voličský
davy	dav	k1gInPc1	dav
jsou	být	k5eAaImIp3nP	být
skupiny	skupina	k1gFnPc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
volí	volit	k5eAaImIp3nP	volit
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
zastávat	zastávat	k5eAaImF	zastávat
určité	určitý	k2eAgFnPc4d1	určitá
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Projevují	projevovat	k5eAaImIp3nP	projevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
slabá	slabý	k2eAgFnSc1d1	slabá
schopnost	schopnost	k1gFnSc1	schopnost
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
kritického	kritický	k2eAgMnSc2d1	kritický
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
lehkověrnost	lehkověrnost	k1gFnSc4	lehkověrnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
rozhodování	rozhodování	k1gNnSc6	rozhodování
hraje	hrát	k5eAaImIp3nS	hrát
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
role	role	k1gFnSc2	role
vůdců	vůdce	k1gMnPc2	vůdce
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
faktorů	faktor	k1gInPc2	faktor
–	–	k?	–
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc1	opakování
<g/>
,	,	kIx,	,
prestiž	prestiž	k1gFnSc1	prestiž
a	a	k8xC	a
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
prestiž	prestiž	k1gFnSc1	prestiž
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
podmínka	podmínka	k1gFnSc1	podmínka
pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
kandidáta	kandidát	k1gMnSc2	kandidát
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
důležitější	důležitý	k2eAgMnSc1d2	důležitější
než	než	k8xS	než
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
programu	program	k1gInSc6	program
kandidáta	kandidát	k1gMnSc4	kandidát
pak	pak	k6eAd1	pak
hrají	hrát	k5eAaImIp3nP	hrát
především	především	k9	především
sliby	slib	k1gInPc1	slib
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ani	ani	k9	ani
není	být	k5eNaImIp3nS	být
tak	tak	k9	tak
důležité	důležitý	k2eAgNnSc1d1	důležité
poté	poté	k6eAd1	poté
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
podat	podat	k5eAaPmF	podat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Shromáždění	shromáždění	k1gNnSc4	shromáždění
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
parlamentech	parlament	k1gInPc6	parlament
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
představitelé	představitel	k1gMnPc1	představitel
vyhrocených	vyhrocený	k2eAgInPc2d1	vyhrocený
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
shromáždění	shromáždění	k1gNnSc1	shromáždění
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
svého	svůj	k3xOyFgMnSc4	svůj
vůdce	vůdce	k1gMnSc4	vůdce
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
moc	moc	k6eAd1	moc
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
vůdce	vůdce	k1gMnSc1	vůdce
velkou	velký	k2eAgFnSc4d1	velká
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
bez	bez	k7c2	bez
prestiže	prestiž	k1gFnSc2	prestiž
nemají	mít	k5eNaImIp3nP	mít
šanci	šance	k1gFnSc4	šance
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
přednesou	přednést	k5eAaPmIp3nP	přednést
jakkoliv	jakkoliv	k6eAd1	jakkoliv
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ostatní	ostatní	k2eAgMnPc1d1	ostatní
poslanci	poslanec	k1gMnPc1	poslanec
takovéto	takovýto	k3xDgFnSc2	takovýto
řeči	řeč	k1gFnSc2	řeč
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kolektivní	kolektivní	k2eAgNnSc1d1	kolektivní
chování	chování	k1gNnSc1	chování
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
je	být	k5eAaImIp3nS	být
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
Zimbardova	Zimbardův	k2eAgFnSc1d1	Zimbardův
teorie	teorie	k1gFnSc1	teorie
deindividuace	deindividuace	k1gFnSc1	deindividuace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
rozpracovává	rozpracovávat	k5eAaImIp3nS	rozpracovávat
a	a	k8xC	a
experimentem	experiment	k1gInSc7	experiment
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
některé	některý	k3yIgInPc4	některý
postřehy	postřeh	k1gInPc4	postřeh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
L.K.	L.K.	k1gMnSc2	L.K.
Hofmana	Hofman	k1gMnSc2	Hofman
a	a	k8xC	a
Z.	Z.	kA	Z.
Ullricha	Ullrich	k1gMnSc4	Ullrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
KRA	kra	k1gFnSc1	kra
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
