<s>
Villette	Villette	k5eAaPmIp2nP
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Villette	Villette	k5eAaPmIp2nP
Autor	autor	k1gMnSc1
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP
Brontëová	Brontëová	k1gFnSc1
Původní	původní	k2eAgFnSc1d1
název	název	k1gInSc4
</s>
<s>
Villette	Villette	k5eAaPmIp2nP
Země	zem	k1gFnPc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
román	román	k1gInSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Elder	Elder	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gInSc1
vydání	vydání	k1gNnSc2
</s>
<s>
1853	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
Shirley	Shirlea	k1gFnPc1
</s>
<s>
The	The	k?
Professor	Professor	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Villette	villetta	k1gFnPc1
je	on	k3xPp3gFnPc4
román	román	k1gInSc1
pro	pro	k7c4
dívky	dívka	k1gFnPc4
a	a	k8xC
ženy	žena	k1gFnPc4
Charlotte	Charlotte	k1gFnSc2
Brönteové	Brönteová	k1gFnSc2
složený	složený	k2eAgMnSc1d1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Román	Román	k1gMnSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
část	část	k1gFnSc1
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
části	část	k1gFnSc6
knihy	kniha	k1gFnSc2
se	se	k3xPyFc4
hlavní	hlavní	k2eAgFnSc1d1
hrdinka	hrdinka	k1gFnSc1
<g/>
,	,	kIx,
slečna	slečna	k1gFnSc1
Lucie	Lucie	k1gFnSc1
Snow	Snow	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
přeplaví	přeplavit	k5eAaPmIp3nS
na	na	k7c4
kontinent	kontinent	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
zde	zde	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
smrti	smrt	k1gFnSc6
dámy	dáma	k1gFnSc2
<g/>
,	,	kIx,
u	u	k7c2
níž	jenž	k3xRgFnSc2
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
služebná	služebná	k1gFnSc1
a	a	k8xC
společnice	společnice	k1gFnSc1
<g/>
,	,	kIx,
našla	najít	k5eAaPmAgFnS
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Najde	najít	k5eAaPmIp3nS
je	být	k5eAaImIp3nS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
(	(	kIx(
<g/>
Villette	Villett	k1gInSc5
<g/>
)	)	kIx)
fiktivního	fiktivní	k2eAgInSc2d1
malého	malý	k2eAgInSc2d1
státečku	státeček	k1gInSc2
Labassecoure	Labassecour	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
vychovatelkou	vychovatelka	k1gFnSc7
dětí	dítě	k1gFnPc2
madame	madame	k1gFnPc2
Beckové	Becková	k1gFnSc2
a	a	k8xC
posléze	posléze	k6eAd1
učitelkou	učitelka	k1gFnSc7
v	v	k7c6
jejím	její	k3xOp3gInSc6
penzionátu	penzionát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
prázdninách	prázdniny	k1gFnPc6
Lucie	Lucie	k1gFnSc2
vážně	vážně	k6eAd1
onemocní	onemocnět	k5eAaPmIp3nS
a	a	k8xC
šťastnou	šťastný	k2eAgFnSc7d1
náhodou	náhoda	k1gFnSc7
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
kmotrou	kmotra	k1gFnSc7
a	a	k8xC
jejím	její	k3xOp3gMnSc7
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
rovněž	rovněž	k9
odešli	odejít	k5eAaPmAgMnP
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
část	část	k1gFnSc1
končí	končit	k5eAaImIp3nS
doručením	doručení	k1gNnSc7
prvního	první	k4xOgInSc2
dopisu	dopis	k1gInSc2
od	od	k7c2
Johna	John	k1gMnSc2
Brettona	Bretton	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
Luciiny	Luciin	k2eAgFnSc2d1
kmotry	kmotra	k1gFnSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
rozkmotřila	rozkmotřit	k5eAaPmAgFnS
s	s	k7c7
Monsieur	Monsieur	k1gMnSc1
Paulem	Paul	k1gMnSc7
(	(	kIx(
<g/>
Davidem	David	k1gMnSc7
Carlem	Carl	k1gMnSc7
Emanuelem	Emanuel	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bratrancem	bratranec	k1gMnSc7
madame	madame	k1gFnSc2
Beckové	Becková	k1gFnSc2
a	a	k8xC
jejím	její	k3xOp3gMnSc7
osobním	osobní	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
pak	pak	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
náhodným	náhodný	k2eAgNnSc7d1
setkáním	setkání	k1gNnSc7
Lucie	Lucie	k1gFnSc2
a	a	k8xC
Johna	John	k1gMnSc2
s	s	k7c7
Monsieurem	Monsieur	k1gMnSc7
de	de	k?
Bassompierre	Bassompierr	k1gInSc5
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
krásnou	krásný	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
Paulinou	Paulin	k2eAgFnSc7d1
Mary	Mary	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
s	s	k7c7
Paulinou	Paulin	k2eAgFnSc7d1
se	se	k3xPyFc4
do	do	k7c2
sebe	sebe	k3xPyFc4
postupně	postupně	k6eAd1
zamilují	zamilovat	k5eAaPmIp3nP
a	a	k8xC
on	on	k3xPp3gMnSc1
se	se	k3xPyFc4
konečně	konečně	k6eAd1
zbaví	zbavit	k5eAaPmIp3nS
vlivu	vliv	k1gInSc2
Paulininy	Paulinin	k2eAgFnSc2d1
sestřenky	sestřenka	k1gFnSc2
Ginevry	Ginevra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
začíná	začínat	k5eAaImIp3nS
odvíjet	odvíjet	k5eAaImF
vztah	vztah	k1gInSc4
Lucie	Lucie	k1gFnSc2
a	a	k8xC
Monsieur	Monsieura	k1gFnPc2
Emanuela	Emanuel	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
však	však	k9
rozdílného	rozdílný	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
cesty	cesta	k1gFnSc2
se	se	k3xPyFc4
jim	on	k3xPp3gInPc3
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
mnoho	mnoho	k4c4
překážek	překážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
paní	paní	k1gFnSc1
Walravensová	Walravensová	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Emanuelovy	Emanuelův	k2eAgFnSc2d1
mrtvé	mrtvý	k2eAgFnSc2d1
snoubenky	snoubenka	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
páter	páter	k1gMnSc1
Silas	Silas	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jej	on	k3xPp3gMnSc4
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
a	a	k8xC
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
nejvíce	hodně	k6eAd3,k6eAd1
popuzuje	popuzovat	k5eAaImIp3nS
právě	právě	k9
rozdílné	rozdílný	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
Lucie	Lucie	k1gFnSc2
a	a	k8xC
Emanuela	Emanuel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
tu	tu	k6eAd1
je	být	k5eAaImIp3nS
sama	sám	k3xTgFnSc1
Luciina	Luciin	k2eAgFnSc1d1
zaměstnavatelka	zaměstnavatelka	k1gFnSc1
<g/>
,	,	kIx,
Madame	madame	k1gFnSc1
Becková	Becková	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
si	se	k3xPyFc3
sama	sám	k3xTgFnSc1
Emanuela	Emanuela	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
vzít	vzít	k5eAaPmF
a	a	k8xC
tak	tak	k6eAd1
mnohé	mnohý	k2eAgNnSc1d1
vytěžit	vytěžit	k5eAaPmF
(	(	kIx(
<g/>
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Emanuel	Emanuel	k1gMnSc1
díky	díky	k7c3
pomoci	pomoc	k1gFnSc3
rodině	rodina	k1gFnSc3
své	svůj	k3xOyFgFnSc2
snoubenky	snoubenka	k1gFnSc2
chudý	chudý	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
zásnubami	zásnuba	k1gFnPc7
Emanuela	Emanuela	k1gFnSc1
a	a	k8xC
Lucie	Lucie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucii	Lucie	k1gFnSc4
Emanuel	Emanuela	k1gFnPc2
předá	předat	k5eAaPmIp3nS
nový	nový	k2eAgInSc4d1
penzionát	penzionát	k1gInSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
odplouvá	odplouvat	k5eAaImIp3nS
do	do	k7c2
zámoří	zámoří	k1gNnSc2
a	a	k8xC
až	až	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
vrátí	vrátit	k5eAaPmIp3nP
<g/>
,	,	kIx,
vezmou	vzít	k5eAaPmIp3nP
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
Román	román	k1gInSc1
však	však	k9
končí	končit	k5eAaImIp3nS
tragicky	tragicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Emanuel	Emanuel	k1gMnSc1
vrací	vracet	k5eAaImIp3nS
<g/>
,	,	kIx,
strhne	strhnout	k5eAaPmIp3nS
se	se	k3xPyFc4
obrovská	obrovský	k2eAgFnSc1d1
bouře	bouře	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
potopí	potopit	k5eAaPmIp3nS
mnoho	mnoho	k4c4
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
tu	tu	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
palubě	paluba	k1gFnSc6
se	se	k3xPyFc4
plavil	plavit	k5eAaImAgMnS
Emanuel	Emanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náplní	náplň	k1gFnPc2
Luciina	Luciin	k2eAgInSc2d1
života	život	k1gInSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
stává	stávat	k5eAaImIp3nS
pouze	pouze	k6eAd1
její	její	k3xOp3gInSc1
penzionát	penzionát	k1gInSc1
na	na	k7c6
předměstí	předměstí	k1gNnSc6
svaté	svatý	k2eAgFnSc2d1
Klotyldy	Klotylda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vydání	vydání	k1gNnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
vydaná	vydaný	k2eAgFnSc1d1
v	v	k7c6
češtině	čeština	k1gFnSc6
roku	rok	k1gInSc2
1999	#num#	k4
nakladatelstvím	nakladatelství	k1gNnPc3
Albatros	albatros	k1gMnSc1
je	být	k5eAaImIp3nS
autentickým	autentický	k2eAgInSc7d1
opisem	opis	k1gInSc7
života	život	k1gInSc2
v	v	k7c6
penzionátech	penzionát	k1gInPc6
a	a	k8xC
zcela	zcela	k6eAd1
uvěřitelnou	uvěřitelný	k2eAgFnSc7d1
výpovědí	výpověď	k1gFnSc7
o	o	k7c6
společnosti	společnost	k1gFnSc6
devatenáctého	devatenáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
je	být	k5eAaImIp3nS
spisovný	spisovný	k2eAgInSc1d1
<g/>
,	,	kIx,
doplňovaný	doplňovaný	k2eAgInSc1d1
občas	občas	k6eAd1
anglickými	anglický	k2eAgMnPc7d1
a	a	k8xC
hlavně	hlavně	k9
francouzskými	francouzský	k2eAgInPc7d1
výrazy	výraz	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
dokonale	dokonale	k6eAd1
podtrhují	podtrhovat	k5eAaImIp3nP
atmosféru	atmosféra	k1gFnSc4
celého	celý	k2eAgInSc2d1
příběhu	příběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
díla	dílo	k1gNnSc2
Villette	Villett	k1gInSc5
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2015081971	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
187400545	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2015081971	#num#	k4
</s>
