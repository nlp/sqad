<s>
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc4	Gosling
O.C.	O.C.	k1gFnSc2	O.C.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1955	[number]	k4	1955
poblíž	poblíž	k7c2	poblíž
Calgary	Calgary	k1gNnSc2	Calgary
<g/>
,	,	kIx,	,
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
softwarový	softwarový	k2eAgMnSc1d1	softwarový
programátor	programátor	k1gMnSc1	programátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
<g/>
,	,	kIx,	,
multiplatformního	multiplatformní	k2eAgInSc2d1	multiplatformní
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Java	Javum	k1gNnSc2	Javum
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pojmenovaného	pojmenovaný	k2eAgMnSc2d1	pojmenovaný
Oak	Oak	k1gMnSc2	Oak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Calgary	Calgary	k1gNnPc1	Calgary
titul	titul	k1gInSc1	titul
B.	B.	kA	B.
<g/>
Sc	Sc	k1gMnPc2	Sc
(	(	kIx(	(
<g/>
bakalář	bakalář	k1gMnSc1	bakalář
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
oboru	obor	k1gInSc6	obor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
na	na	k7c4	na
Carnegie	Carnegie	k1gFnPc4	Carnegie
Mellon	Mellon	k1gInSc4	Mellon
University	universita	k1gFnPc4	universita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Algebraic	Algebraic	k1gMnSc1	Algebraic
Manipulation	Manipulation	k1gInSc1	Manipulation
of	of	k?	of
Constraints	Constraints	k1gInSc1	Constraints
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
textového	textový	k2eAgInSc2d1	textový
editoru	editor	k1gInSc2	editor
Emacs	Emacsa	k1gFnPc2	Emacsa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
pod	pod	k7c7	pod
UNIXem	Unix	k1gInSc7	Unix
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jej	on	k3xPp3gInSc4	on
Gosling	Gosling	k1gInSc4	Gosling
Emacs	Emacs	k1gInSc1	Emacs
(	(	kIx(	(
<g/>
Gosmacs	Gosmacs	k1gInSc1	Gosmacs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
víceprocesorových	víceprocesorový	k2eAgFnPc2d1	víceprocesorová
verzí	verze	k1gFnPc2	verze
UNIXu	Unix	k1gInSc2	Unix
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
překladačů	překladač	k1gMnPc2	překladač
<g/>
,	,	kIx,	,
emailových	emailový	k2eAgMnPc2d1	emailový
agentů	agent	k1gMnPc2	agent
a	a	k8xC	a
window	window	k?	window
managerů	manager	k1gMnPc2	manager
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
zaměstnaný	zaměstnaný	k1gMnSc1	zaměstnaný
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
provedl	provést	k5eAaPmAgInS	provést
originální	originální	k2eAgInSc1d1	originální
návrh	návrh	k1gInSc1	návrh
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Java	Javum	k1gNnSc2	Javum
a	a	k8xC	a
implementoval	implementovat	k5eAaImAgInS	implementovat
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgInSc1	první
překladač	překladač	k1gInSc1	překladač
a	a	k8xC	a
virtuální	virtuální	k2eAgInSc1d1	virtuální
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
dalších	další	k2eAgFnPc2d1	další
verzí	verze	k1gFnPc2	verze
jazyka	jazyk	k1gInSc2	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
NeWS	NeWS	k1gFnPc2	NeWS
<g/>
,	,	kIx,	,
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
UNIXu	Unix	k1gInSc2	Unix
a	a	k8xC	a
windowing	windowing	k1gInSc4	windowing
system	syst	k1gInSc7	syst
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
eliminaci	eliminace	k1gFnSc4	eliminace
grafických	grafický	k2eAgInPc2d1	grafický
a	a	k8xC	a
architektonických	architektonický	k2eAgInPc2d1	architektonický
nedostatků	nedostatek	k1gInPc2	nedostatek
systému	systém	k1gInSc2	systém
Unix	Unix	k1gInSc1	Unix
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
HotJava	HotJava	k1gFnSc1	HotJava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
vědeckého	vědecký	k2eAgMnSc2d1	vědecký
poradce	poradce	k1gMnSc2	poradce
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
vývojem	vývoj	k1gInSc7	vývoj
klientského	klientský	k2eAgInSc2d1	klientský
softwaru	software	k1gInSc2	software
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
pracovní	pracovní	k2eAgFnSc7d1	pracovní
náplní	náplň	k1gFnSc7	náplň
byla	být	k5eAaImAgFnS	být
korekce	korekce	k1gFnSc1	korekce
a	a	k8xC	a
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
pokračujícím	pokračující	k2eAgInSc7d1	pokračující
vývojem	vývoj	k1gInSc7	vývoj
jazyka	jazyk	k1gInSc2	jazyk
Java	Jav	k2eAgFnSc1d1	Java
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
virtuálního	virtuální	k2eAgInSc2d1	virtuální
stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
class	class	k1gInSc4	class
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
firmu	firma	k1gFnSc4	firma
Sun	Sun	kA	Sun
na	na	k7c6	na
konferencích	konference	k1gFnPc6	konference
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
pečlivě	pečlivě	k6eAd1	pečlivě
sledoval	sledovat	k5eAaImAgMnS	sledovat
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
IT	IT	kA	IT
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
komunikoval	komunikovat	k5eAaImAgMnS	komunikovat
s	s	k7c7	s
vývojáři	vývojář	k1gMnPc7	vývojář
i	i	k8xC	i
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohl	moct	k5eAaImAgInS	moct
lépe	dobře	k6eAd2	dobře
předvídat	předvídat	k5eAaImF	předvídat
potřeby	potřeba	k1gFnPc4	potřeba
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
by	by	kYmCp3nS	by
promítl	promítnout	k5eAaPmAgMnS	promítnout
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
technologie	technologie	k1gFnSc2	technologie
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Firmu	firma	k1gFnSc4	firma
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
opustil	opustit	k5eAaPmAgInS	opustit
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
společností	společnost	k1gFnSc7	společnost
Oracle	Oracle	k1gFnSc2	Oracle
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
společností	společnost	k1gFnSc7	společnost
Google	Google	k1gNnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
start-up	startp	k1gMnSc1	start-up
projektu	projekt	k1gInSc2	projekt
Liquid	Liquida	k1gFnPc2	Liquida
Robotics	Roboticsa	k1gFnPc2	Roboticsa
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
figuruje	figurovat	k5eAaImIp3nS	figurovat
jako	jako	k9	jako
poradce	poradce	k1gMnSc1	poradce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Typesafe	Typesaf	k1gInSc5	Typesaf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získal	získat	k5eAaPmAgMnS	získat
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Officer	Officer	k1gMnSc1	Officer
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
.	.	kIx.	.
</s>
<s>
Order	Order	k1gInSc1	Order
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
občanské	občanský	k2eAgNnSc1d1	občanské
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
a	a	k8xC	a
Officer	Officer	k1gInSc1	Officer
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
stupeň	stupeň	k1gInSc1	stupeň
tohoto	tento	k3xDgNnSc2	tento
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
profesních	profesní	k2eAgNnPc2d1	profesní
ocenění	ocenění	k1gNnPc2	ocenění
jako	jako	k8xC	jako
např.	např.	kA	např.
Innovator	Innovator	k1gInSc1	Innovator
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc4	Year
udělované	udělovaný	k2eAgFnSc2d1	udělovaná
časopisem	časopis	k1gInSc7	časopis
Discover	Discover	k1gInSc1	Discover
Magazine	Magazin	k1gInSc5	Magazin
a	a	k8xC	a
Software	software	k1gInSc1	software
Development	Development	k1gInSc1	Development
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
"	"	kIx"	"
<g/>
Programing	Programing	k1gInSc1	Programing
Excellence	Excellence	k1gFnSc2	Excellence
Award	Awarda	k1gFnPc2	Awarda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
deseti	deset	k4xCc2	deset
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
oholit	oholit	k5eAaPmF	oholit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnPc1	jeho
děti	dítě	k1gFnPc1	dítě
ještě	ještě	k9	ještě
nikdy	nikdy	k6eAd1	nikdy
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
bez	bez	k7c2	bez
vousů	vous	k1gInPc2	vous
neviděli	vidět	k5eNaImAgMnP	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Ken	Ken	k?	Ken
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Holmes	Holmes	k1gMnSc1	Holmes
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Java	Jav	k1gInSc2	Jav
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc1	language
<g/>
,	,	kIx,	,
Fourth	Fourth	k1gInSc1	Fourth
Edition	Edition	k1gInSc4	Edition
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesle	k2eAgFnPc4d1	Addison-Wesle
Professional	Professional	k1gFnPc4	Professional
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-321-34980-6	[number]	k4	0-321-34980-6
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Joy	Joy	k1gMnSc1	Joy
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
L.	L.	kA	L.
Steele	Steel	k1gInPc1	Steel
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Gilad	Gilad	k1gInSc4	Gilad
Bracha	brach	k1gMnSc2	brach
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
The	The	k1gMnSc2	The
Java	Javus	k1gMnSc2	Javus
Language	language	k1gFnSc1	language
Specification	Specification	k1gInSc1	Specification
<g/>
,	,	kIx,	,
Third	Third	k1gInSc1	Third
Edition	Edition	k1gInSc4	Edition
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesle	k2eAgFnPc4d1	Addison-Wesle
Professional	Professional	k1gFnPc4	Professional
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-321-24678-0	[number]	k4	0-321-24678-0
Ken	Ken	k1gMnSc1	Ken
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Holmes	Holmes	k1gMnSc1	Holmes
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Java	Jav	k1gInSc2	Jav
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc1	language
<g/>
,	,	kIx,	,
Third	Third	k1gInSc1	Third
Edition	Edition	k1gInSc4	Edition
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesle	k2eAgFnPc4d1	Addison-Wesle
Professional	Professional	k1gFnPc4	Professional
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g />
.	.	kIx.	.
</s>
<s>
0-201-70433-1	[number]	k4	0-201-70433-1
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Joy	Joy	k1gMnSc1	Joy
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
L.	L.	kA	L.
Steele	Steel	k1gInPc1	Steel
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Gilad	Gilad	k1gInSc4	Gilad
Bracha	brach	k1gMnSc2	brach
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
Java	Javus	k1gMnSc2	Javus
Language	language	k1gFnSc1	language
Specification	Specification	k1gInSc1	Specification
<g/>
,	,	kIx,	,
Second	Second	k1gInSc1	Second
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-31008-2	[number]	k4	0-201-31008-2
Gregory	Gregor	k1gMnPc4	Gregor
Bollella	Bollello	k1gNnPc1	Bollello
(	(	kIx(	(
<g/>
Editor	editor	k1gInSc1	editor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Brosgol	Brosgola	k1gFnPc2	Brosgola
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Dibble	Dibble	k1gMnSc1	Dibble
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Furr	Furr	k1gMnSc1	Furr
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Hardin	Hardin	k2eAgMnSc1d1	Hardin
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Turnbull	Turnbull	k1gMnSc1	Turnbull
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Real-Time	Real-Tim	k1gMnSc5	Real-Tim
Specification	Specification	k1gInSc1	Specification
for	forum	k1gNnPc2	forum
Java	Javum	k1gNnSc2	Javum
<g/>
,	,	kIx,	,
Addison	Addison	k1gMnSc1	Addison
Wesley	Weslea	k1gFnSc2	Weslea
Longman	Longman	k1gMnSc1	Longman
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-70323-8	[number]	k4	0-201-70323-8
Ken	Ken	k1gMnSc1	Ken
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Jav	k2eAgFnSc1d1	Java
programming	programming	k1gInSc4	programming
<g />
.	.	kIx.	.
</s>
<s>
language	language	k1gFnSc1	language
Second	Second	k1gMnSc1	Second
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-31006-6	[number]	k4	0-201-31006-6
Ken	Ken	k1gMnSc1	Ken
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Java	k1gFnSc1	Java
programming	programming	k1gInSc4	programming
language	language	k1gFnSc2	language
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Weslea	k1gFnSc2	Addison-Weslea
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-63455-4	[number]	k4	0-201-63455-4
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Joy	Joy	k1gMnSc1	Joy
<g/>
,	,	kIx,	,
Guy	Guy	k1gMnSc1	Guy
L.	L.	kA	L.
Steele	Steel	k1gInPc1	Steel
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
The	The	k1gMnSc2	The
Java	Javus	k1gMnSc2	Javus
Language	language	k1gFnSc1	language
Specification	Specification	k1gInSc1	Specification
<g/>
,	,	kIx,	,
Addison	Addison	k1gMnSc1	Addison
Wesley	Weslea	k1gFnSc2	Weslea
Publishing	Publishing	k1gInSc1	Publishing
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-63451-1	[number]	k4	0-201-63451-1
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Frank	frank	k1gInSc4	frank
Yellin	Yellina	k1gFnPc2	Yellina
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Java	k1gMnSc1	Java
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Java	k1gFnSc1	Java
Application	Application	k1gInSc4	Application
Programming	Programming	k1gInSc1	Programming
Interface	interface	k1gInSc1	interface
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
<g/>
:	:	kIx,	:
Window	Window	k1gFnSc1	Window
Toolkit	Toolkita	k1gFnPc2	Toolkita
and	and	k?	and
Applets	Appletsa	k1gFnPc2	Appletsa
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Weslea	k1gFnSc2	Addison-Weslea
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-63459-7	[number]	k4	0-201-63459-7
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Frank	frank	k1gInSc4	frank
Yellin	Yellina	k1gFnPc2	Yellina
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Java	k1gMnSc1	Java
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Java	Java	k1gFnSc1	Java
Application	Application	k1gInSc4	Application
Programming	Programming	k1gInSc1	Programming
Interface	interface	k1gInSc1	interface
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
Core	Cor	k1gInSc2	Cor
Packages	Packages	k1gInSc1	Packages
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-201-63453-8	[number]	k4	0-201-63453-8
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
McGilton	McGilton	k1gInSc1	McGilton
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Java	Java	k1gMnSc1	Java
language	language	k1gFnSc2	language
Environment	Environment	k1gMnSc1	Environment
<g/>
:	:	kIx,	:
A	A	kA	A
white	white	k5eAaPmIp2nP	white
paper	paper	k1gInSc1	paper
<g/>
,	,	kIx,	,
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
S.	S.	kA	S.
H.	H.	kA	H.
Rosenthal	Rosenthal	k1gMnSc1	Rosenthal
<g/>
,	,	kIx,	,
Michelle	Michelle	k1gFnSc1	Michelle
J.	J.	kA	J.
Arden	Ardeny	k1gFnPc2	Ardeny
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
NeWS	NeWS	k1gMnSc1	NeWS
Book	Book	k1gMnSc1	Book
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Network	network	k1gInSc1	network
<g/>
/	/	kIx~	/
<g/>
Extensible	Extensible	k1gMnSc7	Extensible
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
(	(	kIx(	(
<g/>
Sun	Sun	kA	Sun
Technical	Technical	k1gFnSc2	Technical
Reference	reference	k1gFnSc2	reference
Library	Librara	k1gFnSc2	Librara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-387-96915-2	[number]	k4	0-387-96915-2
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc1	Gosling
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc4	Gosling
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Linuxexpres	Linuxexpres	k1gInSc1	Linuxexpres
<g/>
:	:	kIx,	:
Java	Java	k1gFnSc1	Java
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
při	při	k7c6	při
šálku	šálek	k1gInSc6	šálek
dobré	dobrý	k2eAgFnSc2d1	dobrá
kávy	káva	k1gFnSc2	káva
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
weblog	weblog	k1gInSc1	weblog
</s>
