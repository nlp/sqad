<s>
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc4	Gosling
O.C.	O.C.	k1gFnSc2	O.C.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1955	[number]	k4	1955
poblíž	poblíž	k7c2	poblíž
Calgary	Calgary	k1gNnSc2	Calgary
<g/>
,	,	kIx,	,
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
softwarový	softwarový	k2eAgMnSc1d1	softwarový
programátor	programátor	k1gMnSc1	programátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
<g/>
,	,	kIx,	,
multiplatformního	multiplatformní	k2eAgInSc2d1	multiplatformní
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Java	Javum	k1gNnSc2	Javum
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pojmenovaného	pojmenovaný	k2eAgMnSc2d1	pojmenovaný
Oak	Oak	k1gMnSc2	Oak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
