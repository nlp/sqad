<s>
Gilotina	gilotina	k1gFnSc1	gilotina
(	(	kIx(	(
<g/>
zast.	zast.	k?	zast.
guillotina	guillotina	k1gFnSc1	guillotina
<g/>
,	,	kIx,	,
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
guillotine	guillotin	k1gMnSc5	guillotin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
popravčí	popravčí	k2eAgInSc1d1	popravčí
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
využívaný	využívaný	k2eAgInSc1d1	využívaný
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzencovu	odsouzencův	k2eAgFnSc4d1	odsouzencova
hlavu	hlava	k1gFnSc4	hlava
stíná	stínat	k5eAaImIp3nS	stínat
železná	železný	k2eAgFnSc1d1	železná
sekera	sekera	k1gFnSc1	sekera
s	s	k7c7	s
šikmým	šikmý	k2eAgNnSc7d1	šikmé
ostřím	ostří	k1gNnSc7	ostří
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
značná	značný	k2eAgFnSc1d1	značná
hmotnost	hmotnost	k1gFnSc1	hmotnost
sekery	sekera	k1gFnSc2	sekera
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
razanci	razance	k1gFnSc4	razance
jejího	její	k3xOp3gInSc2	její
dopadu	dopad	k1gInSc2	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
gilotinou	gilotina	k1gFnSc7	gilotina
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
popravy	poprava	k1gFnSc2	poprava
mečem	meč	k1gInSc7	meč
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
způsobů	způsob	k1gInPc2	způsob
dekapitace	dekapitace	k1gFnSc2	dekapitace
neboli	neboli	k8xC	neboli
stětí	stětí	k1gNnSc2	stětí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
se	se	k3xPyFc4	se
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
mnohem	mnohem	k6eAd1	mnohem
humánnější	humánní	k2eAgInSc4d2	humánnější
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
gilotina	gilotina	k1gFnSc1	gilotina
podle	podle	k7c2	podle
propagátora	propagátor	k1gMnSc2	propagátor
a	a	k8xC	a
velkého	velký	k2eAgMnSc2d1	velký
příznivce	příznivec	k1gMnSc2	příznivec
používání	používání	k1gNnSc2	používání
tohoto	tento	k3xDgInSc2	tento
popravčího	popravčí	k2eAgInSc2d1	popravčí
stroje	stroj	k1gInSc2	stroj
-	-	kIx~	-
francouzského	francouzský	k2eAgMnSc2d1	francouzský
lékaře	lékař	k1gMnSc2	lékař
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Guillotina	Guillotina	k1gFnSc1	Guillotina
<g/>
.	.	kIx.	.
</s>
<s>
Určitě	určitě	k6eAd1	určitě
není	být	k5eNaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
přímým	přímý	k2eAgMnSc7d1	přímý
vynálezcem	vynálezce	k1gMnSc7	vynálezce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
popravčí	popravčí	k2eAgInPc1d1	popravčí
stroje	stroj	k1gInPc1	stroj
pracující	pracující	k2eAgInPc1d1	pracující
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
gilotina	gilotina	k1gFnSc1	gilotina
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1792	[number]	k4	1792
za	za	k7c2	za
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prosazena	prosadit	k5eAaPmNgFnS	prosadit
J.	J.	kA	J.
I.	I.	kA	I.
Guillotinem	Guillotin	k1gMnSc7	Guillotin
a	a	k8xC	a
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Louisem	Louis	k1gMnSc7	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Stroji	stroj	k1gInSc3	stroj
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
louison	louison	k1gNnSc1	louison
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
louisette	louisette	k5eAaPmIp2nP	louisette
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
název	název	k1gInSc1	název
gilotina	gilotina	k1gFnSc1	gilotina
<g/>
.	.	kIx.	.
</s>
<s>
Popravy	poprava	k1gFnPc1	poprava
gilotinou	gilotina	k1gFnSc7	gilotina
byly	být	k5eAaImAgFnP	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
a	a	k8xC	a
proto	proto	k8xC	proto
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
gilotina	gilotina	k1gFnSc1	gilotina
instalována	instalovat	k5eAaBmNgFnS	instalovat
na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
náměstí	náměstí	k1gNnSc6	náměstí
du	du	k?	du
Carrousel	Carrousel	k1gInSc1	Carrousel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stála	stát	k5eAaImAgFnS	stát
až	až	k6eAd1	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1793	[number]	k4	1793
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
popravy	poprava	k1gFnSc2	poprava
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
byla	být	k5eAaImAgFnS	být
krátce	krátce	k6eAd1	krátce
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
na	na	k7c4	na
méně	málo	k6eAd2	málo
frekventované	frekventovaný	k2eAgNnSc4d1	frekventované
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
veřejné	veřejný	k2eAgFnSc2d1	veřejná
popravy	poprava	k1gFnSc2	poprava
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
konaly	konat	k5eAaImAgInP	konat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
veřejná	veřejný	k2eAgFnSc1d1	veřejná
poprava	poprava	k1gFnSc1	poprava
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Gilotinou	gilotina	k1gFnSc7	gilotina
se	se	k3xPyFc4	se
popravovalo	popravovat	k5eAaImAgNnS	popravovat
i	i	k9	i
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
popraveným	popravený	k2eAgMnSc7d1	popravený
byl	být	k5eAaImAgMnS	být
Hamida	Hamid	k1gMnSc4	Hamid
Djandoubi	Djandoubi	k1gNnSc7	Djandoubi
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1981	[number]	k4	1981
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
je	být	k5eAaImIp3nS	být
připoután	připoutat	k5eAaPmNgMnS	připoutat
na	na	k7c4	na
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
desku	deska	k1gFnSc4	deska
(	(	kIx(	(
<g/>
bascule	bascule	k1gFnSc1	bascule
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
přečnívala	přečnívat	k5eAaImAgFnS	přečnívat
přes	přes	k7c4	přes
její	její	k3xOp3gInSc4	její
okraj	okraj	k1gInSc4	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
deska	deska	k1gFnSc1	deska
překlopí	překlopit	k5eAaPmIp3nS	překlopit
do	do	k7c2	do
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
nasune	nasunout	k5eAaPmIp3nS	nasunout
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
pod	pod	k7c4	pod
pár	pár	k4xCyI	pár
vysokých	vysoký	k2eAgInPc2d1	vysoký
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
kovových	kovový	k2eAgFnPc6d1	kovová
kolejničkách	kolejnička	k1gFnPc6	kolejnička
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
čepel	čepel	k1gFnSc1	čepel
gilotiny	gilotina	k1gFnSc2	gilotina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
vlastní	vlastní	k2eAgFnSc1d1	vlastní
sekera	sekera	k1gFnSc1	sekera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
je	být	k5eAaImIp3nS	být
připevněno	připevněn	k2eAgNnSc1d1	připevněno
zhruba	zhruba	k6eAd1	zhruba
šedesát	šedesát	k4xCc4	šedesát
kilogramů	kilogram	k1gInPc2	kilogram
těžké	těžký	k2eAgNnSc4d1	těžké
závaží	závaží	k1gNnSc4	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzencův	odsouzencův	k2eAgInSc1d1	odsouzencův
krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
znehybněn	znehybnit	k5eAaPmNgInS	znehybnit
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
deskou	deska	k1gFnSc7	deska
s	s	k7c7	s
otvorem	otvor	k1gInSc7	otvor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
(	(	kIx(	(
<g/>
lunette	lunette	k5eAaPmIp2nP	lunette
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
popravčí	popravčí	k1gMnSc1	popravčí
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
zarážku	zarážka	k1gFnSc4	zarážka
a	a	k8xC	a
čepel	čepel	k1gFnSc4	čepel
sekery	sekera	k1gFnSc2	sekera
se	s	k7c7	s
závažím	závaží	k1gNnSc7	závaží
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
na	na	k7c4	na
krk	krk	k1gInSc4	krk
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
bývá	bývat	k5eAaImIp3nS	bývat
takto	takto	k6eAd1	takto
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
zpravidla	zpravidla	k6eAd1	zpravidla
mezi	mezi	k7c7	mezi
třetím	třetí	k4xOgInSc7	třetí
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
krčním	krční	k2eAgInSc7d1	krční
obratlem	obratel	k1gInSc7	obratel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
prodělala	prodělat	k5eAaPmAgFnS	prodělat
gilotina	gilotina	k1gFnSc1	gilotina
různé	různý	k2eAgFnPc4d1	různá
drobné	drobný	k2eAgFnPc4d1	drobná
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
mírná	mírný	k2eAgNnPc4d1	mírné
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
–	–	k?	–
například	například	k6eAd1	například
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
gumové	gumový	k2eAgInPc1d1	gumový
tlumiče	tlumič	k1gInPc1	tlumič
pádu	pád	k1gInSc2	pád
ostří	ostří	k1gNnSc1	ostří
<g/>
,	,	kIx,	,
lano	lano	k1gNnSc1	lano
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
převodovým	převodový	k2eAgInSc7d1	převodový
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
gilotina	gilotina	k1gFnSc1	gilotina
také	také	k9	také
neblaze	blaze	k6eNd1	blaze
proslula	proslout	k5eAaPmAgFnS	proslout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
němečtí	německý	k2eAgMnPc1d1	německý
nacisté	nacista	k1gMnPc1	nacista
aktivně	aktivně	k6eAd1	aktivně
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
popravám	poprava	k1gFnPc3	poprava
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
resp.	resp.	kA	resp.
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
neblaze	blaze	k6eNd1	blaze
proslulá	proslulý	k2eAgFnSc1d1	proslulá
Pankrácká	pankrácký	k2eAgFnSc1d1	Pankrácká
sekyrárna	sekyrárna	k1gFnSc1	sekyrárna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Praze-Nuslích	Praze-Nusle	k1gFnPc6	Praze-Nusle
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gilotina	gilotina	k1gFnSc1	gilotina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1793	[number]	k4	1793
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
královna	královna	k1gFnSc1	královna
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1898	[number]	k4	1898
Joseph	Josepha	k1gFnPc2	Josepha
Vacher	Vachra	k1gFnPc2	Vachra
<g/>
,	,	kIx,	,
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
2	[number]	k4	2
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
1931	[number]	k4	1931
Peter	Peter	k1gMnSc1	Peter
Kürten	Kürten	k2eAgMnSc1d1	Kürten
<g/>
,	,	kIx,	,
sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
</s>
