<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
pramenem	pramen	k1gInSc7	pramen
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
Sókratovi	Sókrat	k1gMnSc6	Sókrat
jsou	být	k5eAaImIp3nP	být
spisy	spis	k1gInPc4	spis
dvou	dva	k4xCgMnPc6	dva
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
–	–	k?	–
Xenofónta	Xenofónt	k1gMnSc4	Xenofónt
a	a	k8xC	a
Platóna	Platón	k1gMnSc4	Platón
(	(	kIx(	(
<g/>
Obrana	obrana	k1gFnSc1	obrana
Sókratova	Sókratův	k2eAgFnSc1d1	Sókratova
<g/>
,	,	kIx,	,
Kritón	Kritón	k1gInSc1	Kritón
<g/>
,	,	kIx,	,
Faidón	Faidón	k1gInSc1	Faidón
<g/>
,	,	kIx,	,
Symposion	symposion	k1gNnSc1	symposion
<g/>
,	,	kIx,	,
Theaitétos	Theaitétos	k1gInSc1	Theaitétos
<g/>
,	,	kIx,	,
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
