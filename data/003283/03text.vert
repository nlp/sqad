<s>
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gFnSc1	Priest
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
heavy	heava	k1gFnSc2	heava
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
zformovaná	zformovaný	k2eAgFnSc1d1	zformovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Birminghamu	Birmingham	k1gInSc6	Birmingham
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rocka	Rocek	k1gMnSc2	Rocek
Rolla	Roll	k1gMnSc2	Roll
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
a	a	k8xC	a
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Stained	Stained	k1gMnSc1	Stained
Class	Class	k1gInSc1	Class
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
stále	stále	k6eAd1	stále
drží	držet	k5eAaImIp3nS	držet
na	na	k7c6	na
metalové	metalový	k2eAgFnSc6d1	metalová
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
Judas	Judas	k1gInSc4	Judas
Priest	Priest	k1gFnSc1	Priest
nahráli	nahrát	k5eAaBmAgMnP	nahrát
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
alb	album	k1gNnPc2	album
jako	jako	k8xS	jako
British	Britisha	k1gFnPc2	Britisha
Steel	Steela	k1gFnPc2	Steela
<g/>
,	,	kIx,	,
Screaming	Screaming	k1gInSc1	Screaming
for	forum	k1gNnPc2	forum
Vengeance	Vengeance	k1gFnSc2	Vengeance
či	či	k8xC	či
Turbo	turba	k1gFnSc5	turba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
singlů	singl	k1gInPc2	singl
vyniká	vynikat	k5eAaImIp3nS	vynikat
například	například	k6eAd1	například
Breaking	Breaking	k1gInSc1	Breaking
the	the	k?	the
Law	Law	k1gFnSc2	Law
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
také	také	k9	také
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
typickou	typický	k2eAgFnSc4d1	typická
metalovou	metalový	k2eAgFnSc4d1	metalová
módu	móda	k1gFnSc4	móda
<g/>
,	,	kIx,	,
Rob	roba	k1gFnPc2	roba
Halford	Halforda	k1gFnPc2	Halforda
poprvé	poprvé	k6eAd1	poprvé
předvedl	předvést	k5eAaPmAgMnS	předvést
kožené	kožený	k2eAgNnSc4d1	kožené
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
cvočky	cvoček	k1gInPc1	cvoček
<g/>
,	,	kIx,	,
pyramidy	pyramida	k1gFnPc1	pyramida
atd.	atd.	kA	atd.
Inspiraci	inspirace	k1gFnSc4	inspirace
hledal	hledat	k5eAaImAgInS	hledat
v	v	k7c6	v
gay	gay	k1gMnSc1	gay
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
<g/>
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
popularity	popularita	k1gFnSc2	popularita
punkové	punkový	k2eAgInPc1d1	punkový
mody	modus	k1gInPc1	modus
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
glam	glam	k6eAd1	glam
rockových	rockový	k2eAgMnPc2d1	rockový
hudebníků	hudebník	k1gMnPc2	hudebník
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
nebo	nebo	k8xC	nebo
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
K.	K.	kA	K.
Downing	Downing	k1gInSc1	Downing
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
a	a	k8xC	a
John	John	k1gMnSc1	John
Ellis	Ellis	k1gFnSc2	Ellis
se	se	k3xPyFc4	se
znali	znát	k5eAaImAgMnP	znát
již	již	k9	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
vyrůstali	vyrůstat	k5eAaImAgMnP	vyrůstat
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Yew	Yew	k1gMnSc2	Yew
Tree	Tre	k1gMnSc2	Tre
estate	estat	k1gInSc5	estat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
West	West	k1gMnSc1	West
Bromwich	Bromwich	k1gMnSc1	Bromwich
<g/>
.	.	kIx.	.
</s>
<s>
Navstěvovali	Navstěvovat	k5eAaBmAgMnP	Navstěvovat
Churchfields	Churchfields	k1gInSc4	Churchfields
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
All	All	k1gFnSc6	All
Saints	Saintsa	k1gFnPc2	Saintsa
ve	v	k7c4	v
West	West	k1gInSc4	West
Bromwichi	Bromwich	k1gFnSc2	Bromwich
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
a	a	k8xC	a
Hill	Hill	k1gInSc1	Hill
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
sdíleli	sdílet	k5eAaImAgMnP	sdílet
podobné	podobný	k2eAgInPc4d1	podobný
hudební	hudební	k2eAgInPc4d1	hudební
zájmy	zájem	k1gInPc4	zájem
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
,	,	kIx,	,
Cream	Cream	k1gInSc1	Cream
<g/>
,	,	kIx,	,
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
<g/>
)	)	kIx)	)
a	a	k8xC	a
naučili	naučit	k5eAaPmAgMnP	naučit
se	se	k3xPyFc4	se
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1970	[number]	k4	1970
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
po	po	k7c6	po
místním	místní	k2eAgInSc6d1	místní
rozpadlém	rozpadlý	k2eAgInSc6d1	rozpadlý
souboru	soubor	k1gInSc6	soubor
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
Dylanovy	Dylanův	k2eAgFnSc2d1	Dylanova
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
Frankie	Frankie	k1gFnSc1	Frankie
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
Judas	Judas	k1gInSc4	Judas
Priest	Priest	k1gInSc1	Priest
<g/>
"	"	kIx"	"
Původní	původní	k2eAgInSc1d1	původní
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gMnSc1	Priest
byli	být	k5eAaImAgMnP	být
založeny	založit	k5eAaPmNgFnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
Alem	alma	k1gFnPc2	alma
Atkinsem	Atkins	k1gInSc7	Atkins
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Stapenhillem	Stapenhill	k1gInSc7	Stapenhill
(	(	kIx(	(
<g/>
basa	basa	k1gFnSc1	basa
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xC	jako
Brian	Brian	k1gMnSc1	Brian
Stapenhill	Stapenhill	k1gMnSc1	Stapenhill
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Cross	Cross	k1gInSc1	Cross
<g/>
,	,	kIx,	,
W.	W.	kA	W.
Bromwich	Bromwich	k1gInSc1	Bromwich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Johnem	John	k1gMnSc7	John
Partridgem	Partridg	k1gMnSc7	Partridg
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc2d1	bicí
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
c.	c.	k?	c.
1948	[number]	k4	1948
,	,	kIx,	,
W.	W.	kA	W.
Bromwich	Bromwich	k1gInSc1	Bromwich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Perrym	Perrym	k1gInSc1	Perrym
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stappenhill	Stappenhill	k1gMnSc1	Stappenhill
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gInSc1	Priest
<g/>
"	"	kIx"	"
a	a	k8xC	a
nacvičovali	nacvičovat	k5eAaImAgMnP	nacvičovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domě	dům	k1gInSc6	dům
ve	v	k7c6	v
Stone	ston	k1gInSc5	ston
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
Perry	Perra	k1gFnPc4	Perra
zemřel	zemřít	k5eAaPmAgInS	zemřít
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
nehodě	nehoda	k1gFnSc6	nehoda
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
Erniem	Ernius	k1gMnSc7	Ernius
Chatawayem	Chataway	k1gInSc7	Chataway
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
Ernest	Ernest	k1gMnSc1	Ernest
Chataway	Chatawaa	k1gFnSc2	Chatawaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
ve	v	k7c4	v
Winson	Winson	k1gNnSc4	Winson
Green	Grena	k1gFnPc2	Grena
<g/>
,	,	kIx,	,
Birmingham	Birmingham	k1gInSc1	Birmingham
<g/>
,	,	kIx,	,
Warwickshire	Warwickshir	k1gMnSc5	Warwickshir
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1969	[number]	k4	1969
v	v	k7c6	v
George	George	k1gNnSc6	George
Hotelu	hotel	k1gInSc2	hotel
ve	v	k7c6	v
Walsallu	Walsall	k1gInSc6	Walsall
ve	v	k7c6	v
Staffordshire	Staffordshir	k1gInSc5	Staffordshir
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
prosinec	prosinec	k1gInSc4	prosinec
1969	[number]	k4	1969
a	a	k8xC	a
leden	leden	k1gInSc4	leden
1970	[number]	k4	1970
odjela	odjet	k5eAaPmAgFnS	odjet
na	na	k7c4	na
tour	tour	k1gInSc4	tour
do	do	k7c2	do
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
posledním	poslední	k2eAgInSc6d1	poslední
koncertu	koncert	k1gInSc6	koncert
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
The	The	k1gFnSc6	The
Youth	Youtha	k1gFnPc2	Youtha
Centre	centr	k1gInSc5	centr
v	v	k7c6	v
Cannocku	Cannocka	k1gFnSc4	Cannocka
ve	v	k7c6	v
Staffordshire	Staffordshir	k1gMnSc5	Staffordshir
<g/>
.	.	kIx.	.
</s>
<s>
Atkins	Atkins	k6eAd1	Atkins
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
sestavou	sestava	k1gFnSc7	sestava
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
s	s	k7c7	s
názvem	název	k1gInSc7	název
St.	st.	kA	st.
James	James	k1gInSc1	James
v	v	k7c4	v
Wednesbury	Wednesbur	k1gInPc4	Wednesbur
poblíž	poblíž	k6eAd1	poblíž
W.	W.	kA	W.
Bromwiche	Bromwich	k1gInPc1	Bromwich
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
místními	místní	k2eAgFnPc7d1	místní
nazýváno	nazýván	k2eAgNnSc4d1	nazýváno
Holy	hola	k1gFnPc4	hola
Joe	Joe	k1gFnSc2	Joe
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Atkins	Atkins	k1gInSc1	Atkins
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Kennym	Kennym	k1gInSc4	Kennym
Downingem	Downing	k1gInSc7	Downing
<g/>
,	,	kIx,	,
basistou	basista	k1gMnSc7	basista
Ianem	Ianus	k1gMnSc7	Ianus
'	'	kIx"	'
<g/>
Skull	Skull	k1gMnSc1	Skull
<g/>
'	'	kIx"	'
Hillem	Hill	k1gMnSc7	Hill
a	a	k8xC	a
bubeníkem	bubeník	k1gMnSc7	bubeník
Johnem	John	k1gMnSc7	John
Ellisem	Ellis	k1gInSc7	Ellis
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1951	[number]	k4	1951
v	v	k7c6	v
Yew	Yew	k1gFnSc6	Yew
Tree	Treus	k1gMnSc5	Treus
Estate	Estat	k1gMnSc5	Estat
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
Bromwich	Bromwich	k1gInSc1	Bromwich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
kapelu	kapela	k1gFnSc4	kapela
Freight	Freighta	k1gFnPc2	Freighta
(	(	kIx(	(
<g/>
duben-říjen	duben-říjen	k2eAgInSc1d1	duben-říjen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
zpěváka	zpěvák	k1gMnSc4	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Atkins	Atkins	k1gInSc1	Atkins
připojí	připojit	k5eAaPmIp3nS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
název	název	k1gInSc4	název
své	svůj	k3xOyFgFnSc2	svůj
staré	starý	k2eAgFnSc2d1	stará
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Atkinsovy	Atkinsův	k2eAgFnSc2d1	Atkinsova
tety	teta	k1gFnSc2	teta
ve	v	k7c6	v
Stone	ston	k1gInSc5	ston
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
sestava	sestava	k1gFnSc1	sestava
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
Downing	Downing	k1gInSc1	Downing
<g/>
,	,	kIx,	,
Hill	Hill	k1gInSc1	Hill
a	a	k8xC	a
Ellis	Ellis	k1gInSc1	Ellis
hrál	hrát	k5eAaImAgInS	hrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1971	[number]	k4	1971
v	v	k7c6	v
St.	st.	kA	st.
John	John	k1gMnSc1	John
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
Essington	Essington	k1gInSc1	Essington
ve	v	k7c6	v
Staffordshire	Staffordshir	k1gInSc5	Staffordshir
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Downingem	Downing	k1gInSc7	Downing
jako	jako	k9	jako
zastupujícím	zastupující	k2eAgMnSc7d1	zastupující
vůdcem	vůdce	k1gMnSc7	vůdce
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vzdálila	vzdálit	k5eAaPmAgFnS	vzdálit
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
původních	původní	k2eAgInPc2d1	původní
bluesových	bluesový	k2eAgInPc2d1	bluesový
vlivů	vliv	k1gInPc2	vliv
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kvartet	kvartet	k1gInSc1	kvartet
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
bubeníky	bubeník	k1gMnPc7	bubeník
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
hráli	hrát	k5eAaImAgMnP	hrát
jako	jako	k8xS	jako
předkapela	předkapela	k1gFnSc1	předkapela
pro	pro	k7c4	pro
např.	např.	kA	např.
Budgie	Budgie	k1gFnSc2	Budgie
<g/>
,	,	kIx,	,
Thin	Thin	k1gMnSc1	Thin
Lizzy	Lizza	k1gFnSc2	Lizza
a	a	k8xC	a
Trapeze	Trapeze	k1gFnSc2	Trapeze
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
finanční	finanční	k2eAgFnPc4d1	finanční
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
(	(	kIx(	(
<g/>
společnost	společnost	k1gFnSc4	společnost
Tony	Tony	k1gFnSc2	Tony
Iommiho	Iommi	k1gMnSc2	Iommi
IMA	IMA	kA	IMA
<g/>
)	)	kIx)	)
dovedly	dovést	k5eAaPmAgFnP	dovést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
Alana	Alan	k1gMnSc2	Alan
Atkinse	Atkins	k1gMnSc2	Atkins
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc2	bubeník
Alana	Alan	k1gMnSc2	Alan
Moora	Moor	k1gMnSc2	Moor
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Ian	Ian	k1gMnSc1	Ian
Hill	Hill	k1gMnSc1	Hill
chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
Walsall	Walsalla	k1gFnPc2	Walsalla
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Roba	roba	k1gFnSc1	roba
Halforda	Halford	k1gMnSc2	Halford
jako	jako	k8xS	jako
možného	možný	k2eAgMnSc2d1	možný
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Halford	Halford	k6eAd1	Halford
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
připojil	připojit	k5eAaPmAgInS	připojit
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
bubeník	bubeník	k1gMnSc1	bubeník
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
předchozí	předchozí	k2eAgFnSc2d1	předchozí
kapely	kapela	k1gFnSc2	kapela
Hiroshima	Hiroshima	k1gFnSc1	Hiroshima
-	-	kIx~	-
John	John	k1gMnSc1	John
Hinch	Hinch	k1gMnSc1	Hinch
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
jela	jet	k5eAaImAgFnS	jet
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
pro	pro	k7c4	pro
Budgie	Budgie	k1gFnPc4	Budgie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hvězda	hvězda	k1gFnSc1	hvězda
několika	několik	k4yIc2	několik
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
kapela	kapela	k1gFnSc1	kapela
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahrála	nahrát	k5eAaBmAgFnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
jejich	jejich	k3xOp3gFnSc1	jejich
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
přidat	přidat	k5eAaPmF	přidat
dalšího	další	k2eAgMnSc4d1	další
hudebníka	hudebník	k1gMnSc4	hudebník
do	do	k7c2	do
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Downing	Downing	k1gInSc1	Downing
byl	být	k5eAaImAgInS	být
neochotný	ochotný	k2eNgMnSc1d1	neochotný
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
klávesy	klávesa	k1gFnPc4	klávesa
nebo	nebo	k8xC	nebo
žestě	žestě	k1gFnPc4	žestě
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1974	[number]	k4	1974
vybral	vybrat	k5eAaPmAgMnS	vybrat
dalšího	další	k2eAgMnSc4d1	další
kytaristu	kytarista	k1gMnSc4	kytarista
-	-	kIx~	-
Glenna	Glenn	k1gMnSc4	Glenn
Tiptona	Tipton	k1gMnSc4	Tipton
ze	z	k7c2	z
Staffordské	Staffordský	k2eAgFnSc2d1	Staffordský
skupiny	skupina	k1gFnSc2	skupina
Flying	Flying	k1gInSc1	Flying
Hat	hat	k0	hat
Band	banda	k1gFnPc2	banda
jako	jako	k8xC	jako
nového	nový	k2eAgMnSc2d1	nový
člena	člen	k1gMnSc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
kytaristé	kytarista	k1gMnPc1	kytarista
společně	společně	k6eAd1	společně
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
úpravě	úprava	k1gFnSc6	úprava
již	již	k6eAd1	již
existujícího	existující	k2eAgInSc2d1	existující
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
Tipton	Tipton	k1gInSc1	Tipton
získal	získat	k5eAaPmAgInS	získat
podíl	podíl	k1gInSc4	podíl
jako	jako	k9	jako
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1974	[number]	k4	1974
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
svůj	svůj	k3xOyFgInSc4	svůj
debutový	debutový	k2eAgInSc4d1	debutový
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Rocka	Rocka	k1gMnSc1	Rocka
Rolla	Rolla	k1gMnSc1	Rolla
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgInPc1d1	technický
problémy	problém	k1gInPc1	problém
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
na	na	k7c6	na
špatné	špatný	k2eAgFnSc6d1	špatná
kvalitě	kvalita	k1gFnSc6	kvalita
zvuku	zvuk	k1gInSc2	zvuk
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
Rodger	Rodger	k1gMnSc1	Rodger
Bain	Bain	k1gMnSc1	Bain
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
třech	tři	k4xCgNnPc6	tři
albech	album	k1gNnPc6	album
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
albu	album	k1gNnSc6	album
Budgie	Budgie	k1gFnSc1	Budgie
dominoval	dominovat	k5eAaImAgInS	dominovat
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
alba	album	k1gNnSc2	album
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gNnPc7	jeho
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
kapela	kapela	k1gFnSc1	kapela
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
<g/>
Bain	Bain	k1gMnSc1	Bain
rovněž	rovněž	k9	rovněž
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
z	z	k7c2	z
alba	album	k1gNnSc2	album
vyškrtnout	vyškrtnout	k5eAaPmF	vyškrtnout
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
skladby	skladba	k1gFnPc4	skladba
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
ze	z	k7c2	z
živých	živý	k2eAgFnPc2d1	živá
vystoupení	vystoupení	k1gNnPc2	vystoupení
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Tyrant	Tyrant	k1gInSc1	Tyrant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Genocide	Genocid	k1gInSc5	Genocid
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ripper	Ripper	k1gMnSc1	Ripper
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nechal	nechat	k5eAaPmAgMnS	nechat
sestříhat	sestříhat	k5eAaPmF	sestříhat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Caviar	Caviar	k1gInSc1	Caviar
and	and	k?	and
Meths	Meths	k1gInSc1	Meths
<g/>
"	"	kIx"	"
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
10	[number]	k4	10
<g/>
-minutové	inutový	k2eAgFnPc4d1	-minutový
skladby	skladba	k1gFnPc4	skladba
na	na	k7c4	na
2	[number]	k4	2
minutovou	minutový	k2eAgFnSc4d1	minutová
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc1	Tour
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Rocka	Rocka	k1gMnSc1	Rocka
Rolla	Rolla	k1gMnSc1	Rolla
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Judas	Judas	k1gInSc4	Judas
Priest	Priest	k1gFnSc4	Priest
první	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
turnés	turnés	k1gInSc1	turnés
vystoupeními	vystoupení	k1gNnPc7	vystoupení
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jednoho	jeden	k4xCgInSc2	jeden
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Klubbenv	Klubbenva	k1gFnPc2	Klubbenva
Tönsbergu	Tönsberg	k1gInSc2	Tönsberg
<g/>
,	,	kIx,	,
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
od	od	k7c2	od
Osla	Oslo	k1gNnSc2	Oslo
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
místní	místní	k2eAgInPc1d1	místní
tisk	tisk	k1gInSc1	tisk
odměnil	odměnit	k5eAaPmAgInS	odměnit
poněkud	poněkud	k6eAd1	poněkud
negativní	negativní	k2eAgInSc4d1	negativní
recenzíAlbum	recenzíAlbum	k1gInSc4	recenzíAlbum
bylo	být	k5eAaImAgNnS	být
propadákem	propadák	k1gInSc7	propadák
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
finanční	finanční	k2eAgFnSc6d1	finanční
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Pokusila	pokusit	k5eAaPmAgFnS	pokusit
si	se	k3xPyFc3	se
zajistit	zajistit	k5eAaPmF	zajistit
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Gull	Gullum	k1gNnPc2	Gullum
Records	Records	k1gInSc4	Records
získat	získat	k5eAaPmF	získat
měsíční	měsíční	k2eAgInSc4d1	měsíční
plat	plat	k1gInSc4	plat
50	[number]	k4	50
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
společnost	společnost	k1gFnSc1	společnost
Gull	Gulla	k1gFnPc2	Gulla
Records	Recordsa	k1gFnPc2	Recordsa
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byla	být	k5eAaImAgFnS	být
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
tedy	tedy	k9	tedy
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
Album	album	k1gNnSc4	album
Rocka	Rocek	k1gMnSc2	Rocek
Rolla	Roll	k1gMnSc2	Roll
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
kapelou	kapela	k1gFnSc7	kapela
zavrženo	zavrhnout	k5eAaPmNgNnS	zavrhnout
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
písní	píseň	k1gFnPc2	píseň
nebyla	být	k5eNaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
hrána	hrát	k5eAaImNgFnS	hrát
živě	živě	k6eAd1	živě
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
jejich	jejich	k3xOp3gNnSc2	jejich
dalšího	další	k2eAgNnSc2d1	další
alba	album	k1gNnSc2	album
podílela	podílet	k5eAaImAgFnS	podílet
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
během	během	k7c2	během
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
vybrala	vybrat	k5eAaPmAgFnS	vybrat
i	i	k9	i
producenta	producent	k1gMnSc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
Sad	sad	k1gInSc1	sad
Wings	Wings	k1gInSc1	Wings
of	of	k?	of
Destiny	Destina	k1gFnSc2	Destina
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
mnoho	mnoho	k6eAd1	mnoho
staršího	starý	k2eAgInSc2d2	starší
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
posunul	posunout	k5eAaPmAgMnS	posunout
kapelu	kapela	k1gFnSc4	kapela
z	z	k7c2	z
psychedelického	psychedelický	k2eAgInSc2d1	psychedelický
zvuku	zvuk	k1gInSc2	zvuk
do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
<g/>
,	,	kIx,	,
drsnějšího	drsný	k2eAgInSc2d2	drsnější
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
úvodní	úvodní	k2eAgFnSc7d1	úvodní
skladbou	skladba	k1gFnSc7	skladba
<g/>
,	,	kIx,	,
progresivním	progresivní	k2eAgInSc7d1	progresivní
eposem	epos	k1gInSc7	epos
"	"	kIx"	"
<g/>
Victim	Victim	k1gInSc1	Victim
of	of	k?	of
Changes	Changes	k1gInSc1	Changes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
kombinací	kombinace	k1gFnSc7	kombinace
skladeb	skladba	k1gFnPc2	skladba
"	"	kIx"	"
<g/>
Whiskey	Whiskey	k1gInPc1	Whiskey
Woman	Womana	k1gFnPc2	Womana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
klasiky	klasika	k1gFnPc1	klasika
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Ala	ala	k1gFnSc1	ala
Atkinse	Atkinse	k1gFnSc2	Atkinse
a	a	k8xC	a
"	"	kIx"	"
<g/>
Red	Red	k1gFnSc1	Red
Light	Light	k1gInSc1	Light
Lady	lady	k1gFnSc1	lady
<g/>
"	"	kIx"	"
-	-	kIx~	-
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Halford	Halford	k1gMnSc1	Halford
napsal	napsat	k5eAaBmAgMnS	napsat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
předchozí	předchozí	k2eAgFnSc7d1	předchozí
skupinou	skupina	k1gFnSc7	skupina
Hiroshima	Hiroshimum	k1gNnSc2	Hiroshimum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
a	a	k8xC	a
silné	silný	k2eAgNnSc1d1	silné
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Reading	Reading	k1gInSc4	Reading
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
zvýšit	zvýšit	k5eAaPmF	zvýšit
širší	široký	k2eAgInSc4d2	širší
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
rozšířit	rozšířit	k5eAaPmF	rozšířit
fanouškovskou	fanouškovský	k2eAgFnSc4d1	fanouškovská
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
Sin	sin	kA	sin
After	Aftero	k1gNnPc2	Aftero
Sin	sin	kA	sin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
nahráli	nahrát	k5eAaBmAgMnP	nahrát
pod	pod	k7c7	pod
velkou	velký	k2eAgFnSc7d1	velká
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
-	-	kIx~	-
CBS	CBS	kA	CBS
a	a	k8xC	a
první	první	k4xOgInSc4	první
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dostalo	dostat	k5eAaPmAgNnS	dostat
certifikaci	certifikace	k1gFnSc4	certifikace
Gold	Gold	k1gInSc4	Gold
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgNnSc4d2	vyšší
(	(	kIx(	(
<g/>
hodnocení	hodnocení	k1gNnSc4	hodnocení
RIAA	RIAA	kA	RIAA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odchodem	odchod	k1gInSc7	odchod
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
společnosti	společnost	k1gFnSc2	společnost
-	-	kIx~	-
Gull	Gulla	k1gFnPc2	Gulla
ztratila	ztratit	k5eAaPmAgFnS	ztratit
kapela	kapela	k1gFnSc1	kapela
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Sin	sin	kA	sin
After	After	k1gMnSc1	After
Sin	sin	kA	sin
produkoval	produkovat	k5eAaImAgMnS	produkovat
dřívější	dřívější	k2eAgMnSc1d1	dřívější
baskytarista	baskytarista	k1gMnSc1	baskytarista
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
Roger	Roger	k1gMnSc1	Roger
Glover	Glover	k1gMnSc1	Glover
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
najmout	najmout	k5eAaPmF	najmout
studiového	studiový	k2eAgMnSc4d1	studiový
bubeníka	bubeník	k1gMnSc4	bubeník
Simona	Simon	k1gMnSc4	Simon
Phillipse	Phillips	k1gMnSc4	Phillips
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gInSc1	Priest
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
Les	les	k1gInSc4	les
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Leslie	Leslie	k1gFnSc2	Leslie
<g/>
)	)	kIx)	)
Binks	Binks	k1gInSc1	Binks
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
ohromena	ohromit	k5eAaPmNgFnS	ohromit
jeho	jeho	k3xOp3gInSc7	jeho
výkonem	výkon	k1gInSc7	výkon
a	a	k8xC	a
požádala	požádat	k5eAaPmAgFnS	požádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
nahráli	nahrát	k5eAaBmAgMnP	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
desky	deska	k1gFnSc2	deska
Stained	Stained	k1gMnSc1	Stained
Class	Class	k1gInSc1	Class
a	a	k8xC	a
Killing	Killing	k1gInSc1	Killing
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
vydána	vydat	k5eAaPmNgFnS	vydat
jako	jako	k8xC	jako
Hell	Hell	k1gInSc1	Hell
Bent	Benta	k1gFnPc2	Benta
for	forum	k1gNnPc2	forum
Leather	Leathra	k1gFnPc2	Leathra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Binks	Binks	k1gInSc1	Binks
<g/>
,	,	kIx,	,
uvedený	uvedený	k2eAgInSc1d1	uvedený
i	i	k9	i
jako	jako	k8xC	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc1d1	silná
balady	balada	k1gFnPc1	balada
"	"	kIx"	"
<g/>
Beyond	Beyond	k1gInSc1	Beyond
the	the	k?	the
Realms	Realms	k1gInSc1	Realms
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
skvělý	skvělý	k2eAgInSc1d1	skvělý
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
zručný	zručný	k2eAgMnSc1d1	zručný
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
schopnosti	schopnost	k1gFnPc1	schopnost
přidaly	přidat	k5eAaPmAgFnP	přidat
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
pestřejší	pestrý	k2eAgInSc4d2	pestřejší
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Binks	Binks	k1gInSc1	Binks
hrál	hrát	k5eAaImAgInS	hrát
také	také	k9	také
na	na	k7c6	na
albu	album	k1gNnSc6	album
Unleashed	Unleashed	k1gMnSc1	Unleashed
in	in	k?	in
the	the	k?	the
East	Eastum	k1gNnPc2	Eastum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
živě	živě	k6eAd1	živě
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
Killing	Killing	k1gInSc1	Killing
Machine	Machin	k1gInSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
první	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
měla	mít	k5eAaImAgFnS	mít
značné	značný	k2eAgFnPc4d1	značná
stopy	stopa	k1gFnPc4	stopa
vlivu	vliv	k1gInSc2	vliv
Black	Black	k1gInSc1	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
balady	balada	k1gFnPc4	balada
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
deska	deska	k1gFnSc1	deska
Stained	Stained	k1gInSc4	Stained
Class	Class	k1gInSc4	Class
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
balady	balada	k1gFnSc2	balada
žádné	žádný	k3yNgNnSc1	žádný
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
velmi	velmi	k6eAd1	velmi
temné	temný	k2eAgFnSc2d1	temná
"	"	kIx"	"
<g/>
Beyond	Beyonda	k1gFnPc2	Beyonda
the	the	k?	the
Realms	Realms	k1gInSc1	Realms
Death	Death	k1gInSc1	Death
<g/>
"	"	kIx"	"
a	a	k8xC	a
deska	deska	k1gFnSc1	deska
Killing	Killing	k1gInSc1	Killing
Machine	Machin	k1gInSc5	Machin
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc4	první
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
znát	znát	k5eAaImF	znát
komerčnější	komerční	k2eAgInSc1d2	komerčnější
zvuk	zvuk	k1gInSc1	zvuk
s	s	k7c7	s
jednoduššími	jednoduchý	k2eAgFnPc7d2	jednodušší
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
něco	něco	k6eAd1	něco
málo	málo	k1gNnSc4	málo
vlivu	vliv	k1gInSc2	vliv
blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
přijali	přijmout	k5eAaPmAgMnP	přijmout
jejich	jejich	k3xOp3gFnSc4	jejich
dnes	dnes	k6eAd1	dnes
proslulou	proslulý	k2eAgFnSc4d1	proslulá
Leather	Leathra	k1gFnPc2	Leathra
Image	image	k1gInSc1	image
(	(	kIx(	(
<g/>
Kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Halford	Halford	k6eAd1	Halford
již	již	k9	již
jen	jen	k9	jen
dopiloval	dopilovat	k5eAaPmAgMnS	dopilovat
svou	svůj	k3xOyFgFnSc7	svůj
show	show	k1gFnSc7	show
k	k	k7c3	k
naprosté	naprostý	k2eAgFnSc3d1	naprostá
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
při	při	k7c6	při
skladbě	skladba	k1gFnSc6	skladba
Hell	Hell	k1gMnSc1	Hell
Bent	Bent	k1gMnSc1	Bent
for	forum	k1gNnPc2	forum
Leather	Leathra	k1gFnPc2	Leathra
<g/>
,	,	kIx,	,
vřítí	vřítit	k5eAaPmIp3nS	vřítit
s	s	k7c7	s
Harley-Davidsonem	Harley-Davidson	k1gInSc7	Harley-Davidson
<g/>
...	...	k?	...
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
vystoupení	vystoupení	k1gNnSc2	vystoupení
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Killing	Killing	k1gInSc1	Killing
Machine	Machin	k1gInSc5	Machin
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
z	z	k7c2	z
tour	toura	k1gFnPc2	toura
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Unleashed	Unleashed	k1gInSc4	Unleashed
in	in	k?	in
the	the	k?	the
East	East	k1gInSc1	East
-	-	kIx~	-
první	první	k4xOgInPc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
alb	album	k1gNnPc2	album
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
získalo	získat	k5eAaPmAgNnS	získat
platinu	platina	k1gFnSc4	platina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
kritika	kritika	k1gFnSc1	kritika
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
používá	používat	k5eAaImIp3nS	používat
studiová	studiový	k2eAgNnPc4d1	studiové
aranžmá	aranžmá	k1gNnPc4	aranžmá
a	a	k8xC	a
playback	playback	k1gInSc4	playback
k	k	k7c3	k
vylepšení	vylepšení	k1gNnSc3	vylepšení
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
následně	následně	k6eAd1	následně
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k8xS	jako
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
styl	styl	k1gInSc1	styl
hraní	hranit	k5eAaImIp3nS	hranit
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
v	v	k7c4	v
těžší	těžký	k2eAgInSc4d2	těžší
<g/>
,	,	kIx,	,
hrubší	hrubý	k2eAgInSc4d2	hrubší
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
živé	živý	k2eAgFnSc2d1	živá
verze	verze	k1gFnSc2	verze
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Exciter	Exciter	k1gInSc1	Exciter
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Beyond	Beyond	k1gInSc1	Beyond
The	The	k1gFnPc2	The
Realms	Realms	k1gInSc4	Realms
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
"	"	kIx"	"
zněly	znět	k5eAaImAgInP	znět
mnohem	mnohem	k6eAd1	mnohem
drsněji	drsně	k6eAd2	drsně
než	než	k8xS	než
jejich	jejich	k3xOp3gFnPc1	jejich
verze	verze	k1gFnPc1	verze
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
Binks	Binks	k1gInSc1	Binks
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nebyl	být	k5eNaImAgMnS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jednoduššímu	jednoduchý	k2eAgInSc3d2	jednodušší
zvuku	zvuk	k1gInSc3	zvuk
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
rádia	rádio	k1gNnSc2	rádio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Dave	Dav	k1gInSc5	Dav
Holland	Hollando	k1gNnPc2	Hollando
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Trapeze	Trapeze	k1gFnSc2	Trapeze
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
sestavě	sestava	k1gFnSc3	sestava
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
nahráli	nahrát	k5eAaPmAgMnP	nahrát
šest	šest	k4xCc4	šest
studiových	studiový	k2eAgInPc2d1	studiový
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
posbírala	posbírat	k5eAaPmAgFnS	posbírat
různé	různý	k2eAgInPc4d1	různý
stupně	stupeň	k1gInPc4	stupeň
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
British	British	k1gInSc4	British
Steel	Steela	k1gFnPc2	Steela
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
kratší	krátký	k2eAgInPc1d2	kratší
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
rádiový	rádiový	k2eAgInSc1d1	rádiový
zvuk	zvuk	k1gInSc1	zvuk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udrželo	udržet	k5eAaPmAgNnS	udržet
si	se	k3xPyFc3	se
známý	známý	k2eAgMnSc1d1	známý
heavymetalový	heavymetalový	k2eAgMnSc1d1	heavymetalový
duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
United	United	k1gInSc1	United
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Breaking	Breaking	k1gInSc1	Breaking
the	the	k?	the
Law	Law	k1gFnSc2	Law
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
After	After	k1gMnSc1	After
Midnight	Midnight	k1gMnSc1	Midnight
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
slyšet	slyšet	k5eAaImF	slyšet
z	z	k7c2	z
rádia	rádio	k1gNnSc2	rádio
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
na	na	k7c6	na
BBC	BBC	kA	BBC
a	a	k8xC	a
usazují	usazovat	k5eAaImIp3nP	usazovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgMnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
deska	deska	k1gFnSc1	deska
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Point	pointa	k1gFnPc2	pointa
of	of	k?	of
Entry	Entra	k1gFnSc2	Entra
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
neslo	nést	k5eAaImAgNnS	nést
ve	v	k7c6	v
stejném	stejný	k2eAgMnSc6d1	stejný
duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
"	"	kIx"	"
<g/>
British	British	k1gMnSc1	British
Steel	Steel	k1gMnSc1	Steel
Tour	Tour	k1gMnSc1	Tour
<g/>
"	"	kIx"	"
představila	představit	k5eAaPmAgFnS	představit
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Solar	Solar	k1gInSc1	Solar
Angels	Angels	k1gInSc1	Angels
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Heading	Heading	k1gInSc1	Heading
Out	Out	k1gFnSc2	Out
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Highway	Highwa	k1gMnPc7	Highwa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
Screaming	Screaming	k1gInSc4	Screaming
for	forum	k1gNnPc2	forum
Vengeance	Vengeanec	k1gMnSc2	Vengeanec
představilo	představit	k5eAaPmAgNnS	představit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
Another	Anothra	k1gFnPc2	Anothra
Thing	Thing	k1gMnSc1	Thing
Comin	Comin	k1gMnSc1	Comin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
rozhlasovým	rozhlasový	k2eAgInSc7d1	rozhlasový
hitem	hit	k1gInSc7	hit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
písně	píseň	k1gFnPc1	píseň
jako	jako	k9	jako
například	například	k6eAd1	například
intro	intro	k6eAd1	intro
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Hellion	Hellion	k1gInSc1	Hellion
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
"	"	kIx"	"
<g/>
Electric	Electric	k1gMnSc1	Electric
Eye	Eye	k1gMnSc1	Eye
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Riding	Riding	k1gInSc1	Riding
on	on	k3xPp3gInSc1	on
the	the	k?	the
Wind	Wind	k1gInSc1	Wind
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
populární	populární	k2eAgFnPc1d1	populární
při	při	k7c6	při
živých	živý	k2eAgInPc6d1	živý
vystoupeních	vystoupení	k1gNnPc6	vystoupení
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jimi	on	k3xPp3gMnPc7	on
začínaly	začínat	k5eAaImAgInP	začínat
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Take	Take	k1gFnSc1	Take
These	these	k1gFnSc1	these
<g/>
)	)	kIx)	)
Chains	Chains	k1gInSc1	Chains
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
Bob	Bob	k1gMnSc1	Bob
Halligan	Halligan	k1gMnSc1	Halligan
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
singl	singl	k1gInSc1	singl
a	a	k8xC	a
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
double	double	k2eAgFnSc4d1	double
platinu	platina	k1gFnSc4	platina
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
úspěšně	úspěšně	k6eAd1	úspěšně
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
polovinou	polovina	k1gFnSc7	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Freewheel	Freewheel	k1gInSc1	Freewheel
Burning	Burning	k1gInSc1	Burning
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydanou	vydaný	k2eAgFnSc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byla	být	k5eAaImAgFnS	být
pravidelně	pravidelně	k6eAd1	pravidelně
z	z	k7c2	z
rockových	rockový	k2eAgNnPc2d1	rockové
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Defenders	Defendersa	k1gFnPc2	Defendersa
of	of	k?	of
the	the	k?	the
Faith	Faitha	k1gFnPc2	Faitha
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
ho	on	k3xPp3gNnSc4	on
přezídvali	přezídvat	k5eAaImAgMnP	přezídvat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Screaming	Screaming	k1gInSc4	Screaming
for	forum	k1gNnPc2	forum
Vengeance	Vengeanec	k1gMnSc2	Vengeanec
II	II	kA	II
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
podobnosti	podobnost	k1gFnSc3	podobnost
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hity	hit	k1gInPc4	hit
jako	jako	k8xC	jako
již	již	k9	již
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
Freewheel	Freewheela	k1gFnPc2	Freewheela
Burning	Burning	k1gInSc1	Burning
<g/>
,	,	kIx,	,
Sentinel	sentinel	k1gInSc1	sentinel
nebo	nebo	k8xC	nebo
Love	lov	k1gInSc5	lov
Bites	Bites	k1gMnSc1	Bites
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
si	se	k3xPyFc3	se
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
muzikanty	muzikant	k1gMnPc7	muzikant
<g/>
,	,	kIx,	,
zahráli	zahrát	k5eAaPmAgMnP	zahrát
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Live	Liv	k1gFnSc2	Liv
Aid	Aida	k1gFnPc2	Aida
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gInSc1	Priest
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c4	na
JFK	JFK	kA	JFK
Stadium	stadium	k1gNnSc1	stadium
v	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
repertoár	repertoár	k1gInSc1	repertoár
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Living	Living	k1gInSc1	Living
After	After	k1gMnSc1	After
Midnight	Midnight	k1gMnSc1	Midnight
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Green	Green	k1gInSc1	Green
Manalishi	Manalish	k1gFnSc2	Manalish
(	(	kIx(	(
<g/>
With	With	k1gMnSc1	With
The	The	k1gMnSc1	The
Two-Pronged	Two-Pronged	k1gMnSc1	Two-Pronged
Crown	Crown	k1gMnSc1	Crown
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
<g/>
)	)	kIx)	)
Another	Anothra	k1gFnPc2	Anothra
Thing	Thing	k1gInSc1	Thing
Comin	Comin	k1gMnSc1	Comin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Turbo	turba	k1gFnSc5	turba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
přijala	přijmout	k5eAaPmAgFnS	přijmout
pestřejší	pestrý	k2eAgInSc4d2	pestřejší
vzhled	vzhled	k1gInSc4	vzhled
na	na	k7c6	na
živých	živý	k2eAgInPc6d1	živý
vystoupeních	vystoupení	k1gNnPc6	vystoupení
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
mainstreamu	mainstream	k1gInSc2	mainstream
díky	díky	k7c3	díky
přidání	přidání	k1gNnSc3	přidání
kytarových	kytarový	k2eAgInPc2d1	kytarový
syntezátorů	syntezátor	k1gInPc2	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
platinové	platinový	k2eAgNnSc1d1	platinové
a	a	k8xC	a
turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
byla	být	k5eAaImAgFnS	být
deska	deska	k1gFnSc1	deska
fanoušky	fanoušek	k1gMnPc4	fanoušek
zpočátku	zpočátku	k6eAd1	zpočátku
odsuzovaná	odsuzovaný	k2eAgFnSc1d1	odsuzovaná
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
pořízené	pořízený	k2eAgNnSc1d1	pořízené
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc1	název
Priest	Priest	k1gFnSc1	Priest
<g/>
...	...	k?	...
<g/>
Live	Live	k1gInSc1	Live
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
a	a	k8xC	a
fanouškům	fanoušek	k1gMnPc3	fanoušek
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentární	dokumentární	k2eAgNnSc1d1	dokumentární
video	video	k1gNnSc1	video
Heavy	Heava	k1gFnSc2	Heava
Metal	metal	k1gInSc1	metal
Parking	parking	k1gInSc1	parking
Lot	lot	k1gInSc1	lot
natočil	natočit	k5eAaBmAgMnS	natočit
Jeff	Jeff	k1gMnSc1	Jeff
Krulik	Krulik	k1gMnSc1	Krulik
a	a	k8xC	a
John	John	k1gMnSc1	John
Heyn	Heyn	k1gMnSc1	Heyn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
heavymetalové	heavymetalový	k2eAgMnPc4d1	heavymetalový
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gFnSc1	Priest
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
se	s	k7c7	s
speciálním	speciální	k2eAgMnSc7d1	speciální
hostem	host	k1gMnSc7	host
Dokken	Dokkna	k1gFnPc2	Dokkna
<g/>
)	)	kIx)	)
před	před	k7c7	před
Capital	Capital	k1gMnSc1	Capital
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
US	US	kA	US
Airways	Airways	k1gInSc4	Airways
Arena	Aren	k1gInSc2	Aren
<g/>
)	)	kIx)	)
v	v	k7c6	v
Landoveru	landover	k1gInSc6	landover
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maryland	Maryland	k1gInSc1	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1988	[number]	k4	1988
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
Ram	Ram	k1gMnSc1	Ram
It	It	k1gMnSc1	It
Down	Down	k1gMnSc1	Down
a	a	k8xC	a
kromě	kromě	k7c2	kromě
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
představilo	představit	k5eAaPmAgNnS	představit
několik	několik	k4yIc1	několik
přepracovaných	přepracovaný	k2eAgFnPc2d1	přepracovaná
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zbyly	zbýt	k5eAaPmAgInP	zbýt
z	z	k7c2	z
alba	album	k1gNnSc2	album
Turbo	turba	k1gFnSc5	turba
<g/>
.	.	kIx.	.
</s>
<s>
Recenzent	recenzent	k1gMnSc1	recenzent
"	"	kIx"	"
<g/>
Ram	Ram	k1gMnSc1	Ram
It	It	k1gMnSc1	It
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
stylistický	stylistický	k2eAgInSc1d1	stylistický
vývoj	vývoj	k1gInSc1	vývoj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
pokusila	pokusit	k5eAaPmAgFnS	pokusit
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
syntezátorů	syntezátor	k1gInPc2	syntezátor
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
metalu	metal	k1gInSc3	metal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
také	také	k9	také
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
bubeník	bubeník	k1gMnSc1	bubeník
Dave	Dav	k1gInSc5	Dav
Holland	Holland	k1gInSc4	Holland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostává	dostávat	k5eAaImIp3nS	dostávat
grunge	grunge	k6eAd1	grunge
a	a	k8xC	a
klasický	klasický	k2eAgInSc1d1	klasický
metal	metal	k1gInSc1	metal
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Painkiller	Painkiller	k1gInSc1	Painkiller
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
bubeníkem	bubeník	k1gMnSc7	bubeník
-	-	kIx~	-
mladým	mladý	k2eAgMnSc7d1	mladý
a	a	k8xC	a
energickým	energický	k2eAgMnSc7d1	energický
Američanem	Američan	k1gMnSc7	Američan
Scottem	Scott	k1gMnSc7	Scott
Travisem	Travis	k1gInSc7	Travis
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Racer	Racer	k1gMnSc1	Racer
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntezátory	syntezátor	k1gInPc1	syntezátor
byly	být	k5eAaImAgInP	být
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c4	na
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
A	a	k9	a
Touch	Touch	k1gMnSc1	Touch
of	of	k?	of
Evil	Evil	k1gMnSc1	Evil
<g/>
"	"	kIx"	"
vypuštěny	vypuštěn	k2eAgFnPc1d1	vypuštěna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tour	tour	k1gInSc4	tour
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
vystoupily	vystoupit	k5eAaPmAgInP	vystoupit
kapely	kapela	k1gFnPc4	kapela
jako	jako	k8xC	jako
Megadeth	Megadeth	k1gInSc4	Megadeth
<g/>
,	,	kIx,	,
Pantera	panter	k1gMnSc4	panter
<g/>
,	,	kIx,	,
Sepultura	Sepultura	k1gFnSc1	Sepultura
a	a	k8xC	a
Testament	testament	k1gInSc1	testament
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
koncertů	koncert	k1gInPc2	koncert
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
na	na	k7c4	na
Rock	rock	k1gInSc4	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jevištní	jevištní	k2eAgFnSc2d1	jevištní
show	show	k1gFnSc2	show
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Rob	roba	k1gFnPc2	roba
Halford	Halforda	k1gFnPc2	Halforda
často	často	k6eAd1	často
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
svého	svůj	k3xOyFgInSc2	svůj
stroje	stroj	k1gInSc2	stroj
Harley-Davidson	Harley-Davidson	k1gMnSc1	Harley-Davidson
<g/>
,	,	kIx,	,
oblečený	oblečený	k2eAgMnSc1d1	oblečený
v	v	k7c6	v
motorkářském	motorkářský	k2eAgMnSc6d1	motorkářský
křiváku	křivák	k1gMnSc6	křivák
a	a	k8xC	a
se	s	k7c7	s
slunečními	sluneční	k2eAgFnPc7d1	sluneční
brýlemi	brýle	k1gFnPc7	brýle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
Halford	Halford	k1gMnSc1	Halford
vážně	vážně	k6eAd1	vážně
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyjel	vyjet	k5eAaPmAgInS	vyjet
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
a	a	k8xC	a
narazil	narazit	k5eAaPmAgInS	narazit
do	do	k7c2	do
"	"	kIx"	"
<g/>
riseru	riser	k1gInSc2	riser
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
podia	podium	k1gNnSc2	podium
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
stojí	stát	k5eAaImIp3nS	stát
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
schovaný	schovaný	k2eAgInSc1d1	schovaný
za	za	k7c4	za
mračny	mračna	k1gFnPc4	mračna
mlhy	mlha	k1gFnSc2	mlha
ze	z	k7c2	z
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
vystoupení	vystoupení	k1gNnSc1	vystoupení
zpožděno	zpozdit	k5eAaPmNgNnS	zpozdit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odehrán	odehrát	k5eAaPmNgInS	odehrát
celý	celý	k2eAgInSc1d1	celý
set	set	k1gInSc1	set
než	než	k8xS	než
se	se	k3xPyFc4	se
Halford	Halford	k1gInSc1	Halford
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
Hill	Hill	k1gMnSc1	Hill
později	pozdě	k6eAd2	pozdě
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
"	"	kIx"	"
<g/>
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
agónii	agónie	k1gFnSc6	agónie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2007	[number]	k4	2007
Rob	roba	k1gFnPc2	roba
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehoda	nehoda	k1gFnSc1	nehoda
neměla	mít	k5eNaImAgFnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
pozdějším	pozdní	k2eAgInSc7d2	pozdější
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
zatažena	zatáhnout	k5eAaPmNgFnS	zatáhnout
do	do	k7c2	do
občanskoprávní	občanskoprávní	k2eAgFnSc2d1	občanskoprávní
žaloby	žaloba	k1gFnSc2	žaloba
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
sebevraždu	sebevražda	k1gFnSc4	sebevražda
střelnou	střelný	k2eAgFnSc7d1	střelná
zbraní	zbraň	k1gFnSc7	zbraň
20	[number]	k4	20
<g/>
letého	letý	k2eAgInSc2d1	letý
Jamese	Jamesa	k1gFnSc3	Jamesa
Vance	Vance	k1gMnSc1	Vance
a	a	k8xC	a
18	[number]	k4	18
<g/>
letého	letý	k2eAgMnSc2d1	letý
Raymonda	Raymond	k1gMnSc2	Raymond
Belknapa	Belknap	k1gMnSc2	Belknap
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sparks	Sparksa	k1gFnPc2	Sparksa
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1985	[number]	k4	1985
Vance	Vanec	k1gInSc2	Vanec
a	a	k8xC	a
Belknap	Belknap	k1gInSc4	Belknap
<g/>
,	,	kIx,	,
po	po	k7c6	po
hodinách	hodina	k1gFnPc6	hodina
pití	pití	k1gNnSc2	pití
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
kouření	kouření	k1gNnSc2	kouření
marihuany	marihuana	k1gFnSc2	marihuana
a	a	k8xC	a
údajnému	údajný	k2eAgInSc3d1	údajný
poslouchu	poslouch	k1gInSc3	poslouch
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
odešli	odejít	k5eAaPmAgMnP	odejít
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sparks	Sparksa	k1gFnPc2	Sparksa
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
ukončit	ukončit	k5eAaPmF	ukončit
brokovnicí	brokovnice	k1gFnSc7	brokovnice
<g/>
.	.	kIx.	.
</s>
<s>
Belknap	Belknap	k1gMnSc1	Belknap
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
hlaveň	hlaveň	k1gFnSc4	hlaveň
brokovnice	brokovnice	k1gFnSc2	brokovnice
pod	pod	k7c4	pod
bradu	brada	k1gFnSc4	brada
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
spouště	spoušť	k1gFnSc2	spoušť
a	a	k8xC	a
Vance	Vance	k1gMnSc1	Vance
se	se	k3xPyFc4	se
střelil	střelit	k5eAaPmAgMnS	střelit
hned	hned	k6eAd1	hned
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
vážné	vážný	k2eAgNnSc4d1	vážné
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
četných	četný	k2eAgFnPc6d1	četná
komplikacích	komplikace	k1gFnPc6	komplikace
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
mladíků	mladík	k1gMnPc2	mladík
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
tým	tým	k1gInSc1	tým
právníků	právník	k1gMnPc2	právník
tvrdil	tvrdit	k5eAaImAgInS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladba	skladba	k1gFnSc1	skladba
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
"	"	kIx"	"
<g/>
Better	Better	k1gMnSc1	Better
By	by	k9	by
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
Better	Better	k1gMnSc1	Better
Than	Than	k1gMnSc1	Than
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cover	cover	k1gInSc1	cover
od	od	k7c2	od
Spooky	Spook	k1gInPc1	Spook
Tooth	Tootha	k1gFnPc2	Tootha
<g/>
)	)	kIx)	)
z	z	k7c2	z
alba	album	k1gNnSc2	album
Stained	Stained	k1gMnSc1	Stained
Class	Class	k1gInSc1	Class
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podprahové	podprahový	k2eAgNnSc4d1	podprahové
poselství	poselství	k1gNnSc4	poselství
"	"	kIx"	"
<g/>
udělej	udělat	k5eAaPmRp2nS	udělat
to	ten	k3xDgNnSc4	ten
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
údajně	údajně	k6eAd1	údajně
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
žaloby	žaloba	k1gFnSc2	žaloba
upuštěno	upustit	k5eAaPmNgNnS	upustit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
soudce	soudce	k1gMnSc1	soudce
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
"	"	kIx"	"
<g/>
udělej	udělat	k5eAaPmRp2nS	udělat
to	ten	k3xDgNnSc1	ten
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
výsledkem	výsledek	k1gInSc7	výsledek
náhodného	náhodný	k2eAgInSc2d1	náhodný
mixu	mix	k1gInSc2	mix
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
svědků	svědek	k1gMnPc2	svědek
obhajoby	obhajoba	k1gFnSc2	obhajoba
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Timothy	Timotha	k1gMnSc2	Timotha
E.	E.	kA	E.
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
článek	článek	k1gInSc1	článek
pro	pro	k7c4	pro
Skeptical	Skeptical	k1gFnPc4	Skeptical
Inquirer	Inquirer	k1gInSc4	Inquirer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
časově	časově	k6eAd1	časově
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
také	také	k9	také
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
v	v	k7c6	v
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
Dream	Dream	k1gInSc1	Dream
Deceivers	Deceivers	k1gInSc4	Deceivers
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
Behind	Behind	k1gMnSc1	Behind
James	James	k1gMnSc1	James
Vance	Vance	k1gMnSc1	Vance
Vs	Vs	k1gMnSc1	Vs
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
tour	toura	k1gFnPc2	toura
k	k	k7c3	k
albu	album	k1gNnSc3	album
Painkiller	Painkiller	k1gInSc4	Painkiller
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Halford	Halford	k1gMnSc1	Halford
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
náznaky	náznak	k1gInPc1	náznak
napětí	napětí	k1gNnPc2	napětí
uvnitř	uvnitř	k7c2	uvnitř
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Halford	Halford	k1gInSc1	Halford
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
thrashovou	thrashový	k2eAgFnSc4d1	thrashová
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Fight	Fight	k1gInSc4	Fight
se	s	k7c7	s
Scottem	Scott	k1gMnSc7	Scott
Travisem	Travis	k1gInSc7	Travis
za	za	k7c2	za
bicíma	bicí	k2eAgFnPc7d1	bicí
pro	pro	k7c4	pro
studiové	studiový	k2eAgFnPc4d1	studiová
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Kapelu	kapela	k1gFnSc4	kapela
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
touze	touha	k1gFnSc6	touha
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
nová	nový	k2eAgNnPc4d1	nové
hudební	hudební	k2eAgNnPc4d1	hudební
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
smluvním	smluvní	k2eAgInPc3d1	smluvní
závazkům	závazek	k1gInPc3	závazek
s	s	k7c7	s
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Halford	Halford	k1gInSc1	Halford
s	s	k7c7	s
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
výběrové	výběrový	k2eAgFnSc2d1	výběrová
desky	deska	k1gFnSc2	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Metal	metat	k5eAaImAgMnS	metat
Works	Works	kA	Works
'	'	kIx"	'
<g/>
73	[number]	k4	73
-	-	kIx~	-
'	'	kIx"	'
<g/>
93	[number]	k4	93
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
videu	video	k1gNnSc6	video
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgFnSc1d1	dokumentující
jejich	jejich	k3xOp3gFnSc4	jejich
historii	historie	k1gFnSc4	historie
ve	v	k7c4	v
které	který	k3yRgInPc4	který
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
odchod	odchod	k1gInSc1	odchod
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gInSc1	Priest
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c4	o
angažmá	angažmá	k1gNnSc4	angažmá
Ralfa	Ralf	k1gMnSc2	Ralf
Scheperse	Scheperse	k1gFnSc2	Scheperse
(	(	kIx(	(
<g/>
Primal	Primal	k1gMnSc1	Primal
Fear	Fear	k1gMnSc1	Fear
<g/>
,	,	kIx,	,
<g/>
ex-Gamma	ex-Gamma	k1gFnSc1	ex-Gamma
Ray	Ray	k1gMnSc1	Ray
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
hlasově	hlasově	k6eAd1	hlasově
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
Halfordovi	Halforda	k1gMnSc3	Halforda
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Halford	Halford	k1gMnSc1	Halford
veřejně	veřejně	k6eAd1	veřejně
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
gay	gay	k1gMnSc1	gay
<g/>
.	.	kIx.	.
</s>
<s>
Američan	Američan	k1gMnSc1	Američan
Tim	Tim	k?	Tim
"	"	kIx"	"
<g/>
Ripper	Ripper	k1gMnSc1	Ripper
<g/>
"	"	kIx"	"
Owens	Owens	k1gInSc1	Owens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předtím	předtím	k6eAd1	předtím
zpíval	zpívat	k5eAaImAgMnS	zpívat
v	v	k7c6	v
revivalu	revival	k1gInSc6	revival
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gInSc4	Priest
s	s	k7c7	s
názvem	název	k1gInSc7	název
British	British	k1gInSc1	British
Steel	Steela	k1gFnPc2	Steela
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
jako	jako	k8xC	jako
nový	nový	k2eAgMnSc1d1	nový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
vydala	vydat	k5eAaPmAgFnS	vydat
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
-	-	kIx~	-
Jugulator	Jugulator	k1gMnSc1	Jugulator
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
Demolition	Demolition	k1gInSc1	Demolition
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
dvě	dva	k4xCgNnPc4	dva
živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
-	-	kIx~	-
Live	Live	k1gFnSc1	Live
Meltdown	Meltdown	k1gMnSc1	Meltdown
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
Live	Live	k1gNnSc1	Live
in	in	k?	in
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
živé	živá	k1gFnPc4	živá
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
Jugulator	Jugulator	k1gInSc1	Jugulator
prodávalo	prodávat	k5eAaImAgNnS	prodávat
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
dost	dost	k6eAd1	dost
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
recenzí	recenze	k1gFnPc2	recenze
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
epos	epos	k1gInSc4	epos
"	"	kIx"	"
<g/>
Cathedral	Cathedral	k1gMnSc1	Cathedral
Spires	Spires	k1gMnSc1	Spires
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
Ripperových	Ripperův	k2eAgFnPc2d1	Ripperův
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
sobě	se	k3xPyFc3	se
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
stále	stále	k6eAd1	stále
čelili	čelit	k5eAaImAgMnP	čelit
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
reunionu	reunion	k1gInSc6	reunion
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
a	a	k8xC	a
Rob	roba	k1gFnPc2	roba
Halford	Halfordo	k1gNnPc2	Halfordo
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sejdou	sejít	k5eAaPmIp3nP	sejít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2003	[number]	k4	2003
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
bude	být	k5eAaImBp3nS	být
vydán	vydat	k5eAaPmNgInS	vydat
box	box	k1gInSc1	box
set	sto	k4xCgNnPc2	sto
Metalogy	metalog	k1gMnPc7	metalog
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Halfordovu	Halfordův	k2eAgFnSc4d1	Halfordův
dřívější	dřívější	k2eAgFnSc4d1	dřívější
naléhání	naléhání	k1gNnSc3	naléhání
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nemělo	mít	k5eNaImAgNnS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
<g/>
.	.	kIx.	.
</s>
<s>
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
se	s	k7c7	s
s	s	k7c7	s
"	"	kIx"	"
<g/>
Ripperem	Ripper	k1gMnSc7	Ripper
<g/>
"	"	kIx"	"
rozešli	rozejít	k5eAaPmAgMnP	rozejít
v	v	k7c4	v
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
Ripper	Ripper	k1gInSc1	Ripper
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
heavymetalové	heavymetalový	k2eAgFnSc3d1	heavymetalová
kapele	kapela	k1gFnSc3	kapela
Iced	Iced	k1gMnSc1	Iced
Earth	Earth	k1gMnSc1	Earth
<g/>
.	.	kIx.	.
</s>
<s>
Dechberoucí	Dechberoucí	k2eAgInSc1d1	Dechberoucí
Reunion	Reunion	k1gInSc1	Reunion
Tour	Toura	k1gFnPc2	Toura
opět	opět	k6eAd1	opět
zavítalo	zavítat	k5eAaPmAgNnS	zavítat
i	i	k9	i
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
srazilo	srazit	k5eAaPmAgNnS	srazit
na	na	k7c4	na
kolena	koleno	k1gNnPc4	koleno
zaplněnou	zaplněný	k2eAgFnSc4d1	zaplněná
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
Arenu	Areno	k1gNnSc6	Areno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
u	u	k7c2	u
Sony	Sony	kA	Sony
Music	Musice	k1gFnPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Epic	Epic	k1gFnSc1	Epic
Records	Recordsa	k1gFnPc2	Recordsa
napjatě	napjatě	k6eAd1	napjatě
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
album	album	k1gNnSc1	album
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Retribution	Retribution	k1gInSc1	Retribution
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
logickým	logický	k2eAgNnSc7d1	logické
pokračováním	pokračování	k1gNnSc7	pokračování
Painkiller	Painkiller	k1gMnSc1	Painkiller
<g/>
.	.	kIx.	.
</s>
<s>
Získalo	získat	k5eAaPmAgNnS	získat
úspěch	úspěch	k1gInSc4	úspěch
jak	jak	k6eAd1	jak
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
komerčním	komerční	k2eAgNnSc6d1	komerční
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgNnSc6	který
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
Halford	Halford	k1gMnSc1	Halford
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
založit	založit	k5eAaPmF	založit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
Metal	metat	k5eAaImAgMnS	metat
God	God	k1gMnSc1	God
Entertainmen	Entertainmen	k1gInSc4	Entertainmen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vydávat	vydávat	k5eAaPmF	vydávat
jeho	on	k3xPp3gInSc4	on
všechen	všechen	k3xTgInSc4	všechen
sólový	sólový	k2eAgInSc4d1	sólový
materiál	materiál	k1gInSc4	materiál
pod	pod	k7c7	pod
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
remasterována	remasterovat	k5eAaImNgFnS	remasterovat
a	a	k8xC	a
vydána	vydat	k5eAaPmNgFnS	vydat
exkluzivně	exkluzivně	k6eAd1	exkluzivně
přes	přes	k7c4	přes
iTunes	iTunes	k1gInSc4	iTunes
Store	Stor	k1gInSc5	Stor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
záznam	záznam	k1gInSc1	záznam
japonského	japonský	k2eAgInSc2d1	japonský
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
Budokan	Budokana	k1gFnPc2	Budokana
Areny	Arena	k1gMnSc2	Arena
na	na	k7c4	na
DVD	DVD	kA	DVD
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rising	Rising	k1gInSc4	Rising
in	in	k?	in
the	the	k?	the
East	East	k1gInSc1	East
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
vydělali	vydělat	k5eAaPmAgMnP	vydělat
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
jen	jen	k9	jen
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
kolem	kolem	k7c2	kolem
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zařadili	zařadit	k5eAaPmAgMnP	zařadit
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
nejvíce	hodně	k6eAd3	hodně
vydělávajících	vydělávající	k2eAgMnPc2d1	vydělávající
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
nejistě	jistě	k6eNd1	jistě
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k9	což
Rob	roba	k1gFnPc2	roba
Halford	Halford	k1gMnSc1	Halford
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
a	a	k8xC	a
také	také	k9	také
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
show	show	k1gNnSc4	show
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgMnPc1d1	další
DVD	DVD	kA	DVD
-	-	kIx~	-
Live	Liv	k1gMnSc2	Liv
Vengeance	Vengeanec	k1gMnSc2	Vengeanec
'	'	kIx"	'
<g/>
82	[number]	k4	82
<g/>
'	'	kIx"	'
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
Memphisu	Memphis	k1gInSc2	Memphis
během	během	k7c2	během
Screaming	Screaming	k1gInSc1	Screaming
for	forum	k1gNnPc2	forum
Vengeance	Vengeance	k1gFnSc2	Vengeance
turné	turné	k1gNnSc1	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
při	při	k7c6	při
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
zpěvák	zpěvák	k1gMnSc1	zpěvák
Rob	roba	k1gFnPc2	roba
Halford	Halford	k1gMnSc1	Halford
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncepční	koncepční	k2eAgNnSc1d1	koncepční
album	album	k1gNnSc1	album
bude	být	k5eAaImBp3nS	být
pojednávat	pojednávat	k5eAaImF	pojednávat
o	o	k7c6	o
francouzském	francouzský	k2eAgInSc6d1	francouzský
spisovateli	spisovatel	k1gMnSc3	spisovatel
a	a	k8xC	a
věštci	věštec	k1gMnSc3	věštec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Nostradamovi	Nostradamův	k2eAgMnPc1d1	Nostradamův
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
o	o	k7c6	o
metalu	metal	k1gInSc6	metal
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
?	?	kIx.	?
</s>
<s>
Byl	být	k5eAaImAgMnS	být
alchymista	alchymista	k1gMnSc1	alchymista
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
prorok	prorok	k1gMnSc1	prorok
-	-	kIx~	-
osoba	osoba	k1gFnSc1	osoba
výjimečně	výjimečně	k6eAd1	výjimečně
nadaná	nadaný	k2eAgFnSc1d1	nadaná
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
úžasný	úžasný	k2eAgInSc1d1	úžasný
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
,	,	kIx,	,
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
radosti	radost	k1gFnSc2	radost
a	a	k8xC	a
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgInSc1d1	velký
lidský	lidský	k2eAgInSc1d1	lidský
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
vzít	vzít	k5eAaPmF	vzít
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
přeložit	přeložit	k5eAaPmF	přeložit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
máme	mít	k5eAaImIp1nP	mít
co	co	k3yQnSc4	co
do	do	k7c2	do
činění	činění	k1gNnSc2	činění
s	s	k7c7	s
celosvětovým	celosvětový	k2eAgNnSc7d1	celosvětové
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kromě	kromě	k7c2	kromě
nového	nový	k2eAgInSc2d1	nový
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
základ	základ	k1gInSc4	základ
textů	text	k1gInPc2	text
bude	být	k5eAaImBp3nS	být
album	album	k1gNnSc1	album
obsahovat	obsahovat	k5eAaImF	obsahovat
hudební	hudební	k2eAgInPc4d1	hudební
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
překvapit	překvapit	k5eAaPmF	překvapit
naše	náš	k3xOp1gMnPc4	náš
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
obsahovat	obsahovat	k5eAaImF	obsahovat
spoustu	spoustu	k6eAd1	spoustu
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
symfonických	symfonický	k2eAgInPc2d1	symfonický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
přehnané	přehnaný	k2eAgNnSc1d1	přehnané
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
masivní	masivní	k2eAgInPc1d1	masivní
sborové	sborový	k2eAgInPc1d1	sborový
zpěvy	zpěv	k1gInPc1	zpěv
a	a	k8xC	a
klávesy	kláves	k1gInPc1	kláves
se	se	k3xPyFc4	se
představí	představit	k5eAaPmIp3nP	představit
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předtím	předtím	k6eAd1	předtím
byly	být	k5eAaImAgFnP	být
vždycky	vždycky	k6eAd1	vždycky
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Album	album	k1gNnSc1	album
Nostradamus	Nostradamus	k1gInSc4	Nostradamus
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
turné	turné	k1gNnSc4	turné
ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
kapely	kapela	k1gFnPc4	kapela
hovořící	hovořící	k2eAgFnPc1d1	hovořící
proti	proti	k7c3	proti
kupčení	kupčení	k1gNnSc3	kupčení
se	s	k7c7	s
vstupenkami	vstupenka	k1gFnPc7	vstupenka
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
odsuzující	odsuzující	k2eAgInSc1d1	odsuzující
způsob	způsob	k1gInSc1	způsob
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c4	nad
jejich	jejich	k3xOp3gFnSc4	jejich
základní	základní	k2eAgFnSc4d1	základní
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
nálehala	nálehat	k5eAaPmAgFnS	nálehat
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
fanoušky	fanoušek	k1gMnPc4	fanoušek
aby	aby	k9	aby
kupovali	kupovat	k5eAaImAgMnP	kupovat
vstupenky	vstupenka	k1gFnSc2	vstupenka
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
oficiálních	oficiální	k2eAgInPc2d1	oficiální
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
měsíc	měsíc	k1gInSc4	měsíc
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
Megadeth	Megadetha	k1gFnPc2	Megadetha
a	a	k8xC	a
Testament	testament	k1gInSc1	testament
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
spoustě	spousta	k1gFnSc6	spousta
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
turné	turné	k1gNnSc1	turné
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gInSc4	Priest
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
v	v	k7c6	v
Atlantic	Atlantice	k1gFnPc2	Atlantice
Pavilionu	Pavilion	k1gInSc2	Pavilion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyli	být	k5eNaImAgMnP	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
tour	toura	k1gFnPc2	toura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
Halford	Halford	k1gInSc1	Halford
s	s	k7c7	s
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
severoamerické	severoamerický	k2eAgNnSc4d1	severoamerické
turné	turné	k1gNnSc4	turné
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
British	British	k1gMnSc1	British
Steel	Steel	k1gMnSc1	Steel
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
předvedeno	předvést	k5eAaPmNgNnS	předvést
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
v	v	k7c4	v
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
písněmi	píseň	k1gFnPc7	píseň
přidanými	přidaný	k2eAgFnPc7d1	přidaná
do	do	k7c2	do
setlistu	setlista	k1gMnSc4	setlista
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
turné	turné	k1gNnSc1	turné
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
společné	společný	k2eAgNnSc1d1	společné
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Davidem	David	k1gMnSc7	David
Coverdalem	Coverdal	k1gMnSc7	Coverdal
a	a	k8xC	a
Whitesnake	Whitesnake	k1gFnSc7	Whitesnake
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
Whitesnake	Whitesnake	k1gFnSc4	Whitesnake
museli	muset	k5eAaImAgMnP	muset
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
opustit	opustit	k5eAaPmF	opustit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
Coverdalovy	Coverdalův	k2eAgFnSc2d1	Coverdalův
vážné	vážný	k2eAgFnSc2d1	vážná
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
krku	krk	k1gInSc6	krk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
okamžitě	okamžitě	k6eAd1	okamžitě
přestal	přestat	k5eAaPmAgMnS	přestat
zpívat	zpívat	k5eAaImF	zpívat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hrozí	hrozit	k5eAaImIp3nS	hrozit
trvalé	trvalý	k2eAgNnSc4d1	trvalé
poškození	poškození	k1gNnSc4	poškození
hlasivek	hlasivka	k1gFnPc2	hlasivka
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
vydali	vydat	k5eAaPmAgMnP	vydat
nové	nový	k2eAgNnSc4d1	nové
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
11	[number]	k4	11
dříve	dříve	k6eAd2	dříve
nevydaných	vydaný	k2eNgFnPc2d1	nevydaná
živých	živý	k2eAgFnPc2d1	živá
skladeb	skladba	k1gFnPc2	skladba
ze	z	k7c2	z
světových	světový	k2eAgNnPc2d1	světové
turné	turné	k1gNnPc2	turné
2005	[number]	k4	2005
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
A	a	k8xC	a
Touch	Touch	k1gInSc1	Touch
of	of	k?	of
Evil	Evil	k1gInSc1	Evil
<g/>
:	:	kIx,	:
Live	Live	k1gInSc1	Live
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Dissident	Dissident	k1gMnSc1	Dissident
Aggressor	Aggressor	k1gMnSc1	Aggressor
<g/>
"	"	kIx"	"
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
metalový	metalový	k2eAgInSc4d1	metalový
počin	počin	k1gInSc4	počin
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
kapela	kapela	k1gFnSc1	kapela
oznámila	oznámit	k5eAaPmAgFnS	oznámit
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
masivní	masivní	k2eAgNnSc4d1	masivní
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Epitaph	Epitaph	k1gInSc1	Epitaph
a	a	k8xC	a
projelo	projet	k5eAaPmAgNnS	projet
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
včetně	včetně	k7c2	včetně
pražské	pražský	k2eAgFnSc2d1	Pražská
O2	O2	k1gFnSc2	O2
arény	aréna	k1gFnSc2	aréna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
skupina	skupina	k1gFnSc1	skupina
Judas	Judas	k1gInSc1	Judas
Priest	Priest	k1gFnSc1	Priest
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Whitesnake	Whitesnak	k1gInSc2	Whitesnak
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
K.K.	K.K.	k1gMnPc2	K.K.
Downing	Downing	k1gInSc4	Downing
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gNnSc4	on
Richie	Richie	k1gFnPc1	Richie
Faulkner	Faulknra	k1gFnPc2	Faulknra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
kytarista	kytarista	k1gMnSc1	kytarista
Glenn	Glenn	k1gMnSc1	Glenn
Tipton	Tipton	k1gInSc4	Tipton
označil	označit	k5eAaPmAgMnS	označit
Prahu	Praha	k1gFnSc4	Praha
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
oblíbených	oblíbený	k2eAgNnPc2d1	oblíbené
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
kapela	kapela	k1gFnSc1	kapela
dokončila	dokončit	k5eAaPmAgFnS	dokončit
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
turné	turné	k1gNnSc2	turné
Epitaph	Epitapha	k1gFnPc2	Epitapha
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
zavítali	zavítat	k5eAaPmAgMnP	zavítat
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
osmého	osmý	k4xOgInSc2	osmý
května	květen	k1gInSc2	květen
v	v	k7c6	v
pardubické	pardubický	k2eAgFnSc6d1	pardubická
ČEZ	ČEZ	kA	ČEZ
aréně	aréna	k1gFnSc6	aréna
společně	společně	k6eAd1	společně
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Thin	Thino	k1gNnPc2	Thino
Lizzy	Lizza	k1gFnSc2	Lizza
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
opustil	opustit	k5eAaPmAgInS	opustit
skupinu	skupina	k1gFnSc4	skupina
Downing	Downing	k1gInSc1	Downing
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gNnSc4	on
Richie	Richie	k1gFnPc1	Richie
Faulkner	Faulknra	k1gFnPc2	Faulknra
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Redeemer	Redeemra	k1gFnPc2	Redeemra
of	of	k?	of
Souls	Soulsa	k1gFnPc2	Soulsa
<g/>
,	,	kIx,	,
původním	původní	k2eAgInSc7d1	původní
záměrem	záměr	k1gInSc7	záměr
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
stihnout	stihnout	k5eAaPmF	stihnout
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
říkal	říkat	k5eAaImAgMnS	říkat
frontman	frontman	k1gMnSc1	frontman
Halford	Halford	k1gMnSc1	Halford
pro	pro	k7c4	pro
Musicserver	Musicserver	k1gInSc4	Musicserver
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nyní	nyní	k6eAd1	nyní
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
můžeme	moct	k5eAaImIp1nP	moct
udělat	udělat	k5eAaPmF	udělat
potřebný	potřebný	k2eAgInSc4d1	potřebný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vyjde	vyjít	k5eAaPmIp3nS	vyjít
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
mi	já	k3xPp1nSc3	já
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
stihneme	stihnout	k5eAaPmIp1nP	stihnout
vydat	vydat	k5eAaPmF	vydat
ještě	ještě	k9	ještě
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Víte	vědět	k5eAaImIp2nP	vědět
co	co	k9	co
<g/>
,	,	kIx,	,
klidně	klidně	k6eAd1	klidně
si	se	k3xPyFc3	se
zapište	zapsat	k5eAaPmRp2nP	zapsat
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Pojďme	jít	k5eAaImRp1nP	jít
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
těšit	těšit	k5eAaImF	těšit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
Judas	Judas	k1gInSc4	Judas
Priest	Priest	k1gFnSc1	Priest
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidi	člověk	k1gMnPc4	člověk
zase	zase	k9	zase
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
co	co	k3yInSc4	co
házet	házet	k5eAaImF	házet
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
desce	deska	k1gFnSc6	deska
prodloužily	prodloužit	k5eAaPmAgFnP	prodloužit
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xC	jako
death	death	k1gInSc4	death
metalová	metalový	k2eAgFnSc1d1	metalová
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
kritiku	kritika	k1gFnSc4	kritika
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
hudebního	hudební	k2eAgInSc2d1	hudební
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
označení	označení	k1gNnSc3	označení
ohradili	ohradit	k5eAaPmAgMnP	ohradit
a	a	k8xC	a
neznalost	neznalost	k1gFnSc4	neznalost
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
neomluvitelnou	omluvitelný	k2eNgFnSc4d1	neomluvitelná
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
omluvili	omluvit	k5eAaPmAgMnP	omluvit
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
díle	dílo	k1gNnSc6	dílo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Barta	Bart	k1gInSc2	Bart
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
znělce	znělka	k1gFnSc6	znělka
na	na	k7c6	na
tabuli	tabule	k1gFnSc6	tabule
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Judas	Judas	k1gInSc4	Judas
Priest	Priest	k1gFnSc4	Priest
nejsou	být	k5eNaImIp3nP	být
death	death	k1gInSc4	death
metal	metal	k1gInSc1	metal
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
členů	člen	k1gMnPc2	člen
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
Hill	Hill	k1gMnSc1	Hill
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
<s>
Rob	robit	k5eAaImRp2nS	robit
Halford	Halford	k1gInSc1	Halford
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
<s>
Richie	Richie	k1gFnSc1	Richie
Faulkner	Faulkner	k1gMnSc1	Faulkner
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-dosud	osud	k1gInSc1	-dosud
<g/>
)	)	kIx)	)
</s>
<s>
Glenn	Glenn	k1gInSc1	Glenn
Tipton	Tipton	k1gInSc1	Tipton
–	–	k?	–
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
<s>
Scott	Scott	k2eAgInSc1d1	Scott
Travis	Travis	k1gInSc1	Travis
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Rocka	Rocka	k1gMnSc1	Rocka
Rolla	Rolla	k1gMnSc1	Rolla
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Sad	sad	k1gInSc1	sad
Wings	Wings	k1gInSc1	Wings
of	of	k?	of
Destiny	Destina	k1gFnSc2	Destina
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Sin	sin	kA	sin
After	Aftra	k1gFnPc2	Aftra
Sin	sin	kA	sin
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Stained	Stained	k1gInSc1	Stained
Class	Class	k1gInSc1	Class
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Killing	Killing	k1gInSc1	Killing
Machine	Machin	k1gInSc5	Machin
<g/>
/	/	kIx~	/
<g/>
Hell	Hell	k1gMnSc1	Hell
Bent	Bent	k1gMnSc1	Bent
for	forum	k1gNnPc2	forum
Leather	Leathra	k1gFnPc2	Leathra
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
British	British	k1gMnSc1	British
Steel	Steel	k1gMnSc1	Steel
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Point	pointa	k1gFnPc2	pointa
of	of	k?	of
Entry	Entra	k1gMnSc2	Entra
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Screaming	Screaming	k1gInSc1	Screaming
for	forum	k1gNnPc2	forum
Vengeance	Vengeance	k1gFnSc2	Vengeance
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Defenders	Defenders	k1gInSc1	Defenders
of	of	k?	of
the	the	k?	the
Faith	Faith	k1gInSc1	Faith
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Turbo	turba	k1gFnSc5	turba
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Ram	Ram	k1gMnSc1	Ram
It	It	k1gMnSc1	It
Down	Down	k1gMnSc1	Down
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Painkiller	Painkiller	k1gInSc1	Painkiller
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Jugulator	Jugulator	k1gInSc1	Jugulator
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Demolition	Demolition	k1gInSc1	Demolition
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Angel	angel	k1gMnSc1	angel
of	of	k?	of
Retribution	Retribution	k1gInSc1	Retribution
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Nostradamus	Nostradamus	k1gInSc1	Nostradamus
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Redeemer	Redeemer	k1gInSc1	Redeemer
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
