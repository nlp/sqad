<s>
Symbolismus	symbolismus	k1gInSc1	symbolismus
je	být	k5eAaImIp3nS	být
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
