<s>
Symbolismus	symbolismus	k1gInSc1	symbolismus
je	být	k5eAaImIp3nS	být
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Symbolismus	symbolismus	k1gInSc1	symbolismus
byl	být	k5eAaImAgInS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
popisnost	popisnost	k1gFnSc4	popisnost
naturalismu	naturalismus	k1gInSc2	naturalismus
a	a	k8xC	a
parnasismu	parnasismus	k1gInSc2	parnasismus
(	(	kIx(	(
<g/>
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Lumírovci	lumírovec	k1gMnPc1	lumírovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
částečně	částečně	k6eAd1	částečně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
romantismus	romantismus	k1gInSc4	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nelze	lze	k6eNd1	lze
racionálně	racionálně	k6eAd1	racionálně
popsat	popsat	k5eAaPmF	popsat
(	(	kIx(	(
<g/>
nálady	nálada	k1gFnPc1	nálada
<g/>
,	,	kIx,	,
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
city	cit	k1gInPc1	cit
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zobrazit	zobrazit	k5eAaPmF	zobrazit
(	(	kIx(	(
<g/>
nakreslit	nakreslit	k5eAaPmF	nakreslit
<g/>
,	,	kIx,	,
popsat	popsat	k5eAaPmF	popsat
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
nezobrazitelné	zobrazitelný	k2eNgFnSc2d1	nezobrazitelná
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
podstatě	podstata	k1gFnSc3	podstata
skutečnosti	skutečnost	k1gFnSc2	skutečnost
-	-	kIx~	-
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c6	o
vnímání	vnímání	k1gNnSc6	vnímání
umění	umění	k1gNnSc2	umění
všemi	všecek	k3xTgFnPc7	všecek
pěti	pět	k4xCc2	pět
smysly	smysl	k1gInPc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jim	on	k3xPp3gMnPc3	on
měl	mít	k5eAaImAgMnS	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
prostředníkem	prostředník	k1gInSc7	prostředník
mezi	mezi	k7c7	mezi
skutečným	skutečný	k2eAgInSc7d1	skutečný
světem	svět	k1gInSc7	svět
a	a	k8xC	a
"	"	kIx"	"
<g/>
světem	svět	k1gInSc7	svět
duše	duše	k1gFnSc2	duše
<g/>
"	"	kIx"	"
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
náznaku	náznak	k1gInSc6	náznak
odkrývat	odkrývat	k5eAaImF	odkrývat
tajemství	tajemství	k1gNnSc4	tajemství
ukryté	ukrytý	k2eAgNnSc4d1	ukryté
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
nebyl	být	k5eNaImAgInS	být
přímým	přímý	k2eAgNnSc7d1	přímé
pojmenováním	pojmenování	k1gNnSc7	pojmenování
věci	věc	k1gFnSc2	věc
(	(	kIx(	(
<g/>
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
stavu	stav	k1gInSc2	stav
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouhým	pouhý	k2eAgInSc7d1	pouhý
náznakem	náznak	k1gInSc7	náznak
<g/>
,	,	kIx,	,
sugescí	sugesce	k1gFnSc7	sugesce
podstaty	podstata	k1gFnSc2	podstata
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Symbolisté	symbolista	k1gMnPc1	symbolista
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
soustředěným	soustředěný	k2eAgNnSc7d1	soustředěné
vnímáním	vnímání	k1gNnSc7	vnímání
celé	celý	k2eAgFnSc2d1	celá
umělcovy	umělcův	k2eAgFnSc2d1	umělcova
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
všemi	všecek	k3xTgInPc7	všecek
pěti	pět	k4xCc7	pět
smysly	smysl	k1gInPc7	smysl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
splynout	splynout	k5eAaPmF	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Symbolisté	symbolista	k1gMnPc1	symbolista
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
umění	umění	k1gNnSc1	umění
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
chtít	chtít	k5eAaImF	chtít
zachytit	zachytit	k5eAaPmF	zachytit
více	hodně	k6eAd2	hodně
absolutních	absolutní	k2eAgFnPc2d1	absolutní
pravd	pravda	k1gFnPc2	pravda
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
zpřístupněné	zpřístupněný	k2eAgInPc1d1	zpřístupněný
jen	jen	k9	jen
nepřímými	přímý	k2eNgFnPc7d1	nepřímá
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Symbolisté	symbolista	k1gMnPc1	symbolista
se	se	k3xPyFc4	se
dělili	dělit	k5eAaImAgMnP	dělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
chápání	chápání	k1gNnSc1	chápání
symbolu	symbol	k1gInSc2	symbol
bylo	být	k5eAaImAgNnS	být
radikálně	radikálně	k6eAd1	radikálně
odlišné	odlišný	k2eAgNnSc1d1	odlišné
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
představy	představa	k1gFnPc1	představa
byly	být	k5eAaImAgFnP	být
symboly	symbol	k1gInPc4	symbol
jako	jako	k9	jako
<g/>
:	:	kIx,	:
Nápověda	nápověda	k1gFnSc1	nápověda
-	-	kIx~	-
tj.	tj.	kA	tj.
má	mít	k5eAaImIp3nS	mít
čtenáři	čtenář	k1gMnSc3	čtenář
vsugerovat	vsugerovat	k5eAaPmF	vsugerovat
nějaký	nějaký	k3yIgInSc4	nějaký
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
...	...	k?	...
Jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
znak	znak	k1gInSc1	znak
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
nemohl	moct	k5eNaImAgInS	moct
zcela	zcela	k6eAd1	zcela
rozvinout	rozvinout	k5eAaPmF	rozvinout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
byly	být	k5eAaImAgFnP	být
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
populárnější	populární	k2eAgFnSc2d2	populárnější
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
naturalismus	naturalismus	k1gInSc4	naturalismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
na	na	k7c4	na
impresionismus	impresionismus	k1gInSc4	impresionismus
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c4	na
akademismus	akademismus	k1gInSc4	akademismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
využívali	využívat	k5eAaImAgMnP	využívat
báje	báj	k1gFnPc4	báj
<g/>
,	,	kIx,	,
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
měl	mít	k5eAaImAgInS	mít
umožnit	umožnit	k5eAaPmF	umožnit
čtenáři	čtenář	k1gMnSc3	čtenář
uhodnout	uhodnout	k5eAaPmF	uhodnout
tajemství	tajemství	k1gNnSc4	tajemství
ukryté	ukrytý	k2eAgNnSc4d1	ukryté
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
tyto	tento	k3xDgFnPc1	tento
představy	představa	k1gFnPc1	představa
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
nepřímo	přímo	k6eNd1	přímo
(	(	kIx(	(
<g/>
metaforou	metafora	k1gFnSc7	metafora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolismus	symbolismus	k1gInSc1	symbolismus
zahájil	zahájit	k5eAaPmAgInS	zahájit
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
básnické	básnický	k2eAgFnSc2d1	básnická
obraznosti	obraznost	k1gFnSc2	obraznost
<g/>
,	,	kIx,	,
kladl	klást	k5eAaImAgInS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
hudebnost	hudebnost	k1gFnSc4	hudebnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
často	často	k6eAd1	často
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
nesrozumitelnosti	nesrozumitelnost	k1gFnSc3	nesrozumitelnost
a	a	k8xC	a
nepochopitelnosti	nepochopitelnost	k1gFnSc2	nepochopitelnost
jejich	jejich	k3xOp3gInPc2	jejich
náznaků	náznak	k1gInPc2	náznak
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
prvkem	prvek	k1gInSc7	prvek
jejich	jejich	k3xOp3gFnSc2	jejich
poezie	poezie	k1gFnSc2	poezie
byl	být	k5eAaImAgInS	být
volný	volný	k2eAgInSc1d1	volný
verš	verš	k1gInSc1	verš
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
používali	používat	k5eAaImAgMnP	používat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
chce	chtít	k5eAaImIp3nS	chtít
básník	básník	k1gMnSc1	básník
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
především	především	k6eAd1	především
zapůsobit	zapůsobit	k5eAaPmF	zapůsobit
<g/>
,	,	kIx,	,
nezáleží	záležet	k5eNaImIp3nS	záležet
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
plném	plný	k2eAgNnSc6d1	plné
pochopení	pochopení	k1gNnSc6	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
volný	volný	k2eAgInSc1d1	volný
verš	verš	k1gInSc1	verš
<g/>
,	,	kIx,	,
básníci	básník	k1gMnPc1	básník
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
hudebnost	hudebnost	k1gFnSc4	hudebnost
verše	verš	k1gInSc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
propagátorem	propagátor	k1gMnSc7	propagátor
symbolismu	symbolismus	k1gInSc2	symbolismus
časopis	časopis	k1gInSc1	časopis
Moderní	moderní	k2eAgInSc1d1	moderní
revue	revue	k1gFnSc3	revue
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
odmítány	odmítat	k5eAaImNgInP	odmítat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
představy	představ	k1gInPc1	představ
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
se	se	k3xPyFc4	se
ideál	ideál	k1gInSc1	ideál
svobodného	svobodný	k2eAgInSc2d1	svobodný
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
ničím	ničit	k5eAaImIp1nS	ničit
nespoutaného	spoutaný	k2eNgMnSc4d1	nespoutaný
jedince	jedinec	k1gMnSc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Otokar	Otokar	k1gMnSc1	Otokar
Březina	Březina	k1gMnSc1	Březina
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
Karel	Karel	k1gMnSc1	Karel
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
Antonín	Antonín	k1gMnSc1	Antonín
Sova	Sova	k1gMnSc1	Sova
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
Jan	Jan	k1gMnSc1	Jan
Opolský	opolský	k2eAgMnSc1d1	opolský
Také	také	k9	také
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
měl	mít	k5eAaImAgInS	mít
symbolismus	symbolismus	k1gInSc1	symbolismus
jistý	jistý	k2eAgInSc4d1	jistý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
symbolistických	symbolistický	k2eAgMnPc2d1	symbolistický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
hudebními	hudební	k2eAgMnPc7d1	hudební
nadšenci	nadšenec	k1gMnPc7	nadšenec
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgMnSc1	který
důsledně	důsledně	k6eAd1	důsledně
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
leitmotivy	leitmotiv	k1gInPc4	leitmotiv
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
příznačný	příznačný	k2eAgInSc1d1	příznačný
motiv	motiv	k1gInSc1	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Symbolistická	symbolistický	k2eAgFnSc1d1	symbolistická
estetika	estetika	k1gFnSc1	estetika
měla	mít	k5eAaImAgFnS	mít
hluboký	hluboký	k2eAgInSc4d1	hluboký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
Clauda	Claud	k1gMnSc4	Claud
Debussyho	Debussy	k1gMnSc4	Debussy
<g/>
,	,	kIx,	,
Erika	Erik	k1gMnSc4	Erik
Satie	Satie	k1gFnSc2	Satie
anebo	anebo	k8xC	anebo
Alexandra	Alexandr	k1gMnSc2	Alexandr
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Skrjabina	Skrjabin	k2eAgMnSc2d1	Skrjabin
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
starých	starý	k2eAgInPc2d1	starý
filmů	film	k1gInPc2	film
bylo	být	k5eAaImAgNnS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
symbolismem	symbolismus	k1gInSc7	symbolismus
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
jejich	jejich	k3xOp3gNnPc4	jejich
tvůrci	tvůrce	k1gMnPc1	tvůrce
převzali	převzít	k5eAaPmAgMnP	převzít
vizuální	vizuální	k2eAgNnSc4d1	vizuální
užívání	užívání	k1gNnSc4	užívání
metafor	metafora	k1gFnPc2	metafora
<g/>
.	.	kIx.	.
</s>
<s>
Symbolistickým	symbolistický	k2eAgNnSc7d1	symbolistické
pojetím	pojetí	k1gNnSc7	pojetí
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
především	především	k9	především
německé	německý	k2eAgInPc1d1	německý
expresionistické	expresionistický	k2eAgInPc1d1	expresionistický
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kinomatografii	kinomatografie	k1gFnSc6	kinomatografie
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc1	vliv
symbolismu	symbolismus	k1gInSc2	symbolismus
projevil	projevit	k5eAaPmAgInS	projevit
především	především	k9	především
u	u	k7c2	u
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
Davida	David	k1gMnSc2	David
Wark	Wark	k1gMnSc1	Wark
Griffitha	Griffitha	k1gMnSc1	Griffitha
<g/>
.	.	kIx.	.
</s>
<s>
Symbolistické	symbolistický	k2eAgNnSc1d1	symbolistické
užívání	užívání	k1gNnSc1	užívání
metafor	metafora	k1gFnPc2	metafora
žilo	žít	k5eAaImAgNnS	žít
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
hororech	horor	k1gInPc6	horor
dánského	dánský	k2eAgMnSc4d1	dánský
režiséra	režisér	k1gMnSc4	režisér
Carla	Carl	k1gMnSc4	Carl
Theodora	Theodor	k1gMnSc4	Theodor
Dreyera	Dreyer	k1gMnSc4	Dreyer
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Upír	upír	k1gMnSc1	upír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
–	–	k?	–
svazek	svazek	k1gInSc4	svazek
24	[number]	k4	24
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
483-485	[number]	k4	483-485
–	–	k?	–
heslo	heslo	k1gNnSc1	heslo
Symbolisté	symbolista	k1gMnPc5	symbolista
</s>
