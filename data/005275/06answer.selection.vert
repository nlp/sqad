<s>
Hermeneutická	hermeneutický	k2eAgFnSc1d1	hermeneutická
religionistika	religionistika	k1gFnSc1	religionistika
neboli	neboli	k8xC	neboli
hermeneutika	hermeneutika	k1gFnSc1	hermeneutika
náboženství	náboženství	k1gNnSc2	náboženství
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vyložit	vyložit	k5eAaPmF	vyložit
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
význam	význam	k1gInSc4	význam
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
dílčích	dílčí	k2eAgInPc6d1	dílčí
aspektech	aspekt	k1gInPc6	aspekt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
