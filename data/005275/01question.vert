<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
část	část	k1gFnSc1	část
religionistiky	religionistika	k1gFnSc2	religionistika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vyložit	vyložit	k5eAaPmF	vyložit
smysl	smysl	k1gInSc4	smysl
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
?	?	kIx.	?
</s>
