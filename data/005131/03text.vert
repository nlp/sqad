<s>
Etnografie	etnografie	k1gFnSc1	etnografie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
ethnos	ethnos	k1gMnSc1	ethnos
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
grafein	grafein	k1gInSc4	grafein
<g/>
,	,	kIx,	,
popisovat	popisovat	k5eAaImF	popisovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
národopis	národopis	k1gInSc4	národopis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
a	a	k8xC	a
popisující	popisující	k2eAgInPc4d1	popisující
modely	model	k1gInPc4	model
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
lidských	lidský	k2eAgFnPc6d1	lidská
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
etnologie	etnologie	k1gFnSc2	etnologie
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc1	synonymum
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgFnSc4d1	srovnávací
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
užívají	užívat	k5eAaImIp3nP	užívat
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
jazykové	jazykový	k2eAgFnSc6d1	jazyková
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pozvolna	pozvolna	k6eAd1	pozvolna
převládá	převládat	k5eAaImIp3nS	převládat
označení	označení	k1gNnSc1	označení
kulturní	kulturní	k2eAgFnSc2d1	kulturní
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Etnografie	etnografie	k1gFnSc1	etnografie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
romantického	romantický	k2eAgInSc2d1	romantický
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
"	"	kIx"	"
<g/>
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
venkovská	venkovský	k2eAgNnPc1d1	venkovské
společenství	společenství	k1gNnPc1	společenství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
o	o	k7c4	o
malé	malý	k2eAgInPc4d1	malý
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
o	o	k7c4	o
lidové	lidový	k2eAgNnSc4d1	lidové
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
o	o	k7c4	o
folklor	folklor	k1gInSc4	folklor
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
etnografové	etnograf	k1gMnPc1	etnograf
orientují	orientovat	k5eAaBmIp3nP	orientovat
na	na	k7c4	na
tradiční	tradiční	k2eAgFnPc4d1	tradiční
společnosti	společnost	k1gFnPc4	společnost
s	s	k7c7	s
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
industriální	industriální	k2eAgFnSc2d1	industriální
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
koloniálním	koloniální	k2eAgNnSc7d1	koloniální
panstvím	panství	k1gNnSc7	panství
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
především	především	k6eAd1	především
domorodé	domorodý	k2eAgInPc1d1	domorodý
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
zámořských	zámořský	k2eAgFnPc6d1	zámořská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
oblastí	oblast	k1gFnSc7	oblast
zájmu	zájem	k1gInSc2	zájem
lidové	lidový	k2eAgInPc1d1	lidový
zvyky	zvyk	k1gInPc1	zvyk
<g/>
,	,	kIx,	,
kroje	kroj	k1gInPc1	kroj
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
pověsti	pověst	k1gFnPc1	pověst
a	a	k8xC	a
pohádky	pohádka	k1gFnPc1	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
etnografové	etnograf	k1gMnPc1	etnograf
zabývají	zabývat	k5eAaImIp3nP	zabývat
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
popisem	popis	k1gInSc7	popis
subkultur	subkultura	k1gFnPc2	subkultura
různých	různý	k2eAgNnPc2d1	různé
dílčích	dílčí	k2eAgNnPc2d1	dílčí
společenství	společenství	k1gNnPc2	společenství
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
imigrantů	imigrant	k1gMnPc2	imigrant
nebo	nebo	k8xC	nebo
školních	školní	k2eAgMnPc2d1	školní
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
