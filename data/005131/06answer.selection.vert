<s>
Etnografie	etnografie	k1gFnSc1	etnografie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
ethnos	ethnos	k1gMnSc1	ethnos
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
grafein	grafein	k1gInSc4	grafein
<g/>
,	,	kIx,	,
popisovat	popisovat	k5eAaImF	popisovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
národopis	národopis	k1gInSc4	národopis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc1d1	zkoumající
a	a	k8xC	a
popisující	popisující	k2eAgInPc4d1	popisující
modely	model	k1gInPc4	model
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
lidských	lidský	k2eAgFnPc6d1	lidská
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
