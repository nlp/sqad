<s>
Chlorečnan	chlorečnan	k1gInSc1
draselný	draselný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
toxická	toxický	k2eAgFnSc1d1
a	a	k8xC
zdraví	zdravit	k5eAaImIp3nS
škodlivá	škodlivý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
znečišťuje	znečišťovat	k5eAaImIp3nS
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>