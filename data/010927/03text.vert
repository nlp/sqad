<p>
<s>
Štefan	Štefan	k1gMnSc1	Štefan
Buľko	Buľko	k1gNnSc4	Buľko
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1914	[number]	k4	1914
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
Strany	strana	k1gFnSc2	strana
slovenské	slovenský	k2eAgFnSc2d1	slovenská
obrody	obroda	k1gFnSc2	obroda
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
zasedl	zasednout	k5eAaPmAgMnS	zasednout
do	do	k7c2	do
slovenské	slovenský	k2eAgFnSc2d1	slovenská
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
135	[number]	k4	135
-	-	kIx~	-
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Východoslovenský	východoslovenský	k2eAgInSc1d1	východoslovenský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křeslo	křeslo	k1gNnSc4	křeslo
nabyl	nabýt	k5eAaPmAgInS	nabýt
až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1973	[number]	k4	1973
po	po	k7c6	po
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poslanec	poslanec	k1gMnSc1	poslanec
Ján	Ján	k1gMnSc1	Ján
Haško	Haško	k1gNnSc4	Haško
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
FS	FS	kA	FS
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
Ernest	Ernest	k1gMnSc1	Ernest
Balog	Balog	k1gMnSc1	Balog
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
postu	post	k1gInSc6	post
krajského	krajský	k2eAgMnSc2d1	krajský
tajemníka	tajemník	k1gMnSc2	tajemník
Strany	strana	k1gFnSc2	strana
slovenské	slovenský	k2eAgFnSc2d1	slovenská
obrody	obroda	k1gFnSc2	obroda
pro	pro	k7c4	pro
Východoslovenský	východoslovenský	k2eAgInSc4d1	východoslovenský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
též	též	k9	též
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Štefan	Štefan	k1gMnSc1	Štefan
Buľko	Buľko	k1gNnSc4	Buľko
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
