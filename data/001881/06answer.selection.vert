<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c4	za
první	první	k4xOgFnSc4	první
systémově	systémově	k6eAd1	systémově
použitelné	použitelný	k2eAgNnSc4d1	použitelné
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
uvádí	uvádět	k5eAaImIp3nS	uvádět
penicilin	penicilin	k1gInSc1	penicilin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
účinků	účinek	k1gInPc2	účinek
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
Alexander	Alexandra	k1gFnPc2	Alexandra
Fleming	Fleming	k1gInSc1	Fleming
<g/>
.	.	kIx.	.
</s>
