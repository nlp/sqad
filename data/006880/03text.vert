<s>
Krewella	Krewella	k1gFnSc1	Krewella
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
electro	electro	k6eAd1	electro
<g/>
/	/	kIx~	/
<g/>
dubstepová	dubstepový	k2eAgFnSc1d1	dubstepová
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
Jahan	Jahana	k1gFnPc2	Jahana
a	a	k8xC	a
Yasmine	Yasmin	k1gInSc5	Yasmin
Yousaf	Yousaf	k1gInSc1	Yousaf
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
vydaly	vydat	k5eAaPmAgFnP	vydat
2	[number]	k4	2
Extended	Extended	k1gInSc4	Extended
Play-e	Play-	k1gInSc2	Play-
a	a	k8xC	a
1	[number]	k4	1
album	album	k1gNnSc4	album
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
Get	Get	k1gFnSc2	Get
Wet	Wet	k1gFnSc2	Wet
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
obdarována	obdarovat	k5eAaPmNgFnS	obdarovat
pár	pár	k4xCyI	pár
cenami	cena	k1gFnPc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
singly	singl	k1gInPc4	singl
patří	patřit	k5eAaImIp3nS	patřit
"	"	kIx"	"
<g/>
Alive	Aliev	k1gFnPc1	Aliev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Live	Live	k1gFnPc2	Live
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Killin	Killin	k2eAgMnSc1d1	Killin
<g/>
'	'	kIx"	'
It	It	k1gMnSc1	It
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Krewella	Krewello	k1gNnSc2	Krewello
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krewella	Krewello	k1gNnSc2	Krewello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Krewella	Krewella	k1gMnSc1	Krewella
Krewella	Krewella	k1gMnSc1	Krewella
na	na	k7c4	na
SoundCloudu	SoundClouda	k1gFnSc4	SoundClouda
Krewella	Krewello	k1gNnSc2	Krewello
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
Krewella	Krewello	k1gNnSc2	Krewello
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
Krewella	Krewello	k1gNnSc2	Krewello
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
Jahan	Jahan	k1gMnSc1	Jahan
Yousaf	Yousaf	k1gMnSc1	Yousaf
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
Jahan	Jahan	k1gMnSc1	Jahan
Yousaf	Yousaf	k1gMnSc1	Yousaf
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
Yasmine	Yasmin	k1gInSc5	Yasmin
Yousaf	Yousaf	k1gMnSc1	Yousaf
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
Yasmine	Yasmin	k1gInSc5	Yasmin
Yousaf	Yousaf	k1gMnSc1	Yousaf
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
