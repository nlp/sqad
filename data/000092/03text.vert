<s>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
nebo	nebo	k8xC	nebo
Pacifik	Pacifik	k1gInSc1	Pacifik
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
oceán	oceán	k1gInSc1	oceán
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
165,25	[number]	k4	165,25
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
všechna	všechen	k3xTgFnSc1	všechen
pevnina	pevnina	k1gFnSc1	pevnina
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
148,94	[number]	k4	148,94
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
od	od	k7c2	od
Arktidy	Arktida	k1gFnSc2	Arktida
na	na	k7c6	na
severu	sever	k1gInSc6	sever
po	po	k7c4	po
Antarktidu	Antarktida	k1gFnSc4	Antarktida
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podle	podle	k7c2	podle
Národní	národní	k2eAgFnSc2d1	národní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hydrografické	hydrografický	k2eAgFnSc2d1	hydrografická
organizace	organizace	k1gFnSc2	organizace
však	však	k9	však
sahá	sahat	k5eAaImIp3nS	sahat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
jižní	jižní	k2eAgNnSc4d1	jižní
60	[number]	k4	60
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Austrálií	Austrálie	k1gFnSc7	Austrálie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc7d1	jižní
Amerikou	Amerika	k1gFnSc7	Amerika
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
rozlohou	rozloha	k1gFnSc7	rozloha
165,25	[number]	k4	165,25
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
155,6	[number]	k4	155,6
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
při	při	k7c6	při
uvažování	uvažování	k1gNnSc6	uvažování
Jižního	jižní	k2eAgInSc2d1	jižní
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
okrajových	okrajový	k2eAgNnPc2d1	okrajové
moří	moře	k1gNnPc2	moře
zabírá	zabírat	k5eAaImIp3nS	zabírat
46	[number]	k4	46
%	%	kIx~	%
(	(	kIx(	(
<g/>
43	[number]	k4	43
%	%	kIx~	%
<g/>
)	)	kIx)	)
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
32	[number]	k4	32
%	%	kIx~	%
(	(	kIx(	(
<g/>
30,5	[number]	k4	30,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
celkového	celkový	k2eAgInSc2d1	celkový
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
měří	měřit	k5eAaImIp3nS	měřit
přes	přes	k7c4	přes
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
obvodu	obvod	k1gInSc2	obvod
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
jeho	on	k3xPp3gNnSc2	on
délka	délka	k1gFnSc1	délka
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
16	[number]	k4	16
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
oceán	oceán	k1gInSc1	oceán
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
Severní	severní	k2eAgInSc4d1	severní
pacifický	pacifický	k2eAgInSc4d1	pacifický
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
pacifický	pacifický	k2eAgInSc1d1	pacifický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
příkop	příkop	k1gInSc1	příkop
<g/>
,	,	kIx,	,
při	při	k7c6	při
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Severního	severní	k2eAgInSc2d1	severní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
10	[number]	k4	10
994	[number]	k4	994
metrů	metr	k1gInPc2	metr
nejhlubším	hluboký	k2eAgNnSc7d3	nejhlubší
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ostatních	ostatní	k2eAgInPc6d1	ostatní
oceánech	oceán	k1gInPc6	oceán
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
lemován	lemovat	k5eAaImNgInS	lemovat
prstencem	prstenec	k1gInSc7	prstenec
činných	činný	k2eAgFnPc2d1	činná
sopek	sopka	k1gFnPc2	sopka
-	-	kIx~	-
Pacifický	pacifický	k2eAgInSc1d1	pacifický
ohňový	ohňový	k2eAgInSc1d1	ohňový
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
častá	častý	k2eAgNnPc1d1	časté
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
-	-	kIx~	-
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
přibližně	přibližně	k6eAd1	přibližně
800	[number]	k4	800
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
17	[number]	k4	17
%	%	kIx~	%
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
názvy	název	k1gInPc1	název
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
plavby	plavba	k1gFnSc2	plavba
portugalského	portugalský	k2eAgMnSc2d1	portugalský
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Fernã	Fernã	k1gMnSc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnSc2	Magalhã
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
Filipíny	Filipíny	k1gFnPc4	Filipíny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1520	[number]	k4	1520
zažil	zažít	k5eAaPmAgMnS	zažít
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
vodách	voda	k1gFnPc6	voda
klidnou	klidný	k2eAgFnSc4d1	klidná
plavbu	plavba	k1gFnSc4	plavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
se	se	k3xPyFc4	se
moře	moře	k1gNnSc1	moře
nazývá	nazývat	k5eAaImIp3nS	nazývat
Mare	Mare	k1gFnSc3	Mare
Pacificum	Pacificum	k1gInSc4	Pacificum
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
poklidné	poklidný	k2eAgNnSc4d1	poklidné
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
Pacifický	pacifický	k2eAgInSc1d1	pacifický
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
objevil	objevit	k5eAaPmAgInS	objevit
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
Španěl	Španěl	k1gMnSc1	Španěl
Vasco	Vasco	k1gMnSc1	Vasco
Nuňez	Nuňez	k1gMnSc1	Nuňez
de	de	k?	de
Balboa	Balboa	k1gMnSc1	Balboa
(	(	kIx(	(
<g/>
1476	[number]	k4	1476
<g/>
-	-	kIx~	-
<g/>
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1513	[number]	k4	1513
překročil	překročit	k5eAaPmAgInS	překročit
Panamskou	panamský	k2eAgFnSc4d1	Panamská
šíji	šíje	k1gFnSc4	šíje
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
se	se	k3xPyFc4	se
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
objevil	objevit	k5eAaPmAgInS	objevit
ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1507	[number]	k4	1507
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
kartografové	kartograf	k1gMnPc1	kartograf
neměli	mít	k5eNaImAgMnP	mít
vůbec	vůbec	k9	vůbec
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Filipínské	filipínský	k2eAgNnSc1d1	filipínské
moře	moře	k1gNnSc1	moře
Korálové	korálový	k2eAgNnSc1d1	korálové
moře	moře	k1gNnSc1	moře
Jihočínské	jihočínský	k2eAgNnSc1d1	Jihočínské
moře	moře	k1gNnSc1	moře
Tasmanovo	Tasmanův	k2eAgNnSc1d1	Tasmanovo
moře	moře	k1gNnSc1	moře
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
Japonské	japonský	k2eAgNnSc1d1	Japonské
moře	moře	k1gNnSc1	moře
Ochotské	ochotský	k2eAgNnSc1d1	Ochotské
moře	moře	k1gNnSc2	moře
</s>
