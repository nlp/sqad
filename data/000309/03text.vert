<s>
Eurozóna	Eurozóna	k1gFnSc1	Eurozóna
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
euro	euro	k1gNnSc4	euro
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
společná	společný	k2eAgFnSc1d1	společná
evropská	evropský	k2eAgFnSc1d1	Evropská
měna	měna	k1gFnSc1	měna
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
euro	euro	k1gNnSc4	euro
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
zavedly	zavést	k5eAaPmAgFnP	zavést
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
11	[number]	k4	11
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnPc2	emu
<g/>
;	;	kIx,	;
euro	euro	k1gNnSc1	euro
zavedeno	zavést	k5eAaPmNgNnS	zavést
v	v	k7c6	v
bezhotovostní	bezhotovostní	k2eAgFnSc6d1	bezhotovostní
podobě	podoba	k1gFnSc6	podoba
<g/>
;	;	kIx,	;
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
začala	začít	k5eAaPmAgFnS	začít
provádět	provádět	k5eAaImF	provádět
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Řecko	Řecko	k1gNnSc1	Řecko
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
euro	euro	k1gNnSc1	euro
zavedeno	zavést	k5eAaPmNgNnS	zavést
v	v	k7c6	v
hotovostní	hotovostní	k2eAgFnSc6d1	hotovostní
podobě	podoba	k1gFnSc6	podoba
-	-	kIx~	-
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
dány	dán	k2eAgInPc1d1	dán
euromince	eurominec	k1gInPc1	eurominec
a	a	k8xC	a
eurobankovky	eurobankovka	k1gFnPc1	eurobankovka
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g />
.	.	kIx.	.
</s>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Kypr	Kypr	k1gInSc1	Kypr
a	a	k8xC	a
Malta	Malta	k1gFnSc1	Malta
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgFnP	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Slovensko	Slovensko	k1gNnSc1	Slovensko
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Estonsko	Estonsko	k1gNnSc1	Estonsko
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnSc2	emu
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Litva	Litva	k1gFnSc1	Litva
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
EMU	emu	k1gMnPc2	emu
</s>
<s>
Členy	člen	k1gMnPc4	člen
eurozóny	eurozóna	k1gFnSc2	eurozóna
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
:	:	kIx,	:
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
eurozóny	eurozóna	k1gFnSc2	eurozóna
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
oficiální	oficiální	k2eAgNnSc1d1	oficiální
platidlo	platidlo	k1gNnSc1	platidlo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nejvzdálenějších	vzdálený	k2eAgInPc6d3	nejvzdálenější
regionech	region	k1gInPc6	region
EU	EU	kA	EU
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gFnSc1	Guadeloupe
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gInSc1	Martinik
<g/>
,	,	kIx,	,
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
Mayotte	Mayott	k1gMnSc5	Mayott
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Azory	azor	k1gInPc1	azor
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
jsou	být	k5eAaImIp3nP	být
zavázány	zavázat	k5eAaPmNgFnP	zavázat
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
povinnosti	povinnost	k1gFnSc2	povinnost
jsou	být	k5eAaImIp3nP	být
vyvázány	vyvázán	k2eAgFnPc1d1	vyvázána
pouze	pouze	k6eAd1	pouze
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mají	mít	k5eAaImIp3nP	mít
vyjednané	vyjednaný	k2eAgInPc1d1	vyjednaný
výjimky	výjimek	k1gInPc1	výjimek
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
opt-out	optut	k1gMnSc1	opt-out
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
si	se	k3xPyFc3	se
nevstoupením	nevstoupení	k1gNnPc3	nevstoupení
do	do	k7c2	do
ERM	ERM	kA	ERM
II	II	kA	II
zařídilo	zařídit	k5eAaPmAgNnS	zařídit
de	de	k?	de
facto	facto	k1gNnSc1	facto
opt-out	optut	k1gMnSc1	opt-out
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgInPc2d1	zbylý
šest	šest	k4xCc4	šest
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
prozatím	prozatím	k6eAd1	prozatím
euro	euro	k1gNnSc1	euro
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
nezavedlo	zavést	k5eNaPmAgNnS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Eurobarometr	Eurobarometr	k1gInSc4	Eurobarometr
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
eura	euro	k1gNnSc2	euro
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
29	[number]	k4	29
<g/>
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
7	[number]	k4	7
procentních	procentní	k2eAgInPc2d1	procentní
bodů	bod	k1gInPc2	bod
"	"	kIx"	"
<g/>
určitě	určitě	k6eAd1	určitě
ano	ano	k9	ano
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
používají	používat	k5eAaImIp3nP	používat
euro	euro	k1gNnSc1	euro
jako	jako	k8xS	jako
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
Monako	Monako	k1gNnSc4	Monako
<g/>
,	,	kIx,	,
San	San	k1gFnSc4	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc4	euro
zavedly	zavést	k5eAaPmAgFnP	zavést
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
měnové	měnový	k2eAgFnSc2d1	měnová
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Monaka	Monako	k1gNnSc2	Monako
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
a	a	k8xC	a
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
)	)	kIx)	)
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
čtyři	čtyři	k4xCgFnPc1	čtyři
země	zem	k1gFnPc1	zem
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
razit	razit	k5eAaImF	razit
omezené	omezený	k2eAgNnSc4d1	omezené
množství	množství	k1gNnSc4	množství
euromincí	eurominký	k2eAgMnPc1d1	eurominký
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rubovou	rubový	k2eAgFnSc7d1	rubová
národní	národní	k2eAgFnSc7d1	národní
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sanmarinské	Sanmarinský	k2eAgFnPc1d1	Sanmarinská
<g/>
,	,	kIx,	,
vatikánské	vatikánský	k2eAgFnPc1d1	Vatikánská
a	a	k8xC	a
monacké	monacký	k2eAgFnPc1d1	monacká
mince	mince	k1gFnPc1	mince
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.2002	.2002	k4	.2002
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
andorrské	andorrský	k2eAgFnPc1d1	andorrská
mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
s	s	k7c7	s
EU	EU	kA	EU
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
eura	euro	k1gNnSc2	euro
mají	mít	k5eAaImIp3nP	mít
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
rovněž	rovněž	k9	rovněž
dvě	dva	k4xCgFnPc1	dva
francouzská	francouzský	k2eAgNnPc4d1	francouzské
zámořská	zámořský	k2eAgNnPc4d1	zámořské
společenství	společenství	k1gNnPc4	společenství
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc4	Miquelon
a	a	k8xC	a
Saint-Barthélemy	Saint-Barthélema	k1gFnPc4	Saint-Barthélema
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
nemohou	moct	k5eNaImIp3nP	moct
vydávat	vydávat	k5eAaImF	vydávat
euromince	eurominko	k6eAd1	eurominko
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rubovou	rubový	k2eAgFnSc7d1	rubová
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jednostranně	jednostranně	k6eAd1	jednostranně
bez	bez	k7c2	bez
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
euro	euro	k1gNnSc1	euro
zavedly	zavést	k5eAaPmAgInP	zavést
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
vojenských	vojenský	k2eAgFnPc6d1	vojenská
základnách	základna	k1gFnPc6	základna
Akrotiri	Akrotir	k1gFnSc2	Akrotir
a	a	k8xC	a
Dhekelia	Dhekelium	k1gNnSc2	Dhekelium
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
razit	razit	k5eAaImF	razit
vlastní	vlastní	k2eAgInPc4d1	vlastní
euromince	eurominec	k1gInPc4	eurominec
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
se	se	k3xPyFc4	se
euro	euro	k1gNnSc1	euro
jako	jako	k8xC	jako
měna	měna	k1gFnSc1	měna
de	de	k?	de
iure	iure	k6eAd1	iure
používá	používat	k5eAaImIp3nS	používat
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
neobydlených	obydlený	k2eNgNnPc6d1	neobydlené
francouzských	francouzský	k2eAgNnPc6d1	francouzské
zámořských	zámořský	k2eAgNnPc6d1	zámořské
územích	území	k1gNnPc6	území
-	-	kIx~	-
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
antarktická	antarktický	k2eAgNnPc1d1	antarktické
území	území	k1gNnPc1	území
a	a	k8xC	a
Clippertonův	Clippertonův	k2eAgInSc1d1	Clippertonův
ostrov	ostrov	k1gInSc1	ostrov
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měny	měna	k1gFnSc2	měna
navázané	navázaný	k2eAgFnSc2d1	navázaná
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
používaly	používat	k5eAaImAgFnP	používat
měny	měna	k1gFnPc1	měna
navázané	navázaný	k2eAgFnPc1d1	navázaná
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
3	[number]	k4	3
francouzská	francouzský	k2eAgNnPc4d1	francouzské
zámořská	zámořský	k2eAgNnPc4d1	zámořské
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
3	[number]	k4	3
malé	malý	k2eAgInPc4d1	malý
ostrovní	ostrovní	k2eAgInPc4d1	ostrovní
státy	stát	k1gInPc4	stát
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
14	[number]	k4	14
států	stát	k1gInPc2	stát
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
1	[number]	k4	1
severoevropský	severoevropský	k2eAgInSc1d1	severoevropský
stát	stát	k1gInSc1	stát
zapojený	zapojený	k2eAgInSc1d1	zapojený
do	do	k7c2	do
ERM	ERM	kA	ERM
II	II	kA	II
a	a	k8xC	a
2	[number]	k4	2
balkánské	balkánský	k2eAgInPc1d1	balkánský
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
evropského	evropský	k2eAgInSc2d1	evropský
mechanismu	mechanismus	k1gInSc2	mechanismus
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
II	II	kA	II
(	(	kIx(	(
<g/>
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnSc2	zem
zapojené	zapojený	k2eAgFnSc6d1	zapojená
do	do	k7c2	do
ERM	ERM	kA	ERM
II	II	kA	II
musí	muset	k5eAaImIp3nS	muset
kurzy	kurz	k1gInPc4	kurz
svých	svůj	k3xOyFgFnPc2	svůj
měn	měna	k1gFnPc2	měna
udržovat	udržovat	k5eAaImF	udržovat
v	v	k7c6	v
povoleném	povolený	k2eAgNnSc6d1	povolené
fluktuačním	fluktuační	k2eAgNnSc6d1	fluktuační
pásmu	pásmo	k1gNnSc6	pásmo
±	±	k?	±
15	[number]	k4	15
%	%	kIx~	%
od	od	k7c2	od
stanoveného	stanovený	k2eAgInSc2d1	stanovený
středního	střední	k2eAgInSc2d1	střední
kurzu	kurz	k1gInSc2	kurz
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc2d1	centrální
parity	parita	k1gFnSc2	parita
<g/>
)	)	kIx)	)
k	k	k7c3	k
euru	euro	k1gNnSc3	euro
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
dodržovat	dodržovat	k5eAaImF	dodržovat
užší	úzký	k2eAgNnSc1d2	užší
fluktuační	fluktuační	k2eAgNnSc1d1	fluktuační
pásmo	pásmo	k1gNnSc1	pásmo
±	±	k?	±
2,25	[number]	k4	2,25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Alespoň	alespoň	k9	alespoň
dvouleté	dvouletý	k2eAgNnSc1d1	dvouleté
setrvání	setrvání	k1gNnSc1	setrvání
v	v	k7c6	v
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
dodržováno	dodržovat	k5eAaImNgNnS	dodržovat
povolené	povolený	k2eAgNnSc1d1	povolené
fluktuační	fluktuační	k2eAgNnSc1d1	fluktuační
pásmo	pásmo	k1gNnSc1	pásmo
pohybu	pohyb	k1gInSc2	pohyb
kurzu	kurz	k1gInSc2	kurz
a	a	k8xC	a
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
devalvaci	devalvace	k1gFnSc3	devalvace
centrální	centrální	k2eAgFnSc2d1	centrální
parity	parita	k1gFnSc2	parita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
vstupu	vstup	k1gInSc2	vstup
země	zem	k1gFnSc2	zem
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
(	(	kIx(	(
<g/>
bulharský	bulharský	k2eAgInSc4d1	bulharský
lev	lev	k1gInSc4	lev
<g/>
)	)	kIx)	)
navázánu	navázán	k2eAgFnSc4d1	navázána
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
kurzu	kurz	k1gInSc6	kurz
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
(	(	kIx(	(
<g/>
konvertibilní	konvertibilní	k2eAgFnSc1d1	konvertibilní
marka	marka	k1gFnSc1	marka
<g/>
)	)	kIx)	)
navázánu	navázán	k2eAgFnSc4d1	navázána
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
poměru	poměr	k1gInSc6	poměr
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Kapverdy	Kapverda	k1gFnPc1	Kapverda
a	a	k8xC	a
Komory	komora	k1gFnPc1	komora
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
měny	měna	k1gFnPc4	měna
(	(	kIx(	(
<g/>
kapverdské	kapverdský	k2eAgNnSc4d1	Kapverdské
escudo	escudo	k1gNnSc4	escudo
a	a	k8xC	a
komorský	komorský	k2eAgInSc4d1	komorský
frank	frank	k1gInSc4	frank
<g/>
)	)	kIx)	)
pevně	pevně	k6eAd1	pevně
navázány	navázán	k2eAgInPc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Benin	Benin	k1gMnSc1	Benin
<g/>
,	,	kIx,	,
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
<g/>
,	,	kIx,	,
Gabon	Gabon	k1gMnSc1	Gabon
<g/>
,	,	kIx,	,
Guinea-Bissau	Guinea-Bissaus	k1gInSc2	Guinea-Bissaus
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
<g/>
,	,	kIx,	,
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Togo	Togo	k1gNnSc1	Togo
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
CFA	CFA	kA	CFA
frank	frank	k1gInSc1	frank
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
navázán	navázán	k2eAgInSc1d1	navázán
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
poměru	poměr	k1gInSc6	poměr
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
CFA	CFA	kA	CFA
frank	frank	k1gInSc1	frank
pevně	pevně	k6eAd1	pevně
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
a	a	k8xC	a
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
jsou	být	k5eAaImIp3nP	být
území	území	k1gNnPc1	území
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
CFP	CFP	kA	CFP
frank	frank	k1gInSc1	frank
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
CFA	CFA	kA	CFA
frank	frank	k1gInSc4	frank
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
kurzu	kurz	k1gInSc6	kurz
navázán	navázat	k5eAaPmNgMnS	navázat
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
CFP	CFP	kA	CFP
frank	frank	k1gInSc1	frank
pevně	pevně	k6eAd1	pevně
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Svatotomášská	Svatotomášský	k2eAgNnPc1d1	Svatotomášský
dobra	dobro	k1gNnPc1	dobro
(	(	kIx(	(
<g/>
měna	měna	k1gFnSc1	měna
Svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
a	a	k8xC	a
Princova	princův	k2eAgInSc2d1	princův
ostrova	ostrov	k1gInSc2	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
navázána	navázán	k2eAgFnSc1d1	navázána
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
334	[number]	k4	334
000	[number]	k4	000
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státech	stát	k1gInPc6	stát
a	a	k8xC	a
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
měny	měna	k1gFnPc1	měna
byly	být	k5eAaImAgFnP	být
navázány	navázat	k5eAaPmNgFnP	navázat
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
209	[number]	k4	209
000	[number]	k4	000
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
