<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1863	[number]	k4	1863
v	v	k7c6	v
Lø	Lø	k1gFnSc6	Lø
<g/>
,	,	kIx,	,
Hedmark	Hedmark	k1gInSc1	Hedmark
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
norský	norský	k2eAgMnSc1d1	norský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
vlivný	vlivný	k2eAgMnSc1d1	vlivný
představitel	představitel	k1gMnSc1	představitel
moderny	moderna	k1gFnSc2	moderna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
z	z	k7c2	z
duchovní	duchovní	k2eAgFnSc2d1	duchovní
atmosféry	atmosféra	k1gFnSc2	atmosféra
secese	secese	k1gFnSc2	secese
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
malířské	malířský	k2eAgNnSc4d1	malířské
dílo	dílo	k1gNnSc4	dílo
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgInPc1	některý
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
rysy	rys	k1gInPc1	rys
tohoto	tento	k3xDgInSc2	tento
slohu	sloh	k1gInSc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
kreslířské	kreslířský	k2eAgFnSc6d1	kreslířská
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
naturalismem	naturalismus	k1gInSc7	naturalismus
<g/>
,	,	kIx,	,
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
impresionisty	impresionista	k1gMnPc7	impresionista
a	a	k8xC	a
postimpresionisty	postimpresionista	k1gMnPc7	postimpresionista
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Gauguinem	Gauguino	k1gNnSc7	Gauguino
a	a	k8xC	a
Seuratem	Seurat	k1gInSc7	Seurat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
inklinuje	inklinovat	k5eAaImIp3nS	inklinovat
k	k	k7c3	k
symbolismu	symbolismus	k1gInSc3	symbolismus
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
expresionismus	expresionismus	k1gInSc4	expresionismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
Ráno	ráno	k6eAd1	ráno
–	–	k?	–
Dívka	dívka	k1gFnSc1	dívka
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Munchovo	Munchův	k2eAgNnSc4d1	Munchovo
naturalistické	naturalistický	k2eAgNnSc4d1	naturalistické
období	období	k1gNnSc4	období
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xS	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
norského	norský	k2eAgNnSc2d1	norské
umění	umění	k1gNnSc2	umění
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
výstavou	výstava	k1gFnSc7	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
expresionistickou	expresionistický	k2eAgFnSc4d1	expresionistická
skupinu	skupina	k1gFnSc4	skupina
Die	Die	k1gMnSc2	Die
Brücke	Brück	k1gMnSc2	Brück
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
po	po	k7c6	po
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
umělecká	umělecký	k2eAgFnSc1d1	umělecká
skupina	skupina	k1gFnSc1	skupina
Osma	osma	k1gFnSc1	osma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
známé	známý	k2eAgInPc4d1	známý
obrazy	obraz	k1gInPc4	obraz
patří	patřit	k5eAaImIp3nS	patřit
Nemocné	mocný	k2eNgNnSc1d1	nemocné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Výkřik	výkřik	k1gInSc1	výkřik
<g/>
,	,	kIx,	,
Madona	Madona	k1gFnSc1	Madona
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
nemocné	nemocný	k2eAgFnSc2d1	nemocná
nebo	nebo	k8xC	nebo
Tanec	tanec	k1gInSc1	tanec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
Å	Å	k?	Å
v	v	k7c6	v
Lø	Lø	k1gMnSc6	Lø
Christianu	Christian	k1gMnSc6	Christian
Munchovi	Munch	k1gMnSc6	Munch
<g/>
,	,	kIx,	,
vojenskému	vojenský	k2eAgMnSc3d1	vojenský
lékaři	lékař	k1gMnSc3	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lauře	Laura	k1gFnSc3	Laura
Cathrine	Cathrin	k1gInSc5	Cathrin
Bjø	Bjø	k1gFnPc7	Bjø
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
jednu	jeden	k4xCgFnSc4	jeden
starší	starý	k2eAgFnSc4d2	starší
sestru	sestra	k1gFnSc4	sestra
Johannu	Johanen	k2eAgFnSc4d1	Johanna
Sophii	Sophie	k1gFnSc4	Sophie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
mladší	mladý	k2eAgMnSc1d2	mladší
sourozence	sourozenka	k1gFnSc3	sourozenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Kristianie	Kristianie	k1gFnSc2	Kristianie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
zemřela	zemřít	k5eAaPmAgFnS	zemřít
matka	matka	k1gFnSc1	matka
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1	sourozenec
byli	být	k5eAaImAgMnP	být
poté	poté	k6eAd1	poté
vychováváni	vychovávat	k5eAaImNgMnP	vychovávat
tetou	teta	k1gFnSc7	teta
Karen	Karen	k2eAgMnSc1d1	Karen
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ale	ale	k9	ale
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
příklonem	příklon	k1gInSc7	příklon
k	k	k7c3	k
pietismu	pietismus	k1gInSc3	pietismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
jeho	jeho	k3xOp3gNnSc4	jeho
myšlenkové	myšlenkový	k2eAgNnSc1d1	myšlenkové
oddálení	oddálení	k1gNnSc1	oddálení
se	se	k3xPyFc4	se
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
Edvard	Edvard	k1gMnSc1	Edvard
často	často	k6eAd1	často
nemocný	mocný	k2eNgMnSc1d1	nemocný
a	a	k8xC	a
pro	pro	k7c4	pro
zabavení	zabavení	k1gNnSc4	zabavení
si	se	k3xPyFc3	se
kreslil	kreslit	k5eAaImAgMnS	kreslit
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
byl	být	k5eAaImAgInS	být
veden	veden	k2eAgInSc1d1	veden
tetou	teta	k1gFnSc7	teta
a	a	k8xC	a
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
také	také	k9	také
uváděl	uvádět	k5eAaImAgMnS	uvádět
syna	syn	k1gMnSc4	syn
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
bavil	bavit	k5eAaImAgMnS	bavit
děti	dítě	k1gFnPc4	dítě
barvitým	barvitý	k2eAgNnSc7d1	barvité
vyprávěním	vyprávění	k1gNnSc7	vyprávění
strašidelných	strašidelný	k2eAgInPc2d1	strašidelný
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
Edgara	Edgar	k1gMnSc4	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeum	k1gNnSc2	Poeum
<g/>
.	.	kIx.	.
<g/>
Otcovy	otcův	k2eAgInPc4d1	otcův
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
upozorňování	upozorňování	k1gNnSc4	upozorňování
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
sledován	sledovat	k5eAaImNgInS	sledovat
matkou	matka	k1gFnSc7	matka
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
u	u	k7c2	u
mladého	mladý	k2eAgMnSc2d1	mladý
Edvarda	Edvard	k1gMnSc2	Edvard
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
rozvinout	rozvinout	k5eAaPmF	rozvinout
vize	vize	k1gFnPc1	vize
nočních	noční	k2eAgFnPc2d1	noční
můr	můra	k1gFnPc2	můra
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
ani	ani	k8xC	ani
zbytek	zbytek	k1gInSc1	zbytek
sourozenců	sourozenec	k1gMnPc2	sourozenec
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
nebyl	být	k5eNaImAgMnS	být
duševně	duševně	k6eAd1	duševně
ani	ani	k8xC	ani
fyzicky	fyzicky	k6eAd1	fyzicky
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
zemřela	zemřít	k5eAaPmAgFnS	zemřít
nejstarší	starý	k2eAgFnSc1d3	nejstarší
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sourozenců	sourozenec	k1gMnPc2	sourozenec
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
sestra	sestra	k1gFnSc1	sestra
vdala	vdát	k5eAaPmAgFnS	vdát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemřela	zemřít	k5eAaPmAgFnS	zemřít
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladší	mladý	k2eAgFnSc2d2	mladší
sestry	sestra	k1gFnSc2	sestra
Inger	Ingra	k1gFnPc2	Ingra
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
už	už	k6eAd1	už
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samotného	samotný	k2eAgMnSc2d1	samotný
Edvarda	Edvard	k1gMnSc2	Edvard
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
bipolární	bipolární	k2eAgFnSc1d1	bipolární
afektivní	afektivní	k2eAgFnSc1d1	afektivní
porucha	porucha	k1gFnSc1	porucha
<g/>
.	.	kIx.	.
<g/>
Plat	plat	k1gInSc1	plat
Christiana	Christian	k1gMnSc2	Christian
Muncha	Munch	k1gMnSc2	Munch
jakožto	jakožto	k8xS	jakožto
vojenského	vojenský	k2eAgMnSc2d1	vojenský
lékaře	lékař	k1gMnSc2	lékař
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgMnSc1d1	malý
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
privátní	privátní	k2eAgFnSc2d1	privátní
praxe	praxe	k1gFnSc2	praxe
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Munchovi	Munchův	k2eAgMnPc1d1	Munchův
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souviselo	souviset	k5eAaImAgNnS	souviset
časté	častý	k2eAgNnSc1d1	časté
stěhování	stěhování	k1gNnSc1	stěhování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
kresby	kresba	k1gFnPc1	kresba
a	a	k8xC	a
akvarely	akvarel	k1gInPc1	akvarel
Edvarda	Edvard	k1gMnSc2	Edvard
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
právě	právě	k9	právě
tyto	tento	k3xDgInPc1	tento
interiéry	interiér	k1gInPc1	interiér
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnSc1	zařízení
bytu	byt	k1gInSc2	byt
a	a	k8xC	a
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
potom	potom	k6eAd1	potom
i	i	k9	i
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc3	třináct
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
díly	díl	k1gInPc7	díl
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
norskou	norský	k2eAgFnSc7d1	norská
krajinářskou	krajinářský	k2eAgFnSc7d1	krajinářská
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
začal	začít	k5eAaPmAgInS	začít
kopírovat	kopírovat	k5eAaImF	kopírovat
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
potom	potom	k6eAd1	potom
začal	začít	k5eAaPmAgInS	začít
malovat	malovat	k5eAaImF	malovat
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
se	se	k3xPyFc4	se
Munch	Munch	k1gMnSc1	Munch
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
polytechniku	polytechnika	k1gFnSc4	polytechnika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sice	sice	k8xC	sice
vynikal	vynikat	k5eAaImAgMnS	vynikat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
častým	častý	k2eAgFnPc3d1	častá
nemocím	nemoc	k1gFnPc3	nemoc
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
kariéru	kariéra	k1gFnSc4	kariéra
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
malíře	malíř	k1gMnSc2	malíř
zápisem	zápis	k1gInSc7	zápis
na	na	k7c4	na
Kreslířskou	kreslířský	k2eAgFnSc4d1	kreslířská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Kristianii	Kristianie	k1gFnSc6	Kristianie
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
vzniku	vznik	k1gInSc2	vznik
stál	stát	k5eAaImAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
předek	předek	k1gMnSc1	předek
Jacob	Jacoba	k1gFnPc2	Jacoba
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nelibě	libě	k6eNd1	libě
nesl	nést	k5eAaImAgMnS	nést
jak	jak	k8xS	jak
Munchův	Munchův	k2eAgMnSc1d1	Munchův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jeho	jeho	k3xOp3gMnPc1	jeho
sousedé	soused	k1gMnPc1	soused
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
ho	on	k3xPp3gNnSc2	on
nejprve	nejprve	k6eAd1	nejprve
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
klasický	klasický	k2eAgMnSc1d1	klasický
sochař	sochař	k1gMnSc1	sochař
Julius	Julius	k1gMnSc1	Julius
Middelthun	Middelthun	k1gMnSc1	Middelthun
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
naturalistický	naturalistický	k2eAgMnSc1d1	naturalistický
malíř	malíř	k1gMnSc1	malíř
Christian	Christian	k1gMnSc1	Christian
Krohg	Krohg	k1gMnSc1	Krohg
<g/>
.	.	kIx.	.
</s>
<s>
Krohg	Krohg	k1gInSc1	Krohg
ovlivněn	ovlivněn	k2eAgInSc1d1	ovlivněn
Manetem	manet	k1gInSc7	manet
přinesl	přinést	k5eAaPmAgInS	přinést
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
experimentu	experiment	k1gInSc3	experiment
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
podporoval	podporovat	k5eAaImAgMnS	podporovat
Munchovi	Munchův	k2eAgMnPc1d1	Munchův
vrstevníky	vrstevník	k1gMnPc7	vrstevník
<g/>
,	,	kIx,	,
Muncha	Muncha	k1gFnSc1	Muncha
speciálně	speciálně	k6eAd1	speciálně
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
třetí	třetí	k4xOgFnSc3	třetí
generaci	generace	k1gFnSc3	generace
<g/>
"	"	kIx"	"
norského	norský	k2eAgNnSc2d1	norské
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gMnSc1	Munch
brzy	brzy	k6eAd1	brzy
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vstřebat	vstřebat	k5eAaPmF	vstřebat
Krohgovu	Krohgův	k2eAgFnSc4d1	Krohgův
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
mistra	mistr	k1gMnSc4	mistr
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Munch	Munch	k1gMnSc1	Munch
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
výstavu	výstava	k1gFnSc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
vliv	vliv	k1gInSc1	vliv
naturalismu	naturalismus	k1gInSc2	naturalismus
<g/>
,	,	kIx,	,
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
Maneta	manet	k1gInSc2	manet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
1884	[number]	k4	1884
obraz	obraz	k1gInSc1	obraz
Ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
mistra	mistr	k1gMnSc4	mistr
překonal	překonat	k5eAaPmAgMnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
zprvu	zprvu	k6eAd1	zprvu
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
kritikou	kritika	k1gFnSc7	kritika
odmítáno	odmítat	k5eAaImNgNnS	odmítat
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
vystavil	vystavit	k5eAaPmAgInS	vystavit
na	na	k7c6	na
Podzimní	podzimní	k2eAgFnSc6d1	podzimní
výstavě	výstava	k1gFnSc6	výstava
Studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
studii	studie	k1gFnSc4	studie
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
Nemocné	nemocný	k2eAgNnSc1d1	nemocný
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepobouřila	pobouřit	k5eNaPmAgFnS	pobouřit
námětem	námět	k1gInSc7	námět
dítěte	dítě	k1gNnSc2	dítě
umírajícího	umírající	k2eAgInSc2d1	umírající
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svoji	svůj	k3xOyFgFnSc4	svůj
skicovitostí	skicovitost	k1gFnSc7	skicovitost
<g/>
.	.	kIx.	.
<g/>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
také	také	k9	také
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
Munchovými	Munchová	k1gFnPc7	Munchová
stával	stávat	k5eAaImAgInS	stávat
složitější	složitý	k2eAgInSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
nelibě	libě	k6eNd1	libě
nesl	nést	k5eAaImAgMnS	nést
malbu	malba	k1gFnSc4	malba
aktů	akt	k1gInPc2	akt
a	a	k8xC	a
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
kristianskou	kristianský	k2eAgFnSc7d1	kristianský
bohémou	bohéma	k1gFnSc7	bohéma
<g/>
,	,	kIx,	,
především	především	k9	především
potom	potom	k6eAd1	potom
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
nihilistou	nihilista	k1gMnSc7	nihilista
a	a	k8xC	a
anarchistou	anarchista	k1gMnSc7	anarchista
Hansem	Hans	k1gMnSc7	Hans
Jæ	Jæ	k1gMnSc7	Jæ
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
poskytovat	poskytovat	k5eAaImF	poskytovat
další	další	k2eAgInPc4d1	další
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
ničil	ničit	k5eAaImAgInS	ničit
i	i	k9	i
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
bohémským	bohémský	k2eAgInSc7d1	bohémský
kruhem	kruh	k1gInSc7	kruh
se	se	k3xPyFc4	se
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
na	na	k7c6	na
Munchově	Munchův	k2eAgNnSc6d1	Munchovo
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
platil	platit	k5eAaImAgInS	platit
za	za	k7c2	za
rezervovaného	rezervovaný	k2eAgMnSc2d1	rezervovaný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vychovaného	vychovaný	k2eAgMnSc2d1	vychovaný
mladíka	mladík	k1gMnSc2	mladík
s	s	k7c7	s
respektem	respekt	k1gInSc7	respekt
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
bohémy	bohéma	k1gFnSc2	bohéma
začal	začít	k5eAaPmAgInS	začít
pít	pít	k5eAaImF	pít
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
sexuální	sexuální	k2eAgInSc1d1	sexuální
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nevázanějším	vázaný	k2eNgInSc7d2	vázaný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
Munch	Munch	k1gMnSc1	Munch
neztotožňoval	ztotožňovat	k5eNaImAgMnS	ztotožňovat
s	s	k7c7	s
tezí	teze	k1gFnSc7	teze
bohémy	bohéma	k1gFnSc2	bohéma
"	"	kIx"	"
<g/>
chovej	chovat	k5eAaImRp2nS	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
jak	jak	k8xC	jak
nejhůře	zle	k6eAd3	zle
umíš	umět	k5eAaImIp2nS	umět
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
naopak	naopak	k6eAd1	naopak
chápal	chápat	k5eAaImAgMnS	chápat
otcovu	otcův	k2eAgFnSc4d1	otcova
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
době	doba	k1gFnSc6	doba
experimentů	experiment	k1gInPc2	experiment
také	také	k9	také
Munch	Munch	k1gMnSc1	Munch
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
impresionistický	impresionistický	k2eAgInSc4d1	impresionistický
styl	styl	k1gInSc4	styl
nedává	dávat	k5eNaImIp3nS	dávat
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
a	a	k8xC	a
pod	pod	k7c4	pod
Jæ	Jæ	k1gMnPc4	Jæ
vedením	vedení	k1gNnSc7	vedení
začal	začít	k5eAaPmAgInS	začít
malovat	malovat	k5eAaImF	malovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznání	poznání	k1gNnSc2	poznání
svých	svůj	k3xOyFgInPc2	svůj
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hledal	hledat	k5eAaImAgInS	hledat
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc4	styl
používal	používat	k5eAaImAgMnS	používat
Munch	Munch	k1gMnSc1	Munch
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
stylů	styl	k1gInPc2	styl
tahu	tah	k1gInSc2	tah
štětcem	štětec	k1gInSc7	štětec
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
impresionismu	impresionismus	k1gInSc2	impresionismus
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k6eAd1	blízko
obraz	obraz	k1gInSc1	obraz
Rue	Rue	k1gMnSc5	Rue
Lafayette	Lafayett	k1gMnSc5	Lafayett
<g/>
,	,	kIx,	,
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc2	naturalismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podepsal	podepsat	k5eAaPmAgMnS	podepsat
na	na	k7c6	na
portrétu	portrét	k1gInSc6	portrét
Hanse	Hans	k1gMnSc2	Hans
Jæ	Jæ	k1gMnSc2	Jæ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
Inger	Ingra	k1gFnPc2	Ingra
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sklidil	sklidit	k5eAaPmAgInS	sklidit
vlnu	vlna	k1gFnSc4	vlna
nepochopení	nepochopení	k1gNnSc2	nepochopení
a	a	k8xC	a
negativní	negativní	k2eAgFnSc2d1	negativní
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
hraniční	hraniční	k2eAgFnPc4d1	hraniční
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
ostré	ostrý	k2eAgInPc4d1	ostrý
kontrasty	kontrast	k1gInPc4	kontrast
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
zárodky	zárodek	k1gInPc4	zárodek
vyzrálého	vyzrálý	k2eAgInSc2d1	vyzrálý
specifického	specifický	k2eAgInSc2d1	specifický
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
pečlivě	pečlivě	k6eAd1	pečlivě
promýšlet	promýšlet	k5eAaImF	promýšlet
kompozice	kompozice	k1gFnPc4	kompozice
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
citů	cit	k1gInPc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
stylisticky	stylisticky	k6eAd1	stylisticky
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
postimpresionismem	postimpresionismus	k1gInSc7	postimpresionismus
<g/>
,	,	kIx,	,
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
obrazu	obraz	k1gInSc2	obraz
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
symbolismus	symbolismus	k1gInSc4	symbolismus
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Munch	Munch	k1gInSc1	Munch
zobrazoval	zobrazovat	k5eAaImAgInS	zobrazovat
více	hodně	k6eAd2	hodně
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
hnutí	hnutí	k1gNnSc4	hnutí
než	než	k8xS	než
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
pořádal	pořádat	k5eAaImAgMnS	pořádat
Munch	Munch	k1gMnSc1	Munch
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výstavu	výstava	k1gFnSc4	výstava
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
obrazů	obraz	k1gInPc2	obraz
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc1	uznání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
,	,	kIx,	,
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Munchovi	Munch	k1gMnSc3	Munch
získat	získat	k5eAaPmF	získat
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
u	u	k7c2	u
malíře	malíř	k1gMnSc2	malíř
Léona	Léon	k1gMnSc2	Léon
Bonata	Bonat	k1gMnSc2	Bonat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paříž	Paříž	k1gFnSc1	Paříž
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
přišel	přijít	k5eAaPmAgInS	přijít
Munch	Munch	k1gInSc1	Munch
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konání	konání	k1gNnSc2	konání
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
Ráno	ráno	k6eAd1	ráno
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
pavilonu	pavilon	k1gInSc6	pavilon
<g/>
.	.	kIx.	.
</s>
<s>
Rána	Rán	k1gMnSc4	Rán
trávil	trávit	k5eAaImAgMnS	trávit
v	v	k7c6	v
Bonnatově	Bonnatův	k2eAgInSc6d1	Bonnatův
ateliéru	ateliér	k1gInSc6	ateliér
a	a	k8xC	a
odpoledne	odpoledne	k6eAd1	odpoledne
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
<g/>
,	,	kIx,	,
v	v	k7c6	v
galeriích	galerie	k1gFnPc6	galerie
a	a	k8xC	a
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studenti	student	k1gMnPc1	student
kopírovali	kopírovat	k5eAaImAgMnP	kopírovat
zavedené	zavedený	k2eAgMnPc4d1	zavedený
autory	autor	k1gMnPc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Muncha	Muncha	k1gFnSc1	Muncha
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
mistrově	mistrův	k2eAgInSc6d1	mistrův
ateliéru	ateliér	k1gInSc6	ateliér
nudila	nudit	k5eAaImAgFnS	nudit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užíval	užívat	k5eAaImAgInS	užívat
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInPc4	jeho
výklady	výklad	k1gInPc4	výklad
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
<g/>
.	.	kIx.	.
<g/>
Munch	Muncha	k1gFnPc2	Muncha
byl	být	k5eAaImAgInS	být
fascinován	fascinován	k2eAgInSc1d1	fascinován
moderním	moderní	k2eAgNnSc7d1	moderní
evropským	evropský	k2eAgNnSc7d1	Evropské
malířstvím	malířství	k1gNnSc7	malířství
a	a	k8xC	a
prokazatelné	prokazatelný	k2eAgNnSc4d1	prokazatelné
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgMnPc1	tři
malíři	malíř	k1gMnPc1	malíř
<g/>
:	:	kIx,	:
Paul	Paul	k1gMnSc1	Paul
Gauguin	Gauguin	k1gMnSc1	Gauguin
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
van	vana	k1gFnPc2	vana
Gogh	Gogha	k1gFnPc2	Gogha
a	a	k8xC	a
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Toulouse-Lautrec	Toulouse-Lautrec	k1gMnSc1	Toulouse-Lautrec
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
známí	známý	k2eAgMnPc1d1	známý
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
používání	používání	k1gNnSc4	používání
barev	barva	k1gFnPc2	barva
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
pocitů	pocit	k1gInPc2	pocit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Gaugina	Gaugin	k1gMnSc2	Gaugin
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
také	také	k9	také
jeho	jeho	k3xOp3gFnSc7	jeho
revoltou	revolta	k1gFnSc7	revolta
proti	proti	k7c3	proti
realismu	realismus	k1gInSc3	realismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
krédem	krédo	k1gNnSc7	krédo
"	"	kIx"	"
<g/>
umění	umění	k1gNnPc2	umění
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
ne	ne	k9	ne
imitace	imitace	k1gFnSc1	imitace
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Whistlera	Whistler	k1gMnSc2	Whistler
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Munchovi	Munch	k1gMnSc6	Munch
řekl	říct	k5eAaPmAgMnS	říct
jeden	jeden	k4xCgMnSc1	jeden
jeho	jeho	k3xOp3gMnSc1	jeho
berlínský	berlínský	k2eAgMnSc1d1	berlínský
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepotřeboval	potřebovat	k5eNaImAgMnS	potřebovat
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
Tahiti	Tahiti	k1gNnSc6	Tahiti
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
primitivní	primitivní	k2eAgFnSc4d1	primitivní
lidskou	lidský	k2eAgFnSc4d1	lidská
přirozenost	přirozenost	k1gFnSc4	přirozenost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgMnS	mít
svoje	svůj	k3xOyFgNnPc4	svůj
Tahiti	Tahiti	k1gNnPc4	Tahiti
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Munchův	Munchův	k2eAgMnSc1d1	Munchův
otec	otec	k1gMnSc1	otec
Christian	Christian	k1gMnSc1	Christian
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
rodinu	rodina	k1gFnSc4	rodina
nezaopatřenou	zaopatřený	k2eNgFnSc4d1	nezaopatřená
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
zařídil	zařídit	k5eAaPmAgMnS	zařídit
velkou	velký	k2eAgFnSc4d1	velká
půjčku	půjčka	k1gFnSc4	půjčka
od	od	k7c2	od
bohatého	bohatý	k2eAgMnSc2d1	bohatý
norského	norský	k2eAgMnSc2d1	norský
sběratele	sběratel	k1gMnSc2	sběratel
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
převzal	převzít	k5eAaPmAgMnS	převzít
starost	starost	k1gFnSc4	starost
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
rodiny	rodina	k1gFnSc2	rodina
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
přivedla	přivést	k5eAaPmAgFnS	přivést
Edvarda	Edvard	k1gMnSc4	Edvard
do	do	k7c2	do
depresí	deprese	k1gFnPc2	deprese
<g/>
,	,	kIx,	,
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
skicovitostí	skicovitost	k1gFnSc7	skicovitost
<g/>
,	,	kIx,	,
světlými	světlý	k2eAgInPc7d1	světlý
městskými	městský	k2eAgInPc7d1	městský
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
pointilismem	pointilismus	k1gInSc7	pointilismus
Georges	Georges	k1gMnSc1	Georges
Seurata	Seurat	k1gMnSc2	Seurat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Krajina	Krajina	k1gFnSc1	Krajina
v	v	k7c6	v
St.	st.	kA	st.
Cloud	Cloud	k1gInSc4	Cloud
<g/>
,	,	kIx,	,
Jarní	jarní	k2eAgInSc4d1	jarní
den	den	k1gInSc4	den
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Johana	Johan	k1gMnSc2	Johan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlín	Berlín	k1gInSc1	Berlín
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
namaloval	namalovat	k5eAaPmAgMnS	namalovat
pastel	pastel	k1gInSc4	pastel
Melancholie	melancholie	k1gFnSc2	melancholie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
stejnou	stejný	k2eAgFnSc4d1	stejná
scenérii	scenérie	k1gFnSc4	scenérie
jako	jako	k9	jako
u	u	k7c2	u
Inger	Ingra	k1gFnPc2	Ingra
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
barvami	barva	k1gFnPc7	barva
předznamenal	předznamenat	k5eAaPmAgMnS	předznamenat
Munchovu	Munchův	k2eAgFnSc4d1	Munchova
originální	originální	k2eAgFnSc4d1	originální
syntetickou	syntetický	k2eAgFnSc4d1	syntetická
estetiku	estetika	k1gFnSc4	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
pozval	pozvat	k5eAaPmAgInS	pozvat
Spolek	spolek	k1gInSc1	spolek
berlínských	berlínský	k2eAgMnPc2d1	berlínský
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
první	první	k4xOgFnSc6	první
výstavě	výstava	k1gFnSc6	výstava
jednoho	jeden	k4xCgMnSc2	jeden
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkaly	setkat	k5eAaPmAgFnP	setkat
s	s	k7c7	s
negativní	negativní	k2eAgFnSc7d1	negativní
kritikou	kritika	k1gFnSc7	kritika
a	a	k8xC	a
výstava	výstava	k1gFnSc1	výstava
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
zavřena	zavřít	k5eAaPmNgFnS	zavřít
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
Muncha	Muncha	k1gFnSc1	Muncha
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
ale	ale	k9	ale
nebyla	být	k5eNaImAgFnS	být
skličující	skličující	k2eAgFnSc1d1	skličující
<g/>
,	,	kIx,	,
na	na	k7c4	na
podobné	podobný	k2eAgFnPc4d1	podobná
reakce	reakce	k1gFnPc4	reakce
byl	být	k5eAaImAgInS	být
zvyklý	zvyklý	k2eAgInSc1d1	zvyklý
už	už	k6eAd1	už
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
znovu	znovu	k6eAd1	znovu
otevřel	otevřít	k5eAaPmAgMnS	otevřít
a	a	k8xC	a
vydělal	vydělat	k5eAaPmAgMnS	vydělat
na	na	k7c6	na
vstupném	vstupné	k1gNnSc6	vstupné
1800	[number]	k4	1800
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
byla	být	k5eAaImAgFnS	být
také	také	k9	také
společnost	společnost	k1gFnSc1	společnost
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
<g/>
,	,	kIx,	,
nejvýznačnější	význačný	k2eAgFnSc2d3	nejvýznačnější
byl	být	k5eAaImAgMnS	být
August	August	k1gMnSc1	August
Strindberg	Strindberg	k1gMnSc1	Strindberg
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
zkušenostech	zkušenost	k1gFnPc6	zkušenost
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
Klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Munch	Munch	k1gMnSc1	Munch
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
básníkem	básník	k1gMnSc7	básník
Stanisławem	Stanisław	k1gMnSc7	Stanisław
Przybyszewským	Przybyszewský	k1gMnSc7	Przybyszewský
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Muncha	Munch	k1gMnSc4	Munch
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
existenčními	existenční	k2eAgInPc7d1	existenční
problémy	problém	k1gInPc7	problém
–	–	k?	–
byl	být	k5eAaImAgInS	být
vykázán	vykázat	k5eAaPmNgInS	vykázat
soudně	soudně	k6eAd1	soudně
z	z	k7c2	z
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
přátelé	přítel	k1gMnPc1	přítel
našli	najít	k5eAaPmAgMnP	najít
potulujícího	potulující	k2eAgMnSc4d1	potulující
se	se	k3xPyFc4	se
tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
věřitelka	věřitelka	k1gFnSc1	věřitelka
zabavila	zabavit	k5eAaPmAgFnS	zabavit
malířský	malířský	k2eAgInSc4d1	malířský
stojan	stojan	k1gInSc4	stojan
<g/>
.	.	kIx.	.
<g/>
Malby	malba	k1gFnPc4	malba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
<g />
.	.	kIx.	.
</s>
<s>
manifest	manifest	k1gInSc1	manifest
ze	z	k7c2	z
St.	st.	kA	st.
Cloud	Clouda	k1gFnPc2	Clouda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
víc	hodně	k6eAd2	hodně
malovat	malovat	k5eAaImF	malovat
pokoje	pokoj	k1gInPc4	pokoj
<g/>
,	,	kIx,	,
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
čtou	číst	k5eAaImIp3nP	číst
<g/>
,	,	kIx,	,
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pletou	plést	k5eAaImIp3nP	plést
<g/>
,	,	kIx,	,
–	–	k?	–
měli	mít	k5eAaImAgMnP	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
živoucí	živoucí	k2eAgMnPc1d1	živoucí
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
dýchající	dýchající	k2eAgMnPc1d1	dýchající
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgMnPc1d1	trpící
a	a	k8xC	a
milující	milující	k2eAgMnPc1d1	milující
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obrazy	obraz	k1gInPc1	obraz
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
další	další	k2eAgNnSc4d1	další
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
detailnosti	detailnost	k1gFnSc2	detailnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
raný	raný	k2eAgInSc4d1	raný
vyzrálý	vyzrálý	k2eAgInSc4d1	vyzrálý
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
symbolické	symbolický	k2eAgFnPc1d1	symbolická
než	než	k8xS	než
reálné	reálný	k2eAgFnPc1d1	reálná
<g/>
:	:	kIx,	:
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
zachycení	zachycení	k1gNnSc4	zachycení
psychických	psychický	k2eAgInPc2d1	psychický
stavů	stav	k1gInPc2	stav
u	u	k7c2	u
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
Popelu	popel	k1gInSc6	popel
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc4	postava
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
zachycovány	zachycován	k2eAgFnPc1d1	zachycována
jakoby	jakoby	k8xS	jakoby
hrály	hrát	k5eAaImAgFnP	hrát
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
nemocné	nemocná	k1gFnSc2	nemocná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
výrazově	výrazově	k6eAd1	výrazově
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
a	a	k8xC	a
také	také	k9	také
nejznámější	známý	k2eAgInSc1d3	nejznámější
obraz	obraz	k1gInSc1	obraz
Výkřik	výkřik	k1gInSc1	výkřik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Kristianie	Kristianie	k1gFnSc1	Kristianie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
upozadil	upozadit	k5eAaPmAgMnS	upozadit
malbu	malba	k1gFnSc4	malba
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
grafických	grafický	k2eAgFnPc6d1	grafická
dřevorytových	dřevorytový	k2eAgFnPc6d1	dřevorytový
a	a	k8xC	a
litografických	litografický	k2eAgFnPc6d1	litografická
technikách	technika	k1gFnPc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
Vlastní	vlastní	k2eAgInSc1d1	vlastní
portrét	portrét	k1gInSc1	portrét
s	s	k7c7	s
hnátou	hnáta	k1gFnSc7	hnáta
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
využil	využít	k5eAaPmAgMnS	využít
rydlo	rydlo	k1gNnSc4	rydlo
a	a	k8xC	a
inkoust	inkoust	k1gInSc4	inkoust
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
používal	používat	k5eAaImAgMnS	používat
Paul	Paul	k1gMnSc1	Paul
Klee	Kle	k1gInSc2	Kle
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kolorované	kolorovaný	k2eAgFnPc4d1	kolorovaná
verze	verze	k1gFnPc4	verze
grafik	grafikon	k1gNnPc2	grafikon
k	k	k7c3	k
obrazům	obraz	k1gInPc3	obraz
Nemocné	nemocná	k1gFnSc2	nemocná
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
Polibek	polibek	k1gInSc4	polibek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
prodávaly	prodávat	k5eAaImAgFnP	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
ilustraci	ilustrace	k1gFnSc4	ilustrace
Baudelairových	Baudelairův	k2eAgInPc2d1	Baudelairův
Květů	květ	k1gInPc2	květ
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
sice	sice	k8xC	sice
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Baudelaira	Baudelair	k1gInSc2	Baudelair
tvořil	tvořit	k5eAaImAgInS	tvořit
grafiky	grafika	k1gFnPc4	grafika
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
–	–	k?	–
jak	jak	k6eAd1	jak
patrno	patrn	k2eAgNnSc1d1	patrno
na	na	k7c6	na
pozdější	pozdní	k2eAgFnSc6d2	pozdější
Květině	květina	k1gFnSc6	květina
bolesti	bolest	k1gFnSc2	bolest
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Quickborn	Quickborn	k1gInSc1	Quickborn
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
pařížskými	pařížský	k2eAgFnPc7d1	Pařížská
kritiky	kritika	k1gFnSc2	kritika
stále	stále	k6eAd1	stále
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
násilné	násilný	k2eAgNnSc4d1	násilné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výstavy	výstava	k1gFnPc1	výstava
měly	mít	k5eAaImAgFnP	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
koupit	koupit	k5eAaPmF	koupit
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
rybářský	rybářský	k2eAgInSc4d1	rybářský
domek	domek	k1gInSc4	domek
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
Å	Å	k?	Å
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
Šťastný	šťastný	k2eAgInSc4d1	šťastný
dům	dům	k1gInSc4	dům
<g/>
"	"	kIx"	"
a	a	k8xC	a
které	který	k3yQgInPc4	který
poté	poté	k6eAd1	poté
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
skoro	skoro	k6eAd1	skoro
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
po	po	k7c4	po
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
Kristianie	Kristianie	k1gFnSc2	Kristianie
se	se	k3xPyFc4	se
Munch	Munch	k1gMnSc1	Munch
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
nedočkal	dočkat	k5eNaPmAgInS	dočkat
pozitivního	pozitivní	k2eAgNnSc2d1	pozitivní
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
započal	započnout	k5eAaPmAgInS	započnout
Munch	Munch	k1gInSc1	Munch
intimní	intimní	k2eAgInSc4d1	intimní
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Tullou	Tullý	k2eAgFnSc7d1	Tullý
Larsenovou	Larsenová	k1gFnSc7	Larsenová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Muncha	Munch	k1gMnSc4	Munch
poté	poté	k6eAd1	poté
začalo	začít	k5eAaPmAgNnS	začít
nové	nový	k2eAgNnSc1d1	nové
umělecky	umělecky	k6eAd1	umělecky
plodné	plodný	k2eAgNnSc1d1	plodné
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgInSc6	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
především	především	k9	především
krajiny	krajina	k1gFnPc1	krajina
a	a	k8xC	a
Tanec	tanec	k1gInSc1	tanec
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
obraz	obraz	k1gInSc1	obraz
cyklu	cyklus	k1gInSc2	cyklus
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgNnSc2d1	známé
jako	jako	k8xS	jako
Vlys	vlys	k1gInSc1	vlys
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tulla	Tulla	k1gFnSc1	Tulla
projevovala	projevovat	k5eAaImAgFnS	projevovat
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Munch	Munch	k1gInSc1	Munch
se	se	k3xPyFc4	se
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
kvůli	kvůli	k7c3	kvůli
chatrnému	chatrný	k2eAgNnSc3d1	chatrné
zdraví	zdraví	k1gNnSc3	zdraví
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
také	také	k9	také
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
pití	pití	k1gNnSc3	pití
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
si	se	k3xPyFc3	se
do	do	k7c2	do
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
"	"	kIx"	"
<g/>
právo	právo	k1gNnSc1	právo
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
ženit	ženit	k5eAaImF	ženit
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
dostal	dostat	k5eAaPmAgInS	dostat
ránu	rána	k1gFnSc4	rána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Munch	Munch	k1gMnSc1	Munch
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
výstavě	výstava	k1gFnSc6	výstava
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgInS	pozvat
Maxem	Max	k1gMnSc7	Max
Liebermannem	Liebermann	k1gMnSc7	Liebermann
vystavil	vystavit	k5eAaPmAgMnS	vystavit
celý	celý	k2eAgInSc4d1	celý
cyklus	cyklus	k1gInSc4	cyklus
Vlys	vlys	k1gInSc1	vlys
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Berlínští	berlínský	k2eAgMnPc1d1	berlínský
kritikové	kritik	k1gMnPc1	kritik
začali	začít	k5eAaPmAgMnP	začít
na	na	k7c4	na
Muncha	Munch	k1gMnSc4	Munch
nahlížet	nahlížet	k5eAaImF	nahlížet
poněkud	poněkud	k6eAd1	poněkud
pozitivněji	pozitivně	k6eAd2	pozitivně
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
publikum	publikum	k1gNnSc4	publikum
zůstával	zůstávat	k5eAaImAgInS	zůstávat
příliš	příliš	k6eAd1	příliš
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
a	a	k8xC	a
podivný	podivný	k2eAgInSc1d1	podivný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
přilákal	přilákat	k5eAaPmAgInS	přilákat
pozornost	pozornost	k1gFnSc4	pozornost
dvou	dva	k4xCgMnPc2	dva
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
,	,	kIx,	,
Alberta	Albert	k1gMnSc2	Albert
Kollmana	Kollman	k1gMnSc2	Kollman
a	a	k8xC	a
Maxe	Max	k1gMnSc2	Max
Lindeho	Linde	k1gMnSc2	Linde
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Munch	Munch	k1gInSc4	Munch
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k9	jako
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
ho	on	k3xPp3gMnSc4	on
nezbavila	zbavit	k5eNaPmAgFnS	zbavit
sebezničujícího	sebezničující	k2eAgNnSc2d1	sebezničující
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
nepředvidatelných	předvidatelný	k2eNgFnPc2d1	nepředvidatelná
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Notně	notně	k6eAd1	notně
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hádek	hádka	k1gFnPc2	hádka
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
a	a	k8xC	a
jiným	jiný	k2eAgMnSc7d1	jiný
malířem	malíř	k1gMnSc7	malíř
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
se	s	k7c7	s
střelbou	střelba	k1gFnSc7	střelba
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
Tully	Tulla	k1gFnSc2	Tulla
Larsenová	Larsenový	k2eAgFnSc1d1	Larsenový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
s	s	k7c7	s
Munchem	Munch	k1gInSc7	Munch
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
Tulla	Tulla	k1gFnSc1	Tulla
definitivně	definitivně	k6eAd1	definitivně
rozešla	rozejít	k5eAaPmAgFnS	rozejít
a	a	k8xC	a
vzala	vzít	k5eAaPmAgFnS	vzít
si	se	k3xPyFc3	se
mladšího	mladý	k2eAgMnSc2d2	mladší
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gMnSc1	Munch
to	ten	k3xDgNnSc4	ten
přijal	přijmout	k5eAaPmAgMnS	přijmout
jako	jako	k8xS	jako
zradu	zrada	k1gFnSc4	zrada
<g/>
,	,	kIx,	,
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
plně	plně	k6eAd1	plně
neuróza	neuróza	k1gFnSc1	neuróza
a	a	k8xC	a
pocit	pocit	k1gInSc1	pocit
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
řadu	řada	k1gFnSc4	řada
karikatur	karikatura	k1gFnPc2	karikatura
Tully	Tulla	k1gFnSc2	Tulla
a	a	k8xC	a
samotné	samotný	k2eAgFnSc2d1	samotná
emoční	emoční	k2eAgInSc4d1	emoční
kontext	kontext	k1gInSc4	kontext
osudové	osudový	k2eAgFnSc2d1	osudová
střelby	střelba	k1gFnSc2	střelba
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
Maratova	Maratův	k2eAgFnSc1d1	Maratova
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1903	[number]	k4	1903
a	a	k8xC	a
1904	[number]	k4	1904
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
fauvisté	fauvista	k1gMnPc1	fauvista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Muncha	Munch	k1gMnSc4	Munch
uchvátili	uchvátit	k5eAaPmAgMnP	uchvátit
–	–	k?	–
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
–	–	k?	–
svým	svůj	k3xOyFgNnSc7	svůj
drzým	drzý	k2eAgNnSc7d1	drzé
užíváním	užívání	k1gNnSc7	užívání
falešných	falešný	k2eAgFnPc2d1	falešná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
Rodinovy	Rodinův	k2eAgFnPc4d1	Rodinova
sochy	socha	k1gFnPc4	socha
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
si	se	k3xPyFc3	se
nejspíš	nejspíš	k9	nejspíš
vzal	vzít	k5eAaPmAgMnS	vzít
plastičnost	plastičnost	k1gFnSc4	plastičnost
pomáhající	pomáhající	k2eAgMnSc1d1	pomáhající
u	u	k7c2	u
návrhů	návrh	k1gInPc2	návrh
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sochy	socha	k1gFnPc1	socha
nezačal	začít	k5eNaPmAgInS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
dostával	dostávat	k5eAaImAgMnS	dostávat
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
portréty	portrét	k1gInPc4	portrét
a	a	k8xC	a
grafiky	grafika	k1gFnPc4	grafika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
orientovat	orientovat	k5eAaBmF	orientovat
z	z	k7c2	z
krajinářství	krajinářství	k1gNnSc2	krajinářství
na	na	k7c4	na
malbu	malba	k1gFnSc4	malba
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
mu	on	k3xPp3gMnSc3	on
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
přesídlení	přesídlení	k1gNnSc4	přesídlení
do	do	k7c2	do
Warnemünde	Warnemünd	k1gMnSc5	Warnemünd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
podklady	podklad	k1gInPc4	podklad
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
plátnu	plátno	k1gNnSc3	plátno
Koupající	koupající	k2eAgFnSc2d1	koupající
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zhroucení	zhroucení	k1gNnSc1	zhroucení
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInSc1	jeho
psychický	psychický	k2eAgInSc1d1	psychický
stav	stav	k1gInSc1	stav
vážný	vážný	k2eAgInSc1d1	vážný
a	a	k8xC	a
proto	proto	k8xC	proto
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
doktora	doktor	k1gMnSc2	doktor
Daniela	Daniel	k1gMnSc2	Daniel
Jacobsona	Jacobson	k1gMnSc2	Jacobson
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
podrobil	podrobit	k5eAaPmAgInS	podrobit
psychoterapii	psychoterapie	k1gFnSc3	psychoterapie
trvající	trvající	k2eAgFnSc2d1	trvající
osm	osm	k4xCc1	osm
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
opuštění	opuštění	k1gNnSc2	opuštění
kliniky	klinika	k1gFnSc2	klinika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
optimističtější	optimistický	k2eAgInSc1d2	optimističtější
a	a	k8xC	a
barevnější	barevný	k2eAgInSc1d2	barevnější
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc4	co
ještě	ještě	k9	ještě
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
zlepšení	zlepšení	k1gNnSc1	zlepšení
jeho	jeho	k3xOp3gInSc2	jeho
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
názorový	názorový	k2eAgInSc1d1	názorový
obrat	obrat	k1gInSc1	obrat
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
Kristianii	Kristianie	k1gFnSc6	Kristianie
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Muzea	muzeum	k1gNnPc1	muzeum
začala	začít	k5eAaPmAgNnP	začít
nakupovat	nakupovat	k5eAaBmF	nakupovat
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
výzdobu	výzdoba	k1gFnSc4	výzdoba
auly	aula	k1gFnSc2	aula
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Kristianii	Kristianie	k1gFnSc6	Kristianie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Řádem	řád	k1gInSc7	řád
sv.	sv.	kA	sv.
Olafa	Olaf	k1gMnSc2	Olaf
za	za	k7c4	za
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
službu	služba	k1gFnSc4	služba
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
výstava	výstava	k1gFnSc1	výstava
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jacobson	Jacobson	k1gMnSc1	Jacobson
doporučil	doporučit	k5eAaPmAgMnS	doporučit
stýkat	stýkat	k5eAaImF	stýkat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
se	se	k3xPyFc4	se
veřejného	veřejný	k2eAgNnSc2d1	veřejné
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gMnSc1	Munch
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
rady	rada	k1gFnPc4	rada
snažil	snažit	k5eAaImAgMnS	snažit
dodržet	dodržet	k5eAaPmF	dodržet
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souviselo	souviset	k5eAaImAgNnS	souviset
vytvoření	vytvoření	k1gNnSc1	vytvoření
řady	řada	k1gFnSc2	řada
celkových	celkový	k2eAgInPc2d1	celkový
portrétů	portrét	k1gInPc2	portrét
dobré	dobrý	k2eAgFnSc2d1	dobrá
kvality	kvalita	k1gFnSc2	kvalita
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
několik	několik	k4yIc4	několik
krajin	krajina	k1gFnPc2	krajina
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
zachycených	zachycený	k2eAgInPc2d1	zachycený
v	v	k7c6	v
optimistickém	optimistický	k2eAgInSc6d1	optimistický
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
patrnými	patrný	k2eAgInPc7d1	patrný
širokými	široký	k2eAgInPc7d1	široký
a	a	k8xC	a
uvolněnými	uvolněný	k2eAgInPc7d1	uvolněný
tahy	tah	k1gInPc7	tah
štětce	štětec	k1gInSc2	štětec
v	v	k7c6	v
zářivých	zářivý	k2eAgFnPc6d1	zářivá
barvách	barva	k1gFnPc6	barva
s	s	k7c7	s
bílými	bílý	k2eAgNnPc7d1	bílé
místy	místo	k1gNnPc7	místo
a	a	k8xC	a
výjimečným	výjimečný	k2eAgNnSc7d1	výjimečné
užitím	užití	k1gNnSc7	užití
černé	černá	k1gFnSc2	černá
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
příjmu	příjem	k1gInSc2	příjem
si	se	k3xPyFc3	se
koupil	koupit	k5eAaPmAgMnS	koupit
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
usedlost	usedlost	k1gFnSc1	usedlost
v	v	k7c4	v
oselské	oselský	k2eAgInPc4d1	oselský
Ekely	Ekel	k1gInPc4	Ekel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
prožíval	prožívat	k5eAaImAgMnS	prožívat
ambivalentní	ambivalentní	k2eAgInPc4d1	ambivalentní
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
miloval	milovat	k5eAaImAgMnS	milovat
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
přežil	přežít	k5eAaPmAgMnS	přežít
řádění	řádění	k1gNnSc4	řádění
španělské	španělský	k2eAgFnSc2d1	španělská
chřipky	chřipka	k1gFnSc2	chřipka
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
jinak	jinak	k6eAd1	jinak
chatrnému	chatrný	k2eAgNnSc3d1	chatrné
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
jakožto	jakožto	k8xS	jakožto
židé	žid	k1gMnPc1	žid
o	o	k7c4	o
svoje	svůj	k3xOyFgNnSc4	svůj
jmění	jmění	k1gNnSc4	jmění
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
i	i	k9	i
o	o	k7c4	o
život	život	k1gInSc4	život
při	při	k7c6	při
vzestupu	vzestup	k1gInSc6	vzestup
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
norskou	norský	k2eAgFnSc4d1	norská
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohli	moct	k5eAaImAgMnP	moct
němečtí	německý	k2eAgMnPc1d1	německý
umělci	umělec	k1gMnPc1	umělec
tisknout	tisknout	k5eAaImF	tisknout
svoje	svůj	k3xOyFgFnPc4	svůj
grafiky	grafika	k1gFnPc4	grafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c4	v
Ekely	Ekel	k1gInPc4	Ekel
si	se	k3xPyFc3	se
Munch	Munch	k1gMnSc1	Munch
vybíral	vybírat	k5eAaImAgMnS	vybírat
venkovské	venkovský	k2eAgInPc4d1	venkovský
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgInSc4d1	oslavující
zemědělský	zemědělský	k2eAgInSc4d1	zemědělský
život	život	k1gInSc4	život
na	na	k7c6	na
kterých	který	k3yRgMnPc6	který
mu	on	k3xPp3gMnSc3	on
pózoval	pózovat	k5eAaImAgInS	pózovat
také	také	k9	také
jeho	on	k3xPp3gInSc4	on
kůň	kůň	k1gMnSc1	kůň
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostředí	prostředí	k1gNnSc6	prostředí
získal	získat	k5eAaPmAgInS	získat
stálý	stálý	k2eAgInSc1d1	stálý
přísun	přísun	k1gInSc1	přísun
modelek	modelka	k1gFnPc2	modelka
které	který	k3yQgFnSc2	který
mu	on	k3xPp3gMnSc3	on
pózovaly	pózovat	k5eAaImAgFnP	pózovat
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
aktů	akt	k1gInPc2	akt
a	a	k8xC	a
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
měl	mít	k5eAaImAgMnS	mít
možná	možná	k9	možná
sexuální	sexuální	k2eAgInSc4d1	sexuální
styk	styk	k1gInSc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gMnSc1	Munch
se	se	k3xPyFc4	se
také	také	k9	také
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
monumentální	monumentální	k2eAgInPc4d1	monumentální
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
velké	velký	k2eAgFnPc4d1	velká
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c4	na
výzdobu	výzdoba	k1gFnSc4	výzdoba
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
čokoládovnu	čokoládovna	k1gFnSc4	čokoládovna
Freia	Freium	k1gNnSc2	Freium
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
bylo	být	k5eAaImAgNnS	být
Munchovo	Munchův	k2eAgNnSc1d1	Munchovo
dílo	dílo	k1gNnSc1	dílo
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
zvrhlé	zvrhlý	k2eAgNnSc1d1	zvrhlé
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
82	[number]	k4	82
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
bylo	být	k5eAaImAgNnS	být
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Muncha	Munch	k1gMnSc4	Munch
těžká	těžký	k2eAgFnSc1d1	těžká
rána	rána	k1gFnSc1	rána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Německo	Německo	k1gNnSc4	Německo
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
Němci	Němec	k1gMnPc1	Němec
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Norsko	Norsko	k1gNnSc4	Norsko
a	a	k8xC	a
Munch	Munch	k1gInSc4	Munch
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
obavách	obava	k1gFnPc6	obava
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc7d1	celá
sbírkou	sbírka	k1gFnSc7	sbírka
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gMnSc1	Munch
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
nachlazení	nachlazení	k1gNnSc4	nachlazení
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nacistického	nacistický	k2eAgNnSc2d1	nacistické
pohřbu	pohřeb	k1gInSc6	pohřeb
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
kolaboroval	kolaborovat	k5eAaImAgMnS	kolaborovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
umělce	umělec	k1gMnPc4	umělec
===	===	k?	===
</s>
</p>
<p>
<s>
Munchovo	Munchův	k2eAgNnSc1d1	Munchovo
umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
osobité	osobitý	k2eAgNnSc1d1	osobité
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
ho	on	k3xPp3gNnSc2	on
příliš	příliš	k6eAd1	příliš
neobjasňoval	objasňovat	k5eNaImAgMnS	objasňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
symbolismu	symbolismus	k1gInSc2	symbolismus
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
osobnější	osobní	k2eAgInSc1d2	osobnější
než	než	k8xS	než
u	u	k7c2	u
Gustava	Gustav	k1gMnSc2	Gustav
Moreaua	Moreaua	k1gFnSc1	Moreaua
a	a	k8xC	a
Jamese	Jamese	k1gFnSc1	Jamese
Ensora	Ensora	k1gFnSc1	Ensora
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velmi	velmi	k6eAd1	velmi
vlivný	vlivný	k2eAgMnSc1d1	vlivný
<g/>
,	,	kIx,	,
především	především	k9	především
potom	potom	k6eAd1	potom
na	na	k7c6	na
nově	nova	k1gFnSc6	nova
vznikající	vznikající	k2eAgInSc1d1	vznikající
německý	německý	k2eAgInSc1d1	německý
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nevěřím	věřit	k5eNaImIp1nS	věřit
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
nutkání	nutkání	k1gNnSc2	nutkání
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
svého	svůj	k3xOyFgNnSc2	svůj
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Krom	krom	k7c2	krom
jeho	on	k3xPp3gInSc2	on
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
jako	jako	k8xS	jako
předchůdce	předchůdce	k1gMnSc2	předchůdce
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Munch	Munch	k1gMnSc1	Munch
sám	sám	k3xTgInSc4	sám
považován	považován	k2eAgInSc4d1	považován
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
představitele	představitel	k1gMnSc4	představitel
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc7	jeho
obrazy	obraz	k1gInPc7	obraz
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
potom	potom	k6eAd1	potom
Výkřik	výkřik	k1gInSc4	výkřik
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgInP	stát
obecně	obecně	k6eAd1	obecně
známými	známý	k1gMnPc7	známý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
a	a	k8xC	a
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
===	===	k?	===
</s>
</p>
<p>
<s>
S	s	k7c7	s
veskrze	veskrze	k6eAd1	veskrze
pozitivním	pozitivní	k2eAgNnSc7d1	pozitivní
přijetím	přijetí	k1gNnSc7	přijetí
se	se	k3xPyFc4	se
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
setkal	setkat	k5eAaPmAgMnS	setkat
při	při	k7c6	při
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
SUV	SUV	kA	SUV
Mánesu	Mánes	k1gMnSc6	Mánes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
80	[number]	k4	80
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
40	[number]	k4	40
grafik	grafika	k1gFnPc2	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
revue	revue	k1gFnSc1	revue
reprodukovala	reprodukovat	k5eAaBmAgFnS	reprodukovat
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
a	a	k8xC	a
již	již	k6eAd1	již
rok	rok	k1gInSc4	rok
před	před	k7c7	před
Munchovou	Munchův	k2eAgFnSc7d1	Munchova
pražskou	pražský	k2eAgFnSc7d1	Pražská
výstavou	výstava	k1gFnSc7	výstava
obdivnými	obdivný	k2eAgNnPc7d1	obdivné
slovy	slovo	k1gNnPc7	slovo
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
mocnost	mocnost	k1gFnSc1	mocnost
rudimentární	rudimentární	k2eAgFnSc2d1	rudimentární
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	a
smyslného	smyslný	k2eAgInSc2d1	smyslný
tónu	tón	k1gInSc2	tón
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
ocenila	ocenit	k5eAaPmAgNnP	ocenit
Munchova	Munchův	k2eAgNnPc1d1	Munchovo
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
pražském	pražský	k2eAgMnSc6d1	pražský
Mánesu	Mánes	k1gMnSc6	Mánes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
vystavena	vystaven	k2eAgFnSc1d1	vystavena
<g/>
.	.	kIx.	.
<g/>
Munch	Munch	k1gMnSc1	Munch
také	také	k9	také
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
počátky	počátek	k1gInPc4	počátek
českého	český	k2eAgInSc2d1	český
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
výstavě	výstava	k1gFnSc6	výstava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
Munchem	Munch	k1gInSc7	Munch
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
členové	člen	k1gMnPc1	člen
Osmy	osma	k1gFnSc2	osma
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
skutečný	skutečný	k2eAgInSc1d1	skutečný
vliv	vliv	k1gInSc1	vliv
mytizován	mytizován	k2eAgInSc1d1	mytizován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
galeriích	galerie	k1gFnPc6	galerie
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
odkázal	odkázat	k5eAaPmAgMnS	odkázat
Munch	Munch	k1gMnSc1	Munch
zbývající	zbývající	k2eAgNnSc4d1	zbývající
dílo	dílo	k1gNnSc4	dílo
Oslu	Oslo	k1gNnSc3	Oslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
postavilo	postavit	k5eAaPmAgNnS	postavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
Munchovo	Munchův	k2eAgNnSc1d1	Munchovo
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Munch-museet	Munchuseet	k1gInSc1	Munch-museet
<g/>
)	)	kIx)	)
v	v	k7c6	v
Tø	Tø	k1gFnSc6	Tø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
vystavováno	vystavován	k2eAgNnSc1d1	vystavováno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
1100	[number]	k4	1100
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
4500	[number]	k4	4500
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
18	[number]	k4	18
000	[number]	k4	000
tisků	tisk	k1gInPc2	tisk
a	a	k8xC	a
šest	šest	k4xCc1	šest
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
sbírka	sbírka	k1gFnSc1	sbírka
Munchova	Munchův	k2eAgNnSc2d1	Munchovo
díla	dílo	k1gNnSc2	dílo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
také	také	k9	také
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
správcem	správce	k1gMnSc7	správce
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
autorskoprávní	autorskoprávní	k2eAgFnPc4d1	autorskoprávní
náležitosti	náležitost	k1gFnPc4	náležitost
spojné	spojný	k2eAgFnPc4d1	spojná
s	s	k7c7	s
Munchovým	Munchův	k2eAgInSc7d1	Munchův
odkazem	odkaz	k1gInSc7	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
Munchovy	Munchův	k2eAgInPc4d1	Munchův
obrazy	obraz	k1gInPc4	obraz
vystavovány	vystavován	k2eAgInPc4d1	vystavován
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
důležitých	důležitý	k2eAgNnPc6d1	důležité
světových	světový	k2eAgNnPc6d1	světové
muzeích	muzeum	k1gNnPc6	muzeum
a	a	k8xC	a
galeriích	galerie	k1gFnPc6	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
kulturní	kulturní	k2eAgFnSc2d1	kulturní
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Munch	Munch	k1gMnSc1	Munch
prvním	první	k4xOgNnSc7	první
západním	západní	k2eAgMnSc7d1	západní
mistrem	mistr	k1gMnSc7	mistr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
vystavený	vystavený	k2eAgInSc4d1	vystavený
obraz	obraz	k1gInSc4	obraz
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
"	"	kIx"	"
<g/>
Šťastného	Šťastného	k2eAgInSc2d1	Šťastného
domu	dům	k1gInSc2	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nazýval	nazývat	k5eAaImAgInS	nazývat
Munch	Munch	k1gInSc1	Munch
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Å	Å	k?	Å
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zdědila	zdědit	k5eAaPmAgFnS	zdědit
å	å	k?	å
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
malé	malý	k2eAgNnSc1d1	malé
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
dobovým	dobový	k2eAgNnSc7d1	dobové
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
Výkřiku	výkřik	k1gInSc2	výkřik
byla	být	k5eAaImAgFnS	být
ukradena	ukraden	k2eAgFnSc1d1	ukradena
z	z	k7c2	z
norského	norský	k2eAgInSc2d1	norský
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnPc1	galerie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
ukradena	ukraden	k2eAgFnSc1d1	ukradena
jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
Výkřiku	výkřik	k1gInSc2	výkřik
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Madonou	Madona	k1gFnSc7	Madona
z	z	k7c2	z
Munchova	Munchův	k2eAgNnSc2d1	Munchovo
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
všechny	všechen	k3xTgInPc1	všechen
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
našly	najít	k5eAaPmAgFnP	najít
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
znovu	znovu	k6eAd1	znovu
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
loupeže	loupež	k1gFnSc2	loupež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byly	být	k5eAaImAgInP	být
zčásti	zčásti	k6eAd1	zčásti
nenávratně	návratně	k6eNd1	návratně
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pocty	pocta	k1gFnSc2	pocta
a	a	k8xC	a
aukce	aukce	k1gFnSc2	aukce
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
kolorovaný	kolorovaný	k2eAgInSc1d1	kolorovaný
dřevoryt	dřevoryt	k1gInSc1	dřevoryt
To	to	k9	to
mennesker	mennesker	k1gInSc1	mennesker
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
ensomme	ensomit	k5eAaPmRp1nP	ensomit
(	(	kIx(	(
<g/>
Dva	dva	k4xCgMnPc4	dva
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Osamělí	osamělý	k2eAgMnPc1d1	osamělý
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
na	na	k7c6	na
aukci	aukce	k1gFnSc6	aukce
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
za	za	k7c4	za
8,1	[number]	k4	8,1
milionů	milion	k4xCgInPc2	milion
NOK	NOK	kA	NOK
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
rekordní	rekordní	k2eAgFnSc1d1	rekordní
částka	částka	k1gFnSc1	částka
zaplacená	zaplacený	k2eAgFnSc1d1	zaplacená
v	v	k7c6	v
aukci	aukce	k1gFnSc6	aukce
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
Vampýr	vampýr	k1gMnSc1	vampýr
vydražen	vydražen	k2eAgInSc4d1	vydražen
za	za	k7c4	za
38,162	[number]	k4	38,162
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
Sotheby	Sotheb	k1gInPc7	Sotheb
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Munchova	Munchův	k2eAgFnSc1d1	Munchova
podobizna	podobizna	k1gFnSc1	podobizna
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
bankovce	bankovka	k1gFnSc6	bankovka
s	s	k7c7	s
nominální	nominální	k2eAgFnSc7d1	nominální
hodnotou	hodnota	k1gFnSc7	hodnota
1000	[number]	k4	1000
NOK	NOK	kA	NOK
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
stylizací	stylizace	k1gFnSc7	stylizace
obrazu	obraz	k1gInSc2	obraz
Melancholie	melancholie	k1gFnSc2	melancholie
a	a	k8xC	a
se	s	k7c7	s
studií	studie	k1gFnSc7	studie
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
na	na	k7c6	na
reversu	revers	k1gInSc6	revers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
Peterem	Peter	k1gMnSc7	Peter
Watkinsem	Watkins	k1gMnSc7	Watkins
biografický	biografický	k2eAgInSc4d1	biografický
film	film	k1gInSc4	film
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
1889	[number]	k4	1889
–	–	k?	–
Inger	Inger	k1gMnSc1	Inger
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
</s>
</p>
<p>
<s>
1890	[number]	k4	1890
–	–	k?	–
Noc	noc	k1gFnSc4	noc
v	v	k7c6	v
St.	st.	kA	st.
Cloud	Cloud	k1gMnSc1	Cloud
</s>
</p>
<p>
<s>
1892	[number]	k4	1892
–	–	k?	–
Dopoledne	dopoledne	k1gNnSc1	dopoledne
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Johana	Johan	k1gMnSc2	Johan
<g/>
,	,	kIx,	,
Večer	večer	k6eAd1	večer
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Johana	Johan	k1gMnSc2	Johan
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
–	–	k?	–
Bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
Výkřik	výkřik	k1gInSc1	výkřik
<g/>
,	,	kIx,	,
Odloučení	odloučení	k1gNnSc1	odloučení
</s>
</p>
<p>
<s>
1894	[number]	k4	1894
–	–	k?	–
Popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
Úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
Vampýr	vampýr	k1gMnSc1	vampýr
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Przybyszewského	Przybyszewské	k1gNnSc2	Przybyszewské
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
–	–	k?	–
Madona	Madona	k1gFnSc1	Madona
</s>
</p>
<p>
<s>
1895	[number]	k4	1895
–	–	k?	–
Puberta	puberta	k1gFnSc1	puberta
<g/>
,	,	kIx,	,
Vlastní	vlastní	k2eAgInSc1d1	vlastní
portrét	portrét	k1gInSc1	portrét
s	s	k7c7	s
cigaretou	cigareta	k1gFnSc7	cigareta
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
nemocné	nemocná	k1gFnSc2	nemocná
<g/>
,	,	kIx,	,
U	u	k7c2	u
úmrtního	úmrtní	k2eAgNnSc2d1	úmrtní
lože	lože	k1gNnSc2	lože
</s>
</p>
<p>
<s>
1899	[number]	k4	1899
–	–	k?	–
Metabolismus	metabolismus	k1gInSc1	metabolismus
(	(	kIx(	(
<g/>
pracovně	pracovně	k6eAd1	pracovně
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
–	–	k?	–
Tanec	tanec	k1gInSc1	tanec
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
–	–	k?	–
Dívky	dívka	k1gFnSc2	dívka
na	na	k7c6	na
mostě	most	k1gInSc6	most
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Maxe	Max	k1gMnSc2	Max
Liebermanna	Liebermann	k1gMnSc2	Liebermann
Munchův	Munchův	k2eAgInSc4d1	Munchův
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
obraz	obraz	k1gInSc4	obraz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
–	–	k?	–
Maratova	Maratův	k2eAgFnSc1d1	Maratova
smrt	smrt	k1gFnSc1	smrt
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
–	–	k?	–
Koupající	koupající	k2eAgFnSc4d1	koupající
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
obraz	obraz	k1gInSc1	obraz
způsobil	způsobit	k5eAaPmAgInS	způsobit
dohady	dohad	k1gInPc4	dohad
o	o	k7c6	o
Munchově	Munchův	k2eAgFnSc6d1	Munchova
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SCHIEFLER	SCHIEFLER	kA	SCHIEFLER
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
graphische	graphischat	k5eAaPmIp3nS	graphischat
Werk	Werk	k1gInSc4	Werk
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Euphorion	Euphorion	k1gInSc1	Euphorion
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SCHIEFLER	SCHIEFLER	kA	SCHIEFLER
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Verzeichnis	Verzeichnis	k1gInSc1	Verzeichnis
des	des	k1gNnSc2	des
graphischen	graphischen	k2eAgInSc1d1	graphischen
Werks	Werks	k1gInSc1	Werks
Edvard	Edvard	k1gMnSc1	Edvard
Munchs	Munchs	k1gInSc1	Munchs
bis	bis	k?	bis
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Bruno	Bruno	k1gMnSc1	Bruno
Cassirer	Cassirer	k1gMnSc1	Cassirer
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LAMAČ	lamač	k1gMnSc1	lamač
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HELLER	HELLER	kA	HELLER
<g/>
,	,	kIx,	,
Reinhold	Reinhold	k1gInSc1	Reinhold
<g/>
.	.	kIx.	.
</s>
<s>
Munch	Munch	k1gInSc1	Munch
:	:	kIx,	:
His	his	k1gNnSc1	his
life	lif	k1gInSc2	lif
and	and	k?	and
work	work	k1gMnSc1	work
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Murray	Murraa	k1gMnSc2	Murraa
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TIMM	TIMM	kA	TIMM
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
Graphik	Graphik	k1gMnSc1	Graphik
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
:	:	kIx,	:
Henschelverlag	Henschelverlag	k1gMnSc1	Henschelverlag
Kunst	Kunst	k1gMnSc1	Kunst
und	und	k?	und
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EGGUM	EGGUM	kA	EGGUM
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
:	:	kIx,	:
paintings	paintings	k1gInSc1	paintings
<g/>
,	,	kIx,	,
sketches	sketches	k1gInSc1	sketches
<g/>
,	,	kIx,	,
and	and	k?	and
studies	studies	k1gInSc1	studies
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
C.	C.	kA	C.
<g/>
N.	N.	kA	N.
Potter	Potter	k1gMnSc1	Potter
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
517556170	[number]	k4	517556170
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WITTLICH	WITTLICH	kA	WITTLICH
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUNCH	MUNCH	kA	MUNCH
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
<g/>
.	.	kIx.	.
</s>
<s>
Být	být	k5eAaImF	být
sám	sám	k3xTgInSc1	sám
:	:	kIx,	:
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
ohlasy	ohlas	k1gInPc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Arbor	Arbor	k1gInSc1	Arbor
vitae	vita	k1gInSc2	vita
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86300	[number]	k4	86300
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
kompozice	kompozice	k1gFnSc2	kompozice
obrazů	obraz	k1gInPc2	obraz
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Muncha	k1gFnPc2	Muncha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Munch	Munch	k1gMnSc1	Munch
Museum	museum	k1gNnSc4	museum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Gallery	Galler	k1gInPc4	Galler
Munch	Munch	k1gInSc1	Munch
-	-	kIx~	-
Lø	Lø	k1gFnSc1	Lø
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Munch	Munch	k1gInSc1	Munch
na	na	k7c4	na
artcyclopedii	artcyclopedie	k1gFnSc4	artcyclopedie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Catalogue	Catalogue	k1gFnSc1	Catalogue
raisonné	raisonný	k2eAgFnSc2d1	raisonný
of	of	k?	of
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
paintings	paintingsa	k1gFnPc2	paintingsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Odcizená	odcizený	k2eAgNnPc4d1	odcizené
díla	dílo	k1gNnPc4	dílo
E.	E.	kA	E.
Muncha	Muncha	k1gFnSc1	Muncha
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Interpolu	interpol	k1gInSc2	interpol
</s>
</p>
