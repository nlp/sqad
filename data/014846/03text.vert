<s>
Jamolice	Jamolice	k1gFnSc1
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0647	CZ0647	k4
594181	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
647	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
440	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
12,93	12,93	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
350	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
672	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
867201	#num#	k4
Moravský	moravský	k2eAgInSc4d1
Krumlov	Krumlov	k1gInSc4
oujamolice@quick.cz	oujamolice@quick.cz	k1gInSc1
Starostka	starostka	k1gFnSc1
</s>
<s>
Dana	Dana	k1gFnSc1
Jarolímová	Jarolímová	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.obec-jamolice.cz	www.obec-jamolice.cz	k1gInSc1
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
594181	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
56677	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Jamolice	Jamolice	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Jamolitz	Jamolitz	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Znojmo	Znojmo	k1gNnSc4
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
440	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sousedními	sousední	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
sídla	sídlo	k1gNnSc2
jsou	být	k5eAaImIp3nP
Dobřínsko	Dobřínsko	k1gNnSc4
<g/>
,	,	kIx,
Dolní	dolní	k2eAgMnPc4d1
Dubňany	Dubňan	k1gMnPc4
<g/>
,	,	kIx,
Horní	horní	k2eAgMnPc4d1
Dubňany	Dubňan	k1gMnPc4
<g/>
,	,	kIx,
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
Biskoupky	Biskoupek	k1gInPc1
<g/>
,	,	kIx,
Ivančice	Ivančice	k1gFnPc1
<g/>
,	,	kIx,
Dukovany	Dukovany	k1gInPc1
a	a	k8xC
Lhánice	Lhánice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
–	–	k?
hospodářská	hospodářský	k2eAgFnSc1d1
část	část	k1gFnSc1
selského	selský	k2eAgInSc2d1
domu	dům	k1gInSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1281	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
byla	být	k5eAaImAgFnS
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přistavěna	přistavěn	k2eAgFnSc1d1
zvonice	zvonice	k1gFnSc1
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
ke	k	k7c3
kostelu	kostel	k1gInSc3
přistavěna	přistavěn	k2eAgFnSc1d1
sakristie	sakristie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1818	#num#	k4
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
zrekonstruován	zrekonstruovat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
zde	zde	k6eAd1
místní	místní	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
založil	založit	k5eAaPmAgMnS
knihovnu	knihovna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vybavenost	vybavenost	k1gFnSc1
obce	obec	k1gFnSc2
</s>
<s>
V	v	k7c6
Jamolicích	Jamolice	k1gFnPc6
bývala	bývat	k5eAaImAgFnS
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
ročník	ročník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
zde	zde	k6eAd1
funguje	fungovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
místní	místní	k2eAgMnPc1d1
školáci	školák	k1gMnPc1
dojíždějí	dojíždět	k5eAaImIp3nP
do	do	k7c2
Moravského	moravský	k2eAgInSc2d1
Krumlova	Krumlov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
dvě	dva	k4xCgFnPc1
hospody	hospody	k?
a	a	k8xC
jeden	jeden	k4xCgInSc1
obchod	obchod	k1gInSc1
s	s	k7c7
potravinami	potravina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Templštejn	Templštejn	k1gNnSc1
<g/>
,	,	kIx,
zřícenina	zřícenina	k1gFnSc1
hradu	hrad	k1gInSc2
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
vsi	ves	k1gFnSc2
nad	nad	k7c7
břehem	břeh	k1gInSc7
řeky	řeka	k1gFnSc2
Jihlavy	Jihlava	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
-	-	kIx~
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Dobřínsko	Dobřínsko	k1gNnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HAŇÁK	HAŇÁK	kA
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místopis	místopis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mor	mor	k1gInSc1
<g/>
.	.	kIx.
<g/>
-Krumlovský	-Krumlovský	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Musejní	musejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
368	#num#	k4
s.	s.	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jamolice	Jamolice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Jamolice	Jamolice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
Bantice	Bantice	k1gFnSc1
•	•	k?
Běhařovice	Běhařovice	k1gFnSc2
•	•	k?
Bezkov	Bezkov	k1gInSc1
•	•	k?
Bítov	Bítov	k1gInSc4
•	•	k?
Blanné	blanný	k2eAgFnSc2d1
•	•	k?
Blížkovice	Blížkovice	k1gFnSc2
•	•	k?
Bohutice	Bohutice	k1gFnSc2
•	•	k?
Bojanovice	Bojanovice	k1gFnSc2
•	•	k?
Borotice	Borotice	k1gFnSc2
•	•	k?
Boskovštejn	Boskovštejn	k1gMnSc1
•	•	k?
Božice	Božice	k1gFnSc1
•	•	k?
Břežany	Břežany	k1gInPc1
•	•	k?
Citonice	Citonice	k1gFnSc2
•	•	k?
Ctidružice	Ctidružice	k1gFnSc2
•	•	k?
Čejkovice	Čejkovice	k1gFnSc2
•	•	k?
Čermákovice	Čermákovice	k1gFnSc2
•	•	k?
Černín	Černín	k1gMnSc1
•	•	k?
Damnice	Damnice	k1gFnSc2
•	•	k?
Dobelice	Dobelice	k1gFnSc2
•	•	k?
Dobřínsko	Dobřínsko	k1gNnSc1
•	•	k?
Dobšice	Dobšic	k1gMnSc2
•	•	k?
Dolenice	Dolenice	k1gFnSc2
•	•	k?
Dolní	dolní	k2eAgMnPc4d1
Dubňany	Dubňan	k1gMnPc4
•	•	k?
Dyjákovice	Dyjákovice	k1gFnSc1
•	•	k?
Dyjákovičky	Dyjákovička	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dyje	Dyje	k1gFnSc1
•	•	k?
Džbánice	Džbánice	k1gFnSc2
•	•	k?
Grešlové	Grešlový	k2eAgNnSc1d1
Mýto	mýto	k1gNnSc1
•	•	k?
Havraníky	havraník	k1gMnPc4
•	•	k?
Hevlín	Hevlína	k1gFnPc2
•	•	k?
Hluboké	hluboký	k2eAgFnSc2d1
Mašůvky	Mašůvka	k1gFnSc2
•	•	k?
Hnanice	Hnanice	k1gFnSc2
•	•	k?
Hodonice	Hodonice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgInSc1d1
Břečkov	Břečkov	k1gInSc1
•	•	k?
Horní	horní	k2eAgMnPc4d1
Dubňany	Dubňan	k1gMnPc4
•	•	k?
Horní	horní	k2eAgFnSc1d1
Dunajovice	Dunajovice	k1gFnSc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Kounice	Kounice	k1gFnSc1
•	•	k?
Hostěradice	Hostěradice	k1gFnSc2
•	•	k?
Hostim	Hostim	k?
•	•	k?
Hrabětice	Hrabětice	k1gFnSc2
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Hrušovany	Hrušovany	k1gInPc1
nad	nad	k7c7
Jevišovkou	Jevišovka	k1gFnSc7
•	•	k?
Chvalatice	Chvalatice	k1gFnSc2
•	•	k?
Chvalovice	Chvalovice	k1gFnSc2
•	•	k?
Jamolice	Jamolice	k1gFnSc2
•	•	k?
Jaroslavice	Jaroslavice	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Jevišovice	Jevišovice	k1gFnSc1
•	•	k?
Jezeřany-Maršovice	Jezeřany-Maršovice	k1gFnSc2
•	•	k?
Jiřice	Jiřice	k1gFnSc2
u	u	k7c2
Miroslavi	Miroslaev	k1gFnSc6
•	•	k?
Jiřice	Jiřic	k1gMnSc4
u	u	k7c2
Moravských	moravský	k2eAgInPc2d1
Budějovic	Budějovice	k1gInPc2
•	•	k?
Kadov	Kadov	k1gInSc1
•	•	k?
Korolupy	Korolupa	k1gFnSc2
•	•	k?
Kravsko	Kravsko	k1gNnSc1
•	•	k?
Krhovice	Krhovice	k1gFnSc2
•	•	k?
Křepice	Křepice	k1gFnSc2
•	•	k?
Křídlůvky	Křídlůvka	k1gFnSc2
•	•	k?
Kubšice	Kubšice	k1gFnSc2
•	•	k?
Kuchařovice	Kuchařovice	k1gFnSc2
•	•	k?
Kyjovice	Kyjovice	k1gFnSc2
•	•	k?
Lančov	Lančov	k1gInSc1
•	•	k?
Lechovice	Lechovice	k1gFnSc2
•	•	k?
Lesná	lesný	k2eAgFnSc1d1
•	•	k?
Lesonice	Lesonice	k1gFnSc1
•	•	k?
Litobratřice	Litobratřice	k1gFnSc2
•	•	k?
Lubnice	Lubnice	k1gFnSc2
•	•	k?
Lukov	Lukov	k1gInSc1
•	•	k?
Mackovice	Mackovice	k1gFnSc2
•	•	k?
Mašovice	Mašovice	k1gFnSc2
•	•	k?
Medlice	Medlice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mikulovice	Mikulovice	k1gFnSc2
•	•	k?
Milíčovice	Milíčovice	k1gFnSc2
•	•	k?
Miroslav	Miroslav	k1gMnSc1
•	•	k?
Miroslavské	Miroslavský	k2eAgFnSc2d1
Knínice	Knínice	k1gFnSc2
•	•	k?
Morašice	Morašice	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Našiměřice	Našiměřice	k1gFnSc2
•	•	k?
Němčičky	Němčička	k1gFnSc2
•	•	k?
Nový	Nový	k1gMnSc1
Šaldorf-Sedlešovice	Šaldorf-Sedlešovice	k1gFnSc2
•	•	k?
Olbramkostel	Olbramkostel	k1gMnSc1
•	•	k?
Olbramovice	Olbramovice	k1gFnSc2
•	•	k?
Oleksovice	Oleksovice	k1gFnSc2
•	•	k?
Onšov	Onšov	k1gInSc1
•	•	k?
Oslnovice	Oslnovice	k1gFnSc2
•	•	k?
Pavlice	Pavlice	k1gFnSc2
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Plaveč	Plaveč	k1gMnSc1
•	•	k?
Plenkovice	Plenkovice	k1gFnSc1
•	•	k?
Podhradí	Podhradí	k1gNnSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
•	•	k?
Podmolí	Podmolí	k1gFnSc2
•	•	k?
Podmyče	Podmyč	k1gFnSc2
•	•	k?
Práče	práč	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Pravice	pravice	k1gFnSc1
•	•	k?
Prokopov	Prokopov	k1gInSc1
•	•	k?
Prosiměřice	Prosiměřice	k1gFnSc2
•	•	k?
Přeskače	Přeskač	k1gMnSc2
•	•	k?
Rešice	Rešic	k1gMnSc2
•	•	k?
Rozkoš	rozkoš	k1gFnSc4
•	•	k?
Rudlice	Rudlice	k1gFnSc1
•	•	k?
Rybníky	rybník	k1gInPc1
•	•	k?
Skalice	Skalice	k1gFnSc2
•	•	k?
Slatina	slatina	k1gFnSc1
•	•	k?
Slup	slup	k1gFnSc1
•	•	k?
Stálky	stálky	k1gFnPc4
•	•	k?
Starý	starý	k2eAgInSc4d1
Petřín	Petřín	k1gInSc4
•	•	k?
Stošíkovice	Stošíkovice	k1gFnSc2
na	na	k7c6
Louce	louka	k1gFnSc6
•	•	k?
Strachotice	Strachotice	k1gFnSc2
•	•	k?
Střelice	střelice	k1gFnSc2
•	•	k?
Suchohrdly	Suchohrdly	k1gMnSc1
u	u	k7c2
Miroslavi	Miroslaev	k1gFnSc6
•	•	k?
Suchohrdly	Suchohrdly	k1gFnSc2
•	•	k?
Šafov	Šafov	k1gInSc1
•	•	k?
Šanov	Šanov	k1gInSc1
•	•	k?
Šatov	Šatov	k1gInSc1
•	•	k?
Štítary	Štítara	k1gFnSc2
•	•	k?
Šumná	šumný	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Tasovice	Tasovice	k1gFnSc2
•	•	k?
Tavíkovice	Tavíkovice	k1gFnSc2
•	•	k?
Těšetice	Těšetika	k1gFnSc6
•	•	k?
Trnové	trnový	k2eAgFnSc6d1
Pole	pola	k1gFnSc6
•	•	k?
Trstěnice	trstěnice	k1gFnSc2
•	•	k?
Tulešice	Tulešice	k1gFnSc1
•	•	k?
Tvořihráz	Tvořihráza	k1gFnPc2
•	•	k?
Uherčice	Uherčice	k1gFnSc2
•	•	k?
Újezd	Újezd	k1gInSc1
•	•	k?
Únanov	Únanov	k1gInSc1
•	•	k?
Valtrovice	Valtrovice	k1gFnSc2
•	•	k?
Vedrovice	Vedrovice	k1gFnSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
Karlov	Karlov	k1gInSc1
•	•	k?
Vémyslice	Vémyslice	k1gFnSc2
•	•	k?
Vevčice	Vevčice	k1gFnSc2
•	•	k?
Višňové	višňový	k2eAgFnSc2d1
•	•	k?
Vítonice	Vítonice	k1gFnSc2
•	•	k?
Vracovice	Vracovice	k1gFnSc2
•	•	k?
Vranov	Vranov	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
•	•	k?
Vranovská	vranovský	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Vratěnín	Vratěnín	k1gInSc1
•	•	k?
Vrbovec	Vrbovec	k1gInSc1
•	•	k?
Výrovice	Výrovice	k1gFnSc1
•	•	k?
Vysočany	Vysočany	k1gInPc1
•	•	k?
Zálesí	zálesí	k1gNnPc2
•	•	k?
Zblovice	Zblovice	k1gFnSc2
•	•	k?
Znojmo	Znojmo	k1gNnSc1
•	•	k?
Želetice	Želetika	k1gFnSc6
•	•	k?
Žerotice	Žerotice	k1gFnSc2
•	•	k?
Žerůtky	Žerůtka	k1gFnSc2
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
<g/>
.	.	kIx.
</s>
