<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
latviešu	latviesat	k5eAaPmIp1nS	latviesat
valoda	valoda	k1gFnSc1	valoda
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
baltských	baltský	k2eAgInPc2d1	baltský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jím	on	k3xPp3gInSc7	on
mluví	mluvit	k5eAaImIp3nS	mluvit
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgInPc1d1	baltský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lingvisty	lingvista	k1gMnPc4	lingvista
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
mnoho	mnoho	k4c1	mnoho
archaických	archaický	k2eAgInPc2d1	archaický
tvarů	tvar	k1gInPc2	tvar
pocházejících	pocházející	k2eAgInPc2d1	pocházející
z	z	k7c2	z
rané	raný	k2eAgFnSc2d1	raná
indoevropštiny	indoevropština	k1gFnSc2	indoevropština
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c4	o
odštěpení	odštěpení	k1gNnSc4	odštěpení
balto-slovanské	baltolovanský	k2eAgFnSc2d1	balto-slovanský
větve	větev	k1gFnSc2	větev
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Baltská	baltský	k2eAgFnSc1d1	Baltská
a	a	k8xC	a
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
větev	větev	k1gFnSc1	větev
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zatímco	zatímco	k8xS	zatímco
výskyt	výskyt	k1gInSc1	výskyt
archaických	archaický	k2eAgInPc2d1	archaický
tvarů	tvar	k1gInPc2	tvar
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
nezpochybnitelný	zpochybnitelný	k2eNgInSc1d1	nezpochybnitelný
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc1d1	přesný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
baltské	baltský	k2eAgInPc1d1	baltský
jazyky	jazyk	k1gInPc1	jazyk
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
z	z	k7c2	z
indoevropštiny	indoevropština	k1gFnSc2	indoevropština
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východobaltské	Východobaltský	k2eAgInPc1d1	Východobaltský
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgInP	oddělit
od	od	k7c2	od
západobaltských	západobaltský	k2eAgInPc2d1	západobaltský
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
prabaltském	prabaltský	k2eAgInSc6d1	prabaltský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
)	)	kIx)	)
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
400	[number]	k4	400
a	a	k8xC	a
600	[number]	k4	600
n.	n.	k?	n.
l.	l.	k?	l.
Rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
mezi	mezi	k7c7	mezi
lotyštinou	lotyština	k1gFnSc7	lotyština
a	a	k8xC	a
litevštinou	litevština	k1gFnSc7	litevština
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
patrná	patrný	k2eAgFnSc1d1	patrná
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
neexistoval	existovat	k5eNaImAgInS	existovat
jeden	jeden	k4xCgInSc4	jeden
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgNnPc2d1	různé
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
přechodná	přechodný	k2eAgNnPc1d1	přechodné
nářečí	nářečí	k1gNnPc1	nářečí
existovala	existovat	k5eAaImAgNnP	existovat
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
možná	možná	k9	možná
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
se	se	k3xPyFc4	se
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
jazyk	jazyk	k1gInSc1	jazyk
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
latgalština	latgalština	k1gFnSc1	latgalština
a	a	k8xC	a
asimilovala	asimilovat	k5eAaBmAgFnS	asimilovat
s	s	k7c7	s
kuronštinou	kuronština	k1gFnSc7	kuronština
<g/>
,	,	kIx,	,
zemgalštinou	zemgalština	k1gFnSc7	zemgalština
a	a	k8xC	a
sélštinou	sélština	k1gFnSc7	sélština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
známé	známý	k2eAgInPc1d1	známý
nálezy	nález	k1gInPc1	nález
psané	psaný	k2eAgFnSc2d1	psaná
lotyštiny	lotyština	k1gFnSc2	lotyština
jsou	být	k5eAaImIp3nP	být
překlady	překlad	k1gInPc1	překlad
množství	množství	k1gNnSc2	množství
kostelních	kostelní	k2eAgFnPc2d1	kostelní
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
Nicholase	Nicholas	k1gInSc6	Nicholas
Ramma	Rammum	k1gNnSc2	Rammum
<g/>
,	,	kIx,	,
německého	německý	k2eAgMnSc2d1	německý
pastora	pastor	k1gMnSc2	pastor
z	z	k7c2	z
Rigy	Riga	k1gFnSc2	Riga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
posledních	poslední	k2eAgInPc2d1	poslední
živých	živý	k2eAgInPc2d1	živý
baltských	baltský	k2eAgInPc2d1	baltský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
litevština	litevština	k1gFnSc1	litevština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřících	patřící	k2eAgFnPc2d1	patřící
k	k	k7c3	k
indoevropské	indoevropský	k2eAgFnSc3d1	indoevropská
jazykové	jazykový	k2eAgFnSc3d1	jazyková
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšský	lotyšský	k2eAgInSc1d1	lotyšský
a	a	k8xC	a
litevský	litevský	k2eAgInSc1d1	litevský
jazyk	jazyk	k1gInSc1	jazyk
si	se	k3xPyFc3	se
udržely	udržet	k5eAaPmAgInP	udržet
mnoho	mnoho	k4c1	mnoho
rysů	rys	k1gInPc2	rys
prajazyka	prajazyk	k1gInSc2	prajazyk
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
hláskosloví	hláskosloví	k1gNnSc2	hláskosloví
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
mnoho	mnoho	k4c4	mnoho
zdokonalení	zdokonalení	k1gNnPc2	zdokonalení
(	(	kIx(	(
<g/>
u	u	k7c2	u
lotyštiny	lotyština	k1gFnSc2	lotyština
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
litevštiny	litevština	k1gFnSc2	litevština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baltským	baltský	k2eAgMnPc3d1	baltský
jazykům	jazyk	k1gMnPc3	jazyk
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
jazyky	jazyk	k1gInPc1	jazyk
slovanské	slovanský	k2eAgInPc1d1	slovanský
a	a	k8xC	a
germánské	germánský	k2eAgInPc1d1	germánský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nářečí	nářečí	k1gNnSc2	nářečí
==	==	k?	==
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
nářečí	nářečí	k1gNnPc4	nářečí
<g/>
:	:	kIx,	:
livonské	livonský	k2eAgNnSc4d1	livonské
<g/>
,	,	kIx,	,
latgalské	latgalský	k2eAgNnSc4d1	latgalský
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
nářečí	nářečí	k1gNnSc4	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Livonské	Livonský	k2eAgNnSc1d1	Livonský
nářečí	nářečí	k1gNnSc1	nářečí
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
vidzemské	vidzemský	k2eAgMnPc4d1	vidzemský
a	a	k8xC	a
kuronské	kuronský	k2eAgMnPc4d1	kuronský
(	(	kIx(	(
<g/>
také	také	k9	také
tā	tā	k?	tā
nebo	nebo	k8xC	nebo
ventiņ	ventiņ	k?	ventiņ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešní	dnešní	k2eAgFnSc2d1	dnešní
lotyštiny	lotyština	k1gFnSc2	lotyština
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
vidzemské	vidzemský	k2eAgNnSc4d1	vidzemský
<g/>
,	,	kIx,	,
kuronské	kuronský	k2eAgNnSc4d1	kuronský
a	a	k8xC	a
zemgalské	zemgalský	k2eAgNnSc4d1	zemgalské
nářečí	nářečí	k1gNnSc4	nářečí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Lotyšská	lotyšský	k2eAgNnPc1d1	lotyšské
nářečí	nářečí	k1gNnPc1	nářečí
by	by	kYmCp3nP	by
neměla	mít	k5eNaImAgNnP	mít
být	být	k5eAaImF	být
zaměňována	zaměňován	k2eAgNnPc1d1	zaměňováno
s	s	k7c7	s
jazyky	jazyk	k1gMnPc7	jazyk
livonštinou	livonština	k1gFnSc7	livonština
<g/>
,	,	kIx,	,
kuronštinou	kuronština	k1gFnSc7	kuronština
<g/>
,	,	kIx,	,
zemgalštinou	zemgalština	k1gFnSc7	zemgalština
ani	ani	k8xC	ani
sélštinou	sélština	k1gFnSc7	sélština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Livonské	Livonský	k2eAgNnSc1d1	Livonský
nářečí	nářečí	k1gNnSc1	nářečí
===	===	k?	===
</s>
</p>
<p>
<s>
Livonské	Livonský	k2eAgNnSc1d1	Livonský
nářečí	nářečí	k1gNnSc1	nářečí
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
livonštinou	livonština	k1gFnSc7	livonština
než	než	k8xS	než
lotyštinou	lotyština	k1gFnSc7	lotyština
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
livonském	livonský	k2eAgNnSc6d1	livonské
nářečí	nářečí	k1gNnSc6	nářečí
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
melodie	melodie	k1gFnPc4	melodie
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nářečí	nářečí	k1gNnSc6	nářečí
kuronském	kuronský	k2eAgMnSc6d1	kuronský
krátké	krátký	k2eAgFnPc4d1	krátká
samohlásky	samohláska	k1gFnPc4	samohláska
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
slov	slovo	k1gNnPc2	slovo
zanikají	zanikat	k5eAaImIp3nP	zanikat
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
jsou	být	k5eAaImIp3nP	být
zkráceny	zkrácen	k2eAgFnPc1d1	zkrácena
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnPc1d1	osobní
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
rodech	rod	k1gInPc6	rod
jsou	být	k5eAaImIp3nP	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
na	na	k7c4	na
-els	ls	k1gInSc4	-els
a	a	k8xC	a
-ans	ns	k1gInSc4	-ans
<g/>
.	.	kIx.	.
</s>
<s>
Předpona	předpona	k1gFnSc1	předpona
ie-	ie-	k?	ie-
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
e-	e-	k?	e-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgNnSc1d1	střední
nářečí	nářečí	k1gNnSc1	nářečí
===	===	k?	===
</s>
</p>
<p>
<s>
Vidzemské	Vidzemský	k2eAgNnSc4d1	Vidzemský
a	a	k8xC	a
zemgalské	zemgalský	k2eAgNnSc4d1	zemgalské
nářečí	nářečí	k1gNnSc4	nářečí
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
kuronské	kuronský	k2eAgNnSc1d1	kuronský
nářečí	nářečí	k1gNnSc1	nářečí
je	být	k5eAaImIp3nS	být
archaičtější	archaický	k2eAgNnSc1d2	archaičtější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
nářečí	nářečí	k1gNnSc6	nářečí
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc4	tři
melodie	melodie	k1gFnPc4	melodie
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemgalském	zemgalský	k2eAgNnSc6d1	zemgalské
nářečí	nářečí	k1gNnSc6	nářečí
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
použit	použit	k2eAgInSc1d1	použit
znak	znak	k1gInSc1	znak
ŗ	ŗ	k?	ŗ
</s>
</p>
<p>
<s>
==	==	k?	==
Mluvnice	mluvnice	k1gFnSc2	mluvnice
==	==	k?	==
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
je	být	k5eAaImIp3nS	být
flektivní	flektivní	k2eAgInSc4d1	flektivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgInSc1d1	slovní
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jména	jméno	k1gNnSc2	jméno
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lotyštině	lotyština	k1gFnSc6	lotyština
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
jmenné	jmenný	k2eAgInPc1d1	jmenný
rody	rod	k1gInPc1	rod
–	–	k?	–
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
šesti	šest	k4xCc7	šest
pády	pád	k1gInPc7	pád
<g/>
:	:	kIx,	:
nominativem	nominativ	k1gInSc7	nominativ
<g/>
,	,	kIx,	,
genitivem	genitiv	k1gInSc7	genitiv
<g/>
,	,	kIx,	,
dativem	dativ	k1gInSc7	dativ
<g/>
,	,	kIx,	,
akuzativem	akuzativ	k1gInSc7	akuzativ
<g/>
,	,	kIx,	,
lokálem	lokál	k1gInSc7	lokál
a	a	k8xC	a
vokativem	vokativ	k1gInSc7	vokativ
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
gramatiky	gramatika	k1gFnSc2	gramatika
za	za	k7c4	za
akuzativ	akuzativ	k1gInSc4	akuzativ
vkládaly	vkládat	k5eAaImAgFnP	vkládat
instrumentál	instrumentál	k1gInSc4	instrumentál
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
považovaly	považovat	k5eAaImAgFnP	považovat
použití	použití	k1gNnSc4	použití
předložky	předložka	k1gFnSc2	předložka
ar	ar	k1gInSc4	ar
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
českému	český	k2eAgNnSc3d1	české
s	s	k7c7	s
<g/>
,	,	kIx,	,
s	s	k7c7	s
příslušným	příslušný	k2eAgInSc7d1	příslušný
pádem	pád	k1gInSc7	pád
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gramatikách	gramatika	k1gFnPc6	gramatika
se	se	k3xPyFc4	se
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
slovní	slovní	k2eAgNnSc1d1	slovní
označení	označení	k1gNnSc1	označení
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
se	se	k3xPyFc4	se
předchází	předcházet	k5eAaImIp3nS	předcházet
záměnám	záměna	k1gFnPc3	záměna
obou	dva	k4xCgNnPc2	dva
uvedených	uvedený	k2eAgNnPc2d1	uvedené
pojetí	pojetí	k1gNnPc2	pojetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
podle	podle	k7c2	podle
šesti	šest	k4xCc2	šest
vzorů	vzor	k1gInPc2	vzor
(	(	kIx(	(
<g/>
deklinací	deklinace	k1gFnPc2	deklinace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosti	dosti	k6eAd1	dosti
pravidelně	pravidelně	k6eAd1	pravidelně
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
mužské	mužský	k2eAgInPc4d1	mužský
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
převážně	převážně	k6eAd1	převážně
ženské	ženský	k2eAgInPc4d1	ženský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
výrazné	výrazný	k2eAgInPc1d1	výrazný
rysy	rys	k1gInPc1	rys
užívání	užívání	k1gNnSc1	užívání
jmen	jméno	k1gNnPc2	jméno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yQnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
užívání	užívání	k1gNnSc3	užívání
genitivu	genitiv	k1gInSc2	genitiv
záporového	záporový	k2eAgInSc2d1	záporový
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
prostý	prostý	k2eAgInSc1d1	prostý
genitiv	genitiv	k1gInSc1	genitiv
i	i	k8xC	i
dativ	dativ	k1gInSc1	dativ
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
neshodných	shodný	k2eNgInPc6d1	neshodný
přívlastcích	přívlastek	k1gInPc6	přívlastek
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
neexistující	existující	k2eNgNnSc4d1	neexistující
druhy	druh	k1gInPc1	druh
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
vztah	vztah	k1gInSc4	vztah
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
v	v	k7c6	v
kom	kdo	k3yQnSc6	kdo
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
bezpředložkovým	bezpředložkový	k2eAgInSc7d1	bezpředložkový
lokálem	lokál	k1gInSc7	lokál
(	(	kIx(	(
<g/>
předložku	předložka	k1gFnSc4	předložka
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
českému	český	k2eAgInSc3d1	český
v	v	k7c6	v
lotyština	lotyština	k1gFnSc1	lotyština
vůbec	vůbec	k9	vůbec
nemá	mít	k5eNaImIp3nS	mít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bezpředložkový	bezpředložkový	k2eAgInSc1d1	bezpředložkový
dativ	dativ	k1gInSc1	dativ
se	se	k3xPyFc4	se
při	při	k7c6	při
neexistenci	neexistence	k1gFnSc6	neexistence
slovesa	sloveso	k1gNnSc2	sloveso
mít	mít	k5eAaImF	mít
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
vazbách	vazba	k1gFnPc6	vazba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
někomu	někdo	k3yInSc3	někdo
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
=	=	kIx~	=
někdo	někdo	k3yInSc1	někdo
něco	něco	k3yInSc4	něco
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
i	i	k9	i
v	v	k7c6	v
přísudku	přísudek	k1gInSc6	přísudek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInSc1d1	základní
tvar	tvar	k1gInSc1	tvar
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
ve	v	k7c6	v
shodném	shodný	k2eAgInSc6d1	shodný
přívlastku	přívlastek	k1gInSc6	přívlastek
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
neurčitost	neurčitost	k1gFnSc1	neurčitost
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
neurčitý	určitý	k2eNgInSc1d1	neurčitý
člen	člen	k1gInSc1	člen
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
používají	používat	k5eAaImIp3nP	používat
členy	člen	k1gInPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Delším	dlouhý	k2eAgInSc7d2	delší
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
determinativním	determinativní	k2eAgFnPc3d1	determinativní
(	(	kIx(	(
<g/>
příponou	přípona	k1gFnSc7	přípona
vloženou	vložený	k2eAgFnSc7d1	vložená
před	před	k7c4	před
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
splývá	splývat	k5eAaImIp3nS	splývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
určitost	určitost	k1gFnSc1	určitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
nezná	znát	k5eNaImIp3nS	znát
přivlastňovací	přivlastňovací	k2eAgFnSc1d1	přivlastňovací
a	a	k8xC	a
mnohá	mnohý	k2eAgNnPc1d1	mnohé
jiná	jiný	k2eAgNnPc1d1	jiné
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
odvozená	odvozený	k2eAgNnPc1d1	odvozené
od	od	k7c2	od
substantiv	substantivum	k1gNnPc2	substantivum
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
hojně	hojně	k6eAd1	hojně
užívá	užívat	k5eAaImIp3nS	užívat
substantiva	substantivum	k1gNnPc1	substantivum
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
v	v	k7c6	v
neshodném	shodný	k2eNgInSc6d1	neshodný
přívlastku	přívlastek	k1gInSc6	přívlastek
(	(	kIx(	(
<g/>
genitiv	genitiv	k1gInSc1	genitiv
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kā	Kā	k?	Kā
tilts	tilts	k1gInSc1	tilts
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
Karla	Karel	k1gMnSc2	Karel
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
mū	mū	k?	mū
mā	mā	k?	mā
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
nás	my	k3xPp1nPc4	my
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bē	bē	k?	bē
apģ	apģ	k?	apģ
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
dětí	dítě	k1gFnPc2	dítě
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgInPc4d1	dětský
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
sporta	sporta	k1gFnSc1	sporta
apģ	apģ	k?	apģ
bē	bē	k?	bē
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
sportu	sport	k1gInSc6	sport
oděvy	oděv	k1gInPc7	oděv
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgInPc4d1	dětský
sportovní	sportovní	k2eAgInPc4d1	sportovní
oděvy	oděv	k1gInPc4	oděv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
tříd	třída	k1gFnPc2	třída
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
časování	časování	k1gNnSc2	časování
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
běžného	běžný	k2eAgInSc2d1	běžný
minulého	minulý	k2eAgInSc2d1	minulý
i	i	k8xC	i
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
časování	časování	k1gNnSc6	časování
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
ve	v	k7c6	v
kmeni	kmen	k1gInSc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
je	být	k5eAaImIp3nS	být
shodný	shodný	k2eAgInSc1d1	shodný
tvar	tvar	k1gInSc1	tvar
jednotného	jednotný	k2eAgNnSc2d1	jednotné
i	i	k8xC	i
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
koncovku	koncovka	k1gFnSc4	koncovka
-	-	kIx~	-
<g/>
t.	t.	k?	t.
</s>
</p>
<p>
<s>
===	===	k?	===
Předložky	předložka	k1gFnSc2	předložka
===	===	k?	===
</s>
</p>
<p>
<s>
Předložky	předložka	k1gFnPc1	předložka
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
pojí	pojit	k5eAaImIp3nS	pojit
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
pády	pád	k1gInPc7	pád
<g/>
:	:	kIx,	:
genitivem	genitiv	k1gInSc7	genitiv
<g/>
,	,	kIx,	,
akuzativem	akuzativ	k1gInSc7	akuzativ
nebo	nebo	k8xC	nebo
dativem	dativ	k1gInSc7	dativ
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgFnPc4	všechen
předložky	předložka	k1gFnPc4	předložka
s	s	k7c7	s
dativem	dativ	k1gInSc7	dativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
instrumentál	instrumentál	k1gInSc1	instrumentál
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
použití	použití	k1gNnSc4	použití
ar	ar	k1gInSc1	ar
s	s	k7c7	s
akuzativem	akuzativ	k1gInSc7	akuzativ
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
s	s	k7c7	s
dativem	dativ	k1gInSc7	dativ
v	v	k7c6	v
množném	množný	k2eAgInSc6d1	množný
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
použití	použití	k1gNnSc1	použití
té	ten	k3xDgFnSc2	ten
předložky	předložka	k1gFnSc2	předložka
i	i	k9	i
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
významu	význam	k1gInSc6	význam
než	než	k8xS	než
instrumentálním	instrumentální	k2eAgInSc6d1	instrumentální
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
část	část	k1gFnSc1	část
gramatik	gramatika	k1gFnPc2	gramatika
již	již	k6eAd1	již
instrumentál	instrumentál	k1gInSc1	instrumentál
jako	jako	k8xC	jako
pád	pád	k1gInSc1	pád
nevymezuje	vymezovat	k5eNaImIp3nS	vymezovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hláskosloví	hláskosloví	k1gNnSc2	hláskosloví
==	==	k?	==
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pravopisný	pravopisný	k2eAgInSc1d1	pravopisný
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
německých	německý	k2eAgInPc6d1	německý
pravopisných	pravopisný	k2eAgInPc6d1	pravopisný
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
fonetičtějším	fonetický	k2eAgInSc7d2	fonetický
<g/>
,	,	kIx,	,
používajícím	používající	k2eAgInSc7d1	používající
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
diakritiku	diakritika	k1gFnSc4	diakritika
<g/>
,	,	kIx,	,
upravenou	upravený	k2eAgFnSc4d1	upravená
podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
abeceda	abeceda	k1gFnSc1	abeceda
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
33	[number]	k4	33
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Do	do	k7c2	do
reformy	reforma	k1gFnSc2	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
také	také	k9	také
písmeno	písmeno	k1gNnSc1	písmeno
Ŗ	Ŗ	k?	Ŗ
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgFnSc6d1	měkká
R	R	kA	R
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ve	v	k7c6	v
většinové	většinový	k2eAgFnSc6d1	většinová
výslovnosti	výslovnost	k1gFnSc6	výslovnost
neodlišovalo	odlišovat	k5eNaImAgNnS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Emigrantská	emigrantský	k2eAgFnSc1d1	emigrantská
komunita	komunita	k1gFnSc1	komunita
tuto	tento	k3xDgFnSc4	tento
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
reformu	reforma	k1gFnSc4	reforma
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
vydávané	vydávaný	k2eAgInPc1d1	vydávaný
texty	text	k1gInPc1	text
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
toto	tento	k3xDgNnSc4	tento
písmeno	písmeno	k1gNnSc4	písmeno
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc1	všechen
písmena	písmeno	k1gNnPc1	písmeno
označují	označovat	k5eAaImIp3nP	označovat
podobné	podobný	k2eAgInPc1d1	podobný
hlásky	hlásek	k1gInPc1	hlásek
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
či	či	k8xC	či
slovenštině	slovenština	k1gFnSc6	slovenština
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
čtením	čtení	k1gNnSc7	čtení
by	by	kYmCp3nS	by
mluvčí	mluvčí	k1gMnSc1	mluvčí
češtiny	čeština	k1gFnSc2	čeština
neměli	mít	k5eNaImAgMnP	mít
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
s	s	k7c7	s
makronem	makron	k1gInSc7	makron
(	(	kIx(	(
<g/>
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
označují	označovat	k5eAaImIp3nP	označovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odlišnosti	odlišnost	k1gFnPc1	odlišnost
u	u	k7c2	u
souhlásek	souhláska	k1gFnPc2	souhláska
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
jen	jen	k9	jen
grafické	grafický	k2eAgInPc4d1	grafický
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
s	s	k7c7	s
cedillou	cedilla	k1gFnSc7	cedilla
(	(	kIx(	(
<g/>
Ļ	Ļ	k?	Ļ
<g/>
,	,	kIx,	,
Ģ	Ģ	k?	Ģ
<g/>
,	,	kIx,	,
Ķ	Ķ	k?	Ķ
<g/>
,	,	kIx,	,
Ņ	Ņ	k?	Ņ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k8xS	jako
slovenské	slovenský	k2eAgInPc1d1	slovenský
ľ	ľ	k?	ľ
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
ť	ť	k?	ť
<g/>
,	,	kIx,	,
ň.	ň.	k?	ň.
</s>
</p>
<p>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
h	h	k?	h
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
české	český	k2eAgNnSc1d1	české
ch	ch	k0	ch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
v	v	k7c4	v
<g/>
,	,	kIx,	,
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
<g/>
-li	i	k?	-li
slabiku	slabika	k1gFnSc4	slabika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
slovenštině	slovenština	k1gFnSc6	slovenština
označuje	označovat	k5eAaImIp3nS	označovat
hlásku	hláska	k1gFnSc4	hláska
u	u	k7c2	u
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgFnSc3d1	uvedená
modlitbě	modlitba	k1gFnSc3	modlitba
"	"	kIx"	"
<g/>
Tavs	Tavs	k1gInSc1	Tavs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Tvůj	tvůj	k1gMnSc1	tvůj
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
taus	taus	k1gInSc1	taus
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podstatnější	podstatný	k2eAgInPc1d2	podstatnější
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Psané	psaný	k2eAgNnSc4d1	psané
o	o	k7c4	o
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
uo	uo	k?	uo
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
přejatá	přejatý	k2eAgNnPc4d1	přejaté
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
e	e	k0	e
označují	označovat	k5eAaImIp3nP	označovat
tzv.	tzv.	kA	tzv.
široké	široký	k2eAgInPc4d1	široký
lotyšské	lotyšský	k2eAgInPc4d1	lotyšský
e	e	k0	e
<g/>
,	,	kIx,	,
blížící	blížící	k2eAgFnSc6d1	blížící
se	se	k3xPyFc4	se
přehlásce	přehláska	k1gFnSc6	přehláska
ä	ä	k?	ä
<g/>
;	;	kIx,	;
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
neutrálnímu	neutrální	k2eAgInSc3d1	neutrální
e	e	k0	e
se	se	k3xPyFc4	se
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
a	a	k8xC	a
formulovat	formulovat	k5eAaImF	formulovat
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc1	problém
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
lotyštině	lotyština	k1gFnSc6	lotyština
==	==	k?	==
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc4	dva
široce	široko	k6eAd1	široko
uznávané	uznávaný	k2eAgInPc4d1	uznávaný
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
lotyštině	lotyština	k1gFnSc6	lotyština
jsou	být	k5eAaImIp3nP	být
lingvistický	lingvistický	k2eAgInSc4d1	lingvistický
purismus	purismus	k1gInSc4	purismus
a	a	k8xC	a
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
gimalajiešu	gimalajiesat	k5eAaPmIp1nS	gimalajiesat
lā	lā	k?	lā
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gimalajiešu	Gimalajiešu	k1gFnPc3	Gimalajiešu
lā	lā	k?	lā
===	===	k?	===
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
gimalajiešu	gimalajiesat	k5eAaPmIp1nS	gimalajiesat
lā	lā	k?	lā
je	být	k5eAaImIp3nS	být
nesprávné	správný	k2eNgNnSc1d1	nesprávné
pojmenovaní	pojmenovaný	k2eAgMnPc1d1	pojmenovaný
asijského	asijský	k2eAgMnSc4d1	asijský
černého	černý	k2eAgMnSc4d1	černý
medvěda	medvěd	k1gMnSc4	medvěd
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
není	být	k5eNaImIp3nS	být
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
termínem	termín	k1gInSc7	termín
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
těchto	tento	k3xDgInPc2	tento
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
docela	docela	k6eAd1	docela
zábavné	zábavný	k2eAgInPc4d1	zábavný
<g/>
)	)	kIx)	)
jazykové	jazykový	k2eAgInPc4d1	jazykový
omyly	omyl	k1gInPc4	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
omyly	omyl	k1gInPc1	omyl
jsou	být	k5eAaImIp3nP	být
gramatické	gramatický	k2eAgInPc1d1	gramatický
a	a	k8xC	a
stylistické	stylistický	k2eAgInPc1d1	stylistický
<g/>
;	;	kIx,	;
zřejmě	zřejmě	k6eAd1	zřejmě
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
překlepy	překlep	k1gInPc4	překlep
a	a	k8xC	a
chybné	chybný	k2eAgInPc4d1	chybný
překlady	překlad	k1gInPc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
probíhá	probíhat	k5eAaImIp3nS	probíhat
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
chce	chtít	k5eAaImIp3nS	chtít
předejít	předejít	k5eAaPmF	předejít
<g/>
.	.	kIx.	.
</s>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvíce	hodně	k6eAd3	hodně
těchto	tento	k3xDgMnPc2	tento
omylů	omyl	k1gInPc2	omyl
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
hustě	hustě	k6eAd1	hustě
zalidněných	zalidněný	k2eAgFnPc2d1	zalidněná
Rusy	Rus	k1gMnPc4	Rus
a	a	k8xC	a
od	od	k7c2	od
litevských	litevský	k2eAgInPc2d1	litevský
obchodních	obchodní	k2eAgInPc2d1	obchodní
řetězců	řetězec	k1gInPc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Chybné	chybný	k2eAgInPc1d1	chybný
překlady	překlad	k1gInPc1	překlad
nejsou	být	k5eNaImIp3nP	být
gramaticky	gramaticky	k6eAd1	gramaticky
špatné	špatný	k2eAgInPc1d1	špatný
<g/>
,	,	kIx,	,
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
pouze	pouze	k6eAd1	pouze
vadí	vadit	k5eAaImIp3nS	vadit
styl	styl	k1gInSc1	styl
a	a	k8xC	a
volba	volba	k1gFnSc1	volba
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
doslovně	doslovně	k6eAd1	doslovně
přeložená	přeložený	k2eAgNnPc4d1	přeložené
anglická	anglický	k2eAgNnPc4d1	anglické
slova	slovo	k1gNnPc4	slovo
do	do	k7c2	do
lotyštiny	lotyština	k1gFnSc2	lotyština
by	by	kYmCp3nS	by
vypadala	vypadat	k5eAaPmAgFnS	vypadat
podivně	podivně	k6eAd1	podivně
exaltovaná	exaltovaný	k2eAgFnSc1d1	exaltovaná
oproti	oproti	k7c3	oproti
lotyšským	lotyšský	k2eAgMnPc3d1	lotyšský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lingvistický	lingvistický	k2eAgInSc1d1	lingvistický
purismus	purismus	k1gInSc1	purismus
===	===	k?	===
</s>
</p>
<p>
<s>
Purismus	purismus	k1gInSc1	purismus
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
tvořením	tvoření	k1gNnSc7	tvoření
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
nebo	nebo	k8xC	nebo
přičinění	přičinění	k1gNnSc2	přičinění
široké	široký	k2eAgFnSc2d1	široká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
puristé	purista	k1gMnPc1	purista
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
eufonická	eufonický	k2eAgNnPc1d1	eufonický
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc7	jejich
neologismy	neologismus	k1gInPc7	neologismus
zní	znět	k5eAaImIp3nS	znět
spíše	spíše	k9	spíše
jako	jako	k9	jako
cizí	cizí	k2eAgNnPc4d1	cizí
než	než	k8xS	než
lotyšská	lotyšský	k2eAgNnPc4d1	lotyšské
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
výrazů	výraz	k1gInPc2	výraz
je	být	k5eAaImIp3nS	být
zbytečných	zbytečný	k2eAgFnPc2d1	zbytečná
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
doslovné	doslovný	k2eAgInPc1d1	doslovný
překlady	překlad	k1gInPc1	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
dva	dva	k4xCgInPc4	dva
výrazy	výraz	k1gInPc4	výraz
pro	pro	k7c4	pro
telefon	telefon	k1gInSc4	telefon
<g/>
:	:	kIx,	:
tā	tā	k?	tā
(	(	kIx(	(
<g/>
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
telefons	telefons	k6eAd1	telefons
<g/>
)	)	kIx)	)
a	a	k8xC	a
telefons	telefons	k6eAd1	telefons
<g/>
,	,	kIx,	,
tři	tři	k4xCgNnPc4	tři
slova	slovo	k1gNnPc4	slovo
pro	pro	k7c4	pro
počítač	počítač	k1gInSc4	počítač
<g/>
:	:	kIx,	:
dators	dators	k6eAd1	dators
nebo	nebo	k8xC	nebo
kompjū	kompjū	k?	kompjū
(	(	kIx(	(
<g/>
obě	dva	k4xCgNnPc4	dva
jsou	být	k5eAaImIp3nP	být
převzatá	převzatý	k2eAgNnPc4d1	převzaté
<g/>
)	)	kIx)	)
a	a	k8xC	a
skaitļ	skaitļ	k?	skaitļ
(	(	kIx(	(
<g/>
lotyšský	lotyšský	k2eAgInSc1d1	lotyšský
výraz	výraz	k1gInSc1	výraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
mělo	mít	k5eAaImAgNnS	mít
bouřlivý	bouřlivý	k2eAgInSc4d1	bouřlivý
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
svou	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
bylo	být	k5eAaImAgNnS	být
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc7d1	mnohonárodnostní
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
rusifikace	rusifikace	k1gFnSc1	rusifikace
velmi	velmi	k6eAd1	velmi
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
lotyšský	lotyšský	k2eAgInSc4d1	lotyšský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
obdobích	období	k1gNnPc6	období
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
340	[number]	k4	340
000	[number]	k4	000
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
Lotyšů	Lotyš	k1gMnPc2	Lotyš
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
vyhoštěno	vyhoštěn	k2eAgNnSc1d1	vyhoštěno
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
perzekvováno	perzekvován	k2eAgNnSc1d1	perzekvován
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
masová	masový	k2eAgFnSc1d1	masová
imigrace	imigrace	k1gFnSc1	imigrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
ze	z	k7c2	z
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
etnicky	etnicky	k6eAd1	etnicky
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
populace	populace	k1gFnSc2	populace
byl	být	k5eAaImAgInS	být
redukován	redukovat	k5eAaBmNgInS	redukovat
z	z	k7c2	z
80	[number]	k4	80
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
na	na	k7c4	na
52	[number]	k4	52
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
se	se	k3xPyFc4	se
usadilo	usadit	k5eAaPmAgNnS	usadit
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
snahy	snaha	k1gFnSc2	snaha
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
lotyšsky	lotyšsky	k6eAd1	lotyšsky
a	a	k8xC	a
sžít	sžít	k5eAaPmF	sžít
se	se	k3xPyFc4	se
se	s	k7c7	s
zdejšími	zdejší	k2eAgFnPc7d1	zdejší
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
lotyština	lotyština	k1gFnSc1	lotyština
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
řečí	řeč	k1gFnSc7	řeč
jen	jen	k9	jen
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
opětovném	opětovný	k2eAgNnSc6d1	opětovné
získání	získání	k1gNnSc6	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
jazyková	jazykový	k2eAgFnSc1d1	jazyková
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
začlenění	začlenění	k1gNnSc1	začlenění
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
oficiálního	oficiální	k2eAgInSc2d1	oficiální
státního	státní	k2eAgInSc2d1	státní
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
jazyky	jazyk	k1gInPc4	jazyk
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
(	(	kIx(	(
<g/>
přitom	přitom	k6eAd1	přitom
ruština	ruština	k1gFnSc1	ruština
nebyla	být	k5eNaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k9	jako
menšinová	menšinový	k2eAgFnSc1d1	menšinová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
učenci	učenec	k1gMnPc1	učenec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
věci	věc	k1gFnPc1	věc
mohou	moct	k5eAaImIp3nP	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
úpadku	úpadek	k1gInSc3	úpadek
lotyštiny	lotyština	k1gFnSc2	lotyština
<g/>
.	.	kIx.	.
<g/>
Opravdové	opravdový	k2eAgNnSc4d1	opravdové
dvojjazyčné	dvojjazyčný	k2eAgNnSc4d1	dvojjazyčné
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c4	na
vládní	vládní	k2eAgInPc4d1	vládní
náklady	náklad	k1gInPc4	náklad
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Litevci	Litevec	k1gMnPc1	Litevec
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
Estonci	Estonec	k1gMnPc1	Estonec
a	a	k8xC	a
Romové	Rom	k1gMnPc1	Rom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
lotyština	lotyština	k1gFnSc1	lotyština
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
jako	jako	k8xS	jako
druhý	druhý	k4xOgInSc4	druhý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odstranila	odstranit	k5eAaPmAgFnS	odstranit
překážky	překážka	k1gFnPc4	překážka
v	v	k7c6	v
dorozumívání	dorozumívání	k1gNnSc6	dorozumívání
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
každému	každý	k3xTgMnSc3	každý
obyvateli	obyvatel	k1gMnSc3	obyvatel
začlenit	začlenit	k5eAaPmF	začlenit
se	se	k3xPyFc4	se
do	do	k7c2	do
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
vláda	vláda	k1gFnSc1	vláda
platí	platit	k5eAaImIp3nS	platit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
univerzitách	univerzita	k1gFnPc6	univerzita
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
zvládnutí	zvládnutí	k1gNnSc2	zvládnutí
lotyštiny	lotyština	k1gFnPc1	lotyština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lotyštině	lotyština	k1gFnSc6	lotyština
vyučováno	vyučovat	k5eAaImNgNnS	vyučovat
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
forma	forma	k1gFnSc1	forma
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
60	[number]	k4	60
%	%	kIx~	%
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
zde	zde	k6eAd1	zde
existoval	existovat	k5eAaImAgInS	existovat
široký	široký	k2eAgInSc1d1	široký
systém	systém	k1gInSc1	systém
vyučování	vyučování	k1gNnSc2	vyučování
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SVARINSKA	SVARINSKA	kA	SVARINSKA
<g/>
,	,	kIx,	,
Asja	Asjum	k1gNnSc2	Asjum
<g/>
.	.	kIx.	.
</s>
<s>
Latviešu	Latviesat	k5eAaPmIp1nS	Latviesat
valoda	valod	k1gMnSc4	valod
<g/>
.	.	kIx.	.
</s>
<s>
Mā	Mā	k?	Mā
kurss	kurss	k1gInSc1	kurss
25	[number]	k4	25
nodarbī	nodarbī	k?	nodarbī
<g/>
.	.	kIx.	.
/	/	kIx~	/
Latvian	Latvian	k1gInSc1	Latvian
in	in	k?	in
25	[number]	k4	25
lessons	lessonsa	k1gFnPc2	lessonsa
<g/>
.	.	kIx.	.
</s>
<s>
Rī	Rī	k?	Rī
<g/>
:	:	kIx,	:
Zvaigzne	Zvaigzne	k1gFnSc2	Zvaigzne
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
112	[number]	k4	112
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9984	[number]	k4	9984
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
841	[number]	k4	841
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jā	Jā	k?	Jā
Endzelī	Endzelī	k1gFnSc1	Endzelī
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lotyština	lotyština	k1gFnSc1	lotyština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lotyšsko-český	lotyšsko-český	k2eAgInSc1d1	lotyšsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
