<s>
Obec	obec	k1gFnSc1	obec
Telnice	Telnice	k1gFnSc2	Telnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Telnitz	Telnitz	k1gInSc1	Telnitz
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Telnice	Telnice	k1gFnSc2	Telnice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
380	[number]	k4	380
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
1500	[number]	k4	1500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
urbanisticky	urbanisticky	k6eAd1	urbanisticky
zcela	zcela	k6eAd1	zcela
oddělených	oddělený	k2eAgFnPc2d1	oddělená
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jednou	jednou	k6eAd1	jednou
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Nádražní	nádražní	k2eAgFnSc1d1	nádražní
Čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
zástavby	zástavba	k1gFnSc2	zástavba
sousední	sousední	k2eAgFnSc2d1	sousední
obce	obec	k1gFnSc2	obec
Sokolnice	sokolnice	k1gFnSc2	sokolnice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Velkopavlovické	Velkopavlovický	k2eAgFnSc6d1	Velkopavlovická
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnSc1d1	viniční
trať	trať	k1gFnSc1	trať
Šternovsko	Šternovsko	k1gNnSc4	Šternovsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
získala	získat	k5eAaPmAgFnS	získat
obec	obec	k1gFnSc1	obec
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Vesnice	vesnice	k1gFnSc2	vesnice
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Telnici	Telnice	k1gFnSc6	Telnice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1726	[number]	k4	1726
<g/>
-	-	kIx~	-
<g/>
1734	[number]	k4	1734
Na	na	k7c6	na
telnické	telnický	k2eAgFnSc6d1	telnický
faře	fara	k1gFnSc6	fara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
prastarý	prastarý	k2eAgInSc4d1	prastarý
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
spal	spát	k5eAaImAgMnS	spát
i	i	k9	i
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
fary	fara	k1gFnSc2	fara
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
období	období	k1gNnSc2	období
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
významně	významně	k6eAd1	významně
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
hrabě	hrabě	k1gMnSc1	hrabě
Wladimír	Wladimír	k1gMnSc1	Wladimír
Mitrovský	Mitrovský	k2eAgMnSc1d1	Mitrovský
z	z	k7c2	z
Mitrovic	Mitrovice	k1gFnPc2	Mitrovice
a	a	k8xC	a
Nemyšle	Nemyšle	k1gFnSc2	Nemyšle
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
vícero	vícero	k1gNnSc4	vícero
statků	statek	k1gInPc2	statek
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Sokolnicích	sokolnice	k1gFnPc6	sokolnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
faře	fara	k1gFnSc6	fara
čp.	čp.	k?	čp.
<g/>
5	[number]	k4	5
jsou	být	k5eAaImIp3nP	být
uchovány	uchován	k2eAgFnPc1d1	uchována
archiválie	archiválie	k1gFnPc1	archiválie
<g/>
,	,	kIx,	,
nalezená	nalezený	k2eAgFnSc1d1	nalezená
munice	munice	k1gFnSc1	munice
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
a	a	k8xC	a
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
jsou	být	k5eAaImIp3nP	být
dochována	dochován	k2eAgNnPc1d1	dochováno
klasicistní	klasicistní	k2eAgNnPc1d1	klasicistní
bílá	bílý	k2eAgNnPc1d1	bílé
kachlová	kachlový	k2eAgNnPc1d1	kachlové
kamna	kamna	k1gNnPc1	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
kamna	kamna	k1gNnPc1	kamna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bílá	bílý	k2eAgNnPc4d1	bílé
věžová	věžový	k2eAgNnPc4d1	věžové
kamna	kamna	k1gNnPc4	kamna
z	z	k7c2	z
pokojů	pokoj	k1gInPc2	pokoj
a	a	k8xC	a
hnědá	hnědat	k5eAaImIp3nS	hnědat
válcová	válcový	k2eAgFnSc1d1	válcová
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgNnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
faře	fara	k1gFnSc6	fara
jsou	být	k5eAaImIp3nP	být
stoleté	stoletý	k2eAgFnPc1d1	stoletá
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
masivní	masivní	k2eAgFnPc1d1	masivní
podlahy	podlaha	k1gFnPc1	podlaha
<g/>
,	,	kIx,	,
na	na	k7c6	na
chodbě	chodba	k1gFnSc6	chodba
je	být	k5eAaImIp3nS	být
dlažba	dlažba	k1gFnSc1	dlažba
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
×	×	k?	×
<g/>
20	[number]	k4	20
<g/>
cm	cm	kA	cm
<g/>
)	)	kIx)	)
z	z	k7c2	z
19	[number]	k4	19
stol.	stol.	k?	stol.
Stropy	strop	k1gInPc1	strop
jsou	být	k5eAaImIp3nP	být
trámové	trámový	k2eAgInPc1d1	trámový
záklopové	záklopový	k2eAgInPc1d1	záklopový
později	pozdě	k6eAd2	pozdě
podbité	podbitý	k2eAgInPc1d1	podbitý
rákosem	rákos	k1gInSc7	rákos
s	s	k7c7	s
omítkou	omítka	k1gFnSc7	omítka
a	a	k8xC	a
silnými	silný	k2eAgInPc7d1	silný
fabiony	fabion	k1gInPc7	fabion
s	s	k7c7	s
římsou	římsa	k1gFnSc7	římsa
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sálem	sál	k1gInSc7	sál
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sklep	sklep	k1gInSc1	sklep
s	s	k7c7	s
cihlovou	cihlový	k2eAgFnSc7d1	cihlová
valenou	valený	k2eAgFnSc7d1	valená
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
půdě	půda	k1gFnSc6	půda
s	s	k7c7	s
okénky	okénko	k1gNnPc7	okénko
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zazděnými	zazděný	k2eAgMnPc7d1	zazděný
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
litinový	litinový	k2eAgInSc1d1	litinový
erb	erb	k1gInSc4	erb
hrabat	hrabě	k1gNnPc2	hrabě
Mitrovských	Mitrovský	k2eAgNnPc2d1	Mitrovský
snesený	snesený	k2eAgMnSc1d1	snesený
z	z	k7c2	z
fasády	fasáda	k1gFnSc2	fasáda
po	po	k7c6	po
přípojce	přípojka	k1gFnSc6	přípojka
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyvýšeném	vyvýšený	k2eAgNnSc6d1	vyvýšené
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
pohostinství	pohostinství	k1gNnSc1	pohostinství
u	u	k7c2	u
Laštůvků	Laštůvek	k1gInPc2	Laštůvek
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
rychtu	rychta	k1gFnSc4	rychta
či	či	k8xC	či
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgInPc4d1	stejný
parametry	parametr	k1gInPc4	parametr
jako	jako	k8xC	jako
tvrz	tvrz	k1gFnSc1	tvrz
v	v	k7c6	v
Nemili	Nemili	k1gFnSc6	Nemili
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
masivního	masivní	k2eAgNnSc2d1	masivní
smíšeného	smíšený	k2eAgNnSc2d1	smíšené
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Zachován	zachován	k2eAgMnSc1d1	zachován
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
valeně	valeně	k6eAd1	valeně
klenutý	klenutý	k2eAgInSc1d1	klenutý
sklep	sklep	k1gInSc1	sklep
s	s	k7c7	s
výsečí	výseč	k1gFnSc7	výseč
a	a	k8xC	a
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
klenbou	klenba	k1gFnSc7	klenba
s	s	k7c7	s
výsečí	výseč	k1gFnSc7	výseč
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
polovina	polovina	k1gFnSc1	polovina
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zázemí	zázemí	k1gNnSc1	zázemí
hospody	hospody	k?	hospody
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
jako	jako	k8xS	jako
obytný	obytný	k2eAgInSc4d1	obytný
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
barokní	barokní	k2eAgFnSc1d1	barokní
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
daroval	darovat	k5eAaPmAgMnS	darovat
obci	obec	k1gFnSc3	obec
majitel	majitel	k1gMnSc1	majitel
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
sokolnického	sokolnický	k2eAgNnSc2d1	sokolnické
panství	panství	k1gNnSc2	panství
hrabě	hrabě	k1gMnSc1	hrabě
Ditrichštejn	Ditrichštejn	k1gMnSc1	Ditrichštejn
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Několikráte	Několikráte	k6eAd1	Několikráte
změnila	změnit	k5eAaPmAgFnS	změnit
místo	místo	k7c2	místo
umístění	umístění	k1gNnSc2	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stávala	stávat	k5eAaImAgFnS	stávat
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sokolovny	sokolovna	k1gFnSc2	sokolovna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
farou	fara	k1gFnSc7	fara
a	a	k8xC	a
Orlovnou	orlovna	k1gFnSc7	orlovna
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Písky	Písek	k1gInPc1	Písek
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šustr	Šustr	k1gMnSc1	Šustr
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
starosta	starosta	k1gMnSc1	starosta
František	František	k1gMnSc1	František
Vykouřil	vykouřit	k5eAaPmAgMnS	vykouřit
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
reprezentant	reprezentant	k1gMnSc1	reprezentant
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Telnice	Telnice	k1gFnSc2	Telnice
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Telnice	Telnice	k1gFnSc2	Telnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
