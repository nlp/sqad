<s>
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1966	[number]	k4	1966
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
oštěpař	oštěpař	k1gMnSc1	oštěpař
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
oštěpaře	oštěpař	k1gMnSc4	oštěpař
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
tří	tři	k4xCgFnPc2	tři
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
rekordman	rekordman	k1gMnSc1	rekordman
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
současného	současný	k2eAgInSc2d1	současný
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
oštěpem	oštěp	k1gInSc7	oštěp
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
98,48	[number]	k4	98,48
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
nových	nový	k2eAgNnPc2d1	nové
pravidel	pravidlo	k1gNnPc2	pravidlo
bylo	být	k5eAaImAgNnS	být
oštěpem	oštěp	k1gInSc7	oštěp
84	[number]	k4	84
<g/>
krát	krát	k6eAd1	krát
hozeno	hodit	k5eAaPmNgNnS	hodit
přes	přes	k7c4	přes
90	[number]	k4	90
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
84	[number]	k4	84
hodů	hod	k1gInPc2	hod
patří	patřit	k5eAaImIp3nS	patřit
52	[number]	k4	52
Janu	Jan	k1gMnSc3	Jan
Železnému	železný	k2eAgMnSc3d1	železný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Atletem	atlet	k1gMnSc7	atlet
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Atletem	atlet	k1gMnSc7	atlet
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
českým	český	k2eAgMnSc7d1	český
Sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
třikrát	třikrát	k6eAd1	třikrát
přehodil	přehodit	k5eAaPmAgInS	přehodit
95	[number]	k4	95
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
od	od	k7c2	od
změny	změna	k1gFnSc2	změna
těžiště	těžiště	k1gNnSc1	těžiště
oštěpu	oštěp	k1gInSc2	oštěp
nikomu	nikdo	k3yNnSc3	nikdo
jinému	jiný	k2eAgMnSc3d1	jiný
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nominoval	nominovat	k5eAaBmAgMnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
vystupování	vystupování	k1gNnSc4	vystupování
tam	tam	k6eAd1	tam
završil	završit	k5eAaPmAgInS	završit
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
medailí	medaile	k1gFnSc7	medaile
za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
85,92	[number]	k4	85,92
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
je	být	k5eAaImIp3nS	být
také	také	k9	také
světovým	světový	k2eAgInSc7d1	světový
rekordem	rekord	k1gInSc7	rekord
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
veteránů	veterán	k1gMnPc2	veterán
nad	nad	k7c4	nad
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
Göteborg	Göteborg	k1gInSc1	Göteborg
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2004	[number]	k4	2004
Atény	Atény	k1gFnPc1	Atény
-	-	kIx~	-
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2003	[number]	k4	2003
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2002	[number]	k4	2002
Mnichov	Mnichov	k1gInSc1	Mnichov
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
2001	[number]	k4	2001
Edmonton	Edmonton	k1gInSc1	Edmonton
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
rekordu	rekord	k1gInSc6	rekord
šampionátu	šampionát	k1gInSc2	šampionát
92,80	[number]	k4	92,80
m	m	kA	m
2000	[number]	k4	2000
Sydney	Sydney	k1gNnSc2	Sydney
-	-	kIx~	-
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c4	v
olym	olym	k1gInSc4	olym
<g/>
.	.	kIx.	.
rekordu	rekord	k1gInSc2	rekord
90,17	[number]	k4	90,17
m	m	kA	m
1999	[number]	k4	1999
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
oštěpaři	oštěpař	k1gMnPc7	oštěpař
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
24	[number]	k4	24
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Sevilla	Sevilla	k1gFnSc1	Sevilla
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
87,67	[number]	k4	87,67
m	m	kA	m
1997	[number]	k4	1997
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
oštěpaři	oštěpař	k1gMnPc7	oštěpař
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
Atény	Atény	k1gFnPc1	Atény
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
1996	[number]	k4	1996
Atlanta	Atlanta	k1gFnSc1	Atlanta
-	-	kIx~	-
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
88,16	[number]	k4	88,16
m	m	kA	m
1996	[number]	k4	1996
Jena	Jen	k1gInSc2	Jen
-	-	kIx~	-
Mítink	mítink	k1gInSc1	mítink
EAA	EAA	kA	EAA
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
98,48	[number]	k4	98,48
m	m	kA	m
1995	[number]	k4	1995
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
oštěpu	oštěp	k1gInSc6	oštěp
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Göteborg	Göteborg	k1gInSc1	Göteborg
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
89,58	[number]	k4	89,58
m	m	kA	m
1994	[number]	k4	1994
Helsinki	Helsink	k1gFnPc1	Helsink
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
<g />
.	.	kIx.	.
</s>
<s>
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
82,58	[number]	k4	82,58
m	m	kA	m
1993	[number]	k4	1993
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
oštěpu	oštěp	k1gInSc6	oštěp
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
Sheffield	Sheffielda	k1gFnPc2	Sheffielda
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
95,66	[number]	k4	95,66
m	m	kA	m
1993	[number]	k4	1993
Pietersburg	Pietersburg	k1gInSc4	Pietersburg
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
95,54	[number]	k4	95,54
m	m	kA	m
1993	[number]	k4	1993
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g />
.	.	kIx.	.
</s>
<s>
85,98	[number]	k4	85,98
m	m	kA	m
1992	[number]	k4	1992
Barcelona	Barcelona	k1gFnSc1	Barcelona
-	-	kIx~	-
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
89,66	[number]	k4	89,66
m	m	kA	m
1992	[number]	k4	1992
Oslo	Oslo	k1gNnPc2	Oslo
-	-	kIx~	-
Mítink	mítink	k1gInSc1	mítink
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
ligy	liga	k1gFnSc2	liga
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
94,74	[number]	k4	94,74
m	m	kA	m
1991	[number]	k4	1991
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
oštěpu	oštěp	k1gInSc6	oštěp
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
Tokio	Tokio	k1gNnSc1	Tokio
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
1990	[number]	k4	1990
Split	Split	k1gInSc1	Split
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
1990	[number]	k4	1990
Oslo	Oslo	k1gNnSc2	Oslo
-	-	kIx~	-
Mítink	mítink	k1gInSc1	mítink
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
ligy	liga	k1gFnSc2	liga
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
89,66	[number]	k4	89,66
m	m	kA	m
1988	[number]	k4	1988
Soul	Soul	k1gInSc1	Soul
-	-	kIx~	-
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
1987	[number]	k4	1987
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
1987	[number]	k4	1987
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g />
.	.	kIx.	.
</s>
<s>
1987	[number]	k4	1987
Nitra	Nitra	k1gFnSc1	Nitra
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČSSR	ČSSR	kA	ČSSR
-	-	kIx~	-
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
87,66	[number]	k4	87,66
m	m	kA	m
1986	[number]	k4	1986
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
1983	[number]	k4	1983
Schwechat	Schwechat	k1gFnSc1	Schwechat
-	-	kIx~	-
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
juniorů	junior	k1gMnPc2	junior
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Výsledkové	výsledkový	k2eAgFnPc1d1	výsledková
statistiky	statistika	k1gFnPc1	statistika
byly	být	k5eAaImAgFnP	být
zpracovány	zpracovat	k5eAaPmNgFnP	zpracovat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
ověřenu	ověřen	k2eAgFnSc4d1	ověřena
aktuální	aktuální	k2eAgFnSc4d1	aktuální
platnost	platnost	k1gFnSc4	platnost
k	k	k7c3	k
12	[number]	k4	12
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
světový	světový	k2eAgMnSc1d1	světový
rekordman	rekordman	k1gMnSc1	rekordman
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
úspěchy	úspěch	k1gInPc4	úspěch
několikrát	několikrát	k6eAd1	několikrát
oceněn	ocenit	k5eAaPmNgInS	ocenit
2001	[number]	k4	2001
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
posedmé	posedmé	k4xO	posedmé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
počtvrté	počtvrté	k4xO	počtvrté
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
státní	státní	k2eAgFnSc4d1	státní
medaili	medaile	k1gFnSc4	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
"	"	kIx"	"
2000	[number]	k4	2000
vítěz	vítěz	k1gMnSc1	vítěz
<g />
.	.	kIx.	.
</s>
<s>
ankety	anketa	k1gFnPc1	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
světa	svět	k1gInSc2	svět
1997	[number]	k4	1997
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
"	"	kIx"	"
<g/>
Atlet	atlet	k1gMnSc1	atlet
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
1996	[number]	k4	1996
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
1995	[number]	k4	1995
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
ocenění	ocenění	k1gNnSc2	ocenění
ČOV	ČOV	kA	ČOV
-	-	kIx~	-
ceny	cena	k1gFnSc2	cena
Stanislava	Stanislav	k1gMnSc2	Stanislav
Guta	Gutus	k1gMnSc2	Gutus
Jarkovského	Jarkovský	k2eAgMnSc2d1	Jarkovský
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Atlet	atlet	k1gMnSc1	atlet
světa	svět	k1gInSc2	svět
1994	[number]	k4	1994
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Atlet	atlet	k1gMnSc1	atlet
světa	svět	k1gInSc2	svět
1991	[number]	k4	1991
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
Českého	český	k2eAgInSc2d1	český
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
světové	světový	k2eAgMnPc4d1	světový
sportovce	sportovec	k1gMnPc4	sportovec
i	i	k9	i
v	v	k7c6	v
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
olympijském	olympijský	k2eAgInSc6d1	olympijský
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Hod	hod	k1gInSc4	hod
oštěpem	oštěp	k1gInSc7	oštěp
98,48	[number]	k4	98,48
m	m	kA	m
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
ER	ER	kA	ER
<g/>
,	,	kIx,	,
NR	NR	kA	NR
<g/>
;	;	kIx,	;
Jena	Jena	k1gMnSc1	Jena
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
trenér	trenér	k1gMnSc1	trenér
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
svěřence	svěřenec	k1gMnPc4	svěřenec
patřila	patřit	k5eAaImAgFnS	patřit
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
Barbora	Barbora	k1gFnSc1	Barbora
Špotáková	Špotáková	k1gFnSc1	Špotáková
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
trénuje	trénovat	k5eAaImIp3nS	trénovat
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Veselého	Veselý	k1gMnSc2	Veselý
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
úspěšné	úspěšný	k2eAgMnPc4d1	úspěšný
oštěpaře	oštěpař	k1gMnPc4	oštěpař
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
Česko	Česko	k1gNnSc1	Česko
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Železný	železný	k2eAgMnSc1d1	železný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
www.olympic.cz	www.olympic.cz	k1gInSc4	www.olympic.cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
atletickytrenink	atletickytrenink	k1gInSc4	atletickytrenink
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Článek	článek	k1gInSc1	článek
o	o	k7c4	o
SR	SR	kA	SR
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
oštěpem	oštěp	k1gInSc7	oštěp
na	na	k7c4	na
blogu	bloga	k1gFnSc4	bloga
DinosaurusBlog	DinosaurusBloga	k1gFnPc2	DinosaurusBloga
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
