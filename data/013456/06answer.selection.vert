<s>
Lazaretní	lazaretní	k2eAgNnSc1d1	lazaretní
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přeměnilo	přeměnit	k5eAaPmAgNnS	přeměnit
na	na	k7c4	na
samostatný	samostatný	k2eAgInSc4d1	samostatný
rytířský	rytířský	k2eAgInSc4d1	rytířský
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
začal	začít	k5eAaPmAgInS	začít
sdružovat	sdružovat	k5eAaImF	sdružovat
německé	německý	k2eAgMnPc4d1	německý
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosud	dosud	k6eAd1	dosud
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
frankofonních	frankofonní	k2eAgMnPc2d1	frankofonní
johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
