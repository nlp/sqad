<p>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Bratři	bratr	k1gMnPc1	bratr
německého	německý	k2eAgInSc2d1	německý
řádu	řád	k1gInSc2	řád
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
či	či	k8xC	či
Řád	řád	k1gInSc1	řád
bratří	bratr	k1gMnPc2	bratr
německého	německý	k2eAgInSc2d1	německý
domu	dům	k1gInSc2	dům
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Ordo	orda	k1gFnSc5	orda
domus	domus	k1gInSc4	domus
Sanctæ	Sanctæ	k1gFnSc1	Sanctæ
Mariæ	Mariæ	k1gFnSc1	Mariæ
Theutonicorum	Theutonicorum	k1gInSc4	Theutonicorum
Hierosolymitanorum	Hierosolymitanorum	k1gInSc1	Hierosolymitanorum
či	či	k8xC	či
Ordo	orda	k1gFnSc5	orda
fratrum	fratrum	k1gNnSc1	fratrum
domus	domus	k1gInSc4	domus
hospitalis	hospitalis	k1gFnSc2	hospitalis
Sanctæ	Sanctæ	k1gFnSc2	Sanctæ
Mariæ	Mariæ	k1gMnPc2	Mariæ
Theutonicorum	Theutonicorum	k1gInSc1	Theutonicorum
Hierosolymitanorum	Hierosolymitanorum	k1gNnSc4	Hierosolymitanorum
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Orden	Orden	k1gInSc1	Orden
der	drát	k5eAaImRp2nS	drát
Brüder	Brüder	k1gInSc1	Brüder
vom	vom	k?	vom
Deutschen	Deutschen	k2eAgInSc1d1	Deutschen
Haus	Haus	k1gInSc1	Haus
der	drát	k5eAaImRp2nS	drát
Heiligen	Heiligen	k1gInSc4	Heiligen
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
duchovních	duchovní	k1gMnPc2	duchovní
rytířských	rytířský	k2eAgInPc2d1	rytířský
řádů	řád	k1gInPc2	řád
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Jeruzalémském	jeruzalémský	k2eAgNnSc6d1	Jeruzalémské
království	království	k1gNnSc6	království
na	na	k7c6	na
území	území	k1gNnSc6	území
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
a	a	k8xC	a
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
rytířských	rytířský	k2eAgMnPc2d1	rytířský
řádů	řád	k1gInPc2	řád
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
územní	územní	k2eAgNnSc4d1	územní
rozšíření	rozšíření	k1gNnSc4	rozšíření
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
jen	jen	k9	jen
s	s	k7c7	s
Řádem	řád	k1gInSc7	řád
maltézských	maltézský	k2eAgMnPc2d1	maltézský
rytířů	rytíř	k1gMnPc2	rytíř
nebo	nebo	k8xC	nebo
se	s	k7c7	s
zaniklým	zaniklý	k2eAgInSc7d1	zaniklý
Řádem	řád	k1gInSc7	řád
templářů	templář	k1gMnPc2	templář
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
křížových	křížový	k2eAgNnPc6d1	křížové
taženích	tažení	k1gNnPc6	tažení
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc4	svůj
význačnou	význačný	k2eAgFnSc4d1	význačná
roli	role	k1gFnSc4	role
sehrál	sehrát	k5eAaPmAgInS	sehrát
teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
na	na	k7c6	na
území	území	k1gNnSc6	území
zaniklého	zaniklý	k2eAgNnSc2d1	zaniklé
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmistr	velmistr	k1gMnSc1	velmistr
řádu	řád	k1gInSc2	řád
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
generální	generální	k2eAgFnSc1d1	generální
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
archiv	archiv	k1gInSc1	archiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
sídlí	sídlet	k5eAaImIp3nS	sídlet
tzv.	tzv.	kA	tzv.
generální	generální	k2eAgMnSc1d1	generální
prokurátor	prokurátor	k1gMnSc1	prokurátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
řád	řád	k1gInSc4	řád
před	před	k7c7	před
Svatým	svatý	k2eAgInSc7d1	svatý
stolcem	stolec	k1gInSc7	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
má	mít	k5eAaImIp3nS	mít
mužskou	mužský	k2eAgFnSc4d1	mužská
i	i	k8xC	i
ženskou	ženský	k2eAgFnSc4d1	ženská
větev	větev	k1gFnSc4	větev
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úřední	úřední	k2eAgInSc1d1	úřední
název	název	k1gInSc1	název
současného	současný	k2eAgInSc2d1	současný
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Řád	řád	k1gInSc1	řád
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
sester	sestra	k1gFnPc2	sestra
německého	německý	k2eAgInSc2d1	německý
Domu	dům	k1gInSc2	dům
panny	panna	k1gFnSc2	panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
(	(	kIx(	(
<g/>
němčině	němčina	k1gFnSc6	němčina
se	se	k3xPyFc4	se
řád	řád	k1gInSc1	řád
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Haus	Haus	k1gInSc1	Haus
der	drát	k5eAaImRp2nS	drát
Ritter	Ritter	k1gInSc1	Ritter
des	des	k1gNnSc6	des
Hospitals	Hospitals	k1gInSc1	Hospitals
Sankt	Sankt	k1gInSc1	Sankt
Marien	Marien	k2eAgInSc1d1	Marien
der	drát	k5eAaImRp2nS	drát
Deutschen	Deutschen	k1gInSc1	Deutschen
zu	zu	k?	zu
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
zkrácených	zkrácený	k2eAgInPc2d1	zkrácený
názvů	název	k1gInPc2	název
<g/>
:	:	kIx,	:
Deutscher	Deutschra	k1gFnPc2	Deutschra
Orden	Ordna	k1gFnPc2	Ordna
<g/>
,	,	kIx,	,
Deutschherrenorden	Deutschherrenordna	k1gFnPc2	Deutschherrenordna
<g/>
,	,	kIx,	,
Deutscher	Deutschra	k1gFnPc2	Deutschra
Ritterorden	Ritterordna	k1gFnPc2	Ritterordna
či	či	k8xC	či
Deutschritterorden	Deutschritterordna	k1gFnPc2	Deutschritterordna
<g/>
.	.	kIx.	.
latinský	latinský	k2eAgInSc1d1	latinský
krátký	krátký	k2eAgInSc1d1	krátký
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
Ordo	orda	k1gFnSc5	orda
Theutonicus	Theutonicus	k1gInSc4	Theutonicus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
polská	polský	k2eAgFnSc1d1	polská
pojmenování	pojmenování	k1gNnSc4	pojmenování
Zakon	Zakon	k1gInSc1	Zakon
krzyżacki	krzyżack	k1gFnPc1	krzyżack
či	či	k8xC	či
Zakon	Zakon	k1gNnSc1	Zakon
niemiecki	niemieck	k1gFnSc2	niemieck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc4	dějiny
rytířského	rytířský	k2eAgInSc2d1	rytířský
řádu	řád	k1gInSc2	řád
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
řádu	řád	k1gInSc2	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
špitálním	špitální	k2eAgInSc7d1	špitální
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
historie	historie	k1gFnSc1	historie
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1190	[number]	k4	1190
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Akkonu	Akkon	k1gInSc2	Akkon
za	za	k7c2	za
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
založili	založit	k5eAaPmAgMnP	založit
brémští	brémský	k2eAgMnPc1d1	brémský
a	a	k8xC	a
lübečtí	lübecký	k2eAgMnPc1d1	lübecký
kupci	kupec	k1gMnPc1	kupec
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
<g/>
,	,	kIx,	,
polní	polní	k2eAgMnSc1d1	polní
lazaret	lazaret	k1gInSc4	lazaret
pro	pro	k7c4	pro
poutníky	poutník	k1gMnPc4	poutník
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1199	[number]	k4	1199
pověřil	pověřit	k5eAaPmAgMnS	pověřit
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
o	o	k7c6	o
nemocnici	nemocnice	k1gFnSc6	nemocnice
starali	starat	k5eAaImAgMnP	starat
<g/>
,	,	kIx,	,
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
pohanům	pohan	k1gMnPc3	pohan
<g/>
.	.	kIx.	.
</s>
<s>
Lazaretní	lazaretní	k2eAgNnSc1d1	lazaretní
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přeměnilo	přeměnit	k5eAaPmAgNnS	přeměnit
na	na	k7c4	na
samostatný	samostatný	k2eAgInSc4d1	samostatný
rytířský	rytířský	k2eAgInSc4d1	rytířský
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
začal	začít	k5eAaPmAgInS	začít
sdružovat	sdružovat	k5eAaImF	sdružovat
německé	německý	k2eAgMnPc4d1	německý
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dosud	dosud	k6eAd1	dosud
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
frankofonních	frankofonní	k2eAgMnPc2d1	frankofonní
johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
sídlem	sídlo	k1gNnSc7	sídlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Akko	Akko	k6eAd1	Akko
(	(	kIx(	(
<g/>
Acre	Acre	k1gFnSc1	Acre
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Související	související	k2eAgInPc4d1	související
články	článek	k1gInPc4	článek
<g/>
:	:	kIx,	:
Křížové	Křížové	k2eAgFnPc4d1	Křížové
výpravy	výprava	k1gFnPc4	výprava
<g/>
,	,	kIx,	,
Křižácké	křižácký	k2eAgInPc4d1	křižácký
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
,	,	kIx,	,
Třetí	třetí	k4xOgFnSc1	třetí
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
Pátá	pátý	k4xOgFnSc1	pátý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
Šestá	šestý	k4xOgFnSc1	šestý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
Devátá	devátý	k4xOgFnSc1	devátý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
křižácké	křižácký	k2eAgNnSc1d1	křižácké
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
omezeno	omezit	k5eAaPmNgNnS	omezit
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
sil	síla	k1gFnPc2	síla
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
vychýlen	vychýlit	k5eAaPmNgInS	vychýlit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
starších	starý	k2eAgInPc2d2	starší
rytířských	rytířský	k2eAgInPc2d1	rytířský
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
konkurenčních	konkurenční	k2eAgMnPc2d1	konkurenční
templářů	templář	k1gMnPc2	templář
a	a	k8xC	a
johanitů	johanita	k1gMnPc2	johanita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
řád	řád	k1gInSc1	řád
snažil	snažit	k5eAaImAgInS	snažit
najít	najít	k5eAaPmF	najít
nové	nový	k2eAgNnSc4d1	nové
působiště	působiště	k1gNnSc4	působiště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1211	[number]	k4	1211
je	být	k5eAaImIp3nS	být
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ondřej	Ondřej	k1gMnSc1	Ondřej
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Uherský	uherský	k2eAgInSc1d1	uherský
pozval	pozvat	k5eAaPmAgInS	pozvat
do	do	k7c2	do
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
rytířů	rytíř	k1gMnPc2	rytíř
tu	ten	k3xDgFnSc4	ten
bylo	být	k5eAaImAgNnS	být
bránit	bránit	k5eAaImF	bránit
východní	východní	k2eAgFnPc4d1	východní
hranice	hranice	k1gFnPc4	hranice
uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
před	před	k7c4	před
vpády	vpád	k1gInPc4	vpád
kočovníků	kočovník	k1gMnPc2	kočovník
a	a	k8xC	a
šířit	šířit	k5eAaImF	šířit
v	v	k7c6	v
zakarpatských	zakarpatský	k2eAgFnPc6d1	Zakarpatská
oblastech	oblast	k1gFnPc6	oblast
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
uherskou	uherský	k2eAgFnSc4d1	uherská
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
před	před	k7c7	před
pohanskými	pohanský	k2eAgInPc7d1	pohanský
Kumány	Kumán	k1gInPc7	Kumán
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zajistit	zajistit	k5eAaPmF	zajistit
si	se	k3xPyFc3	se
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
usiloval	usilovat	k5eAaImAgMnS	usilovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
o	o	k7c6	o
vynětí	vynětí	k1gNnSc6	vynětí
svěřeného	svěřený	k2eAgInSc2d1	svěřený
mu	on	k3xPp3gMnSc3	on
území	území	k1gNnSc1	území
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
II	II	kA	II
<g/>
.	.	kIx.	.
ho	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
roku	rok	k1gInSc2	rok
1225	[number]	k4	1225
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1225	[number]	k4	1225
<g/>
/	/	kIx~	/
<g/>
1226	[number]	k4	1226
pozval	pozvat	k5eAaPmAgMnS	pozvat
kníže	kníže	k1gMnSc1	kníže
Konrád	Konrád	k1gMnSc1	Konrád
I.	I.	kA	I.
Mazovský	Mazovský	k1gMnSc1	Mazovský
řádové	řádový	k2eAgFnSc2d1	řádová
rytíře	rytíř	k1gMnSc4	rytíř
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jim	on	k3xPp3gMnPc3	on
odměnou	odměna	k1gFnSc7	odměna
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
prusko-mazovského	pruskoazovský	k2eAgNnSc2d1	prusko-mazovský
pohraničí	pohraničí	k1gNnSc2	pohraničí
državu	država	k1gFnSc4	država
chełmiňské	chełmiňský	k2eAgFnSc2d1	chełmiňský
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
knížecí	knížecí	k2eAgFnSc2d1	knížecí
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
předchozím	předchozí	k2eAgFnPc3d1	předchozí
zkušenostem	zkušenost	k1gFnPc3	zkušenost
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
rytíři	rytíř	k1gMnPc7	rytíř
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
neprozřetelný	prozřetelný	k2eNgInSc4d1	neprozřetelný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Konrád	Konrád	k1gMnSc1	Konrád
byl	být	k5eAaImAgMnS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
získat	získat	k5eAaPmF	získat
pomoc	pomoc	k1gFnSc4	pomoc
řádu	řád	k1gInSc2	řád
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
nájezdy	nájezd	k1gInPc7	nájezd
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Prusů	Prus	k1gMnPc2	Prus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosud	dosud	k6eAd1	dosud
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
nájezdech	nájezd	k1gInPc6	nájezd
nebrali	brát	k5eNaImAgMnP	brát
ohledy	ohled	k1gInPc4	ohled
ani	ani	k8xC	ani
na	na	k7c4	na
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
snažil	snažit	k5eAaImAgMnS	snažit
problém	problém	k1gInSc4	problém
vyřešit	vyřešit	k5eAaPmF	vyřešit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporoval	podporovat	k5eAaImAgMnS	podporovat
formování	formování	k1gNnSc4	formování
nového	nový	k2eAgInSc2d1	nový
rytířského	rytířský	k2eAgInSc2d1	rytířský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
nebyla	být	k5eNaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
početnější	početní	k2eAgMnSc1d2	početnější
a	a	k8xC	a
zkušenější	zkušený	k2eAgMnSc1d2	zkušenější
z	z	k7c2	z
válek	válka	k1gFnPc2	válka
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
úkol	úkol	k1gInSc4	úkol
vhodnější	vhodný	k2eAgInSc4d2	vhodnější
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Konrád	Konrád	k1gMnSc1	Konrád
řádu	řád	k1gInSc2	řád
držbu	držba	k1gFnSc4	držba
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dobyta	dobýt	k5eAaPmNgFnS	dobýt
na	na	k7c6	na
Prusích	Prusích	k?	Prusích
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
bule	bula	k1gFnSc6	bula
z	z	k7c2	z
Rimini	Rimin	k1gMnPc1	Rimin
(	(	kIx(	(
<g/>
1226	[number]	k4	1226
<g/>
)	)	kIx)	)
udělil	udělit	k5eAaPmAgMnS	udělit
německým	německý	k2eAgMnSc7d1	německý
rytířům	rytíř	k1gMnPc3	rytíř
právo	právo	k1gNnSc1	právo
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
uzurpovat	uzurpovat	k5eAaBmF	uzurpovat
východní	východní	k2eAgFnSc2d1	východní
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
udělil	udělit	k5eAaPmAgInS	udělit
velmistrům	velmistr	k1gMnPc3	velmistr
řádu	řád	k1gInSc3	řád
používat	používat	k5eAaImF	používat
i	i	k9	i
zlatě	zlatě	k6eAd1	zlatě
zdobený	zdobený	k2eAgInSc1d1	zdobený
kříž	kříž	k1gInSc1	kříž
s	s	k7c7	s
erbem	erb	k1gInSc7	erb
říšského	říšský	k2eAgMnSc2d1	říšský
orla	orel	k1gMnSc2	orel
uprostřed	uprostřed	k7c2	uprostřed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
řád	řád	k1gInSc4	řád
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1234	[number]	k4	1234
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zfalšovaného	zfalšovaný	k2eAgInSc2d1	zfalšovaný
rukopisu	rukopis	k1gInSc2	rukopis
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
krušvický	krušvický	k2eAgInSc1d1	krušvický
falsifikát	falsifikát	k1gInSc1	falsifikát
<g/>
)	)	kIx)	)
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
přiznal	přiznat	k5eAaPmAgMnS	přiznat
řádu	řád	k1gInSc3	řád
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
na	na	k7c6	na
územích	území	k1gNnPc6	území
Konráda	Konrád	k1gMnSc2	Konrád
Mazovského	Mazovského	k2eAgNnSc4d1	Mazovského
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
a	a	k8xC	a
bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
obývaná	obývaný	k2eAgFnSc1d1	obývaná
roztroušenými	roztroušený	k2eAgFnPc7d1	roztroušená
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
nejednotnými	jednotný	k2eNgInPc7d1	nejednotný
baltskými	baltský	k2eAgInPc7d1	baltský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
přínosné	přínosný	k2eAgFnSc6d1	přínosná
blízkosti	blízkost	k1gFnSc6	blízkost
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
obchodních	obchodní	k2eAgFnPc2d1	obchodní
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
nevýrazné	výrazný	k2eNgFnSc2d1	nevýrazná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
organizace	organizace	k1gFnSc2	organizace
stane	stanout	k5eAaPmIp3nS	stanout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
významná	významný	k2eAgFnSc1d1	významná
a	a	k8xC	a
vlivná	vlivný	k2eAgFnSc1d1	vlivná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
soupeřit	soupeřit	k5eAaImF	soupeřit
i	i	k9	i
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
královstvími	království	k1gNnPc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
s	s	k7c7	s
Řádem	řád	k1gInSc7	řád
mečových	mečový	k2eAgMnPc2d1	mečový
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Prusové	Prus	k1gMnPc1	Prus
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
kleštích	kleště	k1gFnPc6	kleště
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1283	[number]	k4	1283
bezohledně	bezohledně	k6eAd1	bezohledně
podmaněni	podmanit	k5eAaPmNgMnP	podmanit
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
kladli	klást	k5eAaImAgMnP	klást
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1283	[number]	k4	1283
se	se	k3xPyFc4	se
Prusové	Prus	k1gMnPc1	Prus
vzmohli	vzmoct	k5eAaPmAgMnP	vzmoct
na	na	k7c4	na
mohutné	mohutný	k2eAgNnSc4d1	mohutné
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
částečně	částečně	k6eAd1	částečně
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
a	a	k8xC	a
iniciovaný	iniciovaný	k2eAgInSc1d1	iniciovaný
sousedním	sousední	k2eAgNnSc7d1	sousední
Pomořanskem	Pomořansko	k1gNnSc7	Pomořansko
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
ochromit	ochromit	k5eAaPmF	ochromit
vzmáhající	vzmáhající	k2eAgInSc4d1	vzmáhající
se	se	k3xPyFc4	se
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řádový	řádový	k2eAgInSc1d1	řádový
stát	stát	k1gInSc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Pruska	Prusko	k1gNnSc2	Prusko
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mocný	mocný	k2eAgInSc1d1	mocný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
prosperující	prosperující	k2eAgInSc1d1	prosperující
Stát	stát	k1gInSc1	stát
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Staat	Staat	k1gInSc1	Staat
des	des	k1gNnSc2	des
Deutschen	Deutschen	k2eAgInSc1d1	Deutschen
Ordens	Ordens	k1gInSc1	Ordens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nepřesně	přesně	k6eNd1	přesně
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Pruský	pruský	k2eAgInSc1d1	pruský
řádový	řádový	k2eAgInSc1d1	řádový
stát	stát	k1gInSc1	stát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
)	)	kIx)	)
či	či	k8xC	či
zastarale	zastarale	k6eAd1	zastarale
"	"	kIx"	"
<g/>
Řádová	řádový	k2eAgFnSc1d1	řádová
země	země	k1gFnSc1	země
Prusy	Prus	k1gMnPc4	Prus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
řádový	řádový	k2eAgInSc1d1	řádový
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
(	(	kIx(	(
<g/>
Terra	Terra	k1gFnSc1	Terra
Mariana	Mariana	k1gFnSc1	Mariana
<g/>
)	)	kIx)	)
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
livonská	livonský	k2eAgFnSc1d1	livonská
větev	větev	k1gFnSc1	větev
řádu	řád	k1gInSc2	řád
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
livonská	livonský	k2eAgFnSc1d1	livonská
větev	větev	k1gFnSc1	větev
počali	počnout	k5eAaPmAgMnP	počnout
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
s	s	k7c7	s
dominantní	dominantní	k2eAgFnSc7d1	dominantní
sítí	síť	k1gFnSc7	síť
vojenských	vojenský	k2eAgNnPc2d1	vojenské
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
netradičně	tradičně	k6eNd1	tradičně
budovány	budovat	k5eAaImNgFnP	budovat
z	z	k7c2	z
pálených	pálený	k2eAgFnPc2d1	pálená
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
donutit	donutit	k5eAaPmF	donutit
místní	místní	k2eAgNnSc4d1	místní
porobené	porobený	k2eAgNnSc4d1	porobené
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
přijmout	přijmout	k5eAaPmF	přijmout
křest	křest	k1gInSc4	křest
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přivedl	přivést	k5eAaPmAgInS	přivést
řád	řád	k1gInSc1	řád
na	na	k7c4	na
své	své	k1gNnSc4	své
území	území	k1gNnSc2	území
mnoho	mnoho	k4c1	mnoho
německých	německý	k2eAgMnPc2d1	německý
osadníků	osadník	k1gMnPc2	osadník
a	a	k8xC	a
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
germanizováno	germanizovat	k5eAaBmNgNnS	germanizovat
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
rovněž	rovněž	k6eAd1	rovněž
byly	být	k5eAaImAgInP	být
položeny	položit	k5eAaPmNgInP	položit
i	i	k9	i
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
zrod	zrod	k1gInSc4	zrod
budoucího	budoucí	k2eAgInSc2d1	budoucí
německého	německý	k2eAgInSc2d1	německý
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc4d1	původní
pobaltské	pobaltský	k2eAgNnSc1d1	pobaltské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
takto	takto	k6eAd1	takto
kompletně	kompletně	k6eAd1	kompletně
asimilováno	asimilován	k2eAgNnSc1d1	asimilováno
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kultura	kultura	k1gFnSc1	kultura
i	i	k8xC	i
jazyk	jazyk	k1gInSc4	jazyk
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
štědré	štědrý	k2eAgInPc1d1	štědrý
výnosy	výnos	k1gInPc1	výnos
i	i	k8xC	i
podpora	podpora	k1gFnSc1	podpora
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
řád	řád	k1gInSc1	řád
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
Západu	západ	k1gInSc2	západ
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
neustálého	neustálý	k2eAgInSc2d1	neustálý
přísunu	přísun	k1gInSc2	přísun
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
řad	řada	k1gFnPc2	řada
nepřicházeli	přicházet	k5eNaImAgMnP	přicházet
pouze	pouze	k6eAd1	pouze
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
rytíři-šlechtici	rytíři-šlechtik	k1gMnPc1	rytíři-šlechtik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
kdokoli	kdokoli	k3yInSc1	kdokoli
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
nebo	nebo	k8xC	nebo
zvládal	zvládat	k5eAaImAgInS	zvládat
německý	německý	k2eAgInSc1d1	německý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádu	řád	k1gInSc6	řád
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
(	(	kIx(	(
<g/>
jako	jako	k9	jako
"	"	kIx"	"
<g/>
host	host	k1gMnSc1	host
<g/>
"	"	kIx"	"
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
západní	západní	k2eAgMnPc4d1	západní
šlechtice	šlechtic	k1gMnPc4	šlechtic
vítanou	vítaný	k2eAgFnSc7d1	vítaná
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
,	,	kIx,	,
kterak	kterak	k8xS	kterak
získat	získat	k5eAaPmF	získat
bojové	bojový	k2eAgFnPc4d1	bojová
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
i	i	k9	i
majetek	majetek	k1gInSc4	majetek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
válečné	válečný	k2eAgFnSc2d1	válečná
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
pruského	pruský	k2eAgMnSc2d1	pruský
a	a	k8xC	a
pomořanského	pomořanský	k2eAgNnSc2d1	Pomořanské
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
agrese	agrese	k1gFnSc1	agrese
řádu	řád	k1gInSc2	řád
obrátila	obrátit	k5eAaPmAgFnS	obrátit
proti	proti	k7c3	proti
pohanské	pohanský	k2eAgFnSc3d1	pohanská
Litvě	Litva	k1gFnSc3	Litva
a	a	k8xC	a
pravoslavným	pravoslavný	k2eAgNnSc7d1	pravoslavné
ruským	ruský	k2eAgNnSc7d1	ruské
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
z	z	k7c2	z
katolického	katolický	k2eAgNnSc2d1	katolické
hlediska	hledisko	k1gNnSc2	hledisko
za	za	k7c4	za
heretiky	heretik	k1gMnPc4	heretik
či	či	k8xC	či
schizmatiky	schizmatik	k1gMnPc4	schizmatik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
proti	proti	k7c3	proti
katolickému	katolický	k2eAgNnSc3d1	katolické
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
udržovalo	udržovat	k5eAaImAgNnS	udržovat
blízké	blízký	k2eAgNnSc1d1	blízké
spojenectví	spojenectví	k1gNnSc1	spojenectví
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
livonští	livonský	k2eAgMnPc1d1	livonský
řádoví	řádový	k2eAgMnPc1d1	řádový
rytíři	rytíř	k1gMnPc1	rytíř
v	v	k7c4	v
koordinaci	koordinace	k1gFnSc4	koordinace
se	s	k7c7	s
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
vylodila	vylodit	k5eAaPmAgNnP	vylodit
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Něvy	Něva	k1gFnSc2	Něva
<g/>
,	,	kIx,	,
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
pevnosti	pevnost	k1gFnSc3	pevnost
Izborsk	Izborsk	k1gInSc4	Izborsk
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
svoje	své	k1gNnSc1	své
brány	brána	k1gFnSc2	brána
důležité	důležitý	k2eAgNnSc1d1	důležité
obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
Pskov	Pskov	k1gInSc1	Pskov
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vládnoucí	vládnoucí	k2eAgMnPc1d1	vládnoucí
bojaři	bojar	k1gMnPc1	bojar
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
uznat	uznat	k5eAaPmF	uznat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
Pskov	Pskov	k1gInSc1	Pskov
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
novgorodské	novgorodský	k2eAgFnSc2d1	Novgorodská
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rytíři	rytíř	k1gMnPc1	rytíř
odsud	odsud	k6eAd1	odsud
podnikali	podnikat	k5eAaImAgMnP	podnikat
další	další	k2eAgFnPc4d1	další
výpravy	výprava	k1gFnPc4	výprava
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
nebezpečně	bezpečně	k6eNd1	bezpečně
blížili	blížit	k5eAaImAgMnP	blížit
k	k	k7c3	k
Novgorodu	Novgorod	k1gInSc3	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Novgorodský	novgorodský	k2eAgMnSc1d1	novgorodský
kníže	kníže	k1gMnSc1	kníže
Alexandr	Alexandr	k1gMnSc1	Alexandr
Něvský	něvský	k2eAgMnSc1d1	něvský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
porazil	porazit	k5eAaPmAgMnS	porazit
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
Švédy	švéda	k1gFnSc2	švéda
na	na	k7c6	na
Něvě	Něva	k1gFnSc6	Něva
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
urychleně	urychleně	k6eAd1	urychleně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
tu	ten	k3xDgFnSc4	ten
křižáckou	křižácký	k2eAgFnSc4d1	křižácká
pevnost	pevnost	k1gFnSc4	pevnost
Koporje	Koporj	k1gFnSc2	Koporj
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ji	on	k3xPp3gFnSc4	on
rozbořit	rozbořit	k5eAaPmF	rozbořit
(	(	kIx(	(
<g/>
1241	[number]	k4	1241
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
řádovou	řádový	k2eAgFnSc4d1	řádová
posádku	posádka	k1gFnSc4	posádka
z	z	k7c2	z
Pskova	Pskov	k1gInSc2	Pskov
<g/>
.	.	kIx.	.
</s>
<s>
Křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
vtrhnout	vtrhnout	k5eAaPmF	vtrhnout
na	na	k7c4	na
novgorodskou	novgorodský	k2eAgFnSc4d1	Novgorodská
půdu	půda	k1gFnSc4	půda
severně	severně	k6eAd1	severně
Čudského	čudský	k2eAgNnSc2d1	Čudské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
zastavil	zastavit	k5eAaPmAgMnS	zastavit
Alexandr	Alexandr	k1gMnSc1	Alexandr
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1242	[number]	k4	1242
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ledové	ledový	k2eAgFnSc3d1	ledová
<g/>
"	"	kIx"	"
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
zamrzlé	zamrzlý	k2eAgFnSc6d1	zamrzlá
jezerní	jezerní	k2eAgFnSc6d1	jezerní
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
ukončila	ukončit	k5eAaPmAgFnS	ukončit
s	s	k7c7	s
definitivní	definitivní	k2eAgFnSc7d1	definitivní
plaností	planost	k1gFnSc7	planost
pokusy	pokus	k1gInPc1	pokus
livonských	livonský	k2eAgMnPc2d1	livonský
rytířů	rytíř	k1gMnPc2	rytíř
proniknout	proniknout	k5eAaPmF	proniknout
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
východní	východní	k2eAgFnSc4d1	východní
expanzi	expanze	k1gFnSc4	expanze
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1254-1255	[number]	k4	1254-1255
vydal	vydat	k5eAaPmAgInS	vydat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
členů	člen	k1gMnPc2	člen
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
na	na	k7c4	na
Přemyslovu	Přemyslův	k2eAgFnSc4d1	Přemyslova
počest	počest	k1gFnSc4	počest
Königsberg	Königsberg	k1gInSc1	Königsberg
(	(	kIx(	(
<g/>
Královec	Královec	k1gInSc1	Královec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působiště	působiště	k1gNnSc1	působiště
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
se	se	k3xPyFc4	se
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
plně	plně	k6eAd1	plně
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řád	řád	k1gInSc1	řád
postupně	postupně	k6eAd1	postupně
založil	založit	k5eAaPmAgInS	založit
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1274	[number]	k4	1274
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
hrad	hrad	k1gInSc1	hrad
Marienburg	Marienburg	k1gInSc1	Marienburg
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Malbork	Malbork	k1gInSc1	Malbork
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
podhradí	podhradí	k1gNnSc6	podhradí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brzy	brzy	k6eAd1	brzy
získala	získat	k5eAaPmAgNnP	získat
městská	městský	k2eAgNnPc1d1	Městské
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1309	[number]	k4	1309
se	se	k3xPyFc4	se
Marienburk	Marienburk	k1gInSc1	Marienburk
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
velmistra	velmistr	k1gMnSc4	velmistr
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
řád	řád	k1gInSc1	řád
obsadil	obsadit	k5eAaPmAgInS	obsadit
Východní	východní	k2eAgNnSc4d1	východní
Pomoří	Pomoří	k1gNnSc4	Pomoří
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1321	[number]	k4	1321
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
k	k	k7c3	k
papežskému	papežský	k2eAgInSc3d1	papežský
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řád	řád	k1gInSc1	řád
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
území	území	k1gNnSc1	území
Polsku	Polsko	k1gNnSc6	Polsko
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Úplatkem	úplatek	k1gInSc7	úplatek
řád	řád	k1gInSc1	řád
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vymohl	vymoct	k5eAaPmAgMnS	vymoct
na	na	k7c4	na
papeži	papež	k1gMnSc3	papež
zrušení	zrušení	k1gNnSc6	zrušení
rozsudku	rozsudek	k1gInSc3	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
papež	papež	k1gMnSc1	papež
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Avignonském	avignonský	k2eAgNnSc6d1	avignonské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
a	a	k8xC	a
rozsudek	rozsudek	k1gInSc4	rozsudek
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1330	[number]	k4	1330
řád	řád	k1gInSc1	řád
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Pustošil	pustošit	k5eAaImAgMnS	pustošit
Kujavy	Kujava	k1gFnPc4	Kujava
a	a	k8xC	a
Velkopolsko	Velkopolsko	k1gNnSc4	Velkopolsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1331	[number]	k4	1331
byla	být	k5eAaImAgNnP	být
řádová	řádový	k2eAgNnPc1d1	řádové
vojska	vojsko	k1gNnPc1	vojsko
zastavena	zastaven	k2eAgNnPc1d1	zastaveno
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Płowce	Płowce	k1gFnSc2	Płowce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
politických	politický	k2eAgInPc2d1	politický
cílů	cíl	k1gInPc2	cíl
Polska	Polsko	k1gNnSc2	Polsko
stalo	stát	k5eAaPmAgNnS	stát
zničení	zničení	k1gNnSc1	zničení
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
projevoval	projevovat	k5eAaImAgInS	projevovat
Řád	řád	k1gInSc4	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
o	o	k7c6	o
Žmuď	Žmuď	k1gFnSc6	Žmuď
(	(	kIx(	(
<g/>
Žemaitsko	Žemaitsko	k1gNnSc1	Žemaitsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
území	území	k1gNnSc6	území
litevských	litevský	k2eAgInPc2d1	litevský
Žemaitů	Žemait	k1gInPc2	Žemait
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vklíněno	vklínit	k5eAaPmNgNnS	vklínit
mezi	mezi	k7c4	mezi
území	území	k1gNnSc4	území
řádových	řádový	k2eAgInPc2d1	řádový
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
část	část	k1gFnSc4	část
Žmudi	Žmud	k1gMnPc1	Žmud
první	první	k4xOgMnSc1	první
známější	známý	k2eAgMnSc1d2	známější
představitel	představitel	k1gMnSc1	představitel
litevského	litevský	k2eAgInSc2d1	litevský
státu	stát	k1gInSc2	stát
kníže	kníže	k1gMnSc1	kníže
Mindaugas	Mindaugas	k1gMnSc1	Mindaugas
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
koalice	koalice	k1gFnSc1	koalice
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
podmaněných	podmaněný	k2eAgMnPc2d1	podmaněný
žemaitských	žemaitský	k2eAgMnPc2d1	žemaitský
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
Jatvingů	Jatving	k1gInPc2	Jatving
<g/>
,	,	kIx,	,
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
Mindaugas	Mindaugas	k1gInSc4	Mindaugas
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
odstoupením	odstoupení	k1gNnSc7	odstoupení
Žmudi	Žmud	k1gMnPc1	Žmud
řádu	řád	k1gInSc3	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
řád	řád	k1gInSc1	řád
pokusil	pokusit	k5eAaPmAgInS	pokusit
převzít	převzít	k5eAaPmF	převzít
Žmuď	Žmuď	k1gFnPc4	Žmuď
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
těžkou	těžký	k2eAgFnSc4d1	těžká
porážku	porážka	k1gFnSc4	porážka
u	u	k7c2	u
Durbeského	Durbeský	k2eAgNnSc2d1	Durbeský
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Mindaugas	Mindaugas	k1gInSc1	Mindaugas
svoje	svůj	k3xOyFgInPc4	svůj
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
rytíři	rytíř	k1gMnPc7	rytíř
přerušil	přerušit	k5eAaPmAgMnS	přerušit
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
pohanským	pohanský	k2eAgInPc3d1	pohanský
kultům	kult	k1gInPc3	kult
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Žmudi	Žmud	k1gMnPc1	Žmud
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
objektem	objekt	k1gInSc7	objekt
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
křižácké	křižácký	k2eAgFnSc2d1	křižácká
expanze	expanze	k1gFnSc2	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
útočili	útočit	k5eAaImAgMnP	útočit
řádoví	řádový	k2eAgMnPc1d1	řádový
rytíři	rytíř	k1gMnPc1	rytíř
z	z	k7c2	z
Prus	Prus	k1gMnSc1	Prus
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
livonská	livonský	k2eAgFnSc1d1	livonská
větev	větev	k1gFnSc1	větev
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
právě	právě	k9	právě
opanovala	opanovat	k5eAaPmAgFnS	opanovat
území	území	k1gNnSc2	území
Zemgalů	Zemgal	k1gInPc2	Zemgal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmach	rozmach	k1gInSc1	rozmach
řádu	řád	k1gInSc2	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
se	se	k3xPyFc4	se
řád	řád	k1gInSc1	řád
dočkal	dočkat	k5eAaPmAgInS	dočkat
za	za	k7c2	za
velmistra	velmistr	k1gMnSc2	velmistr
Winricha	Winrich	k1gMnSc2	Winrich
von	von	k1gInSc1	von
Kniprode	Kniprod	k1gInSc5	Kniprod
v	v	k7c6	v
letech	let	k1gInPc6	let
1351	[number]	k4	1351
<g/>
–	–	k?	–
<g/>
1382	[number]	k4	1382
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
jeho	jeho	k3xOp3gFnPc1	jeho
pozice	pozice	k1gFnPc1	pozice
slábnout	slábnout	k5eAaImF	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
uzavření	uzavření	k1gNnSc2	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Krevě	Kreva	k1gFnSc6	Kreva
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
k	k	k7c3	k
christianizaci	christianizace	k1gFnSc3	christianizace
Litevců	Litevec	k1gMnPc2	Litevec
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
spojenectví	spojenectví	k1gNnSc1	spojenectví
proti	proti	k7c3	proti
řádu	řád	k1gInSc3	řád
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc4	ten
němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
nyní	nyní	k6eAd1	nyní
ztratili	ztratit	k5eAaPmAgMnP	ztratit
ideologický	ideologický	k2eAgInSc4d1	ideologický
důvod	důvod	k1gInSc4	důvod
k	k	k7c3	k
agresi	agrese	k1gFnSc3	agrese
na	na	k7c4	na
litevské	litevský	k2eAgNnSc4d1	litevské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
zneužil	zneužít	k5eAaPmAgMnS	zneužít
bratranec	bratranec	k1gMnSc1	bratranec
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
a	a	k8xC	a
litevského	litevský	k2eAgMnSc2d1	litevský
velkoknížete	velkokníže	k1gMnSc2	velkokníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagella	Jagella	k1gMnSc1	Jagella
Vytautas	Vytautas	k1gMnSc1	Vytautas
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Salinu	salina	k1gFnSc4	salina
tajnou	tajný	k2eAgFnSc4d1	tajná
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
osamostatnění	osamostatnění	k1gNnSc1	osamostatnění
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
Žmudi	Žmud	k1gMnPc1	Žmud
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Paktování	paktování	k1gNnSc1	paktování
se	se	k3xPyFc4	se
s	s	k7c7	s
řádem	řád	k1gInSc7	řád
proti	proti	k7c3	proti
příbuzným	příbuzná	k1gFnPc3	příbuzná
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
litevské	litevský	k2eAgFnSc6d1	Litevská
vládnoucí	vládnoucí	k2eAgFnSc6d1	vládnoucí
dynastii	dynastie	k1gFnSc6	dynastie
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
moc	moc	k6eAd1	moc
ničím	nic	k3yNnSc7	nic
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
zahájili	zahájit	k5eAaPmAgMnP	zahájit
křižáci	křižák	k1gMnPc1	křižák
dobývání	dobývání	k1gNnSc6	dobývání
Žmudi	Žmud	k1gMnPc1	Žmud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
však	však	k9	však
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
roku	rok	k1gInSc2	rok
1401	[number]	k4	1401
proti	proti	k7c3	proti
dobyvatelům	dobyvatel	k1gMnPc3	dobyvatel
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
podpořil	podpořit	k5eAaPmAgMnS	podpořit
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Vytautas	Vytautas	k1gMnSc1	Vytautas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nebyla	být	k5eNaImAgFnS	být
litevská	litevský	k2eAgFnSc1d1	Litevská
strana	strana	k1gFnSc1	strana
navzdory	navzdory	k7c3	navzdory
polské	polský	k2eAgFnSc3d1	polská
pomoci	pomoc	k1gFnSc3	pomoc
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
vmísil	vmísit	k5eAaPmAgMnS	vmísit
římský	římský	k2eAgMnSc1d1	římský
papež	papež	k1gMnSc1	papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
IX	IX	kA	IX
<g/>
.	.	kIx.	.
a	a	k8xC	a
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
roku	rok	k1gInSc2	rok
1404	[number]	k4	1404
uzavření	uzavření	k1gNnSc2	uzavření
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byla	být	k5eAaImAgFnS	být
Žmuď	Žmuď	k1gFnSc1	Žmuď
přiřčena	přiřčen	k2eAgFnSc1d1	přiřčena
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Porážka	porážka	k1gFnSc1	porážka
a	a	k8xC	a
úpadek	úpadek	k1gInSc1	úpadek
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
na	na	k7c4	na
Žmudi	Žmud	k1gMnPc1	Žmud
další	další	k2eAgNnSc4d1	další
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
řádu	řád	k1gInSc3	řád
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
Vytautas	Vytautas	k1gMnSc1	Vytautas
opět	opět	k6eAd1	opět
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
podpořit	podpořit	k5eAaPmF	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
doufal	doufat	k5eAaImAgInS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
<g/>
,	,	kIx,	,
zachová	zachovat	k5eAaPmIp3nS	zachovat
Polsko	Polsko	k1gNnSc4	Polsko
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jagiello	Jagiello	k1gNnSc4	Jagiello
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
velké	velký	k2eAgFnSc2d1	velká
války	válka	k1gFnSc2	válka
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
a	a	k8xC	a
pruském	pruský	k2eAgNnSc6d1	pruské
území	území	k1gNnSc6	území
a	a	k8xC	a
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1410	[number]	k4	1410
katastrofální	katastrofální	k2eAgFnSc7d1	katastrofální
porážkou	porážka	k1gFnSc7	porážka
řádu	řád	k1gInSc2	řád
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
(	(	kIx(	(
<g/>
Tannenbergu	Tannenberg	k1gInSc2	Tannenberg
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Žalgiris	Žalgiris	k1gFnSc1	Žalgiris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc1	bitva
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
obrovské	obrovský	k2eAgInPc4d1	obrovský
vojenské	vojenský	k2eAgInPc4d1	vojenský
kontingenty	kontingent	k1gInPc4	kontingent
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
účastnických	účastnický	k2eAgFnPc2d1	účastnická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
žoldnéři	žoldnér	k1gMnPc1	žoldnér
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
polsko-litevském	polskoitevský	k2eAgNnSc6d1	polsko-litevské
vojsku	vojsko	k1gNnSc6	vojsko
<g/>
,	,	kIx,	,
vedeném	vedený	k2eAgInSc6d1	vedený
Jagellem	Jagell	k1gMnSc7	Jagell
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
bojovali	bojovat	k5eAaImAgMnP	bojovat
Tataři	Tatar	k1gMnPc1	Tatar
a	a	k8xC	a
pomocný	pomocný	k2eAgInSc1d1	pomocný
oddíl	oddíl	k1gInSc1	oddíl
z	z	k7c2	z
moldavského	moldavský	k2eAgNnSc2d1	moldavské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
vazalsky	vazalsky	k6eAd1	vazalsky
závislého	závislý	k2eAgInSc2d1	závislý
na	na	k7c6	na
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Moravané	Moravan	k1gMnPc1	Moravan
se	se	k3xPyFc4	se
bitvy	bitva	k1gFnSc2	bitva
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
řádových	řádový	k2eAgMnPc2d1	řádový
rytířů	rytíř	k1gMnPc2	rytíř
včetně	včetně	k7c2	včetně
velmistra	velmistr	k1gMnSc2	velmistr
Ulricha	Ulrich	k1gMnSc2	Ulrich
von	von	k1gInSc4	von
Jungingen	Jungingen	k1gInSc4	Jungingen
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
také	také	k9	také
livonští	livonský	k2eAgMnPc1d1	livonský
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Příznivou	příznivý	k2eAgFnSc7d1	příznivá
okolností	okolnost	k1gFnSc7	okolnost
pro	pro	k7c4	pro
řád	řád	k1gInSc4	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
byly	být	k5eAaImAgInP	být
značné	značný	k2eAgInPc1d1	značný
rozpory	rozpor	k1gInPc1	rozpor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
existovaly	existovat	k5eAaImAgInP	existovat
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
protivníky	protivník	k1gMnPc7	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1411	[number]	k4	1411
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
první	první	k4xOgInSc1	první
toruňský	toruňský	k2eAgInSc1d1	toruňský
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
vzdát	vzdát	k5eAaPmF	vzdát
Žmudi	Žmud	k1gMnPc1	Žmud
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
<g/>
,	,	kIx,	,
a	a	k8xC	a
Dobřínska	Dobřínsko	k1gNnSc2	Dobřínsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
získalo	získat	k5eAaPmAgNnS	získat
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
musel	muset	k5eAaImAgMnS	muset
polské	polský	k2eAgFnSc3d1	polská
straně	strana	k1gFnSc3	strana
zaplatit	zaplatit	k5eAaPmF	zaplatit
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kontribuce	kontribuce	k1gFnPc4	kontribuce
jako	jako	k8xC	jako
výkupné	výkupný	k2eAgFnPc4d1	výkupná
za	za	k7c4	za
zajaté	zajatý	k2eAgMnPc4d1	zajatý
členy	člen	k1gMnPc4	člen
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Soudí	soudit	k5eAaImIp3nS	soudit
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Litva	Litva	k1gFnSc1	Litva
získaly	získat	k5eAaPmAgFnP	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírového	mírový	k2eAgNnSc2d1	Mírové
ujednání	ujednání	k1gNnSc2	ujednání
<g/>
,	,	kIx,	,
neodpovídalo	odpovídat	k5eNaImAgNnS	odpovídat
vojenskému	vojenský	k2eAgInSc3d1	vojenský
významu	význam	k1gInSc3	význam
grunwaldského	grunwaldský	k2eAgNnSc2d1	grunwaldský
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
porážka	porážka	k1gFnSc1	porážka
čekala	čekat	k5eAaImAgFnS	čekat
slábnoucí	slábnoucí	k2eAgInSc4d1	slábnoucí
řád	řád	k1gInSc4	řád
v	v	k7c6	v
třináctileté	třináctiletý	k2eAgFnSc6d1	třináctiletá
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
pruskými	pruský	k2eAgNnPc7d1	pruské
městy	město	k1gNnPc7	město
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
probíhala	probíhat	k5eAaImAgNnP	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1454	[number]	k4	1454
<g/>
-	-	kIx~	-
<g/>
1466	[number]	k4	1466
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
tzv.	tzv.	kA	tzv.
druhý	druhý	k4xOgInSc4	druhý
toruňský	toruňský	k2eAgInSc4d1	toruňský
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jehož	jehož	k3xOyRp3gNnPc2	jehož
ustanovení	ustanovení	k1gNnPc2	ustanovení
Polsku	Polska	k1gFnSc4	Polska
připadly	připadnout	k5eAaPmAgInP	připadnout
tzv.	tzv.	kA	tzv.
Prusy	Prus	k1gMnPc4	Prus
královské	královský	k2eAgNnSc1d1	královské
(	(	kIx(	(
<g/>
Východní	východní	k2eAgNnSc4d1	východní
Pomoří	Pomoří	k1gNnSc4	Pomoří
nebo	nebo	k8xC	nebo
pozdější	pozdní	k2eAgNnSc4d2	pozdější
Západní	západní	k2eAgNnSc4d1	západní
Prusko	Prusko	k1gNnSc4	Prusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Marienburgem	Marienburg	k1gInSc7	Marienburg
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dosud	dosud	k6eAd1	dosud
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
velmistra	velmistr	k1gMnSc2	velmistr
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
Gdaňskem	Gdaňsk	k1gInSc7	Gdaňsk
<g/>
,	,	kIx,	,
Toruní	Toruň	k1gFnSc7	Toruň
a	a	k8xC	a
Elblagem	Elblag	k1gInSc7	Elblag
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
zbývajícího	zbývající	k2eAgNnSc2d1	zbývající
řádového	řádový	k2eAgNnSc2d1	řádové
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Prus	Prus	k1gMnSc1	Prus
křižáckých	křižácký	k2eAgNnPc2d1	křižácké
<g/>
,	,	kIx,	,
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
velmistra	velmistr	k1gMnSc2	velmistr
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgInS	stát
Königsberg	Königsberg	k1gInSc1	Königsberg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Královec	Královec	k1gInSc1	Královec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
založené	založený	k2eAgInPc1d1	založený
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
řádový	řádový	k2eAgInSc1d1	řádový
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
formálně	formálně	k6eAd1	formálně
polským	polský	k2eAgNnSc7d1	polské
lénem	léno	k1gNnSc7	léno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
roztržce	roztržka	k1gFnSc3	roztržka
mezi	mezi	k7c7	mezi
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
velmistr	velmistr	k1gMnSc1	velmistr
řádu	řád	k1gInSc2	řád
Fridrich	Fridrich	k1gMnSc1	Fridrich
von	von	k1gInSc4	von
Wettin	Wettin	k1gMnSc1	Wettin
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
složit	složit	k5eAaPmF	složit
vazalskou	vazalský	k2eAgFnSc4d1	vazalská
přísahu	přísaha	k1gFnSc4	přísaha
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
válka	válka	k1gFnSc1	válka
nevypukla	vypuknout	k5eNaPmAgFnS	vypuknout
jedině	jedině	k6eAd1	jedině
díky	díky	k7c3	díky
změnám	změna	k1gFnPc3	změna
na	na	k7c6	na
polském	polský	k2eAgInSc6d1	polský
trůně	trůn	k1gInSc6	trůn
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
smrti	smrt	k1gFnSc6	smrt
velmistra	velmistr	k1gMnSc2	velmistr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
velmistr	velmistr	k1gMnSc1	velmistr
řádu	řád	k1gInSc2	řád
Albrecht	Albrecht	k1gMnSc1	Albrecht
Braniborský	braniborský	k2eAgInSc1d1	braniborský
přijal	přijmout	k5eAaPmAgInS	přijmout
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
luteránskou	luteránský	k2eAgFnSc4d1	luteránská
reformaci	reformace	k1gFnSc4	reformace
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	se	k3xPyFc4	se
za	za	k7c4	za
světského	světský	k2eAgMnSc4d1	světský
vládce	vládce	k1gMnSc4	vládce
dosud	dosud	k6eAd1	dosud
řádového	řádový	k2eAgNnSc2d1	řádové
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Albrecht	Albrecht	k1gMnSc1	Albrecht
jako	jako	k8xC	jako
vládce	vládce	k1gMnSc1	vládce
tzv.	tzv.	kA	tzv.
Prus	Prus	k1gMnSc1	Prus
knížecích	knížecí	k2eAgNnPc6d1	knížecí
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
vazalskou	vazalský	k2eAgFnSc4d1	vazalská
přísahu	přísaha	k1gFnSc4	přísaha
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řád	řád	k1gInSc1	řád
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sekularizaci	sekularizace	k1gFnSc6	sekularizace
území	území	k1gNnSc2	území
pruského	pruský	k2eAgInSc2d1	pruský
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
řádu	řád	k1gInSc2	řád
poté	poté	k6eAd1	poté
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
Mergentheimu	Mergentheim	k1gInSc2	Mergentheim
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Württembersku	Württembersko	k1gNnSc6	Württembersko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
velmistry	velmistr	k1gMnPc7	velmistr
patřilo	patřit	k5eAaImAgNnS	patřit
až	až	k6eAd1	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
mnoho	mnoho	k6eAd1	mnoho
příslušníků	příslušník	k1gMnPc2	příslušník
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Řád	řád	k1gInSc1	řád
řeholních	řeholní	k2eAgMnPc2d1	řeholní
kanovníků	kanovník	k1gMnPc2	kanovník
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ztratil	ztratit	k5eAaPmAgInS	ztratit
řád	řád	k1gInSc1	řád
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
přikázal	přikázat	k5eAaPmAgMnS	přikázat
dokonce	dokonce	k9	dokonce
řád	řád	k1gInSc4	řád
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
dostal	dostat	k5eAaPmAgInS	dostat
řád	řád	k1gInSc4	řád
nová	nový	k2eAgNnPc4d1	nové
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existoval	existovat	k5eAaImAgInS	existovat
výlučně	výlučně	k6eAd1	výlučně
jako	jako	k8xC	jako
špitální	špitální	k2eAgInSc1d1	špitální
řád	řád	k1gInSc1	řád
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
dům	dům	k1gInSc1	dům
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	nedaleko	k7c2	nedaleko
svatoštěpánského	svatoštěpánský	k2eAgInSc2d1	svatoštěpánský
dómu	dóm	k1gInSc2	dóm
v	v	k7c6	v
Singerstrasse	Singerstrassa	k1gFnSc6	Singerstrassa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnPc4d1	přístupná
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Přístupná	přístupný	k2eAgFnSc1d1	přístupná
je	být	k5eAaImIp3nS	být
také	také	k9	také
řádová	řádový	k2eAgFnSc1d1	řádová
klenotnice	klenotnice	k1gFnSc1	klenotnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
===	===	k?	===
</s>
</p>
<p>
<s>
Zničující	zničující	k2eAgInSc4d1	zničující
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
řád	řád	k1gInSc4	řád
měl	mít	k5eAaImAgInS	mít
nástup	nástup	k1gInSc1	nástup
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
skalní	skalní	k2eAgMnSc1d1	skalní
odpůrce	odpůrce	k1gMnSc1	odpůrce
a	a	k8xC	a
trpěl	trpět	k5eAaImAgInS	trpět
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
pronásledováním	pronásledování	k1gNnSc7	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
zdarma	zdarma	k6eAd1	zdarma
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
pozemky	pozemek	k1gInPc4	pozemek
a	a	k8xC	a
objekty	objekt	k1gInPc4	objekt
ve	v	k7c6	v
slezském	slezský	k2eAgNnSc6d1	Slezské
pohraničí	pohraničí	k1gNnSc6	pohraničí
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Hrabyně	Hrabyně	k1gFnSc2	Hrabyně
<g/>
)	)	kIx)	)
k	k	k7c3	k
obranným	obranný	k2eAgInPc3d1	obranný
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
také	také	k9	také
půjčili	půjčit	k5eAaPmAgMnP	půjčit
státu	stát	k1gInSc3	stát
peníze	peníz	k1gInSc2	peníz
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
československého	československý	k2eAgNnSc2d1	Československé
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
okupační	okupační	k2eAgInPc1d1	okupační
orgány	orgán	k1gInPc1	orgán
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
nad	nad	k7c7	nad
protektorátem	protektorát	k1gInSc7	protektorát
(	(	kIx(	(
<g/>
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
nejdříve	dříve	k6eAd3	dříve
uvalily	uvalit	k5eAaPmAgInP	uvalit
nucenou	nucený	k2eAgFnSc4d1	nucená
správu	správa	k1gFnSc4	správa
na	na	k7c4	na
majetek	majetek	k1gInSc4	majetek
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
zkonfiskovaly	zkonfiskovat	k5eAaPmAgFnP	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
řád	řád	k1gInSc1	řád
krutě	krutě	k6eAd1	krutě
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gMnPc2	jeho
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
mužské	mužský	k2eAgFnSc2d1	mužská
větve	větev	k1gFnSc2	větev
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nacisty	nacista	k1gMnPc4	nacista
vězněna	vězněn	k2eAgFnSc1d1	vězněna
<g/>
,	,	kIx,	,
popravena	popraven	k2eAgFnSc1d1	popravena
nebo	nebo	k8xC	nebo
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
a	a	k8xC	a
řádový	řádový	k2eAgInSc1d1	řádový
majetek	majetek	k1gInSc1	majetek
byl	být	k5eAaImAgInS	být
nacisty	nacista	k1gMnPc7	nacista
konfiskován	konfiskován	k2eAgInSc4d1	konfiskován
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následovalo	následovat	k5eAaImAgNnS	následovat
po	po	k7c6	po
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
pronásledování	pronásledování	k1gNnSc6	pronásledování
i	i	k8xC	i
pronásledování	pronásledování	k1gNnSc4	pronásledování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
německou	německý	k2eAgFnSc4d1	německá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadnárodní	nadnárodní	k2eAgInPc1d1	nadnárodní
<g/>
,	,	kIx,	,
protinacisticky	protinacisticky	k6eAd1	protinacisticky
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
a	a	k8xC	a
nacisty	nacista	k1gMnSc2	nacista
pronásledovanou	pronásledovaný	k2eAgFnSc4d1	pronásledovaná
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
československé	československý	k2eAgFnSc2d1	Československá
–	–	k?	–
komunisty	komunista	k1gMnPc7	komunista
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
–	–	k?	–
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
konfiskaci	konfiskace	k1gFnSc4	konfiskace
majetku	majetek	k1gInSc2	majetek
řádu	řád	k1gInSc2	řád
podle	podle	k7c2	podle
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
a	a	k8xC	a
provedlo	provést	k5eAaPmAgNnS	provést
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
zakazovalo	zakazovat	k5eAaImAgNnS	zakazovat
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
nařízení	nařízení	k1gNnPc2	nařízení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
konfiskace	konfiskace	k1gFnSc2	konfiskace
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
neodůvodněné	odůvodněný	k2eNgInPc4d1	neodůvodněný
a	a	k8xC	a
nezákonné	zákonný	k2eNgInPc4d1	nezákonný
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
náleží	náležet	k5eAaImIp3nP	náležet
konfiskovaný	konfiskovaný	k2eAgInSc4d1	konfiskovaný
majetek	majetek	k1gInSc4	majetek
stále	stále	k6eAd1	stále
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
převrat	převrat	k1gInSc1	převrat
ale	ale	k9	ale
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
převzetí	převzetí	k1gNnSc4	převzetí
majetku	majetek	k1gInSc2	majetek
řádem	řád	k1gInSc7	řád
a	a	k8xC	a
komunistické	komunistický	k2eAgInPc1d1	komunistický
úřady	úřad	k1gInPc1	úřad
poté	poté	k6eAd1	poté
vyhnaly	vyhnat	k5eAaPmAgInP	vyhnat
všechny	všechen	k3xTgFnPc4	všechen
rytíře	rytíř	k1gMnSc4	rytíř
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
odsunu	odsun	k1gInSc2	odsun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jen	jen	k9	jen
dva	dva	k4xCgMnPc1	dva
rytíři-řeholníci	rytíři-řeholník	k1gMnPc1	rytíři-řeholník
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
krutě	krutě	k6eAd1	krutě
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
<g/>
,	,	kIx,	,
řeholníci	řeholník	k1gMnPc1	řeholník
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
familiáři	familiář	k1gMnPc1	familiář
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nezákonně	zákonně	k6eNd1	zákonně
internováni	internován	k2eAgMnPc1d1	internován
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pak	pak	k6eAd1	pak
odsouzeni	odsouzen	k2eAgMnPc1d1	odsouzen
ve	v	k7c6	v
vykonstruovaných	vykonstruovaný	k2eAgInPc6d1	vykonstruovaný
procesech	proces	k1gInPc6	proces
a	a	k8xC	a
vězněni	vězněn	k2eAgMnPc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Helena	Helena	k1gFnSc1	Helena
Knajblová	Knajblová	k1gFnSc1	Knajblová
na	na	k7c4	na
následky	následek	k1gInPc4	následek
věznění	věznění	k1gNnSc2	věznění
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
řádu	řád	k1gInSc2	řád
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovená	obnovený	k2eAgFnSc1d1	obnovená
činnost	činnost	k1gFnSc1	činnost
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
řád	řád	k1gInSc4	řád
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
obnovil	obnovit	k5eAaPmAgInS	obnovit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
zdejší	zdejší	k2eAgNnSc1d1	zdejší
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
provinciál	provinciál	k1gMnSc1	provinciál
i	i	k8xC	i
představená	představená	k1gFnSc1	představená
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mužská	mužský	k2eAgFnSc1d1	mužská
tak	tak	k6eAd1	tak
ženská	ženský	k2eAgFnSc1d1	ženská
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jako	jako	k8xC	jako
české	český	k2eAgFnSc2d1	Česká
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
větev	větev	k1gFnSc1	větev
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc4	název
Milosrdné	milosrdný	k2eAgFnSc2d1	milosrdná
sestry	sestra	k1gFnSc2	sestra
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
řád	řád	k1gInSc1	řád
užívá	užívat	k5eAaImIp3nS	užívat
oficiální	oficiální	k2eAgInPc4d1	oficiální
názvy	název	k1gInPc4	název
"	"	kIx"	"
<g/>
Německý	německý	k2eAgInSc4d1	německý
řád	řád	k1gInSc4	řád
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
Bratři	bratr	k1gMnPc1	bratr
německého	německý	k2eAgInSc2d1	německý
řádu	řád	k1gInSc2	řád
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
a	a	k8xC	a
sociálních	sociální	k2eAgNnPc2d1	sociální
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
na	na	k7c4	na
charitativní	charitativní	k2eAgFnSc4d1	charitativní
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vypomáhá	vypomáhat	k5eAaImIp3nS	vypomáhat
také	také	k9	také
v	v	k7c6	v
duchovní	duchovní	k2eAgFnSc6d1	duchovní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
majetkoprávním	majetkoprávní	k2eAgInSc7d1	majetkoprávní
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
o	o	k7c4	o
majetek	majetek	k1gInSc4	majetek
řádu	řád	k1gInSc2	řád
zkonfiskovaný	zkonfiskovaný	k2eAgInSc4d1	zkonfiskovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnPc4d3	veliký
vášně	vášeň	k1gFnPc4	vášeň
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
spor	spor	k1gInSc1	spor
o	o	k7c4	o
hrad	hrad	k1gInSc4	hrad
Bouzov	Bouzovo	k1gNnPc2	Bouzovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
řád	řád	k1gInSc1	řád
převzal	převzít	k5eAaPmAgInS	převzít
Moravské	moravský	k2eAgNnSc4d1	Moravské
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
hodlá	hodlat	k5eAaImIp3nS	hodlat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Církevní	církevní	k2eAgNnPc4d1	církevní
gymnázium	gymnázium	k1gNnSc4	gymnázium
Německého	německý	k2eAgInSc2d1	německý
řádu	řád	k1gInSc2	řád
vybudovat	vybudovat	k5eAaPmF	vybudovat
prestižní	prestižní	k2eAgFnSc4d1	prestižní
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
instituci	instituce	k1gFnSc4	instituce
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
<g/>
,	,	kIx,	,
rakouské	rakouský	k2eAgFnSc3d1	rakouská
a	a	k8xC	a
italské	italský	k2eAgFnSc2d1	italská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
německá	německý	k2eAgFnSc1d1	německá
tak	tak	k8xS	tak
i	i	k9	i
polská	polský	k2eAgFnSc1d1	polská
historiografie	historiografie	k1gFnSc1	historiografie
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
značné	značný	k2eAgFnSc2d1	značná
potíže	potíž	k1gFnSc2	potíž
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
působení	působení	k1gNnSc4	působení
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
z	z	k7c2	z
neutrálního	neutrální	k2eAgNnSc2d1	neutrální
stanoviska	stanovisko	k1gNnSc2	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
polská	polský	k2eAgFnSc1d1	polská
historiografie	historiografie	k1gFnSc1	historiografie
tíhla	tíhnout	k5eAaImAgFnS	tíhnout
k	k	k7c3	k
výrokům	výrok	k1gInPc3	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
vláda	vláda	k1gFnSc1	vláda
Řádu	řád	k1gInSc2	řád
znamenala	znamenat	k5eAaImAgFnS	znamenat
útisk	útisk	k1gInSc4	útisk
a	a	k8xC	a
kolonizaci	kolonizace	k1gFnSc4	kolonizace
polských	polský	k2eAgNnPc2d1	polské
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
historiografie	historiografie	k1gFnSc1	historiografie
často	často	k6eAd1	často
mínila	mínit	k5eAaImAgFnS	mínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstoupením	odstoupení	k1gNnSc7	odstoupení
řádových	řádový	k2eAgFnPc2d1	řádová
zemí	zem	k1gFnPc2	zem
Polsku	Polsko	k1gNnSc6	Polsko
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zradě	zrada	k1gFnSc3	zrada
a	a	k8xC	a
vydání	vydání	k1gNnSc6	vydání
německých	německý	k2eAgNnPc2d1	německé
území	území	k1gNnPc2	území
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
pod	pod	k7c4	pod
cizí	cizí	k2eAgFnSc4d1	cizí
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
lze	lze	k6eAd1	lze
však	však	k9	však
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
národní	národní	k2eAgNnSc4d1	národní
či	či	k8xC	či
národnostní	národnostní	k2eAgNnSc4d1	národnostní
střetnutí	střetnutí	k1gNnSc4	střetnutí
mezi	mezi	k7c7	mezi
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
Slovany	Slovan	k1gMnPc7	Slovan
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ale	ale	k9	ale
o	o	k7c4	o
konflikty	konflikt	k1gInPc4	konflikt
mezi	mezi	k7c7	mezi
konkurujícími	konkurující	k2eAgFnPc7d1	konkurující
lokálními	lokální	k2eAgFnPc7d1	lokální
mocnostmi	mocnost	k1gFnPc7	mocnost
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chtěly	chtít	k5eAaImAgFnP	chtít
expandovat	expandovat	k5eAaImF	expandovat
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Heraldika	heraldika	k1gFnSc1	heraldika
==	==	k?	==
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
řádů	řád	k1gInPc2	řád
byl	být	k5eAaImAgInS	být
tvar	tvar	k1gInSc1	tvar
černého	černý	k2eAgInSc2d1	černý
kříže	kříž	k1gInSc2	kříž
často	často	k6eAd1	často
měněn	měněn	k2eAgInSc1d1	měněn
<g/>
.	.	kIx.	.
</s>
<s>
Zlatě	zlatě	k6eAd1	zlatě
lemovaný	lemovaný	k2eAgInSc1d1	lemovaný
černý	černý	k2eAgInSc1d1	černý
heraldický	heraldický	k2eAgInSc1d1	heraldický
kříž	kříž	k1gInSc1	kříž
doplněný	doplněný	k2eAgInSc1d1	doplněný
zlatým	zlatý	k2eAgInSc7d1	zlatý
erbem	erb	k1gInSc7	erb
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
orlicí	orlice	k1gFnSc7	orlice
byl	být	k5eAaImAgInS	být
výhradně	výhradně	k6eAd1	výhradně
znakem	znak	k1gInSc7	znak
velmistra	velmistr	k1gMnSc4	velmistr
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgNnSc1d1	vojenské
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gNnSc4	on
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
za	za	k7c2	za
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
nacistické	nacistický	k2eAgFnSc2d1	nacistická
letecké	letecký	k2eAgFnSc2d1	letecká
síly	síla	k1gFnSc2	síla
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ADAM	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
:	:	kIx,	:
malý	malý	k2eAgInSc4d1	malý
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
Německého	německý	k2eAgInSc2d1	německý
řádu	řád	k1gInSc2	řád
:	:	kIx,	:
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
komorní	komorní	k2eAgFnPc4d1	komorní
bailivy	bailiva	k1gFnSc2	bailiva
<g/>
.	.	kIx.	.
</s>
<s>
Svitavy	Svitava	k1gFnPc1	Svitava
<g/>
:	:	kIx,	:
Trinitas	Trinitas	k1gInSc1	Trinitas
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86036	[number]	k4	86036
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ADAM	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
254	[number]	k4	254
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
283	[number]	k4	283
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
IRGANG	IRGANG	kA	IRGANG
<g/>
,	,	kIx,	,
Winfried	Winfried	k1gInSc1	Winfried
<g/>
.	.	kIx.	.
</s>
<s>
Freudenthal	Freudenthal	k1gMnSc1	Freudenthal
als	als	k?	als
Herrschaft	Herrschaft	k1gInSc1	Herrschaft
des	des	k1gNnSc2	des
Deutschen	Deutschen	k2eAgInSc1d1	Deutschen
Ordens	Ordens	k1gInSc1	Ordens
1621	[number]	k4	1621
<g/>
-	-	kIx~	-
<g/>
1725	[number]	k4	1725
<g/>
.	.	kIx.	.
</s>
<s>
Bonn	Bonn	k1gInSc1	Bonn
;	;	kIx,	;
Bad	Bad	k1gMnSc1	Bad
Godesberg	Godesberg	k1gMnSc1	Godesberg
<g/>
:	:	kIx,	:
Verl	Verl	k1gMnSc1	Verl
<g/>
.	.	kIx.	.
</s>
<s>
Wissenschaftl	Wissenschaftnout	k5eAaPmAgMnS	Wissenschaftnout
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
276	[number]	k4	276
s.	s.	k?	s.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
;	;	kIx,	;
SKŘIVÁNEK	Skřivánek	k1gMnSc1	Skřivánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Synergon	Synergon	k1gInSc1	Synergon
;	;	kIx,	;
Klub	klub	k1gInSc1	klub
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
heraldiku	heraldika	k1gFnSc4	heraldika
a	a	k8xC	a
genealogii	genealogie	k1gFnSc4	genealogie
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
127	[number]	k4	127
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902448	[number]	k4	902448
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NICOLLE	NICOLLE	kA	NICOLLE
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
:	:	kIx,	:
1190	[number]	k4	1190
<g/>
-	-	kIx~	-
<g/>
1561	[number]	k4	1561
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
62	[number]	k4	62
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
2581	[number]	k4	2581
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TURNBULL	TURNBULL	kA	TURNBULL
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
:	:	kIx,	:
cihlové	cihlový	k2eAgInPc1d1	cihlový
hrady	hrad	k1gInPc1	hrad
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
1230	[number]	k4	1230
<g/>
-	-	kIx~	-
<g/>
1466	[number]	k4	1466
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2369	[number]	k4	2369
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZIEGLER	ZIEGLER	kA	ZIEGLER
<g/>
,	,	kIx,	,
Uwe	Uwe	k1gFnSc1	Uwe
<g/>
.	.	kIx.	.
</s>
<s>
Kreuz	Kreuz	k1gMnSc1	Kreuz
und	und	k?	und
Schwert	Schwert	k1gMnSc1	Schwert
:	:	kIx,	:
die	die	k?	die
Geschichte	Geschicht	k1gInSc5	Geschicht
des	des	k1gNnSc6	des
Deutschen	Deutschen	k2eAgInSc4d1	Deutschen
Ordens	Ordens	k1gInSc4	Ordens
<g/>
.	.	kIx.	.
</s>
<s>
Köln	Köln	k1gNnSc1	Köln
<g/>
:	:	kIx,	:
Böhlau	Böhlaus	k1gInSc2	Böhlaus
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
412	[number]	k4	412
<g/>
-	-	kIx~	-
<g/>
13402	[number]	k4	13402
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Velmistři	velmistr	k1gMnPc1	velmistr
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
</s>
</p>
<p>
<s>
Rytířské	rytířský	k2eAgInPc4d1	rytířský
řády	řád	k1gInPc4	řád
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
mečových	mečový	k2eAgMnPc2d1	mečový
bratří	bratr	k1gMnPc2	bratr
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
dobřínských	dobřínský	k2eAgMnPc2d1	dobřínský
rytířů	rytíř	k1gMnPc2	rytíř
</s>
</p>
<p>
<s>
Prusko	Prusko	k1gNnSc1	Prusko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
rytířský	rytířský	k2eAgInSc1d1	rytířský
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
</s>
</p>
<p>
<s>
Řádové	řádový	k2eAgFnPc1d1	řádová
insignie	insignie	k1gFnPc1	insignie
</s>
</p>
<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Stieber	Stieber	k1gMnSc1	Stieber
<g/>
:	:	kIx,	:
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
-	-	kIx~	-
dobré	dobrý	k2eAgNnSc1d1	dobré
zdání	zdání	k1gNnSc1	zdání
</s>
</p>
