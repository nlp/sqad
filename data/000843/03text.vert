<s>
Prix	Prix	k1gInSc1	Prix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Amérique	Amériqu	k1gMnSc2	Amériqu
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Cena	cena	k1gFnSc1	cena
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovinový	rovinový	k2eAgInSc1d1	rovinový
dostih	dostih	k1gInSc1	dostih
klusáků	klusák	k1gMnPc2	klusák
kategorie	kategorie	k1gFnSc2	kategorie
G	G	kA	G
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
dostihovém	dostihový	k2eAgNnSc6d1	dostihové
závodišti	závodiště	k1gNnSc6	závodiště
Vincennes	Vincennesa	k1gFnPc2	Vincennesa
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
poslední	poslední	k2eAgFnSc4d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
koně	kůň	k1gMnPc4	kůň
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4-10	[number]	k4	4-10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
dotován	dotovat	k5eAaBmNgInS	dotovat
výhrou	výhra	k1gFnSc7	výhra
milión	milión	k4xCgInSc1	milión
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
polovina	polovina	k1gFnSc1	polovina
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Prix	Prix	k1gInSc1	Prix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Amérique	Amérique	k1gFnSc1	Amérique
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c4	na
2700	[number]	k4	2700
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
1920	[number]	k4	1920
se	se	k3xPyFc4	se
běžel	běžet	k5eAaImAgInS	běžet
na	na	k7c4	na
2500	[number]	k4	2500
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
2625	[number]	k4	2625
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
variacemi	variace	k1gFnPc7	variace
plus	plus	k6eAd1	plus
nebo	nebo	k8xC	nebo
mínus	mínus	k6eAd1	mínus
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
zkušebně	zkušebně	k6eAd1	zkušebně
běželo	běžet	k5eAaImAgNnS	běžet
na	na	k7c4	na
2850	[number]	k4	2850
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946-1947	[number]	k4	1946-1947
na	na	k7c4	na
2800	[number]	k4	2800
m	m	kA	m
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
1984	[number]	k4	1984
na	na	k7c4	na
2600	[number]	k4	2600
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985-1993	[number]	k4	1985-1993
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
2650	[number]	k4	2650
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
Ready	ready	k0	ready
Cash	cash	k1gFnSc1	cash
(	(	kIx(	(
<g/>
Franck	Franck	k1gMnSc1	Franck
Nivard	Nivard	k1gMnSc1	Nivard
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
Oyonnax	Oyonnax	k1gInSc1	Oyonnax
(	(	kIx(	(
<g/>
Sebastien	Sebastien	k2eAgInSc1d1	Sebastien
Ernault	Ernault	k1gInSc1	Ernault
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Meaulnes	Meaulnes	k1gMnSc1	Meaulnes
du	du	k?	du
Corta	Corta	k1gMnSc1	Corta
(	(	kIx(	(
<g/>
Franck	Franck	k1gMnSc1	Franck
Nivard	Nivard	k1gMnSc1	Nivard
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
Offshore	Offshor	k1gInSc5	Offshor
Dream	Dream	k1gInSc4	Dream
(	(	kIx(	(
<g/>
Pierre	Pierr	k1gMnSc5	Pierr
Levesque	Levesquus	k1gMnSc5	Levesquus
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
Offshore	Offshor	k1gInSc5	Offshor
Dream	Dream	k1gInSc4	Dream
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Levesque	Levesquus	k1gMnSc5	Levesquus
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Gigant	gigant	k1gMnSc1	gigant
Neo	Neo	k1gMnSc1	Neo
(	(	kIx(	(
<g/>
Dominik	Dominik	k1gMnSc1	Dominik
Locqueneux	Locqueneux	k1gInSc1	Locqueneux
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
Jag	Jaga	k1gFnPc2	Jaga
de	de	k?	de
Bellouet	Bellouet	k1gMnSc1	Bellouet
(	(	kIx(	(
<g/>
Christophe	Christophe	k1gFnSc1	Christophe
Gallier	Gallier	k1gMnSc1	Gallier
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
Kesaco	Kesaco	k6eAd1	Kesaco
Phedo	Phedo	k1gNnSc1	Phedo
(	(	kIx(	(
<g/>
Jean-Michel	Jean-Michel	k1gInSc1	Jean-Michel
Bazire	Bazir	k1gInSc5	Bazir
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
Abano	Abano	k6eAd1	Abano
As	as	k9	as
(	(	kIx(	(
<g/>
Jos	Jos	k1gMnSc1	Jos
Verbeeck	Verbeeck	k1gMnSc1	Verbeeck
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Varenne	Varenn	k1gInSc5	Varenn
(	(	kIx(	(
<g/>
Giampaolo	Giampaola	k1gFnSc5	Giampaola
Minnucci	Minnucce	k1gMnSc6	Minnucce
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
Varenne	Varennout	k5eAaPmIp3nS	Varennout
(	(	kIx(	(
<g/>
Giampaolo	Giampaola	k1gFnSc5	Giampaola
Minnucci	Minnucce	k1gMnSc6	Minnucce
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Général	Général	k1gFnSc2	Général
du	du	k?	du
Pommeau	Pommeaus	k1gInSc2	Pommeaus
(	(	kIx(	(
<g/>
Jules	Jules	k1gMnSc1	Jules
Lepennetier	Lepennetier	k1gMnSc1	Lepennetier
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Moni	Moni	k?	Moni
Maker	makro	k1gNnPc2	makro
(	(	kIx(	(
<g/>
Jean-Michel	Jean-Michel	k1gInSc1	Jean-Michel
Bazire	Bazir	k1gInSc5	Bazir
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
Dryade	Dryad	k1gInSc5	Dryad
des	des	k1gNnPc6	des
Bois	Bois	k1gInSc4	Bois
(	(	kIx(	(
<g/>
Jos	Jos	k1gMnSc1	Jos
Verbeeck	Verbeeck	k1gMnSc1	Verbeeck
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
Abo	Abo	k1gMnSc1	Abo
Volo	Volo	k1gMnSc1	Volo
(	(	kIx(	(
<g/>
Jos	Jos	k1gMnSc1	Jos
Verbeeck	Verbeeck	k1gMnSc1	Verbeeck
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
Cocktail	cocktail	k1gInSc1	cocktail
Jet	jet	k2eAgInSc1d1	jet
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Jean-Etienne	Jean-Etiennout	k5eAaPmIp3nS	Jean-Etiennout
Dubois	Dubois	k1gFnSc1	Dubois
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
Ina	Ina	k1gMnSc1	Ina
Scot	Scot	k1gMnSc1	Scot
(	(	kIx(	(
<g/>
Helen	Helena	k1gFnPc2	Helena
Johansson	Johansson	k1gInSc1	Johansson
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
Sea	Sea	k1gFnSc1	Sea
Cove	Cove	k1gFnSc1	Cove
(	(	kIx(	(
<g/>
Jos	Jos	k1gMnSc1	Jos
Verbeeck	Verbeeck	k1gMnSc1	Verbeeck
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
Queen	Queen	k1gInSc1	Queen
L.	L.	kA	L.
(	(	kIx(	(
<g/>
Stig	Stig	k1gMnSc1	Stig
H.	H.	kA	H.
Johansson	Johansson	k1gMnSc1	Johansson
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Verdict	Verdictum	k1gNnPc2	Verdictum
Gédé	Gédá	k1gFnSc2	Gédá
(	(	kIx(	(
<g/>
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Hallais	Hallais	k1gFnSc7	Hallais
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
Ténor	Ténor	k1gMnSc1	Ténor
de	de	k?	de
Baune	Baun	k1gInSc5	Baun
(	(	kIx(	(
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Bossuet	Bossueta	k1gFnPc2	Bossueta
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1990	[number]	k4	1990
Ourasi	Ouras	k1gMnPc1	Ouras
(	(	kIx(	(
<g/>
Michel-Marcel	Michel-Marcel	k1gMnSc1	Michel-Marcel
Gougeon	Gougeon	k1gMnSc1	Gougeon
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
Queila	Queila	k1gFnSc1	Queila
Gédé	Gédá	k1gFnSc2	Gédá
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Baudron	Baudron	k1gMnSc1	Baudron
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
Ourasi	Ouras	k1gMnPc1	Ouras
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc4d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc4	Gougeon
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
Ourasi	Ouras	k1gMnPc1	Ouras
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc4d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc4	Gougeon
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
Ourasi	Ouras	k1gMnPc1	Ouras
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc4d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc4	Gougeon
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
Lutin	Lutin	k1gMnSc1	Lutin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Isigny	Isigna	k1gMnSc2	Isigna
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
André	André	k1gMnPc2	André
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
Lurabo	Luraba	k1gMnSc5	Luraba
(	(	kIx(	(
<g/>
Michel-Marcel	Michel-Marcel	k1gMnSc6	Michel-Marcel
Gougeon	Gougeon	k1gNnSc1	Gougeon
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
Idéal	Idéal	k1gMnSc1	Idéal
du	du	k?	du
Gazeau	Gazeaa	k1gMnSc4	Gazeaa
(	(	kIx(	(
<g/>
Eugè	Eugè	k1gMnSc4	Eugè
Lefè	Lefè	k1gMnSc4	Lefè
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
Hymour	Hymoura	k1gFnPc2	Hymoura
(	(	kIx(	(
<g/>
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Dubois	Dubois	k1gFnSc7	Dubois
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
Idéal	Idéal	k1gMnSc1	Idéal
du	du	k?	du
Gazeau	Gazeaa	k1gMnSc4	Gazeaa
(	(	kIx(	(
<g/>
Eugè	Eugè	k1gMnSc4	Eugè
Lefè	Lefè	k1gMnSc4	Lefè
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
Eléazar	Eléazar	k1gInSc1	Eléazar
(	(	kIx(	(
<g/>
Léopold	Léopold	k1gInSc1	Léopold
Verroken	Verroken	k1gInSc1	Verroken
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
<g />
.	.	kIx.	.
</s>
<s>
High	High	k1gInSc1	High
Echelon	Echelon	k1gInSc1	Echelon
(	(	kIx(	(
<g/>
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Dubois	Dubois	k1gFnSc7	Dubois
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Grandpré	Grandprý	k2eAgFnSc2d1	Grandprý
(	(	kIx(	(
<g/>
Pierre-Désiré	Pierre-Désirý	k2eAgFnSc2d1	Pierre-Désirý
Allaire	Allair	k1gInSc5	Allair
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
Bellino	Bellin	k2eAgNnSc1d1	Bellino
II	II	kA	II
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc1d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc1	Gougeon
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Bellino	Bellin	k2eAgNnSc1d1	Bellino
II	II	kA	II
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc1d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc1	Gougeon
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
Bellino	Bellin	k2eAgNnSc1d1	Bellino
II	II	kA	II
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc1d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc1	Gougeon
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
Delmonica	Delmonica	k1gMnSc1	Delmonica
Hanover	Hanover	k1gMnSc1	Hanover
(	(	kIx(	(
<g/>
F.	F.	kA	F.
Frömming	Frömming	k1gInSc1	Frömming
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1973	[number]	k4	1973
Dart	Dart	k1gMnSc1	Dart
Hanover	Hanover	k1gMnSc1	Hanover
(	(	kIx(	(
<g/>
Berndt	Berndt	k1gMnSc1	Berndt
Lindtstedt	Lindtstedt	k1gMnSc1	Lindtstedt
<g/>
)	)	kIx)	)
1972	[number]	k4	1972
Tidalium	Tidalium	k1gNnSc1	Tidalium
Pélo	Pélo	k6eAd1	Pélo
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Mary	Mary	k1gFnSc2	Mary
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
Tidalium	Tidalium	k1gNnSc1	Tidalium
Pélo	Pélo	k6eAd1	Pélo
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Mary	Mary	k1gFnSc2	Mary
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
Toscan	Toscan	k1gMnSc1	Toscan
(	(	kIx(	(
<g/>
Michel-Marcel	Michel-Marcel	k1gMnSc1	Michel-Marcel
Gougeon	Gougeon	k1gMnSc1	Gougeon
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
Upsalin	Upsalin	k1gInSc1	Upsalin
(	(	kIx(	(
<g/>
Louis	Louis	k1gMnSc1	Louis
Sauvé	Sauvá	k1gFnSc2	Sauvá
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
Roquépine	Roquépin	k1gInSc5	Roquépin
(	(	kIx(	(
<g/>
Jean-René	Jean-Rená	k1gFnSc3	Jean-Rená
<g />
.	.	kIx.	.
</s>
<s>
Gougeon	Gougeon	k1gNnSc1	Gougeon
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
Roquépine	Roquépin	k1gInSc5	Roquépin
(	(	kIx(	(
<g/>
Henri	Henri	k1gNnSc3	Henri
Lévesque	Lévesque	k1gNnSc3	Lévesque
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
Roquépine	Roquépin	k1gInSc5	Roquépin
(	(	kIx(	(
<g/>
Jean-René	Jean-Rený	k2eAgNnSc4d1	Jean-Rený
Gougeon	Gougeon	k1gNnSc4	Gougeon
<g/>
)	)	kIx)	)
1965	[number]	k4	1965
Ozo	Ozo	k1gMnSc1	Ozo
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Frömming	Frömming	k1gInSc1	Frömming
<g/>
)	)	kIx)	)
1964	[number]	k4	1964
Nike	Nike	k1gFnPc2	Nike
Hanover	Hanover	k1gMnSc1	Hanover
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Frömming	Frömming	k1gInSc1	Frömming
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
Ozo	Ozo	k1gFnPc2	Ozo
(	(	kIx(	(
<g/>
Roger	Roger	k1gInSc1	Roger
Massue	Massu	k1gInSc2	Massu
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
Newstar	Newstar	k1gMnSc1	Newstar
(	(	kIx(	(
<g/>
Walter	Walter	k1gMnSc1	Walter
Baroncini	Baroncin	k1gMnPc1	Baroncin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1961	[number]	k4	1961
Masina	Masino	k1gNnSc2	Masino
(	(	kIx(	(
<g/>
François	François	k1gFnSc1	François
Brohier	Brohier	k1gMnSc1	Brohier
<g/>
)	)	kIx)	)
1960	[number]	k4	1960
Hairos	Hairosa	k1gFnPc2	Hairosa
II	II	kA	II
(	(	kIx(	(
<g/>
Willem	Willo	k1gNnSc7	Willo
Geersen	Geersen	k2eAgMnSc1d1	Geersen
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
Jamin	Jamin	k1gMnSc1	Jamin
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Riaud	Riaud	k1gMnSc1	Riaud
<g/>
)	)	kIx)	)
1958	[number]	k4	1958
Jamin	Jamin	k1gMnSc1	Jamin
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Riaud	Riaud	k1gMnSc1	Riaud
<g/>
)	)	kIx)	)
1957	[number]	k4	1957
Gélinotte	Gélinott	k1gInSc5	Gélinott
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Céran-Maillard	Céran-Maillard	k1gMnSc1	Céran-Maillard
<g/>
)	)	kIx)	)
1956	[number]	k4	1956
Gélinotte	Gélinott	k1gInSc5	Gélinott
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Céran-Maillard	Céran-Maillard	k1gMnSc1	Céran-Maillard
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1955	[number]	k4	1955
Fortunato	Fortunat	k2eAgNnSc1d1	Fortunato
II	II	kA	II
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Céran-Maillard	Céran-Maillard	k1gMnSc1	Céran-Maillard
<g/>
)	)	kIx)	)
1954	[number]	k4	1954
Feu	Feu	k1gFnSc1	Feu
Follet	Follet	k1gInSc1	Follet
X	X	kA	X
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Riaud	Riaud	k1gMnSc1	Riaud
<g/>
)	)	kIx)	)
1953	[number]	k4	1953
Permit	Permit	k1gInSc1	Permit
(	(	kIx(	(
<g/>
Walter	Walter	k1gMnSc1	Walter
Heitmann	Heitmann	k1gMnSc1	Heitmann
<g/>
)	)	kIx)	)
1952	[number]	k4	1952
Cancanniè	Cancanniè	k1gMnSc1	Cancanniè
(	(	kIx(	(
<g/>
Jonel	Jonel	k1gMnSc1	Jonel
Chryriacos	Chryriacos	k1gMnSc1	Chryriacos
<g/>
)	)	kIx)	)
1951	[number]	k4	1951
Mighty	Mighta	k1gMnSc2	Mighta
Ned	Ned	k1gMnSc2	Ned
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
Scotch	Scotch	k1gInSc1	Scotch
Fez	fez	k1gInSc4	fez
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Sören	Sören	k1gInSc1	Sören
Nordin	Nordina	k1gFnPc2	Nordina
<g/>
)	)	kIx)	)
1949	[number]	k4	1949
Venutar	Venutara	k1gFnPc2	Venutara
(	(	kIx(	(
<g/>
F.	F.	kA	F.
Réaud	Réaud	k1gMnSc1	Réaud
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
Mighty	Mighta	k1gMnSc2	Mighta
Ned	Ned	k1gMnSc2	Ned
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Antonellini	Antonellin	k2eAgMnPc1d1	Antonellin
<g/>
)	)	kIx)	)
1947	[number]	k4	1947
Mistero	Mistero	k1gNnSc1	Mistero
(	(	kIx(	(
<g/>
Romolo	Romola	k1gFnSc5	Romola
Ossani	Ossaň	k1gFnSc6	Ossaň
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Céran-Maillard	Céran-Maillard	k1gMnSc1	Céran-Maillard
<g/>
)	)	kIx)	)
1945	[number]	k4	1945
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Céran-Maillard	Céran-Maillard	k1gMnSc1	Céran-Maillard
<g/>
)	)	kIx)	)
1944	[number]	k4	1944
Profane	Profan	k1gMnSc5	Profan
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
A.	A.	kA	A.
Sourroubille	Sourroubille	k1gFnSc1	Sourroubille
<g/>
)	)	kIx)	)
1943	[number]	k4	1943
Nebuleuse	Nebuleuse	k1gFnSc2	Nebuleuse
V	V	kA	V
(	(	kIx(	(
<g/>
R.	R.	kA	R.
Simonard	Simonard	k1gMnSc1	Simonard
<g/>
)	)	kIx)	)
1942	[number]	k4	1942
Neulisse	Neulisse	k1gFnSc1	Neulisse
(	(	kIx(	(
<g/>
C.	C.	kA	C.
Domergue	Domergue	k1gFnSc6	Domergue
<g/>
)	)	kIx)	)
1941	[number]	k4	1941
Závoz	závoz	k1gInSc1	závoz
zrušen	zrušen	k2eAgInSc1d1	zrušen
1940	[number]	k4	1940
Závod	závod	k1gInSc1	závod
zrušen	zrušen	k2eAgInSc1d1	zrušen
1939	[number]	k4	1939
De	De	k?	De
Sota	Sotus	k1gMnSc4	Sotus
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
De	De	k?	De
Sota	Sotus	k1gMnSc4	Sotus
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1937	[number]	k4	1937
Muscletone	Muscleton	k1gInSc5	Muscleton
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1936	[number]	k4	1936
Javari	Javari	k1gNnPc2	Javari
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Perlbag	Perlbag	k1gMnSc1	Perlbag
<g/>
)	)	kIx)	)
1935	[number]	k4	1935
Muscletone	Muscleton	k1gInSc5	Muscleton
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1934	[number]	k4	1934
Walter	Walter	k1gMnSc1	Walter
Dear	Dear	k1gMnSc1	Dear
(	(	kIx(	(
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Mills	Mills	k1gInSc1	Mills
<g/>
)	)	kIx)	)
1933	[number]	k4	1933
Amazone	Amazon	k1gInSc5	Amazon
B	B	kA	B
(	(	kIx(	(
<g/>
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
</s>
<s>
Vanlandeghem	Vanlandegh	k1gInSc7	Vanlandegh
<g/>
)	)	kIx)	)
1932	[number]	k4	1932
Hazleton	Hazleton	k1gInSc1	Hazleton
(	(	kIx(	(
<g/>
Otto	Otto	k1gMnSc1	Otto
Dieffenbacher	Dieffenbachra	k1gFnPc2	Dieffenbachra
<g/>
)	)	kIx)	)
1931	[number]	k4	1931
Hazleton	Hazleton	k1gInSc1	Hazleton
(	(	kIx(	(
<g/>
Otto	Otto	k1gMnSc1	Otto
Dieffenbacher	Dieffenbachra	k1gFnPc2	Dieffenbachra
<g/>
)	)	kIx)	)
1930	[number]	k4	1930
Amazone	Amazon	k1gInSc5	Amazon
B	B	kA	B
(	(	kIx(	(
<g/>
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
</s>
<s>
Vanlandeghem	Vanlandegh	k1gInSc7	Vanlandegh
<g/>
)	)	kIx)	)
1929	[number]	k4	1929
Templier	Templiero	k1gNnPc2	Templiero
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Butti	Butť	k1gFnSc6	Butť
<g/>
)	)	kIx)	)
1928	[number]	k4	1928
Uranie	Uranie	k1gFnSc1	Uranie
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Capovilla	Capovilla	k1gFnSc1	Capovilla
<g/>
)	)	kIx)	)
1927	[number]	k4	1927
Uranie	Uranie	k1gFnSc1	Uranie
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Capovilla	Capovilla	k1gFnSc1	Capovilla
<g/>
)	)	kIx)	)
1926	[number]	k4	1926
Uranie	Uranie	k1gFnSc1	Uranie
(	(	kIx(	(
<g/>
V.	V.	kA	V.
Capovilla	Capovilla	k1gFnSc1	Capovilla
<g/>
)	)	kIx)	)
1925	[number]	k4	1925
Re	re	k9	re
Mac	Mac	kA	Mac
Gregor	Gregor	k1gMnSc1	Gregor
(	(	kIx(	(
<g/>
C.	C.	kA	C.
Dessauze	Dessauha	k1gFnSc6	Dessauha
<g/>
)	)	kIx)	)
1924	[number]	k4	1924
Passeport	Passeport	k1gInSc1	Passeport
(	(	kIx(	(
<g/>
Alexandre	Alexandr	k1gInSc5	Alexandr
Finn	Finn	k1gMnSc1	Finn
<g/>
)	)	kIx)	)
1923	[number]	k4	1923
Passeport	Passeport	k1gInSc1	Passeport
(	(	kIx(	(
<g/>
P.	P.	kA	P.
Viel	Viel	k1gMnSc1	Viel
<g/>
)	)	kIx)	)
1922	[number]	k4	1922
Reynolds	Reynolds	k1gInSc1	Reynolds
V	V	kA	V
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Gougeon	Gougeon	k1gNnSc4	Gougeon
<g/>
)	)	kIx)	)
1921	[number]	k4	1921
Pro	pro	k7c4	pro
Patria	Patrium	k1gNnPc4	Patrium
(	(	kIx(	(
<g/>
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
</s>
<s>
Monsieur	Monsieur	k1gMnSc1	Monsieur
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
Pro	pro	k7c4	pro
Patria	Patrium	k1gNnPc4	Patrium
(	(	kIx(	(
<g/>
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
</s>
<s>
Monsieur	Monsieur	k1gMnSc1	Monsieur
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Prix	Prix	k1gInSc1	Prix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Amérique	Amérique	k1gInSc1	Amérique
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
