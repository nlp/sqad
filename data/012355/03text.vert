<p>
<s>
Horkýže	Horkýže	k6eAd1	Horkýže
Slíže	slízat	k5eAaPmIp3nS	slízat
je	on	k3xPp3gFnPc4	on
slovenská	slovenský	k2eAgFnSc1d1	slovenská
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Nitry	Nitra	k1gFnSc2	Nitra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
získala	získat	k5eAaPmAgFnS	získat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
punková	punkový	k2eAgFnSc1d1	punková
<g/>
,	,	kIx,	,
punk	punk	k1gInSc1	punk
rocková	rockový	k2eAgFnSc1d1	rocková
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
rocková	rockový	k2eAgFnSc1d1	rocková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Hrivňák	Hrivňák	k1gMnSc1	Hrivňák
–	–	k?	–
Kuko	Kuka	k1gMnSc5	Kuka
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Mário	Mário	k1gMnSc1	Mário
Sabo	Sabo	k1gMnSc1	Sabo
–	–	k?	–
Sabotér	sabotér	k1gMnSc1	sabotér
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Štefánik	Štefánik	k1gMnSc1	Štefánik
–	–	k?	–
Doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Viršík	Viršík	k1gMnSc1	Viršík
–	–	k?	–
Vandel	Vandlo	k1gNnPc2	Vandlo
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Claude	Claud	k1gInSc5	Claud
Vandel	Vandlo	k1gNnPc2	Vandlo
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc1d1	bicí
</s>
</p>
<p>
<s>
Veronika	Veronika	k1gFnSc1	Veronika
Smetanová	Smetanová	k1gFnSc1	Smetanová
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Košovan	Košovan	k1gMnSc1	Košovan
–	–	k?	–
Košo	Košo	k1gMnSc1	Košo
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgMnSc1d1	bicí
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Žiak	Žiaka	k1gFnPc2	Žiaka
–	–	k?	–
Apíčko	Apíčko	k1gNnSc1	Apíčko
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
Noro	Nora	k1gFnSc5	Nora
Ivančík	Ivančík	k1gMnSc1	Ivančík
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oného	oné	k1gMnSc2	oné
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vo	Vo	k?	Vo
štvorici	štvorik	k1gMnPc1	štvorik
po	po	k7c6	po
opici	opice	k1gFnSc6	opice
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ja	Ja	k?	Ja
chaču	chaču	k5eAaPmIp1nS	chaču
tebja	tebja	k1gFnSc1	tebja
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
Chorobná	chorobný	k2eAgFnSc1d1	chorobná
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kýže	Kýže	k1gInSc1	Kýže
sliz	sliz	k1gInSc1	sliz
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alibaba	Alibaba	k1gFnSc1	Alibaba
a	a	k8xC	a
40	[number]	k4	40
krátkych	krátkych	k1gInSc1	krátkych
songov	songov	k1gInSc4	songov
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ritero	Ritero	k1gNnSc1	Ritero
Xaperle	Xaperl	k1gMnSc2	Xaperl
Bax	Bax	k1gMnSc2	Bax
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ukáž	Ukáž	k6eAd1	Ukáž
tú	tú	k0	tú
tvoju	tvoju	k?	tvoju
ZOO	zoo	k1gNnPc1	zoo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
54	[number]	k4	54
dole	dole	k6eAd1	dole
hlavou	hlava	k1gFnSc7	hlava
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
St.	st.	kA	st.
Mary	Mary	k1gFnSc1	Mary
Huana	Huana	k1gFnSc1	Huana
Ganja	Ganja	k1gFnSc1	Ganja
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pustite	Pustit	k1gInSc5	Pustit
Karola	Karola	k1gFnSc1	Karola
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Živák	Živák	k1gMnSc1	Živák
(	(	kIx(	(
<g/>
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wanted	Wanted	k1gMnSc1	Wanted
Dead	Dead	k1gMnSc1	Dead
or	or	k?	or
Alive	Aliev	k1gFnSc2	Aliev
(	(	kIx(	(
<g/>
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
Best	Best	k1gMnSc1	Best
uff	uff	k?	uff
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
dobrej	dobrej	k?	dobrej
viere	vier	k1gInSc5	vier
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dema	Demum	k1gNnSc2	Demum
===	===	k?	===
</s>
</p>
<p>
<s>
Prvý	prvý	k4xOgMnSc1	prvý
slíž	slíž	k1gMnSc1	slíž
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Smaragdové	smaragdový	k2eAgNnSc1d1	Smaragdové
oči	oko	k1gNnPc1	oko
</s>
</p>
<p>
<s>
Kontiňju	Kontiňju	k5eAaPmIp1nS	Kontiňju
...	...	k?	...
<g/>
Pokračujeme	pokračovat	k5eAaImIp1nP	pokračovat
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Horkýže	Horkýž	k1gFnSc2	Horkýž
Slíže	slízat	k5eAaPmIp3nS	slízat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Horkýže	Horkýž	k1gFnSc2	Horkýž
Slíže	slíž	k1gMnPc4	slíž
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Horkýže	Horkýže	k6eAd1	Horkýže
Slíže	slízat	k5eAaPmIp3nS	slízat
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Horkýže	Horkýže	k6eAd1	Horkýže
Slíže	slízat	k5eAaPmIp3nS	slízat
na	na	k7c4	na
Google	Google	k1gInSc4	Google
<g/>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Horkýže	Horkýže	k6eAd1	Horkýže
Slíže	slízat	k5eAaPmIp3nS	slízat
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Horkýže	Horkýzat	k5eAaPmIp3nS	Horkýzat
Slíže	slíž	k1gMnSc2	slíž
–	–	k?	–
český	český	k2eAgInSc4d1	český
fan	fana	k1gFnPc2	fana
web	web	k1gInSc4	web
</s>
</p>
<p>
<s>
Horkýže	Horkýže	k6eAd1	Horkýže
Slíže	slízat	k5eAaPmIp3nS	slízat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
<g/>
.	.	kIx.	.
</s>
<s>
Přijali	přijmout	k5eAaPmAgMnP	přijmout
novou	nový	k2eAgFnSc4d1	nová
baskytaristku	baskytaristka	k1gFnSc4	baskytaristka
</s>
</p>
