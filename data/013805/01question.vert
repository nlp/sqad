<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
výška	výška	k1gFnSc1
značně	značně	k6eAd1
převyšuje	převyšovat	k5eAaImIp3nS
její	její	k3xOp3gInPc4
půdorysné	půdorysný	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
<g/>
?	?	kIx.
</s>