<s>
Věž	věž	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
stavbě	stavba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Věž	věž	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
CN	CN	kA
Tower	Tower	k1gMnSc1
v	v	k7c6
Torontu	Toronto	k1gNnSc6
</s>
<s>
Věž	věž	k1gFnSc1
je	být	k5eAaImIp3nS
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
výška	výška	k1gFnSc1
značně	značně	k6eAd1
převyšuje	převyšovat	k5eAaImIp3nS
půdorysné	půdorysný	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
naprosté	naprostý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
různě	různě	k6eAd1
členěný	členěný	k2eAgInSc4d1
a	a	k8xC
vybavený	vybavený	k2eAgInSc4d1
funkční	funkční	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
však	však	k9
bývá	bývat	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
starověku	starověk	k1gInSc6
a	a	k8xC
středověku	středověk	k1gInSc2
se	se	k3xPyFc4
stavěla	stavět	k5eAaImAgFnS
zpravidla	zpravidla	k6eAd1
kvůli	kvůli	k7c3
opevňovacím	opevňovací	k2eAgInPc3d1
a	a	k8xC
strážním	strážní	k2eAgInPc3d1
úkolům	úkol	k1gInPc3
(	(	kIx(
<g/>
dobře	dobře	k6eAd1
se	se	k3xPyFc4
bránila	bránit	k5eAaImAgFnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
z	z	k7c2
ní	on	k3xPp3gFnSc2
daleko	daleko	k6eAd1
vidět	vidět	k5eAaImF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
prosazovat	prosazovat	k5eAaImF
i	i	k9
další	další	k2eAgInPc4d1
důvody	důvod	k1gInPc4
stavby	stavba	k1gFnSc2
(	(	kIx(
<g/>
prestiž	prestiž	k1gFnSc1
či	či	k8xC
šetření	šetření	k1gNnSc1
místem	místem	k6eAd1
v	v	k7c6
místech	místo	k1gNnPc6
s	s	k7c7
velice	velice	k6eAd1
drahými	drahý	k2eAgInPc7d1
pozemky	pozemek	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
obranu	obrana	k1gFnSc4
ve	v	k7c6
středověku	středověk	k1gInSc6
byly	být	k5eAaImAgFnP
nejdříve	dříve	k6eAd3
stavěny	stavěn	k2eAgFnPc1d1
věže	věž	k1gFnPc1
s	s	k7c7
hranatým	hranatý	k2eAgInSc7d1
půdorysem	půdorys	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
obranu	obrana	k1gFnSc4
jsou	být	k5eAaImIp3nP
lepší	dobrý	k2eAgFnPc1d2
věže	věž	k1gFnPc1
s	s	k7c7
kruhovou	kruhový	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
budovat	budovat	k5eAaImF
převážně	převážně	k6eAd1
tyto	tento	k3xDgFnPc4
věže	věž	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostelní	kostelní	k2eAgFnPc1d1
věže	věž	k1gFnPc1
a	a	k8xC
sakrální	sakrální	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
</s>
<s>
Velmi	velmi	k6eAd1
rozšířenými	rozšířený	k2eAgInPc7d1
typy	typ	k1gInPc7
věží	věž	k1gFnPc2
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
zde	zde	k6eAd1
zejména	zejména	k9
v	v	k7c6
historických	historický	k2eAgNnPc6d1
jádrech	jádro	k1gNnPc6
obcí	obec	k1gFnPc2
a	a	k8xC
měst	město	k1gNnPc2
<g/>
,	,	kIx,
kostelní	kostelní	k2eAgFnPc1d1
věže	věž	k1gFnPc1
a	a	k8xC
věže	věž	k1gFnPc1
dalších	další	k2eAgFnPc2d1
sakrálních	sakrální	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
klášterů	klášter	k1gInPc2
<g/>
)	)	kIx)
případně	případně	k6eAd1
samostatné	samostatný	k2eAgFnPc4d1
zvonice	zvonice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
zvonů	zvon	k1gInPc2
tyto	tento	k3xDgFnPc1
věže	věž	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
opatřeny	opatřen	k2eAgInPc1d1
i	i	k9
věžními	věžní	k2eAgFnPc7d1
hodinami	hodina	k1gFnPc7
a	a	k8xC
pozorovacím	pozorovací	k2eAgInSc7d1
ochozem	ochoz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Rozhledny	rozhledna	k1gFnPc1
a	a	k8xC
telekomunikační	telekomunikační	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
</s>
<s>
Žižkovský	žižkovský	k2eAgInSc1d1
vysílač	vysílač	k1gInSc1
od	od	k7c2
západu	západ	k1gInSc2
z	z	k7c2
Petřínské	petřínský	k2eAgFnSc2d1
rozhledny	rozhledna	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
pozadí	pozadí	k1gNnSc6
vysílač	vysílač	k1gMnSc1
Liblice	Liblika	k1gFnSc3
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Zejména	zejména	k9
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
romantismu	romantismus	k1gInSc2
a	a	k8xC
turistiky	turistika	k1gFnSc2
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
budovat	budovat	k5eAaImF
rozhledny	rozhledna	k1gFnPc1
<g/>
,	,	kIx,
od	od	k7c2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
též	tenž	k3xDgFnSc2
telekomunikační	telekomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
například	například	k6eAd1
jako	jako	k8xC,k8xS
vysílače	vysílač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průmyslové	průmyslový	k2eAgFnPc1d1
věže	věž	k1gFnPc1
</s>
<s>
Speciálními	speciální	k2eAgInPc7d1
typy	typ	k1gInPc7
průmyslových	průmyslový	k2eAgFnPc2d1
věží	věž	k1gFnPc2
jsou	být	k5eAaImIp3nP
těžní	těžní	k2eAgFnPc1d1
věže	věž	k1gFnPc1
hlubinných	hlubinný	k2eAgInPc2d1
dolů	dol	k1gInPc2
nebo	nebo	k8xC
chladicí	chladicí	k2eAgFnPc1d1
věže	věž	k1gFnPc1
u	u	k7c2
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářským	hospodářský	k2eAgInPc3d1
účelům	účel	k1gInPc3
obvykle	obvykle	k6eAd1
slouží	sloužit	k5eAaImIp3nS
také	také	k9
vodárenské	vodárenský	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1
typy	typ	k1gInPc1
</s>
<s>
Mezi	mezi	k7c4
speciální	speciální	k2eAgInPc4d1
typy	typ	k1gInPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
dobývací	dobývací	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
speciální	speciální	k2eAgInSc1d1
pohyblivý	pohyblivý	k2eAgInSc1d1
obléhací	obléhací	k2eAgInSc1d1
stroj	stroj	k1gInSc1
užívaný	užívaný	k2eAgInSc1d1
při	při	k7c6
obléhání	obléhání	k1gNnSc6
hradů	hrad	k1gInPc2
a	a	k8xC
pevností	pevnost	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KINDERSLEY	KINDERSLEY	kA
<g/>
,	,	kIx,
Dorling	Dorling	k1gInSc1
<g/>
.	.	kIx.
1001	#num#	k4
otázka	otázka	k1gFnSc1
a	a	k8xC
odpověď	odpověď	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
TIMY	TIMY	kA
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88799	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
a	a	k8xC
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
věží	věž	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
televizních	televizní	k2eAgFnPc2d1
věží	věž	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Towers	Towersa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
věž	věž	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kostelní	kostelní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Kostelní	kostelní	k2eAgFnSc2d1
typy	typa	k1gFnSc2
</s>
<s>
Bazilika	bazilika	k1gFnSc1
•	•	k?
Gotická	gotický	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
•	•	k?
Halový	halový	k2eAgInSc1d1
kostel	kostel	k1gInSc1
•	•	k?
Pseudobazilika	Pseudobazilika	k1gFnSc1
•	•	k?
Rotunda	rotunda	k1gFnSc1
Lodě	loď	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
druhy	druh	k1gInPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
•	•	k?
Boční	boční	k2eAgFnSc1d1
loď	loď	k1gFnSc1
•	•	k?
Transept	transept	k1gInSc1
•	•	k?
Křížení	křížení	k1gNnSc2
•	•	k?
Dvojlodí	Dvojlodí	k1gNnSc2
•	•	k?
Trojlodí	trojlodí	k1gNnSc2
•	•	k?
Vícelodní	vícelodní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Prostory	prostora	k1gFnSc2
kostela	kostel	k1gInSc2
</s>
<s>
Apsida	apsida	k1gFnSc1
•	•	k?
Apsidová	apsidový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
•	•	k?
Empora	empora	k1gFnSc1
•	•	k?
Chór	chór	k1gInSc1
•	•	k?
Chórový	chórový	k2eAgInSc1d1
ochoz	ochoz	k1gInSc1
•	•	k?
Kaple	kaple	k1gFnSc1
•	•	k?
Kněžiště	kněžiště	k1gNnSc2
•	•	k?
Kruchta	kruchta	k1gFnSc1
•	•	k?
Krypta	krypta	k1gFnSc1
•	•	k?
Sakristie	sakristie	k1gFnSc2
•	•	k?
Věž	věž	k1gFnSc4
Jiné	jiný	k2eAgInPc4d1
architektonické	architektonický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
</s>
<s>
Ambon	ambon	k1gInSc1
•	•	k?
Baldachýn	baldachýn	k1gInSc1
•	•	k?
Fiála	fiála	k1gFnSc1
•	•	k?
Chrlič	chrlič	k1gInSc1
•	•	k?
Ikonostas	ikonostas	k1gInSc1
•	•	k?
Kazatelna	kazatelna	k1gFnSc1
•	•	k?
Klenba	klenba	k1gFnSc1
•	•	k?
Krab	krab	k1gInSc1
•	•	k?
Kružba	kružba	k1gFnSc1
•	•	k?
Křížová	Křížová	k1gFnSc1
kytka	kytka	k1gFnSc1
•	•	k?
Kupole	kupole	k1gFnSc1
•	•	k?
Lektorium	Lektorium	k1gNnSc1
•	•	k?
Lucerna	lucerna	k1gFnSc1
•	•	k?
Okulus	Okulus	k1gInSc1
•	•	k?
Oltář	Oltář	k1gInSc1
•	•	k?
Oltářní	oltářní	k2eAgFnSc1d1
mřížka	mřížka	k1gFnSc1
•	•	k?
Opěrný	opěrný	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Portál	portál	k1gInSc1
•	•	k?
Portikus	portikus	k1gInSc1
•	•	k?
Přípora	přípora	k1gFnSc1
•	•	k?
Rozeta	rozeta	k1gFnSc1
•	•	k?
Sanktusník	sanktusník	k1gInSc1
•	•	k?
Triforium	triforium	k1gNnSc4
•	•	k?
Tympanon	tympanon	k1gInSc1
•	•	k?
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
•	•	k?
Vitráž	vitráž	k1gFnSc1
•	•	k?
Výklenek	výklenek	k1gInSc1
(	(	kIx(
<g/>
Nika	nika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Žebro	žebro	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4203577-6	4203577-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
196	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85136296	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85136296	#num#	k4
</s>
