<s>
Saussurovo	Saussurův	k2eAgNnSc1d1	Saussurovo
nejvlivnější	vlivný	k2eAgNnSc1d3	nejvlivnější
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Kurs	kurs	k1gInSc1	kurs
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
(	(	kIx(	(
<g/>
Course	Course	k1gFnSc1	Course
de	de	k?	de
linguistique	linguistiquat	k5eAaPmIp3nS	linguistiquat
générale	générale	k6eAd1	générale
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1989	[number]	k4	1989
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Františka	František	k1gMnSc2	František
Čermáka	Čermák	k1gMnSc2	Čermák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
