<s>
Ikonodulie	ikonodulie	k1gFnSc1
či	či	k8xC
ikonofilie	ikonofilie	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ε	ε	kIx~
eikón	eikón	k1gInSc1
obraz	obraz	k1gInSc1
+	+	kIx~
δ	δ	kIx~
dúleia	dúleia	k1gFnSc1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
označující	označující	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
uctívání	uctívání	k1gNnSc2
obrazů	obraz	k1gInPc2
svatých	svatá	k1gFnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
ikon	ikon	k1gFnPc2
<g/>
.	.	kIx.
</s>