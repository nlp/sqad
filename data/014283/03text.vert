<s>
Ikonodulie	ikonodulie	k1gFnSc1
</s>
<s>
Ikonodulie	ikonodulie	k1gFnSc1
či	či	k8xC
ikonofilie	ikonofilie	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
ε	ε	kIx~
eikón	eikón	k1gInSc1
obraz	obraz	k1gInSc1
+	+	kIx~
δ	δ	kIx~
dúleia	dúleia	k1gFnSc1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
označující	označující	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
uctívání	uctívání	k1gNnSc2
obrazů	obraz	k1gInPc2
svatých	svatá	k1gFnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
ikon	ikon	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmu	pojem	k1gInSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
obrazoboreckým	obrazoborecký	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
v	v	k7c6
byzantské	byzantský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byli	být	k5eAaImAgMnP
uctívači	uctívač	k1gMnPc1
obrazů	obraz	k1gInPc2
některými	některý	k3yIgFnPc7
císaři	císař	k1gMnPc1
pronásledováni	pronásledovat	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s>
Kořeny	kořen	k1gInPc1
</s>
<s>
„	„	k?
<g/>
Nezobrazíš	zobrazit	k5eNaPmIp2nS
<g/>
...	...	k?
<g/>
“	“	k?
–	–	k?
stojí	stát	k5eAaImIp3nS
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
přikázání	přikázání	k1gNnSc6
Desatera	desatero	k1gNnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc4
i	i	k8xC
rané	raný	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
zavrhly	zavrhnout	k5eAaPmAgInP
uctívání	uctívání	k1gNnSc3
obrazů	obraz	k1gInPc2
jako	jako	k8xS,k8xC
projev	projev	k1gInSc4
pohanství	pohanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
stanovil	stanovit	k5eAaPmAgInS
později	pozdě	k6eAd2
také	také	k9
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
proti	proti	k7c3
figurální	figurální	k2eAgFnSc3d1
výzdobě	výzdoba	k1gFnSc3
církevních	církevní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
však	však	k9
postupně	postupně	k6eAd1
slábl	slábnout	k5eAaImAgMnS
<g/>
,	,	kIx,
nepochybně	pochybně	k6eNd1
úměrně	úměrně	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zanikaly	zanikat	k5eAaImAgInP
pohanské	pohanský	k2eAgInPc1d1
kulty	kult	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanská	křesťanský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
na	na	k7c6
Východě	východ	k1gInSc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
připustila	připustit	k5eAaPmAgFnS
uctívání	uctívání	k1gNnSc4
obrazů	obraz	k1gInPc2
jako	jako	k8xS,k8xC
projev	projev	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
zbožnosti	zbožnost	k1gFnSc2
<g/>
,	,	kIx,
během	během	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
začala	začít	k5eAaPmAgFnS
sama	sám	k3xTgFnSc1
považovat	považovat	k5eAaImF
zobrazení	zobrazení	k1gNnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
a	a	k8xC
úctu	úcta	k1gFnSc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
za	za	k7c4
vyjádření	vyjádření	k1gNnSc4
pravé	pravý	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
úctu	úcta	k1gFnSc4
k	k	k7c3
obrazům	obraz	k1gInPc3
se	se	k3xPyFc4
vyslovil	vyslovit	k5eAaPmAgMnS
i	i	k9
papež	papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
590	#num#	k4
<g/>
–	–	k?
<g/>
604	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
marseilleskému	marseilleský	k2eAgMnSc3d1
biskupu	biskup	k1gMnSc3
Serenovi	Seren	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
obraz	obraz	k1gInSc1
představuje	představovat	k5eAaImIp3nS
pro	pro	k7c4
věřící	věřící	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
neumějí	umět	k5eNaImIp3nP
číst	číst	k5eAaImF
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
Písmo	písmo	k1gNnSc4
pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
to	ten	k3xDgNnSc4
dovedou	dovést	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Ikonodulie	ikonodulie	k1gFnSc1
<g/>
,	,	kIx,
uctívání	uctívání	k1gNnSc1
obrazů	obraz	k1gInPc2
svatých	svatá	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
mezi	mezi	k7c7
řeholním	řeholní	k2eAgNnSc7d1
duchovenstvem	duchovenstvo	k1gNnSc7
i	i	k8xC
širokými	široký	k2eAgFnPc7d1
laickými	laický	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
obyvatelstva	obyvatelstvo	k1gNnSc2
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemalou	malý	k2eNgFnSc4d1
zásluhu	zásluha	k1gFnSc4
na	na	k7c6
tom	ten	k3xDgNnSc6
měla	mít	k5eAaImAgFnS
sama	sám	k3xTgFnSc1
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
mniši	mnich	k1gMnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
z	z	k7c2
výroby	výroba	k1gFnSc2
a	a	k8xC
prodeje	prodej	k1gInSc2
ikon	ikona	k1gFnPc2
bohatla	bohatnout	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
úcta	úcta	k1gFnSc1
neměla	mít	k5eNaImAgFnS
být	být	k5eAaImF
prokazována	prokazován	k2eAgFnSc1d1
obrazu	obraz	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
něm	on	k3xPp3gMnSc6
znázorněn	znázorněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
se	s	k7c7
předmětem	předmět	k1gInSc7
uctívání	uctívání	k1gNnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
řeckých	řecký	k2eAgMnPc2d1
věřících	věřící	k1gMnPc2
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
části	část	k1gFnSc6
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
ikony	ikona	k1gFnSc2
samy	sám	k3xTgFnPc1
<g/>
,	,	kIx,
kterým	který	k3yQgInPc3,k3yRgInPc3,k3yIgInPc3
byla	být	k5eAaImAgFnS
navíc	navíc	k6eAd1
připisována	připisován	k2eAgFnSc1d1
moc	moc	k1gFnSc1
způsobit	způsobit	k5eAaPmF
zázraky	zázrak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
budilo	budit	k5eAaImAgNnS
nevoli	nevole	k1gFnSc4
u	u	k7c2
vzdělanějších	vzdělaný	k2eAgFnPc2d2
vrstev	vrstva	k1gFnPc2
kléru	klér	k1gInSc2
i	i	k8xC
světské	světský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
považovaly	považovat	k5eAaImAgFnP
takové	takový	k3xDgNnSc4
jednání	jednání	k1gNnSc4
za	za	k7c4
modlářství	modlářství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uctívání	uctívání	k1gNnSc1
obrazů	obraz	k1gInPc2
bylo	být	k5eAaImAgNnS
odmítáno	odmítat	k5eAaImNgNnS
zejména	zejména	k9
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
provinciích	provincie	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
působily	působit	k5eAaImAgInP
zbytky	zbytek	k1gInPc1
monofyzitů	monofyzita	k1gMnPc2
<g/>
,	,	kIx,
šířící	šířící	k2eAgFnSc1d1
se	se	k3xPyFc4
sekta	sekta	k1gFnSc1
paulikiánů	paulikián	k1gMnPc2
a	a	k8xC
také	také	k9
kontakty	kontakt	k1gInPc4
s	s	k7c7
židovským	židovský	k2eAgNnSc7d1
a	a	k8xC
muslimským	muslimský	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
v	v	k7c6
byzantské	byzantský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
na	na	k7c6
počátku	počátek	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
hrozivě	hrozivě	k6eAd1
rýsovat	rýsovat	k5eAaImF
nábožensko-ideologický	nábožensko-ideologický	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
nepochybně	pochybně	k6eNd1
hrály	hrát	k5eAaImAgInP
také	také	k9
odlišné	odlišný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
i	i	k8xC
tendence	tendence	k1gFnSc1
vývoje	vývoj	k1gInSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ikona	ikona	k1gFnSc1
</s>
<s>
Obrazoborectví	obrazoborectví	k1gNnSc1
</s>
<s>
Druhý	druhý	k4xOgInSc1
nikajský	nikajský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4145409-1	4145409-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85064084	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85064084	#num#	k4
</s>
