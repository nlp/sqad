<p>
<s>
Geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
smyslu	smysl	k1gInSc6	smysl
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
použil	použít	k5eAaPmAgMnS	použít
americký	americký	k2eAgMnSc1d1	americký
geolog	geolog	k1gMnSc1	geolog
W.	W.	kA	W.
J.	J.	kA	J.
McGee	McGee	k1gNnSc4	McGee
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Objektem	objekt	k1gInSc7	objekt
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
-	-	kIx~	-
reliéf	reliéf	k1gInSc1	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
řešení	řešení	k1gNnSc1	řešení
vztahů	vztah	k1gInPc2	vztah
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
objektu	objekt	k1gInSc2	objekt
tj.	tj.	kA	tj.
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
složkami	složka	k1gFnPc7	složka
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Farského	farský	k2eAgInSc2d1	farský
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
vědní	vědní	k2eAgFnSc1d1	vědní
disciplinou	disciplina	k1gFnSc7	disciplina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
předmětu	předmět	k1gInSc2	předmět
zabývá	zabývat	k5eAaImIp3nS	zabývat
georeliéfem	georeliéf	k1gInSc7	georeliéf
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc7	jeho
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
tvary	tvar	k1gInPc7	tvar
a	a	k8xC	a
způsoby	způsob	k1gInPc1	způsob
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
procesy	proces	k1gInPc7	proces
vedoucími	vedoucí	k2eAgInPc7d1	vedoucí
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
materiálního	materiální	k2eAgInSc2d1	materiální
základu	základ	k1gInSc2	základ
těchto	tento	k3xDgInPc2	tento
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
==	==	k?	==
</s>
</p>
<p>
<s>
regionální	regionální	k2eAgInSc4d1	regionální
</s>
</p>
<p>
<s>
obecná	obecná	k1gFnSc1	obecná
</s>
</p>
<p>
<s>
strukturní	strukturní	k2eAgFnSc1d1	strukturní
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
klimatická	klimatický	k2eAgFnSc1d1	klimatická
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
klimatogenetická	klimatogenetický	k2eAgFnSc1d1	klimatogenetický
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
dynamická	dynamický	k2eAgFnSc1d1	dynamická
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
paleogeomorfologie	paleogeomorfologie	k1gFnSc1	paleogeomorfologie
</s>
</p>
<p>
<s>
antropogenní	antropogenní	k2eAgFnSc1d1	antropogenní
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
teoretická	teoretický	k2eAgFnSc1d1	teoretická
geomorfologieZemské	geomorfologieZemský	k2eAgInPc1d1	geomorfologieZemský
tvary	tvar	k1gInPc4	tvar
vznikají	vznikat	k5eAaImIp3nP	vznikat
působením	působení	k1gNnPc3	působení
endogenních	endogenní	k2eAgInPc2d1	endogenní
(	(	kIx(	(
<g/>
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
<g/>
)	)	kIx)	)
a	a	k8xC	a
exogenních	exogenní	k2eAgInPc2d1	exogenní
(	(	kIx(	(
<g/>
vnějších	vnější	k2eAgInPc2d1	vnější
<g/>
)	)	kIx)	)
činitelů	činitel	k1gInPc2	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
např.	např.	kA	např.
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vnější	vnější	k2eAgMnPc1d1	vnější
geomorfologičtí	geomorfologický	k2eAgMnPc1d1	geomorfologický
činitelé	činitel	k1gMnPc1	činitel
==	==	k?	==
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgMnPc1d1	vnější
geomorfologičtí	geomorfologický	k2eAgMnPc1d1	geomorfologický
činitelé	činitel	k1gMnPc1	činitel
působí	působit	k5eAaImIp3nP	působit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
erozně	erozně	k6eAd1	erozně
</s>
</p>
<p>
<s>
transportně	transportně	k6eAd1	transportně
</s>
</p>
<p>
<s>
akumulačněNejdůležitějším	akumulačněNejdůležitý	k2eAgInSc7d2	akumulačněNejdůležitý
předpokladem	předpoklad	k1gInSc7	předpoklad
působení	působení	k1gNnSc2	působení
vnějších	vnější	k2eAgMnPc2d1	vnější
činitelů	činitel	k1gMnPc2	činitel
je	být	k5eAaImIp3nS	být
zvětrávání	zvětrávání	k1gNnSc1	zvětrávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnPc4	působení
vody	voda	k1gFnSc2	voda
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
působení	působení	k1gNnSc2	působení
vody	voda	k1gFnSc2	voda
rozumíme	rozumět	k5eAaImIp1nP	rozumět
působení	působení	k1gNnSc4	působení
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geomorfologické	geomorfologický	k2eAgInPc4d1	geomorfologický
tvary	tvar	k1gInPc4	tvar
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
tímto	tento	k3xDgNnSc7	tento
působením	působení	k1gNnSc7	působení
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ron	ron	k1gInSc1	ron
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jakési	jakýsi	k3yIgNnSc4	jakýsi
plošné	plošný	k2eAgNnSc4d1	plošné
smývání	smývání	k1gNnSc4	smývání
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
nezpevněné	zpevněný	k2eNgFnPc1d1	nezpevněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
erozní	erozní	k2eAgFnPc1d1	erozní
rýhy	rýha	k1gFnPc1	rýha
-	-	kIx~	-
rýhy	rýha	k1gFnPc1	rýha
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
erozní	erozní	k2eAgFnSc7d1	erozní
činností	činnost	k1gFnSc7	činnost
dopadající	dopadající	k2eAgFnSc2d1	dopadající
srážkové	srážkový	k2eAgFnSc2d1	srážková
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
rýhy	rýha	k1gFnSc2	rýha
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tvrdosti	tvrdost	k1gFnSc6	tvrdost
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
hornina	hornina	k1gFnSc1	hornina
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
rýhy	rýha	k1gFnSc2	rýha
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
V.	V.	kA	V.
V	v	k7c6	v
měkčích	měkký	k2eAgFnPc6d2	měkčí
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
rýhy	rýha	k1gFnSc2	rýha
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U.	U.	kA	U.
</s>
</p>
<p>
<s>
zemní	zemní	k2eAgFnPc1d1	zemní
pyramidy	pyramida	k1gFnPc1	pyramida
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
úzkého	úzký	k2eAgInSc2d1	úzký
pilíře	pilíř	k1gInSc2	pilíř
se	s	k7c7	s
širším	široký	k2eAgInSc7d2	širší
kamenem	kámen	k1gInSc7	kámen
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
např.	např.	kA	např.
o	o	k7c4	o
Kokořínské	Kokořínský	k2eAgFnPc4d1	Kokořínská
pokličky	poklička	k1gFnPc4	poklička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
badland	badland	k1gInSc1	badland
-	-	kIx~	-
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
z	z	k7c2	z
měkkých	měkký	k2eAgInPc2d1	měkký
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
prudkého	prudký	k2eAgInSc2d1	prudký
deště	dešť	k1gInSc2	dešť
tu	tu	k6eAd1	tu
vznikají	vznikat	k5eAaImIp3nP	vznikat
rýhy	rýha	k1gFnPc1	rýha
o	o	k7c6	o
různé	různý	k2eAgFnSc6d1	různá
hloubce	hloubka	k1gFnSc6	hloubka
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvlášť	zvlášť	k6eAd1	zvlášť
nesourodý	sourodý	k2eNgInSc1d1	nesourodý
a	a	k8xC	a
neschůdný	schůdný	k2eNgInSc1d1	neschůdný
terén	terén	k1gInSc1	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnPc1	působení
řek	řeka	k1gFnPc2	řeka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnPc2	působení
ledovců	ledovec	k1gInPc2	ledovec
===	===	k?	===
</s>
</p>
<p>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
svojí	svůj	k3xOyFgFnSc7	svůj
tíhou	tíha	k1gFnSc7	tíha
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
ohlazování	ohlazování	k1gNnSc2	ohlazování
skal	skála	k1gFnPc2	skála
nebo	nebo	k8xC	nebo
tvoření	tvoření	k1gNnSc2	tvoření
údolí	údolí	k1gNnSc2	údolí
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
tvary	tvar	k1gInPc1	tvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
moréna	moréna	k1gFnSc1	moréna
</s>
</p>
<p>
<s>
pleso	pleso	k1gNnSc1	pleso
</s>
</p>
<p>
<s>
oblík	oblík	k1gInSc1	oblík
-	-	kIx~	-
zaoblený	zaoblený	k2eAgInSc1d1	zaoblený
malý	malý	k2eAgInSc1d1	malý
kopec	kopec	k1gInSc1	kopec
s	s	k7c7	s
podložím	podloží	k1gNnSc7	podloží
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
odolal	odolat	k5eAaPmAgMnS	odolat
plošné	plošný	k2eAgFnSc3d1	plošná
ledovcové	ledovcový	k2eAgFnSc3d1	ledovcová
erozi	eroze	k1gFnSc3	eroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
</s>
</p>
<p>
<s>
thufur	thufur	k1gMnSc1	thufur
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
kopeček	kopeček	k1gInSc1	kopeček
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
výška	výška	k1gFnSc1	výška
1	[number]	k4	1
m	m	kA	m
<g/>
)	)	kIx)	)
s	s	k7c7	s
ledovým	ledový	k2eAgNnSc7d1	ledové
jádrem	jádro	k1gNnSc7	jádro
</s>
</p>
<p>
<s>
pingo	pingo	k1gNnSc1	pingo
-	-	kIx~	-
osamocený	osamocený	k2eAgInSc1d1	osamocený
pahorek	pahorek	k1gInSc1	pahorek
s	s	k7c7	s
ledovým	ledový	k2eAgNnSc7d1	ledové
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
výška	výška	k1gFnSc1	výška
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
50	[number]	k4	50
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
trvale	trvale	k6eAd1	trvale
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ledovcový	ledovcový	k2eAgInSc1d1	ledovcový
stůl	stůl	k1gInSc1	stůl
-	-	kIx~	-
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
vypadající	vypadající	k2eAgInSc1d1	vypadající
jako	jako	k8xS	jako
hřib	hřib	k1gInSc1	hřib
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
tvoří	tvořit	k5eAaImIp3nS	tvořit
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
slunečním	sluneční	k2eAgInPc3d1	sluneční
paprskům	paprsek	k1gInPc3	paprsek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
noha	noha	k1gFnSc1	noha
tvořená	tvořený	k2eAgFnSc1d1	tvořená
ledovcem	ledovec	k1gInSc7	ledovec
roztála	roztát	k5eAaPmAgFnS	roztát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
trog	trog	k1gInSc1	trog
</s>
</p>
<p>
<s>
fjord	fjord	k1gInSc1	fjord
</s>
</p>
<p>
<s>
matterhorn	matterhorn	k1gNnSc1	matterhorn
-	-	kIx~	-
jehlanovitá	jehlanovitý	k2eAgFnSc1d1	jehlanovitá
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
samostatných	samostatný	k2eAgInPc2d1	samostatný
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
obrušují	obrušovat	k5eAaImIp3nP	obrušovat
do	do	k7c2	do
stále	stále	k6eAd1	stále
strmější	strmý	k2eAgFnSc2d2	strmější
a	a	k8xC	a
ostřejší	ostrý	k2eAgFnSc2d2	ostřejší
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
Matterhorn	Matterhorn	k1gInSc4	Matterhorn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působení	působení	k1gNnPc2	působení
moře	moře	k1gNnSc2	moře
===	===	k?	===
</s>
</p>
<p>
<s>
Moře	moře	k1gNnSc1	moře
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vzezření	vzezření	k1gNnSc4	vzezření
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
buď	buď	k8xC	buď
erozivně	erozivně	k6eAd1	erozivně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
akumulačně	akumulačně	k6eAd1	akumulačně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Akumulační	akumulační	k2eAgInPc1d1	akumulační
tvary	tvar	k1gInPc1	tvar
====	====	k?	====
</s>
</p>
<p>
<s>
písečná	písečný	k2eAgFnSc1d1	písečná
kosa	kosa	k1gFnSc1	kosa
</s>
</p>
<p>
<s>
písečný	písečný	k2eAgInSc1d1	písečný
val	val	k1gInSc1	val
</s>
</p>
<p>
<s>
pláž	pláž	k1gFnSc1	pláž
</s>
</p>
<p>
<s>
====	====	k?	====
Erozní	erozní	k2eAgInPc1d1	erozní
tvary	tvar	k1gInPc1	tvar
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
mysech	mys	k1gInPc6	mys
a	a	k8xC	a
klifech	klif	k1gInPc6	klif
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mořská	mořský	k2eAgFnSc1d1	mořská
(	(	kIx(	(
<g/>
skalní	skalní	k2eAgFnSc1d1	skalní
<g/>
)	)	kIx)	)
brána	brána	k1gFnSc1	brána
</s>
</p>
<p>
<s>
skalní	skalní	k2eAgInSc1d1	skalní
pilíř	pilíř	k1gInSc1	pilíř
-	-	kIx~	-
zůstane	zůstat	k5eAaPmIp3nS	zůstat
po	po	k7c6	po
zřícení	zřícení	k1gNnSc6	zřícení
brány	brána	k1gFnSc2	brána
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologie	geomorfologie	k1gFnPc4	geomorfologie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
==	==	k?	==
</s>
</p>
<p>
<s>
provincie	provincie	k1gFnSc1	provincie
</s>
</p>
<p>
<s>
subprovincie	subprovincie	k1gFnSc1	subprovincie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
<g/>
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
subprovincie	subprovincie	k1gFnSc1	subprovincie
</s>
</p>
<p>
<s>
Česko-moravská	českooravský	k2eAgFnSc1d1	česko-moravská
subprovincie	subprovincie	k1gFnSc1	subprovincie
</s>
</p>
<p>
<s>
Krušnohorská	krušnohorský	k2eAgFnSc1d1	Krušnohorská
subprovincie	subprovincie	k1gFnSc1	subprovincie
</s>
</p>
<p>
<s>
Krkonošsko-jesenická	krkonošskoesenický	k2eAgFnSc1d1	krkonošsko-jesenický
(	(	kIx(	(
<g/>
Sudetská	sudetský	k2eAgFnSc1d1	sudetská
<g/>
)	)	kIx)	)
subprovincie	subprovincie	k1gFnSc1	subprovincie
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
tabule	tabule	k1gFnSc1	tabule
</s>
</p>
<p>
<s>
Středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
nížina	nížina	k1gFnSc1	nížina
</s>
</p>
<p>
<s>
Středopolské	Středopolský	k2eAgFnPc1d1	Středopolský
nížiny	nížina	k1gFnPc1	nížina
</s>
</p>
<p>
<s>
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
</s>
</p>
<p>
<s>
Vněkarpatské	Vněkarpatský	k2eAgFnPc1d1	Vněkarpatská
sníženiny	sníženina	k1gFnPc1	sníženina
</s>
</p>
<p>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
</s>
</p>
<p>
<s>
Západopanonská	Západopanonský	k2eAgFnSc1d1	Západopanonská
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Geografie	geografie	k1gFnSc1	geografie
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Morris	Morris	k1gFnSc2	Morris
Davis	Davis	k1gFnSc2	Davis
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BALATKA	balatka	k1gFnSc1	balatka
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
;	;	kIx,	;
RUBÍN	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
skalních	skalní	k2eAgInPc2d1	skalní
<g/>
,	,	kIx,	,
zemních	zemní	k2eAgInPc2d1	zemní
a	a	k8xC	a
půdních	půdní	k2eAgInPc2d1	půdní
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zeměpis	zeměpis	k1gInSc1	zeměpis
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Geomorfologie	geomorfologie	k1gFnSc2	geomorfologie
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
web	web	k1gInSc1	web
-	-	kIx~	-
Geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
