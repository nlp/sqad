<s>
Věra	Věra	k1gFnSc1	Věra
Rosí	rosit	k5eAaImIp3nS	rosit
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Veronika	Veronika	k1gFnSc1	Veronika
Schelleová	Schelleová	k1gFnSc1	Schelleová
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
.	.	kIx.	.
</s>
<s>
Absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
obory	obor	k1gInPc1	obor
bohemistika	bohemistika	k1gFnSc1	bohemistika
a	a	k8xC	a
germanistika	germanistika	k1gFnSc1	germanistika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
studuje	studovat	k5eAaImIp3nS	studovat
bohemistiku	bohemistika	k1gFnSc4	bohemistika
postgraduálně	postgraduálně	k6eAd1	postgraduálně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
sbírku	sbírka	k1gFnSc4	sbírka
básní	báseň	k1gFnPc2	báseň
Holý	holý	k2eAgInSc1d1	holý
bílý	bílý	k2eAgInSc1d1	bílý
kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86055	[number]	k4	86055
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
získala	získat	k5eAaPmAgFnS	získat
Cenu	cena	k1gFnSc4	cena
Jiřího	Jiří	k1gMnSc4	Jiří
Ortena	Orten	k1gMnSc4	Orten
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydala	vydat	k5eAaPmAgFnS	vydat
druhou	druhý	k4xOgFnSc4	druhý
sbírku	sbírka	k1gFnSc4	sbírka
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
nůžky	nůžky	k1gFnPc1	nůžky
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Věra	Věra	k1gFnSc1	Věra
Rosí	rosit	k5eAaImIp3nP	rosit
</s>
