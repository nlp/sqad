<s>
Ostrov	ostrov	k1gInSc1	ostrov
Graciosa	Graciosa	k1gFnSc1	Graciosa
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
expedicí	expedice	k1gFnPc2	expedice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zde	zde	k6eAd1	zde
studují	studovat	k5eAaImIp3nP	studovat
zejména	zejména	k9	zejména
problematiku	problematika	k1gFnSc4	problematika
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
<g/>
.	.	kIx.	.
</s>
