<s>
Sorání	Soránět	k5eAaImIp3nP	Soránět
(	(	kIx(	(
<g/>
kurdsky	kurdsky	k6eAd1	kurdsky
soranî	soranî	k?	soranî
<g/>
,	,	kIx,	,
kurmancî	kurmancî	k?	kurmancî
xwarû	xwarû	k?	xwarû
<g/>
,	,	kIx,	,
kurdî	kurdî	k?	kurdî
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
س	س	k?	س
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kurdský	kurdský	k2eAgInSc1d1	kurdský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Sorání	Sorání	k1gNnSc1	Sorání
je	být	k5eAaImIp3nS	být
gramaticky	gramaticky	k6eAd1	gramaticky
a	a	k8xC	a
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgNnSc1d1	blízké
kurmándží	kurmándžet	k5eAaImIp3nS	kurmándžet
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
ale	ale	k9	ale
arabskou	arabský	k2eAgFnSc4d1	arabská
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
kurdským	kurdský	k2eAgInPc3d1	kurdský
dialektům	dialekt	k1gInPc3	dialekt
má	mít	k5eAaImIp3nS	mít
sorání	soránět	k5eAaImIp3nS	soránět
literární	literární	k2eAgFnSc4d1	literární
tradici	tradice	k1gFnSc4	tradice
jak	jak	k8xC	jak
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
.	.	kIx.	.
ى	ى	k?	ى
,	,	kIx,	,
<g/>
ێ	ێ	k?	ێ
,	,	kIx,	,
<g/>
ە	ە	k?	ە
,	,	kIx,	,
<g/>
ھ	ھ	k?	ھ
,	,	kIx,	,
<g/>
ۆ	ۆ	k?	ۆ
,	,	kIx,	,
<g/>
و	و	k?	و
,	,	kIx,	,
<g/>
و	و	k?	و
,	,	kIx,	,
<g/>
ن	ن	k?	ن
,	,	kIx,	,
<g/>
م	م	k?	م
,	,	kIx,	,
<g/>
ڵ	ڵ	k?	ڵ
,	,	kIx,	,
<g/>
ل	ل	k?	ل
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
گ	گ	k?	گ
,	,	kIx,	,
<g/>
ک	ک	k?	ک
,	,	kIx,	,
<g/>
ق	ق	k?	ق
,	,	kIx,	,
<g/>
ڤ	ڤ	k?	ڤ
,	,	kIx,	,
<g/>
ف	ف	k?	ف
,	,	kIx,	,
<g/>
غ	غ	k?	غ
,	,	kIx,	,
<g/>
ع	ع	k?	ع
,	,	kIx,	,
<g/>
ش	ش	k?	ش
,	,	kIx,	,
<g/>
س	س	k?	س
,	,	kIx,	,
<g/>
ژ	ژ	k?	ژ
,	,	kIx,	,
<g/>
ز	ز	k?	ز
,	,	kIx,	,
<g/>
ڕ	ڕ	k?	ڕ
,	,	kIx,	,
<g/>
ر	ر	k?	ر
,	,	kIx,	,
<g/>
د	د	k?	د
,	,	kIx,	,
<g/>
خ	خ	k?	خ
,	,	kIx,	,
<g/>
ح	ح	k?	ح
,	,	kIx,	,
<g/>
چ	چ	k?	چ
,	,	kIx,	,
<g/>
ج	ج	k?	ج
,	,	kIx,	,
<g/>
ت	ت	k?	ت
,	,	kIx,	,
<g/>
پ	پ	k?	پ
,	,	kIx,	,
<g/>
ب	ب	k?	ب
,	,	kIx,	,
<g/>
ا	ا	k?	ا
STROHMEIER	STROHMEIER	kA	STROHMEIER
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
YALCIN-HECKMANN	YALCIN-HECKMANN	k1gMnSc1	YALCIN-HECKMANN
<g/>
,	,	kIx,	,
Lale	Lale	k1gFnSc1	Lale
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Kurden	Kurdna	k1gFnPc2	Kurdna
<g/>
:	:	kIx,	:
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
,	,	kIx,	,
Politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
Kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
:	:	kIx,	:
Beck	Beck	k1gInSc1	Beck
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3406421296	[number]	k4	3406421296
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
ABDULLAH	ABDULLAH	kA	ABDULLAH
<g/>
,	,	kIx,	,
Jamal	Jamal	k1gMnSc1	Jamal
Jalal	Jalal	k1gMnSc1	Jalal
<g/>
;	;	kIx,	;
MCCARUS	MCCARUS	kA	MCCARUS
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
N.	N.	kA	N.
<g/>
.	.	kIx.	.
</s>
<s>
Kurdish	Kurdish	k1gMnSc1	Kurdish
Basic	Basic	kA	Basic
Course	Course	k1gFnSc1	Course
<g/>
:	:	kIx,	:
Dialect	Dialect	k1gInSc1	Dialect
of	of	k?	of
Sulaimania	Sulaimanium	k1gNnSc2	Sulaimanium
<g/>
,	,	kIx,	,
Iraq	Iraq	k1gFnSc2	Iraq
<g/>
.	.	kIx.	.
</s>
<s>
Ann	Ann	k?	Ann
Arbor	Arbor	k1gInSc1	Arbor
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Michigan	Michigan	k1gInSc1	Michigan
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
916798	[number]	k4	916798
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
