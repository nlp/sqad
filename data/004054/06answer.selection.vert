<s>
Mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mn	Mn	k1gFnSc2	Mn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Manganum	Manganum	k1gInSc1	Manganum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
paramagnetický	paramagnetický	k2eAgInSc1d1	paramagnetický
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
