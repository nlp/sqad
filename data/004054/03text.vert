<s>
Mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Mn	Mn	k1gFnSc2	Mn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Manganum	Manganum	k1gInSc1	Manganum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
paramagnetický	paramagnetický	k2eAgInSc1d1	paramagnetický
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
jako	jako	k8xC	jako
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
a	a	k8xC	a
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgMnSc1d1	kovový
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
a	a	k8xC	a
značně	značně	k6eAd1	značně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
prvek	prvek	k1gInSc1	prvek
světle	světle	k6eAd1	světle
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
elektropozitivní	elektropozitivní	k2eAgInSc1d1	elektropozitivní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejelektropozitivnější	elektropozitivný	k2eAgInSc1d3	elektropozitivný
po	po	k7c6	po
alkalických	alkalický	k2eAgInPc6d1	alkalický
kovech	kov	k1gInPc6	kov
<g/>
,	,	kIx,	,
kovech	kov	k1gInPc6	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
stabilních	stabilní	k2eAgFnPc6d1	stabilní
modifikacích	modifikace	k1gFnPc6	modifikace
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
,	,	kIx,	,
β	β	k?	β
a	a	k8xC	a
γ	γ	k?	γ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
modifikace	modifikace	k1gFnSc1	modifikace
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
za	za	k7c2	za
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
742	[number]	k4	742
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
1070	[number]	k4	1070
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1070	[number]	k4	1070
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
1160	[number]	k4	1160
°	°	k?	°
<g/>
C.	C.	kA	C.
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
modifikace	modifikace	k1gFnPc1	modifikace
jsou	být	k5eAaImIp3nP	být
křehké	křehký	k2eAgFnPc1d1	křehká
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
aluminotermické	aluminotermický	k2eAgFnSc6d1	aluminotermický
výrobě	výroba	k1gFnSc6	výroba
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
elektrolytickém	elektrolytický	k2eAgNnSc6d1	elektrolytické
vylučování	vylučování	k1gNnSc6	vylučování
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
tažná	tažný	k2eAgFnSc1d1	tažná
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
modifikace	modifikace	k1gFnSc1	modifikace
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
nad	nad	k7c7	nad
1160	[number]	k4	1160
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
δ	δ	k?	δ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
klesá	klesat	k5eAaImIp3nS	klesat
zásaditost	zásaditost	k1gFnSc1	zásaditost
prvku	prvek	k1gInSc2	prvek
a	a	k8xC	a
stoupá	stoupat	k5eAaImIp3nS	stoupat
kyselost	kyselost	k1gFnSc1	kyselost
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
svých	svůj	k3xOyFgFnPc6	svůj
vlastnostech	vlastnost	k1gFnPc6	vlastnost
i	i	k8xC	i
sloučeninách	sloučenina	k1gFnPc6	sloučenina
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
prvkům	prvek	k1gInPc3	prvek
a	a	k8xC	a
sloučeninám	sloučenina	k1gFnPc3	sloučenina
sedmé	sedmý	k4xOgFnSc2	sedmý
hlavní	hlavní	k2eAgFnSc2d1	hlavní
podskupiny	podskupina	k1gFnSc2	podskupina
–	–	k?	–
halogenům	halogen	k1gInPc3	halogen
–	–	k?	–
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
chloru	chlor	k1gInSc2	chlor
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
–	–	k?	–
chloristany	chloristan	k1gInPc7	chloristan
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
manganistanům	manganistan	k1gInPc3	manganistan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
mocenství	mocenství	k1gNnPc2	mocenství
od	od	k7c2	od
Mn	Mn	k1gFnSc2	Mn
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
po	po	k7c6	po
Mn	Mn	k1gFnSc6	Mn
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgInPc1d3	nejstálejší
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
sloučeniny	sloučenina	k1gFnPc1	sloučenina
manganu	mangan	k1gInSc2	mangan
Mn	Mn	k1gFnPc1	Mn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
a	a	k8xC	a
Mn	Mn	k1gMnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
i	i	k9	i
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
Mn	Mn	k1gFnSc2	Mn
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
i	i	k8xC	i
Mn	Mn	k1gMnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
je	být	k5eAaImIp3nS	být
mangan	mangan	k1gInSc1	mangan
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
a	a	k8xC	a
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnPc1d1	dusičná
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
koncentrace	koncentrace	k1gFnSc2	koncentrace
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
buď	buď	k8xC	buď
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
dusnatého	dusnatý	k2eAgInSc2d1	dusnatý
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
dusičitého	dusičitý	k2eAgInSc2d1	dusičitý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
poměrně	poměrně	k6eAd1	poměrně
podobný	podobný	k2eAgInSc1d1	podobný
železu	železo	k1gNnSc3	železo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Jemně	jemně	k6eAd1	jemně
rozetřený	rozetřený	k2eAgInSc1d1	rozetřený
práškový	práškový	k2eAgInSc1d1	práškový
mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
pyroforický	pyroforický	k2eAgInSc1d1	pyroforický
–	–	k?	–
je	být	k5eAaImIp3nS	být
samozápalný	samozápalný	k2eAgInSc1d1	samozápalný
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopný	schopný	k2eAgMnSc1d1	schopný
rozkládat	rozkládat	k5eAaImF	rozkládat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
teplot	teplota	k1gFnPc2	teplota
málo	málo	k6eAd1	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
prvky	prvek	k1gInPc7	prvek
–	–	k?	–
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
halogeny	halogen	k1gInPc4	halogen
<g/>
,	,	kIx,	,
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
uhlík	uhlík	k1gInSc1	uhlík
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
bor	bor	k1gInSc1	bor
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
manganičitý	manganičitý	k2eAgInSc1d1	manganičitý
–	–	k?	–
burel	burel	k1gInSc1	burel
–	–	k?	–
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
odrůdu	odrůda	k1gFnSc4	odrůda
magnetovce	magnetovec	k1gInSc2	magnetovec
(	(	kIx(	(
<g/>
magnes	magnes	k1gInSc1	magnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
filozof	filozof	k1gMnSc1	filozof
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
nazval	nazvat	k5eAaBmAgMnS	nazvat
burel	burel	k1gInSc4	burel
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ženskou	ženský	k2eAgFnSc4d1	ženská
<g/>
"	"	kIx"	"
odrůdu	odrůda	k1gFnSc4	odrůda
magnetovce	magnetovec	k1gInSc2	magnetovec
<g/>
.	.	kIx.	.
</s>
<s>
Středověk	středověk	k1gInSc1	středověk
již	již	k6eAd1	již
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c4	mezi
magnes	magnes	k1gInSc4	magnes
nebo	nebo	k8xC	nebo
magnesius	magnesius	k1gInSc4	magnesius
lapis	lapis	k1gInSc1	lapis
(	(	kIx(	(
<g/>
magnetovec	magnetovec	k1gInSc1	magnetovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
magnesia	magnesium	k1gNnPc1	magnesium
nebo	nebo	k8xC	nebo
pseudomagnes	pseudomagnes	k1gInSc1	pseudomagnes
(	(	kIx(	(
<g/>
falešný	falešný	k2eAgInSc1d1	falešný
magnet	magnet	k1gInSc1	magnet
čili	čili	k8xC	čili
burel	burel	k1gInSc1	burel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
dali	dát	k5eAaPmAgMnP	dát
skláři	sklář	k1gMnPc1	sklář
burelu	burel	k1gInSc2	burel
název	název	k1gInSc1	název
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
schopnosti	schopnost	k1gFnSc2	schopnost
odbarvovat	odbarvovat	k5eAaImF	odbarvovat
železnaté	železnatý	k2eAgNnSc4d1	železnatý
sklo	sklo	k1gNnSc4	sklo
sklářské	sklářský	k2eAgNnSc4d1	sklářské
mýdlo	mýdlo	k1gNnSc4	mýdlo
a	a	k8xC	a
změnili	změnit	k5eAaPmAgMnP	změnit
jeho	on	k3xPp3gInSc4	on
název	název	k1gInSc4	název
na	na	k7c4	na
manganes	manganes	k1gInSc4	manganes
neboli	neboli	k8xC	neboli
lapis	lapis	k1gInSc4	lapis
manganensis	manganensis	k1gFnSc2	manganensis
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
burel	burel	k1gInSc4	burel
železnou	železný	k2eAgFnSc7d1	železná
rudou	ruda	k1gFnSc7	ruda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgMnS	udržet
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
konečně	konečně	k6eAd1	konečně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
ruda	ruda	k1gFnSc1	ruda
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Carl	Carl	k1gMnSc1	Carl
W.	W.	kA	W.
Scheele	Scheel	k1gInPc1	Scheel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předložil	předložit	k5eAaPmAgInS	předložit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
nezvratné	zvratný	k2eNgInPc1d1	nezvratný
důkazy	důkaz	k1gInPc1	důkaz
Akademii	akademie	k1gFnSc4	akademie
věd	věda	k1gFnPc2	věda
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
mangan	mangan	k1gInSc4	mangan
izolovat	izolovat	k5eAaBmF	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Izoloval	izolovat	k5eAaBmAgMnS	izolovat
ho	on	k3xPp3gNnSc4	on
Johan	Johan	k1gMnSc1	Johan
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Gahn	Gahn	k1gMnSc1	Gahn
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
burelu	burel	k1gInSc2	burel
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
a	a	k8xC	a
olejem	olej	k1gInSc7	olej
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoků	roztok	k1gInPc2	roztok
manganatých	manganatý	k2eAgFnPc2d1	manganatý
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
dostal	dostat	k5eAaPmAgInS	dostat
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
první	první	k4xOgInSc1	první
název	název	k1gInSc1	název
manganesium	manganesium	k1gNnSc4	manganesium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
mangan	mangan	k1gInSc4	mangan
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
manganum	manganum	k1gNnSc1	manganum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
záměně	záměna	k1gFnSc3	záměna
s	s	k7c7	s
hořčíkem	hořčík	k1gInSc7	hořčík
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
magnesium	magnesium	k1gNnSc1	magnesium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
mezitím	mezitím	k6eAd1	mezitím
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
prvkem	prvek	k1gInSc7	prvek
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
značným	značný	k2eAgNnSc7d1	značné
zastoupením	zastoupení	k1gNnSc7	zastoupení
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
činí	činit	k5eAaImIp3nS	činit
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
manganu	mangan	k1gInSc2	mangan
kolem	kolo	k1gNnSc7	kolo
0,9	[number]	k4	0,9
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
nebo	nebo	k8xC	nebo
1000	[number]	k4	1000
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
dvanácté	dvanáctý	k4xOgNnSc4	dvanáctý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
železe	železo	k1gNnSc6	železo
a	a	k8xC	a
titanu	titan	k1gInSc6	titan
třetí	třetí	k4xOgFnSc1	třetí
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
kov	kov	k1gInSc4	kov
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
2	[number]	k4	2
mikrogramů	mikrogram	k1gInPc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
manganu	mangan	k1gInSc2	mangan
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
mangan	mangan	k1gInSc1	mangan
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
současně	současně	k6eAd1	současně
s	s	k7c7	s
rudami	ruda	k1gFnPc7	ruda
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
minerálem	minerál	k1gInSc7	minerál
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
pyroluzit	pyroluzit	k1gInSc1	pyroluzit
(	(	kIx(	(
<g/>
burel	burel	k1gInSc1	burel
<g/>
)	)	kIx)	)
MnO	MnO	k1gFnSc1	MnO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
významnější	významný	k2eAgInPc1d2	významnější
nerosty	nerost	k1gInPc1	nerost
jsou	být	k5eAaImIp3nP	být
hausmannit	hausmannit	k1gInSc4	hausmannit
Mn	Mn	k1gFnSc2	Mn
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
braunit	braunit	k1gInSc1	braunit
Mn	Mn	k1gFnSc1	Mn
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
manganit	manganit	k1gInSc1	manganit
MnO	MnO	k1gFnSc1	MnO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
rhodochrozit	rhodochrozit	k5eAaPmF	rhodochrozit
neboli	neboli	k8xC	neboli
dialogit	dialogit	k1gInSc1	dialogit
MnCO	MnCO	k1gFnSc2	MnCO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc4d1	významný
minerály	minerál	k1gInPc4	minerál
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
wolframit	wolframit	k1gInSc4	wolframit
(	(	kIx(	(
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
triplit	triplit	k1gInSc1	triplit
(	(	kIx(	(
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tephroit	tephroit	k1gMnSc1	tephroit
Mn	Mn	k1gFnSc2	Mn
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
tantalit	tantalit	k1gInSc1	tantalit
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
Ta	ten	k3xDgFnSc1	ten
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
těžba	těžba	k1gFnSc1	těžba
manganových	manganový	k2eAgFnPc2d1	manganová
rud	ruda	k1gFnPc2	ruda
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vytěží	vytěžit	k5eAaPmIp3nS	vytěžit
3,4	[number]	k4	3,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2,13	[number]	k4	2,13
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
JAR	jar	k1gInSc1	jar
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1	[number]	k4	1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
Gabonu	Gabon	k1gInSc6	Gabon
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
0,58	[number]	k4	0,58
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
0,5	[number]	k4	0,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
rudy	ruda	k1gFnPc1	ruda
manganu	mangan	k1gInSc2	mangan
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
manganové	manganový	k2eAgInPc1d1	manganový
konkrece	konkreec	k1gInPc1	konkreec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
útvary	útvar	k1gInPc1	útvar
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
od	od	k7c2	od
průměru	průměr	k1gInSc2	průměr
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
až	až	k8xS	až
velikost	velikost	k1gFnSc1	velikost
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
míče	míč	k1gInSc2	míč
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1872	[number]	k4	1872
až	až	k9	až
1876	[number]	k4	1876
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
místy	místo	k1gNnPc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
oceánské	oceánský	k2eAgFnPc1d1	oceánská
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánů	oceán	k1gInPc2	oceán
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1012	[number]	k4	1012
tun	tuna	k1gFnPc2	tuna
manganových	manganový	k2eAgFnPc2d1	manganová
konkrecí	konkrecí	k2eAgFnSc4d1	konkrecí
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
usadí	usadit	k5eAaPmIp3nP	usadit
řádově	řádově	k6eAd1	řádově
107	[number]	k4	107
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Konkrece	Konkreko	k6eAd1	Konkreko
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
zvětrávání	zvětrávání	k1gNnSc6	zvětrávání
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
vyplavování	vyplavování	k1gNnSc6	vyplavování
hornin	hornina	k1gFnPc2	hornina
do	do	k7c2	do
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
následně	následně	k6eAd1	následně
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
opět	opět	k6eAd1	opět
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
kulovité	kulovitý	k2eAgInPc4d1	kulovitý
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Konkrece	Konkrece	k1gFnPc1	Konkrece
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
sloučenin	sloučenina	k1gFnPc2	sloučenina
přechodných	přechodný	k2eAgInPc2d1	přechodný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
převládají	převládat	k5eAaImIp3nP	převládat
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
oxidy	oxid	k1gInPc4	oxid
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
konkrece	konkrece	k1gFnPc1	konkrece
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
kobalt	kobalt	k1gInSc4	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Rudy	ruda	k1gFnPc1	ruda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
průmyslovému	průmyslový	k2eAgNnSc3d1	průmyslové
získávání	získávání	k1gNnSc3	získávání
kovů	kov	k1gInPc2	kov
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
nejméně	málo	k6eAd3	málo
35	[number]	k4	35
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
rudy	ruda	k1gFnPc1	ruda
nejsou	být	k5eNaImIp3nP	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nejvhodnější	vhodný	k2eAgInPc1d3	nejvhodnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
vážně	vážně	k6eAd1	vážně
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
těžby	těžba	k1gFnSc2	těžba
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konkrece	konkreko	k6eAd1	konkreko
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
000	[number]	k4	000
m.	m.	k?	m.
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
těžbě	těžba	k1gFnSc6	těžba
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
dokonce	dokonce	k9	dokonce
podílet	podílet	k5eAaImF	podílet
i	i	k9	i
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
pokles	pokles	k1gInSc1	pokles
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
pokles	pokles	k1gInSc1	pokles
jejich	jejich	k3xOp3gFnPc2	jejich
cen	cena	k1gFnPc2	cena
však	však	k9	však
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
výroby	výroba	k1gFnSc2	výroba
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
redukce	redukce	k1gFnPc4	redukce
hliníkem	hliník	k1gInSc7	hliník
nebo	nebo	k8xC	nebo
křemíkem	křemík	k1gInSc7	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
redukci	redukce	k1gFnSc6	redukce
uhlíkem	uhlík	k1gInSc7	uhlík
vznikají	vznikat	k5eAaImIp3nP	vznikat
pouze	pouze	k6eAd1	pouze
karbidy	karbid	k1gInPc1	karbid
Jelikož	jelikož	k8xS	jelikož
většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
manganu	mangan	k1gInSc2	mangan
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
se	se	k3xPyFc4	se
rudy	ruda	k1gFnSc2	ruda
manganu	mangan	k1gInSc2	mangan
hlavně	hlavně	k6eAd1	hlavně
redukcí	redukce	k1gFnSc7	redukce
směsi	směs	k1gFnSc2	směs
MnO	MnO	k1gFnSc2	MnO
<g/>
2	[number]	k4	2
a	a	k8xC	a
FeO	FeO	k1gFnSc6	FeO
<g/>
3	[number]	k4	3
uhlíkem	uhlík	k1gInSc7	uhlík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ferromanganu	ferromangan	k1gInSc2	ferromangan
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
manganu	mangan	k1gInSc2	mangan
kolem	kolem	k7c2	kolem
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slitina	slitina	k1gFnSc1	slitina
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
vyhovující	vyhovující	k2eAgNnSc1d1	vyhovující
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
hutní	hutní	k2eAgNnSc4d1	hutní
zpracování	zpracování	k1gNnSc4	zpracování
při	při	k7c6	při
legování	legování	k1gNnSc6	legování
ocelí	ocel	k1gFnPc2	ocel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
je	být	k5eAaImIp3nS	být
železo	železo	k1gNnSc1	železo
přítomno	přítomen	k2eAgNnSc1d1	přítomno
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
složka	složka	k1gFnSc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
aluminotermicky	aluminotermicky	k6eAd1	aluminotermicky
redukcí	redukce	k1gFnSc7	redukce
kovovým	kovový	k2eAgInSc7d1	kovový
hliníkem	hliník	k1gInSc7	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
burelu	burel	k1gInSc2	burel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
by	by	kYmCp3nS	by
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
reagoval	reagovat	k5eAaBmAgInS	reagovat
příliš	příliš	k6eAd1	příliš
prudce	prudko	k6eAd1	prudko
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
nejprve	nejprve	k6eAd1	nejprve
převést	převést	k5eAaPmF	převést
na	na	k7c6	na
Mn	Mn	k1gFnSc6	Mn
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
klidněji	klidně	k6eAd2	klidně
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
Mn	Mn	k1gFnSc1	Mn
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
s	s	k7c7	s
hliníkem	hliník	k1gInSc7	hliník
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
3	[number]	k4	3
Mn	Mn	k1gFnSc1	Mn
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
+	+	kIx~	+
8	[number]	k4	8
Al	ala	k1gFnPc2	ala
→	→	k?	→
4	[number]	k4	4
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
9	[number]	k4	9
Mn	Mn	k1gFnPc2	Mn
Zvláště	zvláště	k6eAd1	zvláště
čistý	čistý	k2eAgInSc1d1	čistý
mangan	mangan	k1gInSc1	mangan
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
síranu	síran	k1gInSc2	síran
manganatého	manganatý	k2eAgInSc2d1	manganatý
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
světové	světový	k2eAgFnSc2d1	světová
těžby	těžba	k1gFnSc2	těžba
manganu	mangan	k1gInSc2	mangan
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
výroba	výroba	k1gFnSc1	výroba
oceli	ocel	k1gFnSc2	ocel
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
asi	asi	k9	asi
95	[number]	k4	95
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
manganového	manganový	k2eAgInSc2d1	manganový
bronzu	bronz	k1gInSc2	bronz
a	a	k8xC	a
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
a	a	k8xC	a
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Manganistan	manganistan	k1gInSc1	manganistan
draselný	draselný	k2eAgInSc1d1	draselný
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
oxidačními	oxidační	k2eAgFnPc7d1	oxidační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc4	nezávadnost
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
roztoky	roztok	k1gInPc1	roztok
KMnO	KMnO	k1gFnSc2	KMnO
<g/>
4	[number]	k4	4
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
masa	maso	k1gNnSc2	maso
nebo	nebo	k8xC	nebo
syrové	syrový	k2eAgFnSc2d1	syrová
zeleniny	zelenina	k1gFnSc2	zelenina
v	v	k7c6	v
rizikových	rizikový	k2eAgFnPc6d1	riziková
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
dezinfekce	dezinfekce	k1gFnSc2	dezinfekce
roztoky	roztoka	k1gFnSc2	roztoka
manganistanu	manganistan	k1gInSc2	manganistan
je	být	k5eAaImIp3nS	být
vznikající	vznikající	k2eAgInSc1d1	vznikající
tmavý	tmavý	k2eAgInSc1d1	tmavý
burel	burel	k1gInSc1	burel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
brání	bránit	k5eAaImIp3nS	bránit
použití	použití	k1gNnSc4	použití
manganistanu	manganistan	k1gInSc2	manganistan
při	při	k7c6	při
dezinfekci	dezinfekce	k1gFnSc6	dezinfekce
textilií	textilie	k1gFnPc2	textilie
nebo	nebo	k8xC	nebo
bytových	bytový	k2eAgFnPc2d1	bytová
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Oxidačních	oxidační	k2eAgFnPc2d1	oxidační
vlastností	vlastnost	k1gFnPc2	vlastnost
manganistanu	manganistan	k1gInSc2	manganistan
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
směsí	směs	k1gFnPc2	směs
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
kyslíku	kyslík	k1gInSc2	kyslík
pro	pro	k7c4	pro
kontrolované	kontrolovaný	k2eAgNnSc4d1	kontrolované
hoření	hoření	k1gNnSc4	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
manganatý	manganatý	k2eAgInSc1d1	manganatý
a	a	k8xC	a
chlorid	chlorid	k1gInSc1	chlorid
manganatý	manganatý	k2eAgInSc1d1	manganatý
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
barvířství	barvířství	k1gNnSc6	barvířství
<g/>
,	,	kIx,	,
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
tkanin	tkanina	k1gFnPc2	tkanina
a	a	k8xC	a
k	k	k7c3	k
moření	moření	k1gNnSc3	moření
osiva	osivo	k1gNnSc2	osivo
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
manganatý	manganatý	k2eAgInSc1d1	manganatý
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sikativ	sikativ	k1gInSc4	sikativ
pro	pro	k7c4	pro
fermeže	fermež	k1gFnPc4	fermež
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
manganu	mangan	k1gInSc2	mangan
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
ještě	ještě	k9	ještě
některé	některý	k3yIgFnPc1	některý
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
malířské	malířský	k2eAgFnPc1d1	malířská
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přírodním	přírodní	k2eAgFnPc3d1	přírodní
barvám	barva	k1gFnPc3	barva
manganu	mangan	k1gInSc2	mangan
patří	patřit	k5eAaImIp3nP	patřit
umbra	umbra	k1gFnSc1	umbra
a	a	k8xC	a
k	k	k7c3	k
umělým	umělý	k2eAgFnPc3d1	umělá
manganová	manganový	k2eAgFnSc1d1	manganová
hněď	hněď	k1gFnSc1	hněď
(	(	kIx(	(
<g/>
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
uhličitan	uhličitan	k1gInSc1	uhličitan
manganatý	manganatý	k2eAgInSc1d1	manganatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manganová	manganový	k2eAgFnSc1d1	manganová
běloba	běloba	k1gFnSc1	běloba
(	(	kIx(	(
<g/>
uhličitan	uhličitan	k1gInSc1	uhličitan
manganatý	manganatý	k2eAgInSc1d1	manganatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manganová	manganový	k2eAgFnSc1d1	manganová
zeleň	zeleň	k1gFnSc1	zeleň
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
kasselská	kasselský	k2eAgFnSc1d1	Kasselská
zeleň	zeleň	k1gFnSc1	zeleň
<g/>
)	)	kIx)	)
a	a	k8xC	a
permanentní	permanentní	k2eAgFnSc4d1	permanentní
violeť	violeť	k1gFnSc4	violeť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ocelářském	ocelářský	k2eAgInSc6d1	ocelářský
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nS	sloužit
mangan	mangan	k1gInSc1	mangan
především	především	k6eAd1	především
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
tavbě	tavba	k1gFnSc6	tavba
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
váže	vázat	k5eAaImIp3nS	vázat
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
z	z	k7c2	z
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
oceli	ocel	k1gFnSc2	ocel
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
tedy	tedy	k9	tedy
jako	jako	k9	jako
desulfurační	desulfurační	k2eAgFnSc1d1	desulfurační
a	a	k8xC	a
deoxidační	deoxidační	k2eAgFnSc1d1	deoxidační
přísada	přísada	k1gFnSc1	přísada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převede	převést	k5eAaPmIp3nS	převést
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
S	s	k7c7	s
a	a	k8xC	a
O	o	k7c6	o
do	do	k7c2	do
strusky	struska	k1gFnSc2	struska
a	a	k8xC	a
vyčistí	vyčistit	k5eAaPmIp3nS	vyčistit
tak	tak	k6eAd1	tak
taveninu	tavenina	k1gFnSc4	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
tavby	tavba	k1gFnSc2	tavba
však	však	k9	však
v	v	k7c6	v
oceli	ocel	k1gFnSc6	ocel
vždy	vždy	k6eAd1	vždy
určité	určitý	k2eAgNnSc1d1	určité
procento	procento	k1gNnSc1	procento
elementárního	elementární	k2eAgInSc2d1	elementární
manganu	mangan	k1gInSc2	mangan
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
nezreagovaný	zreagovaný	k2eNgInSc1d1	nezreagovaný
přebytek	přebytek	k1gInSc1	přebytek
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
S	s	k7c7	s
a	a	k8xC	a
O	o	k0	o
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
záměrně	záměrně	k6eAd1	záměrně
vyšší	vysoký	k2eAgInSc1d2	vyšší
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
jiných	jiný	k2eAgFnPc2d1	jiná
mechanických	mechanický	k2eAgFnPc2d1	mechanická
vlastností	vlastnost	k1gFnPc2	vlastnost
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
manganu	mangan	k1gInSc2	mangan
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
oceli	ocel	k1gFnSc2	ocel
vždy	vždy	k6eAd1	vždy
jako	jako	k8xC	jako
základní	základní	k2eAgFnSc4d1	základní
složku	složka	k1gFnSc4	složka
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc4	chrom
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
nikl	niknout	k5eAaImAgMnS	niknout
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
slitinou	slitina	k1gFnSc7	slitina
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
ferromangan	ferromangan	k1gInSc1	ferromangan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
slitina	slitina	k1gFnSc1	slitina
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
silikomangan	silikomangan	k1gInSc1	silikomangan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
a	a	k8xC	a
zrcadlovina	zrcadlovina	k1gFnSc1	zrcadlovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
mimořádně	mimořádně	k6eAd1	mimořádně
důležitou	důležitý	k2eAgFnSc7d1	důležitá
slitinou	slitina	k1gFnSc7	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
dural	dural	k1gInSc1	dural
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
označuje	označovat	k5eAaImIp3nS	označovat
skupinu	skupina	k1gFnSc4	skupina
velmi	velmi	k6eAd1	velmi
lehkých	lehký	k2eAgFnPc2d1	lehká
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
odolných	odolný	k2eAgFnPc2d1	odolná
slitin	slitina	k1gFnPc2	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
hořčíku	hořčík	k1gInSc2	hořčík
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Heuslerovy	Heuslerův	k2eAgFnPc1d1	Heuslerův
slitiny	slitina	k1gFnPc1	slitina
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
Friedrich	Friedrich	k1gMnSc1	Friedrich
Heusler	Heusler	k1gMnSc1	Heusler
<g/>
.	.	kIx.	.
</s>
<s>
Heusler	Heusler	k1gMnSc1	Heusler
totiž	totiž	k9	totiž
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mangan	mangan	k1gInSc1	mangan
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
kovy	kov	k1gInPc7	kov
–	–	k?	–
například	například	k6eAd1	například
hliníkem	hliník	k1gInSc7	hliník
<g/>
,	,	kIx,	,
cínem	cín	k1gInSc7	cín
nebo	nebo	k8xC	nebo
antimonem	antimon	k1gInSc7	antimon
–	–	k?	–
slitiny	slitina	k1gFnSc2	slitina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ferromagnetické	ferromagnetický	k2eAgFnPc1d1	ferromagnetická
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ferromagnetický	ferromagnetický	k2eAgInSc4d1	ferromagnetický
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
slitinách	slitina	k1gFnPc6	slitina
vznikají	vznikat	k5eAaImIp3nP	vznikat
intermetalické	intermetalický	k2eAgFnPc1d1	intermetalická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
směsné	směsný	k2eAgInPc1d1	směsný
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
manganu	mangan	k1gInSc2	mangan
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
může	moct	k5eAaImIp3nS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
jasnost	jasnost	k1gFnSc4	jasnost
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
zelenavý	zelenavý	k2eAgInSc1d1	zelenavý
nádech	nádech	k1gInSc1	nádech
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
stopy	stopa	k1gFnPc4	stopa
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
keramických	keramický	k2eAgInPc2d1	keramický
materiálů	materiál	k1gInPc2	materiál
nebo	nebo	k8xC	nebo
porcelánu	porcelán	k1gInSc2	porcelán
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
glazování	glazování	k1gNnSc1	glazování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
vypálený	vypálený	k2eAgInSc1d1	vypálený
střep	střep	k1gInSc1	střep
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
tekuté	tekutý	k2eAgFnSc2d1	tekutá
glazury	glazura	k1gFnSc2	glazura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
barvicí	barvicí	k2eAgInPc4d1	barvicí
pigmenty	pigment	k1gInPc4	pigment
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většinou	většina	k1gFnSc7	většina
soli	sůl	k1gFnSc2	sůl
různých	různý	k2eAgInPc2d1	různý
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Opětným	opětný	k2eAgNnSc7d1	opětné
vypálením	vypálení	k1gNnSc7	vypálení
předmětu	předmět	k1gInSc2	předmět
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
se	se	k3xPyFc4	se
glazura	glazura	k1gFnSc1	glazura
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
různých	různý	k2eAgInPc2d1	různý
směsných	směsný	k2eAgInPc2d1	směsný
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
křemičitanů	křemičitan	k1gInPc2	křemičitan
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trvale	trvale	k6eAd1	trvale
zbarví	zbarvit	k5eAaPmIp3nP	zbarvit
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
solemi	sůl	k1gFnPc7	sůl
manganu	mangan	k1gInSc2	mangan
se	se	k3xPyFc4	se
do	do	k7c2	do
glazur	glazura	k1gFnPc2	glazura
přidávají	přidávat	k5eAaImIp3nP	přidávat
obvykle	obvykle	k6eAd1	obvykle
sloučeniny	sloučenina	k1gFnPc4	sloučenina
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
výsledným	výsledný	k2eAgInSc7d1	výsledný
efektem	efekt	k1gInSc7	efekt
je	být	k5eAaImIp3nS	být
hnědé	hnědý	k2eAgNnSc4d1	hnědé
až	až	k8xS	až
červeno-hnědé	červenonědý	k2eAgNnSc4d1	červeno-hnědé
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
komerčně	komerčně	k6eAd1	komerčně
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
elektrický	elektrický	k2eAgInSc1d1	elektrický
galvanický	galvanický	k2eAgInSc1d1	galvanický
článek	článek	k1gInSc1	článek
(	(	kIx(	(
<g/>
baterie	baterie	k1gFnSc1	baterie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
zinkové	zinkový	k2eAgFnSc2d1	zinková
katody	katoda	k1gFnSc2	katoda
a	a	k8xC	a
anody	anoda	k1gFnSc2	anoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořil	tvořit	k5eAaImAgInS	tvořit
grafitový	grafitový	k2eAgInSc1d1	grafitový
váleček	váleček	k1gInSc1	váleček
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
pastě	pasta	k1gFnSc6	pasta
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
(	(	kIx(	(
<g/>
burel	burel	k1gInSc1	burel
<g/>
)	)	kIx)	)
MnO	MnO	k1gFnSc1	MnO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
napětí	napětí	k1gNnSc4	napětí
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
V	V	kA	V
a	a	k8xC	a
při	při	k7c6	při
odběru	odběr	k1gInSc6	odběr
proudu	proud	k1gInSc2	proud
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
elementárního	elementární	k2eAgInSc2d1	elementární
zinku	zinek	k1gInSc2	zinek
na	na	k7c6	na
Zn	zn	kA	zn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
redukci	redukce	k1gFnSc3	redukce
čtyřmocného	čtyřmocný	k2eAgInSc2d1	čtyřmocný
manganu	mangan	k1gInSc2	mangan
na	na	k7c6	na
Mn	Mn	k1gFnSc6	Mn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
desetiletí	desetiletí	k1gNnPc2	desetiletí
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
jinými	jiný	k2eAgInPc7d1	jiný
typy	typ	k1gInPc7	typ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
výkon	výkon	k1gInSc4	výkon
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
nehrozí	hrozit	k5eNaImIp3nS	hrozit
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
riziko	riziko	k1gNnSc1	riziko
korozního	korozní	k2eAgNnSc2d1	korozní
zničení	zničení	k1gNnSc2	zničení
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
články	článek	k1gInPc4	článek
obsahující	obsahující	k2eAgInSc4d1	obsahující
burel	burel	k1gInSc4	burel
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
komerčně	komerčně	k6eAd1	komerčně
využívají	využívat	k5eAaPmIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
sloučenin	sloučenina	k1gFnPc2	sloučenina
manganu	mangan	k1gInSc2	mangan
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
sloučeniny	sloučenina	k1gFnPc1	sloučenina
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Mn	Mn	k1gFnSc2	Mn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
a	a	k8xC	a
Mn	Mn	k1gMnSc1	Mn
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sloučenin	sloučenina	k1gFnPc2	sloučenina
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
toxická	toxický	k2eAgFnSc1d1	toxická
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgNnSc1d1	barevné
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
dvojmocného	dvojmocný	k2eAgInSc2d1	dvojmocný
manganu	mangan	k1gInSc2	mangan
Mn	Mn	k1gFnSc2	Mn
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
narůžovělé	narůžovělý	k2eAgNnSc1d1	narůžovělé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
kyseliny	kyselina	k1gFnSc2	kyselina
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc1	hydroxid
manganatý	manganatý	k2eAgInSc1d1	manganatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
neutrálních	neutrální	k2eAgInPc6d1	neutrální
roztocích	roztok	k1gInPc6	roztok
při	při	k7c6	při
delším	dlouhý	k2eAgNnSc6d2	delší
stání	stání	k1gNnSc6	stání
nejsou	být	k5eNaImIp3nP	být
manganaté	manganatý	k2eAgFnPc1d1	manganatý
soli	sůl	k1gFnPc1	sůl
úplně	úplně	k6eAd1	úplně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
se	se	k3xPyFc4	se
na	na	k7c4	na
soli	sůl	k1gFnPc4	sůl
manganité	manganitý	k2eAgNnSc4d1	manganitý
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
manganičitý	manganičitý	k2eAgInSc4d1	manganičitý
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
manganaté	manganatý	k2eAgFnPc1d1	manganatý
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
také	také	k9	také
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
manganatý	manganatý	k2eAgInSc1d1	manganatý
MnO	MnO	k1gFnSc4	MnO
je	být	k5eAaImIp3nS	být
zelenošedý	zelenošedý	k2eAgInSc1d1	zelenošedý
až	až	k9	až
trávově	trávově	k6eAd1	trávově
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Jemně	jemně	k6eAd1	jemně
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
prášek	prášek	k1gInSc1	prášek
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
manganatý	manganatý	k2eAgInSc4d1	manganatý
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
jako	jako	k8xC	jako
nerost	nerost	k1gInSc4	nerost
manganosit	manganosit	k5eAaPmF	manganosit
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
manganatý	manganatý	k2eAgMnSc1d1	manganatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
redukcí	redukce	k1gFnSc7	redukce
vyšších	vysoký	k2eAgInPc2d2	vyšší
oxidů	oxid	k1gInPc2	oxid
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
bílá	bílý	k2eAgFnSc1d1	bílá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
hnědne	hnědnout	k5eAaImIp3nS	hnědnout
až	až	k6eAd1	až
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
pyrochroit	pyrochroit	k5eAaPmF	pyrochroit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
manganatých	manganatý	k2eAgMnPc2d1	manganatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
hydroxidem	hydroxid	k1gInSc7	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnCl	MnCl	k1gInSc4	MnCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
chloridy	chlorid	k1gInPc7	chlorid
tvoří	tvořit	k5eAaImIp3nP	tvořit
adiční	adiční	k2eAgFnSc1d1	adiční
i	i	k8xC	i
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
manganatý	manganatý	k2eAgMnSc1d1	manganatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
nebo	nebo	k8xC	nebo
spálením	spálení	k1gNnPc3	spálení
kovového	kovový	k2eAgInSc2d1	kovový
manganu	mangan	k1gInSc2	mangan
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnBr	MnBr	k1gInSc4	MnBr
<g/>
2	[number]	k4	2
a	a	k8xC	a
jodid	jodid	k1gInSc1	jodid
manganatý	manganatý	k2eAgInSc1d1	manganatý
MnI	mnout	k5eAaImRp2nS	mnout
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
narůžovělé	narůžovělý	k2eAgFnPc1d1	narůžovělá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
popř.	popř.	kA	popř.
kyselině	kyselina	k1gFnSc6	kyselina
jodovodíkové	jodovodíkový	k2eAgFnSc6d1	jodovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnF	MnF	k1gFnSc7	MnF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
a	a	k8xC	a
komplexní	komplexní	k2eAgFnPc4d1	komplexní
soli	sůl	k1gFnPc4	sůl
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
s	s	k7c7	s
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnSO	MnSO	k1gFnSc7	MnSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
téměř	téměř	k6eAd1	téměř
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
krásně	krásně	k6eAd1	krásně
růžový	růžový	k2eAgInSc1d1	růžový
pentahydrát	pentahydrát	k1gInSc1	pentahydrát
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
manganatá	manganatý	k2eAgFnSc1d1	manganatý
skalice	skalice	k1gFnSc1	skalice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výchozí	výchozí	k2eAgFnSc1d1	výchozí
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
elektrolytickou	elektrolytický	k2eAgFnSc4d1	elektrolytická
přípravu	příprava	k1gFnSc4	příprava
čistého	čistý	k2eAgInSc2d1	čistý
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgNnSc1d1	sírové
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
železnatým	železnatý	k2eAgInSc7d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnCO	MnCO	k1gFnSc7	MnCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
se	se	k3xPyFc4	se
obyčejně	obyčejně	k6eAd1	obyčejně
sráží	srážet	k5eAaImIp3nS	srážet
hnědý	hnědý	k2eAgInSc1d1	hnědý
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
uhličitan	uhličitan	k1gInSc1	uhličitan
manganatý	manganatý	k2eAgInSc1d1	manganatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
minerál	minerál	k1gInSc1	minerál
dialogit	dialogit	k1gInSc1	dialogit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
srážením	srážení	k1gNnSc7	srážení
manganatých	manganatý	k2eAgMnPc2d1	manganatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
uhličitanem	uhličitan	k1gInSc7	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
tvořící	tvořící	k2eAgFnPc1d1	tvořící
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
manganatý	manganatý	k2eAgMnSc1d1	manganatý
MnS	MnS	k1gMnSc1	MnS
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
růžová	růžový	k2eAgFnSc1d1	růžová
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
přechází	přecházet	k5eAaImIp3nS	přecházet
ve	v	k7c4	v
stálejší	stálý	k2eAgFnSc4d2	stálejší
zelenou	zelený	k2eAgFnSc4d1	zelená
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
alabandin	alabandin	k1gInSc1	alabandin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
disulfid	disulfid	k1gInSc4	disulfid
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnS	MnS	k1gFnSc7	MnS
<g/>
2	[number]	k4	2
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
hauerit	hauerit	k1gInSc1	hauerit
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
manganatý	manganatý	k2eAgMnSc1d1	manganatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
manganatých	manganatý	k2eAgMnPc2d1	manganatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
suflidem	suflid	k1gInSc7	suflid
nebo	nebo	k8xC	nebo
kyselinou	kyselina	k1gFnSc7	kyselina
sirovodíkovou	sirovodíkový	k2eAgFnSc7d1	sirovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Stálejší	stálý	k2eAgMnPc1d2	stálejší
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgFnPc4d1	komplexní
soli	sůl	k1gFnPc4	sůl
kyanidu	kyanid	k1gInSc2	kyanid
manganatého	manganatý	k2eAgInSc2d1	manganatý
–	–	k?	–
kyanomanganatany	kyanomanganatan	k1gInPc1	kyanomanganatan
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgNnPc6	který
je	být	k5eAaImIp3nS	být
pojednáno	pojednán	k2eAgNnSc1d1	pojednáno
níže	nízce	k6eAd2	nízce
v	v	k7c6	v
komplexních	komplexní	k2eAgFnPc6d1	komplexní
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dihydrátu	dihydrát	k1gInSc2	dihydrát
a	a	k8xC	a
trihydrátu	trihydrát	k1gInSc2	trihydrát
žlutá	žlutat	k5eAaImIp3nS	žlutat
a	a	k8xC	a
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tetrahydrátu	tetrahydrát	k1gInSc2	tetrahydrát
zelená	zelenat	k5eAaImIp3nS	zelenat
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
tetrahydrátu	tetrahydrát	k1gInSc2	tetrahydrát
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
při	při	k7c6	při
zřeďování	zřeďování	k1gNnSc6	zřeďování
roztoku	roztok	k1gInSc2	roztok
mění	měnit	k5eAaImIp3nS	měnit
barva	barva	k1gFnSc1	barva
roztoku	roztok	k1gInSc2	roztok
ze	z	k7c2	z
zelené	zelená	k1gFnSc2	zelená
do	do	k7c2	do
růžové	růžový	k2eAgFnSc2d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
manganatý	manganatý	k2eAgInSc1d1	manganatý
tvoří	tvořit	k5eAaImIp3nS	tvořit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
pojednáno	pojednán	k2eAgNnSc1d1	pojednáno
níže	nízce	k6eAd2	nízce
v	v	k7c6	v
komplexních	komplexní	k2eAgFnPc6d1	komplexní
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
manganatý	manganatý	k2eAgMnSc1d1	manganatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
thiokyanaté	thiokyanatý	k2eAgFnSc6d1	thiokyanatý
<g/>
.	.	kIx.	.
</s>
<s>
Manganité	Manganitý	k2eAgFnPc1d1	Manganitý
soli	sůl	k1gFnPc1	sůl
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
mangan	mangan	k1gInSc4	mangan
úplně	úplně	k6eAd1	úplně
typické	typický	k2eAgNnSc4d1	typické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
redukcí	redukce	k1gFnPc2	redukce
manganičitých	manganičitý	k2eAgFnPc2d1	manganičitý
sloučenin	sloučenina	k1gFnPc2	sloučenina
nebo	nebo	k8xC	nebo
oxidací	oxidace	k1gFnPc2	oxidace
manganatých	manganatý	k2eAgFnPc2d1	manganatý
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Manganité	Manganitý	k2eAgFnPc1d1	Manganitý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
ani	ani	k8xC	ani
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
nerozkládají	rozkládat	k5eNaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
temně	temně	k6eAd1	temně
zbarveny	zbarven	k2eAgFnPc1d1	zbarvena
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
–	–	k?	–
někdy	někdy	k6eAd1	někdy
nejde	jít	k5eNaImIp3nS	jít
soli	sůl	k1gFnPc4	sůl
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
manganitý	manganitý	k2eAgInSc4d1	manganitý
Mn	Mn	k1gFnSc7	Mn
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
amorfní	amorfní	k2eAgInSc1d1	amorfní
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
braunit	braunit	k5eAaPmF	braunit
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
pražením	pražení	k1gNnSc7	pražení
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
žíháním	žíhání	k1gNnSc7	žíhání
manganatých	manganatý	k2eAgFnPc2d1	manganatý
solí	sůl	k1gFnPc2	sůl
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Manganová	manganový	k2eAgFnSc1d1	manganová
hněď	hněď	k1gFnSc1	hněď
je	být	k5eAaImIp3nS	být
hydratovaný	hydratovaný	k2eAgInSc4d1	hydratovaný
oxid	oxid	k1gInSc4	oxid
manganitý	manganitý	k2eAgInSc4d1	manganitý
Mn	Mn	k1gFnSc7	Mn
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
nH	nH	k?	nH
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
černohnědou	černohnědý	k2eAgFnSc4d1	černohnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
přidáváním	přidávání	k1gNnSc7	přidávání
chlorového	chlorový	k2eAgNnSc2d1	chlorové
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
vápenaté	vápenatý	k2eAgFnSc2d1	vápenatá
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
roztokům	roztok	k1gInPc3	roztok
chloridu	chlorid	k1gInSc2	chlorid
manganatého	manganatý	k2eAgMnSc2d1	manganatý
<g/>
.	.	kIx.	.
</s>
<s>
Kaštanově	kaštanově	k6eAd1	kaštanově
hnědá	hnědat	k5eAaImIp3nS	hnědat
barva	barva	k1gFnSc1	barva
připravená	připravený	k2eAgFnSc1d1	připravená
mletím	mletí	k1gNnSc7	mletí
a	a	k8xC	a
pálením	pálení	k1gNnSc7	pálení
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
manganitého	manganitý	k2eAgInSc2d1	manganitý
<g/>
,	,	kIx,	,
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
železnatého	železnatý	k2eAgInSc2d1	železnatý
a	a	k8xC	a
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
umbra	umbra	k1gFnSc1	umbra
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
manganitý	manganitý	k2eAgInSc4d1	manganitý
MnCl	MnCl	k1gInSc4	MnCl
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
temně	temně	k6eAd1	temně
hnědém	hnědý	k2eAgInSc6d1	hnědý
roztoku	roztok	k1gInSc6	roztok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
působením	působení	k1gNnSc7	působení
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
manganičitý	manganičitý	k2eAgInSc4d1	manganičitý
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jej	on	k3xPp3gInSc4	on
vyloučit	vyloučit	k5eAaPmF	vyloučit
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
komplexní	komplexní	k2eAgFnPc4d1	komplexní
soli	sůl	k1gFnPc4	sůl
–	–	k?	–
chloromanganitany	chloromanganitan	k1gInPc4	chloromanganitan
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
manganitý	manganitý	k2eAgInSc4d1	manganitý
MnF	MnF	k1gFnSc7	MnF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
rubínově	rubínově	k6eAd1	rubínově
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
manganitý	manganitý	k2eAgMnSc1d1	manganitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
jodid	jodid	k1gInSc4	jodid
manganatý	manganatý	k2eAgInSc4d1	manganatý
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
manganitý	manganitý	k2eAgInSc4d1	manganitý
Mn	Mn	k1gFnSc7	Mn
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nS	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
kamence	kamenec	k1gInPc4	kamenec
<g/>
)	)	kIx)	)
s	s	k7c7	s
komplexním	komplexní	k2eAgInSc7d1	komplexní
kationem	kation	k1gInSc7	kation
[	[	kIx(	[
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
a	a	k8xC	a
disulfatomanganitany	disulfatomanganitan	k1gInPc1	disulfatomanganitan
s	s	k7c7	s
komplexním	komplexní	k2eAgInSc7d1	komplexní
anionem	anion	k1gInSc7	anion
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
oxidu	oxid	k1gInSc2	oxid
manganitého	manganitý	k2eAgInSc2d1	manganitý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
manganitého	manganitý	k2eAgInSc2d1	manganitý
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
Mn	Mn	k1gFnSc2	Mn
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc1d3	veliký
praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
burel	burel	k1gInSc1	burel
<g/>
.	.	kIx.	.
</s>
<s>
Burel	burel	k1gInSc1	burel
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
manganičité	manganičitý	k2eAgFnPc1d1	manganičitý
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
pouze	pouze	k6eAd1	pouze
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
stabilním	stabilní	k2eAgFnPc3d1	stabilní
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
komplexní	komplexní	k2eAgInPc4d1	komplexní
manganičitany	manganičitan	k1gInPc4	manganičitan
–	–	k?	–
acidomanganičitany	acidomanganičitan	k1gInPc4	acidomanganičitan
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
manganičitý	manganičitý	k2eAgInSc1d1	manganičitý
neboli	neboli	k8xC	neboli
burel	burel	k1gInSc1	burel
MnO	MnO	k1gFnSc2	MnO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
prášek	prášek	k1gInSc1	prášek
se	s	k7c7	s
slabě	slabě	k6eAd1	slabě
oxidačními	oxidační	k2eAgFnPc7d1	oxidační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
pyrolusit	pyrolusit	k1gInSc1	pyrolusit
<g/>
,	,	kIx,	,
polianit	polianit	k1gInSc1	polianit
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc1d1	šedý
burel	burel	k1gInSc1	burel
<g/>
,	,	kIx,	,
psilomelan	psilomelan	k1gInSc1	psilomelan
<g/>
,	,	kIx,	,
waad	waad	k1gInSc1	waad
neboli	neboli	k8xC	neboli
manganová	manganový	k2eAgFnSc1d1	manganová
pěna	pěna	k1gFnSc1	pěna
a	a	k8xC	a
manganová	manganový	k2eAgFnSc1d1	manganová
čerň	čerň	k1gFnSc1	čerň
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
připravený	připravený	k2eAgInSc4d1	připravený
burel	burel	k1gInSc4	burel
s	s	k7c7	s
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
uplatnění	uplatnění	k1gNnSc1	uplatnění
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
galvanických	galvanický	k2eAgInPc2d1	galvanický
článků	článek	k1gInPc2	článek
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
v	v	k7c6	v
laboratorním	laboratorní	k2eAgNnSc6d1	laboratorní
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
činidlo	činidlo	k1gNnSc1	činidlo
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
plynného	plynný	k2eAgInSc2d1	plynný
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Hydratovaný	hydratovaný	k2eAgInSc1d1	hydratovaný
oxid	oxid	k1gInSc1	oxid
manganičitý	manganičitý	k2eAgInSc1d1	manganičitý
je	být	k5eAaImIp3nS	být
hnědá	hnědat	k5eAaImIp3nS	hnědat
až	až	k9	až
načernalá	načernalý	k2eAgFnSc1d1	načernalá
<g/>
,	,	kIx,	,
amfoterní	amfoterní	k2eAgFnSc1d1	amfoterní
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
oxidací	oxidace	k1gFnSc7	oxidace
manganatých	manganatý	k2eAgInPc2d1	manganatý
iontů	ion	k1gInPc2	ion
nebo	nebo	k8xC	nebo
redukcí	redukce	k1gFnPc2	redukce
mangananů	manganan	k1gInPc2	manganan
či	či	k8xC	či
manganistanů	manganistan	k1gInPc2	manganistan
<g/>
.	.	kIx.	.
</s>
<s>
Manganičitan	Manganičitan	k1gInSc4	Manganičitan
manganatý	manganatý	k2eAgInSc4d1	manganatý
neboli	neboli	k8xC	neboli
oxid	oxid	k1gInSc4	oxid
manganato-manganičitý	manganatoanganičitý	k2eAgInSc4d1	manganato-manganičitý
Mn	Mn	k1gFnSc7	Mn
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
hausmannit	hausmannit	k5eAaImF	hausmannit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
manganitého	manganitý	k2eAgInSc2d1	manganitý
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
manganičitý	manganičitý	k2eAgInSc1d1	manganičitý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc1d1	stabilní
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
manganičitý	manganičitý	k2eAgInSc4d1	manganičitý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
oxidací	oxidace	k1gFnSc7	oxidace
síranu	síran	k1gInSc2	síran
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
manganistanem	manganistan	k1gInSc7	manganistan
draselným	draselný	k2eAgInSc7d1	draselný
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
tavení	tavení	k1gNnSc6	tavení
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
s	s	k7c7	s
dusičnanem	dusičnan	k1gInSc7	dusičnan
a	a	k8xC	a
uhličitanem	uhličitan	k1gInSc7	uhličitan
sodným	sodný	k2eAgInSc7d1	sodný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
modré	modrý	k2eAgFnPc1d1	modrá
manganičnany	manganičnana	k1gFnPc1	manganičnana
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Manganičnany	Manganičnana	k1gFnPc1	Manganičnana
lze	lze	k6eAd1	lze
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
žíháním	žíhání	k1gNnSc7	žíhání
libovolného	libovolný	k2eAgInSc2d1	libovolný
oxidu	oxid	k1gInSc2	oxid
manganu	mangan	k1gInSc2	mangan
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
barnatým	barnatý	k2eAgInSc7d1	barnatý
při	při	k7c6	při
900	[number]	k4	900
°	°	k?	°
<g/>
C.	C.	kA	C.
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
manganičnany	manganičnana	k1gFnPc1	manganičnana
používány	používat	k5eAaImNgFnP	používat
jako	jako	k8xS	jako
nátěrová	nátěrový	k2eAgFnSc1d1	nátěrová
barva	barva	k1gFnSc1	barva
–	–	k?	–
manganová	manganový	k2eAgFnSc1d1	manganová
modř	modř	k1gFnSc1	modř
<g/>
.	.	kIx.	.
</s>
<s>
Manganové	manganový	k2eAgFnPc1d1	manganová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
oxidovat	oxidovat	k5eAaBmF	oxidovat
na	na	k7c4	na
manganisté	manganistý	k2eAgFnPc4d1	manganistý
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
manganany	manganan	k1gInPc1	manganan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
sytě	sytě	k6eAd1	sytě
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
štěpí	štěpit	k5eAaImIp3nP	štěpit
na	na	k7c4	na
manganistan	manganistan	k1gInSc4	manganistan
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
manganičitý	manganičitý	k2eAgInSc4d1	manganičitý
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
manganistany	manganistan	k1gInPc4	manganistan
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
silná	silný	k2eAgNnPc4d1	silné
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Manganany	Manganana	k1gFnPc1	Manganana
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
s	s	k7c7	s
dusičnanem	dusičnan	k1gInSc7	dusičnan
nebo	nebo	k8xC	nebo
žíháním	žíhání	k1gNnSc7	žíhání
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
manganičitým	manganičitý	k2eAgInSc7d1	manganičitý
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
sedmimocného	sedmimocný	k2eAgInSc2d1	sedmimocný
manganu	mangan	k1gInSc2	mangan
–	–	k?	–
manganistany	manganistan	k1gInPc4	manganistan
<g/>
.	.	kIx.	.
</s>
<s>
Manganistany	manganistan	k1gInPc1	manganistan
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
oxidací	oxidace	k1gFnSc7	oxidace
mangananů	manganan	k1gInPc2	manganan
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
manganistý	manganistý	k2eAgInSc4d1	manganistý
Mn	Mn	k1gFnSc7	Mn
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgInSc4d1	těžký
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc4d1	tmavý
se	se	k3xPyFc4	se
zelenožlutým	zelenožlutý	k2eAgInSc7d1	zelenožlutý
leskem	lesk	k1gInSc7	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
vybuchuje	vybuchovat	k5eAaImIp3nS	vybuchovat
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
manganičitý	manganičitý	k2eAgInSc1d1	manganičitý
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
manganisté	manganistý	k2eAgFnSc2d1	manganistý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
dehydratací	dehydratace	k1gFnSc7	dehydratace
manganistanu	manganistan	k1gInSc2	manganistan
draselného	draselný	k2eAgInSc2d1	draselný
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
manganistá	manganistý	k2eAgFnSc1d1	manganistý
HMnO	HMnO	k1gFnSc1	HMnO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
má	mít	k5eAaImIp3nS	mít
fialovou	fialový	k2eAgFnSc4d1	fialová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
kyselina	kyselina	k1gFnSc1	kyselina
podobná	podobný	k2eAgFnSc1d1	podobná
kyselině	kyselina	k1gFnSc3	kyselina
chloristé	chloristý	k2eAgFnPc1d1	chloristá
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
manganistá	manganistý	k2eAgFnSc1d1	manganistý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
manganistého	manganistý	k2eAgInSc2d1	manganistý
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
směsi	směs	k1gFnSc2	směs
manganaté	manganatý	k2eAgFnSc2d1	manganatý
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
olovičitým	olovičitý	k2eAgInSc7d1	olovičitý
v	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Manganistan	manganistan	k1gInSc4	manganistan
draselný	draselný	k2eAgInSc4d1	draselný
KMnO	KMnO	k1gFnSc7	KMnO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
hypermangan	hypermangan	k1gInSc1	hypermangan
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
jsou	být	k5eAaImIp3nP	být
roztoky	roztok	k1gInPc1	roztok
KMnO	KMnO	k1gFnSc2	KMnO
<g/>
4	[number]	k4	4
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
oxidimetrických	oxidimetrický	k2eAgNnPc2d1	oxidimetrický
činidel	činidlo	k1gNnPc2	činidlo
pro	pro	k7c4	pro
redox-titrace	redoxitrace	k1gFnPc4	redox-titrace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
titraci	titrace	k1gFnSc6	titrace
je	být	k5eAaImIp3nS	být
analyzovaná	analyzovaný	k2eAgFnSc1d1	analyzovaná
látka	látka	k1gFnSc1	látka
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vzorku	vzorek	k1gInSc2	vzorek
kvantitativně	kvantitativně	k6eAd1	kvantitativně
oxidována	oxidovat	k5eAaBmNgFnS	oxidovat
postupnými	postupný	k2eAgInPc7d1	postupný
přesnými	přesný	k2eAgInPc7d1	přesný
přídavky	přídavek	k1gInPc7	přídavek
roztoku	roztok	k1gInSc2	roztok
manganistanu	manganistan	k1gInSc2	manganistan
do	do	k7c2	do
definovaného	definovaný	k2eAgNnSc2d1	definované
stádia	stádium	k1gNnSc2	stádium
a	a	k8xC	a
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
vzorek	vzorek	k1gInSc1	vzorek
zoxidován	zoxidován	k2eAgInSc1d1	zoxidován
<g/>
,	,	kIx,	,
odečte	odečíst	k5eAaPmIp3nS	odečíst
se	se	k3xPyFc4	se
objem	objem	k1gInSc1	objem
spotřebovaného	spotřebovaný	k2eAgInSc2d1	spotřebovaný
titračního	titrační	k2eAgInSc2d1	titrační
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
údaje	údaj	k1gInSc2	údaj
lze	lze	k6eAd1	lze
spočíst	spočíst	k5eAaPmF	spočíst
množství	množství	k1gNnSc4	množství
analyzované	analyzovaný	k2eAgFnSc2d1	analyzovaná
látky	látka	k1gFnSc2	látka
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
uvedené	uvedený	k2eAgFnSc2d1	uvedená
rovnováhy	rovnováha	k1gFnSc2	rovnováha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
bod	bod	k1gInSc1	bod
ekvivalence	ekvivalence	k1gFnSc2	ekvivalence
a	a	k8xC	a
dosažení	dosažení	k1gNnSc4	dosažení
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
indikováno	indikovat	k5eAaBmNgNnS	indikovat
obvykle	obvykle	k6eAd1	obvykle
potenciometricky	potenciometricky	k6eAd1	potenciometricky
nebo	nebo	k8xC	nebo
i	i	k9	i
vizuálně	vizuálně	k6eAd1	vizuálně
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vhodného	vhodný	k2eAgInSc2d1	vhodný
indikátoru	indikátor	k1gInSc2	indikátor
<g/>
.	.	kIx.	.
</s>
<s>
Manganistan	manganistan	k1gInSc1	manganistan
amonný	amonný	k2eAgInSc1d1	amonný
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
MnO	MnO	k1gFnSc1	MnO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
explozivní	explozivní	k2eAgFnSc1d1	explozivní
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
nad	nad	k7c7	nad
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
či	či	k8xC	či
při	při	k7c6	při
tření	tření	k1gNnSc6	tření
krystalů	krystal	k1gInPc2	krystal
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
explozi	exploze	k1gFnSc3	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgInSc2d1	manganičitý
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Manganistan	manganistan	k1gInSc1	manganistan
sodný	sodný	k2eAgInSc1d1	sodný
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
podobná	podobný	k2eAgFnSc1d1	podobná
na	na	k7c4	na
manganistan	manganistan	k1gInSc4	manganistan
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
reaktivnější	reaktivní	k2eAgNnSc1d2	reaktivnější
a	a	k8xC	a
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
lépe	dobře	k6eAd2	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
dražší	drahý	k2eAgMnSc1d2	dražší
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
manganistan	manganistan	k1gInSc1	manganistan
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
Oxidační	oxidační	k2eAgInSc1d1	oxidační
stav	stav	k1gInSc1	stav
IV	IV	kA	IV
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
oxidačním	oxidační	k2eAgInSc7d1	oxidační
stavem	stav	k1gInSc7	stav
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
však	však	k9	však
malý	malý	k2eAgInSc1d1	malý
–	–	k?	–
známé	známý	k2eAgMnPc4d1	známý
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
acidomanganičitany	acidomanganičitan	k1gInPc1	acidomanganičitan
[	[	kIx(	[
<g/>
MnX	MnX	k1gFnSc1	MnX
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
a	a	k8xC	a
[	[	kIx(	[
<g/>
MnX	MnX	k1gFnSc1	MnX
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
<g/>
=	=	kIx~	=
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
IO3	IO3	k1gFnSc1	IO3
a	a	k8xC	a
CN	CN	kA	CN
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zvláště	zvláště	k6eAd1	zvláště
stabilním	stabilní	k2eAgFnPc3d1	stabilní
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
chloromanganičitany	chloromanganičitan	k1gInPc1	chloromanganičitan
a	a	k8xC	a
fluoromanganičitany	fluoromanganičitan	k1gInPc1	fluoromanganičitan
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
manganem	mangan	k1gInSc7	mangan
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
III	III	kA	III
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
silné	silný	k2eAgFnPc4d1	silná
oxidační	oxidační	k2eAgFnPc4d1	oxidační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
disproporciaci	disproporciace	k1gFnSc3	disproporciace
na	na	k7c4	na
MnIV	MnIV	k1gFnSc4	MnIV
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc4	oxid
manganičitý	manganičitý	k2eAgInSc4d1	manganičitý
MnO	MnO	k1gFnSc7	MnO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
MnII	MnII	k1gFnSc1	MnII
<g/>
.	.	kIx.	.
</s>
<s>
Kyslíkové	kyslíkový	k2eAgInPc1d1	kyslíkový
donorové	donorový	k2eAgInPc1d1	donorový
atomy	atom	k1gInPc1	atom
však	však	k9	však
tento	tento	k3xDgInSc4	tento
oxidační	oxidační	k2eAgInSc4d1	oxidační
stupeň	stupeň	k1gInSc4	stupeň
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
bílý	bílý	k2eAgInSc1d1	bílý
hydroxid	hydroxid	k1gInSc1	hydroxid
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
hydratovaný	hydratovaný	k2eAgInSc4d1	hydratovaný
Mn	Mn	k1gFnSc7	Mn
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hnědnutí	hnědnutí	k1gNnSc3	hnědnutí
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
[	[	kIx(	[
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
acac	acac	k1gFnSc1	acac
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
oxidací	oxidace	k1gFnSc7	oxidace
manganatých	manganatý	k2eAgFnPc2d1	manganatý
solí	sůl	k1gFnPc2	sůl
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
penta-	penta-	k?	penta-
<g/>
2,4	[number]	k4	2,4
<g/>
-dionu	ion	k1gInSc2	-dion
(	(	kIx(	(
<g/>
acetylacetonu	acetylaceton	k1gInSc2	acetylaceton
<g/>
,	,	kIx,	,
acac	acac	k1gFnSc1	acac
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
také	také	k9	také
trihydrát	trihydrát	k1gInSc1	trihydrát
oxalatomanganitanu	oxalatomanganitan	k1gInSc2	oxalatomanganitan
draselného	draselný	k2eAgInSc2d1	draselný
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
•	•	k?	•
3	[number]	k4	3
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Ostatní	ostatní	k2eAgInPc4d1	ostatní
anionty	anion	k1gInPc4	anion
schopné	schopný	k2eAgFnSc2d1	schopná
koordinace	koordinace	k1gFnSc2	koordinace
(	(	kIx(	(
<g/>
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
a	a	k8xC	a
síran	síran	k1gInSc1	síran
<g/>
)	)	kIx)	)
však	však	k9	však
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
MnII	MnII	k1gFnSc4	MnII
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
MnIII	MnIII	k1gFnSc2	MnIII
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
oktaedrické	oktaedrický	k2eAgFnPc1d1	oktaedrický
a	a	k8xC	a
vysokospinové	vysokospinový	k2eAgFnPc1d1	vysokospinový
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
nízkospinový	nízkospinový	k2eAgInSc1d1	nízkospinový
oktaedrický	oktaedrický	k2eAgInSc1d1	oktaedrický
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
červený	červený	k2eAgInSc1d1	červený
hexakyanomanganitý	hexakyanomanganitý	k2eAgInSc1d1	hexakyanomanganitý
anion	anion	k1gInSc1	anion
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
proháněním	prohánění	k1gNnSc7	prohánění
vzduchu	vzduch	k1gInSc2	vzduch
vodným	vodný	k2eAgInSc7d1	vodný
roztokem	roztok	k1gInSc7	roztok
obsahující	obsahující	k2eAgFnSc2d1	obsahující
Mn	Mn	k1gFnSc2	Mn
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
CN-	CN-	k1gFnSc1	CN-
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
komplexy	komplex	k1gInPc1	komplex
[	[	kIx(	[
<g/>
MnX	MnX	k1gFnSc1	MnX
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
=	=	kIx~	=
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
(	(	kIx(	(
<g/>
fluoromanganitany	fluoromanganitan	k1gInPc1	fluoromanganitan
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
červené	červený	k2eAgFnPc1d1	červená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
bipyH	bipyH	k?	bipyH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
[	[	kIx(	[
<g/>
MnCl	MnCl	k1gInSc1	MnCl
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
tetragonálně	tetragonálně	k6eAd1	tetragonálně
pyramidální	pyramidální	k2eAgNnSc1d1	pyramidální
uspořádání	uspořádání	k1gNnSc1	uspořádání
koordinační	koordinační	k2eAgFnSc2d1	koordinační
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
tvoří	tvořit	k5eAaImIp3nS	tvořit
mangan	mangan	k1gInSc4	mangan
nejvíce	nejvíce	k6eAd1	nejvíce
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Stálost	stálost	k1gFnSc1	stálost
MnII	MnII	k1gMnPc2	MnII
vůči	vůči	k7c3	vůči
oxidaci	oxidace	k1gFnSc3	oxidace
i	i	k8xC	i
redukci	redukce	k1gFnSc3	redukce
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
vlivem	vlivem	k7c2	vlivem
symetrické	symetrický	k2eAgFnSc2d1	symetrická
konfigurace	konfigurace	k1gFnSc2	konfigurace
d	d	k?	d
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Vysokospinový	Vysokospinový	k2eAgInSc1d1	Vysokospinový
stav	stav	k1gInSc1	stav
ale	ale	k8xC	ale
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
žádnou	žádný	k3yNgFnSc4	žádný
stabilizační	stabilizační	k2eAgFnSc4d1	stabilizační
energii	energie	k1gFnSc4	energie
ligandového	ligandový	k2eAgNnSc2d1	ligandový
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
proto	proto	k8xC	proto
konstanty	konstanta	k1gFnPc1	konstanta
stability	stabilita	k1gFnSc2	stabilita
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
MII	MII	kA	MII
komplexy	komplex	k1gInPc4	komplex
nižší	nízký	k2eAgInPc4d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Nejtypičtější	typický	k2eAgInSc1d3	nejtypičtější
komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
růžový	růžový	k2eAgInSc1d1	růžový
hexaaqua-manganatý	hexaaquaanganatý	k2eAgInSc1d1	hexaaqua-manganatý
kation	kation	k1gInSc1	kation
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
vysokospinovým	vysokospinův	k2eAgInPc3d1	vysokospinův
oktaedrickým	oktaedrický	k2eAgInPc3d1	oktaedrický
komplexům	komplex	k1gInPc3	komplex
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
manganaté	manganatý	k2eAgInPc1d1	manganatý
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
ethylendiaminem	ethylendiamin	k1gInSc7	ethylendiamin
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chelatonem	chelaton	k1gInSc7	chelaton
III	III	kA	III
(	(	kIx(	(
<g/>
ethylendiamintetraoctová	ethylendiamintetraoctový	k2eAgFnSc1d1	ethylendiamintetraoctový
kyselina	kyselina	k1gFnSc1	kyselina
–	–	k?	–
EDTA	EDTA	kA	EDTA
<g/>
)	)	kIx)	)
a	a	k8xC	a
šťavelovou	šťavelový	k2eAgFnSc7d1	šťavelová
kyselinou	kyselina	k1gFnSc7	kyselina
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nízkospinové	Nízkospinový	k2eAgInPc1d1	Nízkospinový
komplexy	komplex	k1gInPc1	komplex
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvojmocný	dvojmocný	k2eAgInSc4d1	dvojmocný
mangan	mangan	k1gInSc4	mangan
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
ligandy	liganda	k1gFnPc4	liganda
ležícimi	ležíci	k1gFnPc7	ležíci
ve	v	k7c6	v
spektrochemické	spektrochemický	k2eAgFnSc6d1	spektrochemický
řadě	řada	k1gFnSc6	řada
zcela	zcela	k6eAd1	zcela
napravo	napravo	k6eAd1	napravo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgMnPc3d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
kyanidové	kyanidový	k2eAgInPc4d1	kyanidový
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Oktaedrický	oktaedrický	k2eAgInSc1d1	oktaedrický
modrofialový	modrofialový	k2eAgInSc1d1	modrofialový
hexakyanomanganatanový	hexakyanomanganatanový	k2eAgInSc1d1	hexakyanomanganatanový
anion	anion	k1gInSc1	anion
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
obsahujícím	obsahující	k2eAgMnSc7d1	obsahující
nadbytek	nadbytek	k1gInSc4	nadbytek
CN-	CN-	k1gMnPc1	CN-
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
kyslíku	kyslík	k1gInSc2	kyslík
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
tmavě	tmavě	k6eAd1	tmavě
červený	červený	k2eAgInSc4d1	červený
hexakyanomanganitanový	hexakyanomanganitanový	k2eAgInSc4d1	hexakyanomanganitanový
anion	anion	k1gInSc4	anion
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
(	(	kIx(	(
<g/>
samotný	samotný	k2eAgInSc1d1	samotný
kyanid	kyanid	k1gInSc1	kyanid
manganitý	manganitý	k2eAgInSc1d1	manganitý
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nepřístupu	nepřístup	k1gInSc2	nepřístup
vzduchu	vzduch	k1gInSc2	vzduch
působením	působení	k1gNnSc7	působení
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
krupice	krupice	k1gFnSc2	krupice
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
žluté	žlutý	k2eAgFnPc4d1	žlutá
kyanomangannany	kyanomangannana	k1gFnPc4	kyanomangannana
M	M	kA	M
<g/>
5	[number]	k4	5
<g/>
I	I	kA	I
<g/>
[	[	kIx(	[
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
bezbarvé	bezbarvý	k2eAgNnSc1d1	bezbarvé
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
komplexy	komplex	k1gInPc1	komplex
se	s	k7c7	s
žlutozelenou	žlutozelený	k2eAgFnSc7d1	žlutozelená
barvou	barva	k1gFnSc7	barva
s	s	k7c7	s
tetraedrickým	tetraedrický	k2eAgInSc7d1	tetraedrický
anionem	anion	k1gInSc7	anion
[	[	kIx(	[
<g/>
MnX	MnX	k1gFnSc1	MnX
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
(	(	kIx(	(
<g/>
X	X	kA	X
=	=	kIx~	=
Cl-	Cl-	k1gMnSc1	Cl-
<g/>
,	,	kIx,	,
Br-	Br-	k1gMnSc1	Br-
<g/>
,	,	kIx,	,
I-	I-	k1gMnSc1	I-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
krystalizací	krystalizace	k1gFnSc7	krystalizace
z	z	k7c2	z
ethanolických	ethanolický	k2eAgInPc2d1	ethanolický
roztoků	roztok	k1gInPc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
komplexy	komplex	k1gInPc4	komplex
i	i	k9	i
s	s	k7c7	s
nižšími	nízký	k2eAgNnPc7d2	nižší
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
čísly	číslo	k1gNnPc7	číslo
než	než	k8xS	než
MnII	MnII	k1gFnPc7	MnII
a	a	k8xC	a
MnIII	MnIII	k1gFnPc7	MnIII
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
(	(	kIx(	(
<g/>
sice	sice	k8xC	sice
spíše	spíše	k9	spíše
organické	organický	k2eAgFnPc1d1	organická
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mangan	mangan	k1gInSc4	mangan
i	i	k9	i
se	s	k7c7	s
zápornými	záporný	k2eAgNnPc7d1	záporné
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
organický	organický	k2eAgInSc1d1	organický
komplex	komplex	k1gInSc1	komplex
manganu	mangan	k1gInSc2	mangan
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
dekakarbonyl	dekakarbonyl	k1gInSc1	dekakarbonyl
dimanganu	dimangan	k1gInSc2	dimangan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
karbonyly	karbonyl	k1gInPc4	karbonyl
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
má	mít	k5eAaImIp3nS	mít
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
Mn	Mn	k1gFnSc2	Mn
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc7d1	výchozí
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
dalších	další	k2eAgFnPc2d1	další
komplexních	komplexní	k2eAgFnPc2d1	komplexní
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
manganu	mangan	k1gInSc2	mangan
jsou	být	k5eAaImIp3nP	být
organokovy	organokův	k2eAgFnPc1d1	organokův
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
komplexní	komplexní	k2eAgFnPc4d1	komplexní
karbonylové	karbonylový	k2eAgFnPc4d1	karbonylová
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
karbonyly	karbonyl	k1gInPc1	karbonyl
manganu	mangan	k1gInSc2	mangan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgNnPc6	který
je	být	k5eAaImIp3nS	být
pojednáno	pojednán	k2eAgNnSc1d1	pojednáno
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
manganičitého	manganičitý	k2eAgNnSc2d1	manganičitý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
LiAlH	LiAlH	k1gFnSc2	LiAlH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
za	za	k7c2	za
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
obsahem	obsah	k1gInSc7	obsah
CO	co	k8xS	co
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
manganatý	manganatý	k2eAgInSc1d1	manganatý
Mn	Mn	k1gMnSc7	Mn
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
z	z	k7c2	z
vodných	vodný	k2eAgInPc2d1	vodný
roztoků	roztok	k1gInPc2	roztok
jako	jako	k8xS	jako
červený	červený	k2eAgInSc1d1	červený
tetrahydrát	tetrahydrát	k1gInSc1	tetrahydrát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
stimulační	stimulační	k2eAgNnSc1d1	stimulační
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
,	,	kIx,	,
sikativ	sikativ	k1gInSc1	sikativ
a	a	k8xC	a
jako	jako	k9	jako
přenašeč	přenašeč	k1gInSc4	přenašeč
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnSc6d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
manganatý	manganatý	k2eAgInSc4d1	manganatý
MnC	MnC	k1gFnSc7	MnC
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
,	,	kIx,	,
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
formě	forma	k1gFnSc6	forma
je	být	k5eAaImIp3nS	být
šťavelan	šťavelan	k1gInSc1	šťavelan
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
manganatého	manganatý	k2eAgInSc2d1	manganatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
šťavelové	šťavelový	k2eAgFnSc6d1	šťavelová
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
manganu	mangan	k1gInSc2	mangan
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
přísun	přísun	k1gInSc1	přísun
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
správnou	správný	k2eAgFnSc4d1	správná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
nedostatek	nedostatek	k1gInSc1	nedostatek
manganu	mangan	k1gInSc2	mangan
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
vede	vést	k5eAaImIp3nS	vést
především	především	k9	především
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
v	v	k7c6	v
cévním	cévní	k2eAgInSc6d1	cévní
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nežádoucím	žádoucí	k2eNgFnPc3d1	nežádoucí
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
metabolizmu	metabolizmus	k1gInSc6	metabolizmus
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
ukládání	ukládání	k1gNnSc3	ukládání
na	na	k7c4	na
cévní	cévní	k2eAgFnSc4d1	cévní
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
měřítku	měřítko	k1gNnSc6	měřítko
značně	značně	k6eAd1	značně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Mangan	mangan	k1gInSc1	mangan
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k9	i
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
metabolismus	metabolismus	k1gInSc4	metabolismus
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
nedostatek	nedostatek	k1gInSc1	nedostatek
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
onemocnění	onemocnění	k1gNnSc2	onemocnění
cukrovkou	cukrovka	k1gFnSc7	cukrovka
(	(	kIx(	(
<g/>
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
přirozenými	přirozený	k2eAgInPc7d1	přirozený
zdroji	zdroj	k1gInPc7	zdroj
manganu	mangan	k1gInSc2	mangan
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
strava	strava	k1gFnSc1	strava
jako	jako	k8xS	jako
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
,	,	kIx,	,
hrášek	hrášek	k1gInSc1	hrášek
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
borůvky	borůvka	k1gFnPc1	borůvka
<g/>
,	,	kIx,	,
špenát	špenát	k1gInSc1	špenát
a	a	k8xC	a
ořechy	ořech	k1gInPc1	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
mg	mg	kA	mg
Mn	Mn	k1gFnSc2	Mn
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
přebytek	přebytek	k1gInSc1	přebytek
manganu	mangan	k1gInSc2	mangan
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
působí	působit	k5eAaImIp3nS	působit
negativně	negativně	k6eAd1	negativně
především	především	k9	především
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
potíže	potíž	k1gFnPc4	potíž
podobné	podobný	k2eAgFnPc4d1	podobná
projevům	projev	k1gInPc3	projev
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
expozice	expozice	k1gFnSc1	expozice
vysokými	vysoký	k2eAgFnPc7d1	vysoká
dávkami	dávka	k1gFnPc7	dávka
manganu	mangan	k1gInSc2	mangan
může	moct	k5eAaImIp3nS	moct
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
vznik	vznik	k1gInSc4	vznik
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
přijímají	přijímat	k5eAaImIp3nP	přijímat
mangan	mangan	k1gInSc4	mangan
jako	jako	k8xC	jako
všechny	všechen	k3xTgInPc4	všechen
minerály	minerál	k1gInPc4	minerál
z	z	k7c2	z
půdního	půdní	k2eAgInSc2d1	půdní
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
špatně	špatně	k6eAd1	špatně
rozpustných	rozpustný	k2eAgInPc6d1	rozpustný
oxidech	oxid	k1gInPc6	oxid
nebo	nebo	k8xC	nebo
dobře	dobře	k6eAd1	dobře
rozpustných	rozpustný	k2eAgInPc6d1	rozpustný
chelátech	chelát	k1gInPc6	chelát
<g/>
.	.	kIx.	.
</s>
<s>
Cheláty	Chelát	k1gInPc1	Chelát
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
příznivější	příznivý	k2eAgMnSc1d2	příznivější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
je	být	k5eAaImIp3nS	být
mangan	mangan	k1gInSc4	mangan
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
komplexu	komplex	k1gInSc2	komplex
rozkládajícího	rozkládající	k2eAgInSc2d1	rozkládající
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
primární	primární	k2eAgFnSc1d1	primární
fáze	fáze	k1gFnSc1	fáze
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
superoxiddismutázách	superoxiddismutáza	k1gFnPc6	superoxiddismutáza
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
chloroplasty	chloroplast	k1gInPc4	chloroplast
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
chlorózy	chloróza	k1gFnSc2	chloróza
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mangan	mangan	k1gInSc1	mangan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mangan	mangan	k1gInSc1	mangan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
National	National	k1gMnSc1	National
Pollutant	Pollutant	k1gMnSc1	Pollutant
Inventory	Inventor	k1gInPc4	Inventor
–	–	k?	–
Manganese	Manganese	k1gFnSc2	Manganese
and	and	k?	and
compounds	compounds	k6eAd1	compounds
Fact	Fact	k2eAgMnSc1d1	Fact
Sheet	Sheet	k1gMnSc1	Sheet
Neurotoxicity	Neurotoxicita	k1gFnSc2	Neurotoxicita
of	of	k?	of
inhaled	inhaled	k1gInSc1	inhaled
manganese	manganese	k1gFnSc1	manganese
<g/>
:	:	kIx,	:
Public	publicum	k1gNnPc2	publicum
health	healtha	k1gFnPc2	healtha
danger	dangra	k1gFnPc2	dangra
in	in	k?	in
the	the	k?	the
shower	shower	k1gInSc1	shower
<g/>
?	?	kIx.	?
</s>
<s>
NIOSH	NIOSH	kA	NIOSH
Manganese	Manganese	k1gFnSc1	Manganese
Topic	Topic	k1gMnSc1	Topic
Page	Pag	k1gInSc2	Pag
Link	Link	k1gMnSc1	Link
Found	Found	k1gMnSc1	Found
Between	Between	k2eAgInSc4d1	Between
Parkinson	Parkinson	k1gInSc4	Parkinson
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Disease	Diseasa	k1gFnSc6	Diseasa
Genes	Genes	k1gInSc1	Genes
And	Anda	k1gFnPc2	Anda
Manganese	Manganese	k1gFnSc2	Manganese
Poisoning	Poisoning	k1gInSc1	Poisoning
The	The	k1gMnSc1	The
periodic	periodic	k1gMnSc1	periodic
table	tablo	k1gNnSc6	tablo
of	of	k?	of
videos	videos	k1gInSc1	videos
<g/>
:	:	kIx,	:
Manganese	Manganese	k1gFnSc1	Manganese
</s>
