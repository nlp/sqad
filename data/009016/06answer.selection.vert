<s>
Samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
stalo	stát	k5eAaPmAgNnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradice	tradice	k1gFnPc4	tradice
státnosti	státnost	k1gFnSc2	státnost
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Českého	český	k2eAgNnSc2d1	české
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInSc4d1	sahající
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
