<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ottokar	Ottokar	k1gMnSc1	Ottokar
I.	I.	kA	I.
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
;	;	kIx,	;
1155	[number]	k4	1155
<g/>
/	/	kIx~	/
<g/>
1167	[number]	k4	1167
<g/>
?	?	kIx.	?
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
a	a	k8xC	a
1197	[number]	k4	1197
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1198	[number]	k4	1198
<g/>
–	–	k?	–
<g/>
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dědičně	dědičně	k6eAd1	dědičně
zajistit	zajistit	k5eAaPmF	zajistit
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
i	i	k8xC	i
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
