<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ottokar	Ottokar	k1gMnSc1	Ottokar
I.	I.	kA	I.
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
;	;	kIx,	;
1155	[number]	k4	1155
<g/>
/	/	kIx~	/
<g/>
1167	[number]	k4	1167
<g/>
?	?	kIx.	?
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
a	a	k8xC	a
1197	[number]	k4	1197
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
1198	[number]	k4	1198
<g/>
–	–	k?	–
<g/>
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dědičně	dědičně	k6eAd1	dědičně
zajistit	zajistit	k5eAaPmF	zajistit
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
i	i	k8xC	i
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
druhé	druhý	k4xOgFnSc2	druhý
manželky	manželka	k1gFnSc2	manželka
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Judity	Judita	k1gFnPc4	Judita
Durynské	durynský	k2eAgFnPc4d1	Durynská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1173	[number]	k4	1173
<g/>
–	–	k?	–
<g/>
1179	[number]	k4	1179
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Adlétou	Adléta	k1gFnSc7	Adléta
Míšeňskou	míšeňský	k2eAgFnSc7d1	Míšeňská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1179	[number]	k4	1179
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
svého	svůj	k3xOyFgMnSc2	svůj
vládnoucího	vládnoucí	k2eAgMnSc2d1	vládnoucí
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Bedřicha	Bedřich	k1gMnSc2	Bedřich
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
markraběte	markrabě	k1gMnSc2	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následných	následný	k2eAgInPc6d1	následný
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
stál	stát	k5eAaImAgMnS	stát
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
stal	stát	k5eAaPmAgInS	stát
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
(	(	kIx(	(
<g/>
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
strýce	strýc	k1gMnSc2	strýc
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
)	)	kIx)	)
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Břetislavem	Břetislav	k1gMnSc7	Břetislav
i	i	k8xC	i
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
trávil	trávit	k5eAaImAgMnS	trávit
léta	léto	k1gNnSc2	léto
1193	[number]	k4	1193
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
opět	opět	k6eAd1	opět
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Jindřich	Jindřich	k1gMnSc1	Jindřich
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jindřichem	Jindřich	k1gMnSc7	Jindřich
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Přemysl	Přemysl	k1gMnSc1	Přemysl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1197	[number]	k4	1197
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
císařskou	císařský	k2eAgFnSc4d1	císařská
korunu	koruna	k1gFnSc4	koruna
mezi	mezi	k7c7	mezi
Štaufy	Štauf	k1gInPc7	Štauf
a	a	k8xC	a
Welfy	Welf	k1gInPc7	Welf
<g/>
.	.	kIx.	.
</s>
<s>
Proslulou	proslulý	k2eAgFnSc7d1	proslulá
sérií	série	k1gFnSc7	série
změn	změna	k1gFnPc2	změna
svého	svůj	k1gMnSc2	svůj
stranictví	stranictví	k1gNnSc2	stranictví
během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
bojů	boj	k1gInPc2	boj
získal	získat	k5eAaPmAgMnS	získat
postupně	postupně	k6eAd1	postupně
Přemysl	Přemysl	k1gMnSc1	Přemysl
potvrzení	potvrzení	k1gNnSc2	potvrzení
dědičného	dědičný	k2eAgInSc2d1	dědičný
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
nejen	nejen	k6eAd1	nejen
od	od	k7c2	od
obou	dva	k4xCgFnPc2	dva
válčících	válčící	k2eAgFnPc2d1	válčící
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
1198	[number]	k4	1198
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
a	a	k8xC	a
1203	[number]	k4	1203
Ota	Ota	k1gMnSc1	Ota
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
i	i	k9	i
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
Inocence	Inocenc	k1gMnSc2	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
komplikována	komplikovat	k5eAaBmNgFnS	komplikovat
rozchodem	rozchod	k1gInSc7	rozchod
s	s	k7c7	s
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Adlétou	Adlétý	k2eAgFnSc7d1	Adlétý
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Konstancií	Konstancie	k1gFnSc7	Konstancie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jistý	jistý	k2eAgInSc4d1	jistý
vrchol	vrchol	k1gInSc4	vrchol
Přemyslova	Přemyslův	k2eAgNnSc2d1	Přemyslovo
snažení	snažení	k1gNnSc2	snažení
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
vykládán	vykládat	k5eAaImNgInS	vykládat
zisk	zisk	k1gInSc1	zisk
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
<g/>
,	,	kIx,	,
privilegia	privilegium	k1gNnPc4	privilegium
upravujícího	upravující	k2eAgInSc2d1	upravující
poměr	poměr	k1gInSc1	poměr
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
k	k	k7c3	k
Říši	říš	k1gFnSc3	říš
a	a	k8xC	a
zaručujícího	zaručující	k2eAgInSc2d1	zaručující
dědičnost	dědičnost	k1gFnSc1	dědičnost
českého	český	k2eAgInSc2d1	český
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
soustředil	soustředit	k5eAaPmAgMnS	soustředit
především	především	k6eAd1	především
na	na	k7c4	na
konsolidaci	konsolidace	k1gFnSc4	konsolidace
domácích	domácí	k2eAgInPc2d1	domácí
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
bezproblémového	bezproblémový	k2eAgNnSc2d1	bezproblémové
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
pro	pro	k7c4	pro
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dosahování	dosahování	k1gNnSc6	dosahování
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
postupoval	postupovat	k5eAaImAgInS	postupovat
často	často	k6eAd1	často
nekompromisně	kompromisně	k6eNd1	kompromisně
a	a	k8xC	a
neváhal	váhat	k5eNaImAgMnS	váhat
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
)	)	kIx)	)
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bránily	bránit	k5eAaImAgFnP	bránit
jejich	jejich	k3xOp3gNnSc4	jejich
dosažení	dosažení	k1gNnSc4	dosažení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Judity	Judita	k1gFnSc2	Judita
Durynské	durynský	k2eAgInPc1d1	durynský
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
v	v	k7c6	v
předpokládaném	předpokládaný	k2eAgInSc6d1	předpokládaný
roce	rok	k1gInSc6	rok
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
kladeno	klást	k5eAaImNgNnS	klást
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1155	[number]	k4	1155
<g/>
,	,	kIx,	,
před	před	k7c4	před
rok	rok	k1gInSc4	rok
1160	[number]	k4	1160
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
až	až	k9	až
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1165	[number]	k4	1165
<g/>
–	–	k?	–
<g/>
1167	[number]	k4	1167
<g/>
.	.	kIx.	.
</s>
<s>
Bezstarostný	bezstarostný	k2eAgInSc1d1	bezstarostný
život	život	k1gInSc1	život
královského	královský	k2eAgNnSc2d1	královské
dítěte	dítě	k1gNnSc2	dítě
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1172	[number]	k4	1172
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Bedřicha	Bedřich	k1gMnSc2	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
dřívější	dřívější	k2eAgFnPc4d1	dřívější
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
nebude	být	k5eNaImBp3nS	být
klást	klást	k5eAaImF	klást
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
šlechta	šlechta	k1gFnSc1	šlechta
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
postupně	postupně	k6eAd1	postupně
smíří	smířit	k5eAaPmIp3nP	smířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
však	však	k9	však
hluboce	hluboko	k6eAd1	hluboko
zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
a	a	k8xC	a
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
využil	využít	k5eAaPmAgMnS	využít
nastalé	nastalý	k2eAgFnPc4d1	nastalá
situace	situace	k1gFnPc4	situace
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
své	svůj	k3xOyFgFnSc2	svůj
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
českých	český	k2eAgFnPc2d1	Česká
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1173	[number]	k4	1173
Fridrich	Fridrich	k1gMnSc1	Fridrich
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bedřicha	Bedřich	k1gMnSc2	Bedřich
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
českého	český	k2eAgMnSc2d1	český
knížete	kníže	k1gMnSc2	kníže
nahradí	nahradit	k5eAaPmIp3nS	nahradit
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
císaře	císař	k1gMnSc2	císař
obratem	obrat	k1gInSc7	obrat
titulu	titul	k1gInSc2	titul
vzdal	vzdát	k5eAaPmAgMnS	vzdát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Soběslava	Soběslav	k1gMnSc2	Soběslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zásah	zásah	k1gInSc1	zásah
císaře	císař	k1gMnSc2	císař
takřka	takřka	k6eAd1	takřka
ze	z	k7c2	z
dne	den	k1gInSc2	den
na	na	k7c4	na
den	den	k1gInSc4	den
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Vladislava	Vladislav	k1gMnSc2	Vladislav
i	i	k9	i
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
běžence	běženec	k1gMnSc2	běženec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
museli	muset	k5eAaImAgMnP	muset
zakoušet	zakoušet	k5eAaImF	zakoušet
hořkost	hořkost	k1gFnSc4	hořkost
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
osudu	osud	k1gInSc6	osud
mladších	mladý	k2eAgMnPc2d2	mladší
synů	syn	k1gMnPc2	syn
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
a	a	k8xC	a
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
se	se	k3xPyFc4	se
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
minimum	minimum	k1gNnSc4	minimum
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
v	v	k7c6	v
letech	let	k1gInPc6	let
1173	[number]	k4	1173
<g/>
–	–	k?	–
<g/>
1179	[number]	k4	1179
pobýval	pobývat	k5eAaImAgMnS	pobývat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
míšeňských	míšeňský	k2eAgInPc2d1	míšeňský
Wettinů	Wettin	k1gInPc2	Wettin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1178	[number]	k4	1178
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Adlétou	Adléta	k1gFnSc7	Adléta
Míšeňskou	míšeňský	k2eAgFnSc7d1	Míšeňská
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Bedřich	Bedřich	k1gMnSc1	Bedřich
stal	stát	k5eAaPmAgMnS	stát
podruhé	podruhé	k6eAd1	podruhé
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
===	===	k?	===
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
značně	značně	k6eAd1	značně
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mezi	mezi	k7c7	mezi
knížetem	kníže	k1gMnSc7	kníže
Bedřichem	Bedřich	k1gMnSc7	Bedřich
a	a	k8xC	a
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1179	[number]	k4	1179
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
staronový	staronový	k2eAgMnSc1d1	staronový
kníže	kníže	k1gMnSc1	kníže
Bedřich	Bedřich	k1gMnSc1	Bedřich
vyřešit	vyřešit	k5eAaPmF	vyřešit
pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
spory	spor	k1gInPc4	spor
ohledně	ohledně	k7c2	ohledně
Vitorazska	Vitorazsk	k1gInSc2	Vitorazsk
do	do	k7c2	do
Chebu	Cheb	k1gInSc2	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
sepsána	sepsán	k2eAgFnSc1d1	sepsána
písemná	písemný	k2eAgFnSc1d1	písemná
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
svědků	svědek	k1gMnPc2	svědek
–	–	k?	–
asi	asi	k9	asi
dvacetiletý	dvacetiletý	k2eAgMnSc1d1	dvacetiletý
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
<g/>
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
pouze	pouze	k6eAd1	pouze
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
)	)	kIx)	)
coby	coby	k?	coby
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
(	(	kIx(	(
<g/>
marggravius	marggravius	k1gInSc1	marggravius
de	de	k?	de
Moravia	Moravia	k1gFnSc1	Moravia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
markraběte	markrabě	k1gMnSc2	markrabě
Přemysl	Přemysl	k1gMnSc1	Přemysl
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
zájmy	zájem	k1gInPc4	zájem
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
Bedřicha	Bedřich	k1gMnSc2	Bedřich
<g/>
,	,	kIx,	,
eliminoval	eliminovat	k5eAaBmAgMnS	eliminovat
ambice	ambice	k1gFnSc2	ambice
znojemského	znojemský	k2eAgMnSc2d1	znojemský
údělníka	údělník	k1gMnSc2	údělník
Konráda	Konrád	k1gMnSc2	Konrád
Oty	Ota	k1gMnSc2	Ota
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
zde	zde	k6eAd1	zde
spravoval	spravovat	k5eAaImAgInS	spravovat
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
úděl	úděl	k1gInSc1	úděl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Přemysla	Přemysl	k1gMnSc2	Přemysl
(	(	kIx(	(
<g/>
a	a	k8xC	a
s	s	k7c7	s
léty	léto	k1gNnPc7	léto
1179	[number]	k4	1179
<g/>
–	–	k?	–
<g/>
1182	[number]	k4	1182
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
spojit	spojit	k5eAaPmF	spojit
i	i	k9	i
českou	český	k2eAgFnSc4d1	Česká
expanzi	expanze	k1gFnSc4	expanze
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
spojenou	spojený	k2eAgFnSc4d1	spojená
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
Holasicka	Holasicko	k1gNnSc2	Holasicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1182	[number]	k4	1182
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
panujícím	panující	k2eAgNnSc7d1	panující
knížetem	kníže	k1gNnSc7wR	kníže
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
propuknutí	propuknutí	k1gNnSc3	propuknutí
vzpoury	vzpoura	k1gFnSc2	vzpoura
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
byl	být	k5eAaImAgMnS	být
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
šlechta	šlechta	k1gFnSc1	šlechta
si	se	k3xPyFc3	se
zvolila	zvolit	k5eAaPmAgFnS	zvolit
za	za	k7c4	za
nového	nový	k2eAgMnSc4d1	nový
knížete	kníže	k1gMnSc4	kníže
Konráda	Konrád	k1gMnSc4	Konrád
Otu	Ota	k1gMnSc4	Ota
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
hledal	hledat	k5eAaImAgMnS	hledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
spor	spor	k1gInSc1	spor
vyřešit	vyřešit	k5eAaPmF	vyřešit
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
v	v	k7c6	v
září	září	k1gNnSc6	září
1182	[number]	k4	1182
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
císař	císař	k1gMnSc1	císař
tvrdě	tvrdě	k6eAd1	tvrdě
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
a	a	k8xC	a
zastrašil	zastrašit	k5eAaPmAgMnS	zastrašit
přítomné	přítomný	k2eAgMnPc4d1	přítomný
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c4	mezi
Bedřicha	Bedřich	k1gMnSc4	Bedřich
a	a	k8xC	a
Konráda	Konrád	k1gMnSc4	Konrád
Otu	Ota	k1gMnSc4	Ota
rozhodně	rozhodně	k6eAd1	rozhodně
nelze	lze	k6eNd1	lze
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
ani	ani	k8xC	ani
jako	jako	k9	jako
její	její	k3xOp3gNnSc4	její
přímé	přímý	k2eAgNnSc4d1	přímé
podřízení	podřízení	k1gNnSc4	podřízení
Říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Bedřichovy	Bedřichův	k2eAgFnPc4d1	Bedřichova
těžkosti	těžkost	k1gFnPc4	těžkost
při	při	k7c6	při
vládě	vláda	k1gFnSc6	vláda
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
počínal	počínat	k5eAaImAgMnS	počínat
mnohem	mnohem	k6eAd1	mnohem
samostatněji	samostatně	k6eAd2	samostatně
<g/>
,	,	kIx,	,
než	než	k8xS	než
bývalo	bývat	k5eAaImAgNnS	bývat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
u	u	k7c2	u
moravských	moravský	k2eAgMnPc2d1	moravský
údělníků	údělník	k1gMnPc2	údělník
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
však	však	k9	však
Bedřichovi	Bedřichův	k2eAgMnPc1d1	Bedřichův
odpůrci	odpůrce	k1gMnPc1	odpůrce
využívali	využívat	k5eAaImAgMnP	využívat
každé	každý	k3xTgFnPc4	každý
příležitosti	příležitost	k1gFnPc4	příležitost
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1184	[number]	k4	1184
využili	využít	k5eAaPmAgMnP	využít
Bedřichovy	Bedřichův	k2eAgFnPc4d1	Bedřichova
nepřítomnosti	nepřítomnost	k1gFnPc4	nepřítomnost
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
(	(	kIx(	(
<g/>
synem	syn	k1gMnSc7	syn
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
<g/>
)	)	kIx)	)
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
Bedřichova	Bedřichův	k2eAgFnSc1d1	Bedřichova
manželka	manželka	k1gFnSc1	manželka
uhájila	uhájit	k5eAaPmAgFnS	uhájit
a	a	k8xC	a
po	po	k7c6	po
Bedřichově	Bedřichův	k2eAgInSc6d1	Bedřichův
návratu	návrat	k1gInSc6	návrat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
salcburského	salcburský	k2eAgMnSc2d1	salcburský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
všichni	všechen	k3xTgMnPc1	všechen
od	od	k7c2	od
Václava	Václav	k1gMnSc2	Václav
odpadli	odpadnout	k5eAaPmAgMnP	odpadnout
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Změněné	změněný	k2eAgFnPc4d1	změněná
situace	situace	k1gFnPc4	situace
využil	využít	k5eAaPmAgMnS	využít
Bedřich	Bedřich	k1gMnSc1	Bedřich
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
vedené	vedený	k2eAgNnSc1d1	vedené
Přemyslem	Přemysl	k1gMnSc7	Přemysl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1185	[number]	k4	1185
několikrát	několikrát	k6eAd1	několikrát
vyplenilo	vyplenit	k5eAaPmAgNnS	vyplenit
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
a	a	k8xC	a
Bítovsko	Bítovsko	k1gNnSc1	Bítovsko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největší	veliký	k2eAgFnSc3d3	veliký
bitvě	bitva	k1gFnSc3	bitva
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Moravany	Moravan	k1gMnPc7	Moravan
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bitvu	bitva	k1gFnSc4	bitva
sice	sice	k8xC	sice
Přemysl	Přemysl	k1gMnSc1	Přemysl
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
tak	tak	k6eAd1	tak
velkými	velký	k2eAgFnPc7d1	velká
ztrátami	ztráta	k1gFnPc7	ztráta
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stáhnout	stáhnout	k5eAaPmF	stáhnout
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následkem	následkem	k7c2	následkem
krvavé	krvavý	k2eAgFnSc2d1	krvavá
bitvy	bitva	k1gFnSc2	bitva
byla	být	k5eAaImAgNnP	být
mírová	mírový	k2eAgNnPc1d1	Mírové
jednání	jednání	k1gNnPc1	jednání
ve	v	k7c6	v
středočeském	středočeský	k2eAgInSc6d1	středočeský
Kníně	Knín	k1gInSc6	Knín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1186	[number]	k4	1186
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Konrád	Konrád	k1gMnSc1	Konrád
Ota	Ota	k1gMnSc1	Ota
uznal	uznat	k5eAaPmAgMnS	uznat
podřízenost	podřízenost	k1gFnSc4	podřízenost
Bedřichovi	Bedřich	k1gMnSc3	Bedřich
<g/>
,	,	kIx,	,
ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
však	však	k9	však
titul	titul	k1gInSc4	titul
markraběte	markrabě	k1gMnSc2	markrabě
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
i	i	k9	i
určen	určit	k5eAaPmNgInS	určit
za	za	k7c2	za
nástupce	nástupce	k1gMnSc2	nástupce
knížete	kníže	k1gMnSc2	kníže
Bedřicha	Bedřich	k1gMnSc2	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
tak	tak	k6eAd1	tak
svitla	svitnout	k5eAaPmAgFnS	svitnout
naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
klidnější	klidný	k2eAgInPc4d2	klidnější
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smíru	smír	k1gInSc6	smír
s	s	k7c7	s
Konrádem	Konrád	k1gMnSc7	Konrád
Otou	Ota	k1gMnSc7	Ota
ovšem	ovšem	k9	ovšem
vyplul	vyplout	k5eAaPmAgMnS	vyplout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
další	další	k2eAgInSc4d1	další
Bedřichův	Bedřichův	k2eAgInSc4d1	Bedřichův
spor	spor	k1gInSc4	spor
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
a	a	k8xC	a
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Břetislavem	Břetislav	k1gMnSc7	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
ukončil	ukončit	k5eAaPmAgMnS	ukončit
císař	císař	k1gMnSc1	císař
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
privilegiem	privilegium	k1gNnSc7	privilegium
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
příslušnost	příslušnost	k1gFnSc4	příslušnost
pražského	pražský	k2eAgMnSc2d1	pražský
biskupa	biskup	k1gMnSc2	biskup
k	k	k7c3	k
říšským	říšský	k2eAgMnPc3d1	říšský
knížatům	kníže	k1gMnPc3wR	kníže
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
knížeti	kníže	k1gNnSc6wR	kníže
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
oslabení	oslabení	k1gNnSc3	oslabení
knížete	kníže	k1gMnSc2	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Bedřichově	Bedřichův	k2eAgFnSc6d1	Bedřichova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1189	[number]	k4	1189
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
na	na	k7c6	na
základu	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
Knína	Knín	k1gInSc2	Knín
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
českých	český	k2eAgMnPc2d1	český
předáků	předák	k1gMnPc2	předák
ujal	ujmout	k5eAaPmAgMnS	ujmout
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
Konrádovy	Konrádův	k2eAgFnSc2d1	Konrádova
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
1189	[number]	k4	1189
<g/>
–	–	k?	–
<g/>
1191	[number]	k4	1191
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
žádné	žádný	k3yNgFnPc1	žádný
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Přemyslovi	Přemysl	k1gMnSc6	Přemysl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ani	ani	k8xC	ani
jako	jako	k8xS	jako
svědek	svědek	k1gMnSc1	svědek
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
listiny	listina	k1gFnSc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
zemřel	zemřít	k5eAaPmAgMnS	zemřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1191	[number]	k4	1191
bez	bez	k7c2	bez
potomstva	potomstvo	k1gNnSc2	potomstvo
a	a	k8xC	a
knížecí	knížecí	k2eAgInSc4d1	knížecí
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
(	(	kIx(	(
<g/>
snad	snad	k9	snad
nejstarší	starý	k2eAgMnSc1d3	nejstarší
<g/>
)	)	kIx)	)
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
knížete	kníže	k1gMnSc2	kníže
Soběslava	Soběslav	k1gMnSc2	Soběslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sporech	spor	k1gInPc6	spor
mezi	mezi	k7c7	mezi
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Břetislavem	Břetislav	k1gMnSc7	Břetislav
a	a	k8xC	a
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
Přemysla	Přemysl	k1gMnSc2	Přemysl
proti	proti	k7c3	proti
Václavovi	Václav	k1gMnSc3	Václav
<g/>
,	,	kIx,	,
odebral	odebrat	k5eAaPmAgMnS	odebrat
se	se	k3xPyFc4	se
biskup	biskup	k1gMnSc1	biskup
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
za	za	k7c7	za
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knížetem	kníže	k1gMnSc7	kníže
(	(	kIx(	(
<g/>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
(	(	kIx(	(
<g/>
1193	[number]	k4	1193
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
nastolený	nastolený	k2eAgMnSc1d1	nastolený
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
necítil	cítit	k5eNaImAgMnS	cítit
být	být	k5eAaImF	být
pražskému	pražský	k2eAgMnSc3d1	pražský
biskupu	biskup	k1gMnSc3	biskup
ani	ani	k8xC	ani
císaři	císař	k1gMnSc3	císař
nijak	nijak	k6eAd1	nijak
zavázán	zavázat	k5eAaPmNgMnS	zavázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nemínil	mínit	k5eNaImAgMnS	mínit
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
zaplacení	zaplacení	k1gNnSc4	zaplacení
částky	částka	k1gFnSc2	částka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
císaři	císař	k1gMnPc1	císař
Jindřich	Jindřich	k1gMnSc1	Jindřich
Břetislav	Břetislav	k1gMnSc1	Břetislav
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozladěný	rozladěný	k2eAgMnSc1d1	rozladěný
biskup	biskup	k1gMnSc1	biskup
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Santiaga	Santiago	k1gNnSc2	Santiago
de	de	k?	de
Compostela	Compostel	k1gMnSc2	Compostel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zadržen	zadržet	k5eAaPmNgMnS	zadržet
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
následně	následně	k6eAd1	následně
držen	držen	k2eAgInSc1d1	držen
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
jako	jako	k8xS	jako
rukojmí	rukojmí	k1gNnPc2	rukojmí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhoršovaly	zhoršovat	k5eAaImAgInP	zhoršovat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
také	také	k9	také
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
aktivněji	aktivně	k6eAd2	aktivně
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc3	jeho
předchůdci	předchůdce	k1gMnSc3	předchůdce
zapojoval	zapojovat	k5eAaImAgInS	zapojovat
do	do	k7c2	do
říšských	říšský	k2eAgFnPc2d1	říšská
záležitostí	záležitost	k1gFnPc2	záležitost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pomoc	pomoc	k1gFnSc1	pomoc
příbuznému	příbuzný	k1gMnSc3	příbuzný
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
z	z	k7c2	z
Bogenu	Bogen	k1gInSc2	Bogen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1192	[number]	k4	1192
a	a	k8xC	a
1193	[number]	k4	1193
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
chystanou	chystaný	k2eAgFnSc7d1	chystaná
proticísařskou	proticísařský	k2eAgFnSc7d1	proticísařský
koalicí	koalice	k1gFnSc7	koalice
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
brabantským	brabantský	k2eAgMnSc7d1	brabantský
vévodou	vévoda	k1gMnSc7	vévoda
Jindřichem	Jindřich	k1gMnSc7	Jindřich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
hlavou	hlava	k1gFnSc7	hlava
welfské	welfský	k2eAgFnSc2d1	welfský
opozice	opozice	k1gFnSc2	opozice
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Lvem	Lev	k1gMnSc7	Lev
<g/>
,	,	kIx,	,
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
Richardem	Richard	k1gMnSc7	Richard
Lví	lví	k2eAgFnSc7d1	lví
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
Přemyslovým	Přemyslův	k2eAgMnSc7d1	Přemyslův
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
Albrechtem	Albrecht	k1gMnSc7	Albrecht
Míšeňským	míšeňský	k2eAgMnSc7d1	míšeňský
<g/>
.	.	kIx.	.
</s>
<s>
Spiknutí	spiknutí	k1gNnSc1	spiknutí
bylo	být	k5eAaImAgNnS	být
prozrazeno	prozradit	k5eAaPmNgNnS	prozradit
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
zajat	zajmout	k5eAaPmNgMnS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nejvíce	hodně	k6eAd3	hodně
doplatil	doplatit	k5eAaPmAgMnS	doplatit
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1193	[number]	k4	1193
zbavil	zbavit	k5eAaPmAgMnS	zbavit
knížecí	knížecí	k2eAgFnPc4d1	knížecí
hodnosti	hodnost	k1gFnPc4	hodnost
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
překvapivě	překvapivě	k6eAd1	překvapivě
dosadil	dosadit	k5eAaPmAgInS	dosadit
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Břetislava	Břetislav	k1gMnSc4	Břetislav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
biskupa	biskup	k1gMnSc4	biskup
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Břetislava	Břetislav	k1gMnSc4	Břetislav
ochoten	ochoten	k2eAgInSc1d1	ochoten
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
biskupovi	biskup	k1gMnSc3	biskup
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
než	než	k8xS	než
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
domoci	domoct	k5eAaPmF	domoct
vlastní	vlastní	k2eAgFnSc7d1	vlastní
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1193	[number]	k4	1193
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
Jindřich	Jindřich	k1gMnSc1	Jindřich
Břetislav	Břetislav	k1gMnSc1	Břetislav
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
Spytihněvem	Spytihněv	k1gInSc7	Spytihněv
a	a	k8xC	a
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
tak	tak	k6eAd1	tak
lehce	lehko	k6eAd1	lehko
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
silné	silný	k2eAgNnSc4d1	silné
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
nepřátelům	nepřítel	k1gMnPc3	nepřítel
vstříc	vstříc	k6eAd1	vstříc
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
u	u	k7c2	u
Zdic	Zdice	k1gFnPc2	Zdice
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
však	však	k9	však
většina	většina	k1gFnSc1	většina
předních	přední	k2eAgMnPc2d1	přední
velmožů	velmož	k1gMnPc2	velmož
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ještě	ještě	k6eAd1	ještě
nedávno	nedávno	k6eAd1	nedávno
slibovali	slibovat	k5eAaImAgMnP	slibovat
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
svou	svůj	k3xOyFgFnSc4	svůj
věrnost	věrnost	k1gFnSc4	věrnost
a	a	k8xC	a
nabízeli	nabízet	k5eAaImAgMnP	nabízet
jako	jako	k8xS	jako
rukojmí	rukojmí	k1gMnPc1	rukojmí
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Přemysla	Přemysl	k1gMnSc4	Přemysl
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
biskupova	biskupův	k2eAgInSc2d1	biskupův
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
Přemysl	Přemysl	k1gMnSc1	Přemysl
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
do	do	k7c2	do
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
následující	následující	k2eAgInSc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
hájil	hájit	k5eAaImAgMnS	hájit
<g/>
,	,	kIx,	,
než	než	k8xS	než
před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
roku	rok	k1gInSc2	rok
1193	[number]	k4	1193
hrad	hrad	k1gInSc1	hrad
kapituloval	kapitulovat	k5eAaBmAgInS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
ale	ale	k9	ale
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
s	s	k7c7	s
hrstkou	hrstka	k1gFnSc7	hrstka
svých	svůj	k3xOyFgFnPc2	svůj
věrných	věrný	k2eAgFnPc2d1	věrná
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
Břetislav	Břetislav	k1gMnSc1	Břetislav
podrobil	podrobit	k5eAaPmAgMnS	podrobit
i	i	k9	i
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
zajal	zajmout	k5eAaPmAgMnS	zajmout
Vladislava	Vladislav	k1gMnSc4	Vladislav
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Azyl	azyl	k1gInSc4	azyl
nalezl	naleznout	k5eAaPmAgMnS	naleznout
Přemysl	Přemysl	k1gMnSc1	Přemysl
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
Albrechta	Albrecht	k1gMnSc2	Albrecht
Míšenského	míšenský	k2eAgMnSc2d1	míšenský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
dalšího	další	k2eAgMnSc2d1	další
příbuzného	příbuzný	k1gMnSc2	příbuzný
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Bogenu	Bogen	k1gInSc2	Bogen
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
zdraví	zdraví	k1gNnSc1	zdraví
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Břetislava	Břetislav	k1gMnSc2	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgMnS	využít
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
až	až	k9	až
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
velmožů	velmož	k1gMnPc2	velmož
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
za	za	k7c4	za
nemocného	nemocný	k2eAgMnSc4d1	nemocný
biskupa	biskup	k1gMnSc4	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
výpad	výpad	k1gInSc4	výpad
českými	český	k2eAgMnPc7d1	český
velmoži	velmož	k1gMnPc7	velmož
a	a	k8xC	a
Spytihněvem	Spytihněv	k1gInSc7	Spytihněv
Brněnským	brněnský	k2eAgInSc7d1	brněnský
odražen	odražen	k2eAgInSc1d1	odražen
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
biskup	biskup	k1gMnSc1	biskup
přestával	přestávat	k5eAaImAgMnS	přestávat
důvěřovat	důvěřovat	k5eAaImF	důvěřovat
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Správou	správa	k1gFnSc7	správa
země	zem	k1gFnSc2	zem
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Spytihněva	Spytihněvo	k1gNnPc4	Spytihněvo
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
přenést	přenést	k5eAaPmF	přenést
na	na	k7c4	na
říšské	říšský	k2eAgNnSc4d1	říšské
území	území	k1gNnSc4	území
do	do	k7c2	do
Chebu	Cheb	k1gInSc2	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
nejistotě	nejistota	k1gFnSc6	nejistota
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1197	[number]	k4	1197
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
povolán	povolán	k2eAgMnSc1d1	povolán
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvotním	prvotní	k2eAgNnSc6d1	prvotní
překvapení	překvapení	k1gNnSc6	překvapení
ze	z	k7c2	z
zvolení	zvolení	k1gNnSc2	zvolení
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Přemysl	Přemysl	k1gMnSc1	Přemysl
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
využil	využít	k5eAaPmAgMnS	využít
nově	nově	k6eAd1	nově
nastalé	nastalý	k2eAgFnPc4d1	nastalá
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgMnS	mít
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc4d2	silnější
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
se	se	k3xPyFc4	se
po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
biskupem	biskup	k1gMnSc7	biskup
Danielem	Daniel	k1gMnSc7	Daniel
a	a	k8xC	a
velmoži	velmož	k1gMnPc7	velmož
tajně	tajně	k6eAd1	tajně
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
čtvrtstoletí	čtvrtstoletí	k1gNnSc6	čtvrtstoletí
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
knížecí	knížecí	k2eAgInSc4d1	knížecí
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
takřka	takřka	k6eAd1	takřka
neminul	minout	k5eNaImAgInS	minout
rok	rok	k1gInSc1	rok
bez	bez	k7c2	bez
bratrovražedných	bratrovražedný	k2eAgInPc2d1	bratrovražedný
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
nastaly	nastat	k5eAaPmAgFnP	nastat
konečně	konečně	k6eAd1	konečně
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
klidnější	klidný	k2eAgFnSc1d2	klidnější
časy	čas	k1gInPc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Přemyslem	Přemysl	k1gMnSc7	Přemysl
a	a	k8xC	a
Vladislavem	Vladislav	k1gMnSc7	Vladislav
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgMnS	být
i	i	k9	i
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
nebude	být	k5eNaImBp3nS	být
mstít	mstít	k5eAaImF	mstít
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
opakovaně	opakovaně	k6eAd1	opakovaně
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
zradili	zradit	k5eAaPmAgMnP	zradit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
sice	sice	k8xC	sice
asi	asi	k9	asi
zklamalo	zklamat	k5eAaPmAgNnS	zklamat
jeho	jeho	k3xOp3gInPc4	jeho
věrné	věrný	k2eAgInPc4d1	věrný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vytrvali	vytrvat	k5eAaPmAgMnP	vytrvat
ve	v	k7c4	v
vyhnanství	vyhnanství	k1gNnSc4	vyhnanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získalo	získat	k5eAaPmAgNnS	získat
mu	on	k3xPp3gMnSc3	on
důvěru	důvěra	k1gFnSc4	důvěra
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
části	část	k1gFnSc2	část
předáků	předák	k1gMnPc2	předák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
královskou	královský	k2eAgFnSc7d1	královská
korunou	koruna	k1gFnSc7	koruna
===	===	k?	===
</s>
</p>
<p>
<s>
Zklidnění	zklidnění	k1gNnSc1	zklidnění
domácích	domácí	k2eAgInPc2d1	domácí
poměrů	poměr	k1gInPc2	poměr
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
rozvinout	rozvinout	k5eAaPmF	rozvinout
čilou	čilý	k2eAgFnSc4d1	čilá
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
pozic	pozice	k1gFnPc2	pozice
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počínajícím	počínající	k2eAgInSc6d1	počínající
boji	boj	k1gInSc6	boj
o	o	k7c4	o
císařskou	císařský	k2eAgFnSc4d1	císařská
korunu	koruna	k1gFnSc4	koruna
mezi	mezi	k7c4	mezi
Štaufy	Štauf	k1gInPc4	Štauf
a	a	k8xC	a
Welfy	Welf	k1gInPc4	Welf
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Filipa	Filip	k1gMnSc2	Filip
Švábského	švábský	k2eAgMnSc2d1	švábský
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
spojenec	spojenec	k1gMnSc1	spojenec
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
k	k	k7c3	k
Rýnu	Rýn	k1gInSc3	Rýn
proti	proti	k7c3	proti
Otovi	Ota	k1gMnSc3	Ota
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Brunšvickému	brunšvický	k2eAgMnSc3d1	brunšvický
a	a	k8xC	a
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pomoci	pomoc	k1gFnSc2	pomoc
jej	on	k3xPp3gMnSc4	on
Filip	Filip	k1gMnSc1	Filip
v	v	k7c6	v
září	září	k1gNnSc6	září
či	či	k8xC	či
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1198	[number]	k4	1198
nechal	nechat	k5eAaPmAgInS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
vystavil	vystavit	k5eAaPmAgInS	vystavit
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
privilegium	privilegium	k1gNnSc4	privilegium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
ozvuky	ozvuk	k1gInPc1	ozvuk
můžeme	moct	k5eAaImIp1nP	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
bule	bula	k1gFnSc6	bula
sicilské	sicilský	k2eAgFnSc6d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
měli	mít	k5eAaImAgMnP	mít
Přemysl	Přemysl	k1gMnSc1	Přemysl
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
užívat	užívat	k5eAaImF	užívat
dědičně	dědičně	k6eAd1	dědičně
a	a	k8xC	a
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c2	za
krále	král	k1gMnSc2	král
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yRnSc4	kdo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
svým	svůj	k3xOyFgMnPc3	svůj
biskupům	biskup	k1gMnPc3	biskup
udělovat	udělovat	k5eAaImF	udělovat
investituru	investitura	k1gFnSc4	investitura
<g/>
.	.	kIx.	.
<g/>
Přemysl	Přemysl	k1gMnSc1	Přemysl
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
rozřešil	rozřešit	k5eAaPmAgMnS	rozřešit
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
obětí	oběť	k1gFnSc7	oběť
nové	nový	k2eAgFnSc2d1	nová
situace	situace	k1gFnSc2	situace
stal	stát	k5eAaPmAgMnS	stát
Spytihněv	Spytihněv	k1gMnSc1	Spytihněv
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
zřejmě	zřejmě	k6eAd1	zřejmě
roku	rok	k1gInSc2	rok
1198	[number]	k4	1198
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
zmrzačený	zmrzačený	k2eAgMnSc1d1	zmrzačený
příbuzný	příbuzný	k1gMnSc1	příbuzný
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1198	[number]	k4	1198
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
skonal	skonat	k5eAaPmAgMnS	skonat
i	i	k9	i
Spytihněvův	Spytihněvův	k2eAgMnSc1d1	Spytihněvův
bratr	bratr	k1gMnSc1	bratr
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
v	v	k7c6	v
letech	let	k1gInPc6	let
1200	[number]	k4	1200
<g/>
–	–	k?	–
<g/>
1201	[number]	k4	1201
za	za	k7c2	za
neznámých	známý	k2eNgFnPc2d1	neznámá
okolností	okolnost	k1gFnPc2	okolnost
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
i	i	k9	i
oba	dva	k4xCgMnPc1	dva
olomoučtí	olomoucký	k2eAgMnPc1d1	olomoucký
údělníci	údělník	k1gMnPc1	údělník
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Břetislavův	Břetislavův	k2eAgMnSc1d1	Břetislavův
syn	syn	k1gMnSc1	syn
Sifrid	Sifrida	k1gFnPc2	Sifrida
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
snad	snad	k9	snad
pod	pod	k7c7	pod
Přemyslovým	Přemyslův	k2eAgInSc7d1	Přemyslův
nátlakem	nátlak	k1gInSc7	nátlak
<g/>
)	)	kIx)	)
vzdal	vzdát	k5eAaPmAgMnS	vzdát
světské	světský	k2eAgFnPc4d1	světská
kariéry	kariéra	k1gFnPc4	kariéra
a	a	k8xC	a
zvolil	zvolit	k5eAaPmAgMnS	zvolit
si	se	k3xPyFc3	se
církevní	církevní	k2eAgFnSc4d1	církevní
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
znojemský	znojemský	k2eAgMnSc1d1	znojemský
a	a	k8xC	a
brněnský	brněnský	k2eAgInSc1d1	brněnský
úděl	úděl	k1gInSc1	úděl
pak	pak	k6eAd1	pak
převzal	převzít	k5eAaPmAgInS	převzít
markrabí	markrabí	k1gMnSc1	markrabí
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
si	se	k3xPyFc3	se
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
Přemysl	Přemysl	k1gMnSc1	Přemysl
ponechal	ponechat	k5eAaPmAgMnS	ponechat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
přímé	přímý	k2eAgFnSc6d1	přímá
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozvod	rozvod	k1gInSc1	rozvod
s	s	k7c7	s
Adlétou	Adléta	k1gFnSc7	Adléta
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
přibližně	přibližně	k6eAd1	přibližně
dvacetiletém	dvacetiletý	k2eAgNnSc6d1	dvacetileté
manželství	manželství	k1gNnSc6	manželství
Přemysl	Přemysl	k1gMnSc1	Přemysl
snad	snad	k9	snad
roku	rok	k1gInSc2	rok
1198	[number]	k4	1198
zapudil	zapudit	k5eAaPmAgInS	zapudit
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Adlétu	Adlét	k2eAgFnSc4d1	Adlét
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
formální	formální	k2eAgInSc1d1	formální
důvod	důvod	k1gInSc1	důvod
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
posloužil	posloužit	k5eAaPmAgMnS	posloužit
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
stupeň	stupeň	k1gInSc4	stupeň
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
(	(	kIx(	(
<g/>
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Žemlička	Žemlička	k1gMnSc1	Žemlička
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
"	"	kIx"	"
<g/>
nespoutaná	spoutaný	k2eNgFnSc1d1	nespoutaná
smyslnost	smyslnost	k1gFnSc1	smyslnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
(	(	kIx(	(
<g/>
Vaníček	Vaníček	k1gMnSc1	Vaníček
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
potvrzení	potvrzení	k1gNnSc4	potvrzení
královských	královský	k2eAgFnPc2d1	královská
ambicí	ambice	k1gFnPc2	ambice
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
nové	nový	k2eAgFnSc2d1	nová
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
<g/>
Novou	nový	k2eAgFnSc7d1	nová
Přemyslovou	Přemyslův	k2eAgFnSc7d1	Přemyslova
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1199	[number]	k4	1199
stala	stát	k5eAaPmAgFnS	stát
Konstancie	Konstancie	k1gFnSc1	Konstancie
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Emericha	Emerich	k1gMnSc2	Emerich
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
nové	nový	k2eAgFnSc6d1	nová
svatbě	svatba	k1gFnSc6	svatba
spor	spor	k1gInSc1	spor
neutichl	utichnout	k5eNaPmAgInS	utichnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Adléta	Adléta	k1gFnSc1	Adléta
požádala	požádat	k5eAaPmAgFnS	požádat
papeže	papež	k1gMnSc4	papež
Inocence	Inocenc	k1gMnSc4	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
svého	svůj	k3xOyFgNnSc2	svůj
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
zajištění	zajištění	k1gNnSc2	zajištění
nástupnických	nástupnický	k2eAgNnPc2d1	nástupnické
práv	právo	k1gNnPc2	právo
pro	pro	k7c4	pro
syna	syn	k1gMnSc4	syn
Vratislava	Vratislav	k1gMnSc4	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
odešla	odejít	k5eAaPmAgFnS	odejít
zapuzená	zapuzený	k2eAgFnSc1d1	zapuzená
žena	žena	k1gFnSc1	žena
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Dětřichovi	Dětřich	k1gMnSc3	Dětřich
a	a	k8xC	a
strýci	strýc	k1gMnSc3	strýc
Dedovi	Deda	k1gMnSc3	Deda
a	a	k8xC	a
ve	v	k7c6	v
Wettinech	Wettin	k1gMnPc6	Wettin
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
Přemysl	Přemysl	k1gMnSc1	Přemysl
nesmiřitelné	smiřitelný	k2eNgMnPc4d1	nesmiřitelný
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
listu	list	k1gInSc6	list
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
kléru	klér	k1gInSc2	klér
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
"	"	kIx"	"
<g/>
českém	český	k2eAgMnSc6d1	český
vévodovi	vévoda	k1gMnSc6	vévoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
zapudil	zapudit	k5eAaPmAgMnS	zapudit
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
cizoložnici	cizoložnice	k1gFnSc4	cizoložnice
<g/>
.	.	kIx.	.
<g/>
Adléta	Adléta	k1gFnSc1	Adléta
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
nakrátko	nakrátko	k6eAd1	nakrátko
v	v	k7c6	v
letech	let	k1gInPc6	let
1204	[number]	k4	1204
<g/>
–	–	k?	–
<g/>
1205	[number]	k4	1205
přijata	přijmout	k5eAaPmNgFnS	přijmout
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c6	o
Filipem	Filip	k1gMnSc7	Filip
Švábským	švábský	k2eAgFnPc3d1	Švábská
vynucené	vynucený	k2eAgNnSc4d1	vynucené
politické	politický	k2eAgNnSc4d1	politické
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Změněné	změněný	k2eAgFnPc4d1	změněná
situace	situace	k1gFnPc4	situace
využil	využít	k5eAaPmAgMnS	využít
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1205	[number]	k4	1205
provdal	provdat	k5eAaPmAgMnS	provdat
svou	svůj	k3xOyFgFnSc4	svůj
a	a	k8xC	a
Adlétinu	Adlétin	k2eAgFnSc4d1	Adlétin
dceru	dcera	k1gFnSc4	dcera
Markétu	Markéta	k1gFnSc4	Markéta
za	za	k7c4	za
dánského	dánský	k2eAgMnSc4d1	dánský
krále	král	k1gMnSc4	král
Valdemara	Valdemara	k1gFnSc1	Valdemara
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1205	[number]	k4	1205
obdařila	obdařit	k5eAaPmAgFnS	obdařit
Konstancie	Konstancie	k1gFnSc1	Konstancie
Přemysla	Přemysl	k1gMnSc2	Přemysl
synem	syn	k1gMnSc7	syn
Václavem	Václav	k1gMnSc7	Václav
a	a	k8xC	a
král	král	k1gMnSc1	král
opětovně	opětovně	k6eAd1	opětovně
zapudil	zapudit	k5eAaPmAgMnS	zapudit
Adlétu	Adléta	k1gFnSc4	Adléta
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
bral	brát	k5eAaImAgInS	brát
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
mohl	moct	k5eAaImAgInS	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
roku	rok	k1gInSc2	rok
1206	[number]	k4	1206
zahájil	zahájit	k5eAaPmAgInS	zahájit
nový	nový	k2eAgInSc1d1	nový
proces	proces	k1gInSc1	proces
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
nevhodnému	vhodný	k2eNgInSc3d1	nevhodný
<g/>
"	"	kIx"	"
manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
uvržen	uvrhnout	k5eAaPmNgMnS	uvrhnout
do	do	k7c2	do
církevní	církevní	k2eAgFnSc2d1	církevní
klatby	klatba	k1gFnSc2	klatba
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc4	spor
nakonec	nakonec	k6eAd1	nakonec
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
až	až	k9	až
Adlétina	Adlétin	k2eAgFnSc1d1	Adlétin
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1211	[number]	k4	1211
<g/>
.	.	kIx.	.
</s>
<s>
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
nakonec	nakonec	k6eAd1	nakonec
legitimoval	legitimovat	k5eAaBmAgInS	legitimovat
všechny	všechen	k3xTgFnPc4	všechen
královy	králův	k2eAgFnPc4d1	králova
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
obou	dva	k4xCgNnPc2	dva
manželství	manželství	k1gNnPc2	manželství
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
uznal	uznat	k5eAaPmAgInS	uznat
svazek	svazek	k1gInSc4	svazek
s	s	k7c7	s
Konstancií	Konstancie	k1gFnSc7	Konstancie
<g/>
.	.	kIx.	.
</s>
<s>
Prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
Vratislav	Vratislav	k1gMnSc1	Vratislav
se	se	k3xPyFc4	se
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůnu	trůn	k1gInSc6	trůn
nikdy	nikdy	k6eAd1	nikdy
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
říšský	říšský	k2eAgInSc4d1	říšský
trůn	trůn	k1gInSc4	trůn
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
říšský	říšský	k2eAgInSc4d1	říšský
trůn	trůn	k1gInSc4	trůn
hodlal	hodlat	k5eAaImAgInS	hodlat
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1201	[number]	k4	1201
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Otu	Ota	k1gMnSc4	Ota
Brunšvického	brunšvický	k2eAgMnSc4d1	brunšvický
za	za	k7c4	za
jediného	jediný	k2eAgMnSc4d1	jediný
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Přemysla	Přemysl	k1gMnSc2	Přemysl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
sliboval	slibovat	k5eAaImAgMnS	slibovat
legitimní	legitimní	k2eAgNnSc4d1	legitimní
udělení	udělení	k1gNnSc4	udělení
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
bez	bez	k7c2	bez
papežova	papežův	k2eAgInSc2d1	papežův
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1201	[number]	k4	1201
<g/>
–	–	k?	–
<g/>
1202	[number]	k4	1202
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jednotná	jednotný	k2eAgFnSc1d1	jednotná
fronta	fronta	k1gFnSc1	fronta
štaufských	štaufský	k2eAgMnPc2d1	štaufský
spojenců	spojenec	k1gMnPc2	spojenec
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
rozpadat	rozpadat	k5eAaImF	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Přemysl	Přemysl	k1gMnSc1	Přemysl
tehdy	tehdy	k6eAd1	tehdy
zahájil	zahájit	k5eAaPmAgMnS	zahájit
proslulou	proslulý	k2eAgFnSc4d1	proslulá
sérii	série	k1gFnSc4	série
změn	změna	k1gFnPc2	změna
svého	své	k1gNnSc2	své
stranictví	stranictví	k1gNnSc2	stranictví
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
chovala	chovat	k5eAaImAgFnS	chovat
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Přemyslovo	Přemyslův	k2eAgNnSc4d1	Přemyslovo
odpadnutí	odpadnutí	k1gNnSc4	odpadnutí
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1203	[number]	k4	1203
udělil	udělit	k5eAaPmAgInS	udělit
Čechy	Čech	k1gMnPc4	Čech
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
Děpoltovi	Děpoltův	k2eAgMnPc1d1	Děpoltův
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
s	s	k7c7	s
Přemyslem	Přemysl	k1gMnSc7	Přemysl
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neznámém	známý	k2eNgInSc6d1	neznámý
sporu	spor	k1gInSc6	spor
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
měl	mít	k5eAaImAgMnS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
Filipově	Filipův	k2eAgFnSc6d1	Filipova
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1203	[number]	k4	1203
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následných	následný	k2eAgFnPc2d1	následná
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1203	[number]	k4	1203
podruhé	podruhé	k6eAd1	podruhé
korunován	korunovat	k5eAaBmNgInS	korunovat
papežským	papežský	k2eAgMnSc7d1	papežský
legátem	legát	k1gMnSc7	legát
Guidem	Guid	k1gMnSc7	Guid
z	z	k7c2	z
Praeneste	Praenest	k1gInSc5	Praenest
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mu	on	k3xPp3gMnSc3	on
Ota	Ota	k1gMnSc1	Ota
Brunšvický	brunšvický	k2eAgInSc1d1	brunšvický
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
všechny	všechen	k3xTgFnPc4	všechen
dříve	dříve	k6eAd2	dříve
udělené	udělený	k2eAgFnPc4d1	udělená
královské	královský	k2eAgFnPc4d1	královská
výsady	výsada	k1gFnPc4	výsada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
pak	pak	k6eAd1	pak
Přemyslův	Přemyslův	k2eAgInSc4d1	Přemyslův
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
uznal	uznat	k5eAaPmAgMnS	uznat
i	i	k9	i
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příznivou	příznivý	k2eAgFnSc4d1	příznivá
situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
pokusil	pokusit	k5eAaPmAgMnS	pokusit
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
povýšení	povýšení	k1gNnSc3	povýšení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c6	na
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žádost	žádost	k1gFnSc1	žádost
papeži	papež	k1gMnSc3	papež
nebyla	být	k5eNaImAgFnS	být
vyslyšena	vyslyšet	k5eAaPmNgFnS	vyslyšet
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
slabou	slabý	k2eAgFnSc7d1	slabá
útěchou	útěcha	k1gFnSc7	útěcha
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
kanonizace	kanonizace	k1gFnSc2	kanonizace
zakladatele	zakladatel	k1gMnSc2	zakladatel
sázavského	sázavský	k2eAgInSc2d1	sázavský
kláštera	klášter	k1gInSc2	klášter
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
rovněž	rovněž	k9	rovněž
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
nové	nový	k2eAgFnSc6d1	nová
vojenské	vojenský	k2eAgFnSc6d1	vojenská
ofenzívě	ofenzíva	k1gFnSc6	ofenzíva
Filipa	Filip	k1gMnSc2	Filip
Švábského	švábský	k2eAgInSc2d1	švábský
pracně	pracně	k6eAd1	pracně
budovaná	budovaný	k2eAgFnSc1d1	budovaná
welfská	welfský	k2eAgFnSc1d1	welfský
koalice	koalice	k1gFnSc1	koalice
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Filipových	Filipových	k2eAgMnPc2d1	Filipových
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1205	[number]	k4	1205
byl	být	k5eAaImAgMnS	být
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
pomazán	pomazat	k5eAaPmNgMnS	pomazat
a	a	k8xC	a
korunován	korunovat	k5eAaBmNgMnS	korunovat
říšským	říšský	k2eAgMnSc7d1	říšský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
<g/>
)	)	kIx)	)
a	a	k8xC	a
korunovaci	korunovace	k1gFnSc4	korunovace
provedl	provést	k5eAaPmAgMnS	provést
správný	správný	k2eAgMnSc1d1	správný
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
(	(	kIx(	(
<g/>
kolínský	kolínský	k2eAgMnSc1d1	kolínský
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
padly	padnout	k5eAaPmAgFnP	padnout
poslední	poslední	k2eAgFnPc4d1	poslední
formální	formální	k2eAgFnPc4d1	formální
a	a	k8xC	a
procedurální	procedurální	k2eAgFnPc4d1	procedurální
námitky	námitka	k1gFnPc4	námitka
proti	proti	k7c3	proti
Filipově	Filipův	k2eAgFnSc3d1	Filipova
volbě	volba	k1gFnSc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
získával	získávat	k5eAaImAgMnS	získávat
převahu	převaha	k1gFnSc4	převaha
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1208	[number]	k4	1208
svolal	svolat	k5eAaPmAgMnS	svolat
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
definitivně	definitivně	k6eAd1	definitivně
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
welfský	welfský	k2eAgInSc4d1	welfský
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1207	[number]	k4	1207
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Švábský	švábský	k2eAgMnSc1d1	švábský
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
zásnuby	zásnuba	k1gFnSc2	zásnuba
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
dvouletým	dvouletý	k2eAgMnSc7d1	dvouletý
Přemyslovým	Přemyslův	k2eAgMnSc7d1	Přemyslův
synem	syn	k1gMnSc7	syn
Václavem	Václav	k1gMnSc7	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1208	[number]	k4	1208
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
něco	něco	k6eAd1	něco
"	"	kIx"	"
<g/>
u	u	k7c2	u
Němců	Němec	k1gMnPc2	Němec
dosud	dosud	k6eAd1	dosud
neslýchaného	slýchaný	k2eNgInSc2d1	neslýchaný
<g/>
"	"	kIx"	"
a	a	k8xC	a
bavorský	bavorský	k2eAgMnSc1d1	bavorský
falckrabí	falckrabí	k1gMnSc1	falckrabí
Ota	Ota	k1gMnSc1	Ota
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
nic	nic	k6eAd1	nic
netušícího	tušící	k2eNgMnSc4d1	netušící
Filipa	Filip	k1gMnSc4	Filip
v	v	k7c6	v
době	doba	k1gFnSc6	doba
polední	polední	k2eAgFnSc2d1	polední
siesty	siesta	k1gFnSc2	siesta
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
osobní	osobní	k2eAgFnSc2d1	osobní
motivace	motivace	k1gFnSc2	motivace
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Královraždu	královražda	k1gFnSc4	královražda
pomstil	pomstit	k5eAaImAgMnS	pomstit
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Kalden	Kaldna	k1gFnPc2	Kaldna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
exkomunikovaného	exkomunikovaný	k2eAgMnSc4d1	exkomunikovaný
vraha	vrah	k1gMnSc4	vrah
po	po	k7c6	po
roce	rok	k1gInSc6	rok
našel	najít	k5eAaPmAgMnS	najít
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1209	[number]	k4	1209
zabil	zabít	k5eAaPmAgMnS	zabít
nedaleko	nedaleko	k7c2	nedaleko
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
.	.	kIx.	.
<g/>
Smrtí	smrt	k1gFnSc7	smrt
Filipa	Filip	k1gMnSc2	Filip
Švábského	švábský	k2eAgMnSc2d1	švábský
však	však	k8xC	však
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
Přemyslovy	Přemyslův	k2eAgInPc1d1	Přemyslův
plány	plán	k1gInPc1	plán
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budoucnost	budoucnost	k1gFnSc4	budoucnost
pojily	pojit	k5eAaImAgInP	pojit
s	s	k7c7	s
Filipem	Filip	k1gMnSc7	Filip
a	a	k8xC	a
s	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
k	k	k7c3	k
Welfům	Welf	k1gInPc3	Welf
již	již	k6eAd1	již
nepočítaly	počítat	k5eNaImAgFnP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1209	[number]	k4	1209
byl	být	k5eAaImAgMnS	být
Ota	Ota	k1gMnSc1	Ota
Brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
korunován	korunovat	k5eAaBmNgMnS	korunovat
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
nadále	nadále	k6eAd1	nadále
chladný	chladný	k2eAgMnSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Obnovováním	obnovování	k1gNnSc7	obnovování
říšské	říšský	k2eAgFnSc2d1	říšská
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
však	však	k9	však
zaskočil	zaskočit	k5eAaPmAgMnS	zaskočit
Inocence	Inocenc	k1gMnSc4	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1210	[number]	k4	1210
exkomunikoval	exkomunikovat	k5eAaBmAgInS	exkomunikovat
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
říšská	říšský	k2eAgNnPc4d1	říšské
knížata	kníže	k1gNnPc4	kníže
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
volbě	volba	k1gFnSc3	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1211	[number]	k4	1211
začalo	začít	k5eAaPmAgNnS	začít
vznikat	vznikat	k5eAaImF	vznikat
"	"	kIx"	"
<g/>
společenství	společenství	k1gNnSc1	společenství
<g/>
"	"	kIx"	"
císařových	císařová	k1gFnPc2	císařová
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samém	samý	k3xTgInSc6	samý
jádru	jádro	k1gNnSc6	jádro
tohoto	tento	k3xDgNnSc2	tento
společenství	společenství	k1gNnSc2	společenství
stál	stát	k5eAaImAgMnS	stát
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
mohučský	mohučský	k2eAgMnSc1d1	mohučský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Sigfried	Sigfried	k1gMnSc1	Sigfried
a	a	k8xC	a
durynský	durynský	k2eAgMnSc1d1	durynský
lantkrabě	lantkrabě	k1gMnSc1	lantkrabě
Heřman	Heřman	k1gMnSc1	Heřman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vévoda	vévoda	k1gMnSc1	vévoda
Leopold	Leopold	k1gMnSc1	Leopold
a	a	k8xC	a
bavorský	bavorský	k2eAgMnSc1d1	bavorský
vévoda	vévoda	k1gMnSc1	vévoda
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
zde	zde	k6eAd1	zde
uvedení	uvedený	k2eAgMnPc1d1	uvedený
se	se	k3xPyFc4	se
začátkem	začátek	k1gInSc7	začátek
září	září	k1gNnSc2	září
sjeli	sjet	k5eAaPmAgMnP	sjet
do	do	k7c2	do
Norimberku	Norimberk	k1gInSc2	Norimberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvolili	zvolit	k5eAaPmAgMnP	zvolit
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
předběžně	předběžně	k6eAd1	předběžně
vyvolili	vyvolit	k5eAaPmAgMnP	vyvolit
<g/>
)	)	kIx)	)
sicilského	sicilský	k2eAgMnSc4d1	sicilský
krále	král	k1gMnSc4	král
Fridricha	Fridrich	k1gMnSc4	Fridrich
za	za	k7c2	za
německého	německý	k2eAgMnSc2d1	německý
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
vytáhl	vytáhnout	k5eAaPmAgInS	vytáhnout
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
Fridrich	Fridrich	k1gMnSc1	Fridrich
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
postupně	postupně	k6eAd1	postupně
přidávali	přidávat	k5eAaImAgMnP	přidávat
další	další	k2eAgMnPc1d1	další
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
začal	začít	k5eAaPmAgInS	začít
odměňovat	odměňovat	k5eAaImF	odměňovat
své	svůj	k3xOyFgInPc4	svůj
věrné	věrný	k2eAgInPc4d1	věrný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvíce	hodně	k6eAd3	hodně
oceněné	oceněný	k2eAgInPc4d1	oceněný
patřili	patřit	k5eAaImAgMnP	patřit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
i	i	k8xC	i
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
<g/>
Ota	Ota	k1gMnSc1	Ota
Brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
dodržel	dodržet	k5eAaPmAgMnS	dodržet
slovo	slovo	k1gNnSc4	slovo
dané	daný	k2eAgFnSc2d1	daná
míšeňskému	míšeňský	k2eAgMnSc3d1	míšeňský
markraběti	markrabě	k1gMnSc3	markrabě
Dětřichovi	Dětřich	k1gMnSc3	Dětřich
a	a	k8xC	a
české	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
odňal	odnít	k5eAaPmAgMnS	odnít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1212	[number]	k4	1212
vystavil	vystavit	k5eAaPmAgMnS	vystavit
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štaufský	Štaufský	k2eAgMnSc1d1	Štaufský
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakarovi	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
a	a	k8xC	a
Vladislavu	Vladislav	k1gMnSc6	Vladislav
Jindřichovi	Jindřich	k1gMnSc6	Jindřich
tři	tři	k4xCgFnPc4	tři
listiny	listina	k1gFnPc4	listina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zpečetěny	zpečetit	k5eAaPmNgFnP	zpečetit
zlatou	zlatý	k2eAgFnSc7d1	zlatá
pečetí	pečeť	k1gFnSc7	pečeť
(	(	kIx(	(
<g/>
bulou	bula	k1gFnSc7	bula
<g/>
)	)	kIx)	)
sicilského	sicilský	k2eAgMnSc4d1	sicilský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
majestátní	majestátní	k2eAgFnSc2d1	majestátní
pečeti	pečeť	k1gFnSc2	pečeť
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
vžitý	vžitý	k2eAgInSc1d1	vžitý
název	název	k1gInSc1	název
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Pečeť	pečeť	k1gFnSc1	pečeť
použitá	použitý	k2eAgFnSc1d1	použitá
u	u	k7c2	u
listin	listina	k1gFnPc2	listina
nebyla	být	k5eNaImAgFnS	být
říšská	říšský	k2eAgFnSc1d1	říšská
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
říšskou	říšský	k2eAgFnSc4d1	říšská
pečeť	pečeť	k1gFnSc4	pečeť
ještě	ještě	k6eAd1	ještě
neměl	mít	k5eNaImAgMnS	mít
a	a	k8xC	a
ani	ani	k8xC	ani
jí	jíst	k5eAaImIp3nS	jíst
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
pouze	pouze	k6eAd1	pouze
designovaný	designovaný	k2eAgMnSc1d1	designovaný
vládce	vládce	k1gMnSc1	vládce
<g/>
)	)	kIx)	)
mít	mít	k5eAaImF	mít
nemohl	moct	k5eNaImAgInS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
listiny	listina	k1gFnPc1	listina
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
a	a	k8xC	a
navenek	navenek	k6eAd1	navenek
tvoří	tvořit	k5eAaImIp3nP	tvořit
nerozdílný	rozdílný	k2eNgInSc4d1	nerozdílný
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
předlohy	předloha	k1gFnSc2	předloha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
písař	písař	k1gMnSc1	písař
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
latinsky	latinsky	k6eAd1	latinsky
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Fredericus	Fredericus	k1gInSc1	Fredericus
divina	divin	k2eAgInSc2d1	divin
favente	favent	k1gInSc5	favent
clementia	clementium	k1gNnSc2	clementium
Romanorum	Romanorum	k1gNnSc1	Romanorum
imperator	imperator	k1gInSc1	imperator
electus	electus	k1gMnSc1	electus
et	et	k?	et
semper	semper	k1gMnSc1	semper
augustus	augustus	k1gMnSc1	augustus
<g/>
,	,	kIx,	,
rex	rex	k?	rex
Sicilie	Sicilie	k1gFnSc1	Sicilie
<g/>
,	,	kIx,	,
ducatus	ducatus	k1gMnSc1	ducatus
Apulie	Apulie	k1gFnSc2	Apulie
et	et	k?	et
principatus	principatus	k1gInSc1	principatus
Capue	Capue	k1gFnSc1	Capue
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
upravovala	upravovat	k5eAaImAgFnS	upravovat
především	především	k9	především
postavení	postavení	k1gNnSc4	postavení
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
získal	získat	k5eAaPmAgMnS	získat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
potvrzení	potvrzení	k1gNnSc4	potvrzení
královské	královský	k2eAgFnSc2d1	královská
hodnosti	hodnost	k1gFnSc2	hodnost
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
,	,	kIx,	,
potvrzení	potvrzení	k1gNnSc4	potvrzení
svobodné	svobodný	k2eAgFnSc2d1	svobodná
volby	volba	k1gFnSc2	volba
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
investitury	investitura	k1gFnSc2	investitura
pražského	pražský	k2eAgInSc2d1	pražský
i	i	k8xC	i
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
biskupa	biskup	k1gInSc2	biskup
<g/>
,	,	kIx,	,
záruku	záruka	k1gFnSc4	záruka
neporušitelnosti	neporušitelnost	k1gFnSc2	neporušitelnost
zemských	zemský	k2eAgFnPc2d1	zemská
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Vladislav	Vladislav	k1gMnSc1	Vladislav
pak	pak	k6eAd1	pak
potvrzení	potvrzení	k1gNnSc3	potvrzení
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
"	"	kIx"	"
<g/>
markrabstvím	markrabství	k1gNnSc7	markrabství
moravským	moravský	k2eAgNnSc7d1	Moravské
<g/>
"	"	kIx"	"
aníž	aníž	k6eAd1	aníž
by	by	kYmCp3nS	by
však	však	k9	však
přitom	přitom	k6eAd1	přitom
byl	být	k5eAaImAgInS	být
narušen	narušen	k2eAgInSc1d1	narušen
Přemyslův	Přemyslův	k2eAgInSc1d1	Přemyslův
mocenský	mocenský	k2eAgInSc1d1	mocenský
primát	primát	k1gInSc1	primát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
s	s	k7c7	s
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
biskupa	biskup	k1gMnSc2	biskup
Daniela	Daniel	k1gMnSc2	Daniel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1214	[number]	k4	1214
se	se	k3xPyFc4	se
novým	nový	k2eAgInSc7d1	nový
pražským	pražský	k2eAgInSc7d1	pražský
biskupem	biskup	k1gInSc7	biskup
stal	stát	k5eAaPmAgMnS	stát
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgInSc7	ten
Ondřej	Ondřej	k1gMnSc1	Ondřej
zastával	zastávat	k5eAaImAgMnS	zastávat
četné	četný	k2eAgFnPc4d1	četná
církevní	církevní	k2eAgFnPc4d1	církevní
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1207	[number]	k4	1207
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
probošt	probošt	k1gMnSc1	probošt
kapituly	kapitula	k1gFnSc2	kapitula
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
funkcí	funkce	k1gFnSc7	funkce
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
spjat	spjat	k2eAgMnSc1d1	spjat
i	i	k8xC	i
úřad	úřad	k1gInSc1	úřad
králova	králův	k2eAgMnSc2d1	králův
kancléře	kancléř	k1gMnSc2	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bezproblémový	bezproblémový	k2eAgInSc4d1	bezproblémový
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
Přemyslem	Přemysl	k1gMnSc7	Přemysl
a	a	k8xC	a
Ondřejem	Ondřej	k1gMnSc7	Ondřej
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
Ondřejova	Ondřejův	k2eAgFnSc1d1	Ondřejova
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
Čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
lateránském	lateránský	k2eAgInSc6d1	lateránský
koncilu	koncil	k1gInSc6	koncil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1215	[number]	k4	1215
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
jako	jako	k8xS	jako
svébytném	svébytný	k2eAgNnSc6d1	svébytné
tělese	těleso	k1gNnSc6	těleso
vzal	vzít	k5eAaPmAgInS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
prosadit	prosadit	k5eAaPmF	prosadit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Ondřejovy	Ondřejův	k2eAgFnPc1d1	Ondřejova
představy	představa	k1gFnPc1	představa
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1216	[number]	k4	1216
biskup	biskup	k1gInSc1	biskup
narychlo	narychlo	k6eAd1	narychlo
opustil	opustit	k5eAaPmAgInS	opustit
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
,	,	kIx,	,
odebral	odebrat	k5eAaPmAgMnS	odebrat
se	se	k3xPyFc4	se
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nad	nad	k7c7	nad
pražskou	pražský	k2eAgFnSc7d1	Pražská
diecézí	diecéze	k1gFnSc7	diecéze
interdikt	interdikt	k1gInSc1	interdikt
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Přemysla	Přemysl	k1gMnSc4	Přemysl
a	a	k8xC	a
českou	český	k2eAgFnSc4d1	Česká
šlechtu	šlechta	k1gFnSc4	šlechta
z	z	k7c2	z
omezování	omezování	k1gNnSc2	omezování
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
poukázal	poukázat	k5eAaPmAgInS	poukázat
na	na	k7c4	na
složité	složitý	k2eAgNnSc4d1	složité
spolužití	spolužití	k1gNnSc4	spolužití
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
říšských	říšský	k2eAgNnPc2d1	říšské
biskupství	biskupství	k1gNnPc2	biskupství
bylo	být	k5eAaImAgNnS	být
pražské	pražský	k2eAgNnSc1d1	Pražské
i	i	k8xC	i
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
biskupství	biskupství	k1gNnSc1	biskupství
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
závislé	závislý	k2eAgInPc4d1	závislý
na	na	k7c6	na
Přemyslovcích	Přemyslovec	k1gMnPc6	Přemyslovec
a	a	k8xC	a
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
plnil	plnit	k5eAaImAgMnS	plnit
spíše	spíše	k9	spíše
funkci	funkce	k1gFnSc4	funkce
osobního	osobní	k2eAgMnSc2d1	osobní
panovníkova	panovníkův	k2eAgMnSc2d1	panovníkův
kaplana	kaplan	k1gMnSc2	kaplan
<g/>
.	.	kIx.	.
<g/>
Pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
Ondřej	Ondřej	k1gMnSc1	Ondřej
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
především	především	k9	především
na	na	k7c4	na
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
odvádění	odvádění	k1gNnSc4	odvádění
desátků	desátek	k1gInPc2	desátek
<g/>
,	,	kIx,	,
že	že	k8xS	že
kněze	kněz	k1gMnPc4	kněz
ke	k	k7c3	k
kostelům	kostel	k1gInPc3	kostel
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
laikové	laik	k1gMnPc1	laik
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
na	na	k7c4	na
souzení	souzení	k1gNnSc4	souzení
duchovních	duchovní	k2eAgFnPc2d1	duchovní
osob	osoba	k1gFnPc2	osoba
světskými	světský	k2eAgInPc7d1	světský
soudy	soud	k1gInPc7	soud
apod.	apod.	kA	apod.
Přemysl	Přemysl	k1gMnSc1	Přemysl
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgNnSc4d1	počáteční
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
biskupových	biskupův	k2eAgNnPc2d1	biskupovo
obvinění	obvinění	k1gNnPc2	obvinění
posléze	posléze	k6eAd1	posléze
některé	některý	k3yIgInPc4	některý
přehmaty	přehmat	k1gInPc4	přehmat
uznal	uznat	k5eAaPmAgInS	uznat
a	a	k8xC	a
sliboval	slibovat	k5eAaImAgInS	slibovat
jejich	jejich	k3xOp3gFnSc4	jejich
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
navenek	navenek	k6eAd1	navenek
se	se	k3xPyFc4	se
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
stavěla	stavět	k5eAaImAgFnS	stavět
do	do	k7c2	do
role	role	k1gFnSc2	role
zprostředkovatele	zprostředkovatel	k1gMnSc2	zprostředkovatel
<g/>
,	,	kIx,	,
vcelku	vcelku	k6eAd1	vcelku
pochopitelně	pochopitelně	k6eAd1	pochopitelně
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
stranila	stranit	k5eAaImAgFnS	stranit
Ondřejovi	Ondřejův	k2eAgMnPc1d1	Ondřejův
<g/>
.	.	kIx.	.
<g/>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
měl	mít	k5eAaImAgMnS	mít
Přemysl	Přemysl	k1gMnSc1	Přemysl
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
straně	strana	k1gFnSc6	strana
především	především	k6eAd1	především
představitele	představitel	k1gMnPc4	představitel
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
straně	strana	k1gFnSc6	strana
stáli	stát	k5eAaImAgMnP	stát
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
církevní	církevní	k2eAgMnPc1d1	církevní
představitelé	představitel	k1gMnPc1	představitel
(	(	kIx(	(
<g/>
pražský	pražský	k2eAgMnSc1d1	pražský
děkan	děkan	k1gMnSc1	děkan
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
litoměřický	litoměřický	k2eAgMnSc1d1	litoměřický
probošt	probošt	k1gMnSc1	probošt
Benedikt	Benedikt	k1gMnSc1	Benedikt
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Robert	Robert	k1gMnSc1	Robert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
pomalu	pomalu	k6eAd1	pomalu
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
i	i	k9	i
podporu	podpora	k1gFnSc4	podpora
kléru	klér	k1gInSc2	klér
své	svůj	k3xOyFgFnSc2	svůj
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
kompromisem	kompromis	k1gInSc7	kompromis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
na	na	k7c6	na
Šacké	Šacký	k2eAgFnSc6d1	Šacký
hoře	hora	k1gFnSc6	hora
někde	někde	k6eAd1	někde
na	na	k7c6	na
moravsko-rakouském	moravskoakouský	k2eAgNnSc6d1	moravsko-rakouský
pomezí	pomezí	k1gNnSc6	pomezí
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
papežského	papežský	k2eAgMnSc2d1	papežský
legáta	legát	k1gMnSc2	legát
Řehoře	Řehoř	k1gMnSc2	Řehoř
de	de	k?	de
Crescencio	Crescencio	k6eAd1	Crescencio
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
prelátů	prelát	k1gInPc2	prelát
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vyhlášený	vyhlášený	k2eAgInSc4d1	vyhlášený
kompromis	kompromis	k1gInSc4	kompromis
uspokojil	uspokojit	k5eAaPmAgMnS	uspokojit
jak	jak	k6eAd1	jak
Přemysla	Přemysl	k1gMnSc4	Přemysl
<g/>
,	,	kIx,	,
tak	tak	k9	tak
papeže	papež	k1gMnSc4	papež
Honoria	Honorium	k1gNnSc2	Honorium
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
nespokojeným	spokojený	k2eNgInSc7d1	nespokojený
tak	tak	k8xC	tak
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
biskup	biskup	k1gMnSc1	biskup
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
s	s	k7c7	s
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc4d1	definitivní
tečku	tečka	k1gFnSc4	tečka
za	za	k7c7	za
sporem	spor	k1gInSc7	spor
znamenalo	znamenat	k5eAaImAgNnS	znamenat
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
privilegium	privilegium	k1gNnSc1	privilegium
české	český	k2eAgFnSc2d1	Česká
církve	církev	k1gFnSc2	církev
vydané	vydaný	k2eAgFnSc2d1	vydaná
Přemyslem	Přemysl	k1gMnSc7	Přemysl
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1222	[number]	k4	1222
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamenalo	znamenat	k5eAaImAgNnS	znamenat
omezení	omezení	k1gNnSc4	omezení
pravomocí	pravomoc	k1gFnPc2	pravomoc
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
vůči	vůči	k7c3	vůči
jimi	on	k3xPp3gMnPc7	on
zakládaným	zakládaný	k2eAgFnPc3d1	zakládaná
církevním	církevní	k2eAgFnPc3d1	církevní
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řešení	řešení	k1gNnSc1	řešení
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
současně	současně	k6eAd1	současně
se	s	k7c7	s
sporem	spor	k1gInSc7	spor
s	s	k7c7	s
biskupem	biskup	k1gMnSc7	biskup
Ondřejem	Ondřej	k1gMnSc7	Ondřej
věnoval	věnovat	k5eAaImAgMnS	věnovat
Přemysl	Přemysl	k1gMnSc1	Přemysl
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozornost	pozornost	k1gFnSc4	pozornost
i	i	k8xC	i
otázce	otázka	k1gFnSc3	otázka
svého	svůj	k3xOyFgMnSc2	svůj
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
zmiňovala	zmiňovat	k5eAaImAgFnS	zmiňovat
dědičné	dědičný	k2eAgNnSc4d1	dědičné
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
král	král	k1gMnSc1	král
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
nesamozřejmost	nesamozřejmost	k1gFnSc4	nesamozřejmost
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
snažil	snažit	k5eAaImAgInS	snažit
o	o	k7c6	o
potvrzení	potvrzení	k1gNnSc3	potvrzení
jeho	jeho	k3xOp3gNnPc2	jeho
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
největší	veliký	k2eAgFnSc4d3	veliký
komplikaci	komplikace	k1gFnSc4	komplikace
představoval	představovat	k5eAaImAgInS	představovat
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
,	,	kIx,	,
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
syn	syn	k1gMnSc1	syn
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Adlétou	Adléta	k1gFnSc7	Adléta
Míšeňskou	míšeňský	k2eAgFnSc7d1	Míšeňská
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
podpoře	podpora	k1gFnSc3	podpora
svých	svůj	k3xOyFgFnPc2	svůj
míšeňských	míšeňský	k2eAgFnPc2d1	Míšeňská
příbuzných	příbuzná	k1gFnPc2	příbuzná
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
Václava	Václav	k1gMnSc2	Václav
vystupňovali	vystupňovat	k5eAaPmAgMnP	vystupňovat
své	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
i	i	k8xC	i
Děpoltici	Děpoltice	k1gFnSc4	Děpoltice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
letech	let	k1gInPc6	let
1215	[number]	k4	1215
<g/>
–	–	k?	–
<g/>
1217	[number]	k4	1217
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
mezi	mezi	k7c7	mezi
Přemyslem	Přemysl	k1gMnSc7	Přemysl
a	a	k8xC	a
Děpoltici	Děpoltik	k1gMnPc1	Děpoltik
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
následnému	následný	k2eAgNnSc3d1	následné
vyhnání	vyhnání	k1gNnSc3	vyhnání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
již	již	k6eAd1	již
nic	nic	k3yNnSc1	nic
nebránilo	bránit	k5eNaImAgNnS	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
obecném	obecný	k2eAgInSc6d1	obecný
shromáždění	shromáždění	k1gNnSc2	shromáždění
Čechů	Čech	k1gMnPc2	Čech
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1216	[number]	k4	1216
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
když	když	k8xS	když
Přemysl	Přemysl	k1gMnSc1	Přemysl
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
volbu	volba	k1gFnSc4	volba
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
jak	jak	k8xS	jak
římskoněmeckého	římskoněmecký	k2eAgMnSc4d1	římskoněmecký
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
mohučského	mohučský	k2eAgMnSc4d1	mohučský
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jindřicha	Jindřich	k1gMnSc2	Jindřich
a	a	k8xC	a
také	také	k9	také
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
s	s	k7c7	s
Václavovou	Václavův	k2eAgFnSc7d1	Václavova
volbou	volba	k1gFnSc7	volba
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
žádost	žádost	k1gFnSc4	žádost
mu	on	k3xPp3gMnSc3	on
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1216	[number]	k4	1216
udělil	udělit	k5eAaPmAgInS	udělit
Čechy	Čech	k1gMnPc4	Čech
v	v	k7c6	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1222	[number]	k4	1222
zemřel	zemřít	k5eAaPmAgMnS	zemřít
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
syn	syn	k1gMnSc1	syn
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
ztratil	ztratit	k5eAaPmAgMnS	ztratit
nejen	nejen	k6eAd1	nejen
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nejpevnějšího	pevný	k2eAgMnSc4d3	nejpevnější
spojence	spojenec	k1gMnSc4	spojenec
a	a	k8xC	a
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
dostaly	dostat	k5eAaPmAgInP	dostat
zásah	zásah	k1gInSc4	zásah
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
povznesení	povznesení	k1gNnSc4	povznesení
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
upevnění	upevnění	k1gNnSc6	upevnění
pout	pouto	k1gNnPc2	pouto
k	k	k7c3	k
císařskému	císařský	k2eAgInSc3d1	císařský
rodu	rod	k1gInSc3	rod
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Přemysl	Přemysl	k1gMnSc1	Přemysl
příslibu	příslib	k1gInSc2	příslib
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
ožení	oženit	k5eAaPmIp3nS	oženit
s	s	k7c7	s
Přemyslovou	Přemyslův	k2eAgFnSc7d1	Přemyslova
dcerou	dcera	k1gFnSc7	dcera
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
roli	role	k1gFnSc4	role
římskoněmecké	římskoněmecký	k2eAgFnSc2d1	římskoněmecká
královny	královna	k1gFnSc2	královna
se	se	k3xPyFc4	se
Anežka	Anežka	k1gFnSc1	Anežka
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
rakouského	rakouský	k2eAgMnSc2d1	rakouský
vévody	vévoda	k1gMnSc2	vévoda
Leopolda	Leopold	k1gMnSc2	Leopold
VI	VI	kA	VI
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
astronomické	astronomický	k2eAgNnSc4d1	astronomické
Anežčino	Anežčin	k2eAgNnSc4d1	Anežčino
věno	věno	k1gNnSc4	věno
Jindřich	Jindřich	k1gMnSc1	Jindřich
své	svůj	k3xOyFgNnSc4	svůj
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
vévody	vévoda	k1gMnSc2	vévoda
Leopolda	Leopold	k1gMnSc2	Leopold
Markétou	Markéta	k1gFnSc7	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
odplata	odplata	k1gFnSc1	odplata
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nedala	dát	k5eNaPmAgNnP	dát
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
obrátila	obrátit	k5eAaPmAgFnS	obrátit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Leopoldovi	Leopold	k1gMnSc3	Leopold
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
oprávněně	oprávněně	k6eAd1	oprávněně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
strůjce	strůjce	k1gMnSc4	strůjce
intriky	intrika	k1gFnSc2	intrika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
osudová	osudový	k2eAgFnSc1d1	osudová
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
Anežčin	Anežčin	k2eAgInSc4d1	Anežčin
životaběh	životaběh	k1gInSc4	životaběh
<g/>
.	.	kIx.	.
</s>
<s>
Vpád	vpád	k1gInSc1	vpád
českých	český	k2eAgNnPc2d1	české
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1226	[number]	k4	1226
se	se	k3xPyFc4	se
však	však	k9	však
Rakušanům	Rakušan	k1gMnPc3	Rakušan
podařilo	podařit	k5eAaPmAgNnS	podařit
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
úspěšnější	úspěšný	k2eAgInSc1d2	úspěšnější
útok	útok	k1gInSc1	útok
následoval	následovat	k5eAaImAgInS	následovat
po	po	k7c6	po
Leopoldově	Leopoldův	k2eAgFnSc6d1	Leopoldova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1230	[number]	k4	1230
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1227	[number]	k4	1227
zemřel	zemřít	k5eAaPmAgMnS	zemřít
teprve	teprve	k6eAd1	teprve
dvacetiletý	dvacetiletý	k2eAgMnSc1d1	dvacetiletý
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Vladislav	Vladislav	k1gMnSc1	Vladislav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1228	[number]	k4	1228
nechal	nechat	k5eAaPmAgMnS	nechat
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
svého	svůj	k3xOyFgMnSc2	svůj
druhorozeného	druhorozený	k2eAgMnSc2d1	druhorozený
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Konstancií	Konstancie	k1gFnSc7	Konstancie
Uherskou	uherský	k2eAgFnSc7d1	uherská
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1224	[number]	k4	1224
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
obdařen	obdařit	k5eAaPmNgMnS	obdařit
titulem	titul	k1gInSc7	titul
knížete	kníže	k1gNnSc4wR	kníže
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
a	a	k8xC	a
budyšínského	budyšínský	k2eAgNnSc2d1	budyšínský
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
až	až	k9	až
do	do	k7c2	do
posledních	poslední	k2eAgFnPc2d1	poslední
chvil	chvíle	k1gFnPc2	chvíle
uchoval	uchovat	k5eAaPmAgInS	uchovat
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1230	[number]	k4	1230
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
opukové	opukový	k2eAgFnSc6d1	opuková
tumbě	tumba	k1gFnSc6	tumba
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Petra	Petr	k1gMnSc2	Petr
Parléře	Parléř	k1gMnSc2	Parléř
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Ostatků	ostatek	k1gInPc2	ostatek
ve	v	k7c6	v
svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Dožil	dožít	k5eAaPmAgMnS	dožít
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
věku	věk	k1gInSc2	věk
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
antropologického	antropologický	k2eAgInSc2d1	antropologický
průzkumu	průzkum	k1gInSc2	průzkum
Emanuela	Emanuel	k1gMnSc2	Emanuel
Vlčka	Vlček	k1gMnSc2	Vlček
byl	být	k5eAaImAgMnS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
166	[number]	k4	166
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
cm	cm	kA	cm
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
měl	mít	k5eAaImAgInS	mít
až	až	k9	až
žensky	žensky	k6eAd1	žensky
gracilní	gracilní	k2eAgInSc4d1	gracilní
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
respektován	respektován	k2eAgMnSc1d1	respektován
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
silného	silný	k2eAgNnSc2d1	silné
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
oceňována	oceňovat	k5eAaImNgFnS	oceňovat
jeho	jeho	k3xOp3gFnSc1	jeho
vojenská	vojenský	k2eAgFnSc1d1	vojenská
zdatnost	zdatnost	k1gFnSc1	zdatnost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
rozvaha	rozvaha	k1gFnSc1	rozvaha
<g/>
,	,	kIx,	,
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stabilitu	stabilita	k1gFnSc4	stabilita
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
i	i	k8xC	i
loajalita	loajalita	k1gFnSc1	loajalita
předáků	předák	k1gMnPc2	předák
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
také	také	k9	také
osobnost	osobnost	k1gFnSc4	osobnost
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgMnSc6	který
měl	mít	k5eAaImAgMnS	mít
Přemysl	Přemysl	k1gMnSc1	Přemysl
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
nejen	nejen	k6eAd1	nejen
spolehlivého	spolehlivý	k2eAgMnSc2d1	spolehlivý
spojence	spojenec	k1gMnSc2	spojenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rovnocenného	rovnocenný	k2eAgMnSc4d1	rovnocenný
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
rysech	rys	k1gInPc6	rys
platí	platit	k5eAaImIp3nS	platit
hodnocení	hodnocení	k1gNnSc1	hodnocení
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vlády	vláda	k1gFnSc2	vláda
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
českého	český	k2eAgNnSc2d1	české
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
románský	románský	k2eAgInSc1d1	románský
sloh	sloh	k1gInSc1	sloh
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pozdní	pozdní	k2eAgFnSc2d1	pozdní
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
panovníka	panovník	k1gMnSc2	panovník
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
mnohé	mnohý	k2eAgFnPc1d1	mnohá
nákladné	nákladný	k2eAgFnPc1d1	nákladná
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
byl	být	k5eAaImAgInS	být
stavebně	stavebně	k6eAd1	stavebně
upraven	upravit	k5eAaPmNgInS	upravit
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tympanonu	tympanon	k1gInSc6	tympanon
na	na	k7c6	na
reliéfu	reliéf	k1gInSc6	reliéf
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
i	i	k8xC	i
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
abatyší	abatyše	k1gFnSc7	abatyše
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
heraldika	heraldika	k1gFnSc1	heraldika
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
má	mít	k5eAaImIp3nS	mít
počátek	počátek	k1gInSc4	počátek
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
pečeť	pečeť	k1gFnSc1	pečeť
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
a	a	k8xC	a
náleží	náležet	k5eAaImIp3nS	náležet
Hroznatovi	Hroznat	k1gMnSc3	Hroznat
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc3	zakladatel
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Teplé	Teplá	k1gFnSc6	Teplá
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1213	[number]	k4	1213
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
durynského	durynský	k2eAgMnSc2d1	durynský
lantkraběte	lantkrabě	k1gMnSc2	lantkrabě
Heřmana	Heřman	k1gMnSc2	Heřman
bohatě	bohatě	k6eAd1	bohatě
zdobený	zdobený	k2eAgInSc1d1	zdobený
žaltář	žaltář	k1gInSc1	žaltář
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
iluminacemi	iluminace	k1gFnPc7	iluminace
sousedních	sousední	k2eAgMnPc2d1	sousední
panovníků	panovník	k1gMnPc2	panovník
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Konstancií	Konstancie	k1gFnPc2	Konstancie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
vlna	vlna	k1gFnSc1	vlna
rytířské	rytířský	k2eAgFnSc2d1	rytířská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rychle	rychle	k6eAd1	rychle
šířila	šířit	k5eAaImAgFnS	šířit
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
Přemysl	Přemysl	k1gMnSc1	Přemysl
neobjevil	objevit	k5eNaPmAgMnS	objevit
mezi	mezi	k7c7	mezi
hrdiny	hrdina	k1gMnPc7	hrdina
v	v	k7c6	v
trubadúrských	trubadúrský	k2eAgFnPc6d1	trubadúrská
písních	píseň	k1gFnPc6	píseň
potulných	potulný	k2eAgMnPc2d1	potulný
minesengrů	minesengr	k1gMnPc2	minesengr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
do	do	k7c2	do
závěru	závěr	k1gInSc2	závěr
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vlády	vláda	k1gFnSc2	vláda
patří	patřit	k5eAaImIp3nS	patřit
rytířské	rytířský	k2eAgNnSc4d1	rytířské
klání	klání	k1gNnSc4	klání
svedené	svedený	k2eAgFnSc2d1	svedená
(	(	kIx(	(
<g/>
a	a	k8xC	a
popsané	popsaný	k2eAgFnPc4d1	popsaná
<g/>
)	)	kIx)	)
štýrským	štýrský	k2eAgMnSc7d1	štýrský
Oldřichem	Oldřich	k1gMnSc7	Oldřich
z	z	k7c2	z
Lichtenštejna	Lichtenštejna	k1gFnSc1	Lichtenštejna
<g/>
.	.	kIx.	.
<g/>
Život	život	k1gInSc1	život
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
se	se	k3xPyFc4	se
odrazil	odrazit	k5eAaPmAgInS	odrazit
především	především	k9	především
v	v	k7c6	v
kronikářských	kronikářský	k2eAgInPc6d1	kronikářský
dílech	díl	k1gInPc6	díl
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
opat	opat	k1gMnSc1	opat
milevského	milevský	k2eAgInSc2d1	milevský
kláštera	klášter	k1gInSc2	klášter
Jarloch	Jarloch	k1gMnSc1	Jarloch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sepisoval	sepisovat	k5eAaImAgMnS	sepisovat
svou	svůj	k3xOyFgFnSc4	svůj
kroniku	kronika	k1gFnSc4	kronika
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vyprávění	vyprávění	k1gNnSc1	vyprávění
končí	končit	k5eAaImIp3nS	končit
už	už	k6eAd1	už
rokem	rok	k1gInSc7	rok
1198	[number]	k4	1198
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
i	i	k9	i
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
Přemysl	Přemysl	k1gMnSc1	Přemysl
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
často	často	k6eAd1	často
zůstával	zůstávat	k5eAaImAgInS	zůstávat
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
svých	svůj	k3xOyFgMnPc2	svůj
potomků	potomek	k1gMnPc2	potomek
a	a	k8xC	a
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
svých	svůj	k3xOyFgFnPc2	svůj
dcer	dcera	k1gFnPc2	dcera
Anežky	Anežka	k1gFnSc2	Anežka
a	a	k8xC	a
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
vnuka	vnuk	k1gMnSc2	vnuk
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
stal	stát	k5eAaPmAgMnS	stát
hrdinou	hrdina	k1gMnSc7	hrdina
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Ludmily	Ludmila	k1gFnSc2	Ludmila
Vaňkové	Vaňkové	k2eAgInSc4d1	Vaňkové
Příběh	příběh	k1gInSc4	příběh
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
Kdo	kdo	k3yRnSc1	kdo
na	na	k7c4	na
kamenný	kamenný	k2eAgInSc4d1	kamenný
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
Cestou	cesta	k1gFnSc7	cesta
krále	král	k1gMnSc2	král
a	a	k8xC	a
Dítě	Dítě	k1gMnSc2	Dítě
z	z	k7c2	z
Apulie	Apulie	k1gFnSc2	Apulie
a	a	k8xC	a
také	také	k6eAd1	také
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Vondrušky	Vondruška	k1gMnSc2	Vondruška
Velký	velký	k2eAgMnSc1d1	velký
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
zpodobněn	zpodobnit	k5eAaPmNgInS	zpodobnit
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Kulhánkem	Kulhánek	k1gMnSc7	Kulhánek
na	na	k7c6	na
lícové	lícový	k2eAgFnSc6d1	lícová
straně	strana	k1gFnSc6	strana
nové	nový	k2eAgFnSc2d1	nová
papírové	papírový	k2eAgFnSc2d1	papírová
dvacetikoruny	dvacetikoruna	k1gFnSc2	dvacetikoruna
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
bankovka	bankovka	k1gFnSc1	bankovka
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
s	s	k7c7	s
Přemyslem	Přemysl	k1gMnSc7	Přemysl
stále	stále	k6eAd1	stále
setkat	setkat	k5eAaPmF	setkat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
podobizna	podobizna	k1gFnSc1	podobizna
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
dvanáctikorunové	dvanáctikorunový	k2eAgFnSc6d1	dvanáctikorunový
poštovní	poštovní	k2eAgFnSc6d1	poštovní
známce	známka	k1gFnSc6	známka
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Dědiční	dědičný	k2eAgMnPc1d1	dědičný
králové	král	k1gMnPc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
vypracována	vypracovat	k5eAaPmNgNnP	vypracovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
bankovka	bankovka	k1gFnSc1	bankovka
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přemysl	Přemysl	k1gMnSc1	Přemysl
nebo	nebo	k8xC	nebo
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
?	?	kIx.	?
</s>
<s>
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
znám	znát	k5eAaImIp1nS	znát
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
zdvojeným	zdvojený	k2eAgNnSc7d1	zdvojené
jménem	jméno	k1gNnSc7	jméno
jako	jako	k9	jako
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakara	k1gFnPc2	Otakara
I.	I.	kA	I.
Nebyl	být	k5eNaImAgInS	být
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
ani	ani	k8xC	ani
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
z	z	k7c2	z
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
používal	používat	k5eAaImAgMnS	používat
zdvojené	zdvojený	k2eAgNnSc4d1	zdvojené
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
nazýván	nazývat	k5eAaImNgInS	nazývat
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
,	,	kIx,	,
cizí	cizí	k2eAgFnSc7d1	cizí
prostředí	prostředí	k1gNnSc6	prostředí
ho	on	k3xPp3gMnSc2	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
znalo	znát	k5eAaImAgNnS	znát
takřka	takřka	k6eAd1	takřka
výlučně	výlučně	k6eAd1	výlučně
jako	jako	k8xC	jako
Otakara	Otakara	k1gFnSc1	Otakara
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
pečetích	pečeť	k1gFnPc6	pečeť
se	se	k3xPyFc4	se
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
první	první	k4xOgFnSc2	první
pečeti	pečeť	k1gFnSc2	pečeť
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
letech	let	k1gInPc6	let
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
používal	používat	k5eAaImAgInS	používat
jméno	jméno	k1gNnSc4	jméno
Otakar	Otakara	k1gFnPc2	Otakara
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Otakar	Otakar	k1gMnSc1	Otakar
je	být	k5eAaImIp3nS	být
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
germánský	germánský	k2eAgInSc4d1	germánský
Odowakar	Odowakar	k1gInSc4	Odowakar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
majetek	majetek	k1gInSc4	majetek
střežící	střežící	k2eAgInSc4d1	střežící
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Otakar	Otakara	k1gFnPc2	Otakara
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
souznačně	souznačně	k6eAd1	souznačně
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojeného	zdvojený	k2eAgNnSc2d1	zdvojené
jména	jméno	k1gNnSc2	jméno
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
českou	český	k2eAgFnSc7d1	Česká
královskou	královský	k2eAgFnSc7d1	královská
kanceláří	kancelář	k1gFnSc7	kancelář
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Otakarus	Otakarus	k1gMnSc1	Otakarus
<g/>
,	,	kIx,	,
dei	dei	k?	dei
gratia	gratia	k1gFnSc1	gratia
<g/>
,	,	kIx,	,
qui	qui	k?	qui
et	et	k?	et
Premisel	Premisel	k1gInSc1	Premisel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Josef	Josef	k1gMnSc1	Josef
Kalousek	Kalousek	k1gMnSc1	Kalousek
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
správnější	správní	k2eAgNnSc1d2	správnější
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
jen	jen	k9	jen
jména	jméno	k1gNnPc1	jméno
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
historiografie	historiografie	k1gFnSc1	historiografie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Vaníček	Vaníček	k1gMnSc1	Vaníček
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
preferuje	preferovat	k5eAaImIp3nS	preferovat
obvykle	obvykle	k6eAd1	obvykle
používání	používání	k1gNnSc4	používání
podoby	podoba	k1gFnSc2	podoba
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
podobou	podoba	k1gFnSc7	podoba
Přemysl	Přemysl	k1gMnSc1	Přemysl
I.	I.	kA	I.
Otakar	Otakar	k1gMnSc1	Otakar
<g/>
.	.	kIx.	.
</s>
<s>
Dalimilova	Dalimilův	k2eAgFnSc1d1	Dalimilova
kronika	kronika	k1gFnSc1	kronika
jméno	jméno	k1gNnSc4	jméno
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Brunšvickým	brunšvický	k2eAgMnPc3d1	brunšvický
<g/>
:	:	kIx,	:
Na	na	k7c6	na
biřmovániu	biřmovánium	k1gNnSc6	biřmovánium
ciesař	ciesař	k1gMnSc1	ciesař
kněziu	knězius	k1gMnSc6	knězius
rúšku	rúšek	k1gMnSc6	rúšek
vzváza	vzváza	k1gFnSc1	vzváza
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ottakar	Ottakar	k1gMnSc1	Ottakar
<g/>
,	,	kIx,	,
točiúš	točiúš	k1gMnSc1	točiúš
Ottě	Ottě	k1gMnSc2	Ottě
mił	mił	k?	mił
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
mu	on	k3xPp3gNnSc3	on
řiekati	řiekat	k5eAaPmF	řiekat
káza	káza	k6eAd1	káza
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
měst	město	k1gNnPc2	město
za	za	k7c4	za
vlády	vláda	k1gFnPc4	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
prvních	první	k4xOgNnPc2	první
měst	město	k1gNnPc2	město
navazoval	navazovat	k5eAaImAgMnS	navazovat
na	na	k7c4	na
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
sídelní	sídelní	k2eAgInPc4d1	sídelní
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
institucionalizovaných	institucionalizovaný	k2eAgNnPc2d1	institucionalizované
měst	město	k1gNnPc2	město
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgNnP	existovat
důležitá	důležitý	k2eAgNnPc1d1	důležité
centrální	centrální	k2eAgNnPc1d1	centrální
místa	místo	k1gNnPc1	místo
městům	město	k1gNnPc3	město
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
povaha	povaha	k1gFnSc1	povaha
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
rozvinutým	rozvinutý	k2eAgNnPc3d1	rozvinuté
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
docházelo	docházet	k5eAaImAgNnS	docházet
již	již	k9	již
za	za	k7c2	za
Přemyslových	Přemyslův	k2eAgMnPc2d1	Přemyslův
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ještě	ještě	k6eAd1	ještě
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
středověkých	středověký	k2eAgNnPc6d1	středověké
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
založení	založení	k1gNnSc6	založení
<g/>
)	)	kIx)	)
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc4	proces
vzniku	vznik	k1gInSc2	vznik
středověkým	středověký	k2eAgInSc7d1	středověký
měst	město	k1gNnPc2	město
tak	tak	k9	tak
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
za	za	k7c2	za
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
pocházejí	pocházet	k5eAaImIp3nP	pocházet
také	také	k9	také
nejstarší	starý	k2eAgNnPc1d3	nejstarší
dochovaná	dochovaný	k2eAgNnPc1d1	dochované
městská	městský	k2eAgNnPc1d1	Městské
privilegia	privilegium	k1gNnPc1	privilegium
pro	pro	k7c4	pro
Uničov	Uničov	k1gInSc4	Uničov
(	(	kIx(	(
<g/>
1223	[number]	k4	1223
<g/>
)	)	kIx)	)
a	a	k8xC	a
Opavu	Opava	k1gFnSc4	Opava
(	(	kIx(	(
<g/>
1224	[number]	k4	1224
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
ale	ale	k9	ale
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc1	jeden
nemá	mít	k5eNaImIp3nS	mít
zakládací	zakládací	k2eAgInSc4d1	zakládací
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
již	již	k6eAd1	již
udělená	udělený	k2eAgFnSc1d1	udělená
městská	městský	k2eAgFnSc1d1	městská
práva	práv	k2eAgFnSc1d1	práva
<g/>
.	.	kIx.	.
<g/>
Prvenství	prvenství	k1gNnPc2	prvenství
(	(	kIx(	(
<g/>
nebudeme	být	k5eNaImBp1nP	být
<g/>
-li	i	k?	-li
počítat	počítat	k5eAaImF	počítat
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nepatřil	patřit	k5eNaImAgInS	patřit
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
měst	město	k1gNnPc2	město
patří	patřit	k5eAaImIp3nS	patřit
severu	sever	k1gInSc3	sever
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vzniká	vznikat	k5eAaImIp3nS	vznikat
asi	asi	k9	asi
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1213	[number]	k4	1213
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Uničov	Uničov	k1gInSc4	Uničov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
převzal	převzít	k5eAaPmAgInS	převzít
magdeburská	magdeburský	k2eAgNnPc4d1	magdeburské
práva	právo	k1gNnPc4	právo
od	od	k7c2	od
Bruntálu	Bruntál	k1gInSc6	Bruntál
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
asi	asi	k9	asi
v	v	k7c6	v
letech	let	k1gInPc6	let
1213	[number]	k4	1213
<g/>
–	–	k?	–
<g/>
1220	[number]	k4	1220
Opava	Opava	k1gFnSc1	Opava
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
slezských	slezský	k2eAgInPc2d1	slezský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
ležících	ležící	k2eAgFnPc2d1	ležící
<g/>
)	)	kIx)	)
Hlubčic	hlubčice	k1gFnPc2	hlubčice
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
prvotním	prvotní	k2eAgInSc6d1	prvotní
zakladatelském	zakladatelský	k2eAgInSc6d1	zakladatelský
rozmachu	rozmach	k1gInSc6	rozmach
následuje	následovat	k5eAaImIp3nS	následovat
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Znojmo	Znojmo	k1gNnSc1	Znojmo
(	(	kIx(	(
<g/>
1226	[number]	k4	1226
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jemnice	Jemnice	k1gFnSc1	Jemnice
(	(	kIx(	(
<g/>
1227	[number]	k4	1227
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
pak	pak	k6eAd1	pak
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
1225	[number]	k4	1225
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Sporné	sporný	k2eAgInPc1d1	sporný
jsou	být	k5eAaImIp3nP	být
počátky	počátek	k1gInPc1	počátek
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
kladeny	klást	k5eAaImNgFnP	klást
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
závěru	závěra	k1gFnSc4	závěra
vlády	vláda	k1gFnSc2	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
Přemyslovy	Přemyslův	k2eAgFnSc2d1	Přemyslova
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
možný	možný	k2eAgInSc1d1	možný
i	i	k8xC	i
vznik	vznik	k1gInSc1	vznik
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
a	a	k8xC	a
Žatce	Žatec	k1gInSc2	Žatec
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
nově	nově	k6eAd1	nově
vznikajících	vznikající	k2eAgNnPc6d1	vznikající
městech	město	k1gNnPc6	město
žil	žít	k5eAaImAgMnS	žít
jen	jen	k9	jen
zlomek	zlomek	k1gInSc4	zlomek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
vysoce	vysoce	k6eAd1	vysoce
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
demografickou	demografický	k2eAgFnSc4d1	demografická
váhu	váha	k1gFnSc4	váha
a	a	k8xC	a
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stávala	stávat	k5eAaImAgFnS	stávat
středisky	středisko	k1gNnPc7	středisko
rozvoje	rozvoj	k1gInSc2	rozvoj
středověké	středověký	k2eAgFnSc2d1	středověká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
1178	[number]	k4	1178
Adléta	Adléta	k1gFnSc1	Adléta
Míšeňská	míšeňský	k2eAgFnSc1d1	Míšeňská
(	(	kIx(	(
<g/>
1160	[number]	k4	1160
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1211	[number]	k4	1211
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
(	(	kIx(	(
<g/>
před	před	k7c7	před
1181	[number]	k4	1181
–	–	k?	–
po	po	k7c4	po
1225	[number]	k4	1225
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
/	/	kIx~	/
<g/>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Dánská	dánský	k2eAgFnSc1d1	dánská
(	(	kIx(	(
<g/>
1186	[number]	k4	1186
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1213	[number]	k4	1213
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1205	[number]	k4	1205
Valdemar	Valdemara	k1gFnPc2	Valdemara
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vítězný	vítězný	k2eAgMnSc1d1	vítězný
<g/>
,	,	kIx,	,
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
Božislava	Božislava	k1gFnSc1	Božislava
∞	∞	k?	∞
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
z	z	k7c2	z
Ortenburgu	Ortenburg	k1gInSc2	Ortenburg
</s>
</p>
<p>
<s>
Hedvika	Hedvika	k1gFnSc1	Hedvika
–	–	k?	–
jeptiška	jeptiška	k1gFnSc1	jeptiška
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
(	(	kIx(	(
<g/>
1199	[number]	k4	1199
<g/>
)	)	kIx)	)
Konstancie	Konstancie	k1gFnSc1	Konstancie
Uherská	uherský	k2eAgFnSc1d1	uherská
(	(	kIx(	(
<g/>
1181	[number]	k4	1181
<g/>
–	–	k?	–
<g/>
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
(	(	kIx(	(
<g/>
1200	[number]	k4	1200
<g/>
–	–	k?	–
<g/>
1201	[number]	k4	1201
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Judita	Judita	k1gFnSc1	Judita
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
(	(	kIx(	(
<g/>
†	†	k?	†
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1213	[number]	k4	1213
Bernard	Bernard	k1gMnSc1	Bernard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sponheimský	Sponheimský	k2eAgMnSc1d1	Sponheimský
<g/>
,	,	kIx,	,
korutanský	korutanský	k2eAgMnSc1d1	korutanský
vévoda	vévoda	k1gMnSc1	vévoda
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Lehnická	Lehnický	k2eAgFnSc1d1	Lehnická
(	(	kIx(	(
<g/>
1204	[number]	k4	1204
<g/>
–	–	k?	–
<g/>
1265	[number]	k4	1265
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pobožný	pobožný	k2eAgMnSc1d1	pobožný
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1205	[number]	k4	1205
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
∞	∞	k?	∞
1224	[number]	k4	1224
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
Štaufská	Štaufský	k2eAgNnPc4d1	Štaufský
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
(	(	kIx(	(
<g/>
1207	[number]	k4	1207
<g/>
–	–	k?	–
<g/>
1227	[number]	k4	1227
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
(	(	kIx(	(
<g/>
1209	[number]	k4	1209
<g/>
–	–	k?	–
<g/>
1239	[number]	k4	1239
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
∞	∞	k?	∞
Markéta	Markéta	k1gFnSc1	Markéta
z	z	k7c2	z
Meranu	Meran	k1gInSc2	Meran
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
</s>
</p>
<p>
<s>
Vilemína	Vilemína	k1gFnSc1	Vilemína
Česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
1210	[number]	k4	1210
<g/>
–	–	k?	–
<g/>
1281	[number]	k4	1281
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Anežka	Anežka	k1gFnSc1	Anežka
Česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
1211	[number]	k4	1211
<g/>
–	–	k?	–
<g/>
1282	[number]	k4	1282
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
za	za	k7c2	za
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
446	[number]	k4	446
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
do	do	k7c2	do
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
1214	[number]	k4	1214
s.	s.	k?	s.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
královské	královský	k2eAgFnPc1d1	královská
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
a	a	k8xC	a
Václava	Václava	k1gFnSc1	Václava
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
1085	[number]	k4	1085
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
II	II	kA	II
<g/>
.	.	kIx.	.
1197	[number]	k4	1197
<g/>
–	–	k?	–
<g/>
1250	[number]	k4	1250
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
582	[number]	k4	582
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
česká	český	k2eAgNnPc4d1	české
království	království	k1gNnPc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
438	[number]	k4	438
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
278	[number]	k4	278
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Podivuhodný	podivuhodný	k2eAgInSc1d1	podivuhodný
příběh	příběh	k1gInSc1	příběh
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
316	[number]	k4	316
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
682	[number]	k4	682
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
1034	[number]	k4	1034
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
712	[number]	k4	712
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
905	[number]	k4	905
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Čech	Čechy	k1gFnPc2	Čechy
královských	královský	k2eAgFnPc2d1	královská
1198	[number]	k4	1198
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
964	[number]	k4	964
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
Panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
feudalismu	feudalismus	k1gInSc2	feudalismus
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
361	[number]	k4	361
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
281	[number]	k4	281
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakara	k1gFnPc2	Otakara
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rodokmen	rodokmen	k1gInSc1	rodokmen
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
po	po	k7c4	po
Přemysla	Přemysl	k1gMnSc4	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
a	a	k8xC	a
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Exilová	exilový	k2eAgNnPc4d1	exilové
léta	léto	k1gNnPc4	léto
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přemyslův	Přemyslův	k2eAgInSc1d1	Přemyslův
denár	denár	k1gInSc1	denár
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1193	[number]	k4	1193
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
pečeť	pečeť	k1gFnSc1	pečeť
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
