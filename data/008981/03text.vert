<p>
<s>
Norština	norština	k1gFnSc1	norština
(	(	kIx(	(
<g/>
norsk	norsk	k1gInSc1	norsk
[	[	kIx(	[
<g/>
nɔ	nɔ	k?	nɔ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
Norové	Norové	k2eAgNnSc1d1	Norové
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
(	(	kIx(	(
<g/>
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
norštiny	norština	k1gFnSc2	norština
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
stará	starat	k5eAaImIp3nS	starat
severština	severština	k1gFnSc1	severština
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dánské	dánský	k2eAgFnSc2d1	dánská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgMnSc1d1	trvající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
dánština	dánština	k1gFnSc1	dánština
především	především	k6eAd1	především
jazyk	jazyk	k1gInSc4	jazyk
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
měst	město	k1gNnPc2	město
a	a	k8xC	a
vzdělaných	vzdělaný	k2eAgFnPc2d1	vzdělaná
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Venkovské	venkovský	k2eAgNnSc1d1	venkovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
však	však	k9	však
hovořilo	hovořit	k5eAaImAgNnS	hovořit
nářečími	nářečí	k1gNnPc7	nářečí
zachovávajícími	zachovávající	k2eAgFnPc7d1	zachovávající
staronorské	staronorský	k2eAgFnPc1d1	staronorský
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
podmínil	podmínit	k5eAaPmAgInS	podmínit
vznik	vznik	k1gInSc4	vznik
specifické	specifický	k2eAgFnSc2d1	specifická
jazykové	jazykový	k2eAgFnSc2d1	jazyková
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
rovnoprávné	rovnoprávný	k2eAgFnPc1d1	rovnoprávná
spisovné	spisovný	k2eAgFnPc1d1	spisovná
formy	forma	k1gFnPc1	forma
norského	norský	k2eAgInSc2d1	norský
jazyka	jazyk	k1gInSc2	jazyk
–	–	k?	–
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
bokmå	bokmå	k?	bokmå
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
a	a	k8xC	a
nynorsk	nynorsk	k1gInSc1	nynorsk
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
norštiny	norština	k1gFnSc2	norština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
latinka	latinka	k1gFnSc1	latinka
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
æ	æ	k?	æ
<g/>
,	,	kIx,	,
ø	ø	k?	ø
a	a	k8xC	a
å	å	k?	å
Mluvnice	mluvnice	k1gFnSc1	mluvnice
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
staré	starý	k2eAgFnSc3d1	stará
severštině	severština	k1gFnSc3	severština
značně	značně	k6eAd1	značně
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
norština	norština	k1gFnSc1	norština
má	mít	k5eAaImIp3nS	mít
redukovanou	redukovaný	k2eAgFnSc4d1	redukovaná
flexi	flexe	k1gFnSc4	flexe
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	on	k3xPp3gFnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
převažující	převažující	k2eAgNnSc1d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgMnSc1d1	určitý
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
postpozitivní	postpozitivní	k2eAgMnSc1d1	postpozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výslovnost	výslovnost	k1gFnSc4	výslovnost
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
dvojí	dvojí	k4xRgNnSc4	dvojí
intonací	intonace	k1gFnPc2	intonace
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
v	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
komplementarity	komplementarita	k1gFnSc2	komplementarita
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Norština	norština	k1gFnSc1	norština
je	být	k5eAaImIp3nS	být
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
severogermánské	severogermánský	k2eAgFnSc2d1	severogermánský
větve	větev	k1gFnSc2	větev
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nynorsk	Nynorsk	k1gInSc1	Nynorsk
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
faerštinou	faerština	k1gFnSc7	faerština
a	a	k8xC	a
islandštinou	islandština	k1gFnSc7	islandština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
západoskandinávské	západoskandinávský	k2eAgInPc4d1	západoskandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dánština	dánština	k1gFnSc1	dánština
a	a	k8xC	a
švédština	švédština	k1gFnSc1	švédština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
východoskandinávské	východoskandinávský	k2eAgNnSc4d1	východoskandinávský
<g/>
.	.	kIx.	.
</s>
<s>
Bokmå	Bokmå	k?	Bokmå
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
riksmå	riksmå	k?	riksmå
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
dánštiny	dánština	k1gFnSc2	dánština
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
východní	východní	k2eAgFnSc3d1	východní
větvi	větev	k1gFnSc3	větev
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
fonologických	fonologický	k2eAgInPc2d1	fonologický
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
základ	základ	k1gInSc4	základ
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nářečního	nářeční	k2eAgNnSc2d1	nářeční
štěpení	štěpení	k1gNnSc2	štěpení
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
rozdělení	rozdělení	k1gNnSc1	rozdělení
řadí	řadit	k5eAaImIp3nS	řadit
norštinu	norština	k1gFnSc4	norština
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
mezi	mezi	k7c4	mezi
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
charakter	charakter	k1gInSc1	charakter
analytických	analytický	k2eAgInPc2d1	analytický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
skandinávské	skandinávský	k2eAgInPc1d1	skandinávský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
faerština	faerština	k1gFnSc1	faerština
<g/>
,	,	kIx,	,
islandština	islandština	k1gFnSc1	islandština
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
charakter	charakter	k1gInSc4	charakter
flektivních	flektivní	k2eAgInPc2d1	flektivní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
norština	norština	k1gFnSc1	norština
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
asi	asi	k9	asi
95	[number]	k4	95
%	%	kIx~	%
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
4,7	[number]	k4	4,7
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
norskými	norský	k2eAgFnPc7d1	norská
menšinami	menšina	k1gFnPc7	menšina
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
je	být	k5eAaImIp3nS	být
odhadován	odhadován	k2eAgInSc1d1	odhadován
na	na	k7c4	na
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Norština	norština	k1gFnSc1	norština
je	být	k5eAaImIp3nS	být
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
dánským	dánský	k2eAgMnSc7d1	dánský
i	i	k9	i
švédským	švédský	k2eAgMnSc7d1	švédský
mluvčím	mluvčí	k1gMnSc7	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
obvykle	obvykle	k6eAd1	obvykle
dobře	dobře	k6eAd1	dobře
rozumějí	rozumět	k5eAaImIp3nP	rozumět
dánským	dánský	k2eAgInPc3d1	dánský
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
mluvená	mluvený	k2eAgFnSc1d1	mluvená
dánština	dánština	k1gFnSc1	dánština
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Mluvená	mluvený	k2eAgFnSc1d1	mluvená
švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
srozumitelnější	srozumitelný	k2eAgMnPc1d2	srozumitelnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
norštiny	norština	k1gFnSc2	norština
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
těchto	tento	k3xDgNnPc2	tento
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stará	starý	k2eAgFnSc1d1	stará
norština	norština	k1gFnSc1	norština
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
1350	[number]	k4	1350
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
norština	norština	k1gFnSc1	norština
1350	[number]	k4	1350
<g/>
–	–	k?	–
<g/>
1525	[number]	k4	1525
</s>
</p>
<p>
<s>
moderní	moderní	k2eAgFnSc1d1	moderní
norština	norština	k1gFnSc1	norština
1525	[number]	k4	1525
<g/>
–	–	k?	–
<g/>
současnostNorština	současnostNorština	k1gFnSc1	současnostNorština
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
(	(	kIx(	(
<g/>
norrø	norrø	k?	norrø
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	let	k1gInPc6	let
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
též	též	k9	též
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xS	jako
praseverské	praseverský	k2eAgFnPc1d1	praseverský
(	(	kIx(	(
<g/>
urnordisk	urnordisk	k1gInSc1	urnordisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
začala	začít	k5eAaPmAgFnS	začít
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgNnSc1d1	východní
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
se	se	k3xPyFc4	se
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
odlišila	odlišit	k5eAaPmAgFnS	odlišit
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
vývojem	vývoj	k1gInSc7	vývoj
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
a	a	k8xC	a
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
zapisovala	zapisovat	k5eAaImAgFnS	zapisovat
runovým	runový	k2eAgNnSc7d1	runové
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Latinka	latinka	k1gFnSc1	latinka
se	se	k3xPyFc4	se
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k6eAd1	až
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
trvajícím	trvající	k2eAgInSc7d1	trvající
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
staré	starý	k2eAgFnSc6d1	stará
norštině	norština	k1gFnSc6	norština
(	(	kIx(	(
<g/>
gammalnorsk	gammalnorsk	k1gInSc1	gammalnorsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
současné	současný	k2eAgFnSc2d1	současná
norštiny	norština	k1gFnSc2	norština
flektivním	flektivní	k2eAgInSc7d1	flektivní
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
se	se	k3xPyFc4	se
systémem	systém	k1gInSc7	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
a	a	k8xC	a
rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
číslo	číslo	k1gNnSc4	číslo
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1350	[number]	k4	1350
a	a	k8xC	a
1525	[number]	k4	1525
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
středonorské	středonorský	k2eAgNnSc1d1	středonorský
(	(	kIx(	(
<g/>
mellomnorsk	mellomnorsk	k1gInSc1	mellomnorsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
morfologie	morfologie	k1gFnSc2	morfologie
<g/>
,	,	kIx,	,
norština	norština	k1gFnSc1	norština
nabyla	nabýt	k5eAaPmAgFnS	nabýt
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1397	[number]	k4	1397
přešlo	přejít	k5eAaPmAgNnS	přejít
Norsko	Norsko	k1gNnSc1	Norsko
pod	pod	k7c4	pod
dánskou	dánský	k2eAgFnSc4d1	dánská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
Norska	Norsko	k1gNnSc2	Norsko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postupně	postupně	k6eAd1	postupně
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
norštinu	norština	k1gFnSc4	norština
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
úředního	úřední	k2eAgNnSc2d1	úřední
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
vznikala	vznikat	k5eAaImAgFnS	vznikat
nářečí	nářečí	k1gNnSc2	nářečí
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
převažovala	převažovat	k5eAaImAgFnS	převažovat
dánská	dánský	k2eAgFnSc1d1	dánská
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
a	a	k8xC	a
morfologie	morfologie	k1gFnSc1	morfologie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
norská	norský	k2eAgFnSc1d1	norská
fonetika	fonetika	k1gFnSc1	fonetika
a	a	k8xC	a
syntax	syntax	k1gFnSc1	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
vývoje	vývoj	k1gInSc2	vývoj
staré	starý	k2eAgFnSc2d1	stará
norštiny	norština	k1gFnSc2	norština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
následném	následný	k2eAgNnSc6d1	následné
připojení	připojení	k1gNnSc6	připojení
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
používala	používat	k5eAaImAgFnS	používat
dánština	dánština	k1gFnSc1	dánština
jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
však	však	k9	však
dva	dva	k4xCgInPc4	dva
názorové	názorový	k2eAgInPc4d1	názorový
proudy	proud	k1gInPc4	proud
usilující	usilující	k2eAgInPc4d1	usilující
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
spisovného	spisovný	k2eAgInSc2d1	spisovný
norského	norský	k2eAgInSc2d1	norský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Henrik	Henrik	k1gMnSc1	Henrik
Wergeland	Wergelando	k1gNnPc2	Wergelando
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
kolegové	kolega	k1gMnPc1	kolega
seskupení	seskupení	k1gNnPc2	seskupení
kolem	kolem	k7c2	kolem
časopisu	časopis	k1gInSc2	časopis
Statsborgeren	Statsborgerna	k1gFnPc2	Statsborgerna
začali	začít	k5eAaPmAgMnP	začít
obměňovat	obměňovat	k5eAaImF	obměňovat
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
a	a	k8xC	a
pravopis	pravopis	k1gInSc4	pravopis
dánštiny	dánština	k1gFnSc2	dánština
a	a	k8xC	a
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
je	být	k5eAaImIp3nS	být
norským	norský	k2eAgFnPc3d1	norská
zvyklostem	zvyklost	k1gFnPc3	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dialektu	dialekt	k1gInSc2	dialekt
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
v	v	k7c6	v
Kristiánii	kristiánie	k1gFnSc6	kristiánie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
dánsko-norský	dánskoorský	k2eAgInSc1d1	dánsko-norský
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
riksmå	riksmå	k?	riksmå
(	(	kIx(	(
<g/>
říšský	říšský	k2eAgInSc4d1	říšský
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc4d1	státní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
proud	proud	k1gInSc1	proud
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
staronorským	staronorský	k2eAgInPc3d1	staronorský
kořenům	kořen	k1gInPc3	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Ivar	Ivar	k1gInSc1	Ivar
Aasen	Aasen	k1gInSc1	Aasen
studoval	studovat	k5eAaImAgInS	studovat
norská	norský	k2eAgNnPc4d1	norské
nářečí	nářečí	k1gNnPc4	nářečí
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgMnPc2	svůj
výzkumů	výzkum	k1gInPc2	výzkum
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
základy	základ	k1gInPc4	základ
landsmå	landsmå	k?	landsmå
(	(	kIx(	(
<g/>
zemský	zemský	k2eAgInSc1d1	zemský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
používán	používat	k5eAaImNgInS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
a	a	k8xC	a
jako	jako	k9	jako
druhý	druhý	k4xOgInSc4	druhý
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
uznán	uznat	k5eAaPmNgInS	uznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
nové	nový	k2eAgInPc1d1	nový
názvy	název	k1gInPc1	název
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
jazykových	jazykový	k2eAgFnPc2d1	jazyková
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
landsmå	landsmå	k?	landsmå
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
nynorsk	nynorsk	k1gInSc4	nynorsk
a	a	k8xC	a
upravený	upravený	k2eAgInSc4d1	upravený
riksmå	riksmå	k?	riksmå
na	na	k7c4	na
bokmå	bokmå	k?	bokmå
<g/>
.	.	kIx.	.
</s>
<s>
Riksmå	Riksmå	k?	Riksmå
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používaná	používaný	k2eAgFnSc1d1	používaná
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
forma	forma	k1gFnSc1	forma
nové	nový	k2eAgFnSc2d1	nová
norštiny	norština	k1gFnSc2	norština
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hø	hø	k?	hø
(	(	kIx(	(
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
norština	norština	k1gFnSc1	norština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
germánského	germánský	k2eAgInSc2d1	germánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
cizí	cizí	k2eAgNnPc1d1	cizí
slova	slovo	k1gNnPc1	slovo
do	do	k7c2	do
norštiny	norština	k1gFnSc2	norština
přejímána	přejímat	k5eAaImNgFnS	přejímat
především	především	k6eAd1	především
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dánštiny	dánština	k1gFnSc2	dánština
–	–	k?	–
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přejala	přejmout	k5eAaPmAgFnS	přejmout
norština	norština	k1gFnSc1	norština
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
zejména	zejména	k9	zejména
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
byly	být	k5eAaImAgFnP	být
přejaty	přejmout	k5eAaPmNgFnP	přejmout
v	v	k7c6	v
cizí	cizí	k2eAgFnSc6d1	cizí
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jsou	být	k5eAaImIp3nP	být
doslovnými	doslovný	k2eAgInPc7d1	doslovný
překlady	překlad	k1gInPc7	překlad
(	(	kIx(	(
<g/>
kalky	kalk	k1gInPc7	kalk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgNnPc2d1	mnohé
dánských	dánský	k2eAgNnPc2d1	dánské
slov	slovo	k1gNnPc2	slovo
byly	být	k5eAaImAgFnP	být
původní	původní	k2eAgFnPc1d1	původní
přípony	přípona	k1gFnPc1	přípona
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
norskými	norský	k2eAgInPc7d1	norský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jazykových	jazykový	k2eAgFnPc2d1	jazyková
reforem	reforma	k1gFnPc2	reforma
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
mnohdy	mnohdy	k6eAd1	mnohdy
částečně	částečně	k6eAd1	částečně
změněna	změnit	k5eAaPmNgFnS	změnit
hlásková	hláskový	k2eAgFnSc1d1	hlásková
podoba	podoba	k1gFnSc1	podoba
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Bokmå	Bokmå	k?	Bokmå
je	být	k5eAaImIp3nS	být
přejímání	přejímání	k1gNnSc1	přejímání
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
více	hodně	k6eAd2	hodně
otevřený	otevřený	k2eAgInSc4d1	otevřený
než	než	k8xS	než
"	"	kIx"	"
<g/>
purističtější	puristický	k2eAgFnSc1d2	purističtější
<g/>
"	"	kIx"	"
nová	nový	k2eAgFnSc1d1	nová
norština	norština	k1gFnSc1	norština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
norština	norština	k1gFnSc1	norština
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
má	mít	k5eAaImIp3nS	mít
norština	norština	k1gFnSc1	norština
dvě	dva	k4xCgFnPc4	dva
spisovné	spisovný	k2eAgFnPc4d1	spisovná
varianty	varianta	k1gFnPc4	varianta
–	–	k?	–
bokmå	bokmå	k?	bokmå
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
knižní	knižní	k2eAgFnSc1d1	knižní
řeč	řeč	k1gFnSc1	řeč
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
nynorsk	nynorsk	k1gInSc4	nynorsk
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
norština	norština	k1gFnSc1	norština
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
rovnocenné	rovnocenný	k2eAgNnSc1d1	rovnocenné
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
obce	obec	k1gFnPc1	obec
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nP	volit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
bude	být	k5eAaImBp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
při	při	k7c6	při
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
používalo	používat	k5eAaImAgNnS	používat
160	[number]	k4	160
obcí	obec	k1gFnPc2	obec
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
115	[number]	k4	115
nynorsk	nynorsk	k1gInSc4	nynorsk
a	a	k8xC	a
156	[number]	k4	156
bylo	být	k5eAaImAgNnS	být
neutrálních	neutrální	k2eAgNnPc2d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
žáků	žák	k1gMnPc2	žák
rovněž	rovněž	k9	rovněž
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
varianta	varianta	k1gFnSc1	varianta
bude	být	k5eAaImBp3nS	být
vyučovacím	vyučovací	k2eAgMnSc7d1	vyučovací
jazykem	jazyk	k1gMnSc7	jazyk
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vyučován	vyučovat	k5eAaImNgInS	vyučovat
jako	jako	k8xS	jako
povinný	povinný	k2eAgInSc1d1	povinný
předmět	předmět	k1gInSc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Bokmå	Bokmå	k?	Bokmå
dnes	dnes	k6eAd1	dnes
převažuje	převažovat	k5eAaImIp3nS	převažovat
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
norštiny	norština	k1gFnSc2	norština
nad	nad	k7c7	nad
novou	nový	k2eAgFnSc7d1	nová
norštinou	norština	k1gFnSc7	norština
a	a	k8xC	a
nad	nad	k7c7	nad
riksmå	riksmå	k?	riksmå
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
forma	forma	k1gFnSc1	forma
bokmå	bokmå	k?	bokmå
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
používalo	používat	k5eAaImAgNnS	používat
novou	nový	k2eAgFnSc4d1	nová
norštinu	norština	k1gFnSc4	norština
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
4652	[number]	k4	4652
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
norštině	norština	k1gFnSc6	norština
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
460	[number]	k4	460
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
oba	dva	k4xCgInPc1	dva
spisovné	spisovný	k2eAgInPc1d1	spisovný
jazyky	jazyk	k1gInPc1	jazyk
byly	být	k5eAaImAgInP	být
několikrát	několikrát	k6eAd1	několikrát
reformovány	reformovat	k5eAaBmNgInP	reformovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
sblížení	sblížení	k1gNnSc1	sblížení
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc1	vytvoření
jednotné	jednotný	k2eAgFnSc2d1	jednotná
obecné	obecný	k2eAgFnSc2d1	obecná
norštiny	norština	k1gFnSc2	norština
(	(	kIx(	(
<g/>
samnorsk	samnorsk	k1gInSc1	samnorsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
landsmå	landsmå	k?	landsmå
<g/>
/	/	kIx~	/
<g/>
nové	nový	k2eAgFnSc2d1	nová
norštiny	norština	k1gFnSc2	norština
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
původně	původně	k6eAd1	původně
vycházela	vycházet	k5eAaImAgFnS	vycházet
ze	z	k7c2	z
západonorských	západonorský	k2eAgNnPc2d1	západonorský
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sblížení	sblížení	k1gNnSc4	sblížení
s	s	k7c7	s
nářečími	nářečí	k1gNnPc7	nářečí
východního	východní	k2eAgNnSc2d1	východní
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
riksmå	riksmå	k?	riksmå
<g/>
/	/	kIx~	/
<g/>
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
hovorové	hovorový	k2eAgFnSc3d1	hovorová
norštině	norština	k1gFnSc3	norština
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
přiblížit	přiblížit	k5eAaPmF	přiblížit
nové	nový	k2eAgFnSc3d1	nová
norštině	norština	k1gFnSc3	norština
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
radikální	radikální	k2eAgInPc1d1	radikální
tvary	tvar	k1gInPc1	tvar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
nářečních	nářeční	k2eAgInPc2d1	nářeční
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
však	však	k9	však
všeobecně	všeobecně	k6eAd1	všeobecně
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
uživatelů	uživatel	k1gMnPc2	uživatel
dává	dávat	k5eAaImIp3nS	dávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
přednost	přednost	k1gFnSc4	přednost
tvarům	tvar	k1gInPc3	tvar
konzervativním	konzervativní	k2eAgNnPc3d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
reforma	reforma	k1gFnSc1	reforma
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
"	"	kIx"	"
<g/>
rehabilitovala	rehabilitovat	k5eAaBmAgFnS	rehabilitovat
<g/>
"	"	kIx"	"
starší	starý	k2eAgInPc1d2	starší
tvary	tvar	k1gInPc1	tvar
bokmå	bokmå	k?	bokmå
oproti	oproti	k7c3	oproti
tvarům	tvar	k1gInPc3	tvar
radikálním	radikální	k2eAgInPc3d1	radikální
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Nářečí	nářečí	k1gNnSc2	nářečí
==	==	k?	==
</s>
</p>
<p>
<s>
Norská	norský	k2eAgNnPc1d1	norské
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
východonorská	východonorský	k2eAgNnPc1d1	východonorský
nářečí	nářečí	k1gNnPc1	nářečí
(	(	kIx(	(
<g/>
ø	ø	k?	ø
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
nářečí	nářečí	k1gNnSc2	nářečí
Trø	Trø	k1gFnSc1	Trø
(	(	kIx(	(
<g/>
trø	trø	k?	trø
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
<s>
západonorská	západonorský	k2eAgNnPc1d1	západonorský
nářečí	nářečí	k1gNnPc1	nářečí
(	(	kIx(	(
<g/>
vestnorsk	vestnorsk	k1gInSc1	vestnorsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
severonorská	severonorský	k2eAgFnSc1d1	severonorský
(	(	kIx(	(
<g/>
nordnorsk	nordnorsk	k1gInSc1	nordnorsk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Některá	některý	k3yIgNnPc1	některý
nářečí	nářečí	k1gNnPc1	nářečí
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
tak	tak	k6eAd1	tak
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
dobře	dobře	k6eAd1	dobře
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východonorská	Východonorský	k2eAgNnPc1d1	Východonorský
nářečí	nářečí	k1gNnPc1	nářečí
mají	mít	k5eAaImIp3nP	mít
následující	následující	k2eAgInPc1d1	následující
rysy	rys	k1gInPc1	rys
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
západonorských	západonorský	k2eAgFnPc2d1	západonorský
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
dvouslabičných	dvouslabičný	k2eAgNnPc6d1	dvouslabičné
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
první	první	k4xOgFnSc1	první
slabika	slabika	k1gFnSc1	slabika
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
norštině	norština	k1gFnSc6	norština
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
druhé	druhý	k4xOgFnSc2	druhý
slabiky	slabika	k1gFnSc2	slabika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Trø	Trø	k1gFnSc6	Trø
zcela	zcela	k6eAd1	zcela
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
s	s	k7c7	s
historicky	historicky	k6eAd1	historicky
krátkou	krátký	k2eAgFnSc7d1	krátká
první	první	k4xOgFnSc7	první
slabikou	slabika	k1gFnSc7	slabika
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
redukci	redukce	k1gFnSc3	redukce
<g/>
/	/	kIx~	/
<g/>
zániku	zánik	k1gInSc3	zánik
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
tvarech	tvar	k1gInPc6	tvar
infinitivu	infinitiv	k1gInSc2	infinitiv
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rozštěpený	rozštěpený	k2eAgInSc4d1	rozštěpený
<g/>
"	"	kIx"	"
infinitiv	infinitiv	k1gInSc4	infinitiv
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
Gramatika	gramatika	k1gFnSc1	gramatika
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
kaste	kaste	k5eAaPmIp2nP	kaste
[	[	kIx(	[
<g/>
-ɘ	-ɘ	k?	-ɘ
<g/>
]	]	kIx)	]
<	<	kIx(	<
kasta	kasta	k1gFnSc1	kasta
(	(	kIx(	(
<g/>
házet	házet	k5eAaImF	házet
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
lesa	les	k1gInSc2	les
(	(	kIx(	(
<g/>
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
tlusté	tlustý	k2eAgNnSc1d1	tlusté
l	l	kA	l
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
ɽ	ɽ	k?	ɽ
<g/>
]	]	kIx)	]
–	–	k?	–
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
hláska	hláska	k1gFnSc1	hláska
<g/>
,	,	kIx,	,
poziční	poziční	k2eAgFnSc1d1	poziční
varianta	varianta	k1gFnSc1	varianta
(	(	kIx(	(
<g/>
alofon	alofon	k1gInSc1	alofon
<g/>
)	)	kIx)	)
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
skupiny	skupina	k1gFnSc2	skupina
/	/	kIx~	/
<g/>
rð	rð	k?	rð
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
<g/>
Nářeční	nářeční	k2eAgInSc1d1	nářeční
nebo	nebo	k8xC	nebo
nářečně	nářečně	k6eAd1	nářečně
zabarvená	zabarvený	k2eAgFnSc1d1	zabarvená
mluva	mluva	k1gFnSc1	mluva
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
divadle	divadlo	k1gNnSc6	divadlo
apod.	apod.	kA	apod.
Existují	existovat	k5eAaImIp3nP	existovat
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
psaná	psaný	k2eAgNnPc4d1	psané
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
pravopis	pravopis	k1gInSc1	pravopis
==	==	k?	==
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
abeceda	abeceda	k1gFnSc1	abeceda
má	mít	k5eAaImIp3nS	mít
29	[number]	k4	29
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
norštiny	norština	k1gFnSc2	norština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
latinka	latinka	k1gFnSc1	latinka
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
3	[number]	k4	3
dodatečná	dodatečný	k2eAgNnPc4d1	dodatečné
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
Å	Å	k?	Å
(	(	kIx(	(
<g/>
å	å	k?	å
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
norské	norský	k2eAgFnSc2d1	norská
abecedy	abeceda	k1gFnSc2	abeceda
zavedeno	zavést	k5eAaPmNgNnS	zavést
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
používané	používaný	k2eAgFnSc2d1	používaná
Aa	Aa	k1gFnSc2	Aa
(	(	kIx(	(
<g/>
aa	aa	k?	aa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
spojení	spojení	k1gNnSc1	spojení
písmen	písmeno	k1gNnPc2	písmeno
aa	aa	k?	aa
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
a	a	k8xC	a
ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
dokumentech	dokument	k1gInPc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
aa	aa	k?	aa
řadí	řadit	k5eAaImIp3nS	řadit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
å	å	k?	å
až	až	k9	až
na	na	k7c4	na
konec	konec	k1gInSc4	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
abeceda	abeceda	k1gFnSc1	abeceda
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
švédské	švédský	k2eAgFnSc2d1	švédská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
dánskou	dánský	k2eAgFnSc7d1	dánská
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
abeceda	abeceda	k1gFnSc1	abeceda
totiž	totiž	k9	totiž
používá	používat	k5eAaImIp3nS	používat
Ä	Ä	kA	Ä
(	(	kIx(	(
<g/>
ä	ä	k?	ä
<g/>
)	)	kIx)	)
místo	místo	k1gNnSc1	místo
Æ	Æ	k?	Æ
(	(	kIx(	(
<g/>
æ	æ	k?	æ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ö	Ö	kA	Ö
(	(	kIx(	(
<g/>
ö	ö	k?	ö
<g/>
)	)	kIx)	)
místo	místo	k1gNnSc1	místo
Ø	Ø	k?	Ø
(	(	kIx(	(
<g/>
ø	ø	k?	ø
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pořadí	pořadí	k1gNnSc1	pořadí
posledních	poslední	k2eAgNnPc2d1	poslední
tří	tři	k4xCgNnPc2	tři
písmen	písmeno	k1gNnPc2	písmeno
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
abecedě	abeceda	k1gFnSc6	abeceda
jiné	jiný	k2eAgFnSc6d1	jiná
(	(	kIx(	(
<g/>
Å	Å	k?	Å
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norský	norský	k2eAgInSc1d1	norský
pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
fonologický	fonologický	k2eAgMnSc1d1	fonologický
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jedné	jeden	k4xCgFnSc3	jeden
hlásce	hláska	k1gFnSc3	hláska
(	(	kIx(	(
<g/>
fonému	foném	k1gInSc3	foném
<g/>
)	)	kIx)	)
zpravidla	zpravidla	k6eAd1	zpravidla
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
jediný	jediný	k2eAgInSc1d1	jediný
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Zachovává	zachovávat	k5eAaImIp3nS	zachovávat
však	však	k9	však
mnoho	mnoho	k4c4	mnoho
historických	historický	k2eAgInPc2d1	historický
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
psaní	psaní	k1gNnSc1	psaní
skupin	skupina	k1gFnPc2	skupina
dj	dj	k?	dj
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
hj	hj	k?	hj
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
hv	hv	k?	hv
[	[	kIx(	[
<g/>
v	v	k7c4	v
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
různý	různý	k2eAgInSc4d1	různý
způsob	způsob	k1gInSc4	způsob
psaní	psaní	k1gNnSc2	psaní
téže	tenže	k3xDgFnSc2	tenže
hlásky	hláska	k1gFnSc2	hláska
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sj	sj	k?	sj
<g/>
,	,	kIx,	,
sk	sk	k?	sk
<g/>
,	,	kIx,	,
skj	skj	k?	skj
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
výjimek	výjimka	k1gFnPc2	výjimka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
spřežky	spřežka	k1gFnPc4	spřežka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Norština	norština	k1gFnSc1	norština
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
bohatý	bohatý	k2eAgInSc4d1	bohatý
systém	systém	k1gInSc4	systém
samohlásek	samohláska	k1gFnPc2	samohláska
–	–	k?	–
9	[number]	k4	9
krátkých	krátká	k1gFnPc2	krátká
<g/>
,	,	kIx,	,
9	[number]	k4	9
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
a	a	k8xC	a
6	[number]	k4	6
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
Souhláskových	souhláskový	k2eAgInPc2d1	souhláskový
fonémů	foném	k1gInPc2	foném
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Dlouze	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
slabiky	slabika	k1gFnSc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
následována	následovat	k5eAaImNgFnS	následovat
krátkou	krátký	k2eAgFnSc7d1	krátká
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc7	žádný
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
samohlásce	samohláska	k1gFnSc6	samohláska
následuje	následovat	k5eAaImIp3nS	následovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
souhláska	souhláska	k1gFnSc1	souhláska
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pravidlo	pravidlo	k1gNnSc4	pravidlo
komplementarity	komplementarita	k1gFnSc2	komplementarita
délky	délka	k1gFnSc2	délka
či	či	k8xC	či
slabičná	slabičný	k2eAgFnSc1d1	slabičná
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
tak	tak	k6eAd1	tak
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
–	–	k?	–
střecha	střecha	k1gFnSc1	střecha
<g/>
,	,	kIx,	,
takk	takk	k1gInSc1	takk
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
–	–	k?	–
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
slabiky	slabika	k1gFnPc1	slabika
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
krátké	krátký	k2eAgFnPc1d1	krátká
samohlásky	samohláska	k1gFnPc1	samohláska
i	i	k8xC	i
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
též	též	k9	též
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
ɘ	ɘ	k?	ɘ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bakke	bakke	k1gFnSc6	bakke
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
–	–	k?	–
kopec	kopec	k1gInSc1	kopec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
/	/	kIx~	/
<g/>
rd	rd	k?	rd
<g/>
,	,	kIx,	,
rl	rl	k?	rl
<g/>
,	,	kIx,	,
rn	rn	k?	rn
<g/>
,	,	kIx,	,
rs	rs	k?	rs
<g/>
,	,	kIx,	,
rt	rt	k?	rt
<g/>
/	/	kIx~	/
obvykle	obvykle	k6eAd1	obvykle
zaniká	zanikat	k5eAaImIp3nS	zanikat
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
/	/	kIx~	/
a	a	k8xC	a
následující	následující	k2eAgFnSc1d1	následující
souhláska	souhláska	k1gFnSc1	souhláska
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
retroflexní	retroflexní	k2eAgFnSc1d1	retroflexní
[	[	kIx(	[
<g/>
ɖ	ɖ	k?	ɖ
<g/>
,	,	kIx,	,
ɭ	ɭ	k?	ɭ
<g/>
,	,	kIx,	,
ɳ	ɳ	k?	ɳ
<g/>
,	,	kIx,	,
ʂ	ʂ	k?	ʂ
<g/>
,	,	kIx,	,
ʈ	ʈ	k?	ʈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
samostatné	samostatný	k2eAgInPc4d1	samostatný
fonémy	foném	k1gInPc4	foném
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
asimilaci	asimilace	k1gFnSc3	asimilace
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
hlavní	hlavní	k2eAgInSc1d1	hlavní
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
kmenové	kmenový	k2eAgFnSc6d1	kmenová
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
první	první	k4xOgFnSc1	první
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
norština	norština	k1gFnSc1	norština
melodický	melodický	k2eAgInSc4d1	melodický
přízvuk	přízvuk	k1gInSc4	přízvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
s	s	k7c7	s
intonací	intonace	k1gFnSc7	intonace
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
přízvuk	přízvuk	k1gInSc1	přízvuk
I	I	kA	I
a	a	k8xC	a
přízvuk	přízvuk	k1gInSc1	přízvuk
II	II	kA	II
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
akcent	akcent	k1gInSc1	akcent
I	i	k8xC	i
a	a	k8xC	a
akcent	akcent	k1gInSc1	akcent
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
s	s	k7c7	s
přízvukem	přízvuk	k1gInSc7	přízvuk
I	i	k9	i
mají	mít	k5eAaImIp3nP	mít
klesavou	klesavý	k2eAgFnSc4d1	klesavá
intonaci	intonace	k1gFnSc4	intonace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc2	intonace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc4	přízvuk
II	II	kA	II
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
jen	jen	k9	jen
slova	slovo	k1gNnPc1	slovo
nejméně	málo	k6eAd3	málo
dvouslabičná	dvouslabičný	k2eAgNnPc1d1	dvouslabičné
<g/>
.	.	kIx.	.
</s>
<s>
Intonace	intonace	k1gFnSc1	intonace
u	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
na	na	k7c6	na
přízvučné	přízvučný	k2eAgFnSc6d1	přízvučná
slabice	slabika	k1gFnSc6	slabika
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnSc1d1	následující
slabika	slabika	k1gFnSc1	slabika
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyslovena	vyslovit	k5eAaPmNgFnS	vyslovit
s	s	k7c7	s
tónem	tón	k1gInSc7	tón
vyšším	vysoký	k2eAgInSc7d2	vyšší
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
2	[number]	k4	2
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc2	intonace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
norské	norský	k2eAgFnSc2d1	norská
prozódie	prozódie	k1gFnSc2	prozódie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
přízvuku	přízvuk	k1gInSc2	přízvuk
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
jsou	být	k5eAaImIp3nP	být
komplikovaná	komplikovaný	k2eAgNnPc1d1	komplikované
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
mluvnice	mluvnice	k1gFnSc1	mluvnice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
podobných	podobný	k2eAgInPc2d1	podobný
principů	princip	k1gInPc2	princip
jako	jako	k8xS	jako
mluvnice	mluvnice	k1gFnSc1	mluvnice
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
pochopení	pochopení	k1gNnSc4	pochopení
je	být	k5eAaImIp3nS	být
výhodou	výhoda	k1gFnSc7	výhoda
znalost	znalost	k1gFnSc1	znalost
jiného	jiný	k2eAgInSc2d1	jiný
germánského	germánský	k2eAgInSc2d1	germánský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
angličtiny	angličtina	k1gFnSc2	angličtina
nebo	nebo	k8xC	nebo
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
flexe	flexe	k1gFnSc1	flexe
dává	dávat	k5eAaImIp3nS	dávat
současné	současný	k2eAgFnSc3d1	současná
norštině	norština	k1gFnSc3	norština
převažující	převažující	k2eAgInSc4d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc4d1	určitý
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
bokmå	bokmå	k?	bokmå
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
norštinou	norština	k1gFnSc7	norština
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
bokmå	bokmå	k?	bokmå
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
dánštinou	dánština	k1gFnSc7	dánština
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
norština	norština	k1gFnSc1	norština
je	být	k5eAaImIp3nS	být
bližší	blízký	k2eAgInSc1d2	bližší
živým	živý	k2eAgNnSc7d1	živé
nářečím	nářečí	k1gNnSc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Pravopisné	pravopisný	k2eAgFnPc1d1	pravopisná
reformy	reforma	k1gFnPc1	reforma
zavedly	zavést	k5eAaPmAgFnP	zavést
v	v	k7c6	v
bokmå	bokmå	k?	bokmå
tzv.	tzv.	kA	tzv.
radikální	radikální	k2eAgInPc1d1	radikální
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
napomoci	napomoct	k5eAaPmF	napomoct
sblížení	sblížení	k1gNnPc4	sblížení
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
norštinou	norština	k1gFnSc7	norština
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mluvčích	mluvčí	k1gMnPc2	mluvčí
však	však	k9	však
stále	stále	k6eAd1	stále
dává	dávat	k5eAaImIp3nS	dávat
spíše	spíše	k9	spíše
přednost	přednost	k1gFnSc4	přednost
konzervativním	konzervativní	k2eAgInPc3d1	konzervativní
tvarům	tvar	k1gInPc3	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dále	daleko	k6eAd2	daleko
uvedeny	uveden	k2eAgInPc1d1	uveden
dva	dva	k4xCgInPc1	dva
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
tvar	tvar	k1gInSc1	tvar
za	za	k7c7	za
lomítkem	lomítko	k1gNnSc7	lomítko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
norštině	norština	k1gFnSc6	norština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
v	v	k7c6	v
norštině	norština	k1gFnSc6	norština
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
následující	následující	k2eAgInPc1d1	následující
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
determinanty	determinant	k1gInPc1	determinant
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
číslovky	číslovka	k1gFnSc2	číslovka
<g/>
,	,	kIx,	,
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
a	a	k8xC	a
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
členy	člen	k1gInPc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnSc2	předložka
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnSc2	spojka
souřadicí	souřadicí	k2eAgFnSc2d1	souřadicí
(	(	kIx(	(
<g/>
konjunkce	konjunkce	k1gFnSc2	konjunkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnSc2	spojka
podřadicí	podřadicí	k2eAgFnSc2d1	podřadicí
(	(	kIx(	(
<g/>
subjunkce	subjunkce	k1gFnSc2	subjunkce
<g/>
)	)	kIx)	)
a	a	k8xC	a
citoslovce	citoslovce	k1gNnSc1	citoslovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
norština	norština	k1gFnSc1	norština
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
nářečí	nářečí	k1gNnSc2	nářečí
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
riksmå	riksmå	k?	riksmå
tvary	tvar	k1gInPc4	tvar
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
tvary	tvar	k1gInPc1	tvar
rodu	rod	k1gInSc2	rod
mužského	mužský	k2eAgInSc2d1	mužský
–	–	k?	–
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
tak	tak	k6eAd1	tak
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
dánštině	dánština	k1gFnSc6	dánština
a	a	k8xC	a
švédštině	švédština	k1gFnSc6	švédština
společný	společný	k2eAgInSc4d1	společný
rod	rod	k1gInSc4	rod
(	(	kIx(	(
<g/>
utrum	utrum	k1gNnSc2	utrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravopisné	pravopisný	k2eAgFnPc1d1	pravopisná
reformy	reforma	k1gFnPc1	reforma
zavedly	zavést	k5eAaPmAgFnP	zavést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sblížení	sblížení	k1gNnSc1	sblížení
obou	dva	k4xCgFnPc2	dva
spisovných	spisovný	k2eAgFnPc2d1	spisovná
norem	norma	k1gFnPc2	norma
do	do	k7c2	do
bokmå	bokmå	k?	bokmå
opět	opět	k6eAd1	opět
tvary	tvar	k1gInPc4	tvar
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
všeobecně	všeobecně	k6eAd1	všeobecně
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mluvčích	mluvčí	k1gMnPc2	mluvčí
je	být	k5eAaImIp3nS	být
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
vulgární	vulgární	k2eAgFnSc4d1	vulgární
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
je	on	k3xPp3gMnPc4	on
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
</s>
</p>
<p>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
determinována	determinovat	k5eAaBmNgFnS	determinovat
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
:	:	kIx,	:
en	en	k?	en
<g/>
/	/	kIx~	/
<g/>
ein	ein	k?	ein
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ei	ei	k?	ei
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
et	et	k?	et
<g/>
/	/	kIx~	/
<g/>
eit	eit	k?	eit
(	(	kIx(	(
<g/>
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
určitý	určitý	k2eAgInSc4d1	určitý
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
postpozitivní	postpozitivní	k2eAgInSc1d1	postpozitivní
–	–	k?	–
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
-en	n	k?	-en
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
-a	-a	k?	-a
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
-et	t	k?	-et
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
-ne	e	k?	-ne
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
a.	a.	k?	a.
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
navíc	navíc	k6eAd1	navíc
<g />
.	.	kIx.	.
</s>
<s>
předchází	předcházet	k5eAaImIp3nS	předcházet
ještě	ještě	k9	ještě
volný	volný	k2eAgInSc1d1	volný
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
–	–	k?	–
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
ž.	ž.	k?	ž.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
det	det	k?	det
(	(	kIx(	(
<g/>
stř	stř	k?	stř
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
[	[	kIx(	[
<g/>
diː	diː	k?	diː
<g/>
]	]	kIx)	]
<g/>
/	/	kIx~	/
<g/>
dei	dei	k?	dei
(	(	kIx(	(
<g/>
mn	mn	k?	mn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
determinováno	determinovat	k5eAaBmNgNnS	determinovat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bokmå	bokmå	k?	bokmå
však	však	k9	však
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
odpadá	odpadat	k5eAaImIp3nS	odpadat
připojený	připojený	k2eAgInSc4d1	připojený
člen	člen	k1gInSc4	člen
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
abstrakt	abstraktum	k1gNnPc2	abstraktum
a	a	k8xC	a
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
</s>
</p>
<p>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
systému	systém	k1gInSc2	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
nominativ	nominativ	k1gInSc1	nominativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
zakončení	zakončení	k1gNnSc1	zakončení
-s	-s	k?	-s
<g/>
;	;	kIx,	;
např.	např.	kA	např.
Peters	Peters	k1gInSc1	Peters
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
Petrovo	Petrův	k2eAgNnSc4d1	Petrovo
auto	auto	k1gNnSc4	auto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
výlučně	výlučně	k6eAd1	výlučně
přivlastňovací	přivlastňovací	k2eAgFnSc4d1	přivlastňovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
větných	větný	k2eAgInPc2d1	větný
vztahů	vztah	k1gInPc2	vztah
používá	používat	k5eAaImIp3nS	používat
předložky	předložka	k1gFnPc4	předložka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorovém	hovorový	k2eAgInSc6d1	hovorový
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
tvary	tvar	k1gInPc7	tvar
genitivu	genitiv	k1gInSc2	genitiv
používají	používat	k5eAaImIp3nP	používat
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
upřednostňováno	upřednostňován	k2eAgNnSc1d1	upřednostňováno
opisné	opisný	k2eAgNnSc1d1	opisné
vyjádření	vyjádření	k1gNnSc1	vyjádření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
v	v	k7c6	v
bokmå	bokmå	k?	bokmå
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k8xC	i
tvary	tvar	k1gInPc4	tvar
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hå	hå	k?	hå
i	i	k8xC	i
hå	hå	k?	hå
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
)	)	kIx)	)
ruka	ruka	k1gFnSc1	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jménaPřídavná	jménaPřídavný	k2eAgNnPc1d1	jménaPřídavný
jména	jméno	k1gNnPc1	jméno
mají	mít	k5eAaImIp3nP	mít
dvojí	dvojí	k4xRgInSc4	dvojí
typ	typ	k1gInSc4	typ
skloňování	skloňování	k1gNnSc2	skloňování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
silné	silný	k2eAgFnPc1d1	silná
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
členem	člen	k1gInSc7	člen
určitým	určitý	k2eAgInSc7d1	určitý
<g/>
)	)	kIx)	)
–	–	k?	–
jednotné	jednotný	k2eAgNnSc4d1	jednotné
zakončení	zakončení	k1gNnSc4	zakončení
-e	-e	k?	-e
<g/>
,	,	kIx,	,
např.	např.	kA	např.
det	det	k?	det
store	stor	k1gInSc5	stor
huset	huset	k5eAaImF	huset
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
<g/>
)	)	kIx)	)
velký	velký	k2eAgInSc1d1	velký
dům	dům	k1gInSc1	dům
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
slabé	slabý	k2eAgNnSc1d1	slabé
(	(	kIx(	(
<g/>
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
–	–	k?	–
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
rodě	rod	k1gInSc6	rod
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
-t	-t	k?	-t
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
godt	godt	k1gInSc1	godt
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
obou	dva	k4xCgInPc2	dva
rodů	rod	k1gInPc2	rod
-e	-e	k?	-e
<g/>
,	,	kIx,	,
např.	např.	kA	např.
et	et	k?	et
stort	stort	k1gInSc1	stort
hus	husa	k1gFnPc2	husa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nějaký	nějaký	k3yIgInSc1	nějaký
<g/>
)	)	kIx)	)
velký	velký	k2eAgInSc1d1	velký
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
husene	husen	k1gInSc5	husen
er	er	k?	er
store	stor	k1gMnSc5	stor
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc4	ten
<g/>
)	)	kIx)	)
domy	dům	k1gInPc4	dům
jsou	být	k5eAaImIp3nP	být
velké	velká	k1gFnPc1	velká
<g/>
.	.	kIx.	.
<g/>
Číslovky	číslovka	k1gFnPc1	číslovka
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
číslovky	číslovka	k1gFnPc1	číslovka
jsou	být	k5eAaImIp3nP	být
nesklonné	sklonný	k2eNgNnSc4d1	nesklonné
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
číslovky	číslovka	k1gFnSc2	číslovka
1	[number]	k4	1
(	(	kIx(	(
<g/>
én	én	k?	én
<g/>
,	,	kIx,	,
ett	ett	k?	ett
<g/>
/	/	kIx~	/
<g/>
ein	ein	k?	ein
<g/>
,	,	kIx,	,
ei	ei	k?	ei
<g/>
,	,	kIx,	,
eit	eit	k?	eit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
úředně	úředně	k6eAd1	úředně
zaveden	zavést	k5eAaPmNgInS	zavést
způsob	způsob	k1gInSc1	způsob
počítání	počítání	k1gNnSc2	počítání
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
desítky	desítka	k1gFnSc2	desítka
<g/>
–	–	k?	–
<g/>
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
femtifem	femtif	k1gInSc7	femtif
<g/>
,	,	kIx,	,
padesát	padesát	k4xCc1	padesát
pět	pět	k4xCc4	pět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
tradiční	tradiční	k2eAgNnSc1d1	tradiční
obrácené	obrácený	k2eAgNnSc1d1	obrácené
pořadí	pořadí	k1gNnSc1	pořadí
(	(	kIx(	(
<g/>
femogfemti	femogfemt	k5eAaImF	femogfemt
<g/>
,	,	kIx,	,
pětapadesát	pětapadesát	k4xCc4	pětapadesát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadové	řadový	k2eAgFnPc1d1	řadová
číslovky	číslovka	k1gFnPc1	číslovka
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
výše	výše	k1gFnSc1	výše
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-ende	-ende	k6eAd1	-ende
<g/>
/	/	kIx~	/
<g/>
-ande	nde	k6eAd1	-ande
(	(	kIx(	(
<g/>
sjuende	sjuend	k1gInSc5	sjuend
<g/>
/	/	kIx~	/
<g/>
sjuande	sjuand	k1gInSc5	sjuand
<g/>
,	,	kIx,	,
sedmý	sedmý	k4xOgMnSc1	sedmý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
sloves	sloveso	k1gNnPc2	sloveso
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
a	a	k8xC	a
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocnými	pomocný	k2eAgNnPc7d1	pomocné
slovesy	sloveso	k1gNnPc7	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
neobejdou	obejít	k5eNaPmIp3nP	obejít
bez	bez	k7c2	bez
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
skoro	skoro	k6eAd1	skoro
všech	všecek	k3xTgFnPc2	všecek
odlišujících	odlišující	k2eAgFnPc2d1	odlišující
osobních	osobní	k2eAgFnPc2d1	osobní
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
r.	r.	kA	r.
</s>
</p>
<p>
<s>
V	v	k7c6	v
infinitivu	infinitiv	k1gInSc6	infinitiv
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
sloves	sloveso	k1gNnPc2	sloveso
koncovku	koncovka	k1gFnSc4	koncovka
-e	-e	k?	-e
(	(	kIx(	(
<g/>
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
norštině	norština	k1gFnSc6	norština
též	též	k9	též
-a	-a	k?	-a
–	–	k?	–
používání	používání	k1gNnSc2	používání
obou	dva	k4xCgNnPc2	dva
zakončení	zakončení	k1gNnPc2	zakončení
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rozštěpený	rozštěpený	k2eAgInSc1d1	rozštěpený
<g/>
"	"	kIx"	"
infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
,	,	kIx,	,
klø	klø	k?	klø
infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kaste	kaste	k5eAaPmIp2nP	kaste
<g/>
/	/	kIx~	/
<g/>
kasta	kasta	k1gFnSc1	kasta
<g/>
,	,	kIx,	,
házet	házet	k5eAaImF	házet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
slovesa	sloveso	k1gNnPc1	sloveso
končí	končit	k5eAaImIp3nP	končit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
å	å	k?	å
(	(	kIx(	(
<g/>
např.	např.	kA	např.
det	det	k?	det
er	er	k?	er
mulig	mulig	k1gInSc1	mulig
å	å	k?	å
gjø	gjø	k?	gjø
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
to	ten	k3xDgNnSc4	ten
udělat	udělat	k5eAaPmF	udělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trpný	trpný	k2eAgInSc1d1	trpný
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
opisně	opisně	k6eAd1	opisně
pomocí	pomocí	k7c2	pomocí
slovesa	sloveso	k1gNnSc2	sloveso
bli	bli	k?	bli
(	(	kIx(	(
<g/>
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
a	a	k8xC	a
příčestí	příčestí	k1gNnSc2	příčestí
trpného	trpný	k2eAgNnSc2d1	trpné
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
koncovky	koncovka	k1gFnSc2	koncovka
-s	-s	k?	-s
<g/>
,	,	kIx,	,
např.	např.	kA	např.
det	det	k?	det
må	må	k?	må
gjø	gjø	k?	gjø
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
udělat	udělat	k5eAaPmF	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
švédštiny	švédština	k1gFnSc2	švédština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
běžný	běžný	k2eAgInSc1d1	běžný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
norštině	norština	k1gFnSc6	norština
používá	používat	k5eAaImIp3nS	používat
omezeně	omezeně	k6eAd1	omezeně
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
infinitivu	infinitiv	k1gInSc6	infinitiv
a	a	k8xC	a
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
všechny	všechen	k3xTgInPc4	všechen
germánské	germánský	k2eAgInPc4d1	germánský
jazyky	jazyk	k1gInPc4	jazyk
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
i	i	k9	i
norština	norština	k1gFnSc1	norština
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
a	a	k8xC	a
příčestí	příčestí	k1gNnSc6	příčestí
trpném	trpný	k2eAgNnSc6d1	trpné
přehlásku	přehláska	k1gFnSc4	přehláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
synge	syng	k1gFnPc1	syng
–	–	k?	–
sang	sang	k1gMnSc1	sang
–	–	k?	–
sunget	sunget	k1gMnSc1	sunget
<g/>
,	,	kIx,	,
zpívat	zpívat	k5eAaImF	zpívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Syntax	syntax	k1gFnSc1	syntax
</s>
</p>
<p>
<s>
Norština	norština	k1gFnSc1	norština
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
slovosled	slovosled	k1gInSc4	slovosled
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
–	–	k?	–
přísudek	přísudek	k1gInSc1	přísudek
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
–	–	k?	–
předmět	předmět	k1gInSc1	předmět
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
sloveso	sloveso	k1gNnSc1	sloveso
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
tvaru	tvar	k1gInSc6	tvar
stojí	stát	k5eAaImIp3nS	stát
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
přehozením	přehození	k1gNnSc7	přehození
přísudku	přísudek	k1gInSc2	přísudek
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
<g/>
.	.	kIx.	.
</s>
<s>
Inverzní	inverzní	k2eAgInSc1d1	inverzní
slovosled	slovosled	k1gInSc1	slovosled
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
větě	věta	k1gFnSc6	věta
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
jiný	jiný	k2eAgInSc1d1	jiný
větný	větný	k2eAgInSc1d1	větný
člen	člen	k1gInSc1	člen
než	než	k8xS	než
podmět	podmět	k1gInSc1	podmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
větě	věta	k1gFnSc6	věta
zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
jediný	jediný	k2eAgInSc1d1	jediný
záporný	záporný	k2eAgInSc1d1	záporný
výraz	výraz	k1gInSc1	výraz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jeg	jeg	k?	jeg
har	har	k?	har
ingenting	ingenting	k1gInSc1	ingenting
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ne	ne	k9	ne
<g/>
]	]	kIx)	]
<g/>
mám	mít	k5eAaImIp1nS	mít
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápor	zápor	k1gInSc1	zápor
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
částicí	částice	k1gFnSc7	částice
ikke	ikk	k1gFnSc2	ikk
<g/>
/	/	kIx~	/
<g/>
ikkje	ikkje	k1gFnSc1	ikkje
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obvykle	obvykle	k6eAd1	obvykle
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
slovesem	sloveso	k1gNnSc7	sloveso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jeg	jeg	k?	jeg
gjø	gjø	k?	gjø
ikke	ikkat	k5eAaPmIp3nS	ikkat
<g/>
,	,	kIx,	,
nedělám	dělat	k5eNaImIp1nS	dělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přívlastky	přívlastek	k1gInPc1	přívlastek
vyjádřené	vyjádřený	k2eAgFnSc2d1	vyjádřená
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
číslovkami	číslovka	k1gFnPc7	číslovka
<g/>
,	,	kIx,	,
ukazovacími	ukazovací	k2eAgNnPc7d1	ukazovací
zájmeny	zájmeno	k1gNnPc7	zájmeno
a	a	k8xC	a
genitivem	genitiv	k1gInSc7	genitiv
stojí	stát	k5eAaImIp3nP	stát
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
norské	norský	k2eAgFnSc2d1	norská
syntaxe	syntax	k1gFnSc2	syntax
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
germánským	germánský	k2eAgInPc3d1	germánský
jazykům	jazyk	k1gInPc3	jazyk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
postavení	postavení	k1gNnSc1	postavení
přivlastňovacích	přivlastňovací	k2eAgNnPc2d1	přivlastňovací
zájmen	zájmeno	k1gNnPc2	zájmeno
za	za	k7c7	za
podstatnými	podstatný	k2eAgNnPc7d1	podstatné
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
faren	faren	k1gInSc4	faren
min	mina	k1gFnPc2	mina
=	=	kIx~	=
min	mina	k1gFnPc2	mina
far	fara	k1gFnPc2	fara
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
přibírá	přibírat	k5eAaImIp3nS	přibírat
navíc	navíc	k6eAd1	navíc
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdvořilé	zdvořilý	k2eAgNnSc1d1	zdvořilé
oslovení	oslovení	k1gNnSc1	oslovení
</s>
</p>
<p>
<s>
V	v	k7c6	v
norštině	norština	k1gFnSc6	norština
existuje	existovat	k5eAaImIp3nS	existovat
zdvořilé	zdvořilý	k2eAgNnSc4d1	zdvořilé
onikání	onikání	k1gNnSc4	onikání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oslovení	oslovení	k1gNnSc1	oslovení
pomocí	pomoc	k1gFnPc2	pomoc
zájmena	zájmeno	k1gNnSc2	zájmeno
De	De	k?	De
(	(	kIx(	(
<g/>
oni	onen	k3xDgMnPc1	onen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
dosti	dosti	k6eAd1	dosti
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
Norové	Nor	k1gMnPc1	Nor
si	se	k3xPyFc3	se
běžně	běžně	k6eAd1	běžně
tykají	tykat	k5eAaImIp3nP	tykat
<g/>
.	.	kIx.	.
</s>
<s>
Oslovení	oslovení	k1gNnSc1	oslovení
du	du	k?	du
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
mezi	mezi	k7c7	mezi
zcela	zcela	k6eAd1	zcela
neznámými	známý	k2eNgMnPc7d1	neznámý
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
instituce	instituce	k1gFnPc1	instituce
==	==	k?	==
</s>
</p>
<p>
<s>
Poradním	poradní	k2eAgInSc7d1	poradní
orgánem	orgán	k1gInSc7	orgán
norské	norský	k2eAgFnSc2d1	norská
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
norského	norský	k2eAgInSc2d1	norský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jazykového	jazykový	k2eAgNnSc2d1	jazykové
plánování	plánování	k1gNnSc2	plánování
je	být	k5eAaImIp3nS	být
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Språ	Språ	k1gFnSc1	Språ
<g/>
;	;	kIx,	;
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Norsk	Norsk	k1gInSc1	Norsk
språ	språ	k?	språ
<g/>
,	,	kIx,	,
Norská	norský	k2eAgFnSc1d1	norská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
mluveného	mluvený	k2eAgInSc2d1	mluvený
i	i	k8xC	i
psaného	psaný	k2eAgInSc2d1	psaný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
aktivity	aktivita	k1gFnPc4	aktivita
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
znalostí	znalost	k1gFnPc2	znalost
norštiny	norština	k1gFnSc2	norština
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
toleranci	tolerance	k1gFnSc4	tolerance
a	a	k8xC	a
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
respekt	respekt	k1gInSc4	respekt
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
uživateli	uživatel	k1gMnPc7	uživatel
norštiny	norština	k1gFnSc2	norština
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
různých	různý	k2eAgFnPc6d1	různá
varietách	varieta	k1gFnPc6	varieta
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
práva	právo	k1gNnSc2	právo
všech	všecek	k3xTgMnPc2	všecek
občanů	občan	k1gMnPc2	občan
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
užívání	užívání	k1gNnSc4	užívání
norštiny	norština	k1gFnSc2	norština
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
poradenství	poradenství	k1gNnSc4	poradenství
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
norštiny	norština	k1gFnSc2	norština
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
používání	používání	k1gNnSc1	používání
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
a	a	k8xC	a
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
<g/>
,	,	kIx,	,
předkládá	předkládat	k5eAaImIp3nS	předkládat
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c6	o
zásadách	zásada	k1gFnPc6	zásada
kodifikace	kodifikace	k1gFnSc2	kodifikace
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
místních	místní	k2eAgNnPc2d1	místní
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
předkládá	předkládat	k5eAaImIp3nS	předkládat
návrhy	návrh	k1gInPc4	návrh
jazykových	jazykový	k2eAgFnPc2d1	jazyková
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
poradenství	poradenství	k1gNnSc4	poradenství
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
severských	severský	k2eAgFnPc2d1	severská
zemí	zem	k1gFnPc2	zem
při	při	k7c6	při
jazykové	jazykový	k2eAgFnSc6d1	jazyková
kultivaci	kultivace	k1gFnSc6	kultivace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
rada	rada	k1gFnSc1	rada
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
a	a	k8xC	a
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
norské	norský	k2eAgNnSc4d1	norské
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtletně	čtvrtletně	k6eAd1	čtvrtletně
vydává	vydávat	k5eAaPmIp3nS	vydávat
časopis	časopis	k1gInSc1	časopis
Språ	Språ	k1gFnSc2	Språ
(	(	kIx(	(
<g/>
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
noviny	novina	k1gFnPc1	novina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možnosti	možnost	k1gFnSc3	možnost
studia	studio	k1gNnSc2	studio
norštiny	norština	k1gFnSc2	norština
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Norštinu	norština	k1gFnSc4	norština
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgInPc1d1	Karlův
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
volitelný	volitelný	k2eAgInSc4d1	volitelný
předmět	předmět	k1gInSc4	předmět
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Obory	obora	k1gFnPc1	obora
nejsou	být	k5eNaImIp3nP	být
otvírány	otvírán	k2eAgInPc4d1	otvírán
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc4	studium
norštiny	norština	k1gFnSc2	norština
nabízejí	nabízet	k5eAaImIp3nP	nabízet
též	též	k9	též
některé	některý	k3yIgFnPc1	některý
soukromé	soukromý	k2eAgFnPc1d1	soukromá
jazykové	jazykový	k2eAgFnPc1d1	jazyková
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
lektoři	lektor	k1gMnPc1	lektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
několik	několik	k4yIc1	několik
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgNnPc6	který
jsou	být	k5eAaImIp3nP	být
ukázány	ukázán	k2eAgInPc1d1	ukázán
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
bokmå	bokmå	k?	bokmå
a	a	k8xC	a
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
tradiční	tradiční	k2eAgFnSc7d1	tradiční
formou	forma	k1gFnSc7	forma
riksmå	riksmå	k?	riksmå
(	(	kIx(	(
<g/>
nejpodobnější	podobný	k2eAgFnSc6d3	nejpodobnější
dánštině	dánština	k1gFnSc6	dánština
<g/>
)	)	kIx)	)
a	a	k8xC	a
dánštinou	dánština	k1gFnSc7	dánština
samotnou	samotný	k2eAgFnSc7d1	samotná
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
ukázka	ukázka	k1gFnSc1	ukázka
===	===	k?	===
</s>
</p>
<p>
<s>
Ja	Ja	k?	Ja
<g/>
,	,	kIx,	,
vi	vi	k?	vi
elsker	elsker	k1gInSc1	elsker
dette	dette	k5eAaPmIp2nP	dette
landet	landet	k1gInSc4	landet
[	[	kIx(	[
<g/>
jɑ	jɑ	k?	jɑ
<g/>
,	,	kIx,	,
viː	viː	k?	viː
ɛ	ɛ	k?	ɛ
dɛ	dɛ	k?	dɛ
lɑ	lɑ	k?	lɑ
<g/>
]	]	kIx)	]
–	–	k?	–
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
milujeme	milovat	k5eAaImIp1nP	milovat
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
norské	norský	k2eAgFnSc2d1	norská
hymny	hymna	k1gFnSc2	hymna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
uvádíme	uvádět	k5eAaImIp1nP	uvádět
i	i	k9	i
švédský	švédský	k2eAgInSc4d1	švédský
a	a	k8xC	a
dánský	dánský	k2eAgInSc4d1	dánský
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BERKOV	BERKOV	kA	BERKOV
<g/>
,	,	kIx,	,
Valerij	Valerij	k1gMnSc1	Valerij
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
=	=	kIx~	=
Sovremennyje	Sovremennyje	k1gFnSc1	Sovremennyje
germanskije	germanskít	k5eAaPmIp3nS	germanskít
jazyki	jazyki	k6eAd1	jazyki
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Renata	Renata	k1gFnSc1	Renata
Blatná	blatný	k2eAgFnSc1d1	Blatná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
čes.	čes.	k?	čes.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
268	[number]	k4	268
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUDAL	BUDAL	kA	BUDAL
<g/>
,	,	kIx,	,
Jostein	Jostein	k2eAgInSc1d1	Jostein
<g/>
.	.	kIx.	.
</s>
<s>
Fem	Fem	k?	Fem
tonar	tonar	k1gInSc1	tonar
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
:	:	kIx,	:
Unipub	Unipub	k1gInSc1	Unipub
AS	as	k1gInSc1	as
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
996588	[number]	k4	996588
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUDAL	BUDAL	kA	BUDAL
<g/>
,	,	kIx,	,
Jostein	Jostein	k2eAgInSc1d1	Jostein
<g/>
.	.	kIx.	.
</s>
<s>
Tonar	Tonar	k1gInSc1	Tonar
i	i	k8xC	i
skandinaviske	skandinaviske	k1gFnSc1	skandinaviske
språ	språ	k?	språ
<g/>
.	.	kIx.	.
</s>
<s>
Norsk	Norsk	k1gInSc1	Norsk
-	-	kIx~	-
svensk	svensk	k1gInSc1	svensk
-	-	kIx~	-
dansk	dansk	k1gInSc1	dansk
i	i	k8xC	i
nordisk	nordisk	k1gInSc1	nordisk
samtale	samtal	k1gMnSc5	samtal
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
:	:	kIx,	:
Unipub	Unipub	k1gInSc1	Unipub
AS	as	k1gInSc1	as
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
996588	[number]	k4	996588
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HROCH	Hroch	k1gMnSc1	Hroch
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
;	;	kIx,	;
KADEČKOVÁ	KADEČKOVÁ	kA	KADEČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
;	;	kIx,	;
BAKKE	BAKKE	kA	BAKKE
<g/>
,	,	kIx,	,
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
Noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
340	[number]	k4	340
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
407	[number]	k4	407
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
146	[number]	k4	146
<g/>
–	–	k?	–
<g/>
148	[number]	k4	148
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNUDSEN	KNUDSEN	kA	KNUDSEN
<g/>
,	,	kIx,	,
Trygve	Trygev	k1gFnPc1	Trygev
<g/>
.	.	kIx.	.
</s>
<s>
Kapitler	Kapitler	k1gInSc1	Kapitler
av	av	k?	av
norsk	norsk	k1gInSc4	norsk
sproghistorie	sproghistorie	k1gFnSc2	sproghistorie
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
/	/	kIx~	/
<g/>
Bergen	Bergen	k1gInSc1	Bergen
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KRISTOFFERSEN	KRISTOFFERSEN	kA	KRISTOFFERSEN
<g/>
,	,	kIx,	,
Gjert	Gjert	k1gInSc1	Gjert
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
phonology	phonolog	k1gMnPc7	phonolog
of	of	k?	of
Norwegian	Norwegiana	k1gFnPc2	Norwegiana
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
823765	[number]	k4	823765
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norsk	Norsk	k1gInSc1	Norsk
språ	språ	k?	språ
<g/>
.	.	kIx.	.
</s>
<s>
Godt	Godt	k1gInSc1	Godt
språ	språ	k?	språ
i	i	k8xC	i
læ	læ	k?	læ
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
:	:	kIx,	:
Norsk	Norsk	k1gInSc1	Norsk
språ	språ	k?	språ
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
990559	[number]	k4	990559
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEIP	SEIP	kA	SEIP
<g/>
,	,	kIx,	,
Didrik	Didrik	k1gMnSc1	Didrik
Arup	Arup	k1gMnSc1	Arup
<g/>
.	.	kIx.	.
</s>
<s>
Liten	Liten	k2eAgInSc1d1	Liten
norsk	norsk	k1gInSc1	norsk
språ	språ	k?	språ
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STRANDSKOGEN	STRANDSKOGEN	kA	STRANDSKOGEN
<g/>
,	,	kIx,	,
Å	Å	k?	Å
<g/>
;	;	kIx,	;
STRANDSKOGEN	STRANDSKOGEN	kA	STRANDSKOGEN
<g/>
,	,	kIx,	,
Rolf	Rolf	k1gInSc1	Rolf
<g/>
.	.	kIx.	.
</s>
<s>
Norsk	Norsk	k1gInSc1	Norsk
grammatikk	grammatikka	k1gFnPc2	grammatikka
for	forum	k1gNnPc2	forum
utlendinger	utlendinger	k1gInSc1	utlendinger
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Gyldendal	Gyldendal	k1gFnSc1	Gyldendal
Norsk	Norsk	k1gInSc1	Norsk
Forlag	Forlag	k1gInSc1	Forlag
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10324	[number]	k4	10324
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VINJE	VINJE	kA	VINJE
<g/>
,	,	kIx,	,
Finn-Erik	Finn-Erik	k1gMnSc1	Finn-Erik
<g/>
.	.	kIx.	.
</s>
<s>
Et	Et	k?	Et
språ	språ	k?	språ
i	i	k8xC	i
utvikling	utvikling	k1gInSc1	utvikling
<g/>
.	.	kIx.	.
</s>
<s>
Noen	Noen	k1gMnSc1	Noen
hovedlinjer	hovedlinjer	k1gMnSc1	hovedlinjer
i	i	k9	i
norsk	norsk	k1gInSc4	norsk
språ	språ	k?	språ
fra	fra	k?	fra
reformasjonen	reformasjonen	k2eAgInSc4d1	reformasjonen
til	til	k1gInSc4	til
vå	vå	k?	vå
dager	dager	k1gInSc1	dager
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Učebnice	učebnice	k1gFnSc1	učebnice
====	====	k?	====
</s>
</p>
<p>
<s>
MIKOLÁŠKOVÁ	Mikolášková	k1gFnSc1	Mikolášková
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
;	;	kIx,	;
ŠVEJDOVÁ-PINKASOVÁ	ŠVEJDOVÁ-PINKASOVÁ	k1gFnSc1	ŠVEJDOVÁ-PINKASOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
WINKLEROVÁ	Winklerová	k1gFnSc1	Winklerová
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Norština	norština	k1gFnSc1	norština
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
samouky	samouk	k1gMnPc4	samouk
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Voznice	voznice	k1gFnSc1	voznice
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Leda	leda	k6eAd1	leda
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
408	[number]	k4	408
<g/>
,	,	kIx,	,
136	[number]	k4	136
s.	s.	k?	s.
<g/>
,	,	kIx,	,
klíč	klíč	k1gInSc1	klíč
a	a	k8xC	a
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
2	[number]	k4	2
CD-ROM	CD-ROM	k1gFnPc2	CD-ROM
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7335	[number]	k4	7335
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NEFZAOUI	NEFZAOUI	kA	NEFZAOUI
<g/>
,	,	kIx,	,
Sissel	Sissel	k1gInSc1	Sissel
<g/>
;	;	kIx,	;
VRBOVÁ	Vrbová	k1gFnSc1	Vrbová
<g/>
,	,	kIx,	,
Jarka	Jarka	k1gFnSc1	Jarka
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
norštiny	norština	k1gFnSc2	norština
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
132	[number]	k4	132
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
754	[number]	k4	754
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
norština	norština	k1gFnSc1	norština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
norština	norština	k1gFnSc1	norština
ve	v	k7c4	v
WikislovníkuNorský	WikislovníkuNorský	k2eAgInSc4d1	WikislovníkuNorský
jazyk	jazyk	k1gInSc4	jazyk
–	–	k?	–
o	o	k7c6	o
norštině	norština	k1gFnSc6	norština
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
norštiny	norština	k1gFnSc2	norština
–	–	k?	–
Severské	severský	k2eAgInPc4d1	severský
listy	list	k1gInPc4	list
</s>
</p>
<p>
<s>
Norsko-český	norsko-český	k2eAgInSc1d1	norsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Lekce	lekce	k1gFnSc1	lekce
norštiny	norština	k1gFnSc2	norština
zdarma	zdarma	k6eAd1	zdarma
–	–	k?	–
Norge	Norge	k1gInSc1	Norge
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
norská	norský	k2eAgFnSc1d1	norská
konverzace	konverzace	k1gFnSc1	konverzace
volně	volně	k6eAd1	volně
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
–	–	k?	–
i	i	k9	i
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Důležitá	důležitý	k2eAgNnPc1d1	důležité
slovíčka	slovíčko	k1gNnPc1	slovíčko
norštiny	norština	k1gFnSc2	norština
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zdarma	zdarma	k6eAd1	zdarma
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
no	no	k9	no
<g/>
)	)	kIx)	)
Språ	Språ	k1gFnSc7	Språ
–	–	k?	–
norská	norský	k2eAgFnSc1d1	norská
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
rada	rada	k1gFnSc1	rada
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
no	no	k9	no
<g/>
)	)	kIx)	)
Nosksidene	Nosksiden	k1gInSc5	Nosksiden
–	–	k?	–
kurzy	kurz	k1gInPc1	kurz
norštiny	norština	k1gFnSc2	norština
(	(	kIx(	(
<g/>
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
no	no	k9	no
<g/>
)	)	kIx)	)
Lexin	Lexin	k2eAgInSc1d1	Lexin
–	–	k?	–
norský	norský	k2eAgInSc1d1	norský
slovník	slovník	k1gInSc1	slovník
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
a	a	k8xC	a
skloňováním	skloňování	k1gNnSc7	skloňování
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
no	no	k9	no
<g/>
)	)	kIx)	)
Bokmå	Bokmå	k1gFnPc7	Bokmå
og	og	k?	og
Nynorskordboka	Nynorskordboek	k1gInSc2	Nynorskordboek
–	–	k?	–
norský	norský	k2eAgInSc1d1	norský
slovník	slovník	k1gInSc1	slovník
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Norwegian	Norwegian	k1gMnSc1	Norwegian
Grammar	Grammar	k1gMnSc1	Grammar
–	–	k?	–
gramatika	gramatika	k1gFnSc1	gramatika
norštiny	norština	k1gFnSc2	norština
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ethnologue	Ethnologue	k1gFnSc1	Ethnologue
report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
–	–	k?	–
norština	norština	k1gFnSc1	norština
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Ethnologue	Ethnologu	k1gFnSc2	Ethnologu
</s>
</p>
