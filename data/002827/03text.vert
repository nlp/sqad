<s>
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1891	[number]	k4	1891
Háj	háj	k1gInSc4	háj
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Háji	háj	k1gInSc6	háj
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nemohl	moct	k5eNaImAgMnS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
koním	koní	k2eAgInPc3d1	koní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
studijní	studijní	k2eAgInPc1d1	studijní
výsledky	výsledek	k1gInPc1	výsledek
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgInPc1d1	dobrý
a	a	k8xC	a
tak	tak	k6eAd1	tak
středoškolská	středoškolský	k2eAgNnPc4d1	středoškolské
studia	studio	k1gNnPc4	studio
dokončil	dokončit	k5eAaPmAgInS	dokončit
maturitou	maturita	k1gFnSc7	maturita
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
studoval	studovat	k5eAaImAgMnS	studovat
nejdříve	dříve	k6eAd3	dříve
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
nemoci	nemoc	k1gFnSc6	nemoc
matky	matka	k1gFnSc2	matka
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
kolegyní	kolegyně	k1gFnSc7	kolegyně
ze	z	k7c2	z
studií-lékařkou	studiíékařka	k1gFnSc7	studií-lékařka
Ludmilou	Ludmila	k1gFnSc7	Ludmila
Tuhou	tuha	k1gFnSc7	tuha
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
společnou	společný	k2eAgFnSc4d1	společná
ordinaci	ordinace	k1gFnSc4	ordinace
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Vančurovým	Vančurová	k1gFnPc3	Vančurová
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Alena	Alena	k1gFnSc1	Alena
(	(	kIx(	(
<g/>
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Santarová	Santarová	k1gFnSc1	Santarová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc1d2	pozdější
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
postavil	postavit	k5eAaPmAgMnS	postavit
dům	dům	k1gInSc4	dům
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
spisovatelské	spisovatelský	k2eAgFnSc2d1	spisovatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahna	k1gFnPc2	Mahna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
předsedou	předseda	k1gMnSc7	předseda
Devětsilu	Devětsil	k1gInSc2	Devětsil
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přívrženec	přívrženec	k1gMnSc1	přívrženec
poetismu	poetismus	k1gInSc2	poetismus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Levé	levý	k2eAgFnSc2d1	levá
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
časopisech	časopis	k1gInPc6	časopis
-	-	kIx~	-
Červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
Kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
a	a	k8xC	a
Panorama	panorama	k1gNnSc1	panorama
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
však	však	k9	však
podepsal	podepsat	k5eAaPmAgMnS	podepsat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
šesti	šest	k4xCc7	šest
dalšími	další	k2eAgFnPc7d1	další
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
umělci	umělec	k1gMnSc3	umělec
tzv.	tzv.	kA	tzv.
Manifest	manifest	k1gInSc1	manifest
sedmi	sedm	k4xCc2	sedm
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Gottwaldovo	Gottwaldův	k2eAgNnSc4d1	Gottwaldovo
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
signatáři	signatář	k1gMnPc7	signatář
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
postoji	postoj	k1gInPc7	postoj
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
marxistické	marxistický	k2eAgFnSc3d1	marxistická
levici	levice	k1gFnSc3	levice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
období	období	k1gNnSc6	období
března	březen	k1gInSc2	březen
až	až	k8xS	až
května	květen	k1gInSc2	květen
jako	jako	k8xS	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
"	"	kIx"	"
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
samostatnost	samostatnost	k1gFnSc1	samostatnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
"	"	kIx"	"
<g/>
Národní	národní	k2eAgNnSc4d1	národní
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převzal	převzít	k5eAaPmAgInS	převzít
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
Jindřichu	Jindřich	k1gMnSc6	Jindřich
Vodákovi	Vodák	k1gMnSc6	Vodák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
divadelní	divadelní	k2eAgFnSc2d1	divadelní
rubriky	rubrika	k1gFnSc2	rubrika
"	"	kIx"	"
<g/>
Českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritik	k1gMnPc4	kritik
podepisoval	podepisovat	k5eAaImAgInS	podepisovat
šifrou	šifra	k1gFnSc7	šifra
-jv	v	k?	-jv
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
nebo	nebo	k8xC	nebo
-W-	-W-	k?	-W-
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
zde	zde	k6eAd1	zde
připravil	připravit	k5eAaPmAgInS	připravit
21	[number]	k4	21
divadelních	divadelní	k2eAgFnPc2d1	divadelní
kritik	kritika	k1gFnPc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
Družstevní	družstevní	k2eAgFnSc2d1	družstevní
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
vyšly	vyjít	k5eAaPmAgInP	vyjít
i	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Československé	československý	k2eAgFnSc2d1	Československá
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
vedl	vést	k5eAaImAgInS	vést
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
sekci	sekce	k1gFnSc4	sekce
Výboru	výbor	k1gInSc2	výbor
inteligence	inteligence	k1gFnSc2	inteligence
-	-	kIx~	-
ilegální	ilegální	k2eAgFnSc2d1	ilegální
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
při	při	k7c6	při
komunistickém	komunistický	k2eAgInSc6d1	komunistický
Ústředním	ústřední	k2eAgInSc6d1	ústřední
národním	národní	k2eAgInSc6d1	národní
revolučním	revoluční	k2eAgInSc6d1	revoluční
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c6	na
Kobyliské	kobyliský	k2eAgFnSc6d1	Kobyliská
střelnici	střelnice	k1gFnSc6	střelnice
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
prozaické	prozaický	k2eAgNnSc1d1	prozaické
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
expresionismem	expresionismus	k1gInSc7	expresionismus
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
experimentů	experiment	k1gInPc2	experiment
-	-	kIx~	-
hledá	hledat	k5eAaImIp3nS	hledat
nové	nový	k2eAgInPc4d1	nový
způsoby	způsob	k1gInPc4	způsob
vyjádření	vyjádření	k1gNnSc2	vyjádření
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
díla	dílo	k1gNnPc4	dílo
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
specifický	specifický	k2eAgInSc1d1	specifický
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
sloh	sloh	k1gInSc1	sloh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
větnou	větný	k2eAgFnSc4d1	větná
stavbu	stavba	k1gFnSc4	stavba
staré	starý	k2eAgFnSc2d1	stará
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
zvukomalbu	zvukomalba	k1gFnSc4	zvukomalba
<g/>
.	.	kIx.	.
</s>
<s>
Vančurův	Vančurův	k2eAgInSc1d1	Vančurův
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Archaická	archaický	k2eAgNnPc4d1	archaické
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
knižní	knižní	k2eAgInPc4d1	knižní
výrazy	výraz	k1gInPc4	výraz
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
mluvou	mluva	k1gFnSc7	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Větná	větný	k2eAgFnSc1d1	větná
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
složitá	složitý	k2eAgNnPc4d1	složité
souvětí	souvětí	k1gNnPc4	souvětí
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
jimi	on	k3xPp3gMnPc7	on
důležitost	důležitost	k1gFnSc4	důležitost
daného	daný	k2eAgInSc2d1	daný
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
dává	dávat	k5eAaImIp3nS	dávat
dílům	díl	k1gInPc3	díl
monumentální	monumentální	k2eAgFnSc2d1	monumentální
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
potlačován	potlačován	k2eAgMnSc1d1	potlačován
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
má	mít	k5eAaImIp3nS	mít
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
k	k	k7c3	k
ději	děj	k1gInSc3	děj
<g/>
,	,	kIx,	,
přerušuje	přerušovat	k5eAaImIp3nS	přerušovat
vyprávění	vyprávění	k1gNnPc4	vyprávění
a	a	k8xC	a
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
chování	chování	k1gNnSc1	chování
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
příběhů	příběh	k1gInPc2	příběh
připomínají	připomínat	k5eAaImIp3nP	připomínat
filmové	filmový	k2eAgInPc1d1	filmový
scénáře	scénář	k1gInPc1	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
prožitky	prožitek	k1gInPc4	prožitek
<g/>
,	,	kIx,	,
detailně	detailně	k6eAd1	detailně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
barevnost	barevnost	k1gFnSc4	barevnost
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
city	cit	k1gInPc7	cit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Bystrozraký	bystrozraký	k2eAgMnSc1d1	bystrozraký
-	-	kIx~	-
povídka	povídka	k1gFnSc1	povídka
Pekař	Pekař	k1gMnSc1	Pekař
Jan	Jan	k1gMnSc1	Jan
Marhoul	Marhoula	k1gFnPc2	Marhoula
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Román	román	k1gInSc1	román
se	s	k7c7	s
sociálním	sociální	k2eAgNnSc7d1	sociální
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
laskavý	laskavý	k2eAgMnSc1d1	laskavý
a	a	k8xC	a
hodný	hodný	k2eAgMnSc1d1	hodný
pekař	pekař	k1gMnSc1	pekař
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dobrota	dobrota	k1gFnSc1	dobrota
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
socialistických	socialistický	k2eAgFnPc2d1	socialistická
představ	představa	k1gFnPc2	představa
-	-	kIx~	-
dává	dávat	k5eAaImIp3nS	dávat
lidem	lid	k1gInSc7	lid
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zkrachuje	zkrachovat	k5eAaPmIp3nS	zkrachovat
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
nikdo	nikdo	k3yNnSc1	nikdo
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
v	v	k7c6	v
Pánkově	Pánkův	k2eAgFnSc6d1	Pánkova
pekárně	pekárna	k1gFnSc6	pekárna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
Marhoulovi	Marhoulův	k2eAgMnPc1d1	Marhoulův
slibuje	slibovat	k5eAaImIp3nS	slibovat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokončí	dokončit	k5eAaPmIp3nS	dokončit
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
Benešova	Benešov	k1gInSc2	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
špičkovým	špičkový	k2eAgMnSc7d1	špičkový
reprezentantem	reprezentant	k1gMnSc7	reprezentant
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
levicově	levicově	k6eAd1	levicově
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
orná	orný	k2eAgFnSc1d1	orná
a	a	k8xC	a
válečná	válečný	k2eAgFnSc1d1	válečná
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
V.	V.	kA	V.
Vančura	Vančura	k1gMnSc1	Vančura
začal	začít	k5eAaPmAgMnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
místy	místy	k6eAd1	místy
klade	klást	k5eAaImIp3nS	klást
velké	velký	k2eAgInPc4d1	velký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
jednotný	jednotný	k2eAgInSc4d1	jednotný
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
věcnou	věcný	k2eAgFnSc4d1	věcná
ani	ani	k8xC	ani
jinou	jiný	k2eAgFnSc4d1	jiná
souvislost	souvislost	k1gFnSc4	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tu	tu	k6eAd1	tu
dva	dva	k4xCgInPc4	dva
světy	svět	k1gInPc1	svět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
staví	stavit	k5eAaBmIp3nP	stavit
do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
-	-	kIx~	-
mírový	mírový	k2eAgInSc1d1	mírový
vesnický	vesnický	k2eAgInSc1d1	vesnický
a	a	k8xC	a
válečný	válečný	k2eAgInSc1d1	válečný
<g/>
.	.	kIx.	.
</s>
<s>
Vančura	Vančura	k1gMnSc1	Vančura
chce	chtít	k5eAaImIp3nS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
válka	válka	k1gFnSc1	válka
převrací	převracet	k5eAaImIp3nS	převracet
životy	život	k1gInPc4	život
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
zlodějíček	zlodějíček	k1gMnSc1	zlodějíček
a	a	k8xC	a
ničema	ničema	k1gMnSc1	ničema
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
hrdinou	hrdina	k1gMnSc7	hrdina
-	-	kIx~	-
neoslavuje	oslavovat	k5eNaImIp3nS	oslavovat
jeho	jeho	k3xOp3gNnSc4	jeho
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
vyzdvihován	vyzdvihován	k2eAgInSc1d1	vyzdvihován
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oslavován	oslavován	k2eAgMnSc1d1	oslavován
ničema	ničema	k1gMnSc1	ničema
<g/>
.	.	kIx.	.
</s>
<s>
Rozmarné	rozmarný	k2eAgNnSc1d1	Rozmarné
léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Humoristická	humoristický	k2eAgFnSc1d1	humoristická
novela	novela	k1gFnSc1	novela
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
není	být	k5eNaImIp3nS	být
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
vystihnout	vystihnout	k5eAaPmF	vystihnout
atmosféru	atmosféra	k1gFnSc4	atmosféra
malých	malý	k2eAgFnPc2d1	malá
lázní	lázeň	k1gFnPc2	lázeň
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Orši	Orš	k1gFnSc2	Orš
(	(	kIx(	(
<g/>
inspirací	inspirace	k1gFnSc7	inspirace
byly	být	k5eAaImAgFnP	být
říční	říční	k2eAgFnPc1d1	říční
lázně	lázeň	k1gFnPc1	lázeň
u	u	k7c2	u
Vančurova	Vančurův	k2eAgNnSc2d1	Vančurovo
bydliště	bydliště	k1gNnSc2	bydliště
Zbraslavi	Zbraslav	k1gFnSc2	Zbraslav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Krokových	krokový	k2eAgInPc2d1	krokový
Varů	Vary	k1gInPc2	Vary
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
kouzelník	kouzelník	k1gMnSc1	kouzelník
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
asistentkou	asistentka	k1gFnSc7	asistentka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
získat	získat	k5eAaPmF	získat
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
hrdinové	hrdina	k1gMnPc1	hrdina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
velkých	velký	k2eAgFnPc2d1	velká
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
není	být	k5eNaImIp3nS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
po	po	k7c6	po
odjezdu	odjezd	k1gInSc6	odjezd
kouzelníka	kouzelník	k1gMnSc2	kouzelník
vše	všechen	k3xTgNnSc1	všechen
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
starých	starý	k2eAgFnPc2d1	stará
zaběhlých	zaběhlý	k2eAgFnPc2d1	zaběhlá
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Slohem	sloh	k1gInSc7	sloh
lze	lze	k6eAd1	lze
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
humoresku	humoreska	k1gFnSc4	humoreska
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
poetismem	poetismus	k1gInSc7	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Kriticky	kriticky	k6eAd1	kriticky
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
dílu	dílo	k1gNnSc3	dílo
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
k	k	k7c3	k
celé	celý	k2eAgFnSc6d1	celá
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
Vančurova	Vančurův	k2eAgNnSc2d1	Vančurovo
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
tzv.	tzv.	kA	tzv.
programářské	programářský	k2eAgFnSc6d1	programářský
linii	linie	k1gFnSc6	linie
<g/>
)	)	kIx)	)
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Jan	Jan	k1gMnSc1	Jan
Lopatka	lopatka	k1gFnSc1	lopatka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
prostředí	prostředí	k1gNnSc4	prostředí
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Zakarpatské	zakarpatský	k2eAgFnSc2d1	Zakarpatská
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
svár	svár	k1gInSc1	svár
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
fikce	fikce	k1gFnSc2	fikce
a	a	k8xC	a
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
patrné	patrný	k2eAgNnSc4d1	patrné
Vančurovo	Vančurův	k2eAgNnSc4d1	Vančurovo
okouzlení	okouzlení	k1gNnSc4	okouzlení
Prahou	Praha	k1gFnSc7	Praha
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Jazykově	jazykově	k6eAd1	jazykově
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkou	velký	k2eAgFnSc7d1	velká
metaforičností	metaforičnost	k1gFnSc7	metaforičnost
<g/>
,	,	kIx,	,
syntaktickou	syntaktický	k2eAgFnSc7d1	syntaktická
a	a	k8xC	a
kompoziční	kompoziční	k2eAgFnSc7d1	kompoziční
složitostí	složitost	k1gFnSc7	složitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
Vančura	Vančura	k1gMnSc1	Vančura
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc4	jeho
odklon	odklon	k1gInSc4	odklon
k	k	k7c3	k
realismu	realismus	k1gInSc3	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Hrdelní	hrdelní	k2eAgFnSc1d1	hrdelní
pře	pře	k1gFnSc1	pře
anebo	anebo	k8xC	anebo
přísloví	přísloví	k1gNnSc1	přísloví
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
viny	vina	k1gFnSc2	vina
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
přátel	přítel	k1gMnPc2	přítel
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
odhalit	odhalit	k5eAaPmF	odhalit
starou	starý	k2eAgFnSc4d1	stará
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc1d1	otevřený
-	-	kIx~	-
vítězí	vítězit	k5eAaImIp3nS	vítězit
hodnoty	hodnota	k1gFnSc2	hodnota
plného	plný	k2eAgInSc2d1	plný
a	a	k8xC	a
radostného	radostný	k2eAgInSc2d1	radostný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
oslavou	oslava	k1gFnSc7	oslava
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
"	"	kIx"	"
<g/>
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kritické	kritický	k2eAgInPc4d1	kritický
výpady	výpad	k1gInPc4	výpad
proti	proti	k7c3	proti
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jazykové	jazykový	k2eAgFnSc6d1	jazyková
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
velká	velký	k2eAgFnSc1d1	velká
frekvence	frekvence	k1gFnSc1	frekvence
lidových	lidový	k2eAgNnPc2d1	lidové
rčení	rčení	k1gNnPc2	rčení
a	a	k8xC	a
přísloví	přísloví	k1gNnSc2	přísloví
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
zdramatizován	zdramatizovat	k5eAaPmNgInS	zdramatizovat
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
dramatizace	dramatizace	k1gFnSc1	dramatizace
Karel	Karel	k1gMnSc1	Karel
Tachovský	tachovský	k2eAgMnSc1d1	tachovský
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Hradil	Hradil	k1gMnSc1	Hradil
<g/>
.	.	kIx.	.
</s>
<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc1	důraz
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
až	až	k9	až
pak	pak	k6eAd1	pak
na	na	k7c4	na
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Osnovou	osnova	k1gFnSc7	osnova
je	být	k5eAaImIp3nS	být
baladický	baladický	k2eAgInSc1d1	baladický
příběh	příběh	k1gInSc1	příběh
plný	plný	k2eAgInSc1d1	plný
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vystižení	vystižení	k1gNnSc4	vystižení
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
vylíčení	vylíčení	k1gNnSc2	vylíčení
prudké	prudký	k2eAgFnSc2d1	prudká
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Dějovým	dějový	k2eAgNnSc7d1	dějové
pozadím	pozadí	k1gNnSc7	pozadí
je	být	k5eAaImIp3nS	být
středověk	středověk	k1gInSc1	středověk
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
líčeno	líčen	k2eAgNnSc1d1	líčeno
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
dvou	dva	k4xCgInPc2	dva
loupežnických	loupežnický	k2eAgInPc2d1	loupežnický
rodů	rod	k1gInPc2	rod
Lazarů	Lazar	k1gMnPc2	Lazar
a	a	k8xC	a
Kozlíků	kozlík	k1gMnPc2	kozlík
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Kozlíka	kozlík	k1gMnSc2	kozlík
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
lásky	láska	k1gFnSc2	láska
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
oba	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zbavit	zbavit	k5eAaPmF	zbavit
svá	svůj	k3xOyFgNnPc4	svůj
území	území	k1gNnPc4	území
loupežníků	loupežník	k1gMnPc2	loupežník
a	a	k8xC	a
po	po	k7c6	po
kraji	kraj	k1gInSc6	kraj
putují	putovat	k5eAaImIp3nP	putovat
pluky	pluk	k1gInPc1	pluk
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
rodiny	rodina	k1gFnPc1	rodina
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lazar	Lazar	k1gMnSc1	Lazar
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vymyslet	vymyslet	k5eAaPmF	vymyslet
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vetřít	vetřít	k5eAaPmF	vetřít
do	do	k7c2	do
přízně	přízeň	k1gFnSc2	přízeň
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Kozlík	kozlík	k1gInSc1	kozlík
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
vojsku	vojsko	k1gNnSc3	vojsko
nepodlehnout	podlehnout	k5eNaPmF	podlehnout
a	a	k8xC	a
držet	držet	k5eAaImF	držet
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
"	"	kIx"	"
<g/>
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Mikoláše	Mikoláš	k1gMnSc4	Mikoláš
k	k	k7c3	k
Lazarovým	Lazarová	k1gFnPc3	Lazarová
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
a	a	k8xC	a
porazili	porazit	k5eAaPmAgMnP	porazit
královo	králův	k2eAgNnSc4d1	královo
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
hádce	hádka	k1gFnSc3	hádka
a	a	k8xC	a
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
poraněný	poraněný	k2eAgMnSc1d1	poraněný
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
Roháček	roháček	k1gInSc4	roháček
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
Kozlíků	kozlík	k1gInPc2	kozlík
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zde	zde	k6eAd1	zde
velký	velký	k2eAgInSc1d1	velký
povyk	povyk	k1gInSc1	povyk
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
zahanben	zahanben	k2eAgInSc1d1	zahanben
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
nechal	nechat	k5eAaPmAgMnS	nechat
zbít	zbít	k5eAaPmF	zbít
<g/>
.	.	kIx.	.
</s>
<s>
Starý	Starý	k1gMnSc1	Starý
Kozlík	kozlík	k1gMnSc1	kozlík
synem	syn	k1gMnSc7	syn
opovrhuje	opovrhovat	k5eAaImIp3nS	opovrhovat
a	a	k8xC	a
vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
syny	syn	k1gMnPc7	syn
na	na	k7c4	na
Obořiště	Obořiště	k1gNnSc4	Obořiště
k	k	k7c3	k
Lazarům	Lazar	k1gMnPc3	Lazar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prázdné	prázdný	k2eAgNnSc1d1	prázdné
-	-	kIx~	-
Lazarovi	Lazar	k1gMnSc3	Lazar
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Kozlíkové	kozlíkový	k2eAgFnPc1d1	Kozlíková
tvrz	tvrz	k1gFnSc1	tvrz
vypálí	vypálit	k5eAaPmIp3nS	vypálit
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
několik	několik	k4yIc4	několik
přesunů	přesun	k1gInPc2	přesun
loupežníků	loupežník	k1gMnPc2	loupežník
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
uzdraví	uzdravit	k5eAaPmIp3nS	uzdravit
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
dojet	dojet	k5eAaPmF	dojet
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgNnSc4	který
celou	celá	k1gFnSc4	celá
dobu	doba	k1gFnSc4	doba
prahne	prahnout	k5eAaImIp3nS	prahnout
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
deseti	deset	k4xCc7	deset
členy	člen	k1gMnPc4	člen
tlupy	tlup	k1gInPc1	tlup
dojedou	dojet	k5eAaPmIp3nP	dojet
na	na	k7c4	na
Obořiště	Obořiště	k1gNnSc4	Obořiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přepadnou	přepadnout	k5eAaPmIp3nP	přepadnout
rodinu	rodina	k1gFnSc4	rodina
Lazarových	Lazarová	k1gFnPc2	Lazarová
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
si	se	k3xPyFc3	se
odtud	odtud	k6eAd1	odtud
odvede	odvést	k5eAaPmIp3nS	odvést
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
Lazarových	Lazarových	k2eAgFnPc2d1	Lazarových
dcer	dcera	k1gFnPc2	dcera
-	-	kIx~	-
Markétu	Markéta	k1gFnSc4	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
byla	být	k5eAaImAgFnS	být
zaslíbena	zaslíbit	k5eAaPmNgFnS	zaslíbit
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
nesla	nést	k5eAaImAgFnS	nést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
milenkou	milenka	k1gFnSc7	milenka
loupežníka	loupežník	k1gMnSc2	loupežník
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
ale	ale	k9	ale
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Loupežníci	loupežník	k1gMnPc1	loupežník
stále	stále	k6eAd1	stále
svádějí	svádět	k5eAaImIp3nP	svádět
boje	boj	k1gInPc1	boj
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
hvozdu	hvozd	k1gInSc2	hvozd
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
vojáků	voják	k1gMnPc2	voják
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
je	on	k3xPp3gNnSc4	on
však	však	k9	však
následují	následovat	k5eAaImIp3nP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
další	další	k2eAgFnSc1d1	další
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Kristián	Kristián	k1gMnSc1	Kristián
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zajatců	zajatec	k1gMnPc2	zajatec
Kozlíkových	kozlíkový	k2eAgMnPc2d1	kozlíkový
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
německého	německý	k2eAgMnSc2d1	německý
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Kozlíkovy	kozlíkův	k2eAgFnSc2d1	Kozlíkova
dcery	dcera	k1gFnSc2	dcera
Alexandry	Alexandra	k1gFnSc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
královské	královský	k2eAgNnSc1d1	královské
vojsko	vojsko	k1gNnSc1	vojsko
ve	v	k7c6	v
veliké	veliký	k2eAgFnSc6d1	veliká
přesile	přesila	k1gFnSc6	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Kozlíkovi	kozlíkův	k2eAgMnPc1d1	kozlíkův
bijí	bít	k5eAaImIp3nP	bít
jako	jako	k9	jako
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
dát	dát	k5eAaPmF	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
Kozlík	kozlík	k1gMnSc1	kozlík
je	být	k5eAaImIp3nS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tlupy	tlupa	k1gFnSc2	tlupa
se	se	k3xPyFc4	se
sejde	sejít	k5eAaPmIp3nS	sejít
na	na	k7c6	na
smluveném	smluvený	k2eAgNnSc6d1	smluvené
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
matky	matka	k1gFnSc2	matka
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
otce	otec	k1gMnSc4	otec
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nepovede	vést	k5eNaImIp3nS	vést
a	a	k8xC	a
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
jsou	být	k5eAaImIp3nP	být
zajati	zajat	k2eAgMnPc1d1	zajat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
popraveni	popraven	k2eAgMnPc1d1	popraven
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
i	i	k8xC	i
Alexandra	Alexandra	k1gFnSc1	Alexandra
porodí	porodit	k5eAaPmIp3nS	porodit
<g/>
.	.	kIx.	.
</s>
<s>
Alexandra	Alexandra	k1gFnSc1	Alexandra
se	se	k3xPyFc4	se
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
Markéta	Markéta	k1gFnSc1	Markéta
vychová	vychovat	k5eAaPmIp3nS	vychovat
obě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
zavržena	zavržen	k2eAgFnSc1d1	zavržena
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
ke	k	k7c3	k
Kozlíkovým	kozlíkový	k2eAgMnPc3d1	kozlíkový
<g/>
.	.	kIx.	.
</s>
<s>
Zbylým	zbylý	k2eAgFnPc3d1	zbylá
ženám	žena	k1gFnPc3	žena
z	z	k7c2	z
tlupy	tlupa	k1gFnSc2	tlupa
je	být	k5eAaImIp3nS	být
zabaven	zabaven	k2eAgInSc4d1	zabaven
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
vystěhovány	vystěhovat	k5eAaPmNgInP	vystěhovat
na	na	k7c4	na
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracovaly	pracovat	k5eAaImAgFnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Vančura	Vančura	k1gMnSc1	Vančura
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
próz	próza	k1gFnPc2	próza
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Útěk	útěk	k1gInSc1	útěk
do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
tragická	tragický	k2eAgFnSc1d1	tragická
láska	láska	k1gFnSc1	láska
Češky	Češka	k1gFnSc2	Češka
a	a	k8xC	a
Slováka	Slovák	k1gMnSc4	Slovák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Vančurovi	Vančura	k1gMnSc3	Vančura
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
konfrontovat	konfrontovat	k5eAaBmF	konfrontovat
národní	národní	k2eAgFnSc2d1	národní
povahy	povaha	k1gFnSc2	povaha
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
královny	královna	k1gFnSc2	královna
Dorotky	Dorotka	k1gFnSc2	Dorotka
Soubor	soubor	k1gInSc1	soubor
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
starých	starý	k2eAgInPc2d1	starý
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Kratochvíle	kratochvíle	k1gFnSc2	kratochvíle
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
XII	XII	kA	XII
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
košíkářem	košíkář	k1gMnSc7	košíkář
a	a	k8xC	a
jehož	jehož	k3xOyRp3gMnSc1	jehož
poslední	poslední	k2eAgMnSc1d1	poslední
majitel	majitel	k1gMnSc1	majitel
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosazen	dosazen	k2eAgMnSc1d1	dosazen
neurozený	urozený	k2eNgMnSc1d1	neurozený
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
zbohatlík	zbohatlík	k1gMnSc1	zbohatlík
Stoklasa	Stoklasa	k1gMnSc1	Stoklasa
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
kosmopolitní	kosmopolitní	k2eAgFnSc2d1	kosmopolitní
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
Skotka	Skotka	k1gFnSc1	Skotka
Ellen	Ellna	k1gFnPc2	Ellna
<g/>
,	,	kIx,	,
o	o	k7c4	o
krávy	kráva	k1gFnPc4	kráva
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
Švýcar	Švýcar	k1gMnSc1	Švýcar
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přichází	přicházet	k5eAaImIp3nS	přicházet
pro	pro	k7c4	pro
knihy	kniha	k1gFnPc4	kniha
holandský	holandský	k2eAgMnSc1d1	holandský
obchodník	obchodník	k1gMnSc1	obchodník
Huylidenn	Huylidenn	k1gMnSc1	Huylidenn
z	z	k7c2	z
Haagu	Haag	k1gInSc2	Haag
<g/>
)	)	kIx)	)
údajný	údajný	k2eAgMnSc1d1	údajný
ruský	ruský	k2eAgMnSc1d1	ruský
kníže	kníže	k1gMnSc1	kníže
prevoschoditělstvo	prevoschoditělstvo	k1gNnSc4	prevoschoditělstvo
Alexander	Alexandra	k1gFnPc2	Alexandra
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Megalrogov	Megalrogov	k1gInSc4	Megalrogov
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
sluhou	sluha	k1gMnSc7	sluha
Váňou	Váňa	k1gMnSc7	Váňa
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
důstojně	důstojně	k6eAd1	důstojně
v	v	k7c6	v
mysliveckém	myslivecký	k2eAgInSc6d1	myslivecký
kabátci	kabátec	k1gInSc6	kabátec
a	a	k8xC	a
s	s	k7c7	s
nahajkou	nahajka	k1gFnSc7	nahajka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všem	všecek	k3xTgMnPc3	všecek
aristokratické	aristokratický	k2eAgFnPc1d1	aristokratická
způsoby	způsob	k1gInPc1	způsob
minulého	minulý	k2eAgMnSc2d1	minulý
pána	pán	k1gMnSc2	pán
scházejí	scházet	k5eAaImIp3nP	scházet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
knížete	kníže	k1gNnSc4wR	kníže
přijmou	přijmout	k5eAaPmIp3nP	přijmout
<g/>
,	,	kIx,	,
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Milosti	milost	k1gFnPc1	milost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgMnSc1	tento
kníže	kníže	k1gMnSc1	kníže
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
staré	starý	k2eAgInPc4d1	starý
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Megalrogov	Megalrogov	k1gInSc1	Megalrogov
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
nedůvěrou	nedůvěra	k1gFnSc7	nedůvěra
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Jana	Jan	k1gMnSc2	Jan
Lhoty	Lhota	k1gMnSc2	Lhota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c2	za
barona	baron	k1gMnSc2	baron
Prášila	Prášil	k1gMnSc2	Prášil
a	a	k8xC	a
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
pronáší	pronášet	k5eAaImIp3nS	pronášet
kníže	kníže	k1gMnSc1	kníže
slova	slovo	k1gNnSc2	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
fleuret	fleuret	k1gInSc4	fleuret
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgMnSc1d1	dobrý
žák	žák	k1gMnSc1	žák
té	ten	k3xDgFnSc2	ten
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Spera	Spera	k1gFnSc1	Spera
říká	říkat	k5eAaImIp3nS	říkat
flanderská	flanderský	k2eAgFnSc1d1	Flanderská
<g/>
.	.	kIx.	.
</s>
<s>
Jste	být	k5eAaImIp2nP	být
pevný	pevný	k2eAgInSc4d1	pevný
v	v	k7c6	v
kolenou	koleno	k1gNnPc6	koleno
a	a	k8xC	a
máte	mít	k5eAaImIp2nP	mít
hbitou	hbitý	k2eAgFnSc4d1	hbitá
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Věru	Věra	k1gFnSc4	Věra
...	...	k?	...
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
či	či	k8xC	či
čtyři	čtyři	k4xCgInPc1	čtyři
lekce	lekce	k1gFnSc2	lekce
bych	by	kYmCp1nS	by
z	z	k7c2	z
vás	vy	k3xPp2nPc2	vy
měl	mít	k5eAaImAgInS	mít
strach	strach	k1gInSc1	strach
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kníže	kníže	k1gNnSc1wR	kníže
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
vynikající	vynikající	k2eAgMnSc1d1	vynikající
šermíř	šermíř	k1gMnSc1	šermíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k8xC	jako
znalec	znalec	k1gMnSc1	znalec
malířství	malířství	k1gNnSc2	malířství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Podobný	podobný	k2eAgInSc1d1	podobný
odstín	odstín	k1gInSc1	odstín
hnědi	hněď	k1gFnSc2	hněď
mi	já	k3xPp1nSc3	já
zježí	zježit	k5eAaPmIp3nS	zježit
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
dívám	dívat	k5eAaImIp1nS	dívat
na	na	k7c4	na
kobalt	kobalt	k1gInSc4	kobalt
<g/>
,	,	kIx,	,
mám	mít	k5eAaImIp1nS	mít
husí	husí	k2eAgFnSc4d1	husí
kůži	kůže	k1gFnSc4	kůže
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Když	když	k8xS	když
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
loučí	loučit	k5eAaImIp3nS	loučit
se	se	k3xPyFc4	se
výrokem	výrok	k1gInSc7	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
zásluha	zásluha	k1gFnSc1	zásluha
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
psí	psí	k1gNnSc6	psí
dána	dát	k5eAaPmNgFnS	dát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
utrhači	utrhač	k1gMnPc1	utrhač
sedí	sedit	k5eAaImIp3nP	sedit
rána	ráno	k1gNnPc4	ráno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tři	tři	k4xCgFnPc1	tři
řeky	řeka	k1gFnPc1	řeka
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
románu	román	k1gInSc6	román
se	se	k3xPyFc4	se
Vančura	Vančura	k1gMnSc1	Vančura
začíná	začínat	k5eAaImIp3nS	začínat
zabývat	zabývat	k5eAaImF	zabývat
současností	současnost	k1gFnSc7	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vylíčení	vylíčení	k1gNnSc4	vylíčení
života	život	k1gInSc2	život
selského	selské	k1gNnSc2	selské
synka	synek	k1gMnSc2	synek
Jana	Jan	k1gMnSc2	Jan
Kostky	Kostka	k1gMnSc2	Kostka
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
až	až	k9	až
po	po	k7c4	po
první	první	k4xOgFnSc4	první
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
pozornost	pozornost	k1gFnSc1	pozornost
věnována	věnovat	k5eAaPmNgFnS	věnovat
ději	děj	k1gInPc7	děj
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jazyku	jazyk	k1gMnSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
protiválečné	protiválečný	k2eAgNnSc1d1	protiválečné
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Horvatova	Horvatův	k2eAgFnSc1d1	Horvatova
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
k	k	k7c3	k
připravované	připravovaný	k2eAgFnSc3d1	připravovaná
trilogii	trilogie	k1gFnSc3	trilogie
Koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
pouze	pouze	k6eAd1	pouze
tento	tento	k3xDgInSc1	tento
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
na	na	k7c6	na
historických	historický	k2eAgInPc6d1	historický
podkladech	podklad	k1gInPc6	podklad
<g/>
,	,	kIx,	,
z	z	k7c2	z
původně	původně	k6eAd1	původně
zamýšlených	zamýšlený	k2eAgInPc2d1	zamýšlený
šesti	šest	k4xCc2	šest
dílů	díl	k1gInPc2	díl
napsal	napsat	k5eAaBmAgInS	napsat
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
gestapem	gestapo	k1gNnSc7	gestapo
uprostřed	uprostřed	k7c2	uprostřed
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
tak	tak	k6eAd1	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
-	-	kIx~	-
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
souvislé	souvislý	k2eAgNnSc4d1	souvislé
vyprávění	vyprávění	k1gNnSc4	vyprávění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
stylizované	stylizovaný	k2eAgInPc4d1	stylizovaný
příběhy	příběh	k1gInPc4	příběh
slavných	slavný	k2eAgFnPc2d1	slavná
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
Kosmas	Kosmas	k1gMnSc1	Kosmas
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
praotce	praotec	k1gMnSc2	praotec
Čecha	Čech	k1gMnSc2	Čech
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
posilovat	posilovat	k5eAaImF	posilovat
národní	národní	k2eAgNnSc1d1	národní
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
v	v	k7c6	v
osudových	osudový	k2eAgFnPc6d1	osudová
chvílích	chvíle	k1gFnPc6	chvíle
pomnichovských	pomnichovský	k2eAgFnPc6d1	pomnichovská
<g/>
.	.	kIx.	.
</s>
<s>
Amazonský	amazonský	k2eAgInSc1d1	amazonský
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
již	již	k6eAd1	již
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jeho	jeho	k3xOp3gInSc1	jeho
typický	typický	k2eAgInSc1d1	typický
a	a	k8xC	a
nenapodobitelný	napodobitelný	k2eNgInSc1d1	nenapodobitelný
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
lyrizaci	lyrizace	k1gFnSc6	lyrizace
epiky	epika	k1gFnSc2	epika
<g/>
,	,	kIx,	,
obrazném	obrazný	k2eAgNnSc6d1	obrazné
vidění	vidění	k1gNnSc6	vidění
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
oslabení	oslabení	k1gNnSc4	oslabení
časové	časový	k2eAgFnSc2d1	časová
a	a	k8xC	a
dějové	dějový	k2eAgFnSc2d1	dějová
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
,	,	kIx,	,
častý	častý	k2eAgInSc1d1	častý
výskyt	výskyt	k1gInSc1	výskyt
metafor	metafora	k1gFnPc2	metafora
<g/>
,	,	kIx,	,
hyperbolizace	hyperbolizace	k1gFnSc2	hyperbolizace
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
monumentalita	monumentalita	k1gFnSc1	monumentalita
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
umocněná	umocněný	k2eAgFnSc1d1	umocněná
složitou	složitý	k2eAgFnSc7d1	složitá
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
složkou	složka	k1gFnSc7	složka
díla	dílo	k1gNnSc2	dílo
Občan	občan	k1gMnSc1	občan
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
-	-	kIx~	-
sborník	sborník	k1gInSc4	sborník
Kněz	kněz	k1gMnSc1	kněz
Gudari	Gudar	k1gFnSc2	Gudar
-	-	kIx~	-
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
ročenka	ročenka	k1gFnSc1	ročenka
Kosmas	Kosmas	k1gMnSc1	Kosmas
Kubula	Kubula	k1gFnSc1	Kubula
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Kubikula	Kubikula	k1gFnSc1	Kubikula
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
určená	určený	k2eAgFnSc1d1	určená
dětem	dítě	k1gFnPc3	dítě
o	o	k7c4	o
medvědáři	medvědář	k1gMnSc3	medvědář
<g/>
,	,	kIx,	,
medvědovi	medvěd	k1gMnSc3	medvěd
a	a	k8xC	a
medvědím	medvědí	k2eAgNnSc6d1	medvědí
strašidle	strašidlo	k1gNnSc6	strašidlo
Jezero	jezero	k1gNnSc4	jezero
Ukereve	Ukereev	k1gFnPc1	Ukereev
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
-	-	kIx~	-
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zde	zde	k6eAd1	zde
myšlenky	myšlenka	k1gFnPc1	myšlenka
proti	proti	k7c3	proti
kolonizaci	kolonizace	k1gFnSc3	kolonizace
<g/>
.	.	kIx.	.
</s>
<s>
Josefína	Josefína	k1gFnSc1	Josefína
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
variace	variace	k1gFnPc1	variace
na	na	k7c4	na
Pygmalion	Pygmalion	k1gInSc4	Pygmalion
G.	G.	kA	G.
B.	B.	kA	B.
Shawa	Shawa	k1gMnSc1	Shawa
<g/>
,	,	kIx,	,
barová	barový	k2eAgFnSc1d1	barová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Josefína	Josefína	k1gFnSc1	Josefína
se	se	k3xPyFc4	se
pozná	poznat	k5eAaPmIp3nS	poznat
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
z	z	k7c2	z
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
učit	učit	k5eAaImF	učit
a	a	k8xC	a
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
utíká	utíkat	k5eAaImIp3nS	utíkat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nelíbí	líbit	k5eNaImIp3nS	líbit
přetvářka	přetvářka	k1gFnSc1	přetvářka
<g/>
,	,	kIx,	,
intriky	intrika	k1gFnPc1	intrika
<g/>
,	,	kIx,	,
maloměšťáctví	maloměšťáctví	k1gNnSc1	maloměšťáctví
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
hrána	hrát	k5eAaImNgFnS	hrát
až	až	k9	až
v	v	k7c6	v
r.	r.	kA	r.
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Alchymista	alchymista	k1gMnSc1	alchymista
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
sada	sada	k1gFnSc1	sada
děl	dělo	k1gNnPc2	dělo
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgFnSc1d1	začínající
Hrdelní	hrdelní	k2eAgFnSc7d1	hrdelní
pří	pře	k1gFnSc7	pře
<g/>
,	,	kIx,	,
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
dramatem	drama	k1gNnSc7	drama
<g/>
,	,	kIx,	,
Alchymistou	alchymista	k1gMnSc7	alchymista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Vančurova	Vančurův	k2eAgFnSc1d1	Vančurova
třetí	třetí	k4xOgFnSc1	třetí
celovečerní	celovečerní	k2eAgFnSc1d1	celovečerní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1932	[number]	k4	1932
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Alchymista	alchymista	k1gMnSc1	alchymista
je	být	k5eAaImIp3nS	být
skutečný	skutečný	k2eAgMnSc1d1	skutečný
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
dodatečně	dodatečně	k6eAd1	dodatečně
kvalifikuje	kvalifikovat	k5eAaBmIp3nS	kvalifikovat
předpovědí	předpověď	k1gFnSc7	předpověď
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
do	do	k7c2	do
role	role	k1gFnSc2	role
podvodného	podvodný	k2eAgMnSc2d1	podvodný
výrobce	výrobce	k1gMnSc2	výrobce
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
vehnán	vehnat	k5eAaPmNgInS	vehnat
chtivostí	chtivost	k1gFnSc7	chtivost
a	a	k8xC	a
vášněmi	vášeň	k1gFnPc7	vášeň
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc7d1	vlastní
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sdílel	sdílet	k5eAaImAgInS	sdílet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
míněním	mínění	k1gNnSc7	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
proměňovat	proměňovat	k5eAaImF	proměňovat
kovy	kov	k1gInPc4	kov
ve	v	k7c4	v
zlato	zlato	k1gNnSc4	zlato
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
možno	možno	k6eAd1	možno
a	a	k8xC	a
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
má	mít	k5eAaImIp3nS	mít
rozřešení	rozřešení	k1gNnSc4	rozřešení
tajemství	tajemství	k1gNnSc2	tajemství
už	už	k9	už
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Martin	Martin	k1gInSc4	Martin
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
bratří	bratr	k1gMnPc2	bratr
Koryčanů	Koryčan	k1gMnPc2	Koryčan
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
života	život	k1gInSc2	život
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
vědec	vědec	k1gMnSc1	vědec
hvězdář	hvězdář	k1gMnSc1	hvězdář
Alessandro	Alessandra	k1gFnSc5	Alessandra
del	del	k?	del
Morone	Moron	k1gMnSc5	Moron
<g/>
.	.	kIx.	.
</s>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
del	del	k?	del
Morone	Moron	k1gInSc5	Moron
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
inspirativní	inspirativní	k2eAgMnSc1d1	inspirativní
a	a	k8xC	a
strhující	strhující	k2eAgInSc1d1	strhující
příklad	příklad	k1gInSc1	příklad
jižního	jižní	k2eAgInSc2d1	jižní
životního	životní	k2eAgInSc2d1	životní
slohu	sloh	k1gInSc2	sloh
<g/>
,	,	kIx,	,
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
vlastně	vlastně	k9	vlastně
všecko	všecek	k3xTgNnSc4	všecek
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yRnSc4	co
sáhne	sáhnout	k5eAaPmIp3nS	sáhnout
a	a	k8xC	a
všecky	všecek	k3xTgInPc4	všecek
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
nechává	nechávat	k5eAaImIp3nS	nechávat
proměnit	proměnit	k5eAaPmF	proměnit
ve	v	k7c6	v
zosobnění	zosobnění	k1gNnSc6	zosobnění
žádosti	žádost	k1gFnSc2	žádost
po	po	k7c6	po
penězích	peníze	k1gInPc6	peníze
a	a	k8xC	a
po	po	k7c6	po
zlatě	zlato	k1gNnSc6	zlato
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
v	v	k7c4	v
žádostivost	žádostivost	k1gFnSc4	žádostivost
po	po	k7c6	po
poznání	poznání	k1gNnSc6	poznání
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
v	v	k7c4	v
nešťastnou	šťastný	k2eNgFnSc4d1	nešťastná
hříšnici	hříšnice	k1gFnSc4	hříšnice
<g/>
.	.	kIx.	.
</s>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
nepůsobí	působit	k5eNaImIp3nP	působit
svým	svůj	k3xOyFgInSc7	svůj
temperamentem	temperament	k1gInSc7	temperament
na	na	k7c4	na
psychicky	psychicky	k6eAd1	psychicky
neutišený	utišený	k2eNgInSc4d1	utišený
stav	stav	k1gInSc4	stav
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Koryčanových	Koryčanův	k2eAgMnPc2d1	Koryčanův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
nemalý	malý	k2eNgInSc4d1	nemalý
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
manželů	manžel	k1gMnPc2	manžel
Koryčanových	Koryčanův	k2eAgInPc2d1	Koryčanův
<g/>
,	,	kIx,	,
Evu	Eva	k1gFnSc4	Eva
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c4	na
bratra	bratr	k1gMnSc4	bratr
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgInSc2d1	mladý
Bubna	Bubn	k1gInSc2	Bubn
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Alessandro	Alessandra	k1gFnSc5	Alessandra
probudí	probudit	k5eAaPmIp3nS	probudit
velkou	velký	k2eAgFnSc4d1	velká
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nakonec	nakonec	k6eAd1	nakonec
odvahu	odvaha	k1gFnSc4	odvaha
odejít	odejít	k5eAaPmF	odejít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
;	;	kIx,	;
v	v	k7c6	v
Bubnovi	Bubn	k1gMnSc6	Bubn
je	být	k5eAaImIp3nS	být
probuzeno	probuzen	k2eAgNnSc1d1	probuzeno
nadšení	nadšení	k1gNnSc1	nadšení
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
odpojení	odpojení	k1gNnSc6	odpojení
od	od	k7c2	od
mistra	mistr	k1gMnSc2	mistr
míří	mířit	k5eAaImIp3nS	mířit
výš	vysoce	k6eAd2	vysoce
než	než	k8xS	než
jeho	jeho	k3xOp3gInSc1	jeho
vzor	vzor	k1gInSc1	vzor
sám	sám	k3xTgInSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Alessandrovi	Alessandr	k1gMnSc3	Alessandr
stojí	stát	k5eAaImIp3nS	stát
také	také	k6eAd1	také
další	další	k2eAgFnSc1d1	další
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
Třešť	třeštit	k5eAaImRp2nS	třeštit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ideologickou	ideologický	k2eAgFnSc7d1	ideologická
oporou	opora	k1gFnSc7	opora
hraběcí	hraběcí	k2eAgFnSc2d1	hraběcí
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
spolu	spolu	k6eAd1	spolu
vedou	vést	k5eAaImIp3nP	vést
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
Třešť	třeštit	k5eAaImRp2nS	třeštit
neustále	neustále	k6eAd1	neustále
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
obžalovává	obžalovávat	k5eAaImIp3nS	obžalovávat
Alessandra	Alessandra	k1gFnSc1	Alessandra
z	z	k7c2	z
rozvratu	rozvrat	k1gInSc2	rozvrat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nemocná	nemocný	k2eAgFnSc1d1	nemocná
dívka	dívka	k1gFnSc1	dívka
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
-	-	kIx~	-
hry	hra	k1gFnSc2	hra
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
v	v	k7c6	v
době	doba	k1gFnSc6	doba
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
avantgardním	avantgardní	k2eAgNnSc7d1	avantgardní
Osvobozeným	osvobozený	k2eAgNnSc7d1	osvobozené
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
maturitou	maturita	k1gFnSc7	maturita
(	(	kIx(	(
<g/>
námět	námět	k1gInSc1	námět
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Juliem	Julius	k1gMnSc7	Julius
Schmittem	Schmitt	k1gMnSc7	Schmitt
režie	režie	k1gFnSc2	režie
se	s	k7c7	s
Svatoplukem	Svatopluk	k1gMnSc7	Svatopluk
Innemannem	Innemann	k1gMnSc7	Innemann
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
námět	námět	k1gInSc1	námět
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Bursa	bursa	k1gFnSc1	bursa
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
námět	námět	k1gInSc1	námět
<g/>
,	,	kIx,	,
sceénář	sceénář	k1gInSc1	sceénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Marijka	Marijka	k1gFnSc1	Marijka
nevěrnice	nevěrnice	k1gFnSc1	nevěrnice
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Kubáskem	Kubásek	k1gMnSc7	Kubásek
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
obé	obé	k1gNnSc1	obé
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Kubáskem	Kubásek	k1gMnSc7	Kubásek
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
Pekař	Pekař	k1gMnSc1	Pekař
Jan	Jan	k1gMnSc1	Jan
Marhoul	Marhoul	k1gInSc1	Marhoul
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Jaromír	Jaromír	k1gMnSc1	Jaromír
Vašta	Vašta	k1gMnSc1	Vašta
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Vášová	Vášová	k1gFnSc1	Vášová
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
Kubula	Kubula	k1gFnSc1	Kubula
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Kubikula	Kubikula	k1gFnSc1	Kubikula
(	(	kIx(	(
<g/>
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Karpaš	Karpaš	k1gMnSc1	Karpaš
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Rozmarné	rozmarný	k2eAgNnSc1d1	Rozmarné
léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Menzl	Menzl	k1gMnSc1	Menzl
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
Magda	Magda	k1gFnSc1	Magda
Vášáryová	Vášáryová	k1gFnSc1	Vášáryová
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Luk	luk	k1gInSc1	luk
královny	královna	k1gFnSc2	královna
Dorotky	Dorotka	k1gFnSc2	Dorotka
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Kubula	Kubula	k1gFnSc1	Kubula
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Kubikula	Kubikula	k1gFnSc1	Kubikula
v	v	k7c6	v
Vařečkách	vařečka	k1gFnPc6	vařečka
a	a	k8xC	a
Hrncích	hrnec	k1gInPc6	hrnec
(	(	kIx(	(
<g/>
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Karpaš	Karpaš	k1gMnSc1	Karpaš
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
starých	starý	k2eAgInPc2d1	starý
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Menzl	Menzl	k1gMnSc1	Menzl
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Jiří	Jiří	k1gMnSc1	Jiří
Abrhám	Abrha	k1gFnPc3	Abrha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Útěk	útěk	k1gInSc1	útěk
do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
(	(	kIx(	(
<g/>
filmový	filmový	k2eAgInSc1d1	filmový
sestřih	sestřih	k1gInSc1	sestřih
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Miloslav	Miloslava	k1gFnPc2	Miloslava
Luther	Luthra	k1gFnPc2	Luthra
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
filmografie	filmografie	k1gFnSc1	filmografie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
televizních	televizní	k2eAgFnPc2d1	televizní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Rozmarné	rozmarný	k2eAgFnSc2d1	rozmarná
létoČas	létoČas	k1gInSc1	létoČas
Kalendář	kalendář	k1gInSc1	kalendář
more	mor	k1gInSc5	mor
Gregoriano	Gregoriana	k1gFnSc5	Gregoriana
rudl	rudnout	k5eAaImAgMnS	rudnout
prvou	prvý	k4xOgFnSc7	prvý
nedělí	neděle	k1gFnSc7	neděle
červnovou	červnový	k2eAgFnSc7d1	červnová
a	a	k8xC	a
zněly	znět	k5eAaImAgInP	znět
veliké	veliký	k2eAgInPc1d1	veliký
zvony	zvon	k1gInPc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
šel	jít	k5eAaImAgInS	jít
vpřed	vpřed	k6eAd1	vpřed
rychlým	rychlý	k2eAgInSc7d1	rychlý
krokem	krok	k1gInSc7	krok
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
bývá	bývat	k5eAaImIp3nS	bývat
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
prázdně	prázdně	k6eAd1	prázdně
a	a	k8xC	a
o	o	k7c6	o
velkých	velký	k2eAgInPc6d1	velký
svátcích	svátek	k1gInPc6	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Blížila	blížit	k5eAaImAgFnS	blížit
se	se	k3xPyFc4	se
hodina	hodina	k1gFnSc1	hodina
osmá	osmý	k4xOgFnSc1	osmý
<g/>
,	,	kIx,	,
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čenich	čenich	k1gInSc1	čenich
smečky	smečka	k1gFnSc2	smečka
denního	denní	k2eAgInSc2d1	denní
času	čas	k1gInSc2	čas
a	a	k8xC	a
že	že	k8xS	že
vás	vy	k3xPp2nPc4	vy
vyslídí	vyslídit	k5eAaPmIp3nS	vyslídit
<g/>
,	,	kIx,	,
stůj	stát	k5eAaImRp2nS	stát
co	co	k8xS	co
stůj	stát	k5eAaImRp2nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
a	a	k8xC	a
poloha	poloha	k1gFnSc1	poloha
ústavu	ústav	k1gInSc2	ústav
Antonína	Antonín	k1gMnSc2	Antonín
Důry	důra	k1gFnSc2	důra
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
zpěvem	zpěv	k1gInSc7	zpěv
a	a	k8xC	a
rozmarnou	rozmarný	k2eAgFnSc7d1	rozmarná
hrou	hra	k1gFnSc7	hra
byl	být	k5eAaImAgInS	být
počat	počat	k2eAgInSc1d1	počat
děj	děj	k1gInSc1	děj
tohoto	tento	k3xDgNnSc2	tento
vypravování	vypravování	k1gNnSc2	vypravování
v	v	k7c6	v
plovoucím	plovoucí	k2eAgInSc6d1	plovoucí
domě	dům	k1gInSc6	dům
Důrově	Důrův	k2eAgInSc6d1	Důrův
<g/>
.	.	kIx.	.
</s>
<s>
Prám	prám	k1gInSc1	prám
Antonínův	Antonínův	k2eAgInSc1d1	Antonínův
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
zbudovány	zbudován	k2eAgFnPc1d1	zbudována
lehké	lehký	k2eAgFnPc1d1	lehká
stavby	stavba	k1gFnPc1	stavba
plovárenské	plovárenský	k2eAgFnPc1d1	Plovárenská
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
upoután	upoután	k2eAgMnSc1d1	upoután
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hřbet	hřbet	k1gInSc1	hřbet
Orše	Orš	k1gFnSc2	Orš
poněkud	poněkud	k6eAd1	poněkud
čeří	čeřit	k5eAaImIp3nS	čeřit
<g/>
,	,	kIx,	,
větře	vítr	k1gInSc5	vítr
písčinu	písčina	k1gFnSc4	písčina
zvící	zvící	k2eAgFnSc4d1	zvící
padesáti	padesát	k4xCc2	padesát
sáhů	sáh	k1gInPc2	sáh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
břeh	břeh	k1gInSc1	břeh
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
městu	město	k1gNnSc3	město
je	být	k5eAaImIp3nS	být
pokryt	pokryt	k2eAgMnSc1d1	pokryt
vrbinami	vrbina	k1gFnPc7	vrbina
<g/>
,	,	kIx,	,
končícími	končící	k2eAgFnPc7d1	končící
se	se	k3xPyFc4	se
před	před	k7c7	před
zahradami	zahrada	k1gFnPc7	zahrada
jirchářů	jirchář	k1gMnPc2	jirchář
a	a	k8xC	a
výrobců	výrobce	k1gMnPc2	výrobce
oplatek	oplatka	k1gFnPc2	oplatka
<g/>
.	.	kIx.	.
</s>
<s>
Zarůstá	zarůstat	k5eAaImIp3nS	zarůstat
příliš	příliš	k6eAd1	příliš
každého	každý	k3xTgNnSc2	každý
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
zachovávaje	zachovávat	k5eAaImSgInS	zachovávat
vzhled	vzhled	k1gInSc4	vzhled
ježatosti	ježatost	k1gFnSc2	ježatost
téměř	téměř	k6eAd1	téměř
neušlechtilé	ušlechtilý	k2eNgFnPc1d1	neušlechtilá
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
jich	on	k3xPp3gNnPc2	on
neštípí	štípit	k5eNaImIp3nS	štípit
<g/>
,	,	kIx,	,
a	a	k8xC	a
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jdou	jít	k5eAaImIp3nP	jít
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
,	,	kIx,	,
nezbývá	zbývat	k5eNaImIp3nS	zbývat
než	než	k8xS	než
maličko	maličko	k6eAd1	maličko
stezek	stezka	k1gFnPc2	stezka
<g/>
,	,	kIx,	,
žel	žel	k9	žel
<g/>
,	,	kIx,	,
úzkých	úzký	k2eAgFnPc2d1	úzká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
cest	cesta	k1gFnPc2	cesta
je	být	k5eAaImIp3nS	být
zaraženo	zarazit	k5eAaPmNgNnS	zarazit
po	po	k7c6	po
tyči	tyč	k1gFnSc6	tyč
zhruba	zhruba	k6eAd1	zhruba
omalované	omalovaný	k2eAgFnSc2d1	omalovaná
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
oslice	oslice	k1gFnSc1	oslice
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
Říční	říční	k2eAgFnPc1d1	říční
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
