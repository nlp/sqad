<s>
Uvnitř	uvnitř	k7c2	uvnitř
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stále	stále	k6eAd1	stále
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobá	podobat	k5eAaImIp3nS	podobat
té	ten	k3xDgFnSc2	ten
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
<g/>
.	.	kIx.	.
</s>
