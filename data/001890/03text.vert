<s>
Mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
je	být	k5eAaImIp3nS	být
membránově	membránově	k6eAd1	membránově
obalená	obalený	k2eAgFnSc1d1	obalená
organela	organela	k1gFnSc1	organela
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
eukaryotických	eukaryotický	k2eAgMnPc2d1	eukaryotický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lidských	lidský	k2eAgFnPc2d1	lidská
<g/>
)	)	kIx)	)
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
i	i	k9	i
několika	několik	k4yIc2	několik
mikrometrů	mikrometr	k1gInPc2	mikrometr
a	a	k8xC	a
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
struktury	struktura	k1gFnPc4	struktura
klobásovitého	klobásovitý	k2eAgInSc2d1	klobásovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
spíše	spíše	k9	spíše
bohatě	bohatě	k6eAd1	bohatě
se	se	k3xPyFc4	se
větvící	větvící	k2eAgFnSc1d1	větvící
síť	síť	k1gFnSc1	síť
vláken	vlákno	k1gNnPc2	vlákno
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
se	se	k3xPyFc4	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
dá	dát	k5eAaPmIp3nS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
buněčné	buněčný	k2eAgFnSc3d1	buněčná
elektrárně	elektrárna	k1gFnSc3	elektrárna
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
díky	díky	k7c3	díky
procesům	proces	k1gInPc3	proces
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
vzniká	vznikat	k5eAaImIp3nS	vznikat
energeticky	energeticky	k6eAd1	energeticky
bohatý	bohatý	k2eAgInSc1d1	bohatý
adenosintrifosfát	adenosintrifosfát	k1gInSc1	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
používaný	používaný	k2eAgInSc4d1	používaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
palivo	palivo	k1gNnSc1	palivo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
průběh	průběh	k1gInSc4	průběh
jiných	jiný	k2eAgFnPc2d1	jiná
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
je	být	k5eAaImIp3nS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
dvěma	dva	k4xCgFnPc7	dva
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
a	a	k8xC	a
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
bariéru	bariéra	k1gFnSc4	bariéra
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
molekuly	molekula	k1gFnPc4	molekula
představuje	představovat	k5eAaImIp3nS	představovat
spíše	spíše	k9	spíše
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
membráně	membrána	k1gFnSc6	membrána
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
také	také	k6eAd1	také
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
ty	ten	k3xDgInPc4	ten
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
metabolické	metabolický	k2eAgInPc4d1	metabolický
pochody	pochod	k1gInPc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
probíhá	probíhat	k5eAaImIp3nS	probíhat
Krebsův	Krebsův	k2eAgInSc1d1	Krebsův
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
beta-oxidace	betaxidace	k1gFnSc1	beta-oxidace
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
buněčná	buněčný	k2eAgFnSc1d1	buněčná
diferenciace	diferenciace	k1gFnSc1	diferenciace
<g/>
,	,	kIx,	,
buněčná	buněčný	k2eAgFnSc1d1	buněčná
smrt	smrt	k1gFnSc1	smrt
i	i	k8xC	i
kontrola	kontrola	k1gFnSc1	kontrola
buněčného	buněčný	k2eAgInSc2d1	buněčný
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
poruchy	poruch	k1gInPc7	poruch
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
za	za	k7c4	za
následek	následek	k1gInSc4	následek
různá	různý	k2eAgNnPc4d1	různé
mitochondriální	mitochondriální	k2eAgNnPc4d1	mitochondriální
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obvykle	obvykle	k6eAd1	obvykle
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
neschopností	neschopnost	k1gFnSc7	neschopnost
správně	správně	k6eAd1	správně
provádět	provádět	k5eAaImF	provádět
metabolické	metabolický	k2eAgInPc4d1	metabolický
mitochondriální	mitochondriální	k2eAgInPc4d1	mitochondriální
pochody	pochod	k1gInPc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mitochondrií	mitochondrie	k1gFnSc7	mitochondrie
je	být	k5eAaImIp3nS	být
činí	činit	k5eAaImIp3nS	činit
unikátními	unikátní	k2eAgInPc7d1	unikátní
z	z	k7c2	z
evolučního	evoluční	k2eAgNnSc2d1	evoluční
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
totiž	totiž	k9	totiž
připomínají	připomínat	k5eAaImIp3nP	připomínat
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
opravdu	opravdu	k6eAd1	opravdu
před	před	k7c7	před
asi	asi	k9	asi
dvěma	dva	k4xCgFnPc7	dva
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
scénář	scénář	k1gInSc1	scénář
však	však	k9	však
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
znám	znát	k5eAaImIp1nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stále	stále	k6eAd1	stále
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobá	podobat	k5eAaImIp3nS	podobat
té	ten	k3xDgFnSc2	ten
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
plastidy	plastid	k1gInPc7	plastid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
semiautonomní	semiautonomní	k2eAgFnPc4d1	semiautonomní
buněčné	buněčný	k2eAgFnPc4d1	buněčná
organely	organela	k1gFnPc4	organela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
redukci	redukce	k1gFnSc3	redukce
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
hydrogenozomy	hydrogenozom	k1gInPc1	hydrogenozom
a	a	k8xC	a
mitozomy	mitozom	k1gInPc1	mitozom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
mikroskopie	mikroskopie	k1gFnPc1	mikroskopie
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
identifikovány	identifikovat	k5eAaBmNgFnP	identifikovat
různé	různý	k2eAgFnPc1d1	různá
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
až	až	k9	až
později	pozdě	k6eAd2	pozdě
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
představují	představovat	k5eAaImIp3nP	představovat
jediný	jediný	k2eAgInSc4d1	jediný
typ	typ	k1gInSc4	typ
organely	organela	k1gFnSc2	organela
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mitochondrii	mitochondrie	k1gFnSc4	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
různě	různě	k6eAd1	různě
vypadající	vypadající	k2eAgFnPc4d1	vypadající
struktury	struktura	k1gFnPc4	struktura
bylo	být	k5eAaImAgNnS	být
používáno	používán	k2eAgNnSc4d1	používáno
nespočetné	spočetný	k2eNgNnSc4d1	nespočetné
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
termínů	termín	k1gInPc2	termín
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
chondriozom	chondriozom	k1gInSc4	chondriozom
<g/>
,	,	kIx,	,
chondrioplast	chondrioplast	k1gInSc4	chondrioplast
<g/>
,	,	kIx,	,
fuchsinofilní	fuchsinofilní	k2eAgNnSc4d1	fuchsinofilní
či	či	k8xC	či
parabazální	parabazální	k2eAgNnSc4d1	parabazální
tělísko	tělísko	k1gNnSc4	tělísko
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
slova	slovo	k1gNnSc2	slovo
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
zrno	zrno	k1gNnSc4	zrno
(	(	kIx(	(
<g/>
χ	χ	k?	χ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
chondros	chondrosa	k1gFnPc2	chondrosa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
anglického	anglický	k2eAgNnSc2d1	anglické
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
grain	grain	k1gInSc1	grain
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
německého	německý	k2eAgMnSc2d1	německý
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Korn	Korn	k1gMnSc1	Korn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zlepšením	zlepšení	k1gNnSc7	zlepšení
pozorovacích	pozorovací	k2eAgFnPc2d1	pozorovací
metod	metoda	k1gFnPc2	metoda
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
mohou	moct	k5eAaImIp3nP	moct
nabývat	nabývat	k5eAaImF	nabývat
i	i	k9	i
vláknitého	vláknitý	k2eAgInSc2d1	vláknitý
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
řecky	řecky	k6eAd1	řecky
μ	μ	k?	μ
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
mitos	mitosa	k1gFnPc2	mitosa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazy	výraz	k1gInPc1	výraz
chondros	chondrosa	k1gFnPc2	chondrosa
a	a	k8xC	a
mitos	mitosa	k1gFnPc2	mitosa
daly	dát	k5eAaPmAgFnP	dát
společně	společně	k6eAd1	společně
vzniknout	vzniknout	k5eAaPmF	vzniknout
složenině	složenina	k1gFnSc3	složenina
<g/>
,	,	kIx,	,
slovu	slovo	k1gNnSc3	slovo
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
B.	B.	kA	B.
F.	F.	kA	F.
Kingsbury	Kingsbura	k1gFnSc2	Kingsbura
a	a	k8xC	a
O.	O.	kA	O.
Warburg	Warburg	k1gMnSc1	Warburg
prokázali	prokázat	k5eAaPmAgMnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mitochondrie	mitochondrie	k1gFnPc4	mitochondrie
jsou	být	k5eAaImIp3nP	být
sídlem	sídlo	k1gNnSc7	sídlo
energetického	energetický	k2eAgInSc2d1	energetický
metabolismu	metabolismus	k1gInSc2	metabolismus
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Detaily	detail	k1gInPc1	detail
složitých	složitý	k2eAgInPc2d1	složitý
procesů	proces	k1gInPc2	proces
odehrávajících	odehrávající	k2eAgFnPc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
však	však	k9	však
byly	být	k5eAaImAgFnP	být
odhalovány	odhalovat	k5eAaImNgFnP	odhalovat
postupně	postupně	k6eAd1	postupně
(	(	kIx(	(
<g/>
Hans	Hans	k1gMnSc1	Hans
Adolf	Adolf	k1gMnSc1	Adolf
Krebs	Krebsa	k1gFnPc2	Krebsa
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
-	-	kIx~	-
Krebsův	Krebsův	k2eAgInSc1d1	Krebsův
cyklus	cyklus	k1gInSc1	cyklus
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
přispěla	přispět	k5eAaPmAgFnS	přispět
zejména	zejména	k9	zejména
postupná	postupný	k2eAgFnSc1d1	postupná
izolace	izolace	k1gFnSc1	izolace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
enzymů	enzym	k1gInPc2	enzym
účastnících	účastnící	k2eAgInPc2d1	účastnící
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
odhalovány	odhalován	k2eAgFnPc1d1	odhalována
podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
obaleny	obalen	k2eAgFnPc1d1	obalena
dvěma	dva	k4xCgFnPc7	dva
membránami	membrána	k1gFnPc7	membrána
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
i	i	k9	i
existence	existence	k1gFnPc1	existence
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
eukaryogeneze	eukaryogeneze	k1gFnSc2	eukaryogeneze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
akceptováno	akceptován	k2eAgNnSc1d1	akceptováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
představují	představovat	k5eAaImIp3nP	představovat
potomky	potomek	k1gMnPc7	potomek
endosymbiotické	endosymbiotický	k2eAgFnSc2d1	endosymbiotická
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
vzniku	vznik	k1gInSc2	vznik
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
transformovala	transformovat	k5eAaBmAgFnS	transformovat
v	v	k7c4	v
semiautonomní	semiautonomní	k2eAgFnSc4d1	semiautonomní
organelu	organela	k1gFnSc4	organela
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
touto	tento	k3xDgFnSc7	tento
bakterií	bakterie	k1gFnSc7	bakterie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
primitivní	primitivní	k2eAgFnSc2d1	primitivní
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zástupce	zástupce	k1gMnSc1	zástupce
alfaproteobakterií	alfaproteobakterie	k1gFnPc2	alfaproteobakterie
z	z	k7c2	z
příbuzenského	příbuzenský	k2eAgInSc2d1	příbuzenský
okruhu	okruh	k1gInSc2	okruh
rodu	rod	k1gInSc2	rod
Rickettsia	Rickettsia	k1gFnSc1	Rickettsia
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názory	názor	k1gInPc1	názor
však	však	k9	však
byly	být	k5eAaImAgFnP	být
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
odmítány	odmítán	k2eAgInPc1d1	odmítán
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nehodící	hodící	k2eNgNnSc1d1	nehodící
se	se	k3xPyFc4	se
do	do	k7c2	do
slušné	slušný	k2eAgFnSc2d1	slušná
přírodovědné	přírodovědný	k2eAgFnSc2d1	přírodovědná
společnosti	společnost	k1gFnSc2	společnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
takovém	takový	k3xDgInSc6	takový
původu	původ	k1gInSc6	původ
se	se	k3xPyFc4	se
však	však	k9	však
paradoxně	paradoxně	k6eAd1	paradoxně
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
nebyl	být	k5eNaImAgMnS	být
ustanoven	ustanoven	k2eAgInSc4d1	ustanoven
termín	termín	k1gInSc4	termín
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Richard	Richard	k1gMnSc1	Richard
Altmann	Altmann	k1gMnSc1	Altmann
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgMnPc7d1	známý
zastánci	zastánce	k1gMnPc7	zastánce
této	tento	k3xDgFnSc6	tento
tzv.	tzv.	kA	tzv.
endosymbiotické	endosymbiotický	k2eAgFnSc2d1	endosymbiotická
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Konstantin	Konstantin	k1gMnSc1	Konstantin
Merežkovskij	Merežkovskij	k1gMnSc1	Merežkovskij
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Lynn	Lynno	k1gNnPc2	Lynno
Margulisová	Margulisový	k2eAgFnSc1d1	Margulisová
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
scénář	scénář	k1gInSc1	scénář
"	"	kIx"	"
<g/>
endosymbiotické	endosymbiotický	k2eAgFnPc1d1	endosymbiotická
události	událost	k1gFnPc1	událost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
fakticky	fakticky	k6eAd1	fakticky
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
mitochondrii	mitochondrie	k1gFnSc4	mitochondrie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
poměrně	poměrně	k6eAd1	poměrně
zahalen	zahalit	k5eAaPmNgInS	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
metoda	metoda	k1gFnSc1	metoda
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
pozorování	pozorování	k1gNnSc6	pozorování
mutací	mutace	k1gFnPc2	mutace
v	v	k7c6	v
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
2	[number]	k4	2
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samotné	samotný	k2eAgFnSc2d1	samotná
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
také	také	k9	také
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
prokázané	prokázaný	k2eAgNnSc4d1	prokázané
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
známé	známý	k2eAgInPc4d1	známý
eukaryotické	eukaryotický	k2eAgInPc4d1	eukaryotický
organizmy	organizmus	k1gInPc4	organizmus
buď	buď	k8xC	buď
mitochondrii	mitochondrie	k1gFnSc4	mitochondrie
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
předka	předek	k1gMnSc4	předek
eukaryota	eukaryot	k1gMnSc4	eukaryot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mitochondrii	mitochondrie	k1gFnSc4	mitochondrie
měl	mít	k5eAaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
od	od	k7c2	od
termínu	termín	k1gInSc2	termín
Archezoa	Archezo	k1gInSc2	Archezo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označoval	označovat	k5eAaImAgInS	označovat
skupiny	skupina	k1gFnPc4	skupina
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
"	"	kIx"	"
<g/>
endosymbiotickou	endosymbiotický	k2eAgFnSc7d1	endosymbiotická
událostí	událost	k1gFnSc7	událost
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
žádné	žádný	k3yNgNnSc1	žádný
takové	takový	k3xDgNnSc1	takový
nebyly	být	k5eNaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jednou	jednou	k6eAd1	jednou
jedinkrát	jedinkrát	k6eAd1	jedinkrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
současné	současný	k2eAgFnPc1d1	současná
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
unikátní	unikátní	k2eAgFnSc2d1	unikátní
události	událost	k1gFnSc2	událost
(	(	kIx(	(
<g/>
mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
monofyletickém	monofyletický	k2eAgInSc6d1	monofyletický
původu	původ	k1gInSc6	původ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
scénář	scénář	k1gInSc1	scénář
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
například	například	k6eAd1	například
známé	známý	k2eAgFnPc1d1	známá
vodíkové	vodíkový	k2eAgFnPc1d1	vodíková
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všechny	všechen	k3xTgInPc1	všechen
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
hostitelskou	hostitelský	k2eAgFnSc7d1	hostitelská
buňkou	buňka	k1gFnSc7	buňka
byla	být	k5eAaImAgFnS	být
anaerobní	anaerobní	k2eAgFnSc1d1	anaerobní
<g/>
,	,	kIx,	,
vodík	vodík	k1gInSc1	vodík
metabolizující	metabolizující	k2eAgInSc1d1	metabolizující
<g/>
,	,	kIx,	,
autotrofní	autotrofní	k2eAgFnSc1d1	autotrofní
archebakterie	archebakterie	k1gFnSc1	archebakterie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
pohltila	pohltit	k5eAaPmAgFnS	pohltit
symbiotickou	symbiotický	k2eAgFnSc4d1	symbiotická
bakterii	bakterie	k1gFnSc4	bakterie
schopnou	schopný	k2eAgFnSc4d1	schopná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
respirace	respirace	k1gFnSc2	respirace
produkovat	produkovat	k5eAaImF	produkovat
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
této	tento	k3xDgFnSc2	tento
bakterie	bakterie	k1gFnSc2	bakterie
následně	následně	k6eAd1	následně
prošli	projít	k5eAaPmAgMnP	projít
evolucí	evoluce	k1gFnPc2	evoluce
a	a	k8xC	a
změnili	změnit	k5eAaPmAgMnP	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
mitochondrie	mitochondrie	k1gFnPc4	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
či	či	k8xC	či
onak	onak	k6eAd1	onak
<g/>
,	,	kIx,	,
po	po	k7c6	po
endosymbiotické	endosymbiotický	k2eAgFnSc6d1	endosymbiotická
události	událost	k1gFnSc6	událost
muselo	muset	k5eAaImAgNnS	muset
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
redukci	redukce	k1gFnSc3	redukce
genomu	genom	k1gInSc2	genom
symbiotické	symbiotický	k2eAgFnSc2d1	symbiotická
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
horizontálnímu	horizontální	k2eAgInSc3d1	horizontální
transferu	transfer	k1gInSc3	transfer
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přechodu	přechod	k1gInSc2	přechod
části	část	k1gFnSc2	část
genů	gen	k1gInPc2	gen
z	z	k7c2	z
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
600	[number]	k4	600
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
mitochondriálních	mitochondriální	k2eAgInPc2d1	mitochondriální
proteinů	protein	k1gInPc2	protein
kódováno	kódován	k2eAgNnSc1d1	kódováno
jadernou	jaderný	k2eAgFnSc7d1	jaderná
DNA	DNA	kA	DNA
a	a	k8xC	a
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
nanejvýš	nanejvýš	k6eAd1	nanejvýš
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
genů	gen	k1gInPc2	gen
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kap	kap	k1gInSc1	kap
<g/>
.	.	kIx.	.
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
všechny	všechen	k3xTgInPc4	všechen
současné	současný	k2eAgInPc4d1	současný
známé	známý	k2eAgInPc4d1	známý
eukaryotické	eukaryotický	k2eAgInPc4d1	eukaryotický
organizmy	organizmus	k1gInPc4	organizmus
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mitochondrii	mitochondrie	k1gFnSc4	mitochondrie
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
u	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
eukaryot	eukaryota	k1gFnPc2	eukaryota
byly	být	k5eAaImAgFnP	být
místo	místo	k7c2	místo
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
popsány	popsat	k5eAaPmNgInP	popsat
jen	jen	k6eAd1	jen
jakési	jakýsi	k3yIgFnPc1	jakýsi
redukované	redukovaný	k2eAgFnPc1d1	redukovaná
organely	organela	k1gFnPc1	organela
neschopné	schopný	k2eNgFnSc2d1	neschopná
oxidativní	oxidativní	k2eAgFnSc2d1	oxidativní
fosforylace	fosforylace	k1gFnSc2	fosforylace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
hydrogenozomy	hydrogenozom	k1gInPc4	hydrogenozom
a	a	k8xC	a
mitozomy	mitozom	k1gInPc4	mitozom
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
schopné	schopný	k2eAgFnPc1d1	schopná
syntézy	syntéza	k1gFnPc1	syntéza
ATP	atp	kA	atp
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
a	a	k8xC	a
hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
do	do	k7c2	do
anaerobního	anaerobní	k2eAgNnSc2d1	anaerobní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogenozomy	Hydrogenozom	k1gInPc1	Hydrogenozom
a	a	k8xC	a
mitozomy	mitozom	k1gInPc1	mitozom
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
zejména	zejména	k9	zejména
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
výhradně	výhradně	k6eAd1	výhradně
<g/>
)	)	kIx)	)
v	v	k7c6	v
říších	říš	k1gFnPc6	říš
Excavata	Excavat	k1gMnSc2	Excavat
a	a	k8xC	a
Amoebozoa	Amoebozous	k1gMnSc2	Amoebozous
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Entamoeba	Entamoeba	k1gFnSc1	Entamoeba
histolytica	histolyticum	k1gNnSc2	histolyticum
<g/>
,	,	kIx,	,
Giardia	Giardium	k1gNnSc2	Giardium
intestinalis	intestinalis	k1gFnSc2	intestinalis
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnSc2d1	různá
mikrosporidie	mikrosporidie	k1gFnSc2	mikrosporidie
(	(	kIx(	(
<g/>
Microsporidia	Microsporidium	k1gNnSc2	Microsporidium
<g/>
)	)	kIx)	)
a	a	k8xC	a
chytridiomycety	chytridiomycet	k1gInPc1	chytridiomycet
(	(	kIx(	(
<g/>
Chytridiomycota	Chytridiomycota	k1gFnSc1	Chytridiomycota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
různí	různý	k2eAgMnPc1d1	různý
anaerobní	anaerobní	k2eAgMnPc1d1	anaerobní
nálevníci	nálevník	k1gMnPc1	nálevník
(	(	kIx(	(
<g/>
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
eukaryot	eukaryota	k1gFnPc2	eukaryota
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
nebyly	být	k5eNaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
ani	ani	k8xC	ani
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
žádné	žádný	k3yNgFnPc4	žádný
jiné	jiný	k2eAgFnPc4d1	jiná
organely	organela	k1gFnPc4	organela
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
velice	velice	k6eAd1	velice
rozmanité	rozmanitý	k2eAgNnSc4d1	rozmanité
prostorové	prostorový	k2eAgNnSc4d1	prostorové
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgInSc4d1	sahající
od	od	k7c2	od
vláknité	vláknitý	k2eAgFnSc2d1	vláknitá
struktury	struktura	k1gFnSc2	struktura
až	až	k9	až
po	po	k7c4	po
kompaktní	kompaktní	k2eAgNnPc4d1	kompaktní
zrna	zrno	k1gNnPc4	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
hepatocytech	hepatocyt	k1gInPc6	hepatocyt
a	a	k8xC	a
fibroblastech	fibroblast	k1gInPc6	fibroblast
mají	mít	k5eAaImIp3nP	mít
klobásovitý	klobásovitý	k2eAgInSc4d1	klobásovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
rozměry	rozměr	k1gInPc7	rozměr
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
μ	μ	k?	μ
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
1	[number]	k4	1
μ	μ	k?	μ
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
také	také	k6eAd1	také
připomínají	připomínat	k5eAaImIp3nP	připomínat
bakterie	bakterie	k1gFnPc1	bakterie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
E.	E.	kA	E.
coli	coli	k6eAd1	coli
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
μ	μ	k?	μ
a	a	k8xC	a
šířky	šířka	k1gFnPc1	šířka
0,6	[number]	k4	0,6
μ	μ	k?	μ
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
spíše	spíše	k9	spíše
síťovité	síťovitý	k2eAgNnSc4d1	síťovitý
retikulum	retikulum	k1gNnSc4	retikulum
mitochondriálních	mitochondriální	k2eAgFnPc2d1	mitochondriální
membrán	membrána	k1gFnPc2	membrána
tvořené	tvořený	k2eAgInPc1d1	tvořený
tenkými	tenký	k2eAgInPc7d1	tenký
větvícími	větvící	k2eAgInPc7d1	větvící
se	s	k7c7	s
trubičkami	trubička	k1gFnPc7	trubička
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
v	v	k7c6	v
hepatocytech	hepatocyt	k1gInPc6	hepatocyt
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
800	[number]	k4	800
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc1d1	lidský
oocyt	oocyt	k1gInSc1	oocyt
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
měňavka	měňavka	k1gFnSc1	měňavka
Chaos	chaos	k1gInSc1	chaos
chaos	chaos	k1gInSc4	chaos
dokonce	dokonce	k9	dokonce
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
lidská	lidský	k2eAgFnSc1d1	lidská
spermie	spermie	k1gFnSc1	spermie
jen	jen	k9	jen
několik	několik	k4yIc4	několik
a	a	k8xC	a
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
nebo	nebo	k8xC	nebo
buňka	buňka	k1gFnSc1	buňka
lidské	lidský	k2eAgFnSc2d1	lidská
sítnice	sítnice	k1gFnSc2	sítnice
jen	jen	k6eAd1	jen
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nS	množit
procesem	proces	k1gInSc7	proces
podobným	podobný	k2eAgInSc7d1	podobný
binárnímu	binární	k2eAgNnSc3d1	binární
dělení	dělení	k1gNnSc3	dělení
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
docházet	docházet	k5eAaImF	docházet
i	i	k8xC	i
opačnému	opačný	k2eAgInSc3d1	opačný
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
fúzi	fúze	k1gFnSc4	fúze
dvou	dva	k4xCgFnPc2	dva
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
biochemické	biochemický	k2eAgFnPc4d1	biochemická
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
615	[number]	k4	615
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
polovina	polovina	k1gFnSc1	polovina
mitochondriálních	mitochondriální	k2eAgInPc2d1	mitochondriální
proteinů	protein	k1gInPc2	protein
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
specifická	specifický	k2eAgFnSc1d1	specifická
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
například	například	k6eAd1	například
primární	primární	k2eAgInPc1d1	primární
plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
oddělena	oddělen	k2eAgFnSc1d1	oddělena
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
dvěma	dva	k4xCgFnPc7	dva
fosfolipidovými	fosfolipidový	k2eAgFnPc7d1	fosfolipidová
membránami	membrána	k1gFnPc7	membrána
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
mezimembránový	mezimembránový	k2eAgInSc4d1	mezimembránový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
membrány	membrána	k1gFnSc2	membrána
gramnegativních	gramnegativní	k2eAgFnPc2d1	gramnegativní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Mezimembránový	Mezimembránový	k2eAgInSc1d1	Mezimembránový
prostor	prostor	k1gInSc1	prostor
má	mít	k5eAaImIp3nS	mít
složení	složení	k1gNnSc4	složení
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
okolního	okolní	k2eAgInSc2d1	okolní
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
než	než	k8xS	než
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
mitochondriální	mitochondriální	k2eAgInSc1d1	mitochondriální
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
matrix	matrix	k1gInSc1	matrix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mezimembránovém	mezimembránový	k2eAgInSc6d1	mezimembránový
prostoru	prostor	k1gInSc6	prostor
zastoupen	zastoupen	k2eAgInSc4d1	zastoupen
hlavně	hlavně	k9	hlavně
cytochrom	cytochrom	k1gInSc4	cytochrom
c	c	k0	c
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
kinázy	kináza	k1gFnPc4	kináza
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
představuje	představovat	k5eAaImIp3nS	představovat
první	první	k4xOgFnSc4	první
bariéru	bariéra	k1gFnSc4	bariéra
mezi	mezi	k7c7	mezi
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
prostředím	prostředí	k1gNnSc7	prostředí
organely	organela	k1gFnSc2	organela
a	a	k8xC	a
okolním	okolní	k2eAgInSc7d1	okolní
cytosolem	cytosol	k1gInSc7	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
endoplazmatickým	endoplazmatický	k2eAgNnSc7d1	endoplazmatické
retikulem	retikulum	k1gNnSc7	retikulum
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podobá	podobat	k5eAaImIp3nS	podobat
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
stavbou	stavba	k1gFnSc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ploše	plocha	k1gFnSc6	plocha
mnoho	mnoho	k4c4	mnoho
pórů	pór	k1gInPc2	pór
(	(	kIx(	(
<g/>
mitochondriální	mitochondriální	k2eAgFnPc4d1	mitochondriální
poriny	porina	k1gFnPc4	porina
VDAC	VDAC	kA	VDAC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
membrána	membrána	k1gFnSc1	membrána
propustná	propustný	k2eAgFnSc1d1	propustná
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
molekulovou	molekulový	k2eAgFnSc7d1	molekulová
hmotností	hmotnost	k1gFnSc7	hmotnost
nepřesahující	přesahující	k2eNgInSc4d1	nepřesahující
přibližně	přibližně	k6eAd1	přibližně
5000	[number]	k4	5000
Da	Da	k1gFnPc2	Da
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
10	[number]	k4	10
000	[number]	k4	000
Da	Da	k1gFnPc2	Da
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
spíše	spíše	k9	spíše
jen	jen	k9	jen
vstupu	vstup	k1gInSc3	vstup
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
především	především	k6eAd1	především
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgInP	být
póry	pór	k1gInPc1	pór
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
unikly	uniknout	k5eAaPmAgInP	uniknout
z	z	k7c2	z
mezimembránového	mezimembránový	k2eAgInSc2d1	mezimembránový
prostoru	prostor	k1gInSc2	prostor
důležité	důležitý	k2eAgInPc1d1	důležitý
proteiny	protein	k1gInPc1	protein
dýchacího	dýchací	k2eAgInSc2d1	dýchací
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvolněním	uvolnění	k1gNnSc7	uvolnění
cytochromu	cytochrom	k1gInSc2	cytochrom
c	c	k0	c
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
programovaná	programovaný	k2eAgFnSc1d1	programovaná
buněčná	buněčný	k2eAgFnSc1d1	buněčná
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
proteiny	protein	k1gInPc7	protein
transportovat	transportovat	k5eAaBmF	transportovat
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tzv.	tzv.	kA	tzv.
Tom	Tom	k1gMnSc1	Tom
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	on	k3xPp3gNnSc4	on
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
do	do	k7c2	do
intermembránového	intermembránový	k2eAgInSc2d1	intermembránový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
membráně	membrána	k1gFnSc6	membrána
nacházejí	nacházet	k5eAaImIp3nP	nacházet
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
metabolismu	metabolismus	k1gInSc2	metabolismus
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
fosfolipidů	fosfolipid	k1gInPc2	fosfolipid
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zvlněna	zvlnit	k5eAaPmNgFnS	zvlnit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
kristy	krista	k1gMnSc2	krista
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jaterních	jaterní	k2eAgFnPc2d1	jaterní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
kristy	krista	k1gFnPc1	krista
ještě	ještě	k9	ještě
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
membrány	membrána	k1gFnSc2	membrána
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
15	[number]	k4	15
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
plocha	plocha	k1gFnSc1	plocha
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
membrány	membrána	k1gFnSc2	membrána
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
závisí	záviset	k5eAaImIp3nS	záviset
velikost	velikost	k1gFnSc1	velikost
krist	krist	k5eAaPmF	krist
na	na	k7c6	na
metabolické	metabolický	k2eAgFnSc6d1	metabolická
aktivitě	aktivita	k1gFnSc6	aktivita
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
funkčnosti	funkčnost	k1gFnSc2	funkčnost
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
propouštět	propouštět	k5eAaImF	propouštět
molekuly	molekula	k1gFnPc4	molekula
jen	jen	k9	jen
velice	velice	k6eAd1	velice
selektivně	selektivně	k6eAd1	selektivně
<g/>
,	,	kIx,	,
ionty	ion	k1gInPc4	ion
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
téměř	téměř	k6eAd1	téměř
nedifundují	difundovat	k5eNaImIp3nP	difundovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
díky	díky	k7c3	díky
speciální	speciální	k2eAgFnSc3d1	speciální
stavbě	stavba	k1gFnSc3	stavba
fosfolipidové	fosfolipidový	k2eAgFnSc2d1	fosfolipidová
dvouvrstvy	dvouvrstvo	k1gNnPc7	dvouvrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
totiž	totiž	k9	totiž
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
fosfolipid	fosfolipid	k1gInSc1	fosfolipid
kardiolipin	kardiolipina	k1gFnPc2	kardiolipina
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
fosfátové	fosfátový	k2eAgFnSc2d1	fosfátová
hlavy	hlava	k1gFnSc2	hlava
vychází	vycházet	k5eAaImIp3nS	vycházet
ne	ne	k9	ne
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
čtyři	čtyři	k4xCgFnPc1	čtyři
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
membráně	membrána	k1gFnSc6	membrána
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
umístěno	umístěn	k2eAgNnSc1d1	umístěno
množství	množství	k1gNnSc1	množství
enzymů	enzym	k1gInPc2	enzym
tzv.	tzv.	kA	tzv.
dýchacího	dýchací	k2eAgInSc2d1	dýchací
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ATP	atp	kA	atp
syntázy	syntáza	k1gFnSc2	syntáza
a	a	k8xC	a
enzymu	enzym	k1gInSc2	enzym
ANT	Ant	k1gMnSc1	Ant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vynáší	vynášet	k5eAaImIp3nS	vynášet
konečný	konečný	k2eAgInSc1d1	konečný
produkt	produkt	k1gInSc1	produkt
dýchání	dýchání	k1gNnSc2	dýchání
-	-	kIx~	-
adenosintrifosfát	adenosintrifosfát	k1gInSc1	adenosintrifosfát
-	-	kIx~	-
ven	ven	k6eAd1	ven
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ta	ten	k3xDgFnSc1	ten
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc4	přenos
bílkovin	bílkovina	k1gFnPc2	bílkovina
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
-	-	kIx~	-
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Tim	Tim	k?	Tim
komplex	komplex	k1gInSc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
matrix	matrix	k1gInSc1	matrix
<g/>
,	,	kIx,	,
hustá	hustý	k2eAgFnSc1d1	hustá
hmota	hmota	k1gFnSc1	hmota
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vody	voda	k1gFnSc2	voda
méně	málo	k6eAd2	málo
než	než	k8xS	než
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
zejména	zejména	k9	zejména
enzymy	enzym	k1gInPc1	enzym
Krebsova	Krebsův	k2eAgInSc2d1	Krebsův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
matrix	matrix	k1gInSc1	matrix
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
různé	různý	k2eAgInPc4d1	různý
nukleotidové	nukleotidový	k2eAgInPc4d1	nukleotidový
koenzymy	koenzym	k1gInPc4	koenzym
<g/>
,	,	kIx,	,
anorganické	anorganický	k2eAgInPc4d1	anorganický
ionty	ion	k1gInPc4	ion
(	(	kIx(	(
<g/>
vápník	vápník	k1gInSc4	vápník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
struktur	struktura	k1gFnPc2	struktura
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
RNA	RNA	kA	RNA
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
mitochondriální	mitochondriální	k2eAgInPc4d1	mitochondriální
ribozomy	ribozom	k1gInPc4	ribozom
(	(	kIx(	(
<g/>
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
endosymbiotickou	endosymbiotický	k2eAgFnSc7d1	endosymbiotická
teorií	teorie	k1gFnSc7	teorie
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
ribozomům	ribozom	k1gInPc3	ribozom
bakteriálním	bakteriální	k2eAgInPc3d1	bakteriální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgMnPc4d1	schopný
vyrábět	vyrábět	k5eAaImF	vyrábět
si	se	k3xPyFc3	se
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
mitochondriálních	mitochondriální	k2eAgFnPc2d1	mitochondriální
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vytvářena	vytvářit	k5eAaPmNgFnS	vytvářit
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
(	(	kIx(	(
<g/>
z	z	k7c2	z
jaderných	jaderný	k2eAgInPc2d1	jaderný
genů	gen	k1gInPc2	gen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
proteiny	protein	k1gInPc1	protein
jsou	být	k5eAaImIp3nP	být
vzápětí	vzápětí	k6eAd1	vzápětí
přenášeny	přenášet	k5eAaImNgInP	přenášet
skrz	skrz	k7c4	skrz
jednu	jeden	k4xCgFnSc4	jeden
či	či	k8xC	či
obě	dva	k4xCgFnPc4	dva
mitochondriální	mitochondriální	k2eAgFnPc4d1	mitochondriální
membrány	membrána	k1gFnPc4	membrána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
skončily	skončit	k5eAaPmAgInP	skončit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgNnSc2	svůj
finálního	finální	k2eAgNnSc2d1	finální
určení	určení	k1gNnSc2	určení
<g/>
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
do	do	k7c2	do
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
molekulární	molekulární	k2eAgInSc1d1	molekulární
proces	proces	k1gInSc1	proces
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
transportních	transportní	k2eAgInPc2d1	transportní
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgInSc1d1	mitochondriální
protein	protein	k1gInSc1	protein
je	být	k5eAaImIp3nS	být
rozeznán	rozeznat	k5eAaPmNgInS	rozeznat
podle	podle	k7c2	podle
speciální	speciální	k2eAgFnSc2d1	speciální
aminokyselinové	aminokyselinový	k2eAgFnSc2d1	aminokyselinová
signální	signální	k2eAgFnSc2d1	signální
sekvence	sekvence	k1gFnSc2	sekvence
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
bazické	bazický	k2eAgFnPc4d1	bazická
<g/>
,	,	kIx,	,
hydroxylované	hydroxylovaný	k2eAgFnPc4d1	hydroxylovaný
a	a	k8xC	a
hydrofobní	hydrofobní	k2eAgFnPc4d1	hydrofobní
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sekvence	sekvence	k1gFnSc1	sekvence
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
N-konci	Nonec	k1gInSc6	N-konec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
transportu	transport	k1gInSc2	transport
z	z	k7c2	z
bílkoviny	bílkovina	k1gFnSc2	bílkovina
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
se	se	k3xPyFc4	se
mitochondriální	mitochondriální	k2eAgFnPc1d1	mitochondriální
bílkoviny	bílkovina	k1gFnPc1	bílkovina
obvykle	obvykle	k6eAd1	obvykle
navážou	navázat	k5eAaPmIp3nP	navázat
na	na	k7c4	na
chaperony	chaperon	k1gInPc4	chaperon
typu	typ	k1gInSc2	typ
Hsp	Hsp	k1gFnSc2	Hsp
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
nesbaleném	sbalený	k2eNgInSc6d1	sbalený
stavu	stav	k1gInSc6	stav
-	-	kIx~	-
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
transport	transport	k1gInSc4	transport
bílkovin	bílkovina	k1gFnPc2	bílkovina
skrz	skrz	k7c4	skrz
kanály	kanál	k1gInPc4	kanál
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
překážkou	překážka	k1gFnSc7	překážka
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
Tom	Tom	k1gMnSc1	Tom
kanály	kanál	k1gInPc7	kanál
Tom	Tom	k1gMnSc1	Tom
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
mitochondriální	mitochondriální	k2eAgFnPc1d1	mitochondriální
bílkoviny	bílkovina	k1gFnPc1	bílkovina
se	se	k3xPyFc4	se
začlení	začlenit	k5eAaPmIp3nP	začlenit
do	do	k7c2	do
vnější	vnější	k2eAgFnSc2d1	vnější
membrány	membrána	k1gFnSc2	membrána
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
namátkou	namátkou	k6eAd1	namátkou
právě	právě	k9	právě
Tom	Tom	k1gMnSc1	Tom
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
mezimembránovém	mezimembránový	k2eAgInSc6d1	mezimembránový
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
svou	svůj	k3xOyFgFnSc4	svůj
pouť	pouť	k1gFnSc4	pouť
skrz	skrz	k7c4	skrz
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Proteiny	protein	k1gInPc1	protein
vstupující	vstupující	k2eAgInPc1d1	vstupující
do	do	k7c2	do
matrix	matrix	k1gInSc1	matrix
využívají	využívat	k5eAaImIp3nP	využívat
komplex	komplex	k1gInSc4	komplex
Tim	Tim	k?	Tim
<g/>
23	[number]	k4	23
(	(	kIx(	(
<g/>
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
Tim	Tim	k?	Tim
<g/>
23	[number]	k4	23
a	a	k8xC	a
Tim	Tim	k?	Tim
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
začlenit	začlenit	k5eAaPmF	začlenit
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
zůstat	zůstat	k5eAaPmF	zůstat
tam	tam	k6eAd1	tam
jako	jako	k8xS	jako
transmembránové	transmembránové	k2eAgMnPc1d1	transmembránové
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
komplex	komplex	k1gInSc4	komplex
Tim	Tim	k?	Tim
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
probíhá	probíhat	k5eAaImIp3nS	probíhat
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
zejména	zejména	k9	zejména
glykolýzy	glykolýza	k1gFnSc2	glykolýza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sled	sled	k1gInSc1	sled
reakcí	reakce	k1gFnSc7	reakce
představuje	představovat	k5eAaImIp3nS	představovat
rozklad	rozklad	k1gInSc1	rozklad
různých	různý	k2eAgFnPc2d1	různá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
adenosintrifosfátu	adenosintrifosfát	k1gInSc2	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
z	z	k7c2	z
ADP	ADP	kA	ADP
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgFnSc7d1	výchozí
látkou	látka	k1gFnSc7	látka
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
pyruvát	pyruvát	k5eAaPmF	pyruvát
a	a	k8xC	a
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
látky	látka	k1gFnPc1	látka
v	v	k7c4	v
mitochondriální	mitochondriální	k2eAgInSc4d1	mitochondriální
matrix	matrix	k1gInSc4	matrix
prochází	procházet	k5eAaImIp3nS	procházet
reakcemi	reakce	k1gFnPc7	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
acetylkoenzym	acetylkoenzym	k1gInSc1	acetylkoenzym
A	A	kA	A
(	(	kIx(	(
<g/>
pyruvát	pyruvát	k1gMnSc1	pyruvát
prochází	procházet	k5eAaImIp3nS	procházet
dekarboxylací	dekarboxylace	k1gFnPc2	dekarboxylace
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
beta-oxidací	betaxidací	k2eAgFnPc1d1	beta-oxidací
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Acetylkoenzym	Acetylkoenzym	k1gInSc1	Acetylkoenzym
A	a	k9	a
následně	následně	k6eAd1	následně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Krebsova	Krebsův	k2eAgInSc2d1	Krebsův
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
redukci	redukce	k1gFnSc4	redukce
koenzymů	koenzym	k1gInPc2	koenzym
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
na	na	k7c6	na
NADH	NADH	kA	NADH
a	a	k8xC	a
FAD	FAD	kA	FAD
na	na	k7c4	na
FADH	FADH	kA	FADH
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
koenzymů	koenzym	k1gInPc2	koenzym
jsou	být	k5eAaImIp3nP	být
předávány	předávat	k5eAaImNgFnP	předávat
do	do	k7c2	do
dýchacího	dýchací	k2eAgInSc2d1	dýchací
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
membráně	membrána	k1gFnSc6	membrána
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInSc1d1	dýchací
řetězec	řetězec	k1gInSc1	řetězec
přenáší	přenášet	k5eAaImIp3nS	přenášet
do	do	k7c2	do
mezimembránového	mezimembránový	k2eAgInSc2d1	mezimembránový
prostoru	prostor	k1gInSc2	prostor
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
kationty	kation	k1gInPc1	kation
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
membránami	membrána	k1gFnPc7	membrána
kyselé	kyselý	k2eAgFnSc2d1	kyselá
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
pH	ph	kA	ph
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
prochází	procházet	k5eAaImIp3nS	procházet
otvorem	otvor	k1gInSc7	otvor
v	v	k7c6	v
enzymu	enzym	k1gInSc6	enzym
ATP	atp	kA	atp
syntáze	syntáze	k1gFnSc2	syntáze
zpět	zpět	k6eAd1	zpět
dovnitř	dovnitř	k6eAd1	dovnitř
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Průchodem	průchod	k1gInSc7	průchod
H	H	kA	H
<g/>
+	+	kIx~	+
však	však	k9	však
tento	tento	k3xDgInSc4	tento
enzym	enzym	k1gInSc4	enzym
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
kýženým	kýžený	k2eAgInSc7d1	kýžený
produktem	produkt	k1gInSc7	produkt
celého	celý	k2eAgInSc2d1	celý
sledu	sled	k1gInSc2	sled
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
známá	známý	k2eAgFnSc1d1	známá
převážně	převážně	k6eAd1	převážně
jako	jako	k8xC	jako
energetická	energetický	k2eAgFnSc1d1	energetická
jednotka	jednotka	k1gFnSc1	jednotka
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiřazeny	přiřazen	k2eAgFnPc4d1	přiřazena
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
důležité	důležitý	k2eAgFnPc4d1	důležitá
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
buněčným	buněčný	k2eAgNnSc7d1	buněčné
dýcháním	dýchání	k1gNnSc7	dýchání
souvisí	souviset	k5eAaImIp3nS	souviset
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
v	v	k7c6	v
hnědé	hnědý	k2eAgFnSc6d1	hnědá
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
schopné	schopný	k2eAgFnPc1d1	schopná
produkovat	produkovat	k5eAaImF	produkovat
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
je	být	k5eAaImIp3nS	být
elegantní	elegantní	k2eAgInSc1d1	elegantní
<g/>
:	:	kIx,	:
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
kationty	kation	k1gInPc1	kation
procházely	procházet	k5eAaImAgInP	procházet
ATP	atp	kA	atp
syntázou	syntáza	k1gFnSc7	syntáza
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
protonovým	protonový	k2eAgInSc7d1	protonový
kanálem	kanál	k1gInSc7	kanál
termogeninem	termogenin	k1gInSc7	termogenin
(	(	kIx(	(
<g/>
UCP	UCP	kA	UCP
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
může	moct	k5eAaImIp3nS	moct
matrix	matrix	k1gInSc1	matrix
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
vápníku	vápník	k1gInSc2	vápník
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
udržují	udržovat	k5eAaImIp3nP	udržovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
homeostázu	homeostáza	k1gFnSc4	homeostáza
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Dovnitř	dovnitř	k6eAd1	dovnitř
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
ionty	ion	k1gInPc1	ion
vápníku	vápník	k1gInSc2	vápník
speciálním	speciální	k2eAgInSc7d1	speciální
přenašečem	přenašeč	k1gInSc7	přenašeč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
membránovým	membránový	k2eAgInSc7d1	membránový
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
,	,	kIx,	,
při	při	k7c6	při
masivním	masivní	k2eAgNnSc6d1	masivní
uvolňování	uvolňování	k1gNnSc6	uvolňování
vápníku	vápník	k1gInSc2	vápník
ven	ven	k6eAd1	ven
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
navodit	navodit	k5eAaPmF	navodit
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
určitá	určitý	k2eAgFnSc1d1	určitá
odpověď	odpověď	k1gFnSc1	odpověď
(	(	kIx(	(
<g/>
produkce	produkce	k1gFnSc1	produkce
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mitochondrii	mitochondrie	k1gFnSc6	mitochondrie
přítomna	přítomen	k2eAgFnSc1d1	přítomna
řada	řada	k1gFnSc1	řada
proteinů	protein	k1gInPc2	protein
spouštějících	spouštějící	k2eAgInPc2d1	spouštějící
apoptózu	apoptóza	k1gFnSc4	apoptóza
<g/>
,	,	kIx,	,
programovanou	programovaný	k2eAgFnSc4d1	programovaná
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
smrt	smrt	k1gFnSc4	smrt
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
regulace	regulace	k1gFnSc2	regulace
buněčné	buněčný	k2eAgFnSc2d1	buněčná
proliferace	proliferace	k1gFnSc2	proliferace
a	a	k8xC	a
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
,	,	kIx,	,
probíhají	probíhat	k5eAaImIp3nP	probíhat
však	však	k9	však
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
syntézy	syntéza	k1gFnSc2	syntéza
hemu	hem	k1gInSc2	hem
a	a	k8xC	a
steroidů	steroid	k1gInPc2	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
)	)	kIx)	)
produkovány	produkován	k2eAgInPc1d1	produkován
kyslíkové	kyslíkový	k2eAgInPc1d1	kyslíkový
radikály	radikál	k1gInPc1	radikál
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
četné	četný	k2eAgFnPc1d1	četná
mutace	mutace	k1gFnPc1	mutace
v	v	k7c4	v
mitochondriální	mitochondriální	k2eAgNnPc4d1	mitochondriální
DNA	dno	k1gNnPc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgNnPc4	tento
DNA	dno	k1gNnPc4	dno
poškozována	poškozován	k2eAgNnPc4d1	poškozováno
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
defektnější	defektní	k2eAgFnPc1d2	defektní
molekuly	molekula	k1gFnPc4	molekula
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
spíš	spíš	k9	spíš
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
degenerativní	degenerativní	k2eAgFnPc1d1	degenerativní
změny	změna	k1gFnPc1	změna
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
během	během	k7c2	během
lidského	lidský	k2eAgNnSc2d1	lidské
stárnutí	stárnutí	k1gNnSc2	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
změny	změna	k1gFnPc1	změna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dávány	dávat	k5eAaImNgInP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
Parkinsonovou	Parkinsonový	k2eAgFnSc7d1	Parkinsonová
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
(	(	kIx(	(
<g/>
též	též	k9	též
mtDNA	mtDNA	k?	mtDNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k9	tak
součást	součást	k1gFnSc4	součást
mimojaderné	mimojaderný	k2eAgFnSc2d1	mimojaderná
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
genomu	genom	k1gInSc2	genom
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
zpravidla	zpravidla	k6eAd1	zpravidla
kruhová	kruhový	k2eAgFnSc1d1	kruhová
(	(	kIx(	(
<g/>
cirkulární	cirkulární	k2eAgFnSc1d1	cirkulární
<g/>
)	)	kIx)	)
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
prokaryontnímu	prokaryontní	k2eAgInSc3d1	prokaryontní
nukleoidu	nukleoid	k1gInSc3	nukleoid
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
eukaryotickým	eukaryotický	k2eAgInPc3d1	eukaryotický
chromozomům	chromozom	k1gInPc3	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jaderným	jaderný	k2eAgInSc7d1	jaderný
genomem	genom	k1gInSc7	genom
velice	velice	k6eAd1	velice
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lidská	lidský	k2eAgFnSc1d1	lidská
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
16	[number]	k4	16
569	[number]	k4	569
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
37	[number]	k4	37
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
24	[number]	k4	24
představují	představovat	k5eAaImIp3nP	představovat
geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
různou	různý	k2eAgFnSc4d1	různá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
2	[number]	k4	2
geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
16S	[number]	k4	16S
a	a	k8xC	a
23S	[number]	k4	23S
rRNA	rRNA	k?	rRNA
a	a	k8xC	a
22	[number]	k4	22
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgInPc2d1	zbývající
13	[number]	k4	13
genů	gen	k1gInPc2	gen
kóduje	kódovat	k5eAaBmIp3nS	kódovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
mitochondriální	mitochondriální	k2eAgInPc4d1	mitochondriální
polypeptidy	polypeptid	k1gInPc4	polypeptid
podílející	podílející	k2eAgInPc4d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
enzymatické	enzymatický	k2eAgFnSc6d1	enzymatická
výbavě	výbava	k1gFnSc6	výbava
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mtDNA	mtDNA	k?	mtDNA
v	v	k7c6	v
mitochondrii	mitochondrie	k1gFnSc6	mitochondrie
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
několika	několik	k4yIc6	několik
kopiích	kopie	k1gFnPc6	kopie
a	a	k8xC	a
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mitochondrií	mitochondrie	k1gFnSc7	mitochondrie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
i	i	k9	i
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
paradoxně	paradoxně	k6eAd1	paradoxně
představovat	představovat	k5eAaImF	představovat
genom	genom	k1gInSc4	genom
v	v	k7c6	v
mitochondrii	mitochondrie	k1gFnSc6	mitochondrie
většinovou	většinový	k2eAgFnSc4d1	většinová
složku	složka	k1gFnSc4	složka
celkového	celkový	k2eAgInSc2d1	celkový
buněčného	buněčný	k2eAgInSc2d1	buněčný
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nS	dědit
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
maternálně	maternálně	k6eAd1	maternálně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
ze	z	k7c2	z
spermie	spermie	k1gFnSc2	spermie
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
obvykle	obvykle	k6eAd1	obvykle
veškerá	veškerý	k3xTgFnSc1	veškerý
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
zárodku	zárodek	k1gInSc2	zárodek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
mtDNA	mtDNA	k?	mtDNA
velice	velice	k6eAd1	velice
cenným	cenný	k2eAgInSc7d1	cenný
nástrojem	nástroj	k1gInSc7	nástroj
genetiků	genetik	k1gMnPc2	genetik
a	a	k8xC	a
molekulárních	molekulární	k2eAgMnPc2d1	molekulární
biologů	biolog	k1gMnPc2	biolog
-	-	kIx~	-
geny	gen	k1gInPc1	gen
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
nerekombinují	rekombinovat	k5eNaImIp3nP	rekombinovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mtDNA	mtDNA	k?	mtDNA
mutuje	mutovat	k5eAaImIp3nS	mutovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
jaderný	jaderný	k2eAgInSc4d1	jaderný
genom	genom	k1gInSc4	genom
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zkoumat	zkoumat	k5eAaImF	zkoumat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
kratším	krátký	k2eAgNnSc6d2	kratší
časovém	časový	k2eAgNnSc6d1	časové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mtDNA	mtDNA	k?	mtDNA
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zkoumat	zkoumat	k5eAaImF	zkoumat
například	například	k6eAd1	například
migrace	migrace	k1gFnSc1	migrace
lidských	lidský	k2eAgFnPc2d1	lidská
populací	populace	k1gFnPc2	populace
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
termín	termín	k1gInSc1	termín
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
Eva	Eva	k1gFnSc1	Eva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
mitochondriální	mitochondriální	k2eAgNnSc1d1	mitochondriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondriální	mitochondriální	k2eAgNnSc1d1	mitochondriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
takové	takový	k3xDgNnSc4	takový
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
způsobené	způsobený	k2eAgNnSc1d1	způsobené
poruchou	porucha	k1gFnSc7	porucha
funkce	funkce	k1gFnSc2	funkce
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
významu	význam	k1gInSc6	význam
jak	jak	k6eAd1	jak
vrozené	vrozený	k2eAgNnSc1d1	vrozené
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
získané	získaný	k2eAgInPc1d1	získaný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrozené	vrozený	k2eAgInPc4d1	vrozený
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
onemocnění	onemocnění	k1gNnSc3	onemocnění
způsobené	způsobený	k2eAgFnPc1d1	způsobená
mutacemi	mutace	k1gFnPc7	mutace
v	v	k7c6	v
genech	gen	k1gInPc6	gen
v	v	k7c4	v
jaderné	jaderný	k2eAgFnPc4d1	jaderná
DNA	DNA	kA	DNA
či	či	k8xC	či
v	v	k7c4	v
mitochondriální	mitochondriální	k2eAgNnPc4d1	mitochondriální
DNA	dno	k1gNnPc4	dno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
získané	získaný	k2eAgInPc1d1	získaný
poruchy	poruch	k1gInPc1	poruch
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výsledkem	výsledek	k1gInSc7	výsledek
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc1	užívání
léků	lék	k1gInPc2	lék
či	či	k8xC	či
vlivem	vliv	k1gInSc7	vliv
negativních	negativní	k2eAgFnPc2d1	negativní
podmínek	podmínka	k1gFnPc2	podmínka
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
onemocnění	onemocnění	k1gNnSc4	onemocnění
způsobená	způsobený	k2eAgFnSc1d1	způsobená
poruchami	porucha	k1gFnPc7	porucha
v	v	k7c6	v
mitochondriálním	mitochondriální	k2eAgInSc6d1	mitochondriální
metabolismu	metabolismus	k1gInSc6	metabolismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
poruchy	porucha	k1gFnPc1	porucha
oxidativní	oxidativní	k2eAgFnSc2d1	oxidativní
fosforylace	fosforylace	k1gFnSc2	fosforylace
<g/>
.	.	kIx.	.
</s>
