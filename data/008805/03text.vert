<p>
<s>
Recitace	recitace	k1gFnSc1	recitace
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
ústního	ústní	k2eAgNnSc2d1	ústní
přednášení	přednášení	k1gNnSc2	přednášení
či	či	k8xC	či
přednesu	přednes	k1gInSc2	přednes
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přednesu	přednes	k1gInSc6	přednes
uměleckém	umělecký	k2eAgInSc6d1	umělecký
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
recitace	recitace	k1gFnSc2	recitace
výstižně	výstižně	k6eAd1	výstižně
a	a	k8xC	a
účinně	účinně	k6eAd1	účinně
tlumočit	tlumočit	k5eAaImF	tlumočit
autorovy	autorův	k2eAgFnPc4d1	autorova
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mluvenou	mluvený	k2eAgFnSc7d1	mluvená
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recitace	recitace	k1gFnSc1	recitace
veršů	verš	k1gInPc2	verš
==	==	k?	==
</s>
</p>
<p>
<s>
Recitace	recitace	k1gFnSc1	recitace
veršů	verš	k1gInPc2	verš
je	být	k5eAaImIp3nS	být
realizace	realizace	k1gFnSc1	realizace
básnického	básnický	k2eAgInSc2d1	básnický
textu	text	k1gInSc2	text
s	s	k7c7	s
individuálním	individuální	k2eAgInSc7d1	individuální
tvůrčím	tvůrčí	k2eAgInSc7d1	tvůrčí
podílem	podíl	k1gInSc7	podíl
interpreta	interpret	k1gMnSc2	interpret
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recitátor	recitátor	k1gMnSc1	recitátor
==	==	k?	==
</s>
</p>
<p>
<s>
Recitátor	recitátor	k1gMnSc1	recitátor
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
recituje	recitovat	k5eAaImIp3nS	recitovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
provádí	provádět	k5eAaImIp3nS	provádět
recitaci	recitace	k1gFnSc4	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vnímatelem	vnímatel	k1gMnSc7	vnímatel
<g/>
,	,	kIx,	,
recituje	recitovat	k5eAaImIp3nS	recitovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
reprodukčním	reprodukční	k2eAgMnSc7d1	reprodukční
umělcem	umělec	k1gMnSc7	umělec
realizujícím	realizující	k2eAgInSc7d1	realizující
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
recitátora	recitátor	k1gMnSc2	recitátor
==	==	k?	==
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
recitátora	recitátor	k1gMnSc4	recitátor
lze	lze	k6eAd1	lze
hodnotit	hodnotit	k5eAaImF	hodnotit
zejména	zejména	k9	zejména
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hlasový	hlasový	k2eAgInSc1d1	hlasový
výkon	výkon	k1gInSc1	výkon
</s>
</p>
<p>
<s>
dech	dech	k1gInSc1	dech
</s>
</p>
<p>
<s>
výslovnost	výslovnost	k1gFnSc1	výslovnost
</s>
</p>
<p>
<s>
tempo	tempo	k1gNnSc1	tempo
přednesu	přednes	k1gInSc2	přednes
</s>
</p>
<p>
<s>
optická	optický	k2eAgFnSc1d1	optická
opora	opora	k1gFnSc1	opora
řeči	řeč	k1gFnSc2	řeč
–	–	k?	–
mimika	mimika	k1gFnSc1	mimika
a	a	k8xC	a
gestikulace	gestikulace	k1gFnSc1	gestikulace
</s>
</p>
<p>
<s>
chování	chování	k1gNnSc1	chování
</s>
</p>
<p>
<s>
způsob	způsob	k1gInSc1	způsob
přednesu	přednést	k5eAaPmIp1nS	přednést
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
recitace	recitace	k1gFnSc2	recitace
</s>
</p>
<p>
<s>
tematika	tematika	k1gFnSc1	tematika
</s>
</p>
<p>
<s>
==	==	k?	==
Důvody	důvod	k1gInPc1	důvod
poslechu	poslech	k1gInSc2	poslech
recitace	recitace	k1gFnSc2	recitace
==	==	k?	==
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
rádo	rád	k2eAgNnSc4d1	rádo
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
recitování	recitování	k1gNnSc4	recitování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
recitaci	recitace	k1gFnSc6	recitace
vynikne	vyniknout	k5eAaPmIp3nS	vyniknout
modulování	modulování	k1gNnSc1	modulování
literárního	literární	k2eAgInSc2d1	literární
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
správně	správně	k6eAd1	správně
dynamické	dynamický	k2eAgFnPc4d1	dynamická
a	a	k8xC	a
intonační	intonační	k2eAgFnPc4d1	intonační
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
čte	číst	k5eAaImIp3nS	číst
libovolný	libovolný	k2eAgInSc4d1	libovolný
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
potichu	potichu	k6eAd1	potichu
či	či	k8xC	či
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmínky	podmínka	k1gFnPc4	podmínka
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
recitace	recitace	k1gFnSc2	recitace
==	==	k?	==
</s>
</p>
<p>
<s>
Recitátor	recitátor	k1gMnSc1	recitátor
mluví	mluvit	k5eAaImIp3nS	mluvit
k	k	k7c3	k
posluchačům	posluchač	k1gMnPc3	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
splněny	splnit	k5eAaPmNgFnP	splnit
určité	určitý	k2eAgFnPc1d1	určitá
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
recitovaný	recitovaný	k2eAgInSc1d1	recitovaný
text	text	k1gInSc1	text
správně	správně	k6eAd1	správně
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
recitátora	recitátor	k1gMnSc2	recitátor
k	k	k7c3	k
posluchači	posluchač	k1gMnSc3	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
podmínky	podmínka	k1gFnPc4	podmínka
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
k	k	k7c3	k
recitaci	recitace	k1gFnSc3	recitace
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
slyšitelnost	slyšitelnost	k1gFnSc1	slyšitelnost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
recitátor	recitátor	k1gMnSc1	recitátor
zvolil	zvolit	k5eAaPmAgMnS	zvolit
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
hlasitost	hlasitost	k1gFnSc4	hlasitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nezávisí	záviset	k5eNaImIp3nS	záviset
jen	jen	k9	jen
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
důraznosti	důraznost	k1gFnSc2	důraznost
hlasu	hlas	k1gInSc2	hlas
recitátora	recitátor	k1gMnSc2	recitátor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vše	všechen	k3xTgNnSc1	všechen
velmi	velmi	k6eAd1	velmi
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
recituje	recitovat	k5eAaImIp3nS	recitovat
<g/>
,	,	kIx,	,
ozvučený	ozvučený	k2eAgMnSc1d1	ozvučený
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
lámání	lámání	k1gNnSc3	lámání
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
má	mít	k5eAaImIp3nS	mít
recitátor	recitátor	k1gMnSc1	recitátor
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
slyšitelností	slyšitelnost	k1gFnSc7	slyšitelnost
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
podmínky	podmínka	k1gFnPc1	podmínka
lze	lze	k6eAd1	lze
splnit	splnit	k5eAaPmF	splnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
recitátor	recitátor	k1gMnSc1	recitátor
dostatečně	dostatečně	k6eAd1	dostatečně
výkonný	výkonný	k2eAgInSc4d1	výkonný
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
přesnou	přesný	k2eAgFnSc4d1	přesná
výslovnost	výslovnost	k1gFnSc4	výslovnost
a	a	k8xC	a
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
recitovaný	recitovaný	k2eAgInSc4d1	recitovaný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Neopomenutelnou	opomenutelný	k2eNgFnSc7d1	neopomenutelná
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
recitátorovo	recitátorův	k2eAgNnSc4d1	recitátorův
vystupování	vystupování	k1gNnSc4	vystupování
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vizuální	vizuální	k2eAgFnSc3d1	vizuální
stránce	stránka	k1gFnSc3	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Lepšího	dobrý	k2eAgInSc2d2	lepší
přednesu	přednes	k1gInSc2	přednes
(	(	kIx(	(
<g/>
výsledku	výsledek	k1gInSc2	výsledek
působení	působení	k1gNnSc2	působení
přednesu	přednes	k1gInSc2	přednes
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
docílit	docílit	k5eAaPmF	docílit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
recitátor	recitátor	k1gMnSc1	recitátor
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přednášejícím	přednášející	k2eAgInSc7d1	přednášející
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
bude	být	k5eAaImBp3nS	být
při	při	k7c6	při
přednášení	přednášení	k1gNnSc6	přednášení
využívat	využívat	k5eAaImF	využívat
mimiku	mimika	k1gFnSc4	mimika
a	a	k8xC	a
gestikulaci	gestikulace	k1gFnSc4	gestikulace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
velmi	velmi	k6eAd1	velmi
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mají	mít	k5eAaImIp3nP	mít
posluchači	posluchač	k1gMnPc1	posluchač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
básně	báseň	k1gFnSc2	báseň
k	k	k7c3	k
recitaci	recitace	k1gFnSc3	recitace
==	==	k?	==
</s>
</p>
<p>
<s>
Zkuste	zkusit	k5eAaPmRp2nP	zkusit
si	se	k3xPyFc3	se
nahlas	nahlas	k6eAd1	nahlas
přednést	přednést	k5eAaPmF	přednést
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
odrecitovat	odrecitovat	k5eAaPmF	odrecitovat
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnSc1d1	následující
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
známé	známý	k2eAgNnSc4d1	známé
rozpočítadlo	rozpočítadlo	k1gNnSc4	rozpočítadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
dílčí	dílčí	k2eAgFnPc1d1	dílčí
části	část	k1gFnPc1	část
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
tvoří	tvořit	k5eAaImIp3nP	tvořit
věty	věta	k1gFnPc1	věta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Recitovat	recitovat	k5eAaImF	recitovat
takovou	takový	k3xDgFnSc4	takový
báseň	báseň	k1gFnSc4	báseň
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
do	do	k7c2	do
toho	ten	k3xDgInSc2	ten
dát	dát	k5eAaPmF	dát
"	"	kIx"	"
<g/>
kus	kus	k6eAd1	kus
sebe	sebe	k3xPyFc4	sebe
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
udělat	udělat	k5eAaPmF	udělat
recitaci	recitace	k1gFnSc4	recitace
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stojí	stát	k5eAaImIp3nS	stát
javor	javor	k1gInSc1	javor
u	u	k7c2	u
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
říkají	říkat	k5eAaImIp3nP	říkat
mu	on	k3xPp3gNnSc3	on
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
na	na	k7c4	na
koho	kdo	k3yRnSc4	kdo
to	ten	k3xDgNnSc1	ten
slovo	slovo	k1gNnSc1	slovo
padne	padnout	k5eAaImIp3nS	padnout
</s>
</p>
<p>
<s>
ten	ten	k3xDgMnSc1	ten
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předpoklady	předpoklad	k1gInPc1	předpoklad
recitace	recitace	k1gFnSc2	recitace
==	==	k?	==
</s>
</p>
<p>
<s>
Interpretace	interpretace	k1gFnSc1	interpretace
ochuzuje	ochuzovat	k5eAaImIp3nS	ochuzovat
text	text	k1gInSc4	text
o	o	k7c4	o
nerealizované	realizovaný	k2eNgFnPc4d1	nerealizovaná
potenciální	potenciální	k2eAgFnPc4d1	potenciální
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
recitátor	recitátor	k1gMnSc1	recitátor
obohacuje	obohacovat	k5eAaImIp3nS	obohacovat
dílo	dílo	k1gNnSc4	dílo
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
přinese	přinést	k5eAaPmIp3nS	přinést
jeho	jeho	k3xOp3gInSc4	jeho
přednes	přednes	k1gInSc4	přednes
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
recitaci	recitace	k1gFnSc6	recitace
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
recitovaný	recitovaný	k2eAgInSc4d1	recitovaný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
přínos	přínos	k1gInSc1	přínos
recitátora	recitátor	k1gMnSc2	recitátor
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
recitátor	recitátor	k1gMnSc1	recitátor
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
při	při	k7c6	při
přednesu	přednes	k1gInSc6	přednes
svou	svůj	k3xOyFgFnSc4	svůj
osobitost	osobitost	k1gFnSc4	osobitost
<g/>
,	,	kIx,	,
ochuzoval	ochuzovat	k5eAaImAgMnS	ochuzovat
by	by	kYmCp3nS	by
recitaci	recitace	k1gFnSc4	recitace
o	o	k7c4	o
důležitou	důležitý	k2eAgFnSc4d1	důležitá
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gNnSc2	on
vyjádření	vyjádření	k1gNnSc2	vyjádření
jakožto	jakožto	k8xS	jakožto
prostředníka	prostředník	k1gMnSc4	prostředník
mezi	mezi	k7c7	mezi
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
autorem	autor	k1gMnSc7	autor
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
publikem	publikum	k1gNnSc7	publikum
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
text	text	k1gInSc1	text
přednáší	přednášet	k5eAaImIp3nS	přednášet
<g/>
,	,	kIx,	,
recituje	recitovat	k5eAaImIp3nS	recitovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
recitátor	recitátor	k1gMnSc1	recitátor
text	text	k1gInSc4	text
spíše	spíše	k9	spíše
kazil	kazit	k5eAaImAgMnS	kazit
<g/>
,	,	kIx,	,
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
předlohu	předloha	k1gFnSc4	předloha
recitace	recitace	k1gFnSc2	recitace
a	a	k8xC	a
snižoval	snižovat	k5eAaImAgInS	snižovat
tak	tak	k6eAd1	tak
značně	značně	k6eAd1	značně
úroveň	úroveň	k1gFnSc4	úroveň
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokladem	předpoklad	k1gInSc7	předpoklad
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
recitace	recitace	k1gFnSc2	recitace
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
splnění	splnění	k1gNnSc4	splnění
posluchačovy	posluchačův	k2eAgFnSc2d1	posluchačova
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
recitátor	recitátor	k1gMnSc1	recitátor
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
tlumočit	tlumočit	k5eAaImF	tlumočit
básníkovo	básníkův	k2eAgNnSc4d1	básníkovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otázkou	otázka	k1gFnSc7	otázka
správné	správný	k2eAgFnSc3d1	správná
recitaci	recitace	k1gFnSc3	recitace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
nebo	nebo	k8xC	nebo
jak	jak	k6eAd1	jak
musí	muset	k5eAaImIp3nS	muset
recitátor	recitátor	k1gMnSc1	recitátor
respektovat	respektovat	k5eAaImF	respektovat
autora	autor	k1gMnSc4	autor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
to	ten	k3xDgNnSc1	ten
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
více	hodně	k6eAd2	hodně
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
recitátor	recitátor	k1gMnSc1	recitátor
respektuje	respektovat	k5eAaImIp3nS	respektovat
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přednes	přednes	k1gInSc1	přednes
si	se	k3xPyFc3	se
upraví	upravit	k5eAaPmIp3nS	upravit
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
do	do	k7c2	do
recitace	recitace	k1gFnSc2	recitace
nedal	dát	k5eNaPmAgMnS	dát
nic	nic	k3yNnSc1	nic
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
recitace	recitace	k1gFnSc1	recitace
pouhým	pouhý	k2eAgNnSc7d1	pouhé
čtením	čtení	k1gNnSc7	čtení
bez	bez	k7c2	bez
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
důležitých	důležitý	k2eAgFnPc2d1	důležitá
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
takový	takový	k3xDgInSc1	takový
přednes	přednes	k1gInSc1	přednes
bychom	by	kYmCp1nP	by
ani	ani	k9	ani
nemohli	moct	k5eNaImAgMnP	moct
nazvat	nazvat	k5eAaBmF	nazvat
recitací	recitace	k1gFnSc7	recitace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recitátorské	recitátorský	k2eAgFnSc2d1	recitátorský
soutěže	soutěž	k1gFnSc2	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
příležitostech	příležitost	k1gFnPc6	příležitost
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
soutěže	soutěž	k1gFnPc1	soutěž
v	v	k7c6	v
recitování	recitování	k1gNnSc6	recitování
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pořádány	pořádat	k5eAaImNgFnP	pořádat
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dětských	dětský	k2eAgInPc2d1	dětský
zájmových	zájmový	k2eAgInPc2d1	zájmový
kroužků	kroužek	k1gInPc2	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
recitátorská	recitátorský	k2eAgFnSc1d1	recitátorský
soutěž	soutěž	k1gFnSc1	soutěž
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Pořádalo	pořádat	k5eAaImAgNnS	pořádat
ji	on	k3xPp3gFnSc4	on
bývalé	bývalý	k2eAgNnSc1d1	bývalé
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
bylo	být	k5eAaImAgNnS	být
povznést	povznést	k5eAaPmF	povznést
recitační	recitační	k2eAgNnSc1d1	recitační
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
rozvinout	rozvinout	k5eAaPmF	rozvinout
působivost	působivost	k1gFnSc4	působivost
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
na	na	k7c4	na
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
a	a	k8xC	a
vzorem	vzor	k1gInSc7	vzor
vzniku	vznik	k1gInSc2	vznik
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
bylo	být	k5eAaImAgNnS	být
nejspíše	nejspíše	k9	nejspíše
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
stav	stav	k1gInSc1	stav
recitačního	recitační	k2eAgNnSc2d1	recitační
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc4	soutěž
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
145	[number]	k4	145
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
profesionální	profesionální	k2eAgFnSc6d1	profesionální
herce	herka	k1gFnSc6	herka
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobře	dobře	k6eAd1	dobře
ovládají	ovládat	k5eAaImIp3nP	ovládat
mluvený	mluvený	k2eAgInSc4d1	mluvený
přednes	přednes	k1gInSc4	přednes
<g/>
.	.	kIx.	.
</s>
<s>
Vítězů	vítěz	k1gMnPc2	vítěz
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
poznámek	poznámka	k1gFnPc2	poznámka
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
recitace	recitace	k1gFnSc2	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://nase-rec.ujc.cas.cz/archiv.php?art=4538	[url]	k4	http://nase-rec.ujc.cas.cz/archiv.php?art=4538
</s>
</p>
<p>
<s>
Předpoklady	předpoklad	k1gInPc4	předpoklad
recitace	recitace	k1gFnSc2	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
FF	ff	kA	ff
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
https://digilib.phil.muni.cz/bitstream/handle/11222.digilib/120166/SpisyFF_136-1968-1_25.pdf?sequence=1	[url]	k4	https://digilib.phil.muni.cz/bitstream/handle/11222.digilib/120166/SpisyFF_136-1968-1_25.pdf?sequence=1
</s>
</p>
<p>
<s>
Recitace	recitace	k1gFnSc1	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://slovnik-cizich-slov.abz.cz/web.php/slovo/recitace	[url]	k1gFnSc2	http://slovnik-cizich-slov.abz.cz/web.php/slovo/recitace
</s>
</p>
