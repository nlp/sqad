<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
EU	EU	kA	EU
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
kruh	kruh	k1gInSc4	kruh
dvanácti	dvanáct	k4xCc2	dvanáct
zlatých	zlatý	k2eAgFnPc2d1	zlatá
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
vlajka	vlajka	k1gFnSc1	vlajka
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
Radou	rada	k1gFnSc7	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
začátkem	začátkem	k7c2	začátkem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
hledala	hledat	k5eAaImAgFnS	hledat
vhodný	vhodný	k2eAgInSc4d1	vhodný
motiv	motiv	k1gInSc4	motiv
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnSc4d1	budoucí
vlajku	vlajka	k1gFnSc4	vlajka
EU	EU	kA	EU
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
šlechtic	šlechtic	k1gMnSc1	šlechtic
japonsko-rakouského	japonskoakouský	k2eAgInSc2d1	japonsko-rakouský
původu	původ	k1gInSc2	původ
Richard	Richard	k1gMnSc1	Richard
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Coudenhove-Kalergi	Coudenhove-Kalerge	k1gFnSc4	Coudenhove-Kalerge
přejmout	přejmout	k5eAaPmF	přejmout
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
jím	jíst	k5eAaImIp1nS	jíst
založené	založený	k2eAgNnSc1d1	založené
<g/>
,	,	kIx,	,
Panevropské	panevropský	k2eAgFnSc2d1	panevropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
kruhu	kruh	k1gInSc6	kruh
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kruh	kruh	k1gInSc1	kruh
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
japonské	japonský	k2eAgFnSc3d1	japonská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Kalergi	Kalerg	k1gFnSc3	Kalerg
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
své	svůj	k3xOyFgFnSc3	svůj
japonské	japonský	k2eAgFnSc3d1	japonská
matce	matka	k1gFnSc3	matka
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
však	však	k9	však
zamítnut	zamítnut	k2eAgInSc1d1	zamítnut
kvůli	kvůli	k7c3	kvůli
protestům	protest	k1gInPc3	protest
Turecka	Turecko	k1gNnSc2	Turecko
proti	proti	k7c3	proti
přítomnosti	přítomnost	k1gFnSc3	přítomnost
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hlavní	hlavní	k2eAgInSc1d1	hlavní
motiv	motiv	k1gInSc1	motiv
japonské	japonský	k2eAgFnSc2d1	japonská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
kruh	kruh	k1gInSc4	kruh
uprostřed	uprostřed	k7c2	uprostřed
jednobarevného	jednobarevný	k2eAgNnSc2d1	jednobarevné
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
na	na	k7c4	na
na	na	k7c6	na
finálním	finální	k2eAgInSc6d1	finální
návrhu	návrh	k1gInSc6	návrh
Arsè	Arsè	k1gFnSc2	Arsè
Heitze	Heitze	k1gFnSc2	Heitze
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
12	[number]	k4	12
jako	jako	k8xS	jako
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
vybráno	vybrat	k5eAaPmNgNnS	vybrat
kvůli	kvůli	k7c3	kvůli
politické	politický	k2eAgFnSc3d1	politická
neutrálnosti	neutrálnost	k1gFnSc3	neutrálnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Haitz	Haitz	k1gMnSc1	Haitz
připustil	připustit	k5eAaPmAgMnS	připustit
inspiraci	inspirace	k1gFnSc4	inspirace
mariánskou	mariánský	k2eAgFnSc7d1	Mariánská
ikonografií	ikonografie	k1gFnSc7	ikonografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
Radou	Rada	k1gMnSc7	Rada
Evropy	Evropa	k1gFnSc2	Evropa
přijata	přijat	k2eAgFnSc1d1	přijata
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1955	[number]	k4	1955
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ji	on	k3xPp3gFnSc4	on
hlavní	hlavní	k2eAgMnSc1d1	hlavní
herold	herold	k1gMnSc1	herold
Irska	Irsko	k1gNnSc2	Irsko
Gerard	Gerarda	k1gFnPc2	Gerarda
Slevin	Slevina	k1gFnPc2	Slevina
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
stala	stát	k5eAaPmAgFnS	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Maastrichtské	maastrichtský	k2eAgFnSc2d1	Maastrichtská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Společenství	společenství	k1gNnSc3	společenství
nahradila	nahradit	k5eAaPmAgFnS	nahradit
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
také	také	k9	také
jeho	jeho	k3xOp3gFnSc4	jeho
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovšem	ovšem	k9	ovšem
zrušením	zrušení	k1gNnSc7	zrušení
Maastrichtské	maastrichtský	k2eAgFnSc2d1	Maastrichtská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
nahrazením	nahrazení	k1gNnSc7	nahrazení
Lisabonskou	lisabonský	k2eAgFnSc7d1	Lisabonská
smlouvou	smlouva	k1gFnSc7	smlouva
nebyla	být	k5eNaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
smlouvě	smlouva	k1gFnSc6	smlouva
deklarována	deklarován	k2eAgFnSc1d1	deklarována
jako	jako	k8xC	jako
vlajka	vlajka	k1gFnSc1	vlajka
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiálně	oficiálně	k6eAd1	oficiálně
EU	EU	kA	EU
žádnou	žádný	k3yNgFnSc4	žádný
vlajku	vlajka	k1gFnSc4	vlajka
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
EU	EU	kA	EU
prohlásily	prohlásit	k5eAaPmAgInP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
takové	takový	k3xDgNnSc4	takový
vyjádření	vyjádření	k1gNnSc4	vyjádření
nevydaly	vydat	k5eNaPmAgInP	vydat
je	on	k3xPp3gNnSc4	on
i	i	k9	i
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
všech	všecek	k3xTgFnPc2	všecek
eurobankovek	eurobankovka	k1gFnPc2	eurobankovka
<g/>
,	,	kIx,	,
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
kruh	kruh	k1gInSc1	kruh
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
ustálen	ustálit	k5eAaPmNgInS	ustálit
na	na	k7c6	na
dvanácti	dvanáct	k4xCc6	dvanáct
a	a	k8xC	a
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
členů	člen	k1gMnPc2	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
měla	mít	k5eAaImAgFnS	mít
Rada	rada	k1gFnSc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
budoucí	budoucí	k2eAgFnSc1d1	budoucí
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
jednu	jeden	k4xCgFnSc4	jeden
hvězdu	hvězda	k1gFnSc4	hvězda
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
při	při	k7c6	při
přijetí	přijetí	k1gNnSc6	přijetí
nových	nový	k2eAgMnPc2d1	nový
členů	člen	k1gMnPc2	člen
již	již	k6eAd1	již
neměnil	měnit	k5eNaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
bylo	být	k5eAaImAgNnS	být
sporné	sporný	k2eAgNnSc1d1	sporné
území	území	k1gNnSc1	území
Sársko	Sársko	k1gNnSc1	Sársko
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc1d1	vlastní
hvězda	hvězda	k1gFnSc1	hvězda
by	by	kYmCp3nS	by
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
představu	představa	k1gFnSc4	představa
samostatnosti	samostatnost	k1gFnSc2	samostatnost
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
Francie	Francie	k1gFnSc1	Francie
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
14	[number]	k4	14
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
představu	představa	k1gFnSc4	představa
pohlcení	pohlcení	k1gNnSc1	pohlcení
Sárska	Sársko	k1gNnSc2	Sársko
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mýtem	mýtus	k1gInSc7	mýtus
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
poté	poté	k6eAd1	poté
italský	italský	k2eAgMnSc1d1	italský
zástupce	zástupce	k1gMnSc1	zástupce
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
13	[number]	k4	13
je	být	k5eAaImIp3nS	být
nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
vlajka	vlajka	k1gFnSc1	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
měla	mít	k5eAaImAgFnS	mít
tento	tento	k3xDgInSc4	tento
počet	počet	k1gInSc4	počet
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdy	hvězda	k1gFnPc4	hvězda
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
ideály	ideál	k1gInPc1	ideál
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
souladu	soulad	k1gInSc2	soulad
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgInPc7d1	evropský
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
inspirace	inspirace	k1gFnSc1	inspirace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
EHS	EHS	kA	EHS
vlajku	vlajka	k1gFnSc4	vlajka
přijalo	přijmout	k5eAaPmAgNnS	přijmout
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
její	její	k3xOp3gMnSc1	její
autor	autor	k1gMnSc1	autor
Arsene	arsen	k1gInSc5	arsen
Heitz	Heitz	k1gInSc4	Heitz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
návrhu	návrh	k1gInSc6	návrh
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
také	také	k6eAd1	také
textem	text	k1gInSc7	text
12	[number]	k4	12
<g/>
.	.	kIx.	.
kapitoly	kapitola	k1gFnPc4	kapitola
biblické	biblický	k2eAgFnSc2d1	biblická
knihy	kniha	k1gFnSc2	kniha
Zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
ženu	žena	k1gFnSc4	žena
s	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
hvězdami	hvězda	k1gFnPc7	hvězda
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
umění	umění	k1gNnSc6	umění
často	často	k6eAd1	často
spojen	spojit	k5eAaPmNgInS	spojit
také	také	k6eAd1	také
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
právě	právě	k9	právě
s	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
hvězdami	hvězda	k1gFnPc7	hvězda
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
též	též	k9	též
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
Mariiných	Mariin	k2eAgInPc2d1	Mariin
atributů	atribut	k1gInPc2	atribut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Časté	častý	k2eAgFnPc1d1	častá
chyby	chyba	k1gFnPc1	chyba
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
ukázky	ukázka	k1gFnPc1	ukázka
jsou	být	k5eAaImIp3nP	být
častými	častý	k2eAgFnPc7d1	častá
chybami	chyba	k1gFnPc7	chyba
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlajka	vlajka	k1gFnSc1	vlajka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Graphical	Graphicat	k5eAaPmAgMnS	Graphicat
specifications	specifications	k6eAd1	specifications
for	forum	k1gNnPc2	forum
the	the	k?	the
European	European	k1gMnSc1	European
Emblem	Emblo	k1gNnSc7	Emblo
</s>
</p>
<p>
<s>
EU	EU	kA	EU
Insignia	Insignium	k1gNnSc2	Insignium
<g/>
/	/	kIx~	/
<g/>
Flag	flag	k1gInSc1	flag
Site	Sit	k1gFnSc2	Sit
</s>
</p>
<p>
<s>
Symbolizing	Symbolizing	k1gInSc1	Symbolizing
Europe	Europ	k1gInSc5	Europ
<g/>
:	:	kIx,	:
The	The	k1gFnSc3	The
EU	EU	kA	EU
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flag	flag	k1gInSc1	flag
<g/>
,	,	kIx,	,
Anthem	anthem	k1gInSc1	anthem
<g/>
,	,	kIx,	,
Holiday	Holidaa	k1gFnPc1	Holidaa
and	and	k?	and
Motto	motto	k1gNnSc1	motto
</s>
</p>
