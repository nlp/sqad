<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
zpravidla	zpravidla	k6eAd1	zpravidla
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
virům	vir	k1gInPc3	vir
<g/>
,	,	kIx,	,
bakteriím	bakterie	k1gFnPc3	bakterie
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
patogenům	patogen	k1gInPc3	patogen
či	či	k8xC	či
částicím	částice	k1gFnPc3	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nádorovým	nádorový	k2eAgFnPc3d1	nádorová
buňkám	buňka	k1gFnPc3	buňka
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
všem	všecek	k3xTgMnPc3	všecek
organismu	organismus	k1gInSc6	organismus
cizím	cizí	k2eAgMnPc3d1	cizí
materiálům	materiál	k1gInPc3	materiál
<g/>
.	.	kIx.	.
</s>
