<s>
Ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
Aves	Aves	k1gInSc1	Aves
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dvojnozí	dvojnohý	k2eAgMnPc1d1	dvojnohý
<g/>
,	,	kIx,	,
teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
a	a	k8xC	a
vejce	vejce	k1gNnPc4	vejce
snášející	snášející	k2eAgFnPc4d1	snášející
obratlovci	obratlovec	k1gMnPc7	obratlovec
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
nové	nový	k2eAgFnSc2d1	nová
systematiky	systematika	k1gFnSc2	systematika
patřící	patřící	k2eAgMnPc1d1	patřící
mezi	mezi	k7c4	mezi
teropodní	teropodní	k2eAgMnPc4d1	teropodní
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
a	a	k8xC	a
obecněji	obecně	k6eAd2	obecně
diapsidy	diapsida	k1gFnSc2	diapsida
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
především	především	k9	především
přítomností	přítomnost	k1gFnSc7	přítomnost
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
trojprstými	trojprstý	k2eAgFnPc7d1	trojprstý
předními	přední	k2eAgFnPc7d1	přední
končetinami	končetina	k1gFnPc7	končetina
přeměněnými	přeměněný	k2eAgFnPc7d1	přeměněná
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
redukovaným	redukovaný	k2eAgInSc7d1	redukovaný
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
srůsty	srůst	k1gInPc7	srůst
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
asi	asi	k9	asi
9978	[number]	k4	9978
druhů	druh	k1gInPc2	druh
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4083	[number]	k4	4083
druhů	druh	k1gInPc2	druh
nepěvců	nepěvce	k1gMnPc2	nepěvce
a	a	k8xC	a
5895	[number]	k4	5895
druhů	druh	k1gInPc2	druh
pěvců	pěvec	k1gMnPc2	pěvec
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
2000	[number]	k4	2000
druhů	druh	k1gInPc2	druh
fosilních	fosilní	k2eAgInPc2d1	fosilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
pokládáni	pokládán	k2eAgMnPc1d1	pokládán
za	za	k7c4	za
potomky	potomek	k1gMnPc4	potomek
drobných	drobný	k2eAgMnPc2d1	drobný
teropodních	teropodní	k2eAgMnPc2d1	teropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
maniraptorů	maniraptor	k1gInPc2	maniraptor
<g/>
.	.	kIx.	.
</s>
<s>
Dinosaury	dinosaurus	k1gMnPc4	dinosaurus
jako	jako	k8xS	jako
předky	předek	k1gMnPc4	předek
ptáků	pták	k1gMnPc2	pták
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
již	již	k6eAd1	již
Carl	Carl	k1gMnSc1	Carl
Gegenbaur	Gegenbaur	k1gMnSc1	Gegenbaur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
přesně	přesně	k6eAd1	přesně
zformulovanou	zformulovaný	k2eAgFnSc4d1	zformulovaná
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
původu	původ	k1gInSc6	původ
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
výzkumům	výzkum	k1gInPc3	výzkum
Johna	John	k1gMnSc2	John
Ostroma	Ostroma	k1gFnSc1	Ostroma
respektována	respektovat	k5eAaImNgFnS	respektovat
naprostou	naprostý	k2eAgFnSc7d1	naprostá
většinou	většina	k1gFnSc7	většina
paleontologů	paleontolog	k1gMnPc2	paleontolog
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
ornitology	ornitolog	k1gMnPc7	ornitolog
<g/>
.	.	kIx.	.
</s>
<s>
Určení	určení	k1gNnSc1	určení
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
známého	známý	k2eAgMnSc2d1	známý
ptáka	pták	k1gMnSc2	pták
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
jména	jméno	k1gNnSc2	jméno
Aves	Avesa	k1gFnPc2	Avesa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Klad	klad	k1gInSc1	klad
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
Aves	Aves	k1gInSc4	Aves
označuje	označovat	k5eAaImIp3nS	označovat
Jacques	Jacques	k1gMnSc1	Jacques
Gauthier	Gauthier	k1gMnSc1	Gauthier
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
neontologů	neontolog	k1gMnPc2	neontolog
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInSc1d1	fosilní
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
molekulárně	molekulárně	k6eAd1	molekulárně
biologické	biologický	k2eAgFnSc2d1	biologická
evidence	evidence	k1gFnSc2	evidence
však	však	k9	však
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
už	už	k6eAd1	už
existovalo	existovat	k5eAaImAgNnS	existovat
37	[number]	k4	37
skupin	skupina	k1gFnPc2	skupina
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
paleontologů	paleontolog	k1gMnPc2	paleontolog
ale	ale	k9	ale
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
zahrnout	zahrnout	k5eAaPmF	zahrnout
do	do	k7c2	do
Aves	Avesa	k1gFnPc2	Avesa
i	i	k9	i
taxony	taxon	k1gInPc1	taxon
stojící	stojící	k2eAgInPc1d1	stojící
mimo	mimo	k7c4	mimo
skupinu	skupina	k1gFnSc4	skupina
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
všemi	všecek	k3xTgInPc7	všecek
žijícími	žijící	k2eAgInPc7d1	žijící
druhy	druh	k1gInPc7	druh
a	a	k8xC	a
definovat	definovat	k5eAaBmF	definovat
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
Archaeopteryx	Archaeopteryx	k1gInSc4	Archaeopteryx
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
poslední	poslední	k2eAgInSc4d1	poslední
společný	společný	k2eAgInSc4d1	společný
předek	předek	k1gInSc4	předek
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nejstarším	starý	k2eAgMnSc7d3	nejstarší
ptákem	pták	k1gMnSc7	pták
stává	stávat	k5eAaImIp3nS	stávat
sám	sám	k3xTgMnSc1	sám
pozdně	pozdně	k6eAd1	pozdně
jurský	jurský	k2eAgInSc4d1	jurský
Archaeopteryx	Archaeopteryx	k1gInSc4	Archaeopteryx
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
asi	asi	k9	asi
před	před	k7c7	před
150	[number]	k4	150
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznosti	příbuznost	k1gFnPc1	příbuznost
mezi	mezi	k7c4	mezi
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
mnoho	mnoho	k4c1	mnoho
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
významným	významný	k2eAgNnSc7d1	významné
potvrzením	potvrzení	k1gNnSc7	potvrzení
této	tento	k3xDgFnSc2	tento
vývojové	vývojový	k2eAgFnSc2d1	vývojová
vazby	vazba	k1gFnSc2	vazba
byl	být	k5eAaImAgInS	být
především	především	k9	především
objev	objev	k1gInSc1	objev
opeřených	opeřený	k2eAgMnPc2d1	opeřený
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
30	[number]	k4	30
rodů	rod	k1gInPc2	rod
opeřených	opeřený	k2eAgMnPc2d1	opeřený
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
fylogenetické	fylogenetický	k2eAgFnSc2d1	fylogenetická
systematiky	systematika	k1gFnSc2	systematika
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
považování	považování	k1gNnSc4	považování
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
potomky	potomek	k1gMnPc4	potomek
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
podskupinu	podskupina	k1gFnSc4	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
ptáků	pták	k1gMnPc2	pták
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnPc1d1	velká
jako	jako	k8xS	jako
u	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
nebo	nebo	k8xC	nebo
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
ptáci	pták	k1gMnPc1	pták
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
svojí	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
ty	ten	k3xDgInPc4	ten
nejmenší	malý	k2eAgInPc4d3	nejmenší
jen	jen	k8xS	jen
asi	asi	k9	asi
45	[number]	k4	45
<g/>
tisíckrát	tisíckrát	k6eAd1	tisíckrát
(	(	kIx(	(
<g/>
létavé	létavý	k2eAgFnSc2d1	létavá
formy	forma	k1gFnSc2	forma
jen	jen	k9	jen
10	[number]	k4	10
<g/>
tisíckrát	tisíckrát	k6eAd1	tisíckrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
60	[number]	k4	60
miliónům	milión	k4xCgInPc3	milión
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejmenší	malý	k2eAgMnPc4d3	nejmenší
ptáky	pták	k1gMnPc4	pták
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
kolibříci	kolibřík	k1gMnPc1	kolibřík
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2,5	[number]	k4	2,5
g	g	kA	g
<g/>
)	)	kIx)	)
a	a	k8xC	a
pěvci	pěvec	k1gMnSc6	pěvec
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
za	za	k7c4	za
vůbec	vůbec	k9	vůbec
nejmenšího	malý	k2eAgMnSc4d3	nejmenší
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
kolibřík	kolibřík	k1gMnSc1	kolibřík
kubánský	kubánský	k2eAgMnSc1d1	kubánský
(	(	kIx(	(
<g/>
Calypte	Calypt	k1gInSc5	Calypt
helenae	helenaus	k1gMnSc5	helenaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
i	i	k9	i
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
měří	měřit	k5eAaImIp3nS	měřit
6	[number]	k4	6
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
1,5	[number]	k4	1,5
g.	g.	k?	g.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgMnPc1d3	nejmenší
králíčci	králíček	k1gMnPc1	králíček
(	(	kIx(	(
<g/>
Regulus	Regulus	k1gInSc1	Regulus
spp	spp	k?	spp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
4,5	[number]	k4	4,5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
g.	g.	k?	g.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
pštros	pštros	k1gMnSc1	pštros
dvouprstý	dvouprstý	k2eAgMnSc1d1	dvouprstý
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
m	m	kA	m
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
až	až	k9	až
150	[number]	k4	150
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
létavých	létavý	k2eAgInPc2d1	létavý
pak	pak	k6eAd1	pak
někteří	některý	k3yIgMnPc1	některý
albatrosi	albatros	k1gMnPc1	albatros
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
až	až	k6eAd1	až
3,5	[number]	k4	3,5
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kondor	kondor	k1gMnSc1	kondor
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc1	rozpětí
3	[number]	k4	3
m	m	kA	m
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
orel	orel	k1gMnSc1	orel
mořský	mořský	k2eAgMnSc1d1	mořský
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc2	rozpětí
až	až	k9	až
2,5	[number]	k4	2,5
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc1d2	veliký
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
vymřelé	vymřelý	k2eAgInPc1d1	vymřelý
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Novozélandští	novozélandský	k2eAgMnPc1d1	novozélandský
ptáci	pták	k1gMnPc1	pták
moa	moa	k?	moa
(	(	kIx(	(
<g/>
Dinornithiformes	Dinornithiformes	k1gMnSc1	Dinornithiformes
<g/>
)	)	kIx)	)
měřili	měřit	k5eAaImAgMnP	měřit
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
až	až	k9	až
3,6	[number]	k4	3,6
m	m	kA	m
a	a	k8xC	a
vážili	vážit	k5eAaImAgMnP	vážit
až	až	k9	až
250	[number]	k4	250
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskarští	madagaskarský	k2eAgMnPc1d1	madagaskarský
běžci	běžec	k1gMnPc1	běžec
řádu	řád	k1gInSc2	řád
Aepyornithiformes	Aepyornithiformesa	k1gFnPc2	Aepyornithiformesa
byli	být	k5eAaImAgMnP	být
nižší	nízký	k2eAgMnPc1d2	nižší
(	(	kIx(	(
<g/>
3	[number]	k4	3
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
těžší	těžký	k2eAgMnSc1d2	těžší
(	(	kIx(	(
<g/>
500	[number]	k4	500
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
snášeli	snášet	k5eAaImAgMnP	snášet
také	také	k9	také
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgNnPc1d3	veliký
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
25	[number]	k4	25
×	×	k?	×
34	[number]	k4	34
cm	cm	kA	cm
a	a	k8xC	a
těžká	těžký	k2eAgFnSc1d1	těžká
10	[number]	k4	10
kg	kg	kA	kg
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
7	[number]	k4	7
pštrosím	pštrosí	k2eAgNnPc3d1	pštrosí
vejcím	vejce	k1gNnPc3	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
létavých	létavý	k2eAgMnPc2d1	létavý
byl	být	k5eAaImAgMnS	být
největší	veliký	k2eAgFnSc7d3	veliký
Argentavis	Argentavis	k1gFnSc7	Argentavis
magnificens	magnificens	k1gInSc1	magnificens
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vážil	vážit	k5eAaImAgInS	vážit
60	[number]	k4	60
až	až	k9	až
110	[number]	k4	110
kg	kg	kA	kg
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měřil	měřit	k5eAaImAgInS	měřit
přes	přes	k7c4	přes
8	[number]	k4	8
m.	m.	k?	m.
Vnější	vnější	k2eAgInSc4d1	vnější
vzhled	vzhled	k1gInSc4	vzhled
současných	současný	k2eAgMnPc2d1	současný
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
velikostí	velikost	k1gFnSc7	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
velikostí	velikost	k1gFnSc7	velikost
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
krku	krk	k1gInSc2	krk
a	a	k8xC	a
typu	typ	k1gInSc2	typ
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
suchozemské	suchozemský	k2eAgMnPc4d1	suchozemský
obratlovce	obratlovec	k1gMnPc4	obratlovec
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
aktivního	aktivní	k2eAgInSc2d1	aktivní
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
charakterizováni	charakterizován	k2eAgMnPc1d1	charakterizován
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
čelistmi	čelist	k1gFnPc7	čelist
bez	bez	k7c2	bez
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
přeměněnými	přeměněný	k2eAgFnPc7d1	přeměněná
rohovinovými	rohovinový	k2eAgFnPc7d1	rohovinová
ramfotékou	ramfotéká	k1gFnSc4	ramfotéká
v	v	k7c4	v
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
,	,	kIx,	,
dýcháním	dýchání	k1gNnSc7	dýchání
skrze	skrze	k?	skrze
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
vaky	vak	k1gInPc1	vak
<g/>
,	,	kIx,	,
pronikající	pronikající	k2eAgInPc1d1	pronikající
do	do	k7c2	do
postkraniální	postkraniální	k2eAgFnSc2d1	postkraniální
kostry	kostra	k1gFnSc2	kostra
<g/>
,	,	kIx,	,
a	a	k8xC	a
kostrou	kostra	k1gFnSc7	kostra
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
mnoha	mnoho	k4c2	mnoho
srostlých	srostlý	k2eAgInPc2d1	srostlý
elementů	element	k1gInPc2	element
(	(	kIx(	(
<g/>
pygostyl	pygostyl	k1gInSc1	pygostyl
<g/>
,	,	kIx,	,
karpometakarpus	karpometakarpus	k1gMnSc1	karpometakarpus
<g/>
,	,	kIx,	,
tarzometatarzus	tarzometatarzus	k1gMnSc1	tarzometatarzus
a	a	k8xC	a
tibiotarzus	tibiotarzus	k1gMnSc1	tibiotarzus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
přeměněny	přeměněn	k2eAgFnPc4d1	přeměněna
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
umožňující	umožňující	k2eAgInSc4d1	umožňující
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
běžci	běžec	k1gMnPc1	běžec
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
endemické	endemický	k2eAgInPc4d1	endemický
ostrovní	ostrovní	k2eAgInPc4d1	ostrovní
druhy	druh	k1gInPc4	druh
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
k	k	k7c3	k
letu	let	k1gInSc3	let
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Létavé	létavý	k2eAgMnPc4d1	létavý
ptáky	pták	k1gMnPc4	pták
dále	daleko	k6eAd2	daleko
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
kost	kost	k1gFnSc1	kost
prsní	prsní	k2eAgFnSc1d1	prsní
s	s	k7c7	s
kýlem	kýl	k1gInSc7	kýl
pro	pro	k7c4	pro
úpon	úpon	k1gInSc4	úpon
hrudních	hrudní	k2eAgInPc2d1	hrudní
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
přímo	přímo	k6eAd1	přímo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
letovým	letový	k2eAgFnPc3d1	letová
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Kostru	kostra	k1gFnSc4	kostra
mají	mít	k5eAaImIp3nP	mít
lehkou	lehký	k2eAgFnSc4d1	lehká
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgFnPc1d1	dutá
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
podniká	podnikat	k5eAaImIp3nS	podnikat
každoročně	každoročně	k6eAd1	každoročně
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
migrační	migrační	k2eAgFnPc4d1	migrační
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
ptáků	pták	k1gMnPc2	pták
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
také	také	k9	také
krátké	krátký	k2eAgInPc4d1	krátký
nepravidelné	pravidelný	k2eNgInPc4d1	nepravidelný
tahy	tah	k1gInPc4	tah
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
sociální	sociální	k2eAgMnPc1d1	sociální
živočichové	živočich	k1gMnPc1	živočich
a	a	k8xC	a
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
vizuálních	vizuální	k2eAgInPc2d1	vizuální
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
voláním	volání	k1gNnSc7	volání
a	a	k8xC	a
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
,	,	kIx,	,
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc3d1	sociální
činnosti	činnost	k1gFnSc3	činnost
včetně	včetně	k7c2	včetně
společného	společný	k2eAgInSc2d1	společný
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
shromažďování	shromažďování	k1gNnSc1	shromažďování
a	a	k8xC	a
napadání	napadání	k1gNnSc1	napadání
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
společensky	společensky	k6eAd1	společensky
monogamní	monogamní	k2eAgMnPc1d1	monogamní
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
krátkodobým	krátkodobý	k2eAgNnSc7d1	krátkodobé
nebo	nebo	k8xC	nebo
řidčeji	řídce	k6eAd2	řídce
i	i	k9	i
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
životem	život	k1gInSc7	život
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
;	;	kIx,	;
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
polygamní	polygamní	k2eAgInPc1d1	polygamní
nebo	nebo	k8xC	nebo
polyandrické	polyandrický	k2eAgInPc1d1	polyandrický
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
snášejí	snášet	k5eAaImIp3nP	snášet
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obvykle	obvykle	k6eAd1	obvykle
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
a	a	k8xC	a
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
je	on	k3xPp3gFnPc4	on
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vylíhnutí	vylíhnutí	k1gNnPc2	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
41	[number]	k4	41
stupni	stupeň	k1gInPc7	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
ekonomicky	ekonomicky	k6eAd1	ekonomicky
důležití	důležitý	k2eAgMnPc1d1	důležitý
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
získávané	získávaný	k2eAgFnSc2d1	získávaná
buď	buď	k8xC	buď
lovem	lov	k1gInSc7	lov
(	(	kIx(	(
<g/>
čižba	čižba	k1gFnSc1	čižba
<g/>
,	,	kIx,	,
myslivost	myslivost	k1gFnSc1	myslivost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
chovem	chov	k1gInSc7	chov
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsou	být	k5eAaImIp3nP	být
užiteční	užitečný	k2eAgMnPc1d1	užitečný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
"	"	kIx"	"
<g/>
škůdci	škůdce	k1gMnPc7	škůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
pěvci	pěvec	k1gMnPc7	pěvec
nebo	nebo	k8xC	nebo
papoušci	papoušek	k1gMnPc1	papoušek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
chováni	chovat	k5eAaImNgMnP	chovat
jako	jako	k8xC	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnPc1d1	ptačí
figury	figura	k1gFnPc1	figura
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
aspektech	aspekt	k1gInPc6	aspekt
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
od	od	k7c2	od
náboženství	náboženství	k1gNnSc2	náboženství
až	až	k9	až
po	po	k7c4	po
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
120	[number]	k4	120
až	až	k9	až
130	[number]	k4	130
druhů	druh	k1gMnPc2	druh
ptáků	pták	k1gMnPc2	pták
vyhubil	vyhubit	k5eAaPmAgMnS	vyhubit
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
aktivitám	aktivita	k1gFnPc3	aktivita
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovku	stovka	k1gFnSc4	stovka
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
okolo	okolo	k7c2	okolo
1	[number]	k4	1
200	[number]	k4	200
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
hrozí	hrozit	k5eAaImIp3nP	hrozit
vyhynutí	vyhynutí	k1gNnSc4	vyhynutí
díky	díky	k7c3	díky
lidským	lidský	k2eAgFnPc3d1	lidská
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
198	[number]	k4	198
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
mokřady	mokřad	k1gInPc4	mokřad
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
potravu	potrava	k1gFnSc4	potrava
ptáků	pták	k1gMnPc2	pták
patří	patřit	k5eAaImIp3nS	patřit
nektar	nektar	k1gInSc1	nektar
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
zdechliny	zdechlina	k1gFnPc1	zdechlina
nebo	nebo	k8xC	nebo
jiní	jiný	k2eAgMnPc1d1	jiný
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
skupinou	skupina	k1gFnSc7	skupina
živočichů	živočich	k1gMnPc2	živočich
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ornitologie	ornitologie	k1gFnSc1	ornitologie
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
teropodních	teropodní	k2eAgInPc2d1	teropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
velké	velký	k2eAgNnSc4d1	velké
vymírání	vymírání	k1gNnSc4	vymírání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
eocénu	eocén	k1gInSc6	eocén
rozrůznila	rozrůznit	k5eAaPmAgFnS	rozrůznit
do	do	k7c2	do
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
žijícími	žijící	k2eAgMnPc7d1	žijící
příbuznými	příbuzný	k1gMnPc7	příbuzný
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
–	–	k?	–
<g/>
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nS	tvořit
klad	klad	k1gInSc4	klad
Archosauria	Archosaurium	k1gNnSc2	Archosaurium
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zdokumentován	zdokumentovat	k5eAaPmNgMnS	zdokumentovat
díky	díky	k7c3	díky
fosilnímu	fosilní	k2eAgInSc3d1	fosilní
záznamu	záznam	k1gInSc3	záznam
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
vývoj	vývoj	k1gInSc1	vývoj
peří	peřit	k5eAaImIp3nS	peřit
a	a	k8xC	a
změny	změna	k1gFnPc4	změna
kostry	kostra	k1gFnSc2	kostra
nutné	nutný	k2eAgFnPc1d1	nutná
pro	pro	k7c4	pro
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
pasivního	pasivní	k2eAgMnSc2d1	pasivní
k	k	k7c3	k
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
</s>
<s>
Určení	určení	k1gNnSc1	určení
sesterské	sesterský	k2eAgFnSc2d1	sesterská
skupiny	skupina	k1gFnSc2	skupina
ptáků	pták	k1gMnPc2	pták
opět	opět	k6eAd1	opět
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
jména	jméno	k1gNnSc2	jméno
Aves	Avesa	k1gFnPc2	Avesa
<g/>
:	:	kIx,	:
v	v	k7c6	v
Gauthierově	Gauthierův	k2eAgFnSc6d1	Gauthierův
definici	definice	k1gFnSc6	definice
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
žijících	žijící	k2eAgInPc6d1	žijící
druzích	druh	k1gInPc6	druh
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Ichthyornis	Ichthyornis	k1gFnPc1	Ichthyornis
nebo	nebo	k8xC	nebo
Iaceornis	Iaceornis	k1gFnPc1	Iaceornis
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
podobní	podobný	k2eAgMnPc1d1	podobný
žijícím	žijící	k2eAgMnPc3d1	žijící
ptákům	pták	k1gMnPc3	pták
a	a	k8xC	a
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
nápadným	nápadný	k2eAgInSc7d1	nápadný
primitivním	primitivní	k2eAgInSc7d1	primitivní
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
představují	představovat	k5eAaImIp3nP	představovat
zuby	zub	k1gInPc1	zub
v	v	k7c6	v
čelistech	čelist	k1gFnPc6	čelist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chiappeho	Chiappe	k1gMnSc2	Chiappe
definici	definice	k1gFnSc6	definice
<g />
.	.	kIx.	.
</s>
<s>
zase	zase	k9	zase
nejčastěji	často	k6eAd3	často
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Scansoriopterygidae	Scansoriopterygidae	k1gNnSc2	Scansoriopterygidae
nebo	nebo	k8xC	nebo
Deinonychosauria	Deinonychosaurium	k1gNnSc2	Deinonychosaurium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
klady	klad	k1gInPc4	klad
Troodontidae	Troodontida	k1gInSc2	Troodontida
a	a	k8xC	a
Dromaeosauridae	Dromaeosaurida	k1gInSc2	Dromaeosaurida
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
známý	známý	k2eAgInSc1d1	známý
Microraptor	Microraptor	k1gInSc1	Microraptor
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Velociraptor	Velociraptor	k1gInSc1	Velociraptor
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Peří	peřit	k5eAaImIp3nP	peřit
ale	ale	k8xC	ale
měli	mít	k5eAaImAgMnP	mít
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
odvození	odvození	k1gNnSc1	odvození
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
nutně	nutně	k6eAd1	nutně
starší	starý	k2eAgMnPc1d2	starší
<g/>
)	)	kIx)	)
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Sinosauropteryx	Sinosauropteryx	k1gInSc1	Sinosauropteryx
nebo	nebo	k8xC	nebo
Dilong	Dilong	k1gInSc1	Dilong
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dýchání	dýchání	k1gNnSc4	dýchání
usnadňované	usnadňovaný	k2eAgInPc4d1	usnadňovaný
vzdušnými	vzdušný	k2eAgInPc7d1	vzdušný
vaky	vak	k1gInPc7	vak
<g/>
,	,	kIx,	,
duté	dutý	k2eAgInPc1d1	dutý
obratle	obratel	k1gInPc1	obratel
<g/>
,	,	kIx,	,
endotermie	endotermie	k1gFnPc1	endotermie
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
vláknité	vláknitý	k2eAgNnSc4d1	vláknité
peří	peří	k1gNnSc4	peří
byly	být	k5eAaImAgInP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
pterosauři	pterosaurus	k1gMnPc1	pterosaurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
revertovaný	revertovaný	k2eAgInSc1d1	revertovaný
palec	palec	k1gInSc1	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
alula	alula	k6eAd1	alula
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgInSc4d1	aktivní
let	let	k1gInSc4	let
nebo	nebo	k8xC	nebo
karpometakarpus	karpometakarpus	k1gInSc4	karpometakarpus
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
ani	ani	k8xC	ani
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
taxonů	taxon	k1gInPc2	taxon
historicky	historicky	k6eAd1	historicky
pokládaných	pokládaný	k2eAgInPc2d1	pokládaný
za	za	k7c4	za
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovéto	takovýto	k3xDgFnSc6	takovýto
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
typické	typický	k2eAgInPc1d1	typický
ptačí	ptačí	k2eAgInPc1d1	ptačí
znaky	znak	k1gInPc1	znak
přibývají	přibývat	k5eAaImIp3nP	přibývat
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
časové	časový	k2eAgFnSc6d1	časová
škále	škála	k1gFnSc6	škála
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
již	již	k9	již
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
ptáka	pták	k1gMnSc4	pták
a	a	k8xC	a
co	co	k3yQnSc4	co
ještě	ještě	k9	ještě
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
ptáka	pták	k1gMnSc2	pták
je	být	k5eAaImIp3nS	být
především	především	k9	především
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
Archaeopteryx	Archaeopteryx	k1gInSc1	Archaeopteryx
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
nález	nález	k1gInSc1	nález
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
(	(	kIx(	(
<g/>
popsán	popsat	k5eAaPmNgInS	popsat
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
způsobil	způsobit	k5eAaPmAgMnS	způsobit
senzaci	senzace	k1gFnSc4	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
fosilní	fosilní	k2eAgInPc4d1	fosilní
taxony	taxon	k1gInPc4	taxon
patří	patřit	k5eAaImIp3nS	patřit
čínský	čínský	k2eAgInSc1d1	čínský
Confuciusornis	Confuciusornis	k1gInSc1	Confuciusornis
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
ptáků	pták	k1gMnPc2	pták
se	s	k7c7	s
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
pygostylem	pygostyl	k1gInSc7	pygostyl
<g/>
,	,	kIx,	,
Ichthyornis	Ichthyornis	k1gInSc1	Ichthyornis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzdáleně	vzdáleně	k6eAd1	vzdáleně
připomíná	připomínat	k5eAaImIp3nS	připomínat
rybáka	rybák	k1gMnSc4	rybák
se	s	k7c7	s
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
a	a	k8xC	a
dravý	dravý	k2eAgInSc1d1	dravý
Hesperornis	Hesperornis	k1gInSc1	Hesperornis
s	s	k7c7	s
nepneumatizovanými	pneumatizovaný	k2eNgFnPc7d1	pneumatizovaný
kostmi	kost	k1gFnPc7	kost
a	a	k8xC	a
redukcí	redukce	k1gFnSc7	redukce
křídel	křídlo	k1gNnPc2	křídlo
podobného	podobný	k2eAgNnSc2d1	podobné
dnešním	dnešní	k2eAgFnPc3d1	dnešní
potápkám	potápka	k1gFnPc3	potápka
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
kladem	klad	k1gInSc7	klad
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc4d1	zahrnující
jak	jak	k8xC	jak
mnoho	mnoho	k4c4	mnoho
fosilních	fosilní	k2eAgInPc2d1	fosilní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
všechny	všechen	k3xTgMnPc4	všechen
žijící	žijící	k2eAgMnPc4d1	žijící
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
Ornithothoraces	Ornithothoraces	k1gMnSc1	Ornithothoraces
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
báze	báze	k1gFnSc2	báze
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
množství	množství	k1gNnSc4	množství
odvozených	odvozený	k2eAgInPc2d1	odvozený
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
křidélko	křidélko	k1gNnSc1	křidélko
(	(	kIx(	(
<g/>
alula	alula	k1gFnSc1	alula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karpometakarpus	karpometakarpus	k1gInSc1	karpometakarpus
<g/>
,	,	kIx,	,
tarzometatarzus	tarzometatarzus	k1gInSc1	tarzometatarzus
<g/>
,	,	kIx,	,
revertovaný	revertovaný	k2eAgInSc1d1	revertovaný
palec	palec	k1gInSc1	palec
<g/>
,	,	kIx,	,
prsní	prsní	k2eAgFnSc1d1	prsní
kost	kost	k1gFnSc1	kost
s	s	k7c7	s
masivním	masivní	k2eAgInSc7d1	masivní
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
,	,	kIx,	,
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
jamka	jamka	k1gFnSc1	jamka
na	na	k7c6	na
lopatce	lopatka	k1gFnSc6	lopatka
pro	pro	k7c4	pro
skloubení	skloubení	k1gNnSc4	skloubení
s	s	k7c7	s
pažní	pažní	k2eAgFnSc7d1	pažní
kostí	kost	k1gFnSc7	kost
a	a	k8xC	a
aktivní	aktivní	k2eAgInSc4d1	aktivní
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Ornitotoracini	Ornitotoracin	k1gMnPc1	Ornitotoracin
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
protiptáky	protipták	k1gInPc1	protipták
(	(	kIx(	(
<g/>
Enantiornithes	Enantiornithes	k1gInSc1	Enantiornithes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Gobipteryx	Gobipteryx	k1gInSc1	Gobipteryx
nebo	nebo	k8xC	nebo
Sinornis	Sinornis	k1gInSc1	Sinornis
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravé	pravý	k2eAgMnPc4d1	pravý
ptáky	pták	k1gMnPc4	pták
(	(	kIx(	(
<g/>
Euornithes	Euornithes	k1gMnSc1	Euornithes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
nejbazálnějšími	bazální	k2eAgMnPc7d3	bazální
zástupci	zástupce	k1gMnPc7	zástupce
je	být	k5eAaImIp3nS	být
klad	klad	k1gInSc1	klad
Hongshanornithidae	Hongshanornithida	k1gFnSc2	Hongshanornithida
a	a	k8xC	a
nelétavý	létavý	k2eNgInSc4d1	nelétavý
Patagopteryx	Patagopteryx	k1gInSc4	Patagopteryx
a	a	k8xC	a
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
všichni	všechen	k3xTgMnPc1	všechen
moderní	moderní	k2eAgMnPc1d1	moderní
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
Neornithes	Neornithes	k1gMnSc1	Neornithes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
báze	báze	k1gFnSc2	báze
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
Palaeognathae	Palaeognatha	k1gInPc4	Palaeognatha
<g/>
–	–	k?	–
<g/>
zahrnující	zahrnující	k2eAgMnPc4d1	zahrnující
nelétavé	létavý	k2eNgMnPc4d1	nelétavý
běžce	běžec	k1gMnPc4	běžec
(	(	kIx(	(
<g/>
kivi	kivi	k1gMnPc4	kivi
<g/>
,	,	kIx,	,
emu	emu	k1gMnPc4	emu
<g/>
,	,	kIx,	,
nandu	nandu	k1gMnPc4	nandu
<g/>
,	,	kIx,	,
pštrosy	pštros	k1gMnPc4	pštros
<g/>
,	,	kIx,	,
kasuáry	kasuár	k1gMnPc4	kasuár
a	a	k8xC	a
vyhynulé	vyhynulý	k2eAgMnPc4d1	vyhynulý
ptáky	pták	k1gMnPc4	pták
moa	moa	k?	moa
<g/>
)	)	kIx)	)
a	a	k8xC	a
létavé	létavý	k2eAgFnPc1d1	létavá
tinamy	tinama	k1gFnPc1	tinama
–	–	k?	–
a	a	k8xC	a
Neognathae	Neognathae	k1gInSc1	Neognathae
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c6	na
paleognáty	paleognát	k1gInPc7	paleognát
a	a	k8xC	a
neognáty	neognát	k1gInPc7	neognát
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
neognátů	neognát	k1gInPc2	neognát
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
značně	značně	k6eAd1	značně
nestálý	stálý	k2eNgInSc1d1	nestálý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
velmi	velmi	k6eAd1	velmi
rychlou	rychlý	k2eAgFnSc7d1	rychlá
evoluční	evoluční	k2eAgFnSc7d1	evoluční
radiací	radiace	k1gFnSc7	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
další	další	k2eAgNnSc4d1	další
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c4	na
Galloanserae	Galloanserae	k1gFnSc4	Galloanserae
(	(	kIx(	(
<g/>
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
a	a	k8xC	a
vrubozobí	vrubozobý	k2eAgMnPc1d1	vrubozobý
<g/>
,	,	kIx,	,
a	a	k8xC	a
Neoaves	Neoaves	k1gInSc1	Neoaves
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
struktura	struktura	k1gFnSc1	struktura
Neoaves	Neoavesa	k1gFnPc2	Neoavesa
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
stabilní	stabilní	k2eAgNnSc1d1	stabilní
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
však	však	k9	však
odhalily	odhalit	k5eAaPmAgFnP	odhalit
existenci	existence	k1gFnSc4	existence
dalších	další	k2eAgNnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
,	,	kIx,	,
Metaves	Metavesa	k1gFnPc2	Metavesa
a	a	k8xC	a
Coronaves	Coronavesa	k1gFnPc2	Coronavesa
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Evoluce	evoluce	k1gFnSc1	evoluce
a	a	k8xC	a
fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
ptáků	pták	k1gMnPc2	pták
Všichni	všechen	k3xTgMnPc1	všechen
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
přirozených	přirozený	k2eAgFnPc2d1	přirozená
(	(	kIx(	(
<g/>
monofyletických	monofyletický	k2eAgFnPc2d1	monofyletická
<g/>
)	)	kIx)	)
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
složení	složení	k1gNnSc1	složení
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
příbuzenství	příbuzenství	k1gNnSc1	příbuzenství
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
často	často	k6eAd1	často
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
"	"	kIx"	"
<g/>
čeledím	čeleď	k1gFnPc3	čeleď
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
"	"	kIx"	"
<g/>
řádům	řád	k1gInPc3	řád
<g/>
"	"	kIx"	"
klasické	klasický	k2eAgFnSc2d1	klasická
systematiky	systematika	k1gFnSc2	systematika
Wetmore	Wetmor	k1gInSc5	Wetmor
<g/>
'	'	kIx"	'
<g/>
a.	a.	k?	a.
Rovněž	rovněž	k9	rovněž
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc4d1	znám
nejzákladnější	základní	k2eAgInPc4d3	nejzákladnější
(	(	kIx(	(
<g/>
nejhlubší	hluboký	k2eAgInPc4d3	nejhlubší
<g/>
)	)	kIx)	)
příbuzenské	příbuzenský	k2eAgInPc4d1	příbuzenský
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
skupinami	skupina	k1gFnPc7	skupina
<g/>
:	:	kIx,	:
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
běžci	běžec	k1gMnPc1	běžec
(	(	kIx(	(
<g/>
kiwi	kiwi	k1gNnSc1	kiwi
<g/>
,	,	kIx,	,
emu	emu	k1gMnPc1	emu
<g/>
,	,	kIx,	,
nandu	nandu	k1gMnPc1	nandu
<g/>
,	,	kIx,	,
pštrosi	pštros	k1gMnPc1	pštros
<g/>
,	,	kIx,	,
kasuáři	kasuář	k1gMnPc1	kasuář
<g/>
,	,	kIx,	,
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
ptáci	pták	k1gMnPc1	pták
moa	moa	k?	moa
<g/>
)	)	kIx)	)
a	a	k8xC	a
tinamy	tinama	k1gFnPc1	tinama
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
klad	klad	k1gInSc4	klad
Palaeognathae	Palaeognatha	k1gInSc2	Palaeognatha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
skupinou	skupina	k1gFnSc7	skupina
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgMnPc3	všecek
zbývajícím	zbývající	k2eAgMnPc3d1	zbývající
žijícím	žijící	k2eAgMnPc3d1	žijící
ptákům	pták	k1gMnPc3	pták
–	–	k?	–
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
Neognathae	Neognathae	k1gFnSc4	Neognathae
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
neognátů	neognát	k1gInPc2	neognát
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozeznatelný	rozeznatelný	k2eAgInSc1d1	rozeznatelný
klad	klad	k1gInSc1	klad
Galloanserae	Galloanserae	k1gFnSc1	Galloanserae
(	(	kIx(	(
<g/>
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
(	(	kIx(	(
<g/>
Galliformes	Galliformes	k1gMnSc1	Galliformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
(	(	kIx(	(
<g/>
Anseriformes	Anseriformes	k1gMnSc1	Anseriformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
neognáti	neognát	k1gMnPc1	neognát
mimo	mimo	k7c4	mimo
drůbež	drůbež	k1gFnSc4	drůbež
tvoří	tvořit	k5eAaImIp3nP	tvořit
další	další	k2eAgFnSc4d1	další
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
Neoaves	Neoaves	k1gInSc4	Neoaves
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
tak	tak	k8xS	tak
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
vztahy	vztah	k1gInPc1	vztah
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
"	"	kIx"	"
<g/>
řády	řád	k1gInPc7	řád
<g/>
"	"	kIx"	"
neoavianů	neoavian	k1gInPc2	neoavian
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
odhalily	odhalit	k5eAaPmAgFnP	odhalit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genomických	genomický	k2eAgNnPc2d1	genomické
dat	datum	k1gNnPc2	datum
existenci	existence	k1gFnSc4	existence
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
,	,	kIx,	,
Metaves	Metavesa	k1gFnPc2	Metavesa
a	a	k8xC	a
Coronaves	Coronavesa	k1gFnPc2	Coronavesa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nS	patřit
řada	řada	k1gFnSc1	řada
tropických	tropický	k2eAgMnPc2d1	tropický
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
faetonovitých	faetonovitý	k2eAgMnPc2d1	faetonovitý
<g/>
,	,	kIx,	,
mesitů	mesit	k1gInPc2	mesit
<g/>
,	,	kIx,	,
kaguů	kagu	k1gInPc2	kagu
<g/>
,	,	kIx,	,
slunatcovitých	slunatcovitý	k2eAgInPc2d1	slunatcovitý
a	a	k8xC	a
gvačarů	gvačar	k1gInPc2	gvačar
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
řazených	řazený	k2eAgFnPc2d1	řazená
mezi	mezi	k7c4	mezi
krátkokřídlé	krátkokřídlý	k2eAgFnPc4d1	krátkokřídlý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
–	–	k?	–
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
zařaditelnost	zařaditelnost	k1gFnSc4	zařaditelnost
–	–	k?	–
do	do	k7c2	do
vlastních	vlastní	k2eAgInPc2d1	vlastní
"	"	kIx"	"
<g/>
řádů	řád	k1gInPc2	řád
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
také	také	k9	také
měkkozobí	měkkozobit	k5eAaPmIp3nS	měkkozobit
včetně	včetně	k7c2	včetně
holubů	holub	k1gMnPc2	holub
<g/>
.	.	kIx.	.
</s>
<s>
Coronaves	Coronaves	k1gInSc1	Coronaves
by	by	kYmCp3nS	by
potom	potom	k6eAd1	potom
mělo	mít	k5eAaImAgNnS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
většinu	většina	k1gFnSc4	většina
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
–	–	k?	–
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
tohoto	tento	k3xDgInSc2	tento
kladu	klad	k1gInSc2	klad
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
tvořit	tvořit	k5eAaImF	tvořit
skupina	skupina	k1gFnSc1	skupina
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
neformálně	formálně	k6eNd1	formálně
"	"	kIx"	"
<g/>
land	land	k6eAd1	land
birds	birds	k6eAd1	birds
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgMnPc1d1	pozemní
ptáci	pták	k1gMnPc1	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
existenci	existence	k1gFnSc4	existence
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
pouze	pouze	k6eAd1	pouze
molekulární	molekulární	k2eAgFnPc1d1	molekulární
studie	studie	k1gFnPc1	studie
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgMnPc1d1	pozemní
ptáci	pták	k1gMnPc1	pták
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
např.	např.	kA	např.
sovy	sova	k1gFnPc1	sova
<g/>
,	,	kIx,	,
jestřábovité	jestřábovitý	k2eAgFnPc1d1	jestřábovitý
<g/>
,	,	kIx,	,
srostloprsté	srostloprstý	k2eAgFnPc1d1	srostloprstý
<g/>
,	,	kIx,	,
sokolovité	sokolovitý	k2eAgFnPc1d1	sokolovitý
<g/>
,	,	kIx,	,
papoušky	papoušek	k1gMnPc7	papoušek
nebo	nebo	k8xC	nebo
seriemy	seriem	k1gMnPc7	seriem
<g/>
,	,	kIx,	,
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnSc4d3	veliký
podskupinu	podskupina	k1gFnSc4	podskupina
ovšem	ovšem	k9	ovšem
tvoří	tvořit	k5eAaImIp3nP	tvořit
pěvci	pěvec	k1gMnPc1	pěvec
(	(	kIx(	(
<g/>
Passeriformes	Passeriformes	k1gMnSc1	Passeriformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
Metaves	Metaves	k1gInSc4	Metaves
a	a	k8xC	a
Coronaves	Coronaves	k1gInSc4	Coronaves
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
i	i	k8xC	i
obě	dva	k4xCgFnPc4	dva
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc2d3	veliký
molekulární	molekulární	k2eAgFnSc2d1	molekulární
kladistické	kladistický	k2eAgFnSc2d1	kladistická
analýzy	analýza	k1gFnSc2	analýza
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
Ericson	Ericsona	k1gFnPc2	Ericsona
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hackett	Hackett	k1gInSc1	Hackett
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
jít	jít	k5eAaImF	jít
o	o	k7c4	o
pouhý	pouhý	k2eAgInSc4d1	pouhý
artefakt	artefakt	k1gInSc4	artefakt
<g/>
,	,	kIx,	,
způsobení	způsobený	k2eAgMnPc1d1	způsobený
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
7	[number]	k4	7
<g/>
.	.	kIx.	.
intronu	intron	k1gInSc3	intron
genu	gen	k1gInSc2	gen
pro	pro	k7c4	pro
beta-fibrinogen	betaibrinogen	k1gInSc4	beta-fibrinogen
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
monofyletické	monofyletický	k2eAgFnSc2d1	monofyletická
skupiny	skupina	k1gFnSc2	skupina
Metaves	Metavesa	k1gFnPc2	Metavesa
a	a	k8xC	a
analýzy	analýza	k1gFnPc1	analýza
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
genech	gen	k1gInPc6	gen
rozdělení	rozdělení	k1gNnSc2	rozdělení
neoavianů	neoavian	k1gMnPc2	neoavian
na	na	k7c4	na
Metaves	Metaves	k1gInSc4	Metaves
<g/>
/	/	kIx~	/
<g/>
Coronaves	Coronaves	k1gInSc4	Coronaves
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
intronem	intron	k1gInSc7	intron
beta-fibrinogenu	betaibrinogen	k1gInSc2	beta-fibrinogen
poněkud	poněkud	k6eAd1	poněkud
překvapivě	překvapivě	k6eAd1	překvapivě
toto	tento	k3xDgNnSc4	tento
uspořádání	uspořádání	k1gNnSc4	uspořádání
podpořily	podpořit	k5eAaPmAgFnP	podpořit
ještě	ještě	k6eAd1	ještě
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
hypotéz	hypotéza	k1gFnPc2	hypotéza
v	v	k7c6	v
ptačí	ptačí	k2eAgFnSc6d1	ptačí
systematice	systematika	k1gFnSc6	systematika
nabízí	nabízet	k5eAaImIp3nS	nabízet
Mayr	Mayr	k1gInSc1	Mayr
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
nepodpořil	podpořit	k5eNaPmAgMnS	podpořit
rozdělení	rozdělení	k1gNnSc4	rozdělení
na	na	k7c4	na
Metaves	Metaves	k1gInSc4	Metaves
a	a	k8xC	a
Coronaves	Coronaves	k1gInSc4	Coronaves
<g/>
,	,	kIx,	,
naznačil	naznačit	k5eAaPmAgMnS	naznačit
ale	ale	k8xC	ale
existenci	existence	k1gFnSc4	existence
dvou	dva	k4xCgInPc2	dva
jiných	jiný	k2eAgInPc2d1	jiný
velkých	velký	k2eAgInPc2d1	velký
neoavianních	oavianní	k2eNgInPc2d1	oavianní
kladů	klad	k1gInPc2	klad
<g/>
,	,	kIx,	,
charakteristických	charakteristický	k2eAgNnPc2d1	charakteristické
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pozemních	pozemní	k2eAgMnPc2d1	pozemní
ptáků	pták	k1gMnPc2	pták
známých	známý	k1gMnPc2	známý
z	z	k7c2	z
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
studií	studie	k1gFnPc2	studie
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
i	i	k9	i
o	o	k7c4	o
vodní	vodní	k2eAgMnPc4d1	vodní
ptáky	pták	k1gMnPc4	pták
<g/>
:	:	kIx,	:
skupinu	skupina	k1gFnSc4	skupina
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
potáplicemi	potáplice	k1gFnPc7	potáplice
<g/>
,	,	kIx,	,
tučňáky	tučňák	k1gMnPc7	tučňák
<g/>
,	,	kIx,	,
ibisy	ibis	k1gMnPc7	ibis
<g/>
,	,	kIx,	,
čápovitými	čápovitý	k2eAgInPc7d1	čápovitý
<g/>
,	,	kIx,	,
pelikány	pelikán	k1gInPc7	pelikán
<g/>
,	,	kIx,	,
fregatkami	fregatka	k1gFnPc7	fregatka
<g/>
,	,	kIx,	,
faetonovitými	faetonovitý	k2eAgFnPc7d1	faetonovitý
a	a	k8xC	a
člunozobcem	člunozobec	k1gInSc7	člunozobec
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
snaží	snažit	k5eAaImIp3nS	snažit
zavést	zavést	k5eAaPmF	zavést
jméno	jméno	k1gNnSc4	jméno
Aequornithes	Aequornithesa	k1gFnPc2	Aequornithesa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
metodologického	metodologický	k2eAgNnSc2d1	metodologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
ptačí	ptačí	k2eAgFnSc1d1	ptačí
systematika	systematika	k1gFnSc1	systematika
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
kladistických	kladistický	k2eAgFnPc6d1	kladistická
analýzách	analýza	k1gFnPc6	analýza
genomů	genom	k1gInPc2	genom
(	(	kIx(	(
<g/>
mitochondriálního	mitochondriální	k2eAgMnSc2d1	mitochondriální
i	i	k8xC	i
jaderného	jaderný	k2eAgInSc2d1	jaderný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
–	–	k?	–
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
–	–	k?	–
morfologických	morfologický	k2eAgInPc2d1	morfologický
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
kladistiky	kladistika	k1gFnSc2	kladistika
na	na	k7c6	na
DNA	DNA	kA	DNA
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
dřívější	dřívější	k2eAgFnSc4d1	dřívější
fenetickou	fenetický	k2eAgFnSc4d1	fenetický
techniku	technika	k1gFnSc4	technika
DNA-DNA	DNA-DNA	k1gFnSc2	DNA-DNA
hybridizace	hybridizace	k1gFnSc2	hybridizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měří	měřit	k5eAaImIp3nS	měřit
podobnost	podobnost	k1gFnSc4	podobnost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
zděděná	zděděný	k2eAgFnSc1d1	zděděná
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hybridizaci	hybridizace	k1gFnSc6	hybridizace
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
velké	velký	k2eAgFnPc1d1	velká
analýzy	analýza	k1gFnPc1	analýza
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
předložili	předložit	k5eAaPmAgMnP	předložit
Sibley	Siblea	k1gMnSc2	Siblea
&	&	k?	&
Ahlquist	Ahlquist	k1gMnSc1	Ahlquist
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
..	..	k?	..
Její	její	k3xOp3gInPc1	její
závěry	závěr	k1gInPc1	závěr
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
seskupování	seskupování	k1gNnSc4	seskupování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
většími	veliký	k2eAgFnPc7d2	veliký
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vlivné	vlivný	k2eAgFnPc1d1	vlivná
moderní	moderní	k2eAgFnPc1d1	moderní
analýzy	analýza	k1gFnPc1	analýza
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
kladistických	kladistický	k2eAgFnPc6d1	kladistická
metodách	metoda	k1gFnPc6	metoda
<g/>
,	,	kIx,	,
představili	představit	k5eAaPmAgMnP	představit
Ericson	Ericson	k1gInSc4	Ericson
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Hackett	Hackett	k1gInSc1	Hackett
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
se	s	k7c7	s
169	[number]	k4	169
ingroup	ingroup	k1gInSc4	ingroup
taxony	taxon	k1gInPc4	taxon
a	a	k8xC	a
32	[number]	k4	32
kilobázemi	kilobáze	k1gFnPc7	kilobáze
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
dosud	dosud	k6eAd1	dosud
největším	veliký	k2eAgInSc7d3	veliký
systematickým	systematický	k2eAgInSc7d1	systematický
rozborem	rozbor	k1gInSc7	rozbor
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
morfologickou	morfologický	k2eAgFnSc4d1	morfologická
kladistickou	kladistický	k2eAgFnSc4d1	kladistická
analýzu	analýza	k1gFnSc4	analýza
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Livezey	Livezea	k1gMnSc2	Livezea
&	&	k?	&
Zusi	Zus	k1gFnSc2	Zus
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc1	jejich
rozbor	rozbor	k1gInSc1	rozbor
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
150	[number]	k4	150
ingroup	ingroup	k1gInSc4	ingroup
taxonů	taxon	k1gInPc2	taxon
a	a	k8xC	a
2954	[number]	k4	2954
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
nadřád	nadřád	k1gInSc1	nadřád
<g/>
:	:	kIx,	:
běžci	běžec	k1gMnPc1	běžec
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
tinamy	tinama	k1gFnPc1	tinama
(	(	kIx(	(
<g/>
Tinamiformes	Tinamiformes	k1gInSc1	Tinamiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
nanduové	nandu	k1gMnPc1	nandu
(	(	kIx(	(
<g/>
Rheiformes	Rheiformes	k1gMnSc1	Rheiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
pštrosi	pštros	k1gMnPc1	pštros
(	(	kIx(	(
<g/>
Struthioniformes	Struthioniformes	k1gMnSc1	Struthioniformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
kasuárové	kasuár	k1gMnPc1	kasuár
(	(	kIx(	(
<g/>
Casuariiformes	Casuariiformes	k1gMnSc1	Casuariiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
kiviové	kivi	k1gMnPc1	kivi
(	(	kIx(	(
<g/>
Apterygiformes	Apterygiformes	k1gMnSc1	Apterygiformes
<g/>
)	)	kIx)	)
nadřád	nadřád	k1gInSc1	nadřád
<g/>
:	:	kIx,	:
letci	letec	k1gMnPc1	letec
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
(	(	kIx(	(
<g/>
Anseriformes	Anseriformes	k1gMnSc1	Anseriformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
(	(	kIx(	(
<g/>
Galliformes	Galliformes	k1gMnSc1	Galliformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
tučňáci	tučňák	k1gMnPc1	tučňák
(	(	kIx(	(
<g/>
Sphenisciformes	Sphenisciformes	k1gMnSc1	Sphenisciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
potáplice	potáplice	k1gFnSc1	potáplice
(	(	kIx(	(
<g/>
Gaviiformes	Gaviiformes	k1gInSc1	Gaviiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
potápky	potápka	k1gFnPc1	potápka
(	(	kIx(	(
<g/>
Podicipediformes	Podicipediformes	k1gInSc1	Podicipediformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
trubkonosí	trubkonosit	k5eAaPmIp3nS	trubkonosit
(	(	kIx(	(
<g/>
Procellariiformes	Procellariiformes	k1gInSc1	Procellariiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
veslonozí	veslonozí	k1gMnPc1	veslonozí
(	(	kIx(	(
<g/>
Pelicaniformes	Pelicaniformes	k1gMnSc1	Pelicaniformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
brodiví	brodivý	k2eAgMnPc1d1	brodivý
(	(	kIx(	(
<g/>
Ciconiiformes	Ciconiiformes	k1gMnSc1	Ciconiiformes
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
plameňáci	plameňák	k1gMnPc1	plameňák
(	(	kIx(	(
<g/>
Phoenicopteriformes	Phoenicopteriformes	k1gMnSc1	Phoenicopteriformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
dravci	dravec	k1gMnPc1	dravec
(	(	kIx(	(
<g/>
Accipitriformes	Accipitriformes	k1gMnSc1	Accipitriformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
krátkokřídlí	krátkokřídlí	k1gMnPc1	krátkokřídlí
(	(	kIx(	(
<g/>
Gruiformes	Gruiformes	k1gMnSc1	Gruiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
dlouhokřídlí	dlouhokřídlý	k2eAgMnPc1d1	dlouhokřídlý
(	(	kIx(	(
<g/>
Charadriiformes	Charadriiformes	k1gMnSc1	Charadriiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
měkkozobí	měkkozobit	k5eAaPmIp3nS	měkkozobit
(	(	kIx(	(
<g/>
Columbiformes	Columbiformes	k1gInSc1	Columbiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
papoušci	papoušek	k1gMnPc1	papoušek
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Psittaciformes	Psittaciformes	k1gInSc1	Psittaciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
sokoli	sokol	k1gMnPc1	sokol
(	(	kIx(	(
<g/>
Falconiformes	Falconiformes	k1gMnSc1	Falconiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
kukačky	kukačka	k1gFnPc1	kukačka
(	(	kIx(	(
<g/>
Cuculiformes	Cuculiformes	k1gInSc1	Cuculiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
sovy	sova	k1gFnPc1	sova
(	(	kIx(	(
<g/>
Strigiformes	Strigiformes	k1gInSc1	Strigiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
lelkové	lelek	k1gMnPc1	lelek
(	(	kIx(	(
<g/>
Caprimulgriformes	Caprimulgriformes	k1gMnSc1	Caprimulgriformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
svišťouni	svišťoun	k1gMnPc1	svišťoun
(	(	kIx(	(
<g/>
Apodiformes	Apodiformes	k1gMnSc1	Apodiformes
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
srostloprstí	srostloprstý	k2eAgMnPc1d1	srostloprstý
(	(	kIx(	(
<g/>
Coraciiformes	Coraciiformes	k1gMnSc1	Coraciiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
šplhavci	šplhavec	k1gMnPc1	šplhavec
(	(	kIx(	(
<g/>
Piciformes	Piciformes	k1gMnSc1	Piciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
trogoni	trogon	k1gMnPc1	trogon
(	(	kIx(	(
<g/>
Trogoniformes	Trogoniformes	k1gMnSc1	Trogoniformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
myšáci	myšák	k1gMnPc1	myšák
(	(	kIx(	(
<g/>
Coliiformes	Coliiformes	k1gMnSc1	Coliiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
pěvci	pěvec	k1gMnPc1	pěvec
(	(	kIx(	(
<g/>
Passeriformes	Passeriformes	k1gMnSc1	Passeriformes
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
skřivanovití	skřivanovitý	k2eAgMnPc1d1	skřivanovitý
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
skřivan	skřivan	k1gMnSc1	skřivan
polní	polní	k2eAgMnSc1d1	polní
<g/>
,	,	kIx,	,
chocholouš	chocholouš	k1gMnSc1	chocholouš
obecný	obecný	k2eAgInSc4d1	obecný
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
vlaštovkovití	vlaštovkovitý	k2eAgMnPc1d1	vlaštovkovitý
–	–	k?	–
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
jiřička	jiřička	k1gFnSc1	jiřička
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
břehule	břehule	k1gFnSc1	břehule
říční	říční	k2eAgFnSc1d1	říční
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
drozdovití	drozdovitý	k2eAgMnPc1d1	drozdovitý
–	–	k?	–
drozd	drozd	k1gMnSc1	drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
<g/>
,	,	kIx,	,
slavík	slavík	k1gInSc1	slavík
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
,	,	kIx,	,
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
pěnicovití	pěnicovitý	k2eAgMnPc1d1	pěnicovitý
–	–	k?	–
rod	rod	k1gInSc1	rod
pěnice	pěnice	k1gFnSc1	pěnice
<g/>
,	,	kIx,	,
sedmihlásek	sedmihlásek	k1gMnSc1	sedmihlásek
hajní	hajní	k2eAgMnSc1d1	hajní
<g/>
,	,	kIx,	,
rákosník	rákosník	k1gMnSc1	rákosník
<g />
.	.	kIx.	.
</s>
<s>
velký	velký	k2eAgInSc1d1	velký
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
konipasovití	konipasovitý	k2eAgMnPc1d1	konipasovitý
–	–	k?	–
konipas	konipas	k1gMnSc1	konipas
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
linduška	linduška	k1gFnSc1	linduška
lesní	lesní	k2eAgFnSc1d1	lesní
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
ťuhýkovití	ťuhýkovitý	k2eAgMnPc1d1	ťuhýkovitý
–	–	k?	–
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
obecný	obecný	k2eAgMnSc1d1	obecný
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
sýkorovití	sýkorovitý	k2eAgMnPc1d1	sýkorovitý
–	–	k?	–
rod	rod	k1gInSc1	rod
sýkora	sýkora	k1gFnSc1	sýkora
<g/>
,	,	kIx,	,
mlynařík	mlynařík	k1gMnSc1	mlynařík
dlouhoocasý	dlouhoocasý	k2eAgMnSc1d1	dlouhoocasý
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
pěnkavovití	pěnkavovití	k1gMnPc1	pěnkavovití
–	–	k?	–
pěnkava	pěnkava	k1gFnSc1	pěnkava
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
stehlík	stehlík	k1gMnSc1	stehlík
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
čížek	čížek	k1gMnSc1	čížek
lesní	lesní	k2eAgMnSc1d1	lesní
<g/>
,	,	kIx,	,
konopka	konopka	k1gFnSc1	konopka
obecná	obecná	k1gFnSc1	obecná
čeleď	čeleď	k1gFnSc1	čeleď
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
snovačovití	snovačovitý	k2eAgMnPc1d1	snovačovitý
–	–	k?	–
vrabec	vrabec	k1gMnSc1	vrabec
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
vrabec	vrabec	k1gMnSc1	vrabec
polní	polní	k2eAgFnSc4d1	polní
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
:	:	kIx,	:
špačkovití	špačkovitý	k2eAgMnPc1d1	špačkovitý
–	–	k?	–
špaček	špaček	k1gInSc1	špaček
obecný	obecný	k2eAgInSc1d1	obecný
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
krkavcovití	krkavcovitý	k2eAgMnPc1d1	krkavcovitý
–	–	k?	–
krkavec	krkavec	k1gMnSc1	krkavec
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
vrána	vrána	k1gFnSc1	vrána
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
sojka	sojka	k1gFnSc1	sojka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
havran	havran	k1gMnSc1	havran
polní	polní	k2eAgMnSc1d1	polní
<g/>
,	,	kIx,	,
kavka	kavka	k1gFnSc1	kavka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
straka	straka	k1gFnSc1	straka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
ořešník	ořešník	k1gInSc1	ořešník
kropenatý	kropenatý	k2eAgInSc1d1	kropenatý
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
nedokonalosti	nedokonalost	k1gFnPc1	nedokonalost
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
řád	řád	k1gInSc1	řád
svišťouni	svišťoun	k1gMnPc1	svišťoun
se	se	k3xPyFc4	se
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
svišťouny	svišťoun	k1gInPc4	svišťoun
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
řád	řád	k1gInSc4	řád
kolibříci	kolibřík	k1gMnPc1	kolibřík
(	(	kIx(	(
<g/>
Trochiliformes	Trochiliformes	k1gMnSc1	Trochiliformes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
krátkokřídlých	krátkokřídlí	k1gMnPc2	krátkokřídlí
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
řád	řád	k1gInSc1	řád
perepelové	perepelové	k2eAgInSc1d1	perepelové
(	(	kIx(	(
<g/>
Turniciformes	Turniciformes	k1gInSc1	Turniciformes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stepokurové	stepokur	k1gMnPc1	stepokur
(	(	kIx(	(
<g/>
Pteroclidiformes	Pteroclidiformes	k1gMnSc1	Pteroclidiformes
<g/>
)	)	kIx)	)
a	a	k8xC	a
turakové	turako	k1gMnPc1	turako
(	(	kIx(	(
<g/>
Musophagiformes	Musophagiformes	k1gInSc1	Musophagiformes
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
byli	být	k5eAaImAgMnP	být
uznáni	uznán	k2eAgMnPc1d1	uznán
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Sibley-Ahlquistova	Sibley-Ahlquistův	k2eAgFnSc1d1	Sibley-Ahlquistův
systematika	systematika	k1gFnSc1	systematika
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
DNA-DNA	DNA-DNA	k1gFnSc6	DNA-DNA
hybridizaci	hybridizace	k1gFnSc6	hybridizace
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
od	od	k7c2	od
tradice	tradice	k1gFnSc2	tradice
odchýlila	odchýlit	k5eAaPmAgFnS	odchýlit
<g/>
:	:	kIx,	:
podtřída	podtřída	k1gFnSc1	podtřída
<g/>
:	:	kIx,	:
Palaeognathae	Palaeognathae	k1gInSc1	Palaeognathae
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
pštrosi	pštros	k1gMnPc1	pštros
(	(	kIx(	(
<g/>
Struthioniformes	Struthioniformes	k1gMnSc1	Struthioniformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
tinamy	tinama	k1gFnPc1	tinama
(	(	kIx(	(
<g/>
Tinamiformes	Tinamiformes	k1gInSc1	Tinamiformes
<g/>
)	)	kIx)	)
podtřída	podtřída	k1gFnSc1	podtřída
<g/>
:	:	kIx,	:
Neognathae	Neognathae	k1gInSc1	Neognathae
taxon	taxon	k1gInSc1	taxon
<g/>
:	:	kIx,	:
Galloanserae	Galloanserae	k1gInSc1	Galloanserae
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Galliformes	Galliformes	k1gInSc1	Galliformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
vrubozobí	vrubozobí	k1gMnPc1	vrubozobí
(	(	kIx(	(
<g/>
Anseriformes	Anseriformes	k1gMnSc1	Anseriformes
<g/>
)	)	kIx)	)
taxon	taxon	k1gInSc1	taxon
<g/>
:	:	kIx,	:
Neoaves	Neoaves	k1gInSc1	Neoaves
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
perepelové	perepelové	k2eAgInSc1d1	perepelové
(	(	kIx(	(
<g/>
Turniciformes	Turniciformes	k1gInSc1	Turniciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
šplhavci	šplhavec	k1gMnPc1	šplhavec
(	(	kIx(	(
<g/>
Piciformes	Piciformes	k1gMnSc1	Piciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
leskovci	leskovec	k1gMnPc1	leskovec
(	(	kIx(	(
<g/>
Galbuliformes	Galbuliformes	k1gMnSc1	Galbuliformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
zoborožci	zoborožec	k1gMnPc1	zoborožec
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Bucerotiformes	Bucerotiformes	k1gInSc1	Bucerotiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
dudci	dudek	k1gMnPc1	dudek
(	(	kIx(	(
<g/>
Upupiformes	Upupiformes	k1gMnSc1	Upupiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
trogoni	trogon	k1gMnPc1	trogon
(	(	kIx(	(
<g/>
Trogoniformes	Trogoniformes	k1gMnSc1	Trogoniformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
srostloprstí	srostloprstý	k2eAgMnPc1d1	srostloprstý
(	(	kIx(	(
<g/>
Coraciiformes	Coraciiformes	k1gMnSc1	Coraciiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
myšáci	myšák	k1gMnPc1	myšák
(	(	kIx(	(
<g/>
Coliiformes	Coliiformes	k1gMnSc1	Coliiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
kukačky	kukačka	k1gFnPc1	kukačka
(	(	kIx(	(
<g/>
Cuculiformes	Cuculiformes	k1gInSc1	Cuculiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
papoušci	papoušek	k1gMnPc1	papoušek
(	(	kIx(	(
<g/>
Psittaciformes	Psittaciformes	k1gMnSc1	Psittaciformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
svišťouni	svišťoun	k1gMnPc1	svišťoun
(	(	kIx(	(
<g/>
Apodiformes	Apodiformes	k1gMnSc1	Apodiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
kolibříci	kolibřík	k1gMnPc1	kolibřík
(	(	kIx(	(
<g/>
Trochiliformes	Trochiliformes	k1gMnSc1	Trochiliformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
turakové	turako	k1gMnPc1	turako
(	(	kIx(	(
<g/>
Musophagiformes	Musophagiformes	k1gMnSc1	Musophagiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
sovy	sova	k1gFnPc1	sova
(	(	kIx(	(
<g/>
Strigiformes	Strigiformes	k1gInSc1	Strigiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
měkkozobí	měkkozobit	k5eAaPmIp3nS	měkkozobit
(	(	kIx(	(
<g/>
Columbiformes	Columbiformes	k1gInSc1	Columbiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
krátkokřídlí	krátkokřídlí	k1gMnPc1	krátkokřídlí
(	(	kIx(	(
<g/>
Gruiformes	Gruiformes	k1gMnSc1	Gruiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
brodiví	brodivý	k2eAgMnPc1d1	brodivý
(	(	kIx(	(
<g/>
Ciconiiformes	Ciconiiformes	k1gMnSc1	Ciconiiformes
<g/>
)	)	kIx)	)
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
pěvci	pěvec	k1gMnPc1	pěvec
(	(	kIx(	(
<g/>
Passeriformes	Passeriformes	k1gInSc1	Passeriformes
<g/>
)	)	kIx)	)
Lelkové	lelek	k1gMnPc1	lelek
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
součástí	součást	k1gFnSc7	součást
řádu	řád	k1gInSc2	řád
sov	sova	k1gFnPc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhokřídlí	Dlouhokřídlý	k2eAgMnPc1d1	Dlouhokřídlý
<g/>
,	,	kIx,	,
plameňáci	plameňák	k1gMnPc1	plameňák
<g/>
,	,	kIx,	,
trubkonosí	trubkonosý	k2eAgMnPc1d1	trubkonosý
<g/>
,	,	kIx,	,
tučnáci	tučnák	k1gMnPc1	tučnák
<g/>
,	,	kIx,	,
veslonozí	veslonozí	k1gMnPc1	veslonozí
<g/>
,	,	kIx,	,
potáplice	potáplice	k1gFnPc1	potáplice
<g/>
,	,	kIx,	,
potápky	potápka	k1gFnPc1	potápka
a	a	k8xC	a
dravci	dravec	k1gMnPc1	dravec
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
součástí	součást	k1gFnPc2	součást
řádu	řád	k1gInSc3	řád
brodiví	brodivý	k2eAgMnPc5d1	brodivý
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
Livezeye	Livezey	k1gMnSc2	Livezey
a	a	k8xC	a
Zusiho	Zusi	k1gMnSc2	Zusi
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
setkala	setkat	k5eAaPmAgNnP	setkat
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
<g/>
:	:	kIx,	:
kódování	kódování	k1gNnSc4	kódování
některých	některý	k3yIgInPc2	některý
taxonů	taxon	k1gInPc2	taxon
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
analýze	analýza	k1gFnSc6	analýza
bylo	být	k5eAaImAgNnS	být
chybné	chybný	k2eAgNnSc1d1	chybné
<g/>
,	,	kIx,	,
zásadní	zásadní	k2eAgInPc4d1	zásadní
uzly	uzel	k1gInPc4	uzel
na	na	k7c6	na
výsledném	výsledný	k2eAgInSc6d1	výsledný
kladogramu	kladogram	k1gInSc6	kladogram
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spojovaly	spojovat	k5eAaImAgInP	spojovat
několik	několik	k4yIc4	několik
taxonů	taxon	k1gInPc2	taxon
na	na	k7c6	na
"	"	kIx"	"
<g/>
úrovni	úroveň	k1gFnSc6	úroveň
nadřádu	nadřád	k1gInSc2	nadřád
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
klasifikace	klasifikace	k1gFnSc1	klasifikace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
autoři	autor	k1gMnPc1	autor
předložili	předložit	k5eAaPmAgMnP	předložit
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
několik	několik	k4yIc4	několik
parafyletických	parafyletický	k2eAgInPc2d1	parafyletický
taxonů	taxon	k1gInPc2	taxon
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
vlastní	vlastní	k2eAgFnSc1d1	vlastní
analýza	analýza	k1gFnSc1	analýza
nenašla	najít	k5eNaPmAgFnS	najít
žádnou	žádný	k3yNgFnSc4	žádný
podporu	podpora	k1gFnSc4	podpora
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Palaeoaves	Palaeoaves	k1gInSc4	Palaeoaves
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
všechny	všechen	k3xTgMnPc4	všechen
druhohorní	druhohorní	k2eAgMnPc4d1	druhohorní
ptáky	pták	k1gMnPc4	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
systematika	systematika	k1gFnSc1	systematika
přitom	přitom	k6eAd1	přitom
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
pouze	pouze	k6eAd1	pouze
monofyletické	monofyletický	k2eAgFnPc4d1	monofyletická
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
klady	klad	k1gInPc7	klad
<g/>
.	.	kIx.	.
</s>
<s>
Livezey	Livezea	k1gFnPc1	Livezea
a	a	k8xC	a
Zusi	Zus	k1gFnPc1	Zus
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
přesto	přesto	k8xC	přesto
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc4	několik
fylogenetických	fylogenetický	k2eAgFnPc2d1	fylogenetická
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
odhalených	odhalený	k2eAgFnPc2d1	odhalená
dříve	dříve	k6eAd2	dříve
analýzami	analýza	k1gFnPc7	analýza
genetických	genetický	k2eAgFnPc2d1	genetická
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
i	i	k9	i
v	v	k7c6	v
morfologii	morfologie	k1gFnSc6	morfologie
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
sedmi	sedm	k4xCc6	sedm
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největší	veliký	k2eAgFnPc1d3	veliký
rozmanitosti	rozmanitost	k1gFnPc1	rozmanitost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
buď	buď	k8xC	buď
vyšší	vysoký	k2eAgInSc1d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
speciace	speciace	k1gFnSc1	speciace
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rychlostí	rychlost	k1gFnSc7	rychlost
vymírání	vymírání	k1gNnSc2	vymírání
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
žít	žít	k5eAaImF	žít
a	a	k8xC	a
nacházet	nacházet	k5eAaImF	nacházet
potravu	potrava	k1gFnSc4	potrava
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
lokalit	lokalita	k1gFnPc2	lokalita
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
např.	např.	kA	např.
sněžní	sněžní	k2eAgMnPc1d1	sněžní
buřňáci	buřňák	k1gMnPc1	buřňák
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
koloniích	kolonie	k1gFnPc6	kolonie
až	až	k9	až
440	[number]	k4	440
km	km	kA	km
hluboko	hluboko	k6eAd1	hluboko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
adaptovalo	adaptovat	k5eAaBmAgNnS	adaptovat
na	na	k7c4	na
život	život	k1gInSc4	život
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
oceánech	oceán	k1gInPc6	oceán
–	–	k?	–
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
druhy	druh	k1gMnPc7	druh
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
setkáme	setkat	k5eAaPmIp1nP	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
tučňáků	tučňák	k1gMnPc2	tučňák
se	se	k3xPyFc4	se
potápějí	potápět	k5eAaImIp3nP	potápět
do	do	k7c2	do
rekordních	rekordní	k2eAgFnPc2d1	rekordní
hloubek	hloubka	k1gFnPc2	hloubka
okolo	okolo	k7c2	okolo
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
kolonizovalo	kolonizovat	k5eAaBmAgNnS	kolonizovat
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
byly	být	k5eAaImAgFnP	být
vysazeny	vysadit	k5eAaPmNgFnP	vysadit
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
zavádění	zavádění	k1gNnPc2	zavádění
byla	být	k5eAaImAgFnS	být
dobře	dobře	k6eAd1	dobře
uvážena	uvážen	k2eAgFnSc1d1	uvážen
<g/>
:	:	kIx,	:
např.	např.	kA	např.
bažant	bažant	k1gMnSc1	bažant
obecný	obecný	k2eAgMnSc1d1	obecný
byl	být	k5eAaImAgMnS	být
vysazován	vysazován	k2eAgMnSc1d1	vysazován
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jako	jako	k8xC	jako
lovná	lovný	k2eAgFnSc1d1	lovná
pernatá	pernatý	k2eAgFnSc1d1	pernatá
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
náhodní	náhodný	k2eAgMnPc1d1	náhodný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
papoušek	papoušek	k1gMnSc1	papoušek
mniší	mniší	k2eAgMnSc1d1	mniší
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
útěcích	útěk	k1gInPc6	útěk
z	z	k7c2	z
klecí	klec	k1gFnPc2	klec
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
kolonie	kolonie	k1gFnSc2	kolonie
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
severoamerických	severoamerický	k2eAgNnPc6d1	severoamerické
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
volavka	volavka	k1gFnSc1	volavka
rusohlavá	rusohlavý	k2eAgFnSc1d1	rusohlavá
<g/>
,	,	kIx,	,
čimango	čimango	k6eAd1	čimango
žlutavý	žlutavý	k2eAgMnSc1d1	žlutavý
a	a	k8xC	a
kakadu	kakadu	k1gMnSc1	kakadu
růžový	růžový	k2eAgInSc4d1	růžový
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
hluboko	hluboko	k6eAd1	hluboko
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
výskytu	výskyt	k1gInSc2	výskyt
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jim	on	k3xPp3gMnPc3	on
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
činnost	činnost	k1gFnSc1	činnost
člověka	člověk	k1gMnSc2	člověk
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nové	nový	k2eAgFnPc4d1	nová
vhodné	vhodný	k2eAgFnPc4d1	vhodná
lokality	lokalita	k1gFnPc4	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Anatomie	anatomie	k1gFnSc1	anatomie
ptáků	pták	k1gMnPc2	pták
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
obratlovci	obratlovec	k1gMnPc7	obratlovec
mají	mít	k5eAaImIp3nP	mít
ptáci	pták	k1gMnPc1	pták
uspořádání	uspořádání	k1gNnSc4	uspořádání
těla	tělo	k1gNnSc2	tělo
vykazující	vykazující	k2eAgFnSc2d1	vykazující
mnoho	mnoho	k4c1	mnoho
neobvyklých	obvyklý	k2eNgNnPc2d1	neobvyklé
přizpůsobení	přizpůsobení	k1gNnPc2	přizpůsobení
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
usnadnění	usnadnění	k1gNnSc3	usnadnění
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
lehkých	lehký	k2eAgFnPc2d1	lehká
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgFnPc4d1	dutá
a	a	k8xC	a
vyplněné	vyplněný	k2eAgFnPc4d1	vyplněná
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dutiny	dutina	k1gFnPc1	dutina
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
dýchacím	dýchací	k2eAgNnSc7d1	dýchací
ústrojím	ústrojí	k1gNnSc7	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
duté	dutý	k2eAgFnPc1d1	dutá
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
.	.	kIx.	.
</s>
<s>
Lebeční	lebeční	k2eAgFnPc1d1	lebeční
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgFnPc1d1	spojená
a	a	k8xC	a
bez	bez	k7c2	bez
zřetelných	zřetelný	k2eAgInPc2d1	zřetelný
švů	šev	k1gInPc2	šev
<g/>
.	.	kIx.	.
</s>
<s>
Očnice	očnice	k1gFnPc1	očnice
jsou	být	k5eAaImIp3nP	být
mohutné	mohutný	k2eAgInPc1d1	mohutný
a	a	k8xC	a
oddělené	oddělený	k2eAgInPc1d1	oddělený
kostní	kostní	k2eAgFnSc7d1	kostní
přepážkou	přepážka	k1gFnSc7	přepážka
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
krční	krční	k2eAgInSc4d1	krční
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgInSc4d1	hrudní
<g/>
,	,	kIx,	,
křížový	křížový	k2eAgInSc4d1	křížový
a	a	k8xC	a
ocasní	ocasní	k2eAgInSc4d1	ocasní
oddíl	oddíl	k1gInSc4	oddíl
s	s	k7c7	s
ohebnými	ohebný	k2eAgInPc7d1	ohebný
krčními	krční	k2eAgInPc7d1	krční
obratli	obratel	k1gInPc7	obratel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
u	u	k7c2	u
obratlů	obratel	k1gInPc2	obratel
předcházejícím	předcházející	k2eAgInPc3d1	předcházející
hrudním	hrudní	k2eAgInPc3d1	hrudní
obratlům	obratel	k1gInPc3	obratel
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Kost	kost	k1gFnSc1	kost
kyčelní	kyčelní	k2eAgFnSc1d1	kyčelní
je	být	k5eAaImIp3nS	být
srostlá	srostlý	k2eAgFnSc1d1	srostlá
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
s	s	k7c7	s
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
jsou	být	k5eAaImIp3nP	být
zploštěná	zploštěný	k2eAgNnPc1d1	zploštěné
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
upnuta	upnut	k2eAgNnPc1d1	upnuto
k	k	k7c3	k
mohutné	mohutný	k2eAgFnSc3d1	mohutná
deskovité	deskovitý	k2eAgFnSc3d1	deskovitá
prsní	prsní	k2eAgFnSc3d1	prsní
kosti	kost	k1gFnSc3	kost
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgInP	připojit
létací	létací	k2eAgInPc1d1	létací
svaly	sval	k1gInPc1	sval
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
nelétaví	létavý	k2eNgMnPc1d1	nelétavý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
prsní	prsní	k2eAgFnSc4d1	prsní
kost	kost	k1gFnSc4	kost
poměrně	poměrně	k6eAd1	poměrně
malou	malý	k2eAgFnSc4d1	malá
a	a	k8xC	a
bez	bez	k7c2	bez
hřebene	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ptáci	pták	k1gMnPc1	pták
nemočí	močit	k5eNaImIp3nP	močit
<g/>
.	.	kIx.	.
</s>
<s>
Ledviny	ledvina	k1gFnPc1	ledvina
extrahují	extrahovat	k5eAaBmIp3nP	extrahovat
z	z	k7c2	z
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
dusíkatý	dusíkatý	k2eAgInSc4d1	dusíkatý
odpad	odpad	k1gInSc4	odpad
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
místo	místo	k7c2	místo
vylučování	vylučování	k1gNnSc2	vylučování
jako	jako	k8xS	jako
močoviny	močovina	k1gFnSc2	močovina
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
je	být	k5eAaImIp3nS	být
vylučován	vylučovat	k5eAaImNgInS	vylučovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
také	také	k9	také
vyměšují	vyměšovat	k5eAaImIp3nP	vyměšovat
spíše	spíše	k9	spíše
kreatin	kreatin	k1gInSc4	kreatin
než	než	k8xS	než
kreatinin	kreatinin	k1gInSc4	kreatinin
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
zbavení	zbavení	k1gNnSc6	zbavení
se	s	k7c7	s
zbytku	zbytek	k1gInSc2	zbytek
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
bílý	bílý	k2eAgInSc4d1	bílý
povlak	povlak	k1gInSc4	povlak
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
každé	každý	k3xTgFnSc2	každý
ledviny	ledvina	k1gFnSc2	ledvina
vede	vést	k5eAaImIp3nS	vést
močovod	močovod	k1gInSc1	močovod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
oddílu	oddíl	k1gInSc2	oddíl
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
kloace	kloaka	k1gFnSc6	kloaka
se	se	k3xPyFc4	se
moč	moč	k1gFnSc1	moč
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
trusu	trus	k1gInSc2	trus
je	být	k5eAaImIp3nS	být
stírána	stírán	k2eAgFnSc1d1	stírána
a	a	k8xC	a
s	s	k7c7	s
trusem	trus	k1gInSc7	trus
vylučována	vylučovat	k5eAaImNgFnS	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trusu	trus	k1gInSc6	trus
se	se	k3xPyFc4	se
moč	moč	k1gFnSc1	moč
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
bílý	bílý	k2eAgInSc4d1	bílý
povlak	povlak	k1gInSc4	povlak
<g/>
.	.	kIx.	.
</s>
<s>
Kloaka	kloaka	k1gFnSc1	kloaka
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
vývodem	vývod	k1gInSc7	vývod
trávicí	trávicí	k2eAgFnSc2d1	trávicí
<g/>
,	,	kIx,	,
vylučovací	vylučovací	k2eAgFnSc2d1	vylučovací
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
nestravitelné	stravitelný	k2eNgInPc1d1	nestravitelný
zbytky	zbytek	k1gInPc1	zbytek
potravy	potrava	k1gFnSc2	potrava
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
v	v	k7c6	v
chuchvalcích	chuchvalec	k1gInPc6	chuchvalec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vývržky	vývržek	k1gInPc1	vývržek
<g/>
)	)	kIx)	)
zpět	zpět	k6eAd1	zpět
jícnem	jícen	k1gInSc7	jícen
a	a	k8xC	a
ústní	ústní	k2eAgFnSc7d1	ústní
dutinou	dutina	k1gFnSc7	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejkomplexnějších	komplexní	k2eAgNnPc2d3	nejkomplexnější
dýchacích	dýchací	k2eAgNnPc2d1	dýchací
ústrojí	ústrojí	k1gNnPc2	ústrojí
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pták	pták	k1gMnSc1	pták
nadechuje	nadechovat	k5eAaImIp3nS	nadechovat
<g/>
,	,	kIx,	,
75	[number]	k4	75
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
vzduchu	vzduch	k1gInSc2	vzduch
obtéká	obtékat	k5eAaImIp3nS	obtékat
plíce	plíce	k1gFnPc1	plíce
a	a	k8xC	a
vtéká	vtékat	k5eAaImIp3nS	vtékat
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
probíhajících	probíhající	k2eAgInPc2d1	probíhající
od	od	k7c2	od
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
dutinami	dutina	k1gFnPc7	dutina
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
plní	plnit	k5eAaImIp3nP	plnit
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
25	[number]	k4	25
%	%	kIx~	%
vzduchu	vzduch	k1gInSc2	vzduch
jde	jít	k5eAaImIp3nS	jít
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pták	pták	k1gMnSc1	pták
vydechuje	vydechovat	k5eAaImIp3nS	vydechovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
výdech	výdech	k1gInSc4	výdech
používán	používán	k2eAgInSc4d1	používán
vzduch	vzduch	k1gInSc4	vzduch
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
dostává	dostávat	k5eAaImIp3nS	dostávat
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
vzduch	vzduch	k1gInSc1	vzduch
ze	z	k7c2	z
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnPc1d1	ptačí
plíce	plíce	k1gFnPc1	plíce
tak	tak	k6eAd1	tak
přijímají	přijímat	k5eAaImIp3nP	přijímat
čerstvý	čerstvý	k2eAgInSc4d1	čerstvý
vzduch	vzduch	k1gInSc4	vzduch
jak	jak	k8xS	jak
při	při	k7c6	při
nádechu	nádech	k1gInSc6	nádech
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
při	při	k7c6	při
výdechu	výdech	k1gInSc6	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgInSc1d1	ptačí
zpěv	zpěv	k1gInSc1	zpěv
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
orgánu	orgán	k1gInSc6	orgán
zvaném	zvaný	k2eAgInSc6d1	zvaný
syrinx	syrinx	k1gInSc1	syrinx
<g/>
,	,	kIx,	,
svalnaté	svalnatý	k2eAgFnSc3d1	svalnatá
komoře	komora	k1gFnSc3	komora
s	s	k7c7	s
několika	několik	k4yIc7	několik
bubínkovými	bubínkový	k2eAgFnPc7d1	bubínková
blánami	blána	k1gFnPc7	blána
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgNnSc1d1	ptačí
srdce	srdce	k1gNnSc1	srdce
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
oddíly	oddíl	k1gInPc4	oddíl
<g/>
,	,	kIx,	,
z	z	k7c2	z
levé	levá	k1gFnSc2	levá
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
svalnaté	svalnatý	k2eAgFnSc2d1	svalnatá
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pravý	pravý	k2eAgInSc4d1	pravý
aortální	aortální	k2eAgInSc4d1	aortální
oblouk	oblouk	k1gInSc4	oblouk
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
systémové	systémový	k2eAgFnSc3d1	systémová
aortě	aorta	k1gFnSc3	aorta
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
levý	levý	k2eAgInSc1d1	levý
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
dutá	dutý	k2eAgFnSc1d1	dutá
žíla	žíla	k1gFnSc1	žíla
přijímá	přijímat	k5eAaImIp3nS	přijímat
krev	krev	k1gFnSc4	krev
z	z	k7c2	z
končetin	končetina	k1gFnPc2	končetina
přes	přes	k7c4	přes
vstupní	vstupní	k2eAgInSc4d1	vstupní
ledvinový	ledvinový	k2eAgInSc4d1	ledvinový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jádrové	jádrový	k2eAgFnPc4d1	jádrová
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Zažívací	zažívací	k2eAgFnSc1d1	zažívací
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
hrdelní	hrdelní	k2eAgInSc4d1	hrdelní
vak	vak	k1gInSc4	vak
nebo	nebo	k8xC	nebo
vole	vole	k1gNnSc4	vole
pro	pro	k7c4	pro
uskladnění	uskladnění	k1gNnSc4	uskladnění
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
žaludky	žaludek	k1gInPc4	žaludek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
jsou	být	k5eAaImIp3nP	být
spolykané	spolykaný	k2eAgInPc4d1	spolykaný
kamínky	kamínek	k1gInPc4	kamínek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
ptákům	pták	k1gMnPc3	pták
drtit	drtit	k5eAaImF	drtit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
soustava	soustava	k1gFnSc1	soustava
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zajistit	zajistit	k5eAaPmF	zajistit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
strávení	strávení	k1gNnSc4	strávení
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
stěhovaví	stěhovavý	k2eAgMnPc1d1	stěhovavý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
přidanou	přidaný	k2eAgFnSc4d1	přidaná
schopnost	schopnost	k1gFnSc4	schopnost
snížit	snížit	k5eAaPmF	snížit
obsah	obsah	k1gInSc4	obsah
části	část	k1gFnSc2	část
střev	střevo	k1gNnPc2	střevo
před	před	k7c7	před
migrací	migrace	k1gFnSc7	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
přiměřeném	přiměřený	k2eAgInSc6d1	přiměřený
velikosti	velikost	k1gFnSc6	velikost
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyvinutější	vyvinutý	k2eAgFnSc7d3	nejvyvinutější
částí	část	k1gFnSc7	část
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
funkce	funkce	k1gFnPc4	funkce
souvisící	souvisící	k2eAgFnPc4d1	souvisící
s	s	k7c7	s
letem	let	k1gInSc7	let
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mozeček	mozeček	k1gInSc1	mozeček
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
pohyb	pohyb	k1gInSc1	pohyb
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
mozek	mozek	k1gInSc1	mozek
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
prvky	prvek	k1gInPc4	prvek
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc2	plavání
<g/>
,	,	kIx,	,
páření	páření	k1gNnSc2	páření
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Zrakový	zrakový	k2eAgInSc1d1	zrakový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
vysoce	vysoce	k6eAd1	vysoce
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
stavbě	stavba	k1gFnSc6	stavba
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
okem	oke	k1gNnSc7	oke
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
očima	oko	k1gNnPc7	oko
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
mají	mít	k5eAaImIp3nP	mít
široké	široký	k2eAgNnSc4d1	široké
zorné	zorný	k2eAgNnSc4d1	zorné
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
očima	oko	k1gNnPc7	oko
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
sovy	sova	k1gFnPc1	sova
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
binokulární	binokulární	k2eAgMnPc1d1	binokulární
vidění	vidění	k1gNnSc3	vidění
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
odhadovat	odhadovat	k5eAaImF	odhadovat
hloubku	hloubka	k1gFnSc4	hloubka
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgMnPc1d1	vodní
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
pružné	pružný	k2eAgFnPc4d1	pružná
čočky	čočka	k1gFnPc4	čočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jim	on	k3xPp3gMnPc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vidění	vidění	k1gNnSc4	vidění
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
i	i	k8xC	i
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
foevu	foeva	k1gFnSc4	foeva
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
tetrachromatičtí	tetrachromatický	k2eAgMnPc1d1	tetrachromatický
díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
očních	oční	k2eAgFnPc2d1	oční
buněk	buňka	k1gFnPc2	buňka
citlivých	citlivý	k2eAgInPc2d1	citlivý
na	na	k7c4	na
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
modrou	modrý	k2eAgFnSc4d1	modrá
a	a	k8xC	a
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vnímat	vnímat	k5eAaImF	vnímat
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaPmNgNnS	využívat
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
a	a	k8xC	a
námluvách	námluva	k1gFnPc6	námluva
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
na	na	k7c4	na
peří	peří	k1gNnSc4	peří
vzory	vzor	k1gInPc4	vzor
viditelné	viditelný	k2eAgInPc4d1	viditelný
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
světle	světlo	k1gNnSc6	světlo
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nerozeznatelné	rozeznatelný	k2eNgNnSc1d1	nerozeznatelné
lidským	lidský	k2eAgNnSc7d1	lidské
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
také	také	k9	také
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
<g/>
;	;	kIx,	;
např.	např.	kA	např.
poštolky	poštolka	k1gFnSc2	poštolka
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
kořisti	kořist	k1gFnSc6	kořist
často	často	k6eAd1	často
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
sledují	sledovat	k5eAaImIp3nP	sledovat
močovou	močový	k2eAgFnSc4d1	močová
stopu	stopa	k1gFnSc4	stopa
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc4d1	vnější
ochranu	ochrana	k1gFnSc4	ochrana
oka	oko	k1gNnSc2	oko
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
svrchní	svrchní	k2eAgNnSc1d1	svrchní
a	a	k8xC	a
spodní	spodní	k2eAgNnSc1d1	spodní
víčko	víčko	k1gNnSc1	víčko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
jako	jako	k8xC	jako
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
čištěno	čistit	k5eAaImNgNnS	čistit
jemnou	jemný	k2eAgFnSc7d1	jemná
blankou	blanka	k1gFnSc7	blanka
<g/>
,	,	kIx,	,
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
koutku	koutek	k1gInSc6	koutek
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mžurkou	mžurka	k1gFnSc7	mžurka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
oko	oko	k1gNnSc4	oko
překrýt	překrýt	k5eAaPmF	překrýt
směrem	směr	k1gInSc7	směr
napříč	napříč	k6eAd1	napříč
<g/>
.	.	kIx.	.
</s>
<s>
Mžurka	mžurka	k1gFnSc1	mžurka
také	také	k9	také
kryje	krýt	k5eAaImIp3nS	krýt
oko	oko	k1gNnSc1	oko
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k6eAd1	tak
jako	jako	k9	jako
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
čočka	čočka	k1gFnSc1	čočka
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ptáci	pták	k1gMnPc1	pták
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
spodní	spodní	k2eAgNnPc1d1	spodní
víčka	víčko	k1gNnPc1	víčko
zvednutá	zvednutý	k2eAgNnPc1d1	zvednuté
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
sítnice	sítnice	k1gFnSc1	sítnice
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
rozvod	rozvod	k1gInSc4	rozvod
krve	krev	k1gFnSc2	krev
umístěný	umístěný	k2eAgInSc4d1	umístěný
ve	v	k7c6	v
sklivci	sklivec	k1gInSc6	sklivec
bohatě	bohatě	k6eAd1	bohatě
prokrvený	prokrvený	k2eAgInSc1d1	prokrvený
výrůstek	výrůstek	k1gInSc1	výrůstek
zvaný	zvaný	k2eAgInSc1d1	zvaný
hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
.	.	kIx.	.
</s>
<s>
Ptačímu	ptačí	k2eAgInSc3d1	ptačí
zevnímu	zevní	k2eAgInSc3d1	zevní
zvukovodu	zvukovod	k1gInSc3	zvukovod
chybí	chybit	k5eAaPmIp3nS	chybit
vnější	vnější	k2eAgInSc4d1	vnější
ušní	ušní	k2eAgInSc4d1	ušní
boltec	boltec	k1gInSc4	boltec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
překryt	překrýt	k5eAaPmNgInS	překrýt
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kalous	kalous	k1gMnSc1	kalous
<g/>
,	,	kIx,	,
výr	výr	k1gMnSc1	výr
<g/>
,	,	kIx,	,
výreček	výreček	k1gMnSc1	výreček
<g/>
)	)	kIx)	)
toto	tento	k3xDgNnSc1	tento
peří	peřit	k5eAaImIp3nS	peřit
trochu	trochu	k6eAd1	trochu
uši	ucho	k1gNnPc4	ucho
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
uchu	ucho	k1gNnSc6	ucho
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
hlemýžď	hlemýžď	k1gMnSc1	hlemýžď
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ale	ale	k9	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
savců	savec	k1gMnPc2	savec
není	být	k5eNaImIp3nS	být
spirálovitý	spirálovitý	k2eAgInSc1d1	spirálovitý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
má	mít	k5eAaImIp3nS	mít
slabě	slabě	k6eAd1	slabě
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
kiviové	kivi	k1gMnPc1	kivi
<g/>
,	,	kIx,	,
supové	sup	k1gMnPc1	sup
či	či	k8xC	či
trubkonosí	trubkonosý	k2eAgMnPc1d1	trubkonosý
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
pachy	pach	k1gInPc4	pach
zásadní	zásadní	k2eAgInPc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
orientaci	orientace	k1gFnSc6	orientace
či	či	k8xC	či
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
proti	proti	k7c3	proti
dravcům	dravec	k1gMnPc3	dravec
chemickou	chemický	k2eAgFnSc4d1	chemická
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
trubkonosí	trubkonosý	k2eAgMnPc1d1	trubkonosý
mohou	moct	k5eAaImIp3nP	moct
vyvrhnout	vyvrhnout	k5eAaPmF	vyvrhnout
proti	proti	k7c3	proti
útočníkovi	útočník	k1gMnSc3	útočník
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
olejovitý	olejovitý	k2eAgInSc1d1	olejovitý
výměšek	výměšek	k1gInSc1	výměšek
z	z	k7c2	z
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
pěvců	pěvec	k1gMnPc2	pěvec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Pitohui	Pitohui	k1gNnSc2	Pitohui
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c4	na
Nové	Nové	k2eAgNnSc4d1	Nové
Guinei	Guinei	k1gNnSc4	Guinei
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jedovatou	jedovatý	k2eAgFnSc4d1	jedovatá
látku	látka	k1gFnSc4	látka
homobatrachotoxin	homobatrachotoxina	k1gFnPc2	homobatrachotoxina
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
a	a	k8xC	a
peří	peří	k1gNnSc6	peří
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
články	článek	k1gInPc1	článek
<g/>
:	:	kIx,	:
Peří	peří	k1gNnSc1	peří
a	a	k8xC	a
Let	let	k1gInSc1	let
pomocí	pomoc	k1gFnPc2	pomoc
peří	peřit	k5eAaImIp3nS	peřit
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
ptáky	pták	k1gMnPc4	pták
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
žijících	žijící	k2eAgFnPc2d1	žijící
skupin	skupina	k1gFnPc2	skupina
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokrytí	pokrytí	k1gNnSc1	pokrytí
kůže	kůže	k1gFnSc2	kůže
peřím	peřit	k5eAaImIp1nS	peřit
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
epidermální	epidermální	k2eAgInSc1d1	epidermální
kožní	kožní	k2eAgInSc1d1	kožní
porost	porost	k1gInSc1	porost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
šupiny	šupina	k1gFnPc1	šupina
u	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
nebo	nebo	k8xC	nebo
srst	srst	k1gFnSc4	srst
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
mnoho	mnoho	k4c4	mnoho
různorodých	různorodý	k2eAgFnPc2d1	různorodá
funkcí	funkce	k1gFnPc2	funkce
<g/>
:	:	kIx,	:
plní	plnit	k5eAaImIp3nP	plnit
termoregulační	termoregulační	k2eAgFnSc4d1	termoregulační
funkci	funkce	k1gFnSc4	funkce
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tepelně	tepelně	k6eAd1	tepelně
izoluje	izolovat	k5eAaBmIp3nS	izolovat
tělo	tělo	k1gNnSc4	tělo
ptáka	pták	k1gMnSc2	pták
za	za	k7c2	za
chladného	chladný	k2eAgNnSc2d1	chladné
počasí	počasí	k1gNnSc2	počasí
nebo	nebo	k8xC	nebo
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
ptačí	ptačí	k2eAgInSc4d1	ptačí
let	let	k1gInSc4	let
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
používáno	používat	k5eAaImNgNnS	používat
při	při	k7c6	při
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
maskování	maskování	k1gNnSc6	maskování
a	a	k8xC	a
signalizaci	signalizace	k1gFnSc6	signalizace
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgNnSc1d1	ptačí
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
slouží	sloužit	k5eAaImIp3nS	sloužit
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgInSc2	jeden
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
tak	tak	k6eAd1	tak
provádí	provádět	k5eAaImIp3nP	provádět
jeho	jeho	k3xOp3gNnSc4	jeho
čištění	čištění	k1gNnSc4	čištění
a	a	k8xC	a
rovnání	rovnání	k1gNnSc4	rovnání
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
používají	používat	k5eAaImIp3nP	používat
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
peří	peří	k1gNnSc4	peří
cizích	cizí	k2eAgFnPc2d1	cizí
částeček	částečka	k1gFnPc2	částečka
a	a	k8xC	a
roztírají	roztírat	k5eAaImIp3nP	roztírat
si	se	k3xPyFc3	se
po	po	k7c6	po
peří	peří	k1gNnSc6	peří
olejovité	olejovitý	k2eAgInPc4d1	olejovitý
výměšky	výměšek	k1gInPc4	výměšek
z	z	k7c2	z
kostrční	kostrční	k2eAgFnSc2d1	kostrční
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
peří	peří	k1gNnSc1	peří
nepromáčí	promáčet	k5eNaBmIp3nS	promáčet
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
mu	on	k3xPp3gMnSc3	on
pružnost	pružnost	k1gFnSc1	pružnost
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
antibakteriální	antibakteriální	k2eAgInSc1d1	antibakteriální
prostředek	prostředek	k1gInSc1	prostředek
potlačující	potlačující	k2eAgInSc4d1	potlačující
růst	růst	k1gInSc4	růst
baktérií	baktérie	k1gFnPc2	baktérie
snižujících	snižující	k2eAgFnPc2d1	snižující
kvalitu	kvalita	k1gFnSc4	kvalita
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
péče	péče	k1gFnSc2	péče
o	o	k7c6	o
peří	peří	k1gNnSc6	peří
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
kyseliny	kyselina	k1gFnSc2	kyselina
mravenčí	mravenčí	k2eAgFnSc2d1	mravenčí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
z	z	k7c2	z
peří	peří	k1gNnSc2	peří
zahánět	zahánět	k5eAaImF	zahánět
parazity	parazit	k1gMnPc7	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
se	se	k3xPyFc4	se
souvislým	souvislý	k2eAgInSc7d1	souvislý
praporem	prapor	k1gInSc7	prapor
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
vzhled	vzhled	k1gInSc4	vzhled
ptačího	ptačí	k2eAgNnSc2d1	ptačí
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
krycí	krycí	k2eAgNnSc1d1	krycí
nebo	nebo	k8xC	nebo
konturové	konturový	k2eAgNnSc1d1	konturové
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
peří	peří	k1gNnSc1	peří
má	mít	k5eAaImIp3nS	mít
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
osten	osten	k1gInSc4	osten
i	i	k8xC	i
brk	brk	k1gInSc4	brk
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
většina	většina	k1gFnSc1	většina
peří	peří	k1gNnSc2	peří
rostoucího	rostoucí	k2eAgNnSc2d1	rostoucí
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgInPc1d1	Krátké
a	a	k8xC	a
měkké	měkký	k2eAgInPc1d1	měkký
brky	brk	k1gInPc1	brk
s	s	k7c7	s
navzájem	navzájem	k6eAd1	navzájem
nespojenými	spojený	k2eNgInPc7d1	nespojený
paprsky	paprsek	k1gInPc7	paprsek
a	a	k8xC	a
větvičkami	větvička	k1gFnPc7	větvička
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc1	znak
prachového	prachový	k2eAgNnSc2d1	prachové
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
první	první	k4xOgInSc1	první
chmýřitý	chmýřitý	k2eAgInSc1d1	chmýřitý
šat	šat	k1gInSc1	šat
mladých	mladý	k2eAgMnPc2d1	mladý
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
také	také	k9	také
obal	obal	k1gInSc4	obal
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
peřím	peří	k1gNnSc7	peří
ptáků	pták	k1gMnPc2	pták
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
peří	peří	k1gNnSc1	peří
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
kolísat	kolísat	k5eAaImF	kolísat
podle	podle	k7c2	podle
stáří	stáří	k1gNnSc2	stáří
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
sociálního	sociální	k2eAgNnSc2d1	sociální
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc2	poškození
peří	peří	k1gNnSc2	peří
podle	podle	k7c2	podle
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
se	se	k3xPyFc4	se
jedinci	jedinec	k1gMnPc1	jedinec
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
opeření	opeření	k1gNnSc6	opeření
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nP	peřit
ptákům	pták	k1gMnPc3	pták
pravidelně	pravidelně	k6eAd1	pravidelně
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
vytlačováním	vytlačování	k1gNnSc7	vytlačování
starých	starý	k2eAgNnPc2d1	staré
per	pero	k1gNnPc2	pero
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
pery	pero	k1gNnPc7	pero
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Pelichání	pelichání	k1gNnSc1	pelichání
peří	peřit	k5eAaImIp3nS	peřit
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
probíhá	probíhat	k5eAaImIp3nS	probíhat
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
výměna	výměna	k1gFnSc1	výměna
peří	peří	k1gNnSc2	peří
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgInSc3	ten
velcí	velký	k2eAgMnPc1d1	velký
dravci	dravec	k1gMnPc1	dravec
mohou	moct	k5eAaImIp3nP	moct
pelichat	pelichat	k5eAaImF	pelichat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kachnám	kachna	k1gFnPc3	kachna
a	a	k8xC	a
husám	husa	k1gFnPc3	husa
pelichá	pelichat	k5eAaImIp3nS	pelichat
veškeré	veškerý	k3xTgNnSc4	veškerý
peří	peří	k1gNnSc4	peří
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
měsíce	měsíc	k1gInSc2	měsíc
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
ptačí	ptačí	k2eAgInPc1d1	ptačí
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc1d1	odlišný
vzory	vzor	k1gInPc1	vzor
a	a	k8xC	a
strategie	strategie	k1gFnSc1	strategie
línání	línání	k1gNnSc2	línání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pelichání	pelichání	k1gNnSc3	pelichání
od	od	k7c2	od
vnějších	vnější	k2eAgNnPc2d1	vnější
per	pero	k1gNnPc2	pero
k	k	k7c3	k
perům	pero	k1gNnPc3	pero
vnitřním	vnitřní	k2eAgNnPc3d1	vnitřní
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
naopak	naopak	k6eAd1	naopak
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
veškeré	veškerý	k3xTgNnSc4	veškerý
peří	peří	k1gNnSc4	peří
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
způsob	způsob	k1gInSc4	způsob
nebo	nebo	k8xC	nebo
také	také	k9	také
centripetální	centripetální	k2eAgNnSc4d1	centripetální
pelichání	pelichání	k1gNnSc4	pelichání
jako	jako	k8xC	jako
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
pelichání	pelichání	k1gNnPc4	pelichání
rejdovacích	rejdovací	k2eAgNnPc2d1	rejdovací
per	pero	k1gNnPc2	pero
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
u	u	k7c2	u
bažantovitých	bažantovitý	k2eAgMnPc2d1	bažantovitý
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
centrifugální	centrifugální	k2eAgNnSc4d1	centrifugální
pelichání	pelichání	k1gNnSc4	pelichání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
u	u	k7c2	u
rejdovacích	rejdovací	k2eAgNnPc2d1	rejdovací
per	pero	k1gNnPc2	pero
datlů	datel	k1gMnPc2	datel
a	a	k8xC	a
šoupálků	šoupálek	k1gMnPc2	šoupálek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
střední	střední	k1gMnPc1	střední
pár	pár	k4xCyI	pár
peří	peří	k1gNnSc2	peří
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
jako	jako	k9	jako
poslední	poslední	k2eAgNnSc1d1	poslední
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
těmto	tento	k3xDgMnPc3	tento
druhům	druh	k1gMnPc3	druh
umožněno	umožněn	k2eAgNnSc4d1	umožněno
šplhání	šplhání	k1gNnSc4	šplhání
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
vzor	vzor	k1gInSc1	vzor
způsobu	způsob	k1gInSc2	způsob
pelichání	pelichání	k1gNnSc2	pelichání
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
na	na	k7c6	na
vrabci	vrabec	k1gMnSc6	vrabec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
je	být	k5eAaImIp3nS	být
nahrazováno	nahrazován	k2eAgNnSc1d1	nahrazováno
vnější	vnější	k2eAgNnSc1d1	vnější
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
peří	peřit	k5eAaImIp3nS	peřit
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
u	u	k7c2	u
ocasních	ocasní	k2eAgNnPc2d1	ocasní
per	pero	k1gNnPc2	pero
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
ke	k	k7c3	k
krajům	kraj	k1gInPc3	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
neroste	růst	k5eNaImIp3nS	růst
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
se	se	k3xPyFc4	se
na	na	k7c6	na
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
plochách	plocha	k1gFnPc6	plocha
nebo	nebo	k8xC	nebo
pruzích	pruh	k1gInPc6	pruh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pernice	pernice	k?	pernice
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
pterylae	pterylae	k6eAd1	pterylae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Distribuční	distribuční	k2eAgInPc1d1	distribuční
vzorky	vzorek	k1gInPc1	vzorek
těchto	tento	k3xDgFnPc2	tento
plošek	ploška	k1gFnPc2	ploška
nebo	nebo	k8xC	nebo
proužků	proužek	k1gInPc2	proužek
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgInP	využívat
v	v	k7c6	v
taxonomii	taxonomie	k1gFnSc6	taxonomie
a	a	k8xC	a
systematice	systematika	k1gFnSc6	systematika
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hnízděním	hnízdění	k1gNnSc7	hnízdění
<g/>
,	,	kIx,	,
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
většiny	většina	k1gFnSc2	většina
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
styku	styk	k1gInSc2	styk
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
vejci	vejce	k1gNnPc7	vejce
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
hnízdní	hnízdní	k2eAgFnSc1d1	hnízdní
nažina	nažina	k1gFnSc1	nažina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
místo	místo	k1gNnSc4	místo
bez	bez	k7c2	bez
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
buď	buď	k8xC	buď
zcela	zcela	k6eAd1	zcela
holá	holý	k2eAgFnSc1d1	holá
<g/>
,	,	kIx,	,
či	či	k8xC	či
jenom	jenom	k9	jenom
řídce	řídce	k6eAd1	řídce
porostlá	porostlý	k2eAgFnSc1d1	porostlá
prachovým	prachový	k2eAgMnPc3d1	prachový
peřím	peřit	k5eAaImIp1nS	peřit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hodně	hodně	k6eAd1	hodně
prokrvená	prokrvený	k2eAgFnSc1d1	prokrvená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
usnadněn	usnadněn	k2eAgInSc4d1	usnadněn
transfer	transfer	k1gInSc4	transfer
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
let	léto	k1gNnPc2	léto
Schopnost	schopnost	k1gFnSc1	schopnost
letu	let	k1gInSc2	let
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
je	on	k3xPp3gInPc4	on
tak	tak	k9	tak
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skupin	skupina	k1gFnPc2	skupina
obratlovců	obratlovec	k1gMnPc2	obratlovec
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
netopýrů	netopýr	k1gMnPc2	netopýr
a	a	k8xC	a
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
pterosaurů	pterosaurus	k1gMnPc2	pterosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pro	pro	k7c4	pro
převažující	převažující	k2eAgNnSc4d1	převažující
množství	množství	k1gNnSc4	množství
druhů	druh	k1gInPc2	druh
hlavní	hlavní	k2eAgInSc1d1	hlavní
prostředek	prostředek	k1gInSc1	prostředek
pohybu	pohyb	k1gInSc2	pohyb
slouží	sloužit	k5eAaImIp3nS	sloužit
při	při	k7c6	při
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
<g/>
,	,	kIx,	,
lovu	lov	k1gInSc3	lov
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
útěku	útěk	k1gInSc2	útěk
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
letu	let	k1gInSc3	let
přizpůsobeni	přizpůsoben	k2eAgMnPc1d1	přizpůsoben
různě	různě	k6eAd1	různě
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
lehkou	lehký	k2eAgFnSc4d1	lehká
kostru	kostra	k1gFnSc4	kostra
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
prsní	prsní	k2eAgInPc1d1	prsní
svaly	sval	k1gInPc1	sval
zajišťující	zajišťující	k2eAgInSc1d1	zajišťující
pohyb	pohyb	k1gInSc4	pohyb
křídel	křídlo	k1gNnPc2	křídlo
dolů	dolů	k6eAd1	dolů
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
15	[number]	k4	15
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
ptáka	pták	k1gMnSc2	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svaly	sval	k1gInPc1	sval
podklíční	podklíčnět	k5eAaPmIp3nP	podklíčnět
zvedající	zvedající	k2eAgNnPc4d1	zvedající
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
přeměněné	přeměněný	k2eAgFnPc4d1	přeměněná
v	v	k7c4	v
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
křídla	křídlo	k1gNnSc2	křídlo
určuje	určovat	k5eAaImIp3nS	určovat
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
typ	typ	k1gInSc1	typ
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
využívají	využívat	k5eAaPmIp3nP	využívat
především	především	k6eAd1	především
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
tzv.	tzv.	kA	tzv.
veslovací	veslovací	k2eAgInSc4d1	veslovací
let	let	k1gInSc4	let
s	s	k7c7	s
méně	málo	k6eAd2	málo
náročným	náročný	k2eAgInSc7d1	náročný
klouzavým	klouzavý	k2eAgInSc7d1	klouzavý
letem	let	k1gInSc7	let
nebo	nebo	k8xC	nebo
plachtěním	plachtění	k1gNnSc7	plachtění
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
60	[number]	k4	60
druhů	druh	k1gInPc2	druh
existujících	existující	k2eAgInPc2d1	existující
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
není	být	k5eNaImIp3nS	být
letu	let	k1gInSc6	let
schopno	schopen	k2eAgNnSc1d1	schopno
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
nebyly	být	k5eNaImAgFnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
letu	let	k1gInSc3	let
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
neschopné	schopný	k2eNgInPc1d1	neschopný
letu	let	k1gInSc3	let
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
často	často	k6eAd1	často
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
souostrovích	souostroví	k1gNnPc6	souostroví
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
následkem	následkem	k7c2	následkem
nedostatku	nedostatek	k1gInSc2	nedostatek
pozemních	pozemní	k2eAgMnPc2d1	pozemní
predátorů	predátor	k1gMnPc2	predátor
a	a	k8xC	a
omezených	omezený	k2eAgInPc2d1	omezený
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
schopností	schopnost	k1gFnPc2	schopnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
letu	let	k1gInSc3	let
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
křídla	křídlo	k1gNnPc1	křídlo
pro	pro	k7c4	pro
veslování	veslování	k1gNnSc4	veslování
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
alky	alka	k1gFnPc1	alka
<g/>
,	,	kIx,	,
buřňáci	buřňák	k1gMnPc1	buřňák
či	či	k8xC	či
skorci	skorec	k1gMnPc1	skorec
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
způsobem	způsob	k1gInSc7	způsob
pohybu	pohyb	k1gInSc2	pohyb
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
mávavý	mávavý	k2eAgInSc4d1	mávavý
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rytmicky	rytmicky	k6eAd1	rytmicky
zvedají	zvedat	k5eAaImIp3nP	zvedat
a	a	k8xC	a
spouštějí	spouštět	k5eAaImIp3nP	spouštět
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
letu	let	k1gInSc2	let
regulují	regulovat	k5eAaImIp3nP	regulovat
počtem	počet	k1gInSc7	počet
pohybů	pohyb	k1gInPc2	pohyb
a	a	k8xC	a
nakloněním	naklonění	k1gNnPc3	naklonění
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
letu	let	k1gInSc2	let
zejména	zejména	k9	zejména
u	u	k7c2	u
velkých	velký	k2eAgMnPc2d1	velký
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
plachtění	plachtění	k1gNnSc1	plachtění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
křídla	křídlo	k1gNnSc2	křídlo
prakticky	prakticky	k6eAd1	prakticky
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
využívají	využívat	k5eAaPmIp3nP	využívat
jen	jen	k9	jen
nosné	nosný	k2eAgFnPc1d1	nosná
síly	síla	k1gFnPc1	síla
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
formou	forma	k1gFnSc7	forma
letu	let	k1gInSc2	let
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
třepotavý	třepotavý	k2eAgInSc4d1	třepotavý
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
rychlým	rychlý	k2eAgNnSc7d1	rychlé
máváním	mávání	k1gNnSc7	mávání
křídel	křídlo	k1gNnPc2	křídlo
pták	pták	k1gMnSc1	pták
zastaví	zastavit	k5eAaPmIp3nS	zastavit
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
poštolka	poštolka	k1gFnSc1	poštolka
<g/>
,	,	kIx,	,
rybák	rybák	k1gMnSc1	rybák
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
typem	typ	k1gInSc7	typ
letu	let	k1gInSc2	let
i	i	k8xC	i
z	z	k7c2	z
energetického	energetický	k2eAgNnSc2d1	energetické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
vířivý	vířivý	k2eAgInSc4d1	vířivý
let	let	k1gInSc4	let
kolibříků	kolibřík	k1gMnPc2	kolibřík
<g/>
,	,	kIx,	,
umožňovaný	umožňovaný	k2eAgInSc1d1	umožňovaný
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
frekvencí	frekvence	k1gFnSc7	frekvence
pohybu	pohyb	k1gInSc2	pohyb
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
letu	let	k1gInSc2	let
ptáků	pták	k1gMnPc2	pták
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
překonává	překonávat	k5eAaImIp3nS	překonávat
hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
nejrychlejším	rychlý	k2eAgMnPc3d3	nejrychlejší
patří	patřit	k5eAaImIp3nS	patřit
husy	husa	k1gFnPc4	husa
<g/>
,	,	kIx,	,
kachny	kachna	k1gFnPc4	kachna
<g/>
,	,	kIx,	,
rorýsi	rorýs	k1gMnPc1	rorýs
aj.	aj.	kA	aj.
Drobní	drobný	k2eAgMnPc1d1	drobný
pěvci	pěvec	k1gMnPc1	pěvec
létají	létat	k5eAaImIp3nP	létat
zpravidla	zpravidla	k6eAd1	zpravidla
rychlostí	rychlost	k1gFnSc7	rychlost
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgMnSc1d1	poštovní
holubi	holub	k1gMnPc1	holub
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
80	[number]	k4	80
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dobří	dobrý	k2eAgMnPc1d1	dobrý
letci	letec	k1gMnPc1	letec
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
schopni	schopen	k2eAgMnPc1d1	schopen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pronásledování	pronásledování	k1gNnSc4	pronásledování
kořisti	kořist	k1gFnSc2	kořist
<g/>
)	)	kIx)	)
krátkodobě	krátkodobě	k6eAd1	krátkodobě
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
350	[number]	k4	350
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
řízeno	řízen	k2eAgNnSc1d1	řízeno
instinkty	instinkt	k1gInPc7	instinkt
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
aktivních	aktivní	k2eAgMnPc2d1	aktivní
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
sovy	sova	k1gFnSc2	sova
či	či	k8xC	či
lelci	lelek	k1gMnPc1	lelek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
soumraky	soumrak	k1gInPc4	soumrak
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
na	na	k7c4	na
lov	lov	k1gInSc4	lov
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vhodného	vhodný	k2eAgInSc2d1	vhodný
přílivu	příliv	k1gInSc2	příliv
<g/>
,	,	kIx,	,
lhostejno	lhostejno	k6eAd1	lhostejno
jestli	jestli	k8xS	jestli
ve	v	k7c6	v
dne	den	k1gInSc2	den
nebo	nebo	k8xC	nebo
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
různou	různý	k2eAgFnSc7d1	různá
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
nektar	nektar	k1gInSc4	nektar
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
zdechliny	zdechlina	k1gFnPc4	zdechlina
a	a	k8xC	a
různé	různý	k2eAgMnPc4d1	různý
drobné	drobný	k2eAgMnPc4d1	drobný
živočichy	živočich	k1gMnPc4	živočich
včetně	včetně	k7c2	včetně
dalších	další	k2eAgMnPc2d1	další
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ptáci	pták	k1gMnPc1	pták
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgInPc4	žádný
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
zažívací	zažívací	k2eAgFnSc1d1	zažívací
soustava	soustava	k1gFnSc1	soustava
speciálně	speciálně	k6eAd1	speciálně
přizpůsobena	přizpůsobit	k5eAaPmNgFnS	přizpůsobit
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
celých	celý	k2eAgInPc2d1	celý
<g/>
,	,	kIx,	,
nepřežvýkaných	přežvýkaný	k2eNgInPc2d1	přežvýkaný
kousků	kousek	k1gInPc2	kousek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgFnPc4d1	různá
strategie	strategie	k1gFnPc4	strategie
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Sběr	sběr	k1gInSc1	sběr
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
semen	semeno	k1gNnPc2	semeno
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pěvci	pěvec	k1gMnPc1	pěvec
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
taktiku	taktika	k1gFnSc4	taktika
výpadu	výpad	k1gInSc2	výpad
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
lovení	lovení	k1gNnSc2	lovení
letícího	letící	k2eAgInSc2d1	letící
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
živící	živící	k2eAgMnPc1d1	živící
se	se	k3xPyFc4	se
nektarem	nektar	k1gInSc7	nektar
jako	jako	k9	jako
kolibříci	kolibřík	k1gMnPc1	kolibřík
<g/>
,	,	kIx,	,
papoušci	papoušek	k1gMnPc1	papoušek
lori	lori	k1gMnSc1	lori
<g/>
,	,	kIx,	,
strdimilovití	strdimilovitý	k2eAgMnPc1d1	strdimilovitý
<g/>
,	,	kIx,	,
kystráčkovití	kystráčkovitý	k2eAgMnPc1d1	kystráčkovitý
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
zpěvaví	zpěvavý	k2eAgMnPc1d1	zpěvavý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
usnadněn	usnadněn	k2eAgInSc4d1	usnadněn
sběr	sběr	k1gInSc4	sběr
potravy	potrava	k1gFnSc2	potrava
díky	díky	k7c3	díky
jazýčkům	jazýček	k1gInPc3	jazýček
speciálně	speciálně	k6eAd1	speciálně
přizpůsobeným	přizpůsobený	k2eAgFnPc3d1	přizpůsobená
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
kartáčků	kartáček	k1gInPc2	kartáček
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
zobáky	zobák	k1gInPc7	zobák
speciálně	speciálně	k6eAd1	speciálně
uzpůsobenými	uzpůsobený	k2eAgInPc7d1	uzpůsobený
pro	pro	k7c4	pro
určité	určitý	k2eAgInPc4d1	určitý
typy	typ	k1gInPc4	typ
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Sondování	sondování	k1gNnSc1	sondování
zobákem	zobák	k1gInSc7	zobák
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
používají	používat	k5eAaImIp3nP	používat
kiviové	kivi	k1gMnPc1	kivi
a	a	k8xC	a
bahňáci	bahňák	k1gMnPc1	bahňák
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
zobáky	zobák	k1gInPc7	zobák
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bahňáků	bahňák	k1gInPc2	bahňák
jsou	být	k5eAaImIp3nP	být
délka	délka	k1gFnSc1	délka
zobáků	zobák	k1gInPc2	zobák
a	a	k8xC	a
metody	metoda	k1gFnSc2	metoda
krmení	krmení	k1gNnSc1	krmení
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
potravní	potravní	k2eAgFnSc7d1	potravní
nikou	nika	k1gFnSc7	nika
<g/>
.	.	kIx.	.
</s>
<s>
Stíhání	stíhání	k1gNnSc1	stíhání
střemhlavým	střemhlavý	k2eAgInSc7d1	střemhlavý
letem	let	k1gInSc7	let
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgInSc1d1	vlastní
sokolům	sokol	k1gMnPc3	sokol
a	a	k8xC	a
jestřábům	jestřáb	k1gMnPc3	jestřáb
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
pak	pak	k6eAd1	pak
podobný	podobný	k2eAgInSc4d1	podobný
styl	styl	k1gInSc4	styl
lovu	lov	k1gInSc2	lov
používají	používat	k5eAaImIp3nP	používat
např.	např.	kA	např.
potáplice	potáplice	k1gFnPc1	potáplice
<g/>
,	,	kIx,	,
poláci	polák	k1gMnPc1	polák
a	a	k8xC	a
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
skokem	skokem	k6eAd1	skokem
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
provádí	provádět	k5eAaImIp3nP	provádět
terejovití	terejovití	k1gMnPc1	terejovití
<g/>
,	,	kIx,	,
ledňáčkovití	ledňáčkovitý	k2eAgMnPc1d1	ledňáčkovitý
a	a	k8xC	a
rybáci	rybák	k1gMnPc1	rybák
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
buřňáků	buřňák	k1gMnPc2	buřňák
<g/>
,	,	kIx,	,
plameňáci	plameňák	k1gMnPc1	plameňák
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
kachny	kachna	k1gFnPc1	kachna
získávají	získávat	k5eAaImIp3nP	získávat
potravu	potrava	k1gFnSc4	potrava
procezováním	procezování	k1gNnSc7	procezování
<g/>
.	.	kIx.	.
</s>
<s>
Husy	husa	k1gFnPc1	husa
a	a	k8xC	a
kachny	kachna	k1gFnPc1	kachna
především	především	k9	především
spásají	spásat	k5eAaImIp3nP	spásat
trávu	tráva	k1gFnSc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
provozují	provozovat	k5eAaImIp3nP	provozovat
kleptoparasitismus	kleptoparasitismus	k1gInSc4	kleptoparasitismus
kradením	kradení	k1gNnPc3	kradení
potravy	potrava	k1gFnSc2	potrava
ostatním	ostatní	k2eAgMnSc7d1	ostatní
ptákům	pták	k1gMnPc3	pták
–	–	k?	–
fregatky	fregatka	k1gFnPc1	fregatka
<g/>
,	,	kIx,	,
racci	racek	k1gMnPc1	racek
a	a	k8xC	a
chaluhy	chaluha	k1gFnPc1	chaluha
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc4	druh
používající	používající	k2eAgFnPc1d1	používající
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
obstarávání	obstarávání	k1gNnSc4	obstarávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Kleptoparasitismus	Kleptoparasitismus	k1gInSc1	Kleptoparasitismus
není	být	k5eNaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
žádného	žádný	k3yNgInSc2	žádný
druhu	druh	k1gInSc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
doplňkovým	doplňkový	k2eAgInSc7d1	doplňkový
zdrojem	zdroj	k1gInSc7	zdroj
k	k	k7c3	k
potravě	potrava	k1gFnSc3	potrava
hlavní	hlavní	k2eAgFnSc2d1	hlavní
<g/>
,	,	kIx,	,
získávané	získávaný	k2eAgFnSc2d1	získávaná
lovem	lov	k1gInSc7	lov
<g/>
;	;	kIx,	;
studie	studie	k1gFnSc1	studie
fregatek	fregatka	k1gFnPc2	fregatka
páskovaných	páskovaný	k2eAgFnPc2d1	páskovaná
kradoucích	kradoucí	k2eAgFnPc2d1	kradoucí
potravu	potrava	k1gFnSc4	potrava
terejům	terej	k1gMnPc3	terej
maskovým	maskový	k2eAgFnPc3d1	Masková
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fregatky	fregatka	k1gFnPc1	fregatka
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
maximálně	maximálně	k6eAd1	maximálně
získat	získat	k5eAaPmF	získat
40	[number]	k4	40
%	%	kIx~	%
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgMnSc1d2	nižší
–	–	k?	–
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
jako	jako	k8xS	jako
supové	sup	k1gMnPc1	sup
a	a	k8xC	a
rackové	racek	k1gMnPc1	racek
jsou	být	k5eAaImIp3nP	být
mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
strategií	strategie	k1gFnPc2	strategie
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
potravy	potrava	k1gFnSc2	potrava
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
různou	různý	k2eAgFnSc7d1	různá
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
specialisty	specialista	k1gMnPc4	specialista
soustřeďující	soustřeďující	k2eAgFnSc1d1	soustřeďující
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
na	na	k7c4	na
speciální	speciální	k2eAgFnSc4d1	speciální
potravu	potrava	k1gFnSc4	potrava
nebo	nebo	k8xC	nebo
využívající	využívající	k2eAgFnSc4d1	využívající
jedinou	jediný	k2eAgFnSc4d1	jediná
strategii	strategie	k1gFnSc4	strategie
při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
ptačí	ptačí	k2eAgInPc1d1	ptačí
druhy	druh	k1gInPc1	druh
migrují	migrovat	k5eAaImIp3nP	migrovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
využily	využít	k5eAaPmAgFnP	využít
globální	globální	k2eAgInSc4d1	globální
rozdíly	rozdíl	k1gInPc4	rozdíl
sezónních	sezónní	k2eAgFnPc2d1	sezónní
teplot	teplota	k1gFnPc2	teplota
k	k	k7c3	k
optimalizaci	optimalizace	k1gFnSc3	optimalizace
dostupnosti	dostupnost	k1gFnSc2	dostupnost
zdrojů	zdroj	k1gInPc2	zdroj
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
hnízdních	hnízdní	k2eAgFnPc2d1	hnízdní
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
migrace	migrace	k1gFnPc1	migrace
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vnitrozemských	vnitrozemský	k2eAgMnPc2d1	vnitrozemský
<g/>
,	,	kIx,	,
pobřežních	pobřežní	k2eAgMnPc2d1	pobřežní
či	či	k8xC	či
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
každoročně	každoročně	k6eAd1	každoročně
dalekou	daleký	k2eAgFnSc4d1	daleká
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
měnící	měnící	k2eAgFnSc6d1	měnící
se	se	k3xPyFc4	se
délce	délka	k1gFnSc6	délka
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c6	na
povětrnostních	povětrnostní	k2eAgFnPc6d1	povětrnostní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnPc1	migrace
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgFnP	charakterizovat
hnízdním	hnízdní	k2eAgNnSc7d1	hnízdní
obdobím	období	k1gNnSc7	období
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
nebo	nebo	k8xC	nebo
arktické	arktický	k2eAgFnPc4d1	arktická
či	či	k8xC	či
antarktické	antarktický	k2eAgFnPc4d1	antarktická
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
zimovišti	zimoviště	k1gNnSc6	zimoviště
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
tropů	trop	k1gMnPc2	trop
nebo	nebo	k8xC	nebo
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
zahájí	zahájit	k5eAaPmIp3nS	zahájit
migraci	migrace	k1gFnSc3	migrace
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
ptáci	pták	k1gMnPc1	pták
podstatně	podstatně	k6eAd1	podstatně
své	svůj	k3xOyFgFnPc4	svůj
tukové	tukový	k2eAgFnPc4d1	tuková
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
redukují	redukovat	k5eAaBmIp3nP	redukovat
velikost	velikost	k1gFnSc4	velikost
některých	některý	k3yIgInPc2	některý
svých	svůj	k3xOyFgInPc2	svůj
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
energeticky	energeticky	k6eAd1	energeticky
velice	velice	k6eAd1	velice
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
ptáci	pták	k1gMnPc1	pták
překonávají	překonávat	k5eAaImIp3nP	překonávat
bez	bez	k7c2	bez
doplňování	doplňování	k1gNnSc2	doplňování
potravy	potrava	k1gFnSc2	potrava
pouště	poušť	k1gFnSc2	poušť
nebo	nebo	k8xC	nebo
oceány	oceán	k1gInPc7	oceán
<g/>
;	;	kIx,	;
vnitrozemští	vnitrozemský	k2eAgMnPc1d1	vnitrozemský
ptáci	pták	k1gMnPc1	pták
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
lety	let	k1gInPc4	let
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
až	až	k9	až
2.500	[number]	k4	2.500
km	km	kA	km
a	a	k8xC	a
bahňáci	bahňák	k1gMnPc1	bahňák
až	až	k9	až
4.000	[number]	k4	4.000
km	km	kA	km
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
břehouš	břehouš	k1gMnSc1	břehouš
rudý	rudý	k1gMnSc1	rudý
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
bez	bez	k7c2	bez
zastávky	zastávka	k1gFnSc2	zastávka
tah	tah	k1gInSc1	tah
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
až	až	k9	až
10.200	[number]	k4	10.200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
také	také	k9	také
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
migrační	migrační	k2eAgFnPc1d1	migrační
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
cestu	cesta	k1gFnSc4	cesta
podniká	podnikat	k5eAaImIp3nS	podnikat
např.	např.	kA	např.
buřňák	buřňák	k1gMnSc1	buřňák
temný	temný	k2eAgMnSc1d1	temný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandem	Zéland	k1gInSc7	Zéland
a	a	k8xC	a
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
a	a	k8xC	a
severské	severský	k2eAgNnSc4d1	severské
léto	léto	k1gNnSc4	léto
tráví	trávit	k5eAaImIp3nP	trávit
krmením	krmení	k1gNnSc7	krmení
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
Pacifiku	Pacifik	k1gInSc6	Pacifik
poblíž	poblíž	k7c2	poblíž
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
okružní	okružní	k2eAgFnSc4d1	okružní
cestu	cesta	k1gFnSc4	cesta
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
přibližně	přibližně	k6eAd1	přibližně
64.000	[number]	k4	64.000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
po	po	k7c6	po
vyhnízdění	vyhnízdění	k1gNnSc6	vyhnízdění
<g/>
,	,	kIx,	,
toulají	toulat	k5eAaImIp3nP	toulat
se	se	k3xPyFc4	se
po	po	k7c6	po
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgFnPc4	žádný
pevně	pevně	k6eAd1	pevně
zažité	zažitý	k2eAgFnPc4d1	zažitá
stěhovací	stěhovací	k2eAgFnPc4d1	stěhovací
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Albatrosi	albatros	k1gMnPc1	albatros
<g/>
,	,	kIx,	,
hnízdící	hnízdící	k2eAgMnSc1d1	hnízdící
v	v	k7c6	v
jižních	jižní	k2eAgNnPc6d1	jižní
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
cirkumpolární	cirkumpolární	k2eAgFnPc4d1	cirkumpolární
cesty	cesta	k1gFnPc4	cesta
mezi	mezi	k7c7	mezi
hnízdními	hnízdní	k2eAgNnPc7d1	hnízdní
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
provádějí	provádět	k5eAaImIp3nP	provádět
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
způsoby	způsob	k1gInPc4	způsob
migrace	migrace	k1gFnSc2	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
pouštějí	pouštět	k5eAaImIp3nP	pouštět
do	do	k7c2	do
kratších	krátký	k2eAgFnPc2d2	kratší
cest	cesta	k1gFnPc2	cesta
jen	jen	k6eAd1	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgNnSc2	ten
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuly	vyhnout	k5eAaPmAgInP	vyhnout
špatnému	špatný	k2eAgNnSc3d1	špatné
počasí	počasí	k1gNnSc3	počasí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
získání	získání	k1gNnSc3	získání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
invazní	invazní	k2eAgInPc1d1	invazní
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
letech	let	k1gInPc6	let
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
vůbec	vůbec	k9	vůbec
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
migrace	migrace	k1gFnSc2	migrace
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
dostupností	dostupnost	k1gFnSc7	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takovéto	takovýto	k3xDgInPc4	takovýto
migrující	migrující	k2eAgInPc4d1	migrující
druhy	druh	k1gInPc4	druh
jsou	být	k5eAaImIp3nP	být
zařazovány	zařazován	k2eAgInPc4d1	zařazován
např.	např.	kA	např.
severské	severský	k2eAgFnPc4d1	severská
pěnkavy	pěnkava	k1gFnPc4	pěnkava
<g/>
,	,	kIx,	,
arktické	arktický	k2eAgFnPc4d1	arktická
sovy	sova	k1gFnPc4	sova
a	a	k8xC	a
brkoslavové	brkoslav	k1gMnPc1	brkoslav
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
cestovat	cestovat	k5eAaImF	cestovat
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přesahujících	přesahující	k2eAgFnPc2d1	přesahující
jejich	jejich	k3xOp3gNnSc4	jejich
teritoriální	teritoriální	k2eAgNnSc4d1	teritoriální
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
migrujícími	migrující	k2eAgFnPc7d1	migrující
i	i	k9	i
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
;	;	kIx,	;
jiní	jiný	k1gMnPc1	jiný
provádějí	provádět	k5eAaImIp3nP	provádět
pouze	pouze	k6eAd1	pouze
částečnou	částečný	k2eAgFnSc4d1	částečná
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
migruje	migrovat	k5eAaImIp3nS	migrovat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
samice	samice	k1gFnSc1	samice
a	a	k8xC	a
subdominantní	subdominantní	k2eAgMnPc1d1	subdominantní
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgFnSc1d1	částečná
migrace	migrace	k1gFnSc1	migrace
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
velké	velký	k2eAgNnSc4d1	velké
procento	procento	k1gNnSc4	procento
migračního	migrační	k2eAgNnSc2d1	migrační
chování	chování	k1gNnSc2	chování
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
<g/>
;	;	kIx,	;
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
průzkumy	průzkum	k1gInPc4	průzkum
zjistily	zjistit	k5eAaPmAgFnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
44	[number]	k4	44
%	%	kIx~	%
studovaných	studovaný	k2eAgMnPc2d1	studovaný
nezpěvavých	zpěvavý	k2eNgMnPc2d1	zpěvavý
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
32	[number]	k4	32
%	%	kIx~	%
pěvců	pěvec	k1gMnPc2	pěvec
byli	být	k5eAaImAgMnP	být
částečně	částečně	k6eAd1	částečně
stěhovaví	stěhovavý	k2eAgMnPc1d1	stěhovavý
<g/>
.	.	kIx.	.
</s>
<s>
Výšková	výškový	k2eAgFnSc1d1	výšková
migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
stěhování	stěhování	k1gNnSc2	stěhování
ptáků	pták	k1gMnPc2	pták
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
období	období	k1gNnSc1	období
páření	páření	k1gNnSc2	páření
a	a	k8xC	a
výchovy	výchova	k1gFnSc2	výchova
mláďat	mládě	k1gNnPc2	mládě
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
a	a	k8xC	a
s	s	k7c7	s
pominutím	pominutí	k1gNnSc7	pominutí
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
pak	pak	k6eAd1	pak
nastává	nastávat	k5eAaImIp3nS	nastávat
stěhování	stěhování	k1gNnSc1	stěhování
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
nižších	nízký	k2eAgFnPc2d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
impulsem	impuls	k1gInSc7	impuls
migrace	migrace	k1gFnSc2	migrace
jsou	být	k5eAaImIp3nP	být
teplotní	teplotní	k2eAgFnPc1d1	teplotní
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
také	také	k9	také
zahájena	zahájit	k5eAaPmNgFnS	zahájit
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
normální	normální	k2eAgNnSc1d1	normální
teritorium	teritorium	k1gNnSc1	teritorium
začne	začít	k5eAaPmIp3nS	začít
stávat	stávat	k5eAaImF	stávat
nehostinným	hostinný	k2eNgInPc3d1	nehostinný
také	také	k6eAd1	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatku	nedostatek	k1gInSc2	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
kočovat	kočovat	k5eAaImF	kočovat
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc4	žádný
pevně	pevně	k6eAd1	pevně
vymezené	vymezený	k2eAgNnSc4d1	vymezené
teritorium	teritorium	k1gNnSc4	teritorium
a	a	k8xC	a
přesunují	přesunovat	k5eAaImIp3nP	přesunovat
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počasí	počasí	k1gNnSc6	počasí
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc6	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
např.	např.	kA	např.
nejsou	být	k5eNaImIp3nP	být
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podnikají	podnikat	k5eAaImIp3nP	podnikat
nějaké	nějaký	k3yIgNnSc1	nějaký
ohromující	ohromující	k2eAgFnPc1d1	ohromující
cesty	cesta	k1gFnPc1	cesta
ani	ani	k8xC	ani
nežijí	žít	k5eNaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
buď	buď	k8xC	buď
žijí	žít	k5eAaImIp3nP	žít
rozptýleně	rozptýleně	k6eAd1	rozptýleně
<g/>
,	,	kIx,	,
kočovně	kočovně	k6eAd1	kočovně
nebo	nebo	k8xC	nebo
pořádají	pořádat	k5eAaImIp3nP	pořádat
nepravidelné	pravidelný	k2eNgFnPc4d1	nepravidelná
a	a	k8xC	a
malé	malý	k2eAgFnPc4d1	malá
migrační	migrační	k2eAgFnPc4d1	migrační
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
ptáků	pták	k1gMnPc2	pták
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
obrovské	obrovský	k2eAgFnPc4d1	obrovská
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
přesně	přesně	k6eAd1	přesně
do	do	k7c2	do
určitých	určitý	k2eAgFnPc2d1	určitá
lokalit	lokalita	k1gFnPc2	lokalita
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
známa	znám	k2eAgNnPc1d1	známo
<g/>
;	;	kIx,	;
při	při	k7c6	při
experimentu	experiment	k1gInSc6	experiment
provedeném	provedený	k2eAgInSc6d1	provedený
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
s	s	k7c7	s
buřňákem	buřňák	k1gMnSc7	buřňák
severním	severní	k2eAgMnSc7d1	severní
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
pták	pták	k1gMnSc1	pták
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kolonie	kolonie	k1gFnSc2	kolonie
na	na	k7c4	na
ostrovu	ostrov	k1gInSc3	ostrov
Skomer	Skomer	k1gInSc1	Skomer
u	u	k7c2	u
Walesu	Wales	k1gInSc2	Wales
během	během	k7c2	během
13	[number]	k4	13
dnů	den	k1gInPc2	den
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
5.150	[number]	k4	5.150
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
navigaci	navigace	k1gFnSc3	navigace
během	během	k7c2	během
migrace	migrace	k1gFnSc2	migrace
různé	různý	k2eAgFnSc2d1	různá
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
denní	denní	k2eAgFnSc6d1	denní
migraci	migrace	k1gFnSc6	migrace
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
navigaci	navigace	k1gFnSc3	navigace
využíváno	využívat	k5eAaImNgNnS	využívat
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
používán	používán	k2eAgInSc4d1	používán
stelární	stelární	k2eAgInSc4d1	stelární
kompas	kompas	k1gInSc4	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
orientují	orientovat	k5eAaBmIp3nP	orientovat
podle	podle	k7c2	podle
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
směru	směr	k1gInSc2	směr
tahu	tah	k1gInSc2	tah
své	svůj	k3xOyFgFnSc2	svůj
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
pudově	pudově	k6eAd1	pudově
určit	určit	k5eAaPmF	určit
odklon	odklon	k1gInSc4	odklon
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Orientace	orientace	k1gFnSc1	orientace
podle	podle	k7c2	podle
stelárního	stelární	k2eAgInSc2d1	stelární
kompasu	kompas	k1gInSc2	kompas
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
okolo	okolo	k7c2	okolo
Polárky	Polárka	k1gFnSc2	Polárka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
posílena	posílit	k5eAaPmNgFnS	posílit
schopností	schopnost	k1gFnPc2	schopnost
vnímat	vnímat	k5eAaImF	vnímat
zemský	zemský	k2eAgInSc4d1	zemský
geomagnetismus	geomagnetismus	k1gInSc4	geomagnetismus
díky	díky	k7c3	díky
specializovaným	specializovaný	k2eAgInPc3d1	specializovaný
fotoreceptorům	fotoreceptor	k1gInPc3	fotoreceptor
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
hlavně	hlavně	k6eAd1	hlavně
používáním	používání	k1gNnSc7	používání
vizuálních	vizuální	k2eAgInPc2d1	vizuální
a	a	k8xC	a
zvukových	zvukový	k2eAgInPc2d1	zvukový
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
signály	signál	k1gInPc1	signál
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mezidruhové	mezidruhový	k2eAgFnPc1d1	mezidruhová
nebo	nebo	k8xC	nebo
vnitrodruhové	vnitrodruhový	k2eAgFnPc1d1	vnitrodruhová
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgFnPc1d1	vizuální
komunikace	komunikace	k1gFnPc1	komunikace
na	na	k7c6	na
ptácích	pták	k1gMnPc6	pták
slouží	sloužit	k5eAaImIp3nS	sloužit
mnoha	mnoho	k4c2	mnoho
funkcím	funkce	k1gFnPc3	funkce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
na	na	k7c4	na
peří	peří	k1gNnSc4	peří
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
ocenění	ocenění	k1gNnSc4	ocenění
a	a	k8xC	a
potvrzení	potvrzení	k1gNnSc4	potvrzení
sociální	sociální	k2eAgFnSc2d1	sociální
dominance	dominance	k1gFnSc2	dominance
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
připravenost	připravenost	k1gFnSc4	připravenost
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
může	moct	k5eAaImIp3nS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
slunatce	slunatec	k1gMnSc2	slunatec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
větší	veliký	k2eAgMnPc4d2	veliký
dravce	dravec	k1gMnPc4	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
zobrazení	zobrazení	k1gNnSc1	zobrazení
na	na	k7c6	na
peří	peří	k1gNnSc6	peří
je	být	k5eAaImIp3nS	být
užíváno	užívat	k5eAaImNgNnS	užívat
pro	pro	k7c4	pro
odrazení	odrazení	k1gNnSc4	odrazení
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
krahujci	krahujec	k1gMnPc1	krahujec
<g/>
,	,	kIx,	,
a	a	k8xC	a
ochraně	ochrana	k1gFnSc3	ochrana
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnPc1	variace
v	v	k7c6	v
peří	peří	k1gNnSc6	peří
také	také	k9	také
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
identifikaci	identifikace	k1gFnSc4	identifikace
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1	vizuální
komunikace	komunikace	k1gFnSc1	komunikace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
rituální	rituální	k2eAgNnSc1d1	rituální
předvádění	předvádění	k1gNnSc1	předvádění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
signalizující	signalizující	k2eAgInSc4d1	signalizující
útok	útok	k1gInSc4	útok
nebo	nebo	k8xC	nebo
podřízenost	podřízenost	k1gFnSc4	podřízenost
<g/>
,	,	kIx,	,
či	či	k8xC	či
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
párových	párový	k2eAgFnPc2d1	párová
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ritualisované	ritualisovaný	k2eAgNnSc1d1	ritualisovaný
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
nesignalizovanými	signalizovaný	k2eNgFnPc7d1	signalizovaný
akcemi	akce	k1gFnPc7	akce
jako	jako	k8xC	jako
urovnánvání	urovnánvánět	k5eAaImIp3nS	urovnánvánět
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
časově	časově	k6eAd1	časově
rozlišené	rozlišený	k2eAgFnSc2d1	rozlišená
pozice	pozice	k1gFnSc2	pozice
pér	pér	k?	pér
<g/>
,	,	kIx,	,
klování	klování	k1gNnSc4	klování
nebo	nebo	k8xC	nebo
další	další	k2eAgNnSc4d1	další
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
propracované	propracovaný	k2eAgFnPc1d1	propracovaná
ukázky	ukázka	k1gFnPc1	ukázka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
během	během	k7c2	během
námluv	námluva	k1gFnPc2	námluva
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
při	při	k7c6	při
tanci	tanec	k1gInSc6	tanec
v	v	k7c6	v
toku	tok	k1gInSc6	tok
u	u	k7c2	u
albatrosů	albatros	k1gMnPc2	albatros
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
vytvoření	vytvoření	k1gNnSc1	vytvoření
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
párového	párový	k2eAgNnSc2d1	párové
pouta	pouto	k1gNnSc2	pouto
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
tento	tento	k3xDgInSc4	tento
jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
rajek	rajka	k1gFnPc2	rajka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úspěch	úspěch	k1gInSc1	úspěch
samců	samec	k1gMnPc2	samec
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
peří	peří	k1gNnSc6	peří
a	a	k8xC	a
kvalitě	kvalita	k1gFnSc3	kvalita
jeho	jeho	k3xOp3gNnSc2	jeho
předvádění	předvádění	k1gNnSc2	předvádění
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
ptáků	pták	k1gMnPc2	pták
mohou	moct	k5eAaImIp3nP	moct
svou	svůj	k3xOyFgFnSc4	svůj
zdatnost	zdatnost	k1gFnSc4	zdatnost
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
také	také	k9	také
např.	např.	kA	např.
stavbou	stavba	k1gFnSc7	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
;	;	kIx,	;
samice	samice	k1gFnSc1	samice
snovačů	snovač	k1gMnPc2	snovač
jako	jako	k9	jako
snovač	snovač	k1gMnSc1	snovač
asijský	asijský	k2eAgMnSc1d1	asijský
si	se	k3xPyFc3	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
za	za	k7c4	za
partnery	partner	k1gMnPc4	partner
jenom	jenom	k8xS	jenom
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
dobrými	dobrý	k2eAgMnPc7d1	dobrý
staviteli	stavitel	k1gMnPc7	stavitel
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
lemčíků	lemčík	k1gMnPc2	lemčík
jsou	být	k5eAaImIp3nP	být
přitahovány	přitahován	k2eAgFnPc1d1	přitahována
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
altánků	altánek	k1gInPc2	altánek
samců	samec	k1gMnPc2	samec
vyzdobených	vyzdobený	k2eAgFnPc2d1	vyzdobená
různými	různý	k2eAgInPc7d1	různý
výraznými	výrazný	k2eAgInPc7d1	výrazný
dekoračními	dekorační	k2eAgInPc7d1	dekorační
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vizuální	vizuální	k2eAgFnSc2d1	vizuální
komunikace	komunikace	k1gFnSc2	komunikace
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
proslulí	proslulý	k2eAgMnPc1d1	proslulý
svými	svůj	k3xOyFgFnPc7	svůj
zvukovými	zvukový	k2eAgFnPc7d1	zvuková
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
i	i	k8xC	i
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
zvukovými	zvukový	k2eAgInPc7d1	zvukový
komunikačními	komunikační	k2eAgInPc7d1	komunikační
prostředky	prostředek	k1gInPc7	prostředek
ptáků	pták	k1gMnPc2	pták
<g/>
;	;	kIx,	;
ačkoli	ačkoli	k8xS	ačkoli
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
mechanických	mechanický	k2eAgInPc2d1	mechanický
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozeznění	rozeznění	k1gNnSc4	rozeznění
per	pero	k1gNnPc2	pero
pomocí	pomocí	k7c2	pomocí
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
u	u	k7c2	u
samců	samec	k1gInPc2	samec
bekasíny	bekasína	k1gFnSc2	bekasína
otavní	otavní	k2eAgMnSc1d1	otavní
<g/>
,	,	kIx,	,
teritoriální	teritoriální	k2eAgNnPc4d1	teritoriální
bubnování	bubnování	k1gNnPc4	bubnování
u	u	k7c2	u
datlů	datel	k1gMnPc2	datel
či	či	k8xC	či
používání	používání	k1gNnSc2	používání
nástrojů	nástroj	k1gInPc2	nástroj
k	k	k7c3	k
tlučení	tlučení	k1gNnSc3	tlučení
jako	jako	k9	jako
u	u	k7c2	u
kakadu	kakadu	k1gMnSc2	kakadu
arového	arový	k2eAgMnSc2d1	arový
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgNnSc1d1	ptačí
volání	volání	k1gNnSc1	volání
a	a	k8xC	a
písně	píseň	k1gFnPc1	píseň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgInPc1d1	složitý
<g/>
;	;	kIx,	;
zvuk	zvuk	k1gInSc1	zvuk
bývá	bývat	k5eAaImIp3nS	bývat
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
v	v	k7c6	v
syrinxu	syrinx	k1gInSc6	syrinx
umístěným	umístěný	k2eAgInSc7d1	umístěný
na	na	k7c4	na
průdušnice	průdušnice	k1gFnPc4	průdušnice
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jejího	její	k3xOp3gNnSc2	její
rozdvojení	rozdvojení	k1gNnSc2	rozdvojení
na	na	k7c4	na
průdušky	průduška	k1gFnPc4	průduška
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
umí	umět	k5eAaImIp3nP	umět
ovládat	ovládat	k5eAaImF	ovládat
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
čase	čas	k1gInSc6	čas
vznik	vznik	k1gInSc1	vznik
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
písní	píseň	k1gFnPc2	píseň
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnPc1	volání
jsou	být	k5eAaImIp3nP	být
používána	používat	k5eAaImNgNnP	používat
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojeno	spojit	k5eAaPmNgNnS	spojit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
využívána	využívat	k5eAaImNgNnP	využívat
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vábení	vábení	k1gNnSc3	vábení
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
partnerů	partner	k1gMnPc2	partner
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
společných	společný	k2eAgFnPc6d1	společná
skupinách	skupina	k1gFnPc6	skupina
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vizuální	vizuální	k2eAgFnSc7d1	vizuální
komunikací	komunikace	k1gFnSc7	komunikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
předávat	předávat	k5eAaImF	předávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
kvalitě	kvalita	k1gFnSc6	kvalita
samců	samec	k1gInPc2	samec
a	a	k8xC	a
pomoci	pomoc	k1gFnSc2	pomoc
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
pro	pro	k7c4	pro
vymezení	vymezení	k1gNnSc4	vymezení
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Volání	volání	k1gNnSc1	volání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používáno	používat	k5eAaImNgNnS	používat
také	také	k9	také
k	k	k7c3	k
rozpoznávání	rozpoznávání	k1gNnSc3	rozpoznávání
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
rodičům	rodič	k1gMnPc3	rodič
nalézt	nalézt	k5eAaPmF	nalézt
mláďata	mládě	k1gNnPc4	mládě
v	v	k7c6	v
přeplněných	přeplněný	k2eAgFnPc6d1	přeplněná
koloniích	kolonie	k1gFnPc6	kolonie
nebo	nebo	k8xC	nebo
dospělým	dospělí	k1gMnPc3	dospělí
shledat	shledat	k5eAaPmF	shledat
se	se	k3xPyFc4	se
s	s	k7c7	s
partnery	partner	k1gMnPc7	partner
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
volání	volání	k1gNnSc1	volání
týká	týkat	k5eAaImIp3nS	týkat
také	také	k9	také
varování	varování	k1gNnSc1	varování
dalších	další	k2eAgMnPc2d1	další
ptáků	pták	k1gMnPc2	pták
před	před	k7c7	před
potenciálními	potenciální	k2eAgMnPc7d1	potenciální
predátory	predátor	k1gMnPc7	predátor
<g/>
;	;	kIx,	;
volání	volání	k1gNnSc1	volání
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
detailní	detailní	k2eAgInSc4d1	detailní
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
specifickou	specifický	k2eAgFnSc4d1	specifická
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
hrozby	hrozba	k1gFnSc2	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
nebo	nebo	k8xC	nebo
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
rodinných	rodinný	k2eAgFnPc6d1	rodinná
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
ptáci	pták	k1gMnPc1	pták
často	často	k6eAd1	často
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
velká	velký	k2eAgNnPc4d1	velké
hejna	hejno	k1gNnPc4	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
seskupování	seskupování	k1gNnSc2	seskupování
se	se	k3xPyFc4	se
do	do	k7c2	do
hejn	hejno	k1gNnPc2	hejno
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc4d1	různá
a	a	k8xC	a
hejna	hejno	k1gNnPc1	hejno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výslovně	výslovně	k6eAd1	výslovně
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Shlukování	shlukování	k1gNnSc1	shlukování
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
své	svůj	k3xOyFgFnPc4	svůj
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
společensky	společensky	k6eAd1	společensky
níže	nízce	k6eAd2	nízce
postavení	postavení	k1gNnSc4	postavení
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
područí	područí	k1gNnSc6	područí
více	hodně	k6eAd2	hodně
dominantních	dominantní	k2eAgMnPc2d1	dominantní
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
obětovat	obětovat	k5eAaBmF	obětovat
krmnou	krmný	k2eAgFnSc4d1	krmná
efektivitu	efektivita	k1gFnSc4	efektivita
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zisku	zisk	k1gInSc2	zisk
jiných	jiný	k2eAgFnPc2d1	jiná
výhod	výhoda	k1gFnPc2	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
výhodami	výhoda	k1gFnPc7	výhoda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
bezpečí	bezpečí	k1gNnSc4	bezpečí
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
a	a	k8xC	a
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
krmná	krmný	k2eAgFnSc1d1	krmná
efektivita	efektivita	k1gFnSc1	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
dravcům	dravec	k1gMnPc3	dravec
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
důležitá	důležitý	k2eAgFnSc1d1	důležitá
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
predátoři	predátor	k1gMnPc1	predátor
často	často	k6eAd1	často
útočí	útočit	k5eAaImIp3nP	útočit
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
a	a	k8xC	a
včasné	včasný	k2eAgNnSc4d1	včasné
varování	varování	k1gNnSc4	varování
díky	díky	k7c3	díky
pozorování	pozorování	k1gNnSc3	pozorování
více	hodně	k6eAd2	hodně
jedinců	jedinec	k1gMnPc2	jedinec
tak	tak	k9	tak
nabývá	nabývat	k5eAaImIp3nS	nabývat
na	na	k7c6	na
důležitosti	důležitost	k1gFnSc6	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
druhově	druhově	k6eAd1	druhově
smíšených	smíšený	k2eAgNnPc2d1	smíšené
hejn	hejno	k1gNnPc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
mnohodruhová	mnohodruhový	k2eAgNnPc1d1	mnohodruhový
hejna	hejno	k1gNnPc1	hejno
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
skupin	skupina	k1gFnPc2	skupina
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zvětšující	zvětšující	k2eAgFnSc1d1	zvětšující
se	se	k3xPyFc4	se
množstevní	množstevní	k2eAgFnSc1d1	množstevní
výhoda	výhoda	k1gFnSc1	výhoda
je	být	k5eAaImIp3nS	být
však	však	k9	však
snižována	snižovat	k5eAaImNgFnS	snižovat
konkurencí	konkurence	k1gFnSc7	konkurence
při	při	k7c6	při
potenciálním	potenciální	k2eAgNnSc6d1	potenciální
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
také	také	k9	také
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
živočichy	živočich	k1gMnPc7	živočich
<g/>
;	;	kIx,	;
mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
lovící	lovící	k2eAgFnSc2d1	lovící
ryby	ryba	k1gFnSc2	ryba
vrháním	vrhání	k1gNnSc7	vrhání
se	se	k3xPyFc4	se
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
delfíny	delfín	k1gMnPc7	delfín
a	a	k8xC	a
tuňáky	tuňák	k1gMnPc7	tuňák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
rybí	rybí	k2eAgNnSc1d1	rybí
hejna	hejno	k1gNnPc1	hejno
vyhánějí	vyhánět	k5eAaImIp3nP	vyhánět
vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
;	;	kIx,	;
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
také	také	k6eAd1	také
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
mangustou	mangusta	k1gFnSc7	mangusta
jižní	jižní	k2eAgMnPc1d1	jižní
a	a	k8xC	a
zoborožci	zoborožec	k1gMnPc1	zoborožec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zoborožec	zoborožec	k1gMnSc1	zoborožec
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
mangustu	mangusta	k1gFnSc4	mangusta
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
společného	společný	k2eAgNnSc2d1	společné
hrabání	hrabání	k1gNnSc2	hrabání
a	a	k8xC	a
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
se	se	k3xPyFc4	se
varování	varování	k1gNnSc2	varování
před	před	k7c7	před
dravci	dravec	k1gMnPc7	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
partnerství	partnerství	k1gNnSc1	partnerství
medozvěstek	medozvěstka	k1gFnPc2	medozvěstka
s	s	k7c7	s
medojedy	medojed	k1gMnPc7	medojed
<g/>
,	,	kIx,	,
paviány	pavián	k1gMnPc7	pavián
<g/>
,	,	kIx,	,
jezevci	jezevec	k1gMnPc7	jezevec
nebo	nebo	k8xC	nebo
lidmi	člověk	k1gMnPc7	člověk
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
včelích	včelí	k2eAgInPc2d1	včelí
příbytků	příbytek	k1gInPc2	příbytek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dovede	dovést	k5eAaPmIp3nS	dovést
a	a	k8xC	a
po	po	k7c6	po
vybrání	vybrání	k1gNnSc6	vybrání
medu	med	k1gInSc2	med
si	se	k3xPyFc3	se
sesbírá	sesbírat	k5eAaPmIp3nS	sesbírat
svůj	svůj	k3xOyFgInSc4	svůj
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
vosk	vosk	k1gInSc4	vosk
<g/>
,	,	kIx,	,
včely	včela	k1gFnPc4	včela
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc4	larva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
metabolismu	metabolismus	k1gInSc2	metabolismus
ptáků	pták	k1gMnPc2	pták
během	během	k7c2	během
aktivní	aktivní	k2eAgFnSc2d1	aktivní
části	část	k1gFnSc2	část
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
odpočinkem	odpočinek	k1gInSc7	odpočinek
ve	v	k7c6	v
zbylé	zbylý	k2eAgFnSc6d1	zbylá
části	část	k1gFnSc6	část
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Spící	spící	k2eAgMnPc1d1	spící
ptáci	pták	k1gMnPc1	pták
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
typ	typ	k1gInSc4	typ
spánku	spánek	k1gInSc2	spánek
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
ostražitý	ostražitý	k2eAgInSc1d1	ostražitý
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
periody	perioda	k1gFnSc2	perioda
odpočinku	odpočinek	k1gInSc2	odpočinek
jsou	být	k5eAaImIp3nP	být
promíchány	promíchat	k5eAaPmNgFnP	promíchat
s	s	k7c7	s
rychlým	rychlý	k2eAgNnSc7d1	rychlé
otvíráním	otvírání	k1gNnSc7	otvírání
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
citlivost	citlivost	k1gFnSc1	citlivost
ptáků	pták	k1gMnPc2	pták
na	na	k7c4	na
vyrušení	vyrušení	k1gNnSc4	vyrušení
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jim	on	k3xPp3gMnPc3	on
rychlý	rychlý	k2eAgInSc1d1	rychlý
únik	únik	k1gInSc1	únik
před	před	k7c7	před
případnou	případný	k2eAgFnSc7d1	případná
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rorýsi	rorýs	k1gMnPc1	rorýs
dokáží	dokázat	k5eAaPmIp3nP	dokázat
spát	spát	k5eAaImF	spát
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
experimentálně	experimentálně	k6eAd1	experimentálně
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existují	existovat	k5eAaImIp3nP	existovat
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaPmF	využívat
určitých	určitý	k2eAgInPc2d1	určitý
druhů	druh	k1gInPc2	druh
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
dokonce	dokonce	k9	dokonce
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
nemají	mít	k5eNaImIp3nP	mít
potní	potní	k2eAgFnPc4d1	potní
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
schováním	schování	k1gNnSc7	schování
do	do	k7c2	do
stínu	stín	k1gInSc2	stín
<g/>
,	,	kIx,	,
stáním	stání	k1gNnSc7	stání
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
zrychleným	zrychlený	k2eAgNnSc7d1	zrychlené
dýcháním	dýchání	k1gNnSc7	dýchání
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
zvětšováním	zvětšování	k1gNnSc7	zvětšování
své	svůj	k3xOyFgFnSc2	svůj
plochy	plocha	k1gFnSc2	plocha
roztahováním	roztahování	k1gNnSc7	roztahování
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
vibrováním	vibrování	k1gNnSc7	vibrování
svého	svůj	k3xOyFgInSc2	svůj
hrdelního	hrdelní	k2eAgInSc2d1	hrdelní
laloku	lalok	k1gInSc2	lalok
nebo	nebo	k8xC	nebo
močením	močení	k1gNnSc7	močení
si	se	k3xPyFc3	se
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
ochlazování	ochlazování	k1gNnSc4	ochlazování
vypařující	vypařující	k2eAgFnSc2d1	vypařující
se	se	k3xPyFc4	se
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
spících	spící	k2eAgMnPc2d1	spící
ptáků	pták	k1gMnPc2	pták
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
krk	krk	k1gInSc4	krk
nad	nad	k7c4	nad
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
zastrkává	zastrkávat	k5eAaImIp3nS	zastrkávat
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
peří	peří	k1gNnSc2	peří
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
kryjí	krýt	k5eAaImIp3nP	krýt
své	svůj	k3xOyFgInPc4	svůj
zobáky	zobák	k1gInPc4	zobák
v	v	k7c6	v
peří	peří	k1gNnSc6	peří
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
také	také	k9	také
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
noze	noha	k1gFnSc6	noha
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
druhou	druhý	k4xOgFnSc4	druhý
nohu	noha	k1gFnSc4	noha
schovávají	schovávat	k5eAaImIp3nP	schovávat
do	do	k7c2	do
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c2	za
chladného	chladný	k2eAgNnSc2d1	chladné
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
je	být	k5eAaImIp3nS	být
také	také	k9	také
společný	společný	k2eAgInSc4d1	společný
hřad	hřad	k1gInSc4	hřad
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
ztráta	ztráta	k1gFnSc1	ztráta
tělesného	tělesný	k2eAgNnSc2d1	tělesné
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
rizika	riziko	k1gNnSc2	riziko
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
napadením	napadení	k1gNnSc7	napadení
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Hřadovací	Hřadovací	k2eAgNnPc1d1	Hřadovací
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vybírána	vybírán	k2eAgFnSc1d1	vybírána
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
termoregulaci	termoregulace	k1gFnSc3	termoregulace
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Stromoví	stromový	k2eAgMnPc1d1	stromový
ptáci	pták	k1gMnPc1	pták
využívají	využívat	k5eAaImIp3nP	využívat
šlachy	šlacha	k1gFnPc4	šlacha
svalu	sval	k1gInSc2	sval
zadní	zadní	k2eAgFnSc2d1	zadní
končetiny	končetina	k1gFnSc2	končetina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
bez	bez	k7c2	bez
neustálého	neustálý	k2eAgNnSc2d1	neustálé
stahování	stahování	k1gNnSc2	stahování
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
také	také	k9	také
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
pohybujících	pohybující	k2eAgMnPc2d1	pohybující
se	se	k3xPyFc4	se
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
křepelky	křepelka	k1gFnPc1	křepelka
a	a	k8xC	a
bažanti	bažant	k1gMnPc1	bažant
<g/>
,	,	kIx,	,
hřaduje	hřadovat	k5eAaImIp3nS	hřadovat
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
papoušci	papoušek	k1gMnPc1	papoušek
rodu	rod	k1gInSc2	rod
Loriculus	Loriculus	k1gInSc4	Loriculus
hřadují	hřadovat	k5eAaImIp3nP	hřadovat
zavěšeni	zavěšen	k2eAgMnPc1d1	zavěšen
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kolibříci	kolibřík	k1gMnPc1	kolibřík
každou	každý	k3xTgFnSc4	každý
noc	noc	k1gFnSc4	noc
upadají	upadat	k5eAaImIp3nP	upadat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
strnulosti	strnulost	k1gFnSc2	strnulost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
rychlost	rychlost	k1gFnSc1	rychlost
jejich	jejich	k3xOp3gInSc2	jejich
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
adaptace	adaptace	k1gFnSc1	adaptace
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
u	u	k7c2	u
okolo	okolo	k7c2	okolo
sta	sto	k4xCgNnSc2	sto
dalších	další	k2eAgInPc2d1	další
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lelčíků	lelčík	k1gInPc2	lelčík
<g/>
,	,	kIx,	,
lelků	lelek	k1gInPc2	lelek
a	a	k8xC	a
lasoleti	lasole	k1gNnSc6	lasole
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
lelek	lelek	k1gInSc1	lelek
americký	americký	k2eAgInSc1d1	americký
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
hibernace	hibernace	k1gFnSc2	hibernace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
společensky	společensky	k6eAd1	společensky
monogamní	monogamní	k2eAgInPc1d1	monogamní
<g/>
;	;	kIx,	;
i	i	k9	i
když	když	k8xS	když
polygynie	polygynie	k1gFnSc1	polygynie
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
polyandrie	polyandrie	k1gFnSc1	polyandrie
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polygamie	polygamie	k1gFnSc1	polygamie
<g/>
,	,	kIx,	,
polygynandrie	polygynandrie	k1gFnSc1	polygynandrie
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
samice	samice	k1gFnSc1	samice
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pár	pár	k1gInSc1	pár
s	s	k7c7	s
několika	několik	k4yIc7	několik
samci	samec	k1gMnPc7	samec
a	a	k8xC	a
samec	samec	k1gMnSc1	samec
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pár	pár	k1gInSc1	pár
s	s	k7c7	s
několika	několik	k4yIc7	několik
samicemi	samice	k1gFnPc7	samice
<g/>
)	)	kIx)	)
a	a	k8xC	a
smíšené	smíšený	k2eAgInPc1d1	smíšený
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
rovněž	rovněž	k9	rovněž
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislostech	závislost	k1gFnPc6	závislost
na	na	k7c6	na
okolnostech	okolnost	k1gFnPc6	okolnost
<g/>
,	,	kIx,	,
využívat	využívat	k5eAaPmF	využívat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
monogamních	monogamní	k2eAgInPc2d1	monogamní
druhů	druh	k1gInPc2	druh
tvoří	tvořit	k5eAaImIp3nP	tvořit
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
několik	několik	k4yIc4	několik
hnízdních	hnízdní	k2eAgNnPc2d1	hnízdní
období	období	k1gNnPc2	období
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
párové	párový	k2eAgFnSc2d1	párová
pouto	pouto	k1gNnSc1	pouto
může	moct	k5eAaImIp3nS	moct
přetrvat	přetrvat	k5eAaPmF	přetrvat
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
monogamního	monogamní	k2eAgInSc2d1	monogamní
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
je	být	k5eAaImIp3nS	být
oboustranná	oboustranný	k2eAgFnSc1d1	oboustranná
péče	péče	k1gFnSc1	péče
rodičů	rodič	k1gMnPc2	rodič
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
vzácností	vzácnost	k1gFnSc7	vzácnost
otcovská	otcovský	k2eAgFnSc1d1	otcovská
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
docela	docela	k6eAd1	docela
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
než	než	k8xS	než
u	u	k7c2	u
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
jiné	jiný	k2eAgFnSc2d1	jiná
třídy	třída	k1gFnSc2	třída
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
samčí	samčí	k2eAgFnSc4d1	samčí
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
důležitou	důležitý	k2eAgFnSc4d1	důležitá
nebo	nebo	k8xC	nebo
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
pro	pro	k7c4	pro
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
zdatnost	zdatnost	k1gFnSc4	zdatnost
samic	samice	k1gFnPc2	samice
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
nejsou	být	k5eNaImIp3nP	být
samice	samice	k1gFnPc1	samice
schopny	schopen	k2eAgFnPc1d1	schopna
mláďata	mládě	k1gNnPc4	mládě
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
samců	samec	k1gMnPc2	samec
úspěšně	úspěšně	k6eAd1	úspěšně
odchovat	odchovat	k5eAaPmF	odchovat
<g/>
.	.	kIx.	.
</s>
<s>
Polygamní	polygamní	k2eAgInPc1d1	polygamní
systémy	systém	k1gInPc1	systém
pramení	pramenit	k5eAaImIp3nP	pramenit
ze	z	k7c2	z
schopnosti	schopnost	k1gFnSc2	schopnost
samic	samice	k1gFnPc2	samice
vychovávat	vychovávat	k5eAaImF	vychovávat
mláďata	mládě	k1gNnPc4	mládě
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Dělba	dělba	k1gFnSc1	dělba
práce	práce	k1gFnSc2	práce
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
<g/>
,	,	kIx,	,
obraně	obraně	k6eAd1	obraně
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
,	,	kIx,	,
krmení	krmení	k1gNnSc1	krmení
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
obrany	obrana	k1gFnSc2	obrana
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
někdy	někdy	k6eAd1	někdy
u	u	k7c2	u
monogamních	monogamní	k2eAgInPc2d1	monogamní
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
sociální	sociální	k2eAgFnSc1d1	sociální
monogamie	monogamie	k1gFnSc1	monogamie
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
zcela	zcela	k6eAd1	zcela
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
nevěra	nevěra	k1gFnSc1	nevěra
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dodatečného	dodatečný	k2eAgNnSc2d1	dodatečné
párového	párový	k2eAgNnSc2d1	párové
páření	páření	k1gNnSc2	páření
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
sociálně	sociálně	k6eAd1	sociálně
monogamních	monogamní	k2eAgInPc2d1	monogamní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nevěra	nevěra	k1gFnSc1	nevěra
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
brát	brát	k5eAaImF	brát
formu	forma	k1gFnSc4	forma
nuceného	nucený	k2eAgNnSc2d1	nucené
páření	páření	k1gNnSc2	páření
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
)	)	kIx)	)
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
kachnovitých	kachnovití	k1gMnPc2	kachnovití
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
u	u	k7c2	u
dominantních	dominantní	k2eAgInPc2d1	dominantní
samců	samec	k1gInPc2	samec
či	či	k8xC	či
samic	samice	k1gFnPc2	samice
s	s	k7c7	s
podřízenými	podřízený	k2eAgMnPc7d1	podřízený
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
samice	samice	k1gFnPc4	samice
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
potomky	potomek	k1gMnPc7	potomek
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
možnost	možnost	k1gFnSc1	možnost
obdržení	obdržení	k1gNnSc2	obdržení
lepší	dobrý	k2eAgFnSc2d2	lepší
genové	genový	k2eAgFnSc2d1	genová
výbavy	výbava	k1gFnSc2	výbava
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
eliminována	eliminovat	k5eAaBmNgFnS	eliminovat
případná	případný	k2eAgFnSc1d1	případná
neplodnost	neplodnost	k1gFnSc1	neplodnost
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
provozují	provozovat	k5eAaImIp3nP	provozovat
mimopartnerské	mimopartnerský	k2eAgNnSc4d1	mimopartnerský
páření	páření	k1gNnSc4	páření
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zvýšení	zvýšení	k1gNnSc1	zvýšení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
svého	svůj	k3xOyFgNnSc2	svůj
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
určité	určitý	k2eAgFnPc4d1	určitá
formy	forma	k1gFnPc4	forma
námluv	námluva	k1gFnPc2	námluva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
prováděny	provádět	k5eAaImNgInP	provádět
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nějaký	nějaký	k3yIgInSc4	nějaký
druh	druh	k1gInSc4	druh
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
vystoupení	vystoupení	k1gNnPc1	vystoupení
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
docela	docela	k6eAd1	docela
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
používány	používat	k5eAaImNgFnP	používat
takové	takový	k3xDgFnPc1	takový
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
metody	metoda	k1gFnPc1	metoda
jako	jako	k8xS	jako
bubnování	bubnování	k1gNnPc1	bubnování
křídly	křídlo	k1gNnPc7	křídlo
nebo	nebo	k8xC	nebo
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
tancování	tancování	k1gNnSc4	tancování
<g/>
,	,	kIx,	,
zásnubní	zásnubní	k2eAgInPc4d1	zásnubní
lety	let	k1gInPc4	let
nebo	nebo	k8xC	nebo
skupinové	skupinový	k2eAgFnPc4d1	skupinová
toky	toka	k1gFnPc4	toka
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
nejčastěji	často	k6eAd3	často
vybírají	vybírat	k5eAaImIp3nP	vybírat
samce	samec	k1gMnSc4	samec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
např.	např.	kA	např.
u	u	k7c2	u
polyandrických	polyandrický	k2eAgInPc2d1	polyandrický
lyskonohů	lyskonoh	k1gInPc2	lyskonoh
si	se	k3xPyFc3	se
samečkové	sameček	k1gMnPc1	sameček
vybírají	vybírat	k5eAaImIp3nP	vybírat
pestře	pestro	k6eAd1	pestro
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
samičky	samička	k1gFnPc4	samička
<g/>
.	.	kIx.	.
</s>
<s>
Námluvy	námluva	k1gFnSc2	námluva
krmením	krmení	k1gNnSc7	krmení
<g/>
,	,	kIx,	,
cukrováním	cukrování	k1gNnSc7	cukrování
a	a	k8xC	a
čechráním	čechrání	k1gNnSc7	čechrání
peří	peří	k1gNnSc2	peří
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
prováděno	provádět	k5eAaImNgNnS	provádět
již	již	k9	již
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ptáky	pták	k1gMnPc4	pták
spárovanými	spárovaný	k2eAgFnPc7d1	spárovaná
a	a	k8xC	a
pářícími	pářící	k2eAgFnPc7d1	pářící
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
brání	bránit	k5eAaImIp3nS	bránit
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
své	svůj	k3xOyFgFnSc6	svůj
území	území	k1gNnSc4	území
před	před	k7c7	před
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
teritoria	teritorium	k1gNnSc2	teritorium
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
shánění	shánění	k1gNnSc2	shánění
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
účinně	účinně	k6eAd1	účinně
bránit	bránit	k5eAaImF	bránit
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
mořští	mořský	k2eAgMnPc1d1	mořský
ptáci	pták	k1gMnPc1	pták
nebo	nebo	k8xC	nebo
rorýsi	rorýs	k1gMnPc1	rorýs
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
hnízdních	hnízdní	k2eAgFnPc2d1	hnízdní
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
proti	proti	k7c3	proti
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
jedinci	jedinec	k1gMnPc1	jedinec
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
pak	pak	k6eAd1	pak
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
malé	malý	k2eAgFnPc4d1	malá
hnízdní	hnízdní	k2eAgFnPc4d1	hnízdní
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
soutěžení	soutěžení	k1gNnPc4	soutěžení
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mezidruhové	mezidruhový	k2eAgNnSc1d1	mezidruhové
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
uvnitř	uvnitř	k7c2	uvnitř
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
místa	místo	k1gNnPc4	místo
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
velmi	velmi	k6eAd1	velmi
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ptáci	pták	k1gMnPc1	pták
snášejí	snášet	k5eAaImIp3nP	snášet
amniotická	amniotický	k2eAgNnPc4d1	amniotický
vejce	vejce	k1gNnPc4	vejce
s	s	k7c7	s
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
skořápkami	skořápka	k1gFnPc7	skořápka
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
vajec	vejce	k1gNnPc2	vejce
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
:	:	kIx,	:
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
snášena	snášet	k5eAaImNgNnP	snášet
do	do	k7c2	do
děr	děra	k1gFnPc2	děra
nebo	nebo	k8xC	nebo
doupat	doupě	k1gNnPc2	doupě
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
bílá	bílý	k2eAgFnSc1d1	bílá
nebo	nebo	k8xC	nebo
bledá	bledý	k2eAgFnSc1d1	bledá
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vejce	vejce	k1gNnSc1	vejce
v	v	k7c6	v
otevřených	otevřený	k2eAgInPc6d1	otevřený
hnízdech	hnízdo	k1gNnPc6	hnízdo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
u	u	k7c2	u
dlouhokřídlých	dlouhokřídlý	k2eAgMnPc2d1	dlouhokřídlý
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
maskovaná	maskovaný	k2eAgNnPc1d1	maskované
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
výjimky	výjimka	k1gFnPc4	výjimka
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
zemních	zemní	k2eAgInPc2d1	zemní
lelků	lelek	k1gInPc2	lelek
má	mít	k5eAaImIp3nS	mít
vejce	vejce	k1gNnPc4	vejce
bílá	bílý	k2eAgNnPc4d1	bílé
a	a	k8xC	a
maskování	maskování	k1gNnPc4	maskování
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťován	k2eAgNnSc1d1	zajišťováno
peřím	peří	k1gNnSc7	peří
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
hnízdního	hnízdní	k2eAgInSc2d1	hnízdní
parazitismu	parazitismus	k1gInSc2	parazitismus
prováděného	prováděný	k2eAgInSc2d1	prováděný
např.	např.	kA	např.
některými	některý	k3yIgFnPc7	některý
kukačkami	kukačka	k1gFnPc7	kukačka
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nP	měnit
barvy	barva	k1gFnPc4	barva
svých	svůj	k3xOyFgNnPc2	svůj
vajec	vejce	k1gNnPc2	vejce
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
snížení	snížení	k1gNnSc1	snížení
šance	šance	k1gFnSc2	šance
na	na	k7c4	na
snesení	snesení	k1gNnSc4	snesení
kukaččího	kukaččí	k2eAgNnSc2d1	kukaččí
vejce	vejce	k1gNnSc2	vejce
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kukaččí	kukaččí	k2eAgFnPc1d1	kukaččí
samičky	samička	k1gFnPc1	samička
mají	mít	k5eAaImIp3nP	mít
snahu	snaha	k1gFnSc4	snaha
snést	snést	k5eAaPmF	snést
své	svůj	k3xOyFgNnSc4	svůj
vejce	vejce	k1gNnSc4	vejce
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
hostitele	hostitel	k1gMnSc2	hostitel
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
zabarvenými	zabarvený	k2eAgNnPc7d1	zabarvené
vejci	vejce	k1gNnPc7	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
uložena	uložit	k5eAaPmNgNnP	uložit
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
propracované	propracovaný	k2eAgInPc4d1	propracovaný
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
snovačů	snovač	k1gMnPc2	snovač
či	či	k8xC	či
vlhovců	vlhovec	k1gMnPc2	vlhovec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
velmi	velmi	k6eAd1	velmi
primitivní	primitivní	k2eAgMnSc1d1	primitivní
jako	jako	k8xC	jako
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
albatrosů	albatros	k1gMnPc2	albatros
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hnízda	hnízdo	k1gNnPc1	hnízdo
skládají	skládat	k5eAaImIp3nP	skládat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
hromádky	hromádka	k1gFnSc2	hromádka
seškrábané	seškrábaný	k2eAgFnSc2d1	seškrábaná
ze	z	k7c2	z
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
si	se	k3xPyFc3	se
nestaví	stavit	k5eNaImIp3nP	stavit
žádné	žádný	k3yNgNnSc4	žádný
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
na	na	k7c6	na
útesech	útes	k1gInPc6	útes
hnízdící	hnízdící	k2eAgMnPc1d1	hnízdící
alkouni	alkoun	k1gMnPc1	alkoun
úzkozobí	úzkozobý	k2eAgMnPc1d1	úzkozobý
kladou	klást	k5eAaImIp3nP	klást
svá	svůj	k3xOyFgNnPc4	svůj
vejce	vejce	k1gNnPc4	vejce
na	na	k7c4	na
holou	holý	k2eAgFnSc4d1	holá
skálu	skála	k1gFnSc4	skála
a	a	k8xC	a
vejce	vejce	k1gNnSc4	vejce
tučňáka	tučňák	k1gMnSc2	tučňák
císařského	císařský	k2eAgMnSc2d1	císařský
jsou	být	k5eAaImIp3nP	být
ukládána	ukládat	k5eAaImNgNnP	ukládat
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
svrchu	svrchu	k6eAd1	svrchu
jsou	být	k5eAaImIp3nP	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
kožním	kožní	k2eAgInSc7d1	kožní
záhybem	záhyb	k1gInSc7	záhyb
na	na	k7c6	na
břiše	břich	k1gInSc6	břich
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
převládající	převládající	k2eAgInSc1d1	převládající
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nově	nově	k6eAd1	nově
vylíhlá	vylíhlý	k2eAgNnPc1d1	vylíhlé
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
předčasně	předčasně	k6eAd1	předčasně
vyspělá	vyspělý	k2eAgNnPc1d1	vyspělé
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
si	se	k3xPyFc3	se
staví	stavit	k5eAaPmIp3nS	stavit
propracovaná	propracovaný	k2eAgNnPc4d1	propracované
hnízda	hnízdo	k1gNnPc4	hnízdo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
kupole	kupole	k1gFnSc2	kupole
<g/>
,	,	kIx,	,
talíře	talíř	k1gInSc2	talíř
<g/>
,	,	kIx,	,
rámu	rám	k1gInSc2	rám
nebo	nebo	k8xC	nebo
doupěte	doupě	k1gNnSc2	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hnízd	hnízdo	k1gNnPc2	hnízdo
je	být	k5eAaImIp3nS	být
stavěna	stavit	k5eAaImNgFnS	stavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
co	co	k3yRnSc4	co
nejméně	málo	k6eAd3	málo
nápadná	nápadný	k2eAgFnSc1d1	nápadná
<g/>
,	,	kIx,	,
skrytá	skrytý	k2eAgFnSc1d1	skrytá
a	a	k8xC	a
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
tak	tak	k6eAd1	tak
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
otevřená	otevřený	k2eAgNnPc1d1	otevřené
hnízda	hnízdo	k1gNnPc1	hnízdo
jsou	být	k5eAaImIp3nP	být
stavěna	stavit	k5eAaImNgNnP	stavit
ptáky	pták	k1gMnPc4	pták
v	v	k7c6	v
hnízdních	hnízdní	k2eAgFnPc6d1	hnízdní
koloniích	kolonie	k1gFnPc6	kolonie
nebo	nebo	k8xC	nebo
druhy	druh	k1gInPc1	druh
schopných	schopný	k2eAgMnPc2d1	schopný
účinné	účinný	k2eAgFnSc2d1	účinná
obrany	obrana	k1gFnSc2	obrana
svého	svůj	k3xOyFgNnSc2	svůj
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
stavěna	stavěn	k2eAgNnPc1d1	stavěno
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
si	se	k3xPyFc3	se
specificky	specificky	k6eAd1	specificky
vybírají	vybírat	k5eAaImIp3nP	vybírat
rostliny	rostlina	k1gFnPc4	rostlina
jako	jako	k8xS	jako
řebříček	řebříček	k1gInSc4	řebříček
obecný	obecný	k2eAgInSc4d1	obecný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
redukující	redukující	k2eAgMnPc4d1	redukující
hnízdní	hnízdní	k2eAgMnPc4d1	hnízdní
parazity	parazit	k1gMnPc4	parazit
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
roztoči	roztoč	k1gMnPc7	roztoč
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
šance	šance	k1gFnSc1	šance
mláďat	mládě	k1gNnPc2	mládě
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
často	často	k6eAd1	často
vystlána	vystlán	k2eAgFnSc1d1	vystlána
peřím	peřit	k5eAaImIp1nS	peřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
úniku	únik	k1gInSc3	únik
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
je	být	k5eAaImIp3nS	být
udržována	udržován	k2eAgFnSc1d1	udržována
optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
nakladením	nakladení	k1gNnSc7	nakladení
prvního	první	k4xOgNnSc2	první
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
potomci	potomek	k1gMnPc1	potomek
líhnou	líhnout	k5eAaImIp3nP	líhnout
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
sledu	sled	k1gInSc6	sled
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgNnP	být
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
vejce	vejce	k1gNnPc1	vejce
snášena	snášen	k2eAgNnPc1d1	snášeno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
monogamních	monogamní	k2eAgInPc2d1	monogamní
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rodiče	rodič	k1gMnPc1	rodič
v	v	k7c6	v
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
;	;	kIx,	;
u	u	k7c2	u
polygamních	polygamní	k2eAgInPc2d1	polygamní
druhů	druh	k1gInPc2	druh
plní	plnit	k5eAaImIp3nS	plnit
veškeré	veškerý	k3xTgFnSc2	veškerý
rodičovské	rodičovský	k2eAgFnSc2d1	rodičovská
povinnosti	povinnost	k1gFnSc2	povinnost
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
přechází	přecházet	k5eAaImIp3nS	přecházet
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
k	k	k7c3	k
vejcím	vejce	k1gNnPc3	vejce
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
hnízdní	hnízdní	k2eAgFnSc4d1	hnízdní
nažinu	nažina	k1gFnSc4	nažina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
holé	holý	k2eAgFnSc2d1	holá
kůže	kůže	k1gFnSc2	kůže
na	na	k7c6	na
břichu	břich	k1gInSc6	břich
nebo	nebo	k8xC	nebo
hrudi	hruď	k1gFnSc6	hruď
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedících	sedící	k2eAgMnPc2d1	sedící
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
energeticky	energeticky	k6eAd1	energeticky
náročný	náročný	k2eAgInSc4d1	náročný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dospělí	dospělý	k2eAgMnPc1d1	dospělý
albatrosi	albatros	k1gMnPc1	albatros
ztratí	ztratit	k5eAaPmIp3nP	ztratit
za	za	k7c4	za
den	den	k1gInSc4	den
sezení	sezení	k1gNnSc2	sezení
až	až	k9	až
83	[number]	k4	83
gramů	gram	k1gInPc2	gram
své	svůj	k3xOyFgFnSc2	svůj
tělesné	tělesný	k2eAgFnSc2d1	tělesná
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
taboni	tabon	k1gMnPc1	tabon
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
jako	jako	k8xS	jako
teplo	teplo	k1gNnSc4	teplo
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
sezení	sezení	k1gNnSc1	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
tlející	tlející	k2eAgFnPc4d1	tlející
rostliny	rostlina	k1gFnPc4	rostlina
nebo	nebo	k8xC	nebo
vulkanické	vulkanický	k2eAgInPc4d1	vulkanický
zdroje	zdroj	k1gInPc4	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
sezení	sezení	k1gNnSc2	sezení
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
10	[number]	k4	10
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
datli	datel	k1gMnPc1	datel
<g/>
,	,	kIx,	,
kukačky	kukačka	k1gFnPc1	kukačka
a	a	k8xC	a
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
)	)	kIx)	)
do	do	k7c2	do
80	[number]	k4	80
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
albatrosovití	albatrosovitý	k2eAgMnPc1d1	albatrosovitý
a	a	k8xC	a
kiviové	kivi	k1gMnPc1	kivi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mláďata	mládě	k1gNnPc4	mládě
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
bezmocná	bezmocný	k2eAgFnSc1d1	bezmocná
až	až	k6eAd1	až
samostatná	samostatný	k2eAgFnSc1d1	samostatná
<g/>
.	.	kIx.	.
</s>
<s>
Bezmocná	bezmocný	k2eAgNnPc1d1	bezmocné
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
jako	jako	k8xS	jako
altriciální	altriciální	k2eAgFnSc1d1	altriciální
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
zpravidla	zpravidla	k6eAd1	zpravidla
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
holá	holý	k2eAgFnSc1d1	holá
a	a	k8xC	a
často	často	k6eAd1	často
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
;	;	kIx,	;
mláďata	mládě	k1gNnPc4	mládě
po	po	k7c4	po
vylíhnutí	vylíhnutí	k1gNnSc4	vylíhnutí
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
a	a	k8xC	a
krytá	krytý	k2eAgFnSc1d1	krytá
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
jsou	být	k5eAaImIp3nP	být
prekociální	prekociální	k2eAgMnPc1d1	prekociální
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
poloprekociální	poloprekociální	k2eAgMnSc1d1	poloprekociální
nebo	nebo	k8xC	nebo
poloaltriciální	poloaltriciální	k2eAgMnSc1d1	poloaltriciální
<g/>
.	.	kIx.	.
</s>
<s>
Altriciální	Altriciální	k2eAgNnPc1d1	Altriciální
mláďata	mládě	k1gNnPc1	mládě
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
termoregulaci	termoregulace	k1gFnSc6	termoregulace
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
tak	tak	k6eAd1	tak
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
k	k	k7c3	k
vysezení	vysezení	k1gNnSc3	vysezení
než	než	k8xS	než
mláďata	mládě	k1gNnPc4	mládě
prekociální	prekociální	k2eAgFnPc4d1	prekociální
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
a	a	k8xC	a
povaha	povaha	k1gFnSc1	povaha
rodičovské	rodičovský	k2eAgFnSc2d1	rodičovská
péče	péče	k1gFnSc2	péče
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
řádech	řád	k1gInPc6	řád
a	a	k8xC	a
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Extrémem	extrém	k1gInSc7	extrém
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
péče	péče	k1gFnSc1	péče
tabonů	tabon	k1gInPc2	tabon
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
líhnutím	líhnutí	k1gNnSc7	líhnutí
<g/>
;	;	kIx,	;
čerstvě	čerstvě	k6eAd1	čerstvě
vylíhlá	vylíhlý	k2eAgNnPc1d1	vylíhlé
kuřata	kuře	k1gNnPc1	kuře
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
rodičů	rodič	k1gMnPc2	rodič
vyhrabou	vyhrabat	k5eAaPmIp3nP	vyhrabat
z	z	k7c2	z
hnízdní	hnízdní	k2eAgFnSc2d1	hnízdní
kupy	kupa	k1gFnSc2	kupa
listí	listí	k1gNnSc2	listí
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
soběstačná	soběstačný	k2eAgNnPc1d1	soběstačné
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
extrém	extrém	k1gInSc1	extrém
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
doby	doba	k1gFnSc2	doba
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Rodiči	rodič	k1gMnSc3	rodič
nejdéle	dlouho	k6eAd3	dlouho
se	s	k7c7	s
starajícími	starající	k2eAgMnPc7d1	starající
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
jsou	být	k5eAaImIp3nP	být
fregatky	fregatka	k1gFnPc1	fregatka
páskované	páskovaný	k2eAgFnPc1d1	páskovaná
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
mláďata	mládě	k1gNnPc1	mládě
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
ve	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
opeření	opeření	k1gNnSc2	opeření
a	a	k8xC	a
poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
dalších	další	k2eAgInPc2d1	další
čtrnáct	čtrnáct	k4xCc4	čtrnáct
měsíců	měsíc	k1gInPc2	měsíc
krmena	krmen	k2eAgFnSc1d1	krmena
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
pečují	pečovat	k5eAaImIp3nP	pečovat
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
mláďat	mládě	k1gNnPc2	mládě
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
páru	pára	k1gFnSc4	pára
i	i	k8xC	i
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
pomocníci	pomocník	k1gMnPc1	pomocník
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
mláďata	mládě	k1gNnPc1	mládě
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
snůšek	snůška	k1gFnPc2	snůška
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
výchova	výchova	k1gFnSc1	výchova
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
obzvlášť	obzvlášť	k6eAd1	obzvlášť
běžná	běžný	k2eAgFnSc1d1	běžná
u	u	k7c2	u
krkavcovitých	krkavcovitý	k2eAgFnPc2d1	krkavcovitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
pokřovník	pokřovník	k1gInSc1	pokřovník
<g/>
,	,	kIx,	,
luňák	luňák	k1gMnSc1	luňák
červený	červený	k2eAgMnSc1d1	červený
nebo	nebo	k8xC	nebo
flétňák	flétňák	k1gInSc1	flétňák
australský	australský	k2eAgInSc1d1	australský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
plně	plně	k6eAd1	plně
opeřena	opeřen	k2eAgNnPc1d1	opeřen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
starým	starý	k1gMnSc7	starý
ptákem	pták	k1gMnSc7	pták
a	a	k8xC	a
mládětem	mládě	k1gNnSc7	mládě
ještě	ještě	k6eAd1	ještě
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
hned	hned	k6eAd1	hned
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
např.	např.	kA	např.
u	u	k7c2	u
alkounů	alkoun	k1gMnPc2	alkoun
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mláďata	mládě	k1gNnPc1	mládě
následují	následovat	k5eAaImIp3nP	následovat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
své	svůj	k3xOyFgMnPc4	svůj
volající	volající	k2eAgMnPc4d1	volající
rodiče	rodič	k1gMnPc4	rodič
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
číhá	číhat	k5eAaImIp3nS	číhat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pozemských	pozemský	k2eAgMnPc2d1	pozemský
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kachny	kachna	k1gFnPc1	kachna
<g/>
,	,	kIx,	,
opouštějí	opouštět	k5eAaImIp3nP	opouštět
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
mláďata	mládě	k1gNnPc4	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
hned	hned	k6eAd1	hned
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
získají	získat	k5eAaPmIp3nP	získat
schopnost	schopnost	k1gFnSc4	schopnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
také	také	k9	také
po	po	k7c6	po
opeření	opeření	k1gNnSc6	opeření
mění	měnit	k5eAaImIp3nS	měnit
péče	péče	k1gFnSc1	péče
rodičů	rodič	k1gMnPc2	rodič
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
;	;	kIx,	;
u	u	k7c2	u
albatrosů	albatros	k1gMnPc2	albatros
mláďata	mládě	k1gNnPc4	mládě
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
již	již	k6eAd1	již
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
žádnou	žádný	k3yNgFnSc4	žádný
další	další	k2eAgFnSc4d1	další
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
doplňkové	doplňkový	k2eAgFnSc6d1	doplňková
formě	forma	k1gFnSc6	forma
krmení	krmení	k1gNnPc2	krmení
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
absolvovat	absolvovat	k5eAaPmF	absolvovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
migrační	migrační	k2eAgFnSc4d1	migrační
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
parazitismus	parazitismus	k1gInSc1	parazitismus
Ačkoliv	ačkoliv	k8xS	ačkoliv
hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
parazitismus	parazitismus	k1gInSc1	parazitismus
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
kladení	kladení	k1gNnSc4	kladení
vajec	vejce	k1gNnPc2	vejce
do	do	k7c2	do
hnízd	hnízdo	k1gNnPc2	hnízdo
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
svými	svůj	k3xOyFgMnPc7	svůj
adoptivními	adoptivní	k2eAgMnPc7d1	adoptivní
rodiči	rodič	k1gMnPc7	rodič
přijata	přijat	k2eAgFnSc1d1	přijata
a	a	k8xC	a
vysezena	vysezen	k2eAgFnSc1d1	vysezen
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zničení	zničení	k1gNnSc2	zničení
jejich	jejich	k3xOp3gNnPc2	jejich
vlastních	vlastní	k2eAgNnPc2d1	vlastní
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
ptáků	pták	k1gMnPc2	pták
provozujících	provozující	k2eAgInPc2d1	provozující
hnízdní	hnízdní	k2eAgInSc4d1	hnízdní
parasitismus	parasitismus	k1gInSc4	parasitismus
<g/>
:	:	kIx,	:
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
sami	sám	k3xTgMnPc1	sám
vysedět	vysedět	k5eAaPmF	vysedět
svá	svůj	k3xOyFgNnPc4	svůj
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vysezení	vysezení	k1gNnSc4	vysezení
musí	muset	k5eAaImIp3nS	muset
nalézt	nalézt	k5eAaPmF	nalézt
vhodné	vhodný	k2eAgNnSc1d1	vhodné
hnízdo	hnízdo	k1gNnSc1	hnízdo
jiného	jiný	k2eAgInSc2d1	jiný
ptačího	ptačí	k2eAgInSc2d1	ptačí
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
mláďata	mládě	k1gNnPc4	mládě
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
vychovávat	vychovávat	k5eAaImF	vychovávat
samy	sám	k3xTgFnPc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příležitostně	příležitostně	k6eAd1	příležitostně
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
hnízdní	hnízdní	k2eAgInSc4d1	hnízdní
parasitismus	parasitismus	k1gInSc4	parasitismus
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zvýšení	zvýšení	k1gNnSc1	zvýšení
svého	svůj	k3xOyFgInSc2	svůj
reprodukčního	reprodukční	k2eAgInSc2d1	reprodukční
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
sta	sto	k4xCgNnSc2	sto
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zahrnujících	zahrnující	k2eAgMnPc6d1	zahrnující
medozvěstkovité	medozvěstkovitý	k2eAgFnPc4d1	medozvěstkovitý
<g/>
,	,	kIx,	,
vlhovcovité	vlhovcovitý	k2eAgFnPc4d1	vlhovcovitý
<g/>
,	,	kIx,	,
astrildovité	astrildovitý	k2eAgFnPc4d1	astrildovitý
a	a	k8xC	a
kachnice	kachnice	k1gFnPc4	kachnice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
běžnými	běžný	k2eAgMnPc7d1	běžný
hnízdními	hnízdní	k2eAgMnPc7d1	hnízdní
parazity	parazit	k1gMnPc7	parazit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nejznámější	známý	k2eAgMnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
kukačkovití	kukačkovitý	k2eAgMnPc1d1	kukačkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
mláďata	mládě	k1gNnPc1	mládě
hnízdních	hnízdní	k2eAgMnPc2d1	hnízdní
parazitů	parazit	k1gMnPc2	parazit
jsou	být	k5eAaImIp3nP	být
adaptována	adaptován	k2eAgFnSc1d1	adaptována
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zničila	zničit	k5eAaPmAgFnS	zničit
celou	celý	k2eAgFnSc4d1	celá
snůšku	snůška	k1gFnSc4	snůška
hostitele	hostitel	k1gMnSc2	hostitel
buď	buď	k8xC	buď
vyhazováním	vyhazování	k1gNnSc7	vyhazování
vajec	vejce	k1gNnPc2	vejce
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zabíjením	zabíjení	k1gNnSc7	zabíjení
ostatních	ostatní	k2eAgNnPc2d1	ostatní
vylíhlých	vylíhlý	k2eAgNnPc2d1	vylíhlé
ptáčat	ptáče	k1gNnPc2	ptáče
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
všechnu	všechen	k3xTgFnSc4	všechen
potravu	potrava	k1gFnSc4	potrava
donášenou	donášený	k2eAgFnSc4d1	donášená
adoptivními	adoptivní	k2eAgMnPc7d1	adoptivní
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
KNĚŽOUREK	kněžourek	k1gMnSc1	kněžourek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
atlas	atlas	k1gInSc1	atlas
ptáků	pták	k1gMnPc2	pták
ku	k	k7c3	k
Kněžourkovu	kněžourkův	k2eAgInSc3d1	kněžourkův
Velkému	velký	k2eAgInSc3d1	velký
přírodopisu	přírodopis	k1gInSc3	přírodopis
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
4	[number]	k4	4
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KNĚŽOUREK	kněžourek	k1gMnSc1	kněžourek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
přírodopis	přírodopis	k1gInSc1	přírodopis
ptáků	pták	k1gMnPc2	pták
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
ku	k	k7c3	k
ptactvu	ptactvo	k1gNnSc3	ptactvo
zemí	zem	k1gFnPc2	zem
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
rakouských	rakouský	k2eAgMnPc2d1	rakouský
<g/>
.	.	kIx.	.
2	[number]	k4	2
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ptáci	pták	k1gMnPc1	pták
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pták	pták	k1gMnSc1	pták
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Ptáci	pták	k1gMnPc1	pták
na	na	k7c4	na
BioLibu	BioLiba	k1gFnSc4	BioLiba
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
jmen	jméno	k1gNnPc2	jméno
ptáků	pták	k1gMnPc2	pták
světa	svět	k1gInSc2	svět
Databáze	databáze	k1gFnSc2	databáze
pozorování	pozorování	k1gNnPc2	pozorování
ptáků	pták	k1gMnPc2	pták
birds	birds	k6eAd1	birds
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Atlas	Atlas	k1gInSc4	Atlas
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
klíč	klíč	k1gInSc1	klíč
pro	pro	k7c4	pro
identifikaci	identifikace	k1gFnSc4	identifikace
Databáze	databáze	k1gFnSc2	databáze
stránek	stránka	k1gFnPc2	stránka
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
</s>
