<s>
Ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
Aves	Aves	k1gInSc1	Aves
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dvojnozí	dvojnohý	k2eAgMnPc1d1	dvojnohý
<g/>
,	,	kIx,	,
teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
a	a	k8xC	a
vejce	vejce	k1gNnPc4	vejce
snášející	snášející	k2eAgFnPc4d1	snášející
obratlovci	obratlovec	k1gMnPc7	obratlovec
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
nové	nový	k2eAgFnSc2d1	nová
systematiky	systematika	k1gFnSc2	systematika
patřící	patřící	k2eAgMnPc1d1	patřící
mezi	mezi	k7c4	mezi
teropodní	teropodní	k2eAgMnPc4d1	teropodní
dinosaury	dinosaurus	k1gMnPc4	dinosaurus
a	a	k8xC	a
obecněji	obecně	k6eAd2	obecně
diapsidy	diapsida	k1gFnSc2	diapsida
<g/>
.	.	kIx.	.
</s>
