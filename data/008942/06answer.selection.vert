<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgMnSc1d1	žlutý
Saladinův	Saladinův	k2eAgMnSc1d1	Saladinův
orel	orel	k1gMnSc1	orel
hledící	hledící	k2eAgInSc4d1	hledící
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
<g/>
.	.	kIx.	.
</s>
