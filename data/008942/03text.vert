<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgMnSc1d1	žlutý
Saladinův	Saladinův	k2eAgMnSc1d1	Saladinův
orel	orel	k1gMnSc1	orel
hledící	hledící	k2eAgInSc4d1	hledící
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
(	(	kIx(	(
<g/>
levý	levý	k2eAgInSc4d1	levý
a	a	k8xC	a
pravý	pravý	k2eAgInSc4d1	pravý
pruh	pruh	k1gInSc4	pruh
má	mít	k5eAaImIp3nS	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drápech	dráp	k1gInPc6	dráp
drží	držet	k5eAaImIp3nS	držet
panel	panel	k1gInSc1	panel
s	s	k7c7	s
textem	text	k1gInSc7	text
AL-GUMHURÍJA	AL-GUMHURÍJA	k1gMnPc1	AL-GUMHURÍJA
MISR	MISR	kA	MISR
AL-ARABÍJA	AL-ARABÍJA	k1gFnSc1	AL-ARABÍJA
(	(	kIx(	(
<g/>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Orel	Orel	k1gMnSc1	Orel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
jako	jako	k9	jako
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
barevnější	barevný	k2eAgNnSc1d2	barevnější
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
orel	orel	k1gMnSc1	orel
vybarvený	vybarvený	k2eAgMnSc1d1	vybarvený
<g/>
:	:	kIx,	:
levá	levý	k2eAgFnSc1d1	levá
část	část	k1gFnSc1	část
štítu	štít	k1gInSc2	štít
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
pravá	pravý	k2eAgFnSc1d1	pravá
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
<g/>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
obětí	oběť	k1gFnPc2	oběť
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
znamená	znamenat	k5eAaImIp3nS	znamenat
zářící	zářící	k2eAgFnSc1d1	zářící
budoucnost	budoucnost	k1gFnSc1	budoucnost
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
pochmurnou	pochmurný	k2eAgFnSc4d1	pochmurná
koloniální	koloniální	k2eAgFnSc4d1	koloniální
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Egypta	Egypt	k1gInSc2	Egypt
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
tato	tento	k3xDgFnSc1	tento
trikolóra	trikolóra	k1gFnSc1	trikolóra
také	také	k9	také
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
znaku	znak	k1gInSc2	znak
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
klín	klín	k1gInSc4	klín
nebo	nebo	k8xC	nebo
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
roku	rok	k1gInSc2	rok
395	[number]	k4	395
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
připadlo	připadnout	k5eAaPmAgNnS	připadnout
dnešní	dnešní	k2eAgNnSc1d1	dnešní
území	území	k1gNnSc1	území
Egypta	Egypt	k1gInSc2	Egypt
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
islamizaci	islamizace	k1gFnSc3	islamizace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
byl	být	k5eAaImAgInS	být
Egypt	Egypt	k1gInSc1	Egypt
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
vlajkami	vlajka	k1gFnPc7	vlajka
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
<g/>
,	,	kIx,	,
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Egypta	Egypt	k1gInSc2	Egypt
vlajky	vlajka	k1gFnSc2	vlajka
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc4	ten
tvořil	tvořit	k5eAaImAgInS	tvořit
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
půlměsíci	půlměsíc	k1gInPc7	půlměsíc
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
obrázky	obrázek	k1gInPc4	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
přidána	přidán	k2eAgFnSc1d1	přidána
osmicípá	osmicípý	k2eAgFnSc1d1	osmicípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
pěticípou	pěticípý	k2eAgFnSc4d1	pěticípá
<g/>
.	.	kIx.	.
<g/>
Egypt	Egypt	k1gInSc1	Egypt
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
značnou	značný	k2eAgFnSc4d1	značná
míru	míra	k1gFnSc4	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ismaila	Ismail	k1gMnSc2	Ismail
Paši	paša	k1gMnSc2	paša
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
již	již	k9	již
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
předchůdce	předchůdce	k1gMnSc4	předchůdce
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
zavedena	zaveden	k2eAgFnSc1d1	zavedena
osobní	osobní	k2eAgFnSc1d1	osobní
vlajka	vlajka	k1gFnSc1	vlajka
chediva	chediv	k1gMnSc2	chediv
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
paši	paša	k1gMnSc2	paša
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Egyptské	egyptský	k2eAgNnSc4d1	egyptské
chedivství	chedivství	k1gNnSc4	chedivství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
publikacích	publikace	k1gFnPc6	publikace
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
egyptská	egyptský	k2eAgFnSc1d1	egyptská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
bílými	bílý	k2eAgInPc7d1	bílý
půlměsíci	půlměsíc	k1gInPc7	půlměsíc
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
bílými	bílý	k2eAgFnPc7d1	bílá
sedmicípými	sedmicípý	k2eAgFnPc7d1	sedmicípá
hvězdami	hvězda	k1gFnPc7	hvězda
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
změněny	změněn	k2eAgFnPc4d1	změněna
na	na	k7c4	na
pěticípé	pěticípý	k2eAgMnPc4d1	pěticípý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
osmanské	osmanský	k2eAgFnPc1d1	Osmanská
vlajky	vlajka	k1gFnPc1	vlajka
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
však	však	k9	však
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
říši	říše	k1gFnSc4	říše
závislý	závislý	k2eAgInSc1d1	závislý
jen	jen	k6eAd1	jen
formálně	formálně	k6eAd1	formálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
zobrazené	zobrazený	k2eAgFnSc3d1	zobrazená
vlajce	vlajka	k1gFnSc3	vlajka
má	mít	k5eAaImIp3nS	mít
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
cípy	cíp	k1gInPc1	cíp
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Egypt	Egypt	k1gInSc1	Egypt
okupován	okupován	k2eAgInSc1d1	okupován
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
formálně	formálně	k6eAd1	formálně
zůstal	zůstat	k5eAaPmAgInS	zůstat
nadále	nadále	k6eAd1	nadále
součástí	součást	k1gFnSc7	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
chediva	chediv	k1gMnSc2	chediv
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k8xC	však
součástí	součást	k1gFnSc7	součást
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1914	[number]	k4	1914
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
turecko-egyptský	tureckogyptský	k2eAgInSc1d1	turecko-egyptský
svazek	svazek	k1gInSc1	svazek
oficiálně	oficiálně	k6eAd1	oficiálně
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Egyptský	egyptský	k2eAgInSc1d1	egyptský
sultanát	sultanát	k1gInSc1	sultanát
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
zavedena	zavést	k5eAaPmNgFnS	zavést
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1923	[number]	k4	1923
se	s	k7c7	s
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
stal	stát	k5eAaPmAgInS	stát
zelený	zelený	k2eAgInSc1d1	zelený
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc1	tři
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
jeden	jeden	k4xCgInSc1	jeden
cíp	cíp	k1gInSc1	cíp
vždy	vždy	k6eAd1	vždy
směřuje	směřovat	k5eAaImIp3nS	směřovat
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
okraji	okraj	k1gInSc3	okraj
listu	list	k1gInSc2	list
a	a	k8xC	a
ležícími	ležící	k2eAgFnPc7d1	ležící
na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
ležícího	ležící	k2eAgInSc2d1	ležící
uvnitř	uvnitř	k7c2	uvnitř
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
tvořeného	tvořený	k2eAgInSc2d1	tvořený
vlastním	vlastní	k2eAgInSc7d1	vlastní
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
pomyslnou	pomyslný	k2eAgFnSc7d1	pomyslná
spojnicí	spojnice	k1gFnSc7	spojnice
obou	dva	k4xCgInPc2	dva
růžků	růžek	k1gInPc2	růžek
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
egyptské	egyptský	k2eAgFnPc4d1	egyptská
nacionalisty	nacionalista	k1gMnPc7	nacionalista
<g/>
,	,	kIx,	,
muslimy	muslim	k1gMnPc7	muslim
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
úrodném	úrodný	k2eAgNnSc6d1	úrodné
údolí	údolí	k1gNnSc6	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Půlměsíc	půlměsíc	k1gInSc1	půlměsíc
byl	být	k5eAaImAgInS	být
symbolem	symbol	k1gInSc7	symbol
Islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
3	[number]	k4	3
hlavní	hlavní	k2eAgFnSc6d1	hlavní
části	část	k1gFnSc6	část
společného	společný	k2eAgNnSc2d1	společné
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Núbii	Núbie	k1gFnSc4	Núbie
a	a	k8xC	a
Súdán	Súdán	k1gInSc4	Súdán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
3	[number]	k4	3
egyptská	egyptský	k2eAgNnPc1d1	egyptské
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
:	:	kIx,	:
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
judaismus	judaismus	k1gInSc4	judaismus
<g/>
.	.	kIx.	.
<g/>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgFnP	zavést
také	také	k9	také
standarty	standarta	k1gFnPc1	standarta
pro	pro	k7c4	pro
krále	král	k1gMnSc4	král
a	a	k8xC	a
korunního	korunní	k2eAgMnSc4d1	korunní
prince	princ	k1gMnSc4	princ
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
variantách	varianta	k1gFnPc6	varianta
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgFnSc2d1	běžná
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc2d1	námořní
a	a	k8xC	a
letecké	letecký	k2eAgFnSc2d1	letecká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Standarty	standarta	k1gFnPc1	standarta
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
byly	být	k5eAaImAgFnP	být
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
zakončeny	zakončen	k2eAgInPc1d1	zakončen
dvěma	dva	k4xCgInPc7	dva
prameny	pramen	k1gInPc7	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1951	[number]	k4	1951
byla	být	k5eAaImAgFnS	být
jednostranně	jednostranně	k6eAd1	jednostranně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
britsko-egyptská	britskogyptský	k2eAgFnSc1d1	britsko-egyptský
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
následovaly	následovat	k5eAaImAgInP	následovat
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
a	a	k8xC	a
po	po	k7c6	po
Egyptské	egyptský	k2eAgFnSc6d1	egyptská
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
Muhammad	Muhammad	k1gInSc1	Muhammad
Nadžíb	Nadžíb	k1gInSc1	Nadžíb
<g/>
,	,	kIx,	,
Gamál	Gamál	k1gMnSc1	Gamál
Násir	Násir	k1gMnSc1	Násir
<g/>
)	)	kIx)	)
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
král	král	k1gMnSc1	král
Farúk	Farúk	k1gMnSc1	Farúk
I.	I.	kA	I.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
(	(	kIx(	(
<g/>
po	po	k7c6	po
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
Egyptského	egyptský	k2eAgNnSc2d1	egyptské
osvobozeneckého	osvobozenecký	k2eAgNnSc2d1	osvobozenecké
hnutí	hnutí	k1gNnSc2	hnutí
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
panafrických	panafrický	k2eAgFnPc6d1	panafrická
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
červeným	červené	k1gNnSc7	červené
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
černým	černý	k2eAgNnSc7d1	černé
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
byl	být	k5eAaImAgInS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
znak	znak	k1gInSc1	znak
<g/>
:	:	kIx,	:
žlutý	žlutý	k2eAgMnSc1d1	žlutý
orel	orel	k1gMnSc1	orel
hledící	hledící	k2eAgFnSc2d1	hledící
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Saladinův	Saladinův	k2eAgMnSc1d1	Saladinův
orel	orel	k1gMnSc1	orel
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
zelený	zelený	k2eAgInSc1d1	zelený
kulatý	kulatý	k2eAgInSc1d1	kulatý
štít	štít	k1gInSc1	štít
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
otevřeným	otevřený	k2eAgInSc7d1	otevřený
k	k	k7c3	k
hornímu	horní	k2eAgInSc3d1	horní
okraji	okraj	k1gInSc3	okraj
a	a	k8xC	a
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
třemi	tři	k4xCgFnPc7	tři
bílými	bílý	k2eAgFnPc7d1	bílá
pěticípými	pěticípý	k2eAgFnPc7d1	pěticípá
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1958	[number]	k4	1958
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1961	[number]	k4	1961
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
spojil	spojit	k5eAaPmAgInS	spojit
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
federace	federace	k1gFnSc2	federace
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
pruh	pruh	k1gInSc4	pruh
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
vlajce	vlajka	k1gFnSc6	vlajka
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
znaku	znak	k1gInSc2	znak
však	však	k9	však
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pruhu	pruh	k1gInSc2	pruh
umístěny	umístěn	k2eAgFnPc1d1	umístěna
dvě	dva	k4xCgFnPc1	dva
zelené	zelený	k2eAgFnPc1d1	zelená
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInPc1d1	symbolizující
oba	dva	k4xCgInPc1	dva
členy	člen	k1gInPc1	člen
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
existovala	existovat	k5eAaImAgFnS	existovat
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
)	)	kIx)	)
i	i	k8xC	i
konfederace	konfederace	k1gFnSc2	konfederace
Sjednocené	sjednocený	k2eAgInPc1d1	sjednocený
arabské	arabský	k2eAgInPc1d1	arabský
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
a	a	k8xC	a
Jemen	Jemen	k1gInSc1	Jemen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1961	[number]	k4	1961
se	se	k3xPyFc4	se
federace	federace	k1gFnSc1	federace
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgInS	ponechat
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
i	i	k8xC	i
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
(	(	kIx(	(
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
Federace	federace	k1gFnSc2	federace
arabských	arabský	k2eAgFnPc2d1	arabská
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gFnPc7	jejíž
členy	člen	k1gMnPc4	člen
byly	být	k5eAaImAgFnP	být
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
na	na	k7c6	na
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
federace	federace	k1gFnSc2	federace
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
červeno-bílo-černá	červenoílo-černý	k2eAgFnSc1d1	červeno-bílo-černý
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
znakem	znak	k1gInSc7	znak
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
heraldicky	heraldicky	k6eAd1	heraldicky
vlevo	vlevo	k6eAd1	vlevo
hledící	hledící	k2eAgMnSc1d1	hledící
žlutý	žlutý	k2eAgMnSc1d1	žlutý
sokol	sokol	k1gMnSc1	sokol
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
kmene	kmen	k1gInSc2	kmen
Kurajšovců	Kurajšovec	k1gMnPc2	Kurajšovec
<g/>
)	)	kIx)	)
s	s	k7c7	s
prázdným	prázdný	k2eAgInSc7d1	prázdný
žlutým	žlutý	k2eAgInSc7d1	žlutý
štítem	štít	k1gInSc7	štít
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
pařátech	pařát	k1gInPc6	pařát
stuhu	stuha	k1gFnSc4	stuha
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
textem	text	k1gInSc7	text
Federace	federace	k1gFnSc2	federace
arabských	arabský	k2eAgFnPc2d1	arabská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stuhou	stuha	k1gFnSc7	stuha
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
umístěn	umístit	k5eAaPmNgInS	umístit
arabský	arabský	k2eAgInSc1d1	arabský
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
oběti	oběť	k1gFnPc4	oběť
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mír	mír	k1gInSc4	mír
a	a	k8xC	a
černá	černat	k5eAaImIp3nS	černat
chmurnou	chmurný	k2eAgFnSc4d1	chmurná
koloniální	koloniální	k2eAgFnSc4d1	koloniální
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1984	[number]	k4	1984
Egypt	Egypt	k1gInSc1	Egypt
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
federace	federace	k1gFnSc1	federace
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
(	(	kIx(	(
<g/>
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Federace	federace	k1gFnSc2	federace
arabských	arabský	k2eAgFnPc2d1	arabská
republik	republika	k1gFnPc2	republika
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
datum	datum	k1gNnSc1	datum
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1984	[number]	k4	1984
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
znaku	znak	k1gInSc2	znak
užívaného	užívaný	k2eAgInSc2d1	užívaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
neobsahoval	obsahovat	k5eNaImAgInS	obsahovat
dvě	dva	k4xCgFnPc4	dva
zelené	zelený	k2eAgFnPc4d1	zelená
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
žluté	žlutý	k2eAgFnSc6d1	žlutá
kartuši	kartuš	k1gFnSc6	kartuš
byl	být	k5eAaImAgInS	být
černý	černý	k2eAgInSc1d1	černý
arabský	arabský	k2eAgInSc1d1	arabský
nápis	nápis	k1gInSc1	nápis
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrhy	návrh	k1gInPc1	návrh
egyptských	egyptský	k2eAgFnPc2d1	egyptská
vlajek	vlajka	k1gFnPc2	vlajka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
egyptských	egyptský	k2eAgInPc2d1	egyptský
guvernorátů	guvernorát	k1gInPc2	guvernorát
==	==	k?	==
</s>
</p>
<p>
<s>
Egypt	Egypt	k1gInSc1	Egypt
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
27	[number]	k4	27
guvernorátů	guvernorát	k1gInPc2	guvernorát
(	(	kIx(	(
<g/>
muháfazát	muháfazát	k1gInSc1	muháfazát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
byly	být	k5eAaImAgFnP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
o	o	k7c4	o
guvernorát	guvernorát	k1gInSc4	guvernorát
Helvan	Helvan	k1gMnSc1	Helvan
a	a	k8xC	a
Šestý	šestý	k4xOgInSc1	šestý
říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
zanedlouho	zanedlouho	k6eAd1	zanedlouho
však	však	k9	však
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
a	a	k8xC	a
seznam	seznam	k1gInSc1	seznam
znovu	znovu	k6eAd1	znovu
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jednotky	jednotka	k1gFnPc1	jednotka
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazeni	zobrazen	k2eAgMnPc1d1	zobrazen
je	on	k3xPp3gNnSc4	on
však	však	k9	však
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Egypta	Egypt	k1gInSc2	Egypt
</s>
</p>
<p>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Egypta	Egypt	k1gInSc2	Egypt
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
