<s>
Klaviatura	klaviatura	k1gFnSc1	klaviatura
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
černých	černý	k2eAgFnPc2d1	černá
a	a	k8xC	a
bílých	bílý	k2eAgFnPc2d1	bílá
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
stiskem	stisk	k1gInSc7	stisk
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pianista	pianista	k1gMnSc1	pianista
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
