<s>
Nuda	nuda	k1gFnSc1	nuda
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komedie	komedie	k1gFnSc1	komedie
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Morávka	Morávek	k1gMnSc2	Morávek
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
pět	pět	k4xCc4	pět
Českých	český	k2eAgInPc2d1	český
lvů	lev	k1gInPc2	lev
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
)	)	kIx)	)
a	a	k8xC	a
střih	střih	k1gInSc1	střih
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mírně	mírně	k6eAd1	mírně
retardovaný	retardovaný	k2eAgMnSc1d1	retardovaný
mladík	mladík	k1gMnSc1	mladík
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
strávit	strávit	k5eAaPmF	strávit
první	první	k4xOgFnSc4	první
noc	noc	k1gFnSc4	noc
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Holánová	Holánová	k1gFnSc1	Holánová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
připravují	připravovat	k5eAaImIp3nP	připravovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pečlivě	pečlivě	k6eAd1	pečlivě
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
zádrhelích	zádrhel	k1gInPc6	zádrhel
se	se	k3xPyFc4	se
věc	věc	k1gFnSc1	věc
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ústřední	ústřední	k2eAgFnSc2d1	ústřední
dvojice	dvojice	k1gFnSc2	dvojice
se	se	k3xPyFc4	se
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
noc	noc	k1gFnSc4	noc
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
domě	dům	k1gInSc6	dům
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
soulož	soulož	k1gFnSc4	soulož
více	hodně	k6eAd2	hodně
dvojic	dvojice	k1gFnPc2	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
ne	ne	k9	ne
tak	tak	k9	tak
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
...	...	k?	...
České	český	k2eAgNnSc1d1	české
filmové	filmový	k2eAgNnSc1d1	filmové
nebe	nebe	k1gNnSc1	nebe
-	-	kIx~	-
Nuda	nuda	k1gFnSc1	nuda
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
