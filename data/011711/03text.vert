<p>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
folklórní	folklórní	k2eAgInSc1d1	folklórní
žánr	žánr	k1gInSc1	žánr
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
ústní	ústní	k2eAgFnPc4d1	ústní
lidové	lidový	k2eAgFnPc4d1	lidová
slovesnosti	slovesnost	k1gFnPc4	slovesnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecná	obecný	k2eAgFnSc1d1	obecná
charakteristika	charakteristika	k1gFnSc1	charakteristika
pohádek	pohádka	k1gFnPc2	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vyprávění	vyprávění	k1gNnSc4	vyprávění
založené	založený	k2eAgNnSc4d1	založené
na	na	k7c4	na
básnické	básnický	k2eAgFnPc4d1	básnická
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
nadpřirozených	nadpřirozený	k2eAgInPc2d1	nadpřirozený
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohádkový	pohádkový	k2eAgInSc1d1	pohádkový
příběh	příběh	k1gInSc1	příběh
není	být	k5eNaImIp3nS	být
původně	původně	k6eAd1	původně
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
samotný	samotný	k2eAgInSc1d1	samotný
není	být	k5eNaImIp3nS	být
vypravěčem	vypravěč	k1gMnSc7	vypravěč
prezentován	prezentován	k2eAgMnSc1d1	prezentován
jako	jako	k8xS	jako
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
a	a	k8xC	a
posluchači	posluchač	k1gMnPc1	posluchač
zpravidla	zpravidla	k6eAd1	zpravidla
také	také	k9	také
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
odehrát	odehrát	k5eAaPmF	odehrát
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
slohové	slohový	k2eAgFnSc6d1	slohová
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
pohádka	pohádka	k1gFnSc1	pohádka
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
ustálenými	ustálený	k2eAgInPc7d1	ustálený
začátky	začátek	k1gInPc7	začátek
i	i	k8xC	i
závěry	závěr	k1gInPc7	závěr
a	a	k8xC	a
doslovně	doslovně	k6eAd1	doslovně
opakované	opakovaný	k2eAgFnSc3d1	opakovaná
a	a	k8xC	a
přejímané	přejímaný	k2eAgFnSc3d1	Přejímaná
přímé	přímý	k2eAgFnSc3d1	přímá
řeči	řeč	k1gFnSc3	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
zde	zde	k6eAd1	zde
také	také	k9	také
sehrávají	sehrávat	k5eAaImIp3nP	sehrávat
některá	některý	k3yIgNnPc4	některý
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
číslo	číslo	k1gNnSc1	číslo
tři	tři	k4xCgNnPc1	tři
(	(	kIx(	(
<g/>
trojí	trojí	k4xRgNnPc4	trojí
opakování	opakování	k1gNnPc4	opakování
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc1	předmět
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
sudičky	sudička	k1gFnPc1	sudička
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
čísla	číslo	k1gNnSc2	číslo
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
<g/>
,	,	kIx,	,
dvanáct	dvanáct	k4xCc4	dvanáct
<g/>
,	,	kIx,	,
třináct	třináct	k4xCc4	třináct
<g/>
,	,	kIx,	,
čtyřicet	čtyřicet	k4xCc4	čtyřicet
<g/>
,	,	kIx,	,
sto	sto	k4xCgNnSc4	sto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
syžetu	syžet	k1gInSc2	syžet
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
gradace	gradace	k1gFnSc1	gradace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Figury	figura	k1gFnPc1	figura
a	a	k8xC	a
situace	situace	k1gFnPc1	situace
nejsou	být	k5eNaImIp3nP	být
podrobně	podrobně	k6eAd1	podrobně
specifikovány	specifikován	k2eAgFnPc1d1	specifikována
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
obecné	obecný	k2eAgInPc1d1	obecný
(	(	kIx(	(
<g/>
v	v	k7c6	v
dalekém	daleký	k2eAgNnSc6d1	daleké
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neuplatňuje	uplatňovat	k5eNaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
skutečná	skutečný	k2eAgFnSc1d1	skutečná
geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jména	jméno	k1gNnSc2	jméno
historických	historický	k2eAgFnPc2d1	historická
nebo	nebo	k8xC	nebo
současných	současný	k2eAgFnPc2d1	současná
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
neurčité	určitý	k2eNgFnSc6d1	neurčitá
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
tedy	tedy	k9	tedy
uváděny	uváděn	k2eAgInPc1d1	uváděn
letopočty	letopočet	k1gInPc1	letopočet
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
jen	jen	k9	jen
obecné	obecný	k2eAgInPc4d1	obecný
odkazy	odkaz	k1gInPc4	odkaz
(	(	kIx(	(
<g/>
za	za	k7c2	za
dávných	dávný	k2eAgInPc2d1	dávný
časů	čas	k1gInPc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nereálné	reálný	k2eNgNnSc1d1	nereálné
a	a	k8xC	a
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
pohádkovém	pohádkový	k2eAgInSc6d1	pohádkový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazněny	zdůrazněn	k2eAgInPc1d1	zdůrazněn
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
</s>
</p>
<p>
<s>
kontrasty	kontrast	k1gInPc1	kontrast
<g/>
:	:	kIx,	:
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
/	/	kIx~	/
<g/>
zlý	zlý	k2eAgInSc1d1	zlý
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
<g/>
/	/	kIx~	/
<g/>
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc1d1	bohatý
<g/>
/	/	kIx~	/
<g/>
chudý	chudý	k2eAgMnSc1d1	chudý
<g/>
,	,	kIx,	,
vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
/	/	kIx~	/
<g/>
prostý	prostý	k2eAgMnSc1d1	prostý
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
dějových	dějový	k2eAgFnPc2d1	dějová
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
,	,	kIx,	,
nestává	stávat	k5eNaImIp3nS	stávat
se	se	k3xPyFc4	se
pohádka	pohádka	k1gFnSc1	pohádka
volnou	volnou	k6eAd1	volnou
hrou	hra	k1gFnSc7	hra
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
má	mít	k5eAaImIp3nS	mít
přísná	přísný	k2eAgNnPc4d1	přísné
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
pohádky	pohádka	k1gFnSc2	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
pohádky	pohádka	k1gFnSc2	pohádka
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgFnPc1d1	jistá
zápletky	zápletka	k1gFnPc1	zápletka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
6000	[number]	k4	6000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnPc1d1	stará
jako	jako	k8xC	jako
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradovaná	tradovaný	k2eAgNnPc4d1	tradované
vyprávění	vyprávění	k1gNnPc4	vyprávění
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
evropské	evropský	k2eAgFnSc2d1	Evropská
pohádky	pohádka	k1gFnSc2	pohádka
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
ve	v	k7c6	v
staroindickém	staroindický	k2eAgInSc6d1	staroindický
eposu	epos	k1gInSc6	epos
(	(	kIx(	(
<g/>
Pančatantra	Pančatantrum	k1gNnSc2	Pančatantrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staroegyptské	staroegyptský	k2eAgFnSc6d1	staroegyptská
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
Anu	Anu	k1gFnSc1	Anu
a	a	k8xC	a
Bata	Bata	k1gFnSc1	Bata
<g/>
,	,	kIx,	,
Rhampsinitův	Rhampsinitův	k2eAgInSc1d1	Rhampsinitův
poklad	poklad	k1gInSc1	poklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohádkové	pohádkový	k2eAgInPc1d1	pohádkový
prvky	prvek	k1gInPc1	prvek
lze	lze	k6eAd1	lze
též	též	k9	též
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
starověkých	starověký	k2eAgInPc6d1	starověký
mýtech	mýtus	k1gInPc6	mýtus
předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
a	a	k8xC	a
v	v	k7c6	v
obsáhlých	obsáhlý	k2eAgInPc6d1	obsáhlý
epických	epický	k2eAgInPc6d1	epický
textech	text	k1gInPc6	text
(	(	kIx(	(
<g/>
Gilgameš	Gilgameš	k1gMnSc1	Gilgameš
<g/>
,	,	kIx,	,
Etana	Etaen	k2eAgFnSc1d1	Etaen
illujanka	illujanka	k1gFnSc1	illujanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
antických	antický	k2eAgInPc6d1	antický
textech	text	k1gInPc6	text
(	(	kIx(	(
<g/>
Amor	Amor	k1gMnSc1	Amor
a	a	k8xC	a
Psýché	Psýché	k1gFnSc1	Psýché
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Literární	literární	k2eAgNnSc1d1	literární
zpracování	zpracování	k1gNnSc1	zpracování
pohádek	pohádka	k1gFnPc2	pohádka
přišlo	přijít	k5eAaPmAgNnS	přijít
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
je	on	k3xPp3gNnPc4	on
Dekameron	Dekameron	k1gInSc1	Dekameron
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
G.	G.	kA	G.
<g/>
Boccaccia	Boccaccium	k1gNnPc4	Boccaccium
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
anglické	anglický	k2eAgNnSc1d1	anglické
dílo	dílo	k1gNnSc1	dílo
Gesta	gesto	k1gNnSc2	gesto
Romanorum	Romanorum	k1gInSc4	Romanorum
<g/>
,	,	kIx,	,
spis	spis	k1gInSc4	spis
G.	G.	kA	G.
<g/>
F.	F.	kA	F.
<g/>
Straparoly	Straparol	k1gInPc1	Straparol
Le	Le	k1gFnSc2	Le
piacevoli	piacevole	k1gFnSc3	piacevole
notti	notť	k1gFnSc2	notť
(	(	kIx(	(
<g/>
1550	[number]	k4	1550
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pentameron	Pentameron	k1gMnSc1	Pentameron
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
G.	G.	kA	G.
<g/>
Basileho	Basile	k1gMnSc2	Basile
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1696	[number]	k4	1696
vyšla	vyjít	k5eAaPmAgFnS	vyjít
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
sbírka	sbírka	k1gFnSc1	sbírka
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Perraulta	Perraulta	k1gMnSc1	Perraulta
Pohádky	pohádka	k1gFnSc2	pohádka
matky	matka	k1gFnSc2	matka
Husy	husa	k1gFnSc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
pohádek	pohádka	k1gFnPc2	pohádka
Tisíc	tisíc	k4xCgInSc1	tisíc
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
1704	[number]	k4	1704
–	–	k?	–
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
sbírka	sbírka	k1gFnSc1	sbírka
Kinder	Kindra	k1gFnPc2	Kindra
und	und	k?	und
Hausmärchen	Hausmärchna	k1gFnPc2	Hausmärchna
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
–	–	k?	–
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
čítající	čítající	k2eAgFnSc1d1	čítající
204	[number]	k4	204
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
shromážděných	shromážděný	k2eAgInPc2d1	shromážděný
bratry	bratr	k1gMnPc7	bratr
Grimmovými	Grimmův	k2eAgMnPc7d1	Grimmův
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
</s>
</p>
<p>
<s>
badatelským	badatelský	k2eAgNnSc7d1	badatelské
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
i	i	k9	i
Súpis	Súpis	k1gFnSc1	Súpis
slovenských	slovenský	k2eAgInPc2d1	slovenský
rozprávok	rozprávok	k1gInSc4	rozprávok
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
-31	-31	k4	-31
<g/>
)	)	kIx)	)
od	od	k7c2	od
J.	J.	kA	J.
<g/>
Polívky	polívka	k1gFnSc2	polívka
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
pohádky	pohádka	k1gFnSc2	pohádka
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
podle	podle	k7c2	podle
základních	základní	k2eAgInPc2d1	základní
námětů	námět	k1gInPc2	námět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rozprávky	rozprávka	k1gFnSc2	rozprávka
o	o	k7c6	o
nadprirodzených	nadprirodzená	k1gFnPc6	nadprirodzená
<g/>
,	,	kIx,	,
zvieracie	zvieracie	k1gFnPc1	zvieracie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
badatel	badatel	k1gMnSc1	badatel
V.	V.	kA	V.
Tille	Till	k1gMnSc4	Till
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
řadí	řadit	k5eAaImIp3nP	řadit
pohádky	pohádka	k1gFnPc4	pohádka
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
u	u	k7c2	u
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známých	známý	k2eAgInPc2d1	známý
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
i	i	k9	i
originální	originální	k2eAgInSc4d1	originální
název	název	k1gInSc4	název
<g/>
.1	.1	k4	.1
</s>
</p>
<p>
<s>
==	==	k?	==
Folklorní	folklorní	k2eAgFnPc4d1	folklorní
pohádky	pohádka	k1gFnPc4	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Folklórní	folklórní	k2eAgFnPc1d1	folklórní
pohádky	pohádka	k1gFnPc1	pohádka
jsou	být	k5eAaImIp3nP	být
útvar	útvar	k1gInSc4	útvar
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
,	,	kIx,	,
ústním	ústní	k2eAgNnSc7d1	ústní
podáním	podání	k1gNnSc7	podání
tak	tak	k6eAd1	tak
prošly	projít	k5eAaPmAgFnP	projít
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgFnPc2d1	různá
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
pohádky	pohádka	k1gFnPc1	pohádka
jsou	být	k5eAaImIp3nP	být
žánrem	žánr	k1gInSc7	žánr
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
a	a	k8xC	a
nejrozšířenějším	rozšířený	k2eAgFnPc3d3	nejrozšířenější
formám	forma	k1gFnPc3	forma
ústní	ústní	k2eAgFnSc2d1	ústní
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
menší	malý	k2eAgFnPc4d2	menší
sociální	sociální	k2eAgFnPc4d1	sociální
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
odrážel	odrážet	k5eAaImAgMnS	odrážet
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc7	vztah
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
či	či	k8xC	či
fantastickém	fantastický	k2eAgInSc6d1	fantastický
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
satiricky	satiricky	k6eAd1	satiricky
anebo	anebo	k8xC	anebo
humorně	humorně	k6eAd1	humorně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrdinové	hrdina	k1gMnPc1	hrdina
překonali	překonat	k5eAaPmAgMnP	překonat
útlak	útlak	k1gInSc4	útlak
<g/>
,	,	kIx,	,
bídu	bída	k1gFnSc4	bída
a	a	k8xC	a
chudobu	chudoba	k1gFnSc4	chudoba
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
dobrosrdečností	dobrosrdečnost	k1gFnSc7	dobrosrdečnost
a	a	k8xC	a
mravností	mravnost	k1gFnSc7	mravnost
vítězí	vítězit	k5eAaImIp3nS	vítězit
nad	nad	k7c7	nad
zlem	zlo	k1gNnSc7	zlo
<g/>
,	,	kIx,	,
úklady	úklad	k1gInPc4	úklad
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
draků	drak	k1gMnPc2	drak
(	(	kIx(	(
<g/>
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
zlých	zlý	k2eAgFnPc2d1	zlá
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
postav	postava	k1gFnPc2	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
českých	český	k2eAgFnPc2d1	Česká
pohádek	pohádka	k1gFnPc2	pohádka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlo	zlo	k1gNnSc1	zlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
často	často	k6eAd1	často
napraveno	napraven	k2eAgNnSc1d1	napraveno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ale	ale	k8xC	ale
jedinou	jediný	k2eAgFnSc7d1	jediná
neautorskou	autorský	k2eNgFnSc7d1	neautorská
pohádkou	pohádka	k1gFnSc7	pohádka
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
pohádka	pohádka	k1gFnSc1	pohádka
O	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
měsíčkách	měsíček	k1gInPc6	měsíček
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
postava	postava	k1gFnSc1	postava
Honzy	Honza	k1gMnSc2	Honza
není	být	k5eNaImIp3nS	být
zdejší	zdejší	k2eAgNnSc1d1	zdejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
folklórních	folklórní	k2eAgFnPc2d1	folklórní
pohádek	pohádka	k1gFnPc2	pohádka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
světových	světový	k2eAgFnPc6d1	světová
kulturách	kultura	k1gFnPc6	kultura
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc1d1	podobný
rysy	rys	k1gInPc1	rys
i	i	k8xC	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
kontakt	kontakt	k1gInSc1	kontakt
těchto	tento	k3xDgFnPc2	tento
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
pohádky	pohádka	k1gFnPc1	pohádka
tak	tak	k9	tak
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
v	v	k7c4	v
podání	podání	k1gNnSc4	podání
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
u	u	k7c2	u
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
asijských	asijský	k2eAgFnPc2d1	asijská
pohádek	pohádka	k1gFnPc2	pohádka
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
zarážející	zarážející	k2eAgMnSc1d1	zarážející
–	–	k?	–
tyto	tento	k3xDgFnPc1	tento
mohly	moct	k5eAaImAgFnP	moct
cestovat	cestovat	k5eAaImF	cestovat
s	s	k7c7	s
obchodníky	obchodník	k1gMnPc7	obchodník
<g/>
,	,	kIx,	,
námořníky	námořník	k1gMnPc7	námořník
či	či	k8xC	či
vojáky	voják	k1gMnPc7	voják
–	–	k?	–
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
je	být	k5eAaImIp3nS	být
však	však	k9	však
podobnost	podobnost	k1gFnSc4	podobnost
pohádek	pohádka	k1gFnPc2	pohádka
euroasijských	euroasijský	k2eAgMnPc2d1	euroasijský
a	a	k8xC	a
indiánských	indiánský	k2eAgMnPc2d1	indiánský
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
pohledy	pohled	k1gInPc4	pohled
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Archetypální	archetypální	k2eAgFnSc1d1	archetypální
teorie	teorie	k1gFnSc1	teorie
–	–	k?	–
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
situacích	situace	k1gFnPc6	situace
jednají	jednat	k5eAaImIp3nP	jednat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
i	i	k9	i
podobné	podobný	k2eAgInPc4d1	podobný
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
migrace	migrace	k1gFnSc2	migrace
pohádek	pohádka	k1gFnPc2	pohádka
–	–	k?	–
zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
tato	tento	k3xDgFnSc1	tento
shoda	shoda	k1gFnSc1	shoda
vysvětlována	vysvětlován	k2eAgFnSc1d1	vysvětlována
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předci	předek	k1gMnPc1	předek
indiánů	indián	k1gMnPc2	indián
si	se	k3xPyFc3	se
již	již	k6eAd1	již
základy	základ	k1gInPc4	základ
těchto	tento	k3xDgFnPc2	tento
pohádek	pohádka	k1gFnPc2	pohádka
vzali	vzít	k5eAaPmAgMnP	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
při	pře	k1gFnSc4	pře
stěhování	stěhování	k1gNnSc2	stěhování
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
Folklorní	folklorní	k2eAgFnPc1d1	folklorní
pohádky	pohádka	k1gFnPc1	pohádka
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
předváděny	předvádět	k5eAaImNgFnP	předvádět
formou	forma	k1gFnSc7	forma
domácího	domácí	k2eAgNnSc2d1	domácí
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
těchto	tento	k3xDgFnPc2	tento
soukromých	soukromý	k2eAgFnPc2d1	soukromá
představení	představení	k1gNnPc1	představení
však	však	k9	však
neexistují	existovat	k5eNaImIp3nP	existovat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc4	žádný
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autorské	autorský	k2eAgFnPc4d1	autorská
pohádky	pohádka	k1gFnPc4	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Autorské	autorský	k2eAgFnPc1d1	autorská
pohádky	pohádka	k1gFnPc1	pohádka
rozhodně	rozhodně	k6eAd1	rozhodně
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
doménou	doména	k1gFnSc7	doména
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
pohádky	pohádka	k1gFnPc1	pohádka
vznikaly	vznikat	k5eAaImAgFnP	vznikat
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zpracováním	zpracování	k1gNnSc7	zpracování
nějakého	nějaký	k3yIgNnSc2	nějaký
známého	známý	k2eAgNnSc2d1	známé
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
takto	takto	k6eAd1	takto
byla	být	k5eAaImAgFnS	být
zpracovávána	zpracováván	k2eAgFnSc1d1	zpracovávána
např.	např.	kA	např.
rytířská	rytířský	k2eAgFnSc1d1	rytířská
epika	epika	k1gFnSc1	epika
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
–	–	k?	–
prostého	prostý	k2eAgNnSc2d1	prosté
–	–	k?	–
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
autorských	autorský	k2eAgFnPc2d1	autorská
pohádek	pohádka	k1gFnPc2	pohádka
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
započítávat	započítávat	k5eAaImF	započítávat
i	i	k9	i
tzv.	tzv.	kA	tzv.
divadelní	divadelní	k2eAgFnSc2d1	divadelní
a	a	k8xC	a
filmové	filmový	k2eAgFnSc2d1	filmová
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zpravidla	zpravidla	k6eAd1	zpravidla
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zásadnímu	zásadní	k2eAgNnSc3d1	zásadní
narušení	narušení	k1gNnSc3	narušení
původního	původní	k2eAgInSc2d1	původní
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
původní	původní	k2eAgInSc4d1	původní
smysl	smysl	k1gInSc4	smysl
pohádky	pohádka	k1gFnSc2	pohádka
často	často	k6eAd1	často
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
záměru	záměr	k1gInSc2	záměr
zpracovatele	zpracovatel	k1gMnSc2	zpracovatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
–	–	k?	–
české	český	k2eAgNnSc1d1	české
národní	národní	k2eAgNnSc1d1	národní
obrození	obrození	k1gNnSc1	obrození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tvorba	tvorba	k1gFnSc1	tvorba
pohádek	pohádka	k1gFnPc2	pohádka
komercializuje	komercializovat	k5eAaBmIp3nS	komercializovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
místy	místy	k6eAd1	místy
ke	k	k7c3	k
směřování	směřování	k1gNnSc3	směřování
k	k	k7c3	k
poutavosti	poutavost	k1gFnSc3	poutavost
a	a	k8xC	a
zábavnosti	zábavnost	k1gFnSc3	zábavnost
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
hlubšího	hluboký	k2eAgInSc2d2	hlubší
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
poučnosti	poučnost	k1gFnSc2	poučnost
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc4d1	nová
formy	forma	k1gFnPc4	forma
pohádek	pohádka	k1gFnPc2	pohádka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
videoher	videohra	k1gFnPc2	videohra
a	a	k8xC	a
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgNnPc1d1	základní
témata	téma	k1gNnPc1	téma
==	==	k?	==
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
motivem	motiv	k1gInSc7	motiv
pohádek	pohádka	k1gFnPc2	pohádka
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
boj	boj	k1gInSc4	boj
dobra	dobro	k1gNnSc2	dobro
se	s	k7c7	s
zlem	zlo	k1gNnSc7	zlo
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
určené	určený	k2eAgFnPc4d1	určená
dětem	dítě	k1gFnPc3	dítě
končívají	končívat	k5eAaImIp3nP	končívat
vždy	vždy	k6eAd1	vždy
vítězstvím	vítězství	k1gNnSc7	vítězství
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Dobro	dobro	k1gNnSc1	dobro
i	i	k8xC	i
zlo	zlo	k1gNnSc1	zlo
bývá	bývat	k5eAaImIp3nS	bývat
povětšinou	povětšinou	k6eAd1	povětšinou
zosobněno	zosobněn	k2eAgNnSc1d1	zosobněno
do	do	k7c2	do
nějakých	nějaký	k3yIgFnPc2	nějaký
fantaskních	fantaskní	k2eAgFnPc2d1	fantaskní
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mívají	mívat	k5eAaImIp3nP	mívat
velmi	velmi	k6eAd1	velmi
vyhraněné	vyhraněný	k2eAgFnPc1d1	vyhraněná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
určené	určený	k2eAgFnPc1d1	určená
dospělému	dospělý	k2eAgNnSc3d1	dospělé
publiku	publikum	k1gNnSc3	publikum
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
velice	velice	k6eAd1	velice
hrubé	hrubý	k2eAgInPc1d1	hrubý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
s	s	k7c7	s
erotickým	erotický	k2eAgInSc7d1	erotický
podtextem	podtext	k1gInSc7	podtext
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Podkrušnohoří	Podkrušnohoří	k1gNnSc4	Podkrušnohoří
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
erotickým	erotický	k2eAgInSc7d1	erotický
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
pohádky	pohádka	k1gFnPc4	pohádka
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
až	až	k9	až
do	do	k7c2	do
mýtů	mýtus	k1gInPc2	mýtus
nebo	nebo	k8xC	nebo
reagovaly	reagovat	k5eAaBmAgFnP	reagovat
na	na	k7c4	na
drastické	drastický	k2eAgFnPc4d1	drastická
situace	situace	k1gFnPc4	situace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyplenění	vyplenění	k1gNnSc1	vyplenění
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
neúroda	neúroda	k1gFnSc1	neúroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
určeny	určit	k5eAaPmNgFnP	určit
různé	různý	k2eAgFnPc1d1	různá
variace	variace	k1gFnPc1	variace
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Angela	Angela	k1gFnSc1	Angela
Carterová	Carterová	k1gFnSc1	Carterová
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
povídkách	povídka	k1gFnPc6	povídka
zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
u	u	k7c2	u
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
motivů	motiv	k1gInPc2	motiv
sexuální	sexuální	k2eAgInSc1d1	sexuální
podtext	podtext	k1gInSc1	podtext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
podání	podání	k1gNnSc6	podání
například	například	k6eAd1	například
třináctiletá	třináctiletý	k2eAgFnSc1d1	třináctiletá
Karkula	Karkula	k1gFnSc1	Karkula
ve	v	k7c4	v
Společenství	společenství	k1gNnSc4	společenství
vlků	vlk	k1gMnPc2	vlk
svede	svést	k5eAaPmIp3nS	svést
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zachránila	zachránit	k5eAaPmAgFnS	zachránit
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
Modrovousova	modrovousův	k2eAgFnSc1d1	Modrovousova
manželka	manželka	k1gFnSc1	manželka
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
coby	coby	k?	coby
týraná	týraný	k2eAgFnSc1d1	týraná
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Parodií	parodie	k1gFnSc7	parodie
na	na	k7c4	na
pohádky	pohádka	k1gFnPc4	pohádka
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
Shrek	Shrek	k1gInSc1	Shrek
nebo	nebo	k8xC	nebo
Princezna	princezna	k1gFnSc1	princezna
nevěsta	nevěsta	k1gFnSc1	nevěsta
Williama	William	k1gMnSc4	William
Goldmana	Goldman	k1gMnSc4	Goldman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
pohádek	pohádka	k1gFnPc2	pohádka
je	být	k5eAaImIp3nS	být
motiv	motiv	k1gInSc4	motiv
daleké	daleký	k2eAgFnSc2d1	daleká
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
záměrem	záměr	k1gInSc7	záměr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zisk	zisk	k1gInSc4	zisk
slávy	sláva	k1gFnSc2	sláva
či	či	k8xC	či
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
stavěn	stavit	k5eAaImNgInS	stavit
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Interpretace	interpretace	k1gFnSc2	interpretace
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
variant	varianta	k1gFnPc2	varianta
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
těch	ten	k3xDgInPc2	ten
určených	určený	k2eAgInPc2d1	určený
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
obsažen	obsažen	k2eAgInSc1d1	obsažen
morální	morální	k2eAgInSc1d1	morální
apel	apel	k1gInSc1	apel
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
návodem	návod	k1gInSc7	návod
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rozlišit	rozlišit	k5eAaPmF	rozlišit
dobro	dobro	k1gNnSc4	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
poradit	poradit	k5eAaPmF	poradit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
některé	některý	k3yIgFnPc1	některý
moderní	moderní	k2eAgFnPc1d1	moderní
pohádky	pohádka	k1gFnPc1	pohádka
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
spoléhat	spoléhat	k5eAaImF	spoléhat
jen	jen	k9	jen
na	na	k7c4	na
barvitý	barvitý	k2eAgInSc4d1	barvitý
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc4d1	fantastický
motivy	motiv	k1gInPc4	motiv
pohádek	pohádka	k1gFnPc2	pohádka
ale	ale	k8xC	ale
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nebyly	být	k5eNaImAgFnP	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
ozdobou	ozdoba	k1gFnSc7	ozdoba
–	–	k?	–
měly	mít	k5eAaImAgFnP	mít
svůj	svůj	k3xOyFgInSc4	svůj
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
právě	právě	k9	právě
morální	morální	k2eAgMnSc1d1	morální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Perrault	Perrault	k1gMnSc1	Perrault
například	například	k6eAd1	například
vtěsnal	vtěsnat	k5eAaPmAgMnS	vtěsnat
do	do	k7c2	do
pohádky	pohádka	k1gFnSc2	pohádka
Popelka	Popelka	k1gMnSc1	Popelka
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
takových	takový	k3xDgInPc2	takový
apelů	apel	k1gInPc2	apel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ale	ale	k9	ale
úplně	úplně	k6eAd1	úplně
morální	morální	k2eAgNnSc1d1	morální
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Popelka	Popelka	k1gMnSc1	Popelka
shledá	shledat	k5eAaPmIp3nS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
jsou	být	k5eAaImIp3nP	být
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
neměla	mít	k5eNaImAgFnS	mít
kmotřičku	kmotřička	k1gFnSc4	kmotřička
–	–	k?	–
což	což	k3yRnSc1	což
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
důležitost	důležitost	k1gFnSc4	důležitost
sociální	sociální	k2eAgFnSc2d1	sociální
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vklouznutí	vklouznutí	k1gNnSc1	vklouznutí
nohy	noha	k1gFnSc2	noha
do	do	k7c2	do
střevíčku	střevíček	k1gInSc2	střevíček
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
o	o	k7c6	o
Popelce	Popelka	k1gFnSc6	Popelka
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vykládáno	vykládat	k5eAaImNgNnS	vykládat
jako	jako	k8xC	jako
výrazný	výrazný	k2eAgInSc1d1	výrazný
sexuální	sexuální	k2eAgInSc1d1	sexuální
symbol	symbol	k1gInSc1	symbol
a	a	k8xC	a
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
červené	červený	k2eAgFnSc6d1	červená
karkulce	karkulka	k1gFnSc6	karkulka
bývá	bývat	k5eAaImIp3nS	bývat
interpretována	interpretován	k2eAgFnSc1d1	interpretována
jako	jako	k8xS	jako
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
prostitucí	prostituce	k1gFnSc7	prostituce
nebo	nebo	k8xC	nebo
mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
slunečním	sluneční	k2eAgMnSc6d1	sluneční
bohu	bůh	k1gMnSc6	bůh
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k4c1	mnoho
pohádek	pohádka	k1gFnPc2	pohádka
bylo	být	k5eAaImAgNnS	být
podrobeno	podrobit	k5eAaPmNgNnS	podrobit
Freudovské	freudovský	k2eAgFnSc3d1	freudovská
psychoanalýze	psychoanalýza	k1gFnSc3	psychoanalýza
<g/>
,	,	kIx,	,
Jungovské	Jungovský	k2eAgFnSc3d1	Jungovská
analytické	analytický	k2eAgFnSc3d1	analytická
psychologii	psychologie	k1gFnSc3	psychologie
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
psychologickým	psychologický	k2eAgFnPc3d1	psychologická
analýzám	analýza	k1gFnPc3	analýza
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
interpretací	interpretace	k1gFnPc2	interpretace
ujala	ujmout	k5eAaPmAgFnS	ujmout
jako	jako	k9	jako
definitivní	definitivní	k2eAgFnSc1d1	definitivní
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
analýzy	analýza	k1gFnPc1	analýza
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přikládaly	přikládat	k5eAaImAgFnP	přikládat
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
motivům	motiv	k1gInPc3	motiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejsou	být	k5eNaImIp3nP	být
integrální	integrální	k2eAgFnSc7d1	integrální
částí	část	k1gFnSc7	část
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Modrovous	modrovous	k1gMnSc1	modrovous
například	například	k6eAd1	například
zradu	zrada	k1gFnSc4	zrada
manželky	manželka	k1gFnSc2	manželka
odhalí	odhalit	k5eAaPmIp3nS	odhalit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
klíč	klíč	k1gInSc4	klíč
zanechávající	zanechávající	k2eAgFnSc2d1	zanechávající
krvavé	krvavý	k2eAgFnSc2d1	krvavá
stopy	stopa	k1gFnSc2	stopa
<g/>
,	,	kIx,	,
rozbité	rozbitý	k2eAgNnSc1d1	rozbité
vejce	vejce	k1gNnSc1	vejce
nebo	nebo	k8xC	nebo
zpívající	zpívající	k2eAgFnSc1d1	zpívající
růže	růže	k1gFnSc1	růže
<g/>
.	.	kIx.	.
</s>
<s>
Neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
to	ten	k3xDgNnSc1	ten
zásadně	zásadně	k6eAd1	zásadně
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
interpretace	interpretace	k1gFnSc1	interpretace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
variant	varianta	k1gFnPc2	varianta
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ten	ten	k3xDgInSc4	ten
který	který	k3yQgInSc4	který
prozrazující	prozrazující	k2eAgInSc4d1	prozrazující
objekt	objekt	k1gInSc4	objekt
je	být	k5eAaImIp3nS	být
integrální	integrální	k2eAgFnSc7d1	integrální
částí	část	k1gFnSc7	část
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
folkloristé	folklorista	k1gMnPc1	folklorista
pokládají	pokládat	k5eAaImIp3nP	pokládat
pohádky	pohádka	k1gFnPc4	pohádka
za	za	k7c4	za
historické	historický	k2eAgInPc4d1	historický
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
postava	postava	k1gFnSc1	postava
zlé	zlý	k2eAgFnSc2d1	zlá
macechy	macecha	k1gFnSc2	macecha
je	být	k5eAaImIp3nS	být
interpretována	interpretovat	k5eAaBmNgFnS	interpretovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ženy	žena	k1gFnSc2	žena
často	často	k6eAd1	často
umíraly	umírat	k5eAaImAgInP	umírat
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
manžel	manžel	k1gMnSc1	manžel
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgInS	oženit
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
macecha	macecha	k1gFnSc1	macecha
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
bojovala	bojovat	k5eAaImAgFnS	bojovat
o	o	k7c4	o
přízeň	přízeň	k1gFnSc4	přízeň
i	i	k8xC	i
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Modely	model	k1gInPc1	model
prostředí	prostředí	k1gNnSc2	prostředí
pohádky	pohádka	k1gFnSc2	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Žánr	žánr	k1gInSc1	žánr
pohádky	pohádka	k1gFnSc2	pohádka
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
modely	model	k1gInPc7	model
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
přejímá	přejímat	k5eAaImIp3nS	přejímat
určité	určitý	k2eAgInPc4d1	určitý
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
prostředí	prostředí	k1gNnPc1	prostředí
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tři	tři	k4xCgInPc4	tři
referenční	referenční	k2eAgInPc4d1	referenční
horizonty	horizont	k1gInPc4	horizont
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
</s>
</p>
<p>
<s>
spadají	spadat	k5eAaPmIp3nP	spadat
obecně	obecně	k6eAd1	obecně
rozšířená	rozšířený	k2eAgNnPc4d1	rozšířené
kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
členění	členění	k1gNnSc4	členění
popsala	popsat	k5eAaPmAgFnS	popsat
Hana	Hana	k1gFnSc1	Hana
Šmahelová	Šmahelová	k1gFnSc1	Šmahelová
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
Prolamování	prolamování	k1gNnSc2	prolamování
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
vlastního	vlastní	k2eAgInSc2d1	vlastní
folklorního	folklorní	k2eAgInSc2d1	folklorní
prostředíZákladní	prostředíZákladný	k2eAgMnPc5d1	prostředíZákladný
</s>
</p>
<p>
<s>
rámec	rámec	k1gInSc1	rámec
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
tvoří	tvořit	k5eAaImIp3nP	tvořit
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
rituály	rituál	k1gInPc4	rituál
a	a	k8xC	a
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
</s>
</p>
<p>
<s>
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
jádro	jádro	k1gNnSc4	jádro
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
pohádky	pohádka	k1gFnSc2	pohádka
tak	tak	k6eAd1	tak
</s>
</p>
<p>
<s>
předznamenaly	předznamenat	k5eAaPmAgInP	předznamenat
určité	určitý	k2eAgInPc1d1	určitý
mýty	mýtus	k1gInPc1	mýtus
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnPc1d1	náboženská
a	a	k8xC	a
magické	magický	k2eAgFnPc1d1	magická
představy	představa	k1gFnPc1	představa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
</s>
</p>
<p>
<s>
původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
transformace	transformace	k1gFnSc2	transformace
a	a	k8xC	a
rozpadu	rozpad	k1gInSc3	rozpad
tradičních	tradiční	k2eAgFnPc2d1	tradiční
náboženských	náboženský	k2eAgFnPc2d1	náboženská
</s>
</p>
<p>
<s>
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
ztratil	ztratit	k5eAaPmAgInS	ztratit
svojí	svojit	k5eAaImIp3nS	svojit
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
přežíval	přežívat	k5eAaImAgMnS	přežívat
dále	daleko	k6eAd2	daleko
transformován	transformovat	k5eAaBmNgMnS	transformovat
do	do	k7c2	do
pouhého	pouhý	k2eAgInSc2d1	pouhý
</s>
</p>
<p>
<s>
vyprávění	vyprávění	k1gNnSc1	vyprávění
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
</s>
</p>
<p>
<s>
fázi	fáze	k1gFnSc4	fáze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyprávění	vyprávění	k1gNnSc1	vyprávění
sestoupilo	sestoupit	k5eAaPmAgNnS	sestoupit
do	do	k7c2	do
folklorního	folklorní	k2eAgNnSc2d1	folklorní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
se	se	k3xPyFc4	se
specifická	specifický	k2eAgNnPc1d1	specifické
</s>
</p>
<p>
<s>
narativní	narativní	k2eAgFnSc1d1	narativní
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
té	ten	k3xDgFnSc2	ten
lze	lze	k6eAd1	lze
pohádku	pohádka	k1gFnSc4	pohádka
dobře	dobře	k6eAd1	dobře
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
</s>
</p>
<p>
<s>
především	především	k9	především
o	o	k7c4	o
následnost	následnost	k1gFnSc4	následnost
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
zřetězení	zřetězení	k1gNnSc2	zřetězení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
</s>
</p>
<p>
<s>
akce	akce	k1gFnPc1	akce
otevírající	otevírající	k2eAgFnSc4d1	otevírající
další	další	k2eAgFnSc4d1	další
akci	akce	k1gFnSc4	akce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
princ	princ	k1gMnSc1	princ
musí	muset	k5eAaImIp3nS	muset
splnit	splnit	k5eAaPmF	splnit
těžký	těžký	k2eAgInSc4d1	těžký
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
od	od	k7c2	od
</s>
</p>
<p>
<s>
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
dostal	dostat	k5eAaPmAgInS	dostat
kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
prostředek	prostředek	k1gInSc1	prostředek
k	k	k7c3	k
vysvobození	vysvobození	k1gNnSc3	vysvobození
princezny	princezna	k1gFnSc2	princezna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohádku	pohádka	k1gFnSc4	pohádka
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
podle	podle	k7c2	podle
syžetového	syžetový	k2eAgInSc2d1	syžetový
modelu	model	k1gInSc2	model
<g/>
:	:	kIx,	:
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vyprávění	vyprávění	k1gNnPc4	vyprávění
odvíjející	odvíjející	k2eAgFnPc1d1	odvíjející
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
od	od	k7c2	od
škůdcovství	škůdcovství	k1gNnSc2	škůdcovství
(	(	kIx(	(
<g/>
drak	drak	k1gInSc1	drak
unese	unést	k5eAaPmIp3nS	unést
princeznu	princezna	k1gFnSc4	princezna
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
od	od	k7c2	od
nedostatku	nedostatek	k1gInSc2	nedostatek
(	(	kIx(	(
<g/>
ženich	ženich	k1gMnSc1	ženich
pro	pro	k7c4	pro
princeznu	princezna	k1gFnSc4	princezna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
živá	živý	k2eAgFnSc1d1	živá
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dostranění	dostranění	k1gNnSc3	dostranění
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
získání	získání	k1gNnSc4	získání
odměny	odměna	k1gFnSc2	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Proppa	Propp	k1gMnSc2	Propp
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
morfologickému	morfologický	k2eAgInSc3d1	morfologický
</s>
</p>
<p>
<s>
modelu	model	k1gInSc2	model
kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
pohádky	pohádka	k1gFnSc2	pohádka
ještě	ještě	k9	ještě
sedm	sedm	k4xCc1	sedm
kanonických	kanonický	k2eAgFnPc2d1	kanonická
postav	postava	k1gFnPc2	postava
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ideologií	ideologie	k1gFnSc7	ideologie
</s>
</p>
<p>
<s>
a	a	k8xC	a
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
motivace	motivace	k1gFnSc1	motivace
postav	postava	k1gFnPc2	postava
není	být	k5eNaImIp3nS	být
psychologická	psychologický	k2eAgFnSc1d1	psychologická
a	a	k8xC	a
morální	morální	k2eAgFnSc1d1	morální
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
dějová	dějový	k2eAgFnSc1d1	dějová
–	–	k?	–
</s>
</p>
<p>
<s>
postavy	postava	k1gFnPc1	postava
mají	mít	k5eAaImIp3nP	mít
dané	daný	k2eAgFnPc1d1	daná
určité	určitý	k2eAgFnPc1d1	určitá
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
</s>
</p>
<p>
<s>
literatuře	literatura	k1gFnSc3	literatura
se	se	k3xPyFc4	se
lidová	lidový	k2eAgFnSc1d1	lidová
slovesnost	slovesnost	k1gFnSc1	slovesnost
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
jistými	jistý	k2eAgFnPc7d1	jistá
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
</s>
</p>
<p>
<s>
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ústní	ústní	k2eAgNnSc4d1	ústní
podání	podání	k1gNnSc4	podání
(	(	kIx(	(
<g/>
ústní	ústní	k2eAgFnSc1d1	ústní
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ústní	ústní	k2eAgNnSc1d1	ústní
podání	podání	k1gNnSc1	podání
<g/>
,	,	kIx,	,
orální	orální	k2eAgFnSc1d1	orální
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Lidové	lidový	k2eAgFnPc1d1	lidová
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
balady	balada	k1gFnPc1	balada
se	se	k3xPyFc4	se
zpívají	zpívat	k5eAaImIp3nP	zpívat
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
a	a	k8xC	a
</s>
</p>
<p>
<s>
pověsti	pověst	k1gFnPc1	pověst
se	se	k3xPyFc4	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
nepřímé	přímý	k2eNgFnSc2d1	nepřímá
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zprostředkované	zprostředkovaný	k2eAgInPc1d1	zprostředkovaný
<g/>
,	,	kIx,	,
podávané	podávaný	k2eAgInPc1d1	podávaný
pomocí	pomocí	k7c2	pomocí
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
<g/>
,	,	kIx,	,
na	na	k7c6	na
</s>
</p>
<p>
<s>
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
ústního	ústní	k2eAgNnSc2d1	ústní
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
posluchači	posluchač	k1gMnPc1	posluchač
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
vyprávění	vyprávění	k1gNnSc2	vyprávění
a	a	k8xC	a
</s>
</p>
<p>
<s>
vypravěč	vypravěč	k1gMnSc1	vypravěč
má	mít	k5eAaImIp3nS	mít
nad	nad	k7c7	nad
vyprávěním	vyprávění	k1gNnSc7	vyprávění
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
</s>
</p>
<p>
<s>
folkloru	folklor	k1gInSc2	folklor
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
zde	zde	k6eAd1	zde
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
vypravěči	vypravěč	k1gMnSc6	vypravěč
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
</s>
</p>
<p>
<s>
vyprávění	vyprávění	k1gNnSc1	vyprávění
přednese	přednést	k5eAaPmIp3nS	přednést
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
celé	celý	k2eAgNnSc1d1	celé
vyznění	vyznění	k1gNnSc1	vyznění
<g/>
.	.	kIx.	.
</s>
<s>
Záleželo	záležet	k5eAaImAgNnS	záležet
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
</s>
</p>
<p>
<s>
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
vyprávěno	vyprávět	k5eAaImNgNnS	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Interpret-	Interpret-	k?	Interpret-
</s>
</p>
<p>
<s>
vypravěč	vypravěč	k1gMnSc1	vypravěč
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
folkloru	folklor	k1gInSc6	folklor
klíčový	klíčový	k2eAgInSc4d1	klíčový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Přejímá	přejímat	k5eAaImIp3nS	přejímat
vyprávění	vyprávění	k1gNnSc4	vyprávění
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
vyprávění	vyprávění	k1gNnSc4	vyprávění
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
invence	invence	k1gFnSc2	invence
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgInSc2	svůj
přednesu	přednes	k1gInSc2	přednes
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
</s>
</p>
<p>
<s>
dál	daleko	k6eAd2	daleko
folklorní	folklorní	k2eAgNnSc1d1	folklorní
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
folkloru	folklor	k1gInSc6	folklor
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
typické	typický	k2eAgNnSc1d1	typické
řetězovité	řetězovitý	k2eAgNnSc1d1	řetězovitý
šíření	šíření	k1gNnSc1	šíření
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
</s>
</p>
<p>
<s>
spojené	spojený	k2eAgFnPc4d1	spojená
obměny	obměna	k1gFnPc4	obměna
a	a	k8xC	a
úpravy	úprava	k1gFnPc4	úprava
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Folklorní	folklorní	k2eAgNnPc1d1	folklorní
díla	dílo	k1gNnPc1	dílo
původně	původně	k6eAd1	původně
přežívala	přežívat	k5eAaImAgNnP	přežívat
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
primárních	primární	k2eAgFnPc6d1	primární
sociálních	sociální	k2eAgFnPc6d1	sociální
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
lokálních	lokální	k2eAgNnPc6d1	lokální
společenstvech	společenstvo	k1gNnPc6	společenstvo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
</s>
</p>
<p>
<s>
folklorních	folklorní	k2eAgNnPc2d1	folklorní
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
projevuje	projevovat	k5eAaImIp3nS	projevovat
určitý	určitý	k2eAgInSc4d1	určitý
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
vkus	vkus	k1gInSc4	vkus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
</s>
</p>
<p>
<s>
funkce	funkce	k1gFnSc1	funkce
folklorní	folklorní	k2eAgFnSc2d1	folklorní
pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
folklorního	folklorní	k2eAgNnSc2d1	folklorní
díla	dílo	k1gNnSc2	dílo
celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
díla	dílo	k1gNnSc2	dílo
</s>
</p>
<p>
<s>
literárního	literární	k2eAgInSc2d1	literární
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
funkci	funkce	k1gFnSc4	funkce
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
folklorní	folklorní	k2eAgFnSc1d1	folklorní
pohádka	pohádka	k1gFnSc1	pohádka
</s>
</p>
<p>
<s>
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
funkci	funkce	k1gFnSc4	funkce
estetickou	estetický	k2eAgFnSc4d1	estetická
a	a	k8xC	a
zábavní	zábavní	k2eAgFnSc4d1	zábavní
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
funkci	funkce	k1gFnSc4	funkce
poučnou	poučný	k2eAgFnSc4d1	poučná
a	a	k8xC	a
</s>
</p>
<p>
<s>
užitečnou	užitečný	k2eAgFnSc7d1	užitečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
v	v	k7c6	v
literatuřeV	literatuřeV	k?	literatuřeV
literatuře	literatura	k1gFnSc6	literatura
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
pohádka	pohádka	k1gFnSc1	pohádka
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
dobovému	dobový	k2eAgNnSc3d1	dobové
a	a	k8xC	a
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
vřazena	vřazen	k2eAgFnSc1d1	vřazena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
podléhaly	podléhat	k5eAaImAgFnP	podléhat
pohádky	pohádka	k1gFnPc1	pohádka
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
obecnému	obecný	k2eAgInSc3d1	obecný
</s>
</p>
<p>
<s>
vkusu	vkus	k1gInSc3	vkus
<g/>
,	,	kIx,	,
vyžadujícímu	vyžadující	k2eAgNnSc3d1	vyžadující
vtipné	vtipný	k2eAgFnPc4d1	vtipná
společenské	společenský	k2eAgFnPc4d1	společenská
historky	historka	k1gFnPc4	historka
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
Dekameronu	Dekameron	k1gInSc2	Dekameron
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
</s>
</p>
<p>
<s>
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dvorská	dvorský	k2eAgFnSc1d1	dvorská
morálka	morálka	k1gFnSc1	morálka
literárně	literárně	k6eAd1	literárně
promítla	promítnout	k5eAaPmAgFnS	promítnout
do	do	k7c2	do
pohádek	pohádka	k1gFnPc2	pohádka
sesbíraných	sesbíraný	k2eAgFnPc2d1	sesbíraná
</s>
</p>
<p>
<s>
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
prostých	prostý	k2eAgMnPc2d1	prostý
vesničanů	vesničan	k1gMnPc2	vesničan
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
literárních	literární	k2eAgInPc6d1	literární
</s>
</p>
<p>
<s>
dílech	dílo	k1gNnPc6	dílo
znát	znát	k5eAaImF	znát
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
osvícenství	osvícenství	k1gNnSc3	osvícenství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Pohádkách	pohádka	k1gFnPc6	pohádka
matky	matka	k1gFnSc2	matka
Husy	husa	k1gFnSc2	husa
</s>
</p>
<p>
<s>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
Perraulta	Perraulta	k1gMnSc1	Perraulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zase	zase	k9	zase
pohádky	pohádka	k1gFnPc1	pohádka
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
realistické	realistický	k2eAgFnPc1d1	realistická
</s>
</p>
<p>
<s>
venkovské	venkovský	k2eAgFnSc3d1	venkovská
próze	próza	k1gFnSc3	próza
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
brány	brána	k1gFnPc1	brána
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
,,	,,	k?	,,
<g/>
poučení	poučení	k1gNnSc1	poučení
o	o	k7c6	o
lidu	lid	k1gInSc6	lid
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pohádky	pohádka	k1gFnPc4	pohádka
B.	B.	kA	B.
Němcové	Němcové	k2eAgFnPc4d1	Němcové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnPc1d1	literární
</s>
</p>
<p>
<s>
zpracování	zpracování	k1gNnSc4	zpracování
tedy	tedy	k8xC	tedy
pohádku	pohádka	k1gFnSc4	pohádka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
<g/>
,	,	kIx,	,
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
dobovým	dobový	k2eAgInSc7d1	dobový
</s>
</p>
<p>
<s>
tendencím	tendence	k1gFnPc3	tendence
a	a	k8xC	a
otiskuje	otiskovat	k5eAaImIp3nS	otiskovat
kulturní	kulturní	k2eAgNnSc4d1	kulturní
uvažování	uvažování	k1gNnSc4	uvažování
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
čistě	čistě	k6eAd1	čistě
folklorním	folklorní	k2eAgInPc3d1	folklorní
žánrům	žánr	k1gInPc3	žánr
je	být	k5eAaImIp3nS	být
tak	tak	k8xS	tak
pohádka	pohádka	k1gFnSc1	pohádka
obecně	obecně	k6eAd1	obecně
volněji	volně	k6eAd2	volně
definovatelná	definovatelný	k2eAgFnSc1d1	definovatelná
a	a	k8xC	a
</s>
</p>
<p>
<s>
rozmanitější	rozmanitý	k2eAgFnSc1d2	rozmanitější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
pro	pro	k7c4	pro
mládežV	mládežV	k?	mládežV
historických	historický	k2eAgInPc6d1	historický
</s>
</p>
<p>
<s>
diskusích	diskuse	k1gFnPc6	diskuse
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
probírala	probírat	k5eAaImAgFnS	probírat
vhodnost	vhodnost	k1gFnSc1	vhodnost
pohádek	pohádka	k1gFnPc2	pohádka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
</s>
</p>
<p>
<s>
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
fantazijní	fantazijní	k2eAgInPc4d1	fantazijní
a	a	k8xC	a
nereálné	reálný	k2eNgInPc4d1	nereálný
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spíše	spíše	k9	spíše
</s>
</p>
<p>
<s>
stočily	stočit	k5eAaPmAgInP	stočit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pohádky	pohádka	k1gFnSc2	pohádka
jakožto	jakožto	k8xS	jakožto
vhodného	vhodný	k2eAgInSc2d1	vhodný
výchovného	výchovný	k2eAgInSc2d1	výchovný
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
článek	článek	k1gInSc1	článek
V.	V.	kA	V.
Tilleho	Tille	k1gMnSc4	Tille
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
celou	celý	k2eAgFnSc4d1	celá
debatu	debata	k1gFnSc4	debata
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
</s>
</p>
<p>
<s>
autenticky	autenticky	k6eAd1	autenticky
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
považuje	považovat	k5eAaImIp3nS	považovat
Tille	Tille	k1gInSc4	Tille
za	za	k7c4	za
,,	,,	k?	,,
<g/>
nejnevhodnější	vhodný	k2eNgInSc4d3	nejnevhodnější
</s>
</p>
<p>
<s>
výchovný	výchovný	k2eAgInSc1d1	výchovný
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
spíš	spíš	k9	spíš
předmětem	předmět	k1gInSc7	předmět
pro	pro	k7c4	pro
patologa	patolog	k1gMnSc4	patolog
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
protože	protože	k8xS	protože
,,	,,	k?	,,
<g/>
Pohádky	pohádka	k1gFnPc1	pohádka
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
démanty	démant	k1gInPc1	démant
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uměle	uměle	k6eAd1	uměle
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
jsou	být	k5eAaImIp3nP	být
falešné	falešný	k2eAgInPc1d1	falešný
a	a	k8xC	a
pravé	pravý	k2eAgInPc1d1	pravý
teprve	teprve	k6eAd1	teprve
broušením	broušení	k1gNnSc7	broušení
nabývají	nabývat	k5eAaImIp3nP	nabývat
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
To	ten	k3xDgNnSc1	ten
</s>
</p>
<p>
<s>
celé	celý	k2eAgNnSc1d1	celé
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
prezentaci	prezentace	k1gFnSc4	prezentace
pohádek	pohádka	k1gFnPc2	pohádka
pro	pro	k7c4	pro
dětské	dětský	k2eAgNnSc4d1	dětské
publikum	publikum	k1gNnSc4	publikum
<g/>
:	:	kIx,	:
pohádka	pohádka	k1gFnSc1	pohádka
se	se	k3xPyFc4	se
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
</s>
</p>
<p>
<s>
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
normám	norma	k1gFnPc3	norma
a	a	k8xC	a
konvencím	konvence	k1gFnPc3	konvence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
modely	model	k1gInPc1	model
existence	existence	k1gFnSc2	existence
pohádky	pohádka	k1gFnSc2	pohádka
</s>
</p>
<p>
<s>
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
určitou	určitý	k2eAgFnSc4d1	určitá
stabilizaci	stabilizace	k1gFnSc4	stabilizace
žánrové	žánrový	k2eAgFnSc2d1	žánrová
normy	norma	k1gFnSc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
pohádky	pohádka	k1gFnSc2	pohádka
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
hotový	hotový	k2eAgInSc1d1	hotový
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zřetelně	zřetelně	k6eAd1	zřetelně
přimyká	přimykat	k5eAaImIp3nS	přimykat
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
</s>
</p>
<p>
<s>
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
trochu	trochu	k6eAd1	trochu
odlišným	odlišný	k2eAgInSc7d1	odlišný
modelem	model	k1gInSc7	model
je	být	k5eAaImIp3nS	být
pohádka	pohádka	k1gFnSc1	pohádka
v	v	k7c6	v
básnické	básnický	k2eAgFnSc6d1	básnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
především	především	k6eAd1	především
interpretace	interpretace	k1gFnSc1	interpretace
čtenáře	čtenář	k1gMnSc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
</s>
</p>
<p>
<s>
pohádky	pohádka	k1gFnSc2	pohádka
najít	najít	k5eAaPmF	najít
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
básnickému	básnický	k2eAgNnSc3d1	básnické
sebevyjádření	sebevyjádření	k1gNnSc3	sebevyjádření
<g/>
,	,	kIx,	,
autentické	autentický	k2eAgFnSc3d1	autentická
</s>
</p>
<p>
<s>
emocionální	emocionální	k2eAgNnSc1d1	emocionální
<g/>
,	,	kIx,	,
filosofické	filosofický	k2eAgFnSc2d1	filosofická
a	a	k8xC	a
mravní	mravní	k2eAgFnSc2d1	mravní
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
pohádky	pohádka	k1gFnSc2	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
</s>
</p>
<p>
<s>
pohádky	pohádka	k1gFnPc4	pohádka
podléhá	podléhat	k5eAaImIp3nS	podléhat
srovnávacímu	srovnávací	k2eAgNnSc3d1	srovnávací
bádání	bádání	k1gNnSc3	bádání
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
pohádek	pohádka	k1gFnPc2	pohádka
se	s	k7c7	s
</s>
</p>
<p>
<s>
zabývá	zabývat	k5eAaImIp3nS	zabývat
např.	např.	kA	např.
společnost	společnost	k1gFnSc1	společnost
Folklore	folklor	k1gInSc5	folklor
Fellow	Fellow	k1gMnSc1	Fellow
Communications	Communications	k1gInSc1	Communications
(	(	kIx(	(
<g/>
FFC	FFC	kA	FFC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
</s>
</p>
<p>
<s>
prostředí	prostředí	k1gNnSc1	prostředí
sahá	sahat	k5eAaImIp3nS	sahat
sběr	sběr	k1gInSc4	sběr
pohádek	pohádka	k1gFnPc2	pohádka
až	až	k6eAd1	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
ke	k	k7c3	k
sbírkám	sbírka	k1gFnPc3	sbírka
kazatelských	kazatelský	k2eAgFnPc2d1	kazatelská
exempel	exempela	k1gFnPc2	exempela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
sbírky	sbírka	k1gFnPc4	sbírka
lidového	lidový	k2eAgNnSc2d1	lidové
podání	podání	k1gNnSc2	podání
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
především	především	k6eAd1	především
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spolu	spolu	k6eAd1	spolu
</s>
</p>
<p>
<s>
s	s	k7c7	s
romantickým	romantický	k2eAgNnSc7d1	romantické
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgNnSc1d1	lidové
vyprávění	vyprávění	k1gNnSc1	vyprávění
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
relikt	relikt	k1gInSc4	relikt
původní	původní	k2eAgFnSc2d1	původní
</s>
</p>
<p>
<s>
kulturní	kulturní	k2eAgFnSc1d1	kulturní
tradice	tradice	k1gFnSc1	tradice
příslušného	příslušný	k2eAgInSc2d1	příslušný
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
i	i	k9	i
pro	pro	k7c4	pro
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
které	který	k3yQgNnSc1	který
velmi	velmi	k6eAd1	velmi
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
význam	význam	k1gInSc4	význam
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
pohádkovou	pohádkový	k2eAgFnSc7d1	pohádková
sbírkou	sbírka	k1gFnSc7	sbírka
na	na	k7c6	na
území	území	k1gNnSc6	území
čech	čecha	k1gFnPc2	čecha
byla	být	k5eAaImAgFnS	být
</s>
</p>
<p>
<s>
sbírka	sbírka	k1gFnSc1	sbírka
W.A.	W.A.	k1gMnSc2	W.A.
<g/>
Gerleho	Gerle	k1gMnSc2	Gerle
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
<g/>
.	.	kIx.	.
</s>
<s>
Pohádkové	pohádkový	k2eAgFnPc4d1	pohádková
edice	edice	k1gFnPc4	edice
včetně	včetně	k7c2	včetně
Gerleho	Gerle	k1gMnSc2	Gerle
shrnul	shrnout	k5eAaPmAgMnS	shrnout
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Tille	Tille	k6eAd1	Tille
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
České	český	k2eAgFnSc2d1	Česká
pohádky	pohádka	k1gFnSc2	pohádka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
existovalo	existovat	k5eAaImAgNnS	existovat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
</s>
</p>
<p>
<s>
množství	množství	k1gNnSc4	množství
sběratelů	sběratel	k1gMnPc2	sběratel
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
převáděli	převádět	k5eAaImAgMnP	převádět
do	do	k7c2	do
literární	literární	k2eAgFnSc2d1	literární
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
</s>
</p>
<p>
<s>
byla	být	k5eAaImAgFnS	být
pohádka	pohádka	k1gFnSc1	pohádka
upravena	upravit	k5eAaPmNgFnS	upravit
<g/>
,	,	kIx,	,
editována	editovat	k5eAaImNgFnS	editovat
a	a	k8xC	a
podléhala	podléhat	k5eAaImAgFnS	podléhat
určitému	určitý	k2eAgInSc3d1	určitý
literárnímu	literární	k2eAgInSc3d1	literární
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
</s>
</p>
<p>
<s>
byl	být	k5eAaImAgInS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
i	i	k9	i
pro	pro	k7c4	pro
K.	K.	kA	K.
J.	J.	kA	J.
Erbena	Erben	k1gMnSc4	Erben
a	a	k8xC	a
B.	B.	kA	B.
Němcovou	Němcová	k1gFnSc7	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Erben	Erben	k1gMnSc1	Erben
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
překládal	překládat	k5eAaImAgMnS	překládat
i	i	k8xC	i
</s>
</p>
<p>
<s>
pohádky	pohádka	k1gFnPc4	pohádka
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
vnímány	vnímat	k5eAaImNgInP	vnímat
jako	jako	k8xC	jako
</s>
</p>
<p>
<s>
součást	součást	k1gFnSc1	součást
české	český	k2eAgFnSc2d1	Česká
folklorní	folklorní	k2eAgFnSc2d1	folklorní
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
převažující	převažující	k2eAgInSc1d1	převažující
počet	počet	k1gInSc1	počet
</s>
</p>
<p>
<s>
pohádek	pohádka	k1gFnPc2	pohádka
nad	nad	k7c7	nad
pověstmi	pověst	k1gFnPc7	pověst
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
zúženým	zúžený	k2eAgInSc7d1	zúžený
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
pohádek	pohádka	k1gFnPc2	pohádka
</s>
</p>
<p>
<s>
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.1	.1	k4	.1
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
pohádek	pohádka	k1gFnPc2	pohádka
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
i	i	k8xC	i
vznik	vznik	k1gInSc1	vznik
pohádek	pohádka	k1gFnPc2	pohádka
studuje	studovat	k5eAaImIp3nS	studovat
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
folkloristika	folkloristika	k1gFnSc1	folkloristika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanovila	stanovit	k5eAaPmAgFnS	stanovit
čtyři	čtyři	k4xCgFnPc4	čtyři
možné	možný	k2eAgFnPc4d1	možná
teorie	teorie	k1gFnPc4	teorie
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mytologickou	mytologický	k2eAgFnSc4d1	mytologická
</s>
</p>
<p>
<s>
antropologickou	antropologický	k2eAgFnSc4d1	antropologická
</s>
</p>
<p>
<s>
migrační	migrační	k2eAgFnSc1d1	migrační
</s>
</p>
<p>
<s>
historicko-geografickou	historickoeografický	k2eAgFnSc4d1	historicko-geografická
</s>
</p>
<p>
<s>
==	==	k?	==
Autoři	autor	k1gMnPc1	autor
a	a	k8xC	a
sběratelé	sběratel	k1gMnPc1	sběratel
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
pohádkové	pohádkový	k2eAgInPc1d1	pohádkový
soubory	soubor	k1gInPc1	soubor
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ARNE	Arne	k1gMnSc1	Arne
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
;	;	kIx,	;
THOMPSON	THOMPSON	kA	THOMPSON
<g/>
,	,	kIx,	,
S.	S.	kA	S.
The	The	k1gFnPc1	The
Types	Types	k1gMnSc1	Types
of	of	k?	of
The	The	k1gMnSc1	The
Folk-Tal	Folk-Tal	k1gMnSc1	Folk-Tal
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ŠMAHELOVÁ	ŠMAHELOVÁ	kA	ŠMAHELOVÁ
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Prolamování	prolamování	k1gNnSc1	prolamování
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOL	kol	k6eAd1	kol
<g/>
.	.	kIx.	.
</s>
<s>
AUTORŮ	autor	k1gMnPc2	autor
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Národopisná	národopisný	k2eAgFnSc1d1	národopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
věcná	věcný	k2eAgFnSc1d1	věcná
část	část	k1gFnSc1	část
O-	O-	k1gFnSc2	O-
Ž	Ž	kA	Ž
<g/>
;	;	kIx,	;
Etnologický	etnologický	k2eAgInSc1d1	etnologický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
v.v.i.	v.v.i.	k?	v.v.i.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Ústav	ústav	k1gInSc1	ústav
evropské	evropský	k2eAgFnSc2d1	Evropská
etnologie	etnologie	k1gFnSc2	etnologie
FF	ff	kA	ff
MU	MU	kA	MU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
;	;	kIx,	;
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
SIROVÁTKA	SIROVÁTKA	kA	SIROVÁTKA
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Česká	český	k2eAgFnSc1d1	Česká
pohádka	pohádka	k1gFnSc1	pohádka
a	a	k8xC	a
pověst	pověst	k1gFnSc1	pověst
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
a	a	k8xC	a
dětské	dětský	k2eAgFnSc3d1	dětská
literatuře	literatura	k1gFnSc3	literatura
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
etnografii	etnografie	k1gFnSc4	etnografie
a	a	k8xC	a
folkloristiku	folkloristika	k1gFnSc4	folkloristika
AV	AV	kA	AV
ČR	ČR	kA	ČR
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Moravskotřebovská	moravskotřebovský	k2eAgFnSc1d1	moravskotřebovská
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Polívka	Polívka	k1gMnSc1	Polívka
–	–	k?	–
O	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
pohádkách	pohádka	k1gFnPc6	pohádka
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Tille	Tille	k1gFnSc2	Tille
–	–	k?	–
Soupis	soupis	k1gInSc1	soupis
českých	český	k2eAgFnPc2d1	Česká
pohádek	pohádka	k1gFnPc2	pohádka
</s>
</p>
<p>
<s>
LUŽÍK	LUŽÍK	kA	LUŽÍK
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
a	a	k8xC	a
dětská	dětský	k2eAgFnSc1d1	dětská
duše	duše	k1gFnSc1	duše
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
32	[number]	k4	32
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Báje	báje	k1gFnSc1	báje
</s>
</p>
<p>
<s>
Bajka	bajka	k1gFnSc1	bajka
</s>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
žánr	žánr	k1gInSc1	žánr
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pověst	pověst	k1gFnSc1	pověst
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pohádka	pohádka	k1gFnSc1	pohádka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Báchorky	báchorka	k1gFnSc2	báchorka
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pohádka	pohádka	k1gFnSc1	pohádka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
domova	domov	k1gInSc2	domov
a	a	k8xC	a
Myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Novina	novina	k1gFnSc1	novina
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
–	–	k?	–
kapitola	kapitola	k1gFnSc1	kapitola
Národní	národní	k2eAgFnSc2d1	národní
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
253	[number]	k4	253
<g/>
–	–	k?	–
<g/>
260	[number]	k4	260
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
