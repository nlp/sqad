<s>
Glykosidy	glykosid	k1gInPc1	glykosid
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc7d1	vznikající
náhradou	náhrada	k1gFnSc7	náhrada
hydroxylové	hydroxylový	k2eAgFnSc2d1	hydroxylová
poloacetalové	poloacetal	k1gMnPc1	poloacetal
(	(	kIx(	(
<g/>
hemiacetalové	hemiacetal	k1gMnPc1	hemiacetal
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
poloketalové	poloketal	k1gMnPc1	poloketal
(	(	kIx(	(
<g/>
hemiketalové	hemiketal	k1gMnPc1	hemiketal
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
buď	buď	k8xC	buď
jiným	jiný	k2eAgInSc7d1	jiný
cukerným	cukerný	k2eAgInSc7d1	cukerný
nebo	nebo	k8xC	nebo
necukerným	cukerný	k2eNgInSc7d1	cukerný
radikálem	radikál	k1gInSc7	radikál
(	(	kIx(	(
<g/>
zbytkem	zbytek	k1gInSc7	zbytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
z	z	k7c2	z
monosacharidu	monosacharid	k1gInSc2	monosacharid
vzniká	vznikat	k5eAaImIp3nS	vznikat
oligosacharid	oligosacharid	k1gInSc1	oligosacharid
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
disacharid	disacharid	k1gInSc1	disacharid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
disacharidu	disacharid	k1gInSc2	disacharid
trisacharid	trisacharida	k1gFnPc2	trisacharida
atd.	atd.	kA	atd.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
sloučenin	sloučenina	k1gFnPc2	sloučenina
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
homoglykosidy	homoglykosid	k1gInPc4	homoglykosid
(	(	kIx(	(
<g/>
přednost	přednost	k1gFnSc1	přednost
se	se	k3xPyFc4	se
však	však	k9	však
dává	dávat	k5eAaImIp3nS	dávat
pojmu	pojem	k1gInSc2	pojem
oligosacharidy	oligosacharid	k1gInPc1	oligosacharid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
sacharid	sacharid	k1gInSc1	sacharid
spojí	spojit	k5eAaPmIp3nS	spojit
s	s	k7c7	s
necukerným	cukerný	k2eNgInSc7d1	cukerný
radikálem	radikál	k1gInSc7	radikál
(	(	kIx(	(
<g/>
zbytkem	zbytek	k1gInSc7	zbytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
aglykonem	aglykon	k1gInSc7	aglykon
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
glykosidy	glykosid	k1gInPc1	glykosid
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
někdy	někdy	k6eAd1	někdy
heteroglykosidy	heteroglykosida	k1gFnPc4	heteroglykosida
<g/>
.	.	kIx.	.
</s>
<s>
Glykosidy	glykosid	k1gInPc1	glykosid
vznikají	vznikat	k5eAaImIp3nP	vznikat
zásadně	zásadně	k6eAd1	zásadně
od	od	k7c2	od
cyklických	cyklický	k2eAgFnPc2d1	cyklická
forem	forma	k1gFnPc2	forma
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgInPc2d1	tvořící
obvykle	obvykle	k6eAd1	obvykle
buď	buď	k8xC	buď
šestičlennou	šestičlenný	k2eAgFnSc4d1	šestičlenná
heterocyklickou	heterocyklický	k2eAgFnSc4d1	heterocyklická
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
pyranóza	pyranóza	k1gFnSc1	pyranóza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
furanóza	furanóza	k1gFnSc1	furanóza
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vznikající	vznikající	k2eAgInPc1d1	vznikající
glykosidy	glykosid	k1gInPc1	glykosid
nazývají	nazývat	k5eAaImIp3nP	nazývat
pyranosidy	pyranosida	k1gFnPc4	pyranosida
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
furanosidy	furanosid	k1gInPc7	furanosid
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
glykosidy	glykosid	k1gInPc1	glykosid
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pojmenovají	pojmenovat	k5eAaPmIp3nP	pojmenovat
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
příslušného	příslušný	k2eAgInSc2d1	příslušný
sacharidu	sacharid	k1gInSc2	sacharid
<g/>
;	;	kIx,	;
např.	např.	kA	např.
glykosidy	glykosid	k1gInPc4	glykosid
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
spojením	spojení	k1gNnSc7	spojení
glukózy	glukóza	k1gFnSc2	glukóza
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
radikálem	radikál	k1gInSc7	radikál
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
glukosidy	glukosida	k1gFnPc1	glukosida
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
glukopyranosidy	glukopyranosida	k1gFnSc2	glukopyranosida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
ribózy	ribóza	k1gFnSc2	ribóza
ribosidy	ribosida	k1gFnSc2	ribosida
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ribofuranosidy	ribofuranosida	k1gFnSc2	ribofuranosida
<g/>
)	)	kIx)	)
atp.	atp.	kA	atp.
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
při	při	k7c6	při
substituci	substituce	k1gFnSc6	substituce
radikálem	radikál	k1gInSc7	radikál
aglykonu	aglykon	k1gInSc2	aglykon
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
glykosid	glykosid	k1gInSc1	glykosid
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
připojen	připojit	k5eAaPmNgInS	připojit
tento	tento	k3xDgInSc4	tento
radikál	radikál	k1gInSc4	radikál
přes	přes	k7c4	přes
atom	atom	k1gInSc4	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
O-glykosidech	Olykosid	k1gMnPc6	O-glykosid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
připojen	připojit	k5eAaPmNgInS	připojit
atomem	atom	k1gInSc7	atom
dusíku	dusík	k1gInSc2	dusík
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
N-glykosidech	Nlykosid	k1gInPc6	N-glykosid
(	(	kIx(	(
<g/>
správněji	správně	k6eAd2	správně
glykosylaminech	glykosylamin	k1gInPc6	glykosylamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analogicky	analogicky	k6eAd1	analogicky
při	při	k7c6	při
napojení	napojení	k1gNnSc6	napojení
přes	přes	k7c4	přes
atom	atom	k1gInSc4	atom
síry	síra	k1gFnSc2	síra
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
S-glykosidy	Slykosid	k1gInPc4	S-glykosid
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
radikálem	radikál	k1gInSc7	radikál
substituovaná	substituovaný	k2eAgFnSc1d1	substituovaná
poloacetálová	poloacetálový	k2eAgFnSc1d1	poloacetálový
hydroxyskupina	hydroxyskupina	k1gFnSc1	hydroxyskupina
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
možných	možný	k2eAgFnPc2d1	možná
prostorových	prostorový	k2eAgFnPc2d1	prostorová
orientací	orientace	k1gFnPc2	orientace
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
orientace	orientace	k1gFnSc1	orientace
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
v	v	k7c6	v
názvu	název	k1gInSc6	název
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
glykosidu	glykosid	k1gInSc2	glykosid
řeckými	řecký	k2eAgNnPc7d1	řecké
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
;	;	kIx,	;
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
tak	tak	k6eAd1	tak
α	α	k?	α
a	a	k8xC	a
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Prostorová	prostorový	k2eAgFnSc1d1	prostorová
stavba	stavba	k1gFnSc1	stavba
označovaná	označovaný	k2eAgFnSc1d1	označovaná
písmenem	písmeno	k1gNnSc7	písmeno
α	α	k?	α
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
takové	takový	k3xDgFnSc3	takový
konfiguraci	konfigurace	k1gFnSc3	konfigurace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
poloacetálová	poloacetálový	k2eAgFnSc1d1	poloacetálový
hydroxyskupina	hydroxyskupina	k1gFnSc1	hydroxyskupina
orientována	orientovat	k5eAaBmNgFnS	orientovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hydroxylová	hydroxylový	k2eAgFnSc1d1	hydroxylová
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
anomerním	anomerní	k2eAgInSc6d1	anomerní
referenčním	referenční	k2eAgInSc6d1	referenční
atomu	atom	k1gInSc6	atom
<g/>
;	;	kIx,	;
u	u	k7c2	u
pentóz	pentóza	k1gFnPc2	pentóza
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uhlíkový	uhlíkový	k2eAgInSc1d1	uhlíkový
atom	atom	k1gInSc1	atom
č.	č.	k?	č.
4	[number]	k4	4
<g/>
,	,	kIx,	,
u	u	k7c2	u
hexóz	hexóza	k1gFnPc2	hexóza
uhlíkový	uhlíkový	k2eAgInSc4d1	uhlíkový
atom	atom	k1gInSc4	atom
č.	č.	k?	č.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
velmi	velmi	k6eAd1	velmi
významných	významný	k2eAgNnPc2d1	významné
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
homoglykosidů	homoglykosid	k1gInPc2	homoglykosid
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
heterogyklosidům	heterogyklosid	k1gMnPc3	heterogyklosid
všechny	všechen	k3xTgInPc4	všechen
nukleotidy	nukleotid	k1gInPc4	nukleotid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
β	β	k?	β
(	(	kIx(	(
<g/>
složek	složka	k1gFnPc2	složka
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
β	β	k?	β
(	(	kIx(	(
<g/>
složek	složka	k1gFnPc2	složka
DNA	dno	k1gNnSc2	dno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systematické	systematický	k2eAgInPc1d1	systematický
názvy	název	k1gInPc1	název
heteroglykosidů	heteroglykosid	k1gMnPc2	heteroglykosid
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
trojím	trojí	k4xRgInSc7	trojí
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
Za	za	k7c4	za
název	název	k1gInSc4	název
cukerného	cukerný	k2eAgInSc2d1	cukerný
nebo	nebo	k8xC	nebo
necukerného	cukerný	k2eNgInSc2d1	cukerný
radikálu	radikál	k1gInSc2	radikál
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
název	název	k1gInSc1	název
výchozí	výchozí	k2eAgFnSc2d1	výchozí
cukerné	cukerný	k2eAgFnSc2d1	cukerná
složky	složka	k1gFnSc2	složka
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-osid	sida	k1gFnPc2	-osida
<g/>
;	;	kIx,	;
u	u	k7c2	u
heteroglykosidů	heteroglykosid	k1gMnPc2	heteroglykosid
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
struktura	struktura	k1gFnSc1	struktura
aglykonu	aglykon	k1gInSc2	aglykon
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
methyl-α	methyl-α	k?	methyl-α
<g/>
.	.	kIx.	.
</s>
<s>
Uvede	uvést	k5eAaPmIp3nS	uvést
se	se	k3xPyFc4	se
v	v	k7c6	v
kulaté	kulatý	k2eAgFnSc6d1	kulatá
závorce	závorka	k1gFnSc6	závorka
název	název	k1gInSc1	název
výchozí	výchozí	k2eAgFnSc2d1	výchozí
cukerné	cukerný	k2eAgFnSc2d1	cukerná
složky	složka	k1gFnSc2	složka
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
osyl	osylum	k1gNnPc2	osylum
následované	následovaný	k2eAgFnSc2d1	následovaná
názvem	název	k1gInSc7	název
heteroatomu	heteroatom	k1gInSc2	heteroatom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
cukerná	cukerný	k2eAgFnSc1d1	cukerná
složka	složka	k1gFnSc1	složka
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
aglykon	aglykon	k1gInSc4	aglykon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oxy	oxy	k?	oxy
<g/>
,	,	kIx,	,
azo	azo	k?	azo
atp.	atp.	kA	atp.
<g/>
;	;	kIx,	;
před	před	k7c7	před
závorkou	závorka	k1gFnSc7	závorka
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
případný	případný	k2eAgInSc1d1	případný
lokant	lokant	k1gInSc1	lokant
<g/>
,	,	kIx,	,
označující	označující	k2eAgNnSc1d1	označující
místo	místo	k1gNnSc1	místo
substituce	substituce	k1gFnSc1	substituce
v	v	k7c6	v
necukerné	cukerný	k2eNgFnSc6d1	cukerný
složce	složka	k1gFnSc6	složka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
za	za	k7c7	za
závorkou	závorka	k1gFnSc7	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
4	[number]	k4	4
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
acetofenon	acetofenon	k1gMnSc1	acetofenon
<g/>
.	.	kIx.	.
</s>
<s>
Uvede	uvést	k5eAaPmIp3nS	uvést
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
cukerná	cukerný	k2eAgFnSc1d1	cukerná
složka	složka	k1gFnSc1	složka
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
aglykon	aglykon	k1gInSc4	aglykon
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
doplněný	doplněný	k2eAgMnSc1d1	doplněný
lokantem	lokant	k1gMnSc7	lokant
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
indexu	index	k1gInSc6	index
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgInSc7	jenž
následuje	následovat	k5eAaImIp3nS	následovat
v	v	k7c6	v
kulaté	kulatý	k2eAgFnSc6d1	kulatá
závorce	závorka	k1gFnSc6	závorka
název	název	k1gInSc1	název
výchozí	výchozí	k2eAgFnSc2d1	výchozí
cukerné	cukerný	k2eAgFnSc2d1	cukerná
složky	složka	k1gFnSc2	složka
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
osyl	osyla	k1gFnPc2	osyla
<g/>
;	;	kIx,	;
za	za	k7c7	za
závorkou	závorka	k1gFnSc7	závorka
následuje	následovat	k5eAaImIp3nS	následovat
název	název	k1gInSc1	název
necukerné	cukerný	k2eNgFnSc2d1	cukerný
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
O	o	k7c6	o
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
L-serin	Lerin	k1gInSc1	L-serin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
druhého	druhý	k4xOgMnSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
způsobu	způsob	k1gInSc2	způsob
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nejasnostem	nejasnost	k1gFnPc3	nejasnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
kulaté	kulatý	k2eAgFnPc1d1	kulatá
závorky	závorka	k1gFnPc1	závorka
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Glykosidy	glykosid	k1gInPc1	glykosid
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nP	štěpit
(	(	kIx(	(
<g/>
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
zředěných	zředěný	k2eAgFnPc2d1	zředěná
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
nebo	nebo	k8xC	nebo
specifických	specifický	k2eAgInPc2d1	specifický
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
glykosidas	glykosidas	k1gInSc1	glykosidas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
štěpené	štěpený	k2eAgFnSc2d1	štěpená
glykosidové	glykosidový	k2eAgFnSc2d1	glykosidový
vazby	vazba	k1gFnSc2	vazba
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
α	α	k?	α
a	a	k8xC	a
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
Thioglykosidy	Thioglykosida	k1gFnSc2	Thioglykosida
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
enzymem	enzym	k1gInSc7	enzym
myrosináza	myrosináza	k1gFnSc1	myrosináza
(	(	kIx(	(
<g/>
thioglukosidáza	thioglukosidáza	k1gFnSc1	thioglukosidáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
a	a	k8xC	a
glykosidy	glykosid	k1gInPc1	glykosid
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
uložené	uložený	k2eAgNnSc4d1	uložené
odděleně	odděleně	k6eAd1	odděleně
a	a	k8xC	a
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
až	až	k6eAd1	až
v	v	k7c6	v
případě	případ	k1gInSc6	případ
porušení	porušení	k1gNnSc2	porušení
pletiva	pletivo	k1gNnSc2	pletivo
nebo	nebo	k8xC	nebo
následkem	následkem	k7c2	následkem
odumírání	odumírání	k1gNnSc2	odumírání
pletiva	pletivo	k1gNnSc2	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Hydrolytický	hydrolytický	k2eAgInSc1d1	hydrolytický
význam	význam	k1gInSc1	význam
glykosidů	glykosid	k1gInPc2	glykosid
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gFnSc4	jejich
farmakologickou	farmakologický	k2eAgFnSc4d1	farmakologická
účinnost	účinnost	k1gFnSc4	účinnost
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
účinné	účinný	k2eAgInPc4d1	účinný
pouze	pouze	k6eAd1	pouze
nerozštěpené	rozštěpený	k2eNgInPc4d1	rozštěpený
glykosidy	glykosid	k1gInPc4	glykosid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kardioaktivních	kardioaktivní	k2eAgInPc2d1	kardioaktivní
glykosidů	glykosid	k1gInPc2	glykosid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
vznikají	vznikat	k5eAaImIp3nP	vznikat
účinné	účinný	k2eAgFnPc1d1	účinná
látky	látka	k1gFnPc1	látka
až	až	k9	až
po	po	k7c6	po
rozštěpení	rozštěpení	k1gNnSc6	rozštěpení
glykosidu	glykosid	k1gInSc2	glykosid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
glukosinolátů	glukosinolát	k1gInPc2	glukosinolát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
glykosidů	glykosid	k1gInPc2	glykosid
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
značně	značně	k6eAd1	značně
kolísá	kolísat	k5eAaImIp3nS	kolísat
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
metabolismu	metabolismus	k1gInSc6	metabolismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
převažují	převažovat	k5eAaImIp3nP	převažovat
pochody	pochod	k1gInPc7	pochod
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
glykosidy	glykosid	k1gInPc1	glykosid
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
rozkládány	rozkládán	k2eAgFnPc1d1	rozkládána
<g/>
.	.	kIx.	.
</s>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
glykosidů	glykosid	k1gInPc2	glykosid
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jednak	jednak	k8xC	jednak
syntézu	syntéza	k1gFnSc4	syntéza
aglykonu	aglykon	k1gInSc2	aglykon
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
aglykonů	aglykon	k1gInPc2	aglykon
též	též	k9	též
dosti	dosti	k6eAd1	dosti
rozrůzněná	rozrůzněný	k2eAgFnSc1d1	rozrůzněná
<g/>
,	,	kIx,	,
a	a	k8xC	a
glykosylaci	glykosylace	k1gFnSc4	glykosylace
aglykonu	aglykon	k1gInSc2	aglykon
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
enzymy	enzym	k1gInPc4	enzym
glykosyltransferasy	glykosyltransferasa	k1gFnSc2	glykosyltransferasa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
glykosylových	glykosylový	k2eAgInPc2d1	glykosylový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Dárcem	dárce	k1gMnSc7	dárce
glykosylu	glykosyl	k1gInSc2	glykosyl
je	být	k5eAaImIp3nS	být
aldóza-	aldóza-	k?	aldóza-
<g/>
1	[number]	k4	1
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
glykosidů	glykosid	k1gInPc2	glykosid
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
rostlinu	rostlina	k1gFnSc4	rostlina
zejména	zejména	k9	zejména
význam	význam	k1gInSc4	význam
detoxikační	detoxikační	k2eAgInSc4d1	detoxikační
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lipofilní	lipofilní	k2eAgFnPc1d1	lipofilní
toxické	toxický	k2eAgFnPc1d1	toxická
látky	látka	k1gFnPc1	látka
se	se	k3xPyFc4	se
po	po	k7c6	po
navázání	navázání	k1gNnSc6	navázání
cukru	cukr	k1gInSc2	cukr
stávají	stávat	k5eAaImIp3nP	stávat
rozpustnými	rozpustný	k2eAgFnPc7d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
do	do	k7c2	do
buněčné	buněčný	k2eAgFnSc2d1	buněčná
šťávy	šťáva	k1gFnSc2	šťáva
vakuol	vakuola	k1gFnPc2	vakuola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Glykosidy	glykosid	k1gInPc1	glykosid
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hojně	hojně	k6eAd1	hojně
v	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
syntetizovány	syntetizován	k2eAgMnPc4d1	syntetizován
i	i	k8xC	i
živočichy	živočich	k1gMnPc4	živočich
a	a	k8xC	a
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
detoxikaci	detoxikace	k1gFnSc6	detoxikace
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
toxiny	toxin	k1gInPc1	toxin
<g/>
,	,	kIx,	,
odstrašující	odstrašující	k2eAgMnPc4d1	odstrašující
případné	případný	k2eAgMnPc4d1	případný
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
cykasin	cykasin	k1gInSc1	cykasin
v	v	k7c6	v
tropických	tropický	k2eAgInPc6d1	tropický
cykasech	cykas	k1gInPc6	cykas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
farmakologii	farmakologie	k1gFnSc6	farmakologie
jsou	být	k5eAaImIp3nP	být
hojně	hojně	k6eAd1	hojně
užívány	užíván	k2eAgInPc1d1	užíván
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gInSc1	jejich
účinek	účinek	k1gInSc1	účinek
závisí	záviset	k5eAaImIp3nS	záviset
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
aglykonu	aglykon	k1gInSc6	aglykon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navázaný	navázaný	k2eAgInSc1d1	navázaný
sacharid	sacharid	k1gInSc1	sacharid
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gFnPc4	jeho
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podporovat	podporovat	k5eAaImF	podporovat
nebo	nebo	k8xC	nebo
potlačovat	potlačovat	k5eAaImF	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Glykosidy	glykosid	k1gInPc1	glykosid
tvoří	tvořit	k5eAaImIp3nP	tvořit
pestrou	pestrý	k2eAgFnSc4d1	pestrá
skupinu	skupina	k1gFnSc4	skupina
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
vyjmenuji	vyjmenovat	k5eAaPmIp1nS	vyjmenovat
některé	některý	k3yIgFnSc2	některý
skupiny	skupina	k1gFnSc2	skupina
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
roztříděné	roztříděný	k2eAgFnPc4d1	roztříděná
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
aglykonu	aglykon	k1gInSc2	aglykon
<g/>
:	:	kIx,	:
fenolové	fenolový	k2eAgInPc4d1	fenolový
glykosidy	glykosid	k1gInPc4	glykosid
glykosidy	glykosid	k1gInPc4	glykosid
kumarinů	kumarin	k1gInPc2	kumarin
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
derivátů	derivát	k1gInPc2	derivát
flavonoidové	flavonoidový	k2eAgInPc4d1	flavonoidový
glykosidy	glykosid	k1gInPc4	glykosid
antokyanidinové	antokyanidinový	k2eAgInPc4d1	antokyanidinový
glykosidy	glykosid	k1gInPc4	glykosid
antrachinonové	antrachinonový	k2eAgInPc4d1	antrachinonový
glykosidy	glykosid	k1gInPc4	glykosid
kardioaktivní	kardioaktivní	k2eAgInPc4d1	kardioaktivní
glykosidy	glykosid	k1gInPc4	glykosid
saponiny	saponin	k1gInPc4	saponin
kyanogenní	kyanogenní	k2eAgFnPc4d1	kyanogenní
glykosidy	glykosid	k1gInPc4	glykosid
glukosinoláty	glukosinolát	k1gInPc7	glukosinolát
(	(	kIx(	(
<g/>
thioglykosidy	thioglykosid	k1gInPc7	thioglykosid
<g/>
)	)	kIx)	)
glykosidy	glykosid	k1gInPc7	glykosid
s	s	k7c7	s
iridoidovým	iridoidový	k2eAgInSc7d1	iridoidový
aglykonem	aglykon	k1gInSc7	aglykon
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
glykosidy	glykosid	k1gInPc7	glykosid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Poznámky	poznámka	k1gFnSc2	poznámka
k	k	k7c3	k
některým	některý	k3yIgNnPc3	některý
důležitějším	důležitý	k2eAgNnPc3d2	důležitější
pravidlům	pravidlo	k1gNnPc3	pravidlo
nového	nový	k2eAgInSc2d1	nový
návrhu	návrh	k1gInSc2	návrh
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
sacharidů	sacharid	k1gInPc2	sacharid
/	/	kIx~	/
M.	M.	kA	M.
Černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
-	-	kIx~	-
Chemické	chemický	k2eAgFnSc2d1	chemická
listy	lista	k1gFnSc2	lista
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
č.	č.	k?	č.
8	[number]	k4	8
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
215-217	[number]	k4	215-217
</s>
