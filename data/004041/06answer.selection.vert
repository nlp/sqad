<s>
Glykosidy	glykosid	k1gInPc1	glykosid
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc7d1	vznikající
náhradou	náhrada	k1gFnSc7	náhrada
hydroxylové	hydroxylový	k2eAgFnSc2d1	hydroxylová
poloacetalové	poloacetal	k1gMnPc1	poloacetal
(	(	kIx(	(
<g/>
hemiacetalové	hemiacetal	k1gMnPc1	hemiacetal
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
poloketalové	poloketal	k1gMnPc1	poloketal
(	(	kIx(	(
<g/>
hemiketalové	hemiketal	k1gMnPc1	hemiketal
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
buď	buď	k8xC	buď
jiným	jiný	k2eAgInSc7d1	jiný
cukerným	cukerný	k2eAgInSc7d1	cukerný
nebo	nebo	k8xC	nebo
necukerným	cukerný	k2eNgInSc7d1	cukerný
radikálem	radikál	k1gInSc7	radikál
(	(	kIx(	(
<g/>
zbytkem	zbytek	k1gInSc7	zbytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
