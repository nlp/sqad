<s>
SSH	SSH	kA	SSH
(	(	kIx(	(
<g/>
Secure	Secur	k1gMnSc5	Secur
Shell	Shell	k1gInSc1	Shell
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
program	program	k1gInSc4	program
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pro	pro	k7c4	pro
zabezpečený	zabezpečený	k2eAgInSc4d1	zabezpečený
komunikační	komunikační	k2eAgInSc4d1	komunikační
protokol	protokol	k1gInSc4	protokol
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
telnet	telnet	k1gInSc4	telnet
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nezabezpečené	zabezpečený	k2eNgInPc4d1	nezabezpečený
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
shelly	shell	k1gInPc4	shell
(	(	kIx(	(
<g/>
rlogin	rlogin	k1gMnSc1	rlogin
<g/>
,	,	kIx,	,
rsh	rsh	k?	rsh
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
posílají	posílat	k5eAaImIp3nP	posílat
heslo	heslo	k1gNnSc4	heslo
v	v	k7c6	v
nezabezpečené	zabezpečený	k2eNgFnSc6d1	nezabezpečená
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gNnSc4	jeho
odposlechnutí	odposlechnutí	k1gNnSc4	odposlechnutí
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
pomocí	pomocí	k7c2	pomocí
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Šifrování	šifrování	k1gNnSc1	šifrování
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
SSH	SSH	kA	SSH
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
dat	datum	k1gNnPc2	datum
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
přes	přes	k7c4	přes
nedůvěryhodnou	důvěryhodný	k2eNgFnSc4d1	nedůvěryhodná
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
počítači	počítač	k1gInPc7	počítač
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
příkazovému	příkazový	k2eAgInSc3d1	příkazový
řádku	řádek	k1gInSc3	řádek
<g/>
,	,	kIx,	,
kopírování	kopírování	k1gNnSc1	kopírování
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
též	též	k9	též
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
obecný	obecný	k2eAgInSc1d1	obecný
přenos	přenos	k1gInSc1	přenos
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
síťového	síťový	k2eAgNnSc2d1	síťové
tunelování	tunelování	k1gNnSc2	tunelování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
autentizaci	autentizace	k1gFnSc4	autentizace
obou	dva	k4xCgMnPc2	dva
účastníků	účastník	k1gMnPc2	účastník
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
transparentní	transparentní	k2eAgNnSc1d1	transparentní
šifrování	šifrování	k1gNnSc1	šifrování
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc4	zajištění
jejich	jejich	k3xOp3gFnSc2	jejich
integrity	integrita	k1gFnSc2	integrita
a	a	k8xC	a
volitelnou	volitelný	k2eAgFnSc4d1	volitelná
bezeztrátovou	bezeztrátový	k2eAgFnSc4d1	bezeztrátová
kompresi	komprese	k1gFnSc4	komprese
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
standardně	standardně	k6eAd1	standardně
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
na	na	k7c4	na
portu	porta	k1gFnSc4	porta
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Secure	Secur	k1gMnSc5	Secur
Shell	Shell	k1gInSc1	Shell
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejde	jít	k5eNaImIp3nS	jít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
o	o	k7c4	o
náhradu	náhrada	k1gFnSc4	náhrada
shellu	shell	k1gInSc2	shell
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
interpret	interpret	k1gMnSc1	interpret
příkazů	příkaz	k1gInPc2	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
existujícího	existující	k2eAgInSc2d1	existující
programu	program	k1gInSc2	program
rsh	rsh	k?	rsh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zabezpečený	zabezpečený	k2eAgMnSc1d1	zabezpečený
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoku	útok	k1gInSc6	útok
odchytáváním	odchytávání	k1gNnSc7	odchytávání
hesel	heslo	k1gNnPc2	heslo
na	na	k7c4	na
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
síť	síť	k1gFnSc4	síť
helsinské	helsinský	k2eAgFnSc2d1	Helsinská
Technické	technický	k2eAgFnSc2d1	technická
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Helsinki	Helsinki	k1gNnSc1	Helsinki
University	universita	k1gFnSc2	universita
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Tatu	tata	k1gMnSc4	tata
Ylönen	Ylönen	k1gInSc4	Ylönen
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
SSH-	SSH-	k1gFnSc1	SSH-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1995	[number]	k4	1995
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
včetně	včetně	k7c2	včetně
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
jako	jako	k8xS	jako
freeware	freeware	k1gInSc1	freeware
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
základna	základna	k1gFnSc1	základna
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
v	v	k7c6	v
padesáti	padesát	k4xCc2	padesát
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1995	[number]	k4	1995
Ylönen	Ylönen	k1gInSc1	Ylönen
založil	založit	k5eAaPmAgInS	založit
společnost	společnost	k1gFnSc4	společnost
SSH	SSH	kA	SSH
Communications	Communications	k1gInSc4	Communications
Security	Securita	k1gFnSc2	Securita
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
SSH	SSH	kA	SSH
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
SSH	SSH	kA	SSH
používala	používat	k5eAaImAgFnS	používat
části	část	k1gFnSc3	část
svobodného	svobodný	k2eAgNnSc2d1	svobodné
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
např	např	kA	např
GNU	gnu	k1gMnSc1	gnu
libgmp	libgmp	k1gMnSc1	libgmp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pozdější	pozdní	k2eAgFnSc1d2	pozdější
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
závislostí	závislost	k1gFnPc2	závislost
oprostily	oprostit	k5eAaPmAgFnP	oprostit
a	a	k8xC	a
změnily	změnit	k5eAaPmAgFnP	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
proprietární	proprietární	k2eAgInSc4d1	proprietární
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
vydáván	vydávat	k5eAaImNgMnS	vydávat
pod	pod	k7c7	pod
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
vylepšená	vylepšený	k2eAgFnSc1d1	vylepšená
verze	verze	k1gFnSc1	verze
protokolu	protokol	k1gInSc2	protokol
SSH-	SSH-	k1gFnSc2	SSH-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nekompatibilní	kompatibilní	k2eNgFnSc1d1	nekompatibilní
s	s	k7c7	s
SSH-	SSH-	k1gFnSc7	SSH-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Novinkami	novinka	k1gFnPc7	novinka
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
verzi	verze	k1gFnSc6	verze
byla	být	k5eAaImAgFnS	být
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
např.	např.	kA	např.
výměnou	výměna	k1gFnSc7	výměna
klíčů	klíč	k1gInPc2	klíč
pomocí	pomoc	k1gFnSc7	pomoc
Diffie-Hellman	Diffie-Hellman	k1gMnSc1	Diffie-Hellman
algoritmu	algoritmus	k1gInSc2	algoritmus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Diffie-Hellman	Diffie-Hellman	k1gMnSc1	Diffie-Hellman
key	key	k?	key
exchange	exchangat	k5eAaPmIp3nS	exchangat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přísnou	přísný	k2eAgFnSc7d1	přísná
kontrolou	kontrola	k1gFnSc7	kontrola
integrity	integrita	k1gFnSc2	integrita
dat	datum	k1gNnPc2	datum
pomocí	pomocí	k7c2	pomocí
MAC	Mac	kA	Mac
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
vlastností	vlastnost	k1gFnSc7	vlastnost
SSH-2	SSH-2	k1gFnSc2	SSH-2
je	být	k5eAaImIp3nS	být
také	také	k9	také
možnost	možnost	k1gFnSc1	možnost
řídit	řídit	k5eAaImF	řídit
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
shellů	shell	k1gInPc2	shell
pomocí	pomocí	k7c2	pomocí
jednoho	jeden	k4xCgInSc2	jeden
SSH	SSH	kA	SSH
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
používat	používat	k5eAaImF	používat
volně	volně	k6eAd1	volně
šiřitelnou	šiřitelný	k2eAgFnSc4d1	šiřitelná
verzi	verze	k1gFnSc4	verze
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vrátili	vrátit	k5eAaPmAgMnP	vrátit
ke	k	k7c3	k
starší	starý	k2eAgFnSc3d2	starší
verzi	verze	k1gFnSc3	verze
1.2	[number]	k4	1.2
<g/>
.12	.12	k4	.12
původního	původní	k2eAgInSc2d1	původní
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
open	open	k1gNnSc1	open
source	sourec	k1gInSc2	sourec
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
OSSH	OSSH	kA	OSSH
Björna	Björna	k1gFnSc1	Björna
Grönvalla	Grönvalla	k1gFnSc1	Grönvalla
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vývojáři	vývojář	k1gMnPc1	vývojář
OpenBSD	OpenBSD	k1gFnSc2	OpenBSD
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
fork	fork	k1gInSc4	fork
kódu	kód	k1gInSc2	kód
OSSH	OSSH	kA	OSSH
<g/>
,	,	kIx,	,
udělali	udělat	k5eAaPmAgMnP	udělat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k9	tak
OpenSSH	OpenSSH	k1gMnPc1	OpenSSH
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
OpenBSD	OpenBSD	k1gFnPc6	OpenBSD
verze	verze	k1gFnSc2	verze
2.6	[number]	k4	2.6
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
branch	branch	k1gInSc4	branch
OpenSSH	OpenSSH	k1gMnSc2	OpenSSH
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
portování	portování	k1gNnSc4	portování
i	i	k8xC	i
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
využívaly	využívat	k5eAaPmAgFnP	využívat
SSH	SSH	kA	SSH
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
OpenSSH	OpenSSH	k1gMnSc3	OpenSSH
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgNnPc2d3	nejpopulárnější
SSH	SSH	kA	SSH
implementací	implementace	k1gFnPc2	implementace
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
OSSH	OSSH	kA	OSSH
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
stalo	stát	k5eAaPmAgNnS	stát
zastaralým	zastaralý	k2eAgMnSc7d1	zastaralý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
protokol	protokol	k1gInSc1	protokol
SSH-2	SSH-2	k1gFnSc2	SSH-2
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k9	jako
Internetový	internetový	k2eAgInSc1d1	internetový
standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
publikovala	publikovat	k5eAaBmAgFnS	publikovat
pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
IETF	IETF	kA	IETF
"	"	kIx"	"
<g/>
secsh	secsh	k1gMnSc1	secsh
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
RFC	RFC	kA	RFC
4252	[number]	k4	4252
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k8xS	jako
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
náhrada	náhrada	k1gFnSc1	náhrada
starších	starý	k2eAgInPc2d2	starší
protokolů	protokol	k1gInPc2	protokol
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
náhrada	náhrada	k1gFnSc1	náhrada
protokolu	protokol	k1gInSc2	protokol
Telnet	Telneta	k1gFnPc2	Telneta
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
počítači	počítač	k1gInSc6	počítač
přes	přes	k7c4	přes
nezabezpečenou	zabezpečený	k2eNgFnSc4d1	nezabezpečená
síť	síť	k1gFnSc4	síť
náhrada	náhrada	k1gFnSc1	náhrada
protokolu	protokol	k1gInSc2	protokol
Rlogin	Rlogina	k1gFnPc2	Rlogina
<g/>
,	,	kIx,	,
přihlášení	přihlášení	k1gNnSc4	přihlášení
na	na	k7c4	na
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
počítač	počítač	k1gInSc4	počítač
náhrada	náhrada	k1gFnSc1	náhrada
protokolu	protokol	k1gInSc2	protokol
Rsh	Rsh	k1gFnSc1	Rsh
<g/>
,	,	kIx,	,
spouštění	spouštění	k1gNnSc1	spouštění
příkazů	příkaz	k1gInPc2	příkaz
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
počítači	počítač	k1gInSc6	počítač
tunelování	tunelování	k1gNnSc2	tunelování
spojení	spojení	k1gNnSc2	spojení
přesměrování	přesměrování	k1gNnSc2	přesměrování
TCP	TCP	kA	TCP
<g />
.	.	kIx.	.
</s>
<s>
portů	port	k1gInPc2	port
a	a	k8xC	a
X11	X11	k1gFnPc2	X11
spojení	spojení	k1gNnSc1	spojení
zabezpečeným	zabezpečený	k2eAgInSc7d1	zabezpečený
kanálem	kanál	k1gInSc7	kanál
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
přenos	přenos	k1gInSc1	přenos
souborů	soubor	k1gInPc2	soubor
pomocí	pomocí	k7c2	pomocí
SFTP	SFTP	kA	SFTP
nebo	nebo	k8xC	nebo
SCP	SCP	kA	SCP
automatické	automatický	k2eAgNnSc4d1	automatické
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
monitorování	monitorování	k1gNnSc4	monitorování
a	a	k8xC	a
management	management	k1gInSc4	management
serverů	server	k1gInPc2	server
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
připojování	připojování	k1gNnSc1	připojování
složek	složka	k1gFnPc2	složka
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
serveru	server	k1gInSc6	server
jako	jako	k9	jako
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
na	na	k7c6	na
lokálním	lokální	k2eAgInSc6d1	lokální
počítači	počítač	k1gInSc6	počítač
použitím	použití	k1gNnSc7	použití
SSHFS	SSHFS	kA	SSHFS
prohlížení	prohlížení	k1gNnSc4	prohlížení
webu	web	k1gInSc2	web
přes	přes	k7c4	přes
šifrované	šifrovaný	k2eAgInPc4d1	šifrovaný
proxy	prox	k1gInPc4	prox
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
SSH	SSH	kA	SSH
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
SOCKS	SOCKS	kA	SOCKS
protokol	protokol	k1gInSc1	protokol
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
šifrovanou	šifrovaný	k2eAgFnSc7d1	šifrovaná
VPN	VPN	kA	VPN
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
OpenSSH	OpenSSH	k1gFnSc1	OpenSSH
server	server	k1gInSc1	server
a	a	k8xC	a
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
)	)	kIx)	)
Protokol	protokol	k1gInSc1	protokol
SSH-2	SSH-2	k1gFnSc2	SSH-2
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
navrženou	navržený	k2eAgFnSc4d1	navržená
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
architekturu	architektura	k1gFnSc4	architektura
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4251	[number]	k4	4251
<g/>
)	)	kIx)	)
rozdělenou	rozdělená	k1gFnSc7	rozdělená
na	na	k7c4	na
oddělené	oddělený	k2eAgFnPc4d1	oddělená
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1	otevřená
architektura	architektura	k1gFnSc1	architektura
nabízí	nabízet	k5eAaImIp3nS	nabízet
významnou	významný	k2eAgFnSc4d1	významná
flexibilitu	flexibilita	k1gFnSc4	flexibilita
umožňující	umožňující	k2eAgNnSc1d1	umožňující
použití	použití	k1gNnSc1	použití
SSH	SSH	kA	SSH
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
zabezpečený	zabezpečený	k2eAgInSc4d1	zabezpečený
shell	shell	k1gInSc4	shell
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
transportní	transportní	k2eAgFnSc2d1	transportní
vrstvy	vrstva	k1gFnSc2	vrstva
samotné	samotný	k2eAgFnSc2d1	samotná
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
TLS	TLS	kA	TLS
(	(	kIx(	(
<g/>
Transport	transport	k1gInSc1	transport
Layer	Layra	k1gFnPc2	Layra
Security	Securita	k1gFnSc2	Securita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
autentizace	autentizace	k1gFnSc2	autentizace
uživatele	uživatel	k1gMnSc2	uživatel
je	být	k5eAaImIp3nS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
pro	pro	k7c4	pro
snadné	snadný	k2eAgNnSc4d1	snadné
rozšíření	rozšíření	k1gNnSc4	rozšíření
vlastními	vlastní	k2eAgFnPc7d1	vlastní
autentizačními	autentizační	k2eAgFnPc7d1	autentizační
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
spojení	spojení	k1gNnSc2	spojení
nabízí	nabízet	k5eAaImIp3nS	nabízet
použití	použití	k1gNnSc4	použití
více	hodně	k6eAd2	hodně
podružných	podružný	k2eAgFnPc2d1	podružná
relací	relace	k1gFnPc2	relace
přenášených	přenášený	k2eAgFnPc2d1	přenášená
jedním	jeden	k4xCgInSc7	jeden
SSH	SSH	kA	SSH
spojením	spojení	k1gNnSc7	spojení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
BEEP	BEEP	kA	BEEP
(	(	kIx(	(
<g/>
Block	Block	k1gMnSc1	Block
Extensible	Extensible	k1gMnSc1	Extensible
Exchange	Exchange	k1gFnSc4	Exchange
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
)	)	kIx)	)
a	a	k8xC	a
kteroužto	kteroužto	k?	kteroužto
vlastnost	vlastnost	k1gFnSc1	vlastnost
TLS	TLS	kA	TLS
nenabízí	nabízet	k5eNaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Transportní	transportní	k2eAgFnSc1d1	transportní
vrstva	vrstva	k1gFnSc1	vrstva
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4253	[number]	k4	4253
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
počáteční	počáteční	k2eAgFnSc4d1	počáteční
výměnu	výměna	k1gFnSc4	výměna
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
serverovou	serverový	k2eAgFnSc4d1	serverová
autentizaci	autentizace	k1gFnSc4	autentizace
<g/>
,	,	kIx,	,
kompresi	komprese	k1gFnSc4	komprese
a	a	k8xC	a
ověření	ověření	k1gNnSc4	ověření
integrity	integrita	k1gFnSc2	integrita
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vyšší	vysoký	k2eAgFnSc3d2	vyšší
vrstvě	vrstva	k1gFnSc3	vrstva
prostředí	prostředí	k1gNnSc2	prostředí
pro	pro	k7c4	pro
posílání	posílání	k1gNnSc4	posílání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc4	přijímání
nešifrovaných	šifrovaný	k2eNgFnPc2d1	nešifrovaná
až	až	k8xS	až
32,768	[number]	k4	32,768
bytů	byt	k1gInPc2	byt
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
paketů	paket	k1gInPc2	paket
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
prostý	prostý	k2eAgInSc1d1	prostý
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgInPc1d2	delší
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
povoleny	povolit	k5eAaPmNgInP	povolit
implementací	implementace	k1gFnSc7	implementace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transportní	transportní	k2eAgFnSc1d1	transportní
vrstva	vrstva	k1gFnSc1	vrstva
také	také	k9	také
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
opětovnou	opětovný	k2eAgFnSc4d1	opětovná
výměnu	výměna	k1gFnSc4	výměna
klíčů	klíč	k1gInPc2	klíč
-	-	kIx~	-
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c4	po
1	[number]	k4	1
GB	GB	kA	GB
přenesených	přenesený	k2eAgNnPc2d1	přenesené
dat	datum	k1gNnPc2	datum
nebo	nebo	k8xC	nebo
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
1	[number]	k4	1
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nastane	nastat	k5eAaPmIp3nS	nastat
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
autentizace	autentizace	k1gFnSc2	autentizace
uživatele	uživatel	k1gMnSc2	uživatel
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4252	[number]	k4	4252
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
autentizaci	autentizace	k1gFnSc4	autentizace
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
mnoha	mnoho	k4c2	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
autentizace	autentizace	k1gFnSc1	autentizace
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
SSH	SSH	kA	SSH
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
server	server	k1gInSc1	server
pouze	pouze	k6eAd1	pouze
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
autorizační	autorizační	k2eAgInPc4d1	autorizační
požadavky	požadavek	k1gInPc4	požadavek
od	od	k7c2	od
SSH	SSH	kA	SSH
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nejpoužívanějších	používaný	k2eAgFnPc2d3	nejpoužívanější
metod	metoda	k1gFnPc2	metoda
autentizace	autentizace	k1gFnSc1	autentizace
patří	patřit	k5eAaImIp3nS	patřit
metody	metoda	k1gFnPc4	metoda
<g/>
:	:	kIx,	:
password	password	k6eAd1	password
password	password	k6eAd1	password
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
pro	pro	k7c4	pro
přímou	přímý	k2eAgFnSc4d1	přímá
výměnu	výměna	k1gFnSc4	výměna
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
možnost	možnost	k1gFnSc1	možnost
změny	změna	k1gFnSc2	změna
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
není	být	k5eNaImIp3nS	být
implementována	implementován	k2eAgFnSc1d1	implementována
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
publickey	publicke	k2eAgFnPc4d1	publicke
publickey	publickea	k1gFnPc4	publickea
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
přihlášení	přihlášení	k1gNnSc2	přihlášení
pomocí	pomocí	k7c2	pomocí
veřejného	veřejný	k2eAgInSc2d1	veřejný
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
podporuje	podporovat	k5eAaImIp3nS	podporovat
alespoň	alespoň	k9	alespoň
DSA	DSA	kA	DSA
nebo	nebo	k8xC	nebo
RSA	RSA	kA	RSA
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
metody	metoda	k1gFnPc4	metoda
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
X	X	kA	X
<g/>
.509	.509	k4	.509
certifikátech	certifikát	k1gInPc6	certifikát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
útokům	útok	k1gInPc3	útok
hrubou	hrubá	k1gFnSc7	hrubá
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
brute	brute	k5eAaPmIp2nP	brute
force	force	k1gFnSc4	force
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
vypnuta	vypnut	k2eAgFnSc1d1	vypnuta
metoda	metoda	k1gFnSc1	metoda
"	"	kIx"	"
<g/>
password	password	k1gInSc1	password
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
keyboard-interactive	keyboardnteractiv	k1gInSc5	keyboard-interactiv
keyboard-interactive	keyboardnteractiv	k1gInSc5	keyboard-interactiv
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4256	[number]	k4	4256
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc3	který
server	server	k1gInSc1	server
pošle	poslat	k5eAaPmIp3nS	poslat
klientovi	klient	k1gMnSc3	klient
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
výzev	výzva	k1gFnPc2	výzva
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
uživatel	uživatel	k1gMnSc1	uživatel
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pomocí	pomocí	k7c2	pomocí
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
jednorázovou	jednorázový	k2eAgFnSc4d1	jednorázová
autentizaci	autentizace	k1gFnSc4	autentizace
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
Key	Key	k1gMnSc7	Key
nebo	nebo	k8xC	nebo
SecurID	SecurID	k1gMnSc7	SecurID
<g/>
.	.	kIx.	.
</s>
<s>
GSSAPI	GSSAPI	kA	GSSAPI
GSSAPI	GSSAPI	kA	GSSAPI
metody	metoda	k1gFnPc1	metoda
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
rozšiřitelné	rozšiřitelný	k2eAgNnSc4d1	rozšiřitelné
rozhraní	rozhraní	k1gNnSc4	rozhraní
pro	pro	k7c4	pro
autentizaci	autentizace	k1gFnSc4	autentizace
pomocí	pomocí	k7c2	pomocí
externích	externí	k2eAgInPc2d1	externí
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Kerberos	Kerberos	k1gMnSc1	Kerberos
5	[number]	k4	5
nebo	nebo	k8xC	nebo
NTLM	NTLM	kA	NTLM
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgInPc2d1	poskytující
možnost	možnost	k1gFnSc4	možnost
centrální	centrální	k2eAgFnSc2d1	centrální
autentizace	autentizace	k1gFnSc2	autentizace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Single	singl	k1gInSc5	singl
Sign-On	Sign-On	k1gMnSc1	Sign-On
<g/>
,	,	kIx,	,
SSO	SSO	kA	SSO
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přihlášení	přihlášení	k1gNnSc4	přihlášení
pomocí	pomoc	k1gFnPc2	pomoc
SSH	SSH	kA	SSH
relace	relace	k1gFnSc2	relace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
komerčních	komerční	k2eAgInPc6d1	komerční
SSH	SSH	kA	SSH
implementacích	implementace	k1gFnPc6	implementace
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
OpenSSH	OpenSSH	k1gFnSc1	OpenSSH
funkční	funkční	k2eAgFnSc2d1	funkční
GSSAPI	GSSAPI	kA	GSSAPI
také	také	k6eAd1	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
spojení	spojení	k1gNnSc2	spojení
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4254	[number]	k4	4254
<g/>
)	)	kIx)	)
definuje	definovat	k5eAaBmIp3nS	definovat
koncept	koncept	k1gInSc1	koncept
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
požadavků	požadavek	k1gInPc2	požadavek
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
globálních	globální	k2eAgInPc2d1	globální
požadavků	požadavek	k1gInPc2	požadavek
skrze	skrze	k?	skrze
které	který	k3yQgNnSc1	který
jsou	být	k5eAaImIp3nP	být
poskytovány	poskytovat	k5eAaImNgFnP	poskytovat
SSH	SSH	kA	SSH
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
SSH	SSH	kA	SSH
spojení	spojení	k1gNnSc6	spojení
může	moct	k5eAaImIp3nS	moct
hostovat	hostovat	k5eAaImF	hostovat
více	hodně	k6eAd2	hodně
kanálů	kanál	k1gInPc2	kanál
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgInSc4	každý
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
data	datum	k1gNnPc4	datum
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
kanálů	kanál	k1gInPc2	kanál
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
mimopásmových	mimopásmův	k2eAgNnPc2d1	mimopásmův
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
změna	změna	k1gFnSc1	změna
velikosti	velikost	k1gFnSc2	velikost
terminálového	terminálový	k2eAgNnSc2d1	terminálové
okna	okno	k1gNnSc2	okno
nebo	nebo	k8xC	nebo
návratový	návratový	k2eAgInSc4d1	návratový
kód	kód	k1gInSc4	kód
procesu	proces	k1gInSc2	proces
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
klient	klient	k1gMnSc1	klient
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
pomocí	pomocí	k7c2	pomocí
globálního	globální	k2eAgInSc2d1	globální
požadavku	požadavek	k1gInSc2	požadavek
vyžádat	vyžádat	k5eAaPmF	vyžádat
forwardování	forwardování	k1gNnSc4	forwardování
(	(	kIx(	(
<g/>
tunelování	tunelování	k1gNnSc4	tunelování
<g/>
)	)	kIx)	)
portu	port	k1gInSc2	port
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInPc1d1	standardní
typy	typ	k1gInPc1	typ
kanálů	kanál	k1gInPc2	kanál
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
shell	shelnout	k5eAaPmAgMnS	shelnout
shell	shell	k1gMnSc1	shell
pro	pro	k7c4	pro
terminálové	terminálový	k2eAgFnPc4d1	terminálová
shelly	shella	k1gFnPc4	shella
<g/>
,	,	kIx,	,
SFTP	SFTP	kA	SFTP
a	a	k8xC	a
požadavky	požadavek	k1gInPc1	požadavek
exec	exec	k6eAd1	exec
(	(	kIx(	(
<g/>
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
SCP	SCP	kA	SCP
přenosy	přenos	k1gInPc4	přenos
<g/>
)	)	kIx)	)
direct-tcpip	directcpip	k1gMnSc1	direct-tcpip
direct-tcpip	directcpip	k1gMnSc1	direct-tcpip
pro	pro	k7c4	pro
forwardovaná	forwardovaný	k2eAgNnPc4d1	forwardovaný
klient-server	klienterver	k1gMnSc1	klient-server
spojení	spojení	k1gNnSc2	spojení
forwarded-tcpip	forwardedcpip	k1gMnSc1	forwarded-tcpip
forwarded-tcpip	forwardedcpip	k1gMnSc1	forwarded-tcpip
pro	pro	k7c4	pro
forwardovaná	forwardovaný	k2eAgNnPc4d1	forwardovaný
server-klient	serverlient	k1gMnSc1	server-klient
spojení	spojení	k1gNnSc2	spojení
SSHFP	SSHFP	kA	SSHFP
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
DNS	DNS	kA	DNS
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
4255	[number]	k4	4255
<g/>
)	)	kIx)	)
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
veřejné	veřejný	k2eAgInPc4d1	veřejný
otisky	otisk	k1gInPc4	otisk
klíčů	klíč	k1gInPc2	klíč
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
ověření	ověření	k1gNnSc3	ověření
autenticity	autenticita	k1gFnSc2	autenticita
hosta	host	k1gMnSc2	host
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
protistrany	protistrana	k1gFnSc2	protistrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
SSH-1	SSH-1	k1gFnSc2	SSH-1
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
kvůli	kvůli	k7c3	kvůli
bezpečnostním	bezpečnostní	k2eAgInPc3d1	bezpečnostní
nedostatkům	nedostatek	k1gInPc3	nedostatek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
možnost	možnost	k1gFnSc1	možnost
útoku	útok	k1gInSc2	útok
man-in-the-middle	mannheiddle	k6eAd1	man-in-the-middle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
explicitně	explicitně	k6eAd1	explicitně
znemožněno	znemožněn	k2eAgNnSc1d1	znemožněno
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
serverů	server	k1gInPc2	server
a	a	k8xC	a
klientů	klient	k1gMnPc2	klient
používá	používat	k5eAaImIp3nS	používat
SSH-	SSH-	k1gFnSc4	SSH-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
některé	některý	k3yIgFnPc1	některý
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
software	software	k1gInSc4	software
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
SSH-2	SSH-2	k1gFnSc2	SSH-2
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
podporu	podpora	k1gFnSc4	podpora
SSH-1	SSH-1	k1gFnSc4	SSH-1
úplně	úplně	k6eAd1	úplně
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
verzí	verze	k1gFnPc2	verze
protokolu	protokol	k1gInSc2	protokol
SSH	SSH	kA	SSH
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neznámý	známý	k2eNgInSc1d1	neznámý
veřejný	veřejný	k2eAgInSc1d1	veřejný
klíč	klíč	k1gInSc1	klíč
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
jeho	jeho	k3xOp3gInPc7	jeho
schválením	schválení	k1gNnSc7	schválení
řádně	řádně	k6eAd1	řádně
ověřen	ověřen	k2eAgMnSc1d1	ověřen
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
dešifrování	dešifrování	k1gNnSc3	dešifrování
důvěrných	důvěrný	k2eAgFnPc2d1	důvěrná
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
útokům	útok	k1gInPc3	útok
typu	typ	k1gInSc2	typ
man-in-the-middle	mannheiddle	k6eAd1	man-in-the-middle
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
každý	každý	k3xTgInSc4	každý
šifrovaný	šifrovaný	k2eAgInSc4d1	šifrovaný
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
SSH	SSH	kA	SSH
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
nebo	nebo	k8xC	nebo
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nevěří	věřit	k5eNaImIp3nP	věřit
svým	svůj	k3xOyFgMnPc3	svůj
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
mít	mít	k5eAaImF	mít
jejich	jejich	k3xOp3gFnSc4	jejich
komunikaci	komunikace	k1gFnSc4	komunikace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
SSH	SSH	kA	SSH
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zabudované	zabudovaný	k2eAgInPc4d1	zabudovaný
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
mechanismy	mechanismus	k1gInPc4	mechanismus
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
tunelovaných	tunelovaný	k2eAgNnPc2d1	tunelované
spojení	spojení	k1gNnPc2	spojení
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
přenášet	přenášet	k5eAaImF	přenášet
velké	velký	k2eAgInPc4d1	velký
objemy	objem	k1gInPc4	objem
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
tak	tak	k6eAd1	tak
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
vstupní	vstupní	k2eAgInPc4d1	vstupní
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
nebo	nebo	k8xC	nebo
k	k	k7c3	k
průniku	průnik	k1gInSc3	průnik
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tytéž	týž	k3xTgFnPc1	týž
vlastnosti	vlastnost	k1gFnPc1	vlastnost
užitečné	užitečný	k2eAgFnSc2d1	užitečná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šifrování	šifrování	k1gNnSc1	šifrování
služeb	služba	k1gFnPc2	služba
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
POP3	POP3	k1gFnSc1	POP3
nebo	nebo	k8xC	nebo
IMAP	IMAP	kA	IMAP
prostým	prostý	k2eAgNnSc7d1	prosté
použitím	použití	k1gNnSc7	použití
SSH	SSH	kA	SSH
tunelu	tunel	k1gInSc6	tunel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
protokolů	protokol	k1gInPc2	protokol
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
protokol	protokol	k1gInSc1	protokol
SSH	SSH	kA	SSH
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
povolení	povolení	k1gNnSc1	povolení
průchodu	průchod	k1gInSc2	průchod
SSH	SSH	kA	SSH
přes	přes	k7c4	přes
firewall	firewall	k1gInSc4	firewall
může	moct	k5eAaImIp3nS	moct
stát	stát	k1gInSc1	stát
vážným	vážný	k2eAgNnSc7d1	vážné
bezpečnostním	bezpečnostní	k2eAgNnSc7d1	bezpečnostní
rizikem	riziko	k1gNnSc7	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
přesměrováním	přesměrování	k1gNnSc7	přesměrování
portů	port	k1gInPc2	port
totiž	totiž	k9	totiž
některé	některý	k3yIgFnPc4	některý
implementace	implementace	k1gFnPc4	implementace
SSH	SSH	kA	SSH
přímo	přímo	k6eAd1	přímo
podporují	podporovat	k5eAaImIp3nP	podporovat
Layer	Layer	k1gInSc4	Layer
<g/>
2	[number]	k4	2
VPN	VPN	kA	VPN
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivní	efektivní	k2eAgNnSc4d1	efektivní
spojení	spojení	k1gNnSc4	spojení
dvou	dva	k4xCgFnPc2	dva
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
ethernetových	ethernetový	k2eAgFnPc2d1	ethernetová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
ke	k	k7c3	k
stejnému	stejný	k2eAgNnSc3d1	stejné
switchi	switchi	k1gNnSc3	switchi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
hledá	hledat	k5eAaImIp3nS	hledat
řešení	řešení	k1gNnSc1	řešení
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
autentizaci	autentizace	k1gFnSc4	autentizace
uživatele	uživatel	k1gMnSc2	uživatel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
SSH	SSH	kA	SSH
použít	použít	k5eAaPmF	použít
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
vygenerován	vygenerován	k2eAgInSc1d1	vygenerován
pár	pár	k1gInSc1	pár
šifrovacích	šifrovací	k2eAgInPc2d1	šifrovací
klíčů	klíč	k1gInPc2	klíč
-	-	kIx~	-
privátní	privátní	k2eAgInSc4d1	privátní
(	(	kIx(	(
<g/>
soukromý	soukromý	k2eAgInSc4d1	soukromý
<g/>
)	)	kIx)	)
klíč	klíč	k1gInSc4	klíč
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Privátní	privátní	k2eAgNnSc1d1	privátní
je	být	k5eAaImIp3nS	být
bezpečně	bezpečně	k6eAd1	bezpečně
uložen	uložit	k5eAaPmNgInS	uložit
u	u	k7c2	u
uživatele	uživatel	k1gMnSc2	uživatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
heslovou	heslový	k2eAgFnSc7d1	heslová
frází	fráze	k1gFnSc7	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Veřejný	veřejný	k2eAgInSc1d1	veřejný
klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
na	na	k7c4	na
cílový	cílový	k2eAgInSc4d1	cílový
server	server	k1gInSc4	server
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
do	do	k7c2	do
domácího	domácí	k2eAgInSc2d1	domácí
adresáře	adresář	k1gInSc2	adresář
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
~	~	kIx~	~
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
<g/>
ssh	ssh	k?	ssh
<g/>
/	/	kIx~	/
<g/>
authorized_keys	authorized_keys	k1gInSc1	authorized_keys
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
přihlášení	přihlášení	k1gNnSc4	přihlášení
server	server	k1gInSc4	server
veřejným	veřejný	k2eAgInSc7d1	veřejný
klíčem	klíč	k1gInSc7	klíč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
zašifruje	zašifrovat	k5eAaPmIp3nS	zašifrovat
blok	blok	k1gInSc4	blok
náhodných	náhodný	k2eAgNnPc2d1	náhodné
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
výzvu	výzev	k1gInSc2	výzev
typu	typ	k1gInSc2	typ
challenge-response	challengeesponse	k1gFnSc2	challenge-response
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nelze	lze	k6eNd1	lze
snadno	snadno	k6eAd1	snadno
odvodit	odvodit	k5eAaPmF	odvodit
nebo	nebo	k8xC	nebo
uhádnout	uhádnout	k5eAaPmF	uhádnout
a	a	k8xC	a
pošle	pošle	k6eAd1	pošle
ji	on	k3xPp3gFnSc4	on
klientovi	klientův	k2eAgMnPc1d1	klientův
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
výzvu	výzev	k1gInSc2	výzev
pomocí	pomocí	k7c2	pomocí
privátního	privátní	k2eAgInSc2d1	privátní
klíče	klíč	k1gInSc2	klíč
dešifruje	dešifrovat	k5eAaBmIp3nS	dešifrovat
a	a	k8xC	a
dešifrovanou	dešifrovaný	k2eAgFnSc7d1	dešifrovaná
ji	on	k3xPp3gFnSc4	on
pošle	poslat	k5eAaPmIp3nS	poslat
zpět	zpět	k6eAd1	zpět
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
výzva	výzva	k1gFnSc1	výzva
správně	správně	k6eAd1	správně
rozšifrována	rozšifrován	k2eAgFnSc1d1	rozšifrována
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tím	ten	k3xDgNnSc7	ten
server	server	k1gInSc4	server
ověřeno	ověřen	k2eAgNnSc1d1	ověřeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
klient	klient	k1gMnSc1	klient
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
privátní	privátní	k2eAgInSc4d1	privátní
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
veřejnému	veřejný	k2eAgInSc3d1	veřejný
klíči	klíč	k1gInSc3	klíč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
server	server	k1gInSc1	server
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
může	moct	k5eAaImIp3nS	moct
přístup	přístup	k1gInSc4	přístup
schválit	schválit	k5eAaPmF	schválit
(	(	kIx(	(
<g/>
autorizovat	autorizovat	k5eAaBmF	autorizovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
výzva	výzva	k1gFnSc1	výzva
nebude	být	k5eNaImBp3nS	být
správně	správně	k6eAd1	správně
rozšifrována	rozšifrován	k2eAgFnSc1d1	rozšifrována
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
přístup	přístup	k1gInSc1	přístup
pro	pro	k7c4	pro
klienta	klient	k1gMnSc4	klient
zamítnut	zamítnut	k2eAgInSc1d1	zamítnut
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
privátní	privátní	k2eAgInSc1d1	privátní
klíč	klíč	k1gInSc1	klíč
neopustí	opustit	k5eNaPmIp3nS	opustit
klientův	klientův	k2eAgInSc1d1	klientův
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ho	on	k3xPp3gNnSc4	on
někdo	někdo	k3yInSc1	někdo
odcizil	odcizit	k5eAaPmAgMnS	odcizit
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
po	po	k7c6	po
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
autenticity	autenticita	k1gFnSc2	autenticita
klienta	klient	k1gMnSc2	klient
a	a	k8xC	a
umožnění	umožnění	k1gNnSc1	umožnění
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
Bob	Bob	k1gMnSc1	Bob
se	se	k3xPyFc4	se
přihlašuje	přihlašovat	k5eAaImIp3nS	přihlašovat
na	na	k7c4	na
alicePC	alicePC	k?	alicePC
pod	pod	k7c7	pod
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lokální	lokální	k2eAgFnSc2d1	lokální
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
alicePC	alicePC	k?	alicePC
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
IP	IP	kA	IP
adresu	adresa	k1gFnSc4	adresa
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
na	na	k7c4	na
alicePC	alicePC	k?	alicePC
běží	běžet	k5eAaImIp3nS	běžet
ssh	ssh	k?	ssh
démon	démon	k1gMnSc1	démon
<g/>
/	/	kIx~	/
<g/>
server	server	k1gInSc1	server
lze	lze	k6eAd1	lze
ověřit	ověřit	k5eAaPmF	ověřit
výpisem	výpis	k1gInSc7	výpis
#	#	kIx~	#
netstat	netstat	k1gInSc1	netstat
-nltp	ltp	k1gInSc1	-nltp
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bob	Bob	k1gMnSc1	Bob
<g/>
:	:	kIx,	:
$	$	kIx~	$
ssh	ssh	k?	ssh
alice@alicePC.cz	alice@alicePC.cz	k1gMnSc1	alice@alicePC.cz
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
alicePC	alicePC	k?	alicePC
ani	ani	k8xC	ani
bobPC	bobPC	k?	bobPC
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
IP	IP	kA	IP
adresu	adresa	k1gFnSc4	adresa
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
využít	využít	k5eAaPmF	využít
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
-	-	kIx~	-
walterPC	walterPC	k?	walterPC
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
na	na	k7c4	na
Walterův	Walterův	k2eAgInSc4d1	Walterův
stroj	stroj	k1gInSc4	stroj
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
alicePC	alicePC	k?	alicePC
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
běžný	běžný	k2eAgMnSc1d1	běžný
uživatel	uživatel	k1gMnSc1	uživatel
nemůže	moct	k5eNaImIp3nS	moct
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
porty	port	k1gInPc7	port
nižšími	nízký	k2eAgInPc7d2	nižší
než	než	k8xS	než
1024	[number]	k4	1024
a	a	k8xC	a
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
root	root	k5eAaPmF	root
přihlášení	přihlášení	k1gNnSc4	přihlášení
pomocí	pomocí	k7c2	pomocí
ssh	ssh	k?	ssh
nemívá	mívat	k5eNaImIp3nS	mívat
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ssh	ssh	k?	ssh
server	server	k1gInSc1	server
Alice	Alice	k1gFnSc2	Alice
naslouchal	naslouchat	k5eAaImAgInS	naslouchat
např.	např.	kA	např.
na	na	k7c6	na
portu	port	k1gInSc6	port
6262	[number]	k4	6262
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
/	/	kIx~	/
<g/>
etc	etc	k?	etc
<g/>
/	/	kIx~	/
<g/>
ssh	ssh	k?	ssh
<g/>
/	/	kIx~	/
<g/>
sshd_config	sshd_config	k1gMnSc1	sshd_config
stačí	stačit	k5eAaBmIp3nS	stačit
přidat	přidat	k5eAaPmF	přidat
řádek	řádek	k1gInSc4	řádek
Port	port	k1gInSc1	port
6262	[number]	k4	6262
(	(	kIx(	(
<g/>
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
nastavení	nastavení	k1gNnSc2	nastavení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
službu	služba	k1gFnSc4	služba
restartovat	restartovat	k5eAaBmF	restartovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
<g/>
:	:	kIx,	:
Alice	Alice	k1gFnSc1	Alice
<g/>
:	:	kIx,	:
$	$	kIx~	$
ssh	ssh	k?	ssh
walter@walterPC.cz	walter@walterPC.cz	k1gInSc1	walter@walterPC.cz
-p	-p	k?	-p
20085	[number]	k4	20085
-R	-R	k?	-R
6262	[number]	k4	6262
<g/>
:	:	kIx,	:
<g/>
localhost	localhost	k1gFnSc1	localhost
<g/>
:	:	kIx,	:
<g/>
6262	[number]	k4	6262
Bob	Bob	k1gMnSc1	Bob
<g/>
:	:	kIx,	:
$	$	kIx~	$
ssh	ssh	k?	ssh
walter@walterPC.cz	walter@walterPC.cz	k1gInSc1	walter@walterPC.cz
-p	-p	k?	-p
20085	[number]	k4	20085
Bob	bob	k1gInSc1	bob
již	již	k6eAd1	již
z	z	k7c2	z
walterPC	walterPC	k?	walterPC
<g/>
:	:	kIx,	:
$	$	kIx~	$
ssh	ssh	k?	ssh
alice@localhost	alice@localhost	k1gFnSc1	alice@localhost
-p	-p	k?	-p
6262	[number]	k4	6262
SSH	SSH	kA	SSH
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
pro	pro	k7c4	pro
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
se	se	k3xPyFc4	se
při	při	k7c6	při
navázání	navázání	k1gNnSc6	navázání
spojení	spojení	k1gNnSc2	spojení
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
SSH	SSH	kA	SSH
démonu	démon	k1gMnSc3	démon
(	(	kIx(	(
<g/>
SSH	SSH	kA	SSH
daemon	daemon	k1gMnSc1	daemon
<g/>
,	,	kIx,	,
sshd	sshd	k1gMnSc1	sshd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SSH	SSH	kA	SSH
démon	démon	k1gMnSc1	démon
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
nastavení	nastavení	k1gNnSc2	nastavení
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
spojení	spojení	k1gNnSc1	spojení
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
formu	forma	k1gFnSc4	forma
autentizace	autentizace	k1gFnSc2	autentizace
bude	být	k5eAaImBp3nS	být
požadovat	požadovat	k5eAaImF	požadovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
portu	port	k1gInSc6	port
bude	být	k5eAaImBp3nS	být
naslouchat	naslouchat	k5eAaImF	naslouchat
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnSc1	implementace
SSH	SSH	kA	SSH
klientů	klient	k1gMnPc2	klient
i	i	k8xC	i
serverů	server	k1gInPc2	server
(	(	kIx(	(
<g/>
SSH	SSH	kA	SSH
démon	démon	k1gMnSc1	démon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
téměř	téměř	k6eAd1	téměř
pro	pro	k7c4	pro
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
platformu	platforma	k1gFnSc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
jak	jak	k8xS	jak
komerční	komerční	k2eAgFnPc1d1	komerční
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Open	Open	k1gNnSc1	Open
Source	Sourec	k1gInSc2	Sourec
varianty	varianta	k1gFnSc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
oblíbenosti	oblíbenost	k1gFnSc6	oblíbenost
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
používalo	používat	k5eAaImAgNnS	používat
SSH	SSH	kA	SSH
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Lsh	Lsh	k?	Lsh
<g/>
,	,	kIx,	,
implementace	implementace	k1gFnSc1	implementace
SSH	SSH	kA	SSH
klientu	klient	k1gMnSc3	klient
i	i	k9	i
serveru	server	k1gInSc2	server
spravované	spravovaný	k2eAgInPc1d1	spravovaný
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
GNU	gnu	k1gMnSc1	gnu
OpenSSH	OpenSSH	k1gMnSc1	OpenSSH
<g/>
,	,	kIx,	,
open	open	k1gMnSc1	open
source	source	k1gMnSc2	source
implementace	implementace	k1gFnSc2	implementace
SSH	SSH	kA	SSH
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
odštěpená	odštěpený	k2eAgFnSc1d1	odštěpená
z	z	k7c2	z
originálního	originální	k2eAgMnSc2d1	originální
SSH-1	SSH-1	k1gMnSc2	SSH-1
(	(	kIx(	(
<g/>
klient	klient	k1gMnSc1	klient
i	i	k8xC	i
server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
PuTTY	PuTTY	k1gMnSc1	PuTTY
<g/>
,	,	kIx,	,
SSH	SSH	kA	SSH
klient	klient	k1gMnSc1	klient
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
SSH	SSH	kA	SSH
Tectia	Tectia	k1gFnSc1	Tectia
Client	Client	k1gMnSc1	Client
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
PenguiNet	PenguiNeta	k1gFnPc2	PenguiNeta
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
SSHDOS	SSHDOS	kA	SSHDOS
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
WinSCP	WinSCP	k1gFnSc2	WinSCP
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
souborový	souborový	k2eAgInSc1d1	souborový
manažer	manažer	k1gInSc1	manažer
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
knihovnách	knihovna	k1gFnPc6	knihovna
<g />
.	.	kIx.	.
</s>
<s>
Putty	Putt	k1gInPc1	Putt
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
sftp	sftp	k1gInSc1	sftp
a	a	k8xC	a
scp	scp	k?	scp
JavaSSH	JavaSSH	k1gFnSc1	JavaSSH
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Dropbear	Dropbear	k1gMnSc1	Dropbear
<g/>
,	,	kIx,	,
malý	malý	k2eAgMnSc1d1	malý
klient	klient	k1gMnSc1	klient
a	a	k8xC	a
server	server	k1gInSc4	server
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
OS	OS	kA	OS
splňující	splňující	k2eAgFnSc4d1	splňující
normu	norma	k1gFnSc4	norma
POSIX	POSIX	kA	POSIX
Idokorro	Idokorro	k1gNnSc1	Idokorro
Mobile	mobile	k1gNnPc2	mobile
SSH	SSH	kA	SSH
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
implementace	implementace	k1gFnSc2	implementace
SSH	SSH	kA	SSH
pro	pro	k7c4	pro
RIM	RIM	kA	RIM
BlackBerry	BlackBerra	k1gFnSc2	BlackBerra
a	a	k8xC	a
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
Ganymed-SSH	Ganymed-SSH	k1gFnSc2	Ganymed-SSH
<g/>
2	[number]	k4	2
Open	Openo	k1gNnPc2	Openo
Source	Sourec	k1gInSc2	Sourec
klientská	klientský	k2eAgFnSc1d1	klientská
knihovna	knihovna	k1gFnSc1	knihovna
funkcí	funkce	k1gFnPc2	funkce
SSH2	SSH2	k1gFnPc2	SSH2
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
SSH	SSH	kA	SSH
přes	přes	k7c4	přes
HTTP	HTTP	kA	HTTP
TinySSH	TinySSH	k1gMnSc1	TinySSH
<g/>
,	,	kIx,	,
minimalistický	minimalistický	k2eAgMnSc1d1	minimalistický
server	server	k1gInSc4	server
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
BSD	BSD	kA	BSD
</s>
