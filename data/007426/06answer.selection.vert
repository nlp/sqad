<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
13	[number]	k4	13
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
6	[number]	k4	6
cen	cena	k1gFnPc2	cena
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ceny	cena	k1gFnSc2	cena
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc2	Zemeckis
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanksa	k1gFnPc2	Hanksa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
