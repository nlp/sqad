<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
<g/>
,	,	kIx,	,
KBE	KBE	kA	KBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
argentinsko-izraelsko-španělský	argentinskozraelsko-španělský	k2eAgMnSc1d1	argentinsko-izraelsko-španělský
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
operního	operní	k2eAgInSc2d1	operní
domu	dům	k1gInSc2	dům
La	la	k1gNnSc2	la
Scala	scát	k5eAaImAgFnS	scát
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
je	být	k5eAaImIp3nS	být
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
Státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
Pod	pod	k7c7	pod
lipami	lípa	k1gFnPc7	lípa
(	(	kIx(	(
<g/>
Staatsoper	Staatsoper	k1gInSc1	Staatsoper
unter	unter	k1gInSc1	unter
den	den	k1gInSc1	den
Linden	Lindna	k1gFnPc2	Lindna
<g/>
)	)	kIx)	)
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Řádu	řád	k1gInSc2	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgInSc2d1	francouzský
Řádu	řád	k1gInSc2	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
německého	německý	k2eAgInSc2d1	německý
Velkého	velký	k2eAgInSc2d1	velký
spolkového	spolkový	k2eAgInSc2d1	spolkový
řádu	řád	k1gInSc2	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
Ceny	cena	k1gFnPc1	cena
Willyho	Willy	k1gMnSc2	Willy
Brandta	Brandta	k1gFnSc1	Brandta
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgFnPc1d1	španělská
Ceny	cena	k1gFnPc1	cena
Prince	princ	k1gMnSc2	princ
Asturijského	Asturijský	k2eAgMnSc2d1	Asturijský
a	a	k8xC	a
sedminásobným	sedminásobný	k2eAgMnSc7d1	sedminásobný
držitelem	držitel	k1gMnSc7	držitel
Ceny	cena	k1gFnSc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Barenboim	Barenboim	k6eAd1	Barenboim
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jej	on	k3xPp3gInSc4	on
učila	učít	k5eAaPmAgFnS	učít
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
učitele	učitel	k1gMnSc4	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
klavírní	klavírní	k2eAgInSc4d1	klavírní
koncert	koncert	k1gInSc4	koncert
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
pak	pak	k6eAd1	pak
pravidelně	pravidelně	k6eAd1	pravidelně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
pódiích	pódium	k1gNnPc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
koncertní	koncertní	k2eAgInSc1d1	koncertní
debut	debut	k1gInSc1	debut
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Salzburku	Salzburk	k1gInSc6	Salzburk
dirigentský	dirigentský	k2eAgInSc4d1	dirigentský
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
pak	pak	k6eAd1	pak
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
obory	obora	k1gFnSc2	obora
komponování	komponování	k1gNnSc2	komponování
a	a	k8xC	a
dirigování	dirigování	k1gNnSc2	dirigování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barenboim	Barenboim	k1gMnSc1	Barenboim
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
také	také	k9	také
svou	svůj	k3xOyFgFnSc7	svůj
snahou	snaha	k1gFnSc7	snaha
o	o	k7c6	o
usmíření	usmíření	k1gNnSc6	usmíření
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
palestinskými	palestinský	k2eAgMnPc7d1	palestinský
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
palestinským	palestinský	k2eAgMnSc7d1	palestinský
literárním	literární	k2eAgMnSc7d1	literární
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
již	již	k9	již
zemřelým	zemřelý	k2eAgMnSc7d1	zemřelý
Edwardem	Edward	k1gMnSc7	Edward
Saidem	Said	k1gMnSc7	Said
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Orchestr	orchestr	k1gInSc1	orchestr
Západovýchodní	západovýchodní	k2eAgInSc4d1	západovýchodní
díván	díván	k1gInSc4	díván
(	(	kIx(	(
<g/>
West-Eastern	West-Eastern	k1gInSc1	West-Eastern
Divan	divan	k1gInSc1	divan
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
izraelských	izraelský	k2eAgMnPc2d1	izraelský
<g/>
,	,	kIx,	,
palestinských	palestinský	k2eAgMnPc2d1	palestinský
a	a	k8xC	a
španělských	španělský	k2eAgMnPc2d1	španělský
hudebníků	hudebník	k1gMnPc2	hudebník
(	(	kIx(	(
<g/>
věková	věkový	k2eAgFnSc1d1	věková
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
jej	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
orchestr	orchestr	k1gInSc1	orchestr
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Seville	Sevilla	k1gFnSc6	Sevilla
a	a	k8xC	a
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
Orchestr	orchestr	k1gInSc1	orchestr
Západovýchodní	západovýchodní	k2eAgInSc4d1	západovýchodní
díván	díván	k1gInSc4	díván
na	na	k7c6	na
berlínském	berlínský	k2eAgNnSc6d1	berlínské
Lesním	lesní	k2eAgNnSc6d1	lesní
jevišti	jeviště	k1gNnSc6	jeviště
(	(	kIx(	(
<g/>
Waldbühne	Waldbühne	k1gMnSc1	Waldbühne
<g/>
)	)	kIx)	)
před	před	k7c7	před
15	[number]	k4	15
000	[number]	k4	000
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
koncertu	koncert	k1gInSc2	koncert
byly	být	k5eAaImAgFnP	být
předehry	předehra	k1gFnPc1	předehra
k	k	k7c3	k
operám	opera	k1gFnPc3	opera
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdiho	Verdi	k1gMnSc2	Verdi
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
a	a	k8xC	a
také	také	k9	také
Fantastická	fantastický	k2eAgFnSc1d1	fantastická
symfonie	symfonie	k1gFnSc1	symfonie
od	od	k7c2	od
Hectora	Hector	k1gMnSc2	Hector
Berlioze	Berlioz	k1gMnSc2	Berlioz
<g/>
.	.	kIx.	.
<g/>
Díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
vlivných	vlivný	k2eAgMnPc2d1	vlivný
sponzorů	sponzor	k1gMnPc2	sponzor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Daniela	Daniel	k1gMnSc2	Daniel
Barenboima	Barenboim	k1gMnSc2	Barenboim
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přestavováno	přestavován	k2eAgNnSc1d1	přestavováno
nevyužité	využitý	k2eNgNnSc1d1	nevyužité
skladiště	skladiště	k1gNnSc1	skladiště
Státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
Pod	pod	k7c7	pod
lipami	lípa	k1gFnPc7	lípa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
působit	působit	k5eAaImF	působit
"	"	kIx"	"
<g/>
Barenboimova	Barenboimův	k2eAgFnSc1d1	Barenboimův
a	a	k8xC	a
Saidova	Saidův	k2eAgFnSc1d1	Saidova
Akademie	akademie	k1gFnSc1	akademie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Barenboim-Said	Barenboim-Said	k1gInSc1	Barenboim-Said
Akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bude	být	k5eAaImBp3nS	být
místo	místo	k6eAd1	místo
pro	pro	k7c4	pro
až	až	k9	až
100	[number]	k4	100
stipendistů	stipendista	k1gMnPc2	stipendista
<g/>
,	,	kIx,	,
nastávajících	nastávající	k2eAgMnPc2d1	nastávající
hudebníků	hudebník	k1gMnPc2	hudebník
ze	z	k7c2	z
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
koncertní	koncertní	k2eAgInSc4d1	koncertní
sál	sál	k1gInSc4	sál
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
violoncelistka	violoncelistka	k1gFnSc1	violoncelistka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
du	du	k?	du
Pré	pré	k1gNnPc7	pré
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
roztroušenou	roztroušený	k2eAgFnSc4d1	roztroušená
sklerózu	skleróza	k1gFnSc4	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
Důvěrný	důvěrný	k2eAgInSc1d1	důvěrný
vztah	vztah	k1gInSc1	vztah
udržoval	udržovat	k5eAaImAgInS	udržovat
i	i	k9	i
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
klavíristkou	klavíristka	k1gFnSc7	klavíristka
Jelenou	Jelena	k1gFnSc7	Jelena
Baškirovovou	Baškirovová	k1gFnSc7	Baškirovová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
syn	syn	k1gMnSc1	syn
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
manažer	manažer	k1gMnSc1	manažer
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
německé	německý	k2eAgFnSc2d1	německá
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Michael	Michael	k1gMnSc1	Michael
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
houslista	houslista	k1gMnSc1	houslista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgMnSc7d1	státní
občanem	občan	k1gMnSc7	občan
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
také	také	k9	také
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Daniel	Daniela	k1gFnPc2	Daniela
Barenboim	Barenboimo	k1gNnPc2	Barenboimo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.lidovky.cz/svetoznamy-klavirni-recital-daniel-barenboim-vzbudil-v-praze-nadseni-1kt-/ln_kultura.asp?c=A100225_081800_ln_kultura_pks	[url]	k6eAd1	http://www.lidovky.cz/svetoznamy-klavirni-recital-daniel-barenboim-vzbudil-v-praze-nadseni-1kt-/ln_kultura.asp?c=A100225_081800_ln_kultura_pks
</s>
</p>
<p>
<s>
http://www.literarky.cz/kultura/hudba/6244-daniel-barenboim-dirigentem-a-umeleckym-sefem-la-scaly	[url]	k5eAaImAgFnP	http://www.literarky.cz/kultura/hudba/6244-daniel-barenboim-dirigentem-a-umeleckym-sefem-la-scaly
</s>
</p>
<p>
<s>
REITTEREROVÁ	REITTEREROVÁ	kA	REITTEREROVÁ
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
lidské	lidský	k2eAgNnSc4d1	lidské
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
ne	ne	k9	ne
vojenské	vojenský	k2eAgFnSc3d1	vojenská
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
slaví	slavit	k5eAaImIp3nS	slavit
pětasedmdesátiny	pětasedmdesátina	k1gFnPc4	pětasedmdesátina
na	na	k7c6	na
Opeře	opera	k1gFnSc6	opera
plus	plus	k1gInSc1	plus
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
official	official	k1gMnSc1	official
website	websit	k1gInSc5	websit
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
Revealed	Revealed	k1gMnSc1	Revealed
on	on	k3xPp3gMnSc1	on
CNN	CNN	kA	CNN
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Parallels	Parallels	k1gInSc1	Parallels
and	and	k?	and
Paradoxes	Paradoxes	k1gInSc1	Paradoxes
<g/>
,	,	kIx,	,
NPR	NPR	kA	NPR
interview	interview	k1gNnSc1	interview
with	with	k1gInSc1	with
Barenboim	Barenboim	k1gMnSc1	Barenboim
and	and	k?	and
Edward	Edward	k1gMnSc1	Edward
Said	Said	k1gMnSc1	Said
<g/>
,	,	kIx,	,
28	[number]	k4	28
December	December	k1gInSc1	December
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
In	In	k?	In
harmony	harmon	k1gMnPc7	harmon
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Guardian	Guardian	k1gInSc4	Guardian
<g/>
]	]	kIx)	]
newspaper	wspaprat	k5eNaPmRp2nS	wspaprat
feature	featur	k1gMnSc5	featur
on	on	k3xPp3gMnSc1	on
Barenboim	Barenboim	k1gMnSc1	Barenboim
and	and	k?	and
Said	Said	k1gMnSc1	Said
<g/>
,	,	kIx,	,
5	[number]	k4	5
April	April	k1gInSc1	April
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
In	In	k?	In
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
was	was	k?	was
Sound	Sound	k1gInSc1	Sound
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
4	[number]	k4	4
Reith	Reitha	k1gFnPc2	Reitha
Lectures	Lecturesa	k1gFnPc2	Lecturesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
3	[number]	k4	3
interviews	interviewsa	k1gFnPc2	interviewsa
<g/>
,	,	kIx,	,
November	Novembra	k1gFnPc2	Novembra
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Discography	Discograph	k1gInPc1	Discograph
at	at	k?	at
SonyBMG	SonyBMG	k1gMnSc4	SonyBMG
Masterworks	Masterworks	k1gInSc1	Masterworks
</s>
</p>
<p>
<s>
Elgar	Elgar	k1gInSc1	Elgar
Cello	cello	k1gNnSc1	cello
Concerto	Concerta	k1gFnSc5	Concerta
in	in	k?	in
E	E	kA	E
minor	minor	k2eAgFnSc1d1	minor
<g/>
,	,	kIx,	,
opus	opus	k1gInSc1	opus
85	[number]	k4	85
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Du	Du	k?	Du
Pré	pré	k1gNnSc1	pré
with	with	k1gMnSc1	with
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
and	and	k?	and
The	The	k1gMnSc1	The
New	New	k1gFnSc4	New
Philharmonia	Philharmonium	k1gNnSc2	Philharmonium
Orchestra	orchestra	k1gFnSc1	orchestra
on	on	k3xPp3gMnSc1	on
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
Review	Review	k?	Review
<g/>
:	:	kIx,	:
Fidelio	Fidelio	k1gMnSc1	Fidelio
played	played	k1gMnSc1	played
by	by	kYmCp3nS	by
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
and	and	k?	and
the	the	k?	the
West-Eastern	West-Eastern	k1gInSc1	West-Eastern
Divan	divan	k1gInSc1	divan
Orchestra	orchestra	k1gFnSc1	orchestra
</s>
</p>
<p>
<s>
Westphalian	Westphalian	k1gInSc1	Westphalian
Peace	Peace	k1gFnSc2	Peace
Prize	Prize	k1gFnSc2	Prize
</s>
</p>
<p>
<s>
Barenboim	Barenboim	k1gInSc1	Barenboim
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
outstanding	outstanding	k1gInSc4	outstanding
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
the	the	k?	the
symphony	symphon	k1gInPc1	symphon
cycle	cycle	k1gInSc1	cycle
at	at	k?	at
classicstoday	classicstodaa	k1gFnSc2	classicstodaa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
