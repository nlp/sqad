<p>
<s>
Yaleova	Yaleův	k2eAgFnSc1d1	Yaleova
univerzita	univerzita	k1gFnSc1	univerzita
[	[	kIx(	[
<g/>
jejl	jejl	k1gInSc1	jejl
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prestižní	prestižní	k2eAgFnSc1d1	prestižní
soukromá	soukromý	k2eAgFnSc1d1	soukromá
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
New	New	k1gFnSc6	New
Havenu	Haven	k1gInSc2	Haven
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Ivy	Iva	k1gFnSc2	Iva
League	Leagu	k1gInSc2	Leagu
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
univerzitám	univerzita	k1gFnPc3	univerzita
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Maskotem	maskot	k1gInSc7	maskot
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
buldok	buldok	k1gMnSc1	buldok
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Handsome	Handsom	k1gInSc5	Handsom
Dan	Dan	k1gMnSc1	Dan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc7d1	tradiční
barvou	barva	k1gFnSc7	barva
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
Yale	Yale	k1gFnSc1	Yale
blue	blu	k1gFnSc2	blu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Yaleova	Yaleův	k2eAgFnSc1d1	Yaleova
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1701	[number]	k4	1701
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
nejstarší	starý	k2eAgFnSc7d3	nejstarší
institucí	instituce	k1gFnSc7	instituce
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgMnSc4	první
mecenáše	mecenáš	k1gMnSc4	mecenáš
Elihu	Eliha	k1gMnSc4	Eliha
Yalea	Yaleus	k1gMnSc4	Yaleus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1716	[number]	k4	1716
daroval	darovat	k5eAaPmAgInS	darovat
významný	významný	k2eAgInSc1d1	významný
finanční	finanční	k2eAgInSc1d1	finanční
obnos	obnos	k1gInSc1	obnos
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
knih	kniha	k1gFnPc2	kniha
<g/>
;	;	kIx,	;
hovorově	hovorově	k6eAd1	hovorově
bývá	bývat	k5eAaImIp3nS	bývat
též	též	k9	též
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Starý	Starý	k1gMnSc1	Starý
Eli	Eli	k1gMnSc1	Eli
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Eli	Eli	k1gFnPc2	Eli
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
právních	právní	k2eAgFnPc2d1	právní
klinik	klinika	k1gFnPc2	klinika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Současným	současný	k2eAgMnSc7d1	současný
rektorem	rektor	k1gMnSc7	rektor
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
Peter	Peter	k1gMnSc1	Peter
Salovey	Salovea	k1gFnSc2	Salovea
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
funkci	funkce	k1gFnSc4	funkce
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
300	[number]	k4	300
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
11	[number]	k4	11
390	[number]	k4	390
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
kampus	kampus	k1gInSc1	kampus
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
1,1	[number]	k4	1,1
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
Yale	Yale	k1gInSc1	Yale
mezi	mezi	k7c4	mezi
nejvýběrovější	výběrový	k2eAgFnPc4d3	výběrový
univerzity	univerzita	k1gFnPc4	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
přijatých	přijatý	k2eAgMnPc2d1	přijatý
studentů	student	k1gMnPc2	student
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
uchazečům	uchazeč	k1gMnPc3	uchazeč
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
7	[number]	k4	7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ročník	ročník	k1gInSc4	ročník
s	s	k7c7	s
ukončením	ukončení	k1gNnSc7	ukončení
studia	studio	k1gNnSc2	studio
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
1	[number]	k4	1
935	[number]	k4	935
studentů	student	k1gMnPc2	student
z	z	k7c2	z
celkových	celkový	k2eAgNnPc2d1	celkové
30	[number]	k4	30
932	[number]	k4	932
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
stipendií	stipendium	k1gNnPc2	stipendium
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
při	při	k7c6	při
přijímacím	přijímací	k2eAgNnSc6d1	přijímací
řízení	řízení	k1gNnSc6	řízení
neberou	brát	k5eNaImIp3nP	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
postavení	postavení	k1gNnSc4	postavení
uchazeče	uchazeč	k1gMnSc2	uchazeč
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
need-blind	needlind	k1gInSc1	need-blind
admission	admission	k1gInSc1	admission
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
studentů	student	k1gMnPc2	student
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rezidenčních	rezidenční	k2eAgFnPc6d1	rezidenční
kolejích	kolej	k1gFnPc6	kolej
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
kampusu	kampus	k1gInSc6	kampus
<g/>
.	.	kIx.	.
</s>
<s>
Kolejí	kolej	k1gFnSc7	kolej
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
známého	známý	k2eAgMnSc2d1	známý
absolventa	absolvent	k1gMnSc2	absolvent
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
důležitého	důležitý	k2eAgNnSc2d1	důležité
místa	místo	k1gNnSc2	místo
či	či	k8xC	či
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
budou	být	k5eAaImBp3nP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
rezidenční	rezidenční	k2eAgFnPc1d1	rezidenční
koleje	kolej	k1gFnPc1	kolej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Yale	Yale	k1gFnSc6	Yale
také	také	k9	také
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
mýty	mýtus	k1gInPc4	mýtus
opředený	opředený	k2eAgInSc1d1	opředený
spolek	spolek	k1gInSc1	spolek
Skull	Skull	k1gMnSc1	Skull
and	and	k?	and
Bones	Bones	k1gMnSc1	Bones
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Lebka	lebka	k1gFnSc1	lebka
a	a	k8xC	a
hnáty	hnát	k1gInPc1	hnát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
údajně	údajně	k6eAd1	údajně
patřily	patřit	k5eAaImAgInP	patřit
nebo	nebo	k8xC	nebo
i	i	k9	i
nadále	nadále	k6eAd1	nadále
patří	patřit	k5eAaImIp3nS	patřit
některé	některý	k3yIgFnPc4	některý
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
některých	některý	k3yIgMnPc2	některý
bývalých	bývalý	k2eAgMnPc2d1	bývalý
prezidentů	prezident	k1gMnPc2	prezident
USA	USA	kA	USA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
mimoškolní	mimoškolní	k2eAgFnPc4d1	mimoškolní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
hraní	hranit	k5eAaImIp3nS	hranit
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
souboru	soubor	k1gInSc6	soubor
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
i	i	k9	i
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Yale	Yale	k1gNnSc6	Yale
existuje	existovat	k5eAaImIp3nS	existovat
35	[number]	k4	35
sportovních	sportovní	k2eAgNnPc2d1	sportovní
družstev	družstvo	k1gNnPc2	družstvo
(	(	kIx(	(
<g/>
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
Yale	Yal	k1gInPc1	Yal
Bulldogs	Bulldogsa	k1gFnPc2	Bulldogsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
I.	I.	kA	I.
divizi	divize	k1gFnSc6	divize
NCAA	NCAA	kA	NCAA
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
konference	konference	k1gFnSc2	konference
Ivy	Iva	k1gFnSc2	Iva
League	Leagu	k1gFnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Yale	Yale	k1gFnSc1	Yale
versus	versus	k7c1	versus
Harvard	Harvard	k1gInSc1	Harvard
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
Yale	Yale	k1gFnSc1	Yale
a	a	k8xC	a
Harvardova	Harvardův	k2eAgFnSc1d1	Harvardova
univerzita	univerzita	k1gFnSc1	univerzita
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
soupeři	soupeř	k1gMnPc1	soupeř
<g/>
,	,	kIx,	,
nejenom	nejenom	k6eAd1	nejenom
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
veslování	veslování	k1gNnSc6	veslování
či	či	k8xC	či
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
utkání	utkání	k1gNnSc1	utkání
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
týmy	tým	k1gInPc4	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc1d1	populární
a	a	k8xC	a
prestižní	prestižní	k2eAgFnSc1d1	prestižní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
prestižní	prestižní	k2eAgFnSc7d1	prestižní
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
Harvard-Yale	Harvard-Yala	k1gFnSc3	Harvard-Yala
Regatta	Regatt	k1gInSc2	Regatt
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k6eAd1	také
Harvard-Yale	Harvard-Yala	k1gFnSc3	Harvard-Yala
Boat	Boat	k2eAgInSc4d1	Boat
Race	Race	k1gInSc4	Race
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
veslařský	veslařský	k2eAgInSc1d1	veslařský
závod	závod	k1gInSc1	závod
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
univerzitami	univerzita	k1gFnPc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
se	s	k7c7	s
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
jezdí	jezdit	k5eAaImIp3nP	jezdit
každoročně	každoročně	k6eAd1	každoročně
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nejstarší	starý	k2eAgNnSc4d3	nejstarší
americké	americký	k2eAgNnSc4d1	americké
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
sportovní	sportovní	k2eAgNnSc4d1	sportovní
klání	klání	k1gNnSc4	klání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Absolventi	absolvent	k1gMnPc1	absolvent
===	===	k?	===
</s>
</p>
<p>
<s>
George	George	k6eAd1	George
Akerlof	Akerlof	k1gMnSc1	Akerlof
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
</s>
</p>
<p>
<s>
Jennifer	Jennifer	k1gInSc1	Jennifer
Beals	Beals	k1gInSc1	Beals
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Hiram	Hiram	k6eAd1	Hiram
Bingham	Bingham	k1gInSc1	Bingham
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
cestovatel	cestovatel	k1gMnSc1	cestovatel
</s>
</p>
<p>
<s>
Sherrod	Sherroda	k1gFnPc2	Sherroda
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
</s>
</p>
<p>
<s>
George	Georg	k1gInPc1	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
41	[number]	k4	41
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Bushnell	Bushnell	k1gMnSc1	Bushnell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
C.	C.	kA	C.
Calhoun	Calhoun	k1gMnSc1	Calhoun
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Carstens	Carstensa	k1gFnPc2	Carstensa
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
<p>
<s>
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
–	–	k?	–
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
42	[number]	k4	42
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Hillary	Hillar	k1gInPc4	Hillar
Clintonová	Clintonová	k1gFnSc1	Clintonová
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
senátorka	senátorka	k1gFnSc1	senátorka
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Fenimore	Fenimor	k1gInSc5	Fenimor
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
Davis	Davis	k1gFnSc2	Davis
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
W.	W.	kA	W.
Edwards	Edwards	k1gInSc1	Edwards
Deming	Deming	k1gInSc1	Deming
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
statistik	statistik	k1gMnSc1	statistik
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Duchovny	duchovno	k1gNnPc7	duchovno
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
,	,	kIx,	,
severoamerický	severoamerický	k2eAgMnSc1d1	severoamerický
evangelikální	evangelikální	k2eAgMnSc1d1	evangelikální
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Franklin	Franklina	k1gFnPc2	Franklina
Enders	Enders	k1gInSc1	Enders
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1954	[number]	k4	1954
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Fenn	Fenn	k1gMnSc1	Fenn
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Gerald	Gerald	k6eAd1	Gerald
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
38	[number]	k4	38
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Jodie	Jodie	k1gFnSc1	Jodie
Foster	Fostra	k1gFnPc2	Fostra
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Foster	Foster	k1gMnSc1	Foster
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
</s>
</p>
<p>
<s>
Murray	Murraa	k1gFnPc1	Murraa
Gell-Mann	Gell-Manna	k1gFnPc2	Gell-Manna
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Josiah	Josiah	k1gInSc1	Josiah
Willard	Willarda	k1gFnPc2	Willarda
Gibbs	Gibbs	k1gInSc1	Gibbs
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
G.	G.	kA	G.
Gilman	Gilman	k1gMnSc1	Gilman
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
</s>
</p>
<p>
<s>
Grace	Grako	k6eAd1	Grako
Hopperová	Hopperový	k2eAgFnSc1d1	Hopperový
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
počítačová	počítačový	k2eAgFnSc1d1	počítačová
vědkyně	vědkyně	k1gFnSc1	vědkyně
a	a	k8xC	a
úřednice	úřednice	k1gFnSc1	úřednice
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
</s>
</p>
<p>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Hughes	Hughesa	k1gFnPc2	Hughesa
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
</s>
</p>
<p>
<s>
Dick	Dick	k1gInSc1	Dick
Cheney	Chenea	k1gFnSc2	Chenea
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
46	[number]	k4	46
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Ives	Ivesa	k1gFnPc2	Ivesa
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Elia	Elia	k1gMnSc1	Elia
Kazan	Kazan	k1gMnSc1	Kazan
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Krugman	Krugman	k1gMnSc1	Krugman
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Orlando	Orlanda	k1gFnSc5	Orlanda
Lawrence	Lawrence	k1gFnSc5	Lawrence
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
Joshua	Joshua	k1gMnSc1	Joshua
Lederberg	Lederberg	k1gMnSc1	Lederberg
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1958	[number]	k4	1958
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Morris	Morris	k1gFnSc2	Morris
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Sinclair	Sinclair	k1gInSc1	Sinclair
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
1930	[number]	k4	1930
</s>
</p>
<p>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littell	Littell	k1gMnSc1	Littell
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Gary	Gar	k2eAgInPc1d1	Gar
Locke	Lock	k1gInPc1	Lock
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Alvin	Alvin	k1gMnSc1	Alvin
Lucier	Lucier	k1gMnSc1	Lucier
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
experimentátor	experimentátor	k1gMnSc1	experimentátor
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
B.	B.	kA	B.
MacCready	MacCreada	k1gFnPc1	MacCreada
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
letecký	letecký	k2eAgMnSc1d1	letecký
inženýr	inženýr	k1gMnSc1	inženýr
</s>
</p>
<p>
<s>
Frances	Frances	k1gInSc1	Frances
McDormand	McDormand	k1gInSc1	McDormand
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Jordan	Jordan	k1gMnSc1	Jordan
Mechner	Mechner	k1gMnSc1	Mechner
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
herní	herní	k2eAgMnSc1d1	herní
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
F.	F.	kA	F.
B.	B.	kA	B.
Morse	Morse	k1gMnSc1	Morse
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
morseovy	morseův	k2eAgFnSc2d1	morseova
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
portrétů	portrét	k1gInPc2	portrét
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
scén	scéna	k1gFnPc2	scéna
</s>
</p>
<p>
<s>
Clarence	Clarenka	k1gFnSc3	Clarenka
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Newman	Newman	k1gMnSc1	Newman
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
Nivola	Nivola	k1gFnSc1	Nivola
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Lewis	Lewis	k1gFnSc3	Lewis
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Norton	Norton	k1gInSc1	Norton
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
Nyquist	Nyquist	k1gInSc1	Nyquist
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
informatik	informatik	k1gMnSc1	informatik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
švédského	švédský	k2eAgInSc2d1	švédský
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Lars	Lars	k6eAd1	Lars
Onsager	Onsager	k1gMnSc1	Onsager
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Ousterhout	Ousterhout	k1gMnSc1	Ousterhout
<g/>
,	,	kIx,	,
původní	původní	k2eAgMnSc1d1	původní
autor	autor	k1gMnSc1	autor
skriptovacího	skriptovací	k2eAgInSc2d1	skriptovací
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Tcl	Tcl	k1gFnSc2	Tcl
a	a	k8xC	a
platformně	platformně	k6eAd1	platformně
nezávislého	závislý	k2eNgInSc2d1	nezávislý
grafického	grafický	k2eAgInSc2d1	grafický
toolkitu	toolkit	k1gInSc2	toolkit
Tk	Tk	k1gFnSc2	Tk
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Pataki	Patak	k1gFnSc2	Patak
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
guvernérem	guvernér	k1gMnSc7	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
S.	S.	kA	S.
Phelps	Phelps	k1gInSc1	Phelps
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Bronson	Bronson	k1gMnSc1	Bronson
Pinchot	Pinchot	k1gMnSc1	Pinchot
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Cole	cola	k1gFnSc3	cola
Porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
</s>
</p>
<p>
<s>
Pras	prasit	k5eAaImRp2nS	prasit
<g/>
,	,	kIx,	,
haitsko-americký	haitskomerický	k2eAgMnSc1d1	haitsko-americký
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
</s>
</p>
<p>
<s>
Dickinson	Dickinson	k1gInSc1	Dickinson
W.	W.	kA	W.
Richards	Richards	k1gInSc1	Richards
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Mike	Mike	k6eAd1	Mike
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
L.	L.	kA	L.
Rivest	Rivest	k1gMnSc1	Rivest
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kryptografie	kryptografie	k1gFnSc2	kryptografie
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Rothko	Rothko	k1gNnSc1	Rothko
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
malíř	malíř	k1gMnSc1	malíř
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Serra	Serra	k1gMnSc1	Serra
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
umělec	umělec	k1gMnSc1	umělec
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Sargent	Sargent	k1gMnSc1	Sargent
Shriver	Shriver	k1gMnSc1	Shriver
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Spock	Spock	k1gMnSc1	Spock
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
pediatr	pediatr	k1gMnSc1	pediatr
</s>
</p>
<p>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Meryl	Meryl	k1gInSc1	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Howard	Howarda	k1gFnPc2	Howarda
Taft	taft	k1gInSc1	taft
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Vickrey	Vickrea	k1gFnSc2	Vickrea
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
Švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
nástupkyně	nástupkyně	k1gFnSc1	nástupkyně
švédského	švédský	k2eAgInSc2d1	švédský
trůnu	trůn	k1gInSc2	trůn
–	–	k?	–
švédská	švédský	k2eAgFnSc1d1	švédská
korunní	korunní	k2eAgFnSc1d1	korunní
princezna	princezna	k1gFnSc1	princezna
</s>
</p>
<p>
<s>
Sigourney	Sigournea	k1gFnPc1	Sigournea
Weaver	Weavra	k1gFnPc2	Weavra
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Noah	Noah	k1gMnSc1	Noah
Webster	Webster	k1gMnSc1	Webster
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
lexikograf	lexikograf	k1gMnSc1	lexikograf
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
reformátor	reformátor	k1gMnSc1	reformátor
anglického	anglický	k2eAgInSc2d1	anglický
pravopisu	pravopis	k1gInSc2	pravopis
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Hoyt	Hoyt	k1gMnSc1	Hoyt
Whipple	Whipple	k1gMnSc1	Whipple
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1934	[number]	k4	1934
</s>
</p>
<p>
<s>
Eric	Eric	k1gInSc1	Eric
F.	F.	kA	F.
Wieschaus	Wieschaus	k1gInSc1	Wieschaus
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Thornton	Thornton	k1gInSc1	Thornton
Wilder	Wilder	k1gInSc1	Wilder
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
nositel	nositel	k1gMnSc1	nositel
Pulitzerovy	Pulitzerův	k2eAgFnSc2d1	Pulitzerova
ceny	cena	k1gFnSc2	cena
</s>
</p>
<p>
<s>
Naomi	Nao	k1gFnPc7	Nao
Wolfová	Wolfová	k1gFnSc1	Wolfová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
intelektuálka	intelektuálka	k1gFnSc1	intelektuálka
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
konzultantka	konzultantka	k1gFnSc1	konzultantka
</s>
</p>
<p>
<s>
Bob	Bob	k1gMnSc1	Bob
Woodward	Woodward	k1gMnSc1	Woodward
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Fareed	Fareed	k1gInSc1	Fareed
Zakaria	Zakarium	k1gNnSc2	Zakarium
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
žurnalista	žurnalista	k1gMnSc1	žurnalista
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
</s>
</p>
<p>
<s>
===	===	k?	===
Profesoři	profesor	k1gMnPc5	profesor
===	===	k?	===
</s>
</p>
<p>
<s>
Sidney	Sidnea	k1gFnPc1	Sidnea
Altman	Altman	k1gMnSc1	Altman
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Harry	Harr	k1gInPc1	Harr
J.	J.	kA	J.
Benda	Benda	k1gMnSc1	Benda
<g/>
,	,	kIx,	,
indonesista	indonesista	k1gMnSc1	indonesista
československého	československý	k2eAgInSc2d1	československý
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
na	na	k7c4	na
Yaleu	Yalea	k1gFnSc4	Yalea
v	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Gregory	Gregor	k1gMnPc4	Gregor
Crewdson	Crewdson	k1gInSc4	Crewdson
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Gérard	Gérard	k1gMnSc1	Gérard
Debreu	Debreus	k1gInSc2	Debreus
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Derrida	Derrida	k1gFnSc1	Derrida
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
dekonstrukce	dekonstrukce	k1gFnSc2	dekonstrukce
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Gauthier	Gauthier	k1gMnSc1	Gauthier
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
obratlovci	obratlovec	k1gMnPc7	obratlovec
a	a	k8xC	a
systematik	systematik	k1gMnSc1	systematik
</s>
</p>
<p>
<s>
Tjalling	Tjalling	k1gInSc1	Tjalling
Koopmans	Koopmans	k1gInSc1	Koopmans
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
Wangari	Wangari	k6eAd1	Wangari
Maathai	Maathai	k1gNnSc1	Maathai
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Bronisław	Bronisław	k?	Bronisław
Malinowski	Malinowski	k1gNnSc1	Malinowski
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
etnograf	etnograf	k1gMnSc1	etnograf
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
de	de	k?	de
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
</s>
</p>
<p>
<s>
Benoît	Benoît	k1gMnSc1	Benoît
Mandelbrot	Mandelbrot	k1gMnSc1	Mandelbrot
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
fraktální	fraktální	k2eAgFnSc2d1	fraktální
geometrie	geometrie	k1gFnSc2	geometrie
</s>
</p>
<p>
<s>
Erwin	Erwin	k1gMnSc1	Erwin
Neher	Neher	k1gMnSc1	Neher
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Emil	Emil	k1gMnSc1	Emil
Palade	Palad	k1gInSc5	Palad
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Caryl	Caryl	k1gInSc1	Caryl
Phillips	Phillips	k1gInSc1	Phillips
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
britské	britský	k2eAgFnSc2d1	britská
literatury	literatura	k1gFnSc2	literatura
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
A.	A.	kA	A.
Steitz	Steitz	k1gMnSc1	Steitz
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Lawrie	Lawrie	k1gFnSc2	Lawrie
Tatum	Tatum	k?	Tatum
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Tobin	Tobin	k1gMnSc1	Tobin
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Yaleova	Yaleův	k2eAgFnSc1d1	Yaleova
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.yale.edu	www.yale.edu	k6eAd1	www.yale.edu
</s>
</p>
