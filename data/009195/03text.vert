<p>
<s>
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
opera	opera	k1gFnSc1	opera
in	in	k?	in
musica	musicus	k1gMnSc2	musicus
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgNnSc4d1	hudební
dílo	dílo	k1gNnSc4	dílo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělecká	umělecký	k2eAgFnSc1d1	umělecká
forma	forma	k1gFnSc1	forma
spojující	spojující	k2eAgFnSc4d1	spojující
dramatické	dramatický	k2eAgNnSc4d1	dramatické
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
herci	herec	k1gMnPc1	herec
text	text	k1gInSc1	text
(	(	kIx(	(
<g/>
libreto	libreto	k1gNnSc1	libreto
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
či	či	k8xC	či
zčásti	zčásti	k6eAd1	zčásti
zpívají	zpívat	k5eAaImIp3nP	zpívat
za	za	k7c2	za
instrumentálního	instrumentální	k2eAgInSc2d1	instrumentální
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
synteticky	synteticky	k6eAd1	synteticky
spojuje	spojovat	k5eAaImIp3nS	spojovat
více	hodně	k6eAd2	hodně
základních	základní	k2eAgFnPc2d1	základní
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
i	i	k8xC	i
vokální	vokální	k2eAgFnSc4d1	vokální
hudbu	hudba	k1gFnSc4	hudba
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
fixovaná	fixovaný	k2eAgFnSc1d1	fixovaná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
partitury	partitura	k1gFnSc2	partitura
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
provedení	provedení	k1gNnSc6	provedení
hudebníky	hudebník	k1gMnPc4	hudebník
–	–	k?	–
zpravidla	zpravidla	k6eAd1	zpravidla
orchestrem	orchestr	k1gInSc7	orchestr
–	–	k?	–
<g/>
,	,	kIx,	,
pěveckými	pěvecký	k2eAgMnPc7d1	pěvecký
sólisty	sólista	k1gMnPc7	sólista
a	a	k8xC	a
případně	případně	k6eAd1	případně
sborem	sborem	k6eAd1	sborem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
libreta	libreto	k1gNnSc2	libreto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
a	a	k8xC	a
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
též	též	k9	též
balet	balet	k1gInSc1	balet
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
výpravu	výprava	k1gFnSc4	výprava
(	(	kIx(	(
<g/>
scénografii	scénografie	k1gFnSc4	scénografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
masky	maska	k1gFnPc1	maska
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
světelné	světelný	k2eAgInPc1d1	světelný
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
jevištní	jevištní	k2eAgInPc1d1	jevištní
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
dalšími	další	k2eAgFnPc7d1	další
příbuznými	příbuzný	k2eAgFnPc7d1	příbuzná
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
formami	forma	k1gFnPc7	forma
je	být	k5eAaImIp3nS	být
shrnována	shrnován	k2eAgFnSc1d1	shrnován
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
hudební	hudební	k2eAgNnSc4d1	hudební
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
západní	západní	k2eAgFnSc2d1	západní
klasické	klasický	k2eAgFnSc2d1	klasická
umělecké	umělecký	k2eAgFnSc2d1	umělecká
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
rovněž	rovněž	k9	rovněž
tradiční	tradiční	k2eAgFnSc1d1	tradiční
čínská	čínský	k2eAgFnSc1d1	čínská
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
se	se	k3xPyFc4	se
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
opery	opera	k1gFnSc2	opera
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ztraceným	ztracený	k2eAgInSc7d1	ztracený
<g/>
,	,	kIx,	,
dílem	díl	k1gInSc7	díl
považovaným	považovaný	k2eAgInSc7d1	považovaný
za	za	k7c4	za
operu	opera	k1gFnSc4	opera
je	být	k5eAaImIp3nS	být
Dafne	Dafn	k1gInSc5	Dafn
skladatele	skladatel	k1gMnSc2	skladatel
Jacopa	Jacop	k1gMnSc4	Jacop
Periho	Peri	k1gMnSc4	Peri
<g/>
,	,	kIx,	,
uvedeným	uvedený	k2eAgMnSc7d1	uvedený
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
<g/>
;	;	kIx,	;
samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
díla	dílo	k1gNnSc2	dílo
Francesca	Francescus	k1gMnSc2	Francescus
Cavalliho	Cavalli	k1gMnSc2	Cavalli
Le	Le	k1gMnSc2	Le
nozze	nozze	k1gFnSc2	nozze
di	di	k?	di
Teti	teti	k1gFnSc2	teti
e	e	k0	e
di	di	k?	di
Peleo	Peleo	k1gMnSc1	Peleo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
hlavních	hlavní	k2eAgFnPc6d1	hlavní
zemích	zem	k1gFnPc6	zem
Západu	západ	k1gInSc2	západ
<g/>
:	:	kIx,	:
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schütz	Schütz	k1gMnSc1	Schütz
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lully	Lullo	k1gNnPc7	Lullo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
Purcell	Purcell	k1gMnSc1	Purcell
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
tehdy	tehdy	k6eAd1	tehdy
založili	založit	k5eAaPmAgMnP	založit
domácí	domácí	k2eAgFnPc4d1	domácí
operní	operní	k2eAgFnPc4d1	operní
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
silnou	silný	k2eAgFnSc4d1	silná
zejména	zejména	k9	zejména
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
operní	operní	k2eAgFnSc4d1	operní
školu	škola	k1gFnSc4	škola
však	však	k9	však
až	až	k6eAd1	až
dlouho	dlouho	k6eAd1	dlouho
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
vedoucí	vedoucí	k1gFnSc1	vedoucí
zemí	zem	k1gFnPc2	zem
opery	opera	k1gFnSc2	opera
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
vliv	vliv	k1gInSc1	vliv
německy	německy	k6eAd1	německy
hovořících	hovořící	k2eAgMnPc2d1	hovořící
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
:	:	kIx,	:
na	na	k7c4	na
Gluckovu	Gluckův	k2eAgFnSc4d1	Gluckova
reformu	reforma	k1gFnSc4	reforma
italské	italský	k2eAgInPc1d1	italský
"	"	kIx"	"
<g/>
opera	opera	k1gFnSc1	opera
seria	seria	k1gFnSc1	seria
<g/>
"	"	kIx"	"
z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
navázal	navázat	k5eAaPmAgMnS	navázat
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
operní	operní	k2eAgMnSc1d1	operní
skladatel	skladatel	k1gMnSc1	skladatel
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
přes	přes	k7c4	přes
200	[number]	k4	200
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
opeře	opera	k1gFnSc6	opera
první	první	k4xOgFnSc2	první
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvrcholil	vyvrcholit	k5eAaPmAgMnS	vyvrcholit
operní	operní	k2eAgInSc4d1	operní
styl	styl	k1gInSc4	styl
bel	bela	k1gFnPc2	bela
canto	canto	k1gNnSc1	canto
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc1d1	reprezentovaný
autory	autor	k1gMnPc7	autor
jako	jako	k8xS	jako
Gioacchino	Gioacchino	k1gNnSc4	Gioacchino
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
,	,	kIx,	,
Gaetano	Gaetana	k1gFnSc5	Gaetana
Donizetti	Donizett	k1gMnPc1	Donizett
a	a	k8xC	a
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tradice	tradice	k1gFnSc1	tradice
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc2d1	velká
opery	opera	k1gFnSc2	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
grand	grand	k1gMnSc1	grand
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc1d1	spojená
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Giacomo	Giacoma	k1gFnSc5	Giacoma
Meyerbeera	Meyerbeero	k1gNnPc1	Meyerbeero
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
"	"	kIx"	"
opery	opera	k1gFnSc2	opera
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
působili	působit	k5eAaImAgMnP	působit
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdi	Verd	k1gMnPc1	Verd
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
rovněž	rovněž	k9	rovněž
vznik	vznik	k1gInSc1	vznik
dalších	další	k2eAgFnPc2d1	další
národních	národní	k2eAgFnPc2d1	národní
operních	operní	k2eAgFnPc2d1	operní
tradic	tradice	k1gFnPc2	tradice
mimo	mimo	k7c4	mimo
klasické	klasický	k2eAgFnPc4d1	klasická
země	zem	k1gFnPc4	zem
opery	opera	k1gFnSc2	opera
<g/>
;	;	kIx,	;
českou	český	k2eAgFnSc4d1	Česká
operu	opera	k1gFnSc4	opera
tehdy	tehdy	k6eAd1	tehdy
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
především	především	k9	především
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
vliv	vliv	k1gInSc4	vliv
italského	italský	k2eAgInSc2d1	italský
verismu	verismus	k1gInSc2	verismus
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
nových	nový	k2eAgInPc2d1	nový
harmonických	harmonický	k2eAgInPc2d1	harmonický
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
iniciovaných	iniciovaný	k2eAgInPc2d1	iniciovaný
Wagnerem	Wagner	k1gMnSc7	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
operním	operní	k2eAgMnPc3d1	operní
skladatelům	skladatel	k1gMnPc3	skladatel
patřili	patřit	k5eAaImAgMnP	patřit
Giacomo	Giacoma	k1gFnSc5	Giacoma
Puccini	Puccin	k1gMnPc1	Puccin
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Strauss	Strauss	k1gInSc1	Strauss
a	a	k8xC	a
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přineslo	přinést	k5eAaPmAgNnS	přinést
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
experimentů	experiment	k1gInPc2	experiment
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
atonalitu	atonalita	k1gFnSc4	atonalita
a	a	k8xC	a
seriální	seriální	k2eAgFnSc4d1	seriální
hudbu	hudba	k1gFnSc4	hudba
(	(	kIx(	(
<g/>
Arnold	Arnold	k1gMnSc1	Arnold
Schönberg	Schönberg	k1gMnSc1	Schönberg
a	a	k8xC	a
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neoklasicismus	neoklasicismus	k1gInSc1	neoklasicismus
(	(	kIx(	(
<g/>
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
)	)	kIx)	)
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
minimalismus	minimalismus	k1gInSc1	minimalismus
(	(	kIx(	(
<g/>
Philip	Philip	k1gInSc1	Philip
Glass	Glassa	k1gFnPc2	Glassa
a	a	k8xC	a
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
prvenství	prvenství	k1gNnPc1	prvenství
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Díla	dílo	k1gNnPc1	dílo
významná	významný	k2eAgNnPc1d1	významné
jako	jako	k8xC	jako
mezníky	mezník	k1gInPc7	mezník
dějin	dějiny	k1gFnPc2	dějiny
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vymezení	vymezení	k1gNnSc6	vymezení
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgFnPc3d1	jiná
uměleckým	umělecký	k2eAgFnPc3d1	umělecká
formám	forma	k1gFnPc3	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Definice	definice	k1gFnSc1	definice
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
autorů	autor	k1gMnPc2	autor
často	často	k6eAd1	často
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jeana-Jacquese	Jeana-Jacquese	k1gFnSc2	Jeana-Jacquese
Rousseaua	Rousseau	k1gMnSc2	Rousseau
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dramatické	dramatický	k2eAgNnSc4d1	dramatické
a	a	k8xC	a
hudební	hudební	k2eAgNnSc4d1	hudební
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
usiluje	usilovat	k5eAaImIp3nS	usilovat
sjednotit	sjednotit	k5eAaPmF	sjednotit
všechna	všechen	k3xTgNnPc4	všechen
kouzla	kouzlo	k1gNnPc4	kouzlo
umění	umění	k1gNnSc2	umění
v	v	k7c4	v
předvedení	předvedení	k1gNnSc4	předvedení
vášnivého	vášnivý	k2eAgInSc2d1	vášnivý
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
příjemných	příjemný	k2eAgInPc2d1	příjemný
vjemů	vjem	k1gInPc2	vjem
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
iluzi	iluze	k1gFnSc4	iluze
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
hudebními	hudební	k2eAgInPc7d1	hudební
a	a	k8xC	a
divadelními	divadelní	k2eAgInPc7d1	divadelní
žánry	žánr	k1gInPc7	žánr
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
prostupné	prostupný	k2eAgFnPc1d1	prostupná
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
jako	jako	k8xC	jako
žánr	žánr	k1gInSc4	žánr
teprve	teprve	k6eAd1	teprve
formovala	formovat	k5eAaImAgFnS	formovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
nejnovější	nový	k2eAgFnSc4d3	nejnovější
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
hranice	hranice	k1gFnSc1	hranice
žánrů	žánr	k1gInPc2	žánr
cíleně	cíleně	k6eAd1	cíleně
překračuje	překračovat	k5eAaImIp3nS	překračovat
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
nutností	nutnost	k1gFnSc7	nutnost
scénického	scénický	k2eAgNnSc2d1	scénické
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
divadelních	divadelní	k2eAgInPc2d1	divadelní
žánrů	žánr	k1gInPc2	žánr
(	(	kIx(	(
<g/>
činohra	činohra	k1gFnSc1	činohra
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
instrumentální	instrumentální	k2eAgNnSc1d1	instrumentální
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
přednostní	přednostní	k2eAgMnSc1d1	přednostní
a	a	k8xC	a
integrální	integrální	k2eAgFnSc7d1	integrální
úlohou	úloha	k1gFnSc7	úloha
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
další	další	k2eAgInPc4d1	další
určující	určující	k2eAgInPc4d1	určující
znaky	znak	k1gInPc4	znak
opery	opera	k1gFnSc2	opera
bývají	bývat	k5eAaImIp3nP	bývat
považovány	považován	k2eAgInPc1d1	považován
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Fixovanost	Fixovanost	k1gFnSc1	Fixovanost
textové	textový	k2eAgFnSc2d1	textová
i	i	k8xC	i
hudební	hudební	k2eAgFnSc2d1	hudební
složky	složka	k1gFnSc2	složka
v	v	k7c6	v
libretu	libreto	k1gNnSc6	libreto
a	a	k8xC	a
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
některá	některý	k3yIgNnPc4	některý
klasická	klasický	k2eAgNnPc4d1	klasické
i	i	k8xC	i
moderní	moderní	k2eAgNnPc4d1	moderní
díla	dílo	k1gNnPc4	dílo
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
jisté	jistý	k2eAgNnSc4d1	jisté
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
improvizaci	improvizace	k1gFnSc4	improvizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původnost	původnost	k1gFnSc1	původnost
hudební	hudební	k2eAgFnSc2d1	hudební
složky	složka	k1gFnSc2	složka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
opery	opera	k1gFnSc2	opera
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
citace	citace	k1gFnPc1	citace
jiných	jiný	k2eAgNnPc2d1	jiné
děl	dělo	k1gNnPc2	dělo
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
stejného	stejné	k1gNnSc2	stejné
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
autocitaci	autocitace	k1gFnSc6	autocitace
–	–	k?	–
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převažuje	převažovat	k5eAaImIp3nS	převažovat
<g/>
-li	i	k?	-li
hudba	hudba	k1gFnSc1	hudba
nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
<g/>
,	,	kIx,	,
nepovažuje	považovat	k5eNaImIp3nS	považovat
se	se	k3xPyFc4	se
dané	daný	k2eAgNnSc1d1	dané
dílo	dílo	k1gNnSc1	dílo
zpravidla	zpravidla	k6eAd1	zpravidla
za	za	k7c4	za
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
byly	být	k5eAaImAgInP	být
rozšířeny	rozšířen	k2eAgInPc1d1	rozšířen
žánry	žánr	k1gInPc1	žánr
(	(	kIx(	(
<g/>
pasticcio	pasticcio	k1gNnSc1	pasticcio
<g/>
,	,	kIx,	,
vaudeville	vaudeville	k1gInSc1	vaudeville
<g/>
,	,	kIx,	,
ballad	ballad	k1gInSc1	ballad
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
spojující	spojující	k2eAgInSc1d1	spojující
původní	původní	k2eAgInSc1d1	původní
dramatický	dramatický	k2eAgInSc1d1	dramatický
text	text	k1gInSc1	text
s	s	k7c7	s
nepůvodní	původní	k2eNgFnSc7d1	nepůvodní
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Pasticcia	Pasticcia	k1gFnSc1	Pasticcia
barokních	barokní	k2eAgFnPc2d1	barokní
oper	opera	k1gFnPc2	opera
bývají	bývat	k5eAaImIp3nP	bývat
uváděna	uváděn	k2eAgNnPc1d1	uváděno
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
např.	např.	kA	např.
The	The	k1gFnSc1	The
Enchanted	Enchanted	k1gMnSc1	Enchanted
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
MET	met	k1gInSc1	met
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pasticcia	Pasticcium	k1gNnPc1	Pasticcium
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
též	též	k9	též
v	v	k7c6	v
operetě	opereta	k1gFnSc6	opereta
(	(	kIx(	(
<g/>
Schubert-Berté	Schubert-Berta	k1gMnPc1	Schubert-Berta
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
u	u	k7c2	u
tří	tři	k4xCgNnPc2	tři
děvčátek	děvčátko	k1gNnPc2	děvčátko
<g/>
)	)	kIx)	)
a	a	k8xC	a
muzikálu	muzikál	k1gInSc2	muzikál
(	(	kIx(	(
<g/>
Rebelové	rebel	k1gMnPc1	rebel
<g/>
,	,	kIx,	,
Mamma	Mamma	k1gNnSc1	Mamma
Mia	Mia	k1gFnPc2	Mia
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
hudbě	hudba	k1gFnSc3	hudba
text	text	k1gInSc4	text
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
libreto	libreto	k1gNnSc1	libreto
<g/>
)	)	kIx)	)
originální	originální	k2eAgNnSc1d1	originální
být	být	k5eAaImF	být
nemusí	muset	k5eNaImIp3nS	muset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východisko	východisko	k1gNnSc1	východisko
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
tradice	tradice	k1gFnSc2	tradice
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
západní	západní	k2eAgMnPc1d1	západní
skladatelé	skladatel	k1gMnPc1	skladatel
se	se	k3xPyFc4	se
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
např.	např.	kA	např.
japonským	japonský	k2eAgMnPc3d1	japonský
divadlem	divadlo	k1gNnSc7	divadlo
nó	nó	k0	nó
nebo	nebo	k8xC	nebo
čínskou	čínský	k2eAgFnSc7d1	čínská
operou	opera	k1gFnSc7	opera
(	(	kIx(	(
<g/>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Britten	Britten	k2eAgMnSc1d1	Britten
<g/>
:	:	kIx,	:
Tři	tři	k4xCgNnPc1	tři
chrámová	chrámový	k2eAgNnPc1d1	chrámové
podobenství	podobenství	k1gNnPc1	podobenství
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
<g/>
:	:	kIx,	:
Nixon	Nixon	k1gMnSc1	Nixon
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Judith	Judith	k1gMnSc1	Judith
Weir	Weir	k1gMnSc1	Weir
<g/>
:	:	kIx,	:
Noc	noc	k1gFnSc1	noc
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
opeře	opera	k1gFnSc6	opera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
východoasijské	východoasijský	k2eAgMnPc4d1	východoasijský
operní	operní	k2eAgMnPc4d1	operní
skladatele	skladatel	k1gMnPc4	skladatel
jako	jako	k8xS	jako
např.	např.	kA	např.
Ikuma	Ikuma	k1gFnSc1	Ikuma
Dan	Dan	k1gMnSc1	Dan
nebo	nebo	k8xC	nebo
Tan	Tan	k1gMnSc1	Tan
Dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
platnost	platnost	k1gFnSc4	platnost
tohoto	tento	k3xDgNnSc2	tento
kritéria	kritérion	k1gNnSc2	kritérion
se	se	k3xPyFc4	se
však	však	k9	však
tradiční	tradiční	k2eAgNnSc4d1	tradiční
japonské	japonský	k2eAgNnSc4d1	Japonské
nebo	nebo	k8xC	nebo
čínské	čínský	k2eAgNnSc4d1	čínské
hudební	hudební	k2eAgNnSc4d1	hudební
divadlo	divadlo	k1gNnSc4	divadlo
od	od	k7c2	od
opery	opera	k1gFnSc2	opera
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
neexistencí	neexistence	k1gFnSc7	neexistence
definitivního	definitivní	k2eAgInSc2d1	definitivní
zápisu	zápis	k1gInSc2	zápis
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
žánry	žánr	k1gInPc1	žánr
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nesplňují	splňovat	k5eNaImIp3nP	splňovat
určité	určitý	k2eAgInPc4d1	určitý
základní	základní	k2eAgInPc4d1	základní
definiční	definiční	k2eAgInPc4d1	definiční
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
zejména	zejména	k9	zejména
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
televizní	televizní	k2eAgFnSc4d1	televizní
operu	opera	k1gFnSc4	opera
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
žánry	žánr	k1gInPc4	žánr
divadelní	divadelní	k2eAgNnSc1d1	divadelní
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
loutkové	loutkový	k2eAgFnSc2d1	loutková
opery	opera	k1gFnSc2	opera
zase	zase	k9	zase
není	být	k5eNaImIp3nS	být
herec	herec	k1gMnSc1	herec
totožný	totožný	k2eAgMnSc1d1	totožný
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
drama	drama	k1gNnSc1	drama
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
hudební	hudební	k2eAgFnSc4d1	hudební
(	(	kIx(	(
<g/>
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
či	či	k8xC	či
vokální	vokální	k2eAgFnSc4d1	vokální
<g/>
)	)	kIx)	)
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
žánrové	žánrový	k2eAgNnSc4d1	žánrové
zařazení	zařazení	k1gNnSc4	zařazení
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
drama	drama	k1gNnSc4	drama
určující	určující	k2eAgNnSc4d1	určující
hudební	hudební	k2eAgInSc4d1	hudební
(	(	kIx(	(
<g/>
vokální	vokální	k2eAgInSc4d1	vokální
<g/>
)	)	kIx)	)
složka	složka	k1gFnSc1	složka
či	či	k8xC	či
zda	zda	k8xS	zda
převažuje	převažovat	k5eAaImIp3nS	převažovat
složka	složka	k1gFnSc1	složka
činoherní	činoherní	k2eAgFnSc1d1	činoherní
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
době	doba	k1gFnSc6	doba
existovaly	existovat	k5eAaImAgInP	existovat
přechodné	přechodný	k2eAgInPc1d1	přechodný
žánry	žánr	k1gInPc1	žánr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
anglická	anglický	k2eAgFnSc1d1	anglická
semiopera	semiopera	k1gFnSc1	semiopera
nebo	nebo	k8xC	nebo
maska	maska	k1gFnSc1	maska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
divadelním	divadelní	k2eAgInSc7d1	divadelní
žánrem	žánr	k1gInSc7	žánr
singspiel	singspiel	k1gInSc1	singspiel
–	–	k?	–
hra	hra	k1gFnSc1	hra
se	s	k7c7	s
zpěvy	zpěv	k1gInPc7	zpěv
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
kvantitativním	kvantitativní	k2eAgInSc7d1	kvantitativní
a	a	k8xC	a
kvalitativním	kvalitativní	k2eAgInSc7d1	kvalitativní
podílem	podíl	k1gInSc7	podíl
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Rozřazení	rozřazení	k1gNnSc1	rozřazení
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
děl	dělo	k1gNnPc2	dělo
mezi	mezi	k7c4	mezi
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
činohru	činohra	k1gFnSc4	činohra
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
obtížné	obtížný	k2eAgNnSc1d1	obtížné
(	(	kIx(	(
<g/>
dobová	dobový	k2eAgNnPc1d1	dobové
kritéria	kritérion	k1gNnPc1	kritérion
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
Mozartovy	Mozartův	k2eAgInPc1d1	Mozartův
singspiely	singspiel	k1gInPc1	singspiel
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c4	mezi
opery	opera	k1gFnPc4	opera
<g/>
,	,	kIx,	,
singspiely	singspiel	k1gInPc1	singspiel
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Raimunda	Raimunda	k1gFnSc1	Raimunda
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
např.	např.	kA	např.
Wenzela	Wenzela	k1gMnSc2	Wenzela
Müllera	Müller	k1gMnSc2	Müller
mezi	mezi	k7c4	mezi
činohry	činohra	k1gFnPc4	činohra
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
singspiel	singspiel	k1gInSc4	singspiel
Pražští	pražský	k2eAgMnPc1d1	pražský
sládci	sládek	k1gMnPc1	sládek
Prokopa	Prokop	k1gMnSc2	Prokop
Šedivého	Šedivý	k1gMnSc2	Šedivý
a	a	k8xC	a
Vincence	Vincenc	k1gMnSc2	Vincenc
Bartáka	Barták	k1gMnSc2	Barták
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
spíše	spíše	k9	spíše
činoherní	činoherní	k2eAgInSc4d1	činoherní
útvar	útvar	k1gInSc4	útvar
a	a	k8xC	a
za	za	k7c4	za
první	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
operu	opera	k1gFnSc4	opera
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Škroupův	Škroupův	k2eAgInSc1d1	Škroupův
singspiel	singspiel	k1gInSc1	singspiel
Dráteník	dráteník	k1gMnSc1	dráteník
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
dramatu	drama	k1gNnSc2	drama
stojí	stát	k5eAaImIp3nS	stát
řada	řada	k1gFnSc1	řada
moderních	moderní	k2eAgNnPc2d1	moderní
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
Kurta	Kurt	k1gMnSc2	Kurt
Weilla	Weill	k1gMnSc2	Weill
(	(	kIx(	(
<g/>
Krejcarová	krejcarový	k2eAgFnSc1d1	Krejcarová
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
pád	pád	k1gInSc1	pád
města	město	k1gNnSc2	město
Mahagonny	Mahagonna	k1gFnSc2	Mahagonna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dramatu	drama	k1gNnSc2	drama
plně	plně	k6eAd1	plně
podřízená	podřízený	k2eAgFnSc1d1	podřízená
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
taneční	taneční	k2eAgNnSc1d1	taneční
divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Průnik	průnik	k1gInSc1	průnik
opery	opera	k1gFnSc2	opera
s	s	k7c7	s
tanečním	taneční	k2eAgNnSc7d1	taneční
divadlem	divadlo	k1gNnSc7	divadlo
resp.	resp.	kA	resp.
baletem	balet	k1gInSc7	balet
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
již	již	k6eAd1	již
od	od	k7c2	od
barokní	barokní	k2eAgFnSc2d1	barokní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
žánru	žánr	k1gInSc6	žánr
opéra-ballet	opéraallet	k1gInSc1	opéra-ballet
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
např.	např.	kA	např.
v	v	k7c6	v
romantické	romantický	k2eAgFnSc6d1	romantická
ruské	ruský	k2eAgFnSc6d1	ruská
hudbě	hudba	k1gFnSc6	hudba
(	(	kIx(	(
<g/>
Rimskij-Korsakov	Rimskij-Korsakov	k1gInSc1	Rimskij-Korsakov
<g/>
:	:	kIx,	:
Mlada	Mlada	k1gFnSc1	Mlada
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Igora	Igor	k1gMnSc2	Igor
Stravinského	Stravinský	k2eAgMnSc2d1	Stravinský
<g/>
,	,	kIx,	,
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Martinů	Martinů	k1gMnSc2	Martinů
nebo	nebo	k8xC	nebo
Borise	Boris	k1gMnSc2	Boris
Blachera	Blacher	k1gMnSc2	Blacher
(	(	kIx(	(
<g/>
Pruská	pruský	k2eAgFnSc1d1	pruská
pohádka	pohádka	k1gFnSc1	pohádka
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
ryzímu	ryzí	k2eAgInSc3d1	ryzí
baletu	balet	k1gInSc3	balet
<g/>
)	)	kIx)	)
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zpívané	zpívaný	k2eAgFnPc1d1	zpívaná
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dění	dění	k1gNnSc1	dění
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
dominantně	dominantně	k6eAd1	dominantně
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
řada	řada	k1gFnSc1	řada
klasických	klasický	k2eAgFnPc2d1	klasická
oper	opera	k1gFnPc2	opera
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
baletní	baletní	k2eAgFnPc4d1	baletní
scény	scéna	k1gFnPc4	scéna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
podstatné	podstatný	k2eAgInPc1d1	podstatný
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pantomima	pantomima	k1gFnSc1	pantomima
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Fenella	Fenella	k1gFnSc1	Fenella
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnSc1d1	titulní
postava	postava	k1gFnSc1	postava
Auberovy	Auberův	k2eAgFnSc2d1	Auberův
Němé	němý	k2eAgFnSc2d1	němá
z	z	k7c2	z
Portici	Portice	k1gFnSc6	Portice
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tanečně-pantomimická	tanečněantomimický	k2eAgFnSc1d1	tanečně-pantomimický
role	role	k1gFnSc1	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
oratorium	oratorium	k1gNnSc1	oratorium
===	===	k?	===
</s>
</p>
<p>
<s>
Opeře	opera	k1gFnSc3	opera
jsou	být	k5eAaImIp3nP	být
blízké	blízký	k2eAgFnPc1d1	blízká
některé	některý	k3yIgFnPc1	některý
vokálně-instrumentální	vokálněnstrumentální	k2eAgFnPc1d1	vokálně-instrumentální
hudební	hudební	k2eAgFnPc1d1	hudební
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
primárně	primárně	k6eAd1	primárně
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zejména	zejména	k9	zejména
oratorium	oratorium	k1gNnSc1	oratorium
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
podobností	podobnost	k1gFnPc2	podobnost
s	s	k7c7	s
operou	opera	k1gFnSc7	opera
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
společné	společný	k2eAgInPc1d1	společný
předky	předek	k1gInPc1	předek
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
souběžný	souběžný	k2eAgInSc4d1	souběžný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
navíc	navíc	k6eAd1	navíc
mnohá	mnohý	k2eAgNnPc4d1	mnohé
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
uváděla	uvádět	k5eAaImAgFnS	uvádět
scénická	scénický	k2eAgNnPc1d1	scénické
oratoria	oratorium	k1gNnPc4	oratorium
namísto	namísto	k7c2	namísto
oper	opera	k1gFnPc2	opera
o	o	k7c6	o
církevních	církevní	k2eAgInPc6d1	církevní
svátcích	svátek	k1gInPc6	svátek
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
hrát	hrát	k5eAaImF	hrát
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
neepická	epický	k2eNgNnPc1d1	epický
oratoria	oratorium	k1gNnPc1	oratorium
(	(	kIx(	(
<g/>
Verdiho	Verdi	k1gMnSc2	Verdi
Rekviem	rekviem	k1gNnSc4	rekviem
<g/>
,	,	kIx,	,
Händlův	Händlův	k2eAgMnSc1d1	Händlův
Mesiáš	Mesiáš	k1gMnSc1	Mesiáš
<g/>
)	)	kIx)	)
uváděna	uvádět	k5eAaImNgFnS	uvádět
scénicky	scénicky	k6eAd1	scénicky
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
pak	pak	k6eAd1	pak
oratoria	oratorium	k1gNnSc2	oratorium
vypravující	vypravující	k2eAgInSc4d1	vypravující
souvislý	souvislý	k2eAgInSc4d1	souvislý
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
starozákonní	starozákonní	k2eAgInSc4d1	starozákonní
příběh	příběh	k1gInSc4	příběh
či	či	k8xC	či
epizody	epizoda	k1gFnPc4	epizoda
ze	z	k7c2	z
života	život	k1gInSc2	život
světců	světec	k1gMnPc2	světec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Händelovy	Händelův	k2eAgFnPc1d1	Händelova
Susanna	Susanna	k1gFnSc1	Susanna
a	a	k8xC	a
Theodora	Theodora	k1gFnSc1	Theodora
<g/>
,	,	kIx,	,
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
Svatá	svatý	k2eAgFnSc1d1	svatá
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
,	,	kIx,	,
Foersterův	Foersterův	k2eAgMnSc1d1	Foersterův
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
oratoria	oratorium	k1gNnSc2	oratorium
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
žánrovým	žánrový	k2eAgNnSc7d1	žánrové
označením	označení	k1gNnSc7	označení
(	(	kIx(	(
<g/>
duchovní	duchovní	k2eAgFnSc1d1	duchovní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
posvátné	posvátný	k2eAgNnSc1d1	posvátné
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
rappresentazione	rappresentazion	k1gInSc5	rappresentazion
sacra	sacra	k1gFnSc1	sacra
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
sacra	sacra	k1gFnSc1	sacra
<g/>
,	,	kIx,	,
biblická	biblický	k2eAgFnSc1d1	biblická
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
opera-oratorium	operaratorium	k1gNnSc1	opera-oratorium
<g/>
,	,	kIx,	,
operatorium	operatorium	k1gNnSc1	operatorium
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Emilio	Emilio	k1gMnSc1	Emilio
de	de	k?	de
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
Cavalieri	Cavalieri	k1gNnSc1	Cavalieri
<g/>
:	:	kIx,	:
Rappresentazione	Rappresentazion	k1gInSc5	Rappresentazion
di	di	k?	di
Anima	animo	k1gNnSc2	animo
<g/>
,	,	kIx,	,
e	e	k0	e
di	di	k?	di
Corpo	Corpa	k1gFnSc5	Corpa
(	(	kIx(	(
<g/>
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stefano	Stefana	k1gFnSc5	Stefana
Landi	Land	k1gMnPc1	Land
<g/>
:	:	kIx,	:
Il	Il	k1gMnSc1	Il
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Alessio	Alessio	k1gMnSc1	Alessio
(	(	kIx(	(
<g/>
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Étienne-Nicolas	Étienne-Nicolas	k1gInSc1	Étienne-Nicolas
Méhul	Méhul	k1gInSc1	Méhul
<g/>
:	:	kIx,	:
Joseph	Joseph	k1gInSc1	Joseph
(	(	kIx(	(
<g/>
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gioacchino	Gioacchino	k1gNnSc1	Gioacchino
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Mosé	Mosá	k1gFnSc3	Mosá
in	in	k?	in
Egitto	Egitto	k1gNnSc4	Egitto
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jules	Jules	k1gMnSc1	Jules
Massenet	Massenet	k1gMnSc1	Massenet
<g/>
:	:	kIx,	:
Marie-Magdeleine	Marie-Magdelein	k1gInSc5	Marie-Magdelein
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Nielsen	Nielsen	k2eAgMnSc1d1	Nielsen
<g/>
:	:	kIx,	:
Saul	Saul	k1gMnSc1	Saul
og	og	k?	og
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
Debussy	Debuss	k1gInPc1	Debuss
<g/>
:	:	kIx,	:
Le	Le	k1gMnSc5	Le
Martyre	martyr	k1gMnSc5	martyr
de	de	k?	de
Saint	Sainto	k1gNnPc2	Sainto
Sébastien	Sébastien	k1gInSc1	Sébastien
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
<g/>
:	:	kIx,	:
Moses	Moses	k1gMnSc1	Moses
und	und	k?	und
Aron	Aron	k1gMnSc1	Aron
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Darius	Darius	k1gMnSc1	Darius
Milhaud	Milhaud	k1gMnSc1	Milhaud
<g/>
:	:	kIx,	:
Saint	Saint	k1gMnSc1	Saint
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
roi	roi	k?	roi
de	de	k?	de
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olivier	Olivier	k1gMnSc1	Olivier
Messiaen	Messiaen	k1gInSc1	Messiaen
<g/>
:	:	kIx,	:
Saint	Saint	k1gInSc1	Saint
François	François	k1gFnSc2	François
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Assise	Assise	k1gFnSc1	Assise
<g/>
,	,	kIx,	,
Pascal	pascal	k1gInSc1	pascal
Dusapin	Dusapin	k1gInSc1	Dusapin
<g/>
:	:	kIx,	:
Melancholia	Melancholia	k1gFnSc1	Melancholia
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
opereta	opereta	k1gFnSc1	opereta
<g/>
/	/	kIx~	/
<g/>
muzikál	muzikál	k1gInSc1	muzikál
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgNnPc2d1	uvedené
rozhraničení	rozhraničení	k1gNnPc2	rozhraničení
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
operou	opera	k1gFnSc7	opera
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
operetou	opereta	k1gFnSc7	opereta
či	či	k8xC	či
muzikálem	muzikál	k1gInSc7	muzikál
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
otázkou	otázka	k1gFnSc7	otázka
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Opereta	opereta	k1gFnSc1	opereta
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
(	(	kIx(	(
<g/>
komické	komický	k2eAgFnSc2d1	komická
<g/>
)	)	kIx)	)
opery	opera	k1gFnSc2	opera
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
formovat	formovat	k5eAaImF	formovat
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
vážnou	vážný	k2eAgFnSc7d1	vážná
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
populární	populární	k2eAgFnSc7d1	populární
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Muzikál	muzikál	k1gInSc1	muzikál
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
anglosaském	anglosaský	k2eAgNnSc6d1	anglosaské
prostředí	prostředí	k1gNnSc6	prostředí
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
operety	opereta	k1gFnSc2	opereta
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
integrací	integrace	k1gFnSc7	integrace
neevropských	evropský	k2eNgInPc2d1	neevropský
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
hudbou	hudba	k1gFnSc7	hudba
populární	populární	k2eAgFnSc7d1	populární
a	a	k8xC	a
vážnou	vážná	k1gFnSc7	vážná
však	však	k9	však
podléhá	podléhat	k5eAaImIp3nS	podléhat
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
jak	jak	k6eAd1	jak
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
samotnými	samotný	k2eAgMnPc7d1	samotný
tvůrci	tvůrce	k1gMnPc7	tvůrce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
muzikálovým	muzikálový	k2eAgInPc3d1	muzikálový
prostředkům	prostředek	k1gInPc3	prostředek
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
současní	současný	k2eAgMnPc1d1	současný
autoři	autor	k1gMnPc1	autor
muzikál	muzikál	k1gInSc4	muzikál
či	či	k8xC	či
"	"	kIx"	"
<g/>
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
<g/>
"	"	kIx"	"
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
operetu	opereta	k1gFnSc4	opereta
od	od	k7c2	od
opery	opera	k1gFnSc2	opera
přísně	přísně	k6eAd1	přísně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
je	on	k3xPp3gNnSc4	on
naopak	naopak	k6eAd1	naopak
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
žánry	žánr	k1gInPc4	žánr
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
splývající	splývající	k2eAgFnSc6d1	splývající
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
operu	opera	k1gFnSc4	opera
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
plné	plný	k2eAgNnSc1d1	plné
zhudebnění	zhudebnění	k1gNnSc1	zhudebnění
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
operetu	opereta	k1gFnSc4	opereta
a	a	k8xC	a
muzikál	muzikál	k1gInSc4	muzikál
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
střídání	střídání	k1gNnSc1	střídání
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
žádné	žádný	k3yNgFnSc2	žádný
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
určující	určující	k2eAgFnPc1d1	určující
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
hudební	hudební	k2eAgFnSc4d1	hudební
náročnost	náročnost	k1gFnSc4	náročnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
kvalita	kvalita	k1gFnSc1	kvalita
hudby	hudba	k1gFnSc2	hudba
nejsou	být	k5eNaImIp3nP	být
dělícím	dělící	k2eAgNnSc7d1	dělící
měřítkem	měřítko	k1gNnSc7	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
(	(	kIx(	(
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
)	)	kIx)	)
opereta	opereta	k1gFnSc1	opereta
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
muzikálu	muzikál	k1gInSc2	muzikál
zásadně	zásadně	k6eAd1	zásadně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
technikou	technika	k1gFnSc7	technika
zpěvu	zpěv	k1gInSc2	zpěv
účinkujících	účinkující	k1gFnPc2	účinkující
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInPc1	jejich
hlasy	hlas	k1gInPc1	hlas
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
nikdy	nikdy	k6eAd1	nikdy
přímo	přímo	k6eAd1	přímo
elektronicky	elektronicky	k6eAd1	elektronicky
zesilovány	zesilován	k2eAgFnPc1d1	zesilována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Forma	forma	k1gFnSc1	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spoluurčují	spoluurčovat	k5eAaImIp3nP	spoluurčovat
jednak	jednak	k8xC	jednak
dobové	dobový	k2eAgFnPc1d1	dobová
žánrové	žánrový	k2eAgFnPc1d1	žánrová
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
konvence	konvence	k1gFnPc1	konvence
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
individuální	individuální	k2eAgNnSc1d1	individuální
řešení	řešení	k1gNnSc1	řešení
a	a	k8xC	a
styl	styl	k1gInSc1	styl
skladatele	skladatel	k1gMnSc2	skladatel
či	či	k8xC	či
libretisty	libretista	k1gMnSc2	libretista
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
samotný	samotný	k2eAgInSc1d1	samotný
námět	námět	k1gInSc1	námět
daného	daný	k2eAgNnSc2d1	dané
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
všeobecné	všeobecný	k2eAgInPc4d1	všeobecný
vzorce	vzorec	k1gInPc4	vzorec
její	její	k3xOp3gFnSc2	její
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
opery	opera	k1gFnPc1	opera
podle	podle	k7c2	podle
struktury	struktura	k1gFnSc2	struktura
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
opery	opera	k1gFnPc4	opera
číslové	číslový	k2eAgFnSc2d1	číslová
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
proud	proud	k1gInSc1	proud
členěn	členit	k5eAaImNgInS	členit
na	na	k7c4	na
zřetelně	zřetelně	k6eAd1	zřetelně
oddělené	oddělený	k2eAgInPc4d1	oddělený
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
opery	opera	k1gFnPc4	opera
prokomponované	prokomponovaný	k2eAgFnSc2d1	prokomponovaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
hudební	hudební	k2eAgInPc1d1	hudební
proud	proud	k1gInSc1	proud
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
bez	bez	k7c2	bez
systematických	systematický	k2eAgNnPc2d1	systematické
přerušení	přerušení	k1gNnPc2	přerušení
a	a	k8xC	a
nejmenším	malý	k2eAgInSc7d3	nejmenší
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
útvarem	útvar	k1gInSc7	útvar
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
póly	pól	k1gInPc7	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
může	moct	k5eAaImIp3nS	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
přestávkou	přestávka	k1gFnSc7	přestávka
nebo	nebo	k8xC	nebo
mezihrou	mezihra	k1gFnSc7	mezihra
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
zvaných	zvaný	k2eAgNnPc2d1	zvané
dějství	dějství	k1gNnPc2	dějství
(	(	kIx(	(
<g/>
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
akt	akt	k1gInSc1	akt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
obrazy	obraz	k1gInPc4	obraz
(	(	kIx(	(
<g/>
oddělené	oddělený	k2eAgFnPc4d1	oddělená
tzv.	tzv.	kA	tzv.
proměnou	proměna	k1gFnSc7	proměna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výstupy	výstup	k1gInPc4	výstup
či	či	k8xC	či
scény	scéna	k1gFnPc4	scéna
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Číslová	číslový	k2eAgFnSc1d1	číslová
opera	opera	k1gFnSc1	opera
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc2d1	barokní
opery	opera	k1gFnSc2	opera
sestávaly	sestávat	k5eAaImAgInP	sestávat
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
ze	z	k7c2	z
stejnorodého	stejnorodý	k2eAgNnSc2d1	stejnorodé
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
nečleněného	členěný	k2eNgInSc2d1	nečleněný
recitativu	recitativ	k1gInSc2	recitativ
<g/>
,	,	kIx,	,
a	a	k8xC	a
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
tak	tak	k9	tak
definici	definice	k1gFnSc4	definice
prokomponované	prokomponovaný	k2eAgFnSc2d1	prokomponovaná
opery	opera	k1gFnSc2	opera
<g/>
;	;	kIx,	;
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
u	u	k7c2	u
Monteverdiho	Monteverdi	k1gMnSc2	Monteverdi
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kontrast	kontrast	k1gInSc4	kontrast
mezi	mezi	k7c7	mezi
pasážemi	pasáž	k1gFnPc7	pasáž
deklamačními	deklamační	k2eAgFnPc7d1	deklamační
(	(	kIx(	(
<g/>
recitativy	recitativ	k1gInPc4	recitativ
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
děje	děj	k1gInSc2	děj
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpěvnými	zpěvný	k2eAgInPc7d1	zpěvný
(	(	kIx(	(
<g/>
árie	árie	k1gFnPc1	árie
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
duševních	duševní	k2eAgInPc2d1	duševní
stavů	stav	k1gInPc2	stav
postav	postava	k1gFnPc2	postava
<g/>
)	)	kIx)	)
a	a	k8xC	a
prosadila	prosadit	k5eAaPmAgFnS	prosadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
forma	forma	k1gFnSc1	forma
číslové	číslový	k2eAgFnSc2d1	číslová
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
změnách	změna	k1gFnPc6	změna
dominovala	dominovat	k5eAaImAgFnS	dominovat
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslová	číslový	k2eAgFnSc1d1	číslová
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
řetězcem	řetězec	k1gInSc7	řetězec
různých	různý	k2eAgNnPc2d1	různé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřetelně	zřetelně	k6eAd1	zřetelně
oddělených	oddělený	k2eAgInPc2d1	oddělený
a	a	k8xC	a
vesměs	vesměs	k6eAd1	vesměs
kratších	krátký	k2eAgInPc2d2	kratší
hudebních	hudební	k2eAgInPc2d1	hudební
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
zpěvných	zpěvný	k2eAgInPc2d1	zpěvný
nebo	nebo	k8xC	nebo
instrumentálních	instrumentální	k2eAgInPc2d1	instrumentální
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
uceleného	ucelený	k2eAgInSc2d1	ucelený
děje	děj	k1gInSc2	děj
spojeny	spojit	k5eAaPmNgInP	spojit
buď	buď	k8xC	buď
recitativem	recitativ	k1gInSc7	recitativ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mluvenými	mluvený	k2eAgInPc7d1	mluvený
dialogy	dialog	k1gInPc7	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc1d1	hudební
prvky	prvek	k1gInPc1	prvek
opery	opera	k1gFnSc2	opera
jsou	být	k5eAaImIp3nP	být
různorodé	různorodý	k2eAgFnPc1d1	různorodá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
četností	četnost	k1gFnSc7	četnost
a	a	k8xC	a
různou	různý	k2eAgFnSc7d1	různá
váhou	váha	k1gFnSc7	váha
podle	podle	k7c2	podle
požadavků	požadavek	k1gInPc2	požadavek
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
námětu	námět	k1gInSc2	námět
a	a	k8xC	a
autorského	autorský	k2eAgInSc2d1	autorský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
hudebních	hudební	k2eAgInPc2d1	hudební
útvarů	útvar	k1gInPc2	útvar
používaných	používaný	k2eAgInPc2d1	používaný
především	především	k6eAd1	především
v	v	k7c6	v
číslové	číslový	k2eAgFnSc6d1	číslová
opeře	opera	k1gFnSc6	opera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
–	–	k?	–
integrovány	integrovat	k5eAaBmNgInP	integrovat
do	do	k7c2	do
plynulého	plynulý	k2eAgInSc2d1	plynulý
hudebního	hudební	k2eAgInSc2d1	hudební
proudu	proud	k1gInSc2	proud
–	–	k?	–
také	také	k9	také
v	v	k7c6	v
prokomponované	prokomponovaný	k2eAgFnSc6d1	prokomponovaná
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Instrumentální	instrumentální	k2eAgInPc4d1	instrumentální
útvary	útvar	k1gInPc4	útvar
====	====	k?	====
</s>
</p>
<p>
<s>
Operu	opera	k1gFnSc4	opera
zpravidla	zpravidla	k6eAd1	zpravidla
uvádí	uvádět	k5eAaImIp3nS	uvádět
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
samostatná	samostatný	k2eAgFnSc1d1	samostatná
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
skladba	skladba	k1gFnSc1	skladba
zvaná	zvaný	k2eAgFnSc1d1	zvaná
předehra	předehra	k1gFnSc1	předehra
nebo	nebo	k8xC	nebo
ouvertura	ouvertura	k1gFnSc1	ouvertura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
a	a	k8xC	a
klasicismu	klasicismus	k1gInSc6	klasicismus
neměly	mít	k5eNaImAgFnP	mít
předehry	předehra	k1gFnPc1	předehra
žádnou	žádný	k3yNgFnSc7	žádný
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
volnou	volný	k2eAgFnSc4d1	volná
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
zbytku	zbytek	k1gInSc2	zbytek
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
ouvertura	ouvertura	k1gFnSc1	ouvertura
(	(	kIx(	(
<g/>
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
s	s	k7c7	s
pomalou	pomalý	k2eAgFnSc7d1	pomalá
a	a	k8xC	a
důstojnou	důstojný	k2eAgFnSc7d1	důstojná
první	první	k4xOgNnSc4	první
částí	část	k1gFnPc2	část
a	a	k8xC	a
rychlou	rychlý	k2eAgFnSc4d1	rychlá
a	a	k8xC	a
vzrušenou	vzrušený	k2eAgFnSc4d1	vzrušená
částí	část	k1gFnSc7	část
druhou	druhý	k4xOgFnSc4	druhý
<g/>
)	)	kIx)	)
a	a	k8xC	a
třídílná	třídílný	k2eAgFnSc1d1	třídílná
italská	italský	k2eAgFnSc1d1	italská
ouvertura	ouvertura	k1gFnSc1	ouvertura
neboli	neboli	k8xC	neboli
sinfonie	sinfonia	k1gFnPc1	sinfonia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
romantismu	romantismus	k1gInSc6	romantismus
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
typ	typ	k1gInSc1	typ
předehry	předehra	k1gFnSc2	předehra
zpracovávající	zpracovávající	k2eAgInSc4d1	zpracovávající
jeden	jeden	k4xCgInSc4	jeden
hudební	hudební	k2eAgInSc4d1	hudební
motiv	motiv	k1gInSc4	motiv
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
představující	představující	k2eAgFnSc4d1	představující
postupně	postupně	k6eAd1	postupně
řadu	řada	k1gFnSc4	řada
hudebních	hudební	k2eAgInPc2d1	hudební
motivů	motiv	k1gInPc2	motiv
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
zpracování	zpracování	k1gNnSc2	zpracování
od	od	k7c2	od
sonátově	sonátově	k6eAd1	sonátově
propracované	propracovaný	k2eAgFnSc2d1	propracovaná
programní	programní	k2eAgFnSc2d1	programní
předehry	předehra	k1gFnSc2	předehra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
předehra	předehra	k1gFnSc1	předehra
Weberova	Weberův	k2eAgMnSc2d1	Weberův
Čarostřelce	čarostřelec	k1gMnSc2	čarostřelec
<g/>
)	)	kIx)	)
po	po	k7c4	po
prosté	prostý	k2eAgNnSc4d1	prosté
řazení	řazení	k1gNnSc4	řazení
v	v	k7c6	v
předehře	předehra	k1gFnSc6	předehra
typu	typ	k1gInSc2	typ
pot-pourri	potourr	k1gFnSc2	pot-pourr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
předehra	předehra	k1gFnSc1	předehra
Bizetovy	Bizetův	k2eAgFnSc2d1	Bizetova
Carmen	Carmen	k2eAgInSc4d1	Carmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgFnPc1d1	klasická
a	a	k8xC	a
romantické	romantický	k2eAgFnPc1d1	romantická
předehry	předehra	k1gFnPc1	předehra
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
odděleny	oddělit	k5eAaPmNgInP	oddělit
od	od	k7c2	od
opery	opera	k1gFnSc2	opera
pauzou	pauza	k1gFnSc7	pauza
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
hrát	hrát	k5eAaImF	hrát
jako	jako	k9	jako
koncertní	koncertní	k2eAgFnPc4d1	koncertní
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
předehry	předehra	k1gFnPc1	předehra
však	však	k9	však
plynule	plynule	k6eAd1	plynule
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
zpěvního	zpěvní	k2eAgNnSc2d1	zpěvní
čísla	číslo	k1gNnSc2	číslo
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
preludiu	preludium	k1gNnSc6	preludium
či	či	k8xC	či
o	o	k7c6	o
orchestrálním	orchestrální	k2eAgInSc6d1	orchestrální
úvodu	úvod	k1gInSc6	úvod
a	a	k8xC	a
ne	ne	k9	ne
o	o	k7c6	o
ouvertuře	ouvertura	k1gFnSc6	ouvertura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
předeher	předehra	k1gFnPc2	předehra
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
zakončení	zakončení	k1gNnSc2	zakončení
<g/>
:	:	kIx,	:
jedno	jeden	k4xCgNnSc4	jeden
přechází	přecházet	k5eAaImIp3nS	přecházet
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
koncertní	koncertní	k2eAgNnSc1d1	koncertní
zakončení	zakončení	k1gNnSc1	zakončení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
Mozartova	Mozartův	k2eAgMnSc2d1	Mozartův
Dona	Don	k1gMnSc2	Don
Giovanniho	Giovanni	k1gMnSc2	Giovanni
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Preludium	preludium	k1gNnSc1	preludium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několikataktové	několikataktový	k2eAgNnSc1d1	několikataktový
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
uvádět	uvádět	k5eAaImF	uvádět
i	i	k9	i
další	další	k2eAgNnSc4d1	další
dějství	dějství	k1gNnSc4	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
opery	opera	k1gFnPc1	opera
předehru	předehra	k1gFnSc4	předehra
zcela	zcela	k6eAd1	zcela
postrádají	postrádat	k5eAaImIp3nP	postrádat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezihra	mezihra	k1gFnSc1	mezihra
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
entr	entr	k1gInSc1	entr
<g/>
'	'	kIx"	'
<g/>
acte	acte	k1gInSc1	acte
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
intermezzo	intermezzo	k1gNnSc1	intermezzo
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
různá	různý	k2eAgNnPc4d1	různé
dějství	dějství	k1gNnPc4	dějství
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
nebo	nebo	k8xC	nebo
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgNnSc4d1	rychlé
přestavění	přestavění	k1gNnSc4	přestavění
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
opeře	opera	k1gFnSc6	opera
jsou	být	k5eAaImIp3nP	být
mezihry	mezihra	k1gFnPc4	mezihra
často	často	k6eAd1	často
symfonické	symfonický	k2eAgFnPc4d1	symfonická
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hrány	hrát	k5eAaImNgInP	hrát
i	i	k9	i
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
opery	opera	k1gFnSc2	opera
jako	jako	k8xS	jako
koncertní	koncertní	k2eAgFnSc2d1	koncertní
skladby	skladba	k1gFnSc2	skladba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Východ	východ	k1gInSc1	východ
měsíce	měsíc	k1gInSc2	měsíc
<g/>
"	"	kIx"	"
z	z	k7c2	z
Blodkovy	Blodkův	k2eAgFnSc2d1	Blodkova
opery	opera	k1gFnSc2	opera
V	v	k7c6	v
studni	studna	k1gFnSc6	studna
<g/>
,	,	kIx,	,
mezihra	mezihra	k1gFnSc1	mezihra
z	z	k7c2	z
Massenetovy	Massenetův	k2eAgFnSc2d1	Massenetova
opery	opera	k1gFnSc2	opera
Thaï	Thaï	k1gFnSc2	Thaï
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Four	Four	k1gMnSc1	Four
Sea	Sea	k1gMnSc1	Sea
Interludes	Interludes	k1gMnSc1	Interludes
<g/>
"	"	kIx"	"
z	z	k7c2	z
Brittenovy	Brittenův	k2eAgFnSc2d1	Brittenova
opery	opera	k1gFnSc2	opera
Peter	Peter	k1gMnSc1	Peter
Grimes	Grimes	k1gMnSc1	Grimes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
dějství	dějství	k1gNnPc2	dějství
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
instrumentální	instrumentální	k2eAgInPc1d1	instrumentální
úseky	úsek	k1gInPc1	úsek
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
balet	balet	k1gInSc4	balet
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svatební	svatební	k2eAgInPc4d1	svatební
tance	tanec	k1gInPc4	tanec
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
Rusalky	rusalka	k1gFnSc2	rusalka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochody	pochod	k1gInPc1	pochod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pochod	pochod	k1gInSc1	pochod
komediantů	komediant	k1gMnPc2	komediant
v	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pantomimu	pantomima	k1gFnSc4	pantomima
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hudba	hudba	k1gFnSc1	hudba
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
v	v	k7c6	v
Auberově	Auberův	k2eAgFnSc6d1	Auberův
Němé	němý	k2eAgFnSc6d1	němá
z	z	k7c2	z
Portici	Portice	k1gFnSc6	Portice
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
====	====	k?	====
Zpěvní	zpěvní	k2eAgNnPc1d1	zpěvní
čísla	číslo	k1gNnPc1	číslo
====	====	k?	====
</s>
</p>
<p>
<s>
Árie	árie	k1gFnSc1	árie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
chápání	chápání	k1gNnSc6	chápání
obecný	obecný	k2eAgInSc4d1	obecný
pojem	pojem	k1gInSc4	pojem
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
sólové	sólový	k2eAgInPc4d1	sólový
zpěvy	zpěv	k1gInPc4	zpěv
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
tempa	tempo	k1gNnSc2	tempo
<g/>
,	,	kIx,	,
nálady	nálada	k1gFnSc2	nálada
apod.	apod.	kA	apod.
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používán	k2eAgInPc1d1	používán
specifičtější	specifický	k2eAgInPc1d2	specifičtější
pojmy	pojem	k1gInPc1	pojem
<g/>
:	:	kIx,	:
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
kavatina	kavatina	k1gFnSc1	kavatina
<g/>
,	,	kIx,	,
kuplet	kuplet	k1gInSc1	kuplet
<g/>
,	,	kIx,	,
kabaleta	kabaleta	k1gFnSc1	kabaleta
<g/>
,	,	kIx,	,
romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
balada	balada	k1gFnSc1	balada
apod.	apod.	kA	apod.
Árie	árie	k1gFnPc1	árie
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pro	pro	k7c4	pro
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
nejúčinnější	účinný	k2eAgFnSc2d3	nejúčinnější
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc2d3	nejznámější
části	část	k1gFnSc2	část
určité	určitý	k2eAgFnSc2d1	určitá
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
recitativní	recitativní	k2eAgFnSc7d1	recitativní
pasáží	pasáž	k1gFnSc7	pasáž
<g/>
,	,	kIx,	,
uváděny	uváděn	k2eAgInPc4d1	uváděn
koncertně	koncertně	k6eAd1	koncertně
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Árie	árie	k1gFnSc1	árie
popisuje	popisovat	k5eAaImIp3nS	popisovat
zpravidla	zpravidla	k6eAd1	zpravidla
emocionální	emocionální	k2eAgInSc1d1	emocionální
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
nebo	nebo	k8xC	nebo
myšlenky	myšlenka	k1gFnPc1	myšlenka
zpívající	zpívající	k2eAgFnPc4d1	zpívající
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
tok	tok	k1gInSc4	tok
dramatického	dramatický	k2eAgInSc2d1	dramatický
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnPc1d2	kratší
nebo	nebo	k8xC	nebo
lehčí	lehký	k2eAgFnPc1d2	lehčí
árie	árie	k1gFnPc1	árie
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
arieta	ariet	k2eAgFnSc1d1	ariet
<g/>
.	.	kIx.	.
</s>
<s>
Arioso	arioso	k1gNnSc1	arioso
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc4	úsek
melodické	melodický	k2eAgFnSc2d1	melodická
deklamace	deklamace	k1gFnSc2	deklamace
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc4d1	přechodný
tvar	tvar	k1gInSc4	tvar
mezi	mezi	k7c7	mezi
recitativem	recitativ	k1gInSc7	recitativ
a	a	k8xC	a
árií	árie	k1gFnSc7	árie
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
prokomponovaných	prokomponovaný	k2eAgFnPc6d1	prokomponovaná
operách	opera	k1gFnPc6	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ansámbl	ansámbl	k1gInSc1	ansámbl
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
sólových	sólový	k2eAgInPc2d1	sólový
hlasů	hlas	k1gInPc2	hlas
<g/>
:	:	kIx,	:
duet	duet	k1gInSc1	duet
<g/>
,	,	kIx,	,
tercet	tercet	k1gInSc1	tercet
<g/>
,	,	kIx,	,
kvartet	kvartet	k1gInSc1	kvartet
atd.	atd.	kA	atd.
Ansámbly	ansámbl	k1gInPc1	ansámbl
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
postav	postava	k1gFnPc2	postava
typicky	typicky	k6eAd1	typicky
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
při	při	k7c6	při
zastavení	zastavení	k1gNnSc6	zastavení
dramatického	dramatický	k2eAgInSc2d1	dramatický
děje	děj	k1gInSc2	děj
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
árie	árie	k1gFnSc1	árie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
milostná	milostný	k2eAgNnPc1d1	milostné
dueta	dueto	k1gNnPc1	dueto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vícečlenné	vícečlenný	k2eAgInPc1d1	vícečlenný
ansámbly	ansámbl	k1gInPc1	ansámbl
často	často	k6eAd1	často
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
interakce	interakce	k1gFnPc1	interakce
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
posunují	posunovat	k5eAaImIp3nP	posunovat
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
vrcholných	vrcholný	k2eAgInPc6d1	vrcholný
okamžicích	okamžik	k1gInPc6	okamžik
a	a	k8xC	a
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
útvary	útvar	k1gInPc7	útvar
(	(	kIx(	(
<g/>
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
árie	árie	k1gFnPc1	árie
<g/>
)	)	kIx)	)
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
dějství	dějství	k1gNnSc1	dějství
(	(	kIx(	(
<g/>
finále	finále	k1gNnSc1	finále
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
je	být	k5eAaImIp3nS	být
vokální	vokální	k2eAgInSc1d1	vokální
útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
osob	osoba	k1gFnPc2	osoba
bez	bez	k7c2	bez
individuálních	individuální	k2eAgFnPc2d1	individuální
zpěvních	zpěvní	k2eAgFnPc2d1	zpěvní
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
kontrast	kontrast	k1gInSc4	kontrast
k	k	k7c3	k
sólovým	sólový	k2eAgInPc3d1	sólový
útvarům	útvar	k1gInPc3	útvar
a	a	k8xC	a
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
lid	lid	k1gInSc1	lid
nebo	nebo	k8xC	nebo
úžeji	úzko	k6eAd2	úzko
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
sociální	sociální	k2eAgFnSc4d1	sociální
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
operách	opera	k1gFnPc6	opera
je	být	k5eAaImIp3nS	být
sborový	sborový	k2eAgInSc1d1	sborový
zpěv	zpěv	k1gInSc1	zpěv
jejich	jejich	k3xOp3gNnSc7	jejich
těžištěm	těžiště	k1gNnSc7	těžiště
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgMnSc3	jenž
sólové	sólový	k2eAgInPc1d1	sólový
výstupy	výstup	k1gInPc1	výstup
zanikají	zanikat	k5eAaImIp3nP	zanikat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Verdiho	Verdi	k1gMnSc2	Verdi
Nabucco	Nabucco	k1gMnSc1	Nabucco
nebo	nebo	k8xC	nebo
Musorgského	Musorgského	k2eAgMnSc1d1	Musorgského
Boris	Boris	k1gMnSc1	Boris
Godunov	Godunov	k1gInSc1	Godunov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zpívána	zpívat	k5eAaImNgFnS	zpívat
koncertně	koncertně	k6eAd1	koncertně
mimo	mimo	k7c4	mimo
operní	operní	k2eAgInSc4d1	operní
kontext	kontext	k1gInSc4	kontext
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Recitativ	recitativ	k1gInSc1	recitativ
je	být	k5eAaImIp3nS	být
zhudebněný	zhudebněný	k2eAgInSc1d1	zhudebněný
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sleduje	sledovat	k5eAaImIp3nS	sledovat
mluvený	mluvený	k2eAgInSc4d1	mluvený
spád	spád	k1gInSc4	spád
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
těch	ten	k3xDgFnPc2	ten
scén	scéna	k1gFnPc2	scéna
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nesou	nést	k5eAaImIp3nP	nést
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dialogů	dialog	k1gInPc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
a	a	k8xC	a
klasicistní	klasicistní	k2eAgFnSc3d1	klasicistní
hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
rozlišovalo	rozlišovat	k5eAaImAgNnS	rozlišovat
mezi	mezi	k7c4	mezi
secco	secco	k1gNnSc4	secco
recitativem	recitativ	k1gInSc7	recitativ
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
recitativo	recitativa	k1gFnSc5	recitativa
secco	secco	k1gNnSc4	secco
italsky	italsky	k6eAd1	italsky
"	"	kIx"	"
<g/>
secco	secco	k6eAd1	secco
<g/>
"	"	kIx"	"
=	=	kIx~	=
suchý	suchý	k2eAgInSc1d1	suchý
<g/>
)	)	kIx)	)
a	a	k8xC	a
doprovázeným	doprovázený	k2eAgInSc7d1	doprovázený
recitativem	recitativ	k1gInSc7	recitativ
(	(	kIx(	(
<g/>
recitativo	recitativa	k1gFnSc5	recitativa
accompagnato	accompagnato	k6eAd1	accompagnato
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
"	"	kIx"	"
<g/>
accompagnato	accompagnato	k6eAd1	accompagnato
<g/>
"	"	kIx"	"
=	=	kIx~	=
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
secco	secco	k1gNnSc4	secco
recitativu	recitativ	k1gInSc2	recitativ
jsou	být	k5eAaImIp3nP	být
notovány	notován	k2eAgInPc1d1	notován
pouze	pouze	k6eAd1	pouze
zpěvní	zpěvní	k2eAgInSc1d1	zpěvní
hlas	hlas	k1gInSc1	hlas
a	a	k8xC	a
basová	basový	k2eAgFnSc1d1	basová
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zapisován	zapisován	k2eAgInSc1d1	zapisován
i	i	k8xC	i
příslušný	příslušný	k2eAgInSc1d1	příslušný
akordický	akordický	k2eAgInSc1d1	akordický
doprovod	doprovod	k1gInSc1	doprovod
jako	jako	k8xC	jako
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rozepsané	rozepsaný	k2eAgFnSc6d1	rozepsaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jedním	jeden	k4xCgInSc7	jeden
basovým	basový	k2eAgInSc7d1	basový
nástrojem	nástroj	k1gInSc7	nástroj
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
harmonickým	harmonický	k2eAgInSc7d1	harmonický
nástrojem	nástroj	k1gInSc7	nástroj
(	(	kIx(	(
<g/>
drnkacím	drnkací	k2eAgNnSc7d1	drnkací
nebo	nebo	k8xC	nebo
klávesovým	klávesový	k2eAgNnSc7d1	klávesové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
přebírá	přebírat	k5eAaImIp3nS	přebírat
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
jen	jen	k9	jen
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
doprovázeného	doprovázený	k2eAgInSc2d1	doprovázený
recitativu	recitativ	k1gInSc2	recitativ
–	–	k?	–
používaného	používaný	k2eAgNnSc2d1	používané
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
monology	monolog	k1gInPc4	monolog
–	–	k?	–
je	být	k5eAaImIp3nS	být
doprovod	doprovod	k1gInSc1	doprovod
komponován	komponovat	k5eAaImNgInS	komponovat
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
často	často	k6eAd1	často
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
následující	následující	k2eAgFnSc1d1	následující
árie	árie	k1gFnSc1	árie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
situaci	situace	k1gFnSc4	situace
recitativ	recitativ	k1gInSc1	recitativ
připravuje	připravovat	k5eAaImIp3nS	připravovat
<g/>
.	.	kIx.	.
</s>
<s>
Orchestrální	orchestrální	k2eAgInSc1d1	orchestrální
doprovod	doprovod	k1gInSc1	doprovod
přitom	přitom	k6eAd1	přitom
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kontrastnější	kontrastní	k2eAgNnSc1d2	kontrastnější
vypracování	vypracování	k1gNnSc1	vypracování
obsahu	obsah	k1gInSc2	obsah
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
scena	scena	k6eAd1	scena
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
doprovázeného	doprovázený	k2eAgInSc2d1	doprovázený
recitativu	recitativ	k1gInSc2	recitativ
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
árie	árie	k1gFnSc2	árie
nebo	nebo	k8xC	nebo
ansámblu	ansámbl	k1gInSc2	ansámbl
<g/>
.	.	kIx.	.
</s>
<s>
Orchestrální	orchestrální	k2eAgNnSc4d1	orchestrální
zpracování	zpracování	k1gNnSc4	zpracování
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
árii	árie	k1gFnSc4	árie
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
jen	jen	k9	jen
výraznější	výrazný	k2eAgFnSc1d2	výraznější
melodická	melodický	k2eAgFnSc1d1	melodická
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Introdukce	introdukce	k1gFnSc1	introdukce
<g/>
,	,	kIx,	,
mezihra	mezihra	k1gFnSc1	mezihra
a	a	k8xC	a
finále	finále	k1gNnSc1	finále
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
,	,	kIx,	,
kompozitní	kompozitní	k2eAgFnSc1d1	kompozitní
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
formy	forma	k1gFnPc4	forma
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
či	či	k8xC	či
konci	konec	k1gInSc6	konec
jednání	jednání	k1gNnSc2	jednání
nebo	nebo	k8xC	nebo
spojující	spojující	k2eAgFnPc4d1	spojující
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
epizody	epizoda	k1gFnPc4	epizoda
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
forem	forma	k1gFnPc2	forma
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
řetězí	řetězit	k5eAaImIp3nP	řetězit
a	a	k8xC	a
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
kratší	krátký	k2eAgInPc1d2	kratší
útvary	útvar	k1gInPc1	útvar
–	–	k?	–
árie	árie	k1gFnSc2	árie
<g/>
,	,	kIx,	,
ansámbly	ansámbl	k1gInPc4	ansámbl
s	s	k7c7	s
měnícím	měnící	k2eAgMnSc7d1	měnící
se	se	k3xPyFc4	se
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
recitativy	recitativ	k1gInPc1	recitativ
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
–	–	k?	–
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
finále	finále	k1gNnSc1	finále
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
útvarem	útvar	k1gInSc7	útvar
klasické	klasický	k2eAgFnSc2d1	klasická
a	a	k8xC	a
raně	raně	k6eAd1	raně
romantické	romantický	k2eAgFnPc4d1	romantická
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
typů	typ	k1gInPc2	typ
je	být	k5eAaImIp3nS	být
finále	finále	k1gNnSc4	finále
en	en	k?	en
vaudeville	vaudeville	k1gInSc1	vaudeville
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
opéry	opéra	k1gFnSc2	opéra
comique	comiqu	k1gFnSc2	comiqu
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
strofická	strofický	k2eAgFnSc1d1	strofická
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
refrénem	refrén	k1gInSc7	refrén
<g/>
,	,	kIx,	,
zpívaná	zpívaná	k1gFnSc1	zpívaná
střídavě	střídavě	k6eAd1	střídavě
i	i	k9	i
společně	společně	k6eAd1	společně
sólisty	sólista	k1gMnPc4	sólista
a	a	k8xC	a
textově	textově	k6eAd1	textově
shrnující	shrnující	k2eAgNnSc4d1	shrnující
poslání	poslání	k1gNnSc4	poslání
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
finále	finále	k1gNnSc1	finále
Mozartova	Mozartův	k2eAgInSc2d1	Mozartův
Únosu	únos	k1gInSc2	únos
ze	z	k7c2	z
serailu	serail	k1gInSc2	serail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
a	a	k8xC	a
strukturované	strukturovaný	k2eAgNnSc1d1	strukturované
finále	finále	k1gNnSc1	finále
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
italskou	italský	k2eAgFnSc4d1	italská
belcantovou	belcantový	k2eAgFnSc4d1	belcantová
operu	opera	k1gFnSc4	opera
(	(	kIx(	(
<g/>
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
,	,	kIx,	,
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
,	,	kIx,	,
Donizetti	Donizett	k2eAgMnPc1d1	Donizett
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Melodram	melodram	k1gInSc4	melodram
====	====	k?	====
</s>
</p>
<p>
<s>
Melodram	melodram	k1gInSc1	melodram
označuje	označovat	k5eAaImIp3nS	označovat
řeč	řeč	k1gFnSc4	řeč
doprovázenou	doprovázený	k2eAgFnSc7d1	doprovázená
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
operách	opera	k1gFnPc6	opera
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sporadicky	sporadicky	k6eAd1	sporadicky
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
romantismu	romantismus	k1gInSc2	romantismus
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
v	v	k7c6	v
Beethovenově	Beethovenův	k2eAgFnSc6d1	Beethovenova
opeře	opera	k1gFnSc6	opera
Fidelio	Fidelio	k6eAd1	Fidelio
je	být	k5eAaImIp3nS	být
melodram	melodram	k1gInSc1	melodram
uvnitř	uvnitř	k7c2	uvnitř
opery	opera	k1gFnSc2	opera
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žal	žal	k1gInSc1	žal
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
Leonory	Leonora	k1gFnSc2	Leonora
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
vykopat	vykopat	k5eAaPmF	vykopat
hrob	hrob	k1gInSc4	hrob
vlastnímu	vlastní	k2eAgMnSc3d1	vlastní
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
odít	odít	k5eAaPmF	odít
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
operách	opera	k1gFnPc6	opera
s	s	k7c7	s
mluvenými	mluvený	k2eAgInPc7d1	mluvený
dialogy	dialog	k1gInPc7	dialog
může	moct	k5eAaImIp3nS	moct
(	(	kIx(	(
<g/>
namísto	namísto	k7c2	namísto
recitativu	recitativ	k1gInSc2	recitativ
<g/>
)	)	kIx)	)
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
mluveného	mluvený	k2eAgInSc2d1	mluvený
textu	text	k1gInSc2	text
ke	k	k7c3	k
zpívanému	zpívaný	k2eAgMnSc3d1	zpívaný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Šeborově	Šeborův	k2eAgFnSc6d1	Šeborův
Zmařené	zmařený	k2eAgFnSc6d1	zmařená
svatbě	svatba	k1gFnSc6	svatba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Melodram	melodram	k1gInSc1	melodram
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
také	také	k9	také
například	například	k6eAd1	například
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
čtení	čtení	k1gNnSc2	čtení
dopisů	dopis	k1gInPc2	dopis
(	(	kIx(	(
<g/>
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnPc1	vdova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ruggero	Ruggero	k1gNnSc1	Ruggero
Leoncavallo	Leoncavallo	k1gNnSc4	Leoncavallo
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Zazà	Zazà	k1gFnSc2	Zazà
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
melodramaticky	melodramaticky	k6eAd1	melodramaticky
dětskou	dětský	k2eAgFnSc4d1	dětská
roli	role	k1gFnSc4	role
dcery	dcera	k1gFnSc2	dcera
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
opeře	opera	k1gFnSc6	opera
jsou	být	k5eAaImIp3nP	být
melodramatické	melodramatický	k2eAgFnPc1d1	melodramatická
pasáže	pasáž	k1gFnPc1	pasáž
hojně	hojně	k6eAd1	hojně
používány	používat	k5eAaImNgFnP	používat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Martinů	Martinů	k2eAgFnPc6d1	Martinů
Řeckých	řecký	k2eAgFnPc6d1	řecká
pašijích	pašije	k1gFnPc6	pašije
nebo	nebo	k8xC	nebo
Brittenově	Brittenův	k2eAgInSc6d1	Brittenův
Snu	sen	k1gInSc6	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
řeči	řeč	k1gFnSc2	řeč
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
předpsaným	předpsaný	k2eAgInSc7d1	předpsaný
rytmem	rytmus	k1gInSc7	rytmus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
melodií	melodie	k1gFnPc2	melodie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prokomponovaná	prokomponovaný	k2eAgFnSc1d1	prokomponovaná
opera	opera	k1gFnSc1	opera
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
skladatelé	skladatel	k1gMnPc1	skladatel
dodržovat	dodržovat	k5eAaImF	dodržovat
střídání	střídání	k1gNnSc4	střídání
dialogů	dialog	k1gInPc2	dialog
či	či	k8xC	či
recitativů	recitativ	k1gInPc2	recitativ
a	a	k8xC	a
uzavřených	uzavřený	k2eAgNnPc2d1	uzavřené
čísel	číslo	k1gNnPc2	číslo
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
přísně	přísně	k6eAd1	přísně
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
secco	secco	k6eAd1	secco
recitativ	recitativ	k1gInSc4	recitativ
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
tvorbě	tvorba	k1gFnSc6	tvorba
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
útvar	útvar	k1gInSc1	útvar
"	"	kIx"	"
<g/>
scéna	scéna	k1gFnSc1	scéna
a	a	k8xC	a
árie	árie	k1gFnPc1	árie
<g/>
"	"	kIx"	"
s	s	k7c7	s
podstatně	podstatně	k6eAd1	podstatně
menším	malý	k2eAgInSc7d2	menší
kontrastem	kontrast	k1gInSc7	kontrast
deklamačních	deklamační	k2eAgInPc2d1	deklamační
a	a	k8xC	a
písňových	písňový	k2eAgInPc2d1	písňový
úseků	úsek	k1gInPc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
u	u	k7c2	u
Giuseppe	Giusepp	k1gInSc5	Giusepp
Verdiho	Verdi	k1gMnSc2	Verdi
se	se	k3xPyFc4	se
dějství	dějství	k1gNnSc2	dějství
stávají	stávat	k5eAaImIp3nP	stávat
většími	veliký	k2eAgInPc7d2	veliký
hudebními	hudební	k2eAgInPc7d1	hudební
celky	celek	k1gInPc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
tradiční	tradiční	k2eAgFnSc3d1	tradiční
opeře	opera	k1gFnSc3	opera
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
drama	drama	k1gFnSc1	drama
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vzdala	vzdát	k5eAaPmAgFnS	vzdát
hudební	hudební	k2eAgFnPc4d1	hudební
číslové	číslový	k2eAgFnPc4d1	číslová
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
i	i	k8xC	i
text	text	k1gInSc4	text
spojila	spojit	k5eAaPmAgFnS	spojit
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
prokomponovaný	prokomponovaný	k2eAgInSc4d1	prokomponovaný
<g/>
,	,	kIx,	,
symfonicky	symfonicky	k6eAd1	symfonicky
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
celek	celek	k1gInSc1	celek
–	–	k?	–
"	"	kIx"	"
<g/>
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
melodii	melodie	k1gFnSc4	melodie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Wagnerova	Wagnerův	k2eAgFnSc1d1	Wagnerova
hudební	hudební	k2eAgFnSc1d1	hudební
teorie	teorie	k1gFnSc1	teorie
i	i	k8xC	i
praktický	praktický	k2eAgInSc1d1	praktický
příklad	příklad	k1gInSc1	příklad
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
formální	formální	k2eAgInSc4d1	formální
vývoj	vývoj	k1gInSc4	vývoj
opery	opera	k1gFnSc2	opera
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
prokomponovaný	prokomponovaný	k2eAgInSc1d1	prokomponovaný
typ	typ	k1gInSc1	typ
všeobecně	všeobecně	k6eAd1	všeobecně
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
operu	opera	k1gFnSc4	opera
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
Giacoma	Giacom	k1gMnSc2	Giacom
Pucciniho	Puccini	k1gMnSc2	Puccini
nebo	nebo	k8xC	nebo
Julese	Julese	k1gFnSc2	Julese
Masseneta	Massenet	k5eAaPmNgNnP	Massenet
<g/>
)	)	kIx)	)
a	a	k8xC	a
přetrval	přetrvat	k5eAaPmAgMnS	přetrvat
jako	jako	k9	jako
základní	základní	k2eAgInSc4d1	základní
typ	typ	k1gInSc4	typ
i	i	k9	i
pro	pro	k7c4	pro
ranou	raný	k2eAgFnSc4d1	raná
modernu	moderna	k1gFnSc4	moderna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
skladatelů	skladatel	k1gMnPc2	skladatel
obrátila	obrátit	k5eAaPmAgFnS	obrátit
zpátky	zpátky	k6eAd1	zpátky
k	k	k7c3	k
číslovému	číslový	k2eAgInSc3d1	číslový
principu	princip	k1gInSc3	princip
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zoltán	Zoltán	k1gMnSc1	Zoltán
Kodály	Kodála	k1gFnSc2	Kodála
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
nebo	nebo	k8xC	nebo
Kurt	Kurt	k1gMnSc1	Kurt
Weill	Weill	k1gMnSc1	Weill
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
operní	operní	k2eAgFnSc6d1	operní
tvorbě	tvorba	k1gFnSc6	tvorba
obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
základní	základní	k2eAgFnPc1d1	základní
formy	forma	k1gFnPc1	forma
včetně	včetně	k7c2	včetně
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
koexistují	koexistovat	k5eAaImIp3nP	koexistovat
<g/>
.	.	kIx.	.
</s>
<s>
Číslová	číslový	k2eAgFnSc1d1	číslová
opera	opera	k1gFnSc1	opera
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
dceřiných	dceřin	k2eAgInPc6d1	dceřin
žánrech	žánr	k1gInPc6	žánr
operety	opereta	k1gFnSc2	opereta
a	a	k8xC	a
muzikálu	muzikál	k1gInSc2	muzikál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
z	z	k7c2	z
prokomponovaných	prokomponovaný	k2eAgFnPc2d1	prokomponovaná
oper	opera	k1gFnPc2	opera
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
možné	možný	k2eAgNnSc1d1	možné
izolovat	izolovat	k5eAaBmF	izolovat
z	z	k7c2	z
celku	celek	k1gInSc2	celek
určité	určitý	k2eAgFnSc2d1	určitá
"	"	kIx"	"
<g/>
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
<g/>
"	"	kIx"	"
části	část	k1gFnSc2	část
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
jako	jako	k8xC	jako
koncertní	koncertní	k2eAgFnPc1d1	koncertní
árie	árie	k1gFnPc1	árie
nebo	nebo	k8xC	nebo
symfonické	symfonický	k2eAgFnPc1d1	symfonická
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třídění	třídění	k1gNnPc1	třídění
oper	opera	k1gFnPc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
třídění	třídění	k1gNnSc2	třídění
podle	podle	k7c2	podle
formy	forma	k1gFnSc2	forma
na	na	k7c4	na
opery	opera	k1gFnPc4	opera
číslové	číslový	k2eAgFnPc4d1	číslová
a	a	k8xC	a
prokomponované	prokomponovaný	k2eAgNnSc1d1	prokomponované
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
opery	opera	k1gFnPc4	opera
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
jiných	jiný	k2eAgNnPc2d1	jiné
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
divadelní	divadelní	k2eAgNnSc4d1	divadelní
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Výpravné	výpravný	k2eAgFnPc1d1	výpravná
opery	opera	k1gFnPc1	opera
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
nazývány	nazývat	k5eAaImNgFnP	nazývat
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
grand	grand	k1gMnSc1	grand
opéra	opéra	k1gMnSc1	opéra
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
též	též	k9	též
žánrové	žánrový	k2eAgNnSc4d1	žánrové
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
komorní	komorní	k2eAgFnSc1d1	komorní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
intimnější	intimní	k2eAgNnSc1d2	intimnější
dílo	dílo	k1gNnSc1	dílo
psané	psaný	k2eAgNnSc1d1	psané
pro	pro	k7c4	pro
menší	malý	k2eAgInSc4d2	menší
počet	počet	k1gInSc4	počet
zpěváků	zpěvák	k1gMnPc2	zpěvák
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
orchestr	orchestr	k1gInSc4	orchestr
nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnSc4d2	menší
divadelní	divadelní	k2eAgFnSc4d1	divadelní
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Drobný	drobný	k2eAgInSc1d1	drobný
operní	operní	k2eAgInSc1d1	operní
útvar	útvar	k1gInSc1	útvar
označuje	označovat	k5eAaImIp3nS	označovat
například	například	k6eAd1	například
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
miniopera	miniopera	k1gFnSc1	miniopera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
užíval	užívat	k5eAaImAgMnS	užívat
Josef	Josef	k1gMnSc1	Josef
Berg	Berg	k1gMnSc1	Berg
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Milhaudův	Milhaudův	k2eAgInSc4d1	Milhaudův
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
opéra-minute	opérainut	k1gMnSc5	opéra-minut
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
specifického	specifický	k2eAgNnSc2d1	specifické
cílového	cílový	k2eAgNnSc2d1	cílové
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dětská	dětský	k2eAgFnSc1d1	dětská
opera	opera	k1gFnSc1	opera
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
dějství	dějství	k1gNnSc1	dějství
<g/>
:	:	kIx,	:
tříaktová	tříaktový	k2eAgFnSc1d1	tříaktová
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
aktovka	aktovka	k1gFnSc1	aktovka
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
:	:	kIx,	:
opera	opera	k1gFnSc1	opera
barokní	barokní	k2eAgFnSc1d1	barokní
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
stylu	styl	k1gInSc2	styl
belcanto	belcanta	k1gFnSc5	belcanta
<g/>
,	,	kIx,	,
opery	opera	k1gFnPc4	opera
romantické	romantický	k2eAgFnPc4d1	romantická
<g/>
,	,	kIx,	,
expresionistické	expresionistický	k2eAgFnPc4d1	expresionistická
<g/>
,	,	kIx,	,
minimalistické	minimalistický	k2eAgFnPc4d1	minimalistická
<g/>
,	,	kIx,	,
jazzové	jazzový	k2eAgFnPc4d1	jazzová
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
kompoziční	kompoziční	k2eAgFnSc2d1	kompoziční
techniky	technika	k1gFnSc2	technika
<g/>
:	:	kIx,	:
opera	opera	k1gFnSc1	opera
čtvrttónová	čtvrttónový	k2eAgFnSc1d1	čtvrttónová
<g/>
,	,	kIx,	,
atonální	atonální	k2eAgFnSc1d1	atonální
<g/>
,	,	kIx,	,
dodekafonická	dodekafonický	k2eAgFnSc1d1	dodekafonická
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
povahy	povaha	k1gFnSc2	povaha
námětu	námět	k1gInSc2	námět
<g/>
:	:	kIx,	:
opera	opera	k1gFnSc1	opera
lyrická	lyrický	k2eAgFnSc1d1	lyrická
<g/>
,	,	kIx,	,
komická	komický	k2eAgFnSc1d1	komická
<g/>
,	,	kIx,	,
tragická	tragický	k2eAgFnSc1d1	tragická
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
<g/>
,	,	kIx,	,
realistická	realistický	k2eAgFnSc1d1	realistická
atd.	atd.	kA	atd.
Některá	některý	k3yIgNnPc4	některý
označení	označení	k1gNnSc4	označení
vedle	vedle	k7c2	vedle
námětu	námět	k1gInSc2	námět
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
i	i	k9	i
použité	použitý	k2eAgInPc1d1	použitý
hudební	hudební	k2eAgInPc1d1	hudební
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
opera	opera	k1gFnSc1	opera
veristická	veristický	k2eAgFnSc1d1	veristická
<g/>
,	,	kIx,	,
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
,	,	kIx,	,
prostonárodní	prostonárodní	k2eAgFnSc1d1	prostonárodní
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
námět	námět	k1gInSc1	námět
je	být	k5eAaImIp3nS	být
vzat	vzít	k5eAaPmNgInS	vzít
z	z	k7c2	z
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
literární	literární	k2eAgFnSc1d1	literární
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
Literaturoper	Literaturoper	k1gInSc1	Literaturoper
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
pro	pro	k7c4	pro
operu	opera	k1gFnSc4	opera
komponovanou	komponovaný	k2eAgFnSc4d1	komponovaná
na	na	k7c6	na
původně	původně	k6eAd1	původně
neoperní	operní	k2eNgFnSc6d1	operní
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
činoherní	činoherní	k2eAgInSc1d1	činoherní
literární	literární	k2eAgInSc1d1	literární
text	text	k1gInSc1	text
<g/>
.	.	kIx.	.
<g/>
Běžné	běžný	k2eAgNnSc1d1	běžné
je	být	k5eAaImIp3nS	být
řazení	řazení	k1gNnSc1	řazení
podle	podle	k7c2	podle
(	(	kIx(	(
<g/>
sub	sub	k1gNnSc2	sub
<g/>
)	)	kIx)	)
<g/>
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
však	však	k9	však
formální	formální	k2eAgNnSc4d1	formální
<g/>
,	,	kIx,	,
látkové	látkový	k2eAgNnSc4d1	látkové
i	i	k8xC	i
časové	časový	k2eAgNnSc4d1	časové
vymezení	vymezení	k1gNnSc4	vymezení
řady	řada	k1gFnSc2	řada
žánrů	žánr	k1gInPc2	žánr
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
nebo	nebo	k8xC	nebo
nejednoznačné	jednoznačný	k2eNgNnSc1d1	nejednoznačné
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
překrývat	překrývat	k5eAaImF	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
žánrovými	žánrový	k2eAgNnPc7d1	žánrové
označeními	označení	k1gNnPc7	označení
zavedenými	zavedený	k2eAgFnPc7d1	zavedená
hudební	hudební	k2eAgFnSc4d1	hudební
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
až	až	k9	až
následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dílům	díl	k1gInPc3	díl
dodatečně	dodatečně	k6eAd1	dodatečně
přiřčenými	přiřčený	k2eAgMnPc7d1	přiřčený
(	(	kIx(	(
<g/>
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
například	například	k6eAd1	například
opera	opera	k1gFnSc1	opera
seria	serius	k1gMnSc2	serius
<g/>
,	,	kIx,	,
opéra	opér	k1gMnSc2	opér
lyrique	lyriqu	k1gMnSc2	lyriqu
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zařadit	zařadit	k5eAaPmF	zařadit
je	on	k3xPp3gFnPc4	on
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
typu	typ	k1gInSc2	typ
–	–	k?	–
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
označeními	označení	k1gNnPc7	označení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
svým	svůj	k3xOyFgInSc7	svůj
dílům	dílo	k1gNnPc3	dílo
dali	dát	k5eAaPmAgMnP	dát
sami	sám	k3xTgMnPc1	sám
tvůrci	tvůrce	k1gMnPc1	tvůrce
a	a	k8xC	a
která	který	k3yRgNnPc1	který
často	často	k6eAd1	často
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
jejích	její	k3xOp3gFnPc2	její
snahu	snaha	k1gFnSc4	snaha
své	svůj	k3xOyFgNnSc1	svůj
dílo	dílo	k1gNnSc1	dílo
individualizovat	individualizovat	k5eAaImF	individualizovat
<g/>
,	,	kIx,	,
odpoutat	odpoutat	k5eAaPmF	odpoutat
od	od	k7c2	od
žánrových	žánrový	k2eAgInPc2d1	žánrový
předsudků	předsudek	k1gInPc2	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
moderní	moderní	k2eAgMnPc1d1	moderní
avantgardní	avantgardní	k2eAgMnPc1d1	avantgardní
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
žánrovým	žánrový	k2eAgNnSc7d1	žánrové
označením	označení	k1gNnSc7	označení
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
prostého	prostý	k2eAgMnSc2d1	prostý
"	"	kIx"	"
<g/>
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
volí	volit	k5eAaImIp3nP	volit
neutrálně	neutrálně	k6eAd1	neutrálně
znějící	znějící	k2eAgInPc1d1	znějící
názvy	název	k1gInPc1	název
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
jevištní	jevištní	k2eAgFnSc1d1	jevištní
akce	akce	k1gFnSc1	akce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Luigi	Luig	k1gMnSc5	Luig
Nono	Nona	k1gMnSc5	Nona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
scénická	scénický	k2eAgFnSc1d1	scénická
kompozice	kompozice	k1gFnSc1	kompozice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mauricio	Mauricio	k1gMnSc1	Mauricio
Kagel	Kagel	k1gMnSc1	Kagel
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
akce	akce	k1gFnSc1	akce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Luciano	Luciana	k1gFnSc5	Luciana
Berio	Berio	k1gMnSc1	Berio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
operní	operní	k2eAgInPc1d1	operní
žánry	žánr	k1gInPc1	žánr
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
významné	významný	k2eAgFnPc1d1	významná
a	a	k8xC	a
početné	početný	k2eAgFnPc1d1	početná
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
tisíců	tisíc	k4xCgInPc2	tisíc
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
jako	jako	k9	jako
opera	opera	k1gFnSc1	opera
buffa	buffa	k1gFnSc1	buffa
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
jen	jen	k9	jen
hrstka	hrstka	k1gFnSc1	hrstka
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
pojmem	pojem	k1gInSc7	pojem
časová	časový	k2eAgFnSc1d1	časová
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
Zeitoper	Zeitoper	k1gInSc1	Zeitoper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
žánrová	žánrový	k2eAgNnPc4d1	žánrové
označení	označení	k1gNnPc4	označení
jako	jako	k9	jako
"	"	kIx"	"
<g/>
melolog	melolog	k1gMnSc1	melolog
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
:	:	kIx,	:
Lélio	Lélio	k1gMnSc1	Lélio
<g/>
,	,	kIx,	,
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jevištní	jevištní	k2eAgFnSc1d1	jevištní
zásvětná	zásvětný	k2eAgFnSc1d1	zásvětná
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
Bühnenweihfestspiel	Bühnenweihfestspiel	k1gInSc1	Bühnenweihfestspiel
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
:	:	kIx,	:
Parsifal	Parsifal	k1gMnSc1	Parsifal
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Songspiel	Songspiel	k1gMnSc1	Songspiel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Weill	Weill	k1gMnSc1	Weill
<g/>
:	:	kIx,	:
Mahagonny	Mahagonna	k1gMnSc2	Mahagonna
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Boris	Boris	k1gMnSc1	Boris
Blacher	Blachra	k1gFnPc2	Blachra
<g/>
:	:	kIx,	:
Abstrakte	abstrakt	k1gInSc5	abstrakt
Oper	opera	k1gFnPc2	opera
no	no	k9	no
1	[number]	k4	1
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
lyrické	lyrický	k2eAgNnSc1d1	lyrické
uškrcení	uškrcení	k1gNnSc1	uškrcení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Léo	Léo	k1gMnSc1	Léo
Delibes	Delibes	k1gMnSc1	Delibes
<g/>
:	:	kIx,	:
Deux	Deux	k1gInSc1	Deux
sous	sous	k1gInSc1	sous
de	de	k?	de
charbon	charbon	k1gInSc1	charbon
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyhrazena	vyhrazen	k2eAgFnSc1d1	vyhrazena
vždy	vždy	k6eAd1	vždy
pro	pro	k7c4	pro
jediné	jediný	k2eAgNnSc4d1	jediné
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profese	profese	k1gFnSc1	profese
operního	operní	k2eAgNnSc2d1	operní
divadla	divadlo	k1gNnSc2	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Libretista	libretista	k1gMnSc1	libretista
===	===	k?	===
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
a	a	k8xC	a
význam	význam	k1gInSc1	význam
libretisty	libretista	k1gMnSc2	libretista
jako	jako	k8xS	jako
autora	autor	k1gMnSc2	autor
operního	operní	k2eAgInSc2d1	operní
textu	text	k1gInSc2	text
kolísaly	kolísat	k5eAaImAgFnP	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
vztahu	vztah	k1gInSc6	vztah
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
živé	živý	k2eAgInPc1d1	živý
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
předmětem	předmět	k1gInSc7	předmět
některých	některý	k3yIgFnPc2	některý
oper	opera	k1gFnPc2	opera
jako	jako	k8xC	jako
Salieriho	Salieri	k1gMnSc2	Salieri
Prima	prima	k2eAgNnSc2d1	prima
la	la	k1gNnSc2	la
musica	music	k1gInSc2	music
e	e	k0	e
poi	poi	k?	poi
le	le	k?	le
parole	parole	k1gFnSc2	parole
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
a	a	k8xC	a
Straussova	Straussův	k2eAgNnPc1d1	Straussovo
Capriccia	capriccio	k1gNnPc1	capriccio
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvopočátcích	prvopočátek	k1gInPc6	prvopočátek
opery	opera	k1gFnSc2	opera
byla	být	k5eAaImAgNnP	být
slova	slovo	k1gNnPc1	slovo
i	i	k8xC	i
hudba	hudba	k1gFnSc1	hudba
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgFnPc4d1	rovnocenná
a	a	k8xC	a
Ottavio	Ottavio	k6eAd1	Ottavio
Rinuccini	Rinuccin	k2eAgMnPc1d1	Rinuccin
(	(	kIx(	(
<g/>
1562	[number]	k4	1562
<g/>
-	-	kIx~	-
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
libretista	libretista	k1gMnSc1	libretista
prvních	první	k4xOgFnPc2	první
oper	opera	k1gFnPc2	opera
Jacopa	Jacop	k1gMnSc2	Jacop
Periho	Peri	k1gMnSc2	Peri
a	a	k8xC	a
Giulia	Giulius	k1gMnSc2	Giulius
Cacciniho	Caccini	k1gMnSc2	Caccini
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
teoretikem	teoretik	k1gMnSc7	teoretik
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
dominovat	dominovat	k5eAaImF	dominovat
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Mozarta	Mozart	k1gMnSc2	Mozart
je	být	k5eAaImIp3nS	být
poezie	poezie	k1gFnSc1	poezie
"	"	kIx"	"
<g/>
poslušnou	poslušný	k2eAgFnSc7d1	poslušná
dcerou	dcera	k1gFnSc7	dcera
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgNnPc1d1	barokní
libreta	libreto	k1gNnPc1	libreto
tak	tak	k9	tak
byla	být	k5eAaImAgNnP	být
často	často	k6eAd1	často
anonymní	anonymní	k2eAgNnPc1d1	anonymní
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
upřednostňovalo	upřednostňovat	k5eAaImAgNnS	upřednostňovat
známé	známý	k2eAgFnPc4d1	známá
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stále	stále	k6eAd1	stále
opakovaly	opakovat	k5eAaImAgFnP	opakovat
a	a	k8xC	a
několik	několik	k4yIc4	několik
vzorových	vzorový	k2eAgNnPc2d1	vzorové
libret	libreto	k1gNnPc2	libreto
bylo	být	k5eAaImAgNnS	být
zhudebňováno	zhudebňovat	k5eAaImNgNnS	zhudebňovat
stále	stále	k6eAd1	stále
znova	znova	k6eAd1	znova
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
příležitostných	příležitostný	k2eAgFnPc6d1	příležitostná
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
vzorové	vzorový	k2eAgInPc1d1	vzorový
texty	text	k1gInPc1	text
pro	pro	k7c4	pro
italskou	italský	k2eAgFnSc4d1	italská
vážnou	vážný	k2eAgFnSc4d1	vážná
operu	opera	k1gFnSc4	opera
pocházely	pocházet	k5eAaImAgFnP	pocházet
často	často	k6eAd1	často
od	od	k7c2	od
dvou	dva	k4xCgInPc2	dva
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
libretistů	libretista	k1gMnPc2	libretista
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Apostola	Apostola	k1gFnSc1	Apostola
Zena	Zena	k1gFnSc1	Zena
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
-	-	kIx~	-
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
habsburského	habsburský	k2eAgMnSc2d1	habsburský
dvorního	dvorní	k2eAgMnSc2d1	dvorní
básníka	básník	k1gMnSc2	básník
Pietra	Pietr	k1gMnSc2	Pietr
Metastasia	Metastasius	k1gMnSc2	Metastasius
<g/>
;	;	kIx,	;
některá	některý	k3yIgNnPc4	některý
jejich	jejich	k3xOp3gNnPc4	jejich
dramata	drama	k1gNnPc4	drama
byla	být	k5eAaImAgFnS	být
zhudebněna	zhudebněn	k2eAgFnSc1d1	zhudebněna
i	i	k9	i
mnoha	mnoho	k4c7	mnoho
desítkami	desítka	k1gFnPc7	desítka
různých	různý	k2eAgMnPc2d1	různý
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
opery	opera	k1gFnSc2	opera
buffy	buffa	k1gFnSc2	buffa
měla	mít	k5eAaImAgFnS	mít
podobný	podobný	k2eAgInSc4d1	podobný
význam	význam	k1gInSc4	význam
libreta	libreto	k1gNnSc2	libreto
Carla	Carl	k1gMnSc4	Carl
Goldoniho	Goldoni	k1gMnSc4	Goldoni
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
v	v	k7c6	v
klasicistní	klasicistní	k2eAgFnSc6d1	klasicistní
době	doba	k1gFnSc6	doba
začínají	začínat	k5eAaImIp3nP	začínat
skladatelé	skladatel	k1gMnPc1	skladatel
i	i	k8xC	i
divadla	divadlo	k1gNnSc2	divadlo
hledat	hledat	k5eAaImF	hledat
nejen	nejen	k6eAd1	nejen
originální	originální	k2eAgFnSc4d1	originální
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
originální	originální	k2eAgFnSc4d1	originální
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
Gluckovi	Glucek	k1gMnSc3	Glucek
Ranieri	Ranier	k1gFnSc2	Ranier
de	de	k?	de
<g/>
'	'	kIx"	'
Calzabigi	Calzabigi	k1gNnSc1	Calzabigi
nebo	nebo	k8xC	nebo
Mozartovi	Mozartův	k2eAgMnPc1d1	Mozartův
Lorenzo	Lorenza	k1gFnSc5	Lorenza
da	da	k?	da
Ponte	Pont	k1gInSc5	Pont
<g/>
.	.	kIx.	.
</s>
<s>
Originalita	originalita	k1gFnSc1	originalita
a	a	k8xC	a
dramatická	dramatický	k2eAgFnSc1d1	dramatická
účinnost	účinnost	k1gFnSc1	účinnost
textu	text	k1gInSc2	text
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
zpěvoher	zpěvohra	k1gFnPc2	zpěvohra
s	s	k7c7	s
mluvenými	mluvený	k2eAgFnPc7d1	mluvená
pasážemi	pasáž	k1gFnPc7	pasáž
hraných	hraný	k2eAgInPc2d1	hraný
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
jazyce	jazyk	k1gInSc6	jazyk
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Dramatici	dramatik	k1gMnPc1	dramatik
jako	jako	k9	jako
John	John	k1gMnSc1	John
Gay	gay	k1gMnSc1	gay
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
ballad	ballad	k6eAd1	ballad
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
Charles-Simon	Charles-Simon	k1gInSc4	Charles-Simon
Favart	Favarta	k1gFnPc2	Favarta
a	a	k8xC	a
Michel-Jean	Michel-Jeana	k1gFnPc2	Michel-Jeana
Sedaine	Sedain	k1gInSc5	Sedain
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
opéru-comique	opéruomique	k1gFnSc4	opéru-comique
nebo	nebo	k8xC	nebo
Christian	Christian	k1gMnSc1	Christian
Felix	Felix	k1gMnSc1	Felix
Weiße	Weiße	k1gFnSc1	Weiße
a	a	k8xC	a
Emanuel	Emanuel	k1gMnSc1	Emanuel
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
pro	pro	k7c4	pro
německý	německý	k2eAgInSc4d1	německý
singspiel	singspiel	k1gInSc4	singspiel
měli	mít	k5eAaImAgMnP	mít
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
těchto	tento	k3xDgInPc2	tento
žánrů	žánr	k1gInPc2	žánr
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
době	doba	k1gFnSc6	doba
existovala	existovat	k5eAaImAgFnS	existovat
úzká	úzký	k2eAgFnSc1d1	úzká
spolupráce	spolupráce	k1gFnSc1	spolupráce
libretisty	libretista	k1gMnSc2	libretista
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
(	(	kIx(	(
<g/>
Lully	Lulla	k1gMnSc2	Lulla
–	–	k?	–
Philippe	Philipp	k1gInSc5	Philipp
Quinault	Quinault	k1gInSc4	Quinault
<g/>
,	,	kIx,	,
Galuppi	Galuppe	k1gFnSc4	Galuppe
–	–	k?	–
Goldoni	Goldoň	k1gFnSc3	Goldoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typičtější	typický	k2eAgNnPc1d2	typičtější
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
autoři	autor	k1gMnPc1	autor
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
ani	ani	k8xC	ani
nesetkali	setkat	k5eNaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k9	už
jedno	jeden	k4xCgNnSc1	jeden
libreto	libreto	k1gNnSc1	libreto
zhudebňuje	zhudebňovat	k5eAaImIp3nS	zhudebňovat
jediný	jediný	k2eAgMnSc1d1	jediný
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
oba	dva	k4xCgMnPc1	dva
tvůrci	tvůrce	k1gMnPc1	tvůrce
často	často	k6eAd1	často
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
užšího	úzký	k2eAgInSc2d2	užší
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Felice	Felice	k1gFnSc1	Felice
Romani	Romaň	k1gFnSc6	Romaň
napsal	napsat	k5eAaBmAgMnS	napsat
řadu	řada	k1gFnSc4	řada
oper	opera	k1gFnPc2	opera
pro	pro	k7c4	pro
italské	italský	k2eAgMnPc4d1	italský
skladatele	skladatel	k1gMnPc4	skladatel
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nejintenzivněji	intenzivně	k6eAd3	intenzivně
však	však	k9	však
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Bellinim	Bellini	k1gNnSc7	Bellini
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Francesco	Francesco	k6eAd1	Francesco
Maria	Maria	k1gFnSc1	Maria
Piave	Piaev	k1gFnSc2	Piaev
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Verdim	Verdi	k1gNnSc7	Verdi
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
italskými	italský	k2eAgMnPc7d1	italský
skladateli	skladatel	k1gMnPc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Luigi	Luig	k1gMnPc1	Luig
Illica	Illic	k1gInSc2	Illic
byl	být	k5eAaImAgMnS	být
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
libretista	libretista	k1gMnSc1	libretista
veristické	veristický	k2eAgFnSc2d1	veristická
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Eugè	Eugè	k1gFnSc2	Eugè
Scribe	Scrib	k1gInSc5	Scrib
udržoval	udržovat	k5eAaImAgInS	udržovat
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
a	a	k8xC	a
nejužší	úzký	k2eAgInPc4d3	nejužší
pracovní	pracovní	k2eAgInPc4d1	pracovní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Auberem	Auber	k1gInSc7	Auber
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
všichni	všechen	k3xTgMnPc1	všechen
skladatelé	skladatel	k1gMnPc1	skladatel
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
třetině	třetina	k1gFnSc6	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
skládali	skládat	k5eAaImAgMnP	skládat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc4	jeho
libreta	libreto	k1gNnPc4	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
zejména	zejména	k9	zejména
Jules	Jules	k1gMnSc1	Jules
Barbier	Barbier	k1gMnSc1	Barbier
a	a	k8xC	a
Michel	Michel	k1gMnSc1	Michel
Carré	Carrý	k2eAgFnSc2d1	Carrá
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
opeře	opera	k1gFnSc6	opera
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
působili	působit	k5eAaImAgMnP	působit
žádaní	žádaný	k2eAgMnPc1d1	žádaný
libretisté	libretista	k1gMnPc1	libretista
jako	jako	k8xS	jako
Karel	Karel	k1gMnSc1	Karel
Sabina	Sabina	k1gMnSc1	Sabina
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
nebo	nebo	k8xC	nebo
Emanuel	Emanuel	k1gMnSc1	Emanuel
Züngel	Züngel	k1gMnSc1	Züngel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
skladatelé	skladatel	k1gMnPc1	skladatel
si	se	k3xPyFc3	se
psali	psát	k5eAaImAgMnP	psát
libreta	libreto	k1gNnPc4	libreto
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
povýšil	povýšit	k5eAaPmAgMnS	povýšit
tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
na	na	k7c4	na
princip	princip	k1gInSc4	princip
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
svého	svůj	k3xOyFgInSc2	svůj
konceptu	koncept	k1gInSc2	koncept
"	"	kIx"	"
<g/>
Gesamtkunstwerk	Gesamtkunstwerk	k1gInSc1	Gesamtkunstwerk
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
vlastní	vlastní	k2eAgNnPc4d1	vlastní
libreta	libreto	k1gNnPc4	libreto
si	se	k3xPyFc3	se
psali	psát	k5eAaImAgMnP	psát
např.	např.	kA	např.
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
,	,	kIx,	,
Arrigo	Arrigo	k6eAd1	Arrigo
Boito	Boito	k1gNnSc1	Boito
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Foerster	Foerster	k1gMnSc1	Foerster
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Pfitzner	Pfitzner	k1gMnSc1	Pfitzner
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Hurník	Hurník	k1gMnSc1	Hurník
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Wagnerova	Wagnerův	k2eAgMnSc2d1	Wagnerův
syna	syn	k1gMnSc2	syn
Siegfrieda	Siegfried	k1gMnSc2	Siegfried
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
napsat	napsat	k5eAaPmF	napsat
operu	opera	k1gFnSc4	opera
na	na	k7c4	na
původně	původně	k6eAd1	původně
činoherní	činoherní	k2eAgInSc4d1	činoherní
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
skladatel	skladatel	k1gMnSc1	skladatel
sám	sám	k3xTgMnSc1	sám
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
či	či	k8xC	či
upraví	upravit	k5eAaPmIp3nS	upravit
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
učinili	učinit	k5eAaImAgMnP	učinit
např.	např.	kA	např.
Richard	Richarda	k1gFnPc2	Richarda
Strauss	Straussa	k1gFnPc2	Straussa
(	(	kIx(	(
<g/>
Salome	Salom	k1gInSc5	Salom
<g/>
,	,	kIx,	,
Elektra	Elektra	k1gFnSc1	Elektra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
(	(	kIx(	(
<g/>
Vojcek	Vojcka	k1gFnPc2	Vojcka
<g/>
,	,	kIx,	,
Lulu	lula	k1gFnSc4	lula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
(	(	kIx(	(
<g/>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
,	,	kIx,	,
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulos	k1gMnSc1	Makropulos
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Benjamin	Benjamin	k1gMnSc1	Benjamin
Britten	Britten	k2eAgMnSc1d1	Britten
(	(	kIx(	(
<g/>
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojanské	svatojanský	k2eAgFnSc2d1	Svatojanská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
skladatelů	skladatel	k1gMnPc2	skladatel
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
využívá	využívat	k5eAaImIp3nS	využívat
služeb	služba	k1gFnPc2	služba
libretistů	libretista	k1gMnPc2	libretista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
málo	málo	k1gNnSc1	málo
spisovatelů	spisovatel	k1gMnPc2	spisovatel
věnuje	věnovat	k5eAaImIp3nS	věnovat
psaní	psaní	k1gNnSc1	psaní
operních	operní	k2eAgNnPc2d1	operní
libret	libreto	k1gNnPc2	libreto
soustavně	soustavně	k6eAd1	soustavně
(	(	kIx(	(
<g/>
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
spolupráce	spolupráce	k1gFnSc1	spolupráce
Richarda	Richard	k1gMnSc2	Richard
Strausse	Strauss	k1gMnSc2	Strauss
a	a	k8xC	a
Hugo	Hugo	k1gMnSc1	Hugo
von	von	k1gInSc1	von
Hofmannsthala	Hofmannsthal	k1gMnSc2	Hofmannsthal
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
však	však	k9	však
operní	operní	k2eAgNnPc4d1	operní
libreta	libreto	k1gNnPc4	libreto
psali	psát	k5eAaImAgMnP	psát
mnozí	mnohý	k2eAgMnPc1d1	mnohý
významní	významný	k2eAgMnPc1d1	významný
spisovatelé	spisovatel	k1gMnPc1	spisovatel
jako	jako	k8xS	jako
Émile	Émile	k1gNnSc1	Émile
Zola	Zol	k1gInSc2	Zol
(	(	kIx(	(
<g/>
Bruneau	Bruneaus	k1gInSc2	Bruneaus
<g/>
:	:	kIx,	:
Messidor	Messidor	k1gInSc1	Messidor
<g/>
,	,	kIx,	,
Uragán	uragán	k1gInSc1	uragán
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
(	(	kIx(	(
<g/>
R.	R.	kA	R.
Strauss	Strauss	k1gInSc1	Strauss
<g/>
:	:	kIx,	:
Mlčenlivá	mlčenlivý	k2eAgFnSc1d1	mlčenlivá
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
(	(	kIx(	(
<g/>
Weill	Weill	k1gMnSc1	Weill
<g/>
:	:	kIx,	:
Třígrošová	třígrošový	k2eAgFnSc1d1	Třígrošová
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Dessau	Dessaa	k1gFnSc4	Dessaa
<g/>
:	:	kIx,	:
Odsouzení	odsouzení	k1gNnSc1	odsouzení
Lukullovo	Lukullův	k2eAgNnSc1d1	Lukullův
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Wystan	Wystan	k1gInSc1	Wystan
Hugh	Hugha	k1gFnPc2	Hugha
Auden	Auden	k1gInSc1	Auden
(	(	kIx(	(
<g/>
Britten	Britten	k2eAgMnSc1d1	Britten
<g/>
:	:	kIx,	:
Paul	Paul	k1gMnSc1	Paul
Bunyan	Bunyan	k1gMnSc1	Bunyan
<g/>
,	,	kIx,	,
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
prostopášníka	prostopášník	k1gMnSc2	prostopášník
<g/>
,	,	kIx,	,
Henze	Henze	k1gFnSc1	Henze
<g/>
:	:	kIx,	:
Elegie	elegie	k1gFnSc1	elegie
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
milence	milenec	k1gMnPc4	milenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řadě	řad	k1gInSc6	řad
libret	libreto	k1gNnPc2	libreto
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
libretistů	libretista	k1gMnPc2	libretista
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
dva	dva	k4xCgMnPc1	dva
libretisté	libretista	k1gMnPc1	libretista
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
rovnocenném	rovnocenný	k2eAgInSc6d1	rovnocenný
týmu	tým	k1gInSc6	tým
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Barbier	Barbier	k1gInSc1	Barbier
a	a	k8xC	a
Carré	Carrý	k2eAgFnPc1d1	Carrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jeden	jeden	k4xCgInSc1	jeden
psal	psát	k5eAaImAgInS	psát
text	text	k1gInSc4	text
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
ho	on	k3xPp3gMnSc4	on
zveršovával	zveršovávat	k5eAaImAgMnS	zveršovávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Luigi	Luig	k1gMnPc1	Luig
Illica	Illica	k1gMnSc1	Illica
a	a	k8xC	a
Giuseppe	Giusepp	k1gInSc5	Giusepp
Giacosa	Giacosa	k1gFnSc1	Giacosa
v	v	k7c6	v
operách	opera	k1gFnPc6	opera
pro	pro	k7c4	pro
Pucciniho	Puccini	k1gMnSc4	Puccini
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
psal	psát	k5eAaImAgMnS	psát
první	první	k4xOgNnSc4	první
libreto	libreto	k1gNnSc4	libreto
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
ho	on	k3xPp3gMnSc4	on
překládal	překládat	k5eAaImAgMnS	překládat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Wenzig	Wenzig	k1gMnSc1	Wenzig
a	a	k8xC	a
Ervín	Ervín	k1gMnSc1	Ervín
Špindler	Špindler	k1gMnSc1	Špindler
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jeden	jeden	k4xCgInSc4	jeden
platil	platit	k5eAaImAgMnS	platit
za	za	k7c4	za
inspirátora	inspirátor	k1gMnSc4	inspirátor
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c2	za
technického	technický	k2eAgMnSc2d1	technický
vykonavatele	vykonavatel	k1gMnSc2	vykonavatel
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
pracoval	pracovat	k5eAaImAgMnS	pracovat
Scribe	Scrib	k1gInSc5	Scrib
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
libretistů	libretista	k1gMnPc2	libretista
výsledkem	výsledek	k1gInSc7	výsledek
přepracovávání	přepracovávání	k1gNnSc2	přepracovávání
libreta	libreto	k1gNnSc2	libreto
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
:	:	kIx,	:
tak	tak	k9	tak
na	na	k7c6	na
Janáčkově	Janáčkův	k2eAgInSc6d1	Janáčkův
Výletu	výlet	k1gInSc6	výlet
pana	pan	k1gMnSc2	pan
Broučka	Brouček	k1gMnSc2	Brouček
do	do	k7c2	do
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
až	až	k9	až
šest	šest	k4xCc1	šest
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
ovšem	ovšem	k9	ovšem
do	do	k7c2	do
libreta	libreto	k1gNnSc2	libreto
často	často	k6eAd1	často
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
samotní	samotný	k2eAgMnPc1d1	samotný
komponisté	komponista	k1gMnPc1	komponista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
libretisty	libretista	k1gMnPc7	libretista
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
řadu	řada	k1gFnSc4	řada
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
carevnu	carevna	k1gFnSc4	carevna
Kateřinu	Kateřina	k1gFnSc4	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Novgorodskij	Novgorodskij	k1gMnSc1	Novgorodskij
bogatyr	bogatyr	k1gMnSc1	bogatyr
Bojeslajevič	Bojeslajevič	k1gInSc4	Bojeslajevič
1786	[number]	k4	1786
<g/>
,	,	kIx,	,
Gore-bogatyr	Goreogatyr	k1gInSc1	Gore-bogatyr
Kosometovič	Kosometovič	k1gInSc1	Kosometovič
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
Fedul	Fedul	k1gInSc1	Fedul
s	s	k7c7	s
děťmi	děť	k1gFnPc7	děť
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Helminu	Helmin	k1gInSc2	Helmin
von	von	k1gInSc1	von
Chézy	Chéza	k1gFnSc2	Chéza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
napsala	napsat	k5eAaBmAgFnS	napsat
Weberovu	Weberův	k2eAgFnSc4d1	Weberova
Euryanthu	Euryantha	k1gFnSc4	Euryantha
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
opery	opera	k1gFnSc2	opera
měly	mít	k5eAaImAgInP	mít
významné	významný	k2eAgInPc1d1	významný
místo	místo	k7c2	místo
libretistky	libretistka	k1gFnSc2	libretistka
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Hubička	hubička	k1gFnSc1	hubička
<g/>
,	,	kIx,	,
Tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
Čertova	čertův	k2eAgFnSc1d1	Čertova
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
Lejla	Lejla	k1gFnSc1	Lejla
<g/>
,	,	kIx,	,
Blaník	Blaník	k1gInSc1	Blaník
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Červinková-Riegrová	Červinková-Riegrová	k1gFnSc1	Červinková-Riegrová
(	(	kIx(	(
<g/>
Dimitrij	Dimitrij	k1gMnSc1	Dimitrij
<g/>
,	,	kIx,	,
Jakobín	jakobín	k1gMnSc1	jakobín
<g/>
,	,	kIx,	,
Zmařená	zmařený	k2eAgFnSc1d1	zmařená
svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anežka	Anežka	k1gFnSc1	Anežka
Schulzová	Schulzová	k1gFnSc1	Schulzová
(	(	kIx(	(
<g/>
Hedy	Heda	k1gFnPc1	Heda
<g/>
,	,	kIx,	,
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
Arkuna	Arkuna	k1gFnSc1	Arkuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
spoluprací	spolupráce	k1gFnSc7	spolupráce
spisovatelek	spisovatelka	k1gFnPc2	spisovatelka
Myfanwy	Myfanwa	k1gFnSc2	Myfanwa
Piper	Piper	k1gMnSc1	Piper
s	s	k7c7	s
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Brittenem	Britten	k1gMnSc7	Britten
(	(	kIx(	(
<g/>
Utahování	utahování	k1gNnSc3	utahování
šroubu	šroub	k1gInSc2	šroub
/	/	kIx~	/
The	The	k1gMnSc1	The
Turn	Turn	k1gMnSc1	Turn
of	of	k?	of
the	the	k?	the
Screw	Screw	k1gFnSc7	Screw
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Owen	Owen	k1gInSc1	Owen
Wingrave	Wingrav	k1gInSc5	Wingrav
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
/	/	kIx~	/
Death	Deatha	k1gFnPc2	Deatha
in	in	k?	in
Venice	Venice	k1gFnSc2	Venice
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ingeborg	Ingeborg	k1gMnSc1	Ingeborg
Bachmann	Bachmann	k1gMnSc1	Bachmann
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Wernerem	Werner	k1gMnSc7	Werner
Henzem	Henz	k1gMnSc7	Henz
(	(	kIx(	(
<g/>
Princ	princ	k1gMnSc1	princ
homburský	homburský	k2eAgMnSc1d1	homburský
/	/	kIx~	/
Der	drát	k5eAaImRp2nS	drát
Prinz	Prinz	k1gInSc4	Prinz
von	von	k1gInSc1	von
Homburg	Homburg	k1gInSc1	Homburg
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Mladý	mladý	k2eAgMnSc1d1	mladý
lord	lord	k1gMnSc1	lord
/	/	kIx~	/
Der	drát	k5eAaImRp2nS	drát
junge	junge	k1gInSc1	junge
Lord	lord	k1gMnSc1	lord
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
libreta	libreto	k1gNnSc2	libreto
některých	některý	k3yIgFnPc2	některý
známých	známý	k2eAgFnPc2d1	známá
oper	opera	k1gFnPc2	opera
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
napsaly	napsat	k5eAaBmAgFnP	napsat
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Adelheid	Adelheid	k1gInSc1	Adelheid
Wette	Wett	k1gInSc5	Wett
pro	pro	k7c4	pro
Humperdinckovu	Humperdinckův	k2eAgFnSc4d1	Humperdinckova
Perníkovou	perníkový	k2eAgFnSc4d1	Perníková
chaloupku	chaloupka	k1gFnSc4	chaloupka
<g/>
,	,	kIx,	,
Sidonie	Sidonie	k1gFnSc1	Sidonie
Gabriella	Gabriella	k1gFnSc1	Gabriella
Colettová	Colettová	k1gFnSc1	Colettová
pro	pro	k7c4	pro
Ravelovu	Ravelův	k2eAgFnSc4d1	Ravelova
Dítě	Dítě	k2eAgFnSc4d1	Dítě
a	a	k8xC	a
kouzla	kouzlo	k1gNnPc4	kouzlo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
opeře	opera	k1gFnSc6	opera
Marie	Maria	k1gFnSc2	Maria
Charousová-Gardavská	Charousová-Gardavský	k2eAgFnSc1d1	Charousová-Gardavský
pro	pro	k7c4	pro
Trojanův	Trojanův	k2eAgInSc4d1	Trojanův
Kolotoč	kolotoč	k1gInSc4	kolotoč
či	či	k8xC	či
Míla	Míla	k1gFnSc1	Míla
Mellanová	Mellanová	k1gFnSc1	Mellanová
pro	pro	k7c4	pro
Pauerova	Pauerův	k2eAgMnSc4d1	Pauerův
Žvanivého	žvanivý	k2eAgMnSc4d1	žvanivý
Slimejše	Slimejš	k1gMnSc4	Slimejš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výprava	výprava	k1gMnSc1	výprava
===	===	k?	===
</s>
</p>
<p>
<s>
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
výprava	výprava	k1gFnSc1	výprava
<g/>
)	)	kIx)	)
operního	operní	k2eAgNnSc2d1	operní
představení	představení	k1gNnSc2	představení
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc4d1	operní
scénografii	scénografie	k1gFnSc4	scénografie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
)	)	kIx)	)
a	a	k8xC	a
kostýmy	kostým	k1gInPc1	kostým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
italských	italský	k2eAgFnPc2d1	italská
dvorních	dvorní	k2eAgFnPc2d1	dvorní
slavností	slavnost	k1gFnPc2	slavnost
a	a	k8xC	a
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
výprava	výprava	k1gFnSc1	výprava
a	a	k8xC	a
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
překvapivé	překvapivý	k2eAgInPc4d1	překvapivý
scénické	scénický	k2eAgInPc4d1	scénický
efekty	efekt	k1gInPc4	efekt
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
stálých	stálý	k2eAgFnPc2d1	stálá
divadelních	divadelní	k2eAgFnPc2d1	divadelní
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
pokroku	pokrok	k1gInSc3	pokrok
zejména	zejména	k9	zejména
jevištní	jevištní	k2eAgFnSc2d1	jevištní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
proměny	proměna	k1gFnPc1	proměna
kulis	kulisa	k1gFnPc2	kulisa
<g/>
,	,	kIx,	,
propadla	propadlo	k1gNnSc2	propadlo
<g/>
,	,	kIx,	,
létací	létací	k2eAgInPc4d1	létací
stroje	stroj	k1gInPc4	stroj
apod.	apod.	kA	apod.
Dekorace	dekorace	k1gFnPc4	dekorace
barokní	barokní	k2eAgFnSc2d1	barokní
i	i	k8xC	i
klasicistní	klasicistní	k2eAgFnSc2d1	klasicistní
opery	opera	k1gFnSc2	opera
odrážely	odrážet	k5eAaImAgFnP	odrážet
aktuální	aktuální	k2eAgNnSc4d1	aktuální
architektonické	architektonický	k2eAgNnSc4d1	architektonické
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
(	(	kIx(	(
<g/>
nesnažili	snažit	k5eNaImAgMnP	snažit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
imitovat	imitovat	k5eAaBmF	imitovat
antiku	antika	k1gFnSc4	antika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgMnS	odehrávat
děj	děj	k1gInSc4	děj
většiny	většina	k1gFnSc2	většina
oper	opera	k1gFnPc2	opera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
byly	být	k5eAaImAgInP	být
fantazijní	fantazijní	k2eAgInPc1d1	fantazijní
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
operní	operní	k2eAgFnSc1d1	operní
divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
spektakulární	spektakulární	k2eAgFnSc7d1	spektakulární
podívanou	podívaná	k1gFnSc7	podívaná
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
napodobovat	napodobovat	k5eAaImF	napodobovat
i	i	k9	i
šlechta	šlechta	k1gFnSc1	šlechta
i	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Italskou	italský	k2eAgFnSc4d1	italská
divadelní	divadelní	k2eAgFnSc4d1	divadelní
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
šířila	šířit	k5eAaImAgFnS	šířit
například	například	k6eAd1	například
rodina	rodina	k1gFnSc1	rodina
Bibienů	Bibien	k1gMnPc2	Bibien
<g/>
;	;	kIx,	;
Giuseppe	Giusepp	k1gInSc5	Giusepp
Galli-Bibiena	Galli-Bibieno	k1gNnSc2	Galli-Bibieno
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
scénografii	scénografie	k1gFnSc4	scénografie
pro	pro	k7c4	pro
pražskou	pražský	k2eAgFnSc4d1	Pražská
inscenaci	inscenace	k1gFnSc4	inscenace
korunovační	korunovační	k2eAgFnSc2d1	korunovační
opery	opera	k1gFnSc2	opera
Johanna	Johann	k1gMnSc2	Johann
Josepha	Joseph	k1gMnSc2	Joseph
Fuxe	Fux	k1gMnSc2	Fux
Costanza	Costanz	k1gMnSc2	Costanz
e	e	k0	e
Fortezza	Fortezza	k1gFnSc1	Fortezza
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
barokní	barokní	k2eAgFnSc1d1	barokní
divadelní	divadelní	k2eAgFnSc1d1	divadelní
slavnost	slavnost	k1gFnSc1	slavnost
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
opera	opera	k1gFnSc1	opera
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
žánrem	žánr	k1gInSc7	žánr
vzdělané	vzdělaný	k2eAgFnSc2d1	vzdělaná
buržoazie	buržoazie	k1gFnSc2	buržoazie
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
považována	považován	k2eAgFnSc1d1	považována
realističnost	realističnost	k1gFnSc1	realističnost
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
operní	operní	k2eAgNnPc1d1	operní
divadla	divadlo	k1gNnPc1	divadlo
(	(	kIx(	(
<g/>
jako	jako	k9	jako
první	první	k4xOgNnPc4	první
divadla	divadlo	k1gNnPc4	divadlo
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
historickým	historický	k2eAgNnSc7d1	historické
<g/>
,	,	kIx,	,
archeologickým	archeologický	k2eAgNnSc7d1	Archeologické
i	i	k8xC	i
etnografickým	etnografický	k2eAgNnSc7d1	etnografické
studiem	studio	k1gNnSc7	studio
zajistit	zajistit	k5eAaPmF	zajistit
věrnost	věrnost	k1gFnSc4	věrnost
kulis	kulisa	k1gFnPc2	kulisa
i	i	k8xC	i
kostýmů	kostým	k1gInPc2	kostým
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgNnP	snažit
i	i	k9	i
menší	malý	k2eAgNnPc1d2	menší
divadla	divadlo	k1gNnPc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Malované	malovaný	k2eAgFnPc1d1	malovaná
kulisy	kulisa	k1gFnPc1	kulisa
však	však	k9	však
byly	být	k5eAaImAgFnP	být
nákladné	nákladný	k2eAgFnPc1d1	nákladná
a	a	k8xC	a
jen	jen	k9	jen
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
díla	dílo	k1gNnPc4	dílo
prověřená	prověřený	k2eAgNnPc4d1	prověřené
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
scénách	scéna	k1gFnPc6	scéna
nebo	nebo	k8xC	nebo
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
jméno	jméno	k1gNnSc4	jméno
autora	autor	k1gMnSc2	autor
zaručovalo	zaručovat	k5eAaImAgNnS	zaručovat
úspěch	úspěch	k1gInSc4	úspěch
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pořizovaly	pořizovat	k5eAaImAgFnP	pořizovat
pro	pro	k7c4	pro
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
inscenaci	inscenace	k1gFnSc4	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Převažovaly	převažovat	k5eAaImAgFnP	převažovat
typizované	typizovaný	k2eAgFnPc1d1	typizovaná
kulisy	kulisa	k1gFnPc1	kulisa
(	(	kIx(	(
<g/>
gotická	gotický	k2eAgFnSc1d1	gotická
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnSc1d1	skalní
soutěska	soutěska	k1gFnSc1	soutěska
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zavedení	zavedení	k1gNnSc1	zavedení
elektrického	elektrický	k2eAgNnSc2d1	elektrické
osvětlení	osvětlení	k1gNnSc2	osvětlení
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
odklonu	odklon	k1gInSc2	odklon
od	od	k7c2	od
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
plochých	plochý	k2eAgFnPc2d1	plochá
malovaných	malovaný	k2eAgFnPc2d1	malovaná
kulis	kulisa	k1gFnPc2	kulisa
k	k	k7c3	k
plastickému	plastický	k2eAgNnSc3d1	plastické
řešení	řešení	k1gNnSc3	řešení
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
pokračujícím	pokračující	k2eAgMnSc6d1	pokračující
duchu	duch	k1gMnSc6	duch
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
zavrhujícími	zavrhující	k2eAgFnPc7d1	zavrhující
realistického	realistický	k2eAgNnSc2d1	realistické
pojetí	pojetí	k1gNnSc3	pojetí
scény	scéna	k1gFnSc2	scéna
a	a	k8xC	a
kostýmů	kostým	k1gInPc2	kostým
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
abstraktního	abstraktní	k2eAgMnSc2d1	abstraktní
<g/>
,	,	kIx,	,
symbolického	symbolický	k2eAgNnSc2d1	symbolické
řešení	řešení	k1gNnSc2	řešení
zdůrazňujícího	zdůrazňující	k2eAgInSc2d1	zdůrazňující
základní	základní	k2eAgFnPc4d1	základní
myšlenky	myšlenka	k1gFnPc4	myšlenka
díla	dílo	k1gNnSc2	dílo
Adolphe	Adolph	k1gMnSc2	Adolph
Appia	Appius	k1gMnSc2	Appius
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Gordon	Gordon	k1gMnSc1	Gordon
Craig	Craig	k1gMnSc1	Craig
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Roller	Roller	k1gMnSc1	Roller
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
centrem	centr	k1gMnSc7	centr
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
scénografické	scénografický	k2eAgFnSc2d1	scénografická
avantgardy	avantgarda	k1gFnSc2	avantgarda
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
expresionismu	expresionismus	k1gInSc2	expresionismus
a	a	k8xC	a
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
byla	být	k5eAaImAgFnS	být
berlínská	berlínský	k2eAgFnSc1d1	Berlínská
Krollova	Krollův	k2eAgFnSc1d1	Krollův
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracovali	pracovat	k5eAaImAgMnP	pracovat
mj.	mj.	kA	mj.
Ewald	Ewald	k1gMnSc1	Ewald
Dülberg	Dülberg	k1gMnSc1	Dülberg
<g/>
,	,	kIx,	,
Caspar	Caspar	k1gMnSc1	Caspar
Neher	Neher	k1gMnSc1	Neher
<g/>
,	,	kIx,	,
László	László	k1gMnSc1	László
Moholy-Nagy	Moholy-Naga	k1gFnSc2	Moholy-Naga
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Schlemmer	Schlemmer	k1gMnSc1	Schlemmer
a	a	k8xC	a
Giorgio	Giorgio	k1gMnSc1	Giorgio
de	de	k?	de
Chirico	Chirico	k1gMnSc1	Chirico
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
intencích	intence	k1gFnPc6	intence
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
za	za	k7c2	za
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
Emil	Emil	k1gMnSc1	Emil
Preetorius	Preetorius	k1gMnSc1	Preetorius
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
inscenacích	inscenace	k1gFnPc6	inscenace
pro	pro	k7c4	pro
Hudební	hudební	k2eAgFnPc4d1	hudební
slavnosti	slavnost	k1gFnPc4	slavnost
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
tradiční	tradiční	k2eAgFnSc3d1	tradiční
realistické	realistický	k2eAgFnSc3d1	realistická
operní	operní	k2eAgFnSc3d1	operní
výpravě	výprava	k1gFnSc3	výprava
výtvarníci	výtvarník	k1gMnPc1	výtvarník
jako	jako	k8xC	jako
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc6	Maurika
Utrillo	Utrillo	k1gNnSc4	Utrillo
nebo	nebo	k8xC	nebo
Vasilij	Vasilij	k1gFnSc4	Vasilij
Kandinskij	Kandinskij	k1gFnSc2	Kandinskij
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
této	tento	k3xDgFnSc2	tento
umělecké	umělecký	k2eAgFnSc2d1	umělecká
individualizace	individualizace	k1gFnSc2	individualizace
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
sepětí	sepětí	k1gNnSc1	sepětí
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
řešení	řešení	k1gNnSc2	řešení
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
jediným	jediný	k2eAgNnSc7d1	jediné
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
inscenací	inscenace	k1gFnSc7	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
Přestaly	přestat	k5eAaPmAgInP	přestat
se	se	k3xPyFc4	se
tak	tak	k9	tak
používat	používat	k5eAaImF	používat
typizované	typizovaný	k2eAgFnPc4d1	typizovaná
realistické	realistický	k2eAgFnPc4d1	realistická
kulisy	kulisa	k1gFnPc4	kulisa
sloužící	sloužící	k1gFnSc2	sloužící
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
díla	dílo	k1gNnPc4	dílo
odehrávající	odehrávající	k2eAgNnPc1d1	odehrávající
se	se	k3xPyFc4	se
rámcově	rámcově	k6eAd1	rámcově
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejnovější	nový	k2eAgFnSc6d3	nejnovější
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stránka	stránka	k1gFnSc1	stránka
operní	operní	k2eAgFnSc2d1	operní
inscenace	inscenace	k1gFnSc2	inscenace
na	na	k7c6	na
režisérově	režisérův	k2eAgInSc6d1	režisérův
uměleckém	umělecký	k2eAgInSc6d1	umělecký
záměru	záměr	k1gInSc6	záměr
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scénografa	scénograf	k1gMnSc2	scénograf
se	se	k3xPyFc4	se
často	často	k6eAd1	často
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c6	v
jedněch	jeden	k4xCgFnPc6	jeden
rukou	ruka	k1gFnPc6	ruka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Wieland	Wielanda	k1gFnPc2	Wielanda
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gMnSc5	Jean-Pierr
Ponnelle	Ponnell	k1gMnSc5	Ponnell
<g/>
,	,	kIx,	,
Franco	Franco	k6eAd1	Franco
Zefferelli	Zefferell	k1gMnPc1	Zefferell
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
realistická	realistický	k2eAgFnSc1d1	realistická
i	i	k8xC	i
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
výprava	výprava	k1gMnSc1	výprava
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgMnSc1	obojí
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
případným	případný	k2eAgInSc7d1	případný
místním	místní	k2eAgInSc7d1	místní
či	či	k8xC	či
časovým	časový	k2eAgInSc7d1	časový
posunem	posun	k1gInSc7	posun
původního	původní	k2eAgInSc2d1	původní
děje	děj	k1gInSc2	děj
podle	podle	k7c2	podle
intence	intence	k1gFnSc2	intence
režiséra	režisér	k1gMnSc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
režie	režie	k1gFnSc1	režie
bývá	bývat	k5eAaImIp3nS	bývat
právě	právě	k9	právě
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
řešení	řešení	k1gNnSc1	řešení
moderních	moderní	k2eAgFnPc2d1	moderní
operních	operní	k2eAgFnPc2d1	operní
inscenací	inscenace	k1gFnPc2	inscenace
předmětem	předmět	k1gInSc7	předmět
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
scénografie	scénografie	k1gFnSc1	scénografie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
operní	operní	k2eAgFnSc2d1	operní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
tradici	tradice	k1gFnSc4	tradice
podepřenou	podepřený	k2eAgFnSc4d1	podepřená
konáním	konání	k1gNnSc7	konání
scénografického	scénografický	k2eAgInSc2d1	scénografický
festivalu	festival	k1gInSc2	festival
Pražské	pražský	k2eAgNnSc1d1	Pražské
kvadrienále	kvadrienále	k1gNnSc1	kvadrienále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
operních	operní	k2eAgFnPc6d1	operní
inscenacích	inscenace	k1gFnPc6	inscenace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
podílel	podílet	k5eAaImAgMnS	podílet
například	například	k6eAd1	například
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Kotěra	Kotěra	k1gFnSc1	Kotěra
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgMnPc2d1	významný
malířů	malíř	k1gMnPc2	malíř
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Lada	Lada	k1gFnSc1	Lada
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Muzika	muzika	k1gFnSc1	muzika
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
nebo	nebo	k8xC	nebo
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
scénograf	scénograf	k1gMnSc1	scénograf
Josef	Josef	k1gMnSc1	Josef
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
evropské	evropský	k2eAgFnSc2d1	Evropská
poválečné	poválečný	k2eAgFnSc2d1	poválečná
operní	operní	k2eAgFnSc2d1	operní
scénografie	scénografie	k1gFnSc2	scénografie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
scénografy	scénograf	k1gMnPc4	scénograf
působící	působící	k2eAgMnPc4d1	působící
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
soustřeďující	soustřeďující	k2eAgMnSc1d1	soustřeďující
se	se	k3xPyFc4	se
na	na	k7c4	na
operu	opera	k1gFnSc4	opera
patří	patřit	k5eAaImIp3nS	patřit
Daniel	Daniel	k1gMnSc1	Daniel
Dvořák	Dvořák	k1gMnSc1	Dvořák
nebo	nebo	k8xC	nebo
Rocc	Rocc	k1gFnSc1	Rocc
<g/>
,	,	kIx,	,
v	v	k7c6	v
operní	operní	k2eAgFnSc6d1	operní
výpravě	výprava	k1gFnSc6	výprava
se	se	k3xPyFc4	se
však	však	k9	však
nadále	nadále	k6eAd1	nadále
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
i	i	k9	i
výtvarní	výtvarný	k2eAgMnPc1d1	výtvarný
umělci	umělec	k1gMnPc1	umělec
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
nebo	nebo	k8xC	nebo
Bořek	Bořek	k1gMnSc1	Bořek
Šípek	Šípek	k1gMnSc1	Šípek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazyk	jazyk	k1gInSc1	jazyk
představení	představení	k1gNnSc1	představení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
operního	operní	k2eAgInSc2d1	operní
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
šířili	šířit	k5eAaImAgMnP	šířit
operu	opera	k1gFnSc4	opera
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
především	především	k6eAd1	především
italští	italský	k2eAgMnPc1d1	italský
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
zpěváci	zpěvák	k1gMnPc1	zpěvák
a	a	k8xC	a
celé	celý	k2eAgFnPc1d1	celá
operní	operní	k2eAgFnPc1d1	operní
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zprvu	zprvu	k6eAd1	zprvu
především	především	k6eAd1	především
na	na	k7c4	na
zámožné	zámožný	k2eAgInPc4d1	zámožný
panovnické	panovnický	k2eAgInPc4d1	panovnický
a	a	k8xC	a
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
dvory	dvůr	k1gInPc4	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
předpokládat	předpokládat	k5eAaImF	předpokládat
znalost	znalost	k1gFnSc4	znalost
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
i	i	k9	i
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
hry	hra	k1gFnSc2	hra
hrána	hrát	k5eAaImNgFnS	hrát
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
;	;	kIx,	;
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
italští	italský	k2eAgMnPc1d1	italský
libretisté	libretista	k1gMnPc1	libretista
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Apostolo	Apostola	k1gFnSc5	Apostola
Zeno	Zeno	k1gNnSc4	Zeno
a	a	k8xC	a
Pietro	Pietro	k1gNnSc4	Pietro
Metastasio	Metastasio	k6eAd1	Metastasio
působili	působit	k5eAaImAgMnP	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
i	i	k9	i
skladatelé	skladatel	k1gMnPc1	skladatel
jako	jako	k8xS	jako
Händel	Händlo	k1gNnPc2	Händlo
<g/>
,	,	kIx,	,
Hasse	Hasse	k1gFnSc1	Hasse
<g/>
,	,	kIx,	,
Graun	Graun	k1gMnSc1	Graun
<g/>
,	,	kIx,	,
Gluck	Gluck	k1gMnSc1	Gluck
<g/>
,	,	kIx,	,
Mozart	Mozart	k1gMnSc1	Mozart
či	či	k8xC	či
pražský	pražský	k2eAgMnSc1d1	pražský
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Koželuh	koželuh	k1gMnSc1	koželuh
skládali	skládat	k5eAaImAgMnP	skládat
vážné	vážný	k2eAgFnPc4d1	vážná
opery	opera	k1gFnPc4	opera
na	na	k7c4	na
italské	italský	k2eAgInPc4d1	italský
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
přitom	přitom	k6eAd1	přitom
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
libreto	libreto	k1gNnSc1	libreto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
i	i	k8xC	i
s	s	k7c7	s
překladem	překlad	k1gInSc7	překlad
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mohlo	moct	k5eAaImAgNnS	moct
při	při	k7c6	při
představení	představení	k1gNnSc6	představení
číst	číst	k5eAaImF	číst
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
orientovat	orientovat	k5eAaBmF	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvoherní	zpěvoherní	k2eAgFnSc1d1	zpěvoherní
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
v	v	k7c6	v
"	"	kIx"	"
<g/>
nízkých	nízký	k2eAgInPc6d1	nízký
<g/>
"	"	kIx"	"
žánrech	žánr	k1gInPc6	žánr
–	–	k?	–
často	často	k6eAd1	často
nenazývaných	nazývaný	k2eNgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samotné	samotný	k2eAgNnSc1d1	samotné
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
implikovalo	implikovat	k5eAaImAgNnS	implikovat
italský	italský	k2eAgInSc4d1	italský
text	text	k1gInSc4	text
–	–	k?	–
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
ve	v	k7c6	v
dvorních	dvorní	k2eAgNnPc6d1	dvorní
divadlech	divadlo	k1gNnPc6	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
divadlech	divadlo	k1gNnPc6	divadlo
určených	určený	k2eAgFnPc2d1	určená
širším	široký	k2eAgFnPc3d2	širší
vrstvám	vrstva	k1gFnPc3	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
dominovala	dominovat	k5eAaImAgFnS	dominovat
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
i	i	k9	i
italská	italský	k2eAgFnSc1d1	italská
opera	opera	k1gFnSc1	opera
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
trvale	trvale	k6eAd1	trvale
přítomna	přítomen	k2eAgFnSc1d1	přítomna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
myšlenek	myšlenka	k1gFnPc2	myšlenka
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
postupné	postupný	k2eAgFnSc2d1	postupná
demokratizace	demokratizace	k1gFnSc2	demokratizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
operního	operní	k2eAgNnSc2d1	operní
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
podporována	podporován	k2eAgFnSc1d1	podporována
tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
jazycích	jazyk	k1gInPc6	jazyk
i	i	k8xC	i
uvádění	uvádění	k1gNnSc6	uvádění
cizích	cizí	k2eAgInPc2d1	cizí
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
francouzských	francouzský	k2eAgFnPc2d1	francouzská
a	a	k8xC	a
italských	italský	k2eAgFnPc2d1	italská
<g/>
)	)	kIx)	)
oper	opera	k1gFnPc2	opera
v	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Italština	italština	k1gFnSc1	italština
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
mizí	mizet	k5eAaImIp3nS	mizet
z	z	k7c2	z
divadel	divadlo	k1gNnPc2	divadlo
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
i	i	k8xC	i
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
nepřetržitě	přetržitě	k6eNd1	přetržitě
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
jen	jen	k9	jen
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Opera	opera	k1gFnSc1	opera
House	house	k1gNnSc1	house
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
výlučně	výlučně	k6eAd1	výlučně
italsky	italsky	k6eAd1	italsky
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
částečně	částečně	k6eAd1	částečně
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
nahradila	nahradit	k5eAaPmAgFnS	nahradit
italštinu	italština	k1gFnSc4	italština
jako	jako	k8xC	jako
operní	operní	k2eAgInSc4d1	operní
jazyk	jazyk	k1gInSc4	jazyk
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
městech	město	k1gNnPc6	město
mimo	mimo	k6eAd1	mimo
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgNnPc1d1	mluvící
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
)	)	kIx)	)
městské	městský	k2eAgFnSc2d1	městská
elity	elita	k1gFnSc2	elita
němčinu	němčina	k1gFnSc4	němčina
ovládaly	ovládat	k5eAaImAgInP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Repertoár	repertoár	k1gInSc1	repertoár
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
jazycích	jazyk	k1gInPc6	jazyk
právě	právě	k6eAd1	právě
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
jako	jako	k8xC	jako
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejprestižnějším	prestižní	k2eAgInSc6d3	nejprestižnější
uměleckém	umělecký	k2eAgInSc6d1	umělecký
oboru	obor	k1gInSc6	obor
se	se	k3xPyFc4	se
ale	ale	k9	ale
staly	stát	k5eAaPmAgInP	stát
významným	významný	k2eAgInSc7d1	významný
znakem	znak	k1gInSc7	znak
kulturní	kulturní	k2eAgFnSc2d1	kulturní
samostatnosti	samostatnost	k1gFnSc2	samostatnost
řady	řada	k1gFnSc2	řada
evropských	evropský	k2eAgInPc2d1	evropský
národů	národ	k1gInPc2	národ
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
probuzení	probuzení	k1gNnSc2	probuzení
a	a	k8xC	a
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
systematické	systematický	k2eAgNnSc1d1	systematické
překládání	překládání	k1gNnSc1	překládání
všech	všecek	k3xTgNnPc2	všecek
libret	libreto	k1gNnPc2	libreto
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgNnPc2d1	Evropské
divadel	divadlo	k1gNnPc2	divadlo
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
.	.	kIx.	.
</s>
<s>
Publikování	publikování	k1gNnSc1	publikování
libret	libreto	k1gNnPc2	libreto
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gNnSc2	jejich
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
představení	představení	k1gNnSc1	představení
skončilo	skončit	k5eAaPmAgNnS	skončit
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
zatemnění	zatemnění	k1gNnSc4	zatemnění
hlediště	hlediště	k1gNnSc2	hlediště
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
divadlech	divadlo	k1gNnPc6	divadlo
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
však	však	k9	však
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
uvádění	uvádění	k1gNnSc1	uvádění
oper	opera	k1gFnPc2	opera
v	v	k7c6	v
originálním	originální	k2eAgInSc6d1	originální
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
souviselo	souviset	k5eAaImAgNnS	souviset
i	i	k9	i
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
původního	původní	k2eAgInSc2d1	původní
anglickojazyčného	anglickojazyčný	k2eAgInSc2d1	anglickojazyčný
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vedle	vedle	k7c2	vedle
italštiny	italština	k1gFnSc2	italština
též	též	k9	též
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
;	;	kIx,	;
jiné	jiný	k2eAgInPc1d1	jiný
jazyky	jazyk	k1gInPc1	jazyk
jako	jako	k8xC	jako
ruština	ruština	k1gFnSc1	ruština
nebo	nebo	k8xC	nebo
čeština	čeština	k1gFnSc1	čeština
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
až	až	k9	až
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
přešla	přejít	k5eAaPmAgFnS	přejít
další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
evropská	evropský	k2eAgFnSc1d1	Evropská
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
znovuotevřená	znovuotevřený	k2eAgFnSc1d1	znovuotevřená
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
pod	pod	k7c7	pod
Herbertem	Herbert	k1gInSc7	Herbert
von	von	k1gInSc1	von
Karajanem	Karajan	k1gMnSc7	Karajan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
snaha	snaha	k1gFnSc1	snaha
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
mobilitu	mobilita	k1gFnSc4	mobilita
špičkových	špičkový	k2eAgMnPc2d1	špičkový
interpretů	interpret	k1gMnPc2	interpret
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
překladů	překlad	k1gInPc2	překlad
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
tutéž	týž	k3xTgFnSc4	týž
roli	role	k1gFnSc4	role
učit	učit	k5eAaImF	učit
v	v	k7c6	v
několika	několik	k4yIc6	několik
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
plně	plně	k6eAd1	plně
globalizované	globalizovaný	k2eAgFnSc6d1	globalizovaná
společnosti	společnost	k1gFnSc6	společnost
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
do	do	k7c2	do
menších	malý	k2eAgNnPc2d2	menší
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
umožnění	umožnění	k1gNnSc4	umožnění
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
titulků	titulek	k1gInPc2	titulek
<g/>
:	:	kIx,	:
poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
použila	použít	k5eAaPmAgFnS	použít
při	při	k7c6	při
inscenaci	inscenace	k1gFnSc6	inscenace
Straussovy	Straussův	k2eAgInPc1d1	Straussův
Elektry	Elektr	k1gInPc1	Elektr
Canadian	Canadiana	k1gFnPc2	Canadiana
Opera	opera	k1gFnSc1	opera
Company	Compana	k1gFnSc2	Compana
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
přejala	přejmout	k5eAaPmAgFnS	přejmout
i	i	k9	i
Metropolitan	metropolitan	k1gInSc4	metropolitan
Opera	opera	k1gFnSc1	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nad	nad	k7c7	nad
jevištěm	jeviště	k1gNnSc7	jeviště
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
divadlech	divadlo	k1gNnPc6	divadlo
však	však	k8xC	však
též	též	k9	též
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
obrazovkách	obrazovka	k1gFnPc6	obrazovka
před	před	k7c7	před
individuálními	individuální	k2eAgNnPc7d1	individuální
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
menší	malý	k2eAgNnPc1d2	menší
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
některá	některý	k3yIgNnPc4	některý
významná	významný	k2eAgNnPc4d1	významné
divadla	divadlo	k1gNnPc4	divadlo
v	v	k7c6	v
operních	operní	k2eAgMnPc6d1	operní
centrech	centr	k1gMnPc6	centr
(	(	kIx(	(
<g/>
English	English	k1gInSc1	English
National	National	k1gMnSc2	National
Opera	opera	k1gFnSc1	opera
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
Opera	opera	k1gFnSc1	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Komische	Komisch	k1gInSc2	Komisch
Oper	opera	k1gFnPc2	opera
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Volksoper	Volksoper	k1gInSc1	Volksoper
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Théâtre	Théâtr	k1gInSc5	Théâtr
national	nationat	k5eAaPmAgInS	nationat
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Opéra-Comique	Opéra-Comiqu	k1gMnSc2	Opéra-Comiqu
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Staatstheater	Staatstheater	k1gMnSc1	Staatstheater
am	am	k?	am
Gärtnerplatz	Gärtnerplatz	k1gMnSc1	Gärtnerplatz
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nadále	nadále	k6eAd1	nadále
soustavně	soustavně	k6eAd1	soustavně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
opery	opera	k1gFnPc1	opera
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otázka	otázka	k1gFnSc1	otázka
uvádění	uvádění	k1gNnSc2	uvádění
oper	opera	k1gFnPc2	opera
v	v	k7c6	v
originálním	originální	k2eAgInSc6d1	originální
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
diskutována	diskutován	k2eAgFnSc1d1	diskutována
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
svědčí	svědčit	k5eAaImIp3nS	svědčit
jednak	jednak	k8xC	jednak
praktičnost	praktičnost	k1gFnSc1	praktičnost
a	a	k8xC	a
hospodárnost	hospodárnost	k1gFnSc1	hospodárnost
(	(	kIx(	(
<g/>
zpěváci	zpěvák	k1gMnPc1	zpěvák
mohou	moct	k5eAaImIp3nP	moct
zpívat	zpívat	k5eAaImF	zpívat
tutéž	týž	k3xTgFnSc4	týž
roli	role	k1gFnSc4	role
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pořizovat	pořizovat	k5eAaImF	pořizovat
překlad	překlad	k1gInSc4	překlad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
umělecké	umělecký	k2eAgInPc1d1	umělecký
důvody	důvod	k1gInPc1	důvod
<g/>
:	:	kIx,	:
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
psána	psát	k5eAaImNgFnS	psát
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c6	na
melodii	melodie	k1gFnSc6	melodie
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
význam	význam	k1gInSc4	význam
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
kvalitu	kvalita	k1gFnSc4	kvalita
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hlásek	hláska	k1gFnPc2	hláska
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
hudbu	hudba	k1gFnSc4	hudba
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
"	"	kIx"	"
<g/>
nápěvcích	nápěvek	k1gInPc6	nápěvek
<g/>
"	"	kIx"	"
mluvy	mluva	k1gFnPc1	mluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nelze	lze	k6eNd1	lze
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
reflektovat	reflektovat	k5eAaImF	reflektovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
stránky	stránka	k1gFnSc2	stránka
spojení	spojení	k1gNnSc2	spojení
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
pozbývají	pozbývat	k5eAaImIp3nP	pozbývat
ze	z	k7c2	z
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
účinku	účinek	k1gInSc2	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
text	text	k1gInSc1	text
zpíván	zpíván	k2eAgMnSc1d1	zpíván
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
obecenstvu	obecenstvo	k1gNnSc6	obecenstvo
nesrozumitelný	srozumitelný	k2eNgMnSc1d1	nesrozumitelný
a	a	k8xC	a
sami	sám	k3xTgMnPc1	sám
pěvci	pěvec	k1gMnPc1	pěvec
jej	on	k3xPp3gNnSc4	on
mnohdy	mnohdy	k6eAd1	mnohdy
stěží	stěží	k6eAd1	stěží
ovládají	ovládat	k5eAaImIp3nP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
titulky	titulek	k1gInPc1	titulek
nejsou	být	k5eNaImIp3nP	být
přijímány	přijímat	k5eAaImNgInP	přijímat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
i	i	k9	i
scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
synchronizovány	synchronizovat	k5eAaBmNgFnP	synchronizovat
se	s	k7c7	s
zpívaným	zpívaný	k2eAgInSc7d1	zpívaný
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
nevčasné	včasný	k2eNgFnPc4d1	nevčasná
reakce	reakce	k1gFnPc4	reakce
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
překonávají	překonávat	k5eAaImIp3nP	překonávat
nesrozumitelnost	nesrozumitelnost	k1gFnSc4	nesrozumitelnost
zpěvu	zpěv	k1gInSc2	zpěv
pociťovanou	pociťovaný	k2eAgFnSc7d1	pociťovaná
diváky	divák	k1gMnPc7	divák
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
jazyce	jazyk	k1gInSc6	jazyk
často	často	k6eAd1	často
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
cizím	cizí	k2eAgMnSc6d1	cizí
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
zavedení	zavedení	k1gNnSc1	zavedení
řadě	řada	k1gFnSc6	řada
divadel	divadlo	k1gNnPc2	divadlo
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problém	problém	k1gInSc1	problém
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mísí	mísit	k5eAaImIp3nP	mísit
zpívaný	zpívaný	k2eAgInSc4d1	zpívaný
text	text	k1gInSc4	text
s	s	k7c7	s
mluveným	mluvený	k2eAgNnSc7d1	mluvené
slovem	slovo	k1gNnSc7	slovo
(	(	kIx(	(
<g/>
singspiel	singspiel	k1gInSc1	singspiel
<g/>
,	,	kIx,	,
opéra-comique	opéraomique	k1gInSc1	opéra-comique
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgInPc1d1	další
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
žánry	žánr	k1gInPc1	žánr
jako	jako	k8xS	jako
opereta	opereta	k1gFnSc1	opereta
<g/>
,	,	kIx,	,
zarzuela	zarzuela	k1gFnSc1	zarzuela
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zpěváci	zpěvák	k1gMnPc1	zpěvák
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
nemohou	moct	k5eNaImIp3nP	moct
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
plynně	plynně	k6eAd1	plynně
hovořit	hovořit	k5eAaImF	hovořit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
díla	dílo	k1gNnPc1	dílo
uváděna	uvádět	k5eAaImNgNnP	uvádět
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
kombinován	kombinován	k2eAgInSc4d1	kombinován
originální	originální	k2eAgInSc4d1	originální
jazyk	jazyk	k1gInSc4	jazyk
zpívaných	zpívaný	k2eAgFnPc2d1	zpívaná
částí	část	k1gFnPc2	část
s	s	k7c7	s
domácím	domácí	k2eAgInSc7d1	domácí
jazykem	jazyk	k1gInSc7	jazyk
pro	pro	k7c4	pro
mluvené	mluvený	k2eAgInPc4d1	mluvený
dialogy	dialog	k1gInPc4	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
repertoárech	repertoár	k1gInPc6	repertoár
velkých	velký	k2eAgFnPc2d1	velká
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
scén	scéna	k1gFnPc2	scéna
taková	takový	k3xDgNnPc1	takový
díla	dílo	k1gNnPc1	dílo
většinou	většinou	k6eAd1	většinou
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Financování	financování	k1gNnSc1	financování
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
kulturních	kulturní	k2eAgFnPc2d1	kulturní
událostí	událost	k1gFnPc2	událost
sloužících	sloužící	k2eAgFnPc2d1	sloužící
k	k	k7c3	k
reprezentaci	reprezentace	k1gFnSc3	reprezentace
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
brzy	brzy	k6eAd1	brzy
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
její	její	k3xOp3gFnSc4	její
finanční	finanční	k2eAgFnSc4d1	finanční
soběstačnost	soběstačnost	k1gFnSc4	soběstačnost
nikdy	nikdy	k6eAd1	nikdy
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
operními	operní	k2eAgNnPc7d1	operní
divadly	divadlo	k1gNnPc7	divadlo
financovanými	financovaný	k2eAgMnPc7d1	financovaný
panovníky	panovník	k1gMnPc7	panovník
či	či	k8xC	či
majetnými	majetný	k2eAgMnPc7d1	majetný
šlechtici	šlechtic	k1gMnPc7	šlechtic
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
kryly	krýt	k5eAaImAgFnP	krýt
zcela	zcela	k6eAd1	zcela
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
ekonomicky	ekonomicky	k6eAd1	ekonomicky
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
přežití	přežití	k1gNnSc4	přežití
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
zejména	zejména	k9	zejména
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
barokní	barokní	k2eAgFnSc1d1	barokní
opera	opera	k1gFnSc1	opera
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
nepotřebovala	potřebovat	k5eNaImAgFnS	potřebovat
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
počet	počet	k1gInSc4	počet
zpěváků	zpěvák	k1gMnPc2	zpěvák
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kuse	kus	k1gInSc6	kus
zřídka	zřídka	k6eAd1	zřídka
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
šest	šest	k4xCc4	šest
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgNnPc4d1	soukromé
divadla	divadlo	k1gNnPc4	divadlo
proto	proto	k8xC	proto
provozovala	provozovat	k5eAaImAgFnS	provozovat
častěji	často	k6eAd2	často
nenáročné	náročný	k2eNgInPc4d1	nenáročný
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgInPc4d2	nižší
<g/>
"	"	kIx"	"
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
vaudeville	vaudeville	k1gInSc4	vaudeville
<g/>
,	,	kIx,	,
ballad	ballad	k6eAd1	ballad
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
singspiel	singspiel	k1gInSc1	singspiel
nebo	nebo	k8xC	nebo
opéra	opéra	k1gFnSc1	opéra
comique	comique	k1gFnSc1	comique
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
společenských	společenský	k2eAgInPc2d1	společenský
poměrů	poměr	k1gInPc2	poměr
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
osvícenské	osvícenský	k2eAgFnSc2d1	osvícenská
koncepce	koncepce	k1gFnSc2	koncepce
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
jako	jako	k8xC	jako
kulturního	kulturní	k2eAgInSc2d1	kulturní
statku	statek	k1gInSc2	statek
spíše	spíše	k9	spíše
než	než	k8xS	než
pouhé	pouhý	k2eAgFnPc1d1	pouhá
zábavy	zábava	k1gFnPc1	zábava
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
nástup	nástup	k1gInSc1	nástup
romantické	romantický	k2eAgFnSc2d1	romantická
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
náročnější	náročný	k2eAgNnSc1d2	náročnější
na	na	k7c4	na
hudební	hudební	k2eAgNnSc4d1	hudební
provedení	provedení	k1gNnSc4	provedení
i	i	k8xC	i
jevištní	jevištní	k2eAgFnSc4d1	jevištní
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
změnily	změnit	k5eAaPmAgInP	změnit
zásadně	zásadně	k6eAd1	zásadně
i	i	k9	i
divadelní	divadelní	k2eAgInSc4d1	divadelní
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Stálá	stálý	k2eAgFnSc1d1	stálá
soukromá	soukromý	k2eAgFnSc1d1	soukromá
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
šlechtická	šlechtický	k2eAgNnPc4d1	šlechtické
divadla	divadlo	k1gNnPc4	divadlo
buď	buď	k8xC	buď
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
veřejnými	veřejný	k2eAgNnPc7d1	veřejné
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
původně	původně	k6eAd1	původně
soukromě	soukromě	k6eAd1	soukromě
založené	založený	k2eAgNnSc4d1	založené
Nosticovo	Nosticův	k2eAgNnSc4d1	Nosticovo
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1798	[number]	k4	1798
převzala	převzít	k5eAaPmAgFnS	převzít
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
korporace	korporace	k1gFnSc2	korporace
zemská	zemský	k2eAgFnSc1d1	zemská
stavovská	stavovský	k2eAgFnSc1d1	stavovská
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
pronajímala	pronajímat	k5eAaImAgFnS	pronajímat
soukromým	soukromý	k2eAgMnPc3d1	soukromý
provozovatelům	provozovatel	k1gMnPc3	provozovatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
vyhrazenými	vyhrazený	k2eAgInPc7d1	vyhrazený
úkoly	úkol	k1gInPc7	úkol
<g/>
,	,	kIx,	,
povinnostmi	povinnost	k1gFnPc7	povinnost
a	a	k8xC	a
zásadami	zásada	k1gFnPc7	zásada
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
kontrolou	kontrola	k1gFnSc7	kontrola
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
příspěvkem	příspěvek	k1gInSc7	příspěvek
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
význam	význam	k1gInSc1	význam
postupně	postupně	k6eAd1	postupně
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
amatérské	amatérský	k2eAgNnSc4d1	amatérské
divadlo	divadlo	k1gNnSc4	divadlo
včetně	včetně	k7c2	včetně
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
přešla	přejít	k5eAaPmAgFnS	přejít
většina	většina	k1gFnSc1	většina
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
postupně	postupně	k6eAd1	postupně
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
obcí	obec	k1gFnPc2	obec
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
veřejným	veřejný	k2eAgInSc7d1	veřejný
statutem	statut	k1gInSc7	statut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
situací	situace	k1gFnPc2	situace
kritických	kritický	k2eAgFnPc2d1	kritická
pro	pro	k7c4	pro
finanční	finanční	k2eAgFnSc4d1	finanční
situaci	situace	k1gFnSc4	situace
divadel	divadlo	k1gNnPc2	divadlo
jako	jako	k8xC	jako
obě	dva	k4xCgFnPc1	dva
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pražské	pražský	k2eAgNnSc1d1	Pražské
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
takto	takto	k6eAd1	takto
postátněno	postátnit	k5eAaPmNgNnS	postátnit
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Výlučně	výlučně	k6eAd1	výlučně
státní	státní	k2eAgNnSc4d1	státní
provozování	provozování	k1gNnSc4	provozování
divadel	divadlo	k1gNnPc2	divadlo
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
za	za	k7c2	za
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
,	,	kIx,	,
též	též	k9	též
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
příkladu	příklad	k1gInSc2	příklad
divadel	divadlo	k1gNnPc2	divadlo
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
postátňovacích	postátňovací	k2eAgFnPc2d1	postátňovací
vln	vlna	k1gFnPc2	vlna
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
hledány	hledán	k2eAgInPc4d1	hledán
nové	nový	k2eAgInPc4d1	nový
modely	model	k1gInPc4	model
fungování	fungování	k1gNnSc2	fungování
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
převládá	převládat	k5eAaImIp3nS	převládat
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
stálá	stálý	k2eAgNnPc4d1	stálé
operní	operní	k2eAgNnPc4d1	operní
divadla	divadlo	k1gNnPc4	divadlo
vlastněna	vlastněn	k2eAgNnPc4d1	vlastněno
nebo	nebo	k8xC	nebo
kontrolována	kontrolován	k2eAgNnPc4d1	kontrolováno
veřejnoprávními	veřejnoprávní	k2eAgInPc7d1	veřejnoprávní
subjekty	subjekt	k1gInPc7	subjekt
(	(	kIx(	(
<g/>
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
městy	město	k1gNnPc7	město
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
provoz	provoz	k1gInSc1	provoz
je	být	k5eAaImIp3nS	být
hrazen	hradit	k5eAaImNgInS	hradit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
tendence	tendence	k1gFnPc1	tendence
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
klesající	klesající	k2eAgFnSc7d1	klesající
<g/>
:	:	kIx,	:
např.	např.	kA	např.
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
opera	opera	k1gFnSc1	opera
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
je	být	k5eAaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
dotována	dotovat	k5eAaBmNgFnS	dotovat
až	až	k9	až
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc4	podíl
kolem	kolem	k7c2	kolem
70-80	[number]	k4	70-80
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgMnSc1d1	běžný
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
bylo	být	k5eAaImAgNnS	být
razantnější	razantní	k2eAgNnSc1d2	razantnější
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
Royal	Royal	k1gMnSc1	Royal
Opera	opera	k1gFnSc1	opera
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Covent	Covent	k1gInSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
klesl	klesnout	k5eAaPmAgInS	klesnout
státní	státní	k2eAgInSc1d1	státní
příspěvek	příspěvek	k1gInSc1	příspěvek
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
26	[number]	k4	26
%	%	kIx~	%
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
))	))	k?	))
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
40	[number]	k4	40
%	%	kIx~	%
u	u	k7c2	u
milánské	milánský	k2eAgFnSc2d1	Milánská
La	la	k1gNnSc6	la
Scaly	scát	k5eAaImAgFnP	scát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
menší	malý	k2eAgNnPc1d2	menší
divadla	divadlo	k1gNnPc1	divadlo
mají	mít	k5eAaImIp3nP	mít
obecně	obecně	k6eAd1	obecně
větší	veliký	k2eAgFnSc4d2	veliký
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
veřejných	veřejný	k2eAgInPc6d1	veřejný
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgInSc4d2	menší
veřejný	veřejný	k2eAgInSc4d1	veřejný
příspěvek	příspěvek	k1gInSc4	příspěvek
získávají	získávat	k5eAaImIp3nP	získávat
operní	operní	k2eAgNnPc1d1	operní
divadla	divadlo	k1gNnPc1	divadlo
např.	např.	kA	např.
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
nebo	nebo	k8xC	nebo
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
veřejný	veřejný	k2eAgInSc1d1	veřejný
podíl	podíl	k1gInSc1	podíl
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
průměrně	průměrně	k6eAd1	průměrně
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
soukromé	soukromý	k2eAgInPc1d1	soukromý
dary	dar	k1gInPc1	dar
a	a	k8xC	a
sponzorství	sponzorství	k1gNnSc1	sponzorství
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
rozvinuty	rozvinout	k5eAaPmNgFnP	rozvinout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
46	[number]	k4	46
%	%	kIx~	%
příjmů	příjem	k1gInPc2	příjem
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
u	u	k7c2	u
Royal	Royal	k1gMnSc1	Royal
Opera	opera	k1gFnSc1	opera
House	house	k1gNnSc4	house
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
stálé	stálý	k2eAgFnPc1d1	stálá
operní	operní	k2eAgFnPc1d1	operní
scény	scéna	k1gFnPc1	scéna
provozovány	provozovat	k5eAaImNgInP	provozovat
veřejnoprávními	veřejnoprávní	k2eAgFnPc7d1	veřejnoprávní
osobami	osoba	k1gFnPc7	osoba
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
většinu	většina	k1gFnSc4	většina
prostředků	prostředek	k1gInPc2	prostředek
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
rozpočtů	rozpočet	k1gInPc2	rozpočet
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
prodeje	prodej	k1gInPc1	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
činila	činit	k5eAaImAgFnS	činit
část	část	k1gFnSc1	část
získávaná	získávaný	k2eAgFnSc1d1	získávaná
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
rozpočtů	rozpočet	k1gInPc2	rozpočet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
asi	asi	k9	asi
62	[number]	k4	62
%	%	kIx~	%
a	a	k8xC	a
vlastní	vlastní	k2eAgInPc4d1	vlastní
zdroje	zdroj	k1gInPc4	zdroj
34,5	[number]	k4	34,5
%	%	kIx~	%
<g/>
;	;	kIx,	;
sponzorské	sponzorský	k2eAgInPc1d1	sponzorský
dary	dar	k1gInPc1	dar
činily	činit	k5eAaImAgInP	činit
pouze	pouze	k6eAd1	pouze
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
samotná	samotný	k2eAgFnSc1d1	samotná
opera	opera	k1gFnSc1	opera
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
celkových	celkový	k2eAgFnPc6d1	celková
tržbách	tržba	k1gFnPc6	tržba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ještě	ještě	k9	ještě
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
nákladech	náklad	k1gInPc6	náklad
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
personálních	personální	k2eAgFnPc2d1	personální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
Praha	Praha	k1gFnSc1	Praha
činil	činit	k5eAaImAgInS	činit
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
příspěvek	příspěvek	k1gInSc1	příspěvek
zřizovatele	zřizovatel	k1gMnSc2	zřizovatel
54	[number]	k4	54
%	%	kIx~	%
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInPc4d1	vlastní
zdroje	zdroj	k1gInPc4	zdroj
46	[number]	k4	46
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Model	model	k1gInSc1	model
financování	financování	k1gNnSc2	financování
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
americká	americký	k2eAgNnPc1d1	americké
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
financovaná	financovaný	k2eAgFnSc1d1	financovaná
ze	z	k7c2	z
soukromých	soukromý	k2eAgInPc2d1	soukromý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
preferují	preferovat	k5eAaImIp3nP	preferovat
spíše	spíše	k9	spíše
osvědčený	osvědčený	k2eAgInSc4d1	osvědčený
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
repertoár	repertoár	k1gInSc4	repertoár
i	i	k8xC	i
tradiční	tradiční	k2eAgFnSc4d1	tradiční
realistickou	realistický	k2eAgFnSc4d1	realistická
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
výpravu	výprava	k1gFnSc4	výprava
a	a	k8xC	a
využívají	využívat	k5eAaPmIp3nP	využívat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
kult	kult	k1gInSc1	kult
pěveckých	pěvecký	k2eAgFnPc2d1	pěvecká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
státem	stát	k1gInSc7	stát
bohatě	bohatě	k6eAd1	bohatě
dotovaná	dotovaný	k2eAgNnPc1d1	dotované
divadla	divadlo	k1gNnPc1	divadlo
francouzská	francouzský	k2eAgNnPc1d1	francouzské
nebo	nebo	k8xC	nebo
německá	německý	k2eAgNnPc1d1	německé
často	často	k6eAd1	často
uvádějí	uvádět	k5eAaImIp3nP	uvádět
novinky	novinka	k1gFnPc1	novinka
nebo	nebo	k8xC	nebo
opomíjený	opomíjený	k2eAgInSc1d1	opomíjený
historický	historický	k2eAgInSc1d1	historický
repertoár	repertoár	k1gInSc1	repertoár
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
režie	režie	k1gFnSc1	režie
i	i	k8xC	i
výprava	výprava	k1gFnSc1	výprava
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
inovativní	inovativní	k2eAgMnSc1d1	inovativní
a	a	k8xC	a
provokativní	provokativní	k2eAgMnSc1d1	provokativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
oper	opera	k1gFnPc2	opera
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
operní	operní	k2eAgInSc1d1	operní
repertoár	repertoár	k1gInSc1	repertoár
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Operní	operní	k2eAgFnSc1d1	operní
scéna	scéna	k1gFnSc1	scéna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
opera	opera	k1gFnSc1	opera
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
