<p>
<s>
River	River	k1gInSc1	River
Raid	raid	k1gInSc1	raid
je	být	k5eAaImIp3nS	být
akční	akční	k2eAgFnSc1d1	akční
videohra	videohra	k1gFnSc1	videohra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
a	a	k8xC	a
naprogramovala	naprogramovat	k5eAaPmAgFnS	naprogramovat
Carol	Carol	k1gInSc4	Carol
Shaw	Shaw	k1gFnSc2	Shaw
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Activision	Activision	k1gInSc1	Activision
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
ovládá	ovládat	k5eAaImIp3nS	ovládat
letoun	letoun	k1gMnSc1	letoun
letící	letící	k2eAgMnSc1d1	letící
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nP	ničit
nepřátelské	přátelský	k2eNgFnPc1d1	nepřátelská
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
letouny	letoun	k1gInPc1	letoun
<g/>
,	,	kIx,	,
helikoptéry	helikoptéra	k1gFnPc1	helikoptéra
a	a	k8xC	a
mosty	most	k1gInPc1	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
letu	let	k1gInSc2	let
musí	muset	k5eAaImIp3nS	muset
doplňovat	doplňovat	k5eAaImF	doplňovat
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
omezené	omezený	k2eAgFnSc2d1	omezená
velikosti	velikost	k1gFnSc2	velikost
ROM	ROM	kA	ROM
(	(	kIx(	(
<g/>
4	[number]	k4	4
096	[number]	k4	096
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
a	a	k8xC	a
RAM	RAM	kA	RAM
(	(	kIx(	(
<g/>
128	[number]	k4	128
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
i	i	k8xC	i
rozmístění	rozmístění	k1gNnPc2	rozmístění
nepřátel	nepřítel	k1gMnPc2	nepřítel
generováno	generovat	k5eAaImNgNnS	generovat
pomocí	pomocí	k7c2	pomocí
posuvného	posuvný	k2eAgInSc2d1	posuvný
registru	registr	k1gInSc2	registr
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
