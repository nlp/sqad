<p>
<s>
Uzenina	uzenina	k1gFnSc1	uzenina
je	být	k5eAaImIp3nS	být
homogenizovaný	homogenizovaný	k2eAgInSc4d1	homogenizovaný
(	(	kIx(	(
<g/>
mělněný	mělněný	k2eAgInSc4d1	mělněný
<g/>
)	)	kIx)	)
masný	masný	k2eAgInSc4d1	masný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
konzervovaný	konzervovaný	k2eAgMnSc1d1	konzervovaný
uzením	uzení	k1gNnSc7	uzení
nebo	nebo	k8xC	nebo
technologickými	technologický	k2eAgFnPc7d1	technologická
náhražkami	náhražka	k1gFnPc7	náhražka
uzení	uzení	k1gNnSc2	uzení
(	(	kIx(	(
<g/>
rychlosoli	rychlosoli	k6eAd1	rychlosoli
<g/>
,	,	kIx,	,
var	var	k1gInSc1	var
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
odlišujícím	odlišující	k2eAgInSc6d1	odlišující
uzeninu	uzenina	k1gFnSc4	uzenina
od	od	k7c2	od
uzeného	uzený	k2eAgNnSc2d1	uzené
masa	maso	k1gNnSc2	maso
je	být	k5eAaImIp3nS	být
homogenizace	homogenizace	k1gFnSc1	homogenizace
výchozí	výchozí	k2eAgFnSc2d1	výchozí
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
výchozí	výchozí	k2eAgFnSc4d1	výchozí
skladbu	skladba	k1gFnSc4	skladba
suroviny	surovina	k1gFnSc2	surovina
(	(	kIx(	(
<g/>
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
šunka	šunka	k1gFnSc1	šunka
od	od	k7c2	od
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
není	být	k5eNaImIp3nS	být
uzeninou	uzenina	k1gFnSc7	uzenina
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
běžná	běžný	k2eAgFnSc1d1	běžná
konzumní	konzumní	k2eAgFnSc1d1	konzumní
šunka	šunka	k1gFnSc1	šunka
z	z	k7c2	z
formy	forma	k1gFnSc2	forma
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výchozí	výchozí	k2eAgFnSc1d1	výchozí
masná	masný	k2eAgFnSc1d1	Masná
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
uzenin	uzenina	k1gFnPc2	uzenina
(	(	kIx(	(
<g/>
vazné	vazný	k2eAgNnSc1d1	vazné
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mělní	mělnit	k5eAaImIp3nS	mělnit
při	při	k7c6	při
současném	současný	k2eAgNnSc6d1	současné
nebo	nebo	k8xC	nebo
následném	následný	k2eAgNnSc6d1	následné
solení	solení	k1gNnSc6	solení
v	v	k7c6	v
kutrech	kutr	k1gInPc6	kutr
nebo	nebo	k8xC	nebo
mlýncích	mlýnec	k1gInPc6	mlýnec
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
uvolněné	uvolněný	k2eAgFnPc1d1	uvolněná
myofibrilární	myofibrilární	k2eAgFnPc1d1	myofibrilární
bílkoviny	bílkovina	k1gFnPc1	bílkovina
převádějí	převádět	k5eAaImIp3nP	převádět
na	na	k7c4	na
rozpustnou	rozpustný	k2eAgFnSc4d1	rozpustná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
finální	finální	k2eAgFnSc2d1	finální
struktury	struktura	k1gFnSc2	struktura
výrobku	výrobek	k1gInSc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Mělněná	mělněný	k2eAgFnSc1d1	mělněný
surovina	surovina	k1gFnSc1	surovina
(	(	kIx(	(
<g/>
doplněná	doplněná	k1gFnSc1	doplněná
někdy	někdy	k6eAd1	někdy
i	i	k9	i
podílem	podíl	k1gInSc7	podíl
méně	málo	k6eAd2	málo
vazného	vazný	k2eAgNnSc2d1	vazné
masa	maso	k1gNnSc2	maso
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
spojka	spojka	k1gFnSc1	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
případnou	případný	k2eAgFnSc7d1	případná
vložkou	vložka	k1gFnSc7	vložka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
větších	veliký	k2eAgFnPc2d2	veliký
nemělněných	mělněný	k2eNgFnPc2d1	mělněný
částí	část	k1gFnPc2	část
suroviny	surovina	k1gFnSc2	surovina
(	(	kIx(	(
<g/>
kostky	kostka	k1gFnPc1	kostka
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
chutěnosných	chutěnosný	k2eAgInPc2d1	chutěnosný
doplňků	doplněk	k1gInPc2	doplněk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
naráží	narážet	k5eAaPmIp3nS	narážet
do	do	k7c2	do
střev	střevo	k1gNnPc2	střevo
nebo	nebo	k8xC	nebo
plní	plnit	k5eAaImIp3nP	plnit
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
technologicky	technologicky	k6eAd1	technologicky
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
považuje	považovat	k5eAaImIp3nS	považovat
uzeniny	uzenina	k1gFnPc4	uzenina
za	za	k7c4	za
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úpravy	úprava	k1gFnPc1	úprava
==	==	k?	==
</s>
</p>
<p>
<s>
Solení	solení	k1gNnSc1	solení
suroviny	surovina	k1gFnSc2	surovina
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
spojky	spojka	k1gFnSc2	spojka
přímo	přímo	k6eAd1	přímo
během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
mělnění	mělnění	k1gNnSc2	mělnění
<g/>
,	,	kIx,	,
vložka	vložka	k1gFnSc1	vložka
se	se	k3xPyFc4	se
nasoluje	nasolovat	k5eAaImIp3nS	nasolovat
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
operaci	operace	k1gFnSc6	operace
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
charakteru	charakter	k1gInSc6	charakter
povrchovým	povrchový	k2eAgNnSc7d1	povrchové
solením	solení	k1gNnSc7	solení
<g/>
,	,	kIx,	,
namáčením	namáčení	k1gNnSc7	namáčení
do	do	k7c2	do
solanky	solanka	k1gFnSc2	solanka
či	či	k8xC	či
solného	solný	k2eAgInSc2d1	solný
láku	lák	k1gInSc2	lák
<g/>
,	,	kIx,	,
nastřikováním	nastřikování	k1gNnSc7	nastřikování
solného	solný	k2eAgInSc2d1	solný
láku	lák	k1gInSc2	lák
mnohojehlovými	mnohojehlový	k2eAgInPc7d1	mnohojehlový
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
tumblováním	tumblování	k1gNnSc7	tumblování
(	(	kIx(	(
<g/>
kombinovaným	kombinovaný	k2eAgNnSc7d1	kombinované
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
působením	působení	k1gNnSc7	působení
při	při	k7c6	při
přepadávání	přepadávání	k1gNnSc6	přepadávání
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
míchačích	míchač	k1gInPc6	míchač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zvýšením	zvýšení	k1gNnSc7	zvýšení
povrchu	povrch	k1gInSc2	povrch
napichováním	napichování	k1gNnSc7	napichování
sadou	sada	k1gFnSc7	sada
nožů	nůž	k1gInPc2	nůž
v	v	k7c6	v
extraktorech	extraktor	k1gInPc6	extraktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mělnění	mělnění	k1gNnSc1	mělnění
spojky	spojka	k1gFnSc2	spojka
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mlýncích	mlýnec	k1gInPc6	mlýnec
nebo	nebo	k8xC	nebo
kutrech	kutr	k1gInPc6	kutr
se	s	k7c7	s
srpovitými	srpovitý	k2eAgInPc7d1	srpovitý
noži	nůž	k1gInPc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
hrubost	hrubost	k1gFnSc1	hrubost
suroviny	surovina	k1gFnSc2	surovina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mlýnků	mlýnek	k1gInPc2	mlýnek
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
velikostí	velikost	k1gFnSc7	velikost
děrovaných	děrovaný	k2eAgFnPc2d1	děrovaná
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kutrů	kutr	k1gInPc2	kutr
pak	pak	k6eAd1	pak
volbou	volba	k1gFnSc7	volba
otáček	otáčka	k1gFnPc2	otáčka
nožů	nůž	k1gInPc2	nůž
a	a	k8xC	a
míchacího	míchací	k2eAgInSc2d1	míchací
kotle	kotel	k1gInSc2	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
stupně	stupeň	k1gInPc4	stupeň
mělnění	mělnění	k1gNnSc2	mělnění
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přidávat	přidávat	k5eAaImF	přidávat
šupinkový	šupinkový	k2eAgInSc4d1	šupinkový
led	led	k1gInSc4	led
<g/>
,	,	kIx,	,
zabraňující	zabraňující	k2eAgNnPc1d1	zabraňující
nárůstu	nárůst	k1gInSc2	nárůst
teploty	teplota	k1gFnSc2	teplota
kutrované	kutrovaný	k2eAgFnSc2d1	kutrovaný
suroviny	surovina	k1gFnSc2	surovina
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zhoršení	zhoršení	k1gNnSc4	zhoršení
vaznosti	vaznost	k1gFnSc2	vaznost
suroviny	surovina	k1gFnSc2	surovina
předčasným	předčasný	k2eAgNnSc7d1	předčasné
denaturováním	denaturování	k1gNnSc7	denaturování
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vmíchání	vmíchání	k1gNnSc3	vmíchání
vložky	vložka	k1gFnSc2	vložka
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
přímo	přímo	k6eAd1	přímo
kutr	kutr	k1gInSc4	kutr
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vložka	vložka	k1gFnSc1	vložka
vmíchává	vmíchávat	k5eAaImIp3nS	vmíchávat
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
míchačce	míchačka	k1gFnSc6	míchačka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narážení	narážení	k1gNnSc1	narážení
a	a	k8xC	a
tvarování	tvarování	k1gNnSc1	tvarování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
do	do	k7c2	do
technologickým	technologický	k2eAgFnPc3d1	technologická
postupem	postup	k1gInSc7	postup
stanovených	stanovený	k2eAgInPc2d1	stanovený
obalů	obal	k1gInPc2	obal
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
jsou	být	k5eAaImIp3nP	být
užívána	užíván	k2eAgNnPc1d1	užíváno
přírodní	přírodní	k2eAgNnPc1d1	přírodní
nebo	nebo	k8xC	nebo
klihovková	klihovkový	k2eAgNnPc1d1	klihovkový
střeva	střevo	k1gNnPc1	střevo
<g/>
,	,	kIx,	,
polyamidová	polyamidový	k2eAgNnPc1d1	polyamidové
střeva	střevo	k1gNnPc1	střevo
nebo	nebo	k8xC	nebo
u	u	k7c2	u
luxusních	luxusní	k2eAgInPc2d1	luxusní
výrobků	výrobek	k1gInPc2	výrobek
střeva	střevo	k1gNnSc2	střevo
textilní	textilní	k2eAgInPc1d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
narážené	narážený	k2eAgInPc1d1	narážený
do	do	k7c2	do
plastových	plastový	k2eAgNnPc2d1	plastové
střev	střevo	k1gNnPc2	střevo
nelze	lze	k6eNd1	lze
udit	udit	k5eAaImF	udit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uzení	uzení	k1gNnSc1	uzení
výrobku	výrobek	k1gInSc2	výrobek
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
primární	primární	k2eAgInSc4d1	primární
cíl	cíl	k1gInSc4	cíl
konzervaci	konzervace	k1gFnSc4	konzervace
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
sekundárním	sekundární	k2eAgInSc7d1	sekundární
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
dosažení	dosažení	k1gNnSc1	dosažení
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
chuti	chuť	k1gFnSc2	chuť
uzeniny	uzenina	k1gFnSc2	uzenina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
horký	horký	k2eAgInSc1d1	horký
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
běžných	běžný	k2eAgFnPc2d1	běžná
uzenin	uzenina	k1gFnPc2	uzenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgMnSc1d1	teplý
(	(	kIx(	(
<g/>
kusové	kusový	k2eAgInPc4d1	kusový
výrobky	výrobek	k1gInPc4	výrobek
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
studený	studený	k2eAgMnSc1d1	studený
(	(	kIx(	(
<g/>
luxusní	luxusní	k2eAgInPc4d1	luxusní
výrobky	výrobek	k1gInPc4	výrobek
<g/>
)	)	kIx)	)
kouř	kouř	k1gInSc4	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uzení	uzení	k1gNnSc6	uzení
horkým	horký	k2eAgInSc7d1	horký
kouřem	kouř	k1gInSc7	kouř
je	být	k5eAaImIp3nS	být
výrobek	výrobek	k1gInSc1	výrobek
předsušen	předsušen	k2eAgInSc1d1	předsušen
v	v	k7c6	v
horkém	horký	k2eAgInSc6d1	horký
vzduchu	vzduch	k1gInSc6	vzduch
na	na	k7c4	na
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
obsah	obsah	k1gInSc4	obsah
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vystaven	vystavit	k5eAaPmNgInS	vystavit
zauzovacímu	zauzovací	k2eAgNnSc3d1	zauzovací
prostředí	prostředí	k1gNnSc3	prostředí
a	a	k8xC	a
následně	následně	k6eAd1	následně
dovařen	dovařit	k5eAaPmNgInS	dovařit
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
páře	pára	k1gFnSc6	pára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sušení	sušení	k1gNnSc1	sušení
se	se	k3xPyFc4	se
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
u	u	k7c2	u
luxusních	luxusní	k2eAgInPc2d1	luxusní
výrobků	výrobek	k1gInPc2	výrobek
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
snížení	snížení	k1gNnSc1	snížení
aktivity	aktivita	k1gFnSc2	aktivita
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
chuťových	chuťový	k2eAgFnPc2d1	chuťová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Sušené	sušený	k2eAgInPc1d1	sušený
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
trvanlivé	trvanlivý	k2eAgFnPc1d1	trvanlivá
a	a	k8xC	a
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
ceněné	ceněný	k2eAgNnSc1d1	ceněné
<g/>
.	.	kIx.	.
</s>
<s>
Tepelně	tepelně	k6eAd1	tepelně
opracované	opracovaný	k2eAgInPc4d1	opracovaný
výrobky	výrobek	k1gInPc4	výrobek
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
doba	doba	k1gFnSc1	doba
sušení	sušení	k1gNnSc2	sušení
syrových	syrový	k2eAgInPc2d1	syrový
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnSc2	některý
šunky	šunka	k1gFnSc2	šunka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
sušit	sušit	k5eAaImF	sušit
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
následně	následně	k6eAd1	následně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘEHÁK	Řehák	k1gMnSc1	Řehák
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
<g/>
Výroba	výroba	k1gFnSc1	výroba
jemného	jemný	k2eAgNnSc2d1	jemné
uzenářského	uzenářský	k2eAgNnSc2d1	uzenářské
zboží	zboží	k1gNnSc2	zboží
<g/>
:	:	kIx,	:
Praktická	praktický	k2eAgFnSc1d1	praktická
příruční	příruční	k2eAgFnSc1d1	příruční
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
uzenáře	uzenář	k1gMnPc4	uzenář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nákl	Nákl	k1gInSc1	Nákl
<g/>
.	.	kIx.	.
vl	vl	k?	vl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Salám	salám	k1gInSc1	salám
</s>
</p>
<p>
<s>
Párky	párek	k1gInPc1	párek
</s>
</p>
<p>
<s>
Špekáček	špekáček	k1gInSc1	špekáček
</s>
</p>
<p>
<s>
Klobása	klobása	k1gFnSc1	klobása
</s>
</p>
<p>
<s>
Talián	talián	k1gInSc1	talián
</s>
</p>
<p>
<s>
Trampská	trampský	k2eAgFnSc1d1	trampská
cigára	cigára	k1gFnSc1	cigára
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
uzenina	uzenina	k1gFnSc1	uzenina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
uzenina	uzenina	k1gFnSc1	uzenina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Uzenářství	uzenářství	k1gNnSc2	uzenářství
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
Viviente	Vivient	k1gMnSc5	Vivient
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
Chemie	chemie	k1gFnSc2	chemie
v	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
</s>
</p>
