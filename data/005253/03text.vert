<s>
Název	název	k1gInSc1	název
puzzle	puzzle	k1gNnSc2	puzzle
(	(	kIx(	(
<g/>
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
pɐ	pɐ	k?	pɐ
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
též	též	k9	též
zdomácněla	zdomácnět	k5eAaPmAgFnS	zdomácnět
počeštěná	počeštěný	k2eAgFnSc1d1	počeštěná
žertovná	žertovný	k2eAgFnSc1d1	žertovná
výslovnost	výslovnost	k1gFnSc1	výslovnost
"	"	kIx"	"
<g/>
pucle	pucle	k1gFnSc1	pucle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
označuje	označovat	k5eAaImIp3nS	označovat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
skládačku	skládačka	k1gFnSc4	skládačka
<g/>
,	,	kIx,	,
hříčku	hříčka	k1gFnSc4	hříčka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
křížovku	křížovka	k1gFnSc4	křížovka
i	i	k9	i
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Skutečným	skutečný	k2eAgInSc7d1	skutečný
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jigsaw	jigsaw	k?	jigsaw
puzzle	puzzle	k1gNnSc1	puzzle
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgFnSc4d1	popisující
skládačku	skládačka	k1gFnSc4	skládačka
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
rozřezáním	rozřezání	k1gNnSc7	rozřezání
pomalovaného	pomalovaný	k2eAgNnSc2d1	pomalované
prkénka	prkénko	k1gNnSc2	prkénko
lupénkovou	lupénkový	k2eAgFnSc7d1	lupénková
pilou	pila	k1gFnSc7	pila
nebo	nebo	k8xC	nebo
laserem	laser	k1gInSc7	laser
<g/>
.	.	kIx.	.
</s>
<s>
Skládání	skládání	k1gNnSc1	skládání
puzzle	puzzle	k1gNnSc2	puzzle
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
logické	logický	k2eAgNnSc4d1	logické
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
do	do	k7c2	do
sebe	se	k3xPyFc2	se
musejí	muset	k5eAaImIp3nP	muset
přesně	přesně	k6eAd1	přesně
zapadat	zapadat	k5eAaPmF	zapadat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nS	složit
kýžený	kýžený	k2eAgInSc4d1	kýžený
tvar	tvar	k1gInSc4	tvar
či	či	k8xC	či
obrazec	obrazec	k1gInSc4	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
tzv.	tzv.	kA	tzv.
puzzle	puzzle	k1gNnSc4	puzzle
ball	ball	k1gInSc1	ball
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dílky	dílek	k1gInPc1	dílek
zaoblené	zaoblený	k2eAgInPc1d1	zaoblený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
koule	koule	k1gFnSc1	koule
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Puzzle	puzzle	k1gNnSc1	puzzle
ball	balla	k1gFnPc2	balla
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nS	tvořit
logo	logo	k1gNnSc4	logo
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Puzzle	puzzle	k1gNnSc1	puzzle
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
skládat	skládat	k5eAaImF	skládat
třeba	třeba	k6eAd1	třeba
jen	jen	k9	jen
z	z	k7c2	z
několika	několik	k4yIc2	několik
velkých	velký	k2eAgFnPc2d1	velká
částeček	částečka	k1gFnPc2	částečka
(	(	kIx(	(
<g/>
dětská	dětský	k2eAgFnSc1d1	dětská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
jich	on	k3xPp3gInPc2	on
být	být	k5eAaImF	být
také	také	k9	také
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
puzzle	puzzle	k1gNnSc4	puzzle
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
skládáte	skládat	k5eAaImIp2nP	skládat
početnější	početní	k2eAgNnSc4d2	početnější
puzzle	puzzle	k1gNnSc4	puzzle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
udělat	udělat	k5eAaPmF	udělat
si	se	k3xPyFc3	se
v	v	k7c6	v
rozložení	rozložení	k1gNnSc6	rozložení
částeček	částečka	k1gFnPc2	částečka
nějaký	nějaký	k3yIgInSc4	nějaký
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
hromádku	hromádka	k1gFnSc4	hromádka
dát	dát	k5eAaPmF	dát
všechny	všechen	k3xTgFnPc4	všechen
krajové	krajový	k2eAgFnPc4d1	krajová
částečky	částečka	k1gFnPc4	částečka
atd.	atd.	kA	atd.
20	[number]	k4	20
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
24	[number]	k4	24
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
48	[number]	k4	48
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
Mini	mini	k2eAgInPc2d1	mini
54	[number]	k4	54
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
60	[number]	k4	60
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
80	[number]	k4	80
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
99	[number]	k4	99
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
100	[number]	k4	100
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
200	[number]	k4	200
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
260	[number]	k4	260
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
300	[number]	k4	300
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
400	[number]	k4	400
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
500	[number]	k4	500
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
750	[number]	k4	750
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
4000	[number]	k4	4000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
6000	[number]	k4	6000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
9000	[number]	k4	9000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
10000	[number]	k4	10000
dílků	dílek	k1gInPc2	dílek
<g/>
,	,	kIx,	,
13200	[number]	k4	13200
dílků	dílek	k1gInPc2	dílek
a	a	k8xC	a
18000	[number]	k4	18000
dílků	dílek	k1gInPc2	dílek
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
puzzle	puzzle	k1gNnSc1	puzzle
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
"	"	kIx"	"
<g/>
Disney	Disnea	k1gFnSc2	Disnea
Moments	Moments	k1gInSc1	Moments
<g/>
"	"	kIx"	"
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gNnSc2	on
40	[number]	k4	40
320	[number]	k4	320
dílků	dílek	k1gInPc2	dílek
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jej	on	k3xPp3gMnSc4	on
firma	firma	k1gFnSc1	firma
Ravensburger	Ravensburger	k1gInSc1	Ravensburger
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
správné	správný	k2eAgFnSc2d1	správná
výslovnosti	výslovnost	k1gFnSc2	výslovnost
[	[	kIx(	[
<g/>
pazl	pazl	k1gInSc1	pazl
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ujala	ujmout	k5eAaPmAgFnS	ujmout
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
pucle	pucle	k1gFnSc1	pucle
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
puclík	puclík	k?	puclík
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
puzzle	puzzle	k1gNnSc2	puzzle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
puzzle	puzzle	k1gNnSc2	puzzle
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
