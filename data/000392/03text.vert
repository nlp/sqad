<s>
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc4	společnost
vyvíjející	vyvíjející	k2eAgFnSc2d1	vyvíjející
a	a	k8xC	a
publikující	publikující	k2eAgFnSc2d1	publikující
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Firmu	firma	k1gFnSc4	firma
založili	založit	k5eAaPmAgMnP	založit
Mike	Mikus	k1gMnSc5	Mikus
Morhaime	Morhaim	k1gMnSc5	Morhaim
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
Adham	Adham	k1gInSc1	Adham
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
Pearce	Pearce	k1gMnSc1	Pearce
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Blizzard	Blizzard	k1gInSc1	Blizzard
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
notoricky	notoricky	k6eAd1	notoricky
známý	známý	k2eAgInSc1d1	známý
pro	pro	k7c4	pro
zcela	zcela	k6eAd1	zcela
markantní	markantní	k2eAgNnSc4d1	markantní
a	a	k8xC	a
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
nedodržování	nedodržování	k1gNnSc4	nedodržování
ohlášených	ohlášený	k2eAgNnPc2d1	ohlášené
dat	datum	k1gNnPc2	datum
vydání	vydání	k1gNnSc2	vydání
svých	svůj	k3xOyFgFnPc2	svůj
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
hry	hra	k1gFnPc4	hra
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
velmi	velmi	k6eAd1	velmi
populárními	populární	k2eAgFnPc7d1	populární
a	a	k8xC	a
legendárními	legendární	k2eAgFnPc7d1	legendární
klasikami	klasika	k1gFnPc7	klasika
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
série	série	k1gFnSc2	série
her	hra	k1gFnPc2	hra
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
,	,	kIx,	,
Diablo	Diablo	k1gMnSc1	Diablo
a	a	k8xC	a
StarCraft	StarCraft	k1gMnSc1	StarCraft
<g/>
.	.	kIx.	.
</s>
<s>
Blizzard	Blizzard	k1gMnSc1	Blizzard
se	se	k3xPyFc4	se
také	také	k9	také
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
hrou	hra	k1gFnSc7	hra
WarCraft	WarCraft	k1gMnSc1	WarCraft
II	II	kA	II
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
a	a	k8xC	a
chatování	chatování	k1gNnSc4	chatování
zvané	zvaný	k2eAgFnSc2d1	zvaná
Battle	Battle	k1gFnSc2	Battle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prostředí	prostředí	k1gNnSc1	prostředí
nyní	nyní	k6eAd1	nyní
propojuje	propojovat	k5eAaImIp3nS	propojovat
všechny	všechen	k3xTgFnPc4	všechen
jejich	jejich	k3xOp3gFnPc4	jejich
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
spolu	spolu	k6eAd1	spolu
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
hrají	hrát	k5eAaImIp3nP	hrát
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lost	Lost	k1gInSc1	Lost
Vikings	Vikings	k1gInSc1	Vikings
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
Racing	Racing	k1gInSc1	Racing
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
závodní	závodní	k2eAgFnSc1d1	závodní
hra	hra	k1gFnSc1	hra
Blackthorne	Blackthorn	k1gInSc5	Blackthorn
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
fantasy	fantas	k1gInPc1	fantas
The	The	k1gFnSc2	The
Death	Death	k1gMnSc1	Death
and	and	k?	and
Return	Return	k1gMnSc1	Return
of	of	k?	of
Superman	superman	k1gMnSc1	superman
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Warcraft	Warcrafta	k1gFnPc2	Warcrafta
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
strategie	strategie	k1gFnSc1	strategie
<g />
.	.	kIx.	.
</s>
<s>
Justice	justice	k1gFnSc1	justice
League	Leagu	k1gFnSc2	Leagu
Task	Taska	k1gFnPc2	Taska
Force	force	k1gFnSc7	force
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
bojová	bojový	k2eAgFnSc1d1	bojová
hra	hra	k1gFnSc1	hra
The	The	k1gFnPc2	The
Lost	Lost	k2eAgInSc1d1	Lost
Vikings	Vikings	k1gInSc1	Vikings
II	II	kA	II
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Warcraft	Warcraft	k1gInSc1	Warcraft
II	II	kA	II
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
-	-	kIx~	-
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
strategie	strategie	k1gFnSc1	strategie
Warcraft	Warcraft	k1gMnSc1	Warcraft
II	II	kA	II
<g/>
:	:	kIx,	:
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Dark	Dark	k1gMnSc1	Dark
Portal	Portal	k1gMnSc1	Portal
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Diablo	Diablo	k1gFnSc2	Diablo
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
akční	akční	k2eAgFnSc1d1	akční
RPG	RPG	kA	RPG
Diablo	Diablo	k1gFnSc1	Diablo
<g/>
:	:	kIx,	:
Hellfire	Hellfir	k1gInSc5	Hellfir
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
StarCraft	StarCraft	k1gInSc1	StarCraft
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
-	-	kIx~	-
sci-fi	scii	k1gNnSc1	sci-fi
strategie	strategie	k1gFnSc2	strategie
StarCraft	StarCrafta	k1gFnPc2	StarCrafta
<g/>
:	:	kIx,	:
Brood	Brood	k1gInSc1	Brood
War	War	k1gFnSc2	War
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Diablo	Diablo	k1gFnSc2	Diablo
II	II	kA	II
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
akční	akční	k2eAgMnSc1d1	akční
RPG	RPG	kA	RPG
Diablo	Diablo	k1gFnPc2	Diablo
II	II	kA	II
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Lord	lord	k1gMnSc1	lord
of	of	k?	of
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Warcraft	Warcraft	k1gInSc1	Warcraft
III	III	kA	III
<g/>
:	:	kIx,	:
Reign	Reign	k1gMnSc1	Reign
of	of	k?	of
Chaos	chaos	k1gInSc1	chaos
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
-	-	kIx~	-
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
strategie	strategie	k1gFnSc1	strategie
Warcraft	Warcraft	k1gMnSc1	Warcraft
III	III	kA	III
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Frozen	Frozen	k2eAgInSc1d1	Frozen
Throne	Thron	k1gInSc5	Thron
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
MMORPG	MMORPG	kA	MMORPG
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
the	the	k?	the
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
StarCraft	StarCraft	k1gInSc1	StarCraft
II	II	kA	II
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
sci-fi	scii	k1gNnSc1	sci-fi
strategie	strategie	k1gFnPc1	strategie
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Cataclysm	Cataclysm	k1gMnSc1	Cataclysm
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Diablo	Diablo	k1gFnSc2	Diablo
III	III	kA	III
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
třetí	třetí	k4xOgNnSc4	třetí
pokračování	pokračování	k1gNnSc4	pokračování
akčního	akční	k2eAgInSc2d1	akční
RPG	RPG	kA	RPG
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Mists	Mists	k1gInSc1	Mists
of	of	k?	of
Pandaria	Pandarium	k1gNnSc2	Pandarium
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
StarCraft	StarCraft	k1gInSc1	StarCraft
II	II	kA	II
<g/>
:	:	kIx,	:
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
the	the	k?	the
Swarm	Swarm	k1gInSc1	Swarm
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Hearthstone	Hearthston	k1gInSc5	Hearthston
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Heroes	Heroes	k1gMnSc1	Heroes
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
CCG	CCG	kA	CCG
Diablo	Diablo	k1gFnPc2	Diablo
III	III	kA	III
<g/>
:	:	kIx,	:
Reaper	Reaper	k1gMnSc1	Reaper
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
:	:	kIx,	:
Warlords	Warlords	k1gInSc1	Warlords
of	of	k?	of
Draenor	Draenor	k1gInSc1	Draenor
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
Heroes	Heroes	k1gInSc1	Heroes
of	of	k?	of
the	the	k?	the
Storm	Storm	k1gInSc1	Storm
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
MOBA	MOBA	kA	MOBA
StarCraft	StarCrafta	k1gFnPc2	StarCrafta
II	II	kA	II
<g/>
:	:	kIx,	:
Legacy	Legaca	k1gMnSc2	Legaca
of	of	k?	of
the	the	k?	the
Void	Void	k1gInSc1	Void
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
OverWatch	OverWatch	k1gInSc1	OverWatch
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
FPS	FPS	kA	FPS
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Legion	legion	k1gInSc1	legion
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
datadisk	datadisk	k1gInSc1	datadisk
</s>
