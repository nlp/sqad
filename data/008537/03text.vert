<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
portfolia	portfolio	k1gNnSc2	portfolio
produktů	produkt	k1gInPc2	produkt
společnosti	společnost	k1gFnSc2	společnost
Economia	Economia	k1gFnSc1	Economia
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
i	i	k9	i
portál	portál	k1gInSc1	portál
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
společnosti	společnost	k1gFnSc2	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
jako	jako	k9	jako
demonstrace	demonstrace	k1gFnPc1	demonstrace
skriptovacího	skriptovací	k2eAgInSc2d1	skriptovací
jazyka	jazyk	k1gInSc2	jazyk
ASP	ASP	kA	ASP
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zapojil	zapojit	k5eAaPmAgMnS	zapojit
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Atlas	Atlas	k1gMnSc1	Atlas
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
portálů	portál	k1gInPc2	portál
MSN	MSN	kA	MSN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
investor	investor	k1gMnSc1	investor
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
EPIC	EPIC	kA	EPIC
Holding	holding	k1gInSc1	holding
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
expanze	expanze	k1gFnSc1	expanze
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
sesterské	sesterský	k2eAgInPc1d1	sesterský
portály	portál	k1gInPc1	portál
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
a	a	k8xC	a
Atlasua	Atlasua	k1gFnSc1	Atlasua
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
nabízel	nabízet	k5eAaImAgInS	nabízet
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
také	také	k9	také
připojení	připojení	k1gNnSc1	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
přes	přes	k7c4	přes
modem	modem	k1gInSc4	modem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
začaly	začít	k5eAaPmAgInP	začít
opět	opět	k6eAd1	opět
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
MSN	MSN	kA	MSN
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
v	v	k7c6	v
ČR	ČR	kA	ČR
společností	společnost	k1gFnPc2	společnost
Microsoft	Microsoft	kA	Microsoft
podporován	podporovat	k5eAaImNgInS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Atlasu	Atlas	k1gInSc2	Atlas
David	David	k1gMnSc1	David
Duroň	Duroň	k1gMnSc1	Duroň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
nástupem	nástup	k1gInSc7	nástup
byl	být	k5eAaImAgInS	být
odstartován	odstartován	k2eAgInSc1d1	odstartován
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Restart	Restart	k1gInSc1	Restart
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
dvojkou	dvojka	k1gFnSc7	dvojka
české	český	k2eAgFnSc2d1	Česká
portálové	portálový	k2eAgFnSc2d1	portálová
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
statistik	statistika	k1gFnPc2	statistika
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
dopoledne	dopoledne	k1gNnSc2	dopoledne
společností	společnost	k1gFnPc2	společnost
NetMonitor	NetMonitor	k1gMnSc1	NetMonitor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c2	za
září	září	k1gNnSc2	září
2006	[number]	k4	2006
umístil	umístit	k5eAaPmAgMnS	umístit
Atlas	Atlas	k1gMnSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
mezi	mezi	k7c7	mezi
internetovými	internetový	k2eAgInPc7d1	internetový
portály	portál	k1gInPc7	portál
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
umístění	umístění	k1gNnSc2	umístění
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
provozování	provozování	k1gNnSc1	provozování
webové	webový	k2eAgFnSc2d1	webová
aplikace	aplikace	k1gFnSc2	aplikace
Jizdnirady	Jizdnirada	k1gFnSc2	Jizdnirada
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
v	v	k7c6	v
září	září	k1gNnSc6	září
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
tisíc	tisíc	k4xCgInPc2	tisíc
unikátních	unikátní	k2eAgMnPc2d1	unikátní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
vykázal	vykázat	k5eAaPmAgInS	vykázat
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ztrátu	ztráta	k1gFnSc4	ztráta
344	[number]	k4	344
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
při	při	k7c6	při
tržbách	tržba	k1gFnPc6	tržba
187	[number]	k4	187
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
kumulovaná	kumulovaný	k2eAgFnSc1d1	kumulovaná
ztráta	ztráta	k1gFnSc1	ztráta
společnosti	společnost	k1gFnSc2	společnost
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
oznámena	oznámen	k2eAgFnSc1d1	oznámena
integrace	integrace	k1gFnSc1	integrace
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
spustila	spustit	k5eAaPmAgFnS	spustit
skupina	skupina	k1gFnSc1	skupina
vývojářů	vývojář	k1gMnPc2	vývojář
Atlas	Atlas	k1gInSc4	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
stránky	stránka	k1gFnSc2	stránka
Odpojeni	odpojen	k2eAgMnPc1d1	odpojen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
zveřejněnila	zveřejněnit	k5eAaImAgFnS	zveřejněnit
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
průběhu	průběh	k1gInSc3	průběh
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Protest	protest	k1gInSc1	protest
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
tabulkou	tabulka	k1gFnSc7	tabulka
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podali	podat	k5eAaPmAgMnP	podat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
chystali	chystat	k5eAaImAgMnP	chystat
podat	podat	k5eAaPmF	podat
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
se	s	k7c7	s
provozovatelem	provozovatel	k1gMnSc7	provozovatel
portálu	portál	k1gInSc2	portál
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
stává	stávat	k5eAaImIp3nS	stávat
společnost	společnost	k1gFnSc1	společnost
Centrum	centrum	k1gNnSc1	centrum
Holdings	Holdings	k1gInSc1	Holdings
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
NetCentrum	NetCentrum	k1gNnSc1	NetCentrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
také	také	k9	také
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Aktualne	Aktualn	k1gMnSc5	Aktualn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Xchat	Xchat	k1gInSc1	Xchat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nebo	nebo	k8xC	nebo
Žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
kterou	který	k3yIgFnSc4	který
plně	plně	k6eAd1	plně
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
americký	americký	k2eAgInSc1d1	americký
fond	fond	k1gInSc1	fond
Warburg	Warburg	k1gMnSc1	Warburg
Pincus	Pincus	k1gMnSc1	Pincus
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
komunity	komunita	k1gFnPc1	komunita
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
hlavními	hlavní	k2eAgInPc7d1	hlavní
pilíři	pilíř	k1gInPc7	pilíř
strategie	strategie	k1gFnSc2	strategie
firmy	firma	k1gFnSc2	firma
Centrum	centrum	k1gNnSc1	centrum
Holdings	Holdings	k1gInSc1	Holdings
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
Lupě	lupa	k1gFnSc6	lupa
článek	článek	k1gInSc1	článek
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nechystá	chystat	k5eNaImIp3nS	chystat
konec	konec	k1gInSc4	konec
portálu	portál	k1gInSc2	portál
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
ICQ	ICQ	kA	ICQ
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
spustil	spustit	k5eAaPmAgMnS	spustit
českou	český	k2eAgFnSc4d1	Česká
verzi	verze	k1gFnSc4	verze
klienta	klient	k1gMnSc2	klient
ICQ	ICQ	kA	ICQ
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
Atlas	Atlas	k1gInSc1	Atlas
ICQ	ICQ	kA	ICQ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgNnPc4d1	doplněné
hrami	hra	k1gFnPc7	hra
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc1	možnost
posílání	posílání	k1gNnSc2	posílání
SMS	SMS	kA	SMS
zdarma	zdarma	k6eAd1	zdarma
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Vodafone	Vodafon	k1gInSc5	Vodafon
a	a	k8xC	a
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předchozími	předchozí	k2eAgMnPc7d1	předchozí
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
načítání	načítání	k1gNnSc4	načítání
a	a	k8xC	a
zobrazování	zobrazování	k1gNnSc4	zobrazování
rušivých	rušivý	k2eAgFnPc2d1	rušivá
reklam	reklama	k1gFnPc2	reklama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
