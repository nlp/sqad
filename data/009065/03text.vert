<p>
<s>
Tulipán	tulipán	k1gInSc4	tulipán
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Tulipa	Tulipa	k1gFnSc1	Tulipa
sylvestris	sylvestris	k1gFnSc1	sylvestris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
cibulnatá	cibulnatý	k2eAgFnSc1d1	cibulnatá
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
liliovitých	liliovitý	k2eAgMnPc2d1	liliovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
často	často	k6eAd1	často
vysazovaná	vysazovaný	k2eAgFnSc1d1	vysazovaná
pro	pro	k7c4	pro
okrasu	okrasa	k1gFnSc4	okrasa
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
zplaňuje	zplaňovat	k5eAaImIp3nS	zplaňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
cibule	cibule	k1gFnSc2	cibule
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
lehce	lehko	k6eAd1	lehko
sivé	sivý	k2eAgInPc1d1	sivý
kopinaté	kopinatý	k2eAgInPc1d1	kopinatý
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
stonek	stonek	k1gInSc1	stonek
nese	nést	k5eAaImIp3nS	nést
květ	květ	k1gInSc4	květ
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
cca	cca	kA	cca
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
cm	cm	kA	cm
(	(	kIx(	(
<g/>
plně	plně	k6eAd1	plně
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prašníky	prašník	k1gInPc1	prašník
i	i	k8xC	i
pestík	pestík	k1gInSc1	pestík
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
rostliny	rostlina	k1gFnSc2	rostlina
za	za	k7c2	za
květu	květ	k1gInSc2	květ
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
květu	květ	k1gInSc2	květ
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
trojboká	trojboký	k2eAgFnSc1d1	trojboká
zašpičatělá	zašpičatělý	k2eAgFnSc1d1	zašpičatělá
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
cibulovina	cibulovina	k1gFnSc1	cibulovina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
především	především	k9	především
Středozemí	středozemí	k1gNnSc2	středozemí
-	-	kIx~	-
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc2	Sardinie
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
areál	areál	k1gInSc1	areál
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
tulipán	tulipán	k1gInSc1	tulipán
nepůvodní	původní	k2eNgInSc1d1	nepůvodní
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
na	na	k7c6	na
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zplanělou	zplanělý	k2eAgFnSc4d1	zplanělá
populaci	populace	k1gFnSc4	populace
Tulipa	Tulip	k1gMnSc2	Tulip
sylvestris	sylvestris	k1gFnSc2	sylvestris
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gInSc1	sylvestris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
a	a	k8xC	a
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
<g/>
,	,	kIx,	,
též	též	k9	též
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
vinic	vinice	k1gFnPc2	vinice
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zámeckých	zámecký	k2eAgInPc6d1	zámecký
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
spíše	spíše	k9	spíše
sušší	suchý	k2eAgNnSc4d2	sušší
<g/>
,	,	kIx,	,
výhřevnější	výhřevný	k2eAgNnPc4d2	výhřevnější
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
okraje	okraj	k1gInPc1	okraj
trávníků	trávník	k1gInPc2	trávník
nebo	nebo	k8xC	nebo
řídké	řídký	k2eAgInPc4d1	řídký
keřové	keřový	k2eAgInPc4d1	keřový
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Zplanělí	zplanělý	k2eAgMnPc1d1	zplanělý
jedinci	jedinec	k1gMnPc1	jedinec
kvetou	kvést	k5eAaImIp3nP	kvést
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
porosty	porost	k1gInPc4	porost
o	o	k7c6	o
desítkách	desítka	k1gFnPc6	desítka
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ale	ale	k9	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
exempláře	exemplář	k1gInPc1	exemplář
je	on	k3xPp3gMnPc4	on
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
např.	např.	kA	např.
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
pražského	pražský	k2eAgInSc2d1	pražský
Petřína	Petřín	k1gInSc2	Petřín
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Lednickém	lednický	k2eAgInSc6d1	lednický
parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://www.biolib.cz/cz/taxon/id42044/	[url]	k4	https://www.biolib.cz/cz/taxon/id42044/
</s>
</p>
<p>
<s>
http://kvetiny.atlasrostlin.cz/tulipan-lesni	[url]	k5eAaPmRp2nS	http://kvetiny.atlasrostlin.cz/tulipan-lesni
</s>
</p>
<p>
<s>
http://www.naturabohemica.cz/tulipa-sylvestris/	[url]	k?	http://www.naturabohemica.cz/tulipa-sylvestris/
</s>
</p>
