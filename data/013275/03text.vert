<p>
<s>
Elena	Elena	k1gFnSc1	Elena
Antalová	Antalová	k1gFnSc1	Antalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
redaktorka	redaktorka	k1gFnSc1	redaktorka
<g/>
,	,	kIx,	,
dramatička	dramatička	k1gFnSc1	dramatička
a	a	k8xC	a
osvětová	osvětový	k2eAgFnSc1d1	osvětová
pracovnice	pracovnice	k1gFnSc1	pracovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
osvětová	osvětový	k2eAgFnSc1d1	osvětová
pracovnice	pracovnice	k1gFnSc1	pracovnice
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
redaktorka	redaktorka	k1gFnSc1	redaktorka
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Javisko	Javisko	k1gNnSc1	Javisko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dramatička	dramatička	k1gFnSc1	dramatička
debutovala	debutovat	k5eAaBmAgFnS	debutovat
1959	[number]	k4	1959
rozhlasovou	rozhlasový	k2eAgFnSc7d1	rozhlasová
hrou	hra	k1gFnSc7	hra
Lujza	Lujza	k1gFnSc1	Lujza
a	a	k8xC	a
Lotka	Lotka	k1gFnSc1	Lotka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
literárních	literární	k2eAgFnPc2d1	literární
předloh	předloha	k1gFnPc2	předloha
napsala	napsat	k5eAaPmAgFnS	napsat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
20	[number]	k4	20
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
věkové	věkový	k2eAgFnPc4d1	věková
kategorie	kategorie	k1gFnPc4	kategorie
mladých	mladý	k2eAgMnPc2d1	mladý
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
autorku	autorka	k1gFnSc4	autorka
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgFnSc4d1	dospívající
mládež	mládež	k1gFnSc4	mládež
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spoluautorkou	spoluautorka	k1gFnSc7	spoluautorka
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
seriálu	seriál	k1gInSc2	seriál
Čo	Čo	k1gFnSc2	Čo
nového	nový	k2eAgInSc2d1	nový
Bielikovci	Bielikovec	k1gMnSc6	Bielikovec
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
debut	debut	k1gInSc4	debut
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
festivalu	festival	k1gInSc2	festival
původních	původní	k2eAgFnPc2d1	původní
slovenských	slovenský	k2eAgFnPc2d1	slovenská
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Piešťanech	Piešťany	k1gInPc6	Piešťany
získala	získat	k5eAaPmAgFnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopédia	Encyklopédium	k1gNnPc1	Encyklopédium
dramatických	dramatický	k2eAgFnPc2d1	dramatická
umení	umeeň	k1gFnPc2	umeeň
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Elena	Elena	k1gFnSc1	Elena
Antalová	Antalová	k1gFnSc1	Antalová
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
