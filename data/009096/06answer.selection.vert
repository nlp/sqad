<s>
Trojhvězda	trojhvězda	k1gFnSc1	trojhvězda
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
a	a	k8xC	a
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
