<p>
<s>
Trojhvězda	trojhvězda	k1gFnSc1	trojhvězda
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
a	a	k8xC	a
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
trojhvězd	trojhvězda	k1gFnPc2	trojhvězda
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc3d1	známá
i	i	k9	i
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Toliman	Toliman	k1gMnSc1	Toliman
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
jen	jen	k6eAd1	jen
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jasnější	jasný	k2eAgFnSc1d2	jasnější
žlutá	žlutý	k2eAgFnSc1d1	žlutá
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
podobá	podobat	k5eAaImIp3nS	podobat
našemu	náš	k3xOp1gNnSc3	náš
slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
další	další	k2eAgFnSc1d1	další
nenápadná	nápadný	k2eNgFnSc1d1	nenápadná
hvězdička	hvězdička	k1gFnSc1	hvězdička
viditelná	viditelný	k2eAgFnSc1d1	viditelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jen	jen	k9	jen
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
světelný	světelný	k2eAgInSc4d1	světelný
měsíc	měsíc	k1gInSc4	měsíc
od	od	k7c2	od
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
na	na	k7c6	na
spojnici	spojnice	k1gFnSc6	spojnice
mezi	mezi	k7c7	mezi
Alfou	alfa	k1gFnSc7	alfa
Centauri	Centaur	k1gFnSc2	Centaur
a	a	k8xC	a
naším	náš	k3xOp1gNnSc7	náš
Sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
naší	náš	k3xOp1gFnSc7	náš
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
se	se	k3xPyFc4	se
za	za	k7c4	za
mnoho	mnoho	k4c4	mnoho
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnPc1	Centaur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
a	a	k8xC	a
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
vázány	vázán	k2eAgFnPc1d1	vázána
gravitací	gravitace	k1gFnSc7	gravitace
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
promítají	promítat	k5eAaImIp3nP	promítat
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
optická	optický	k2eAgFnSc1d1	optická
trojhvězda	trojhvězda	k1gFnSc1	trojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
trojhvězdu	trojhvězda	k1gFnSc4	trojhvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
trojhvězda	trojhvězda	k1gFnSc1	trojhvězda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
