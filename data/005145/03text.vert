<s>
Hillova	Hillův	k2eAgFnSc1d1	Hillova
sféra	sféra	k1gFnSc1	sféra
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
kolem	kolem	k7c2	kolem
nějakého	nějaký	k3yIgNnSc2	nějaký
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc1	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
těleso	těleso	k1gNnSc4	těleso
silnější	silný	k2eAgNnSc4d2	silnější
gravitační	gravitační	k2eAgInSc4d1	gravitační
vliv	vliv	k1gInSc4	vliv
než	než	k8xS	než
jiné	jiný	k2eAgNnSc4d1	jiné
masivnější	masivní	k2eAgNnSc4d2	masivnější
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yRgNnSc2	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
gravitační	gravitační	k2eAgInSc4d1	gravitační
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
než	než	k8xS	než
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
které	který	k3yQgFnSc2	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
musí	muset	k5eAaImIp3nS	muset
ležet	ležet	k5eAaImF	ležet
celá	celý	k2eAgFnSc1d1	celá
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
jejího	její	k3xOp3gInSc2	její
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc4	tento
měsíc	měsíc	k1gInSc4	měsíc
planeta	planeta	k1gFnSc1	planeta
časem	časem	k6eAd1	časem
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Hillova	Hillův	k2eAgFnSc1d1	Hillova
sféra	sféra	k1gFnSc1	sféra
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
sférický	sférický	k2eAgInSc1d1	sférický
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
Lagrangeovy	Lagrangeův	k2eAgInPc1d1	Lagrangeův
body	bod	k1gInPc1	bod
L1	L1	k1gFnSc3	L1
a	a	k8xC	a
L2	L2	k1gFnSc3	L2
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
hraniční	hraniční	k2eAgInPc4d1	hraniční
body	bod	k1gInPc4	bod
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hillovu	Hillův	k2eAgFnSc4d1	Hillova
sféru	sféra	k1gFnSc4	sféra
definoval	definovat	k5eAaBmAgMnS	definovat
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
George	Georg	k1gFnSc2	Georg
William	William	k1gInSc1	William
Hill	Hill	k1gMnSc1	Hill
na	na	k7c6	na
základě	základ	k1gInSc6	základ
práce	práce	k1gFnSc2	práce
francouzského	francouzský	k2eAgMnSc2d1	francouzský
astronoma	astronom	k1gMnSc2	astronom
Edouarda	Edouard	k1gMnSc2	Edouard
Rocheho	Roche	k1gMnSc2	Roche
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Zemi	zem	k1gFnSc4	zem
má	mít	k5eAaImIp3nS	mít
Hillova	Hillův	k2eAgFnSc1d1	Hillova
sféra	sféra	k1gFnSc1	sféra
poloměr	poloměr	k1gInSc1	poloměr
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Rigorozní	Rigorozní	k2eAgNnSc1d1	Rigorozní
odvození	odvození	k1gNnSc1	odvození
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
gravitační	gravitační	k2eAgFnSc4d1	gravitační
sféru	sféra	k1gFnSc4	sféra
vlivu	vliv	k1gInSc2	vliv
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
vyjádření	vyjádření	k1gNnSc4	vyjádření
potenciálu	potenciál	k1gInSc2	potenciál
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
obou	dva	k4xCgNnPc2	dva
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
také	také	k9	také
přibližně	přibližně	k6eAd1	přibližně
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
mez	mez	k1gFnSc4	mez
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
družice	družice	k1gFnSc1	družice
kolem	kolem	k7c2	kolem
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
těles	těleso	k1gNnPc2	těleso
počitaná	počitaný	k2eAgFnSc1d1	počitaný
zvlášť	zvlášť	k6eAd1	zvlášť
(	(	kIx(	(
<g/>
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
menšího	malý	k2eAgNnSc2d2	menší
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
m	m	kA	m
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
hmotnějšího	hmotný	k2eAgNnSc2d2	hmotnější
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
M	M	kA	M
po	po	k7c6	po
eliptické	eliptický	k2eAgFnSc6d1	eliptická
dráze	dráha	k1gFnSc6	dráha
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
poloosou	poloosa	k1gFnSc7	poloosa
a	a	k8xC	a
a	a	k8xC	a
excentricitou	excentricita	k1gFnSc7	excentricita
e	e	k0	e
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
r	r	kA	r
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
<g />
.	.	kIx.	.
</s>
<s hack="1">
sféry	sféra	k1gFnSc2	sféra
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
menší	malý	k2eAgNnSc4d2	menší
těleso	těleso	k1gNnSc4	těleso
přibližně	přibližně	k6eAd1	přibližně
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
≈	≈	k?	≈
a	a	k8xC	a
(	(	kIx(	(
1	[number]	k4	1
−	−	k?	−
e	e	k0	e
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
3	[number]	k4	3
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
a	a	k8xC	a
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
-e	-e	k?	-e
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
M	M	kA	M
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tvar	tvar	k1gInSc4	tvar
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
menšího	malý	k2eAgNnSc2d2	menší
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
blíží	blížit	k5eAaImIp3nS	blížit
tvaru	tvar	k1gInSc3	tvar
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
excentricity	excentricita	k1gFnSc2	excentricita
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
ji	on	k3xPp3gFnSc4	on
zanedbat	zanedbat	k5eAaPmF	zanedbat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
poloměr	poloměr	k1gInSc4	poloměr
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
určit	určit	k5eAaPmF	určit
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
≈	≈	k?	≈
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
3	[number]	k4	3
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
a	a	k8xC	a
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
<g/>
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
M	M	kA	M
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
přibližný	přibližný	k2eAgInSc1d1	přibližný
<g/>
,	,	kIx,	,
stabilní	stabilní	k2eAgFnSc1d1	stabilní
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
existují	existovat	k5eAaImIp3nP	existovat
spíše	spíše	k9	spíše
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
poloměru	poloměr	k1gInSc2	poloměr
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
platí	platit	k5eAaImIp3nS	platit
tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
<g/>
:	:	kIx,	:
m	m	kA	m
=	=	kIx~	=
5,97	[number]	k4	5,97
<g/>
×	×	k?	×
<g/>
1024	[number]	k4	1024
kg	kg	kA	kg
<g/>
,	,	kIx,	,
M	M	kA	M
=	=	kIx~	=
1,99	[number]	k4	1,99
<g/>
×	×	k?	×
<g/>
1030	[number]	k4	1030
kg	kg	kA	kg
<g/>
,	,	kIx,	,
a	a	k8xC	a
=	=	kIx~	=
149,6	[number]	k4	149,6
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
=	=	kIx~	=
149,6	[number]	k4	149,6
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
m.	m.	k?	m.
Hodnota	hodnota	k1gFnSc1	hodnota
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
pro	pro	k7c4	pro
Zemi	zem	k1gFnSc4	zem
tedy	tedy	k9	tedy
vychází	vycházet	k5eAaImIp3nS	vycházet
kolem	kolem	k7c2	kolem
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
(	(	kIx(	(
<g/>
0,01	[number]	k4	0,01
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Měsíce	měsíc	k1gInSc2	měsíc
má	mít	k5eAaImIp3nS	mít
poloměr	poloměr	k1gInSc4	poloměr
0,384	[number]	k4	0,384
400	[number]	k4	400
milionu	milion	k4xCgInSc2	milion
km	km	kA	km
a	a	k8xC	a
pohodlně	pohodlně	k6eAd1	pohodlně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
spočtené	spočtený	k2eAgFnSc2d1	spočtená
hodnoty	hodnota	k1gFnSc2	hodnota
poloměru	poloměr	k1gInSc2	poloměr
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
pro	pro	k7c4	pro
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nehrozí	hrozit	k5eNaImIp3nS	hrozit
tedy	tedy	k9	tedy
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Měsíc	měsíc	k1gInSc1	měsíc
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
odtržen	odtrhnout	k5eAaPmNgInS	odtrhnout
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
obíhat	obíhat	k5eAaImF	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Rocheova	Rocheův	k2eAgFnSc1d1	Rocheova
sféra	sféra	k1gFnSc1	sféra
</s>
