<s>
Hillova	Hillův	k2eAgFnSc1d1	Hillova
sféra	sféra	k1gFnSc1	sféra
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
kolem	kolem	k7c2	kolem
nějakého	nějaký	k3yIgNnSc2	nějaký
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc1	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
těleso	těleso	k1gNnSc4	těleso
silnější	silný	k2eAgNnSc4d2	silnější
gravitační	gravitační	k2eAgInSc4d1	gravitační
vliv	vliv	k1gInSc4	vliv
než	než	k8xS	než
jiné	jiný	k2eAgNnSc4d1	jiné
masivnější	masivní	k2eAgNnSc4d2	masivnější
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yQgNnSc2	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
