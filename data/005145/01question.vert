<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
těleso	těleso	k1gNnSc4	těleso
větší	veliký	k2eAgNnSc4d2	veliký
gravitační	gravitační	k2eAgInSc4d1	gravitační
vliv	vliv	k1gInSc4	vliv
než	než	k8xS	než
jiné	jiný	k2eAgNnSc4d1	jiné
masivnější	masivní	k2eAgNnSc4d2	masivnější
těleso	těleso	k1gNnSc4	těleso
<g/>
?	?	kIx.	?
</s>
