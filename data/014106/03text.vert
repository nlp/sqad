<s>
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1853	#num#	k4
<g/>
Bern	Bern	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Ženeva	Ženeva	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
designér	designér	k1gMnSc1
<g/>
,	,	kIx,
litograf	litograf	k1gMnSc1
a	a	k8xC
grafik	grafik	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
Ženevská	ženevský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
honorary	honorara	k1gFnPc1
doctor	doctor	k1gInSc1
of	of	k?
the	the	k?
University	universita	k1gFnSc2
of	of	k?
Basel	Basel	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Hector	Hector	k1gMnSc1
Hodler	Hodler	k1gMnSc1
Podpis	podpis	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1853	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
švýcarský	švýcarský	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Ženevské	ženevský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
v	v	k7c6
pohledu	pohled	k1gInSc6
od	od	k7c2
Chexbres	Chexbresa	k1gFnPc2
</s>
<s>
Hodler	Hodler	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Bernu	Bern	k1gInSc6
jako	jako	k9
nejstarší	starý	k2eAgFnSc1d3
ze	z	k7c2
šesti	šest	k4xCc2
dětí	dítě	k1gFnPc2
v	v	k7c6
rodině	rodina	k1gFnSc6
truhláře	truhlář	k1gMnSc2
Jeana	Jean	k1gMnSc2
Hodlera	Hodler	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Marguerite	Marguerit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osmi	osm	k4xCc2
letech	léto	k1gNnPc6
mu	on	k3xPp3gMnSc3
zemřel	zemřít	k5eAaPmAgMnS
otec	otec	k1gMnSc1
a	a	k8xC
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
na	na	k7c4
tuberkulózu	tuberkulóza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
malíře	malíř	k1gMnSc2
Gottlieba	Gottlieb	k1gMnSc2
Schüpacha	Schüpach	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
z	z	k7c2
prvního	první	k4xOgNnSc2
manželství	manželství	k1gNnSc2
5	#num#	k4
dětí	dítě	k1gFnPc2
a	a	k8xC
do	do	k7c2
nově	nově	k6eAd1
rodiny	rodina	k1gFnPc1
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Ženevy	Ženeva	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
malířskou	malířský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
především	především	k9
z	z	k7c2
krajinomaleb	krajinomalba	k1gFnPc2
<g/>
,	,	kIx,
portrétů	portrét	k1gInPc2
a	a	k8xC
figurálních	figurální	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
části	část	k1gFnSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
přešel	přejít	k5eAaPmAgInS
k	k	k7c3
symbolismu	symbolismus	k1gInSc3
a	a	k8xC
secesi	secese	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
také	také	k9
vlastní	vlastní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
říkal	říkat	k5eAaImAgInS
paralelizmus	paralelizmus	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
rysem	rys	k1gInSc7
je	být	k5eAaImIp3nS
symetrické	symetrický	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
figur	figura	k1gFnPc2
jakoby	jakoby	k8xS
v	v	k7c6
tanci	tanec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1889	#num#	k4
se	se	k3xPyFc4
Hodler	Hodler	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Berthe	Berthe	k1gFnSc7
Jacques	Jacques	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
byl	být	k5eAaImAgMnS
Hector	Hector	k1gMnSc1
Hodler	Hodler	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
Světového	světový	k2eAgInSc2d1
esperantského	esperantský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
stál	stát	k5eAaImAgMnS
otci	otec	k1gMnSc3
modelem	model	k1gInSc7
v	v	k7c6
nepřirozených	přirozený	k2eNgFnPc6d1
polohách	poloha	k1gFnPc6
<g/>
,	,	kIx,
oblečený	oblečený	k2eAgMnSc1d1
nebo	nebo	k8xC
nahý	nahý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ve	v	k7c6
33	#num#	k4
letech	léto	k1gNnPc6
nečekaně	nečekaně	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
přičítalo	přičítat	k5eAaImAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
stání	stání	k1gNnSc2
modelem	model	k1gInSc7
podlomilo	podlomit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc1
zdraví	zdraví	k1gNnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
však	však	k9
lékařsky	lékařsky	k6eAd1
ověřeno	ověřen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gMnSc1
odsoudil	odsoudit	k5eAaPmAgMnS
německý	německý	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Remeš	Remeš	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
odstranění	odstranění	k1gNnSc4
svých	svůj	k3xOyFgInPc2
obrazů	obraz	k1gInPc2
z	z	k7c2
německých	německý	k2eAgFnPc2d1
obrazáren	obrazárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
jeho	jeho	k3xOp3gMnPc2
milenka	milenka	k1gFnSc1
Valentine	Valentin	k1gMnSc5
Godé-Darel	Godé-Darel	k1gMnSc1
zemřela	zemřít	k5eAaPmAgFnS
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodler	Hodler	k1gMnSc1
strávil	strávit	k5eAaPmAgMnS
hodiny	hodina	k1gFnPc4
u	u	k7c2
jejího	její	k3xOp3gNnSc2
lože	lože	k1gNnSc2
a	a	k8xC
maloval	malovat	k5eAaImAgMnS
její	její	k3xOp3gNnSc4
utrpení	utrpení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Portrét	portrét	k1gInSc1
Louise-Delphine	Louise-Delphin	k1gMnSc5
Duchosalové	Duchosalové	k2eAgFnPc3d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HUGELSHOFER	HUGELSHOFER	kA
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodler	Hodler	k1gMnSc1
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.deutsche-biographie.de	www.deutsche-biographie.de	k6eAd1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ferdinand	Ferdinand	k1gMnSc1
Hodler	Hodler	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
F.	F.	kA
Hodlera	Hodler	k1gMnSc2
v	v	k7c6
němčině	němčina	k1gFnSc6
</s>
<s>
Publikace	publikace	k1gFnSc1
o	o	k7c6
Ferdinandu	Ferdinand	k1gMnSc6
Hodlerovi	Hodler	k1gMnSc6
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
</s>
<s>
Film	film	k1gInSc1
o	o	k7c6
Ferdinandu	Ferdinand	k1gMnSc6
Hodlerovi	Hodler	k1gMnSc6
</s>
<s>
Román	román	k1gInSc1
o	o	k7c6
Hodlerovi	Hodler	k1gMnSc6
v	v	k7c6
esperantu	esperanto	k1gNnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990009603	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118551817	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0891	#num#	k4
0082	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50031263	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500027184	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
41848532	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50031263	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
