<p>
<s>
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
dub	dub	k1gInSc1	dub
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
listnatým	listnatý	k2eAgInPc3d1	listnatý
stromům	strom	k1gInPc3	strom
v	v	k7c6	v
Lomnických	lomnický	k2eAgInPc6d1	lomnický
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
dožívajícím	dožívající	k2eAgNnSc7d1	dožívající
torzem	torzo	k1gNnSc7	torzo
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
bývaly	bývat	k5eAaImAgFnP	bývat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Blanska	Blansko	k1gNnSc2	Blansko
velké	velký	k2eAgInPc4d1	velký
duby	dub	k1gInPc4	dub
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
podmínky	podmínka	k1gFnSc2	podmínka
jim	on	k3xPp3gMnPc3	on
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
a	a	k8xC	a
staré	starý	k2eAgInPc1d1	starý
bukovo-dubové	bukovoubový	k2eAgInPc1d1	bukovo-dubový
porosty	porost	k1gInPc1	porost
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
široké	široký	k2eAgNnSc4d1	široké
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
ale	ale	k9	ale
padly	padnout	k5eAaImAgInP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
místním	místní	k2eAgFnPc3d1	místní
železárnám	železárna	k1gFnPc3	železárna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
statné	statný	k2eAgInPc1d1	statný
duby	dub	k1gInPc1	dub
a	a	k8xC	a
buky	buk	k1gInPc1	buk
pokáceny	pokácen	k2eAgInPc1d1	pokácen
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1732	[number]	k4	1732
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
Lomnický	lomnický	k2eAgInSc1d1	lomnický
dub	dub	k1gInSc1	dub
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
18	[number]	k4	18
m	m	kA	m
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
6	[number]	k4	6
m	m	kA	m
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
<g/>
:	:	kIx,	:
520	[number]	k4	520
cm	cm	kA	cm
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
průměr	průměr	k1gInSc1	průměr
190	[number]	k4	190
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
550	[number]	k4	550
cm	cm	kA	cm
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
775	[number]	k4	775
cm	cm	kA	cm
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
průměr	průměr	k1gInSc1	průměr
240	[number]	k4	240
cm	cm	kA	cm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
věk	věk	k1gInSc1	věk
<g/>
:	:	kIx,	:
800	[number]	k4	800
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
500-600	[number]	k4	500-600
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
?	?	kIx.	?
</s>
</p>
<p>
<s>
nominován	nominován	k2eAgMnSc1d1	nominován
do	do	k7c2	do
ankety	anketa	k1gFnSc2	anketa
Strom	strom	k1gInSc1	strom
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
:	:	kIx,	:
49	[number]	k4	49
<g/>
°	°	k?	°
<g/>
24	[number]	k4	24
<g/>
'	'	kIx"	'
<g/>
1.48	[number]	k4	1.48
<g/>
"	"	kIx"	"
<g/>
N	N	kA	N
16	[number]	k4	16
<g/>
°	°	k?	°
<g/>
25	[number]	k4	25
<g/>
'	'	kIx"	'
<g/>
47.25	[number]	k4	47.25
<g/>
"	"	kIx"	"
<g/>
EDub	EDub	k1gInSc1	EDub
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
v	v	k7c6	v
Zámeckém	zámecký	k2eAgInSc6d1	zámecký
lese	les	k1gInSc6	les
nad	nad	k7c7	nad
potokem	potok	k1gInSc7	potok
Besének	Besénka	k1gFnPc2	Besénka
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Ledňov	Ledňov	k1gInSc1	Ledňov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stav	stav	k1gInSc1	stav
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
==	==	k?	==
</s>
</p>
<p>
<s>
Torzo	torzo	k1gNnSc1	torzo
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
bez	bez	k7c2	bez
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živé	živý	k2eAgFnSc2d1	živá
části	část	k1gFnSc2	část
stromu	strom	k1gInSc2	strom
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
jedna	jeden	k4xCgFnSc1	jeden
silnější	silný	k2eAgFnSc1d2	silnější
větev	větev	k1gFnSc1	větev
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
původní	původní	k2eAgInSc4d1	původní
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
korunu	koruna	k1gFnSc4	koruna
<g/>
)	)	kIx)	)
a	a	k8xC	a
níž	nízce	k6eAd2	nízce
několik	několik	k4yIc4	několik
mladších	mladý	k2eAgFnPc2d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Dub	dub	k1gInSc1	dub
není	být	k5eNaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
registru	registr	k1gInSc6	registr
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
nebyl	být	k5eNaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
jako	jako	k8xS	jako
památný	památný	k2eAgInSc1d1	památný
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
špatného	špatný	k2eAgInSc2d1	špatný
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Sanace	sanace	k1gFnSc1	sanace
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
existenci	existence	k1gFnSc4	existence
bezpodmínečná	bezpodmínečný	k2eAgFnSc1d1	bezpodmínečná
(	(	kIx(	(
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
rozvalení	rozvalení	k1gNnSc1	rozvalení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
datované	datovaný	k2eAgFnSc2d1	datovaná
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
dubu	dub	k1gInSc2	dub
prý	prý	k9	prý
upálen	upálen	k2eAgMnSc1d1	upálen
prchající	prchající	k2eAgMnSc1d1	prchající
lomnický	lomnický	k2eAgMnSc1d1	lomnický
kněz	kněz	k1gMnSc1	kněz
husitskými	husitský	k2eAgMnPc7d1	husitský
vzbouřenci	vzbouřenec	k1gMnPc7	vzbouřenec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc4	strom
800	[number]	k4	800
let	léto	k1gNnPc2	léto
starý	starý	k2eAgMnSc1d1	starý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
pověstí	pověst	k1gFnSc7	pověst
vyneslo	vynést	k5eAaPmAgNnS	vynést
přízvisko	přízvisko	k1gNnSc1	přízvisko
Žižkův	Žižkův	k2eAgMnSc1d1	Žižkův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
skutečně	skutečně	k6eAd1	skutečně
bývaly	bývat	k5eAaImAgFnP	bývat
stopy	stopa	k1gFnPc1	stopa
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
údajně	údajně	k6eAd1	údajně
novějšího	nový	k2eAgNnSc2d2	novější
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památné	památný	k2eAgInPc4d1	památný
a	a	k8xC	a
významné	významný	k2eAgInPc4d1	významný
stromy	strom	k1gInPc4	strom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Klen	klen	k1gInSc1	klen
v	v	k7c6	v
Lomnici	Lomnice	k1gFnSc6	Lomnice
</s>
</p>
<p>
<s>
Bohunčina	Bohunčin	k2eAgFnSc1d1	Bohunčina
lípa	lípa	k1gFnSc1	lípa
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
buk	buk	k1gInSc1	buk
v	v	k7c6	v
zámecké	zámecký	k2eAgFnSc6d1	zámecká
zahradě	zahrada	k1gFnSc6	zahrada
</s>
</p>
<p>
<s>
Lomnická	lomnický	k2eAgFnSc1d1	Lomnická
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Žižkovy	Žižkův	k2eAgInPc1d1	Žižkův
duby	dub	k1gInPc1	dub
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgFnSc2d1	zimní
</s>
</p>
<p>
<s>
Lomnice	Lomnice	k1gFnSc1	Lomnice
</s>
</p>
<p>
<s>
Zámecký	zámecký	k2eAgInSc1d1	zámecký
les	les	k1gInSc1	les
v	v	k7c6	v
Lomnici	Lomnice	k1gFnSc6	Lomnice
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
upáleném	upálený	k2eAgMnSc6d1	upálený
knězi	kněz	k1gMnSc6	kněz
ekoporadna	ekoporadna	k1gFnSc1	ekoporadna
<g/>
.	.	kIx.	.
<g/>
tisnovsko	tisnovsko	k1gNnSc1	tisnovsko
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
</s>
</p>
<p>
<s>
Snímek	snímek	k1gInSc1	snímek
dubu	dub	k1gInSc2	dub
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ekoporadna	ekoporadno	k1gNnSc2	ekoporadno
<g/>
.	.	kIx.	.
<g/>
tisnovsko	tisnovsko	k1gNnSc1	tisnovsko
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
</s>
</p>
