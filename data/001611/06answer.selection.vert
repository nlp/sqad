<s>
Frits	Frits	k1gInSc1	Frits
(	(	kIx(	(
<g/>
Frederik	Frederik	k1gMnSc1	Frederik
<g/>
)	)	kIx)	)
Zernike	Zernike	k1gInSc1	Zernike
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Noarden	Noardno	k1gNnPc2	Noardno
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
