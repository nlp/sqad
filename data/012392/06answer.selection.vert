<s>
Socha	Socha	k1gMnSc1	Socha
Lenina	Lenin	k1gMnSc2	Lenin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Statue	statue	k1gFnPc1	statue
of	of	k?	of
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
Popradský	popradský	k2eAgMnSc1d1	popradský
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
5	[number]	k4	5
metry	metr	k1gInPc4	metr
největší	veliký	k2eAgFnSc7d3	veliký
sochou	socha	k1gFnSc7	socha
Vladimira	Vladimir	k1gMnSc2	Vladimir
Iljiče	Iljič	k1gMnSc2	Iljič
Lenina	Lenin	k1gMnSc2	Lenin
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
