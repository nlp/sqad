<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Lenina	Lenin	k1gMnSc2	Lenin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Statue	statue	k1gFnPc1	statue
of	of	k?	of
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xS	jako
Popradský	popradský	k2eAgMnSc1d1	popradský
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
5	[number]	k4	5
metry	metr	k1gInPc4	metr
největší	veliký	k2eAgFnSc7d3	veliký
sochou	socha	k1gFnSc7	socha
Vladimira	Vladimir	k1gMnSc2	Vladimir
Iljiče	Iljič	k1gMnSc2	Iljič
Lenina	Lenin	k1gMnSc2	Lenin
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Fremont	Fremonta	k1gFnPc2	Fremonta
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Seattle	Seattle	k1gFnSc2	Seattle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
sochařem	sochař	k1gMnSc7	sochař
Emilem	Emil	k1gMnSc7	Emil
Venkovem	venkov	k1gInSc7	venkov
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
městě	město	k1gNnSc6	město
Poprad	Poprad	k1gInSc1	Poprad
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Venkov	venkov	k1gInSc1	venkov
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
naplňoval	naplňovat	k5eAaImAgMnS	naplňovat
požadavky	požadavek	k1gInPc4	požadavek
státní	státní	k2eAgFnSc2d1	státní
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
Lenina	Lenin	k1gMnSc4	Lenin
vyobrazit	vyobrazit	k5eAaPmF	vyobrazit
jako	jako	k8xS	jako
násilného	násilný	k2eAgMnSc4d1	násilný
revolucionáře	revolucionář	k1gMnSc4	revolucionář
<g/>
;	;	kIx,	;
nejen	nejen	k6eAd1	nejen
tradičního	tradiční	k2eAgMnSc2d1	tradiční
intelektuála	intelektuál	k1gMnSc2	intelektuál
a	a	k8xC	a
teoretika	teoretik	k1gMnSc2	teoretik
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
režimu	režim	k1gInSc2	režim
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
<g/>
Navzdory	navzdory	k6eAd1	navzdory
obecné	obecný	k2eAgFnSc6d1	obecná
víře	víra	k1gFnSc6	víra
nebyla	být	k5eNaImAgFnS	být
socha	socha	k1gFnSc1	socha
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
stržena	strhnout	k5eAaPmNgFnS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
tichosti	tichost	k1gFnSc6	tichost
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
a	a	k8xC	a
uložena	uložit	k5eAaPmNgFnS	uložit
na	na	k7c4	na
pozemek	pozemek	k1gInSc4	pozemek
Technických	technický	k2eAgFnPc2d1	technická
služeb	služba	k1gFnPc2	služba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
restitucí	restituce	k1gFnSc7	restituce
vrácen	vrátit	k5eAaPmNgInS	vrátit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
rodiny	rodina	k1gFnSc2	rodina
Rothových	Rothových	k2eAgFnSc2d1	Rothových
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
radní	radní	k1gMnPc1	radní
města	město	k1gNnSc2	město
Poprad	Poprad	k1gInSc4	Poprad
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
sochu	socha	k1gFnSc4	socha
nikdo	nikdo	k3yNnSc1	nikdo
nekoupí	koupit	k5eNaPmIp3nS	koupit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ji	on	k3xPp3gFnSc4	on
nechají	nechat	k5eAaPmIp3nP	nechat
roztavit	roztavit	k5eAaPmF	roztavit
ve	v	k7c6	v
VSŽ	VSŽ	kA	VSŽ
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
nechají	nechat	k5eAaPmIp3nP	nechat
peníze	peníz	k1gInPc1	peníz
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
najde	najít	k5eAaPmIp3nS	najít
vhodného	vhodný	k2eAgMnSc4d1	vhodný
zájemce	zájemce	k1gMnSc4	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
prohlášení	prohlášení	k1gNnSc6	prohlášení
si	se	k3xPyFc3	se
sochy	socha	k1gFnSc2	socha
všiml	všimnout	k5eAaPmAgMnS	všimnout
majitel	majitel	k1gMnSc1	majitel
zimního	zimní	k2eAgInSc2d1	zimní
stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
1993	[number]	k4	1993
Anton	Anton	k1gMnSc1	Anton
Danko	Danka	k1gFnSc5	Danka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
svého	svůj	k3xOyFgMnSc4	svůj
amerického	americký	k2eAgMnSc4d1	americký
přítele	přítel	k1gMnSc4	přítel
Lewise	Lewise	k1gFnSc2	Lewise
E.	E.	kA	E.
Carpentera	Carpenter	k1gMnSc2	Carpenter
<g/>
,	,	kIx,	,
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
učitele	učitel	k1gMnSc2	učitel
angličtiny	angličtina	k1gFnSc2	angličtina
v	v	k7c6	v
Popradu	Poprad	k1gInSc6	Poprad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
místním	místní	k2eAgMnSc7d1	místní
novinářem	novinář	k1gMnSc7	novinář
Tomášem	Tomáš	k1gMnSc7	Tomáš
Fülöppem	Fülöpp	k1gMnSc7	Fülöpp
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
městské	městský	k2eAgFnPc4d1	městská
radní	radní	k1gFnPc4	radní
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
značnou	značný	k2eAgFnSc4d1	značná
neoblíbenost	neoblíbenost	k1gFnSc4	neoblíbenost
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
stále	stále	k6eAd1	stále
uměleckým	umělecký	k2eAgInSc7d1	umělecký
dílem	díl	k1gInSc7	díl
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zachování	zachování	k1gNnSc4	zachování
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Carpenter	Carpenter	k1gInSc1	Carpenter
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
odkoupení	odkoupení	k1gNnSc4	odkoupení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stalo	stát	k5eAaPmAgNnS	stát
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
Lenina	Lenin	k1gMnSc2	Lenin
potom	potom	k6eAd1	potom
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
částech	část	k1gFnPc6	část
dopravili	dopravit	k5eAaPmAgMnP	dopravit
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
transportem	transport	k1gInSc7	transport
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
značné	značný	k2eAgInPc1d1	značný
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
finanční	finanční	k2eAgFnSc1d1	finanční
–	–	k?	–
přeprava	přeprava	k1gFnSc1	přeprava
stála	stát	k5eAaImAgFnS	stát
téměř	téměř	k6eAd1	téměř
třikrát	třikrát	k6eAd1	třikrát
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
samotná	samotný	k2eAgFnSc1d1	samotná
socha	socha	k1gFnSc1	socha
–	–	k?	–
41	[number]	k4	41
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
rozruchu	rozruch	k1gInSc2	rozruch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
dovozu	dovoz	k1gInSc6	dovoz
sochy	socha	k1gFnSc2	socha
nastal	nastat	k5eAaPmAgInS	nastat
<g/>
,	,	kIx,	,
však	však	k9	však
Carpenter	Carpenter	k1gInSc1	Carpenter
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
tak	tak	k6eAd1	tak
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
za	za	k7c4	za
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
místní	místní	k2eAgFnSc2d1	místní
slévárny	slévárna	k1gFnSc2	slévárna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
roh	roh	k1gInSc4	roh
ulic	ulice	k1gFnPc2	ulice
N	N	kA	N
34	[number]	k4	34
<g/>
th	th	k?	th
St	St	kA	St
a	a	k8xC	a
Evanston	Evanston	k1gInSc1	Evanston
Ave	ave	k1gNnSc2	ave
N	N	kA	N
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Fremont	Fremonta	k1gFnPc2	Fremonta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
severněji	severně	k6eAd2	severně
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Evanston	Evanston	k1gInSc1	Evanston
Ave	ave	k1gNnSc4	ave
N	N	kA	N
<g/>
,	,	kIx,	,
N	N	kA	N
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
St	St	kA	St
a	a	k8xC	a
Fremont	Fremont	k1gInSc1	Fremont
Place	plac	k1gInSc6	plac
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
Fremontského	Fremontský	k2eAgMnSc2d1	Fremontský
trolla	troll	k1gMnSc2	troll
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgFnPc1d1	známá
atrakce	atrakce	k1gFnPc1	atrakce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Carpenterových	Carpenterův	k2eAgFnPc2d1	Carpenterova
pro	pro	k7c4	pro
sochu	socha	k1gFnSc4	socha
nadále	nadále	k6eAd1	nadále
hledá	hledat	k5eAaImIp3nS	hledat
vhodného	vhodný	k2eAgMnSc4d1	vhodný
kupce	kupec	k1gMnSc4	kupec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zvýšení	zvýšení	k1gNnSc4	zvýšení
původní	původní	k2eAgFnSc2d1	původní
ceny	cena	k1gFnSc2	cena
na	na	k7c4	na
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
<s>
Umístění	umístění	k1gNnSc1	umístění
sochy	socha	k1gFnSc2	socha
na	na	k7c6	na
veřejném	veřejný	k2eAgNnSc6d1	veřejné
prostranství	prostranství	k1gNnSc6	prostranství
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
a	a	k8xC	a
provázely	provázet	k5eAaImAgFnP	provázet
ho	on	k3xPp3gNnSc4	on
protesty	protest	k1gInPc1	protest
jak	jak	k8xC	jak
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stala	stát	k5eAaPmAgFnS	stát
vyhledávaným	vyhledávaný	k2eAgInSc7d1	vyhledávaný
cílem	cíl	k1gInSc7	cíl
vandalů	vandal	k1gMnPc2	vandal
–	–	k?	–
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vztyčení	vztyčení	k1gNnSc6	vztyčení
jí	jíst	k5eAaImIp3nS	jíst
neznámý	známý	k2eNgMnSc1d1	neznámý
pachatelé	pachatel	k1gMnPc1	pachatel
pomalovali	pomalovat	k5eAaPmAgMnP	pomalovat
ruce	ruka	k1gFnPc4	ruka
červenou	červená	k1gFnSc7	červená
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
připomínat	připomínat	k5eAaImF	připomínat
krev	krev	k1gFnSc4	krev
jako	jako	k8xC	jako
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
stranou	strana	k1gFnSc7	strana
dopouštěl	dopouštět	k5eAaImAgInS	dopouštět
proti	proti	k7c3	proti
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
umělec	umělec	k1gMnSc1	umělec
Frederick	Frederick	k1gMnSc1	Frederick
Edelblut	Edelblut	k1gMnSc1	Edelblut
o	o	k7c6	o
soše	socha	k1gFnSc6	socha
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neúcta	neúcta	k1gFnSc1	neúcta
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
pošpinění	pošpinění	k1gNnSc2	pošpinění
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
komunistické	komunistický	k2eAgFnSc2d1	komunistická
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
také	také	k9	také
Lenina	Lenin	k1gMnSc4	Lenin
každoročně	každoročně	k6eAd1	každoročně
zdobí	zdobit	k5eAaImIp3nS	zdobit
zářivá	zářivý	k2eAgFnSc1d1	zářivá
rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
hlavu	hlava	k1gFnSc4	hlava
umisťována	umisťován	k2eAgFnSc1d1	umisťován
během	během	k7c2	během
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Socha	socha	k1gFnSc1	socha
Lenina	Lenin	k2eAgFnSc1d1	Lenina
na	na	k7c6	na
Flickru	Flickr	k1gInSc6	Flickr
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
and	and	k?	and
the	the	k?	the
Spanish	Spanish	k1gMnSc1	Spanish
Dancer	Dancer	k1gMnSc1	Dancer
<g/>
;	;	kIx,	;
a	a	k8xC	a
Communist	Communist	k1gMnSc1	Communist
Comes	Comes	k1gMnSc1	Comes
to	ten	k3xDgNnSc4	ten
America	America	k1gFnSc1	America
<g/>
;	;	kIx,	;
Filth	Filth	k1gInSc1	Filth
Party	parta	k1gFnSc2	parta
<g/>
"	"	kIx"	"
–	–	k?	–
díl	díl	k1gInSc1	díl
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Monumentální	monumentální	k2eAgFnSc2d1	monumentální
záhady	záhada	k1gFnSc2	záhada
na	na	k7c4	na
Travel	Travel	k1gInSc4	Travel
Channel	Channel	k1gInSc1	Channel
(	(	kIx(	(
<g/>
start	start	k1gInSc1	start
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
