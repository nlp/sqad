<p>
<s>
Bagr	bagr	k1gInSc1	bagr
<g/>
,	,	kIx,	,
rypadlo	rypadlo	k1gNnSc1	rypadlo
nebo	nebo	k8xC	nebo
také	také	k9	také
exkavátor	exkavátor	k1gInSc1	exkavátor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
excavator	excavator	k1gInSc1	excavator
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
a	a	k8xC	a
přemisťování	přemisťování	k1gNnSc3	přemisťování
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
patří	patřit	k5eAaImIp3nS	patřit
podkop	podkop	k1gInSc1	podkop
s	s	k7c7	s
pracovním	pracovní	k2eAgNnSc7d1	pracovní
zařízením	zařízení	k1gNnSc7	zařízení
(	(	kIx(	(
<g/>
lopatou	lopata	k1gFnSc7	lopata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kabina	kabina	k1gFnSc1	kabina
s	s	k7c7	s
pracovištěm	pracoviště	k1gNnSc7	pracoviště
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
podvozek	podvozek	k1gInSc1	podvozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Bagry	bagr	k1gInPc4	bagr
dělíme	dělit	k5eAaImIp1nP	dělit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hydraulický	hydraulický	k2eAgInSc1d1	hydraulický
bagr	bagr	k1gInSc1	bagr
–	–	k?	–
dnes	dnes	k6eAd1	dnes
většina	většina	k1gFnSc1	většina
bagrů	bagr	k1gInPc2	bagr
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
pracovní	pracovní	k2eAgFnPc1d1	pracovní
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
ovládány	ovládán	k2eAgInPc4d1	ovládán
hydraulickými	hydraulický	k2eAgInPc7d1	hydraulický
válci	válec	k1gInPc7	válec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pístnicemi	pístnice	k1gFnPc7	pístnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lanový	lanový	k2eAgInSc1d1	lanový
bagrZvláštními	bagrZvláštní	k2eAgInPc7d1	bagrZvláštní
druhy	druh	k1gInPc7	druh
bagrů	bagr	k1gInPc2	bagr
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sací	sací	k2eAgInSc1d1	sací
bagr	bagr	k1gInSc1	bagr
</s>
</p>
<p>
<s>
Korečkové	korečkový	k2eAgNnSc1d1	korečkové
rypadlo	rypadlo	k1gNnSc1	rypadlo
</s>
</p>
<p>
<s>
Kolesové	kolesový	k2eAgNnSc1d1	kolesové
rypadloDalší	rypadloDalší	k2eAgNnSc1d1	rypadloDalší
členění	členění	k1gNnSc1	členění
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
pohybu	pohyb	k1gInSc2	pohyb
bagru	bagr	k1gInSc2	bagr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rypadlo-nakladač	Rypadloakladač	k1gInSc1	Rypadlo-nakladač
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
</s>
</p>
<p>
<s>
Bagr	bagr	k1gInSc1	bagr
na	na	k7c6	na
pásovém	pásový	k2eAgInSc6d1	pásový
podvozku	podvozek	k1gInSc6	podvozek
</s>
</p>
<p>
<s>
Bagr	bagr	k1gInSc1	bagr
na	na	k7c6	na
kolovém	kolový	k2eAgInSc6d1	kolový
podvozku	podvozek	k1gInSc6	podvozek
</s>
</p>
<p>
<s>
Kráčivý	kráčivý	k2eAgMnSc1d1	kráčivý
bagrMimo	bagrMimo	k6eAd1	bagrMimo
výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgNnPc1d1	uvedené
dělení	dělení	k1gNnPc1	dělení
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgNnPc1d1	další
hlediska	hledisko	k1gNnPc1	hledisko
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgNnPc1	tento
nemají	mít	k5eNaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bagr	bagr	k1gInSc4	bagr
na	na	k7c6	na
pásovém	pásový	k2eAgInSc6d1	pásový
či	či	k8xC	či
kolovém	kolový	k2eAgInSc6d1	kolový
podvozku	podvozek	k1gInSc6	podvozek
==	==	k?	==
</s>
</p>
<p>
<s>
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc4	tento
typy	typ	k1gInPc4	typ
shodné	shodný	k2eAgInPc4d1	shodný
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc4d1	jediný
rozdíl	rozdíl	k1gInSc4	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podvozku	podvozek	k1gInSc6	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Pásový	pásový	k2eAgInSc1d1	pásový
podvozek	podvozek	k1gInSc1	podvozek
má	mít	k5eAaImIp3nS	mít
výhodu	výhoda	k1gFnSc4	výhoda
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
průchodnosti	průchodnost	k1gFnSc6	průchodnost
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
v	v	k7c6	v
lepší	dobrý	k2eAgFnSc6d2	lepší
stabilitě	stabilita	k1gFnSc6	stabilita
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
kolový	kolový	k2eAgInSc1d1	kolový
podvozek	podvozek	k1gInSc1	podvozek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadnější	snadný	k2eAgInSc1d2	snadnější
a	a	k8xC	a
rychlejší	rychlý	k2eAgInSc1d2	rychlejší
přesun	přesun	k1gInSc1	přesun
bagru	bagr	k1gInSc2	bagr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
po	po	k7c6	po
silnicích	silnice	k1gFnPc6	silnice
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
===	===	k?	===
</s>
</p>
<p>
<s>
Podkop	podkop	k1gInSc1	podkop
(	(	kIx(	(
<g/>
rameno	rameno	k1gNnSc1	rameno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnPc4d1	základní
pracovní	pracovní	k2eAgFnPc4d1	pracovní
části	část	k1gFnPc4	část
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgInPc2	všecek
bagrů	bagr	k1gInPc2	bagr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
upevněn	upevnit	k5eAaPmNgInS	upevnit
na	na	k7c6	na
rámu	rám	k1gInSc6	rám
stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
třech	tři	k4xCgInPc2	tři
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
spojených	spojený	k2eAgFnPc2d1	spojená
pomocí	pomoc	k1gFnPc2	pomoc
čepů	čep	k1gInPc2	čep
a	a	k8xC	a
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
pomocí	pomoc	k1gFnPc2	pomoc
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
válců	válec	k1gInPc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
podkopu	podkopat	k5eAaPmIp1nS	podkopat
nejblíže	blízce	k6eAd3	blízce
s	s	k7c7	s
rámu	rám	k1gInSc6	rám
(	(	kIx(	(
<g/>
kabině	kabina	k1gFnSc6	kabina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
výložník	výložník	k1gInSc1	výložník
(	(	kIx(	(
<g/>
slang	slang	k1gInSc1	slang
<g/>
.	.	kIx.	.
základní	základní	k2eAgNnSc1d1	základní
rameno	rameno	k1gNnSc1	rameno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výložník	výložník	k1gInSc1	výložník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednodílný	jednodílný	k2eAgMnSc1d1	jednodílný
či	či	k8xC	či
dvoudílný	dvoudílný	k2eAgMnSc1d1	dvoudílný
<g/>
.	.	kIx.	.
</s>
<s>
Jednodílný	jednodílný	k2eAgInSc1d1	jednodílný
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
tvaru	tvar	k1gInSc2	tvar
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
banán	banán	k1gInSc1	banán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
,	,	kIx,	,
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgNnSc1d2	spolehlivější
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Standardně	standardně	k6eAd1	standardně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
minibagrů	minibagr	k1gInPc2	minibagr
<g/>
,	,	kIx,	,
rypadel-nakladačů	rypadelakladač	k1gInPc2	rypadel-nakladač
i	i	k8xC	i
kráčivých	kráčivý	k2eAgInPc2d1	kráčivý
bagrů	bagr	k1gInPc2	bagr
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
výložník	výložník	k1gInSc1	výložník
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
větší	veliký	k2eAgInSc4d2	veliký
rozsah	rozsah	k1gInSc4	rozsah
pohybů	pohyb	k1gInPc2	pohyb
při	při	k7c6	při
hloubení	hloubení	k1gNnSc6	hloubení
v	v	k7c6	v
sevřených	sevřený	k2eAgFnPc6d1	sevřená
jamách	jamách	k?	jamách
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Násada	násada	k1gFnSc1	násada
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
částí	část	k1gFnSc7	část
podkopu	podkopat	k5eAaPmIp1nS	podkopat
<g/>
.	.	kIx.	.
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
spodním	spodní	k2eAgInSc6d1	spodní
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
upevněna	upevněn	k2eAgFnSc1d1	upevněna
lopata	lopata	k1gFnSc1	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Násada	násada	k1gFnSc1	násada
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgFnSc1d1	různá
dle	dle	k7c2	dle
určení	určení	k1gNnSc2	určení
stroje	stroj	k1gInSc2	stroj
<g/>
:	:	kIx,	:
standardní	standardní	k2eAgFnSc7d1	standardní
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
dosahem	dosah	k1gInSc7	dosah
<g/>
,	,	kIx,	,
s	s	k7c7	s
teleskopickým	teleskopický	k2eAgNnSc7d1	teleskopické
výsuvem	výsuvo	k1gNnSc7	výsuvo
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Lopata	lopata	k1gFnSc1	lopata
(	(	kIx(	(
<g/>
slang	slang	k1gInSc1	slang
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
lžíce	lžíce	k1gFnPc4	lžíce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
násady	násada	k1gFnSc2	násada
upevněna	upevnit	k5eAaPmNgFnS	upevnit
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
tzv.	tzv.	kA	tzv.
rychloupínáku	rychloupínák	k1gInSc6	rychloupínák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kategorií	kategorie	k1gFnSc7	kategorie
lžíce	lžíce	k1gFnSc2	lžíce
je	být	k5eAaImIp3nS	být
svahovací	svahovací	k2eAgFnSc1d1	svahovací
lžíce	lžíce	k1gFnSc1	lžíce
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
svahovačka	svahovačka	k1gFnSc1	svahovačka
–	–	k?	–
buď	buď	k8xC	buď
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgNnPc1d1	zvané
pravítko	pravítko	k1gNnSc4	pravítko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hydraulicky	hydraulicky	k6eAd1	hydraulicky
naklápěná	naklápěný	k2eAgFnSc1d1	naklápěný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychloupínák	Rychloupínák	k1gMnSc1	Rychloupínák
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
výměnu	výměna	k1gFnSc4	výměna
lopat	lopata	k1gFnPc2	lopata
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
pracovních	pracovní	k2eAgInPc2d1	pracovní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovládán	ovládat	k5eAaImNgInS	ovládat
buď	buď	k8xC	buď
mechanicky	mechanicky	k6eAd1	mechanicky
nebo	nebo	k8xC	nebo
hydraulicky	hydraulicky	k6eAd1	hydraulicky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trhu	trh	k1gInSc6	trh
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgMnPc2d1	různý
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
velikostních	velikostní	k2eAgFnPc2d1	velikostní
řad	řada	k1gFnPc2	řada
rychloupínačů	rychloupínač	k1gInPc2	rychloupínač
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
složité	složitý	k2eAgNnSc1d1	složité
používat	používat	k5eAaImF	používat
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc4d1	velká
lopaty	lopata	k1gFnPc4	lopata
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rotační	rotační	k2eAgFnPc1d1	rotační
a	a	k8xC	a
naklápěcí	naklápěcí	k2eAgFnPc1d1	naklápěcí
hlavy	hlava	k1gFnPc1	hlava
umožní	umožnit	k5eAaPmIp3nP	umožnit
u	u	k7c2	u
vyspělejších	vyspělý	k2eAgInPc2d2	vyspělejší
typů	typ	k1gInPc2	typ
strojů	stroj	k1gInPc2	stroj
používat	používat	k5eAaImF	používat
i	i	k9	i
toto	tento	k3xDgNnSc4	tento
příslušenství	příslušenství	k1gNnSc4	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
přídavných	přídavný	k2eAgInPc2d1	přídavný
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
okruhů	okruh	k1gInPc2	okruh
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ovládat	ovládat	k5eAaImF	ovládat
tyto	tento	k3xDgInPc1	tento
vložené	vložený	k2eAgInPc1d1	vložený
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c6	na
násadě	násada	k1gFnSc6	násada
bagru	bagr	k1gInSc2	bagr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zařízení	zařízení	k1gNnSc4	zařízení
rozšiřující	rozšiřující	k2eAgFnSc2d1	rozšiřující
vlastnosti	vlastnost	k1gFnSc2	vlastnost
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
nahoře	nahoře	k6eAd1	nahoře
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
na	na	k7c4	na
násadu	násada	k1gFnSc4	násada
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
rychloupínačem	rychloupínač	k1gInSc7	rychloupínač
<g/>
.	.	kIx.	.
</s>
<s>
Naklápěcí	naklápěcí	k2eAgFnSc1d1	naklápěcí
hlava	hlava	k1gFnSc1	hlava
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
náklon	náklon	k1gInSc4	náklon
všech	všecek	k3xTgFnPc2	všecek
upnutých	upnutý	k2eAgFnPc2d1	upnutá
lopat	lopata	k1gFnPc2	lopata
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
+	+	kIx~	+
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
st.	st.	kA	st.
kolem	kolem	k6eAd1	kolem
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
osy	osa	k1gFnPc1	osa
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
čepům	čep	k1gInPc3	čep
lopaty	lopata	k1gFnSc2	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Rotátory	rotátor	k1gInPc1	rotátor
umožní	umožnit	k5eAaPmIp3nP	umožnit
točení	točení	k1gNnSc4	točení
lopaty	lopata	k1gFnSc2	lopata
o	o	k7c4	o
360	[number]	k4	360
<g/>
°	°	k?	°
kolem	kolem	k7c2	kolem
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
kopat	kopat	k5eAaImF	kopat
lopatou	lopata	k1gFnSc7	lopata
i	i	k8xC	i
směrem	směr	k1gInSc7	směr
"	"	kIx"	"
<g/>
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyspělejší	vyspělý	k2eAgNnSc1d3	nejvyspělejší
zařízení	zařízení	k1gNnSc1	zařízení
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
naklápěcí	naklápěcí	k2eAgFnSc2d1	naklápěcí
hlavy	hlava	k1gFnSc2	hlava
i	i	k8xC	i
rotátoru	rotátor	k1gInSc2	rotátor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
už	už	k9	už
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
a	a	k8xC	a
nehodí	hodit	k5eNaPmIp3nS	hodit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
minibagrech	minibagr	k1gInPc6	minibagr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
pracovní	pracovní	k2eAgInPc1d1	pracovní
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
Mimo	mimo	k7c4	mimo
lopaty	lopata	k1gFnPc4	lopata
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
bagr	bagr	k1gInSc4	bagr
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
nosič	nosič	k1gInSc4	nosič
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
pracovních	pracovní	k2eAgInPc2d1	pracovní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hydraulické	hydraulický	k2eAgNnSc4d1	hydraulické
kladivo	kladivo	k1gNnSc4	kladivo
<g/>
,	,	kIx,	,
demoliční	demoliční	k2eAgFnPc4d1	demoliční
kleště	kleště	k1gFnPc4	kleště
<g/>
,	,	kIx,	,
drtící	drtící	k2eAgFnSc4d1	drtící
lopatu	lopata	k1gFnSc4	lopata
<g/>
,	,	kIx,	,
třídící	třídící	k2eAgFnSc4d1	třídící
lopatu	lopata	k1gFnSc4	lopata
<g/>
,	,	kIx,	,
nesenou	nesený	k2eAgFnSc4d1	nesená
vibrační	vibrační	k2eAgFnSc4d1	vibrační
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
vrták	vrták	k1gInSc1	vrták
<g/>
,	,	kIx,	,
drapák	drapák	k1gInSc1	drapák
<g/>
,	,	kIx,	,
vidle	vidle	k1gFnSc1	vidle
<g/>
,	,	kIx,	,
jeřábový	jeřábový	k2eAgInSc1d1	jeřábový
nástavec	nástavec	k1gInSc1	nástavec
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Podkop	podkop	k1gInSc1	podkop
je	být	k5eAaImIp3nS	být
upevněn	upevnit	k5eAaPmNgInS	upevnit
na	na	k7c6	na
rámu	rám	k1gInSc6	rám
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
kabina	kabina	k1gFnSc1	kabina
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
motor	motor	k1gInSc1	motor
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgInSc7d1	hydraulický
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mini	mini	k2eAgInPc2d1	mini
bagrů	bagr	k1gInPc2	bagr
<g/>
,	,	kIx,	,
traktorbagrů	traktorbagr	k1gInPc2	traktorbagr
a	a	k8xC	a
menších	malý	k2eAgInPc2d2	menší
kolových	kolový	k2eAgInPc2d1	kolový
bagrů	bagr	k1gInPc2	bagr
je	být	k5eAaImIp3nS	být
podkop	podkop	k1gInSc4	podkop
možno	možno	k6eAd1	možno
natáčet	natáčet	k5eAaImF	natáčet
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obratnost	obratnost	k1gFnSc1	obratnost
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezikus	Mezikus	k1gMnSc1	Mezikus
umožňující	umožňující	k2eAgNnSc4d1	umožňující
natáčení	natáčení	k1gNnSc4	natáčení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
nasazené	nasazený	k2eAgNnSc1d1	nasazené
rameno	rameno	k1gNnSc1	rameno
se	se	k3xPyFc4	se
slengově	slengově	k6eAd1	slengově
označuje	označovat	k5eAaImIp3nS	označovat
Kozlík	kozlík	k1gInSc1	kozlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rám	rám	k1gInSc1	rám
stroje	stroj	k1gInSc2	stroj
s	s	k7c7	s
kabinou	kabina	k1gFnSc7	kabina
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
základního	základní	k2eAgNnSc2d1	základní
provedení	provedení	k1gNnSc2	provedení
bagru	bagr	k1gInSc2	bagr
montován	montovat	k5eAaImNgInS	montovat
na	na	k7c4	na
podvozek	podvozek	k1gInSc4	podvozek
(	(	kIx(	(
<g/>
pásový	pásový	k2eAgMnSc1d1	pásový
či	či	k8xC	či
kolový	kolový	k2eAgMnSc1d1	kolový
<g/>
)	)	kIx)	)
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
točny	točna	k1gFnSc2	točna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
otáčení	otáčení	k1gNnSc4	otáčení
celé	celý	k2eAgFnSc2d1	celá
nástavby	nástavba	k1gFnSc2	nástavba
o	o	k7c4	o
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolový	kolový	k2eAgInSc1d1	kolový
podvozek	podvozek	k1gInSc1	podvozek
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
stability	stabilita	k1gFnSc2	stabilita
doplněn	doplnit	k5eAaPmNgMnS	doplnit
radlicí	radlice	k1gFnSc7	radlice
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
a	a	k8xC	a
sklopnými	sklopný	k2eAgFnPc7d1	sklopná
opěrami	opěra	k1gFnPc7	opěra
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Radlice	radlice	k1gFnSc1	radlice
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
nouzové	nouzový	k2eAgNnSc4d1	nouzové
dozerové	dozerový	k2eAgNnSc4d1	dozerový
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
zahrnutí	zahrnutí	k1gNnSc4	zahrnutí
výkopů	výkop	k1gInPc2	výkop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pásový	pásový	k2eAgInSc1d1	pásový
podvozek	podvozek	k1gInSc1	podvozek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
pásových	pásový	k2eAgFnPc2d1	pásová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
pak	pak	k6eAd1	pak
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
ocelový	ocelový	k2eAgMnSc1d1	ocelový
nebo	nebo	k8xC	nebo
gumový	gumový	k2eAgMnSc1d1	gumový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnací	hnací	k2eAgNnSc4d1	hnací
(	(	kIx(	(
<g/>
turasové	turasový	k2eAgNnSc4d1	turasový
<g/>
)	)	kIx)	)
kolo	kolo	k1gNnSc4	kolo
s	s	k7c7	s
hydromotorem	hydromotor	k1gMnSc7	hydromotor
<g/>
,	,	kIx,	,
volné	volný	k2eAgNnSc1d1	volné
napínací	napínací	k2eAgNnSc1d1	napínací
kolo	kolo	k1gNnSc1	kolo
s	s	k7c7	s
napínacím	napínací	k2eAgNnSc7d1	napínací
zařízením	zařízení	k1gNnSc7	zařízení
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
rolny	rolna	k1gFnPc1	rolna
<g/>
.	.	kIx.	.
</s>
<s>
Pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
s	s	k7c7	s
řetězu	řetěz	k1gInSc3	řetěz
(	(	kIx(	(
<g/>
články	článek	k1gInPc1	článek
řetězu	řetěz	k1gInSc2	řetěz
jsou	být	k5eAaImIp3nP	být
pospojované	pospojovaný	k2eAgInPc1d1	pospojovaný
pomocí	pomocí	k7c2	pomocí
čepů	čep	k1gInPc2	čep
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
mazaném	mazaný	k2eAgNnSc6d1	mazané
či	či	k8xC	či
nemazaném	mazaný	k2eNgMnSc6d1	mazaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
lišt	lišta	k1gFnPc2	lišta
(	(	kIx(	(
<g/>
desek	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lišty	lišta	k1gFnPc4	lišta
dále	daleko	k6eAd2	daleko
rozeznávám	rozeznávat	k5eAaImIp1nS	rozeznávat
jednonožové	jednonožový	k2eAgNnSc1d1	jednonožový
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc4d1	velký
záběr	záběr	k1gInSc4	záběr
<g/>
,	,	kIx,	,
špatné	špatný	k2eAgFnPc4d1	špatná
manévrovací	manévrovací	k2eAgFnPc4d1	manévrovací
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc4d1	typický
pro	pro	k7c4	pro
buldozery	buldozer	k1gInPc4	buldozer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvounožové	dvounožový	k2eAgFnPc1d1	dvounožový
či	či	k8xC	či
třínožové	třínožový	k2eAgFnPc1d1	třínožová
(	(	kIx(	(
<g/>
lepší	dobrý	k2eAgFnPc1d2	lepší
manévrovací	manévrovací	k2eAgFnPc1d1	manévrovací
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
,	,	kIx,	,
horší	zlý	k2eAgInSc1d2	horší
záběr	záběr	k1gInSc1	záběr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bagrů	bagr	k1gInPc2	bagr
jsou	být	k5eAaImIp3nP	být
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
třínožové	třínožový	k2eAgFnPc1d1	třínožová
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rypadlo-nakladač	Rypadloakladač	k1gInSc4	Rypadlo-nakladač
==	==	k?	==
</s>
</p>
<p>
<s>
Rypadlo-nakladač	Rypadloakladač	k1gInSc1	Rypadlo-nakladač
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
traktorbagr	traktorbagr	k1gInSc1	traktorbagr
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
backhoe-loader	backhoeoader	k1gInSc1	backhoe-loader
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
podskupiny	podskupina	k1gFnSc2	podskupina
bagrů	bagr	k1gInPc2	bagr
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zvláště	zvláště	k6eAd1	zvláště
anglická	anglický	k2eAgFnSc1d1	anglická
firma	firma	k1gFnSc1	firma
JCB	JCB	kA	JCB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rypadla-nakladače	Rypadlaakladač	k1gInPc1	Rypadla-nakladač
jsou	být	k5eAaImIp3nP	být
univerzální	univerzální	k2eAgInPc1d1	univerzální
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
na	na	k7c6	na
traktorovém	traktorový	k2eAgInSc6d1	traktorový
podvozku	podvozek	k1gInSc6	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
nakládací	nakládací	k2eAgFnSc1d1	nakládací
lopata	lopata	k1gFnSc1	lopata
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
podkopové	podkopový	k2eAgNnSc4d1	podkopové
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
bagr	bagr	k1gInSc1	bagr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
nakládací	nakládací	k2eAgFnSc1d1	nakládací
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
nakládání	nakládání	k1gNnSc4	nakládání
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc1d1	přední
lopata	lopata	k1gFnSc1	lopata
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
tzv.	tzv.	kA	tzv.
nakládací	nakládací	k2eAgFnPc1d1	nakládací
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
univerzální	univerzální	k2eAgMnSc1d1	univerzální
–	–	k?	–
tzv.	tzv.	kA	tzv.
<g/>
"	"	kIx"	"
<g/>
klapačka	klapačka	k1gFnSc1	klapačka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
moravsky	moravsky	k6eAd1	moravsky
"	"	kIx"	"
<g/>
žralok	žralok	k1gMnSc1	žralok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
6	[number]	k4	6
<g/>
in	in	k?	in
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
lopata	lopata	k1gFnSc1	lopata
je	být	k5eAaImIp3nS	být
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nakládat	nakládat	k5eAaImF	nakládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
i	i	k9	i
jako	jako	k9	jako
dozerovou	dozerový	k2eAgFnSc4d1	dozerový
radlici	radlice	k1gFnSc4	radlice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vybavena	vybavit	k5eAaPmNgFnS	vybavit
i	i	k9	i
paletovacími	paletovací	k2eAgFnPc7d1	paletovací
vidlemi	vidle	k1gFnPc7	vidle
pro	pro	k7c4	pro
manipulaci	manipulace	k1gFnSc4	manipulace
např.	např.	kA	např.
s	s	k7c7	s
paletami	paleta	k1gFnPc7	paleta
cihel	cihla	k1gFnPc2	cihla
na	na	k7c6	na
staveništi	staveniště	k1gNnSc6	staveniště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zadní	zadní	k2eAgNnSc1d1	zadní
podkopové	podkopový	k2eAgNnSc1d1	podkopové
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
konstruováno	konstruován	k2eAgNnSc1d1	konstruováno
jako	jako	k8xS	jako
přesuvné	přesuvný	k2eAgNnSc1d1	přesuvné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
dílů	díl	k1gInPc2	díl
–	–	k?	–
závěsná	závěsný	k2eAgFnSc1d1	závěsná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
otoč	otoč	k1gInSc1	otoč
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
kingpost	kingpost	k1gFnSc1	kingpost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
výložník	výložník	k1gInSc4	výložník
(	(	kIx(	(
<g/>
základní	základní	k2eAgNnSc4d1	základní
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
boom	boom	k1gInSc4	boom
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
násada	násada	k1gFnSc1	násada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
dipper	dipper	k1gInSc1	dipper
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
vybavená	vybavený	k2eAgNnPc4d1	vybavené
teleskopem	teleskop	k1gInSc7	teleskop
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
extradig	extradig	k1gInSc1	extradig
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pracovní	pracovní	k2eAgNnSc1d1	pracovní
zařízení	zařízení	k1gNnSc1	zařízení
slouží	sloužit	k5eAaImIp3nS	sloužit
lžíce	lžíce	k1gFnPc4	lžíce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
bucket	bucket	k5eAaBmF	bucket
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
upevněná	upevněný	k2eAgFnSc1d1	upevněná
na	na	k7c6	na
rychloupínacím	rychloupínací	k2eAgNnSc6d1	rychloupínací
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
středové	středový	k2eAgNnSc1d1	středové
pevné	pevný	k2eAgNnSc1d1	pevné
uložení	uložení	k1gNnSc1	uložení
podkopu	podkop	k1gInSc2	podkop
<g/>
,	,	kIx,	,
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
s	s	k7c7	s
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
sklopnými	sklopný	k2eAgFnPc7d1	sklopná
opěrami	opěra	k1gFnPc7	opěra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
pracovní	pracovní	k2eAgFnSc6d1	pracovní
poloze	poloha	k1gFnSc6	poloha
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
stroj	stroj	k1gInSc4	stroj
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzálnost	univerzálnost	k1gFnSc1	univerzálnost
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
je	být	k5eAaImIp3nS	být
však	však	k9	však
vykoupena	vykoupit	k5eAaPmNgFnS	vykoupit
nižším	nízký	k2eAgInSc7d2	nižší
výkonem	výkon	k1gInSc7	výkon
a	a	k8xC	a
menší	malý	k2eAgFnSc7d2	menší
obratností	obratnost	k1gFnSc7	obratnost
než	než	k8xS	než
u	u	k7c2	u
klasických	klasický	k2eAgInPc2d1	klasický
bagrů	bagr	k1gInPc2	bagr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
nahrazovány	nahrazovat	k5eAaImNgFnP	nahrazovat
jinou	jiný	k2eAgFnSc7d1	jiná
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Minibagr	Minibagr	k1gInSc4	Minibagr
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
získala	získat	k5eAaPmAgFnS	získat
značnou	značný	k2eAgFnSc4d1	značná
oblibu	obliba	k1gFnSc4	obliba
kategorie	kategorie	k1gFnSc2	kategorie
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
bagrů	bagr	k1gInPc2	bagr
na	na	k7c6	na
pásovém	pásový	k2eAgInSc6d1	pásový
podvozku	podvozek	k1gInSc6	podvozek
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
gumovými	gumový	k2eAgInPc7d1	gumový
pásy	pás	k1gInPc7	pás
<g/>
)	)	kIx)	)
do	do	k7c2	do
9	[number]	k4	9
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
obecně	obecně	k6eAd1	obecně
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
minibagr	minibagr	k1gInSc1	minibagr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
velmi	velmi	k6eAd1	velmi
výkonné	výkonný	k2eAgInPc1d1	výkonný
stroje	stroj	k1gInPc1	stroj
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
hloubení	hloubení	k1gNnSc4	hloubení
výkopů	výkop	k1gInPc2	výkop
<g/>
,	,	kIx,	,
základů	základ	k1gInPc2	základ
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
přípravu	příprava	k1gFnSc4	příprava
staveb	stavba	k1gFnPc2	stavba
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Střední	střední	k2eAgFnSc2d1	střední
kategorie	kategorie	k1gFnSc2	kategorie
bagrů	bagr	k1gInPc2	bagr
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
malých	malý	k2eAgMnPc2d1	malý
bratrů	bratr	k1gMnPc2	bratr
nemá	mít	k5eNaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
žádné	žádný	k3yNgNnSc4	žádný
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgFnSc4d3	nejrozšířenější
skupinu	skupina	k1gFnSc4	skupina
bagrů	bagr	k1gInPc2	bagr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
a	a	k8xC	a
demoličních	demoliční	k2eAgFnPc6d1	demoliční
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
kategorii	kategorie	k1gFnSc6	kategorie
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
2	[number]	k4	2
hlavních	hlavní	k2eAgFnPc6d1	hlavní
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pásech	pás	k1gInPc6	pás
a	a	k8xC	a
na	na	k7c6	na
kolovém	kolový	k2eAgInSc6d1	kolový
podvozku	podvozek	k1gInSc6	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kolovém	kolový	k2eAgInSc6d1	kolový
podvozku	podvozek	k1gInSc6	podvozek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hmotností	hmotnost	k1gFnSc7	hmotnost
22	[number]	k4	22
<g/>
t	t	k?	t
<g/>
,	,	kIx,	,
Kolové	kolový	k2eAgInPc4d1	kolový
bagry	bagr	k1gInPc4	bagr
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
kromě	kromě	k7c2	kromě
silnice	silnice	k1gFnSc2	silnice
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
těmto	tento	k3xDgFnPc3	tento
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
dvoucestné	dvoucestný	k2eAgNnSc1d1	dvoucestné
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
a	a	k8xC	a
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velké	velký	k2eAgInPc1d1	velký
bagry	bagr	k1gInPc1	bagr
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
bagry	bagr	k1gInPc1	bagr
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
povrchových	povrchový	k2eAgInPc6d1	povrchový
lomech	lom	k1gInPc6	lom
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
bagry	bagr	k1gInPc4	bagr
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
800	[number]	k4	800
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
najednou	najednou	k6eAd1	najednou
uzvednout	uzvednout	k5eAaPmF	uzvednout
v	v	k7c6	v
lopatě	lopata	k1gFnSc6	lopata
materiál	materiál	k1gInSc4	materiál
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
70	[number]	k4	70
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
technologii	technologie	k1gFnSc4	technologie
"	"	kIx"	"
<g/>
spodní	spodní	k2eAgFnPc1d1	spodní
lopaty	lopata	k1gFnPc1	lopata
<g/>
"	"	kIx"	"
tedy	tedy	k9	tedy
nabírání	nabírání	k1gNnSc2	nabírání
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
nahoru	nahoru	k6eAd1	nahoru
po	po	k7c6	po
stěně	stěna	k1gFnSc6	stěna
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Vyprázdnění	vyprázdnění	k1gNnSc1	vyprázdnění
lopaty	lopata	k1gFnSc2	lopata
pak	pak	k6eAd1	pak
probíhá	probíhat	k5eAaImIp3nS	probíhat
otevřením	otevření	k1gNnSc7	otevření
dna	dno	k1gNnSc2	dno
lopaty	lopata	k1gFnSc2	lopata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
velké	velký	k2eAgInPc1d1	velký
demoliční	demoliční	k2eAgInPc1d1	demoliční
bagry	bagr	k1gInPc1	bagr
s	s	k7c7	s
výškovým	výškový	k2eAgInSc7d1	výškový
dosahem	dosah	k1gInSc7	dosah
přes	přes	k7c4	přes
70	[number]	k4	70
m.	m.	k?	m.
Velké	velký	k2eAgInPc1d1	velký
bagry	bagr	k1gInPc1	bagr
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
natolik	natolik	k6eAd1	natolik
těžké	těžký	k2eAgNnSc1d1	těžké
a	a	k8xC	a
rozměrné	rozměrný	k2eAgNnSc1d1	rozměrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
přeprava	přeprava	k1gFnSc1	přeprava
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
určení	určení	k1gNnSc2	určení
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
po	po	k7c6	po
demontovaných	demontovaný	k2eAgFnPc6d1	demontovaná
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
bagrů	bagr	k1gInPc2	bagr
na	na	k7c4	na
zemní	zemní	k2eAgFnPc4d1	zemní
práce	práce	k1gFnPc4	práce
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
bagrů	bagr	k1gInPc2	bagr
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgFnPc4d1	průmyslová
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
překládku	překládka	k1gFnSc4	překládka
kovového	kovový	k2eAgInSc2d1	kovový
šrotu	šrot	k1gInSc2	šrot
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Jedničkou	jednička	k1gFnSc7	jednička
mezi	mezi	k7c7	mezi
výrobci	výrobce	k1gMnPc7	výrobce
stavebních	stavební	k2eAgInPc2d1	stavební
strojů	stroj	k1gInPc2	stroj
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Caterpillar	Caterpillar	k1gInSc1	Caterpillar
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc1	její
celkový	celkový	k2eAgInSc1d1	celkový
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
výrobě	výroba	k1gFnSc6	výroba
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
velkých	velký	k2eAgInPc2d1	velký
rypadle	rypadlo	k1gNnSc6	rypadlo
je	být	k5eAaImIp3nS	být
Komatsu	Komatsa	k1gFnSc4	Komatsa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
významné	významný	k2eAgMnPc4d1	významný
výrobce	výrobce	k1gMnPc4	výrobce
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
společnost	společnost	k1gFnSc1	společnost
Liebherr	Liebherr	k1gInSc1	Liebherr
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgInPc7	svůj
velkými	velký	k2eAgInPc7d1	velký
lomovými	lomový	k2eAgInPc7d1	lomový
bagry	bagr	k1gInPc7	bagr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
minibagrů	minibagr	k1gInPc2	minibagr
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
špičky	špička	k1gFnPc4	špička
TAKEUCHI	TAKEUCHI	kA	TAKEUCHI
<g/>
,	,	kIx,	,
KUBOTA	KUBOTA	kA	KUBOTA
<g/>
,	,	kIx,	,
YANMAR	YANMAR	kA	YANMAR
<g/>
,	,	kIx,	,
BOBCAT	BOBCAT	kA	BOBCAT
či	či	k8xC	či
NEUSON	NEUSON	kA	NEUSON
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
rypadel-nakladačů	rypadelakladač	k1gInPc2	rypadel-nakladač
je	být	k5eAaImIp3nS	být
pojmem	pojem	k1gInSc7	pojem
společnost	společnost	k1gFnSc1	společnost
JCB	JCB	kA	JCB
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
Joseph	Joseph	k1gMnSc1	Joseph
Cyril	Cyril	k1gMnSc1	Cyril
Bamford	Bamford	k1gMnSc1	Bamford
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
na	na	k7c6	na
otce	otka	k1gFnSc6	otka
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
bagrů	bagr	k1gInPc2	bagr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2105	[number]	k4	2105
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
možnost	možnost	k1gFnSc4	možnost
Hybridních	hybridní	k2eAgInPc2d1	hybridní
pohonů	pohon	k1gInPc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
společnost	společnost	k1gFnSc1	společnost
Caterpillar	Caterpillara	k1gFnPc2	Caterpillara
dodává	dodávat	k5eAaImIp3nS	dodávat
stroje	stroj	k1gInSc2	stroj
které	který	k3yQgFnPc4	který
využívají	využívat	k5eAaPmIp3nP	využívat
přebytečné	přebytečný	k2eAgFnPc4d1	přebytečná
energie	energie	k1gFnPc4	energie
hydraulického	hydraulický	k2eAgInSc2d1	hydraulický
oleje	olej	k1gInSc2	olej
která	který	k3yRgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
otáčení	otáčení	k1gNnSc2	otáčení
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
akumulována	akumulovat	k5eAaBmNgFnS	akumulovat
v	v	k7c6	v
zásobníku	zásobník	k1gInSc6	zásobník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
otáčení	otáčení	k1gNnSc6	otáčení
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
opravdový	opravdový	k2eAgInSc1d1	opravdový
Hybridní	hybridní	k2eAgInSc1d1	hybridní
stroj	stroj	k1gInSc1	stroj
dodává	dodávat	k5eAaImIp3nS	dodávat
společnost	společnost	k1gFnSc4	společnost
Komastu	Komast	k1gInSc2	Komast
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
dieselové	dieselový	k2eAgFnSc2d1	dieselová
a	a	k8xC	a
elektrického	elektrický	k2eAgInSc2d1	elektrický
pohonu	pohon	k1gInSc2	pohon
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
konstrukci	konstrukce	k1gFnSc4	konstrukce
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
otáčení	otáčení	k1gNnSc4	otáčení
kabiny	kabina	k1gFnSc2	kabina
s	s	k7c7	s
ramenem	rameno	k1gNnSc7	rameno
použit	použit	k2eAgInSc1d1	použit
nízko-	nízko-	k?	nízko-
otáčkový	otáčkový	k2eAgInSc4d1	otáčkový
elektromotor	elektromotor	k1gInSc4	elektromotor
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
kroutícím	kroutící	k2eAgInSc7d1	kroutící
momentem	moment	k1gInSc7	moment
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
motor	motor	k1gInSc1	motor
se	se	k3xPyFc4	se
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
otáčení	otáčení	k1gNnSc2	otáčení
kabiny	kabina	k1gFnSc2	kabina
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
generátor	generátor	k1gInSc4	generátor
a	a	k8xC	a
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
energii	energie	k1gFnSc4	energie
pak	pak	k6eAd1	pak
využije	využít	k5eAaPmIp3nS	využít
při	při	k7c6	při
novém	nový	k2eAgNnSc6d1	nové
otáčení	otáčení	k1gNnSc6	otáčení
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
opatření	opatření	k1gNnPc1	opatření
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
značným	značný	k2eAgFnPc3d1	značná
úsporám	úspora	k1gFnPc3	úspora
spotřeby	spotřeba	k1gFnSc2	spotřeba
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vyvážit	vyvážit	k5eAaPmF	vyvážit
vyšší	vysoký	k2eAgInPc4d2	vyšší
pořizovací	pořizovací	k2eAgInPc4d1	pořizovací
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
přesné	přesný	k2eAgFnSc2d1	přesná
práce	práce	k1gFnSc2	práce
při	při	k7c6	při
dokončovacích	dokončovací	k2eAgFnPc6d1	dokončovací
pracích	práce	k1gFnPc6	práce
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
svahovací	svahovací	k2eAgFnSc7d1	svahovací
lopatou	lopata	k1gFnSc7	lopata
<g/>
)	)	kIx)	)
přivedl	přivést	k5eAaPmAgMnS	přivést
i	i	k9	i
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
řízení	řízení	k1gNnSc2	řízení
strojů	stroj	k1gInPc2	stroj
pomocí	pomocí	k7c2	pomocí
laserových	laserový	k2eAgNnPc2d1	laserové
nivelačních	nivelační	k2eAgNnPc2d1	nivelační
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
poloha	poloha	k1gFnSc1	poloha
lopaty	lopata	k1gFnSc2	lopata
je	být	k5eAaImIp3nS	být
porovnávána	porovnáván	k2eAgFnSc1d1	porovnávána
s	s	k7c7	s
informací	informace	k1gFnSc7	informace
z	z	k7c2	z
nivelačního	nivelační	k2eAgInSc2d1	nivelační
laseru	laser	k1gInSc2	laser
a	a	k8xC	a
strojník	strojník	k1gMnSc1	strojník
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
provádět	provádět	k5eAaImF	provádět
dokončovací	dokončovací	k2eAgFnPc4d1	dokončovací
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
trendem	trend	k1gInSc7	trend
jsou	být	k5eAaImIp3nP	být
inteligentní	inteligentní	k2eAgInPc1d1	inteligentní
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomocí	pomocí	k7c2	pomocí
GPS	GPS	kA	GPS
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
možno	možno	k6eAd1	možno
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
3D	[number]	k4	3D
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dokončení	dokončení	k1gNnSc2	dokončení
i	i	k8xC	i
lomených	lomený	k2eAgInPc2d1	lomený
a	a	k8xC	a
zaoblených	zaoblený	k2eAgInPc2d1	zaoblený
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
silničních	silniční	k2eAgInPc2d1	silniční
náspů	násep	k1gInPc2	násep
nebo	nebo	k8xC	nebo
složitých	složitý	k2eAgInPc2d1	složitý
tvarů	tvar	k1gInPc2	tvar
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
nahrán	nahrát	k5eAaBmNgInS	nahrát
3D	[number]	k4	3D
model	model	k1gInSc4	model
plochy	plocha	k1gFnSc2	plocha
kterou	který	k3yQgFnSc4	který
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
strojem	stroj	k1gInSc7	stroj
zpracovat	zpracovat	k5eAaPmF	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
inovátorem	inovátor	k1gMnSc7	inovátor
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
v	v	k7c4	v
je	on	k3xPp3gMnPc4	on
výrobce	výrobce	k1gMnSc1	výrobce
Komastu	Komast	k1gMnSc3	Komast
využívající	využívající	k2eAgFnSc2d1	využívající
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Topcon	Topcona	k1gFnPc2	Topcona
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Důležití	důležitý	k2eAgMnPc1d1	důležitý
výrobci	výrobce	k1gMnPc1	výrobce
==	==	k?	==
</s>
</p>
<p>
<s>
Bobcat	Bobcat	k5eAaPmF	Bobcat
Company	Compana	k1gFnPc4	Compana
</s>
</p>
<p>
<s>
Case	Case	k1gFnSc1	Case
CE	CE	kA	CE
</s>
</p>
<p>
<s>
Caterpillar	Caterpillar	k1gMnSc1	Caterpillar
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doosan	Doosan	k1gMnSc1	Doosan
Infracore	Infracor	k1gInSc5	Infracor
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Daewoo	Daewoo	k1gMnSc1	Daewoo
Heavy	Heava	k1gFnSc2	Heava
Industries	Industries	k1gMnSc1	Industries
&	&	k?	&
Machinery	Machiner	k1gMnPc7	Machiner
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hidromek	Hidromek	k1gMnSc1	Hidromek
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Sti	Sti	k?	Sti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hitachi	Hitachi	k6eAd1	Hitachi
Construction	Construction	k1gInSc1	Construction
Machinery	Machiner	k1gMnPc4	Machiner
</s>
</p>
<p>
<s>
Hydrema	Hydrema	k1gFnSc1	Hydrema
</s>
</p>
<p>
<s>
Hyundai	Hyundai	k1gNnSc1	Hyundai
Heavy	Heava	k1gFnSc2	Heava
Industries	Industriesa	k1gFnPc2	Industriesa
</s>
</p>
<p>
<s>
JCB	JCB	kA	JCB
</s>
</p>
<p>
<s>
Jonh	Jonh	k1gMnSc1	Jonh
Deere	Deer	k1gInSc5	Deer
</s>
</p>
<p>
<s>
Kobelco	Kobelco	k6eAd1	Kobelco
</s>
</p>
<p>
<s>
Komatsu	Komatsa	k1gFnSc4	Komatsa
</s>
</p>
<p>
<s>
Kubota	Kubota	k1gFnSc1	Kubota
</s>
</p>
<p>
<s>
lbxco	lbxco	k6eAd1	lbxco
</s>
</p>
<p>
<s>
Liebherr	Liebherr	k1gMnSc1	Liebherr
</s>
</p>
<p>
<s>
TAKEUCHI	TAKEUCHI	kA	TAKEUCHI
</s>
</p>
<p>
<s>
The	The	k?	The
Manitowoc	Manitowoc	k1gFnSc1	Manitowoc
Company	Compana	k1gFnSc2	Compana
</s>
</p>
<p>
<s>
Mitsubishi	mitsubishi	k1gNnSc1	mitsubishi
Heavy	Heava	k1gFnSc2	Heava
Industries	Industriesa	k1gFnPc2	Industriesa
</s>
</p>
<p>
<s>
Mustang	mustang	k1gMnSc1	mustang
Manufacturing	Manufacturing	k1gInSc4	Manufacturing
</s>
</p>
<p>
<s>
New	New	k?	New
Holland	Holland	k1gInSc1	Holland
Construction	Construction	k1gInSc1	Construction
</s>
</p>
<p>
<s>
NEUMEIER	NEUMEIER	kA	NEUMEIER
</s>
</p>
<p>
<s>
O	o	k7c6	o
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
</s>
</p>
<p>
<s>
Poclain	Poclain	k1gMnSc1	Poclain
</s>
</p>
<p>
<s>
Terex	Terex	k1gInSc1	Terex
Corporation	Corporation	k1gInSc1	Corporation
</s>
</p>
<p>
<s>
Volvo	Volvo	k1gNnSc1	Volvo
Construction	Construction	k1gInSc1	Construction
Equipment	Equipment	k1gMnSc1	Equipment
</s>
</p>
<p>
<s>
Yanmar	Yanmar	k1gInSc1	Yanmar
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Ammann	Ammann	k1gInSc1	Ammann
–	–	k?	–
Yanmar	Yanmar	k1gInSc1	Yanmar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZTS	ZTS	kA	ZTS
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Buldozer	buldozer	k1gInSc1	buldozer
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bagr	bagr	k1gInSc1	bagr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bagr	bagr	k1gInSc1	bagr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
