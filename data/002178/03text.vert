<s>
Columbus	Columbus	k1gInSc1	Columbus
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Ohia	Ohio	k1gNnSc2	Ohio
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Scioto	Sciota	k1gFnSc5	Sciota
a	a	k8xC	a
Olentangy	Olentang	k1gInPc7	Olentang
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Ohia	Ohio	k1gNnSc2	Ohio
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
měl	mít	k5eAaImAgInS	mít
Columbus	Columbus	k1gInSc1	Columbus
711	[number]	k4	711
470	[number]	k4	470
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
730	[number]	k4	730
657	[number]	k4	657
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
oblastí	oblast	k1gFnSc7	oblast
na	na	k7c4	na
1	[number]	k4	1
708	[number]	k4	708
625	[number]	k4	625
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
787	[number]	k4	787
033	[number]	k4	033
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
61,5	[number]	k4	61,5
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
28,0	[number]	k4	28,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
4,1	[number]	k4	4,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,9	[number]	k4	2,9
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,6	[number]	k4	5,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Columbus	Columbus	k1gInSc1	Columbus
Blue	Blu	k1gFnSc2	Blu
Jackets	Jacketsa	k1gFnPc2	Jacketsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
Columbus	Columbus	k1gInSc4	Columbus
pochází	pocházet	k5eAaImIp3nS	pocházet
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
Twenty	Twenta	k1gFnSc2	Twenta
One	One	k1gFnSc2	One
Pilots	Pilotsa	k1gFnPc2	Pilotsa
<g/>
.	.	kIx.	.
</s>
<s>
Prescott	Prescott	k1gMnSc1	Prescott
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
-	-	kIx~	-
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
a	a	k8xC	a
bankéř	bankéř	k1gMnSc1	bankéř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
syn	syn	k1gMnSc1	syn
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
a	a	k8xC	a
vnuk	vnuk	k1gMnSc1	vnuk
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
byli	být	k5eAaImAgMnP	být
prezidenty	prezident	k1gMnPc4	prezident
USA	USA	kA	USA
Donn	donna	k1gFnPc2	donna
Eisele	Eisel	k1gInSc2	Eisel
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
-	-	kIx~	-
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
letec	letec	k1gMnSc1	letec
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
Majel	Majel	k1gMnSc1	Majel
Barrettová	Barrettová	k1gFnSc1	Barrettová
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Rahsaan	Rahsaana	k1gFnPc2	Rahsaana
Roland	Rolanda	k1gFnPc2	Rolanda
Kirk	Kirk	k1gInSc1	Kirk
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
-	-	kIx~	-
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
R.	R.	kA	R.
L.	L.	kA	L.
Stine	Stin	k1gInSc5	Stin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Philip	Philip	k1gMnSc1	Philip
Michael	Michael	k1gMnSc1	Michael
Thomas	Thomas	k1gMnSc1	Thomas
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Beverly	Beverla	k1gFnSc2	Beverla
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Angelo	Angela	k1gFnSc5	Angela
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Richard	Richard	k1gMnSc1	Richard
Biggs	Biggs	k1gInSc1	Biggs
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Josh	Josh	k1gMnSc1	Josh
Radnor	Radnor	k1gMnSc1	Radnor
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Lilia	lilium	k1gNnSc2	lilium
Osterlohová	Osterlohová	k1gFnSc1	Osterlohová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Maggie	Maggie	k1gFnSc1	Maggie
Grace	Grace	k1gFnSc1	Grace
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Roman	Roman	k1gMnSc1	Roman
Atwood	Atwood	k1gInSc1	Atwood
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
YouTuber	YouTuber	k1gMnSc1	YouTuber
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Tyler	Tyler	k1gMnSc1	Tyler
Joseph	Joseph	k1gMnSc1	Joseph
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Twenty	Twenta	k1gFnSc2	Twenta
One	One	k1gMnPc2	One
Pilots	Pilots	k1gInSc1	Pilots
Josh	Josha	k1gFnPc2	Josha
Dun	duna	k1gFnPc2	duna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Twenty	Twenta	k1gFnSc2	Twenta
One	One	k1gFnSc2	One
Pilots	Pilots	k1gInSc1	Pilots
Columbus	Columbus	k1gInSc1	Columbus
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
italský	italský	k2eAgInSc1d1	italský
Janov	Janov	k1gInSc1	Janov
<g/>
.	.	kIx.	.
</s>
