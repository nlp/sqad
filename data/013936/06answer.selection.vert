<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Španělska	Španělsko	k1gNnSc2	Španělsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozená	odvozený	k2eAgFnSc1d1	odvozená
ze	z	k7c2	z
středověkého	středověký	k2eAgInSc2d1	středověký
aragonského	aragonský	k2eAgInSc2d1	aragonský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
