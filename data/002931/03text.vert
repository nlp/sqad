<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
ugrofinský	ugrofinský	k2eAgInSc4d1	ugrofinský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
částech	část	k1gFnPc6	část
srbské	srbský	k2eAgFnSc2d1	Srbská
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
Zakarpatské	zakarpatský	k2eAgFnSc2d1	Zakarpatská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
částech	část	k1gFnPc6	část
rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Burgenlandu	Burgenland	k1gInSc2	Burgenland
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
v	v	k7c6	v
částech	část	k1gFnPc6	část
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsky	maďarsky	k6eAd1	maďarsky
mluví	mluvit	k5eAaImIp3nS	mluvit
celkem	celkem	k6eAd1	celkem
zhruba	zhruba	k6eAd1	zhruba
13,3	[number]	k4	13,3
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgMnPc2d1	současný
mluvčích	mluvčí	k1gMnPc2	mluvčí
celé	celý	k2eAgFnSc2d1	celá
uralské	uralský	k2eAgFnSc2d1	Uralská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
se	se	k3xPyFc4	se
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
používají	používat	k5eAaImIp3nP	používat
spřežky	spřežka	k1gFnPc1	spřežka
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
,	,	kIx,	,
přehlásky	přehláska	k1gFnPc1	přehláska
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
přehlásky	přehláska	k1gFnPc1	přehláska
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
uralské	uralský	k2eAgFnSc2d1	Uralská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
ugrofinské	ugrofinský	k2eAgFnSc2d1	ugrofinská
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yRgNnPc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
finština	finština	k1gFnSc1	finština
<g/>
,	,	kIx,	,
estonština	estonština	k1gFnSc1	estonština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
jistou	jistý	k2eAgFnSc4d1	jistá
podobnost	podobnost	k1gFnSc4	podobnost
mluvnického	mluvnický	k2eAgInSc2d1	mluvnický
systému	systém	k1gInSc2	systém
a	a	k8xC	a
společné	společný	k2eAgInPc4d1	společný
kořeny	kořen	k1gInPc4	kořen
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
však	však	k9	však
je	být	k5eAaImIp3nS	být
příbuznost	příbuznost	k1gFnSc4	příbuznost
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
jazyky	jazyk	k1gInPc7	jazyk
dosti	dosti	k6eAd1	dosti
vzdálená	vzdálený	k2eAgNnPc1d1	vzdálené
-	-	kIx~	-
asi	asi	k9	asi
jako	jako	k9	jako
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
keltskými	keltský	k2eAgInPc7d1	keltský
či	či	k8xC	či
íránskými	íránský	k2eAgInPc7d1	íránský
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgInPc7d3	Nejbližší
jejími	její	k3xOp3gMnPc7	její
příbuznými	příbuzný	k1gMnPc7	příbuzný
jsou	být	k5eAaImIp3nP	být
jazyky	jazyk	k1gInPc1	jazyk
nepočetných	početný	k2eNgMnPc2d1	nepočetný
západosibiřských	západosibiřský	k2eAgMnPc2d1	západosibiřský
Chantů	Chant	k1gMnPc2	Chant
a	a	k8xC	a
Mansijců	Mansijce	k1gMnPc2	Mansijce
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
jazyky	jazyk	k1gInPc1	jazyk
obsko-ugrické	obskogrický	k2eAgInPc1d1	obsko-ugrický
<g/>
.	.	kIx.	.
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
-	-	kIx~	-
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
zcela	zcela	k6eAd1	zcela
odlišnými	odlišný	k2eAgInPc7d1	odlišný
indoevropskými	indoevropský	k2eAgInPc7d1	indoevropský
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
slovanskými	slovanský	k2eAgFnPc7d1	Slovanská
<g/>
,	,	kIx,	,
románskými	románský	k2eAgInPc7d1	románský
a	a	k8xC	a
germánskými	germánský	k2eAgInPc7d1	germánský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
jazykem	jazyk	k1gInSc7	jazyk
příliš	příliš	k6eAd1	příliš
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Areálově	areálově	k6eAd1	areálově
maďarština	maďarština	k1gFnSc1	maďarština
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
jazykového	jazykový	k2eAgInSc2d1	jazykový
svazu	svaz	k1gInSc2	svaz
dunajského	dunajský	k2eAgInSc2d1	dunajský
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsky	maďarsky	k6eAd1	maďarsky
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
Zdroj	zdroj	k1gInSc4	zdroj
<g/>
:	:	kIx,	:
National	National	k1gFnSc1	National
census	census	k1gInSc1	census
<g/>
,	,	kIx,	,
Ethnologue	Ethnologue	k1gNnSc1	Ethnologue
Jádrem	jádro	k1gNnSc7	jádro
maďarské	maďarský	k2eAgFnSc2d1	maďarská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
součástí	součást	k1gFnPc2	součást
Uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
:	:	kIx,	:
jižní	jižní	k2eAgNnSc1d1	jižní
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc4d1	bývalé
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srbská	srbský	k2eAgFnSc1d1	Srbská
provincie	provincie	k1gFnSc1	provincie
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
a	a	k8xC	a
rakouská	rakouský	k2eAgFnSc1d1	rakouská
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
Burgenland	Burgenlanda	k1gFnPc2	Burgenlanda
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
milion	milion	k4xCgInSc1	milion
maďarských	maďarský	k2eAgMnPc2d1	maďarský
mluvčích	mluvčí	k1gMnPc2	mluvčí
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
(	(	kIx(	(
<g/>
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
oficiální	oficiální	k2eAgInSc1d1	oficiální
jazyk	jazyk	k1gInSc1	jazyk
tři	tři	k4xCgInPc4	tři
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
obcí	obec	k1gFnPc2	obec
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
(	(	kIx(	(
<g/>
Hodoš	Hodoš	k1gMnSc1	Hodoš
/	/	kIx~	/
Hodos	Hodos	k1gMnSc1	Hodos
<g/>
,	,	kIx,	,
Dobrovnik	Dobrovnik	k1gMnSc1	Dobrovnik
/	/	kIx~	/
Dobrónak	Dobrónak	k1gMnSc1	Dobrónak
a	a	k8xC	a
Lendava	Lendava	k1gFnSc1	Lendava
/	/	kIx~	/
Lendva	Lendva	k1gFnSc1	Lendva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
slovinštinou	slovinština	k1gFnSc7	slovinština
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
také	také	k9	také
oficiálně	oficiálně	k6eAd1	oficiálně
uznaný	uznaný	k2eAgInSc1d1	uznaný
jazyk	jazyk	k1gInSc1	jazyk
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
regionální	regionální	k2eAgInSc4d1	regionální
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
městech	město	k1gNnPc6	město
a	a	k8xC	a
samosprávných	samosprávný	k2eAgFnPc6d1	samosprávná
jednotkách	jednotka	k1gFnPc6	jednotka
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
etnika	etnikum	k1gNnSc2	etnikum
vyšším	vysoký	k2eAgInSc6d2	vyšší
než	než	k8xS	než
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
dialekty	dialekt	k1gInPc4	dialekt
<g/>
:	:	kIx,	:
Alföld	Alföld	k1gInSc1	Alföld
<g/>
,	,	kIx,	,
Západní	západní	k2eAgInSc1d1	západní
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
Dunaj-Tisza	Dunaj-Tisza	k1gFnSc1	Dunaj-Tisza
<g/>
,	,	kIx,	,
Oblast	oblast	k1gFnSc1	oblast
maďarského	maďarský	k2eAgInSc2d1	maďarský
průsmyku	průsmyk	k1gInSc2	průsmyk
<g/>
,	,	kIx,	,
Severovýchodní	severovýchodní	k2eAgNnSc1d1	severovýchodní
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
sikulština	sikulština	k1gFnSc1	sikulština
či	či	k8xC	či
seklerština	seklerština	k1gFnSc1	seklerština
a	a	k8xC	a
Západní	západní	k2eAgNnSc1d1	západní
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
dialekty	dialekt	k1gInPc1	dialekt
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Maďarským	maďarský	k2eAgMnSc7d1	maďarský
csángó	csángó	k?	csángó
dialektem	dialekt	k1gInSc7	dialekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
etnolekt	etnolekt	k1gInSc4	etnolekt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Bacău	Bacăus	k1gInSc2	Bacăus
(	(	kIx(	(
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Moldávii	Moldávie	k1gFnSc6	Moldávie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
mluvících	mluvící	k2eAgFnPc2d1	mluvící
csángó	csángó	k?	csángó
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
dialekt	dialekt	k1gInSc4	dialekt
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
středověké	středověký	k2eAgFnSc3d1	středověká
maďarštině	maďarština	k1gFnSc3	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
maďarštiny	maďarština	k1gFnSc2	maďarština
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
následujících	následující	k2eAgNnPc2d1	následující
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c6	o
maďarštině	maďarština	k1gFnSc6	maďarština
můžeme	moct	k5eAaImIp1nP	moct
hovořit	hovořit	k5eAaImF	hovořit
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Maďaři	Maďar	k1gMnPc1	Maďar
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
:	:	kIx,	:
pramaďarské	pramaďarský	k2eAgNnSc1d1	pramaďarský
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
896	[number]	k4	896
/	/	kIx~	/
1000	[number]	k4	1000
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
období	období	k1gNnSc1	období
pobytu	pobyt	k1gInSc2	pobyt
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
Uralem	Ural	k1gInSc7	Ural
(	(	kIx(	(
<g/>
asi	asi	k9	asi
do	do	k7c2	do
r.	r.	kA	r.
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pramaďaři	Pramaďař	k1gMnPc1	Pramaďař
žili	žít	k5eAaImAgMnP	žít
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
uralských	uralský	k2eAgNnPc6d1	uralský
sídlech	sídlo	k1gNnPc6	sídlo
období	období	k1gNnSc2	období
migrace	migrace	k1gFnSc2	migrace
(	(	kIx(	(
<g/>
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
896	[number]	k4	896
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Pramaďaři	Pramaďař	k1gMnPc1	Pramaďař
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přesouvali	přesouvat	k5eAaImAgMnP	přesouvat
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
896	[number]	k4	896
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
horního	horní	k2eAgNnSc2d1	horní
<g />
.	.	kIx.	.
</s>
<s>
Potisí	Potisí	k1gNnSc1	Potisí
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
království	království	k1gNnSc4	království
staromaďarské	staromaďarský	k2eAgNnSc4d1	staromaďarský
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
896	[number]	k4	896
/	/	kIx~	/
1000	[number]	k4	1000
-	-	kIx~	-
1541	[number]	k4	1541
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
období	období	k1gNnSc1	období
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
nadvlády	nadvláda	k1gFnSc2	nadvláda
Turků	Turek	k1gMnPc2	Turek
nad	nad	k7c4	nad
Maďary	maďar	k1gInPc4	maďar
středomaďarské	středomaďarský	k2eAgNnSc4d1	středomaďarský
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1541	[number]	k4	1541
-	-	kIx~	-
konec	konec	k1gInSc1	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
období	období	k1gNnSc1	období
do	do	k7c2	do
osvícenství	osvícenství	k1gNnSc2	osvícenství
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
začátku	začátek	k1gInSc2	začátek
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
novomaďarské	novomaďarský	k2eAgNnSc4d1	novomaďarský
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
Jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ugrofinská	ugrofinský	k2eAgFnSc1d1	ugrofinská
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
z	z	k7c2	z
uralských	uralský	k2eAgInPc2d1	uralský
jazyků	jazyk	k1gInPc2	jazyk
oddělila	oddělit	k5eAaPmAgFnS	oddělit
již	již	k6eAd1	již
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pochází	pocházet	k5eAaImIp3nS	pocházet
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
<g/>
)	)	kIx)	)
asi	asi	k9	asi
tisíc	tisíc	k4xCgInSc1	tisíc
základních	základní	k2eAgNnPc2d1	základní
ugrofinských	ugrofinský	k2eAgNnPc2d1	ugrofinský
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
migraci	migrace	k1gFnSc3	migrace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kmenů	kmen	k1gInPc2	kmen
se	se	k3xPyFc4	se
i	i	k9	i
ugrofinská	ugrofinský	k2eAgFnSc1d1	ugrofinská
skupina	skupina	k1gFnSc1	skupina
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Ugrijci	Ugrijce	k1gMnPc1	Ugrijce
(	(	kIx(	(
<g/>
Ugorové	Ugor	k1gMnPc1	Ugor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předci	předek	k1gMnPc1	předek
dnešních	dnešní	k2eAgInPc2d1	dnešní
Maďarů	maďar	k1gInPc2	maďar
<g/>
,	,	kIx,	,
směřovali	směřovat	k5eAaImAgMnP	směřovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
východně	východně	k6eAd1	východně
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
i	i	k8xC	i
jazyk	jazyk	k1gInSc1	jazyk
pak	pak	k6eAd1	pak
ovlivňoval	ovlivňovat	k5eAaImAgInS	ovlivňovat
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
íránských	íránský	k2eAgInPc2d1	íránský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1250	[number]	k4	1250
a	a	k8xC	a
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ugrijci	Ugrijce	k1gMnPc1	Ugrijce
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
kočovný	kočovný	k2eAgInSc4d1	kočovný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
se	se	k3xPyFc4	se
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1	předchůdce
Maďarů	Maďar	k1gMnPc2	Maďar
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Vogulové	Vogulový	k2eAgNnSc1d1	Vogulový
a	a	k8xC	a
Osťakové	Osťakový	k2eAgNnSc1d1	Osťakový
odešli	odejít	k5eAaPmAgMnP	odejít
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Maďaři	Maďar	k1gMnPc1	Maďar
definitivně	definitivně	k6eAd1	definitivně
vyčlenili	vyčlenit	k5eAaPmAgMnP	vyčlenit
z	z	k7c2	z
ugrofinského	ugrofinský	k2eAgNnSc2d1	ugrofinský
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byli	být	k5eAaImAgMnP	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
íránských	íránský	k2eAgInPc2d1	íránský
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Sarmatů	Sarmat	k1gMnPc2	Sarmat
a	a	k8xC	a
Skythů	Skyth	k1gMnPc2	Skyth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
550	[number]	k4	550
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
migraci	migrace	k1gFnSc3	migrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
příval	příval	k1gInSc4	příval
turkických	turkický	k2eAgInPc2d1	turkický
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
značně	značně	k6eAd1	značně
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
maďarských	maďarský	k2eAgNnPc2d1	Maďarské
slov	slovo	k1gNnPc2	slovo
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
turkický	turkický	k2eAgInSc4d1	turkický
původ	původ	k1gInSc4	původ
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
dnešních	dnešní	k2eAgInPc2d1	dnešní
Maďarů	maďar	k1gInPc2	maďar
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
magyar	magyara	k1gFnPc2	magyara
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ugrofinské	ugrofinský	k2eAgInPc4d1	ugrofinský
mon	mon	k?	mon
(	(	kIx(	(
<g/>
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
maď	maď	k?	maď
<g/>
.	.	kIx.	.
mond	mond	k1gInSc1	mond
<g/>
)	)	kIx)	)
a	a	k8xC	a
er	er	k?	er
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
pak	pak	k6eAd1	pak
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
přešlo	přejít	k5eAaPmAgNnS	přejít
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
však	však	k9	však
převzala	převzít	k5eAaPmAgFnS	převzít
jméno	jméno	k1gNnSc4	jméno
z	z	k7c2	z
označení	označení	k1gNnSc2	označení
ungri	ungr	k1gFnSc2	ungr
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Hungari	Hungari	k1gNnSc1	Hungari
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Donu	Don	k1gInSc6	Don
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přidali	přidat	k5eAaPmAgMnP	přidat
ke	k	k7c3	k
kmenovému	kmenový	k2eAgInSc3d1	kmenový
svazu	svaz	k1gInSc3	svaz
Onogurů	Onogur	k1gInPc2	Onogur
(	(	kIx(	(
<g/>
znamená	znamenat	k5eAaImIp3nS	znamenat
deset	deset	k4xCc1	deset
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
turkické	turkický	k2eAgFnSc2d1	turkická
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
západních	západní	k2eAgInPc6d1	západní
písemných	písemný	k2eAgInPc6d1	písemný
záznamech	záznam	k1gInPc6	záznam
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
této	tento	k3xDgFnSc2	tento
říše	říš	k1gFnSc2	říš
zmiňováni	zmiňován	k2eAgMnPc1d1	zmiňován
jako	jako	k9	jako
turci	turek	k1gMnPc1	turek
nebo	nebo	k8xC	nebo
ungri	ungr	k1gMnPc1	ungr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Onogurů	Onogur	k1gInPc2	Onogur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
ungri	ungri	k6eAd1	ungri
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
Maďarům	maďar	k1gInPc3	maďar
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
834	[number]	k4	834
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Maďaři	Maďar	k1gMnPc1	Maďar
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
Podunají	Podunají	k1gNnSc6	Podunají
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
založil	založit	k5eAaPmAgMnS	založit
Štěpán	Štěpán	k1gMnSc1	Štěpán
I.	I.	kA	I.
Uherské	uherský	k2eAgNnSc1d1	Uherské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
počátku	počátek	k1gInSc2	počátek
mnohonárodnostní	mnohonárodnostní	k2eAgFnPc1d1	mnohonárodnostní
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Maďary	Maďar	k1gMnPc7	Maďar
zemi	zem	k1gFnSc4	zem
obývali	obývat	k5eAaImAgMnP	obývat
příslušníci	příslušník	k1gMnPc1	příslušník
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Rumuni	Rumun	k1gMnPc1	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
kontakty	kontakt	k1gInPc1	kontakt
vedly	vést	k5eAaImAgInP	vést
i	i	k9	i
ke	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jazyků	jazyk	k1gInPc2	jazyk
sousedních	sousední	k2eAgInPc2d1	sousední
národů	národ	k1gInPc2	národ
maďarštinu	maďarština	k1gFnSc4	maďarština
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
také	také	k9	také
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xS	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
lingua	lingua	k6eAd1	lingua
franca	franca	k6eAd1	franca
pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
různých	různý	k2eAgInPc2d1	různý
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obývaly	obývat	k5eAaImAgInP	obývat
území	území	k1gNnSc4	území
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Latinu	latina	k1gFnSc4	latina
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
úrovni	úroveň	k1gFnSc6	úroveň
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
ovládala	ovládat	k5eAaImAgFnS	ovládat
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
uherské	uherský	k2eAgFnSc2d1	uherská
šlechty	šlechta	k1gFnSc2	šlechta
i	i	k8xC	i
měšťanstva	měšťanstvo	k1gNnSc2	měšťanstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gMnSc1	jazyk
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
postavení	postavení	k1gNnSc4	postavení
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgMnSc2d1	pozdní
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
maďarském	maďarský	k2eAgInSc6d1	maďarský
jazyce	jazyk	k1gInSc6	jazyk
rezonuji	rezonovat	k5eAaImIp1nS	rezonovat
výpůjčky	výpůjčka	k1gFnPc4	výpůjčka
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
původního	původní	k2eAgNnSc2d1	původní
slovanského	slovanský	k2eAgNnSc2d1	slovanské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
podunajskeho	podunajske	k1gMnSc2	podunajske
prostoru	prostor	k1gInSc2	prostor
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc1	slovo
ze	z	k7c2	z
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
příchozí	příchozí	k1gMnPc1	příchozí
převzali	převzít	k5eAaPmAgMnP	převzít
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
-	-	kIx~	-
patak	patak	k1gInSc1	patak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
maďarských	maďarský	k2eAgNnPc6d1	Maďarské
slovech	slovo	k1gNnPc6	slovo
dokonce	dokonce	k9	dokonce
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
staroslověnská	staroslověnský	k2eAgFnSc1d1	staroslověnská
nosová	nosový	k2eAgFnSc1d1	nosová
výslovnost	výslovnost	k1gFnSc1	výslovnost
(	(	kIx(	(
<g/>
houba	houba	k1gFnSc1	houba
-	-	kIx~	-
gomba	gomba	k?	gomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
maďarské	maďarský	k2eAgFnPc1d1	maďarská
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
runovým	runový	k2eAgNnSc7d1	runové
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
žádné	žádný	k3yNgInPc4	žádný
významné	významný	k2eAgInPc4d1	významný
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
jen	jen	k9	jen
zlomky	zlomek	k1gInPc1	zlomek
obsahující	obsahující	k2eAgInPc1d1	obsahující
převážně	převážně	k6eAd1	převážně
osobní	osobní	k2eAgNnPc4d1	osobní
a	a	k8xC	a
místní	místní	k2eAgNnPc4d1	místní
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pocházejí	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgInPc1	první
maďarské	maďarský	k2eAgInPc1d1	maďarský
zlomky	zlomek	k1gInPc1	zlomek
psané	psaný	k2eAgInPc1d1	psaný
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
vpisky	vpisek	k1gInPc4	vpisek
do	do	k7c2	do
latinských	latinský	k2eAgInPc2d1	latinský
náboženských	náboženský	k2eAgInPc2d1	náboženský
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
souvislý	souvislý	k2eAgInSc1d1	souvislý
maďarský	maďarský	k2eAgInSc1d1	maďarský
text	text	k1gInSc1	text
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsky	maďarsky	k6eAd1	maďarsky
psaná	psaný	k2eAgFnSc1d1	psaná
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
maďarštiny	maďarština	k1gFnSc2	maďarština
byl	být	k5eAaImAgInS	být
pořízen	pořídit	k5eAaPmNgInS	pořídit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1430	[number]	k4	1430
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Husitská	husitský	k2eAgFnSc1d1	husitská
Bible	bible	k1gFnSc1	bible
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
maďarština	maďarština	k1gFnSc1	maďarština
se	se	k3xPyFc4	se
od	od	k7c2	od
současné	současný	k2eAgFnSc2d1	současná
značně	značně	k6eAd1	značně
lišila	lišit	k5eAaImAgFnS	lišit
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
,	,	kIx,	,
gramatikou	gramatika	k1gFnSc7	gramatika
i	i	k8xC	i
hláskoslovím	hláskosloví	k1gNnSc7	hláskosloví
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ztratila	ztratit	k5eAaPmAgFnS	ztratit
dřívější	dřívější	k2eAgFnPc4d1	dřívější
dvojhlásky	dvojhláska	k1gFnPc4	dvojhláska
a	a	k8xC	a
počet	počet	k1gInSc4	počet
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
na	na	k7c4	na
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Ustálila	ustálit	k5eAaPmAgFnS	ustálit
se	se	k3xPyFc4	se
samohlásková	samohláskový	k2eAgFnSc1d1	samohlásková
harmonie	harmonie	k1gFnSc1	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
Uher	Uhry	k1gFnPc2	Uhry
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
turečtiny	turečtina	k1gFnSc2	turečtina
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgFnPc1	první
myšlenky	myšlenka	k1gFnPc1	myšlenka
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
a	a	k8xC	a
potřeba	potřeba	k1gFnSc1	potřeba
většího	veliký	k2eAgNnSc2d2	veliký
praktického	praktický	k2eAgNnSc2d1	praktické
využití	využití	k1gNnSc2	využití
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
patent	patent	k1gInSc1	patent
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
zaváděl	zavádět	k5eAaImAgMnS	zavádět
němčinu	němčina	k1gFnSc4	němčina
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc4d1	jediný
úřední	úřední	k2eAgInSc4d1	úřední
a	a	k8xC	a
vyučovací	vyučovací	k2eAgInSc4d1	vyučovací
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Úředníci	úředník	k1gMnPc1	úředník
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
naučit	naučit	k5eAaPmF	naučit
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Vedly	vést	k5eAaImAgFnP	vést
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
čistě	čistě	k6eAd1	čistě
praktické	praktický	k2eAgInPc1d1	praktický
důvody	důvod	k1gInPc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uchovávala	uchovávat	k5eAaImAgFnS	uchovávat
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc4	postavení
úřední	úřední	k2eAgFnSc2d1	úřední
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
zase	zase	k9	zase
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nedokonalou	dokonalý	k2eNgFnSc4d1	nedokonalá
a	a	k8xC	a
nevhodnou	vhodný	k2eNgFnSc4d1	nevhodná
pro	pro	k7c4	pro
úřední	úřední	k2eAgFnPc4d1	úřední
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jazykově	jazykově	k6eAd1	jazykově
různorodého	různorodý	k2eAgNnSc2d1	různorodé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
maďarsky	maďarsky	k6eAd1	maďarsky
vůbec	vůbec	k9	vůbec
nemluvila	mluvit	k5eNaImAgFnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Jazykové	jazykový	k2eAgNnSc1d1	jazykové
nařízení	nařízení	k1gNnSc1	nařízení
se	se	k3xPyFc4	se
však	však	k9	však
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
modernizaci	modernizace	k1gFnSc4	modernizace
a	a	k8xC	a
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc1	deset
tisíc	tisíc	k4xCgInSc1	tisíc
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
především	především	k9	především
odvozováním	odvozování	k1gNnSc7	odvozování
od	od	k7c2	od
maďarských	maďarský	k2eAgInPc2d1	maďarský
slovních	slovní	k2eAgInPc2d1	slovní
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgFnSc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
maďarštině	maďarština	k1gFnSc3	maďarština
umožněno	umožnit	k5eAaPmNgNnS	umožnit
plnit	plnit	k5eAaImF	plnit
mnoho	mnoho	k4c4	mnoho
nových	nový	k2eAgFnPc2d1	nová
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
první	první	k4xOgFnSc6	první
pravidla	pravidlo	k1gNnPc1	pravidlo
maďarského	maďarský	k2eAgInSc2d1	maďarský
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
1844	[number]	k4	1844
byla	být	k5eAaImAgFnS	být
maďarština	maďarština	k1gFnSc1	maďarština
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
latinu	latina	k1gFnSc4	latina
<g/>
.	.	kIx.	.
</s>
<s>
Maďarští	maďarský	k2eAgMnPc1d1	maďarský
představitelé	představitel	k1gMnPc1	představitel
nebrali	brát	k5eNaImAgMnP	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
jazykové	jazykový	k2eAgInPc4d1	jazykový
požadavky	požadavek	k1gInPc4	požadavek
příslušníků	příslušník	k1gMnPc2	příslušník
ostatních	ostatní	k2eAgInPc2d1	ostatní
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
jejich	jejich	k3xOp3gInSc4	jejich
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
království	království	k1gNnSc6	království
netvořili	tvořit	k5eNaImAgMnP	tvořit
Maďaři	Maďar	k1gMnPc1	Maďar
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
rozpadu	rozpad	k1gInSc6	rozpad
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
maďarský	maďarský	k2eAgInSc1d1	maďarský
stát	stát	k1gInSc1	stát
národnostně	národnostně	k6eAd1	národnostně
a	a	k8xC	a
jazykově	jazykově	k6eAd1	jazykově
jednotným	jednotný	k2eAgNnSc7d1	jednotné
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgNnSc1d1	početné
maďarsky	maďarsky	k6eAd1	maďarsky
mluvící	mluvící	k2eAgNnSc1d1	mluvící
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
země	zem	k1gFnSc2	zem
na	na	k7c4	na
území	území	k1gNnSc4	území
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
maďarštiny	maďarština	k1gFnSc2	maďarština
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
uralskými	uralský	k2eAgInPc7d1	uralský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
přibližně	přibližně	k6eAd1	přibližně
21	[number]	k4	21
%	%	kIx~	%
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ugro-finského	ugroinský	k2eAgMnSc4d1	ugro-finský
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
kořenů	kořen	k1gInPc2	kořen
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
přijata	přijat	k2eAgNnPc1d1	přijato
z	z	k7c2	z
jiných	jiný	k2eAgMnPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
slovanských	slovanský	k2eAgInPc2d1	slovanský
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
11	[number]	k4	11
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
turkických	turkický	k2eAgInPc2d1	turkický
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
9,5	[number]	k4	9,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
evropskými	evropský	k2eAgMnPc7d1	evropský
jazyky	jazyk	k1gMnPc7	jazyk
maďarština	maďarština	k1gFnSc1	maďarština
sdílí	sdílet	k5eAaImIp3nS	sdílet
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
řeckého	řecký	k2eAgInSc2d1	řecký
a	a	k8xC	a
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
kořenů	kořen	k1gInPc2	kořen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
pocházejí	pocházet	k5eAaImIp3nP	pocházet
asi	asi	k9	asi
2,5	[number]	k4	2,5
%	%	kIx~	%
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
30	[number]	k4	30
%	%	kIx~	%
není	být	k5eNaImIp3nS	být
původ	původ	k1gInSc4	původ
jasný	jasný	k2eAgInSc4d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přejímání	přejímání	k1gNnSc6	přejímání
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
slova	slovo	k1gNnSc2	slovo
běžně	běžně	k6eAd1	běžně
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
maďarské	maďarský	k2eAgFnSc3d1	maďarská
fonetice	fonetika	k1gFnSc3	fonetika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slova	slovo	k1gNnSc2	slovo
asztal	asztat	k5eAaPmAgMnS	asztat
-	-	kIx~	-
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
a	a	k8xC	a
udvar	udvar	k1gInSc1	udvar
-	-	kIx~	-
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
polgár	polgár	k1gMnSc1	polgár
-	-	kIx~	-
občan	občan	k1gMnSc1	občan
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
Bürger	Bürger	k1gInSc1	Bürger
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
řeckého	řecký	k2eAgInSc2d1	řecký
a	a	k8xC	a
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
snadno	snadno	k6eAd1	snadno
rozeznatelná	rozeznatelný	k2eAgFnSc1d1	rozeznatelná
i	i	k9	i
pro	pro	k7c4	pro
mluvčí	mluvčí	k1gMnPc4	mluvčí
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
byla	být	k5eAaImAgFnS	být
<g/>
-li	i	k?	-li
převzata	převzat	k2eAgFnSc1d1	převzata
až	až	k9	až
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
vytvářením	vytváření	k1gNnSc7	vytváření
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
existujících	existující	k2eAgInPc2d1	existující
slovních	slovní	k2eAgInPc2d1	slovní
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Odvozování	odvozování	k1gNnSc1	odvozování
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
pomocí	pomocí	k7c2	pomocí
slovotvorných	slovotvorný	k2eAgFnPc2d1	slovotvorná
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
hojné	hojný	k2eAgNnSc1d1	hojné
je	být	k5eAaImIp3nS	být
též	též	k9	též
skládání	skládání	k1gNnSc1	skládání
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
slovotvorby	slovotvorba	k1gFnSc2	slovotvorba
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
<g/>
:	:	kIx,	:
základní	základní	k2eAgNnSc1d1	základní
slovo	slovo	k1gNnSc1	slovo
<g/>
:	:	kIx,	:
ad	ad	k7c4	ad
=	=	kIx~	=
dát	dát	k5eAaPmF	dát
<g/>
,	,	kIx,	,
dávat	dávat	k5eAaImF	dávat
předpony	předpona	k1gFnPc4	předpona
<g/>
:	:	kIx,	:
bead	bead	k6eAd1	bead
=	=	kIx~	=
podat	podat	k5eAaPmF	podat
<g/>
,	,	kIx,	,
elad	elad	k6eAd1	elad
=	=	kIx~	=
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
felad	felad	k6eAd1	felad
=	=	kIx~	=
podat	podat	k5eAaPmF	podat
<g/>
,	,	kIx,	,
kiad	kiad	k6eAd1	kiad
=	=	kIx~	=
vydat	vydat	k5eAaPmF	vydat
přípony	přípona	k1gFnPc4	přípona
<g/>
:	:	kIx,	:
adag	adag	k1gMnSc1	adag
=	=	kIx~	=
dávka	dávka	k1gFnSc1	dávka
<g/>
,	,	kIx,	,
porce	porce	k1gFnSc1	porce
<g/>
,	,	kIx,	,
adó	adó	k?	adó
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s>
daň	daň	k1gFnSc1	daň
předpony	předpona	k1gFnSc2	předpona
i	i	k8xC	i
přípony	přípona	k1gFnSc2	přípona
<g/>
:	:	kIx,	:
kiadó	kiadó	k?	kiadó
=	=	kIx~	=
vydavatel	vydavatel	k1gMnSc1	vydavatel
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
skládání	skládání	k1gNnSc1	skládání
<g/>
:	:	kIx,	:
munkaadó	munkaadó	k?	munkaadó
=	=	kIx~	=
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dává	dávat	k5eAaImIp3nS	dávat
práci	práce	k1gFnSc4	práce
<g/>
)	)	kIx)	)
kéz	kéz	k?	kéz
=	=	kIx~	=
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
kézi	kézi	k1gNnSc1	kézi
=	=	kIx~	=
ruční	ruční	k2eAgFnSc1d1	ruční
munka	munka	k1gFnSc1	munka
=	=	kIx~	=
práce	práce	k1gFnSc1	práce
kézimunka	kézimunka	k1gFnSc1	kézimunka
=	=	kIx~	=
ruční	ruční	k2eAgFnSc1d1	ruční
práce	práce	k1gFnSc1	práce
Maďarština	maďarština	k1gFnSc1	maďarština
používá	používat	k5eAaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
a	a	k8xC	a
spřežky	spřežka	k1gFnSc2	spřežka
<g/>
.	.	kIx.	.
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
abeceda	abeceda	k1gFnSc1	abeceda
(	(	kIx(	(
<g/>
ábécé	ábécé	k1gNnSc1	ábécé
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
44	[number]	k4	44
písmen	písmeno	k1gNnPc2	písmeno
<g/>
:	:	kIx,	:
Maďarský	maďarský	k2eAgInSc1d1	maďarský
pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
fonologický	fonologický	k2eAgInSc1d1	fonologický
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
asimilace	asimilace	k1gFnPc1	asimilace
(	(	kIx(	(
<g/>
splývání	splývání	k1gNnSc1	splývání
<g/>
)	)	kIx)	)
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
neprojevují	projevovat	k5eNaImIp3nP	projevovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
adj	adj	k?	adj
[	[	kIx(	[
<g/>
ɒ	ɒ	k?	ɒ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
dej	dát	k5eAaPmRp2nS	dát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
maďarskému	maďarský	k2eAgInSc3d1	maďarský
pravopisu	pravopis	k1gInSc3	pravopis
<g/>
:	:	kIx,	:
gimnázium	gimnázium	k1gNnSc1	gimnázium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
q	q	k?	q
<g/>
,	,	kIx,	,
w	w	k?	w
a	a	k8xC	a
x	x	k?	x
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
y	y	k?	y
v	v	k7c6	v
maďarských	maďarský	k2eAgNnPc6d1	Maďarské
slovech	slovo	k1gNnPc6	slovo
neoznačuje	označovat	k5eNaImIp3nS	označovat
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
spřežek	spřežka	k1gFnPc2	spřežka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měkčí	měkčit	k5eAaImIp3nS	měkčit
předchozí	předchozí	k2eAgFnSc4d1	předchozí
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příjmeních	příjmení	k1gNnPc6	příjmení
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dosud	dosud	k6eAd1	dosud
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
archaickými	archaický	k2eAgInPc7d1	archaický
pravopisnými	pravopisný	k2eAgInPc7d1	pravopisný
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
spřežka	spřežka	k1gFnSc1	spřežka
cz	cz	k?	cz
(	(	kIx(	(
<g/>
=	=	kIx~	=
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ch	ch	k0	ch
(	(	kIx(	(
<g/>
=	=	kIx~	=
cs	cs	k?	cs
<g/>
)	)	kIx)	)
eö	eö	k?	eö
<g/>
,	,	kIx,	,
ew	ew	k?	ew
<g/>
,	,	kIx,	,
w	w	k?	w
(	(	kIx(	(
<g/>
=	=	kIx~	=
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ő	ő	k?	ő
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aá	aá	k?	aá
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
eé	eé	k?	eé
<g/>
,	,	kIx,	,
oó	oó	k?	oó
(	(	kIx(	(
<g/>
=	=	kIx~	=
á	á	k0	á
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
,	,	kIx,	,
ó	ó	k0	ó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ss	ss	k?	ss
(	(	kIx(	(
<g/>
=	=	kIx~	=
s	s	k7c7	s
<g/>
,	,	kIx,	,
zs	zs	k?	zs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gh	gh	k?	gh
<g/>
,	,	kIx,	,
th	th	k?	th
(	(	kIx(	(
<g/>
=	=	kIx~	=
g	g	kA	g
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncové	koncový	k2eAgInPc1d1	koncový
-y	-y	k?	-y
(	(	kIx(	(
<g/>
=	=	kIx~	=
i	i	k9	i
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
poslední	poslední	k2eAgInSc1d1	poslední
jev	jev	k1gInSc1	jev
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
nejasnost	nejasnost	k1gFnSc4	nejasnost
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
y	y	k?	y
následuje	následovat	k5eAaImIp3nS	následovat
za	za	k7c7	za
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
spřežku	spřežka	k1gFnSc4	spřežka
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Batthyány	Batthyán	k1gInPc1	Batthyán
[	[	kIx(	[
<g/>
baťáni	baťán	k1gMnPc1	baťán
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
aktuálním	aktuální	k2eAgInSc7d1	aktuální
pravopisem	pravopis	k1gInSc7	pravopis
Batyáni	Batyán	k2eAgMnPc1d1	Batyán
<g/>
.	.	kIx.	.
</s>
<s>
Diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
tečky	tečka	k1gFnPc1	tečka
-	-	kIx~	-
označují	označovat	k5eAaImIp3nP	označovat
přední	přední	k2eAgFnPc4d1	přední
zaokrouhlené	zaokrouhlený	k2eAgFnPc4d1	zaokrouhlená
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
:	:	kIx,	:
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ü	ü	k?	ü
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
)	)	kIx)	)
čárka	čárka	k1gFnSc1	čárka
-	-	kIx~	-
označuje	označovat	k5eAaImIp3nS	označovat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
:	:	kIx,	:
á	á	k0	á
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
,	,	kIx,	,
í	í	k0	í
<g/>
,	,	kIx,	,
ó	ó	k0	ó
<g/>
,	,	kIx,	,
ú	ú	k0	ú
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc1	dva
čárky	čárka	k1gFnPc1	čárka
-	-	kIx~	-
označují	označovat	k5eAaImIp3nP	označovat
délku	délka	k1gFnSc4	délka
u	u	k7c2	u
předních	přední	k2eAgFnPc2d1	přední
zaokrouhlených	zaokrouhlený	k2eAgFnPc2d1	zaokrouhlená
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
:	:	kIx,	:
ő	ő	k?	ő
<g/>
,	,	kIx,	,
ű	ű	k?	ű
Maďarština	maďarština	k1gFnSc1	maďarština
používá	používat	k5eAaImIp3nS	používat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
spřežek	spřežka	k1gFnPc2	spřežka
<g/>
:	:	kIx,	:
Zvlášností	Zvlášnost	k1gFnPc2	Zvlášnost
maďarštiny	maďarština	k1gFnSc2	maďarština
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
jako	jako	k9	jako
české	český	k2eAgFnSc2d1	Česká
š	š	k?	š
<g/>
:	:	kIx,	:
sajt	sajt	k1gMnSc1	sajt
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
=	=	kIx~	=
sýr	sýr	k1gInSc1	sýr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vyslovované	vyslovovaný	k2eAgNnSc1d1	vyslovované
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
spřežkou	spřežka	k1gFnSc7	spřežka
sz	sz	k?	sz
(	(	kIx(	(
<g/>
Székesfehérvár	Székesfehérvár	k1gMnSc1	Székesfehérvár
[	[	kIx(	[
<g/>
seː	seː	k?	seː
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
naopak	naopak	k6eAd1	naopak
než	než	k8xS	než
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
<g/>
.	.	kIx.	.
</s>
<s>
Spřežka	spřežka	k1gFnSc1	spřežka
ly	ly	k?	ly
původně	původně	k6eAd1	původně
označovala	označovat	k5eAaImAgFnS	označovat
hlásku	hláska	k1gFnSc4	hláska
[	[	kIx(	[
<g/>
ʎ	ʎ	k?	ʎ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
měkké	měkký	k2eAgFnPc4d1	měkká
slovenské	slovenský	k2eAgFnPc4d1	slovenská
ľ	ľ	k?	ľ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
však	však	k9	však
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
[	[	kIx(	[
<g/>
j	j	k?	j
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
historický	historický	k2eAgInSc4d1	historický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
současná	současný	k2eAgFnSc1d1	současná
výslovnost	výslovnost	k1gFnSc1	výslovnost
už	už	k6eAd1	už
nezohledňuje	zohledňovat	k5eNaImIp3nS	zohledňovat
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
ly	ly	k?	ly
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
žáci	žák	k1gMnPc1	žák
nazpaměť	nazpaměť	k6eAd1	nazpaměť
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojené	zdvojený	k2eAgFnPc1d1	zdvojená
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
dlouze	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
hozzám	hozzat	k5eAaImIp1nS	hozzat
[	[	kIx(	[
<g/>
hozzaː	hozzaː	k?	hozzaː
<g/>
]	]	kIx)	]
/	/	kIx~	/
[	[	kIx(	[
<g/>
hozː	hozː	k?	hozː
<g/>
]	]	kIx)	]
=	=	kIx~	=
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
spřežek	spřežka	k1gFnPc2	spřežka
se	se	k3xPyFc4	se
zdvojuje	zdvojovat	k5eAaImIp3nS	zdvojovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgNnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
(	(	kIx(	(
<g/>
jössz	jössz	k1gInSc1	jössz
[	[	kIx(	[
<g/>
jø	jø	k?	jø
<g/>
]	]	kIx)	]
/	/	kIx~	/
[	[	kIx(	[
<g/>
jø	jø	k?	jø
<g/>
]	]	kIx)	]
=	=	kIx~	=
přicházíš	přicházet	k5eAaImIp2nS	přicházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojením	zdvojení	k1gNnSc7	zdvojení
(	(	kIx(	(
<g/>
délkou	délka	k1gFnSc7	délka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
megy	mega	k1gMnSc2	mega
[	[	kIx(	[
<g/>
mɛ	mɛ	k?	mɛ
<g/>
]	]	kIx)	]
=	=	kIx~	=
jde	jít	k5eAaImIp3nS	jít
vs	vs	k?	vs
<g/>
.	.	kIx.	.
meggy	megga	k1gFnSc2	megga
[	[	kIx(	[
<g/>
mɛ	mɛ	k?	mɛ
<g/>
]	]	kIx)	]
/	/	kIx~	/
[	[	kIx(	[
<g/>
mɛ	mɛ	k?	mɛ
<g/>
]	]	kIx)	]
=	=	kIx~	=
višeň	višeň	k1gFnSc1	višeň
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
na	na	k7c6	na
členech	člen	k1gMnPc6	člen
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
kvalita	kvalita	k1gFnSc1	kvalita
ani	ani	k8xC	ani
délka	délka	k1gFnSc1	délka
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Nedochází	docházet	k5eNaImIp3nP	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
slabik	slabika	k1gFnPc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
krátkých	krátký	k2eAgInPc2d1	krátký
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgFnPc1d1	Krátké
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
tvoří	tvořit	k5eAaImIp3nP	tvořit
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
těchto	tento	k3xDgInPc2	tento
párů	pár	k1gInPc2	pár
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
délkou	délka	k1gFnSc7	délka
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
<g/>
:	:	kIx,	:
<a>
/	/	kIx~	/
<á>
:	:	kIx,	:
krátké	krátká	k1gFnPc1	krátká
a	a	k8xC	a
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
zadní	zadní	k2eAgInSc1d1	zadní
[	[	kIx(	[
<g/>
ɒ	ɒ	k?	ɒ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
/	/	kIx~	/
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
Budapest	Budapest	k1gFnSc1	Budapest
[	[	kIx(	[
<g/>
budɒ	budɒ	k?	budɒ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
á	á	k0	á
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<e>
/	/	kIx~	/
<é>
:	:	kIx,	:
krátké	krátký	k2eAgFnSc2d1	krátká
e	e	k0	e
je	být	k5eAaImIp3nS	být
otevřené	otevřený	k2eAgNnSc1d1	otevřené
[	[	kIx(	[
<g/>
ɛ	ɛ	k?	ɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
é	é	k0	é
je	být	k5eAaImIp3nS	být
zavřené	zavřený	k2eAgNnSc1d1	zavřené
[	[	kIx(	[
<g/>
eː	eː	k?	eː
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
í	í	k0	í
Souhlásky	souhláska	k1gFnPc4	souhláska
se	se	k3xPyFc4	se
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
nemá	mít	k5eNaImIp3nS	mít
souhláskové	souhláskový	k2eAgInPc1d1	souhláskový
fonémy	foném	k1gInPc1	foném
odpovídající	odpovídající	k2eAgInPc1d1	odpovídající
českému	český	k2eAgMnSc3d1	český
/	/	kIx~	/
<g/>
ř	ř	k?	ř
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ch	ch	k0	ch
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
výslovnost	výslovnost	k1gFnSc1	výslovnost
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
neznělá	znělý	k2eNgFnSc1d1	neznělá
[	[	kIx(	[
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
se	se	k3xPyFc4	se
však	však	k9	však
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
zněle	zněle	k6eAd1	zněle
[	[	kIx(	[
<g/>
ɦ	ɦ	k?	ɦ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Palatální	palatální	k2eAgFnPc1d1	palatální
souhlásky	souhláska	k1gFnPc1	souhláska
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ɟ	ɟ	k?	ɟ
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
psané	psaný	k2eAgNnSc1d1	psané
<ty>
a	a	k8xC	a
<gy>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
českým	český	k2eAgInSc7d1	český
/	/	kIx~	/
<g/>
ť	ť	k?	ť
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ď	ď	k?	ď
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
jsou	být	k5eAaImIp3nP	být
popisovány	popisován	k2eAgInPc1d1	popisován
s	s	k7c7	s
frikativním	frikativní	k2eAgInSc7d1	frikativní
šumem	šum	k1gInSc7	šum
[	[	kIx(	[
<g/>
c	c	k0	c
<g/>
͡	͡	k?	͡
<g/>
ç	ç	k?	ç
<g/>
,	,	kIx,	,
ɟ	ɟ	k?	ɟ
<g/>
͡	͡	k?	͡
<g/>
ʝ	ʝ	k?	ʝ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
popis	popis	k1gInSc1	popis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
diskutován	diskutovat	k5eAaImNgInS	diskutovat
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
souhlásek	souhláska	k1gFnPc2	souhláska
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
rozlišování	rozlišování	k1gNnSc4	rozlišování
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
též	též	k9	též
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
zdvojené	zdvojený	k2eAgFnPc4d1	zdvojená
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samohlásková	samohláskový	k2eAgFnSc1d1	samohlásková
(	(	kIx(	(
<g/>
vokalická	vokalický	k2eAgFnSc1d1	vokalický
<g/>
)	)	kIx)	)
harmonie	harmonie	k1gFnSc1	harmonie
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
celou	celý	k2eAgFnSc4d1	celá
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
stavbu	stavba	k1gFnSc4	stavba
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
samohláskové	samohláskový	k2eAgFnSc2d1	samohlásková
harmonie	harmonie	k1gFnSc2	harmonie
dělíme	dělit	k5eAaImIp1nP	dělit
maďarské	maďarský	k2eAgFnPc1d1	maďarská
samohlásky	samohláska	k1gFnPc1	samohláska
na	na	k7c4	na
přední	přední	k2eAgFnPc4d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnPc4d1	zadní
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
slově	slovo	k1gNnSc6	slovo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
jen	jen	k9	jen
přední	přední	k2eAgMnSc1d1	přední
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
zadní	zadní	k2eAgFnPc4d1	zadní
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
telefon	telefon	k1gInSc1	telefon
<g/>
)	)	kIx)	)
a	a	k8xC	a
složeniny	složenina	k1gFnSc2	složenina
(	(	kIx(	(
<g/>
Budapest	Budapest	k1gInSc1	Budapest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
sufixů	sufix	k1gInPc2	sufix
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
varianty	varianta	k1gFnPc4	varianta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nP	volit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
samohláskách	samohláska	k1gFnPc6	samohláska
v	v	k7c6	v
kořeni	kořen	k1gInSc6	kořen
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
(	(	kIx(	(
<g/>
-tok	ok	k1gInSc1	-tok
/	/	kIx~	/
-tek	ek	k1gInSc1	-tek
-	-	kIx~	-
osobní	osobní	k2eAgFnSc1d1	osobní
koncovka	koncovka	k1gFnSc1	koncovka
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoba	osoba	k1gFnSc1	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dolgoztok	dolgoztok	k1gInSc1	dolgoztok
=	=	kIx~	=
pracujete	pracovat	k5eAaImIp2nP	pracovat
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnPc1d1	zadní
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
)	)	kIx)	)
ültek	ültek	k6eAd1	ültek
=	=	kIx~	=
sedíte	sedit	k5eAaImIp2nP	sedit
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnPc1d1	přední
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
)	)	kIx)	)
Samohláska	samohláska	k1gFnSc1	samohláska
i	i	k9	i
/	/	kIx~	/
í	í	k0	í
je	on	k3xPp3gMnPc4	on
sice	sice	k8xC	sice
foneticky	foneticky	k6eAd1	foneticky
přední	přední	k2eAgMnSc1d1	přední
<g/>
,	,	kIx,	,
pravidlo	pravidlo	k1gNnSc1	pravidlo
samohláskové	samohláskový	k2eAgFnSc2d1	samohlásková
harmonie	harmonie	k1gFnSc2	harmonie
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
neuplatňuje	uplatňovat	k5eNaImIp3nS	uplatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
slově	slovo	k1gNnSc6	slovo
s	s	k7c7	s
předními	přední	k2eAgFnPc7d1	přední
i	i	k8xC	i
zadními	zadní	k2eAgFnPc7d1	zadní
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
obsahující	obsahující	k2eAgInSc4d1	obsahující
jen	jen	k6eAd1	jen
samohlásku	samohláska	k1gFnSc4	samohláska
i	i	k8xC	i
/	/	kIx~	/
í	í	k0	í
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
přední	přední	k2eAgMnPc1d1	přední
<g/>
,	,	kIx,	,
např.	např.	kA	např.
illik	illik	k1gInSc1	illik
(	(	kIx(	(
<g/>
slušet	slušet	k5eAaImF	slušet
<g/>
)	)	kIx)	)
-	-	kIx~	-
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
illett	illetta	k1gFnPc2	illetta
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
jako	jako	k8xC	jako
zadní	zadní	k2eAgFnSc1d1	zadní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
iszik	iszik	k1gInSc1	iszik
(	(	kIx(	(
<g/>
pít	pít	k5eAaImF	pít
<g/>
)	)	kIx)	)
-	-	kIx~	-
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
ivott	ivotta	k1gFnPc2	ivotta
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
harmonie	harmonie	k1gFnSc2	harmonie
jediným	jediný	k2eAgInSc7d1	jediný
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
homonymy	homonymum	k1gNnPc7	homonymum
<g/>
:	:	kIx,	:
ír	ír	k?	ír
(	(	kIx(	(
<g/>
psát	psát	k5eAaImF	psát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zadní	zadní	k2eAgInSc1d1	zadní
(	(	kIx(	(
<g/>
írás	írás	k1gInSc1	írás
-	-	kIx~	-
psaní	psaní	k1gNnSc1	psaní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ír	ír	k?	ír
(	(	kIx(	(
<g/>
Ir	Ir	k1gMnSc1	Ir
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgMnSc1d1	přední
(	(	kIx(	(
<g/>
írek	írek	k1gInSc1	írek
-	-	kIx~	-
Irové	Ir	k1gMnPc1	Ir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
i	i	k9	i
/	/	kIx~	/
í	í	k0	í
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
zadopatrová	zadopatrový	k2eAgFnSc1d1	zadopatrový
samohláska	samohláska	k1gFnSc1	samohláska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
laryngální	laryngální	k2eAgFnSc1d1	laryngální
čili	čili	k8xC	čili
zadopatrová	zadopatrový	k2eAgFnSc1d1	zadopatrový
samohláska	samohláska	k1gFnSc1	samohláska
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
staletích	staletí	k1gNnPc6	staletí
se	se	k3xPyFc4	se
spodobnila	spodobnit	k5eAaPmAgFnS	spodobnit
s	s	k7c7	s
předopatrovým	předopatrův	k2eAgInSc7d1	předopatrův
i	i	k8xC	i
/	/	kIx~	/
í.	í.	k?	í.
Rovněž	rovněž	k9	rovněž
přední	přední	k2eAgNnSc4d1	přední
e	e	k0	e
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k8xC	pak
é	é	k0	é
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
se	s	k7c7	s
zadními	zadní	k2eAgFnPc7d1	zadní
samohláskami	samohláska	k1gFnPc7	samohláska
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
přivlastňovací	přivlastňovací	k2eAgFnSc1d1	přivlastňovací
koncovka	koncovka	k1gFnSc1	koncovka
-é	-é	k?	-é
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
-ért	-ért	k1gMnSc1	-ért
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
však	však	k9	však
pravidelně	pravidelně	k6eAd1	pravidelně
střídá	střídat	k5eAaImIp3nS	střídat
se	s	k7c7	s
zadním	zadní	k2eAgInSc7d1	zadní
a	a	k8xC	a
/	/	kIx~	/
á	á	k0	á
nebo	nebo	k8xC	nebo
o	o	k7c4	o
/	/	kIx~	/
ó	ó	k0	ó
(	(	kIx(	(
<g/>
přípony	přípona	k1gFnPc1	přípona
-ás	-ás	k?	-ás
/	/	kIx~	/
-és	-és	k?	-és
<g/>
,	,	kIx,	,
-ság	ág	k1gMnSc1	-ság
/	/	kIx~	/
-ség	ég	k1gMnSc1	-ség
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
samohláskovou	samohláskový	k2eAgFnSc7d1	samohlásková
harmonií	harmonie	k1gFnSc7	harmonie
souvisí	souviset	k5eAaImIp3nS	souviset
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
němčiny	němčina	k1gFnSc2	němčina
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
nikdy	nikdy	k6eAd1	nikdy
nepřehlasuje	přehlasovat	k5eNaBmIp3nS	přehlasovat
o	o	k7c4	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
na	na	k7c6	na
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ü.	ü.	k?	ü.
Každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svůj	svůj	k3xOyFgInSc4	svůj
harmonický	harmonický	k2eAgInSc4d1	harmonický
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
alternaci	alternace	k1gFnSc3	alternace
ö	ö	k?	ö
/	/	kIx~	/
e	e	k0	e
<g/>
,	,	kIx,	,
např.	např.	kA	např.
fölött	fölött	k1gInSc1	fölött
/	/	kIx~	/
felett	felett	k1gInSc1	felett
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
labiální	labiální	k2eAgFnSc1d1	labiální
vokalická	vokalický	k2eAgFnSc1d1	vokalický
harmonie	harmonie	k1gFnSc1	harmonie
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnPc4d1	uvedená
palatální	palatální	k2eAgFnPc4d1	palatální
(	(	kIx(	(
<g/>
patrové	patrový	k2eAgFnPc4d1	patrová
<g/>
)	)	kIx)	)
samohláskové	samohláskový	k2eAgFnPc4d1	samohlásková
harmonie	harmonie	k1gFnPc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
systémem	systém	k1gInSc7	systém
skloňování	skloňování	k1gNnSc4	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc4	časování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
flektivních	flektivní	k2eAgInPc2d1	flektivní
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obvykle	obvykle	k6eAd1	obvykle
platí	platit	k5eAaImIp3nS	platit
pravidlo	pravidlo	k1gNnSc1	pravidlo
jediné	jediný	k2eAgFnSc2d1	jediná
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
více	hodně	k6eAd2	hodně
mluvnických	mluvnický	k2eAgFnPc2d1	mluvnická
kategorií	kategorie	k1gFnPc2	kategorie
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
k	k	k7c3	k
maďarským	maďarský	k2eAgNnPc3d1	Maďarské
slovům	slovo	k1gNnPc3	slovo
lze	lze	k6eAd1	lze
připojit	připojit	k5eAaPmF	připojit
více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
koncovek	koncovka	k1gFnPc2	koncovka
(	(	kIx(	(
<g/>
sufixů	sufix	k1gInPc2	sufix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
"	"	kIx"	"
<g/>
přilepovaní	přilepovaný	k2eAgMnPc1d1	přilepovaný
<g/>
"	"	kIx"	"
sufixů	sufix	k1gInPc2	sufix
(	(	kIx(	(
<g/>
aglutinace	aglutinace	k1gFnSc1	aglutinace
<g/>
)	)	kIx)	)
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
významem	význam	k1gInSc7	význam
řadí	řadit	k5eAaImIp3nP	řadit
maďarštinu	maďarština	k1gFnSc4	maďarština
k	k	k7c3	k
aglutinačnímu	aglutinační	k2eAgInSc3d1	aglutinační
typu	typ	k1gInSc3	typ
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
koncovka	koncovka	k1gFnSc1	koncovka
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
mluvnickou	mluvnický	k2eAgFnSc4d1	mluvnická
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
:	:	kIx,	:
pádová	pádový	k2eAgFnSc1d1	pádová
koncovka	koncovka	k1gFnSc1	koncovka
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
přivlastňovací	přivlastňovací	k2eAgFnSc1d1	přivlastňovací
koncovka	koncovka	k1gFnSc1	koncovka
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
koncovka	koncovka	k1gFnSc1	koncovka
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
koncovka	koncovka	k1gFnSc1	koncovka
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sufixy	sufix	k1gInPc1	sufix
se	se	k3xPyFc4	se
často	často	k6eAd1	často
připojují	připojovat	k5eAaImIp3nP	připojovat
přes	přes	k7c4	přes
různé	různý	k2eAgFnPc4d1	různá
propojovací	propojovací	k2eAgFnPc4d1	propojovací
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
ház-a-i-m-ban	házan	k1gInSc1	ház-a-i-m-ban
=	=	kIx~	=
v	v	k7c6	v
mých	můj	k3xOp1gInPc6	můj
domech	dům	k1gInPc6	dům
ház	ház	k?	ház
=	=	kIx~	=
dům	dům	k1gInSc1	dům
-a-	-	k?	-a-
=	=	kIx~	=
propojovací	propojovací	k2eAgFnSc1d1	propojovací
samohláska	samohláska	k1gFnSc1	samohláska
-i-	-	k?	-i-
=	=	kIx~	=
znak	znak	k1gInSc4	znak
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
přivlastňovacím	přivlastňovací	k2eAgInSc7d1	přivlastňovací
sufixem	sufix	k1gInSc7	sufix
<g/>
)	)	kIx)	)
-m-	-	k?	-m-
=	=	kIx~	=
přivlastňovací	přivlastňovací	k2eAgInSc1d1	přivlastňovací
sufix	sufix	k1gInSc1	sufix
(	(	kIx(	(
<g/>
můj	můj	k3xOp1gInSc1	můj
<g/>
)	)	kIx)	)
-ban	an	k1gNnSc1	-ban
=	=	kIx~	=
pádová	pádový	k2eAgFnSc1d1	pádová
koncovka	koncovka	k1gFnSc1	koncovka
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
polohu	poloha	k1gFnSc4	poloha
uvnitř	uvnitř	k6eAd1	uvnitř
Kořen	kořen	k1gInSc1	kořen
slova	slovo	k1gNnSc2	slovo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
při	při	k7c6	při
přibírání	přibírání	k1gNnSc6	přibírání
koncovek	koncovka	k1gFnPc2	koncovka
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
obvykle	obvykle	k6eAd1	obvykle
zachován	zachovat	k5eAaPmNgInS	zachovat
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
stahování	stahování	k1gNnSc3	stahování
jeho	jeho	k3xOp3gFnSc2	jeho
poslední	poslední	k2eAgFnSc2d1	poslední
samohlásky	samohláska	k1gFnSc2	samohláska
(	(	kIx(	(
<g/>
zkrácení	zkrácení	k1gNnSc4	zkrácení
nebo	nebo	k8xC	nebo
vypuštění	vypuštění	k1gNnSc4	vypuštění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
agár	agár	k1gMnSc1	agár
(	(	kIx(	(
<g/>
ohař	ohař	k1gMnSc1	ohař
<g/>
)	)	kIx)	)
-	-	kIx~	-
agarak	agarak	k6eAd1	agarak
(	(	kIx(	(
<g/>
ohaři	ohař	k1gMnPc7	ohař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
se	se	k3xPyFc4	se
stahují	stahovat	k5eAaImIp3nP	stahovat
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
zakončená	zakončený	k2eAgFnSc1d1	zakončená
-alom	lom	k1gInSc1	-alom
<g/>
,	,	kIx,	,
-elem	lem	k1gInSc1	-elem
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
-almok	lmok	k1gInSc1	-almok
<g/>
,	,	kIx,	,
-elmek	lmek	k1gInSc1	-elmek
<g/>
)	)	kIx)	)
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
končící	končící	k2eAgFnPc1d1	končící
-0	-0	k4	-0
<g/>
X	X	kA	X
<g/>
0	[number]	k4	0
<g/>
g	g	kA	g
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
0	[number]	k4	0
značí	značit	k5eAaImIp3nS	značit
samohlásku	samohláska	k1gFnSc4	samohláska
a	a	k8xC	a
X	X	kA	X
souhlásku	souhláska	k1gFnSc4	souhláska
l	l	kA	l
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
zakončená	zakončený	k2eAgFnSc1d1	zakončená
krátkou	krátký	k2eAgFnSc7d1	krátká
samohláskou	samohláska	k1gFnSc7	samohláska
při	při	k7c6	při
ohýbání	ohýbání	k1gNnSc6	ohýbání
tuto	tento	k3xDgFnSc4	tento
samohlásku	samohláska	k1gFnSc4	samohláska
většinou	většinou	k6eAd1	většinou
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
alma	alma	k1gFnSc1	alma
(	(	kIx(	(
<g/>
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
almából	almából	k1gMnSc1	almából
(	(	kIx(	(
<g/>
z	z	k7c2	z
jablka	jablko	k1gNnSc2	jablko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
almáink	almáink	k1gInSc1	almáink
(	(	kIx(	(
<g/>
naše	náš	k3xOp1gNnPc1	náš
jablka	jablko	k1gNnPc1	jablko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
zakončená	zakončený	k2eAgFnSc1d1	zakončená
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
samohláskou	samohláska	k1gFnSc7	samohláska
někdy	někdy	k6eAd1	někdy
naopak	naopak	k6eAd1	naopak
tuto	tento	k3xDgFnSc4	tento
zkrátí	zkrátit	k5eAaPmIp3nP	zkrátit
a	a	k8xC	a
připojí	připojit	k5eAaPmIp3nP	připojit
koncovku	koncovka	k1gFnSc4	koncovka
přes	přes	k7c4	přes
vložené	vložený	k2eAgInPc4d1	vložený
-v-	-	k?	-v-
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ló	ló	k?	ló
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
-	-	kIx~	-
lovak	lovak	k6eAd1	lovak
(	(	kIx(	(
<g/>
koně	kůň	k1gMnPc1	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kő	kő	k?	kő
(	(	kIx(	(
<g/>
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
-	-	kIx~	-
kövek	kövek	k1gInSc1	kövek
(	(	kIx(	(
<g/>
kameny	kámen	k1gInPc1	kámen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
kořenů	kořen	k1gInPc2	kořen
prochází	procházet	k5eAaImIp3nS	procházet
výraznější	výrazný	k2eAgFnSc7d2	výraznější
proměnou	proměna	k1gFnSc7	proměna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
teher	teher	k1gInSc1	teher
(	(	kIx(	(
<g/>
náklad	náklad	k1gInSc1	náklad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc4	plurál
terhek	terhky	k1gFnPc2	terhky
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
prodlužování	prodlužování	k1gNnSc2	prodlužování
koncové	koncový	k2eAgFnSc2d1	koncová
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgMnSc1d1	univerzální
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
případně	případně	k6eAd1	případně
týkají	týkat	k5eAaImIp3nP	týkat
jen	jen	k6eAd1	jen
přivlastňovacích	přivlastňovací	k2eAgInPc2d1	přivlastňovací
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
plurálu	plurál	k1gInSc2	plurál
<g/>
,	,	kIx,	,
akuzativu	akuzativ	k1gInSc2	akuzativ
a	a	k8xC	a
superessivu	superessiv	k1gInSc2	superessiv
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
<g/>
"	"	kIx"	"
-	-	kIx~	-
koncovka	koncovka	k1gFnSc1	koncovka
-n	-n	k?	-n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikoliv	nikoliv	k9	nikoliv
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
kořene	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přípony	přípona	k1gFnPc1	přípona
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
systematicky	systematicky	k6eAd1	systematicky
pokud	pokud	k8xS	pokud
přípona	přípona	k1gFnSc1	přípona
začínající	začínající	k2eAgFnSc1d1	začínající
na	na	k7c6	na
v	v	k7c4	v
(	(	kIx(	(
<g/>
-val	al	k1gMnSc1	-val
/	/	kIx~	/
-vel	el	k1gMnSc1	-vel
<g/>
,	,	kIx,	,
-vá	á	k?	-vá
/	/	kIx~	/
-vé	é	k?	-vé
<g/>
)	)	kIx)	)
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
zakončenému	zakončený	k2eAgInSc3d1	zakončený
na	na	k7c6	na
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
v	v	k7c6	v
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
této	tento	k3xDgFnSc3	tento
souhlásce	souhláska	k1gFnSc3	souhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
szándék	szándék	k1gMnSc1	szándék
+	+	kIx~	+
-val	al	k1gMnSc1	-val
dává	dávat	k5eAaImIp3nS	dávat
szándékkal	szándékkal	k1gMnSc1	szándékkal
(	(	kIx(	(
<g/>
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
szándékok	szándékok	k1gInSc4	szándékok
tvoří	tvořit	k5eAaImIp3nS	tvořit
szándékokkal	szándékokkat	k5eAaImAgMnS	szándékokkat
(	(	kIx(	(
<g/>
s	s	k7c7	s
úmysly	úmysl	k1gInPc7	úmysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
je	být	k5eAaImIp3nS	být
bezrodý	bezrodý	k2eAgInSc4d1	bezrodý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
u	u	k7c2	u
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
:	:	kIx,	:
ő	ő	k?	ő
znamená	znamenat	k5eAaImIp3nS	znamenat
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
i	i	k9	i
ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
rozlišit	rozlišit	k5eAaPmF	rozlišit
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
možnosti	možnost	k1gFnPc1	možnost
přechylování	přechylování	k1gNnSc2	přechylování
na	na	k7c6	na
úrovní	úroveň	k1gFnPc2	úroveň
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
složeniny	složenina	k1gFnPc1	složenina
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
nő	nő	k?	nő
(	(	kIx(	(
<g/>
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
tanár	tanár	k1gMnSc1	tanár
=	=	kIx~	=
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
tanárnő	tanárnő	k?	tanárnő
=	=	kIx~	=
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
jednotné	jednotný	k2eAgNnSc4d1	jednotné
a	a	k8xC	a
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pomocí	pomocí	k7c2	pomocí
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
sufixu	sufix	k1gInSc2	sufix
-k	-k	k?	-k
nebo	nebo	k8xC	nebo
-i-	-	k?	-i-
(	(	kIx(	(
<g/>
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
přivlastňovacích	přivlastňovací	k2eAgInPc2d1	přivlastňovací
sufixů	sufix	k1gInPc2	sufix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
připojuje	připojovat	k5eAaImIp3nS	připojovat
jen	jen	k9	jen
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
jménu	jméno	k1gNnSc3	jméno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ukazovacímu	ukazovací	k2eAgNnSc3d1	ukazovací
zájmenu	zájmeno	k1gNnSc3	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
slovním	slovní	k2eAgInPc3d1	slovní
druhům	druh	k1gInPc3	druh
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
jen	jen	k9	jen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
blíže	blízce	k6eAd2	blízce
určen	určit	k5eAaPmNgInS	určit
počet	počet	k1gInSc1	počet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
počet	počet	k1gInSc1	počet
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
číslovkou	číslovka	k1gFnSc7	číslovka
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
egy	ego	k1gNnPc7	ego
fa	fa	k1gNnSc1	fa
=	=	kIx~	=
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc4	jeden
<g/>
)	)	kIx)	)
strom	strom	k1gInSc4	strom
fák	fák	k?	fák
=	=	kIx~	=
stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
mnohost	mnohost	k1gFnSc1	mnohost
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
koncovka	koncovka	k1gFnSc1	koncovka
-k	-k	k?	-k
<g/>
)	)	kIx)	)
sok	sok	k1gMnSc1	sok
fa	fa	kA	fa
=	=	kIx~	=
mnoho	mnoho	k6eAd1	mnoho
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
öt	öt	k?	öt
fa	fa	kA	fa
=	=	kIx~	=
pět	pět	k4xCc4	pět
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
číslovka	číslovka	k1gFnSc1	číslovka
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
počet	počet	k1gInSc4	počet
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
Systém	systém	k1gInSc1	systém
přivlastňování	přivlastňování	k1gNnSc2	přivlastňování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
odlišný	odlišný	k2eAgInSc4d1	odlišný
od	od	k7c2	od
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
nepojí	pojit	k5eNaImIp3nP	pojit
s	s	k7c7	s
přivlastňovacími	přivlastňovací	k2eAgNnPc7d1	přivlastňovací
zájmeny	zájmeno	k1gNnPc7	zájmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přibírají	přibírat	k5eAaImIp3nP	přibírat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
přivlastňovací	přivlastňovací	k2eAgInSc4d1	přivlastňovací
sufix	sufix	k1gInSc4	sufix
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
én	én	k?	én
<g/>
)	)	kIx)	)
könyvem	könyv	k1gMnSc7	könyv
<g/>
.	.	kIx.	.
=	=	kIx~	=
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gFnSc1	můj
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pouze	pouze	k6eAd1	pouze
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
přísudku	přísudek	k1gInSc6	přísudek
<g/>
:	:	kIx,	:
Ez	Ez	k1gMnSc1	Ez
a	a	k8xC	a
könyv	könyv	k1gMnSc1	könyv
az	az	k?	az
enyém	enyé	k1gNnSc6	enyé
<g/>
.	.	kIx.	.
=	=	kIx~	=
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gFnSc1	můj
<g/>
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
přivlastňovací	přivlastňovací	k2eAgFnSc1d1	přivlastňovací
koncovka	koncovka	k1gFnSc1	koncovka
-é	-é	k?	-é
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
ke	k	k7c3	k
jménu	jméno	k1gNnSc3	jméno
vlastníka	vlastník	k1gMnSc2	vlastník
<g/>
:	:	kIx,	:
Ez	Ez	k1gMnSc1	Ez
a	a	k8xC	a
könyv	könyv	k1gMnSc1	könyv
Pálé	Pálá	k1gFnSc2	Pálá
<g/>
.	.	kIx.	.
=	=	kIx~	=
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
Přivlastňuje	přivlastňovat	k5eAaImIp3nS	přivlastňovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
konkrétní	konkrétní	k2eAgFnSc3d1	konkrétní
osobě	osoba	k1gFnSc3	osoba
nebo	nebo	k8xC	nebo
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
přibírá	přibírat	k5eAaImIp3nS	přibírat
přivlastňovaná	přivlastňovaný	k2eAgFnSc1d1	přivlastňovaná
věc	věc	k1gFnSc1	věc
přivlastňovací	přivlastňovací	k2eAgFnSc2d1	přivlastňovací
<g />
.	.	kIx.	.
</s>
<s>
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vlastník	vlastník	k1gMnSc1	vlastník
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
buď	buď	k8xC	buď
bez	bez	k7c2	bez
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
dativu	dativ	k1gInSc2	dativ
<g/>
:	:	kIx,	:
kutya	kuty	k2eAgFnSc1d1	kuty
lába	lába	k1gFnSc1	lába
=	=	kIx~	=
noha	noha	k1gFnSc1	noha
psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
pes	pes	k1gMnSc1	pes
jeho	jeho	k3xOp3gFnSc6	jeho
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
kutyának	kutyának	k1gInSc1	kutyának
a	a	k8xC	a
lába	lába	k1gFnSc1	lába
=	=	kIx~	=
noha	noha	k1gFnSc1	noha
psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
psovi	psův	k2eAgMnPc1d1	psův
ta	ten	k3xDgFnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
Pál	Pál	k1gFnSc1	Pál
könyve	könyev	k1gFnSc2	könyev
/	/	kIx~	/
Pálnak	Pálnak	k1gMnSc1	Pálnak
a	a	k8xC	a
könyve	könyvat	k5eAaPmIp3nS	könyvat
=	=	kIx~	=
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
kniha	kniha	k1gFnSc1	kniha
Maďarština	maďarština	k1gFnSc1	maďarština
nemá	mít	k5eNaImIp3nS	mít
sloveso	sloveso	k1gNnSc4	sloveso
s	s	k7c7	s
významem	význam	k1gInSc7	význam
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
vazba	vazba	k1gFnSc1	vazba
se	se	k3xPyFc4	se
slovesem	sloveso	k1gNnSc7	sloveso
van	vana	k1gFnPc2	vana
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
přibírá	přibírat	k5eAaImIp3nS	přibírat
dativní	dativní	k2eAgFnSc4d1	dativní
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
:	:	kIx,	:
Van	van	k1gInSc1	van
kutyám	kutyat	k5eAaBmIp1nS	kutyat
<g/>
.	.	kIx.	.
=	=	kIx~	=
Mám	mít	k5eAaImIp1nS	mít
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gMnSc1	můj
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
Péternek	Péternek	k1gMnSc1	Péternek
van	vana	k1gFnPc2	vana
kutyája	kutyája	k1gMnSc1	kutyája
<g/>
.	.	kIx.	.
=	=	kIx~	=
Petr	Petr	k1gMnSc1	Petr
má	mít	k5eAaImIp3nS	mít
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
Petrovi	Petr	k1gMnSc3	Petr
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnPc2	jeho
pes	peso	k1gNnPc2	peso
<g/>
)	)	kIx)	)
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
determinována	determinován	k2eAgFnSc1d1	determinována
členem	člen	k1gMnSc7	člen
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
:	:	kIx,	:
neurčitý	určitý	k2eNgMnSc1d1	neurčitý
<g/>
:	:	kIx,	:
egy	ego	k1gNnPc7	ego
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
význam	význam	k1gInSc4	význam
jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
určitý	určitý	k2eAgMnSc1d1	určitý
<g/>
:	:	kIx,	:
a	a	k8xC	a
(	(	kIx(	(
<g/>
před	před	k7c7	před
jmény	jméno	k1gNnPc7	jméno
začínajícími	začínající	k2eAgNnPc7d1	začínající
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
az	az	k?	az
(	(	kIx(	(
<g/>
před	před	k7c7	před
jmény	jméno	k1gNnPc7	jméno
začínajícími	začínající	k2eAgNnPc7d1	začínající
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
přibírají	přibírat	k5eAaImIp3nP	přibírat
přivlastňovací	přivlastňovací	k2eAgInPc1d1	přivlastňovací
sufixy	sufix	k1gInPc1	sufix
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
pádových	pádový	k2eAgFnPc2d1	pádová
koncovek	koncovka	k1gFnPc2	koncovka
je	být	k5eAaImIp3nS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
další	další	k2eAgInPc1d1	další
sklonné	sklonný	k2eAgInPc1d1	sklonný
slovní	slovní	k2eAgInPc1d1	slovní
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
tři	tři	k4xCgFnPc4	tři
varianty	varianta	k1gFnPc4	varianta
podle	podle	k7c2	podle
samohláskové	samohláskový	k2eAgFnSc2d1	samohlásková
harmonie	harmonie	k1gFnSc2	harmonie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
tvarem	tvar	k1gInSc7	tvar
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
koncovku	koncovka	k1gFnSc4	koncovka
nemá	mít	k5eNaImIp3nS	mít
<g/>
:	:	kIx,	:
kutya	kutya	k1gMnSc1	kutya
(	(	kIx(	(
<g/>
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ház	ház	k?	ház
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
genitiv	genitiv	k1gInSc1	genitiv
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
takový	takový	k3xDgInSc1	takový
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
opisuje	opisovat	k5eAaImIp3nS	opisovat
se	s	k7c7	s
přivlastňovacími	přivlastňovací	k2eAgFnPc7d1	přivlastňovací
vazbami	vazba	k1gFnPc7	vazba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
(	(	kIx(	(
<g/>
dativu	dativ	k1gInSc2	dativ
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
koncovka	koncovka	k1gFnSc1	koncovka
-nak	ak	k1gMnSc1	-nak
/	/	kIx~	/
-nek	ek	k1gMnSc1	-nek
<g/>
:	:	kIx,	:
háznak	háznak	k1gMnSc1	háznak
(	(	kIx(	(
<g/>
domu	dům	k1gInSc2	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terminativ	terminativum	k1gNnPc2	terminativum
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
-ig	g	k?	-ig
<g/>
:	:	kIx,	:
házig	házig	k1gMnSc1	házig
((	((	k?	((
<g/>
až	až	k6eAd1	až
<g/>
)	)	kIx)	)
k	k	k7c3	k
domu	dům	k1gInSc3	dům
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
-t	-t	k?	-t
<g/>
:	:	kIx,	:
kutyát	kutyát	k1gMnSc1	kutyát
(	(	kIx(	(
<g/>
psa	pes	k1gMnSc4	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kauzál-finál	Kauzálinál	k1gInSc1	Kauzál-finál
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
koncovkou	koncovka	k1gFnSc7	koncovka
-ért	-érta	k1gFnPc2	-érta
<g/>
:	:	kIx,	:
kutyáért	kutyáért	k1gInSc1	kutyáért
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
vokativ	vokativ	k1gInSc1	vokativ
<g/>
)	)	kIx)	)
maďarština	maďarština	k1gFnSc1	maďarština
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádem	pád	k1gInSc7	pád
6	[number]	k4	6
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
lokál	lokál	k1gInSc1	lokál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyjadřován	vyjadřovat	k5eAaImNgInS	vyjadřovat
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
a	a	k8xC	a
pohybu	pohyb	k1gInSc3	pohyb
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
matice	matice	k1gFnSc1	matice
devíti	devět	k4xCc2	devět
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
kam	kam	k6eAd1	kam
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
<g/>
:	:	kIx,	:
házba	házba	k1gMnSc1	házba
-	-	kIx~	-
házban	házban	k1gMnSc1	házban
-	-	kIx~	-
házból	házból	k1gMnSc1	házból
=	=	kIx~	=
do	do	k7c2	do
domu	dům	k1gInSc2	dům
-	-	kIx~	-
v	v	k7c6	v
domě	dům	k1gInSc6	dům
-	-	kIx~	-
z	z	k7c2	z
domu	dům	k1gInSc2	dům
kertbe	kertbat	k5eAaPmIp3nS	kertbat
-	-	kIx~	-
kertben	kertben	k2eAgInSc4d1	kertben
-	-	kIx~	-
kertből	kertből	k1gInSc4	kertből
=	=	kIx~	=
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
-	-	kIx~	-
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
-	-	kIx~	-
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
povrchové	povrchový	k2eAgFnSc2d1	povrchová
<g/>
:	:	kIx,	:
asztalra	asztalra	k6eAd1	asztalra
-	-	kIx~	-
asztalon	asztalon	k1gInSc1	asztalon
-	-	kIx~	-
asztalról	asztalról	k1gInSc1	asztalról
=	=	kIx~	=
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
-	-	kIx~	-
na	na	k7c6	na
stole	stol	k1gInSc6	stol
-	-	kIx~	-
ze	z	k7c2	z
stolu	stol	k1gInSc2	stol
<g />
.	.	kIx.	.
</s>
<s>
székre	székr	k1gMnSc5	székr
-	-	kIx~	-
széken	széken	k2eAgInSc4d1	széken
-	-	kIx~	-
székről	székről	k1gInSc4	székről
=	=	kIx~	=
na	na	k7c4	na
židli	židle	k1gFnSc4	židle
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
židli	židle	k1gFnSc6	židle
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
<g/>
)	)	kIx)	)
-	-	kIx~	-
ze	z	k7c2	z
židle	židle	k1gFnSc2	židle
přibližné	přibližný	k2eAgFnSc2d1	přibližná
<g/>
:	:	kIx,	:
orvoshoz	orvoshoza	k1gFnPc2	orvoshoza
-	-	kIx~	-
orvosnál	orvosnál	k1gInSc1	orvosnál
-	-	kIx~	-
orvostól	orvostól	k1gInSc1	orvostól
=	=	kIx~	=
k	k	k7c3	k
lékaři	lékař	k1gMnPc1	lékař
-	-	kIx~	-
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
-	-	kIx~	-
od	od	k7c2	od
lékaře	lékař	k1gMnSc2	lékař
Péterhez	Péterheza	k1gFnPc2	Péterheza
-	-	kIx~	-
Péternél	Péternél	k1gMnSc1	Péternél
-	-	kIx~	-
Pétertől	Pétertől	k1gMnSc1	Pétertől
=	=	kIx~	=
k	k	k7c3	k
Petrovi	Petr	k1gMnSc3	Petr
-	-	kIx~	-
u	u	k7c2	u
Petra	Petr	k1gMnSc2	Petr
-	-	kIx~	-
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
prostorových	prostorový	k2eAgInPc2d1	prostorový
vztahů	vztah	k1gInPc2	vztah
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
<g/>
,	,	kIx,	,
zpod	zpod	k7c2	zpod
<g/>
,	,	kIx,	,
za	za	k7c7	za
<g/>
,	,	kIx,	,
zpoza	zpoza	k7c2	zpoza
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyjadřována	vyjadřovat	k5eAaImNgFnS	vyjadřovat
systémem	systém	k1gInSc7	systém
záložek	záložka	k1gFnPc2	záložka
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
-ról	ól	k1gMnSc1	-ról
/	/	kIx~	/
-ről	ől	k1gMnSc1	-ről
také	také	k9	také
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
českou	český	k2eAgFnSc4d1	Česká
vazbu	vazba	k1gFnSc4	vazba
"	"	kIx"	"
<g/>
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
časovou	časový	k2eAgFnSc4d1	časová
lokalizaci	lokalizace	k1gFnSc4	lokalizace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
koncovka	koncovka	k1gFnSc1	koncovka
-kor	or	k1gMnSc1	-kor
<g/>
:	:	kIx,	:
éjfélkor	éjfélkor	k1gMnSc1	éjfélkor
(	(	kIx(	(
<g/>
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
(	(	kIx(	(
<g/>
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-val	ala	k1gFnPc2	-vala
/	/	kIx~	/
-vel	el	k1gInSc1	-vel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
koncové	koncový	k2eAgFnSc3d1	koncová
souhlásce	souhláska	k1gFnSc3	souhláska
základního	základní	k2eAgInSc2d1	základní
tvaru	tvar	k1gInSc2	tvar
<g/>
:	:	kIx,	:
almával	almávat	k5eAaPmAgMnS	almávat
(	(	kIx(	(
<g/>
s	s	k7c7	s
jablkem	jablko	k1gNnSc7	jablko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
köszönettel	köszönettel	k1gMnSc1	köszönettel
(	(	kIx(	(
<g/>
s	s	k7c7	s
vděkou	vděka	k1gFnSc7	vděka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
je	být	k5eAaImIp3nS	být
translativ	translatit	k5eAaPmDgInS	translatit
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
změnu	změna	k1gFnSc4	změna
stavu	stav	k1gInSc2	stav
<g/>
:	:	kIx,	:
jég	jég	k?	jég
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
-	-	kIx~	-
jéggé	jéggé	k1gNnSc1	jéggé
válik	válika	k1gFnPc2	válika
(	(	kIx(	(
<g/>
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
změnit	změnit	k5eAaPmF	změnit
se	se	k3xPyFc4	se
v	v	k7c4	v
led	led	k1gInSc4	led
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
proměnu	proměna	k1gFnSc4	proměna
<g/>
:	:	kIx,	:
szép	szép	k1gInSc4	szép
(	(	kIx(	(
<g/>
krásný	krásný	k2eAgInSc4d1	krásný
<g/>
)	)	kIx)	)
-	-	kIx~	-
széppé	széppý	k2eAgInPc4d1	széppý
lesz	lesz	k1gInSc4	lesz
(	(	kIx(	(
<g/>
zkrásnět	zkrásnět	k5eAaPmF	zkrásnět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příponami	přípona	k1gFnPc7	přípona
translativu	translativ	k1gInSc2	translativ
-vá	á	k?	-vá
/	/	kIx~	/
-vé	é	k?	-vé
taky	taky	k6eAd1	taky
tvoříme	tvořit	k5eAaImIp1nP	tvořit
sloveso	sloveso	k1gNnSc4	sloveso
z	z	k7c2	z
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
nevetséges	nevetséges	k1gInSc1	nevetséges
(	(	kIx(	(
<g/>
směšný	směšný	k2eAgMnSc1d1	směšný
<g/>
)	)	kIx)	)
-	-	kIx~	-
nevetségessé	vetségessý	k2eNgInPc4d1	vetségessý
tesz	tesz	k1gInSc4	tesz
(	(	kIx(	(
<g/>
zesměšnit	zesměšnit	k5eAaPmF	zesměšnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uděláme	udělat	k5eAaPmIp1nP	udělat
někoho	někdo	k3yInSc4	někdo
/	/	kIx~	/
něco	něco	k3yInSc4	něco
nějakým	nějaký	k3yIgInSc7	nějaký
/	/	kIx~	/
čím	co	k3yRnSc7	co
<g/>
:	:	kIx,	:
boldogtalan	boldogtalan	k1gMnSc1	boldogtalan
(	(	kIx(	(
<g/>
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
)	)	kIx)	)
-	-	kIx~	-
boldogtalanná	boldogtalanný	k2eAgFnSc1d1	boldogtalanný
tesz	tesz	k1gInSc1	tesz
(	(	kIx(	(
<g/>
udělat	udělat	k5eAaPmF	udělat
nešťastným	šťastný	k2eNgNnSc7d1	nešťastné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
vyjádření	vyjádření	k1gNnSc2	vyjádření
essivu	essiv	k1gInSc6	essiv
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jako	jako	k8xC	jako
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
formální	formální	k2eAgMnSc1d1	formální
-ként	ént	k1gMnSc1	-ként
a	a	k8xC	a
modální	modální	k2eAgMnSc1d1	modální
-ul	l	k?	-ul
/	/	kIx~	/
-ül	-ül	k?	-ül
<g/>
:	:	kIx,	:
orvosként	orvosként	k1gMnSc1	orvosként
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
lékař	lékař	k1gMnSc1	lékař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
például	például	k1gInSc1	például
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
přijímají	přijímat	k5eAaImIp3nP	přijímat
koncovky	koncovka	k1gFnPc1	koncovka
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nepojí	pojit	k5eNaImIp3nS	pojit
s	s	k7c7	s
podstatnými	podstatný	k2eAgNnPc7d1	podstatné
jmény	jméno	k1gNnPc7	jméno
<g/>
:	:	kIx,	:
A	a	k9	a
kutyák	kutyák	k1gInSc1	kutyák
nagyok	nagyok	k1gInSc1	nagyok
<g/>
.	.	kIx.	.
=	=	kIx~	=
Ti	ten	k3xDgMnPc1	ten
psi	pes	k1gMnPc1	pes
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
tvar	tvar	k1gInSc4	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
přísudku	přísudek	k1gInSc2	přísudek
<g/>
)	)	kIx)	)
Milyen	Milyen	k2eAgMnSc1d1	Milyen
inget	inget	k1gMnSc1	inget
akarsz	akarsz	k1gMnSc1	akarsz
<g/>
?	?	kIx.	?
</s>
<s>
Fehéret	Fehéret	k1gInSc1	Fehéret
<g/>
.	.	kIx.	.
=	=	kIx~	=
Jakou	jaký	k3yIgFnSc4	jaký
košili	košile	k1gFnSc4	košile
chceš	chtít	k5eAaImIp2nS	chtít
<g/>
?	?	kIx.	?
</s>
<s>
Bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
akuzativní	akuzativní	k2eAgInSc1d1	akuzativní
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
V	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
přívlastku	přívlastek	k1gInSc2	přívlastek
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
nesklonné	sklonný	k2eNgNnSc1d1	nesklonné
(	(	kIx(	(
<g/>
srovnej	srovnat	k5eAaPmRp2nS	srovnat
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uvedenými	uvedený	k2eAgInPc7d1	uvedený
příklady	příklad	k1gInPc7	příklad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nagy	nagy	k1gInPc4	nagy
kutyák	kutyák	k1gInSc1	kutyák
=	=	kIx~	=
velcí	velký	k2eAgMnPc1d1	velký
psi	pes	k1gMnPc1	pes
Akarom	Akarom	k1gInSc4	Akarom
a	a	k8xC	a
fehér	fehér	k1gInSc4	fehér
inget	inget	k5eAaPmF	inget
<g/>
.	.	kIx.	.
=	=	kIx~	=
Chci	chtít	k5eAaImIp1nS	chtít
tu	ten	k3xDgFnSc4	ten
bílou	bílý	k2eAgFnSc4d1	bílá
košili	košile	k1gFnSc4	košile
<g/>
.	.	kIx.	.
</s>
<s>
Stupňování	stupňování	k1gNnSc1	stupňování
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
komparativ	komparativ	k1gInSc1	komparativ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-abb	bba	k1gFnPc2	-abba
/	/	kIx~	/
-ebb	bb	k1gInSc1	-ebb
(	(	kIx(	(
<g/>
-ší	-ší	k?	-ší
<g/>
,	,	kIx,	,
-ejší	jšit	k5eAaPmIp3nS	-ejšit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
předpony	předpona	k1gFnSc2	předpona
leg-	leg-	k?	leg-
(	(	kIx(	(
<g/>
nej-	nej-	k?	nej-
<g/>
)	)	kIx)	)
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
tvaru	tvar	k1gInSc3	tvar
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
3	[number]	k4	3
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
superlativ	superlativ	k1gInSc1	superlativ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
gyors	gyors	k6eAd1	gyors
gyorsabb	gyorsabb	k1gMnSc1	gyorsabb
leggyorsabb	leggyorsabb	k1gMnSc1	leggyorsabb
=	=	kIx~	=
rychlý	rychlý	k2eAgInSc1d1	rychlý
rychlejší	rychlý	k2eAgFnSc7d2	rychlejší
nejrychlejší	rychlý	k2eAgFnSc7d3	nejrychlejší
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
stupňování	stupňování	k1gNnSc1	stupňování
slova	slovo	k1gNnSc2	slovo
jó	jó	k0	jó
(	(	kIx(	(
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
)	)	kIx)	)
-	-	kIx~	-
jobb	jobb	k1gMnSc1	jobb
<g/>
,	,	kIx,	,
legjobb	legjobb	k1gMnSc1	legjobb
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
adjektivu	adjektivum	k1gNnSc3	adjektivum
nagy	naga	k1gFnSc2	naga
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
)	)	kIx)	)
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
přípona	přípona	k1gFnSc1	přípona
-obb	bb	k1gMnSc1	-obb
(	(	kIx(	(
<g/>
nagyobb	nagyobb	k1gMnSc1	nagyobb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
dvojí	dvojí	k4xRgNnSc4	dvojí
časování	časování	k1gNnSc4	časování
-	-	kIx~	-
určité	určitý	k2eAgNnSc4d1	určité
(	(	kIx(	(
<g/>
předmětné	předmětný	k2eAgNnSc4d1	předmětné
<g/>
)	)	kIx)	)
a	a	k8xC	a
neurčité	určitý	k2eNgNnSc1d1	neurčité
(	(	kIx(	(
<g/>
podmětné	podmětný	k2eAgNnSc1d1	podmětné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
časech	čas	k1gInPc6	čas
a	a	k8xC	a
způsobech	způsob	k1gInPc6	způsob
přibírají	přibírat	k5eAaImIp3nP	přibírat
dvě	dva	k4xCgFnPc1	dva
sady	sada	k1gFnPc1	sada
osobních	osobní	k2eAgFnPc2d1	osobní
koncovek	koncovka	k1gFnPc2	koncovka
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
<g/>
,	,	kIx,	,
neurčitým	určitý	k2eNgInSc7d1	neurčitý
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
žádným	žádný	k3yNgInSc7	žádný
předmětem	předmět	k1gInSc7	předmět
<g/>
:	:	kIx,	:
Építettél	Építettél	k1gInSc1	Építettél
egy	ego	k1gNnPc7	ego
házat	házat	k5eAaBmF	házat
<g/>
.	.	kIx.	.
=	=	kIx~	=
Stavěl	stavět	k5eAaImAgMnS	stavět
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
nějaký	nějaký	k3yIgInSc1	nějaký
<g/>
)	)	kIx)	)
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
neurčité	určitý	k2eNgNnSc1d1	neurčité
časování	časování	k1gNnSc1	časování
<g/>
)	)	kIx)	)
Építetted	Építetted	k1gInSc1	Építetted
a	a	k8xC	a
házat	házat	k1gInSc1	házat
<g/>
.	.	kIx.	.
=	=	kIx~	=
Stavěl	stavět	k5eAaImAgMnS	stavět
si	se	k3xPyFc3	se
ten	ten	k3xDgInSc4	ten
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
určité	určitý	k2eAgNnSc4d1	určité
časování	časování	k1gNnSc4	časování
<g/>
)	)	kIx)	)
Nepřechodná	přechodný	k2eNgFnSc1d1	nepřechodná
(	(	kIx(	(
<g/>
intranzitivní	intranzitivní	k2eAgFnSc1d1	intranzitivní
<g/>
)	)	kIx)	)
slovesa	sloveso	k1gNnPc1	sloveso
nemají	mít	k5eNaImIp3nP	mít
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
neurčité	určitý	k2eNgNnSc4d1	neurčité
časování	časování	k1gNnSc4	časování
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
časování	časování	k1gNnSc6	časování
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
přidávají	přidávat	k5eAaImIp3nP	přidávat
osobní	osobní	k2eAgFnPc1d1	osobní
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
a	a	k8xC	a
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
označovat	označovat	k5eAaImF	označovat
podmět	podmět	k1gInSc1	podmět
osobním	osobní	k2eAgNnSc7d1	osobní
zájmenem	zájmeno	k1gNnSc7	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
podmět	podmět	k1gInSc1	podmět
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vynechat	vynechat	k5eAaPmF	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neurčitém	určitý	k2eNgNnSc6d1	neurčité
časování	časování	k1gNnSc6	časování
navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
osobní	osobní	k2eAgFnSc1d1	osobní
koncovka	koncovka	k1gFnSc1	koncovka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vztah	vztah	k1gInSc1	vztah
já	já	k3xPp1nSc1	já
-	-	kIx~	-
tebe	ty	k3xPp2nSc4	ty
/	/	kIx~	/
vás	vy	k3xPp2nPc2	vy
<g/>
:	:	kIx,	:
utállak	utállak	k6eAd1	utállak
=	=	kIx~	=
nenávidím	návidět	k5eNaImIp1nS	návidět
tě	ty	k3xPp2nSc4	ty
szeretlek	szeretlek	k1gInSc1	szeretlek
=	=	kIx~	=
miluji	milovat	k5eAaImIp1nS	milovat
tě	ty	k3xPp2nSc4	ty
Infinitiv	infinitiv	k1gInSc1	infinitiv
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
koncovkou	koncovka	k1gFnSc7	koncovka
-ni	i	k?	-ni
<g/>
:	:	kIx,	:
olvasni	olvasnout	k5eAaPmRp2nS	olvasnout
=	=	kIx~	=
číst	číst	k5eAaImF	číst
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
časy	čas	k1gInPc4	čas
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
:	:	kIx,	:
minulý	minulý	k2eAgInSc4d1	minulý
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc4d1	přítomný
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc4d1	budoucí
<g/>
.	.	kIx.	.
</s>
<s>
Minulý	minulý	k2eAgInSc1d1	minulý
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
vložením	vložení	k1gNnSc7	vložení
sufixu	sufix	k1gInSc2	sufix
-t-	-	k?	-t-
(	(	kIx(	(
<g/>
s	s	k7c7	s
variantami	varianta	k1gFnPc7	varianta
-ott-	tt-	k?	-ott-
<g/>
,	,	kIx,	,
-ött-	-ött-	k?	-ött-
<g/>
,	,	kIx,	,
-ett-	tt-	k?	-ett-
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
:	:	kIx,	:
szaladtunk	szaladtunk	k6eAd1	szaladtunk
=	=	kIx~	=
běželi	běžet	k5eAaImAgMnP	běžet
jsme	být	k5eAaImIp1nP	být
Budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pomocným	pomocný	k2eAgNnSc7d1	pomocné
slovesem	sloveso	k1gNnSc7	sloveso
fog	fog	k?	fog
+	+	kIx~	+
infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
:	:	kIx,	:
fogunk	fogunk	k6eAd1	fogunk
menni	menen	k2eAgMnPc1d1	menen
=	=	kIx~	=
půjdeme	jít	k5eAaImIp1nP	jít
Odborně	odborně	k6eAd1	odborně
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
mluvnice	mluvnice	k1gFnPc1	mluvnice
však	však	k9	však
nepočítají	počítat	k5eNaImIp3nP	počítat
tuto	tento	k3xDgFnSc4	tento
konstrukci	konstrukce	k1gFnSc4	konstrukce
mezi	mezi	k7c7	mezi
časy	čas	k1gInPc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
skládáním	skládání	k1gNnSc7	skládání
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
budoucnost	budoucnost	k1gFnSc1	budoucnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
přítomným	přítomný	k2eAgInSc7d1	přítomný
časem	čas	k1gInSc7	čas
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
příslovečným	příslovečný	k2eAgNnSc7d1	příslovečné
určením	určení	k1gNnSc7	určení
času	čas	k1gInSc2	čas
<g/>
:	:	kIx,	:
Ma	Ma	k1gMnSc1	Ma
este	est	k1gFnSc2	est
moziba	moziba	k1gMnSc1	moziba
megyek	megyek	k1gMnSc1	megyek
<g/>
.	.	kIx.	.
=	=	kIx~	=
Dnes	dnes	k6eAd1	dnes
večer	večer	k6eAd1	večer
jdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc4d1	jediné
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
sloveso	sloveso	k1gNnSc4	sloveso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
tvary	tvar	k1gInPc4	tvar
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
existenciální	existenciální	k2eAgNnSc1d1	existenciální
sloveso	sloveso	k1gNnSc1	sloveso
lenni	leneň	k1gFnSc3	leneň
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tvary	tvar	k1gInPc4	tvar
se	s	k7c7	s
základem	základ	k1gInSc7	základ
lesz	lesza	k1gFnPc2	lesza
(	(	kIx(	(
<g/>
bude	být	k5eAaImBp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
od	od	k7c2	od
základu	základ	k1gInSc2	základ
van	vana	k1gFnPc2	vana
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovesné	slovesný	k2eAgInPc1d1	slovesný
způsoby	způsob	k1gInPc1	způsob
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
tři	tři	k4xCgInPc4	tři
<g/>
:	:	kIx,	:
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
(	(	kIx(	(
<g/>
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
)	)	kIx)	)
a	a	k8xC	a
spojovací	spojovací	k2eAgInSc1d1	spojovací
(	(	kIx(	(
<g/>
subjunktiv	subjunktiv	k1gInSc1	subjunktiv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kondicionál	kondicionál	k1gInSc1	kondicionál
přítomný	přítomný	k2eAgMnSc1d1	přítomný
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
vložením	vložení	k1gNnSc7	vložení
sufixu	sufix	k1gInSc2	sufix
-ná-	á-	k?	-ná-
/	/	kIx~	/
-né-	é-	k?	-né-
mezi	mezi	k7c4	mezi
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
:	:	kIx,	:
olvasnánk	olvasnánk	k6eAd1	olvasnánk
=	=	kIx~	=
četli	číst	k5eAaImAgMnP	číst
bychom	by	kYmCp1nP	by
Kondicionál	kondicionál	k1gInSc1	kondicionál
minulý	minulý	k2eAgInSc1d1	minulý
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
spojením	spojení	k1gNnSc7	spojení
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
a	a	k8xC	a
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
volna	volno	k1gNnSc2	volno
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
olvastunk	olvastunk	k1gInSc1	olvastunk
volna	volno	k1gNnSc2	volno
=	=	kIx~	=
byli	být	k5eAaImAgMnP	být
bychom	by	kYmCp1nP	by
četli	číst	k5eAaImAgMnP	číst
Subjunktiv	Subjunktiv	k1gInSc4	Subjunktiv
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
nejčastěji	často	k6eAd3	často
rozkaz	rozkaz	k1gInSc1	rozkaz
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přání	přání	k1gNnSc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgNnSc4d2	širší
využití	využití	k1gNnSc4	využití
než	než	k8xS	než
rozkazovací	rozkazovací	k2eAgInSc4d1	rozkazovací
způsob	způsob	k1gInSc4	způsob
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
tvary	tvar	k1gInPc4	tvar
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
osobách	osoba	k1gFnPc6	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
sufix	sufix	k1gInSc1	sufix
-j-	-	k?	-j-
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
mezi	mezi	k7c4	mezi
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
koncovku	koncovka	k1gFnSc4	koncovka
<g/>
:	:	kIx,	:
menjetek	menjetek	k1gInSc1	menjetek
<g/>
!	!	kIx.	!
</s>
<s>
=	=	kIx~	=
jděte	jít	k5eAaImRp2nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
menjen	menjna	k1gFnPc2	menjna
=	=	kIx~	=
ať	ať	k9	ať
jde	jít	k5eAaImIp3nS	jít
/	/	kIx~	/
jděte	jít	k5eAaImRp2nP	jít
(	(	kIx(	(
<g/>
vykání	vykání	k1gNnSc3	vykání
<g/>
)	)	kIx)	)
Při	při	k7c6	při
asimilaci	asimilace	k1gFnSc6	asimilace
souhlásek	souhláska	k1gFnPc2	souhláska
někdy	někdy	k6eAd1	někdy
toto	tento	k3xDgNnSc1	tento
-j-	-	k?	-j-
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
:	:	kIx,	:
olvass	olvass	k6eAd1	olvass
<g/>
!	!	kIx.	!
</s>
<s>
=	=	kIx~	=
čti	číst	k5eAaImRp2nS	číst
<g/>
!	!	kIx.	!
</s>
<s>
Potenciál	potenciál	k1gInSc1	potenciál
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-hat	ata	k1gFnPc2	-hata
/	/	kIx~	/
-het	et	k1gInSc1	-het
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
možnost	možnost	k1gFnSc4	možnost
<g/>
:	:	kIx,	:
olvashat	olvashat	k5eAaBmF	olvashat
=	=	kIx~	=
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
Několik	několik	k4yIc1	několik
maďarských	maďarský	k2eAgNnPc2d1	Maďarské
sloves	sloveso	k1gNnPc2	sloveso
(	(	kIx(	(
<g/>
vesměs	vesměs	k6eAd1	vesměs
velmi	velmi	k6eAd1	velmi
frekventovaných	frekventovaný	k2eAgInPc2d1	frekventovaný
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
časuje	časovat	k5eAaBmIp3nS	časovat
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
se	se	k3xPyFc4	se
však	však	k9	však
časují	časovat	k5eAaBmIp3nP	časovat
vzájemně	vzájemně	k6eAd1	vzájemně
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
uveden	uveden	k2eAgInSc4d1	uveden
infinitiv	infinitiv	k1gInSc4	infinitiv
<g/>
,	,	kIx,	,
oznam	oznámit	k5eAaPmRp2nS	oznámit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg	sg	k?	sg
neurč	neurč	k1gInSc1	neurč
<g/>
.	.	kIx.	.
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
minulý	minulý	k2eAgInSc1d1	minulý
<g/>
,	,	kIx,	,
rozk	rozk	k1gInSc1	rozk
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg	sg	k?	sg
(	(	kIx(	(
<g/>
neurč	neurč	k1gMnSc1	neurč
<g/>
.	.	kIx.	.
a	a	k8xC	a
urč	urč	k?	urč
<g/>
.	.	kIx.	.
<g/>
))	))	k?	))
<g/>
:	:	kIx,	:
aludni	aludnout	k5eAaPmRp2nS	aludnout
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
spát	spát	k5eAaImF	spát
<g/>
)	)	kIx)	)
-	-	kIx~	-
alszik	alszik	k1gMnSc1	alszik
<g/>
,	,	kIx,	,
aludt	aludt	k1gMnSc1	aludt
<g/>
,	,	kIx,	,
aludj	aludj	k1gMnSc1	aludj
/	/	kIx~	/
aludjál	aludjál	k1gMnSc1	aludjál
<g/>
,	,	kIx,	,
aludd	aludd	k1gMnSc1	aludd
/	/	kIx~	/
aludjad	aludjad	k1gInSc1	aludjad
<g/>
;	;	kIx,	;
analogicky	analogicky	k6eAd1	analogicky
se	se	k3xPyFc4	se
časují	časovat	k5eAaBmIp3nP	časovat
feküdni	feküdnout	k5eAaPmRp2nS	feküdnout
(	(	kIx(	(
<g/>
ležet	ležet	k5eAaImF	ležet
<g/>
)	)	kIx)	)
a	a	k8xC	a
nyugodni	nyugodnit	k5eAaPmRp2nS	nyugodnit
(	(	kIx(	(
<g/>
odpočívat	odpočívat	k5eAaImF	odpočívat
<g/>
)	)	kIx)	)
enni	enni	k6eAd1	enni
(	(	kIx(	(
<g/>
jíst	jíst	k5eAaImF	jíst
<g/>
)	)	kIx)	)
-	-	kIx~	-
eszik	eszik	k1gMnSc1	eszik
<g/>
,	,	kIx,	,
evett	evett	k1gMnSc1	evett
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
egyél	egyél	k1gMnSc1	egyél
<g/>
,	,	kIx,	,
edd	edd	k?	edd
/	/	kIx~	/
egyed	egyed	k1gInSc1	egyed
hinni	hineň	k1gFnSc3	hineň
(	(	kIx(	(
<g/>
myslet	myslet	k5eAaImF	myslet
si	se	k3xPyFc3	se
<g/>
)	)	kIx)	)
-	-	kIx~	-
hisz	hisz	k1gMnSc1	hisz
<g/>
,	,	kIx,	,
hitt	hitt	k1gMnSc1	hitt
<g/>
,	,	kIx,	,
higgyél	higgyél	k1gMnSc1	higgyél
<g/>
,	,	kIx,	,
hidd	hidd	k1gMnSc1	hidd
/	/	kIx~	/
higgyed	higgyed	k1gMnSc1	higgyed
inni	inn	k1gFnSc2	inn
(	(	kIx(	(
<g/>
pít	pít	k5eAaImF	pít
<g/>
)	)	kIx)	)
-	-	kIx~	-
iszik	iszik	k1gMnSc1	iszik
<g/>
,	,	kIx,	,
ivott	ivott	k1gMnSc1	ivott
<g/>
,	,	kIx,	,
igyál	igyál	k1gMnSc1	igyál
<g/>
,	,	kIx,	,
idd	idd	k?	idd
/	/	kIx~	/
igyad	igyad	k6eAd1	igyad
jönni	jöneň	k1gFnSc3	jöneň
(	(	kIx(	(
<g/>
přijít	přijít	k5eAaPmF	přijít
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
jön	jön	k?	jön
<g/>
,	,	kIx,	,
jött	jött	k1gInSc1	jött
<g/>
,	,	kIx,	,
gyere	gyer	k1gMnSc5	gyer
/	/	kIx~	/
jöjj	jöjj	k1gInSc1	jöjj
lenni	leneň	k1gFnSc3	leneň
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
-	-	kIx~	-
van	van	k1gInSc1	van
<g/>
,	,	kIx,	,
volt	volt	k1gInSc1	volt
<g/>
,	,	kIx,	,
légy	léga	k1gFnPc1	léga
/	/	kIx~	/
legyél	legyél	k1gInSc1	legyél
menni	meneň	k1gFnSc3	meneň
(	(	kIx(	(
<g/>
jít	jít	k5eAaImF	jít
<g/>
)	)	kIx)	)
-	-	kIx~	-
megy	mega	k1gFnSc2	mega
<g/>
,	,	kIx,	,
ment	menta	k1gFnPc2	menta
<g/>
,	,	kIx,	,
menj	menj	k1gFnSc1	menj
/	/	kIx~	/
menjél	menjél	k1gInSc1	menjél
nőni	nőni	k1gNnSc1	nőni
(	(	kIx(	(
<g/>
růst	růst	k1gInSc1	růst
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
nő	nő	k?	nő
<g/>
,	,	kIx,	,
nőtt	nőtt	k1gMnSc1	nőtt
<g/>
,	,	kIx,	,
nőj	nőj	k?	nőj
/	/	kIx~	/
nőjél	nőjél	k1gMnSc1	nőjél
<g/>
,	,	kIx,	,
nődd	nődd	k1gMnSc1	nődd
/	/	kIx~	/
nőjed	nőjed	k1gMnSc1	nőjed
<g/>
;	;	kIx,	;
analogicky	analogicky	k6eAd1	analogicky
főni	főni	k6eAd1	főni
(	(	kIx(	(
<g/>
vařit	vařit	k5eAaImF	vařit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lőni	lőni	k6eAd1	lőni
(	(	kIx(	(
<g/>
střílet	střílet	k5eAaImF	střílet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
róni	róni	k1gNnSc1	róni
(	(	kIx(	(
<g/>
rýt	rýt	k1gInSc1	rýt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
szőni	szőeň	k1gFnSc6	szőeň
(	(	kIx(	(
<g/>
tkát	tkát	k5eAaImF	tkát
<g/>
)	)	kIx)	)
tenni	teneň	k1gFnSc3	teneň
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
dát	dát	k5eAaPmF	dát
<g/>
)	)	kIx)	)
-	-	kIx~	-
tesz	tesz	k1gInSc1	tesz
<g/>
,	,	kIx,	,
tett	tett	k1gInSc1	tett
<g/>
,	,	kIx,	,
tégy	tégy	k1gInPc1	tégy
/	/	kIx~	/
tegyél	tegyél	k1gMnSc1	tegyél
<g/>
,	,	kIx,	,
tedd	tedd	k1gMnSc1	tedd
/	/	kIx~	/
tegyed	tegyed	k1gMnSc1	tegyed
<g/>
;	;	kIx,	;
analogicky	analogicky	k6eAd1	analogicky
venni	veneň	k1gFnSc3	veneň
(	(	kIx(	(
<g/>
vzít	vzít	k5eAaPmF	vzít
<g/>
)	)	kIx)	)
a	a	k8xC	a
vinni	vinen	k2eAgMnPc1d1	vinen
(	(	kIx(	(
<g/>
nést	nést	k5eAaImF	nést
<g/>
)	)	kIx)	)
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
iková	ikovat	k5eAaImIp3nS	ikovat
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgNnPc4d1	nazvané
takto	takto	k6eAd1	takto
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
tvaru	tvar	k1gInSc2	tvar
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg	sg	k?	sg
oznam	oznámit	k5eAaPmRp2nS	oznámit
<g/>
.	.	kIx.	.
neurč	neurč	k1gInSc1	neurč
<g/>
.	.	kIx.	.
přít	přít	k5eAaImF	přít
<g/>
.	.	kIx.	.
zakončeného	zakončený	k2eAgInSc2d1	zakončený
na	na	k7c4	na
-ik	k	k?	-ik
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
maďarských	maďarský	k2eAgNnPc2d1	Maďarské
sloves	sloveso	k1gNnPc2	sloveso
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
tvar	tvar	k1gInSc4	tvar
současně	současně	k6eAd1	současně
kořenem	kořen	k1gInSc7	kořen
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
bez	bez	k7c2	bez
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iková	Iková	k1gFnSc1	Iková
slovesa	sloveso	k1gNnSc2	sloveso
zpravidla	zpravidla	k6eAd1	zpravidla
tvoří	tvořit	k5eAaImIp3nS	tvořit
odlišně	odlišně	k6eAd1	odlišně
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
tvar	tvar	k1gInSc1	tvar
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg	sg	k?	sg
(	(	kIx(	(
<g/>
zakončené	zakončený	k2eAgNnSc1d1	zakončené
jednotně	jednotně	k6eAd1	jednotně
na	na	k7c6	na
-ék	-ék	k?	-ék
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
význam	význam	k1gInSc4	význam
podobný	podobný	k2eAgInSc4d1	podobný
českým	český	k2eAgInSc7d1	český
zvratným	zvratný	k2eAgNnPc3d1	zvratné
slovesům	sloveso	k1gNnPc3	sloveso
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podmět	podmět	k1gInSc1	podmět
koná	konat	k5eAaImIp3nS	konat
sám	sám	k3xTgInSc1	sám
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
iková	ikový	k2eAgNnPc4d1	ikový
slovesa	sloveso	k1gNnPc4	sloveso
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
nepřechodná	přechodný	k2eNgFnSc1d1	nepřechodná
<g/>
.	.	kIx.	.
</s>
<s>
Postpozice	postpozice	k1gFnSc1	postpozice
(	(	kIx(	(
<g/>
záložky	záložek	k1gInPc1	záložek
<g/>
)	)	kIx)	)
plní	plnit	k5eAaImIp3nP	plnit
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
předložky	předložka	k1gFnPc4	předložka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stojí	stát	k5eAaImIp3nS	stát
až	až	k9	až
za	za	k7c7	za
řídícím	řídící	k2eAgNnSc7d1	řídící
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	s	k7c7	s
základním	základní	k2eAgInSc7d1	základní
pádem	pád	k1gInSc7	pád
(	(	kIx(	(
<g/>
nominativem	nominativ	k1gInSc7	nominativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fungují	fungovat	k5eAaImIp3nP	fungovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Píší	psát	k5eAaImIp3nP	psát
se	se	k3xPyFc4	se
však	však	k9	však
odděleně	odděleně	k6eAd1	odděleně
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
varianty	varianta	k1gFnPc4	varianta
podle	podle	k7c2	podle
samohláskové	samohláskový	k2eAgFnSc2d1	samohlásková
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Postpozice	postpozice	k1gFnPc1	postpozice
vyjadřující	vyjadřující	k2eAgInPc4d1	vyjadřující
místní	místní	k2eAgInPc4d1	místní
vztahy	vztah	k1gInPc4	vztah
tvoří	tvořit	k5eAaImIp3nS	tvořit
trojice	trojice	k1gFnSc1	trojice
jako	jako	k8xS	jako
místní	místní	k2eAgInPc1d1	místní
pády	pád	k1gInPc1	pád
<g/>
:	:	kIx,	:
ágy	ágy	k?	ágy
alá	alá	k?	alá
-	-	kIx~	-
ágy	ágy	k?	ágy
alatt	alatt	k1gInSc1	alatt
-	-	kIx~	-
ágy	ágy	k?	ágy
alól	alól	k1gInSc1	alól
=	=	kIx~	=
pod	pod	k7c4	pod
postel	postel	k1gInSc4	postel
-	-	kIx~	-
pod	pod	k7c7	pod
postelí	postel	k1gFnSc7	postel
-	-	kIx~	-
zpod	zpod	k7c2	zpod
postele	postel	k1gFnSc2	postel
ágy	ágy	k?	ágy
mellé	mellý	k2eAgNnSc1d1	mellý
-	-	kIx~	-
ágy	ágy	k?	ágy
mellett	mellett	k1gInSc1	mellett
-	-	kIx~	-
ágy	ágy	k?	ágy
mellől	mellől	k1gInSc1	mellől
=	=	kIx~	=
vedle	vedle	k7c2	vedle
postel	postel	k1gFnSc1	postel
-	-	kIx~	-
vedle	vedle	k7c2	vedle
postele	postel	k1gFnSc2	postel
-	-	kIx~	-
od	od	k7c2	od
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
<g/>
)	)	kIx)	)
postele	postel	k1gFnSc2	postel
<g />
.	.	kIx.	.
</s>
<s>
ágy	ágy	k?	ágy
elé	elé	k?	elé
-	-	kIx~	-
ágy	ágy	k?	ágy
előtt	előtt	k1gInSc1	előtt
-	-	kIx~	-
ágy	ágy	k?	ágy
elől	elől	k1gInSc1	elől
=	=	kIx~	=
před	před	k7c4	před
postel	postel	k1gInSc4	postel
-	-	kIx~	-
před	před	k7c7	před
postelí	postel	k1gFnSc7	postel
-	-	kIx~	-
zpřed	zpřed	k6eAd1	zpřed
postele	postlat	k5eAaPmIp3nS	postlat
ágy	ágy	k?	ágy
fölé	fölé	k1gNnSc1	fölé
-	-	kIx~	-
ágy	ágy	k?	ágy
felett	felett	k1gInSc1	felett
/	/	kIx~	/
fölött	fölött	k1gInSc1	fölött
-	-	kIx~	-
ágy	ágy	k?	ágy
fölül	fölül	k1gInSc1	fölül
=	=	kIx~	=
nad	nad	k7c4	nad
postel	postel	k1gInSc4	postel
-	-	kIx~	-
nad	nad	k7c7	nad
postelí	postel	k1gFnSc7	postel
-	-	kIx~	-
zponad	zponad	k7c2	zponad
postele	postel	k1gFnSc2	postel
ágy	ágy	k?	ágy
mögé	mögá	k1gFnSc2	mögá
-	-	kIx~	-
ágy	ágy	k?	ágy
mögött	mögött	k1gInSc1	mögött
-	-	kIx~	-
ágy	ágy	k?	ágy
mögül	mögül	k1gInSc1	mögül
=	=	kIx~	=
za	za	k7c4	za
postel	postel	k1gInSc4	postel
-	-	kIx~	-
za	za	k7c7	za
postelí	postel	k1gFnSc7	postel
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zpoza	zpoza	k7c2	zpoza
postele	postel	k1gFnSc2	postel
ágyak	ágyak	k6eAd1	ágyak
közé	közá	k1gFnSc2	közá
-	-	kIx~	-
ágyak	ágyak	k1gMnSc1	ágyak
között	között	k1gMnSc1	között
-	-	kIx~	-
ágyak	ágyak	k1gMnSc1	ágyak
közül	közül	k1gMnSc1	közül
=	=	kIx~	=
mezi	mezi	k7c4	mezi
postele	postel	k1gFnPc4	postel
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
-	-	kIx~	-
mezi	mezi	k7c7	mezi
postelemi	postel	k1gFnPc7	postel
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
<g/>
)	)	kIx)	)
-	-	kIx~	-
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
mezi	mezi	k7c7	mezi
postelemi	postel	k1gFnPc7	postel
Další	další	k2eAgFnPc1d1	další
záložky	záložka	k1gFnPc1	záložka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
körül	körül	k1gInSc1	körül
=	=	kIx~	=
kolem	kolem	k6eAd1	kolem
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
köré	köré	k6eAd1	köré
=	=	kIx~	=
kolem	kolem	k7c2	kolem
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
felől	felől	k1gInSc1	felől
=	=	kIx~	=
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
<g/>
,	,	kIx,	,
felé	felé	k1gNnSc7	felé
=	=	kIx~	=
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
<g/>
,	,	kIx,	,
na	na	k7c4	na
<g/>
,	,	kIx,	,
helyett	helyett	k1gInSc1	helyett
=	=	kIx~	=
místo	místo	k1gNnSc1	místo
(	(	kIx(	(
<g/>
čeho	co	k3yRnSc2	co
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
szerint	szerint	k1gMnSc1	szerint
=	=	kIx~	=
podle	podle	k7c2	podle
(	(	kIx(	(
<g/>
čeho	co	k3yRnSc2	co
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
miatt	miatt	k1gMnSc1	miatt
=	=	kIx~	=
kvůli	kvůli	k7c3	kvůli
<g/>
,	,	kIx,	,
szerint	szerinta	k1gFnPc2	szerinta
=	=	kIx~	=
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
čeho	co	k3yRnSc2	co
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
után	után	k1gMnSc1	után
=	=	kIx~	=
po	po	k7c6	po
<g/>
,	,	kIx,	,
za	za	k7c7	za
<g/>
,	,	kIx,	,
ellen	ellen	k2eAgMnSc1d1	ellen
=	=	kIx~	=
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
által	által	k1gInSc4	által
=	=	kIx~	=
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
<g/>
,	,	kIx,	,
iránt	iránt	k1gInSc1	iránt
=	=	kIx~	=
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
<g/>
,	,	kIx,	,
nélkül	nélküla	k1gFnPc2	nélküla
=	=	kIx~	=
bez	bez	k7c2	bez
Pokud	pokud	k6eAd1	pokud
se	se	k3xPyFc4	se
postpozice	postpozice	k1gFnSc1	postpozice
pojí	pojíst	k5eAaPmIp3nS	pojíst
s	s	k7c7	s
osobními	osobní	k2eAgNnPc7d1	osobní
zájmeny	zájmeno	k1gNnPc7	zájmeno
<g/>
,	,	kIx,	,
přijímají	přijímat	k5eAaImIp3nP	přijímat
osobní	osobní	k2eAgFnPc1d1	osobní
koncovky	koncovka	k1gFnPc1	koncovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
přivlastňovacími	přivlastňovací	k2eAgInPc7d1	přivlastňovací
<g/>
:	:	kIx,	:
alattam	alattam	k1gInSc1	alattam
=	=	kIx~	=
pode	pod	k7c7	pod
mnou	já	k3xPp1nSc7	já
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
én	én	k?	én
alatt	alatt	k1gInSc1	alatt
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
fölötted	fölötted	k1gInSc1	fölötted
=	=	kIx~	=
nad	nad	k7c7	nad
tebou	ty	k3xPp2nSc7	ty
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
záporkou	záporka	k1gFnSc7	záporka
je	být	k5eAaImIp3nS	být
nem	nem	k?	nem
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
sem	sem	k6eAd1	sem
(	(	kIx(	(
<g/>
také	také	k9	také
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
negovaným	negovaný	k2eAgInSc7d1	negovaný
výrazem	výraz	k1gInSc7	výraz
<g/>
:	:	kIx,	:
nem	nem	k?	nem
értem	értem	k1gInSc1	értem
=	=	kIx~	=
nerozumím	rozumět	k5eNaImIp1nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subjunktivu	subjunktivum	k1gNnSc6	subjunktivum
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
nem	nem	k?	nem
používá	používat	k5eAaImIp3nS	používat
ne	ne	k9	ne
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
ne	ne	k9	ne
gyere	gyer	k1gInSc5	gyer
ide	ide	k?	ide
<g/>
!	!	kIx.	!
</s>
<s>
=	=	kIx~	=
nechoď	chodit	k5eNaImRp2nS	chodit
sem	sem	k6eAd1	sem
<g/>
!	!	kIx.	!
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
možný	možný	k2eAgInSc1d1	možný
vícenásobný	vícenásobný	k2eAgInSc1d1	vícenásobný
zápor	zápor	k1gInSc1	zápor
<g/>
:	:	kIx,	:
Nem	Nem	k1gFnSc1	Nem
értek	értek	k1gMnSc1	értek
semmit	semmit	k1gInSc1	semmit
<g/>
.	.	kIx.	.
=	=	kIx~	=
Nerozumím	rozumět	k5eNaImIp1nS	rozumět
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
maďarské	maďarský	k2eAgFnSc2d1	maďarská
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
nahodilý	nahodilý	k2eAgMnSc1d1	nahodilý
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
slovosled	slovosled	k1gInSc1	slovosled
oznamovací	oznamovací	k2eAgFnSc2d1	oznamovací
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
SOV	sova	k1gFnPc2	sova
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
-	-	kIx~	-
předmět	předmět	k1gInSc1	předmět
-	-	kIx~	-
přísudek	přísudek	k1gInSc1	přísudek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větný	větný	k2eAgMnSc1d1	větný
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
kladen	kladen	k2eAgInSc1d1	kladen
důraz	důraz	k1gInSc1	důraz
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
stojí	stát	k5eAaImIp3nS	stát
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
přísudkem	přísudek	k1gInSc7	přísudek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tázacích	tázací	k2eAgFnPc6d1	tázací
větách	věta	k1gFnPc6	věta
bývá	bývat	k5eAaImIp3nS	bývat
obecně	obecně	k6eAd1	obecně
jádro	jádro	k1gNnSc1	jádro
dotazu	dotaz	k1gInSc2	dotaz
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větách	věta	k1gFnPc6	věta
s	s	k7c7	s
tázacím	tázací	k2eAgNnSc7d1	tázací
zájmenem	zájmeno	k1gNnSc7	zájmeno
(	(	kIx(	(
<g/>
doplňovacích	doplňovací	k2eAgInPc2d1	doplňovací
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
slovosled	slovosled	k1gInSc4	slovosled
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
<g/>
:	:	kIx,	:
Hol	hola	k1gFnPc2	hola
van	van	k1gInSc1	van
a	a	k8xC	a
kép	kép	k1gInSc1	kép
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
Kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
<g/>
)	)	kIx)	)
obraz	obraz	k1gInSc1	obraz
<g/>
?	?	kIx.	?
</s>
<s>
V	v	k7c6	v
tázacích	tázací	k2eAgFnPc6d1	tázací
větách	věta	k1gFnPc6	věta
zjišťovacích	zjišťovací	k2eAgInPc2d1	zjišťovací
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
češtině	čeština	k1gFnSc3	čeština
obrácený	obrácený	k2eAgInSc1d1	obrácený
<g/>
:	:	kIx,	:
Kép	kép	k1gInSc1	kép
ez	ez	k?	ez
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obraz	obraz	k1gInSc1	obraz
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
Obraz	obraz	k1gInSc1	obraz
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Itt	Itt	k1gFnSc6	Itt
van	vana	k1gFnPc2	vana
az	az	k?	az
a	a	k8xC	a
fiú	fiú	k0	fiú
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
Je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
chlapec	chlapec	k1gMnSc1	chlapec
tady	tady	k6eAd1	tady
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
Tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Slovosled	slovosled	k1gInSc4	slovosled
záporné	záporný	k2eAgFnSc2d1	záporná
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
:	:	kIx,	:
Ez	Ez	k1gFnSc1	Ez
nem	nem	k?	nem
kép	kép	k1gInSc1	kép
<g/>
.	.	kIx.	.
=	=	kIx~	=
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
maďarštinu	maďarština	k1gFnSc4	maďarština
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
většině	většina	k1gFnSc3	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
obrácené	obrácený	k2eAgNnSc1d1	obrácené
pořadí	pořadí	k1gNnSc1	pořadí
složek	složka	k1gFnPc2	složka
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hodností	hodnost	k1gFnPc2	hodnost
a	a	k8xC	a
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
např.	např.	kA	např.
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
maďarsky	maďarsky	k6eAd1	maďarsky
I.	I.	kA	I.
Károly	Károl	k1gInPc1	Károl
király	királa	k1gFnSc2	királa
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
I.	I.	kA	I.
Karel	Karel	k1gMnSc1	Karel
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
jmény	jméno	k1gNnPc7	jméno
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
László	László	k1gFnSc1	László
Kovács	Kovácsa	k1gFnPc2	Kovácsa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
vždy	vždy	k6eAd1	vždy
Kovács	Kovács	k1gInSc4	Kovács
László	László	k1gFnSc2	László
<g/>
.	.	kIx.	.
</s>
<s>
Obráceně	obráceně	k6eAd1	obráceně
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
i	i	k9	i
data	datum	k1gNnPc4	datum
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
maďarsky	maďarsky	k6eAd1	maďarsky
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
časové	časový	k2eAgFnPc1d1	časová
jednotky	jednotka	k1gFnPc1	jednotka
řazeny	řadit	k5eAaImNgFnP	řadit
od	od	k7c2	od
největší	veliký	k2eAgFnSc2d3	veliký
po	po	k7c4	po
nejmenší	malý	k2eAgInSc4d3	nejmenší
<g/>
.	.	kIx.	.
maďarský	maďarský	k2eAgInSc4d1	maďarský
<g/>
:	:	kIx,	:
magyar	magyar	k1gInSc4	magyar
/	/	kIx~	/
<g/>
mɑ	mɑ	k?	mɑ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
moďor	moďor	k1gInSc1	moďor
<g/>
]	]	kIx)	]
ahoj	ahoj	k0	ahoj
<g/>
:	:	kIx,	:
szia	szium	k1gNnSc2	szium
/	/	kIx~	/
<g/>
siɑ	siɑ	k?	siɑ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
sijo	sijo	k1gNnSc1	sijo
<g/>
]	]	kIx)	]
dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
:	:	kIx,	:
jó	jó	k0	jó
napot	napot	k1gInSc1	napot
(	(	kIx(	(
<g/>
kívánok	kívánok	k1gInSc1	kívánok
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
joː	joː	k?	joː
kivaː	kivaː	k?	kivaː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
jó	jó	k0	jó
nopot	nopot	k1gMnSc1	nopot
kívánok	kívánok	k1gInSc1	kívánok
<g/>
]	]	kIx)	]
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
:	:	kIx,	:
kérem	kérem	k6eAd1	kérem
(	(	kIx(	(
<g/>
szépen	szépen	k2eAgMnSc1d1	szépen
<g/>
)	)	kIx)	)
/	/	kIx~	/
<g/>
keː	keː	k?	keː
seː	seː	k?	seː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
kírem	kírem	k1gInSc1	kírem
sípen	sípna	k1gFnPc2	sípna
<g/>
]	]	kIx)	]
promiň	prominout	k5eAaPmRp2nS	prominout
<g/>
(	(	kIx(	(
<g/>
te	te	k?	te
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bocsánat	bocsánat	k5eAaPmF	bocsánat
/	/	kIx~	/
<g/>
botʃ	botʃ	k?	botʃ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
bočánot	bočánota	k1gFnPc2	bočánota
<g/>
]	]	kIx)	]
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
:	:	kIx,	:
köszönöm	köszönö	k1gNnSc7	köszönö
/	/	kIx~	/
<g/>
kø	kø	k?	kø
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
kösönöm	kösönöm	k6eAd1	kösönöm
<g/>
]	]	kIx)	]
toto	tento	k3xDgNnSc1	tento
/	/	kIx~	/
tohle	tenhle	k3xDgNnSc4	tenhle
<g/>
:	:	kIx,	:
az	az	k?	az
/	/	kIx~	/
<g/>
ɑ	ɑ	k?	ɑ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
ez	ez	k?	ez
/	/	kIx~	/
<g/>
ɛ	ɛ	k?	ɛ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
ez	ez	k?	ez
<g/>
]	]	kIx)	]
kolik	kolika	k1gFnPc2	kolika
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
mennyi	menny	k1gInSc3	menny
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
<g/>
mɛ	mɛ	k?	mɛ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
meňňi	meňňi	k6eAd1	meňňi
<g/>
]	]	kIx)	]
kolik	kolik	k9	kolik
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
mennyibe	mennyibat	k5eAaPmIp3nS	mennyibat
kerül	kerül	k1gMnSc1	kerül
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
<g/>
mɛ	mɛ	k?	mɛ
kɛ	kɛ	k?	kɛ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
meňňibe	meňňibat	k5eAaPmIp3nS	meňňibat
kerül	kerül	k1gMnSc1	kerül
<g/>
]	]	kIx)	]
ano	ano	k9	ano
<g/>
:	:	kIx,	:
igen	igen	k1gInSc1	igen
/	/	kIx~	/
<g/>
iɡ	iɡ	k?	iɡ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
igen	igen	k1gInSc1	igen
<g/>
]	]	kIx)	]
ne	ne	k9	ne
<g/>
:	:	kIx,	:
nem	nem	k?	nem
/	/	kIx~	/
<g/>
nɛ	nɛ	k?	nɛ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
nem	nem	k?	nem
<g/>
]	]	kIx)	]
nerozumím	rozumět	k5eNaImIp1nS	rozumět
<g/>
:	:	kIx,	:
nem	nem	k?	nem
értem	értem	k1gInSc1	értem
/	/	kIx~	/
<g/>
nɛ	nɛ	k?	nɛ
eː	eː	k?	eː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
nem	nem	k?	nem
írtem	írtem	k1gInSc1	írtem
<g/>
]	]	kIx)	]
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
:	:	kIx,	:
nem	nem	k?	nem
tudom	tudom	k1gInSc1	tudom
/	/	kIx~	/
<g/>
nɛ	nɛ	k?	nɛ
tudom	tudom	k1gInSc1	tudom
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
nem	nem	k?	nem
tudom	tudom	k1gInSc1	tudom
<g/>
]	]	kIx)	]
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
WC	WC	kA	WC
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
Hol	holit	k5eAaImRp2nS	holit
van	van	k1gInSc1	van
a	a	k8xC	a
vécé	vécé	k1gNnSc1	vécé
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
<g/>
hol	hola	k1gFnPc2	hola
vɑ	vɑ	k?	vɑ
ɑ	ɑ	k?	ɑ
veː	veː	k?	veː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
hol	holit	k5eAaImRp2nS	holit
von	von	k1gInSc4	von
o	o	k7c6	o
vící	vící	k1gFnSc6	vící
<g/>
]	]	kIx)	]
džus	džus	k1gInSc4	džus
<g/>
:	:	kIx,	:
gyümölcslé	gyümölcslý	k2eAgInPc4d1	gyümölcslý
/	/	kIx~	/
<g/>
ɟ	ɟ	k?	ɟ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
ďümölčlí	ďümölčlí	k1gNnSc1	ďümölčlí
<g/>
]	]	kIx)	]
voda	voda	k1gFnSc1	voda
<g/>
:	:	kIx,	:
víz	vízo	k1gNnPc2	vízo
/	/	kIx~	/
<g/>
viː	viː	k?	viː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
víz	vízo	k1gNnPc2	vízo
<g/>
]	]	kIx)	]
víno	víno	k1gNnSc1	víno
<g/>
:	:	kIx,	:
bor	bor	k1gInSc1	bor
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
bor	bor	k1gInSc1	bor
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
bor	bor	k1gInSc1	bor
<g/>
]	]	kIx)	]
pivo	pivo	k1gNnSc1	pivo
<g/>
:	:	kIx,	:
sör	sör	k?	sör
/	/	kIx~	/
<g/>
ʃ	ʃ	k?	ʃ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
šör	šör	k?	šör
<g/>
]	]	kIx)	]
čaj	čaj	k1gInSc1	čaj
<g/>
:	:	kIx,	:
tea	tea	k1gFnSc1	tea
/	/	kIx~	/
<g/>
tɛ	tɛ	k?	tɛ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
tejo	tejo	k1gNnSc1	tejo
<g/>
]	]	kIx)	]
mléko	mléko	k1gNnSc1	mléko
<g/>
:	:	kIx,	:
tej	tej	k?	tej
/	/	kIx~	/
<g/>
tej	tej	k?	tej
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
tej	tej	k?	tej
<g/>
]	]	kIx)	]
mluvíte	mluvit	k5eAaImIp2nP	mluvit
anglicky	anglicky	k6eAd1	anglicky
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
Beszél	Beszélum	k1gNnPc2	Beszélum
angolul	angolula	k1gFnPc2	angolula
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
<g/>
besél	besélit	k5eAaPmRp2nS	besélit
ɑ	ɑ	k?	ɑ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
besíl	besílit	k5eAaPmRp2nS	besílit
ongolul	ongolul	k1gInSc1	ongolul
<g/>
]	]	kIx)	]
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
Segítség	Segítséga	k1gFnPc2	Segítséga
<g/>
!	!	kIx.	!
</s>
<s>
/	/	kIx~	/
<g/>
ʃ	ʃ	k?	ʃ
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
šegíčíg	šegíčíg	k1gInSc4	šegíčíg
<g/>
]	]	kIx)	]
pracuj	pracovat	k5eAaImRp2nS	pracovat
<g/>
!	!	kIx.	!
</s>
<s>
dolgozz	dolgozz	k1gInSc1	dolgozz
/	/	kIx~	/
<g/>
dolgozː	dolgozː	k?	dolgozː
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
dolgozz	dolgozz	k1gInSc1	dolgozz
<g/>
]	]	kIx)	]
</s>
