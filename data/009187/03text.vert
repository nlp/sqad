<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Strapáková	Strapáková	k1gFnSc1	Strapáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1973	[number]	k4	1973
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
advokátka	advokátka	k1gFnSc1	advokátka
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
aktivistka	aktivistka	k1gFnSc1	aktivistka
a	a	k8xC	a
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
prezidentka	prezidentka	k1gFnSc1	prezidentka
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
45	[number]	k4	45
letech	léto	k1gNnPc6	léto
také	také	k6eAd1	také
jako	jako	k8xC	jako
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
osoba	osoba	k1gFnSc1	osoba
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
ústavní	ústavní	k2eAgFnSc6d1	ústavní
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2018	[number]	k4	2018
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
byla	být	k5eAaImAgFnS	být
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
hnutí	hnutí	k1gNnSc2	hnutí
Progresívne	Progresívne	k1gFnSc2	Progresívne
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
zvolena	zvolit	k5eAaPmNgFnS	zvolit
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
ustavující	ustavující	k2eAgFnSc6d1	ustavující
schůzi	schůze	k1gFnSc6	schůze
tohoto	tento	k3xDgInSc2	tento
subjektu	subjekt	k1gInSc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
neziskovém	ziskový	k2eNgInSc6d1	neziskový
sektoru	sektor	k1gInSc6	sektor
jako	jako	k9	jako
spolupracovnice	spolupracovnice	k1gFnSc1	spolupracovnice
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
VIA	via	k7c4	via
IURIS	IURIS	kA	IURIS
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
posílení	posílení	k1gNnSc1	posílení
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
a	a	k8xC	a
prosazování	prosazování	k1gNnSc2	prosazování
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
ve	v	k7c6	v
vybraných	vybraný	k2eAgFnPc6d1	vybraná
oblastech	oblast	k1gFnPc6	oblast
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
právnická	právnický	k2eAgFnSc1d1	právnická
kariéra	kariéra	k1gFnSc1	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
práv	právo	k1gNnPc2	právo
zakončila	zakončit	k5eAaPmAgFnS	zakončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
samosprávě	samospráva	k1gFnSc6	samospráva
v	v	k7c6	v
Pezinku	Pezink	k1gInSc6	Pezink
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xS	jako
asistentka	asistentka	k1gFnSc1	asistentka
na	na	k7c6	na
právním	právní	k2eAgNnSc6d1	právní
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jako	jako	k9	jako
zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
přednosty	přednosta	k1gMnSc2	přednosta
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
neziskovém	ziskový	k2eNgInSc6d1	neziskový
sektoru	sektor	k1gInSc6	sektor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správě	správa	k1gFnSc3	správa
a	a	k8xC	a
problematice	problematika	k1gFnSc3	problematika
týraných	týraný	k2eAgFnPc2d1	týraná
a	a	k8xC	a
zneužívaných	zneužívaný	k2eAgFnPc2d1	zneužívaná
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorkou	autorka	k1gFnSc7	autorka
a	a	k8xC	a
spoluautorkou	spoluautorka	k1gFnSc7	spoluautorka
několika	několik	k4yIc2	několik
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
členkou	členka	k1gFnSc7	členka
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sítě	síť	k1gFnSc2	síť
environmentálních	environmentální	k2eAgMnPc2d1	environmentální
právníků	právník	k1gMnPc2	právník
ELAW	ELAW	kA	ELAW
(	(	kIx(	(
<g/>
Environmental	Environmental	k1gMnSc1	Environmental
Law	Law	k1gMnSc2	Law
Alliance	Alliance	k1gFnSc2	Alliance
Worldwide	Worldwid	k1gInSc5	Worldwid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
právnička	právnička	k1gFnSc1	právnička
občanské	občanský	k2eAgFnSc2d1	občanská
iniciativy	iniciativa	k1gFnSc2	iniciativa
Skládka	skládka	k1gFnSc1	skládka
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
města	město	k1gNnSc2	město
vyznamenána	vyznamenán	k2eAgFnSc1d1	vyznamenána
Goldmanovou	Goldmanův	k2eAgFnSc7d1	Goldmanova
environmentální	environmentální	k2eAgFnSc7d1	environmentální
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
boj	boj	k1gInSc4	boj
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
skládky	skládka	k1gFnSc2	skládka
odpadu	odpad	k1gInSc2	odpad
v	v	k7c6	v
Pezinku	Pezink	k1gInSc6	Pezink
<g/>
.	.	kIx.	.
</s>
<s>
Problematice	problematika	k1gFnSc3	problematika
pezinské	pezinský	k2eAgFnSc2d1	pezinský
skládky	skládka	k1gFnSc2	skládka
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
věnovala	věnovat	k5eAaImAgFnS	věnovat
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
kontroverzním	kontroverzní	k2eAgMnSc7d1	kontroverzní
podnikatelem	podnikatel	k1gMnSc7	podnikatel
Mariánem	Marián	k1gMnSc7	Marián
Kočnerem	Kočner	k1gMnSc7	Kočner
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
novou	nový	k2eAgFnSc4d1	nová
skládku	skládka	k1gFnSc4	skládka
v	v	k7c6	v
Pezinku	Pezink	k1gInSc6	Pezink
založit	založit	k5eAaPmF	založit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2017	[number]	k4	2017
oznámila	oznámit	k5eAaPmAgFnS	oznámit
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
VIA	via	k7c4	via
IURIS	IURIS	kA	IURIS
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
advokátské	advokátský	k2eAgFnSc6d1	advokátská
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
advokacie	advokacie	k1gFnSc2	advokacie
plánovala	plánovat	k5eAaImAgFnS	plánovat
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
environmentálním	environmentální	k2eAgNnPc3d1	environmentální
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
dcerami	dcera	k1gFnPc7	dcera
v	v	k7c6	v
Pezinku	Pezink	k1gInSc6	Pezink
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
s	s	k7c7	s
Ivanem	Ivan	k1gMnSc7	Ivan
Čaputou	Čaputa	k1gFnSc7	Čaputa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2019	[number]	k4	2019
na	na	k7c6	na
facebookovém	facebookový	k2eAgInSc6d1	facebookový
účtu	účet	k1gInSc6	účet
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
naznačila	naznačit	k5eAaPmAgFnS	naznačit
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
hudebníkem	hudebník	k1gMnSc7	hudebník
<g/>
,	,	kIx,	,
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
<g/>
,	,	kIx,	,
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
překladatelem	překladatel	k1gMnSc7	překladatel
Peterem	Peter	k1gMnSc7	Peter
Konečným	Konečný	k1gMnSc7	Konečný
<g/>
,	,	kIx,	,
pocházejícím	pocházející	k2eAgMnSc7d1	pocházející
také	také	k9	také
z	z	k7c2	z
Pezinku	Pezink	k1gInSc2	Pezink
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ji	on	k3xPp3gFnSc4	on
následně	následně	k6eAd1	následně
začal	začít	k5eAaPmAgInS	začít
doprovázet	doprovázet	k5eAaImF	doprovázet
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
kampani	kampaň	k1gFnSc6	kampaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
Progresivního	progresivní	k2eAgNnSc2d1	progresivní
Slovenska	Slovensko	k1gNnSc2	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
oznámila	oznámit	k5eAaPmAgFnS	oznámit
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
politického	politický	k2eAgNnSc2d1	politické
hnutí	hnutí	k1gNnSc2	hnutí
Progresívne	Progresívne	k1gFnSc2	Progresívne
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
sněmu	sněm	k1gInSc6	sněm
zvolena	zvolit	k5eAaPmNgFnS	zvolit
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidatura	kandidatura	k1gFnSc1	kandidatura
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
post	post	k1gInSc4	post
prezidentky	prezidentka	k1gFnSc2	prezidentka
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
relativní	relativní	k2eAgFnSc4d1	relativní
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
40,57	[number]	k4	40,57
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Bratislavském	bratislavský	k2eAgInSc6d1	bratislavský
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
59,65	[number]	k4	59,65
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
Prešovském	prešovský	k2eAgInSc6d1	prešovský
kraji	kraj	k1gInSc6	kraj
(	(	kIx(	(
<g/>
30,29	[number]	k4	30,29
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
voleb	volba	k1gFnPc2	volba
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
postu	post	k1gInSc2	post
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
Progresívne	Progresívne	k1gFnSc2	Progresívne
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
s	s	k7c7	s
58	[number]	k4	58
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
protikandidátem	protikandidát	k1gMnSc7	protikandidát
Marošem	Maroš	k1gMnSc7	Maroš
Šefčovičem	Šefčovič	k1gMnSc7	Šefčovič
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
komisařem	komisař	k1gMnSc7	komisař
pro	pro	k7c4	pro
energetickou	energetický	k2eAgFnSc4d1	energetická
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VICIÁNOVÁ	VICIÁNOVÁ	kA	VICIÁNOVÁ
<g/>
,	,	kIx,	,
Katarína	Katarína	k1gFnSc1	Katarína
<g/>
;	;	kIx,	;
HOCHELOVÁ	HOCHELOVÁ	kA	HOCHELOVÁ
<g/>
,	,	kIx,	,
Mirina	Mirina	k1gFnSc1	Mirina
<g/>
;	;	kIx,	;
HRUBALA	HRUBALA	kA	HRUBALA
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
a	a	k8xC	a
ČAPUTOVÁ	ČAPUTOVÁ	kA	ČAPUTOVÁ
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
Týranie	Týranie	k1gFnSc1	Týranie
<g/>
,	,	kIx,	,
zneužívanie	zneužívanie	k1gFnSc1	zneužívanie
a	a	k8xC	a
zanedbávanie	zanedbávanie	k1gFnSc1	zanedbávanie
detí	detí	k1gFnSc1	detí
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
JASPIS	jaspis	k1gInSc1	jaspis
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
72	[number]	k4	72
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8085576260	[number]	k4	8085576260
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Čaputová	Čaputový	k2eAgFnSc1d1	Čaputová
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prehovoril	Prehovorit	k5eAaBmAgMnS	Prehovorit
partner	partner	k1gMnSc1	partner
Zuzany	Zuzana	k1gFnSc2	Zuzana
Čaputovej	Čaputovej	k1gMnSc1	Čaputovej
Peter	Peter	k1gMnSc1	Peter
Konečný	Konečný	k1gMnSc1	Konečný
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
'	'	kIx"	'
<g/>
Erin	Erin	k1gInSc1	Erin
Brockovich	Brockovich	k1gMnSc1	Brockovich
<g/>
'	'	kIx"	'
elected	elected	k1gMnSc1	elected
first	first	k1gMnSc1	first
female	female	k6eAd1	female
president	president	k1gMnSc1	president
<g/>
,	,	kIx,	,
in	in	k?	in
rebuke	rebuke	k1gInSc1	rebuke
of	of	k?	of
populism	populism	k1gInSc1	populism
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
