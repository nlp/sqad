<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1901	[number]	k4	1901
Žižkov	Žižkov	k1gInSc1	Žižkov
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
českého	český	k2eAgInSc2d1	český
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
poetismu	poetismus	k1gInSc2	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

