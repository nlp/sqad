<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
je	být	k5eAaImIp3nS	být
abstraktní	abstraktní	k2eAgInSc4d1	abstraktní
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
kvantitu	kvantita	k1gFnSc4	kvantita
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc4	množství
<g/>
)	)	kIx)	)
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
veliké	veliký	k2eAgNnSc1d1	veliké
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
konec	konec	k1gInSc4	konec
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
slovo	slovo	k1gNnSc1	slovo
konečný	konečný	k2eAgMnSc1d1	konečný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
spočíst	spočíst	k5eAaPmF	spočíst
<g/>
,	,	kIx,	,
změřit	změřit	k5eAaPmF	změřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
každé	každý	k3xTgNnSc1	každý
konečné	konečný	k2eAgNnSc1d1	konečné
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
veliký	veliký	k2eAgInSc1d1	veliký
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
atributy	atribut	k1gInPc4	atribut
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
přídavným	přídavný	k2eAgNnSc7d1	přídavné
jménem	jméno	k1gNnSc7	jméno
nekonečný	konečný	k2eNgInSc4d1	nekonečný
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
nemá	mít	k5eNaImIp3nS	mít
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
neohraničenost	neohraničenost	k1gFnSc1	neohraničenost
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
hranic	hranice	k1gFnPc2	hranice
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
nutnou	nutný	k2eAgFnSc7d1	nutná
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
postačující	postačující	k2eAgMnSc1d1	postačující
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
lze	lze	k6eAd1	lze
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
neohraničenosti	neohraničenost	k1gFnSc3	neohraničenost
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
eukleidovské	eukleidovský	k2eAgFnSc6d1	eukleidovská
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
ne	ne	k9	ne
<g/>
/	/	kIx~	/
<g/>
konečnost	konečnost	k1gFnSc4	konečnost
topologickou	topologický	k2eAgFnSc4d1	topologická
a	a	k8xC	a
metrickou	metrický	k2eAgFnSc4d1	metrická
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kulová	kulový	k2eAgFnSc1d1	kulová
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
povrch	povrch	k1gInSc1	povrch
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
metricky	metricky	k6eAd1	metricky
<g/>
)	)	kIx)	)
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neohraničená	ohraničený	k2eNgFnSc1d1	neohraničená
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
má	mít	k5eAaImIp3nS	mít
důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
a	a	k8xC	a
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
studiu	studio	k1gNnSc3	studio
přispěli	přispět	k5eAaPmAgMnP	přispět
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
čeští	český	k2eAgMnPc1d1	český
vědci	vědec	k1gMnPc1	vědec
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Vopěnka	Vopěnka	k1gFnSc1	Vopěnka
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vyprovokovalo	vyprovokovat	k5eAaPmAgNnS	vyprovokovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
úvahy	úvaha	k1gFnPc4	úvaha
i	i	k9	i
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
a	a	k8xC	a
teologii	teologie	k1gFnSc6	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
∞	∞	k?	∞
pro	pro	k7c4	pro
nekonečno	nekonečno	k1gNnSc4	nekonečno
zavedl	zavést	k5eAaPmAgMnS	zavést
anglický	anglický	k2eAgMnSc1d1	anglický
matematik	matematik	k1gMnSc1	matematik
John	John	k1gMnSc1	John
Wallis	Wallis	k1gFnSc4	Wallis
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
běžný	běžný	k2eAgInSc4d1	běžný
Eukleidovský	eukleidovský	k2eAgInSc4d1	eukleidovský
prostor	prostor	k1gInSc4	prostor
(	(	kIx(	(
<g/>
i	i	k9	i
rovina	rovina	k1gFnSc1	rovina
<g/>
)	)	kIx)	)
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
různými	různý	k2eAgNnPc7d1	různé
nekonečny	nekonečno	k1gNnPc7	nekonečno
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
body	bod	k1gInPc4	bod
<g/>
"	"	kIx"	"
s	s	k7c7	s
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
projektivní	projektivní	k2eAgFnSc6d1	projektivní
geometrii	geometrie	k1gFnSc6	geometrie
se	se	k3xPyFc4	se
každé	každý	k3xTgFnPc1	každý
dvě	dva	k4xCgFnPc1	dva
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
přímky	přímka	k1gFnPc1	přímka
protnou	protnout	k5eAaPmIp3nP	protnout
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
můžeme	moct	k5eAaImIp1nP	moct
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
nekonečno	nekonečno	k1gNnSc1	nekonečno
(	(	kIx(	(
<g/>
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
všechny	všechen	k3xTgFnPc1	všechen
přímky	přímka	k1gFnPc1	přímka
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
danou	daný	k2eAgFnSc7d1	daná
přímkou	přímka	k1gFnSc7	přímka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
doplnit	doplnit	k5eAaPmF	doplnit
prostor	prostor	k1gInSc4	prostor
jen	jen	k9	jen
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
nekonečno	nekonečno	k1gNnSc4	nekonečno
(	(	kIx(	(
<g/>
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tak	tak	k6eAd1	tak
topologická	topologický	k2eAgFnSc1d1	topologická
sféra	sféra	k1gFnSc1	sféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
různé	různý	k2eAgInPc1d1	různý
mohutnosti	mohutnost	k1gFnPc4	mohutnost
nekonečen	nekonečno	k1gNnPc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
popis	popis	k1gInSc4	popis
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pojmy	pojem	k1gInPc7	pojem
jako	jako	k8xS	jako
kardinály	kardinál	k1gMnPc7	kardinál
(	(	kIx(	(
<g/>
kardinální	kardinální	k2eAgNnPc1d1	kardinální
čísla	číslo	k1gNnPc1	číslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
ordinály	ordinála	k1gFnPc1	ordinála
(	(	kIx(	(
<g/>
ordinální	ordinální	k2eAgNnPc1d1	ordinální
čísla	číslo	k1gNnPc1	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
porovnání	porovnání	k1gNnSc2	porovnání
mohutnosti	mohutnost	k1gFnSc2	mohutnost
(	(	kIx(	(
<g/>
kardinality	kardinalita	k1gFnSc2	kardinalita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
vytvoření	vytvoření	k1gNnSc2	vytvoření
vzájemně	vzájemně	k6eAd1	vzájemně
jednoznačného	jednoznačný	k2eAgNnSc2d1	jednoznačné
zobrazení	zobrazení	k1gNnSc2	zobrazení
mezi	mezi	k7c7	mezi
množinami	množina	k1gFnPc7	množina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
stejná	stejný	k2eAgFnSc1d1	stejná
mohutnost	mohutnost	k1gFnSc1	mohutnost
množiny	množina	k1gFnSc2	množina
přirozených	přirozený	k2eAgInPc2d1	přirozený
<g/>
,	,	kIx,	,
celých	celý	k2eAgInPc2d1	celý
<g/>
,	,	kIx,	,
racionálních	racionální	k2eAgNnPc2d1	racionální
a	a	k8xC	a
algebraických	algebraický	k2eAgNnPc2d1	algebraické
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
množiny	množina	k1gFnPc1	množina
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgFnSc4d2	menší
mohutnost	mohutnost	k1gFnSc4	mohutnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
množina	množina	k1gFnSc1	množina
čísel	číslo	k1gNnPc2	číslo
transcendentních	transcendentní	k2eAgInPc2d1	transcendentní
<g/>
,	,	kIx,	,
iracionálních	iracionální	k2eAgInPc2d1	iracionální
nebo	nebo	k8xC	nebo
reálných	reálný	k2eAgInPc2d1	reálný
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
takovému	takový	k3xDgNnSc3	takový
pojetí	pojetí	k1gNnSc1	pojetí
nekonečna	nekonečno	k1gNnSc2	nekonečno
bylo	být	k5eAaImAgNnS	být
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
potenciálního	potenciální	k2eAgNnSc2d1	potenciální
k	k	k7c3	k
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
nekonečnu	nekonečno	k1gNnSc3	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálně	potenciálně	k6eAd1	potenciálně
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
představách	představa	k1gFnPc6	představa
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
konečná	konečný	k2eAgFnSc1d1	konečná
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
přibírat	přibírat	k5eAaImF	přibírat
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
množina	množina	k1gFnSc1	množina
ja	ja	k?	ja
pak	pak	k6eAd1	pak
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
brána	brát	k5eAaImNgFnS	brát
jako	jako	k9	jako
(	(	kIx(	(
<g/>
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
)	)	kIx)	)
celek	celek	k1gInSc1	celek
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
provedl	provést	k5eAaPmAgMnS	provést
český	český	k2eAgMnSc1d1	český
matematik	matematik	k1gMnSc1	matematik
Bernard	Bernard	k1gMnSc1	Bernard
Bolzano	Bolzana	k1gFnSc5	Bolzana
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
úplně	úplně	k6eAd1	úplně
nefyzikální	fyzikální	k2eNgInSc4d1	nefyzikální
(	(	kIx(	(
<g/>
výsledkem	výsledek	k1gInSc7	výsledek
měření	měření	k1gNnSc2	měření
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
veličiny	veličina	k1gFnSc2	veličina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nekonečna	nekonečno	k1gNnPc1	nekonečno
se	se	k3xPyFc4	se
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
limitě	limita	k1gFnSc6	limita
-	-	kIx~	-
z	z	k7c2	z
výpočetních	výpočetní	k2eAgInPc2d1	výpočetní
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
snazší	snadný	k2eAgNnSc1d2	snazší
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
nekonečnem	nekonečno	k1gNnSc7	nekonečno
než	než	k8xS	než
s	s	k7c7	s
konečnými	konečný	k2eAgFnPc7d1	konečná
kvantitami	kvantita	k1gFnPc7	kvantita
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
idealizace	idealizace	k1gFnSc1	idealizace
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
"	"	kIx"	"
<g/>
nefyzikální	fyzikální	k2eNgNnSc1d1	nefyzikální
<g/>
"	"	kIx"	"
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
,	,	kIx,	,
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
nekonečnem	nekonečno	k1gNnSc7	nekonečno
-	-	kIx~	-
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
brýlí	brýle	k1gFnPc2	brýle
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poloměr	poloměr	k1gInSc1	poloměr
rovné	rovný	k2eAgFnSc2d1	rovná
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horším	zlý	k2eAgInSc6d2	horší
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nekonečna	nekonečno	k1gNnSc2	nekonečno
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
řešení	řešení	k1gNnSc6	řešení
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
nějaké	nějaký	k3yIgFnSc2	nějaký
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc1	ten
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
matematický	matematický	k2eAgInSc1d1	matematický
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgNnSc6	jaký
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
formulována	formulovat	k5eAaImNgFnS	formulovat
<g/>
,	,	kIx,	,
přestává	přestávat	k5eAaImIp3nS	přestávat
stačit	stačit	k5eAaBmF	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
"	"	kIx"	"
<g/>
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
"	"	kIx"	"
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
hodnoty	hodnota	k1gFnPc1	hodnota
různých	různý	k2eAgFnPc2d1	různá
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
v	v	k7c6	v
singularitě	singularita	k1gFnSc6	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
interpretace	interpretace	k1gFnSc1	interpretace
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
teorie	teorie	k1gFnSc1	teorie
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
meze	mez	k1gFnPc4	mez
své	svůj	k3xOyFgFnSc2	svůj
platnosti	platnost	k1gFnSc2	platnost
a	a	k8xC	a
pro	pro	k7c4	pro
předpovědi	předpověď	k1gFnPc4	předpověď
skutečnosti	skutečnost	k1gFnSc2	skutečnost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
neexistující	existující	k2eNgFnSc1d1	neexistující
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
nekonečny	nekonečno	k1gNnPc7	nekonečno
<g/>
"	"	kIx"	"
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
další	další	k2eAgFnPc1d1	další
moderní	moderní	k2eAgFnPc1d1	moderní
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
teorie	teorie	k1gFnPc1	teorie
a	a	k8xC	a
důmyslné	důmyslný	k2eAgFnPc1d1	důmyslná
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
s	s	k7c7	s
nekonečny	nekonečno	k1gNnPc7	nekonečno
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
současné	současný	k2eAgFnSc2d1	současná
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
renormalizace	renormalizace	k1gFnSc2	renormalizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
