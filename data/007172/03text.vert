<s>
Břeh	břeh	k1gInSc1	břeh
nebo	nebo	k8xC	nebo
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
čára	čára	k1gFnSc1	čára
nebo	nebo	k8xC	nebo
také	také	k9	také
břehová	břehový	k2eAgFnSc1d1	Břehová
linie	linie	k1gFnSc1	linie
nebo	nebo	k8xC	nebo
i	i	k9	i
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
linie	linie	k1gFnSc1	linie
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
souše	souš	k1gFnSc2	souš
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
nebo	nebo	k8xC	nebo
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
břehu	břeh	k1gInSc2	břeh
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
druhem	druh	k1gInSc7	druh
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
<g/>
,	,	kIx,	,
erozí	eroze	k1gFnSc7	eroze
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
také	také	k9	také
sedimenty	sediment	k1gInPc1	sediment
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
může	moct	k5eAaImIp3nS	moct
voda	voda	k1gFnSc1	voda
přinést	přinést	k5eAaPmF	přinést
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
k	k	k7c3	k
pobřežní	pobřežní	k2eAgFnSc3d1	pobřežní
čáře	čára	k1gFnSc3	čára
napojuje	napojovat	k5eAaImIp3nS	napojovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nazývá	nazývat	k5eAaImIp3nS	nazývat
břeh	břeh	k1gInSc4	břeh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
vesnice	vesnice	k1gFnSc2	vesnice
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
pobřeží	pobřeží	k1gNnSc4	pobřeží
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mořských	mořský	k2eAgInPc2d1	mořský
břehů	břeh	k1gInPc2	břeh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Břehy	břeh	k1gInPc1	břeh
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
definují	definovat	k5eAaBmIp3nP	definovat
jako	jako	k9	jako
pravý	pravý	k2eAgInSc4d1	pravý
a	a	k8xC	a
levý	levý	k2eAgInSc4d1	levý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hladina	hladina	k1gFnSc1	hladina
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
břeh	břeh	k1gInSc1	břeh
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
pokud	pokud	k8xS	pokud
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
břeh	břeh	k1gInSc1	břeh
postupuje	postupovat	k5eAaImIp3nS	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Pláž	pláž	k1gFnSc1	pláž
Duna	duna	k1gFnSc1	duna
Nábřeží	nábřeží	k1gNnSc1	nábřeží
<g/>
,	,	kIx,	,
náplavka	náplavka	k1gFnSc1	náplavka
<g/>
,	,	kIx,	,
říční	říční	k2eAgFnSc1d1	říční
navigace	navigace	k1gFnSc1	navigace
Protipovodňová	protipovodňový	k2eAgFnSc1d1	protipovodňová
hráz	hráz	k1gFnSc1	hráz
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
břeh	břeh	k1gInSc4	břeh
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
