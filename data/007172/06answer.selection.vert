<s>
Tvar	tvar	k1gInSc1	tvar
břehu	břeh	k1gInSc2	břeh
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
druhem	druh	k1gInSc7	druh
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
<g/>
,	,	kIx,	,
erozí	eroze	k1gFnSc7	eroze
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
také	také	k9	také
sedimenty	sediment	k1gInPc1	sediment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
může	moct	k5eAaImIp3nS	moct
voda	voda	k1gFnSc1	voda
přinést	přinést	k5eAaPmF	přinést
<g/>
.	.	kIx.	.
</s>
