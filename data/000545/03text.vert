<s>
Sobota	sobota	k1gFnSc1	sobota
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
šestý	šestý	k4xOgInSc4	šestý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
židovském	židovský	k2eAgMnSc6d1	židovský
a	a	k8xC	a
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
sedmým	sedmý	k4xOgNnSc7	sedmý
a	a	k8xC	a
posledním	poslední	k2eAgMnSc6d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
bývá	bývat	k5eAaImIp3nS	bývat
součástí	součást	k1gFnSc7	součást
víkendu	víkend	k1gInSc2	víkend
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dnem	den	k1gInSc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
šabat	šabat	k1gInSc4	šabat
(	(	kIx(	(
<g/>
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rumunské	rumunský	k2eAgFnSc2d1	rumunská
legendy	legenda	k1gFnSc2	legenda
název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
pojmu	pojem	k1gInSc2	pojem
Svatá	svatý	k2eAgFnSc1d1	svatá
Sobota	sobota	k1gFnSc1	sobota
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
dobrá	dobrý	k2eAgFnSc1d1	dobrá
stařena	stařena	k1gFnSc1	stařena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
napravuje	napravovat	k5eAaImIp3nS	napravovat
zbloudilé	zbloudilý	k2eAgFnPc4d1	zbloudilá
duše	duše	k1gFnPc4	duše
zde	zde	k6eAd1	zde
i	i	k8xC	i
na	na	k7c6	na
onom	onen	k3xDgInSc6	onen
světě	svět	k1gInSc6	svět
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
stále	stále	k6eAd1	stále
a	a	k8xC	a
bez	bez	k7c2	bez
oddychu	oddych	k1gInSc2	oddych
a	a	k8xC	a
proto	proto	k8xC	proto
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
železné	železný	k2eAgInPc4d1	železný
topánky	topánky	k1gInPc4	topánky
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Sobota	sobota	k1gFnSc1	sobota
žije	žít	k5eAaImIp3nS	žít
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
příbytkem	příbytek	k1gInSc7	příbytek
je	být	k5eAaImIp3nS	být
Půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
saturday	saturdaa	k1gFnPc1	saturdaa
<g/>
"	"	kIx"	"
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
názvy	název	k1gInPc1	název
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
Saturni	Saturni	k1gMnSc2	Saturni
Dies	Dies	k1gInSc4	Dies
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Den	den	k1gInSc4	den
Saturna	Saturn	k1gMnSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
sobota	sobota	k1gFnSc1	sobota
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
Lokimu	Lokim	k1gMnSc3	Lokim
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
"	"	kIx"	"
<g/>
šanivar	šanivar	k1gInSc4	šanivar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Šani	Šan	k1gFnSc2	Šan
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
védský	védský	k2eAgMnSc1d1	védský
bůh	bůh	k1gMnSc1	bůh
přiřazený	přiřazený	k2eAgInSc4d1	přiřazený
planetě	planeta	k1gFnSc3	planeta
Saturn	Saturn	k1gInSc4	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skandinávských	skandinávský	k2eAgFnPc6d1	skandinávská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
sobota	sobota	k1gFnSc1	sobota
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lördag	Lördag	k1gInSc4	Lördag
nebo	nebo	k8xC	nebo
Löverdag	Löverdag	k1gInSc4	Löverdag
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
slova	slovo	k1gNnSc2	slovo
laugr	laugra	k1gFnPc2	laugra
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
islandský	islandský	k2eAgInSc1d1	islandský
název	název	k1gInSc1	název
Laugardagur	Laugardagura	k1gFnPc2	Laugardagura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
míněno	mínit	k5eAaImNgNnS	mínit
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
koupele	koupel	k1gFnSc2	koupel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Lördag	Lördag	k1gInSc1	Lördag
znamená	znamenat	k5eAaImIp3nS	znamenat
koupelový	koupelový	k2eAgInSc4d1	koupelový
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vděčí	vděčit	k5eAaImIp3nS	vděčit
svému	svůj	k3xOyFgInSc3	svůj
názvu	název	k1gInSc3	název
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vikingové	viking	k1gMnPc1	viking
používali	používat	k5eAaImAgMnP	používat
koupel	koupel	k1gFnSc4	koupel
každou	každý	k3xTgFnSc4	každý
sobotu	sobota	k1gFnSc4	sobota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
sobota	sobota	k1gFnSc1	sobota
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Samstag	Samstag	k1gInSc1	Samstag
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
moderní	moderní	k2eAgFnSc6d1	moderní
němčině	němčina	k1gFnSc6	němčina
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
názvy	název	k1gInPc4	název
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Samstag	Samstag	k1gInSc1	Samstag
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6	Lichtenštejnsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
německy	německy	k6eAd1	německy
hovořící	hovořící	k2eAgFnSc6d1	hovořící
části	část	k1gFnSc6	část
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
také	také	k9	také
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
a	a	k8xC	a
západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starohornoněmeckého	starohornoněmecký	k2eAgNnSc2d1	starohornoněmecké
sambaztac	sambaztac	k6eAd1	sambaztac
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sám	sám	k3xTgMnSc1	sám
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Σ	Σ	k?	Σ
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
hebrejském	hebrejský	k2eAgInSc6d1	hebrejský
ש	ש	k?	ש
(	(	kIx(	(
<g/>
šabat	šabat	k1gInSc4	šabat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
názvem	název	k1gInSc7	název
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
Sonnabend	Sonnabend	k1gInSc4	Sonnabend
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
starohornoněmeckého	starohornoněmecký	k2eAgMnSc2d1	starohornoněmecký
sunnunaband	sunnunaband	k1gInSc4	sunnunaband
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
staroanglickému	staroanglický	k2eAgMnSc3d1	staroanglický
sunnanæ	sunnanæ	k?	sunnanæ
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
je	být	k5eAaImIp3nS	být
myšlen	myšlen	k2eAgInSc1d1	myšlen
"	"	kIx"	"
<g/>
předvečer	předvečer	k1gInSc1	předvečer
slunce	slunce	k1gNnSc1	slunce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
před	před	k7c7	před
nedělí	neděle	k1gFnSc7	neděle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Sonnabend	Sonnabenda	k1gFnPc2	Sonnabenda
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Německé	německý	k2eAgFnSc6d1	německá
demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vestfálských	vestfálský	k2eAgNnPc6d1	vestfálské
nářečích	nářečí	k1gNnPc6	nářečí
byla	být	k5eAaImAgFnS	být
sobota	sobota	k1gFnSc1	sobota
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Satertag	Satertag	k1gInSc1	Satertag
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
příbuzné	příbuzný	k1gMnPc4	příbuzný
s	s	k7c7	s
nizozemským	nizozemský	k2eAgInSc7d1	nizozemský
Zaterdag	Zaterdag	k1gInSc4	Zaterdag
<g/>
,	,	kIx,	,
a	a	k8xC	a
vycházelo	vycházet	k5eAaImAgNnS	vycházet
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
lingvistického	lingvistický	k2eAgInSc2d1	lingvistický
kořene	kořen	k1gInSc2	kořen
jako	jako	k8xC	jako
anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
Saturday	Saturdaa	k1gFnSc2	Saturdaa
<g/>
.	.	kIx.	.
</s>
<s>
Románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
převzaly	převzít	k5eAaPmAgFnP	převzít
název	název	k1gInSc4	název
soboty	sobota	k1gFnSc2	sobota
z	z	k7c2	z
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
šabat	šabat	k1gInSc4	šabat
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
sabato	sabato	k6eAd1	sabato
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
samedi	samed	k1gMnPc1	samed
<g/>
,	,	kIx,	,
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
a	a	k8xC	a
portugalštině	portugalština	k1gFnSc6	portugalština
sábado	sábada	k1gFnSc5	sábada
a	a	k8xC	a
v	v	k7c6	v
rumunštině	rumunština	k1gFnSc6	rumunština
sâmbătă	sâmbătă	k?	sâmbătă
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
maorský	maorský	k2eAgInSc1d1	maorský
název	název	k1gInSc1	název
pro	pro	k7c4	pro
sobotu	sobota	k1gFnSc4	sobota
je	být	k5eAaImIp3nS	být
Rahoroi	Rahoroe	k1gFnSc4	Rahoroe
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
prací	prací	k2eAgInSc4d1	prací
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
islám	islám	k1gInSc1	islám
pohlížejí	pohlížet	k5eAaImIp3nP	pohlížet
na	na	k7c4	na
sobotu	sobota	k1gFnSc4	sobota
jako	jako	k8xS	jako
na	na	k7c4	na
sedmý	sedmý	k4xOgInSc4	sedmý
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
mnoho	mnoho	k6eAd1	mnoho
Evropanů	Evropan	k1gMnPc2	Evropan
považovat	považovat	k5eAaImF	považovat
sobotu	sobota	k1gFnSc4	sobota
za	za	k7c4	za
šestý	šestý	k4xOgInSc4	šestý
(	(	kIx(	(
<g/>
předposlední	předposlední	k2eAgInSc4d1	předposlední
<g/>
)	)	kIx)	)
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
a	a	k8xC	a
neděli	neděle	k1gFnSc4	neděle
za	za	k7c4	za
den	den	k1gInSc4	den
poslední	poslední	k2eAgInSc4d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nynější	nynější	k2eAgFnSc1d1	nynější
pracovně	pracovně	k6eAd1	pracovně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
koncepce	koncepce	k1gFnSc1	koncepce
byla	být	k5eAaImAgFnS	být
formálně	formálně	k6eAd1	formálně
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
ISO	ISO	kA	ISO
8601	[number]	k4	8601
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
neděle	neděle	k1gFnPc4	neděle
svátkem	svátek	k1gInSc7	svátek
je	být	k5eAaImIp3nS	být
sobota	sobota	k1gFnSc1	sobota
částí	část	k1gFnPc2	část
víkendu	víkend	k1gInSc2	víkend
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
dnem	den	k1gInSc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
večírků	večírek	k1gInPc2	večírek
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
právě	právě	k9	právě
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předchází	předcházet	k5eAaImIp3nS	předcházet
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
pro	pro	k7c4	pro
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
bary	bar	k1gInPc4	bar
a	a	k8xC	a
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
sobota	sobota	k1gFnSc1	sobota
sabatem	sabat	k1gInSc7	sabat
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
převzali	převzít	k5eAaPmAgMnP	převzít
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
sobota	sobota	k1gFnSc1	sobota
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
právě	právě	k9	právě
po	po	k7c6	po
sabatu	sabat	k1gInSc6	sabat
<g/>
.	.	kIx.	.
</s>
<s>
Kvakeři	kvaker	k1gMnPc1	kvaker
tradičně	tradičně	k6eAd1	tradičně
nazývají	nazývat	k5eAaImIp3nP	nazývat
sobotu	sobota	k1gFnSc4	sobota
"	"	kIx"	"
<g/>
sedmým	sedmý	k4xOgInSc7	sedmý
dnem	den	k1gInSc7	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
pohanskému	pohanský	k2eAgInSc3d1	pohanský
původu	původ	k1gInSc3	původ
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
islámských	islámský	k2eAgFnPc6d1	islámská
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
svátečním	sváteční	k2eAgNnSc7d1	sváteční
dnem	dno	k1gNnSc7	dno
pátek	pátek	k1gInSc1	pátek
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
šestý	šestý	k4xOgInSc4	šestý
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Sobota	sobota	k1gFnSc1	sobota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
dnem	den	k1gInSc7	den
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
dokonce	dokonce	k9	dokonce
jen	jen	k6eAd1	jen
jediným	jediný	k2eAgInSc7d1	jediný
možným	možný	k2eAgInSc7d1	možný
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
thajského	thajský	k2eAgInSc2d1	thajský
solárního	solární	k2eAgInSc2d1	solární
kalendáře	kalendář	k1gInSc2	kalendář
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
sobotě	sobota	k1gFnSc6	sobota
purpurová	purpurový	k2eAgFnSc1d1	purpurová
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
začne	začít	k5eAaPmIp3nS	začít
špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
udrží	udržet	k5eAaPmIp3nS	udržet
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Adventisté	adventista	k1gMnPc1	adventista
světí	světit	k5eAaImIp3nP	světit
sobotu	sobota	k1gFnSc4	sobota
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
Božího	boží	k2eAgNnSc2d1	boží
stvořitelského	stvořitelský	k2eAgNnSc2d1	stvořitelské
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověr	pověra	k1gFnPc2	pověra
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
dělat	dělat	k5eAaImF	dělat
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ničeho	nic	k3yNnSc2	nic
nepožít	požít	k5eNaPmF	požít
před	před	k7c7	před
východem	východ	k1gInSc7	východ
slunce	slunce	k1gNnSc2	slunce
<g/>
;	;	kIx,	;
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bůh	bůh	k1gMnSc1	bůh
dokončil	dokončit	k5eAaPmAgMnS	dokončit
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
dokončit	dokončit	k5eAaPmF	dokončit
započaté	započatý	k2eAgFnPc4d1	započatá
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezačínat	začínat	k5eNaImF	začínat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
v	v	k7c4	v
sobotní	sobotní	k2eAgInSc4d1	sobotní
večer	večer	k1gInSc4	večer
donesete	donést	k5eAaPmIp2nP	donést
domů	domů	k6eAd1	domů
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
třikrát	třikrát	k6eAd1	třikrát
kápnout	kápnout	k5eAaPmF	kápnout
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
budete	být	k5eAaImBp2nP	být
pít	pít	k5eAaImF	pít
<g/>
;	;	kIx,	;
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
si	se	k3xPyFc3	se
umývat	umývat	k5eAaImF	umývat
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
voda	voda	k1gFnSc1	voda
odplavuje	odplavovat	k5eAaImIp3nS	odplavovat
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
;	;	kIx,	;
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
dělat	dělat	k5eAaImF	dělat
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
šít	šít	k5eAaImF	šít
šaty	šat	k1gInPc4	šat
a	a	k8xC	a
cestovat	cestovat	k5eAaImF	cestovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vstávají	vstávat	k5eAaImIp3nP	vstávat
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
a	a	k8xC	a
umírající	umírající	k2eAgMnPc1d1	umírající
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
onemocnět	onemocnět	k5eAaPmF	onemocnět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
může	moct	k5eAaImIp3nS	moct
přitížit	přitížit	k5eAaPmF	přitížit
-	-	kIx~	-
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
vše	všechen	k3xTgNnSc4	všechen
spěje	spět	k5eAaImIp3nS	spět
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
i	i	k9	i
váš	váš	k3xOp2gInSc1	váš
život	život	k1gInSc1	život
může	moct	k5eAaImIp3nS	moct
skončit	skončit	k5eAaPmF	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
sobota	sobota	k1gFnSc1	sobota
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
sobota	sobota	k1gFnSc1	sobota
Sabat	sabat	k1gInSc4	sabat
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sobota	sobota	k1gFnSc1	sobota
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sobota	sobota	k1gFnSc1	sobota
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
