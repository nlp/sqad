<p>
<s>
10	[number]	k4	10
<g/>
mm	mm	kA	mm
Auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
pistolový	pistolový	k2eAgInSc1d1	pistolový
náboj	náboj	k1gInSc1	náboj
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
Jeffem	Jeff	k1gMnSc7	Jeff
Cooperem	Cooper	k1gMnSc7	Cooper
a	a	k8xC	a
představený	představený	k2eAgInSc1d1	představený
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
návrhu	návrh	k1gInSc2	návrh
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k1gFnSc1	potřeba
určitého	určitý	k2eAgInSc2d1	určitý
výkonnostního	výkonnostní	k2eAgInSc2d1	výkonnostní
kompromisu	kompromis	k1gInSc2	kompromis
mezi	mezi	k7c7	mezi
náboji	náboj	k1gInPc7	náboj
.38	.38	k4	.38
Super	super	k6eAd1	super
Auto	auto	k1gNnSc1	auto
a	a	k8xC	a
.45	.45	k4	.45
ACP	ACP	kA	ACP
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
tohoto	tento	k3xDgInSc2	tento
náboje	náboj	k1gInSc2	náboj
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
i	i	k9	i
pistole	pistole	k1gFnSc1	pistole
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
komorovaná	komorovaný	k2eAgNnPc1d1	komorované
<g/>
,	,	kIx,	,
Bren	Bren	k1gInSc4	Bren
Ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Dornaus	Dornaus	k1gMnSc1	Dornaus
and	and	k?	and
Dixon	Dixon	k1gMnSc1	Dixon
<g/>
.	.	kIx.	.
</s>
<s>
Sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
tohoto	tento	k3xDgInSc2	tento
náboje	náboj	k1gInSc2	náboj
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
firmou	firma	k1gFnSc7	firma
Norma	Norma	k1gFnSc1	Norma
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Synonyma	synonymum	k1gNnSc2	synonymum
==	==	k?	==
</s>
</p>
<p>
<s>
10	[number]	k4	10
mm	mm	kA	mm
Bren	Brena	k1gFnPc2	Brena
Ten	ten	k3xDgMnSc1	ten
</s>
</p>
<p>
<s>
10	[number]	k4	10
mm	mm	kA	mm
Automatic	Automatice	k1gFnPc2	Automatice
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
x	x	k?	x
<g/>
25	[number]	k4	25
mm	mm	kA	mm
</s>
</p>
<p>
<s>
==	==	k?	==
Zbraně	zbraň	k1gFnPc1	zbraň
==	==	k?	==
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
10	[number]	k4	10
mm	mm	kA	mm
Auto	auto	k1gNnSc1	auto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Glock	Glock	k6eAd1	Glock
20	[number]	k4	20
</s>
</p>
<p>
<s>
Para	para	k1gFnSc1	para
Elite	Elit	k1gInSc5	Elit
LS	LS	kA	LS
Hunter	Hunter	k1gInSc1	Hunter
</s>
</p>
<p>
<s>
STI	STI	kA	STI
Perfect	Perfect	k1gInSc4	Perfect
10	[number]	k4	10
</s>
</p>
<p>
<s>
Colt	Colt	k1gInSc1	Colt
Delta	delta	k1gFnSc1	delta
Elite	Elit	k1gInSc5	Elit
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
ručních	ruční	k2eAgFnPc2d1	ruční
zbraní	zbraň	k1gFnPc2	zbraň
</s>
</p>
<p>
<s>
Projektil	projektil	k1gInSc1	projektil
</s>
</p>
<p>
<s>
Palná	palný	k2eAgFnSc1d1	palná
zbraň	zbraň	k1gFnSc1	zbraň
</s>
</p>
