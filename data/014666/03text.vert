<s>
Svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Dobříšska	Dobříšsk	k1gInSc2
a	a	k8xC
Novoknínska	Novoknínsko	k1gNnSc2
</s>
<s>
Svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Dobříšska	Dobříšsk	k1gInSc2
a	a	k8xC
NovoknínskaForma	NovoknínskaFormum	k1gNnSc2
</s>
<s>
dobrovolný	dobrovolný	k2eAgInSc4d1
svazek	svazek	k1gInSc4
obcí	obec	k1gFnPc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Haniš	Haniš	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Městský	městský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Dobříš	dobřit	k5eAaImIp2nS
<g/>
,	,	kIx,
Mírové	mírový	k2eAgFnSc6d1
nám.	nám.	k?
119	#num#	k4
<g/>
,	,	kIx,
263	#num#	k4
01	#num#	k4
Dobříš	dobřit	k5eAaImIp2nS
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Příbram	Příbram	k1gFnSc1
Kontakt	kontakt	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
obec.cim@seznam.cz	obec.cim@seznam.cz	k1gInSc1
Web	web	k1gInSc1
</s>
<s>
dobrisskonovokninsko	dobrisskonovokninsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Dobříšska	Dobříšsk	k1gInSc2
a	a	k8xC
Novoknínska	Novoknínsko	k1gNnSc2
je	být	k5eAaImIp3nS
dobrovolný	dobrovolný	k2eAgInSc4d1
svazek	svazek	k1gInSc4
obcí	obec	k1gFnPc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
v	v	k7c6
okresu	okres	k1gInSc6
Příbram	Příbram	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Dobříš	Dobříš	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
cíle	cíl	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
rozsahu	rozsah	k1gInSc6
uvedeném	uvedený	k2eAgInSc6d1
v	v	k7c6
§	§	k?
50	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
128	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
24	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
sdružené	sdružený	k2eAgFnPc1d1
v	v	k7c6
mikroregionu	mikroregion	k1gInSc6
</s>
<s>
Borotice	Borotice	k1gFnSc1
</s>
<s>
Čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
</s>
<s>
Daleké	daleký	k2eAgMnPc4d1
Dušníky	dušník	k1gMnPc4
</s>
<s>
Dobříš	dobřit	k5eAaImIp2nS
</s>
<s>
Drevníky	Drevník	k1gInPc1
</s>
<s>
Drhovy	Drhov	k1gInPc1
</s>
<s>
Hříměždice	Hříměždice	k1gFnSc1
</s>
<s>
Chotilsko	Chotilsko	k6eAd1
</s>
<s>
Korkyně	Korkyně	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Hraštice	Hraštice	k1gFnSc1
</s>
<s>
Mokrovraty	Mokrovrat	k1gInPc1
</s>
<s>
Nečín	Nečín	k1gMnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
pod	pod	k7c7
Pleší	pleš	k1gFnSc7
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Knín	Knín	k1gInSc1
</s>
<s>
Obořiště	Obořiště	k1gNnSc1
</s>
<s>
Ouběnice	Ouběnice	k1gFnSc1
</s>
<s>
Rosovice	Rosovice	k1gFnSc1
</s>
<s>
Rybníky	rybník	k1gInPc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
</s>
<s>
Svaté	svatý	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Lečice	Lečice	k1gFnSc1
</s>
<s>
Voznice	voznice	k1gFnSc1
</s>
<s>
Županovice	Županovice	k1gFnSc1
</s>
<s>
Cíle	cíl	k1gInPc1
svazku	svazek	k1gInSc2
</s>
<s>
Svazek	svazek	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
a	a	k8xC
prosazování	prosazování	k1gNnSc2
společných	společný	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
členských	členský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
jsou	být	k5eAaImIp3nP
především	především	k6eAd1
péče	péče	k1gFnSc1
o	o	k7c4
všestranný	všestranný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
zájmového	zájmový	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
,	,	kIx,
péče	péče	k1gFnSc2
o	o	k7c4
potřeby	potřeba	k1gFnPc4
občanů	občan	k1gMnPc2
členských	členský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
a	a	k8xC
ochrana	ochrana	k1gFnSc1
veřejného	veřejný	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Svazek	svazek	k1gInSc1
vyvíjí	vyvíjet	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
především	především	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
regionu	region	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
geografickou	geografický	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
vymezenou	vymezený	k2eAgFnSc4d1
katastrálním	katastrální	k2eAgNnSc7d1
územím	území	k1gNnSc7
členských	členský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Předmět	předmět	k1gInSc1
činnosti	činnost	k1gFnSc2
svazku	svazek	k1gInSc2
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
činností	činnost	k1gFnSc7
svazku	svazek	k1gInSc2
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
zejména	zejména	k9
na	na	k7c4
výkon	výkon	k1gInSc4
činností	činnost	k1gFnPc2
směřujících	směřující	k2eAgFnPc2d1
k	k	k7c3
systematickému	systematický	k2eAgMnSc3d1
a	a	k8xC
efektivnímu	efektivní	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
zájmového	zájmový	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
,	,	kIx,
ochranu	ochrana	k1gFnSc4
a	a	k8xC
prosazování	prosazování	k1gNnSc4
společných	společný	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
členských	členský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
spolupráci	spolupráce	k1gFnSc4
při	při	k7c6
rozvíjení	rozvíjení	k1gNnSc6
činností	činnost	k1gFnPc2
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
<g/>
:	:	kIx,
oblasti	oblast	k1gFnSc3
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
sociální	sociální	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
rozvoje	rozvoj	k1gInSc2
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
veřejného	veřejný	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
rozvoje	rozvoj	k1gInSc2
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
rozvoje	rozvoj	k1gInSc2
služeb	služba	k1gFnPc2
v	v	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
zajištění	zajištění	k1gNnSc6
dopravní	dopravní	k2eAgFnSc2d1
obslužnosti	obslužnost	k1gFnSc2
<g/>
,	,	kIx,
zabezpečení	zabezpečení	k1gNnSc2
čistoty	čistota	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
správy	správa	k1gFnSc2
veřejné	veřejný	k2eAgFnSc2d1
zeleně	zeleň	k1gFnSc2
<g/>
,	,	kIx,
zásobování	zásobování	k1gNnSc1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
odvádění	odvádění	k1gNnSc6
a	a	k8xC
čištění	čištění	k1gNnSc6
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
společných	společný	k2eAgInPc2d1
nákupů	nákup	k1gInPc2
energií	energie	k1gFnPc2
<g/>
,	,	kIx,
společné	společný	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
dalších	další	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
regionálního	regionální	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
133029	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
167574736	#num#	k4
</s>
