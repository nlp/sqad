<s>
Svazek	svazek	k1gInSc1
obcí	obec	k1gFnPc2
Dobříšska	Dobříšsk	k1gInSc2
a	a	k8xC
Novoknínska	Novoknínsko	k1gNnSc2
je	být	k5eAaImIp3nS
dobrovolný	dobrovolný	k2eAgInSc4d1
svazek	svazek	k1gInSc4
obcí	obec	k1gFnPc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
v	v	k7c6
okresu	okres	k1gInSc6
Příbram	Příbram	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Dobříš	Dobříš	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
cíle	cíl	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
rozsahu	rozsah	k1gInSc6
uvedeném	uvedený	k2eAgInSc6d1
v	v	k7c6
§	§	k?
50	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
128	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
24	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>