<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
a	a	k8xC	a
výstava	výstava	k1gFnSc1	výstava
aranžovaných	aranžovaný	k2eAgFnPc2d1	aranžovaná
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
den	den	k1gInSc4	den
po	po	k7c6	po
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
i	i	k9	i
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Vstupné	vstupné	k1gNnSc1	vstupné
je	být	k5eAaImIp3nS	být
dobrovolné	dobrovolný	k2eAgNnSc1d1	dobrovolné
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
výtěžek	výtěžek	k1gInSc1	výtěžek
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgNnPc1d1	soutěžní
témata	téma	k1gNnPc1	téma
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgInP	být
měněny	měnit	k5eAaImNgInP	měnit
i	i	k8xC	i
místa	místo	k1gNnPc4	místo
konání	konání	k1gNnSc2	konání
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
odborná	odborný	k2eAgFnSc1d1	odborná
porota	porota	k1gFnSc1	porota
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
floristů	florista	k1gMnPc2	florista
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
porota	porota	k1gFnSc1	porota
během	během	k7c2	během
práce	práce	k1gFnSc2	práce
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
technik	technika	k1gFnPc2	technika
soutěžícími	soutěžící	k2eAgInPc7d1	soutěžící
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
podporoval	podporovat	k5eAaImAgInS	podporovat
velkoobchod	velkoobchod	k1gInSc1	velkoobchod
s	s	k7c7	s
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
floristickými	floristický	k2eAgFnPc7d1	floristická
potřebami	potřeba	k1gFnPc7	potřeba
společnost	společnost	k1gFnSc1	společnost
Vonekl	Vonekl	k1gFnSc2	Vonekl
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
spolupořádala	spolupořádat	k5eAaImAgFnS	spolupořádat
Střední	střední	k2eAgFnSc1d1	střední
zahradnická	zahradnická	k1gFnSc1	zahradnická
a	a	k8xC	a
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
<g/>
.	.	kIx.	.
</s>
<s>
Organizátorem	organizátor	k1gInSc7	organizátor
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
duchovním	duchovní	k1gMnSc7	duchovní
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
Slávek	Slávek	k1gMnSc1	Slávek
Rabušic	Rabušic	k1gMnSc1	Rabušic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Invence	invence	k1gFnPc4	invence
a	a	k8xC	a
řemeslo	řemeslo	k1gNnSc4	řemeslo
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
soutěž	soutěž	k1gFnSc1	soutěž
je	být	k5eAaImIp3nS	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
dobrou	dobrý	k2eAgFnSc7d1	dobrá
řemeslnou	řemeslný	k2eAgFnSc7d1	řemeslná
úrovní	úroveň	k1gFnSc7	úroveň
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
snahou	snaha	k1gFnSc7	snaha
o	o	k7c6	o
invenci	invence	k1gFnSc6	invence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
rovněž	rovněž	k9	rovněž
vysoce	vysoce	k6eAd1	vysoce
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
byla	být	k5eAaImAgFnS	být
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
adventního	adventní	k2eAgInSc2d1	adventní
věnce	věnec	k1gInSc2	věnec
z	z	k7c2	z
kousků	kousek	k1gInPc2	kousek
bílé	bílý	k2eAgFnSc2d1	bílá
čokolády	čokoláda	k1gFnSc2	čokoláda
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
kvetoucí	kvetoucí	k2eAgFnPc1d1	kvetoucí
čemeřice	čemeřice	k1gFnSc1	čemeřice
a	a	k8xC	a
hnědou	hnědý	k2eAgFnSc7d1	hnědá
čokoládou	čokoláda	k1gFnSc7	čokoláda
byl	být	k5eAaImAgInS	být
potřen	potřen	k2eAgInSc1d1	potřen
jednoduše	jednoduše	k6eAd1	jednoduše
přizdobený	přizdobený	k2eAgInSc1d1	přizdobený
paraván	paraván	k1gInSc1	paraván
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
potravin	potravina	k1gFnPc2	potravina
k	k	k7c3	k
takto	takto	k6eAd1	takto
neobvyklým	obvyklý	k2eNgInPc3d1	neobvyklý
účelům	účel	k1gInPc3	účel
poutá	poutat	k5eAaImIp3nS	poutat
pozornost	pozornost	k1gFnSc4	pozornost
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
součástí	součást	k1gFnSc7	součást
showbyznysu	showbyznys	k1gInSc2	showbyznys
(	(	kIx(	(
<g/>
Lady	lady	k1gFnSc1	lady
Gaga	Gaga	k1gMnSc1	Gaga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
potravin	potravina	k1gFnPc2	potravina
při	při	k7c6	při
aranžování	aranžování	k1gNnSc6	aranžování
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
u	u	k7c2	u
jedlých	jedlý	k2eAgNnPc2d1	jedlé
aranžmá	aranžmá	k1gNnPc2	aranžmá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
silně	silně	k6eAd1	silně
toxickými	toxický	k2eAgFnPc7d1	toxická
rostlinami	rostlina	k1gFnPc7	rostlina
není	být	k5eNaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
úpravy	úprava	k1gFnPc4	úprava
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
adventních	adventní	k2eAgInPc2d1	adventní
věnců	věnec	k1gInPc2	věnec
s	s	k7c7	s
hlavičkami	hlavička	k1gFnPc7	hlavička
živých	živý	k2eAgFnPc2d1	živá
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nelze	lze	k6eNd1	lze
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
trvanlivostí	trvanlivost	k1gFnSc7	trvanlivost
květů	květ	k1gInPc2	květ
čtyři	čtyři	k4xCgNnPc4	čtyři
až	až	k9	až
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
úprav	úprava	k1gFnPc2	úprava
svícnů	svícen	k1gInPc2	svícen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
uspořádana	uspořádana	k1gFnSc1	uspořádana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
dříve	dříve	k6eAd2	dříve
pořádal	pořádat	k5eAaImAgInS	pořádat
Svaz	svaz	k1gInSc1	svaz
květinářů	květinář	k1gMnPc2	květinář
a	a	k8xC	a
floristů	florista	k1gMnPc2	florista
ČR	ČR	kA	ČR
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
škola	škola	k1gFnSc1	škola
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
SZeŠ	SZeŠ	k1gFnSc2	SZeŠ
Brno	Brno	k1gNnSc4	Brno
Bohunice	Bohunice	k1gFnPc4	Bohunice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spolupořadatelem	spolupořadatel	k1gMnSc7	spolupořadatel
soutěže	soutěž	k1gFnSc2	soutěž
byla	být	k5eAaImAgFnS	být
Střední	střední	k2eAgFnSc1d1	střední
zahradnická	zahradnická	k1gFnSc1	zahradnická
a	a	k8xC	a
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
tématem	téma	k1gNnSc7	téma
vánoční	vánoční	k2eAgFnSc4d1	vánoční
floristika	floristika	k1gFnSc1	floristika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
floristické	floristický	k2eAgFnSc2d1	floristická
soutěže	soutěž	k1gFnSc2	soutěž
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Památníku	památník	k1gInSc2	památník
písemnictví	písemnictví	k1gNnSc2	písemnictví
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgNnSc1d1	soutěžní
téma	téma	k1gNnSc1	téma
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
Výstava	výstava	k1gFnSc1	výstava
a	a	k8xC	a
vernisáž	vernisáž	k1gFnSc1	vernisáž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
se	se	k3xPyFc4	se
floristická	floristický	k2eAgFnSc1d1	floristická
soutěž	soutěž	k1gFnSc1	soutěž
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
konventu	konvent	k1gInSc2	konvent
"	"	kIx"	"
<g/>
U	u	k7c2	u
milosrdných	milosrdný	k2eAgMnPc2d1	milosrdný
bratří	bratr	k1gMnPc2	bratr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byly	být	k5eAaImAgInP	být
témata	téma	k1gNnPc4	téma
věnec	věnec	k1gInSc4	věnec
(	(	kIx(	(
<g/>
adventní	adventní	k2eAgInSc4d1	adventní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paraván	paraván	k1gInSc1	paraván
<g/>
,,	,,	k?	,,
kytice	kytice	k1gFnPc1	kytice
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kytic	kytice	k1gFnPc2	kytice
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
jako	jako	k9	jako
kytice	kytice	k1gFnPc4	kytice
vázané	vázaný	k2eAgFnPc4d1	vázaná
do	do	k7c2	do
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	pře	k1gFnSc4	pře
samotné	samotný	k2eAgFnSc2d1	samotná
soutěže	soutěž	k1gFnSc2	soutěž
podle	podle	k7c2	podle
spolupořadatele	spolupořadatel	k1gMnSc2	spolupořadatel
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
dbáno	dbán	k2eAgNnSc1d1	dbáno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
vhodně	vhodně	k6eAd1	vhodně
zvolena	zvolen	k2eAgNnPc4d1	zvoleno
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgMnPc4d1	zajímavý
pro	pro	k7c4	pro
soutěžící	soutěžící	k2eAgNnPc4d1	soutěžící
ale	ale	k8xC	ale
také	také	k9	také
aby	aby	kYmCp3nP	aby
vycházely	vycházet	k5eAaImAgInP	vycházet
z	z	k7c2	z
květinářské	květinářský	k2eAgFnSc2d1	květinářská
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
srozumitelné	srozumitelný	k2eAgFnPc4d1	srozumitelná
laické	laický	k2eAgFnPc4d1	laická
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
2002	[number]	k4	2002
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byly	být	k5eAaImAgInP	být
témata	téma	k1gNnPc4	téma
dámský	dámský	k2eAgInSc4d1	dámský
klobouk	klobouk	k1gInSc4	klobouk
<g/>
,	,	kIx,	,
kytice	kytice	k1gFnPc1	kytice
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
květu	květ	k1gInSc2	květ
(	(	kIx(	(
<g/>
k	k	k7c3	k
darování	darování	k1gNnSc3	darování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přízdoba	přízdoba	k1gFnSc1	přízdoba
společenských	společenský	k2eAgInPc2d1	společenský
šatů	šat	k1gInPc2	šat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
juniorů	junior	k1gMnPc2	junior
byl	být	k5eAaImAgInS	být
celkovým	celkový	k2eAgMnSc7d1	celkový
vítězem	vítěz	k1gMnSc7	vítěz
Róbert	Róbert	k1gMnSc1	Róbert
Bartolen	Bartolen	k2eAgMnSc1d1	Bartolen
ze	z	k7c2	z
SZáŠ	SZáŠ	k1gFnSc2	SZáŠ
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
László	László	k1gMnSc1	László
Földes	Földes	k1gMnSc1	Földes
<g/>
,	,	kIx,	,
z	z	k7c2	z
MZLU	MZLU	kA	MZLU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ZF	ZF	kA	ZF
Lednice	Lednice	k1gFnSc2	Lednice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
porota	porota	k1gFnSc1	porota
věnovala	věnovat	k5eAaPmAgFnS	věnovat
Janě	Jana	k1gFnSc3	Jana
Sejkorové	Sejkorová	k1gFnSc3	Sejkorová
ze	z	k7c2	z
SZaŠ	SZaŠ	k1gFnSc2	SZaŠ
a	a	k8xC	a
SOU	sou	k1gInSc2	sou
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Alena	Alena	k1gFnSc1	Alena
Vlachová	Vlachová	k1gFnSc1	Vlachová
<g/>
,	,	kIx,	,
SZaŠ	SZaŠ	k1gFnSc1	SZaŠ
Děčín-Libverda	Děčín-Libverda	k1gFnSc1	Děčín-Libverda
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
díla	dílo	k1gNnPc1	dílo
oceněna	ocenit	k5eAaPmNgFnS	ocenit
druhou	druhý	k4xOgFnSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
tématech	téma	k1gNnPc6	téma
dámský	dámský	k2eAgInSc4d1	dámský
klobouk	klobouk	k1gInSc4	klobouk
a	a	k8xC	a
přízdoba	přízdoba	k1gFnSc1	přízdoba
dámských	dámský	k2eAgInPc2d1	dámský
šatů	šat	k1gInPc2	šat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
seniorů	senior	k1gMnPc2	senior
byl	být	k5eAaImAgInS	být
nejlépe	dobře	k6eAd3	dobře
ohodnocen	ohodnocen	k2eAgMnSc1d1	ohodnocen
Petr	Petr	k1gMnSc1	Petr
Kopáč	kopáč	k1gMnSc1	kopáč
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgFnSc1d1	zastupující
Holandské	holandský	k2eAgFnPc4d1	holandská
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ing.	ing.	kA	ing.
Václava	Václava	k1gFnSc1	Václava
Lojdová	Lojdová	k1gFnSc1	Lojdová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsadila	obsadit	k5eAaPmAgFnS	obsadit
druhé	druhý	k4xOgNnSc4	druhý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
pozici	pozice	k1gFnSc4	pozice
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
Petr	Petr	k1gMnSc1	Petr
Matuška	Matuška	k1gMnSc1	Matuška
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Květiny	květina	k1gFnSc2	květina
Petr	Petr	k1gMnSc1	Petr
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
hodnoceny	hodnocen	k2eAgInPc1d1	hodnocen
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
rovněž	rovněž	k9	rovněž
práce	práce	k1gFnPc4	práce
Renaty	Renata	k1gFnSc2	Renata
Kroutilové	Kroutilové	k2eAgFnSc6d1	Kroutilové
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Květiny	květina	k1gFnSc2	květina
keramika	keramika	k1gFnSc1	keramika
–	–	k?	–
Huszárová	Huszárový	k2eAgFnSc1d1	Huszárová
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
ing.	ing.	kA	ing.
Marie	Maria	k1gFnSc2	Maria
Bittnerové	Bittnerová	k1gFnSc2	Bittnerová
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Bohemiaseed	Bohemiaseed	k1gMnSc1	Bohemiaseed
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2013	[number]	k4	2013
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
úkoly	úkol	k1gInPc4	úkol
spočívaly	spočívat	k5eAaImAgFnP	spočívat
v	v	k7c6	v
smuteční	smuteční	k2eAgFnSc6d1	smuteční
přízdobě	přízdoba	k1gFnSc6	přízdoba
jednoho	jeden	k4xCgInSc2	jeden
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
zhotovení	zhotovení	k1gNnSc4	zhotovení
tradičního	tradiční	k2eAgInSc2d1	tradiční
květinového	květinový	k2eAgInSc2d1	květinový
kříže	kříž	k1gInSc2	kříž
do	do	k7c2	do
podložky	podložka	k1gFnSc2	podložka
oasis	oasis	k1gFnSc2	oasis
a	a	k8xC	a
zhotovení	zhotovení	k1gNnSc2	zhotovení
pohřebního	pohřební	k2eAgInSc2d1	pohřební
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc4	soutěž
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
13	[number]	k4	13
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
zařazených	zařazený	k2eAgFnPc2d1	zařazená
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
junior	junior	k1gMnSc1	junior
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
18	[number]	k4	18
soutěžících	soutěžící	k1gMnPc2	soutěžící
zařazených	zařazený	k2eAgMnPc2d1	zařazený
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
senior	senior	k1gMnSc1	senior
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Vítězem	vítěz	k1gMnSc7	vítěz
juniorské	juniorský	k2eAgFnSc2d1	juniorská
kategorie	kategorie	k1gFnSc2	kategorie
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
Květinový	květinový	k2eAgInSc1d1	květinový
ateliér	ateliér	k1gInSc1	ateliér
V	v	k7c6	v
Ráji	rája	k1gFnSc6	rája
<g/>
,	,	kIx,	,
Lázně	lázeň	k1gFnPc1	lázeň
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
porota	porota	k1gFnSc1	porota
ocenila	ocenit	k5eAaPmAgFnS	ocenit
práce	práce	k1gFnPc4	práce
firmy	firma	k1gFnSc2	firma
Art	Art	k1gFnSc2	Art
de	de	k?	de
Fleurs	Fleurs	k1gInSc1	Fleurs
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
porota	porota	k1gFnSc1	porota
přisoudila	přisoudit	k5eAaPmAgFnS	přisoudit
firmě	firma	k1gFnSc3	firma
Květiny	květina	k1gFnPc4	květina
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
uvedené	uvedený	k2eAgNnSc4d1	uvedené
ocenění	ocenění	k1gNnSc4	ocenění
získala	získat	k5eAaPmAgFnS	získat
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
zahradnická	zahradnický	k2eAgFnSc1d1	zahradnická
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
první	první	k4xOgNnPc1	první
místo	místo	k1gNnSc1	místo
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
smuteční	smuteční	k2eAgInSc4d1	smuteční
věnec	věnec	k1gInSc4	věnec
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zahradnická	zahradnická	k1gFnSc1	zahradnická
Piešťany	Piešťany	k1gInPc4	Piešťany
za	za	k7c4	za
tutéž	týž	k3xTgFnSc4	týž
práci	práce	k1gFnSc4	práce
třetí	třetí	k4xOgFnSc4	třetí
místo	místo	k6eAd1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
seniorské	seniorský	k2eAgFnSc2d1	seniorská
kategorie	kategorie	k1gFnSc2	kategorie
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Pavla	Pavla	k1gFnSc1	Pavla
Plevová	Plevová	k1gFnSc1	Plevová
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
porota	porota	k1gFnSc1	porota
přisoudila	přisoudit	k5eAaPmAgFnS	přisoudit
pracovnice	pracovnice	k1gFnSc1	pracovnice
Klára	Klára	k1gFnSc1	Klára
Záleská	záleský	k2eAgFnSc1d1	Záleská
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Květiny	Květin	k2eAgFnSc2d1	Květina
Miluše	Miluše	k1gFnSc2	Miluše
Huszárová	Huszárový	k2eAgFnSc1d1	Huszárová
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
místem	místem	k6eAd1	místem
práce	práce	k1gFnSc1	práce
porota	porota	k1gFnSc1	porota
ocenila	ocenit	k5eAaPmAgFnS	ocenit
Jana	Jan	k1gMnSc4	Jan
Milta	Milt	k1gMnSc4	Milt
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Květiny	květina	k1gFnSc2	květina
Milt	Milt	k1gInSc1	Milt
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
Lukáš	Lukáš	k1gMnSc1	Lukáš
Kouřil	Kouřil	k1gMnSc1	Kouřil
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Květiny	květina	k1gFnPc1	květina
Oxalis	Oxalis	k1gFnPc1	Oxalis
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
druhé	druhý	k4xOgNnSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
přízdoba	přízdoba	k1gFnSc1	přízdoba
jednoho	jeden	k4xCgInSc2	jeden
květu	květ	k1gInSc2	květ
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zpracování	zpracování	k1gNnSc4	zpracování
tématu	téma	k1gNnSc2	téma
"	"	kIx"	"
<g/>
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Peter	Peter	k1gMnSc1	Peter
Nagy	Naga	k1gFnSc2	Naga
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
(	(	kIx(	(
<g/>
Hodmezövásárhely	Hodmezövásárhela	k1gFnSc2	Hodmezövásárhela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Holišová	Holišový	k2eAgFnSc1d1	Holišová
ze	z	k7c2	z
zahradnické	zahradnický	k2eAgFnSc2d1	zahradnická
fakulty	fakulta	k1gFnSc2	fakulta
VŠ	vš	k0	vš
Lednice	Lednice	k1gFnPc4	Lednice
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
ohodnocena	ohodnocen	k2eAgMnSc4d1	ohodnocen
druhým	druhý	k4xOgNnSc7	druhý
místem	místo	k1gNnSc7	místo
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
přízdoba	přízdoba	k1gFnSc1	přízdoba
jednoho	jeden	k4xCgInSc2	jeden
květu	květ	k1gInSc2	květ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
uroda	uroda	k1gFnSc1	uroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
růže	růže	k1gFnSc1	růže
2012	[number]	k4	2012
</s>
</p>
