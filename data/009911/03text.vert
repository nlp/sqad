<p>
<s>
Schismatrix	Schismatrix	k1gInSc1	Schismatrix
je	být	k5eAaImIp3nS	být
sci-fi	scii	k1gFnSc4	sci-fi
román	román	k1gInSc1	román
Bruce	Bruce	k1gMnSc1	Bruce
Sterlinga	Sterlinga	k1gFnSc1	Sterlinga
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozdělené	rozdělená	k1gFnPc4	rozdělená
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
tábory	tábor	k1gInPc4	tábor
<g/>
:	:	kIx,	:
Tvárné	tvárný	k2eAgFnPc4d1	tvárná
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
genetická	genetický	k2eAgNnPc4d1	genetické
vylepšení	vylepšení	k1gNnPc4	vylepšení
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mechanisty	mechanista	k1gMnPc4	mechanista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
protetiku	protetika	k1gFnSc4	protetika
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
frakce	frakce	k1gFnPc1	frakce
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
osudem	osud	k1gInSc7	osud
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
plány	plán	k1gInPc1	plán
však	však	k9	však
zhatí	zhatit	k5eAaPmIp3nP	zhatit
příchod	příchod	k1gInSc4	příchod
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
druhu	druh	k1gInSc2	druh
Investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
zavedou	zavést	k5eAaPmIp3nP	zavést
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
za	za	k7c4	za
jakou	jaký	k3yIgFnSc4	jaký
cenu	cena	k1gFnSc4	cena
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Tomuto	tento	k3xDgInSc3	tento
románu	román	k1gInSc3	román
předcházela	předcházet	k5eAaImAgFnS	předcházet
pětice	pětice	k1gFnSc1	pětice
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vydána	vydán	k2eAgFnSc1d1	vydána
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Schismatrix	Schismatrix	k1gInSc4	Schismatrix
Plus	plus	k1gNnSc2	plus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bruce	Bruko	k6eAd1	Bruko
Sterling	sterling	k1gInSc1	sterling
si	se	k3xPyFc3	se
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
váží	vážit	k5eAaImIp3nS	vážit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
