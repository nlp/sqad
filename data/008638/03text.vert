<p>
<s>
Marson	Marson	k1gInSc1	Marson
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Marne	Marn	k1gInSc5	Marn
v	v	k7c6	v
regionu	region	k1gInSc6	region
Grand	grand	k1gMnSc1	grand
Est	Est	k1gMnSc1	Est
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
296	[number]	k4	296
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Coupéville	Coupéville	k1gFnSc1	Coupéville
<g/>
,	,	kIx,	,
Courtisols	Courtisols	k1gInSc1	Courtisols
<g/>
,	,	kIx,	,
Dampierre-sur-Moivre	Dampierreur-Moivr	k1gMnSc5	Dampierre-sur-Moivr
<g/>
,	,	kIx,	,
Francheville	Franchevill	k1gMnPc4	Franchevill
<g/>
,	,	kIx,	,
Chepy	Chep	k1gMnPc4	Chep
<g/>
,	,	kIx,	,
Pogny	Pogn	k1gMnPc4	Pogn
<g/>
,	,	kIx,	,
Poix	Poix	k1gInSc1	Poix
<g/>
,	,	kIx,	,
Saint-Germain-la-Ville	Saint-Germaina-Ville	k1gInSc1	Saint-Germain-la-Ville
<g/>
,	,	kIx,	,
Saint-Jean-sur-Moivre	Saint-Jeanur-Moivr	k1gMnSc5	Saint-Jean-sur-Moivr
<g/>
,	,	kIx,	,
Vésigneul-sur-Marne	Vésigneulur-Marn	k1gInSc5	Vésigneul-sur-Marn
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Marne	Marn	k1gMnSc5	Marn
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marson	Marsona	k1gFnPc2	Marsona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
