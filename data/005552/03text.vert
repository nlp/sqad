<s>
Mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
mobil	mobil	k1gInSc1	mobil
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
telefonní	telefonní	k2eAgInPc4d1	telefonní
hovory	hovor	k1gInPc4	hovor
jako	jako	k8xS	jako
normální	normální	k2eAgInSc4d1	normální
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
uživatel	uživatel	k1gMnSc1	uživatel
však	však	k9	však
není	být	k5eNaImIp3nS	být
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končí	končit	k5eAaImIp3nS	končit
telefonní	telefonní	k2eAgFnSc1d1	telefonní
přípojka	přípojka	k1gFnSc1	přípojka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
připojení	připojení	k1gNnSc4	připojení
do	do	k7c2	do
telefonní	telefonní	k2eAgFnSc2d1	telefonní
sítě	síť	k1gFnSc2	síť
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
Celulární	celulární	k2eAgInSc4d1	celulární
(	(	kIx(	(
<g/>
buňkové	buňkový	k2eAgInPc4d1	buňkový
<g/>
)	)	kIx)	)
telefony	telefon	k1gInPc1	telefon
–	–	k?	–
připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
celulárních	celulární	k2eAgFnPc2d1	celulární
rádiových	rádiový	k2eAgFnPc2d1	rádiová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
veřejných	veřejný	k2eAgFnPc2d1	veřejná
pozemních	pozemní	k2eAgFnPc2d1	pozemní
mobilních	mobilní	k2eAgFnPc2d1	mobilní
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
Public	publicum	k1gNnPc2	publicum
Land	Land	k1gMnSc1	Land
Mobile	mobile	k1gNnSc2	mobile
Network	network	k1gInSc1	network
–	–	k?	–
PLMN	PLMN	kA	PLMN
<g/>
)	)	kIx)	)
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
hustěji	husto	k6eAd2	husto
osídlené	osídlený	k2eAgFnPc1d1	osídlená
oblasti	oblast	k1gFnPc1	oblast
většiny	většina	k1gFnSc2	většina
zemí	zem	k1gFnPc2	zem
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
díky	díky	k7c3	díky
roamingovým	roamingový	k2eAgFnPc3d1	roamingová
smlouvám	smlouva	k1gFnPc3	smlouva
lze	lze	k6eAd1	lze
většinou	většina	k1gFnSc7	většina
komunikovat	komunikovat	k5eAaImF	komunikovat
i	i	k9	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
;	;	kIx,	;
překážkou	překážka	k1gFnSc7	překážka
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nekompatibilita	nekompatibilita	k1gFnSc1	nekompatibilita
různých	různý	k2eAgInPc2d1	různý
systémů	systém	k1gInPc2	systém
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
(	(	kIx(	(
<g/>
GSM	GSM	kA	GSM
proti	proti	k7c3	proti
CDMA	CDMA	kA	CDMA
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Satelitní	satelitní	k2eAgInPc1d1	satelitní
telefony	telefon	k1gInPc1	telefon
–	–	k?	–
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
i	i	k9	i
z	z	k7c2	z
odlehlých	odlehlý	k2eAgNnPc2d1	odlehlé
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
pokrytí	pokrytí	k1gNnSc1	pokrytí
buňkovými	buňkový	k2eAgFnPc7d1	buňková
sítěmi	síť	k1gFnPc7	síť
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
telekomunikační	telekomunikační	k2eAgFnSc7d1	telekomunikační
družicí	družice	k1gFnSc7	družice
(	(	kIx(	(
<g/>
satelitem	satelit	k1gInSc7	satelit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
použití	použití	k1gNnSc2	použití
volný	volný	k2eAgInSc4d1	volný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
Bezdrátové	bezdrátový	k2eAgInPc1d1	bezdrátový
telefony	telefon	k1gInPc1	telefon
–	–	k?	–
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
domě	dům	k1gInSc6	dům
nebo	nebo	k8xC	nebo
pozemku	pozemek	k1gInSc6	pozemek
<g/>
;	;	kIx,	;
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
se	se	k3xPyFc4	se
základnovou	základnový	k2eAgFnSc7d1	základnová
stanicí	stanice	k1gFnSc7	stanice
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
;	;	kIx,	;
základnová	základnový	k2eAgFnSc1d1	základnová
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
vlastněna	vlastněn	k2eAgFnSc1d1	vlastněna
účastníkem	účastník	k1gMnSc7	účastník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
připojenou	připojený	k2eAgFnSc4d1	připojená
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
běžnou	běžný	k2eAgFnSc4d1	běžná
pevnou	pevný	k2eAgFnSc4d1	pevná
telefonní	telefonní	k2eAgFnSc4d1	telefonní
linku	linka	k1gFnSc4	linka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
Protože	protože	k8xS	protože
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
používá	používat	k5eAaImIp3nS	používat
buňkovou	buňkový	k2eAgFnSc4d1	buňková
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
GSM	GSM	kA	GSM
UMTS	UMTS	kA	UMTS
<g/>
,	,	kIx,	,
CDMA	CDMA	kA	CDMA
<g/>
,	,	kIx,	,
LTE	LTE	kA	LTE
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
starší	starý	k2eAgInPc1d2	starší
NMT	NMT	kA	NMT
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
AMPS	AMPS	kA	AMPS
<g/>
)	)	kIx)	)
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc1	slovo
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
mobile	mobile	k1gNnSc1	mobile
phone	phonout	k5eAaImIp3nS	phonout
<g/>
)	)	kIx)	)
a	a	k8xC	a
buňkový	buňkový	k2eAgInSc4d1	buňkový
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
cellphone	cellphon	k1gInSc5	cellphon
<g/>
)	)	kIx)	)
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
buňkový	buňkový	k2eAgInSc1d1	buňkový
telefon	telefon	k1gInSc1	telefon
prakticky	prakticky	k6eAd1	prakticky
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
přenosné	přenosný	k2eAgInPc4d1	přenosný
telefony	telefon	k1gInPc4	telefon
používající	používající	k2eAgInPc4d1	používající
jinou	jiný	k2eAgFnSc4d1	jiná
než	než	k8xS	než
buňkovou	buňkový	k2eAgFnSc4d1	buňková
technologii	technologie	k1gFnSc4	technologie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
názvy	název	k1gInPc1	název
satelitní	satelitní	k2eAgInSc1d1	satelitní
telefon	telefon	k1gInSc4	telefon
a	a	k8xC	a
bezdrátový	bezdrátový	k2eAgInSc1d1	bezdrátový
telefon	telefon	k1gInSc1	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nejen	nejen	k6eAd1	nejen
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
telefonní	telefonní	k2eAgFnSc7d1	telefonní
sítí	síť	k1gFnSc7	síť
přímo	přímo	k6eAd1	přímo
volbou	volba	k1gFnSc7	volba
telefonního	telefonní	k2eAgNnSc2d1	telefonní
čísla	číslo	k1gNnSc2	číslo
na	na	k7c6	na
vestavěné	vestavěný	k2eAgFnSc6d1	vestavěná
klávesnici	klávesnice	k1gFnSc6	klávesnice
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
dalších	další	k2eAgFnPc2d1	další
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
SMS	SMS	kA	SMS
<g/>
,	,	kIx,	,
MMS	MMS	kA	MMS
<g/>
,	,	kIx,	,
WAP	WAP	kA	WAP
a	a	k8xC	a
připojení	připojení	k1gNnSc2	připojení
na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrozšířenějším	rozšířený	k2eAgNnPc3d3	nejrozšířenější
elektronickým	elektronický	k2eAgNnPc3d1	elektronické
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
výrobci	výrobce	k1gMnPc1	výrobce
je	on	k3xPp3gMnPc4	on
vybavují	vybavovat	k5eAaImIp3nP	vybavovat
také	také	k9	také
dalšími	další	k2eAgFnPc7d1	další
funkcemi	funkce	k1gFnPc7	funkce
dříve	dříve	k6eAd2	dříve
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
u	u	k7c2	u
Personal	Personal	k1gFnSc2	Personal
Digital	Digital	kA	Digital
Assistant	Assistant	k1gMnSc1	Assistant
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
vybavené	vybavený	k2eAgMnPc4d1	vybavený
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
nahradit	nahradit	k5eAaPmF	nahradit
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
producenty	producent	k1gMnPc4	producent
mobilů	mobil	k1gInPc2	mobil
patří	patřit	k5eAaImIp3nP	patřit
Alcatel	Alcatel	kA	Alcatel
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
BenQ	BenQ	k1gFnSc1	BenQ
Mobile	mobile	k1gNnSc2	mobile
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Siemens	siemens	k1gInSc1	siemens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blackberry	Blackberr	k1gInPc1	Blackberr
<g/>
,	,	kIx,	,
HTC	HTC	kA	HTC
<g/>
,	,	kIx,	,
Huawei	Huawei	k1gNnSc2	Huawei
<g/>
,	,	kIx,	,
LG	LG	kA	LG
Electronics	Electronics	k1gInSc1	Electronics
<g/>
,	,	kIx,	,
Motorola	Motorola	kA	Motorola
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
mobilní	mobilní	k2eAgMnPc1d1	mobilní
operátoři	operátor	k1gMnPc1	operátor
jako	jako	k8xS	jako
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
si	se	k3xPyFc3	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
vyrábět	vyrábět	k5eAaImF	vyrábět
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dodávají	dodávat	k5eAaImIp3nP	dodávat
pod	pod	k7c7	pod
vlastní	vlastní	k2eAgFnSc7d1	vlastní
značkou	značka	k1gFnSc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgMnPc7d1	jediný
českými	český	k2eAgMnPc7d1	český
producenty	producent	k1gMnPc7	producent
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
jsou	být	k5eAaImIp3nP	být
firmy	firma	k1gFnPc1	firma
Verzo	Verza	k1gFnSc5	Verza
a	a	k8xC	a
Jablotron	Jablotron	k1gInSc4	Jablotron
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
sestavila	sestavit	k5eAaPmAgFnS	sestavit
Motorola	Motorola	kA	Motorola
</s>
<s>
Kromě	kromě	k7c2	kromě
mobilních	mobilní	k2eAgInPc2d1	mobilní
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
neveřejné	veřejný	k2eNgInPc1d1	neveřejný
komunikační	komunikační	k2eAgInPc1d1	komunikační
systémy	systém	k1gInPc1	systém
–	–	k?	–
profesionální	profesionální	k2eAgNnSc4d1	profesionální
mobilní	mobilní	k2eAgNnSc4d1	mobilní
rádio	rádio	k1gNnSc4	rádio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prodejnách	prodejna	k1gFnPc6	prodejna
mobilů	mobil	k1gInPc2	mobil
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
věrné	věrný	k2eAgFnSc2d1	věrná
makety	maketa	k1gFnSc2	maketa
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vynález	vynález	k1gInSc1	vynález
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telefonie	telefonie	k1gFnSc2	telefonie
spadá	spadat	k5eAaImIp3nS	spadat
již	již	k6eAd1	již
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
prodávány	prodáván	k2eAgFnPc1d1	prodávána
jako	jako	k8xS	jako
vybavení	vybavení	k1gNnSc1	vybavení
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
skutečně	skutečně	k6eAd1	skutečně
přenosné	přenosný	k2eAgInPc1d1	přenosný
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
kapesní	kapesní	k2eAgInPc4d1	kapesní
<g/>
)	)	kIx)	)
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
systému	systém	k1gInSc2	systém
GSM	GSM	kA	GSM
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
bude	být	k5eAaImBp3nS	být
i	i	k9	i
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc2	pokles
cen	cena	k1gFnPc2	cena
tarifů	tarif	k1gInPc2	tarif
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc2	dostupnost
předplacených	předplacený	k2eAgFnPc2d1	předplacená
karet	kareta	k1gFnPc2	kareta
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
takovému	takový	k3xDgNnSc3	takový
rozšíření	rozšíření	k1gNnSc3	rozšíření
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
mobilních	mobilní	k2eAgInPc2d1	mobilní
účastníků	účastník	k1gMnPc2	účastník
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
překonal	překonat	k5eAaPmAgInS	překonat
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
pevných	pevný	k2eAgFnPc2d1	pevná
telefonních	telefonní	k2eAgFnPc2d1	telefonní
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1	mobilní
telefonie	telefonie	k1gFnSc1	telefonie
přinesla	přinést	k5eAaPmAgFnS	přinést
možnost	možnost	k1gFnSc4	možnost
používání	používání	k1gNnSc3	používání
textových	textový	k2eAgFnPc2d1	textová
a	a	k8xC	a
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
MP3	MP3	k1gFnPc4	MP3
apod.	apod.	kA	apod.
Mobilní	mobilní	k2eAgFnPc4d1	mobilní
sítě	síť	k1gFnPc4	síť
mohou	moct	k5eAaImIp3nP	moct
často	často	k6eAd1	často
profitovat	profitovat	k5eAaBmF	profitovat
i	i	k9	i
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
náklady	náklad	k1gInPc1	náklad
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
většinou	většinou	k6eAd1	většinou
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
hovorů	hovor	k1gInPc2	hovor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
telefonie	telefonie	k1gFnSc1	telefonie
pevných	pevný	k2eAgFnPc2d1	pevná
linek	linka	k1gFnPc2	linka
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgInPc4d2	vyšší
náklady	náklad	k1gInPc4	náklad
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
účastníkem	účastník	k1gMnSc7	účastník
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
rozvinutější	rozvinutý	k2eAgFnSc2d2	rozvinutější
části	část	k1gFnSc2	část
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
jsou	být	k5eAaImIp3nP	být
mobily	mobil	k1gInPc1	mobil
už	už	k6eAd1	už
prakticky	prakticky	k6eAd1	prakticky
univerzální	univerzální	k2eAgInPc1d1	univerzální
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
a	a	k8xC	a
i	i	k9	i
dětská	dětský	k2eAgFnSc1d1	dětská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Trochu	trochu	k6eAd1	trochu
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
USA	USA	kA	USA
–	–	k?	–
i	i	k9	i
když	když	k8xS	když
široce	široko	k6eAd1	široko
používané	používaný	k2eAgInPc1d1	používaný
<g/>
,	,	kIx,	,
tržní	tržní	k2eAgFnSc1d1	tržní
penetrace	penetrace	k1gFnSc1	penetrace
(	(	kIx(	(
<g/>
průnik	průnik	k1gInSc1	průnik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
asi	asi	k9	asi
66	[number]	k4	66
procent	procento	k1gNnPc2	procento
populace	populace	k1gFnSc2	populace
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
patří	patřit	k5eAaImIp3nS	patřit
nekompletní	kompletní	k2eNgNnSc1d1	nekompletní
pokrytí	pokrytí	k1gNnSc1	pokrytí
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
technických	technický	k2eAgInPc2d1	technický
standardů	standard	k1gInPc2	standard
(	(	kIx(	(
<g/>
Mnoho	mnoho	k4c4	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
asijské	asijský	k2eAgFnPc1d1	asijská
země	zem	k1gFnPc1	zem
přijaly	přijmout	k5eAaPmAgFnP	přijmout
GSM	GSM	kA	GSM
standard	standard	k1gInSc1	standard
zákonem	zákon	k1gInSc7	zákon
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnPc1d1	jiná
asijské	asijský	k2eAgFnPc1d1	asijská
země	zem	k1gFnPc1	zem
přijaly	přijmout	k5eAaPmAgFnP	přijmout
standardy	standard	k1gInPc4	standard
CDMA	CDMA	kA	CDMA
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
i	i	k8xC	i
Kanadě	Kanada	k1gFnSc6	Kanada
podobný	podobný	k2eAgInSc1d1	podobný
zákon	zákon	k1gInSc1	zákon
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
si	se	k3xPyFc3	se
standard	standard	k1gInSc1	standard
vybere	vybrat	k5eAaPmIp3nS	vybrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgInPc1d1	vysoký
minimální	minimální	k2eAgInPc1d1	minimální
měsíční	měsíční	k2eAgInPc1d1	měsíční
poplatky	poplatek	k1gInPc1	poplatek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
30	[number]	k4	30
USD	USD	kA	USD
<g/>
)	)	kIx)	)
a	a	k8xC	a
dostupnost	dostupnost	k1gFnSc1	dostupnost
relativně	relativně	k6eAd1	relativně
levných	levný	k2eAgFnPc2d1	levná
pevných	pevný	k2eAgFnPc2d1	pevná
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
asi	asi	k9	asi
30	[number]	k4	30
USD	USD	kA	USD
za	za	k7c4	za
neomezené	omezený	k2eNgInPc4d1	neomezený
lokální	lokální	k2eAgInPc4d1	lokální
hovory	hovor	k1gInPc4	hovor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předplacené	předplacený	k2eAgInPc4d1	předplacený
telefonní	telefonní	k2eAgInPc4d1	telefonní
hovory	hovor	k1gInPc4	hovor
(	(	kIx(	(
<g/>
měsíční	měsíční	k2eAgInSc4d1	měsíční
paušál	paušál	k1gInSc4	paušál
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
předplacené	předplacený	k2eAgFnPc4d1	předplacená
karty	karta	k1gFnPc4	karta
pro	pro	k7c4	pro
mobily	mobil	k1gInPc4	mobil
<g/>
,	,	kIx,	,
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
USA	USA	kA	USA
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dražší	drahý	k2eAgFnPc1d2	dražší
než	než	k8xS	než
porovnatelné	porovnatelný	k2eAgFnPc1d1	porovnatelná
služby	služba	k1gFnPc1	služba
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácení	zkrácení	k1gNnSc1	zkrácení
telefonních	telefonní	k2eAgNnPc2d1	telefonní
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
neregionálních	regionální	k2eNgNnPc2d1	regionální
speciálních	speciální	k2eAgNnPc2d1	speciální
telefonních	telefonní	k2eAgNnPc2d1	telefonní
čísel	číslo	k1gNnPc2	číslo
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgFnPc4d1	mobilní
služby	služba	k1gFnPc4	služba
–	–	k?	–
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cenový	cenový	k2eAgInSc1d1	cenový
systém	systém	k1gInSc1	systém
používaný	používaný	k2eAgInSc1d1	používaný
jinde	jinde	k6eAd1	jinde
(	(	kIx(	(
<g/>
hovory	hovor	k1gInPc4	hovor
stojí	stát	k5eAaImIp3nP	stát
víc	hodně	k6eAd2	hodně
do	do	k7c2	do
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijímány	přijímán	k2eAgInPc1d1	přijímán
jsou	být	k5eAaImIp3nP	být
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
)	)	kIx)	)
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
uživatelé	uživatel	k1gMnPc1	uživatel
platí	platit	k5eAaImIp3nP	platit
za	za	k7c4	za
přicházející	přicházející	k2eAgInPc4d1	přicházející
hovory	hovor	k1gInPc4	hovor
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
od	od	k7c2	od
použití	použití	k1gNnSc2	použití
mobilů	mobil	k1gInPc2	mobil
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
technické	technický	k2eAgInPc1d1	technický
problémy	problém	k1gInPc1	problém
postihují	postihovat	k5eAaImIp3nP	postihovat
mobilní	mobilní	k2eAgFnSc4d1	mobilní
telefonii	telefonie	k1gFnSc4	telefonie
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
používá	používat	k5eAaImIp3nS	používat
stejnou	stejný	k2eAgFnSc4d1	stejná
směs	směs	k1gFnSc4	směs
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
standardů	standard	k1gInPc2	standard
jako	jako	k8xS	jako
USA	USA	kA	USA
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
též	též	k9	též
součástí	součást	k1gFnSc7	součást
severoamerického	severoamerický	k2eAgInSc2d1	severoamerický
plánu	plán	k1gInSc2	plán
číslování	číslování	k1gNnSc1	číslování
(	(	kIx(	(
<g/>
North	North	k1gInSc1	North
American	American	k1gInSc1	American
Numbering	Numbering	k1gInSc1	Numbering
Plan	plan	k1gInSc4	plan
–	–	k?	–
NANP	NANP	kA	NANP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mobily	mobil	k1gInPc1	mobil
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
v	v	k7c6	v
celulárních	celulární	k2eAgFnPc6d1	celulární
sítích	síť	k1gFnPc6	síť
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
standardní	standardní	k2eAgFnSc4d1	standardní
sadu	sada	k1gFnSc4	sada
služeb	služba	k1gFnPc2	služba
GSM	GSM	kA	GSM
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mobilům	mobil	k1gInPc3	mobil
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
vzájemně	vzájemně	k6eAd1	vzájemně
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
mobilu	mobil	k1gInSc2	mobil
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zvolit	zvolit	k5eAaPmF	zvolit
si	se	k3xPyFc3	se
mobilního	mobilní	k2eAgMnSc4d1	mobilní
operátora	operátor	k1gMnSc4	operátor
(	(	kIx(	(
<g/>
poskytovatele	poskytovatel	k1gMnSc4	poskytovatel
přenosu	přenos	k1gInSc2	přenos
<g/>
)	)	kIx)	)
a	a	k8xC	a
zařídit	zařídit	k5eAaPmF	zařídit
si	se	k3xPyFc3	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
GSM	GSM	kA	GSM
operátor	operátor	k1gInSc4	operátor
vydá	vydat	k5eAaPmIp3nS	vydat
SIM	SIM	kA	SIM
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
unikátní	unikátní	k2eAgInPc4d1	unikátní
účastnické	účastnický	k2eAgInPc4d1	účastnický
a	a	k8xC	a
autentizační	autentizační	k2eAgInPc4d1	autentizační
(	(	kIx(	(
<g/>
ověřovací	ověřovací	k2eAgInPc4d1	ověřovací
<g/>
)	)	kIx)	)
parametry	parametr	k1gInPc1	parametr
pro	pro	k7c4	pro
daného	daný	k2eAgMnSc4d1	daný
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
operátor	operátor	k1gInSc1	operátor
vloží	vložit	k5eAaPmIp3nS	vložit
zákazníkův	zákazníkův	k2eAgInSc1d1	zákazníkův
identifikátor	identifikátor	k1gInSc1	identifikátor
mikrotelefonu	mikrotelefon	k1gInSc2	mikrotelefon
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
účastnické	účastnický	k2eAgFnSc2d1	účastnická
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mikrotelefon	mikrotelefon	k1gInSc1	mikrotelefon
mohl	moct	k5eAaImAgInS	moct
realizovat	realizovat	k5eAaBmF	realizovat
hovory	hovor	k1gInPc4	hovor
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vložení	vložení	k1gNnSc6	vložení
SIM	SIM	kA	SIM
karty	karta	k1gFnSc2	karta
do	do	k7c2	do
mobilu	mobil	k1gInSc2	mobil
jsou	být	k5eAaImIp3nP	být
služby	služba	k1gFnPc1	služba
přístupné	přístupný	k2eAgFnPc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Mobily	mobil	k1gInPc1	mobil
nepodporují	podporovat	k5eNaImIp3nP	podporovat
jen	jen	k9	jen
hlasové	hlasový	k2eAgInPc4d1	hlasový
hovory	hovor	k1gInPc4	hovor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
data	datum	k1gNnPc4	datum
a	a	k8xC	a
faxy	fax	k1gInPc4	fax
<g/>
,	,	kIx,	,
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
krátké	krátký	k2eAgFnPc4d1	krátká
zprávy	zpráva	k1gFnPc4	zpráva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
textové	textový	k2eAgFnPc4d1	textová
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
v	v	k7c6	v
článku	článek	k1gInSc6	článek
SMS	SMS	kA	SMS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poskytovat	poskytovat	k5eAaImF	poskytovat
kompletní	kompletní	k2eAgInSc4d1	kompletní
internetový	internetový	k2eAgInSc4d1	internetový
přístup	přístup	k1gInSc4	přístup
použitím	použití	k1gNnSc7	použití
technologií	technologie	k1gFnPc2	technologie
jako	jako	k8xC	jako
GPRS	GPRS	kA	GPRS
<g/>
,	,	kIx,	,
EDGE	EDGE	kA	EDGE
<g/>
,	,	kIx,	,
HSDPA	HSDPA	kA	HSDPA
<g/>
,	,	kIx,	,
LTE	LTE	kA	LTE
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mobily	mobil	k1gInPc1	mobil
mají	mít	k5eAaImIp3nP	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
stáhnout	stáhnout	k5eAaPmF	stáhnout
si	se	k3xPyFc3	se
různé	různý	k2eAgFnPc4d1	různá
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
např	např	kA	např
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
podporují	podporovat	k5eAaImIp3nP	podporovat
službu	služba	k1gFnSc4	služba
"	"	kIx"	"
<g/>
roaming	roaming	k1gInSc4	roaming
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cestování	cestování	k1gNnSc1	cestování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
stejnému	stejný	k2eAgInSc3d1	stejný
mobilu	mobil	k1gInSc3	mobil
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgMnPc1d1	mobilní
operátoři	operátor	k1gMnPc1	operátor
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
roamingu	roaming	k1gInSc6	roaming
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nových	nový	k2eAgInPc2d1	nový
mobilů	mobil	k1gInPc2	mobil
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
též	též	k9	též
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
posílání	posílání	k1gNnSc4	posílání
a	a	k8xC	a
příjem	příjem	k1gInSc4	příjem
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
takzvaně	takzvaně	k6eAd1	takzvaně
MMS	MMS	kA	MMS
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
Sha-Mail	Sha-Mail	k1gMnSc1	Sha-Mail
umožňující	umožňující	k2eAgNnSc4d1	umožňující
zasílání	zasílání	k1gNnSc4	zasílání
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
představený	představený	k2eAgInSc4d1	představený
firmou	firma	k1gFnSc7	firma
J-Phone	J-Phon	k1gInSc5	J-Phon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
zabudovaný	zabudovaný	k2eAgInSc4d1	zabudovaný
digitální	digitální	k2eAgInSc4d1	digitální
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
určité	určitý	k2eAgFnPc4d1	určitá
otázky	otázka	k1gFnPc4	otázka
kolem	kolem	k7c2	kolem
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
proto	proto	k8xC	proto
na	na	k7c4	na
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
zakázala	zakázat	k5eAaPmAgFnS	zakázat
prodej	prodej	k1gFnSc1	prodej
mobilů	mobil	k1gInPc2	mobil
s	s	k7c7	s
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
a	a	k8xC	a
povolila	povolit	k5eAaPmAgFnS	povolit
jej	on	k3xPp3gInSc4	on
až	až	k6eAd1	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
země	zem	k1gFnPc4	zem
poutníkům	poutník	k1gMnPc3	poutník
na	na	k7c4	na
Hadždž	Hadždž	k1gFnSc4	Hadždž
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
mobil	mobil	k1gInSc4	mobil
s	s	k7c7	s
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
požádala	požádat	k5eAaPmAgFnS	požádat
výrobce	výrobce	k1gMnPc4	výrobce
mobilů	mobil	k1gInPc2	mobil
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zajistili	zajistit	k5eAaPmAgMnP	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
snímku	snímek	k1gInSc6	snímek
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Přijímače	přijímač	k1gInPc1	přijímač
GPS	GPS	kA	GPS
(	(	kIx(	(
<g/>
lokalizační	lokalizační	k2eAgFnSc2d1	lokalizační
služby	služba	k1gFnSc2	služba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
integrovány	integrován	k2eAgInPc1d1	integrován
nebo	nebo	k8xC	nebo
připojené	připojený	k2eAgInPc1d1	připojený
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přes	přes	k7c4	přes
Bluetooth	Bluetooth	k1gInSc4	Bluetooth
<g/>
)	)	kIx)	)
k	k	k7c3	k
mobilům	mobil	k1gInPc3	mobil
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
jen	jen	k9	jen
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
pracovníkům	pracovník	k1gMnPc3	pracovník
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
službám	služba	k1gFnPc3	služba
pro	pro	k7c4	pro
odtahování	odtahování	k1gNnSc4	odtahování
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
;	;	kIx,	;
GPS	GPS	kA	GPS
v	v	k7c6	v
mobilu	mobil	k1gInSc6	mobil
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
navigaci	navigace	k1gFnSc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
dnešních	dnešní	k2eAgInPc2d1	dnešní
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
přehrávání	přehrávání	k1gNnSc2	přehrávání
hudby	hudba	k1gFnSc2	hudba
ve	v	k7c6	v
formátech	formát	k1gInPc6	formát
MP3	MP3	k1gFnSc1	MP3
a	a	k8xC	a
WMA	WMA	kA	WMA
<g/>
,	,	kIx,	,
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
sluchátka	sluchátko	k1gNnPc4	sluchátko
<g/>
,	,	kIx,	,
kvalitním	kvalitní	k2eAgMnPc3d1	kvalitní
integrovaným	integrovaný	k2eAgMnPc3d1	integrovaný
reproduktorům	reproduktor	k1gInPc3	reproduktor
<g/>
,	,	kIx,	,
barevnému	barevný	k2eAgInSc3d1	barevný
displeji	displej	k1gInSc3	displej
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnPc4	možnost
přehrávat	přehrávat	k5eAaImF	přehrávat
video	video	k1gNnSc4	video
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
paměťových	paměťový	k2eAgFnPc2d1	paměťová
karet	kareta	k1gFnPc2	kareta
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
MP3	MP3	k1gMnSc4	MP3
nebo	nebo	k8xC	nebo
MP4	MP4	k1gMnSc4	MP4
přehrávač	přehrávač	k1gInSc4	přehrávač
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
velikostním	velikostní	k2eAgNnSc6d1	velikostní
provedení	provedení	k1gNnSc6	provedení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
kladen	kladen	k2eAgInSc1d1	kladen
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
tloušťku	tloušťka	k1gFnSc4	tloušťka
či	či	k8xC	či
velikost	velikost	k1gFnSc4	velikost
mobilu	mobil	k1gInSc2	mobil
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc1d3	nejmenší
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
izraelská	izraelský	k2eAgFnSc1d1	izraelská
společnost	společnost	k1gFnSc1	společnost
Modu	modus	k1gInSc2	modus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mobil	mobil	k1gInSc1	mobil
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
7,2	[number]	k4	7,2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
3,76	[number]	k4	3,76
cm	cm	kA	cm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tlustý	tlustý	k2eAgMnSc1d1	tlustý
7,8	[number]	k4	7,8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1	běžná
funkce	funkce	k1gFnPc1	funkce
dnešních	dnešní	k2eAgInPc2d1	dnešní
mobilů	mobil	k1gInPc2	mobil
<g/>
:	:	kIx,	:
telefonování	telefonování	k1gNnSc1	telefonování
posílání	posílání	k1gNnPc2	posílání
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
SMS	SMS	kA	SMS
<g/>
)	)	kIx)	)
barevný	barevný	k2eAgInSc4d1	barevný
displej	displej	k1gInSc4	displej
vibrační	vibrační	k2eAgNnSc1d1	vibrační
zvonění	zvonění	k1gNnSc1	zvonění
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
budík	budík	k1gInSc4	budík
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
aplikacích	aplikace	k1gFnPc6	aplikace
(	(	kIx(	(
<g/>
app	app	k?	app
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
stáhnete	stáhnout	k5eAaPmIp2nP	stáhnout
do	do	k7c2	do
mobilu	mobil	k1gInSc2	mobil
<g/>
)	)	kIx)	)
posílání	posílání	k1gNnSc4	posílání
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
<g />
.	.	kIx.	.
</s>
<s>
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
MMS	MMS	kA	MMS
<g/>
)	)	kIx)	)
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
technologií	technologie	k1gFnPc2	technologie
GPRS	GPRS	kA	GPRS
<g/>
,	,	kIx,	,
EDGE	EDGE	kA	EDGE
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
HSDPA	HSDPA	kA	HSDPA
<g/>
,	,	kIx,	,
WIFI	WIFI	kA	WIFI
a	a	k8xC	a
nově	nově	k6eAd1	nově
i	i	k9	i
LTE	LTE	kA	LTE
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
funkce	funkce	k1gFnSc1	funkce
handsfree	handsfreat	k5eAaPmIp3nS	handsfreat
slot	slot	k1gInSc4	slot
na	na	k7c4	na
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
hudební	hudební	k2eAgInSc4d1	hudební
přehrávač	přehrávač	k1gInSc4	přehrávač
<g/>
,	,	kIx,	,
přehrávání	přehrávání	k1gNnSc4	přehrávání
<g />
.	.	kIx.	.
</s>
<s>
hudebních	hudební	k2eAgInPc2d1	hudební
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mp	mp	k?	mp
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
wma	wma	k?	wma
<g/>
,	,	kIx,	,
flac	flac	k1gFnSc1	flac
<g/>
)	)	kIx)	)
přehrávač	přehrávač	k1gInSc1	přehrávač
videa	video	k1gNnSc2	video
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
3	[number]	k4	3
<g/>
gp	gp	k?	gp
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
mp	mp	k?	mp
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
avi	avi	k?	avi
<g/>
,	,	kIx,	,
mkv	mkv	k?	mkv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Xvid	Xvid	k1gInSc1	Xvid
a	a	k8xC	a
Dvix	Dvix	k1gInSc1	Dvix
bluetooth	bluetootha	k1gFnPc2	bluetootha
(	(	kIx(	(
<g/>
nejnovější	nový	k2eAgFnSc1d3	nejnovější
4.0	[number]	k4	4.0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
digitální	digitální	k2eAgInSc1d1	digitální
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
(	(	kIx(	(
<g/>
rozlišení	rozlišení	k1gNnSc2	rozlišení
až	až	k9	až
41	[number]	k4	41
megapixelů	megapixel	k1gInPc2	megapixel
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgInSc1d1	digitální
zoom	zoom	k1gInSc1	zoom
<g/>
,	,	kIx,	,
přisvětlovací	přisvětlovací	k2eAgFnSc1d1	přisvětlovací
dioda	dioda	k1gFnSc1	dioda
nebo	nebo	k8xC	nebo
xenonový	xenonový	k2eAgInSc1d1	xenonový
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
pořizování	pořizování	k1gNnSc4	pořizování
a	a	k8xC	a
přehrávání	přehrávání	k1gNnSc4	přehrávání
videa	video	k1gNnSc2	video
až	až	k9	až
ve	v	k7c4	v
Full	Full	k1gInSc4	Full
HD	HD	kA	HD
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
4K	[number]	k4	4K
-	-	kIx~	-
UHD	UHD	kA	UHD
<g/>
)	)	kIx)	)
integrovaný	integrovaný	k2eAgMnSc1d1	integrovaný
GPS	GPS	kA	GPS
přijímač	přijímač	k1gInSc4	přijímač
FM	FM	kA	FM
rádio	rádio	k1gNnSc4	rádio
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
Android	android	k1gInSc1	android
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
)	)	kIx)	)
videohovory	videohovor	k1gInPc4	videohovor
micro	micro	k6eAd1	micro
USB	USB	kA	USB
konektor	konektor	k1gInSc4	konektor
dotykový	dotykový	k2eAgInSc1d1	dotykový
displej	displej	k1gInSc1	displej
AMOLED	AMOLED	kA	AMOLED
displej	displej	k1gInSc1	displej
<g/>
/	/	kIx~	/
super	super	k1gInSc1	super
AMOLED	AMOLED	kA	AMOLED
<g/>
/	/	kIx~	/
super	super	k1gInSc1	super
AMOLED	AMOLED	kA	AMOLED
PRO	pro	k7c4	pro
<g/>
/	/	kIx~	/
Super	super	k1gInSc4	super
AMOLED	AMOLED	kA	AMOLED
HD	HD	kA	HD
(	(	kIx(	(
<g/>
LED	LED	kA	LED
displej	displej	k1gInSc1	displej
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
matricí	matrice	k1gFnSc7	matrice
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInSc1d1	extrémní
kontrast	kontrast	k1gInSc1	kontrast
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
spotřeba	spotřeba	k1gFnSc1	spotřeba
<g/>
)	)	kIx)	)
připojení	připojení	k1gNnSc1	připojení
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
internetu	internet	k1gInSc2	internet
technologií	technologie	k1gFnSc7	technologie
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
videohovory	videohovora	k1gFnSc2	videohovora
NFC	NFC	kA	NFC
FM	FM	kA	FM
vysílač	vysílač	k1gMnSc1	vysílač
flash	flash	k1gMnSc1	flash
player	player	k1gMnSc1	player
proximity	proximita	k1gFnSc2	proximita
sensor	sensor	k1gMnSc1	sensor
multitouch	multitouch	k1gMnSc1	multitouch
kompas	kompas	k1gInSc4	kompas
3,5	[number]	k4	3,5
mm	mm	kA	mm
jack	jack	k6eAd1	jack
většinou	většinou	k6eAd1	většinou
přední	přední	k2eAgMnSc1d1	přední
=	=	kIx~	=
sekundární	sekundární	k2eAgFnSc1d1	sekundární
kamera	kamera	k1gFnSc1	kamera
4	[number]	k4	4
jádrové	jádrový	k2eAgInPc1d1	jádrový
procesory	procesor	k1gInPc1	procesor
Starší	starý	k2eAgInPc1d2	starší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
spíše	spíše	k9	spíše
nepoužívané	používaný	k2eNgFnPc4d1	nepoužívaná
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
infraport	infraport	k1gInSc1	infraport
zrcátko	zrcátko	k1gNnSc1	zrcátko
pro	pro	k7c4	pro
autoportrét	autoportrét	k1gInSc4	autoportrét
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
Symbian	Symbiany	k1gInPc2	Symbiany
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc4	mobile
3D	[number]	k4	3D
display	displaa	k1gFnSc2	displaa
-	-	kIx~	-
3D	[number]	k4	3D
ON	on	k3xPp3gInSc1	on
(	(	kIx(	(
<g/>
2	[number]	k4	2
displeje	displej	k1gInPc1	displej
umístěné	umístěný	k2eAgInPc1d1	umístěný
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
černobílý	černobílý	k2eAgInSc1d1	černobílý
a	a	k8xC	a
zapíná	zapínat	k5eAaImIp3nS	zapínat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
3D	[number]	k4	3D
modu	modus	k1gInSc2	modus
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
mobilů	mobil	k1gInPc2	mobil
HTC	HTC	kA	HTC
Evo	Eva	k1gFnSc5	Eva
3D	[number]	k4	3D
a	a	k8xC	a
LG	LG	kA	LG
Optimus	Optimus	k1gInSc4	Optimus
3	[number]	k4	3
<g/>
D.	D.	kA	D.
připojení	připojený	k2eAgMnPc1d1	připojený
k	k	k7c3	k
WAPu	WAPum	k1gNnSc3	WAPum
podpora	podpora	k1gFnSc1	podpora
JAVA	JAVA	kA	JAVA
her	hra	k1gFnPc2	hra
a	a	k8xC	a
JAVA	JAVA	kA	JAVA
aplikací	aplikace	k1gFnSc7	aplikace
Multimodální	Multimodální	k2eAgFnSc7d1	Multimodální
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
dvoupásmové	dvoupásmový	k2eAgNnSc4d1	dvoupásmové
–	–	k?	–
dualband	dualband	k1gInSc4	dualband
<g/>
,	,	kIx,	,
třípásmové	třípásmový	k2eAgFnPc1d1	třípásmová
triband	triband	k1gInSc4	triband
nebo	nebo	k8xC	nebo
čtyřpásmové	čtyřpásmový	k2eAgInPc4d1	čtyřpásmový
quadband	quadband	k1gInSc4	quadband
<g/>
)	)	kIx)	)
mobily	mobil	k1gInPc1	mobil
jsou	být	k5eAaImIp3nP	být
telefony	telefon	k1gInPc4	telefon
navržené	navržený	k2eAgInPc4d1	navržený
fungovat	fungovat	k5eAaImF	fungovat
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc3	jeden
radiové	radiový	k2eAgFnSc3d1	radiová
frekvenci	frekvence	k1gFnSc3	frekvence
GSM	GSM	kA	GSM
<g/>
.	.	kIx.	.
</s>
<s>
Multimodální	Multimodální	k2eAgInPc1d1	Multimodální
případy	případ	k1gInPc1	případ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
GSM	GSM	kA	GSM
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začínala	začínat	k5eAaImAgFnS	začínat
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
900	[number]	k4	900
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
expandovala	expandovat	k5eAaImAgFnS	expandovat
i	i	k9	i
do	do	k7c2	do
pásem	pásmo	k1gNnPc2	pásmo
1800	[number]	k4	1800
a	a	k8xC	a
1900	[number]	k4	1900
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
multimodální	multimodální	k2eAgInPc1d1	multimodální
telefony	telefon	k1gInPc1	telefon
mohou	moct	k5eAaImIp3nP	moct
fungovat	fungovat	k5eAaImF	fungovat
i	i	k9	i
v	v	k7c6	v
analogových	analogový	k2eAgFnPc6d1	analogová
sítích	síť	k1gFnPc6	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
duální	duální	k2eAgNnSc1d1	duální
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
tri-mód	triód	k1gInSc1	tri-mód
<g/>
:	:	kIx,	:
AMPS	AMPS	kA	AMPS
800	[number]	k4	800
/	/	kIx~	/
CDMA	CDMA	kA	CDMA
800	[number]	k4	800
/	/	kIx~	/
CDMA	CDMA	kA	CDMA
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Multimodální	Multimodální	k2eAgInPc1d1	Multimodální
telefony	telefon	k1gInPc1	telefon
byly	být	k5eAaImAgInP	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
pro	pro	k7c4	pro
umožnění	umožnění	k1gNnSc4	umožnění
roamingu	roaming	k1gInSc2	roaming
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teď	teď	k6eAd1	teď
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
při	při	k7c6	při
zavedení	zavedení	k1gNnSc6	zavedení
WCDMA	WCDMA	kA	WCDMA
(	(	kIx(	(
<g/>
celulární	celulární	k2eAgFnSc2d1	celulární
sítě	síť	k1gFnSc2	síť
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zákazníci	zákazník	k1gMnPc1	zákazník
museli	muset	k5eAaImAgMnP	muset
vzdát	vzdát	k5eAaPmF	vzdát
mobilů	mobil	k1gInPc2	mobil
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
pokrytím	pokrytí	k1gNnSc7	pokrytí
GSM	GSM	kA	GSM
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc1	každý
prodaný	prodaný	k2eAgInSc1d1	prodaný
opravdový	opravdový	k2eAgInSc1d1	opravdový
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
3G	[number]	k4	3G
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
WCDMA	WCDMA	kA	WCDMA
<g/>
/	/	kIx~	/
<g/>
GSM	GSM	kA	GSM
duální	duální	k2eAgInSc1d1	duální
mobil	mobil	k1gInSc1	mobil
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
2.75	[number]	k4	2.75
<g/>
G	G	kA	G
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
CDMA2000	CDMA2000	k1gFnSc4	CDMA2000
nebo	nebo	k8xC	nebo
EDGE	EDGE	kA	EDGE
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
požadavky	požadavek	k1gInPc1	požadavek
zahrnuté	zahrnutý	k2eAgInPc1d1	zahrnutý
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
multimodálních	multimodální	k2eAgInPc2d1	multimodální
mobilů	mobil	k1gInPc2	mobil
je	být	k5eAaImIp3nS	být
nalezení	nalezení	k1gNnSc1	nalezení
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
sdílet	sdílet	k5eAaImF	sdílet
komponenty	komponent	k1gInPc4	komponent
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
telefonní	telefonní	k2eAgFnPc1d1	telefonní
klávesnice	klávesnice	k1gFnPc1	klávesnice
a	a	k8xC	a
displej	displej	k1gInSc1	displej
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sdíleny	sdílen	k2eAgFnPc1d1	sdílena
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
existují	existovat	k5eAaImIp3nP	existovat
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
stupni	stupeň	k1gInSc6	stupeň
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Složitost	složitost	k1gFnSc1	složitost
těchto	tento	k3xDgInPc2	tento
požadavků	požadavek	k1gInPc2	požadavek
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rozdílech	rozdíl	k1gInPc6	rozdíl
mezi	mezi	k7c7	mezi
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
varianty	varianta	k1gFnPc1	varianta
systémů	systém	k1gInPc2	systém
GSM	GSM	kA	GSM
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
různé	různý	k2eAgFnPc4d1	různá
frekvence	frekvence	k1gFnPc4	frekvence
a	a	k8xC	a
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pravé	pravý	k2eAgInPc4d1	pravý
multimodální	multimodální	k2eAgInPc4d1	multimodální
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
raději	rád	k6eAd2	rád
za	za	k7c4	za
multi-pásmové	multiásmový	k2eAgInPc4d1	multi-pásmový
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
multimodálních	multimodální	k2eAgInPc2d1	multimodální
telefonů	telefon	k1gInPc2	telefon
IS-	IS-	k1gFnSc2	IS-
<g/>
95	[number]	k4	95
<g/>
/	/	kIx~	/
<g/>
GSM	GSM	kA	GSM
nebo	nebo	k8xC	nebo
telefonů	telefon	k1gInPc2	telefon
AMPS	AMPS	kA	AMPS
<g/>
/	/	kIx~	/
<g/>
IS-	IS-	k1gFnSc1	IS-
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc4d1	základní
pásmové	pásmový	k2eAgNnSc4d1	pásmové
zpracování	zpracování	k1gNnSc4	zpracování
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
systémy	systém	k1gInPc7	systém
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
reálným	reálný	k2eAgInPc3d1	reálný
problémům	problém	k1gInPc3	problém
při	při	k7c6	při
integraci	integrace	k1gFnSc6	integrace
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
tedy	tedy	k9	tedy
k	k	k7c3	k
objemnějším	objemný	k2eAgInPc3d2	objemnější
telefonům	telefon	k1gInPc3	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
multimodálních	multimodální	k2eAgInPc2d1	multimodální
telefonů	telefon	k1gInPc2	telefon
je	být	k5eAaImIp3nS	být
telefon	telefon	k1gInSc1	telefon
WCDMA	WCDMA	kA	WCDMA
<g/>
/	/	kIx~	/
<g/>
GSM	GSM	kA	GSM
<g/>
.	.	kIx.	.
</s>
<s>
Radiová	radiový	k2eAgNnPc1d1	radiové
rozhraní	rozhraní	k1gNnPc1	rozhraní
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
posílání	posílání	k1gNnSc1	posílání
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
mobilní	mobilní	k2eAgFnSc2d1	mobilní
do	do	k7c2	do
centrální	centrální	k2eAgFnSc2d1	centrální
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
software	software	k1gInSc1	software
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
důležitější	důležitý	k2eAgMnSc1d2	důležitější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysílací	vysílací	k2eAgNnSc1d1	vysílací
rozhraní	rozhraní	k1gNnSc1	rozhraní
WCDMA	WCDMA	kA	WCDMA
bylo	být	k5eAaImAgNnS	být
navrženo	navržen	k2eAgNnSc1d1	navrženo
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c6	na
GSM	GSM	kA	GSM
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgInSc4d1	speciální
mód	mód	k1gInSc4	mód
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
přerušovaný	přerušovaný	k2eAgInSc1d1	přerušovaný
mód	mód	k1gInSc1	mód
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
namísto	namísto	k7c2	namísto
souvislého	souvislý	k2eAgInSc2d1	souvislý
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
mobil	mobil	k1gInSc1	mobil
schopen	schopen	k2eAgInSc1d1	schopen
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
stopnout	stopnout	k5eAaPmF	stopnout
posílání	posílání	k1gNnSc4	posílání
a	a	k8xC	a
zkoušet	zkoušet	k5eAaImF	zkoušet
hledat	hledat	k5eAaImF	hledat
zdroje	zdroj	k1gInPc4	zdroj
signálu	signál	k1gInSc2	signál
GSM	GSM	kA	GSM
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mód	mód	k1gInSc1	mód
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
mezifrekvenční	mezifrekvenční	k2eAgNnSc4d1	mezifrekvenční
předání	předání	k1gNnSc4	předání
s	s	k7c7	s
kanálovými	kanálový	k2eAgNnPc7d1	kanálové
měřeními	měření	k1gNnPc7	měření
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
aproximovány	aproximovat	k5eAaBmNgInP	aproximovat
použitím	použití	k1gNnSc7	použití
"	"	kIx"	"
<g/>
pilotních	pilotní	k2eAgInPc2d1	pilotní
signálů	signál	k1gInPc2	signál
<g/>
"	"	kIx"	"
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
systémech	systém	k1gInPc6	systém
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
CDMA	CDMA	kA	CDMA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
mobilů	mobil	k1gInPc2	mobil
v	v	k7c6	v
sítích	síť	k1gFnPc6	síť
DS-WCDMA	DS-WCDMA	k1gFnSc2	DS-WCDMA
a	a	k8xC	a
MC-CDMA	MC-CDMA	k1gFnSc2	MC-CDMA
–	–	k?	–
3G	[number]	k4	3G
varianta	varianta	k1gFnSc1	varianta
CDMA	CDMA	kA	CDMA
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
čipová	čipový	k2eAgFnSc1d1	čipová
rychlost	rychlost	k1gFnSc1	rychlost
těchto	tento	k3xDgInPc2	tento
mobilů	mobil	k1gInPc2	mobil
nekompatibilní	kompatibilní	k2eNgMnPc1d1	nekompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Částí	část	k1gFnSc7	část
projednávání	projednávání	k1gNnSc2	projednávání
souvisejícího	související	k2eAgNnSc2d1	související
s	s	k7c7	s
patentem	patent	k1gInSc7	patent
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
kompatibilních	kompatibilní	k2eAgFnPc2d1	kompatibilní
čipových	čipový	k2eAgFnPc2d1	čipová
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
vysílací	vysílací	k2eAgNnSc1d1	vysílací
a	a	k8xC	a
systémové	systémový	k2eAgNnSc1d1	systémové
rozhraní	rozhraní	k1gNnSc1	rozhraní
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc1d1	odlišný
i	i	k9	i
na	na	k7c6	na
teoretické	teoretický	k2eAgFnSc6d1	teoretická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
hardwaru	hardware	k1gInSc2	hardware
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
systém	systém	k1gInSc4	systém
v	v	k7c6	v
mobilu	mobil	k1gInSc6	mobil
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
společná	společný	k2eAgFnSc1d1	společná
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc1	rozdíl
budou	být	k5eAaImBp3nP	být
především	především	k6eAd1	především
v	v	k7c6	v
softwaru	software	k1gInSc6	software
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgFnPc2d1	současná
mobilních	mobilní	k2eAgFnPc2d1	mobilní
sítí	síť	k1gFnPc2	síť
používá	používat	k5eAaImIp3nS	používat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
standardů	standard	k1gInPc2	standard
–	–	k?	–
GSM	GSM	kA	GSM
nebo	nebo	k8xC	nebo
CDMA	CDMA	kA	CDMA
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
standard	standard	k1gInSc1	standard
iDEN	iDEN	k?	iDEN
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
exkluzivně	exkluzivně	k6eAd1	exkluzivně
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
Nextel	Nextela	k1gFnPc2	Nextela
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
síť	síť	k1gFnSc1	síť
bude	být	k5eAaImBp3nS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
po	po	k7c6	po
fúzi	fúze	k1gFnSc6	fúze
firmy	firma	k1gFnSc2	firma
Nextel	Nextel	k1gInSc4	Nextel
a	a	k8xC	a
Sprint	sprint	k1gInSc4	sprint
PCS	PCS	kA	PCS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
síť	síť	k1gFnSc4	síť
CDMA	CDMA	kA	CDMA
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
firma	firma	k1gFnSc1	firma
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Wireless	Wireless	k1gInSc1	Wireless
po	po	k7c6	po
fúzi	fúze	k1gFnSc6	fúze
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Cingular	Cingulara	k1gFnPc2	Cingulara
postupně	postupně	k6eAd1	postupně
nahradila	nahradit	k5eAaPmAgFnS	nahradit
TDMA	TDMA	kA	TDMA
sítí	síť	k1gFnPc2	síť
GSM	GSM	kA	GSM
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
novými	nový	k2eAgFnPc7d1	nová
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
i	i	k8xC	i
tady	tady	k6eAd1	tady
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
otázky	otázka	k1gFnPc1	otázka
o	o	k7c6	o
účincích	účinek	k1gInPc6	účinek
používání	používání	k1gNnSc1	používání
mobilů	mobil	k1gInPc2	mobil
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
malá	malý	k2eAgFnSc1d1	malá
vědecká	vědecký	k2eAgFnSc1d1	vědecká
evidence	evidence	k1gFnSc1	evidence
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
případů	případ	k1gInPc2	případ
určitých	určitý	k2eAgInPc2d1	určitý
typů	typ	k1gInPc2	typ
nádorů	nádor	k1gInPc2	nádor
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
pravidelném	pravidelný	k2eAgNnSc6d1	pravidelné
používání	používání	k1gNnSc6	používání
mobilů	mobil	k1gInPc2	mobil
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc1d2	novější
panevropská	panevropský	k2eAgFnSc1d1	panevropská
studie	studie	k1gFnSc1	studie
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
důležité	důležitý	k2eAgInPc4d1	důležitý
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
poškození	poškození	k1gNnSc4	poškození
DNA	dno	k1gNnSc2	dno
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
expertní	expertní	k2eAgFnSc1d1	expertní
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc2	force
on	on	k3xPp3gMnSc1	on
EMF	EMF	kA	EMF
effects	effects	k1gInSc4	effects
on	on	k3xPp3gMnSc1	on
health	health	k1gMnSc1	health
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pověřila	pověřit	k5eAaPmAgFnS	pověřit
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
definitivní	definitivní	k2eAgInSc1d1	definitivní
závěr	závěr	k1gInSc1	závěr
o	o	k7c6	o
platnosti	platnost	k1gFnSc6	platnost
těchto	tento	k3xDgNnPc2	tento
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
míní	mínit	k5eAaImIp3nS	mínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
radiové	radiový	k2eAgNnSc4d1	radiové
pole	pole	k1gNnSc4	pole
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
nemá	mít	k5eNaImIp3nS	mít
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
poškodit	poškodit	k5eAaPmF	poškodit
molekulární	molekulární	k2eAgFnSc2d1	molekulární
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
genetických	genetický	k2eAgFnPc2d1	genetická
mutací	mutace	k1gFnPc2	mutace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ionizující	ionizující	k2eAgNnSc4d1	ionizující
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
působit	působit	k5eAaImF	působit
pouze	pouze	k6eAd1	pouze
tepelné	tepelný	k2eAgInPc4d1	tepelný
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
(	(	kIx(	(
<g/>
pobočka	pobočka	k1gFnSc1	pobočka
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
mohou	moct	k5eAaImIp3nP	moct
napomáhat	napomáhat	k5eAaImF	napomáhat
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
některých	některý	k3yIgInPc2	některý
mozkových	mozkový	k2eAgInPc2d1	mozkový
nádorů	nádor	k1gInPc2	nádor
a	a	k8xC	a
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
2B	[number]	k4	2B
karcinogenů	karcinogen	k1gInPc2	karcinogen
(	(	kIx(	(
<g/>
podezření	podezření	k1gNnSc1	podezření
z	z	k7c2	z
karcinogenity	karcinogenita	k1gFnSc2	karcinogenita
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
nové	nový	k2eAgInPc4d1	nový
výsledky	výsledek	k1gInPc4	výsledek
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
jak	jak	k6eAd1	jak
škodlivost	škodlivost	k1gFnSc4	škodlivost
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
neškodlivost	neškodlivost	k1gFnSc1	neškodlivost
záření	záření	k1gNnSc2	záření
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
eliminovaly	eliminovat	k5eAaBmAgInP	eliminovat
možná	možná	k9	možná
rizika	riziko	k1gNnPc1	riziko
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
přísné	přísný	k2eAgFnPc1d1	přísná
normy	norma	k1gFnPc1	norma
určující	určující	k2eAgFnSc4d1	určující
jejich	jejich	k3xOp3gFnSc4	jejich
maximální	maximální	k2eAgFnSc4d1	maximální
hodnotu	hodnota	k1gFnSc4	hodnota
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
Specific	Specific	k1gMnSc1	Specific
Absorption	Absorption	k1gInSc4	Absorption
Rate	Rat	k1gFnSc2	Rat
(	(	kIx(	(
<g/>
SAR	SAR	kA	SAR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
SAR	SAR	kA	SAR
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nynějších	nynější	k2eAgFnPc2d1	nynější
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
stanovena	stanovit	k5eAaPmNgFnS	stanovit
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
1.6	[number]	k4	1.6
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
živé	živý	k2eAgFnSc2d1	živá
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgFnSc7d1	další
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
souvislost	souvislost	k1gFnSc1	souvislost
používání	používání	k1gNnSc2	používání
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
s	s	k7c7	s
automobilovými	automobilový	k2eAgFnPc7d1	automobilová
nehodami	nehoda	k1gFnPc7	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
zakázaly	zakázat	k5eAaPmAgFnP	zakázat
držet	držet	k5eAaImF	držet
mobil	mobil	k1gInSc4	mobil
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
nebo	nebo	k8xC	nebo
požadují	požadovat	k5eAaImIp3nP	požadovat
použití	použití	k1gNnSc3	použití
systémů	systém	k1gInPc2	systém
"	"	kIx"	"
<g/>
hands-free	handsree	k6eAd1	hands-free
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
už	už	k6eAd1	už
požaduje	požadovat	k5eAaImIp3nS	požadovat
"	"	kIx"	"
<g/>
hands-free	handsree	k1gNnSc1	hands-free
<g/>
"	"	kIx"	"
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
mobilů	mobil	k1gInPc2	mobil
ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
USA	USA	kA	USA
je	on	k3xPp3gNnPc4	on
následují	následovat	k5eAaImIp3nP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
reakce	reakce	k1gFnSc1	reakce
řidičů	řidič	k1gMnPc2	řidič
se	se	k3xPyFc4	se
při	pře	k1gFnSc4	pře
hovoru	hovor	k1gInSc2	hovor
mobilním	mobilní	k2eAgInSc7d1	mobilní
telefonem	telefon	k1gInSc7	telefon
sníží	snížit	k5eAaPmIp3nS	snížit
až	až	k9	až
o	o	k7c4	o
19	[number]	k4	19
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
nedodržování	nedodržování	k1gNnSc2	nedodržování
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
technologie	technologie	k1gFnSc1	technologie
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
kupředu	kupředu	k6eAd1	kupředu
a	a	k8xC	a
datové	datový	k2eAgInPc1d1	datový
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
mobilní	mobilní	k2eAgFnSc4d1	mobilní
síť	síť	k1gFnSc4	síť
rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
rozruchem	rozruch	k1gInSc7	rozruch
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
pásmové	pásmový	k2eAgInPc1d1	pásmový
stožáry	stožár	k1gInPc1	stožár
sítě	síť	k1gFnSc2	síť
3	[number]	k4	3
<g/>
G.	G.	kA	G.
Síť	síť	k1gFnSc1	síť
podnítila	podnítit	k5eAaPmAgFnS	podnítit
mnoho	mnoho	k4c4	mnoho
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
námitek	námitka	k1gFnPc2	námitka
a	a	k8xC	a
společenských	společenský	k2eAgInPc2d1	společenský
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
málo	málo	k1gNnSc1	málo
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
vládami	vláda	k1gFnPc7	vláda
a	a	k8xC	a
skupinami	skupina	k1gFnPc7	skupina
poskytujícími	poskytující	k2eAgInPc7d1	poskytující
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
technické	technický	k2eAgFnSc2d1	technická
aktualizace	aktualizace	k1gFnSc2	aktualizace
na	na	k7c4	na
sítě	síť	k1gFnPc4	síť
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
upgrade	upgrade	k1gInSc1	upgrade
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
ACA	ACA	kA	ACA
(	(	kIx(	(
<g/>
Australian	Australian	k1gInSc1	Australian
Communications	Communicationsa	k1gFnPc2	Communicationsa
Authority	Authorita	k1gFnSc2	Authorita
<g/>
)	)	kIx)	)
a	a	k8xC	a
ARPANSA	ARPANSA	kA	ARPANSA
(	(	kIx(	(
<g/>
Australian	Australian	k1gInSc1	Australian
Radiation	Radiation	k1gInSc1	Radiation
Protection	Protection	k1gInSc1	Protection
And	Anda	k1gFnPc2	Anda
Nuclear	Nucleara	k1gFnPc2	Nucleara
Safety	Safeta	k1gFnSc2	Safeta
Agency	Agenca	k1gFnSc2	Agenca
<g/>
)	)	kIx)	)
oznámily	oznámit	k5eAaPmAgInP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stožáry	stožár	k1gInPc1	stožár
3G	[number]	k4	3G
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
méně	málo	k6eAd2	málo
radiace	radiace	k1gFnSc2	radiace
než	než	k8xS	než
už	už	k6eAd1	už
zavedená	zavedený	k2eAgFnSc1d1	zavedená
síť	síť	k1gFnSc1	síť
2	[number]	k4	2
<g/>
G.	G.	kA	G.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
radiační	radiační	k2eAgInSc1d1	radiační
výkon	výkon	k1gInSc1	výkon
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
watty	watt	k1gInPc4	watt
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
žádné	žádný	k3yNgNnSc4	žádný
vládní	vládní	k2eAgNnSc4d1	vládní
vyjádření	vyjádření	k1gNnSc4	vyjádření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
současné	současný	k2eAgInPc4d1	současný
stožáry	stožár	k1gInPc4	stožár
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
víc	hodně	k6eAd2	hodně
radiace	radiace	k1gFnPc1	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
použití	použití	k1gNnSc1	použití
dalších	další	k2eAgInPc2d1	další
stožárů	stožár	k1gInPc2	stožár
nevyhnutně	vyhnutně	k6eNd1	vyhnutně
značí	značit	k5eAaImIp3nS	značit
víc	hodně	k6eAd2	hodně
zamořenou	zamořený	k2eAgFnSc4d1	zamořená
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
například	například	k6eAd1	například
matky	matka	k1gFnPc4	matka
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
dítětem	dítě	k1gNnSc7	dítě
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
psychický	psychický	k2eAgInSc4d1	psychický
vývoj	vývoj	k1gInSc4	vývoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
mobily	mobil	k1gInPc1	mobil
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc4d1	hlavní
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
problém	problém	k1gInSc4	problém
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
klonování	klonování	k1gNnSc1	klonování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
varianta	varianta	k1gFnSc1	varianta
odcizení	odcizení	k1gNnSc2	odcizení
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgMnSc1d2	složitější
u	u	k7c2	u
nových	nový	k2eAgInPc2d1	nový
digitálních	digitální	k2eAgInPc2d1	digitální
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
uživatelů	uživatel	k1gMnPc2	uživatel
nepochopilo	pochopit	k5eNaPmAgNnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mobil	mobil	k1gInSc1	mobil
je	být	k5eAaImIp3nS	být
doslova	doslova	k6eAd1	doslova
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
přenosná	přenosný	k2eAgFnSc1d1	přenosná
vysílačka	vysílačka	k1gFnSc1	vysílačka
s	s	k7c7	s
nějakými	nějaký	k3yIgInPc7	nějaký
počítačovými	počítačový	k2eAgInPc7d1	počítačový
elementy	element	k1gInPc7	element
<g/>
.	.	kIx.	.
</s>
<s>
Radiové	radiový	k2eAgInPc4d1	radiový
skenery	skener	k1gInPc4	skener
datované	datovaný	k2eAgInPc4d1	datovaný
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
nebo	nebo	k8xC	nebo
1997	[number]	k4	1997
typicky	typicky	k6eAd1	typicky
mohou	moct	k5eAaImIp3nP	moct
zachytit	zachytit	k5eAaPmF	zachytit
staré	starý	k2eAgInPc4d1	starý
analogové	analogový	k2eAgInPc4d1	analogový
celulární	celulární	k2eAgInPc4d1	celulární
telefony	telefon	k1gInPc4	telefon
jednoduše	jednoduše	k6eAd1	jednoduše
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
poslouchat	poslouchat	k5eAaImF	poslouchat
radio	radio	k1gNnSc4	radio
FM	FM	kA	FM
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
po	po	k7c6	po
letech	let	k1gInPc6	let
technologie	technologie	k1gFnSc1	technologie
určila	určit	k5eAaPmAgFnS	určit
pro	pro	k7c4	pro
mobily	mobil	k1gInPc4	mobil
gigahertzové	gigahertzový	k2eAgNnSc4d1	gigahertzový
pásmo	pásmo	k1gNnSc4	pásmo
nad	nad	k7c7	nad
rozsahem	rozsah	k1gInSc7	rozsah
většiny	většina	k1gFnSc2	většina
konvenčních	konvenční	k2eAgInPc2d1	konvenční
skenerů	skener	k1gInPc2	skener
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgInPc2d1	současný
mobilů	mobil	k1gInPc2	mobil
je	být	k5eAaImIp3nS	být
zabezpečena	zabezpečit	k5eAaPmNgFnS	zabezpečit
mnohými	mnohý	k2eAgInPc7d1	mnohý
digitálními	digitální	k2eAgInPc7d1	digitální
šifrovacími	šifrovací	k2eAgInPc7d1	šifrovací
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
nové	nový	k2eAgInPc4d1	nový
prostředky	prostředek	k1gInPc4	prostředek
digitální	digitální	k2eAgFnSc2d1	digitální
komunikace	komunikace	k1gFnSc2	komunikace
jako	jako	k8xC	jako
textové	textový	k2eAgFnPc4d1	textová
zprávy	zpráva	k1gFnPc4	zpráva
a	a	k8xC	a
e-mail	eail	k1gInSc4	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
i	i	k8xC	i
základní	základní	k2eAgInPc1d1	základní
mobily	mobil	k1gInPc1	mobil
mohou	moct	k5eAaImIp3nP	moct
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
textové	textový	k2eAgFnPc4d1	textová
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
dělají	dělat	k5eAaImIp3nP	dělat
přístupnými	přístupný	k2eAgFnPc7d1	přístupná
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
červů	červ	k1gMnPc2	červ
a	a	k8xC	a
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgInPc4d2	lepší
mobily	mobil	k1gInPc4	mobil
provozující	provozující	k2eAgInPc4d1	provozující
e-mail	eail	k1gInSc4	e-mail
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
napadeny	napaden	k2eAgInPc1d1	napaden
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
šířit	šířit	k5eAaImF	šířit
posíláním	posílání	k1gNnSc7	posílání
zpráv	zpráva	k1gFnPc2	zpráva
přes	přes	k7c4	přes
telefonní	telefonní	k2eAgInSc4d1	telefonní
adresář	adresář	k1gInSc4	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
může	moct	k5eAaImIp3nS	moct
dovolit	dovolit	k5eAaPmF	dovolit
cizím	cizí	k2eAgMnPc3d1	cizí
uživatelům	uživatel	k1gMnPc3	uživatel
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
mobilu	mobil	k1gInSc2	mobil
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
hesel	heslo	k1gNnPc2	heslo
nebo	nebo	k8xC	nebo
firemních	firemní	k2eAgNnPc2d1	firemní
dat	datum	k1gNnPc2	datum
uložených	uložený	k2eAgNnPc2d1	uložené
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
viry	vir	k1gInPc1	vir
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
na	na	k7c6	na
realizaci	realizace	k1gFnSc6	realizace
hovorů	hovor	k1gInPc2	hovor
nebo	nebo	k8xC	nebo
posílaní	posílaný	k2eAgMnPc1d1	posílaný
zpráv	zpráva	k1gFnPc2	zpráva
na	na	k7c4	na
cizí	cizí	k2eAgInSc4d1	cizí
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
mobily	mobil	k1gInPc1	mobil
používají	používat	k5eAaImIp3nP	používat
mnoho	mnoho	k4c4	mnoho
systémů	systém	k1gInPc2	systém
vyžadujících	vyžadující	k2eAgInPc2d1	vyžadující
separátní	separátní	k2eAgInPc4d1	separátní
antivirové	antivirový	k2eAgInPc4d1	antivirový
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Agentury	agentura	k1gFnPc1	agentura
jako	jako	k8xC	jako
NSA	NSA	kA	NSA
či	či	k8xC	či
GCHQ	GCHQ	kA	GCHQ
mohou	moct	k5eAaImIp3nP	moct
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
dálkově	dálkově	k6eAd1	dálkově
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Snowden	Snowdna	k1gFnPc2	Snowdna
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
mobil	mobil	k1gInSc4	mobil
vypínat	vypínat	k5eAaImF	vypínat
a	a	k8xC	a
zapínat	zapínat	k5eAaImF	zapínat
<g/>
,	,	kIx,	,
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
nahrávat	nahrávat	k5eAaImF	nahrávat
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
každý	každý	k3xTgInSc4	každý
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
lze	lze	k6eAd1	lze
hacknout	hacknout	k5eAaPmF	hacknout
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
společnost	společnost	k1gFnSc1	společnost
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
Jihokorejské	jihokorejský	k2eAgInPc1d1	jihokorejský
LG	LG	kA	LG
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
stalo	stát	k5eAaPmAgNnS	stát
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
mobilů	mobil	k1gInPc2	mobil
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prodala	prodat	k5eAaPmAgFnS	prodat
společnost	společnost	k1gFnSc1	společnost
64,4	[number]	k4	64,4
milionů	milion	k4xCgInPc2	milion
mobilů	mobil	k1gInPc2	mobil
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
80,5	[number]	k4	80,5
milionů	milion	k4xCgInPc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
již	již	k9	již
196,6	[number]	k4	196,6
milionů	milion	k4xCgInPc2	milion
MT	MT	kA	MT
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
nejvíce	hodně	k6eAd3	hodně
postihla	postihnout	k5eAaPmAgFnS	postihnout
americkou	americký	k2eAgFnSc4d1	americká
společnost	společnost	k1gFnSc4	společnost
Motorola	Motorola	kA	Motorola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
propad	propad	k1gInSc1	propad
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
více	hodně	k6eAd2	hodně
než	než	k8xS	než
127	[number]	k4	127
%	%	kIx~	%
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
217,4	[number]	k4	217,4
mil	míle	k1gFnPc2	míle
<g/>
;	;	kIx,	;
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
159	[number]	k4	159
mil	míle	k1gFnPc2	míle
<g/>
;	;	kIx,	;
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
99,9	[number]	k4	99,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
sice	sice	k8xC	sice
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
výrazný	výrazný	k2eAgInSc1d1	výrazný
nárůst	nárůst	k1gInSc1	nárůst
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
v	v	k7c6	v
uplynulých	uplynulý	k2eAgInPc6d1	uplynulý
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
ztrátovým	ztrátový	k2eAgInSc7d1	ztrátový
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
74,8	[number]	k4	74,8
mil	míle	k1gFnPc2	míle
<g/>
;	;	kIx,	;
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
103,4	[number]	k4	103,4
mil	míle	k1gFnPc2	míle
<g/>
;	;	kIx,	;
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
99,9	[number]	k4	99,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
prodaných	prodaný	k2eAgNnPc2d1	prodané
MT	MT	kA	MT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
prodáno	prodat	k5eAaPmNgNnS	prodat
1177	[number]	k4	1177
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
1125,5	[number]	k4	1125,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
1018,8	[number]	k4	1018,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
nejhorším	zlý	k2eAgInSc7d3	Nejhorší
rokem	rok	k1gInSc7	rok
v	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
1133	[number]	k4	1133
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
mobilů	mobil	k1gInPc2	mobil
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
poprvé	poprvé	k6eAd1	poprvé
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
předchozím	předchozí	k2eAgInSc6d1	předchozí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
dramatickému	dramatický	k2eAgInSc3d1	dramatický
propadu	propad	k1gInSc3	propad
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
připadá	připadat	k5eAaPmIp3nS	připadat
130	[number]	k4	130
mobilních	mobilní	k2eAgNnPc2d1	mobilní
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
penetrace	penetrace	k1gFnSc1	penetrace
130	[number]	k4	130
%	%	kIx~	%
<g/>
.	.	kIx.	.
70	[number]	k4	70
procent	procento	k1gNnPc2	procento
Čechů	Čech	k1gMnPc2	Čech
si	se	k3xPyFc3	se
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
ani	ani	k9	ani
pět	pět	k4xCc4	pět
uložených	uložený	k2eAgNnPc2d1	uložené
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
mobilu	mobil	k1gInSc6	mobil
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
42	[number]	k4	42
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
vůbec	vůbec	k9	vůbec
kontakty	kontakt	k1gInPc4	kontakt
nezálohuje	zálohovat	k5eNaBmIp3nS	zálohovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
telefonních	telefonní	k2eAgNnPc2d1	telefonní
čísel	číslo	k1gNnPc2	číslo
si	se	k3xPyFc3	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
muži	muž	k1gMnPc1	muž
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35	[number]	k4	35
až	až	k9	až
44	[number]	k4	44
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Třetina	třetina	k1gFnSc1	třetina
uživatelů	uživatel	k1gMnPc2	uživatel
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
muže	muž	k1gMnPc4	muž
do	do	k7c2	do
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
platba	platba	k1gFnSc1	platba
za	za	k7c4	za
mobil	mobil	k1gInSc4	mobil
činí	činit	k5eAaImIp3nS	činit
do	do	k7c2	do
800	[number]	k4	800
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
osob	osoba	k1gFnPc2	osoba
používají	používat	k5eAaImIp3nP	používat
přístroj	přístroj	k1gInSc1	přístroj
jako	jako	k8xC	jako
organizátor	organizátor	k1gInSc1	organizátor
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
si	se	k3xPyFc3	se
Češi	Čech	k1gMnPc1	Čech
mění	měnit	k5eAaImIp3nP	měnit
přístroj	přístroj	k1gInSc4	přístroj
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
–	–	k?	–
kupují	kupovat	k5eAaImIp3nP	kupovat
nejvíce	nejvíce	k6eAd1	nejvíce
přístroj	přístroj	k1gInSc4	přístroj
nižší	nízký	k2eAgFnSc2d2	nižší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
s	s	k7c7	s
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
každý	každý	k3xTgMnSc1	každý
druhý	druhý	k4xOgMnSc1	druhý
senior	senior	k1gMnSc1	senior
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
speciální	speciální	k2eAgInPc4d1	speciální
mobily	mobil	k1gInPc4	mobil
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
<g/>
)	)	kIx)	)
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgMnSc1	každý
student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Android	android	k1gInSc1	android
(	(	kIx(	(
<g/>
od	od	k7c2	od
Open	Opena	k1gFnPc2	Opena
Handset	Handseta	k1gFnPc2	Handseta
Alliance	Alliance	k1gFnSc2	Alliance
a	a	k8xC	a
Googlu	Googl	k1gInSc2	Googl
<g/>
)	)	kIx)	)
Bada	Bada	k1gFnSc1	Bada
(	(	kIx(	(
<g/>
od	od	k7c2	od
Samsungu	Samsung	k1gInSc2	Samsung
<g/>
)	)	kIx)	)
iOS	iOS	k?	iOS
(	(	kIx(	(
<g/>
od	od	k7c2	od
Applu	Appl	k1gInSc2	Appl
<g/>
)	)	kIx)	)
Symbian	Symbian	k1gInSc1	Symbian
(	(	kIx(	(
<g/>
od	od	k7c2	od
Nokie	Nokie	k1gFnSc2	Nokie
<g/>
)	)	kIx)	)
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc4	mobile
(	(	kIx(	(
<g/>
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
)	)	kIx)	)
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
(	(	kIx(	(
<g/>
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
)	)	kIx)	)
MeeGo	MeeGo	k6eAd1	MeeGo
(	(	kIx(	(
<g/>
od	od	k7c2	od
Nokie	Nokie	k1gFnSc2	Nokie
<g/>
)	)	kIx)	)
Maemo	Maema	k1gFnSc5	Maema
Java	Javus	k1gMnSc4	Javus
BlackBerry	BlackBerra	k1gMnSc2	BlackBerra
OS	OS	kA	OS
Palm	Palm	k1gMnSc1	Palm
OS	OS	kA	OS
webOS	webOS	k?	webOS
Windows	Windows	kA	Windows
CE	CE	kA	CE
Tizen	Tizna	k1gFnPc2	Tizna
(	(	kIx(	(
<g/>
od	od	k7c2	od
Samsungu	Samsung	k1gInSc2	Samsung
<g/>
)	)	kIx)	)
Mozila	Mozila	k1gFnSc1	Mozila
OS	osa	k1gFnPc2	osa
</s>
