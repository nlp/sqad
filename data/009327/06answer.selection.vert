<s>
Rabi	rabi	k1gMnSc1	rabi
Löw	Löw	k1gMnSc1	Löw
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
v	v	k7c6	v
Poznani	Poznaň	k1gFnSc6	Poznaň
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
do	do	k7c2	do
vlivné	vlivný	k2eAgFnSc2d1	vlivná
rabínské	rabínský	k2eAgFnSc2d1	rabínská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
sahal	sahat	k5eAaImAgInS	sahat
její	její	k3xOp3gInSc1	její
původ	původ	k1gInSc1	původ
až	až	k9	až
k	k	k7c3	k
davidovské	davidovský	k2eAgFnSc3d1	davidovská
královské	královský	k2eAgFnSc3d1	královská
dynastii	dynastie	k1gFnSc3	dynastie
<g/>
.	.	kIx.	.
</s>
