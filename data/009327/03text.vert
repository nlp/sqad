<p>
<s>
Jehuda	Jehuda	k1gMnSc1	Jehuda
Liva	Liva	k1gMnSc1	Liva
ben	ben	k?	ben
Becalel	Becalel	k1gMnSc1	Becalel
(	(	kIx(	(
<g/>
1512	[number]	k4	1512
<g/>
/	/	kIx~	/
<g/>
1520	[number]	k4	1520
<g/>
/	/	kIx~	/
<g/>
1525	[number]	k4	1525
nebo	nebo	k8xC	nebo
1526	[number]	k4	1526
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
uváděn	uvádět	k5eAaImNgInS	uvádět
také	také	k9	také
německým	německý	k2eAgInSc7d1	německý
pravopisem	pravopis	k1gInSc7	pravopis
jako	jako	k8xS	jako
ben	ben	k?	ben
Bezalel	Bezalel	k1gFnSc1	Bezalel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k8xS	jako
Jehuda	Jehuda	k1gMnSc1	Jehuda
Arje	Arj	k1gFnSc2	Arj
ben	ben	k?	ben
Becalel	Becalel	k1gMnSc1	Becalel
<g/>
,	,	kIx,	,
Jehuda	Jehuda	k1gMnSc1	Jehuda
<g />
.	.	kIx.	.
</s>
<s>
Löw	Löw	k?	Löw
ben	ben	k?	ben
Becalel	Becalel	k1gFnSc1	Becalel
(	(	kIx(	(
<g/>
Arje	Arje	k1gFnSc1	Arje
[	[	kIx(	[
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
Lev	Lev	k1gMnSc1	Lev
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Löw	Löw	k1gFnSc1	Löw
a	a	k8xC	a
Liva	Liva	k1gFnSc1	Liva
jsou	být	k5eAaImIp3nP	být
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rabbi	rabbi	k6eAd1	rabbi
Löw	Löw	k1gFnSc1	Löw
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
hebrejským	hebrejský	k2eAgInSc7d1	hebrejský
akronymem	akronym	k1gInSc7	akronym
מ	מ	k?	מ
<g/>
״	״	k?	״
<g/>
ל	ל	k?	ל
–	–	k?	–
MaHaRaL	MaHaRaL	k1gFnSc2	MaHaRaL
(	(	kIx(	(
<g/>
Morenu	Moren	k1gInSc2	Moren
Ha-Rav	Ha-Rav	k1gInSc1	Ha-Rav
Liva	Liv	k1gInSc2	Liv
nebo	nebo	k8xC	nebo
též	též	k9	též
Morenu	Morena	k1gFnSc4	Morena
Ha-gadol	Haadol	k1gInSc1	Ha-gadol
Rabi	rabi	k1gMnSc1	rabi
Liva	Liva	k1gMnSc1	Liva
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gMnSc1	náš
(	(	kIx(	(
<g/>
velký	velký	k2eAgMnSc1d1	velký
<g/>
)	)	kIx)	)
učitel	učitel	k1gMnSc1	učitel
rabbi	rabb	k1gFnSc2	rabb
Liva	Liva	k1gMnSc1	Liva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
židovský	židovský	k2eAgMnSc1d1	židovský
rabín	rabín	k1gMnSc1	rabín
a	a	k8xC	a
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgMnSc1d1	legendární
tvůrce	tvůrce	k1gMnSc1	tvůrce
pražského	pražský	k2eAgMnSc2d1	pražský
golema	golem	k1gMnSc2	golem
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
rabín	rabín	k1gMnSc1	rabín
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
rabín	rabín	k1gMnSc1	rabín
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Rabi	rabi	k1gMnSc1	rabi
Löw	Löw	k1gMnSc1	Löw
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
židovském	židovský	k2eAgNnSc6d1	Židovské
ghettu	ghetto	k1gNnSc6	ghetto
talmudistickou	talmudistický	k2eAgFnSc4d1	talmudistický
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
ješivu	ješiva	k1gFnSc4	ješiva
<g/>
,	,	kIx,	,
1573	[number]	k4	1573
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
upravil	upravit	k5eAaPmAgMnS	upravit
též	též	k9	též
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
spolek	spolek	k1gInSc4	spolek
Chevra	Chevr	k1gMnSc2	Chevr
kadiša	kadišus	k1gMnSc2	kadišus
(	(	kIx(	(
<g/>
pohřební	pohřební	k2eAgNnSc1d1	pohřební
bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
<g/>
,	,	kIx,	,
založen	založen	k2eAgMnSc1d1	založen
1564	[number]	k4	1564
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
strávil	strávit	k5eAaPmAgMnS	strávit
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pohřben	pohřben	k2eAgMnSc1d1	pohřben
a	a	k8xC	a
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nazýván	nazývat	k5eAaImNgInS	nazývat
מ	מ	k?	מ
<g/>
״	״	k?	״
<g/>
ל	ל	k?	ל
מ	מ	k?	מ
–	–	k?	–
"	"	kIx"	"
<g/>
Pražský	pražský	k2eAgMnSc1d1	pražský
Maharal	Maharal	k1gMnSc1	Maharal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Rabi	rabi	k1gMnSc1	rabi
Löw	Löw	k1gMnSc1	Löw
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
v	v	k7c6	v
Poznani	Poznaň	k1gFnSc6	Poznaň
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
do	do	k7c2	do
vlivné	vlivný	k2eAgFnSc2d1	vlivná
rabínské	rabínský	k2eAgFnSc2d1	rabínská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
sahal	sahat	k5eAaImAgInS	sahat
její	její	k3xOp3gInSc1	její
původ	původ	k1gInSc1	původ
až	až	k9	až
k	k	k7c3	k
davidovské	davidovský	k2eAgFnSc3d1	davidovská
královské	královský	k2eAgFnSc3d1	královská
dynastii	dynastie	k1gFnSc3	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starších	starý	k2eAgInPc2d2	starší
a	a	k8xC	a
nepřesných	přesný	k2eNgInPc2d1	nepřesný
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
sice	sice	k8xC	sice
narodit	narodit	k5eAaPmF	narodit
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
(	(	kIx(	(
<g/>
Porýní-Falc	Porýní-Falc	k1gFnSc1	Porýní-Falc
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
odtamtud	odtamtud	k6eAd1	odtamtud
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pouze	pouze	k6eAd1	pouze
pocházela	pocházet	k5eAaImAgFnS	pocházet
snad	snad	k9	snad
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
jmen	jméno	k1gNnPc2	jméno
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
snadno	snadno	k6eAd1	snadno
–	–	k?	–
jeho	jeho	k3xOp3gMnSc7	jeho
otec	otec	k1gMnSc1	otec
Becalel	Becalel	k1gMnSc1	Becalel
ben	ben	k?	ben
Chajim	Chajim	k1gMnSc1	Chajim
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
svůj	svůj	k3xOyFgMnSc1	svůj
původ	původ	k1gMnSc1	původ
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
z	z	k7c2	z
Wormsu	Worms	k1gInSc2	Worms
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
švagrem	švagr	k1gMnSc7	švagr
rabína	rabín	k1gMnSc2	rabín
Jicchaka	Jicchak	k1gMnSc2	Jicchak
Klaubera	Klauber	k1gMnSc2	Klauber
z	z	k7c2	z
Poznaně	Poznaň	k1gFnSc2	Poznaň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgMnPc4	tři
bratry	bratr	k1gMnPc4	bratr
–	–	k?	–
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgInSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiné	jiná	k1gFnSc2	jiná
byl	být	k5eAaImAgMnS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
Sinaj	Sinaj	k1gInSc4	Sinaj
<g/>
,	,	kIx,	,
Chajim	Chajim	k1gMnSc1	Chajim
a	a	k8xC	a
Samson	Samson	k1gMnSc1	Samson
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
renomovaní	renomovaný	k2eAgMnPc1d1	renomovaný
učenci	učenec	k1gMnPc1	učenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jehudův	Jehudův	k2eAgInSc1d1	Jehudův
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
opředen	opříst	k5eAaPmNgInS	opříst
mnoha	mnoho	k4c7	mnoho
pověstmi	pověst	k1gFnPc7	pověst
a	a	k8xC	a
legendami	legenda	k1gFnPc7	legenda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
řada	řada	k1gFnSc1	řada
zlidověla	zlidovět	k5eAaPmAgFnS	zlidovět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
dají	dát	k5eAaPmIp3nP	dát
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
doložit	doložit	k5eAaPmF	doložit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
ješivách	ješiva	k1gFnPc6	ješiva
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiného	jiný	k2eAgNnSc2d1	jiné
podání	podání	k1gNnSc2	podání
se	se	k3xPyFc4	se
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
ješivách	ješiva	k1gFnPc6	ješiva
neabsolvoval	absolvovat	k5eNaPmAgMnS	absolvovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1553	[number]	k4	1553
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
(	(	kIx(	(
<g/>
ve	v	k7c6	v
28	[number]	k4	28
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
přijímá	přijímat	k5eAaImIp3nS	přijímat
úřad	úřad	k1gInSc1	úřad
vrchního	vrchní	k2eAgMnSc2d1	vrchní
moravského	moravský	k2eAgMnSc2d1	moravský
zemského	zemský	k2eAgMnSc2d1	zemský
rabína	rabín	k1gMnSc2	rabín
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvacetiletém	dvacetiletý	k2eAgNnSc6d1	dvacetileté
působení	působení	k1gNnSc6	působení
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
odešel	odejít	k5eAaPmAgMnS	odejít
roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
r.	r.	kA	r.
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgInS	odejít
–	–	k?	–
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
tezí	teze	k1gFnPc2	teze
do	do	k7c2	do
polské	polský	k2eAgFnSc2d1	polská
Poznaně	Poznaň	k1gFnSc2	Poznaň
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiné	jiná	k1gFnSc2	jiná
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
působení	působení	k1gNnSc2	působení
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1592	[number]	k4	1592
setkává	setkávat	k5eAaImIp3nS	setkávat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Rudolfem	Rudolf	k1gMnSc7	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
obsah	obsah	k1gInSc1	obsah
jejich	jejich	k3xOp3gFnSc2	jejich
schůzky	schůzka	k1gFnSc2	schůzka
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
(	(	kIx(	(
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
neověřených	ověřený	k2eNgFnPc2d1	neověřená
<g/>
)	)	kIx)	)
zkazek	zkazka	k1gFnPc2	zkazka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rovněž	rovněž	k9	rovněž
časem	časem	k6eAd1	časem
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
společné	společný	k2eAgFnPc4d1	společná
záliby	záliba	k1gFnPc4	záliba
v	v	k7c6	v
alchymii	alchymie	k1gFnSc6	alchymie
a	a	k8xC	a
kabale	kabala	k1gFnSc6	kabala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
romantická	romantický	k2eAgFnSc1d1	romantická
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
ovšem	ovšem	k9	ovšem
nedoložitelná	doložitelný	k2eNgFnSc1d1	nedoložitelná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
stejně	stejně	k6eAd1	stejně
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
rabiho	rabi	k1gMnSc2	rabi
Löwa	Löwus	k1gMnSc2	Löwus
jako	jako	k8xS	jako
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
pražském	pražský	k2eAgMnSc6d1	pražský
Golemovi	Golem	k1gMnSc6	Golem
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
znovu	znovu	k6eAd1	znovu
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Poznaně	Poznaň	k1gFnSc2	Poznaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
rabínem	rabín	k1gMnSc7	rabín
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
byl	být	k5eAaImAgMnS	být
důvod	důvod	k1gInSc4	důvod
jeho	jeho	k3xOp3gNnSc2	jeho
častého	častý	k2eAgNnSc2d1	časté
stěhování	stěhování	k1gNnSc2	stěhování
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
bezesporu	bezesporu	k9	bezesporu
nejslavnějšímu	slavný	k2eAgMnSc3d3	nejslavnější
rabínovi	rabín	k1gMnSc3	rabín
měla	mít	k5eAaImAgFnS	mít
údajně	údajně	k6eAd1	údajně
zachovat	zachovat	k5eAaPmF	zachovat
velmi	velmi	k6eAd1	velmi
macešsky	macešsky	k6eAd1	macešsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
jednoho	jeden	k4xCgNnSc2	jeden
až	až	k8xS	až
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
vrací	vracet	k5eAaImIp3nS	vracet
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
r.	r.	kA	r.
1609	[number]	k4	1609
<g/>
.	.	kIx.	.
</s>
<s>
Jehuda	Jehuda	k1gMnSc1	Jehuda
ben	ben	k?	ben
Becalel	Becalel	k1gMnSc1	Becalel
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Starém	starý	k2eAgInSc6d1	starý
židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názory	názor	k1gInPc1	názor
a	a	k8xC	a
postoje	postoj	k1gInPc1	postoj
==	==	k?	==
</s>
</p>
<p>
<s>
Rabi	rabi	k1gMnSc1	rabi
Löw	Löw	k1gMnSc1	Löw
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
v	v	k7c6	v
židovském	židovský	k2eAgInSc6d1	židovský
světě	svět	k1gInSc6	svět
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodně	rozhodně	k6eAd1	rozhodně
to	ten	k3xDgNnSc1	ten
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
své	svůj	k3xOyFgInPc4	svůj
styky	styk	k1gInPc4	styk
omezil	omezit	k5eAaPmAgInS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
židovské	židovský	k2eAgFnSc6d1	židovská
souvěrce	souvěrka	k1gFnSc6	souvěrka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
audience	audience	k1gFnSc2	audience
u	u	k7c2	u
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
udržoval	udržovat	k5eAaImAgInS	udržovat
osobní	osobní	k2eAgInPc4d1	osobní
přátelské	přátelský	k2eAgInPc4d1	přátelský
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Tychonem	Tychon	k1gInSc7	Tychon
Brahe	Brah	k1gFnSc2	Brah
i	i	k8xC	i
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
rudolfínská	rudolfínský	k2eAgFnSc1d1	rudolfínská
Praha	Praha	k1gFnSc1	Praha
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ohledně	ohledně	k7c2	ohledně
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
halaše	halacha	k1gFnSc3	halacha
preferoval	preferovat	k5eAaImAgInS	preferovat
studium	studium	k1gNnSc4	studium
přímo	přímo	k6eAd1	přímo
talmudických	talmudický	k2eAgInPc2d1	talmudický
pramenů	pramen	k1gInPc2	pramen
než	než	k8xS	než
tehdy	tehdy	k6eAd1	tehdy
módní	módní	k2eAgNnSc1d1	módní
studium	studium	k1gNnSc1	studium
halachických	halachický	k2eAgInPc2d1	halachický
kodexů	kodex	k1gInPc2	kodex
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
byl	být	k5eAaImAgInS	být
Maimonidova	Maimonidův	k2eAgFnSc1d1	Maimonidova
Mišne	Mišn	k1gInSc5	Mišn
Tora	Tor	k1gMnSc4	Tor
nebo	nebo	k8xC	nebo
Tur	tur	k1gMnSc1	tur
Ašera	Ašera	k1gMnSc1	Ašera
ben	ben	k?	ben
Jechiela	Jechiela	k1gFnSc1	Jechiela
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgInS	být
výjimečným	výjimečný	k2eAgMnSc7d1	výjimečný
pedagogem	pedagog	k1gMnSc7	pedagog
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
podle	podle	k7c2	podle
předepsaných	předepsaný	k2eAgFnPc2d1	předepsaná
osnov	osnova	k1gFnPc2	osnova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blázni	blázen	k1gMnPc1	blázen
dneška	dnešek	k1gInSc2	dnešek
učí	učit	k5eAaImIp3nP	učit
chlapce	chlapec	k1gMnPc4	chlapec
číst	číst	k5eAaImF	číst
Bibli	bible	k1gFnSc4	bible
s	s	k7c7	s
komentářem	komentář	k1gInSc7	komentář
Rašiho	Raši	k1gMnSc2	Raši
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
nerozumějí	rozumět	k5eNaImIp3nP	rozumět
<g/>
,	,	kIx,	,
a	a	k8xC	a
Talmud	talmud	k1gInSc1	talmud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
nemohou	moct	k5eNaImIp3nP	moct
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
měla	mít	k5eAaImAgFnS	mít
znít	znít	k5eAaImF	znít
jeho	jeho	k3xOp3gNnPc4	jeho
slova	slovo	k1gNnPc4	slovo
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
systému	systém	k1gInSc2	systém
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
podpořit	podpořit	k5eAaPmF	podpořit
studium	studium	k1gNnSc4	studium
Mišny	Mišna	k1gFnSc2	Mišna
a	a	k8xC	a
talmudické	talmudický	k2eAgFnSc2d1	talmudická
agady	agada	k1gFnSc2	agada
namísto	namísto	k7c2	namísto
tehdy	tehdy	k6eAd1	tehdy
nového	nový	k2eAgInSc2d1	nový
směru	směr	k1gInSc2	směr
studia	studio	k1gNnSc2	studio
Talmudu	talmud	k1gInSc2	talmud
zvaného	zvaný	k2eAgMnSc2d1	zvaný
pilpul	pilpout	k5eAaPmAgMnS	pilpout
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ale	ale	k9	ale
varoval	varovat	k5eAaImAgMnS	varovat
proti	proti	k7c3	proti
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
upnutí	upnutí	k1gNnSc3	upnutí
se	se	k3xPyFc4	se
na	na	k7c4	na
exaktní	exaktní	k2eAgFnPc4d1	exaktní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
věcech	věc	k1gFnPc6	věc
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Azarju	Azarju	k1gMnSc1	Azarju
de	de	k?	de
Rossiho	Rossi	k1gMnSc4	Rossi
<g/>
.	.	kIx.	.
<g/>
Ohledně	ohledně	k7c2	ohledně
jeho	on	k3xPp3gInSc2	on
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
kabale	kabala	k1gFnSc3	kabala
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Š.	Š.	kA	Š.
J.	J.	kA	J.
Rapaporta	Rapaporta	k1gFnSc1	Rapaporta
se	se	k3xPyFc4	se
kabale	kabala	k1gFnSc3	kabala
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Gershoma	Gershom	k1gMnSc2	Gershom
Scholema	Scholema	k1gFnSc1	Scholema
ovšem	ovšem	k9	ovšem
ano	ano	k9	ano
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
chasidismu	chasidismus	k1gInSc2	chasidismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
komentářů	komentář	k1gInPc2	komentář
rabiho	rabi	k1gMnSc2	rabi
Löwa	Löwus	k1gMnSc2	Löwus
stojí	stát	k5eAaImIp3nS	stát
etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
věnoval	věnovat	k5eAaImAgMnS	věnovat
několik	několik	k4yIc4	několik
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
též	též	k9	též
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
zřejmě	zřejmě	k6eAd1	zřejmě
pramení	pramenit	k5eAaImIp3nS	pramenit
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
znal	znát	k5eAaImAgMnS	znát
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
inspirovat	inspirovat	k5eAaBmF	inspirovat
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Rabbi	Rabbi	k1gNnSc1	Rabbi
Löw	Löw	k1gFnSc2	Löw
bývá	bývat	k5eAaImIp3nS	bývat
rovněž	rovněž	k9	rovněž
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
předchůdce	předchůdce	k1gMnSc2	předchůdce
hnutí	hnutí	k1gNnSc2	hnutí
chasidismu	chasidismus	k1gInSc2	chasidismus
–	–	k?	–
i	i	k8xC	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
spekulaci	spekulace	k1gFnSc4	spekulace
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
podobnostech	podobnost	k1gFnPc6	podobnost
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
rabiho	rabi	k1gMnSc2	rabi
Löwa	Löwus	k1gMnSc2	Löwus
a	a	k8xC	a
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
chasidských	chasidský	k2eAgMnPc2d1	chasidský
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Rabi	rabi	k1gMnSc1	rabi
Löw	Löw	k1gMnSc1	Löw
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
plodný	plodný	k2eAgMnSc1d1	plodný
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
řadu	řada	k1gFnSc4	řada
spisů	spis	k1gInPc2	spis
(	(	kIx(	(
<g/>
výčet	výčet	k1gInSc1	výčet
není	být	k5eNaImIp3nS	být
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Derech	Derech	k1gInSc1	Derech
chajim	chajim	k1gInSc1	chajim
(	(	kIx(	(
<g/>
komentář	komentář	k1gInSc1	komentář
k	k	k7c3	k
Pirkej	Pirkej	k1gFnPc3	Pirkej
avot	avot	k1gInSc4	avot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Netivot	Netivot	k1gMnSc1	Netivot
olam	olámat	k5eAaPmRp2nS	olámat
–	–	k?	–
pokračování	pokračování	k1gNnPc1	pokračování
Derech	Derech	k1gInSc1	Derech
chajim	chajim	k1gInSc1	chajim
</s>
</p>
<p>
<s>
Tif	Tif	k?	Tif
<g/>
'	'	kIx"	'
<g/>
eret	eret	k1gMnSc1	eret
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
–	–	k?	–
o	o	k7c6	o
výjimečnosti	výjimečnost	k1gFnSc6	výjimečnost
Tóry	tóra	k1gFnSc2	tóra
a	a	k8xC	a
jejích	její	k3xOp3gNnPc2	její
přikázání	přikázání	k1gNnPc2	přikázání
</s>
</p>
<p>
<s>
Be	Be	k?	Be
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
ha-gola	haola	k1gFnSc1	ha-gola
–	–	k?	–
pokračování	pokračování	k1gNnSc1	pokračování
Tif	Tif	k1gMnSc1	Tif
<g/>
'	'	kIx"	'
<g/>
eret	eret	k1gMnSc1	eret
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
soustředěné	soustředěný	k2eAgInPc1d1	soustředěný
především	především	k9	především
na	na	k7c4	na
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
obtížných	obtížný	k2eAgFnPc2d1	obtížná
pasáží	pasáž	k1gFnPc2	pasáž
Talmudu	talmud	k1gInSc2	talmud
a	a	k8xC	a
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
Talmudu	talmud	k1gInSc2	talmud
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
</s>
</p>
<p>
<s>
Necach	Necach	k1gMnSc1	Necach
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
–	–	k?	–
o	o	k7c6	o
vykoupení	vykoupení	k1gNnSc6	vykoupení
<g/>
,	,	kIx,	,
mesiášských	mesiášský	k2eAgFnPc6d1	mesiášská
nadějích	naděje	k1gFnPc6	naděje
a	a	k8xC	a
znameních	znamení	k1gNnPc6	znamení
<g/>
,	,	kIx,	,
eschatologii	eschatologie	k1gFnSc3	eschatologie
jako	jako	k8xC	jako
celku	celek	k1gInSc3	celek
</s>
</p>
<p>
<s>
Or	Or	k?	Or
chadaš	chadaš	k1gInSc1	chadaš
–	–	k?	–
o	o	k7c4	o
Ester	Ester	k1gFnSc4	Ester
a	a	k8xC	a
Purim	Purim	k1gInSc4	Purim
</s>
</p>
<p>
<s>
Ner	Ner	k?	Ner
micva	micva	k1gFnSc1	micva
–	–	k?	–
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
Chanuka	chanuka	k1gFnSc1	chanuka
</s>
</p>
<p>
<s>
Gur	Gur	k?	Gur
arje	arjat	k5eAaPmIp3nS	arjat
–	–	k?	–
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
komentáře	komentář	k1gInSc2	komentář
Rašiho	Raši	k1gMnSc2	Raši
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
komentářů	komentář	k1gInPc2	komentář
k	k	k7c3	k
Tanachu	Tanach	k1gInSc3	Tanach
<g/>
,	,	kIx,	,
targumu	targum	k1gInSc3	targum
a	a	k8xC	a
midrašům	midraš	k1gMnPc3	midraš
</s>
</p>
<p>
<s>
Gvurot	Gvurot	k1gInSc1	Gvurot
ha-Šem	ha-Šem	k1gInSc1	ha-Šem
–	–	k?	–
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
Pesach	pesach	k1gInSc1	pesach
<g/>
,	,	kIx,	,
o	o	k7c6	o
vyvedení	vyvedení	k1gNnSc6	vyvedení
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Hagada	Hagada	k1gFnSc1	Hagada
</s>
</p>
<p>
<s>
Pesachová	Pesachový	k2eAgFnSc1d1	Pesachová
hagada	hagada	k1gFnSc1	hagada
–	–	k?	–
včetně	včetně	k7c2	včetně
výkladu	výklad	k1gInSc2	výklad
o	o	k7c4	o
šabat	šabat	k1gInSc4	šabat
ha-gadol	haadola	k1gFnPc2	ha-gadola
</s>
</p>
<p>
<s>
Chidušej	Chidušet	k5eAaImRp2nS	Chidušet
Jore	Jore	k1gInSc1	Jore
de	de	k?	de
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
–	–	k?	–
novely	novela	k1gFnSc2	novela
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
dílu	díl	k1gInSc3	díl
halachického	halachický	k2eAgInSc2d1	halachický
kodexu	kodex	k1gInSc2	kodex
Arba	arba	k1gFnSc1	arba
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
turim	turim	k6eAd1	turim
</s>
</p>
<p>
<s>
řady	řada	k1gFnPc1	řada
dalších	další	k2eAgFnPc2d1	další
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
respons	responsa	k1gFnPc2	responsa
a	a	k8xC	a
rukopisůMaharalovy	rukopisůMaharalův	k2eAgInPc4d1	rukopisůMaharalův
spisy	spis	k1gInPc4	spis
byly	být	k5eAaImAgInP	být
několikrát	několikrát	k6eAd1	několikrát
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
(	(	kIx(	(
<g/>
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Mossad	Mossad	k1gInSc1	Mossad
ha-rav	haat	k5eAaPmDgInS	ha-rat
Kook	Kooko	k1gNnPc2	Kooko
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
nebylo	být	k5eNaImAgNnS	být
dodnes	dodnes	k6eAd1	dodnes
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
výběr	výběr	k1gInSc1	výběr
textů	text	k1gInPc2	text
ze	z	k7c2	z
spisů	spis	k1gInPc2	spis
"	"	kIx"	"
<g/>
Ner	Ner	k1gFnSc1	Ner
micva	micva	k1gFnSc1	micva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Svíce	svíce	k1gFnSc1	svíce
příkazu	příkaz	k1gInSc2	příkaz
<g/>
)	)	kIx)	)
věnovaného	věnovaný	k2eAgInSc2d1	věnovaný
svátku	svátek	k1gInSc2	svátek
Chanuka	chanuka	k1gFnSc1	chanuka
a	a	k8xC	a
"	"	kIx"	"
<g/>
Netivot	Netivot	k1gInSc1	Netivot
olam	olámat	k5eAaPmRp2nS	olámat
<g/>
"	"	kIx"	"
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
P	P	kA	P
<g/>
3	[number]	k4	3
<g/>
K.	K.	kA	K.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KURAS	KURAS	kA	KURAS
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
Golema	Golem	k1gMnSc4	Golem
<g/>
.	.	kIx.	.
</s>
<s>
Rabbi	Rabbi	k1gNnSc1	Rabbi
Löw	Löw	k1gFnSc2	Löw
<g/>
,	,	kIx,	,
židovství	židovství	k1gNnSc2	židovství
a	a	k8xC	a
češství	češství	k1gNnSc2	češství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7281	[number]	k4	7281
<g/>
-	-	kIx~	-
<g/>
462	[number]	k4	462
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAFKA	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
pražský	pražský	k2eAgMnSc1d1	pražský
rabi	rabi	k1gMnSc1	rabi
Jehuda	Jehuda	k1gMnSc1	Jehuda
Löw	Löw	k1gMnSc1	Löw
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
vyprávění	vyprávění	k1gNnPc1	vyprávění
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7017	[number]	k4	7017
<g/>
-	-	kIx~	-
<g/>
816	[number]	k4	816
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOŠNÁŘ	KOŠNÁŘ	kA	KOŠNÁŘ
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
<g/>
.	.	kIx.	.
</s>
<s>
Staropražské	staropražský	k2eAgFnPc1d1	staropražská
pověsti	pověst	k1gFnPc1	pověst
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vincentinum	Vincentinum	k1gNnSc1	Vincentinum
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
TOMEK	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgFnPc1d1	Pražská
židovské	židovský	k2eAgFnPc1d1	židovská
pověsti	pověst	k1gFnPc1	pověst
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
staropražských	staropražský	k2eAgFnPc2d1	staropražská
historických	historický	k2eAgFnPc2d1	historická
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
židovských	židovský	k2eAgInPc2d1	židovský
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
pohanských	pohanský	k2eAgFnPc2d1	pohanská
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Končel	Končel	k1gMnSc1	Končel
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BESALEL	BESALEL	kA	BESALEL
BEN	Ben	k1gInSc1	Ben
LEVA	leva	k1gInSc1	leva
<g/>
,	,	kIx,	,
Jehuda	Jehuda	k1gFnSc1	Jehuda
<g/>
.	.	kIx.	.
</s>
<s>
Lampa	lampa	k1gFnSc1	lampa
přikázání	přikázání	k1gNnPc2	přikázání
(	(	kIx(	(
<g/>
Ner	Ner	k1gMnSc1	Ner
miswa	miswa	k1gMnSc1	miswa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2959-1	[number]	k4	978-80-200-2959-1
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Golem	Golem	k1gMnSc1	Golem
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jehuda	Jehud	k1gMnSc2	Jehud
ben	ben	k?	ben
Becalel	Becalel	k1gMnSc2	Becalel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Židovská	židovská	k1gFnSc1	židovská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
/	/	kIx~	/
<g/>
Jehuda	Jehuda	k1gMnSc1	Jehuda
Löw	Löw	k1gMnSc1	Löw
ben	ben	k?	ben
Becalel	Becalel	k1gMnSc1	Becalel
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Löw	Löw	k?	Löw
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Výstava	výstava	k1gFnSc1	výstava
představí	představit	k5eAaPmIp3nS	představit
rabiho	rabi	k1gMnSc4	rabi
Löwa	Löwum	k1gNnSc2	Löwum
–	–	k?	–
skutečného	skutečný	k2eAgNnSc2d1	skutečné
i	i	k8xC	i
vybájeného	vybájený	k2eAgNnSc2d1	vybájené
–	–	k?	–
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Výstava	výstava	k1gFnSc1	výstava
o	o	k7c6	o
rabim	rabi	k1gMnSc6	rabi
Löwovi	Löwa	k1gMnSc6	Löwa
je	být	k5eAaImIp3nS	být
nádherná	nádherný	k2eAgFnSc1d1	nádherná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stísněná	stísněný	k2eAgFnSc1d1	stísněná
–	–	k?	–
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
<g/>
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
</p>
