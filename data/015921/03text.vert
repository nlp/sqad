<s>
Sluchadlo	sluchadlo	k1gNnSc1
</s>
<s>
Příklad	příklad	k1gInSc1
sluchadla	sluchadlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
většinu	většina	k1gFnSc4
techniky	technika	k1gFnSc2
umístěnu	umístěn	k2eAgFnSc4d1
za	za	k7c7
uchem	ucho	k1gNnSc7
</s>
<s>
Příklad	příklad	k1gInSc1
sluchadla	sluchadlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
se	se	k3xPyFc4
celé	celá	k1gFnSc2
vkládá	vkládat	k5eAaImIp3nS
do	do	k7c2
ucha	ucho	k1gNnSc2
</s>
<s>
Typy	typ	k1gInPc1
sluchadel	sluchadlo	k1gNnPc2
</s>
<s>
Sluchadlo	sluchadlo	k1gNnSc1
(	(	kIx(
<g/>
nesprávně	správně	k6eNd1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
naslouchadlo	naslouchadlo	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc4d1
elektroakustický	elektroakustický	k2eAgInSc4d1
přístroj	přístroj	k1gInSc4
sloužící	sloužící	k2eAgInSc4d1
k	k	k7c3
zlepšení	zlepšení	k1gNnSc3
sluchu	sluch	k1gInSc2
při	při	k7c6
částečné	částečný	k2eAgFnSc6d1
hluchotě	hluchota	k1gFnSc6
zesilováním	zesilování	k1gNnSc7
a	a	k8xC
modulací	modulace	k1gFnSc7
zvuku	zvuk	k1gInSc2
a	a	k8xC
především	především	k9
řeči	řeč	k1gFnSc2
přicházející	přicházející	k2eAgFnSc1d1
z	z	k7c2
okolního	okolní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
historickým	historický	k2eAgMnSc7d1
předchůdcem	předchůdce	k1gMnSc7
byla	být	k5eAaImAgFnS
čistě	čistě	k6eAd1
akustická	akustický	k2eAgNnPc1d1
sluchadla	sluchadlo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
podobu	podoba	k1gFnSc4
jednoduché	jednoduchý	k2eAgFnSc2d1
obrácené	obrácený	k2eAgFnSc2d1
trumpety	trumpeta	k1gFnSc2
a	a	k8xC
zvuk	zvuk	k1gInSc4
zesilovala	zesilovat	k5eAaImAgFnS
pasivně	pasivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemůže	moct	k5eNaImIp3nS
svému	svůj	k3xOyFgMnSc3
nositeli	nositel	k1gMnSc3
zajistit	zajistit	k5eAaPmF
zcela	zcela	k6eAd1
normální	normální	k2eAgInSc4d1
sluch	sluch	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
mělo	mít	k5eAaImAgNnS
by	by	kYmCp3nS
poskytnout	poskytnout	k5eAaPmF
co	co	k9
největší	veliký	k2eAgInSc4d3
přínos	přínos	k1gInSc4
při	při	k7c6
kompenzaci	kompenzace	k1gFnSc6
sluchové	sluchový	k2eAgFnSc2d1
vady	vada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
sluchadel	sluchadlo	k1gNnPc2
<g/>
,	,	kIx,
sluchadla	sluchadlo	k1gNnPc4
závěsná	závěsný	k2eAgNnPc4d1
a	a	k8xC
sluchadla	sluchadlo	k1gNnPc4
nitorušní	nitorušní	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sluchadlo	sluchadlo	k1gNnSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
napájeno	napájet	k5eAaImNgNnS
zinko-vzdušnou	zinko-vzdušný	k2eAgFnSc7d1
baterií	baterie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
sluchadel	sluchadlo	k1gNnPc2
</s>
<s>
Sluchadla	sluchadlo	k1gNnPc4
lze	lze	k6eAd1
dělit	dělit	k5eAaImF
do	do	k7c2
několika	několik	k4yIc2
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
dle	dle	k7c2
tvaru	tvar	k1gInSc2
<g/>
:	:	kIx,
kapesní	kapesní	k2eAgNnPc1d1
<g/>
,	,	kIx,
závěsná	závěsný	k2eAgNnPc1d1
<g/>
,	,	kIx,
brýlová	brýlový	k2eAgNnPc1d1
<g/>
,	,	kIx,
boltcová	boltcový	k2eAgNnPc1d1
<g/>
,	,	kIx,
zvukovodová	zvukovodový	k2eAgNnPc1d1
</s>
<s>
dle	dle	k7c2
způsobu	způsob	k1gInSc2
předání	předání	k1gNnSc4
akustické	akustický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
:	:	kIx,
sluchadla	sluchadlo	k1gNnPc4
pro	pro	k7c4
vzdušné	vzdušný	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
<g/>
,	,	kIx,
sluchadla	sluchadlo	k1gNnPc1
pro	pro	k7c4
kostní	kostní	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
</s>
<s>
dle	dle	k7c2
vlastností	vlastnost	k1gFnPc2
<g/>
:	:	kIx,
sluchadla	sluchadlo	k1gNnSc2
s	s	k7c7
možností	možnost	k1gFnSc7
proměnného	proměnný	k2eAgNnSc2d1
zesílení	zesílení	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
nastavitelným	nastavitelný	k2eAgInSc7d1
frekvenčním	frekvenční	k2eAgInSc7d1
průběhem	průběh	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
omezením	omezení	k1gNnSc7
výstupního	výstupní	k2eAgInSc2d1
akustického	akustický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
modulačními	modulační	k2eAgInPc7d1
prvky	prvek	k1gInPc7
</s>
<s>
Mezi	mezi	k7c4
známé	známý	k2eAgInPc4d1
typy	typ	k1gInPc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
závěsná	závěsný	k2eAgNnPc1d1
sluchadla	sluchadlo	k1gNnPc1
(	(	kIx(
<g/>
BTE	BTE	kA
<g/>
)	)	kIx)
<g/>
jsou	být	k5eAaImIp3nP
vhodná	vhodný	k2eAgFnSc1d1
především	především	k6eAd1
pro	pro	k7c4
těžké	těžký	k2eAgFnPc4d1
sluchové	sluchový	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
anebo	anebo	k8xC
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
zavedení	zavedení	k1gNnSc1
sluchadla	sluchadlo	k1gNnSc2
do	do	k7c2
zvukovodu	zvukovod	k1gInSc2
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
z	z	k7c2
důvodu	důvod	k1gInSc2
úzkého	úzký	k2eAgInSc2d1
zvukovodu	zvukovod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládají	skládat	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
pouzdra	pouzdro	k1gNnSc2
a	a	k8xC
propojení	propojení	k1gNnSc2
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouzdro	pouzdro	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
elektronický	elektronický	k2eAgInSc4d1
obvod	obvod	k1gInSc4
<g/>
,	,	kIx,
ovládání	ovládání	k1gNnSc4
<g/>
,	,	kIx,
baterii	baterie	k1gFnSc4
<g/>
,	,	kIx,
mikrofon	mikrofon	k1gInSc4
<g/>
/	/	kIx~
<g/>
y	y	k?
a	a	k8xC
často	často	k6eAd1
také	také	k9
reproduktor	reproduktor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouzdro	pouzdro	k1gNnSc1
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
za	za	k7c7
ušním	ušní	k2eAgInSc7d1
boltcem	boltec	k1gInSc7
s	s	k7c7
propojením	propojení	k1gNnSc7
vedeným	vedený	k2eAgNnSc7d1
dolů	dol	k1gInPc2
do	do	k7c2
ucha	ucho	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvuk	zvuk	k1gInSc1
ze	z	k7c2
sluchadla	sluchadlo	k1gNnSc2
je	být	k5eAaImIp3nS
do	do	k7c2
ucha	ucho	k1gNnSc2
přenášen	přenášen	k2eAgInSc1d1
akusticky	akusticky	k6eAd1
tenkou	tenký	k2eAgFnSc7d1
plastovou	plastový	k2eAgFnSc7d1
trubičkou	trubička	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
nitroušní	nitroušní	k2eAgFnSc1d1
<g/>
,	,	kIx,
zvukovodovájsou	zvukovodovájsa	k1gFnSc7
vkládány	vkládat	k5eAaImNgInP
přímo	přímo	k6eAd1
do	do	k7c2
zvukovodu	zvukovod	k1gInSc2
<g/>
,	,	kIx,
skořepina	skořepina	k1gFnSc1
je	být	k5eAaImIp3nS
zhotovena	zhotovit	k5eAaPmNgFnS
podle	podle	k7c2
přesného	přesný	k2eAgInSc2d1
otisku	otisk	k1gInSc2
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
v	v	k7c6
sobě	se	k3xPyFc3
celou	celý	k2eAgFnSc4d1
elektroniku	elektronika	k1gFnSc4
<g/>
;	;	kIx,
nedisponují	disponovat	k5eNaBmIp3nP
velkým	velký	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
</s>
<s>
zvukovodová	zvukovodový	k2eAgNnPc1d1
sluchadla	sluchadlo	k1gNnPc1
(	(	kIx(
<g/>
ITE	ITE	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
jsou	být	k5eAaImIp3nP
umístěna	umístit	k5eAaPmNgFnS
ve	v	k7c6
vnějším	vnější	k2eAgInSc6d1
zvukovodu	zvukovod	k1gInSc6
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
concha	conch	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvukovodová	Zvukovodový	k2eAgNnPc1d1
sluchadla	sluchadlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
vyrobena	vyrobit	k5eAaPmNgNnP
individuálně	individuálně	k6eAd1
přímo	přímo	k6eAd1
podle	podle	k7c2
ucha	ucho	k1gNnSc2
pacienta	pacient	k1gMnSc2
a	a	k8xC
navržena	navrhnout	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pohodlně	pohodlně	k6eAd1
pasovala	pasovat	k5eAaImAgFnS,k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
vhodná	vhodný	k2eAgNnPc4d1
pro	pro	k7c4
lehké	lehký	k2eAgInPc4d1
až	až	k9
těžké	těžký	k2eAgFnSc2d1
sluchové	sluchový	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ventilace	ventilace	k1gFnSc1
u	u	k7c2
těchto	tento	k3xDgNnPc2
sluchadel	sluchadlo	k1gNnPc2
může	moct	k5eAaImIp3nS
způsobovat	způsobovat	k5eAaImF
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ventilaci	ventilace	k1gFnSc6
umožňuje	umožňovat	k5eAaImIp3nS
tenká	tenký	k2eAgFnSc1d1
trubička	trubička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
vyrovnání	vyrovnání	k1gNnSc4
tlaku	tlak	k1gInSc2
a	a	k8xC
její	její	k3xOp3gNnSc1
provedení	provedení	k1gNnSc1
má	mít	k5eAaImIp3nS
také	také	k9
vliv	vliv	k1gInSc4
na	na	k7c4
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpětná	zpětný	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
<g/>
,	,	kIx,
svištění	svištění	k1gNnSc1
či	či	k8xC
pískání	pískání	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
prosakováním	prosakování	k1gNnSc7
zvuku	zvuk	k1gInSc2
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
na	na	k7c6
vyšších	vysoký	k2eAgFnPc6d2
frekvencích	frekvence	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
znovu	znovu	k6eAd1
zesílen	zesílit	k5eAaPmNgInS
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
problém	problém	k1gInSc4
u	u	k7c2
těžkých	těžký	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
sluchu	sluch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
větší	veliký	k2eAgNnSc4d2
provedení	provedení	k1gNnSc4
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
baterií	baterie	k1gFnSc7
<g/>
,	,	kIx,
zasahují	zasahovat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
zvukovodového	zvukovodový	k2eAgInSc2d1
vchodu	vchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
kanálová	kanálový	k2eAgNnPc1d1
sluchadla	sluchadlo	k1gNnPc1
(	(	kIx(
<g/>
CIC	cic	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
jsou	být	k5eAaImIp3nP
nejmenším	malý	k2eAgInSc7d3
modelem	model	k1gInSc7
sluchadel	sluchadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gMnSc1
sluchadel	sluchadlo	k1gNnPc2
tvarem	tvar	k1gInSc7
kopíruje	kopírovat	k5eAaImIp3nS
ušní	ušní	k2eAgInSc4d1
kanál	kanál	k1gInSc4
a	a	k8xC
není	být	k5eNaImIp3nS
téměř	téměř	k6eAd1
vidět	vidět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
sluchadla	sluchadlo	k1gNnPc1
vkládají	vkládat	k5eAaImIp3nP
hlouběji	hluboko	k6eAd2
do	do	k7c2
ušního	ušní	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
není	být	k5eNaImIp3nS
vidět	vidět	k5eAaImF
ani	ani	k8xC
při	při	k7c6
přímém	přímý	k2eAgInSc6d1
pohledu	pohled	k1gInSc6
na	na	k7c4
ucho	ucho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skořepina	skořepina	k1gFnSc1
kanálových	kanálový	k2eAgNnPc2d1
sluchadel	sluchadlo	k1gNnPc2
je	být	k5eAaImIp3nS
vyrobena	vyroben	k2eAgFnSc1d1
přímo	přímo	k6eAd1
dle	dle	k7c2
ucha	ucho	k1gNnSc2
pacienta	pacient	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
její	její	k3xOp3gNnSc4
nošení	nošení	k1gNnSc4
není	být	k5eNaImIp3nS
nepohodlné	pohodlný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
obsažena	obsáhnout	k5eAaPmNgFnS
veškerá	veškerý	k3xTgFnSc1
elektronika	elektronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sluchadla	sluchadlo	k1gNnPc1
mají	mít	k5eAaImIp3nP
odvětrávání	odvětrávání	k1gNnSc1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
hluboké	hluboký	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
v	v	k7c6
ušním	ušní	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
poskytuje	poskytovat	k5eAaImIp3nS
přirozenější	přirozený	k2eAgInSc1d2
poslechový	poslechový	k2eAgInSc1d1
zážitek	zážitek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
typů	typ	k1gInPc2
sluchadel	sluchadlo	k1gNnPc2
tento	tento	k3xDgInSc4
typ	typ	k1gInSc1
nezakrývá	zakrývat	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
část	část	k1gFnSc4
ušního	ušní	k2eAgInSc2d1
boltce	boltec	k1gInSc2
plastovou	plastový	k2eAgFnSc7d1
skořepinou	skořepina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
způsobuje	způsobovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zvuk	zvuk	k1gInSc1
je	být	k5eAaImIp3nS
přijímán	přijímat	k5eAaImNgInS
přirozeněji	přirozeně	k6eAd2
tvarem	tvar	k1gInSc7
ušního	ušní	k2eAgInSc2d1
boltce	boltec	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
přenášen	přenášet	k5eAaImNgInS
do	do	k7c2
ušního	ušní	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
stejně	stejně	k6eAd1
jako	jako	k9
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
přenášen	přenášet	k5eAaImNgMnS
bez	bez	k7c2
sluchadla	sluchadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
modely	model	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
použití	použití	k1gNnSc4
mobilního	mobilní	k2eAgInSc2d1
telefonu	telefon	k1gInSc2
jako	jako	k8xS,k8xC
dálkového	dálkový	k2eAgNnSc2d1
ovládání	ovládání	k1gNnSc2
k	k	k7c3
nastavení	nastavení	k1gNnSc3
sluchadla	sluchadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
ITC	ITC	kA
(	(	kIx(
<g/>
in	in	k?
the	the	k?
concha	concha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
vyplňuje	vyplňovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
spodní	spodní	k2eAgFnSc4d1
část	část	k1gFnSc4
zvukovodu	zvukovod	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
uchu	ucho	k1gNnSc6
je	být	k5eAaImIp3nS
již	již	k6eAd1
nápadnější	nápadní	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s>
kapesní	kapesní	k2eAgInSc1d1
sluchadlavyvinul	sluchadlavyvinout	k5eAaPmAgInS
je	on	k3xPp3gFnPc4
Harvey	Harvea	k1gFnPc4
Fletcher	Fletchra	k1gFnPc2
během	během	k7c2
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
pro	pro	k7c4
Bell	bell	k1gInSc4
Laboratories	Laboratoriesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapesní	kapesní	k2eAgNnSc1d1
sluchadla	sluchadlo	k1gNnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
pouzdra	pouzdro	k1gNnSc2
a	a	k8xC
ušního	ušní	k2eAgInSc2d1
závěsu	závěs	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouzdro	pouzdro	k1gNnSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
velikosti	velikost	k1gFnSc3
balíčku	balíček	k1gInSc2
hracích	hrací	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
zesilovač	zesilovač	k1gInSc4
<g/>
,	,	kIx,
baterii	baterie	k1gFnSc4
a	a	k8xC
ovládací	ovládací	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
závěsu	závěs	k1gInSc6
je	být	k5eAaImIp3nS
tenkou	tenký	k2eAgFnSc7d1
ohebnou	ohebný	k2eAgFnSc7d1
šňůrkou	šňůrka	k1gFnSc7
připojen	připojen	k2eAgInSc1d1
miniaturní	miniaturní	k2eAgInSc1d1
reproduktor	reproduktor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
omezení	omezení	k1gNnPc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
velikosti	velikost	k1gFnSc3
malých	malý	k2eAgNnPc2d1
sluchadel	sluchadlo	k1gNnPc2
<g/>
,	,	kIx,
kapesní	kapesní	k2eAgNnSc1d1
sluchadlo	sluchadlo	k1gNnSc1
může	moct	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
velké	velký	k2eAgNnSc4d1
zesílení	zesílení	k1gNnSc4
a	a	k8xC
dlouhou	dlouhý	k2eAgFnSc4d1
životnost	životnost	k1gFnSc4
baterie	baterie	k1gFnSc1
za	za	k7c4
nižší	nízký	k2eAgFnSc4d2
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sluchátku	sluchátko	k1gNnSc6
je	být	k5eAaImIp3nS
nasazena	nasazen	k2eAgFnSc1d1
tvarovka	tvarovka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
s	s	k7c7
reproduktorem	reproduktor	k1gInSc7
ve	v	k7c6
zvukovodu	zvukovod	k1gInSc6
(	(	kIx(
<g/>
RIC	RIC	kA
<g/>
)	)	kIx)
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
sluchadel	sluchadlo	k1gNnPc2
závěsných	závěsný	k2eAgInPc2d1
(	(	kIx(
<g/>
BTE	BTE	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ve	v	k7c6
sluchadlech	sluchadlo	k1gNnPc6
RIC	RIC	kA
umístěn	umístěn	k2eAgInSc1d1
reproduktor	reproduktor	k1gInSc1
přímo	přímo	k6eAd1
v	v	k7c6
ušní	ušní	k2eAgFnSc6d1
koncovce	koncovka	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
zvukovodu	zvukovod	k1gInSc6
<g/>
,	,	kIx,
namísto	namísto	k7c2
v	v	k7c6
pouzdře	pouzdro	k1gNnSc6
sluchadla	sluchadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
RIC	RIC	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
pouzdra	pouzdro	k1gNnSc2
umístěného	umístěný	k2eAgNnSc2d1
za	za	k7c7
uchem	ucho	k1gNnSc7
<g/>
,	,	kIx,
tenkého	tenký	k2eAgInSc2d1
drátku	drátek	k1gInSc2
spojujícím	spojující	k2eAgInSc7d1
pouzdro	pouzdro	k1gNnSc4
s	s	k7c7
reproduktorem	reproduktor	k1gInSc7
a	a	k8xC
vlastního	vlastní	k2eAgInSc2d1
reproduktoru	reproduktor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
vložen	vložit	k5eAaPmNgInS
přímo	přímo	k6eAd1
do	do	k7c2
zvukovodu	zvukovod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reproduktor	reproduktor	k1gInSc1
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
silikonovém	silikonový	k2eAgNnSc6d1
pouzdře	pouzdro	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
formě	forma	k1gFnSc6
špuntu	špuntu	k?
zafixováno	zafixován	k2eAgNnSc4d1
do	do	k7c2
ucha	ucho	k1gNnSc2
pacienta	pacient	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
sluchadla	sluchadlo	k1gNnPc1
ukotvená	ukotvený	k2eAgNnPc1d1
v	v	k7c6
kosti	kost	k1gFnSc6
(	(	kIx(
<g/>
BAHA	BAHA	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Bone	bon	k1gInSc5
Anchored	Anchored	k1gInSc4
Hearing	Hearing	k1gInSc1
Aid	Aida	k1gFnPc2
<g/>
)	)	kIx)
<g/>
je	být	k5eAaImIp3nS
sluchadlo	sluchadlo	k1gNnSc4
zakotvené	zakotvený	k2eAgNnSc4d1
v	v	k7c6
kosti	kost	k1gFnSc6
a	a	k8xC
k	k	k7c3
přenosu	přenos	k1gInSc3
zvuku	zvuk	k1gInSc2
používá	používat	k5eAaImIp3nS
vibrace	vibrace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pacient	pacient	k1gMnSc1
trpí	trpět	k5eAaImIp3nS
poruchou	porucha	k1gFnSc7
vzdušného	vzdušný	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
pro	pro	k7c4
přenos	přenos	k1gInSc4
zvuku	zvuk	k1gInSc2
použije	použít	k5eAaPmIp3nS
tzv.	tzv.	kA
kostní	kostní	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
tekutiny	tekutina	k1gFnPc1
ve	v	k7c6
vnitřním	vnitřní	k2eAgNnSc6d1
uchu	ucho	k1gNnSc6
rozkmitávají	rozkmitávat	k5eAaImIp3nP
přímým	přímý	k2eAgInSc7d1
přenosem	přenos	k1gInSc7
vibrací	vibrace	k1gFnPc2
lebeční	lebeční	k2eAgFnSc2d1
kosti	kost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sluchadla	sluchadlo	k1gNnPc1
BAHA	BAHA	kA
jsou	být	k5eAaImIp3nP
k	k	k7c3
lebeční	lebeční	k2eAgFnSc3d1
kosti	kost	k1gFnSc3
připojena	připojit	k5eAaPmNgFnS
pomocí	pomocí	k7c2
titanového	titanový	k2eAgInSc2d1
implantátu	implantát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
tvoří	tvořit	k5eAaImIp3nP
titanový	titanový	k2eAgInSc4d1
šroub	šroub	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zašroubovaný	zašroubovaný	k2eAgInSc1d1
asi	asi	k9
3	#num#	k4
až	až	k9
4	#num#	k4
mm	mm	kA
do	do	k7c2
lebeční	lebeční	k2eAgFnSc2d1
kosti	kost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vibrace	vibrace	k1gFnPc1
nejsou	být	k5eNaImIp3nP
tlumeny	tlumit	k5eAaImNgFnP
kůží	kůže	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
brýlová	brýlový	k2eAgNnPc1d1
sluchadlaSluchadlo	sluchadlaSluchadlo	k1gNnSc1
je	být	k5eAaImIp3nS
skryto	skrýt	k5eAaPmNgNnS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
obou	dva	k4xCgFnPc6
nožičkách	nožička	k1gFnPc6
brýlí	brýle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
sluchadla	sluchadlo	k1gNnPc1
výjimkou	výjimka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Zpracování	zpracování	k1gNnSc1
signálu	signál	k1gInSc2
</s>
<s>
Každé	každý	k3xTgNnSc1
sluchadlo	sluchadlo	k1gNnSc1
má	mít	k5eAaImIp3nS
přinejmenším	přinejmenším	k6eAd1
mikrofon	mikrofon	k1gInSc4
<g/>
,	,	kIx,
reproduktor	reproduktor	k1gInSc4
<g/>
,	,	kIx,
baterii	baterie	k1gFnSc4
a	a	k8xC
elektrický	elektrický	k2eAgInSc4d1
obvod	obvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrický	elektrický	k2eAgInSc1d1
obvod	obvod	k1gInSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
dle	dle	k7c2
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
stejný	stejný	k2eAgInSc4d1
typ	typ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvody	obvod	k1gInPc7
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
do	do	k7c2
kategorií	kategorie	k1gFnPc2
dle	dle	k7c2
způsobu	způsob	k1gInSc2
zpracování	zpracování	k1gNnSc2
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
analogové	analogový	k2eAgFnPc1d1
nebo	nebo	k8xC
digitální	digitální	k2eAgFnPc1d1
a	a	k8xC
dále	daleko	k6eAd2
dle	dle	k7c2
typů	typ	k1gInPc2
ovládání	ovládání	k1gNnSc2
obvodu	obvod	k1gInSc2
a	a	k8xC
tedy	tedy	k9
nastavitelné	nastavitelný	k2eAgFnSc2d1
nebo	nebo	k8xC
programovatelné	programovatelný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Analogové	analogový	k2eAgInPc1d1
audio	audio	k2eAgInPc1d1
obvody	obvod	k1gInPc1
</s>
<s>
Nastavitelné	nastavitelný	k2eAgNnSc1d1
<g/>
:	:	kIx,
Akustický	akustický	k2eAgInSc1d1
obvod	obvod	k1gInSc1
je	být	k5eAaImIp3nS
analogový	analogový	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
elektronickými	elektronický	k2eAgFnPc7d1
součástkami	součástka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnSc4
nastavení	nastavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foniatr	foniatr	k1gMnSc1
určí	určit	k5eAaPmIp3nS
zesílení	zesílení	k1gNnSc4
a	a	k8xC
ostatní	ostatní	k1gNnSc4
charakteristiky	charakteristika	k1gFnSc2
sluchadla	sluchadlo	k1gNnSc2
potřebné	potřebný	k2eAgNnSc1d1
pro	pro	k7c4
pacienta	pacient	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
nastaví	nastavit	k5eAaPmIp3nS,k5eAaBmIp3nS
sluchadlo	sluchadlo	k1gNnSc4
buď	buď	k8xC
pomocí	pomocí	k7c2
malých	malý	k2eAgNnPc2d1
ovládání	ovládání	k1gNnPc2
přímo	přímo	k6eAd1
na	na	k7c6
sluchadle	sluchadlo	k1gNnSc6
<g/>
,	,	kIx,
anebo	anebo	k8xC
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
obvodu	obvod	k1gInSc2
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
nejméně	málo	k6eAd3
flexibilní	flexibilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc4
funkční	funkční	k2eAgNnSc4d1
sluchadlo	sluchadlo	k1gNnSc4
s	s	k7c7
analogovým	analogový	k2eAgInSc7d1
akustickým	akustický	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Programovatelné	programovatelný	k2eAgNnSc1d1
<g/>
:	:	kIx,
Akustický	akustický	k2eAgInSc1d1
obvod	obvod	k1gInSc1
je	být	k5eAaImIp3nS
analogový	analogový	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
dalšími	další	k2eAgInPc7d1
ovládacími	ovládací	k2eAgInPc7d1
elektrickými	elektrický	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
lze	lze	k6eAd1
naprogramovat	naprogramovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládací	ovládací	k2eAgInSc1d1
obvod	obvod	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
upraven	upravit	k5eAaPmNgInS
během	během	k7c2
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
anebo	anebo	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
foniatr	foniatr	k1gMnSc1
může	moct	k5eAaImIp3nS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
počítače	počítač	k1gInSc2
propojeného	propojený	k2eAgInSc2d1
se	s	k7c7
sluchadlem	sluchadlo	k1gNnSc7
naprogramovat	naprogramovat	k5eAaPmF
přídavné	přídavný	k2eAgInPc4d1
ovládací	ovládací	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacient	pacient	k1gMnSc1
se	se	k3xPyFc4
sluchadlem	sluchadlo	k1gNnSc7
může	moct	k5eAaImIp3nS
měnit	měnit	k5eAaImF
program	program	k1gInSc4
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
akustická	akustický	k2eAgNnPc4d1
prostředí	prostředí	k1gNnPc4
<g/>
,	,	kIx,
buď	buď	k8xC
pomocí	pomocí	k7c2
ovládání	ovládání	k1gNnSc2
na	na	k7c6
samotném	samotný	k2eAgNnSc6d1
sluchadle	sluchadlo	k1gNnSc6
<g/>
,	,	kIx,
dálkovým	dálkový	k2eAgNnSc7d1
ovládáním	ovládání	k1gNnSc7
<g/>
,	,	kIx,
anebo	anebo	k8xC
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
případech	případ	k1gInPc6
ovládací	ovládací	k2eAgInPc4d1
obvody	obvod	k1gInPc4
fungují	fungovat	k5eAaImIp3nP
automaticky	automaticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
obvodů	obvod	k1gInPc2
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
více	hodně	k6eAd2
flexibilní	flexibilní	k2eAgFnSc2d1
oproti	oproti	k7c3
jednoduchým	jednoduchý	k2eAgInPc3d1
ovládacím	ovládací	k2eAgInPc3d1
obvodům	obvod	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc4
sluchadlo	sluchadlo	k1gNnSc4
s	s	k7c7
analogovým	analogový	k2eAgInSc7d1
akustickým	akustický	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
a	a	k8xC
automatickým	automatický	k2eAgInSc7d1
digitálním	digitální	k2eAgInSc7d1
ovládacím	ovládací	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Digitální	digitální	k2eAgInSc1d1
audio	audio	k2eAgInSc1d1
obvod	obvod	k1gInSc1
</s>
<s>
Digitální	digitální	k2eAgInSc1d1
akustický	akustický	k2eAgInSc1d1
obvod	obvod	k1gInSc1
s	s	k7c7
programovatelným	programovatelný	k2eAgInSc7d1
ovládacím	ovládací	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
obvody	obvod	k1gInPc1
<g/>
,	,	kIx,
akustický	akustický	k2eAgInSc1d1
i	i	k8xC
přídavný	přídavný	k2eAgInSc1d1
ovládací	ovládací	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
plně	plně	k6eAd1
digitální	digitální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plně	plně	k6eAd1
digitální	digitální	k2eAgInPc1d1
obvody	obvod	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
implementaci	implementace	k1gFnSc4
mnoha	mnoho	k4c2
přídavných	přídavný	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nejsou	být	k5eNaImIp3nP
možné	možný	k2eAgNnSc4d1
s	s	k7c7
použitím	použití	k1gNnSc7
analogových	analogový	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgInPc1d1
obvody	obvod	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
použity	použít	k5eAaPmNgInP
ve	v	k7c6
všech	všecek	k3xTgInPc6
druzích	druh	k1gInPc6
sluchadel	sluchadlo	k1gNnPc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
nejvíce	nejvíce	k6eAd1,k6eAd3
flexibilní	flexibilní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
například	například	k6eAd1
naprogramována	naprogramovat	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měla	mít	k5eAaImAgFnS
větší	veliký	k2eAgNnSc4d2
zesílení	zesílení	k1gNnSc4
v	v	k7c6
určitém	určitý	k2eAgNnSc6d1
frekvenčním	frekvenční	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgNnSc1d1
sluchadla	sluchadlo	k1gNnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
naprogramována	naprogramován	k2eAgFnSc1d1
více	hodně	k6eAd2
programy	program	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vybírány	vybírat	k5eAaImNgInP
uživatelem	uživatel	k1gMnSc7
<g/>
,	,	kIx,
anebo	anebo	k8xC
jsou	být	k5eAaImIp3nP
plně	plně	k6eAd1
automatická	automatický	k2eAgFnSc1d1
a	a	k8xC
adaptivní	adaptivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
programy	program	k1gInPc1
redukují	redukovat	k5eAaBmIp3nP
akustickou	akustický	k2eAgFnSc4d1
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
(	(	kIx(
<g/>
pískání	pískání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
redukují	redukovat	k5eAaBmIp3nP
hluk	hluk	k1gInSc1
pozadí	pozadí	k1gNnSc2
<g/>
,	,	kIx,
detekují	detekovat	k5eAaImIp3nP
a	a	k8xC
automaticky	automaticky	k6eAd1
nastavují	nastavovat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
hlasitý	hlasitý	k2eAgInSc1d1
vs	vs	k?
potichý	potichý	k2eAgInSc1d1
<g/>
,	,	kIx,
mluva	mluva	k1gFnSc1
vs	vs	k?
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
tichý	tichý	k2eAgInSc1d1
vs	vs	k?
hlučný	hlučný	k2eAgInSc4d1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovládají	ovládat	k5eAaImIp3nP
přídavné	přídavný	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
vícenásobné	vícenásobný	k2eAgInPc4d1
mikrofony	mikrofon	k1gInPc4
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
prostorového	prostorový	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
transponují	transponovat	k5eAaBmIp3nP
frekvence	frekvence	k1gFnPc4
(	(	kIx(
<g/>
posun	posun	k1gInSc1
frekvencí	frekvence	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
uživatel	uživatel	k1gMnSc1
neslyší	slyšet	k5eNaImIp3nS
do	do	k7c2
nižších	nízký	k2eAgInPc2d2
kmitočtů	kmitočet	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
sluch	sluch	k1gInSc4
lepší	dobrý	k2eAgInSc4d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
implementuje	implementovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
dalších	další	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgInPc1d1
obvody	obvod	k1gInPc1
také	také	k9
umožňují	umožňovat	k5eAaImIp3nP
bezdrátové	bezdrátový	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
jak	jak	k6eAd1
akustických	akustický	k2eAgNnPc2d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
ovládacích	ovládací	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládací	ovládací	k2eAgInPc1d1
signály	signál	k1gInPc1
ve	v	k7c6
sluchadle	sluchadlo	k1gNnSc6
v	v	k7c6
jednom	jeden	k4xCgNnSc6
uchu	ucho	k1gNnSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
bezdrátově	bezdrátově	k6eAd1
přeneseny	přenést	k5eAaPmNgFnP
do	do	k7c2
ovládacích	ovládací	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
sluchadla	sluchadlo	k1gNnSc2
na	na	k7c6
druhém	druhý	k4xOgInSc6
uchu	ucho	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zaručeno	zaručit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
audio	audio	k2eAgInSc4d1
signál	signál	k1gInSc4
v	v	k7c6
obou	dva	k4xCgNnPc6
uších	ucho	k1gNnPc6
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
spárovaný	spárovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anebo	anebo	k8xC
akustický	akustický	k2eAgInSc4d1
signál	signál	k1gInSc4
obsahuje	obsahovat	k5eAaImIp3nS
záměrně	záměrně	k6eAd1
rozdíly	rozdíl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
imitují	imitovat	k5eAaBmIp3nP
rozdíly	rozdíl	k1gInPc4
v	v	k7c6
normálním	normální	k2eAgInSc6d1
stereofonním	stereofonní	k2eAgInSc6d1
poslechu	poslech	k1gInSc6
k	k	k7c3
zajištění	zajištění	k1gNnSc3
prostorového	prostorový	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
zvuků	zvuk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akustické	akustický	k2eAgInPc1d1
signály	signál	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
bezdrátově	bezdrátově	k6eAd1
přenášeny	přenášet	k5eAaImNgInP
z	z	k7c2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
vnějších	vnější	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
přes	přes	k7c4
oddělený	oddělený	k2eAgInSc4d1
modul	modul	k1gInSc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
přívěsku	přívěsek	k1gInSc2
zvaný	zvaný	k2eAgInSc4d1
„	„	k?
<g/>
streamer	streamer	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
schopnost	schopnost	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
optimální	optimální	k2eAgNnSc4d1
použití	použití	k1gNnSc4
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
,	,	kIx,
hudebních	hudební	k2eAgInPc2d1
přehrávačů	přehrávač	k1gInPc2
<g/>
,	,	kIx,
vzdálených	vzdálený	k2eAgInPc2d1
mikrofonů	mikrofon	k1gInPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
prakticky	prakticky	k6eAd1
použitelné	použitelný	k2eAgInPc1d1
plně	plně	k6eAd1
digitální	digitální	k2eAgNnSc4d1
sluchadlo	sluchadlo	k1gNnSc4
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KAŠPAR	Kašpar	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
Technické	technický	k2eAgFnPc4d1
a	a	k8xC
kompenzační	kompenzační	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
pro	pro	k7c4
osoby	osoba	k1gFnPc4
se	se	k3xPyFc4
sluchových	sluchový	k2eAgFnPc2d1
postižením	postižení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČKTZJ	ČKTZJ	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sluchadlo	sluchadlo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4025412-4	4025412-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85059616	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85059616	#num#	k4
</s>
