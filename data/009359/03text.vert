<p>
<s>
Polyethylentereftalát	Polyethylentereftalát	k1gInSc1	Polyethylentereftalát
je	být	k5eAaImIp3nS	být
termoplast	termoplast	k1gInSc4	termoplast
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
polyesterů	polyester	k1gInPc2	polyester
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
PET	PET	kA	PET
z	z	k7c2	z
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
ethylen	ethylen	k1gInSc1	ethylen
tereftalát	tereftalát	k1gInSc1	tereftalát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
(	(	kIx(	(
<g/>
ethan-	ethan-	k?	ethan-
<g/>
1,2	[number]	k4	1,2
<g/>
-diolu	iol	k1gInSc2	-diol
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
esterifikací	esterifikace	k1gFnPc2	esterifikace
<g/>
)	)	kIx)	)
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
tereftalovou	tereftalův	k2eAgFnSc7d1	tereftalův
nebo	nebo	k8xC	nebo
transesterifikací	transesterifikace	k1gFnSc7	transesterifikace
dimethyltereftalátu	dimethyltereftalát	k1gInSc2	dimethyltereftalát
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dimethyltereftalátový	Dimethyltereftalátový	k2eAgInSc1d1	Dimethyltereftalátový
proces	proces	k1gInSc1	proces
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
postupu	postup	k1gInSc6	postup
dimethyltereftalát	dimethyltereftalát	k1gInSc1	dimethyltereftalát
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
nadbytkem	nadbytek	k1gInSc7	nadbytek
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
150	[number]	k4	150
–	–	k?	–
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
bazické	bazický	k2eAgFnPc4d1	bazická
katalýzy	katalýza	k1gFnPc4	katalýza
<g/>
.	.	kIx.	.
</s>
<s>
Methanol	methanol	k1gInSc1	methanol
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odstraňován	odstraňovat	k5eAaImNgInS	odstraňovat
destilací	destilace	k1gFnSc7	destilace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
posunuje	posunovat	k5eAaImIp3nS	posunovat
reakční	reakční	k2eAgFnSc4d1	reakční
rovnováhu	rovnováha	k1gFnSc4	rovnováha
požadovaným	požadovaný	k2eAgInSc7d1	požadovaný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
nadbytek	nadbytek	k1gInSc1	nadbytek
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
odstraněn	odstranit	k5eAaPmNgInS	odstranit
vakuovou	vakuový	k2eAgFnSc7d1	vakuová
destilací	destilace	k1gFnSc7	destilace
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
transesterifikační	transesterifikační	k2eAgInSc1d1	transesterifikační
krok	krok	k1gInSc1	krok
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
270	[number]	k4	270
–	–	k?	–
280	[number]	k4	280
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
opětným	opětný	k2eAgNnSc7d1	opětné
odstraňováním	odstraňování	k1gNnSc7	odstraňování
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reakci	reakce	k1gFnSc4	reakce
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
krokC	krokC	k?	krokC
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
HOCH2CH2OH	HOCH2CH2OH	k1gFnSc2	HOCH2CH2OH
→	→	k?	→
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
CH3OH	CH3OH	k1gFnPc2	CH3OH
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
krokn	krokn	k1gInSc1	krokn
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
n	n	k0	n
+	+	kIx~	+
n	n	k0	n
HOCH2CH2OH	HOCH2CH2OH	k1gMnSc5	HOCH2CH2OH
</s>
</p>
<p>
<s>
===	===	k?	===
Postup	postup	k1gInSc4	postup
výroby	výroba	k1gFnSc2	výroba
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
tereftalové	tereftal	k1gMnPc1	tereftal
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
postupu	postup	k1gInSc6	postup
je	být	k5eAaImIp3nS	být
esterifikace	esterifikace	k1gFnSc1	esterifikace
ethylenglykolu	ethylenglykol	k1gInSc2	ethylenglykol
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
tereftalové	tereftal	k1gMnPc1	tereftal
prováděna	provádět	k5eAaImNgNnP	provádět
přímo	přímo	k6eAd1	přímo
za	za	k7c2	za
tlaku	tlak	k1gInSc2	tlak
2,7	[number]	k4	2,7
–	–	k?	–
5,5	[number]	k4	5,5
baru	bar	k1gInSc2	bar
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
220	[number]	k4	220
–	–	k?	–
260	[number]	k4	260
°	°	k?	°
<g/>
C.	C.	kA	C.
Vznikající	vznikající	k2eAgFnSc1d1	vznikající
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
odstraňována	odstraňovat	k5eAaImNgFnS	odstraňovat
destilačně	destilačně	k6eAd1	destilačně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
opět	opět	k6eAd1	opět
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
žádoucímu	žádoucí	k2eAgInSc3d1	žádoucí
posunu	posun	k1gInSc3	posun
reakční	reakční	k2eAgFnSc2d1	reakční
rovnováhy	rovnováha	k1gFnSc2	rovnováha
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
polyethylentereftalátu	polyethylentereftalát	k1gInSc2	polyethylentereftalát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
n	n	k0	n
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
n	n	k0	n
HOCH2CH2OH	HOCH2CH2OH	k1gFnPc6	HOCH2CH2OH
→	→	k?	→
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
n	n	k0	n
+	+	kIx~	+
2	[number]	k4	2
<g/>
n	n	k0	n
H2O	H2O	k1gMnPc4	H2O
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
PET	PET	kA	PET
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
především	především	k6eAd1	především
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vláken	vlákna	k1gFnPc2	vlákna
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
nemačkavostí	nemačkavost	k1gFnPc2	nemačkavost
a	a	k8xC	a
malou	malý	k2eAgFnSc7d1	malá
navlhavostí	navlhavost	k1gFnSc7	navlhavost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dacron	Dacron	k1gMnSc1	Dacron
<g/>
;	;	kIx,	;
na	na	k7c6	na
textilních	textilní	k2eAgInPc6d1	textilní
výrobcích	výrobek	k1gInPc6	výrobek
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
nesprávnou	správný	k2eNgFnSc4d1	nesprávná
zkratku	zkratka	k1gFnSc4	zkratka
PES	peso	k1gNnPc2	peso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
tohoto	tento	k3xDgInSc2	tento
polymeru	polymer	k1gInSc2	polymer
k	k	k7c3	k
polyesterům	polyester	k1gInPc3	polyester
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lahví	lahev	k1gFnPc2	lahev
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
označovaných	označovaný	k2eAgInPc2d1	označovaný
jen	jen	k8xS	jen
zkratkou	zkratka	k1gFnSc7	zkratka
PET	PET	kA	PET
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
obalů	obal	k1gInPc2	obal
a	a	k8xC	a
fólií	fólie	k1gFnPc2	fólie
<g/>
.	.	kIx.	.
</s>
<s>
Tenké	tenký	k2eAgFnPc4d1	tenká
fólie	fólie	k1gFnPc4	fólie
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgFnP	používat
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Mylar	Mylara	k1gFnPc2	Mylara
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znát	k5eAaImIp1nS	znát
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
obchodních	obchodní	k2eAgInPc2d1	obchodní
názvů	název	k1gInPc2	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Arnite	Arnit	k1gInSc5	Arnit
<g/>
,	,	kIx,	,
Impet	Impeta	k1gFnPc2	Impeta
<g/>
,	,	kIx,	,
Rynite	Rynit	k1gMnSc5	Rynit
<g/>
,	,	kIx,	,
Ertalyte	Ertalyt	k1gInSc5	Ertalyt
<g/>
,	,	kIx,	,
Hostaphan	Hostaphan	k1gInSc1	Hostaphan
<g/>
,	,	kIx,	,
Melinex	Melinex	k1gInSc1	Melinex
<g/>
,	,	kIx,	,
Dacron	Dacron	k1gInSc1	Dacron
<g/>
,	,	kIx,	,
Terylene	terylen	k1gInSc5	terylen
<g/>
,	,	kIx,	,
Trevira	Trevir	k1gMnSc2	Trevir
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
procesu	proces	k1gInSc6	proces
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc2	zpracování
(	(	kIx(	(
<g/>
rychlosti	rychlost	k1gFnSc3	rychlost
chladnutí	chladnutí	k1gNnSc2	chladnutí
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
připravit	připravit	k5eAaPmF	připravit
téměř	téměř	k6eAd1	téměř
amorfní	amorfní	k2eAgMnSc1d1	amorfní
PET	PET	kA	PET
(	(	kIx(	(
<g/>
průhledný	průhledný	k2eAgInSc1d1	průhledný
<g/>
)	)	kIx)	)
a	a	k8xC	a
polokrystalický	polokrystalický	k2eAgMnSc1d1	polokrystalický
PET	PET	kA	PET
(	(	kIx(	(
<g/>
mléčně	mléčně	k6eAd1	mléčně
zakalený	zakalený	k2eAgInSc1d1	zakalený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Recyklace	recyklace	k1gFnSc2	recyklace
==	==	k?	==
</s>
</p>
<p>
<s>
PET	PET	kA	PET
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
mechanicky	mechanicky	k6eAd1	mechanicky
recykluje	recyklovat	k5eAaBmIp3nS	recyklovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
čistota	čistota	k1gFnSc1	čistota
separovaného	separovaný	k2eAgInSc2d1	separovaný
odpadního	odpadní	k2eAgInSc2d1	odpadní
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
zálohový	zálohový	k2eAgInSc1d1	zálohový
systém	systém	k1gInSc1	systém
jako	jako	k8xS	jako
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tyto	tento	k3xDgFnPc4	tento
láhve	láhev	k1gFnPc4	láhev
opět	opět	k6eAd1	opět
ze	z	k7c2	z
100	[number]	k4	100
%	%	kIx~	%
recyklovat	recyklovat	k5eAaBmF	recyklovat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
láhví	láhev	k1gFnPc2	láhev
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgInSc1d1	stávající
sběrný	sběrný	k2eAgInSc1d1	sběrný
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
materiál	materiál	k1gInSc1	materiál
degraduje	degradovat	k5eAaBmIp3nS	degradovat
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
čirost	čirost	k1gFnSc1	čirost
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
výzkum	výzkum	k1gInSc1	výzkum
chemické	chemický	k2eAgFnSc2d1	chemická
recyklace	recyklace	k1gFnSc2	recyklace
PET	PET	kA	PET
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
patent	patent	k1gInSc1	patent
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Spotřebitelské	spotřebitelský	k2eAgFnPc1d1	spotřebitelská
aplikace	aplikace	k1gFnPc1	aplikace
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
třídění	třídění	k1gNnSc2	třídění
a	a	k8xC	a
recyklace	recyklace	k1gFnSc2	recyklace
označují	označovat	k5eAaImIp3nP	označovat
jedničkou	jednička	k1gFnSc7	jednička
uprostřed	uprostřed	k7c2	uprostřed
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
ze	z	k7c2	z
zacyklených	zacyklený	k2eAgFnPc2d1	zacyklená
šipek	šipka	k1gFnPc2	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
PET	PET	kA	PET
recykluje	recyklovat	k5eAaBmIp3nS	recyklovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
rovněž	rovněž	k9	rovněž
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
recyklátorů	recyklátor	k1gInPc2	recyklátor
PET	PET	kA	PET
láhví	láhev	k1gFnPc2	láhev
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
degradaci	degradace	k1gFnSc6	degradace
PET	PET	kA	PET
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
acetaldehyd	acetaldehyd	k1gInSc1	acetaldehyd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svým	svůj	k3xOyFgInSc7	svůj
nasládlým	nasládlý	k2eAgInSc7d1	nasládlý
zápachem	zápach	k1gInSc7	zápach
může	moct	k5eAaImIp3nS	moct
znehodnotit	znehodnotit	k5eAaPmF	znehodnotit
obsah	obsah	k1gInSc4	obsah
PET	PET	kA	PET
lahví	lahev	k1gFnSc7	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nápojů	nápoj	k1gInPc2	nápoj
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
oxid	oxid	k1gInSc4	oxid
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
PET	PET	kA	PET
jako	jako	k8xC	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
však	však	k9	však
dají	dát	k5eAaPmIp3nP	dát
jímat	jímat	k5eAaImF	jímat
a	a	k8xC	a
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
již	již	k6eAd1	již
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
samotné	samotný	k2eAgFnSc2d1	samotná
recyklace	recyklace	k1gFnSc2	recyklace
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
:	:	kIx,	:
PET	PET	kA	PET
lahve	lahev	k1gFnPc1	lahev
jsou	být	k5eAaImIp3nP	být
nepoužitelné	použitelný	k2eNgFnPc1d1	nepoužitelná
pro	pro	k7c4	pro
roztoky	roztoka	k1gFnPc4	roztoka
silných	silný	k2eAgFnPc2d1	silná
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
nejsou	být	k5eNaImIp3nP	být
odolné	odolný	k2eAgFnPc1d1	odolná
například	například	k6eAd1	například
vůči	vůči	k7c3	vůči
kyselině	kyselina	k1gFnSc3	kyselina
dusičné	dusičný	k2eAgFnSc3d1	dusičná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
polyethylentereftalát	polyethylentereftalát	k1gInSc1	polyethylentereftalát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Vědci	vědec	k1gMnPc1	vědec
objevili	objevit	k5eAaPmAgMnP	objevit
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
PET	PET	kA	PET
láhve	láhev	k1gFnSc2	láhev
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
