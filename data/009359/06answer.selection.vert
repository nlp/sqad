<s>
Polyethylentereftalát	Polyethylentereftalát	k1gInSc1	Polyethylentereftalát
je	být	k5eAaImIp3nS	být
termoplast	termoplast	k1gInSc4	termoplast
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
polyesterů	polyester	k1gInPc2	polyester
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
PET	PET	kA	PET
z	z	k7c2	z
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
ethylen	ethylen	k1gInSc1	ethylen
tereftalát	tereftalát	k1gInSc1	tereftalát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
