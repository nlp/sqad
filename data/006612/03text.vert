<s>
Klobouk	klobouk	k1gInSc1	klobouk
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
čepicí	čepice	k1gFnSc7	čepice
je	on	k3xPp3gFnPc4	on
větší	veliký	k2eAgFnPc4d2	veliký
a	a	k8xC	a
propracovanější	propracovaný	k2eAgFnPc4d2	propracovanější
a	a	k8xC	a
především	především	k6eAd1	především
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
střecha	střecha	k1gFnSc1	střecha
či	či	k8xC	či
korpus	korpus	k1gInSc1	korpus
mívá	mívat	k5eAaImIp3nS	mívat
ploché	plochý	k2eAgNnSc4d1	ploché
dýnko	dýnko	k1gNnSc4	dýnko
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInSc4d1	spodní
okraj	okraj	k1gInSc4	okraj
je	být	k5eAaImIp3nS	být
široký	široký	k2eAgInSc4d1	široký
<g/>
,	,	kIx,	,
plochý	plochý	k2eAgInSc4d1	plochý
nebo	nebo	k8xC	nebo
zvlněný	zvlněný	k2eAgInSc4d1	zvlněný
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
krempa	krempa	k1gFnSc1	krempa
<g/>
,	,	kIx,	,
obepíná	obepínat	k5eAaImIp3nS	obepínat
celý	celý	k2eAgInSc1d1	celý
obvod	obvod	k1gInSc1	obvod
klobouku	klobouk	k1gInSc2	klobouk
jako	jako	k8xS	jako
okruží	okruží	k1gNnSc2	okruží
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
<g/>
-li	i	k?	-li
klobouk	klobouk	k1gInSc1	klobouk
krempu	krempa	k1gFnSc4	krempa
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
čelní	čelní	k2eAgInSc4d1	čelní
štítek	štítek	k1gInSc4	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
klobouku	klobouk	k1gInSc2	klobouk
je	být	k5eAaImIp3nS	být
vlněné	vlněný	k2eAgNnSc1d1	vlněné
sukno	sukno	k1gNnSc1	sukno
<g/>
,	,	kIx,	,
plst	plst	k1gFnSc1	plst
-	-	kIx~	-
zvířecí	zvířecí	k2eAgInPc1d1	zvířecí
chlupy	chlup	k1gInPc1	chlup
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
materiály	materiál	k1gInPc1	materiál
jiné	jiný	k2eAgInPc1d1	jiný
-	-	kIx~	-
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
lehké	lehký	k2eAgFnPc1d1	lehká
tkaniny	tkanina	k1gFnPc1	tkanina
jako	jako	k8xC	jako
len	len	k1gInSc1	len
<g/>
,	,	kIx,	,
rostlinná	rostlinný	k2eAgNnPc1d1	rostlinné
vlákna	vlákno	k1gNnPc1	vlákno
jako	jako	k8xS	jako
sláma	sláma	k1gFnSc1	sláma
nebo	nebo	k8xC	nebo
štípaný	štípaný	k2eAgInSc1d1	štípaný
bambus	bambus	k1gInSc1	bambus
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Klobouky	Klobouky	k1gInPc1	Klobouky
bývají	bývat	k5eAaImIp3nP	bývat
opatřeny	opatřen	k2eAgInPc1d1	opatřen
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
ozdobami	ozdoba	k1gFnPc7	ozdoba
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
stuhou	stuha	k1gFnSc7	stuha
s	s	k7c7	s
mašlí	mašle	k1gFnSc7	mašle
<g/>
,	,	kIx,	,
štítkem	štítek	k1gInSc7	štítek
<g/>
,	,	kIx,	,
monogramem	monogram	k1gInSc7	monogram
<g/>
,	,	kIx,	,
erbem	erb	k1gInSc7	erb
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stuhou	stuha	k1gFnSc7	stuha
bývá	bývat	k5eAaImIp3nS	bývat
vetknuto	vetknout	k5eAaPmNgNnS	vetknout
ptačí	ptačí	k2eAgNnSc1d1	ptačí
pírko	pírko	k1gNnSc1	pírko
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
dýnko	dýnko	k1gNnSc1	dýnko
ozdobeno	ozdoben	k2eAgNnSc1d1	ozdobeno
chocholem	chochol	k1gInSc7	chochol
z	z	k7c2	z
pštrosího	pštrosí	k2eAgNnSc2d1	pštrosí
či	či	k8xC	či
jiného	jiný	k2eAgNnSc2d1	jiné
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Klobouky	Klobouky	k1gInPc1	Klobouky
dámské	dámský	k2eAgInPc1d1	dámský
mívají	mívat	k5eAaImIp3nP	mívat
dvě	dva	k4xCgFnPc4	dva
mašle	mašle	k1gFnPc4	mašle
k	k	k7c3	k
zavázání	zavázání	k1gNnSc3	zavázání
pod	pod	k7c4	pod
bradu	brada	k1gFnSc4	brada
a	a	k8xC	a
hojnou	hojný	k2eAgFnSc4d1	hojná
výzdobu	výzdoba	k1gFnSc4	výzdoba
na	na	k7c6	na
krempě	krempa	k1gFnSc6	krempa
či	či	k8xC	či
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
umělé	umělý	k2eAgFnPc4d1	umělá
květiny	květina	k1gFnPc4	květina
či	či	k8xC	či
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
již	již	k6eAd1	již
na	na	k7c6	na
vyobrazeních	vyobrazení	k1gNnPc6	vyobrazení
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
klobouky	klobouk	k1gInPc7	klobouk
hromadně	hromadně	k6eAd1	hromadně
začaly	začít	k5eAaPmAgInP	začít
znovu	znovu	k6eAd1	znovu
užívat	užívat	k5eAaImF	užívat
až	až	k9	až
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
nebo	nebo	k8xC	nebo
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
příležitostech	příležitost	k1gFnPc6	příležitost
nahrazoval	nahrazovat	k5eAaImAgMnS	nahrazovat
panovnickou	panovnický	k2eAgFnSc4d1	panovnická
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
knížecí	knížecí	k2eAgInSc1d1	knížecí
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Herzogshut	Herzogshut	k2eAgInSc1d1	Herzogshut
<g/>
)	)	kIx)	)
rakouských	rakouský	k2eAgMnPc2d1	rakouský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgNnPc7d1	významné
obdobími	období	k1gNnPc7	období
rozvoje	rozvoj	k1gInSc2	rozvoj
klobouků	klobouk	k1gInPc2	klobouk
byly	být	k5eAaImAgFnP	být
renesance	renesance	k1gFnPc1	renesance
<g/>
,	,	kIx,	,
baroko	baroko	k1gNnSc1	baroko
a	a	k8xC	a
historické	historický	k2eAgInPc1d1	historický
styly	styl	k1gInPc1	styl
oblékání	oblékání	k1gNnSc2	oblékání
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Klobouky	Klobouky	k1gInPc1	Klobouky
byly	být	k5eAaImAgInP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
odznakem	odznak	k1gInSc7	odznak
důstojenství	důstojenství	k1gNnSc2	důstojenství
(	(	kIx(	(
<g/>
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
církevních	církevní	k2eAgMnPc2d1	církevní
představitelů	představitel	k1gMnPc2	představitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úřadu	úřad	k1gInSc2	úřad
nebo	nebo	k8xC	nebo
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
uniforem	uniforma	k1gFnPc2	uniforma
a	a	k8xC	a
livrejů	livrej	k1gInPc2	livrej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
klobouky	klobouk	k1gInPc1	klobouk
vždy	vždy	k6eAd1	vždy
odznakem	odznak	k1gInSc7	odznak
svobodného	svobodný	k2eAgMnSc2d1	svobodný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
atributem	atribut	k1gInSc7	atribut
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
nositele	nositel	k1gMnSc2	nositel
či	či	k8xC	či
nositelky	nositelka	k1gFnSc2	nositelka
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
myslitelné	myslitelný	k2eAgNnSc1d1	myslitelné
vyjít	vyjít	k5eAaPmF	vyjít
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
prostovlasý	prostovlasý	k2eAgMnSc1d1	prostovlasý
<g/>
/	/	kIx~	/
<g/>
á.	á.	k?	á.
Na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
pozdravu	pozdrav	k1gInSc2	pozdrav
<g/>
,	,	kIx,	,
úcty	úcta	k1gFnSc2	úcta
či	či	k8xC	či
podřízenosti	podřízenost	k1gFnSc2	podřízenost
se	se	k3xPyFc4	se
pánské	pánský	k2eAgInPc1d1	pánský
klobouky	klobouk	k1gInPc1	klobouk
smekaly	smekat	k5eAaImAgInP	smekat
<g/>
.	.	kIx.	.
</s>
<s>
Smeknutí	smeknutí	k1gNnSc1	smeknutí
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
úklon	úklon	k1gInSc4	úklon
hlavy	hlava	k1gFnSc2	hlava
nebo	nebo	k8xC	nebo
poklona	poklona	k1gFnSc1	poklona
horní	horní	k2eAgFnSc2d1	horní
poloviny	polovina	k1gFnSc2	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
při	při	k7c6	při
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
a	a	k8xC	a
podobných	podobný	k2eAgFnPc6d1	podobná
příležitostech	příležitost	k1gFnPc6	příležitost
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
nesměl	smět	k5eNaImAgMnS	smět
žádný	žádný	k3yNgMnSc1	žádný
muž	muž	k1gMnSc1	muž
ponechat	ponechat	k5eAaPmF	ponechat
klobouk	klobouk	k1gInSc4	klobouk
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
nadřízeného	nadřízený	k1gMnSc2	nadřízený
služebně	služebně	k6eAd1	služebně
podřízený	podřízený	k1gMnSc1	podřízený
musel	muset	k5eAaImAgMnS	muset
klobouk	klobouk	k1gInSc4	klobouk
smeknout	smeknout	k5eAaPmF	smeknout
<g/>
.	.	kIx.	.
</s>
<s>
Smeknutím	smeknutí	k1gNnSc7	smeknutí
klobouku	klobouk	k1gInSc2	klobouk
muž	muž	k1gMnSc1	muž
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
nebo	nebo	k8xC	nebo
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
nošení	nošení	k1gNnSc4	nošení
klobouků	klobouk	k1gInPc2	klobouk
příležitostné	příležitostný	k2eAgNnSc1d1	příležitostné
<g/>
,	,	kIx,	,
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
stále	stále	k6eAd1	stále
trvají	trvat	k5eAaImIp3nP	trvat
klobouky	klobouk	k1gInPc1	klobouk
sluneční	sluneční	k2eAgInPc1d1	sluneční
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
bývá	bývat	k5eAaImIp3nS	bývat
tradiční	tradiční	k2eAgInSc4d1	tradiční
typ	typ	k1gInSc4	typ
městského	městský	k2eAgInSc2d1	městský
klobouku	klobouk	k1gInSc2	klobouk
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xC	jako
módní	módní	k2eAgInSc1d1	módní
prvek	prvek	k1gInSc1	prvek
nebo	nebo	k8xC	nebo
určitá	určitý	k2eAgFnSc1d1	určitá
extravagance	extravagance	k1gFnSc1	extravagance
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
většinou	většina	k1gFnSc7	většina
užívají	užívat	k5eAaImIp3nP	užívat
klobouky	klobouk	k1gInPc1	klobouk
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
společenských	společenský	k2eAgFnPc6d1	společenská
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bílý	bílý	k2eAgInSc1d1	bílý
klobouk	klobouk	k1gInSc1	klobouk
se	s	k7c7	s
závojem	závoj	k1gInSc7	závoj
jako	jako	k9	jako
svatební	svatební	k2eAgNnSc4d1	svatební
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
černý	černý	k2eAgInSc1d1	černý
klobouk	klobouk	k1gInSc1	klobouk
se	s	k7c7	s
závojem	závoj	k1gInSc7	závoj
jako	jako	k8xC	jako
smuteční	smuteční	k2eAgFnSc7d1	smuteční
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slušností	slušnost	k1gFnSc7	slušnost
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
brát	brát	k5eAaImF	brát
si	se	k3xPyFc3	se
klobouky	klobouk	k1gInPc4	klobouk
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
dostizích	dostih	k1gInPc6	dostih
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nosí	nosit	k5eAaImIp3nS	nosit
pánský	pánský	k2eAgInSc1d1	pánský
klobouk	klobouk	k1gInSc1	klobouk
"	"	kIx"	"
<g/>
trilby	trilba	k1gFnPc1	trilba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bontonu	bonton	k1gInSc3	bonton
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
smeknout	smeknout	k5eAaPmF	smeknout
klobouk	klobouk	k1gInSc1	klobouk
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc2	restaurace
<g/>
,	,	kIx,	,
kostela	kostel	k1gInSc2	kostel
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
veřejném	veřejný	k2eAgNnSc6d1	veřejné
nesení	nesení	k1gNnSc6	nesení
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
produkci	produkce	k1gFnSc6	produkce
státní	státní	k2eAgFnSc2d1	státní
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
během	během	k7c2	během
pohřbů	pohřeb	k1gInPc2	pohřeb
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
jede	jet	k5eAaImIp3nS	jet
okolo	okolo	k7c2	okolo
<g />
.	.	kIx.	.
</s>
<s>
pohřební	pohřební	k2eAgInSc4d1	pohřební
vůz	vůz	k1gInSc4	vůz
s	s	k7c7	s
nebožtíkem	nebožtík	k1gMnSc7	nebožtík
-	-	kIx~	-
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
australák	australák	k1gMnSc1	australák
buřinka	buřinka	k1gFnSc1	buřinka
-	-	kIx~	-
vyztužený	vyztužený	k2eAgInSc1d1	vyztužený
klobouk	klobouk	k1gInSc1	klobouk
z	z	k7c2	z
plsti	plst	k1gFnSc2	plst
<g/>
,	,	kIx,	,
s	s	k7c7	s
úzkou	úzký	k2eAgFnSc7d1	úzká
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
vzhůru	vzhůru	k6eAd1	vzhůru
otočenou	otočený	k2eAgFnSc7d1	otočená
střechou	střecha	k1gFnSc7	střecha
cylindr	cylindr	k1gInSc1	cylindr
fez	fez	k1gInSc4	fez
-	-	kIx~	-
orientální	orientální	k2eAgInSc4d1	orientální
klobouk	klobouk	k1gInSc4	klobouk
válcového	válcový	k2eAgInSc2d1	válcový
tvaru	tvar	k1gInSc2	tvar
se	s	k7c7	s
střapcem	střapec	k1gInSc7	střapec
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
červené	červený	k2eAgFnPc1d1	červená
barvy	barva	k1gFnPc1	barva
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
střapcem	střapec	k1gInSc7	střapec
girarďák	girarďák	k1gInSc1	girarďák
-	-	kIx~	-
čti	číst	k5eAaImRp2nS	číst
žirarďák	žirarďák	k1gInSc1	žirarďák
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
plstěný	plstěný	k2eAgInSc1d1	plstěný
klobouk	klobouk	k1gInSc1	klobouk
italské	italský	k2eAgFnSc2d1	italská
firmy	firma	k1gFnSc2	firma
Girardi	Girard	k1gMnPc1	Girard
homburg	homburg	k1gInSc1	homburg
-	-	kIx~	-
letní	letní	k2eAgInSc1d1	letní
plstěný	plstěný	k2eAgInSc1d1	plstěný
klobouk	klobouk	k1gInSc1	klobouk
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
bílé	bílý	k2eAgFnSc2d1	bílá
kávy	káva	k1gFnSc2	káva
jungle	jungle	k1gFnSc2	jungle
boonie	boonie	k1gFnSc2	boonie
hat	hat	k0	hat
-	-	kIx~	-
vojenský	vojenský	k2eAgInSc1d1	vojenský
klobouk	klobouk	k1gInSc1	klobouk
z	z	k7c2	z
bavlněné	bavlněný	k2eAgFnSc2d1	bavlněná
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
,	,	kIx,	,
potištěný	potištěný	k2eAgInSc1d1	potištěný
zeleno-hnědo-bílým	zelenonědoílý	k2eAgInSc7d1	zeleno-hnědo-bílý
maskovacím	maskovací	k2eAgInSc7d1	maskovací
vzorem	vzor	k1gInSc7	vzor
klak	klak	k1gInSc1	klak
-	-	kIx~	-
skládací	skládací	k2eAgInSc1d1	skládací
typ	typ	k1gInSc1	typ
cylindru	cylindr	k1gInSc2	cylindr
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgInSc4d1	cestovní
či	či	k8xC	či
kouzelnický	kouzelnický	k2eAgInSc4d1	kouzelnický
montera	monter	k1gMnSc4	monter
-	-	kIx~	-
černý	černý	k2eAgInSc1d1	černý
španělský	španělský	k2eAgInSc1d1	španělský
klobouk	klobouk	k1gInSc1	klobouk
matadorů	matador	k1gMnPc2	matador
při	při	k7c6	při
býčích	býčí	k2eAgInPc6d1	býčí
zápasech	zápas	k1gInPc6	zápas
panama	panama	k2eAgMnPc2d1	panama
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
též	též	k9	též
panamák	panamák	k?	panamák
-	-	kIx~	-
letní	letní	k2eAgInSc1d1	letní
klobouk	klobouk	k1gInSc1	klobouk
středoamerického	středoamerický	k2eAgInSc2d1	středoamerický
původu	původ	k1gInSc2	původ
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
palmy	palma	k1gFnSc2	palma
pastýřský	pastýřský	k2eAgInSc1d1	pastýřský
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
saturno	saturno	k1gNnSc1	saturno
či	či	k8xC	či
cappello	cappello	k1gNnSc1	cappello
romano	romana	k1gFnSc5	romana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
katolickým	katolický	k2eAgNnSc7d1	katolické
duchovenstvem	duchovenstvo	k1gNnSc7	duchovenstvo
a	a	k8xC	a
řádovými	řádový	k2eAgMnPc7d1	řádový
kanovníky	kanovník	k1gMnPc7	kanovník
kardinálský	kardinálský	k2eAgInSc4d1	kardinálský
klobouk	klobouk	k1gInSc4	klobouk
-	-	kIx~	-
purpurové	purpurový	k2eAgFnPc1d1	purpurová
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
šňůrami	šňůra	k1gFnPc7	šňůra
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
šesti	šest	k4xCc7	šest
střapci	střapec	k1gInPc7	střapec
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
<g />
.	.	kIx.	.
</s>
<s>
též	též	k9	též
v	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
erbu	erb	k1gInSc2	erb
arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
klobouk	klobouk	k1gInSc1	klobouk
biskupský	biskupský	k2eAgInSc1d1	biskupský
klobouk	klobouk	k1gInSc1	klobouk
stetson	stetson	k1gInSc1	stetson
-	-	kIx~	-
klobouk	klobouk	k1gInSc1	klobouk
amerických	americký	k2eAgMnPc2d1	americký
kovbojů	kovboj	k1gMnPc2	kovboj
<g/>
,	,	kIx,	,
plstěný	plstěný	k2eAgInSc1d1	plstěný
nebo	nebo	k8xC	nebo
kožený	kožený	k2eAgInSc1d1	kožený
slaměný	slaměný	k2eAgInSc1d1	slaměný
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
slamák	slamák	k1gInSc1	slamák
<g/>
)	)	kIx)	)
slaměný	slaměný	k2eAgInSc1d1	slaměný
kuželový	kuželový	k2eAgInSc1d1	kuželový
klobouk	klobouk	k1gInSc1	klobouk
sombrero	sombrero	k1gNnSc1	sombrero
-	-	kIx~	-
původem	původ	k1gInSc7	původ
mexický	mexický	k2eAgInSc1d1	mexický
kuželovitý	kuželovitý	k2eAgInSc1d1	kuželovitý
klobouk	klobouk	k1gInSc1	klobouk
se	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
střechou	střecha	k1gFnSc7	střecha
toka	toka	k1gFnSc1	toka
-	-	kIx~	-
dámský	dámský	k2eAgInSc1d1	dámský
klobouk	klobouk	k1gInSc1	klobouk
vojenské	vojenský	k2eAgInPc1d1	vojenský
klobouky	klobouk	k1gInPc1	klobouk
<g/>
:	:	kIx,	:
dvourohý	dvourohý	k2eAgInSc1d1	dvourohý
klobouk	klobouk	k1gInSc1	klobouk
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
((	((	k?	((
<g/>
napoleonský	napoleonský	k2eAgMnSc1d1	napoleonský
<g/>
))	))	k?	))
třírohý	třírohý	k2eAgInSc1d1	třírohý
klobouk	klobouk	k1gInSc1	klobouk
-	-	kIx~	-
Klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
houby	houba	k1gFnPc1	houba
<g/>
)	)	kIx)	)
Pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klobouk	klobouk	k1gInSc1	klobouk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
klobouk	klobouk	k1gInSc1	klobouk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Radek	Radek	k1gMnSc1	Radek
POLÁCH	Polách	k1gMnSc1	Polách
<g/>
,	,	kIx,	,
Klobouk	klobouk	k1gInSc1	klobouk
dolů	dol	k1gInPc2	dol
<g/>
:	:	kIx,	:
klobouky	klobouk	k1gInPc1	klobouk
a	a	k8xC	a
pokrývky	pokrývka	k1gFnPc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
slavných	slavný	k2eAgFnPc2d1	slavná
osobností	osobnost	k1gFnPc2	osobnost
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
katalog	katalog	k1gInSc1	katalog
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
výstavy	výstava	k1gFnSc2	výstava
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
Novojičínska	Novojičínsko	k1gNnSc2	Novojičínsko
cv	cv	k?	cv
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
a	a	k8xC	a
ve	v	k7c6	v
Valdštejnském	valdštejnský	k2eAgInSc6d1	valdštejnský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Huts	Huts	k6eAd1	Huts
</s>
