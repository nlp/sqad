<s>
Dysfonie	dysfonie	k1gFnSc1	dysfonie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
fonace	fonace	k1gFnSc2	fonace
čili	čili	k8xC	čili
tvorby	tvorba	k1gFnSc2	tvorba
hlasu	hlas	k1gInSc2	hlas
v	v	k7c6	v
hlasivkách	hlasivka	k1gFnPc6	hlasivka
a	a	k8xC	a
hrtanu	hrtan	k1gInSc6	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klasifikace	klasifikace	k1gFnSc2	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
ICD-10	ICD-10	k1gMnSc1	ICD-10
má	mít	k5eAaImIp3nS	mít
kód	kód	k1gInSc4	kód
R	R	kA	R
<g/>
49.0	[number]	k4	49.0
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
patologická	patologický	k2eAgFnSc1d1	patologická
změna	změna	k1gFnSc1	změna
hlasu	hlas	k1gInSc2	hlas
(	(	kIx(	(
<g/>
chrapot	chrapot	k1gInSc1	chrapot
<g/>
,	,	kIx,	,
přeskakování	přeskakování	k1gNnSc1	přeskakování
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
výšky	výška	k1gFnSc2	výška
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chraplavý	chraplavý	k2eAgMnSc1d1	chraplavý
<g/>
,	,	kIx,	,
zastřený	zastřený	k2eAgMnSc1d1	zastřený
a	a	k8xC	a
pacient	pacient	k1gMnSc1	pacient
nemůže	moct	k5eNaImIp3nS	moct
plně	plně	k6eAd1	plně
ovládat	ovládat	k5eAaImF	ovládat
jeho	jeho	k3xOp3gFnSc4	jeho
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
barvu	barva	k1gFnSc4	barva
nebo	nebo	k8xC	nebo
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Krajní	krajní	k2eAgFnSc1d1	krajní
forma	forma	k1gFnSc1	forma
dysfonie	dysfonie	k1gFnSc2	dysfonie
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
hlasu	hlas	k1gInSc2	hlas
čili	čili	k8xC	čili
afonie	afonie	k1gFnSc2	afonie
<g/>
.	.	kIx.	.
</s>
<s>
Dysfonie	dysfonie	k1gFnSc1	dysfonie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
organická	organický	k2eAgFnSc1d1	organická
nebo	nebo	k8xC	nebo
funkční	funkční	k2eAgFnSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Organická	organický	k2eAgFnSc1d1	organická
dysfonie	dysfonie	k1gFnSc1	dysfonie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
například	například	k6eAd1	například
zánětem	zánět	k1gInSc7	zánět
<g/>
,	,	kIx,	,
novotvarem	novotvar	k1gInSc7	novotvar
nebo	nebo	k8xC	nebo
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
poškozením	poškození	k1gNnSc7	poškození
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hormonálními	hormonální	k2eAgFnPc7d1	hormonální
změnami	změna	k1gFnPc7	změna
například	například	k6eAd1	například
při	při	k7c6	při
mutaci	mutace	k1gFnSc6	mutace
<g/>
,	,	kIx,	,
menopauze	menopauza	k1gFnSc6	menopauza
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
(	(	kIx(	(
<g/>
stařecký	stařecký	k2eAgInSc1d1	stařecký
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1	funkční
dysfonie	dysfonie	k1gFnSc1	dysfonie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
přetížením	přetížení	k1gNnSc7	přetížení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
řečníků	řečník	k1gMnPc2	řečník
a	a	k8xC	a
učitelů	učitel	k1gMnPc2	učitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
špatnými	špatný	k2eAgInPc7d1	špatný
hlasovými	hlasový	k2eAgInPc7d1	hlasový
návyky	návyk	k1gInPc7	návyk
<g/>
,	,	kIx,	,
psychickými	psychický	k2eAgFnPc7d1	psychická
poruchami	porucha	k1gFnPc7	porucha
a	a	k8xC	a
neurózou	neuróza	k1gFnSc7	neuróza
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
být	být	k5eAaImF	být
symptomem	symptom	k1gInSc7	symptom
jiné	jiný	k2eAgFnPc4d1	jiná
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
