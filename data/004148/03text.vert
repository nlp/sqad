<s>
Česká	český	k2eAgFnSc1d1	Česká
kinematografie	kinematografie	k1gFnSc1	kinematografie
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
filmy	film	k1gInPc4	film
natočené	natočený	k2eAgInPc4d1	natočený
na	na	k7c6	na
území	území	k1gNnSc6	území
nynější	nynější	k2eAgFnSc2d1	nynější
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
ohlasu	ohlas	k1gInSc2	ohlas
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
československá	československý	k2eAgFnSc1d1	Československá
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
Obchod	obchod	k1gInSc1	obchod
na	na	k7c6	na
korze	korzo	k1gNnSc6	korzo
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
vlaky	vlak	k1gInPc1	vlak
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kolja	Kolja	k1gMnSc1	Kolja
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
získaly	získat	k5eAaPmAgFnP	získat
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
dalších	další	k2eAgFnPc2d1	další
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
užší	úzký	k2eAgFnSc2d2	užší
nominace	nominace	k1gFnSc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zahrnována	zahrnován	k2eAgFnSc1d1	zahrnována
i	i	k8xC	i
tvorba	tvorba	k1gFnSc1	tvorba
českých	český	k2eAgMnPc2d1	český
exulantů	exulant	k1gMnPc2	exulant
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Passer	Passer	k1gMnSc1	Passer
či	či	k8xC	či
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
filmovým	filmový	k2eAgInSc7d1	filmový
záznamem	záznam	k1gInSc7	záznam
pořízeným	pořízený	k2eAgInSc7d1	pořízený
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
The	The	k1gFnSc2	The
Horitz	Horitza	k1gFnPc2	Horitza
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
věnovaný	věnovaný	k2eAgMnSc1d1	věnovaný
velkolepým	velkolepý	k2eAgFnPc3d1	velkolepá
pašijovým	pašijový	k2eAgFnPc3d1	pašijová
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
kameramanem	kameraman	k1gMnSc7	kameraman
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
natáčel	natáčet	k5eAaImAgMnS	natáčet
krátké	krátký	k2eAgInPc4d1	krátký
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
filmové	filmový	k2eAgFnSc2d1	filmová
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Josefem	Josef	k1gMnSc7	Josef
Švábem-Malostranským	Švábem-Malostranský	k2eAgMnSc7d1	Švábem-Malostranský
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
krátkých	krátký	k2eAgInPc6d1	krátký
filmových	filmový	k2eAgInPc6d1	filmový
skečích	skeč	k1gInPc6	skeč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
Výstavní	výstavní	k2eAgMnSc1d1	výstavní
párkař	párkař	k1gMnSc1	párkař
a	a	k8xC	a
lepič	lepič	k1gMnSc1	lepič
plakátů	plakát	k1gInPc2	plakát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
stálé	stálý	k2eAgNnSc4d1	stálé
kino	kino	k1gNnSc4	kino
založil	založit	k5eAaPmAgMnS	založit
Viktor	Viktor	k1gMnSc1	Viktor
Ponrepo	Ponrepa	k1gFnSc5	Ponrepa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Modré	modrý	k2eAgFnSc2d1	modrá
Štiky	štika	k1gFnSc2	štika
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
měla	mít	k5eAaImAgFnS	mít
kina	kino	k1gNnPc1	kino
podobu	podoba	k1gFnSc4	podoba
kočovných	kočovný	k2eAgInPc2d1	kočovný
stanů	stan	k1gInPc2	stan
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
putovní	putovní	k2eAgFnPc4d1	putovní
atrakce	atrakce	k1gFnPc4	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
natáčely	natáčet	k5eAaImAgInP	natáčet
nacionalistické	nacionalistický	k2eAgInPc1d1	nacionalistický
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgFnPc2d1	jiná
Utrpením	utrpení	k1gNnSc7	utrpení
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
F.	F.	kA	F.
Branald	Branald	k1gMnSc1	Branald
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
S.	S.	kA	S.
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Když	když	k8xS	když
struny	struna	k1gFnPc1	struna
lkají	lkát	k5eAaImIp3nP	lkát
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
září	září	k1gNnSc6	září
1930	[number]	k4	1930
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
konaly	konat	k5eAaImAgFnP	konat
krajně	krajně	k6eAd1	krajně
pravicovým	pravicový	k2eAgInSc7d1	pravicový
tiskem	tisk	k1gInSc7	tisk
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
demonstrace	demonstrace	k1gFnSc2	demonstrace
Čechů	Čech	k1gMnPc2	Čech
proti	proti	k7c3	proti
filmům	film	k1gInPc3	film
mluveným	mluvený	k2eAgInPc3d1	mluvený
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
všech	všecek	k3xTgInPc2	všecek
potenciálně	potenciálně	k6eAd1	potenciálně
pobuřujících	pobuřující	k2eAgInPc2d1	pobuřující
německých	německý	k2eAgInPc2d1	německý
filmů	film	k1gInPc2	film
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
protiválečného	protiválečný	k2eAgInSc2d1	protiválečný
snímku	snímek	k1gInSc2	snímek
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Georg	Georg	k1gMnSc1	Georg
Wilheim	Wilheim	k1gMnSc1	Wilheim
Pabst	Pabst	k1gMnSc1	Pabst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
omezoval	omezovat	k5eAaImAgInS	omezovat
import	import	k1gInSc1	import
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
a	a	k8xC	a
protežoval	protežovat	k5eAaImAgMnS	protežovat
domácí	domácí	k2eAgMnPc4d1	domácí
tvůrce	tvůrce	k1gMnPc4	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
kontingentní	kontingentní	k2eAgInSc1d1	kontingentní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
kvóta	kvóta	k1gFnSc1	kvóta
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
směl	smět	k5eAaImAgMnS	smět
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
film	film	k1gInSc4	film
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jeho	jeho	k3xOp3gMnSc1	jeho
výrobce	výrobce	k1gMnSc1	výrobce
dovézt	dovézt	k5eAaPmF	dovézt
7	[number]	k4	7
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
kvóta	kvóta	k1gFnSc1	kvóta
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
kontingentní	kontingentní	k2eAgInSc1d1	kontingentní
systém	systém	k1gInSc1	systém
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gNnSc4	on
registrační	registrační	k2eAgInSc1d1	registrační
systém	systém	k1gInSc1	systém
<g/>
;	;	kIx,	;
regulace	regulace	k1gFnSc1	regulace
dovozu	dovoz	k1gInSc2	dovoz
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
importéři	importér	k1gMnPc1	importér
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
dovezený	dovezený	k2eAgInSc4d1	dovezený
film	film	k1gInSc4	film
nadále	nadále	k6eAd1	nadále
platili	platit	k5eAaImAgMnP	platit
registrační	registrační	k2eAgInSc4d1	registrační
poplatek	poplatek	k1gInSc4	poplatek
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
peníze	peníz	k1gInPc1	peníz
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Fond	fond	k1gInSc4	fond
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
domácí	domácí	k2eAgFnSc2d1	domácí
výroby	výroba	k1gFnSc2	výroba
dostávaly	dostávat	k5eAaImAgFnP	dostávat
formou	forma	k1gFnSc7	forma
dotací	dotace	k1gFnSc7	dotace
k	k	k7c3	k
československým	československý	k2eAgMnPc3d1	československý
tvůrcům	tvůrce	k1gMnPc3	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byly	být	k5eAaImAgInP	být
zprovozněny	zprovozněn	k2eAgInPc1d1	zprovozněn
ateliéry	ateliér	k1gInPc1	ateliér
A-B	A-B	k1gFnSc1	A-B
Barandov	Barandov	k1gInSc1	Barandov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
první	první	k4xOgFnSc3	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
emigračních	emigrační	k2eAgFnPc2d1	emigrační
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc4	Československo
opustil	opustit	k5eAaPmAgMnS	opustit
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Hugo	Hugo	k1gMnSc1	Hugo
Haas	Haas	k1gInSc4	Haas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
jako	jako	k9	jako
experimentální	experimentální	k2eAgMnSc1d1	experimentální
filmař	filmař	k1gMnSc1	filmař
a	a	k8xC	a
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
Alexander	Alexandra	k1gFnPc2	Alexandra
Hackenschmied	Hackenschmied	k1gInSc1	Hackenschmied
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
používal	používat	k5eAaImAgInS	používat
příjmení	příjmení	k1gNnSc4	příjmení
Hammid	Hammida	k1gFnPc2	Hammida
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
USA	USA	kA	USA
odešli	odejít	k5eAaPmAgMnP	odejít
též	též	k6eAd1	též
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgFnSc3d1	válečná
dokumentaristice	dokumentaristika	k1gFnSc3	dokumentaristika
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Jiří	Jiří	k1gMnSc1	Jiří
Weiss	Weiss	k1gMnSc1	Weiss
<g/>
.	.	kIx.	.
</s>
<s>
Ostravský	ostravský	k2eAgMnSc1d1	ostravský
rodák	rodák	k1gMnSc1	rodák
Karel	Karel	k1gMnSc1	Karel
Reisz	Reisz	k1gMnSc1	Reisz
se	se	k3xPyFc4	se
jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
Wintona	Winton	k1gMnSc2	Winton
a	a	k8xC	a
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
hnutí	hnutí	k1gNnPc2	hnutí
Free	Fre	k1gMnSc2	Fre
Cinema	Cinem	k1gMnSc2	Cinem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
tuhá	tuhý	k2eAgFnSc1d1	tuhá
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Okupanti	okupant	k1gMnPc1	okupant
zřídili	zřídit	k5eAaPmAgMnP	zřídit
dohlížecí	dohlížecí	k2eAgFnSc3d1	dohlížecí
instituci	instituce	k1gFnSc3	instituce
Českomoravské	českomoravský	k2eAgNnSc1d1	Českomoravské
filmové	filmový	k2eAgNnSc1d1	filmové
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
arizací	arizace	k1gFnSc7	arizace
a	a	k8xC	a
germanizací	germanizace	k1gFnSc7	germanizace
českého	český	k2eAgInSc2d1	český
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
a	a	k8xC	a
plakáty	plakát	k1gInPc1	plakát
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
česko-německé	českoěmecký	k2eAgInPc1d1	česko-německý
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
s	s	k7c7	s
židovskými	židovský	k2eAgMnPc7d1	židovský
herci	herec	k1gMnPc7	herec
nesměly	smět	k5eNaImAgFnP	smět
být	být	k5eAaImF	být
promítány	promítán	k2eAgFnPc1d1	promítána
a	a	k8xC	a
filmoví	filmový	k2eAgMnPc1d1	filmový
pracovníci	pracovník	k1gMnPc1	pracovník
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
přezkušováni	přezkušovat	k5eAaImNgMnP	přezkušovat
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
vyvlastnili	vyvlastnit	k5eAaPmAgMnP	vyvlastnit
barrandovské	barrandovský	k2eAgInPc4d1	barrandovský
ateliéry	ateliér	k1gInPc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
filmaři	filmař	k1gMnPc1	filmař
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
umělci	umělec	k1gMnPc1	umělec
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
pro	pro	k7c4	pro
protinacistickou	protinacistický	k2eAgFnSc4d1	protinacistická
činnost	činnost	k1gFnSc4	činnost
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejslavnější	slavný	k2eAgInPc4d3	nejslavnější
filmy	film	k1gInPc4	film
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
Císařův	Císařův	k2eAgMnSc1d1	Císařův
pekař	pekař	k1gMnSc1	pekař
-	-	kIx~	-
Pekařův	Pekařův	k2eAgMnSc1d1	Pekařův
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
Vynález	vynález	k1gInSc1	vynález
zkázy	zkáza	k1gFnSc2	zkáza
<g/>
,	,	kIx,	,
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
král	král	k1gMnSc1	král
<g/>
...	...	k?	...
Už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
české	český	k2eAgNnSc1d1	české
intelektuální	intelektuální	k2eAgInPc1d1	intelektuální
kruhy	kruh	k1gInPc1	kruh
diskutovaly	diskutovat	k5eAaImAgInP	diskutovat
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
zestátnění	zestátnění	k1gNnSc2	zestátnění
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
viděli	vidět	k5eAaImAgMnP	vidět
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
tvorby	tvorba	k1gFnSc2	tvorba
na	na	k7c6	na
komerčních	komerční	k2eAgInPc6d1	komerční
zájmech	zájem	k1gInPc6	zájem
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zestátnění	zestátnění	k1gNnSc3	zestátnění
došlo	dojít	k5eAaPmAgNnS	dojít
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
komunistickým	komunistický	k2eAgInSc7d1	komunistický
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc1	dekret
presidenta	president	k1gMnSc2	president
republiky	republika	k1gFnSc2	republika
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
o	o	k7c4	o
opatření	opatření	k1gNnSc4	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
filmu	film	k1gInSc2	film
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
K	k	k7c3	k
provozu	provoz	k1gInSc3	provoz
filmových	filmový	k2eAgInPc2d1	filmový
atelierů	atelier	k1gInPc2	atelier
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
osvětlených	osvětlený	k2eAgInPc2d1	osvětlený
filmů	film	k1gInPc2	film
kinematografických	kinematografický	k2eAgFnPc2d1	kinematografická
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
laboratornímu	laboratorní	k2eAgNnSc3d1	laboratorní
zpracování	zpracování	k1gNnSc3	zpracování
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
k	k	k7c3	k
půjčování	půjčování	k1gNnSc3	půjčování
filmů	film	k1gInPc2	film
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
promítání	promítání	k1gNnSc3	promítání
je	být	k5eAaImIp3nS	být
oprávněn	oprávněn	k2eAgMnSc1d1	oprávněn
výhradně	výhradně	k6eAd1	výhradně
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
K	k	k7c3	k
dovozu	dovoz	k1gInSc3	dovoz
a	a	k8xC	a
vývozu	vývoz	k1gInSc3	vývoz
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
jest	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
oprávněn	oprávnit	k5eAaPmNgInS	oprávnit
výhradně	výhradně	k6eAd1	výhradně
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c4	po
následující	následující	k2eAgNnPc4d1	následující
čtyři	čtyři	k4xCgNnPc4	čtyři
desetiletí	desetiletí	k1gNnPc4	desetiletí
tak	tak	k8xC	tak
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc4	dovoz
i	i	k8xC	i
distribuci	distribuce	k1gFnSc4	distribuce
filmů	film	k1gInPc2	film
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
plně	plně	k6eAd1	plně
kontroloval	kontrolovat	k5eAaImAgInS	kontrolovat
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
první	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jiráskovské	jiráskovský	k2eAgNnSc4d1	jiráskovské
drama	drama	k1gNnSc4	drama
Jan	Jan	k1gMnSc1	Jan
Roháč	roháč	k1gMnSc1	roháč
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Borský	Borský	k1gMnSc1	Borský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
založena	založen	k2eAgFnSc1d1	založena
Filmová	filmový	k2eAgFnSc1d1	filmová
fakulta	fakulta	k1gFnSc1	fakulta
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
FAMU	FAMU	kA	FAMU
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Čáp	Čáp	k1gMnSc1	Čáp
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
Ladislav	Ladislav	k1gMnSc1	Ladislav
Brom	brom	k1gInSc1	brom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
a	a	k8xC	a
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
především	především	k9	především
nástrojem	nástroj	k1gInSc7	nástroj
komunistické	komunistický	k2eAgFnSc2d1	komunistická
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Náznak	náznak	k1gInSc1	náznak
změny	změna	k1gFnSc2	změna
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
lehce	lehko	k6eAd1	lehko
společensky	společensky	k6eAd1	společensky
kritické	kritický	k2eAgInPc4d1	kritický
filmy	film	k1gInPc4	film
Tři	tři	k4xCgNnPc1	tři
přání	přání	k1gNnPc1	přání
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ján	Ján	k1gMnSc1	Ján
Kadár	Kadár	k1gMnSc1	Kadár
a	a	k8xC	a
Elmar	Elmar	k1gMnSc1	Elmar
Klos	Klosa	k1gFnPc2	Klosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
lvi	lev	k1gMnPc1	lev
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Krška	Krška	k1gMnSc1	Krška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
otců	otec	k1gMnPc2	otec
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Helge	Helg	k1gFnSc2	Helg
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zářijové	zářijový	k2eAgFnSc6d1	zářijová
noci	noc	k1gFnSc6	noc
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
zkritizovány	zkritizovat	k5eAaPmNgInP	zkritizovat
na	na	k7c6	na
I.	I.	kA	I.
festivalu	festival	k1gInSc2	festival
československého	československý	k2eAgInSc2d1	československý
filmu	film	k1gInSc2	film
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
a	a	k8xC	a
následně	následně	k6eAd1	následně
staženy	stáhnout	k5eAaPmNgInP	stáhnout
z	z	k7c2	z
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
odešla	odejít	k5eAaPmAgFnS	odejít
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
okupaci	okupace	k1gFnSc4	okupace
Československa	Československo	k1gNnSc2	Československo
vojsky	vojsky	k6eAd1	vojsky
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgMnPc2d1	významný
režisérů	režisér	k1gMnPc2	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Vlast	vlast	k1gFnSc4	vlast
tak	tak	k6eAd1	tak
opustili	opustit	k5eAaPmAgMnP	opustit
Ivan	Ivan	k1gMnSc1	Ivan
Passer	Passer	k1gMnSc1	Passer
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Boková	bokový	k2eAgFnSc1d1	Boková
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Reischl	Reischl	k1gMnSc1	Reischl
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Votoček	Votočka	k1gFnPc2	Votočka
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Šafařík	Šafařík	k1gMnSc1	Šafařík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Němec	Němec	k1gMnSc1	Němec
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Odmlčet	odmlčet	k5eAaPmF	odmlčet
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
Pavel	Pavel	k1gMnSc1	Pavel
Juráček	Juráček	k1gMnSc1	Juráček
<g/>
,	,	kIx,	,
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
i	i	k8xC	i
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Chytilová	Chytilová	k1gFnSc1	Chytilová
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
Paramount	Paramounta	k1gFnPc2	Paramounta
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
institucí	instituce	k1gFnSc7	instituce
Filmexport	filmexport	k1gInSc1	filmexport
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1970	[number]	k4	1970
a	a	k8xC	a
1989	[number]	k4	1989
tento	tento	k3xDgInSc4	tento
exodus	exodus	k1gInSc1	exodus
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
cenzurou	cenzura	k1gFnSc7	cenzura
způsobily	způsobit	k5eAaPmAgFnP	způsobit
úbytek	úbytek	k1gInSc4	úbytek
filmů	film	k1gInPc2	film
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
autorskou	autorský	k2eAgFnSc4d1	autorská
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
kinematografie	kinematografie	k1gFnSc1	kinematografie
přestala	přestat	k5eAaPmAgFnS	přestat
získávat	získávat	k5eAaImF	získávat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
ohlasy	ohlas	k1gInPc4	ohlas
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
;	;	kIx,	;
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
pouze	pouze	k6eAd1	pouze
jedné	jeden	k4xCgFnSc2	jeden
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
neanglicky	anglicky	k6eNd1	anglicky
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
Menzelův	Menzelův	k2eAgInSc4d1	Menzelův
film	film	k1gInSc4	film
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dominujícími	dominující	k2eAgInPc7d1	dominující
žánry	žánr	k1gInPc7	žánr
byly	být	k5eAaImAgFnP	být
lidové	lidový	k2eAgFnPc4d1	lidová
komedie	komedie	k1gFnPc4	komedie
a	a	k8xC	a
detektivky	detektivka	k1gFnPc4	detektivka
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
Tři	tři	k4xCgMnPc4	tři
oříšky	oříšek	k1gMnPc4	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
každoročně	každoročně	k6eAd1	každoročně
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
v	v	k7c6	v
několika	několik	k4yIc6	několik
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
privatizaci	privatizace	k1gFnSc3	privatizace
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
mimo	mimo	k6eAd1	mimo
státní	státní	k2eAgInSc1d1	státní
monopol	monopol	k1gInSc1	monopol
byl	být	k5eAaImAgInS	být
Tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vít	Vít	k1gMnSc1	Vít
Olmer	Olmer	k1gMnSc1	Olmer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
produkovala	produkovat	k5eAaImAgFnS	produkovat
společnost	společnost	k1gFnSc1	společnost
Bontonfilm	Bontonfilma	k1gFnPc2	Bontonfilma
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
plně	plně	k6eAd1	plně
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
státní	státní	k2eAgFnSc1d1	státní
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
,	,	kIx,	,
filmaři	filmař	k1gMnPc1	filmař
se	se	k3xPyFc4	se
zato	zato	k6eAd1	zato
nově	nově	k6eAd1	nově
museli	muset	k5eAaImAgMnP	muset
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
s	s	k7c7	s
tržním	tržní	k2eAgNnSc7d1	tržní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
product	product	k2eAgInSc1d1	product
placement	placement	k1gInSc1	placement
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
natočené	natočený	k2eAgInPc1d1	natočený
videokamerou	videokamera	k1gFnSc7	videokamera
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k1gMnPc4	jiný
Láska	láska	k1gFnSc1	láska
shora	shora	k6eAd1	shora
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Marek	Marek	k1gMnSc1	Marek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
KLIMEŠ	Klimeš	k1gMnSc1	Klimeš
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
:	:	kIx,	:
Kinematografie	kinematografie	k1gFnSc1	kinematografie
a	a	k8xC	a
stát	stát	k1gInSc1	stát
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7308-641-1	[number]	k4	978-80-7308-641-1
kinematografie	kinematografie	k1gFnSc2	kinematografie
dějiny	dějiny	k1gFnPc4	dějiny
filmu	film	k1gInSc6	film
Filmový	filmový	k2eAgInSc4d1	filmový
přehled	přehled	k1gInSc4	přehled
Webový	webový	k2eAgInSc4d1	webový
portál	portál	k1gInSc4	portál
Národního	národní	k2eAgInSc2d1	národní
filmového	filmový	k2eAgInSc2d1	filmový
archivu	archiv	k1gInSc2	archiv
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
kinematografii	kinematografie	k1gFnSc6	kinematografie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Česká	český	k2eAgFnSc1d1	Česká
kinematografie	kinematografie	k1gFnSc1	kinematografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
