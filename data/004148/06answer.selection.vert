<s>
Prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
kameramanem	kameraman	k1gMnSc7	kameraman
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
natáčel	natáčet	k5eAaImAgMnS	natáčet
krátké	krátký	k2eAgInPc4d1	krátký
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
filmové	filmový	k2eAgFnSc2d1	filmová
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
.	.	kIx.	.
</s>
