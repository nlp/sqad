<s>
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
</s>
<s>
PhDr.	PhDr.	kA
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1940	#num#	k4
<g/>
Nová	nový	k2eAgFnSc1d1
ŘíšeProtektorát	ŘíšeProtektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1940	#num#	k4
Nová	nový	k2eAgFnSc1d1
Říše	říše	k1gFnSc1
okres	okres	k1gInSc1
Jihlava	Jihlava	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
a	a	k8xC
organizátor	organizátor	k1gMnSc1
a	a	k8xC
univerzitní	univerzitní	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
a	a	k8xC
činnost	činnost	k1gFnSc1
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Českého	český	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
hudby	hudba	k1gFnSc2
(	(	kIx(
<g/>
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
ředitelem	ředitel	k1gMnSc7
byl	být	k5eAaImAgMnS
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1990	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc4
získal	získat	k5eAaPmAgInS
nejprve	nejprve	k6eAd1
na	na	k7c6
Státní	státní	k2eAgFnSc6d1
konzervatoři	konzervatoř	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
studoval	studovat	k5eAaImAgInS
obor	obor	k1gInSc1
kontrabas	kontrabas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
a	a	k8xC
teoretická	teoretický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1974	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
Kabinetu	kabinet	k1gInSc6
hudební	hudební	k2eAgFnSc2d1
lexikografie	lexikografie	k1gFnSc2
Filosofické	filosofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
také	také	k9
vedoucím	vedoucí	k1gMnSc7
sekretariátu	sekretariát	k1gInSc2
České	český	k2eAgFnSc2d1
hudební	hudební	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1990	#num#	k4
<g/>
–	–	kIx~
<g/>
1996	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
ředitel	ředitel	k1gMnSc1
Českého	český	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
hudby	hudba	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
působení	působení	k1gNnSc1
bylo	být	k5eAaImAgNnS
významné	významný	k2eAgNnSc1d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
organizace	organizace	k1gFnSc2
hudební	hudební	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
muzikologického	muzikologický	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
muzeu	muzeum	k1gNnSc6
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
např.	např.	kA
o	o	k7c4
zahájení	zahájení	k1gNnSc4
digitalizace	digitalizace	k1gFnPc4
materiálů	materiál	k1gInPc2
a	a	k8xC
historických	historický	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
řešení	řešení	k1gNnSc3
dislokační	dislokační	k2eAgFnSc2d1
problematiky	problematika	k1gFnSc2
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současně	současně	k6eAd1
působil	působit	k5eAaImAgMnS
na	na	k7c4
poloviční	poloviční	k2eAgInSc4d1
úvazek	úvazek	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
jako	jako	k8xS,k8xC
vědecký	vědecký	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
v	v	k7c4
Ústav	ústav	k1gInSc4
teorie	teorie	k1gFnSc2
a	a	k8xC
dějin	dějiny	k1gFnPc2
umění	umění	k1gNnSc2
ČSAV	ČSAV	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
redakční	redakční	k2eAgFnSc2d1
rady	rada	k1gFnSc2
časopisu	časopis	k1gInSc2
Hudební	hudební	k2eAgInPc4d1
rozhledy	rozhled	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
výboru	výbor	k1gInSc2
Mezinárodního	mezinárodní	k2eAgNnSc2d1
hudebněvědného	hudebněvědný	k2eAgNnSc2d1
kolokvia	kolokvium	k1gNnSc2
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Významně	významně	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c4
organizaci	organizace	k1gFnSc4
různých	různý	k2eAgFnPc2d1
mezinárodních	mezinárodní	k2eAgFnPc2d1
hudebních	hudební	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k6eAd1
autorem	autor	k1gMnSc7
mnoha	mnoho	k4c2
hudebněvědných	hudebněvědný	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
v	v	k7c6
českém	český	k2eAgInSc6d1
a	a	k8xC
německém	německý	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
oboru	obor	k1gInSc6
staré	starý	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
a	a	k8xC
notace	notace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
rovněž	rovněž	k9
působí	působit	k5eAaImIp3nS
jako	jako	k9
zástupce	zástupce	k1gMnSc1
vedoucího	vedoucí	k1gMnSc2
katedry	katedra	k1gFnSc2
muzikologie	muzikologie	k1gFnSc2
Filosofické	filosofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
University	universita	k1gFnSc2
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
do	do	k7c2
současné	současný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
pracuje	pracovat	k5eAaImIp3nS
jako	jako	k9
odborný	odborný	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1998	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
jako	jako	k8xC,k8xS
vedoucí	vedoucí	k1gMnSc1
Ústavu	ústav	k1gInSc2
hudební	hudební	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
na	na	k7c6
Filosofické	filosofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
university	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
také	také	k9
vyučuje	vyučovat	k5eAaImIp3nS
teorii	teorie	k1gFnSc4
hudebního	hudební	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
<g/>
,	,	kIx,
stará	starý	k2eAgFnSc1d1
notace	notace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
CV	CV	kA
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
MUNI	MUNI	k?
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Tesař	Tesař	k1gMnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1132275	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5863	#num#	k4
0700	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2012075006	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83936206	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2012075006	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
