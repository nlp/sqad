<s>
Podunajské	podunajský	k2eAgFnPc1d1
Biskupice	Biskupice	k1gFnPc1
</s>
<s>
Podunajské	podunajský	k2eAgFnPc1d1
Biskupice	Biskupice	k1gFnPc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
133	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Bratislavský	bratislavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
II	II	kA
</s>
<s>
Podunajské	podunajský	k2eAgFnPc1d1
Biskupice	Biskupice	k1gFnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
42,5	42,5	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
22	#num#	k4
029	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
518,5	518,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.biskupice.sk	www.biskupice.sk	k1gInSc1
PSČ	PSČ	kA
</s>
<s>
821	#num#	k4
06	#num#	k4
a	a	k8xC
821	#num#	k4
07	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BA	ba	k9
<g/>
,	,	kIx,
BL	BL	kA
a	a	k8xC
BT	BT	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
Bratislavy	Bratislava	k1gFnSc2
</s>
<s>
Podunajské	podunajský	k2eAgFnPc1d1
Biskupice	Biskupice	k1gFnPc1
jsou	být	k5eAaImIp3nP
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Bratislavy	Bratislava	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Bratislava	Bratislava	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	s	k7c7
8	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
42,5	42,5	k4
km²	km²	k?
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
dělá	dělat	k5eAaImIp3nS
největší	veliký	k2eAgFnSc4d3
městskou	městský	k2eAgFnSc4d1
části	část	k1gFnPc4
Bratislavy	Bratislava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
byly	být	k5eAaImAgFnP
samostatnou	samostatný	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
klášter	klášter	k1gInSc1
slovenské	slovenský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Kongregace	kongregace	k1gFnSc2
Milosrdných	milosrdný	k2eAgFnPc2d1
sester	sestra	k1gFnPc2
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klášterním	klášterní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
sv.	sv.	kA
Kříže	Kříž	k1gMnSc2
je	být	k5eAaImIp3nS
pohřbena	pohřben	k2eAgFnSc1d1
blahoslavená	blahoslavený	k2eAgFnSc1d1
Zdenka	Zdenka	k1gFnSc1
Cecília	Cecílium	k1gNnSc2
Schelingová	Schelingový	k2eAgFnSc1d1
<g/>
,	,	kIx,
členka	členka	k1gFnSc1
kongregace	kongregace	k1gFnSc1
a	a	k8xC
mučednice	mučednice	k1gFnSc1
z	z	k7c2
dob	doba	k1gFnPc2
komunistického	komunistický	k2eAgInSc2d1
teroru	teror	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
centru	centrum	k1gNnSc6
obce	obec	k1gFnSc2
je	být	k5eAaImIp3nS
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
morový	morový	k2eAgInSc1d1
sloup	sloup	k1gInSc1
a	a	k8xC
menší	malý	k2eAgNnSc1d2
šlechtické	šlechtický	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejrozsáhlejších	rozsáhlý	k2eAgNnPc2d3
archeologických	archeologický	k2eAgNnPc2d1
nalezišť	naleziště	k1gNnPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
stavbě	stavba	k1gFnSc6
silničního	silniční	k2eAgInSc2d1
obchvatu	obchvat	k1gInSc2
kolem	kolem	k7c2
Bratislavy	Bratislava	k1gFnSc2
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
460	#num#	k4
hrobů	hrob	k1gInPc2
a	a	k8xC
řada	řada	k1gFnSc1
artefaktů	artefakt	k1gInPc2
z	z	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
tzv.	tzv.	kA
Avarský	avarský	k2eAgInSc4d1
kaganát	kaganát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
Viliam	Viliam	k1gMnSc1
Nagy	Naga	k1gFnPc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
Oto	Ota	k1gMnSc5
Nevický	Nevický	k2eAgMnSc5d1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Alžbeta	Alžbet	k2eAgFnSc1d1
Ožvaldová	Ožvaldová	k1gFnSc1
</s>
<s>
od	od	k7c2
2018	#num#	k4
Zoltán	Zoltán	k1gMnSc1
Pék	Pék	k1gMnSc1
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Slováci	Slovák	k1gMnPc1
–	–	k?
17	#num#	k4
082	#num#	k4
(	(	kIx(
<g/>
81,01	81,01	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Maďaři	Maďar	k1gMnPc1
–	–	k?
3	#num#	k4
295	#num#	k4
(	(	kIx(
<g/>
15,63	15,63	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Češi	Čech	k1gMnPc1
–	–	k?
366	#num#	k4
(	(	kIx(
<g/>
1,74	1,74	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnSc1d1
/	/	kIx~
nezjištěna	zjištěn	k2eNgFnSc1d1
–	–	k?
344	#num#	k4
(	(	kIx(
<g/>
1,63	1,63	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
–	–	k?
21	#num#	k4
087	#num#	k4
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Slováci	Slovák	k1gMnPc1
–	–	k?
16	#num#	k4
212	#num#	k4
(	(	kIx(
<g/>
82,09	82,09	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Maďaři	Maďar	k1gMnPc1
–	–	k?
2	#num#	k4
760	#num#	k4
(	(	kIx(
<g/>
13,98	13,98	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Češi	Čech	k1gMnPc1
–	–	k?
285	#num#	k4
(	(	kIx(
<g/>
1,44	1,44	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgMnSc1d1
–	–	k?
235	#num#	k4
(	(	kIx(
<g/>
1,19	1,19	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Nezjištěna	zjištěn	k2eNgFnSc1d1
–	–	k?
257	#num#	k4
(	(	kIx(
<g/>
1,3	1,3	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
–	–	k?
19	#num#	k4
749	#num#	k4
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Slováci	Slovák	k1gMnPc1
–	–	k?
17	#num#	k4
351	#num#	k4
(	(	kIx(
<g/>
84,18	84,18	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Maďaři	Maďar	k1gMnPc1
–	–	k?
2	#num#	k4
231	#num#	k4
(	(	kIx(
<g/>
10,82	10,82	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgMnSc1d1
–	–	k?
502	#num#	k4
(	(	kIx(
<g/>
2,44	2,44	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Nezjištěna	zjištěn	k2eNgFnSc1d1
–	–	k?
527	#num#	k4
(	(	kIx(
<g/>
2,56	2,56	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
–	–	k?
20	#num#	k4
611	#num#	k4
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Partnerské	partnerský	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Orth	Orth	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Donau	donau	k1gInSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Unikátní	unikátní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
i	i	k8xC
avarské	avarský	k2eAgInPc1d1
šperky	šperk	k1gInPc1
<g/>
,	,	kIx,
Slováci	Slovák	k1gMnPc1
hlásí	hlásit	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
archeologický	archeologický	k2eAgInSc4d1
objev	objev	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-28	2017-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Podunajské	podunajský	k2eAgFnSc2d1
Biskupice	Biskupice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Biskupice	Biskupice	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bratislava	Bratislava	k1gFnSc1
–	–	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Členění	členění	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Okresy	okres	k1gInPc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
I	i	k8xC
•	•	k?
Bratislava	Bratislava	k1gFnSc1
II	II	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
III	III	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
IV	IV	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
V	v	k7c6
Městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
</s>
<s>
Čunovo	Čunovo	k1gNnSc1
•	•	k?
Devín	Devín	k1gMnSc1
•	•	k?
Devínska	Devínsko	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Dúbravka	Dúbravka	k1gFnSc1
•	•	k?
Jarovce	Jarovec	k1gInSc2
•	•	k?
Karlova	Karlův	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Lamač	lamač	k1gMnSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
•	•	k?
Petržalka	Petržalka	k1gFnSc1
•	•	k?
Podunajské	podunajský	k2eAgFnSc3d1
Biskupice	Biskupika	k1gFnSc3
•	•	k?
Rača	Rač	k1gInSc2
•	•	k?
Rusovce	Rusovec	k1gMnSc2
•	•	k?
Ružinov	Ružinov	k1gInSc1
•	•	k?
Staré	Staré	k2eAgNnSc4d1
Mesto	Mesto	k1gNnSc4
•	•	k?
Vajnory	Vajnora	k1gFnSc2
•	•	k?
Vrakuňa	Vrakuň	k1gInSc2
•	•	k?
Záhorská	záhorský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Významné	významný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Historické	historický	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
Bratislavský	bratislavský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
•	•	k?
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
•	•	k?
Stará	starat	k5eAaImIp3nS
radnice	radnice	k1gFnSc1
•	•	k?
Michalská	Michalský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Primaciální	primaciální	k2eAgInSc1d1
palác	palác	k1gInSc1
Mosty	most	k1gInPc1
</s>
<s>
Most	most	k1gInSc1
Apollo	Apollo	k1gMnSc1
•	•	k?
Most	most	k1gInSc1
Lafranconi	Lafrancoň	k1gFnSc3
•	•	k?
Most	most	k1gInSc1
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
•	•	k?
Prístavný	Prístavný	k2eAgInSc1d1
most	most	k1gInSc1
•	•	k?
Starý	starý	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
