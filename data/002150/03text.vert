<s>
Jackson	Jackson	k1gMnSc1	Jackson
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
prezidentovi	prezident	k1gMnSc6	prezident
Andrewu	Andrew	k1gMnSc6	Andrew
Jacksonovi	Jackson	k1gMnSc6	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
situované	situovaný	k2eAgNnSc1d1	situované
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Pearl	Pearla	k1gFnPc2	Pearla
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
276,7	[number]	k4	276,7
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
271,7	[number]	k4	271,7
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
5	[number]	k4	5
km2	km2	k4	km2
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgFnSc2d1	teplá
a	a	k8xC	a
vlhké	vlhký	k2eAgFnSc2d1	vlhká
a	a	k8xC	a
zimy	zima	k1gFnSc2	zima
jsou	být	k5eAaImIp3nP	být
mírné	mírný	k2eAgFnPc1d1	mírná
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
francouzsko-kanadským	francouzskoanadský	k2eAgInSc7d1	francouzsko-kanadský
traperem	traper	k1gMnSc7	traper
jménem	jméno	k1gNnSc7	jméno
Louis	Louis	k1gMnSc1	Louis
LeFleur	LeFleur	k1gMnSc1	LeFleur
a	a	k8xC	a
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
LeFleur	LeFleura	k1gFnPc2	LeFleura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bluff	Bluff	k1gInSc1	Bluff
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Mississippi	Mississippi	k1gFnSc2	Mississippi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
a	a	k8xC	a
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Jackson	Jackson	k1gInSc4	Jackson
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
vítězství	vítězství	k1gNnSc2	vítězství
Andrewa	Andrewus	k1gMnSc2	Andrewus
Jacksona	Jackson	k1gMnSc2	Jackson
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc1	Orleans
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
během	během	k7c2	během
britsko-americké	britskomerický	k2eAgFnSc2d1	britsko-americká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
města	město	k1gNnSc2	město
zavedena	zaveden	k2eAgFnSc1d1	zavedena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
se	se	k3xPyFc4	se
během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
odehrála	odehrát	k5eAaPmAgFnS	odehrát
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Jacksonu	Jackson	k1gInSc2	Jackson
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
jednotkami	jednotka	k1gFnPc7	jednotka
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
zvolen	zvolit	k5eAaPmNgMnS	zvolit
černošský	černošský	k2eAgMnSc1d1	černošský
nacionalista	nacionalista	k1gMnSc1	nacionalista
Chokwe	Chokwe	k1gNnSc2	Chokwe
Lumumba	Lumumba	k1gMnSc1	Lumumba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
černošského	černošský	k2eAgInSc2d1	černošský
státu	stát	k1gInSc2	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
USA	USA	kA	USA
zvaného	zvaný	k2eAgInSc2d1	zvaný
"	"	kIx"	"
<g/>
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
New	New	k1gFnPc2	New
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
reparace	reparace	k1gFnSc1	reparace
pro	pro	k7c4	pro
černochy	černoch	k1gMnPc4	černoch
za	za	k7c4	za
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Lumumba	Lumumba	k1gMnSc1	Lumumba
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
osmi	osm	k4xCc6	osm
měsících	měsíc	k1gInPc6	měsíc
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
protesty	protest	k1gInPc4	protest
místních	místní	k2eAgMnPc2d1	místní
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
konspirační	konspirační	k2eAgFnSc2d1	konspirační
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
bělošském	bělošský	k2eAgNnSc6d1	bělošské
rasistickém	rasistický	k2eAgNnSc6d1	rasistické
spiknutí	spiknutí	k1gNnSc6	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Jackson	Jackson	k1gMnSc1	Jackson
malé	malý	k2eAgNnSc4d1	malé
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
před	před	k7c7	před
americkou	americký	k2eAgFnSc7d1	americká
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
statusu	status	k1gInSc3	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
sídlilo	sídlit	k5eAaImAgNnS	sídlit
v	v	k7c6	v
Jacksonu	Jackson	k1gInSc6	Jackson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
881	[number]	k4	881
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
8	[number]	k4	8
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
populace	populace	k1gFnSc1	populace
na	na	k7c4	na
70	[number]	k4	70
000	[number]	k4	000
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Jackson	Jackson	k1gNnSc4	Jackson
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
tam	tam	k6eAd1	tam
sídlilo	sídlit	k5eAaImAgNnS	sídlit
200	[number]	k4	200
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInPc1	jeho
počty	počet	k1gInPc1	počet
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
bílému	bílý	k2eAgInSc3d1	bílý
útěku	útěk	k1gInSc3	útěk
a	a	k8xC	a
suburbanizaci	suburbanizace	k1gFnSc3	suburbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Jacksonu	Jackson	k1gInSc6	Jackson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sídlilo	sídlit	k5eAaImAgNnS	sídlit
184	[number]	k4	184
256	[number]	k4	256
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
67	[number]	k4	67
841	[number]	k4	841
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
44	[number]	k4	44
488	[number]	k4	488
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
678,2	[number]	k4	678,2
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
na	na	k7c4	na
173	[number]	k4	173
514	[number]	k4	514
<g/>
.	.	kIx.	.
18,4	[number]	k4	18,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
79,4	[number]	k4	79,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
1,6	[number]	k4	1,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
28,5	[number]	k4	28,5
%	%	kIx~	%
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
12,4	[number]	k4	12,4
%	%	kIx~	%
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
29,1	[number]	k4	29,1
%	%	kIx~	%
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
19,1	[number]	k4	19,1
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
10,9	[number]	k4	10,9
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
31	[number]	k4	31
let	léto	k1gNnPc2	léto
Cassandra	Cassandra	k1gFnSc1	Cassandra
Wilsonová	Wilsonová	k1gFnSc1	Wilsonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzová	jazzový	k2eAgFnSc1d1	jazzová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Faith	Faith	k1gMnSc1	Faith
Hill	Hill	k1gMnSc1	Hill
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
country	country	k2eAgFnSc1d1	country
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Brian	Brian	k1gMnSc1	Brian
Marshall	Marshall	k1gMnSc1	Marshall
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
kapel	kapela	k1gFnPc2	kapela
Creed	Creed	k1gMnSc1	Creed
a	a	k8xC	a
Alter	Alter	k1gMnSc1	Alter
Bridge	Bridge	k1gFnSc4	Bridge
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jackson	Jackson	k1gNnSc1	Jackson
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnSc1	Mississippi
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jackson	Jacksona	k1gFnPc2	Jacksona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
