<s>
Zelking	Zelking	k1gInSc1	Zelking
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Zelking-Matzleinsdorf	Zelking-Matzleinsdorf	k1gInSc4	Zelking-Matzleinsdorf
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Zříceninu	zřícenina	k1gFnSc4	zřícenina
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
zdejšího	zdejší	k2eAgInSc2d1	zdejší
kopce	kopec	k1gInSc2	kopec
obýval	obývat	k5eAaImAgInS	obývat
rakouský	rakouský	k2eAgInSc1d1	rakouský
rod	rod	k1gInSc1	rod
Zelkingerů	Zelkinger	k1gMnPc2	Zelkinger
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
uvedený	uvedený	k2eAgMnSc1d1	uvedený
Werner	Werner	k1gMnSc1	Werner
von	von	k1gInSc4	von
Zelking	Zelking	k1gInSc1	Zelking
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
hrabat	hrabě	k1gNnPc2	hrabě
Sinzendorfů	Sinzendorf	k1gInPc2	Sinzendorf
<g/>
.	.	kIx.	.
</s>
<s>
Pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
majiteli	majitel	k1gMnPc7	majitel
byli	být	k5eAaImAgMnP	být
hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Rohrau	Rohraus	k1gInSc2	Rohraus
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
Galgozy-Galantha	Galgozy-Galantha	k1gFnSc1	Galgozy-Galantha
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
hrad	hrad	k1gInSc1	hrad
začal	začít	k5eAaPmAgInS	začít
pustnout	pustnout	k5eAaImF	pustnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
pocházela	pocházet	k5eAaImAgFnS	pocházet
Petrissa	Petrissa	k1gFnSc1	Petrissa
von	von	k1gInSc4	von
Zelking	Zelking	k1gInSc1	Zelking
(	(	kIx(	(
<g/>
†	†	k?	†
1318	[number]	k4	1318
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
Heinrich	Heinrich	k1gMnSc1	Heinrich
II	II	kA	II
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Liechtenstein	Liechtenstein	k1gMnSc1	Liechtenstein
(	(	kIx(	(
<g/>
†	†	k?	†
1314	[number]	k4	1314
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
jen	jen	k9	jen
ukázkou	ukázka	k1gFnSc7	ukázka
kamenické	kamenický	k2eAgFnSc2d1	kamenická
práce	práce	k1gFnSc2	práce
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Burgruine	Burgruin	k1gInSc5	Burgruin
Zelking	Zelking	k1gInSc1	Zelking
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
