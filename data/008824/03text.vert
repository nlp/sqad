<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Letocha	Letoch	k1gMnSc2	Letoch
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Rady	rada	k1gFnSc2	rada
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Lerchově	Lerchův	k2eAgFnSc6d1	Lerchova
ulici	ulice	k1gFnSc6	ulice
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
fakultu	fakulta	k1gFnSc4	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
projektant	projektant	k1gMnSc1	projektant
v	v	k7c6	v
Železničním	železniční	k2eAgInSc6d1	železniční
stavitelství	stavitelství	k1gNnSc2	stavitelství
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
investiční	investiční	k2eAgMnSc1d1	investiční
technik	technik	k1gMnSc1	technik
v	v	k7c6	v
Železárnách	železárna	k1gFnPc6	železárna
Veselí	veselit	k5eAaImIp3nP	veselit
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1990	[number]	k4	1990
do	do	k7c2	do
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
města	město	k1gNnSc2	město
Kyjova	Kyjov	k1gInSc2	Kyjov
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Letocha	Letoch	k1gMnSc2	Letoch
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgInSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dospělé	dospělý	k2eAgFnPc4d1	dospělá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jeho	jeho	k3xOp3gFnPc4	jeho
kompetence	kompetence	k1gFnPc4	kompetence
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
krajského	krajský	k2eAgNnSc2d1	krajské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c6	na
kandidátní	kandidátní	k2eAgFnSc6d1	kandidátní
listině	listina	k1gFnSc6	listina
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
už	už	k6eAd1	už
nekandidoval	kandidovat	k5eNaImAgMnS	kandidovat
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
ředitele	ředitel	k1gMnSc2	ředitel
příspěvkové	příspěvkový	k2eAgFnSc2d1	příspěvková
organizace	organizace	k1gFnSc2	organizace
kraje	kraj	k1gInSc2	kraj
Centra	centrum	k1gNnSc2	centrum
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
Kyjov	Kyjov	k1gInSc4	Kyjov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
působí	působit	k5eAaImIp3nS	působit
do	do	k7c2	do
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
přístavbě	přístavba	k1gFnSc6	přístavba
objektu	objekt	k1gInSc2	objekt
s	s	k7c7	s
šedesáti	šedesát	k4xCc7	šedesát
novými	nový	k2eAgNnPc7d1	nové
lůžky	lůžko	k1gNnPc7	lůžko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
