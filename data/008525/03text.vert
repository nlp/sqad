<p>
<s>
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
orgán	orgán	k1gInSc4	orgán
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
všechny	všechen	k3xTgInPc1	všechen
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
stojí	stát	k5eAaImIp3nS	stát
předseda	předseda	k1gMnSc1	předseda
<g/>
;	;	kIx,	;
během	během	k7c2	během
72	[number]	k4	72
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
(	(	kIx(	(
<g/>
od	od	k7c2	od
září	září	k1gNnSc2	září
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
jím	jíst	k5eAaImIp1nS	jíst
je	on	k3xPp3gNnSc4	on
slovenský	slovenský	k2eAgMnSc1d1	slovenský
diplomat	diplomat	k1gMnSc1	diplomat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Lajčák	Lajčák	k1gMnSc1	Lajčák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
projednává	projednávat	k5eAaImIp3nS	projednávat
všechny	všechen	k3xTgFnPc4	všechen
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
Chartou	charta	k1gFnSc7	charta
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
správy	správa	k1gFnSc2	správa
dalších	další	k2eAgInPc2d1	další
hlavních	hlavní	k2eAgInPc2d1	hlavní
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
volí	volit	k5eAaImIp3nP	volit
jejich	jejich	k3xOp3gMnPc4	jejich
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1946	[number]	k4	1946
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
i	i	k9	i
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šest	šest	k4xCc4	šest
hlavních	hlavní	k2eAgInPc2d1	hlavní
výborů	výbor	k1gInPc2	výbor
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
pomocné	pomocný	k2eAgInPc1d1	pomocný
orgány	orgán	k1gInPc1	orgán
si	se	k3xPyFc3	se
Valné	valný	k2eAgNnSc4d1	Valné
shromáždění	shromáždění	k1gNnSc4	shromáždění
tvoří	tvořit	k5eAaImIp3nP	tvořit
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
komise	komise	k1gFnPc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Hlavních	hlavní	k2eAgInPc2d1	hlavní
výborů	výbor	k1gInPc2	výbor
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc1	šest
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
první	první	k4xOgInSc1	první
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
DISEC	DISEC	kA	DISEC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
druhý	druhý	k4xOgInSc1	druhý
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
a	a	k8xC	a
finanční	finanční	k2eAgInSc1d1	finanční
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
ECOFIN	ECOFIN	kA	ECOFIN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
třetí	třetí	k4xOgInSc1	třetí
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Sociální	sociální	k2eAgInSc1d1	sociální
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc1d1	kulturní
a	a	k8xC	a
humanitární	humanitární	k2eAgInSc1d1	humanitární
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
SOCHUM	SOCHUM	k?	SOCHUM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
dekolonizační	dekolonizační	k2eAgInSc1d1	dekolonizační
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
SPECPOL	SPECPOL	kA	SPECPOL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
pátý	pátý	k4xOgInSc1	pátý
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Administrativní	administrativní	k2eAgInSc1d1	administrativní
a	a	k8xC	a
rozpočtový	rozpočtový	k2eAgInSc1d1	rozpočtový
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
šestý	šestý	k4xOgInSc1	šestý
výbor	výbor	k1gInSc1	výbor
–	–	k?	–
Právní	právní	k2eAgInSc1d1	právní
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
<g/>
Předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
místopředsedové	místopředseda	k1gMnPc1	místopředseda
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
předsedové	předseda	k1gMnPc1	předseda
těchto	tento	k3xDgInPc2	tento
výborů	výbor	k1gInPc2	výbor
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
výbor	výbor	k1gInSc4	výbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasování	hlasování	k1gNnPc4	hlasování
ve	v	k7c6	v
Valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
==	==	k?	==
</s>
</p>
<p>
<s>
při	při	k7c6	při
běžných	běžný	k2eAgFnPc6d1	běžná
otázkách	otázka	k1gFnPc6	otázka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
hlasujících	hlasující	k1gMnPc2	hlasující
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
při	při	k7c6	při
podstatných	podstatný	k2eAgFnPc6d1	podstatná
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přijímání	přijímání	k1gNnSc1	přijímání
nových	nový	k2eAgInPc2d1	nový
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1	dvoutřetinová
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
přítomných	přítomný	k1gMnPc2	přítomný
a	a	k8xC	a
hlasujících	hlasující	k1gMnPc2	hlasující
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
soudců	soudce	k1gMnPc2	soudce
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
při	při	k7c6	při
revizi	revize	k1gFnSc6	revize
Charty	charta	k1gFnSc2	charta
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1	dvoutřetinová
většina	většina	k1gFnSc1	většina
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
otázkách	otázka	k1gFnPc6	otázka
se	se	k3xPyFc4	se
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
přijímají	přijímat	k5eAaImIp3nP	přijímat
konsenzem	konsenz	k1gInSc7	konsenz
všech	všecek	k3xTgInPc2	všecek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
konsenzus	konsenzus	k1gInSc1	konsenzus
znamená	znamenat	k5eAaImIp3nS	znamenat
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
souhlas	souhlas	k1gInSc4	souhlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
vznáší	vznášet	k5eAaImIp3nS	vznášet
připomínku	připomínka	k1gFnSc4	připomínka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rokuje	rokovat	k5eAaImIp3nS	rokovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
nevznáší	vznášet	k5eNaImIp3nP	vznášet
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
věc	věc	k1gFnSc1	věc
za	za	k7c4	za
schválenou	schválený	k2eAgFnSc4d1	schválená
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
nerokuje	rokovat	k5eNaImIp3nS	rokovat
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
hlasováních	hlasování	k1gNnPc6	hlasování
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc1	každý
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Přítomní	přítomný	k2eAgMnPc1d1	přítomný
hlasující	hlasující	k1gMnPc1	hlasující
jsou	být	k5eAaImIp3nP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
fyzicky	fyzicky	k6eAd1	fyzicky
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předseda	předseda	k1gMnSc1	předseda
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
==	==	k?	==
</s>
</p>
<p>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
začíná	začínat	k5eAaImIp3nS	začínat
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Předsednictví	předsednictví	k1gNnPc4	předsednictví
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc4	rok
rotuje	rotovat	k5eAaImIp3nS	rotovat
mezi	mezi	k7c7	mezi
5	[number]	k4	5
geografickými	geografický	k2eAgInPc7d1	geografický
regiony	region	k1gInPc7	region
<g/>
:	:	kIx,	:
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
Karibik	Karibik	k1gInSc1	Karibik
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
největších	veliký	k2eAgInPc2d3	veliký
a	a	k8xC	a
nejmocnějších	mocný	k2eAgInPc2d3	nejmocnější
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
stálí	stálý	k2eAgMnPc1d1	stálý
členové	člen	k1gMnPc1	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nepředsedali	předsedat	k5eNaImAgMnP	předsedat
Valnému	valný	k2eAgNnSc3d1	Valné
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kavan	Kavan	k1gMnSc1	Kavan
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
57	[number]	k4	57
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
64	[number]	k4	64
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
===	===	k?	===
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
předsedou	předseda	k1gMnSc7	předseda
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Ali	Ali	k1gMnSc1	Ali
Abdussalam	Abdussalam	k1gInSc4	Abdussalam
Treki	Trek	k1gFnSc2	Trek
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Treki	Treki	k6eAd1	Treki
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc2	úřad
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
po	po	k7c6	po
Miguelovi	Miguel	k1gMnSc6	Miguel
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Escoto	Escota	k1gFnSc5	Escota
Brockmannovi	Brockmannův	k2eAgMnPc1d1	Brockmannův
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Nikaraguy	Nikaragua	k1gFnSc2	Nikaragua
<g/>
)	)	kIx)	)
</s>
</p>
