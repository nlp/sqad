<s>
První	první	k4xOgNnSc1	první
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1946	[number]	k4	1946
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
i	i	k9	i
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
