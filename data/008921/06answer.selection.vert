<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Gabrielle	Gabrielle	k1gInSc1	Gabrielle
Chanel	Chanel	k1gInSc1	Chanel
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1883	[number]	k4	1883
Saumur	Saumura	k1gFnPc2	Saumura
<g/>
,	,	kIx,	,
Pays	Paysa	k1gFnPc2	Paysa
de	de	k?	de
la	la	k1gNnSc4	la
Loire	Loir	k1gInSc5	Loir
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
osobnost	osobnost	k1gFnSc4	osobnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
utváření	utváření	k1gNnSc2	utváření
šatníku	šatník	k1gInSc2	šatník
moderní	moderní	k2eAgFnSc2d1	moderní
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
