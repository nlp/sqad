<s>
Kdo	kdo	k3yInSc1	kdo
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
osobnost	osobnost	k1gFnSc4	osobnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
utváření	utváření	k1gNnSc2	utváření
šatníku	šatník	k1gInSc2	šatník
moderní	moderní	k2eAgFnSc2d1	moderní
ženy	žena	k1gFnSc2	žena
<g/>
?	?	kIx.	?
</s>
