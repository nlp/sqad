<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
přirozených	přirozený	k2eAgInPc2d1	přirozený
logaritmů	logaritmus	k1gInPc2	logaritmus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jako	jako	k9	jako
Napierova	Napierův	k2eAgFnSc1d1	Napierův
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
značeno	značit	k5eAaImNgNnS	značit
písmenem	písmeno	k1gNnSc7	písmeno
e	e	k0	e
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
matematických	matematický	k2eAgFnPc2d1	matematická
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
