<s>
Čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
slovenština	slovenština	k1gFnSc1	slovenština
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
až	až	k9	až
98	[number]	k4	98
%	%	kIx~	%
praslovanské	praslovanský	k2eAgFnSc2d1	praslovanská
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
