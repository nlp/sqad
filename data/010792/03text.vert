<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
je	být	k5eAaImIp3nS	být
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dolní	dolní	k2eAgMnPc4d1	dolní
Dubňany	Dubňan	k1gMnPc4	Dubňan
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
románský	románský	k2eAgInSc1d1	románský
chrám	chrám	k1gInSc1	chrám
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
upravovaný	upravovaný	k2eAgInSc1d1	upravovaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
filiálním	filiální	k2eAgInSc7d1	filiální
kostelem	kostel	k1gInSc7	kostel
farnosti	farnost	k1gFnSc2	farnost
Horní	horní	k2eAgMnPc4d1	horní
Dubňany	Dubňan	k1gMnPc4	Dubňan
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jamolickou	jamolický	k2eAgFnSc7d1	jamolický
komendou	komenda	k1gFnSc7	komenda
Templářů	templář	k1gMnPc2	templář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
koruna	koruna	k1gFnSc1	koruna
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
možná	možná	k9	možná
tehdy	tehdy	k6eAd1	tehdy
dostala	dostat	k5eAaPmAgFnS	dostat
zděný	zděný	k2eAgInSc4d1	zděný
střešní	střešní	k2eAgInSc4d1	střešní
jehlan	jehlan	k1gInSc4	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oprav	oprava	k1gFnPc2	oprava
kostela	kostel	k1gInSc2	kostel
zazděn	zazděn	k2eAgInSc1d1	zazděn
původní	původní	k2eAgInSc1d1	původní
vstup	vstup	k1gInSc1	vstup
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
zdi	zeď	k1gFnSc6	zeď
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
podvěžím	podvěžit	k5eAaPmIp1nS	podvěžit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zbudováno	zbudován	k2eAgNnSc1d1	zbudováno
věžní	věžní	k2eAgNnSc1d1	věžní
schodiště	schodiště	k1gNnSc1	schodiště
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnPc1d1	původní
malá	malý	k2eAgNnPc1d1	malé
hrotitá	hrotitý	k2eAgNnPc1d1	hrotité
okna	okno	k1gNnPc1	okno
byla	být	k5eAaImAgNnP	být
zazděna	zazděn	k2eAgFnSc1d1	zazděna
a	a	k8xC	a
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
byl	být	k5eAaImAgInS	být
upravován	upravovat	k5eAaImNgInS	upravovat
strop	strop	k1gInSc1	strop
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
roku	rok	k1gInSc3	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nacházely	nacházet	k5eAaImAgInP	nacházet
dva	dva	k4xCgInPc1	dva
starobylé	starobylý	k2eAgInPc1d1	starobylý
zvony	zvon	k1gInPc1	zvon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
datovaný	datovaný	k2eAgInSc4d1	datovaný
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nesl	nést	k5eAaImAgInS	nést
německý	německý	k2eAgInSc1d1	německý
nápis	nápis	k1gInSc1	nápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
stojí	stát	k5eAaImIp3nS	stát
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
východním	východní	k2eAgInSc7d1	východní
okrajem	okraj	k1gInSc7	okraj
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednolodní	jednolodní	k2eAgFnSc4d1	jednolodní
orientovanou	orientovaný	k2eAgFnSc4d1	orientovaná
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
polygonálně	polygonálně	k6eAd1	polygonálně
zakončeným	zakončený	k2eAgNnSc7d1	zakončené
kněžištěm	kněžiště	k1gNnSc7	kněžiště
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
kostela	kostel	k1gInSc2	kostel
má	mít	k5eAaImIp3nS	mít
obdélný	obdélný	k2eAgInSc4d1	obdélný
půdorys	půdorys	k1gInSc4	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
západnímu	západní	k2eAgNnSc3d1	západní
průčelí	průčelí	k1gNnSc3	průčelí
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
její	její	k3xOp3gFnSc7	její
jižní	jižní	k2eAgFnSc7d1	jižní
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
lodí	loď	k1gFnSc7	loď
stojí	stát	k5eAaImIp3nS	stát
přístavba	přístavba	k1gFnSc1	přístavba
točitého	točitý	k2eAgNnSc2d1	točité
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
straně	strana	k1gFnSc3	strana
kněžiště	kněžiště	k1gNnSc2	kněžiště
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
sakristie	sakristie	k1gFnSc1	sakristie
obdélného	obdélný	k2eAgInSc2d1	obdélný
půdorysu	půdorys	k1gInSc2	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Fasády	fasáda	k1gFnPc1	fasáda
kostela	kostel	k1gInSc2	kostel
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc4	loď
osvětlují	osvětlovat	k5eAaImIp3nP	osvětlovat
barokní	barokní	k2eAgNnPc1d1	barokní
půlkruhová	půlkruhový	k2eAgNnPc1d1	půlkruhové
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
v	v	k7c6	v
kněžišti	kněžiště	k1gNnSc6	kněžiště
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
okna	okno	k1gNnPc1	okno
podélná	podélný	k2eAgNnPc1d1	podélné
se	se	k3xPyFc4	se
segmentovým	segmentový	k2eAgInSc7d1	segmentový
záklenkem	záklenek	k1gInSc7	záklenek
<g/>
.	.	kIx.	.
</s>
<s>
Zvonicové	Zvonicový	k2eAgNnSc1d1	Zvonicový
patro	patro	k1gNnSc1	patro
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
protáhlými	protáhlý	k2eAgInPc7d1	protáhlý
okny	okno	k1gNnPc7	okno
s	s	k7c7	s
půlkruhovým	půlkruhový	k2eAgInSc7d1	půlkruhový
záklenkem	záklenek	k1gInSc7	záklenek
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
zakončuje	zakončovat	k5eAaImIp3nS	zakončovat
ochoz	ochoz	k1gInSc4	ochoz
s	s	k7c7	s
cimbuřím	cimbuří	k1gNnSc7	cimbuří
<g/>
,	,	kIx,	,
zastřešení	zastřešený	k2eAgMnPc1d1	zastřešený
tvoří	tvořit	k5eAaImIp3nP	tvořit
zděný	zděný	k2eAgInSc4d1	zděný
jehlan	jehlan	k1gInSc4	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
západu	západ	k1gInSc2	západ
podvěžím	podvěžit	k5eAaImIp1nS	podvěžit
<g/>
.	.	kIx.	.
<g/>
Kněžiště	kněžiště	k1gNnSc1	kněžiště
nese	nést	k5eAaImIp3nS	nést
jedno	jeden	k4xCgNnSc1	jeden
pole	pole	k1gNnSc1	pole
křížové	křížový	k2eAgFnSc2d1	křížová
klenby	klenba	k1gFnSc2	klenba
s	s	k7c7	s
lunetovým	lunetový	k2eAgInSc7d1	lunetový
závěrem	závěr	k1gInSc7	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Strop	strop	k1gInSc1	strop
lodi	loď	k1gFnSc2	loď
i	i	k8xC	i
sakristie	sakristie	k1gFnSc2	sakristie
je	být	k5eAaImIp3nS	být
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
stěně	stěna	k1gFnSc6	stěna
kněžiště	kněžiště	k1gNnSc2	kněžiště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sedlový	sedlový	k2eAgInSc1d1	sedlový
vstupní	vstupní	k2eAgInSc1d1	vstupní
portál	portál	k1gInSc1	portál
do	do	k7c2	do
sakristie	sakristie	k1gFnSc2	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
kněžiště	kněžiště	k1gNnSc2	kněžiště
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
zděný	zděný	k2eAgInSc1d1	zděný
půlkruhový	půlkruhový	k2eAgInSc1d1	půlkruhový
vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
lodi	loď	k1gFnSc2	loď
stojí	stát	k5eAaImIp3nS	stát
novodobá	novodobý	k2eAgFnSc1d1	novodobá
hudební	hudební	k2eAgFnSc1d1	hudební
kruchta	kruchta	k1gFnSc1	kruchta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vybavení	vybavení	k1gNnSc6	vybavení
interiéru	interiér	k1gInSc2	interiér
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
pozdně	pozdně	k6eAd1	pozdně
románská	románský	k2eAgFnSc1d1	románská
kamenná	kamenný	k2eAgFnSc1d1	kamenná
křtitelnice	křtitelnice	k1gFnSc1	křtitelnice
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
obraz	obraz	k1gInSc1	obraz
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hřbitov	hřbitov	k1gInSc1	hřbitov
s	s	k7c7	s
márnicí	márnice	k1gFnSc7	márnice
<g/>
,	,	kIx,	,
obehnaný	obehnaný	k2eAgInSc1d1	obehnaný
ohradní	ohradní	k2eAgFnSc7d1	ohradní
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SAMEK	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
474	[number]	k4	474
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
386	[number]	k4	386
<g/>
–	–	k?	–
<g/>
387	[number]	k4	387
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
