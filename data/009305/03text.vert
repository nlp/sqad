<p>
<s>
Brezovica	Brezovica	k1gFnSc1	Brezovica
je	být	k5eAaImIp3nS	být
potok	potok	k1gInSc4	potok
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
Oravě	Orava	k1gFnSc6	Orava
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
okresu	okres	k1gInSc2	okres
Tvrdošín	Tvrdošína	k1gFnPc2	Tvrdošína
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
Oravice	Oravice	k1gFnSc2	Oravice
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
7	[number]	k4	7
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tokem	tok	k1gInSc7	tok
V.	V.	kA	V.
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pramen	pramen	k1gInSc1	pramen
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
výběžku	výběžek	k1gInSc6	výběžek
Skorušinských	Skorušinský	k2eAgInPc2d1	Skorušinský
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
podcelku	podcelek	k1gInSc6	podcelek
Skorušina	Skorušin	k2eAgNnSc2d1	Skorušin
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
svahu	svah	k1gInSc6	svah
Skorušiny	Skorušin	k2eAgFnPc1d1	Skorušina
(	(	kIx(	(
<g/>
1	[number]	k4	1
313,8	[number]	k4	313,8
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
1	[number]	k4	1
155	[number]	k4	155
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pramenné	pramenný	k2eAgFnSc6d1	pramenná
oblasti	oblast	k1gFnSc6	oblast
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
přibírá	přibírat	k5eAaImIp3nS	přibírat
dva	dva	k4xCgInPc4	dva
přítoky	přítok	k1gInPc4	přítok
také	také	k9	také
ze	z	k7c2	z
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
svahů	svah	k1gInPc2	svah
Skorušiny	Skorušin	k2eAgFnSc2d1	Skorušina
a	a	k8xC	a
stáčí	stáčet	k5eAaImIp3nS	stáčet
se	se	k3xPyFc4	se
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
přibírá	přibírat	k5eAaImIp3nS	přibírat
další	další	k2eAgInSc1d1	další
přítok	přítok	k1gInSc1	přítok
ze	z	k7c2	z
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
svahu	svah	k1gInSc2	svah
Skorušiny	Skorušin	k2eAgFnSc2d1	Skorušina
a	a	k8xC	a
z	z	k7c2	z
téže	tenže	k3xDgFnSc2	tenže
strany	strana	k1gFnSc2	strana
přítok	přítok	k1gInSc1	přítok
ze	z	k7c2	z
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
svahu	svah	k1gInSc2	svah
Javoriniek	Javoriniky	k1gFnPc2	Javoriniky
(	(	kIx(	(
<g/>
1	[number]	k4	1
122,7	[number]	k4	122,7
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nevýrazný	výrazný	k2eNgInSc1d1	nevýrazný
oblouk	oblouk	k1gInSc1	oblouk
ohnutý	ohnutý	k2eAgInSc1d1	ohnutý
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
přibírá	přibírat	k5eAaImIp3nS	přibírat
přítok	přítok	k1gInSc4	přítok
nejprve	nejprve	k6eAd1	nejprve
ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
svahu	svah	k1gInSc2	svah
Ostrého	ostrý	k2eAgInSc2d1	ostrý
vrchu	vrch	k1gInSc2	vrch
(	(	kIx(	(
<g/>
942,4	[number]	k4	942,4
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
další	další	k2eAgMnSc1d1	další
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Vyševcové	Vyševcový	k2eAgNnSc1d1	Vyševcový
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
bažinatý	bažinatý	k2eAgMnSc1d1	bažinatý
územím	území	k1gNnSc7	území
východně	východně	k6eAd1	východně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Brezovica	Brezovicum	k1gNnSc2	Brezovicum
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Oravské	oravský	k2eAgFnSc2d1	Oravská
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přibírá	přibírat	k5eAaImIp3nS	přibírat
ještě	ještě	k9	ještě
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Priečneho	Priečne	k1gMnSc2	Priečne
a	a	k8xC	a
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
obce	obec	k1gFnSc2	obec
Liesek	Lieska	k1gFnPc2	Lieska
ústí	ústit	k5eAaImIp3nS	ústit
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
637	[number]	k4	637
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
do	do	k7c2	do
Oravice	Oravice	k1gFnSc2	Oravice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgInPc1d1	jiný
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Teplica	Teplica	k6eAd1	Teplica
</s>
</p>
<p>
<s>
Brezovický	Brezovický	k2eAgInSc1d1	Brezovický
potok	potok	k1gInSc1	potok
</s>
</p>
<p>
<s>
nárečovo	nárečův	k2eAgNnSc1d1	nárečův
<g/>
:	:	kIx,	:
Brezovčík	Brezovčík	k1gInSc1	Brezovčík
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Oravice	Oravice	k1gFnSc1	Oravice
</s>
</p>
<p>
<s>
Brazovica	Brazovica	k6eAd1	Brazovica
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brezovica	Brezovic	k1gInSc2	Brezovic
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
