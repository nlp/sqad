<s>
Marilyn	Marilyn	k1gFnSc1
Manson	Mansona	k1gFnPc2
</s>
<s>
O	o	k7c6
hudební	hudební	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Marilyn	Marilyn	k1gFnSc2
Manson	Mansona	k1gFnPc2
(	(	kIx(
<g/>
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gMnSc1
Brian	Brian	k1gMnSc1
Hugh	Hugh	k1gMnSc1
WarnerZákladní	WarnerZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Brian	Brian	k1gMnSc1
Warner	Warner	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Canton	Canton	k1gInSc1
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Žánry	žánr	k1gInPc1
</s>
<s>
alternative	alternativ	k1gInSc5
metalrůzné	metalrůzný	k2eAgInPc1d1
diskutované	diskutovaný	k2eAgInPc1d1
styly	styl	k1gInPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zpěvák-skladatel	zpěvák-skladatel	k1gMnSc1
<g/>
,	,	kIx,
hudebník	hudebník	k1gMnSc1
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
klávesy	kláves	k1gInPc1
<g/>
,	,	kIx,
flétna	flétna	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
1989	#num#	k4
-	-	kIx~
dosud	dosud	k6eAd1
Vydavatelé	vydavatel	k1gMnPc1
</s>
<s>
Nothing	Nothing	k1gInSc1
<g/>
,	,	kIx,
Interscope	Interscop	k1gInSc5
Příbuzná	příbuzný	k2eAgNnPc5d1
témata	téma	k1gNnPc4
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gMnSc1
Mrs	Mrs	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scabtree	Scabtre	k1gInSc2
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Dita	Dita	k1gFnSc1
Von	von	k1gInSc1
Teese	Teese	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Esmé	Esmé	k6eAd1
Bianco	bianco	k6eAd1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Evan	Evano	k1gNnPc2
Rachel	Rachela	k1gFnPc2
Woodová	Woodová	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brian	Brian	k1gMnSc1
Hugh	Hugh	k1gMnSc1
Warner	Warner	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
Canton	Canton	k1gInSc1
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
<g/>
,	,	kIx,
intelektuál	intelektuál	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dal	dát	k5eAaPmAgMnS
vzniknout	vzniknout	k5eAaPmF
několika	několik	k4yIc7
novým	nový	k2eAgInPc3d1
ideologickým	ideologický	k2eAgInPc3d1
směrům	směr	k1gInPc3
a	a	k8xC
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
spolu	spolu	k6eAd1
se	s	k7c7
skupinami	skupina	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Nine	Nine	k1gNnSc1
Inch	Incha	k1gFnPc2
Nails	Nailsa	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Eurythmics	Eurythmics	k1gInSc4
tvořili	tvořit	k5eAaImAgMnP
tzv.	tzv.	kA
"	"	kIx"
<g/>
Temnou	temný	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
<g/>
"	"	kIx"
populární	populární	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
Marilyn	Marilyn	k1gFnPc2
Manson	Manson	k1gInSc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
svůj	svůj	k3xOyFgInSc4
pseudonym	pseudonym	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
ze	z	k7c2
jmen	jméno	k1gNnPc2
filmové	filmový	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
Marilyn	Marilyn	k1gFnSc1
Monroe	Monroe	k1gFnSc1
a	a	k8xC
známého	známý	k2eAgMnSc2d1
zločince	zločinec	k1gMnSc2
Charlese	Charles	k1gMnSc2
Mansona	Manson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
filozofické	filozofický	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
kritiku	kritika	k1gFnSc4
konzervatismu	konzervatismus	k1gInSc2
a	a	k8xC
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
působení	působení	k1gNnSc6
jako	jako	k9
"	"	kIx"
<g/>
Antikrista	Antikrist	k1gMnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
personu	persona	k1gFnSc4
a	a	k8xC
alter-ego	alter-ego	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
Marilyna	Marilyno	k1gNnSc2
Mansona	Manson	k1gMnSc4
vytvořil	vytvořit	k5eAaPmAgInS
za	za	k7c7
účelem	účel	k1gInSc7
šokovat	šokovat	k5eAaBmF
konzervativní	konzervativní	k2eAgFnSc4d1
<g/>
,	,	kIx,
křesťanskou	křesťanský	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
Americké	americký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krédo	Krédo	k1gNnSc1
</s>
<s>
„	„	k?
</s>
<s>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
mají	mít	k5eAaImIp3nP
takový	takový	k3xDgInSc4
strach	strach	k1gInSc4
z	z	k7c2
toho	ten	k3xDgNnSc2
vypadat	vypadat	k5eAaImF,k5eAaPmF
jinak	jinak	k6eAd1
než	než	k8xS
ostatní	ostatní	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přizpůsobí	přizpůsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
polknou	polknout	k5eAaPmIp3nP
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
jim	on	k3xPp3gMnPc3
je	být	k5eAaImIp3nS
předepsáno	předepsat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raději	rád	k6eAd2
umřu	umřít	k5eAaPmIp1nS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
jsem	být	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Brian	Brian	k1gMnSc1
Warner	Warner	k1gMnSc1
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
a	a	k8xC
ideologie	ideologie	k1gFnSc1
</s>
<s>
Brian	Brian	k1gMnSc1
Warner	Warner	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
do	do	k7c2
silně	silně	k6eAd1
konzervativní	konzervativní	k2eAgFnSc2d1
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobné	podrobný	k2eAgInPc1d1
zážitky	zážitek	k1gInPc1
z	z	k7c2
této	tento	k3xDgFnSc2
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
hodnotí	hodnotit	k5eAaImIp3nS
jako	jako	k9
chybnou	chybný	k2eAgFnSc4d1
popisuje	popisovat	k5eAaImIp3nS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
autobiografické	autobiografický	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
"	"	kIx"
<g/>
Long	Long	k1gMnSc1
Hard	Hard	k1gMnSc1
Roud	Roud	k1gMnSc1
out	out	k?
of	of	k?
Hell	Hell	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
trnitá	trnitý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
Pekla	peklo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
tzv.	tzv.	kA
Episkopální	episkopální	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
Římský	římský	k2eAgMnSc1d1
Katolík	katolík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
dětství	dětství	k1gNnSc4
navštěvoval	navštěvovat	k5eAaImAgMnS
Náboženskou	náboženský	k2eAgFnSc4d1
Křesťanskou	křesťanský	k2eAgFnSc4d1
Základní	základní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Cantonu	Canton	k1gInSc6
<g/>
,	,	kIx,
Ohiu	Ohio	k1gNnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
v	v	k7c6
důsledku	důsledek	k1gInSc6
své	svůj	k3xOyFgFnSc2
nenávisti	nenávist	k1gFnSc2
k	k	k7c3
náboženství	náboženství	k1gNnSc3
přestoupil	přestoupit	k5eAaPmAgInS
na	na	k7c4
školu	škola	k1gFnSc4
GlenOak	GlenOak	k1gInSc1
High	High	k1gMnSc1
School	School	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgFnPc6
školách	škola	k1gFnPc6
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
líného	líný	k2eAgMnSc4d1
<g/>
,	,	kIx,
introvertního	introvertní	k2eAgMnSc4d1
studenta	student	k1gMnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
skrytého	skrytý	k2eAgMnSc4d1
génia	génius	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zlomovým	zlomový	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
jej	on	k3xPp3gMnSc4
donutil	donutit	k5eAaPmAgInS
v	v	k7c6
dětství	dětství	k1gNnSc6
přeorientovat	přeorientovat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
ideologický	ideologický	k2eAgInSc4d1
postoj	postoj	k1gInSc4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
vlastní	vlastní	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
(	(	kIx(
<g/>
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
<g/>
)	)	kIx)
nechovala	chovat	k5eNaImAgFnS
dostatečně	dostatečně	k6eAd1
pobožně	pobožně	k6eAd1
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
samotného	samotný	k2eAgMnSc4d1
za	za	k7c4
toto	tento	k3xDgNnSc4
chování	chování	k1gNnSc4
kritizovali	kritizovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
příklad	příklad	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
biografii	biografie	k1gFnSc6
uvádí	uvádět	k5eAaImIp3nS
příklad	příklad	k1gInSc4
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
dědečka	dědeček	k1gMnSc2
Jacka	Jacek	k1gMnSc2
Warnera	Warner	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
jej	on	k3xPp3gNnSc4
tvrdě	tvrdě	k6eAd1
trestal	trestat	k5eAaImAgMnS
za	za	k7c4
výstřední	výstřední	k2eAgNnSc4d1
chování	chování	k1gNnSc4
a	a	k8xC
tajně	tajně	k6eAd1
měl	mít	k5eAaImAgInS
fetiš	fetiš	k1gInSc1
pro	pro	k7c4
oblékání	oblékání	k1gNnSc4
do	do	k7c2
ženských	ženský	k2eAgInPc2d1
šatů	šat	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jej	on	k3xPp3gMnSc4
bil	bít	k5eAaImAgMnS
za	za	k7c4
utrácení	utrácení	k1gNnSc4
kapesného	kapesné	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
rodinu	rodina	k1gFnSc4
přiváděl	přivádět	k5eAaImAgMnS
do	do	k7c2
finančních	finanční	k2eAgFnPc2d1
krizí	krize	k1gFnPc2
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
velké	velký	k2eAgFnSc3d1
konzumaci	konzumace	k1gFnSc3
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dospělosti	dospělost	k1gFnSc6
rodinu	rodina	k1gFnSc4
opustil	opustit	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
vystupovat	vystupovat	k5eAaImF
pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
alternativním	alternativní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Marilyn	Marilyn	k1gFnSc2
Manson	Manson	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
redaktor	redaktor	k1gMnSc1
a	a	k8xC
novinář	novinář	k1gMnSc1
pro	pro	k7c4
hudební	hudební	k2eAgInSc4d1
časopis	časopis	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
však	však	k9
skládal	skládat	k5eAaImAgMnS
básně	báseň	k1gFnPc4
a	a	k8xC
nápěvky	nápěvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Trent	Trent	k1gMnSc1
Reznor	Reznor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jej	on	k3xPp3gMnSc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
začal	začít	k5eAaPmAgInS
tvořit	tvořit	k5eAaImF
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
skupina	skupina	k1gFnSc1
byla	být	k5eAaImAgFnS
několikrát	několikrát	k6eAd1
přejmenována	přejmenovat	k5eAaPmNgFnS
<g/>
,	,	kIx,
ustálila	ustálit	k5eAaPmAgFnS
se	se	k3xPyFc4
však	však	k9
na	na	k7c6
jménu	jméno	k1gNnSc6
Marilyn	Marilyn	k1gFnSc2
Manson	Manson	k1gInSc4
and	and	k?
the	the	k?
Spooky	Spook	k1gInPc1
Kids	Kids	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
jen	jen	k9
Marilyn	Marilyn	k1gFnSc1
Manson	Mansona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
byl	být	k5eAaImAgInS
ženatý	ženatý	k2eAgInSc1d1
dvakrát	dvakrát	k6eAd1
-	-	kIx~
v	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
-	-	kIx~
2007	#num#	k4
s	s	k7c7
modelkou	modelka	k1gFnSc7
Dita	Dita	k1gFnSc1
Von	von	k1gInSc4
Teese	Teese	k1gFnSc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
měl	mít	k5eAaImAgInS
nemanželský	manželský	k2eNgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
Evan	Evana	k1gFnPc2
Rachel-Wood	Rachel-Wood	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
Lindsay	Lindsa	k1gMnPc7
Usich	Usich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Skandály	skandál	k1gInPc1
a	a	k8xC
popularita	popularita	k1gFnSc1
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Manson	Mansona	k1gFnPc2
svou	svůj	k3xOyFgFnSc4
hudbu	hudba	k1gFnSc4
od	od	k7c2
začátku	začátek	k1gInSc2
cílil	cílit	k5eAaImAgInS
proti	proti	k7c3
konzervativní	konzervativní	k2eAgFnSc3d1
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanská	křesťanský	k2eAgFnSc1d1
sdružení	sdružení	k1gNnPc4
jej	on	k3xPp3gNnSc2
několikrát	několikrát	k6eAd1
za	za	k7c4
jeho	jeho	k3xOp3gFnSc4
kariéru	kariéra	k1gFnSc4
zažalovala	zažalovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
sabotovala	sabotovat	k5eAaImAgFnS
jeho	jeho	k3xOp3gInPc4
koncerty	koncert	k1gInPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
tvořila	tvořit	k5eAaImAgFnS
veřejné	veřejný	k2eAgInPc4d1
protesty	protest	k1gInPc4
proti	proti	k7c3
jeho	jeho	k3xOp3gFnSc3
tvorbě	tvorba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
skandál	skandál	k1gInSc1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
životě	život	k1gInSc6
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
Masakru	masakr	k1gInSc3
na	na	k7c4
High-School	High-School	k1gInSc4
Columbine	Columbin	k1gInSc5
-	-	kIx~
mladými	mladý	k2eAgMnPc7d1
muži	muž	k1gMnPc7
Eric	Eric	k1gFnSc1
Harris	Harris	k1gFnSc1
a	a	k8xC
Dylan	Dylan	k1gMnSc1
Klebold	Klebold	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
ve	v	k7c6
škole	škola	k1gFnSc6
zastřelili	zastřelit	k5eAaPmAgMnP
13	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neprokázala	prokázat	k5eNaPmAgFnS
jejich	jejich	k3xOp3gFnSc1
spojitost	spojitost	k1gFnSc1
s	s	k7c7
Marilynem	Marilyn	k1gMnSc7
Mansonem	Manson	k1gMnSc7
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
obvinila	obvinit	k5eAaPmAgFnS
Briana	Brian	k1gMnSc4
Warnera	Warner	k1gMnSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
hudbě	hudba	k1gFnSc6
navádí	navádět	k5eAaImIp3nS
mladé	mladý	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
k	k	k7c3
násilí	násilí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
skandál	skandál	k1gInSc1
vyvrcholil	vyvrcholit	k5eAaPmAgInS
protestem	protest	k1gInSc7
Křesťanské	křesťanský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
před	před	k7c7
jeho	jeho	k3xOp3gInSc7
koncertem	koncert	k1gInSc7
a	a	k8xC
celá	celý	k2eAgFnSc1d1
událost	událost	k1gFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
zfilmována	zfilmovat	k5eAaPmNgFnS
do	do	k7c2
dokumentárního	dokumentární	k2eAgInSc2d1
<g/>
,	,	kIx,
nezávislého	závislý	k2eNgInSc2d1
filmu	film	k1gInSc2
Michaela	Michael	k1gMnSc2
Moora	Moor	k1gMnSc2
-	-	kIx~
Bowling	bowling	k1gInSc1
for	forum	k1gNnPc2
Columbine	Columbin	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Manson	Manson	k1gNnSc1
se	se	k3xPyFc4
během	během	k7c2
filmu	film	k1gInSc2
obhájil	obhájit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
žijí	žít	k5eAaImIp3nP
vyvinula	vyvinout	k5eAaPmAgFnS
na	na	k7c4
chlapce	chlapec	k1gMnPc4
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
tlak	tlak	k1gInSc1
a	a	k8xC
že	že	k8xS
za	za	k7c4
čin	čin	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
mohou	moct	k5eAaImIp3nP
rodiče	rodič	k1gMnPc1
a	a	k8xC
společnost	společnost	k1gFnSc1
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
ne	ne	k9
jeho	jeho	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
pro	pro	k7c4
incident	incident	k1gInSc4
v	v	k7c6
Columbine	Columbin	k1gInSc5
složil	složit	k5eAaPmAgMnS
skladbu	skladba	k1gFnSc4
na	na	k7c4
své	svůj	k3xOyFgNnSc4
nové	nový	k2eAgNnSc4d1
album	album	k1gNnSc4
Holy	hola	k1gFnSc2
Wood	Wood	k1gMnSc1
-	-	kIx~
The	The	k1gMnSc1
Nobodies	Nobodies	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
v	v	k7c6
textu	text	k1gInSc6
celou	celý	k2eAgFnSc4d1
událost	událost	k1gFnSc4
komentuje	komentovat	k5eAaBmIp3nS
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
V	v	k7c4
ten	ten	k3xDgInSc4
den	den	k1gInSc4
zemřely	zemřít	k5eAaPmAgFnP
děti	dítě	k1gFnPc1
-	-	kIx~
měli	mít	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
v	v	k7c4
ten	ten	k3xDgInSc4
den	den	k1gInSc4
vidět	vidět	k5eAaImF
mou	můj	k3xOp1gFnSc4
sledovanost	sledovanost	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
skandály	skandál	k1gInPc1
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
herečka	herečka	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
bývalá	bývalý	k2eAgFnSc1d1
partnerka	partnerka	k1gFnSc1
Evan	Evana	k1gFnPc2
Rachel	Rachela	k1gFnPc2
Woodová	Woodový	k2eAgFnSc1d1
v	v	k7c6
médiích	médium	k1gNnPc6
nařknula	nařknout	k5eAaPmAgFnS
Mansona	Mansona	k1gFnSc1
z	z	k7c2
agresivního	agresivní	k2eAgNnSc2d1
chování	chování	k1gNnSc2
vůči	vůči	k7c3
ní	on	k3xPp3gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
jejich	jejich	k3xOp3gNnSc2
partnerství	partnerství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fanoušci	Fanoušek	k1gMnPc1
Marilyna	Marilyn	k1gMnSc2
Mansona	Manson	k1gMnSc2
se	se	k3xPyFc4
proti	proti	k7c3
jejím	její	k3xOp3gNnPc3
obviněním	obvinění	k1gNnPc3
ozvali	ozvat	k5eAaPmAgMnP
založením	založení	k1gNnSc7
internetového	internetový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
#	#	kIx~
<g/>
ISupportMarilynManson	ISupportMarilynManson	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
</s>
<s>
Vedle	vedle	k7c2
zpěvu	zpěv	k1gInSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
i	i	k9
malování	malování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
autor	autor	k1gMnSc1
ponurých	ponurý	k2eAgInPc2d1
a	a	k8xC
bizarních	bizarní	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
malovaných	malovaný	k2eAgFnPc2d1
akvarelovými	akvarelový	k2eAgFnPc7d1
barvami	barva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
umělce	umělec	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
autobiografii	autobiografie	k1gFnSc6
nazvané	nazvaný	k2eAgFnSc2d1
The	The	k1gFnSc2
„	„	k?
<g/>
Long	Long	k1gMnSc1
Hard	Hard	k1gMnSc1
Road	Road	k1gMnSc1
out	out	k?
of	of	k?
Hell	Hell	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
německo-polského	německo-polský	k2eAgInSc2d1
původu	původ	k1gInSc2
(	(	kIx(
<g/>
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
majitelem	majitel	k1gMnSc7
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
vyrábějící	vyrábějící	k2eAgFnSc2d1
absinth	absinth	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
název	název	k1gInSc1
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
Mansinthe	Mansinthe	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
všeho	všecek	k3xTgNnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
prodej	prodej	k1gInSc1
tohoto	tento	k3xDgInSc2
alkoholického	alkoholický	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
velice	velice	k6eAd1
daří	dařit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
1994	#num#	k4
Portrait	Portrait	k1gMnSc1
of	of	k?
an	an	k?
American	American	k1gInSc4
Family	Famila	k1gFnSc2
</s>
<s>
1995	#num#	k4
Smells	Smells	k1gInSc1
Like	Lik	k1gFnSc2
Children	Childrna	k1gFnPc2
(	(	kIx(
<g/>
EP	EP	kA
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
Antichrist	Antichrist	k1gMnSc1
Superstar	superstar	k1gFnSc4
</s>
<s>
1997	#num#	k4
Remix	Remix	k1gInSc1
&	&	k?
Repent	Repent	k1gInSc1
(	(	kIx(
<g/>
EP	EP	kA
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Mechanical	Mechanical	k1gFnSc1
Animals	Animalsa	k1gFnPc2
</s>
<s>
1999	#num#	k4
The	The	k1gMnSc1
Last	Last	k1gMnSc1
Tour	Tour	k1gInSc4
on	on	k3xPp3gMnSc1
Earth	Earth	k1gMnSc1
(	(	kIx(
<g/>
live	live	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Holy	hola	k1gFnSc2
Wood	Wooda	k1gFnPc2
(	(	kIx(
<g/>
In	In	k1gFnSc1
the	the	k?
Shadow	Shadow	k1gFnSc2
of	of	k?
the	the	k?
Valley	Vallea	k1gFnSc2
of	of	k?
Death	Death	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
The	The	k1gFnSc1
Golden	Goldna	k1gFnPc2
Age	Age	k1gMnSc2
of	of	k?
Grotesque	Grotesqu	k1gMnSc2
</s>
<s>
2004	#num#	k4
Lest	lest	k1gFnSc1
We	We	k1gFnSc2
Forget	Forget	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Best	Best	k1gMnSc1
of	of	k?
(	(	kIx(
<g/>
kompilace	kompilace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Eat	Eat	k1gFnSc1
Me	Me	k1gFnSc1
<g/>
,	,	kIx,
Drink	drink	k1gInSc1
Me	Me	k1gFnSc2
</s>
<s>
2009	#num#	k4
The	The	k1gMnSc1
High	High	k1gMnSc1
End	End	k1gMnSc1
of	of	k?
Low	Low	k1gMnSc1
</s>
<s>
2012	#num#	k4
Born	Born	k1gMnSc1
Villain	Villain	k1gMnSc1
</s>
<s>
2015	#num#	k4
The	The	k1gFnSc1
Pale	pal	k1gInSc5
Emperor	Emperor	k1gInSc1
</s>
<s>
2017	#num#	k4
Heaven	Heaven	k2eAgInSc1d1
Upside	Upsid	k1gInSc5
Down	Downa	k1gFnPc2
</s>
<s>
2020	#num#	k4
We	We	k1gFnSc1
Are	ar	k1gInSc5
Chaos	chaos	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Marilyn	Marilyn	k1gFnSc2
Manson	Manson	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Marilyn	Marilyn	k1gFnSc2
Manson	Mansona	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Manson	Mansona	k1gFnPc2
and	and	k?
the	the	k?
Spooky	Spook	k1gInPc1
Kids	Kids	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gInSc1
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gMnSc1
•	•	k?
Gil	Gil	k1gMnSc5
Sharone	Sharon	k1gMnSc5
•	•	k?
Paul	Paul	k1gMnSc1
Wiley	Wilea	k1gFnSc2
•	•	k?
Tyler	Tyler	k1gMnSc1
Bates	Bates	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
AldereteTwiggy	AldereteTwigga	k1gFnSc2
Ramirez	Ramirez	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Vrenna	Vrenno	k1gNnSc2
•	•	k?
Ginger	Ginger	k1gMnSc1
Fish	Fish	k1gMnSc1
•	•	k?
Andy	Anda	k1gFnSc2
Gerold	Gerold	k1gMnSc1
•	•	k?
Wes	Wes	k1gMnSc1
Borland	Borland	kA
•	•	k?
Rob	roba	k1gFnPc2
Holliday	Hollidaa	k1gFnSc2
•	•	k?
Tim	Tim	k?
Sköld	Sköld	k1gInSc1
•	•	k?
Madonna	Madonna	k1gFnSc1
Wayne	Wayn	k1gInSc5
Gacy	Gacum	k1gNnPc7
•	•	k?
Mark	Mark	k1gMnSc1
Chaussee	Chausse	k1gFnSc2
•	•	k?
John	John	k1gMnSc1
5	#num#	k4
•	•	k?
Zim	zima	k1gFnPc2
Zum	Zum	k1gFnSc1
•	•	k?
Daisy	Daisa	k1gFnSc2
Berkowitz	Berkowitz	k1gMnSc1
•	•	k?
Sara	Sara	k1gMnSc1
Lee	Lea	k1gFnSc3
Lucas	Lucas	k1gMnSc1
•	•	k?
Gidget	Gidget	k1gMnSc1
Gein	Gein	k1gMnSc1
•	•	k?
Olivia	Olivius	k1gMnSc4
Newton	Newton	k1gMnSc1
Bundy	bunda	k1gFnSc2
•	•	k?
Zsa	Zsa	k1gMnSc1
Zsa	Zsa	k1gMnSc1
Speck	Speck	k1gMnSc1
•	•	k?
Fred	Fred	k1gMnSc1
Sablan	Sablan	k1gMnSc1
•	•	k?
Jason	Jason	k1gMnSc1
Sutter	Sutter	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Portrait	Portrait	k1gInSc1
of	of	k?
an	an	k?
American	American	k1gMnSc1
Family	Famila	k1gFnSc2
•	•	k?
Antichrist	Antichrist	k1gMnSc1
Superstar	superstar	k1gFnSc2
•	•	k?
Mechanical	Mechanical	k1gFnSc2
Animals	Animalsa	k1gFnPc2
•	•	k?
Holy	hola	k1gFnSc2
Wood	Wood	k1gMnSc1
(	(	kIx(
<g/>
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Valley	Vallea	k1gFnSc2
of	of	k?
Death	Death	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc2
Golden	Goldna	k1gFnPc2
Age	Age	k1gFnSc2
of	of	k?
Grotesque	Grotesqu	k1gFnSc2
•	•	k?
Eat	Eat	k1gMnSc1
Me	Me	k1gMnSc1
<g/>
,	,	kIx,
Drink	drink	k1gInSc1
Me	Me	k1gFnSc2
•	•	k?
The	The	k1gMnSc1
High	High	k1gMnSc1
End	End	k1gMnSc1
of	of	k?
Low	Low	k1gMnSc1
•	•	k?
Born	Born	k1gMnSc1
Villain	Villain	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
Pale	pal	k1gInSc5
Emperor	Emperora	k1gFnPc2
•	•	k?
Heaven	Heavna	k1gFnPc2
Upside	Upsid	k1gInSc5
Down	Down	k1gNnSc4
EP	EP	kA
</s>
<s>
Smells	Smells	k6eAd1
Like	Like	k1gFnSc1
Children	Childrna	k1gFnPc2
•	•	k?
Remix	Remix	k1gInSc1
&	&	k?
Repent	Repent	k1gInSc4
Koncertní	koncertní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Last	Last	k2eAgInSc4d1
Tour	Tour	k1gInSc4
on	on	k3xPp3gMnSc1
Earth	Earth	k1gMnSc1
Kompilace	kompilace	k1gFnSc2
</s>
<s>
Lest	lest	k1gFnSc1
We	We	k1gFnPc2
Forget	Forget	k1gMnSc1
•	•	k?
Lost	Lost	k1gMnSc1
&	&	k?
Found	Found	k1gMnSc1
Videa	video	k1gNnSc2
</s>
<s>
Dead	Dead	k6eAd1
to	ten	k3xDgNnSc1
the	the	k?
World	Worldo	k1gNnPc2
•	•	k?
God	God	k1gMnSc2
Is	Is	k1gMnSc2
in	in	k?
the	the	k?
TV	TV	kA
•	•	k?
Guns	Guns	k1gInSc1
<g/>
,	,	kIx,
God	God	k1gMnSc1
and	and	k?
Government	Government	k1gMnSc1
Turné	turné	k1gNnSc2
</s>
<s>
Portrait	Portrait	k1gInSc1
of	of	k?
an	an	k?
American	American	k1gMnSc1
Family	Famila	k1gFnSc2
Tour	Tour	k1gMnSc1
•	•	k?
Smells	Smells	k1gInSc1
Like	Lik	k1gFnSc2
Children	Childrna	k1gFnPc2
Tour	Tour	k1gMnSc1
•	•	k?
Dead	Dead	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
World	Worldo	k1gNnPc2
Tour	Tour	k1gInSc1
•	•	k?
Mechanical	Mechanical	k1gFnSc2
Animals	Animalsa	k1gFnPc2
Tour	Tour	k1gMnSc1
•	•	k?
Beautiful	Beautiful	k1gInSc1
Monsters	Monsters	k1gInSc1
Tour	Tour	k1gInSc1
•	•	k?
Rock	rock	k1gInSc1
Is	Is	k1gMnSc1
Dead	Dead	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Guns	Guns	k1gInSc1
<g/>
,	,	kIx,
God	God	k1gMnSc1
and	and	k?
Government	Government	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Grotesk	grotesk	k1gInSc1
Burlesk	Burlesk	k1gInSc1
Tour	Tour	k1gInSc1
•	•	k?
Against	Against	k1gInSc1
All	All	k1gMnSc1
Gods	Godsa	k1gFnPc2
Tour	Tour	k1gMnSc1
•	•	k?
Rape	rape	k1gNnSc2
of	of	k?
the	the	k?
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
The	The	k1gMnSc1
High	High	k1gMnSc1
End	End	k1gMnSc1
of	of	k?
Low	Low	k1gMnSc1
Tour	Tour	k1gMnSc1
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
Nothing	Nothing	k1gInSc1
Records	Records	k1gInSc1
•	•	k?
Interscope	Interscop	k1gInSc5
Records	Recordsa	k1gFnPc2
•	•	k?
Trent	Trent	k1gMnSc1
Reznor	Reznor	k1gMnSc1
•	•	k?
Sean	Sean	k1gMnSc1
Beavan	Beavan	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
Off	Off	k1gMnSc1
Jill	Jill	k1gMnSc1
•	•	k?
Amboog-a-Lard	Amboog-a-Lard	k1gMnSc1
•	•	k?
Goon	Goon	k1gMnSc1
Moon	Moon	k1gMnSc1
•	•	k?
Loser	Loser	k1gMnSc1
•	•	k?
Satan	Satan	k1gMnSc1
on	on	k3xPp3gMnSc1
Fire	Fire	k1gNnPc7
•	•	k?
Rob	roba	k1gFnPc2
Zombie	Zombie	k1gFnSc1
•	•	k?
Dope	Dop	k1gFnSc2
•	•	k?
gODHEAD	gODHEAD	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30925	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
121603024	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1049	#num#	k4
6263	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
97119848	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
29749590	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
97119848	#num#	k4
</s>
<s>
↑	↑	k?
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biography	Biographa	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DATABAZEKNIH	DATABAZEKNIH	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhá	dlouhý	k2eAgFnSc1d1
trnitá	trnitý	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
pekla	peklo	k1gNnSc2
-	-	kIx~
Neil	Neil	k1gInSc1
Strauss	Strauss	k1gInSc1
|	|	kIx~
Databáze	databáze	k1gFnSc1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
www.databazeknih.cz	www.databazeknih.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Marilyn	Marilyn	k1gFnSc1
Manson	Manson	k1gMnSc1
<g/>
:	:	kIx,
‘	‘	k?
<g/>
I	i	k9
created	created	k1gMnSc1
a	a	k8xC
fake	fakat	k5eAaPmIp3nS
world	world	k6eAd1
because	because	k6eAd1
I	i	k9
didn	didn	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
like	like	k1gInSc1
the	the	k?
one	one	k?
I	i	k8xC
was	was	k?
living	living	k1gInSc1
in	in	k?
<g/>
’	’	k?
<g/>
.	.	kIx.
the	the	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-01-18	2015-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Marilyn	Marilyn	k1gFnSc4
Manson	Mansona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMDb	IMDb	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EDITORS	EDITORS	kA
<g/>
,	,	kIx,
History	Histor	k1gInPc7
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbine	Columbin	k1gInSc5
Shooting	Shooting	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HISTORY	HISTORY	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vymýval	vymývat	k5eAaImAgMnS
mi	já	k3xPp1nSc3
mozek	mozek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězda	Hvězda	k1gMnSc1
Westworld	Westworld	k1gMnSc1
viní	vinit	k5eAaImIp3nS
rockera	rocker	k1gMnSc4
Mansona	Manson	k1gMnSc4
ze	z	k7c2
zneužívání	zneužívání	k1gNnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-02	2021-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
