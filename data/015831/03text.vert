<s>
Würzburské	Würzburský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
</s>
<s>
Würzburské	Würzburský	k2eAgNnSc1d1
velkovévodstvíGroßherzogtum	velkovévodstvíGroßherzogtum	k1gNnSc1
Würzburg	Würzburg	k1gInSc1
</s>
<s>
←	←	k?
</s>
<s>
1805	#num#	k4
<g/>
–	–	k?
<g/>
1814	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Würzburské	Würzburský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Würzburg	Würzburg	k1gMnSc1
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Němci	Němec	k1gMnPc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
němčina	němčina	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
římskokatolické	římskokatolický	k2eAgFnSc3d1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
absolutní	absolutní	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
</s>
<s>
mateřskázemě	mateřskázemě	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Würzburské	Würzburský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Großherzogtum	Großherzogtum	k1gNnSc1
Würzburg	Würzburg	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Vircpurské	Vircpurský	k2eAgNnSc4d1
velkovévodství	velkovévodství	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
kolem	kolem	k7c2
města	město	k1gNnSc2
Würzburg	Würzburg	k1gInSc1
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
smlouvy	smlouva	k1gFnSc2
z	z	k7c2
Lunéville	Lunéville	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
světské	světský	k2eAgFnSc3d1
panství	panství	k1gNnSc1
würzburského	würzburský	k2eAgInSc2d1
biskupa	biskup	k1gInSc2
sekularizováno	sekularizován	k2eAgNnSc1d1
a	a	k8xC
věnováno	věnován	k2eAgNnSc1d1
Bavorskému	bavorský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgMnSc1
rok	rok	k1gInSc4
byl	být	k5eAaImAgMnS
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
kompenzován	kompenzován	k2eAgInSc1d1
za	za	k7c4
ztrátu	ztráta	k1gFnSc4
Toskánska	Toskánsko	k1gNnSc2
nově	nově	k6eAd1
zřízeným	zřízený	k2eAgNnPc3d1
Kurfiřtstvím	kurfiřtství	k1gNnPc3
salcburským	salcburský	k2eAgNnPc3d1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
ale	ale	k9
již	již	k6eAd1
roku	rok	k1gInSc2
1805	#num#	k4
získalo	získat	k5eAaPmAgNnS
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
tedy	tedy	k8xC
náhradou	náhrada	k1gFnSc7
získal	získat	k5eAaPmAgMnS
vévodství	vévodství	k1gNnSc4
würzburské	würzburský	k2eAgFnSc2d1
a	a	k8xC
Bavorsko	Bavorsko	k1gNnSc1
náhradou	náhrada	k1gFnSc7
obdrželo	obdržet	k5eAaPmAgNnS
Tyrolsko	Tyrolsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Ferdinandův	Ferdinandův	k2eAgInSc1d1
stát	stát	k1gInSc1
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
znám	znám	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Kurfiřtství	kurfiřtství	k1gNnSc1
würzburské	würzburský	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
rozpuštění	rozpuštění	k1gNnSc3
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
roku	rok	k1gInSc3
1806	#num#	k4
bylo	být	k5eAaImAgNnS
povýšeno	povýšit	k5eAaPmNgNnS
na	na	k7c6
velkovévodství	velkovévodství	k1gNnSc6
a	a	k8xC
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1810	#num#	k4
byl	být	k5eAaImAgMnS
k	k	k7c3
velkovévodství	velkovévodství	k1gNnSc3
připojen	připojen	k2eAgInSc4d1
Schweinfurt	Schweinfurt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
roku	rok	k1gInSc2
1813	#num#	k4
Ferdinand	Ferdinand	k1gMnSc1
zrušil	zrušit	k5eAaPmAgMnS
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
Napoleonem	napoleon	k1gInSc7
a	a	k8xC
roku	rok	k1gInSc2
1814	#num#	k4
bylo	být	k5eAaImAgNnS
velkovévodství	velkovévodství	k1gNnSc1
zrušeno	zrušit	k5eAaPmNgNnS
a	a	k8xC
začleněno	začleněn	k2eAgNnSc1d1
do	do	k7c2
Bavorska	Bavorsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Vídeňský	vídeňský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
Ferdinandovi	Ferdinand	k1gMnSc3
Toskánsko	Toskánsko	k1gNnSc1
a	a	k8xC
katolická	katolický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
Würzburg	Würzburg	k1gInSc4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1831	#num#	k4
již	již	k6eAd1
bez	bez	k7c2
světské	světský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Grand	grand	k1gMnSc1
Duchy	duch	k1gMnPc4
of	of	k?
Würzburg	Würzburg	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KÖBLER	KÖBLER	kA
<g/>
,	,	kIx,
Gerhard	Gerhard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historisches	Historisches	k1gInSc1
Lexikon	lexikon	k1gInSc4
der	drát	k5eAaImRp2nS
deutschen	deutschna	k1gFnPc2
Länder	Ländero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Munich	Munich	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gInSc1
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3406332900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
639	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Ivo	Ivo	k1gMnSc1
Striedinger	Striedinger	k1gMnSc1
<g/>
:	:	kIx,
Das	Das	k1gMnSc1
Großherzogtum	Großherzogtum	k1gNnSc1
Würzburg	Würzburg	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
ZBLG	ZBLG	kA
6	#num#	k4
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
250	#num#	k4
<g/>
–	–	k?
<g/>
256	#num#	k4
(	(	kIx(
<g/>
Digitalisat	Digitalisat	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Království	království	k1gNnSc1
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Württemberské	Württemberský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Saské	saský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Vestfálské	vestfálský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
*	*	kIx~
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
roku	rok	k1gInSc2
1808	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkovévodství	velkovévodství	k1gNnSc1
</s>
<s>
Bádenské	bádenský	k2eAgInPc4d1
<g/>
**	**	k?
<g/>
,	,	kIx,
velkovévodství	velkovévodství	k1gNnSc1
Berg	Berga	k1gFnPc2
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Hesensko-Darmstadtské	Hesensko-Darmstadtský	k2eAgFnPc1d1
<g/>
**	**	k?
<g/>
,	,	kIx,
Würzburské	Würzburský	k2eAgFnPc1d1
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Frankfurtské	frankfurtský	k2eAgFnSc2d1
<g/>
*	*	kIx~
<g/>
1	#num#	k4
Vévodství	vévodství	k1gNnPc2
</s>
<s>
Anhaltsko	Anhaltsko	k1gNnSc1
(	(	kIx(
<g/>
Bernburg	Bernburg	k1gInSc1
<g/>
,	,	kIx,
Dessau	Dessaa	k1gFnSc4
<g/>
,	,	kIx,
Köthen	Köthen	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lauenbursko	Lauenbursko	k1gNnSc1
<g/>
,	,	kIx,
Meklenbursko	Meklenbursko	k1gNnSc1
(	(	kIx(
<g/>
Střelicko	Střelicko	k1gNnSc1
<g/>
,	,	kIx,
Zvěřínsko	Zvěřínsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nasavské	Nasavský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
(	(	kIx(
<g/>
Nasavsko-Usigen	Nasavsko-Usigen	k1gInSc1
<g/>
,	,	kIx,
Nasavsko-Weilburg	Nasavsko-Weilburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oldenburské	oldenburský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Sasko	Sasko	k1gNnSc1
(	(	kIx(
<g/>
Gothajsko-Altenbursko	Gothajsko-Altenbursko	k1gNnSc1
<g/>
,	,	kIx,
Kobursko-Saalfeldsko	Kobursko-Saalfeldsko	k1gNnSc1
<g/>
,	,	kIx,
Meiningensko	Meiningensko	k1gNnSc1
<g/>
,	,	kIx,
Eisenašsko	Eisenašsko	k1gNnSc1
<g/>
,	,	kIx,
Výmarsko	Výmarsko	k1gNnSc1
,	,	kIx,
Výmarsko-eisenašsko	Výmarsko-eisenašsko	k1gNnSc1
<g/>
)	)	kIx)
Knížectví	knížectví	k1gNnSc1
</s>
<s>
Aschaffenburské	Aschaffenburský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
*	*	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Řezenské	řezenský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
*	*	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Hohenzollernsko	Hohenzollernsko	k1gNnSc1
(	(	kIx(
<g/>
Hechingen	Hechingen	k1gInSc1
<g/>
,	,	kIx,
Sigmaringen	Sigmaringen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Isenburg-Birstein	Isenburg-Birstein	k1gInSc1
<g/>
,	,	kIx,
Leyen	Leyen	k1gInSc1
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
,	,	kIx,
Lippe	Lipp	k1gMnSc5
<g/>
,	,	kIx,
Reuss	Reuss	k1gInSc1
(	(	kIx(
<g/>
Ebersdorf	Ebersdorf	k1gMnSc1
<g/>
,	,	kIx,
Greiz	Greiz	k1gMnSc1
<g/>
,	,	kIx,
Lobenstein	Lobenstein	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Schleiz	Schleiz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Salm	Salm	k1gInSc1
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Schaumburg-Lippe	Schaumburg-Lipp	k1gMnSc5
<g/>
,	,	kIx,
Schwarzbursko	Schwarzburska	k1gMnSc5
(	(	kIx(
<g/>
Rudolstadt	Rudolstadt	k1gInSc1
<g/>
,	,	kIx,
Sondershausen	Sondershausen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Waldeck	Waldeck	k1gInSc1
1	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1810	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1810	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
(	(	kIx(
<g/>
spojeno	spojit	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1811	#num#	k4
<g/>
,	,	kIx,
*	*	kIx~
vytvořeno	vytvořit	k5eAaPmNgNnS
Napoleonem	napoleon	k1gInSc7
<g/>
,	,	kIx,
**	**	k?
povýšeno	povýšen	k2eAgNnSc4d1
Napoleonem	Napoleon	k1gMnSc7
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4120102-4	4120102-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
150260572	#num#	k4
</s>
<s>
↑	↑	k?
U_	U_	k1gFnSc1
<g/>
@	@	kIx~
<g/>
_	_	kIx~
(	(	kIx(
<g/>
S	s	k7c7
<g/>
–	–	k?
<g/>
Z	z	k7c2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
