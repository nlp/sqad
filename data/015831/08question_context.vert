<s>
Würzburské	Würzburský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Großherzogtum	Großherzogtum	k1gNnSc1
Würzburg	Würzburg	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Vircpurské	Vircpurský	k2eAgNnSc4d1
velkovévodství	velkovévodství	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
kolem	kolem	k7c2
města	město	k1gNnSc2
Würzburg	Würzburg	k1gInSc1
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>