<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
žraloci	žralok	k1gMnPc1	žralok
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Otodontidae	Otodontidae	k1gFnPc2	Otodontidae
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
studenokrevné	studenokrevný	k2eAgNnSc4d1	studenokrevné
a	a	k8xC	a
megalodon	megalodon	k1gNnSc4	megalodon
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
jejich	jejich	k3xOp3gMnSc2	jejich
blízkého	blízký	k2eAgMnSc2d1	blízký
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
studenokrevný	studenokrevný	k2eAgMnSc1d1	studenokrevný
<g/>
.	.	kIx.	.
</s>
