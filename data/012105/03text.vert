<p>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
nebo	nebo	k8xC	nebo
ředitelka	ředitelka	k1gFnSc1	ředitelka
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
nějakou	nějaký	k3yIgFnSc4	nějaký
instituci	instituce	k1gFnSc4	instituce
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc4	její
část	část	k1gFnSc4	část
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
obvykle	obvykle	k6eAd1	obvykle
i	i	k8xC	i
nějakou	nějaký	k3yIgFnSc4	nějaký
větší	veliký	k2eAgFnSc4d2	veliký
skupinu	skupina	k1gFnSc4	skupina
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
náměstek	náměstek	k1gMnSc1	náměstek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
velkých	velký	k2eAgFnPc2d1	velká
společností	společnost	k1gFnPc2	společnost
i	i	k8xC	i
veřejných	veřejný	k2eAgFnPc2d1	veřejná
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
nejvýše	vysoce	k6eAd3	vysoce
postaveného	postavený	k2eAgMnSc4d1	postavený
ředitele	ředitel	k1gMnSc4	ředitel
užíváno	užíván	k2eAgNnSc1d1	užíváno
označení	označení	k1gNnSc1	označení
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	též	k6eAd1	též
ústřední	ústřední	k2eAgMnSc1d1	ústřední
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
jemu	on	k3xPp3gMnSc3	on
podřízení	podřízený	k2eAgMnPc1d1	podřízený
ředitelé	ředitel	k1gMnPc1	ředitel
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc1d1	různý
upřesňující	upřesňující	k2eAgInPc1d1	upřesňující
přívlastky	přívlastek	k1gInPc1	přívlastek
podle	podle	k7c2	podle
oborů	obor	k1gInPc2	obor
své	svůj	k3xOyFgFnSc2	svůj
působnosti	působnost	k1gFnSc2	působnost
např	např	kA	např
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
finanční	finanční	k2eAgMnSc1d1	finanční
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
ekonomický	ekonomický	k2eAgMnSc1d1	ekonomický
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
obchodní	obchodní	k2eAgMnSc1d1	obchodní
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
technický	technický	k2eAgMnSc1d1	technický
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
personální	personální	k2eAgMnSc1d1	personální
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
výrobní	výrobní	k2eAgMnSc1d1	výrobní
ředitel	ředitel	k1gMnSc1	ředitel
</s>
</p>
<p>
<s>
ředitel	ředitel	k1gMnSc1	ředitel
závodu	závod	k1gInSc2	závod
nebo	nebo	k8xC	nebo
ředitel	ředitel	k1gMnSc1	ředitel
pobočky	pobočka	k1gFnSc2	pobočka
apod.	apod.	kA	apod.
<g/>
Působnost	působnost	k1gFnSc1	působnost
všech	všecek	k3xTgMnPc2	všecek
ředitelů	ředitel	k1gMnPc2	ředitel
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
vymezena	vymezit	k5eAaPmNgFnS	vymezit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
platných	platný	k2eAgFnPc2d1	platná
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
statutu	statut	k1gInSc2	statut
příslušné	příslušný	k2eAgFnSc2d1	příslušná
organizace	organizace	k1gFnSc2	organizace
či	či	k8xC	či
instituce	instituce	k1gFnSc2	instituce
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgInSc2d1	jiný
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
organizačního	organizační	k2eAgInSc2d1	organizační
předpisu	předpis	k1gInSc2	předpis
statutární	statutární	k2eAgFnSc2d1	statutární
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
ministerstvech	ministerstvo	k1gNnPc6	ministerstvo
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgNnSc4d1	oficiální
funkční	funkční	k2eAgNnSc4d1	funkční
úřední	úřední	k2eAgNnSc4d1	úřední
zařazení	zařazení	k1gNnSc4	zařazení
užívá	užívat	k5eAaImIp3nS	užívat
označení	označení	k1gNnSc4	označení
ředitel	ředitel	k1gMnSc1	ředitel
odboru	odbor	k1gInSc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
podléhá	podléhat	k5eAaImIp3nS	podléhat
náměstkovi	náměstek	k1gMnSc3	náměstek
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
sekce	sekce	k1gFnSc2	sekce
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
správních	správní	k2eAgInPc6d1	správní
úřadech	úřad	k1gInPc6	úřad
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
ředitel	ředitel	k1gMnSc1	ředitel
sekce	sekce	k1gFnSc2	sekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ředitel	ředitel	k1gMnSc1	ředitel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
