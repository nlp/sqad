<s>
Ředitel	ředitel	k1gMnSc1	ředitel
nebo	nebo	k8xC	nebo
ředitelka	ředitelka	k1gFnSc1	ředitelka
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
nějakou	nějaký	k3yIgFnSc4	nějaký
instituci	instituce	k1gFnSc4	instituce
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc4	její
část	část	k1gFnSc4	část
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
obvykle	obvykle	k6eAd1	obvykle
i	i	k8xC	i
nějakou	nějaký	k3yIgFnSc4	nějaký
větší	veliký	k2eAgFnSc4d2	veliký
skupinu	skupina	k1gFnSc4	skupina
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
