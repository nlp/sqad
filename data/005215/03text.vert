<s>
Sargon	Sargon	k1gMnSc1	Sargon
Akkadský	Akkadský	k2eAgMnSc1d1	Akkadský
(	(	kIx(	(
<g/>
akkadsky	akkadsky	k6eAd1	akkadsky
Šarrukén	Šarrukén	k1gInSc1	Šarrukén
-	-	kIx~	-
"	"	kIx"	"
<g/>
Pravý	pravý	k2eAgMnSc1d1	pravý
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
cca	cca	kA	cca
2334	[number]	k4	2334
–	–	k?	–
2279	[number]	k4	2279
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
akkadský	akkadský	k2eAgMnSc1d1	akkadský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
říši	říše	k1gFnSc4	říše
na	na	k7c6	na
území	území	k1gNnSc6	území
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
–	–	k?	–
předtím	předtím	k6eAd1	předtím
zde	zde	k6eAd1	zde
existovaly	existovat	k5eAaImAgInP	existovat
jen	jen	k9	jen
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
původu	původ	k1gInSc3	původ
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nS	vážit
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
matka	matka	k1gFnSc1	matka
pustila	pustit	k5eAaPmAgFnS	pustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
košíku	košík	k1gInSc6	košík
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Mojžíše	Mojžíš	k1gMnPc4	Mojžíš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
krále	král	k1gMnSc2	král
Ur-Zababy	Ur-Zababa	k1gMnSc2	Ur-Zababa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
číšníkem	číšník	k1gMnSc7	číšník
<g/>
;	;	kIx,	;
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
legendy	legenda	k1gFnPc1	legenda
nevysvětlují	vysvětlovat	k5eNaImIp3nP	vysvětlovat
<g/>
.	.	kIx.	.
</s>
<s>
Sargon	Sargon	k1gMnSc1	Sargon
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
semitského	semitský	k2eAgMnSc4d1	semitský
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
sumerského	sumerský	k2eAgInSc2d1	sumerský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Akkad	Akkad	k1gInSc1	Akkad
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
království	království	k1gNnSc1	království
i	i	k8xC	i
národ	národ	k1gInSc1	národ
Akkadů	Akkad	k1gInPc2	Akkad
<g/>
.	.	kIx.	.
</s>
<s>
Uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
několik	několik	k4yIc4	několik
výbojů	výboj	k1gInPc2	výboj
proti	proti	k7c3	proti
okolním	okolní	k2eAgFnPc3d1	okolní
zemím	zem	k1gFnPc3	zem
–	–	k?	–
dobyl	dobýt	k5eAaPmAgInS	dobýt
většinu	většina	k1gFnSc4	většina
Sumeru	Sumer	k1gInSc2	Sumer
a	a	k8xC	a
Elamu	Elam	k1gInSc2	Elam
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnSc2	hranice
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
až	až	k9	až
k	k	k7c3	k
Perskému	perský	k2eAgInSc3d1	perský
zálivu	záliv	k1gInSc3	záliv
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
až	až	k9	až
k	k	k7c3	k
pohoří	pohoří	k1gNnSc3	pohoří
Libanon	Libanon	k1gInSc1	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
krutá	krutý	k2eAgFnSc1d1	krutá
–	–	k?	–
když	když	k8xS	když
dobyla	dobýt	k5eAaPmAgFnS	dobýt
nějaké	nějaký	k3yIgNnSc4	nějaký
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
strhli	strhnout	k5eAaPmAgMnP	strhnout
její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
pobili	pobít	k5eAaPmAgMnP	pobít
nebo	nebo	k8xC	nebo
zotročili	zotročit	k5eAaPmAgMnP	zotročit
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Akkad	Akkad	k1gInSc1	Akkad
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
kotvily	kotvit	k5eAaImAgFnP	kotvit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
lodě	loď	k1gFnPc1	loď
přivážející	přivážející	k2eAgFnSc4d1	přivážející
zboží	zboží	k1gNnSc3	zboží
až	až	k9	až
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
či	či	k8xC	či
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Sargon	Sargon	k1gMnSc1	Sargon
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vybírány	vybírat	k5eAaImNgFnP	vybírat
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
daně	daň	k1gFnPc1	daň
a	a	k8xC	a
tributy	tribut	k1gInPc1	tribut
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
sice	sice	k8xC	sice
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
blahobyt	blahobyt	k1gInSc4	blahobyt
centru	centr	k1gInSc2	centr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
četné	četný	k2eAgFnSc2d1	četná
vzpoury	vzpoura	k1gFnSc2	vzpoura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dovoz	dovoz	k1gInSc1	dovoz
nedostatkových	dostatkový	k2eNgFnPc2d1	nedostatková
surovin	surovina	k1gFnPc2	surovina
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
a	a	k8xC	a
reglementován	reglementovat	k5eAaBmNgInS	reglementovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zřízena	zřízen	k2eAgFnSc1d1	zřízena
byla	být	k5eAaImAgFnS	být
i	i	k9	i
stálá	stálý	k2eAgFnSc1d1	stálá
armáda	armáda	k1gFnSc1	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
5400	[number]	k4	5400
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
účinná	účinný	k2eAgFnSc1d1	účinná
byrokracie	byrokracie	k1gFnSc1	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Sargon	Sargon	k1gMnSc1	Sargon
Akkadský	Akkadský	k2eAgMnSc1d1	Akkadský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
užíval	užívat	k5eAaImAgInS	užívat
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
čtyř	čtyři	k4xCgFnPc2	čtyři
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
mezopotámském	mezopotámský	k2eAgNnSc6d1	mezopotámský
písemnictví	písemnictví	k1gNnSc6	písemnictví
prototypem	prototyp	k1gInSc7	prototyp
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
vládce	vládce	k1gMnSc4	vládce
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgInP	být
tradovány	tradovat	k5eAaImNgInP	tradovat
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisícíletí	tisícíletí	k1gNnSc2	tisícíletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
eposy	epos	k1gInPc1	epos
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnPc1d1	fiktivní
biografie	biografie	k1gFnPc1	biografie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
