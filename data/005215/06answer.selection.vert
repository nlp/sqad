<s>
Sargon	Sargon	k1gMnSc1	Sargon
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
semitského	semitský	k2eAgMnSc4d1	semitský
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
sumerského	sumerský	k2eAgInSc2d1	sumerský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Akkad	Akkad	k1gInSc1	Akkad
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
království	království	k1gNnSc1	království
i	i	k8xC	i
národ	národ	k1gInSc1	národ
Akkadů	Akkad	k1gInPc2	Akkad
<g/>
.	.	kIx.	.
</s>
