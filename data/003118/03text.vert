<s>
Chameleoni	chameleon	k1gMnPc1	chameleon
(	(	kIx(	(
<g/>
Chamaeleoninae	Chamaeleoninae	k1gInSc1	Chamaeleoninae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
ještěrů	ještěr	k1gMnPc2	ještěr
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
chameleonovitých	chameleonovitý	k2eAgMnPc2d1	chameleonovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
4	[number]	k4	4
rody	rod	k1gInPc7	rod
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
všem	všecek	k3xTgMnPc3	všecek
přísluší	příslušet	k5eAaImIp3nS	příslušet
české	český	k2eAgNnSc4d1	české
označení	označení	k1gNnSc4	označení
chameleon	chameleon	k1gMnSc1	chameleon
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
fámou	fáma	k1gFnSc7	fáma
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
chameleonů	chameleon	k1gMnPc2	chameleon
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
podle	podle	k7c2	podle
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
totiž	totiž	k9	totiž
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dívají	dívat	k5eAaImIp3nP	dívat
každým	každý	k3xTgNnSc7	každý
okem	oke	k1gNnSc7	oke
zvlášť	zvlášť	k6eAd1	zvlášť
(	(	kIx(	(
<g/>
monokulární	monokulární	k2eAgNnSc4d1	monokulární
vidění	vidění	k1gNnSc4	vidění
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vymrštitelný	vymrštitelný	k2eAgInSc1d1	vymrštitelný
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Chameleoni	chameleon	k1gMnPc1	chameleon
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
patrně	patrně	k6eAd1	patrně
koncem	koncem	k7c2	koncem
křídy	křída	k1gFnSc2	křída
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Agamidae	Agamida	k1gFnSc2	Agamida
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
fosilie	fosilie	k1gFnPc1	fosilie
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
z	z	k7c2	z
konce	konec	k1gInSc2	konec
mongolské	mongolský	k2eAgFnSc2d1	mongolská
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
z	z	k7c2	z
miocénu	miocén	k1gInSc2	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
žili	žít	k5eAaImAgMnP	žít
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
miocéních	miocéní	k2eAgInPc2d1	miocéní
nálezů	nález	k1gInPc2	nález
čelisti	čelist	k1gFnSc2	čelist
chameleona	chameleon	k1gMnSc2	chameleon
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druh	druh	k1gInSc4	druh
Chameleo	Chameleo	k6eAd1	Chameleo
caroliquarti	caroliquart	k1gMnPc1	caroliquart
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
v	v	k7c6	v
chebské	chebský	k2eAgFnSc6d1	Chebská
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Dolnice	Dolnice	k1gFnSc2	Dolnice
<g/>
.	.	kIx.	.
</s>
<s>
Chameleoni	chameleon	k1gMnPc1	chameleon
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
150	[number]	k4	150
druzích	druh	k1gInPc6	druh
ve	v	k7c6	v
190	[number]	k4	190
poddruzích	poddruh	k1gInPc6	poddruh
(	(	kIx(	(
<g/>
a	a	k8xC	a
stále	stále	k6eAd1	stále
jich	on	k3xPp3gMnPc2	on
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
šesti	šest	k4xCc2	šest
rodů	rod	k1gInPc2	rod
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
podčeledích	podčeleď	k1gFnPc6	podčeleď
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc4	rod
Chameleo	Chameleo	k1gNnSc1	Chameleo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
podrody	podrod	k1gInPc4	podrod
<g/>
:	:	kIx,	:
Chameleo	Chameleo	k1gMnSc1	Chameleo
a	a	k8xC	a
Trioceros	Triocerosa	k1gFnPc2	Triocerosa
<g/>
.	.	kIx.	.
</s>
<s>
Chameleoni	chameleon	k1gMnPc1	chameleon
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
mimo	mimo	k7c4	mimo
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
extrémně	extrémně	k6eAd1	extrémně
suché	suchý	k2eAgFnPc1d1	suchá
pouště	poušť	k1gFnPc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
výskyt	výskyt	k1gInSc1	výskyt
chameleonů	chameleon	k1gMnPc2	chameleon
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
chameleonů	chameleon	k1gMnPc2	chameleon
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
také	také	k9	také
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
druh	druh	k1gMnSc1	druh
<g/>
,	,	kIx,	,
chameleon	chameleon	k1gMnSc1	chameleon
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
kolem	kolem	k7c2	kolem
celého	celý	k2eAgNnSc2d1	celé
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
chameleony	chameleon	k1gMnPc4	chameleon
jsou	být	k5eAaImIp3nP	být
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
pouště	poušť	k1gFnPc1	poušť
a	a	k8xC	a
polopouště	polopoušť	k1gFnPc1	polopoušť
<g/>
,	,	kIx,	,
savany	savana	k1gFnPc1	savana
<g/>
,	,	kIx,	,
nížinné	nížinný	k2eAgInPc1d1	nížinný
i	i	k8xC	i
horské	horský	k2eAgInPc1d1	horský
deštné	deštný	k2eAgInPc1d1	deštný
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
alpínské	alpínský	k2eAgFnPc1d1	alpínská
louky	louka	k1gFnPc1	louka
až	až	k9	až
do	do	k7c2	do
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
4500	[number]	k4	4500
m.	m.	k?	m.
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
vysokých	vysoký	k2eAgInPc6d1	vysoký
keřích	keř	k1gInPc6	keř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
restrické	restrický	k2eAgInPc1d1	restrický
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
chameleona	chameleon	k1gMnSc2	chameleon
jsou	být	k5eAaImIp3nP	být
svou	svůj	k3xOyFgFnSc7	svůj
stavbou	stavba	k1gFnSc7	stavba
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejdokonalejší	dokonalý	k2eAgFnSc4d3	nejdokonalejší
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
živočišné	živočišný	k2eAgFnSc6d1	živočišná
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
samotné	samotný	k2eAgFnPc4d1	samotná
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
hlavy	hlava	k1gFnSc2	hlava
ukryty	ukryt	k2eAgInPc4d1	ukryt
ve	v	k7c6	v
dvojitých	dvojitý	k2eAgInPc6d1	dvojitý
víčkách	víčko	k1gNnPc6	víčko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
a	a	k8xC	a
víčko	víčko	k1gNnSc1	víčko
jsou	být	k5eAaImIp3nP	být
srostlé	srostlý	k2eAgInPc1d1	srostlý
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
očními	oční	k2eAgInPc7d1	oční
svaly	sval	k1gInPc7	sval
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
enormní	enormní	k2eAgFnSc1d1	enormní
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Chameleon	chameleon	k1gMnSc1	chameleon
může	moct	k5eAaImIp3nS	moct
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
sledovat	sledovat	k5eAaImF	sledovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
širokém	široký	k2eAgInSc6d1	široký
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
vertikálně	vertikálně	k6eAd1	vertikálně
může	moct	k5eAaImIp3nS	moct
otáčet	otáčet	k5eAaImF	otáčet
očima	oko	k1gNnPc7	oko
o	o	k7c6	o
120	[number]	k4	120
<g/>
°	°	k?	°
a	a	k8xC	a
horizontálně	horizontálně	k6eAd1	horizontálně
o	o	k7c4	o
190	[number]	k4	190
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hřbetem	hřbet	k1gInSc7	hřbet
se	se	k3xPyFc4	se
však	však	k9	však
nachází	nacházet	k5eAaImIp3nS	nacházet
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chameleon	chameleon	k1gMnSc1	chameleon
nedohlédne	dohlédnout	k5eNaPmIp3nS	dohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
každým	každý	k3xTgNnSc7	každý
okem	oke	k1gNnSc7	oke
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
neslučují	slučovat	k5eNaImIp3nP	slučovat
(	(	kIx(	(
<g/>
nesyntetizují	syntetizovat	k5eNaImIp3nP	syntetizovat
<g/>
)	)	kIx)	)
dohromady	dohromady	k6eAd1	dohromady
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mozek	mozek	k1gInSc1	mozek
je	on	k3xPp3gNnSc4	on
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
každý	každý	k3xTgMnSc1	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
chameleon	chameleon	k1gMnSc1	chameleon
kouká	koukat	k5eAaImIp3nS	koukat
každým	každý	k3xTgNnSc7	každý
okem	oke	k1gNnSc7	oke
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
chce	chtít	k5eAaImIp3nS	chtít
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
vystřelit	vystřelit	k5eAaPmF	vystřelit
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
zamíří	zamířit	k5eAaPmIp3nS	zamířit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
oběma	dva	k4xCgInPc7	dva
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
vidět	vidět	k5eAaImF	vidět
potravu	potrava	k1gFnSc4	potrava
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
až	až	k6eAd1	až
1000	[number]	k4	1000
m.	m.	k?	m.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
vlastnost	vlastnost	k1gFnSc1	vlastnost
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
chameleon	chameleon	k1gMnSc1	chameleon
pohnul	pohnout	k5eAaPmAgMnS	pohnout
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
objekt	objekt	k1gInSc4	objekt
dívat	dívat	k5eAaImF	dívat
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidí	vidět	k5eAaImIp3nS	vidět
dva	dva	k4xCgMnPc4	dva
cvrčky	cvrček	k1gMnPc4	cvrček
v	v	k7c6	v
zákrytu	zákryt	k1gInSc6	zákryt
za	za	k7c7	za
sebou	se	k3xPyFc7	se
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
pohybem	pohyb	k1gInSc7	pohyb
oka	oko	k1gNnSc2	oko
je	být	k5eAaImIp3nS	být
najednou	najednou	k6eAd1	najednou
vidí	vidět	k5eAaImIp3nS	vidět
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cvrčci	cvrček	k1gMnPc1	cvrček
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
dochází	docházet	k5eAaImIp3nS	docházet
posunem	posun	k1gInSc7	posun
tzv.	tzv.	kA	tzv.
uzlového	uzlový	k2eAgInSc2d1	uzlový
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
oko	oko	k1gNnSc1	oko
otáčí	otáčet	k5eAaImIp3nS	otáčet
zřítelnicí	zřítelnice	k1gFnSc7	zřítelnice
dolů	dol	k1gInPc2	dol
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
poloze	poloha	k1gFnSc6	poloha
mu	on	k3xPp3gNnSc3	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
kostěné	kostěný	k2eAgFnSc2d1	kostěná
destičky	destička	k1gFnSc2	destička
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
chameleona	chameleon	k1gMnSc2	chameleon
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
nepřehlédnutelných	přehlédnutelný	k2eNgInPc2d1	nepřehlédnutelný
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klidovém	klidový	k2eAgNnSc6d1	klidové
stadiu	stadion	k1gNnSc6	stadion
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgInSc1d1	dutý
a	a	k8xC	a
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
hltanu	hltan	k1gInSc6	hltan
a	a	k8xC	a
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
spatřena	spatřen	k2eAgFnSc1d1	spatřena
kořist	kořist	k1gFnSc1	kořist
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naplní	naplnit	k5eAaPmIp3nS	naplnit
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gMnSc1	jazyk
chameleon	chameleon	k1gMnSc1	chameleon
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
kontrakcí	kontrakce	k1gFnPc2	kontrakce
kruhového	kruhový	k2eAgNnSc2d1	kruhové
svalstva	svalstvo	k1gNnSc2	svalstvo
v	v	k7c6	v
řapíku	řapík	k1gInSc6	řapík
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
stahy	stah	k1gInPc1	stah
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
jazylku	jazylka	k1gFnSc4	jazylka
s	s	k7c7	s
hrudním	hrudní	k2eAgInSc7d1	hrudní
košem	koš	k1gInSc7	koš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
sliznice	sliznice	k1gFnSc1	sliznice
s	s	k7c7	s
lepkavým	lepkavý	k2eAgInSc7d1	lepkavý
adhézním	adhézní	k2eAgInSc7d1	adhézní
sekretem	sekret	k1gInSc7	sekret
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
jeho	jeho	k3xOp3gFnSc1	jeho
lepkavost	lepkavost	k1gFnSc1	lepkavost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
uchopení	uchopení	k1gNnSc6	uchopení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
africké	africký	k2eAgInPc1d1	africký
druhy	druh	k1gInPc1	druh
tuto	tento	k3xDgFnSc4	tento
sliznici	sliznice	k1gFnSc4	sliznice
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
malým	malý	k2eAgInSc7d1	malý
chápavým	chápavý	k2eAgInSc7d1	chápavý
prstíkem	prstík	k1gInSc7	prstík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
též	též	k9	též
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
uchopit	uchopit	k5eAaPmF	uchopit
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
vystřelení	vystřelení	k1gNnSc2	vystřelení
<g/>
,	,	kIx,	,
uchopení	uchopení	k1gNnSc2	uchopení
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
zpětného	zpětný	k2eAgNnSc2d1	zpětné
zatažení	zatažení	k1gNnSc2	zatažení
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
lovení	lovení	k1gNnSc2	lovení
úplně	úplně	k6eAd1	úplně
bezbranná	bezbranný	k2eAgFnSc1d1	bezbranná
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
často	často	k6eAd1	často
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
délku	délka	k1gFnSc4	délka
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
samotného	samotný	k2eAgMnSc4d1	samotný
chameleona	chameleon	k1gMnSc4	chameleon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
dvakrát	dvakrát	k6eAd1	dvakrát
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
chameleona	chameleon	k1gMnSc2	chameleon
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
někdy	někdy	k6eAd1	někdy
i	i	k9	i
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
u	u	k7c2	u
těla	tělo	k1gNnSc2	tělo
kulatého	kulatý	k2eAgInSc2d1	kulatý
průřezu	průřez	k1gInSc2	průřez
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
průřez	průřez	k1gInSc1	průřez
elipsovitý	elipsovitý	k2eAgInSc1d1	elipsovitý
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
stlačen	stlačen	k2eAgMnSc1d1	stlačen
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
zahrocený	zahrocený	k2eAgInSc1d1	zahrocený
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
chápavý	chápavý	k2eAgMnSc1d1	chápavý
a	a	k8xC	a
při	při	k7c6	při
šplhání	šplhání	k1gNnSc6	šplhání
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
končetina	končetina	k1gFnSc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ocas	ocas	k1gInSc1	ocas
utrhnut	utrhnout	k5eAaPmNgInS	utrhnout
nebo	nebo	k8xC	nebo
useknut	useknout	k5eAaPmNgInS	useknout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
regenerace	regenerace	k1gFnSc2	regenerace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
šplhání	šplhání	k1gNnSc6	šplhání
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
dodatečná	dodatečný	k2eAgFnSc1d1	dodatečná
pátá	pátý	k4xOgFnSc1	pátý
končetina	končetina	k1gFnSc1	končetina
a	a	k8xC	a
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
je	být	k5eAaImIp3nS	být
spirálovitě	spirálovitě	k6eAd1	spirálovitě
stočený	stočený	k2eAgInSc1d1	stočený
<g/>
.	.	kIx.	.
</s>
<s>
Zabarvení	zabarvení	k1gNnSc1	zabarvení
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
chameleonů	chameleon	k1gMnPc2	chameleon
se	s	k7c7	s
sebou	se	k3xPyFc7	se
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
podle	podle	k7c2	podle
zbarvení	zbarvení	k1gNnSc2	zbarvení
těla	tělo	k1gNnSc2	tělo
můžeme	moct	k5eAaImIp1nP	moct
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
chameleon	chameleon	k1gMnSc1	chameleon
cítí	cítit	k5eAaImIp3nS	cítit
nebo	nebo	k8xC	nebo
jakou	jaký	k3yIgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
domluvě	domluva	k1gFnSc3	domluva
mezi	mezi	k7c7	mezi
samotnými	samotný	k2eAgMnPc7d1	samotný
chameleony	chameleon	k1gMnPc7	chameleon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
si	se	k3xPyFc3	se
zbarvením	zbarvení	k1gNnSc7	zbarvení
dávají	dávat	k5eAaImIp3nP	dávat
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
hýbat	hýbat	k5eAaImF	hýbat
<g/>
)	)	kIx)	)
a	a	k8xC	a
namlouvání	namlouvání	k1gNnSc4	namlouvání
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
zbarvení	zbarvení	k1gNnPc2	zbarvení
se	se	k3xPyFc4	se
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nespáří	spářet	k5eNaImIp3nS	spářet
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
druhem	druh	k1gInSc7	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
netrvá	trvat	k5eNaImIp3nS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
3	[number]	k4	3
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Zabarvení	zabarvení	k1gNnSc1	zabarvení
nezáleží	záležet	k5eNaImIp3nS	záležet
ale	ale	k9	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
náladě	nálada	k1gFnSc6	nálada
a	a	k8xC	a
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
intenzitě	intenzita	k1gFnSc6	intenzita
a	a	k8xC	a
spektru	spektrum	k1gNnSc6	spektrum
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
denní	denní	k2eAgFnSc6d1	denní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
pestré	pestrý	k2eAgInPc1d1	pestrý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chameleoni	chameleon	k1gMnPc1	chameleon
mohou	moct	k5eAaImIp3nP	moct
vybarvit	vybarvit	k5eAaPmF	vybarvit
do	do	k7c2	do
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
barvy	barva	k1gFnSc2	barva
-	-	kIx~	-
každý	každý	k3xTgMnSc1	každý
druh	druh	k1gMnSc1	druh
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
omezenou	omezený	k2eAgFnSc4d1	omezená
paletu	paleta	k1gFnSc4	paleta
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
převládají	převládat	k5eAaImIp3nP	převládat
všechny	všechen	k3xTgInPc1	všechen
možné	možný	k2eAgInPc1d1	možný
odstíny	odstín	k1gInPc1	odstín
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgInPc3	některý
druhům	druh	k1gInPc3	druh
nedělá	dělat	k5eNaImIp3nS	dělat
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
vybarvit	vybarvit	k5eAaPmF	vybarvit
i	i	k9	i
do	do	k7c2	do
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnSc2d1	oranžová
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
fialové	fialový	k2eAgFnSc2d1	fialová
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
pokožky	pokožka	k1gFnSc2	pokožka
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
tzv.	tzv.	kA	tzv.
chromatocyty	chromatocyt	k1gInPc4	chromatocyt
umístěné	umístěný	k2eAgInPc4d1	umístěný
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
vrstvách	vrstva	k1gFnPc6	vrstva
v	v	k7c6	v
podkožní	podkožní	k2eAgFnSc6d1	podkožní
tkáni	tkáň	k1gFnSc6	tkáň
-	-	kIx~	-
první	první	k4xOgFnSc1	první
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
žlutými	žlutý	k2eAgFnPc7d1	žlutá
a	a	k8xC	a
červenými	červený	k2eAgFnPc7d1	červená
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
vrstva	vrstva	k1gFnSc1	vrstva
modrými	modrý	k2eAgFnPc7d1	modrá
a	a	k8xC	a
bílými	bílý	k2eAgFnPc7d1	bílá
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
vrstva	vrstva	k1gFnSc1	vrstva
černými	černý	k2eAgFnPc7d1	černá
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
buňky	buňka	k1gFnPc4	buňka
mají	mít	k5eAaImIp3nP	mít
výběžky	výběžek	k1gInPc1	výběžek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
nebo	nebo	k8xC	nebo
mění	měnit	k5eAaImIp3nP	měnit
svoji	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
oddalují	oddalovat	k5eAaImIp3nP	oddalovat
od	od	k7c2	od
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
průhledná	průhledný	k2eAgFnSc1d1	průhledná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
tyto	tento	k3xDgFnPc4	tento
barevné	barevný	k2eAgFnPc4d1	barevná
buňky	buňka	k1gFnPc4	buňka
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
chameleon	chameleon	k1gMnSc1	chameleon
naštvaný	naštvaný	k2eAgMnSc1d1	naštvaný
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
nafukuje	nafukovat	k5eAaImIp3nS	nafukovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
okolí	okolí	k1gNnSc4	okolí
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
se	se	k3xPyFc4	se
do	do	k7c2	do
nejtmavších	tmavý	k2eAgInPc2d3	nejtmavší
odstínů	odstín	k1gInPc2	odstín
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
chameleon	chameleon	k1gMnSc1	chameleon
spí	spát	k5eAaImIp3nS	spát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
uvolněné	uvolněný	k2eAgNnSc1d1	uvolněné
a	a	k8xC	a
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
se	se	k3xPyFc4	se
do	do	k7c2	do
nejsvětlejšího	světlý	k2eAgInSc2d3	nejsvětlejší
odstínu	odstín	k1gInSc2	odstín
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhřívání	vyhřívání	k1gNnSc6	vyhřívání
se	se	k3xPyFc4	se
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vybarvuje	vybarvovat	k5eAaImIp3nS	vybarvovat
do	do	k7c2	do
hnědých	hnědý	k2eAgInPc2d1	hnědý
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
hnědá	hnědý	k2eAgFnSc1d1	hnědá
přijímá	přijímat	k5eAaImIp3nS	přijímat
daleko	daleko	k6eAd1	daleko
lépe	dobře	k6eAd2	dobře
teplo	teplo	k1gNnSc1	teplo
než	než	k8xS	než
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
namlouvání	namlouvání	k1gNnSc6	namlouvání
se	se	k3xPyFc4	se
sameček	sameček	k1gMnSc1	sameček
před	před	k7c7	před
samičkou	samička	k1gFnSc7	samička
"	"	kIx"	"
<g/>
naparuje	naparovat	k5eAaImIp3nS	naparovat
<g/>
"	"	kIx"	"
svým	své	k1gNnSc7	své
krásným	krásný	k2eAgNnSc7d1	krásné
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
vybarvuje	vybarvovat	k5eAaImIp3nS	vybarvovat
se	se	k3xPyFc4	se
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
samičku	samička	k1gFnSc4	samička
uchvátil	uchvátit	k5eAaPmAgMnS	uchvátit
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
uzpůsobeny	uzpůsoben	k2eAgFnPc1d1	uzpůsobena
životu	život	k1gInSc2	život
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
a	a	k8xC	a
keřích	keř	k1gInPc6	keř
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
prsty	prst	k1gInPc4	prst
srostlé	srostlý	k2eAgInPc4d1	srostlý
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
kleštičky	kleštička	k1gFnPc1	kleštička
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
pevné	pevný	k2eAgNnSc4d1	pevné
obejmutí	obejmutí	k1gNnSc4	obejmutí
větvičky	větvička	k1gFnSc2	větvička
a	a	k8xC	a
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
stabilitu	stabilita	k1gFnSc4	stabilita
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
noze	noha	k1gFnSc6	noha
mají	mít	k5eAaImIp3nP	mít
srostlé	srostlý	k2eAgFnPc1d1	srostlá
vždy	vždy	k6eAd1	vždy
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
prsty	prst	k1gInPc4	prst
až	až	k9	až
po	po	k7c4	po
předposlední	předposlední	k2eAgInSc4d1	předposlední
článek	článek	k1gInSc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
dva	dva	k4xCgInPc4	dva
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohou	noha	k1gFnPc6	noha
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
chameleonovi	chameleon	k1gMnSc3	chameleon
stejné	stejný	k2eAgNnSc1d1	stejné
rozložení	rozložení	k1gNnSc1	rozložení
sil	síla	k1gFnPc2	síla
při	při	k7c6	při
uchycení	uchycení	k1gNnSc6	uchycení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
celkově	celkově	k6eAd1	celkově
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
štíhlé	štíhlý	k2eAgFnPc1d1	štíhlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
a	a	k8xC	a
enormně	enormně	k6eAd1	enormně
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
v	v	k7c6	v
ramenním	ramenní	k2eAgInSc6d1	ramenní
a	a	k8xC	a
kyčelním	kyčelní	k2eAgInSc6d1	kyčelní
kloubu	kloub	k1gInSc6	kloub
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
také	také	k6eAd1	také
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
namlouvání	namlouvání	k1gNnSc6	namlouvání
se	se	k3xPyFc4	se
chameleon	chameleon	k1gMnSc1	chameleon
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
ke	k	k7c3	k
každému	každý	k3xTgMnSc3	každý
jinému	jiný	k2eAgMnSc3d1	jiný
chameleonovi	chameleon	k1gMnSc3	chameleon
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
potká	potkat	k5eAaPmIp3nS	potkat
-	-	kIx~	-
odstrašuje	odstrašovat	k5eAaImIp3nS	odstrašovat
ho	on	k3xPp3gInSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samičku	samička	k1gFnSc4	samička
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
svoje	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
vybarvovat	vybarvovat	k5eAaImF	vybarvovat
a	a	k8xC	a
ukazovat	ukazovat	k5eAaImF	ukazovat
svoji	svůj	k3xOyFgFnSc4	svůj
krásnou	krásný	k2eAgFnSc4d1	krásná
kresbu	kresba	k1gFnSc4	kresba
a	a	k8xC	a
trhavými	trhavý	k2eAgInPc7d1	trhavý
pohyby	pohyb	k1gInPc7	pohyb
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
samičce	samička	k1gFnSc3	samička
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhu	druh	k1gInSc2	druh
Chameleo	Chameleo	k6eAd1	Chameleo
Jacksonii	Jacksonie	k1gFnSc4	Jacksonie
toto	tento	k3xDgNnSc1	tento
namlouvání	namlouvání	k1gNnSc1	namlouvání
bývá	bývat	k5eAaImIp3nS	bývat
doplněno	doplnit	k5eAaPmNgNnS	doplnit
koulením	koulení	k1gNnSc7	koulení
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
samička	samička	k1gFnSc1	samička
nevybarví	vybarvit	k5eNaPmIp3nS	vybarvit
do	do	k7c2	do
tmavých	tmavý	k2eAgFnPc2d1	tmavá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
neschopnost	neschopnost	k1gFnSc1	neschopnost
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
nebo	nebo	k8xC	nebo
nezájem	nezájem	k1gInSc4	nezájem
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ji	on	k3xPp3gFnSc4	on
sameček	sameček	k1gMnSc1	sameček
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
všude	všude	k6eAd1	všude
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
tam	tam	k6eAd1	tam
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
kopulaci	kopulace	k1gFnSc4	kopulace
<g/>
.	.	kIx.	.
</s>
<s>
Kopulace	kopulace	k1gFnSc1	kopulace
samotná	samotný	k2eAgFnSc1d1	samotná
trvá	trvat	k5eAaImIp3nS	trvat
tak	tak	k9	tak
10	[number]	k4	10
-	-	kIx~	-
30	[number]	k4	30
min	mina	k1gFnPc2	mina
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zvedá	zvedat	k5eAaImIp3nS	zvedat
samiččina	samiččin	k2eAgFnSc1d1	samiččin
agresivita	agresivita	k1gFnSc1	agresivita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
vzdálení	vzdálení	k1gNnSc3	vzdálení
se	se	k3xPyFc4	se
samečka	sameček	k1gMnSc2	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
živorodé	živorodý	k2eAgFnPc1d1	živorodá
<g/>
.	.	kIx.	.
</s>
<s>
Chameleon	chameleon	k1gMnSc1	chameleon
jemenský	jemenský	k2eAgMnSc1d1	jemenský
Chameleon	chameleon	k1gMnSc1	chameleon
pardálí	pardálí	k2eAgMnSc1d1	pardálí
Chameleon	chameleon	k1gMnSc1	chameleon
obecný	obecný	k2eAgMnSc1d1	obecný
"	"	kIx"	"
<g/>
mini	mini	k2eAgMnSc1d1	mini
chameleon	chameleon	k1gMnSc1	chameleon
<g/>
"	"	kIx"	"
Chameleon	chameleon	k1gMnSc1	chameleon
Jacksonův	Jacksonův	k2eAgMnSc1d1	Jacksonův
Chameleon	chameleon	k1gMnSc1	chameleon
pestrobarevný	pestrobarevný	k2eAgInSc4d1	pestrobarevný
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chameleoni	chameleon	k1gMnPc1	chameleon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chameleon	chameleon	k1gMnSc1	chameleon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
