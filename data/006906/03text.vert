<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Plevna	Plevno	k1gNnSc2	Plevno
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
akce	akce	k1gFnSc1	akce
během	během	k7c2	během
rusko-turecké	ruskourecký	k2eAgFnSc2d1	rusko-turecká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Spojená	spojený	k2eAgFnSc1d1	spojená
ruská	ruský	k2eAgFnSc1d1	ruská
a	a	k8xC	a
rumunská	rumunský	k2eAgNnPc1d1	rumunské
vojska	vojsko	k1gNnPc1	vojsko
oblehla	oblehnout	k5eAaPmAgNnP	oblehnout
město	město	k1gNnSc4	město
Plevno	Plevno	k1gNnSc1	Plevno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bránila	bránit	k5eAaImAgFnS	bránit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
trvalo	trvat	k5eAaImAgNnS	trvat
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Zdrželo	zdržet	k5eAaPmAgNnS	zdržet
hlavní	hlavní	k2eAgInSc4d1	hlavní
ruský	ruský	k2eAgInSc4d1	ruský
postup	postup	k1gInSc4	postup
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
Plevno	Plevno	k1gNnSc4	Plevno
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
000	[number]	k4	000
osmanských	osmanský	k2eAgMnPc2d1	osmanský
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Plevna	Plevno	k1gNnSc2	Plevno
vzato	vzít	k5eAaPmNgNnS	vzít
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Plevna	Plevno	k1gNnSc2	Plevno
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
války	válka	k1gFnSc2	válka
k	k	k7c3	k
přelomu	přelom	k1gInSc3	přelom
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
se	se	k3xPyFc4	se
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
postavení	postavení	k1gNnSc1	postavení
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
válku	válka	k1gFnSc4	válka
jí	on	k3xPp3gFnSc2	on
znovu	znovu	k6eAd1	znovu
vypovědělo	vypovědět	k5eAaPmAgNnS	vypovědět
i	i	k9	i
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Rychlík	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106-497-1	[number]	k4	80-7106-497-1
</s>
