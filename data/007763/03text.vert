<s>
Adam	Adam	k1gMnSc1	Adam
František	František	k1gMnSc1	František
Kollár	Kollár	k1gMnSc1	Kollár
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1718	[number]	k4	1718
<g/>
,	,	kIx,	,
Terchová	Terchová	k1gFnSc1	Terchová
<g/>
,	,	kIx,	,
Uhersko	Uhersko	k1gNnSc1	Uhersko
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovensko-rakouský	slovenskoakouský	k2eAgMnSc1d1	slovensko-rakouský
osvícenský	osvícenský	k2eAgMnSc1d1	osvícenský
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
knihovník	knihovník	k1gMnSc1	knihovník
dvorní	dvorní	k2eAgFnSc2d1	dvorní
knihovny	knihovna	k1gFnSc2	knihovna
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
moudrost	moudrost	k1gFnSc4	moudrost
a	a	k8xC	a
rozhled	rozhled	k1gInSc4	rozhled
ho	on	k3xPp3gMnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
slovenským	slovenský	k2eAgMnSc7d1	slovenský
Sokratem	Sokrates	k1gMnSc7	Sokrates
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
a	a	k8xC	a
Banské	banský	k2eAgFnSc6d1	Banská
Štiavnici	Štiavnica	k1gFnSc6	Štiavnica
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
řádu	řád	k1gInSc2	řád
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
před	před	k7c7	před
vysvěcením	vysvěcení	k1gNnSc7	vysvěcení
<g/>
.	.	kIx.	.
</s>
<s>
Ovládal	ovládat	k5eAaImAgMnS	ovládat
mnoho	mnoho	k4c4	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
slovenčiny	slovenčina	k1gFnSc2	slovenčina
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
maďarštiny	maďarština	k1gFnSc2	maďarština
a	a	k8xC	a
klasických	klasický	k2eAgInPc2d1	klasický
jazyků	jazyk	k1gInPc2	jazyk
také	také	k9	také
turečtinu	turečtina	k1gFnSc4	turečtina
<g/>
,	,	kIx,	,
perštinu	perština	k1gFnSc4	perština
a	a	k8xC	a
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
dvorní	dvorní	k2eAgFnSc6d1	dvorní
knihovně	knihovna	k1gFnSc6	knihovna
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
blízkým	blízký	k2eAgMnSc7d1	blízký
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
vlivného	vlivný	k2eAgMnSc2d1	vlivný
dvorního	dvorní	k2eAgMnSc2d1	dvorní
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
diplomata	diplomat	k1gMnSc2	diplomat
a	a	k8xC	a
knihovníka	knihovník	k1gMnSc2	knihovník
G.	G.	kA	G.
van	vana	k1gFnPc2	vana
Swietena	Swieten	k2eAgFnSc1d1	Swietena
<g/>
,	,	kIx,	,
poradcem	poradce	k1gMnSc7	poradce
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
panovnice	panovnice	k1gFnSc2	panovnice
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
různých	různý	k2eAgFnPc2d1	různá
komisí	komise	k1gFnPc2	komise
a	a	k8xC	a
dvorním	dvorní	k2eAgMnSc7d1	dvorní
rádcem	rádce	k1gMnSc7	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
pramenů	pramen	k1gInPc2	pramen
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
k	k	k7c3	k
turecké	turecký	k2eAgFnSc3d1	turecká
expanzi	expanze	k1gFnSc3	expanze
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
se	se	k3xPyFc4	se
jako	jako	k9	jako
znalec	znalec	k1gMnSc1	znalec
právních	právní	k2eAgFnPc2d1	právní
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
,	,	kIx,	,
slovanských	slovanský	k2eAgInPc2d1	slovanský
a	a	k8xC	a
orientálních	orientální	k2eAgInPc2d1	orientální
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
jako	jako	k9	jako
editor	editor	k1gInSc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
založit	založit	k5eAaPmF	založit
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
společnost	společnost	k1gFnSc4	společnost
Societas	Societas	k1gInSc1	Societas
literaria	literarium	k1gNnSc2	literarium
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
však	však	k9	však
nezískal	získat	k5eNaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
anonymně	anonymně	k6eAd1	anonymně
vydávat	vydávat	k5eAaImF	vydávat
čtrnáctideník	čtrnáctideník	k1gInSc4	čtrnáctideník
Privilegierte	Privilegiert	k1gInSc5	Privilegiert
Anzeigen	Anzeigen	k1gInSc1	Anzeigen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neměl	mít	k5eNaImAgInS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
návrzích	návrh	k1gInPc6	návrh
požadoval	požadovat	k5eAaImAgMnS	požadovat
zrušení	zrušení	k1gNnSc4	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
zdanění	zdanění	k1gNnSc2	zdanění
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stará	starý	k2eAgFnSc1d1	stará
feudální	feudální	k2eAgFnSc1d1	feudální
společnost	společnost	k1gFnSc1	společnost
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zásadní	zásadní	k2eAgFnPc4d1	zásadní
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
F.	F.	kA	F.
Kollár	Kollár	k1gMnSc1	Kollár
svými	svůj	k3xOyFgMnPc7	svůj
názory	názor	k1gInPc4	názor
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
školní	školní	k2eAgFnPc4d1	školní
reformy	reforma	k1gFnPc4	reforma
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Projevil	projevit	k5eAaPmAgMnS	projevit
se	se	k3xPyFc4	se
jako	jako	k9	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
schvaloval	schvalovat	k5eAaImAgInS	schvalovat
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
občanskou	občanský	k2eAgFnSc4d1	občanská
rovnost	rovnost	k1gFnSc4	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlechta	šlechta	k1gFnSc1	šlechta
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
posílení	posílení	k1gNnSc4	posílení
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
dílo	dílo	k1gNnSc4	dílo
De	De	k?	De
originibus	originibus	k1gInSc1	originibus
et	et	k?	et
usu	usus	k1gInSc2	usus
perpetuo	perpetuo	k1gMnSc1	perpetuo
potestatis	potestatis	k1gFnSc2	potestatis
legilatoriae	legilatoriae	k1gFnSc1	legilatoriae
circa	circa	k1gFnSc1	circa
sacra	sacra	k1gFnSc1	sacra
apost	apost	k1gFnSc1	apost
<g/>
.	.	kIx.	.
regnum	regnum	k1gInSc1	regnum
Ungariae	Ungaria	k1gInSc2	Ungaria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kollárovo	Kollárův	k2eAgNnSc1d1	Kollárovo
vystoupení	vystoupení	k1gNnSc1	vystoupení
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
privilegované	privilegovaný	k2eAgInPc4d1	privilegovaný
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
veřejně	veřejně	k6eAd1	veřejně
spáleno	spálit	k5eAaPmNgNnS	spálit
na	na	k7c6	na
bratislavském	bratislavský	k2eAgNnSc6d1	Bratislavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
studium	studium	k1gNnSc4	studium
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
použil	použít	k5eAaPmAgInS	použít
pojem	pojem	k1gInSc4	pojem
etnologie	etnologie	k1gFnSc2	etnologie
<g/>
.	.	kIx.	.
1761	[number]	k4	1761
<g/>
/	/	kIx~	/
<g/>
1762	[number]	k4	1762
–	–	k?	–
Analecta	Analect	k1gInSc2	Analect
monumentorum	monumentorum	k1gInSc1	monumentorum
omnis	omnis	k1gFnSc2	omnis
aevi	aev	k1gFnSc2	aev
Vindobonensia	Vindobonensia	k1gFnSc1	Vindobonensia
<g/>
,	,	kIx,	,
I-II	I-II	k1gFnSc1	I-II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
sbírka	sbírka	k1gFnSc1	sbírka
dokumentů	dokument	k1gInPc2	dokument
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
1762	[number]	k4	1762
–	–	k?	–
Casp	Casp	k1gMnSc1	Casp
<g/>
.	.	kIx.	.
</s>
<s>
Ursini	Ursin	k1gMnPc1	Ursin
Velii	Velie	k1gFnSc6	Velie
de	de	k?	de
bello	belnout	k5eAaPmAgNnS	belnout
Pannonico	Pannonico	k6eAd1	Pannonico
libri	libri	k6eAd1	libri
decem	decem	k6eAd1	decem
cum	cum	k?	cum
adnotationibus	adnotationibus	k1gInSc1	adnotationibus
et	et	k?	et
appendice	appendice	k1gFnSc2	appendice
critico	critico	k6eAd1	critico
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Deset	deset	k4xCc1	deset
knih	kniha	k1gFnPc2	kniha
C.	C.	kA	C.
Ursínyho	Ursíny	k1gMnSc2	Ursíny
o	o	k7c6	o
uherské	uherský	k2eAgFnSc6d1	uherská
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
a	a	k8xC	a
kritickým	kritický	k2eAgInSc7d1	kritický
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
1763	[number]	k4	1763
–	–	k?	–
Nicolai	Nicola	k1gFnSc2	Nicola
Olahi	Olahi	k1gNnSc2	Olahi
<g/>
...	...	k?	...
Hungaria	Hungarium	k1gNnSc2	Hungarium
et	et	k?	et
Attila	Attila	k1gMnSc1	Attila
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
<g />
.	.	kIx.	.
</s>
<s>
díla	dílo	k1gNnPc1	dílo
od	od	k7c2	od
Mikuláše	mikuláš	k1gInSc2	mikuláš
Oláha	Oláha	k1gFnSc1	Oláha
1764	[number]	k4	1764
–	–	k?	–
De	De	k?	De
originibus	originibus	k1gInSc1	originibus
et	et	k?	et
usu	usus	k1gInSc2	usus
perpetuo	perpetuo	k1gMnSc1	perpetuo
potestatis	potestatis	k1gFnSc2	potestatis
legislatoriae	legislatoriae	k1gFnSc1	legislatoriae
circa	circa	k1gFnSc1	circa
sacra	sacra	k1gFnSc1	sacra
apost	apost	k1gFnSc1	apost
<g/>
.	.	kIx.	.
regnum	regnum	k1gInSc1	regnum
Ungariae	Ungaria	k1gInSc2	Ungaria
(	(	kIx(	(
<g/>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
stálém	stálý	k2eAgNnSc6d1	stálé
používání	používání	k1gNnSc6	používání
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
církevních	církevní	k2eAgFnPc6d1	církevní
věcech	věc	k1gFnPc6	věc
apoštolských	apoštolský	k2eAgMnPc2d1	apoštolský
králů	král	k1gMnPc2	král
uherských	uherský	k2eAgMnPc2d1	uherský
<g/>
)	)	kIx)	)
1769	[number]	k4	1769
–	–	k?	–
De	De	k?	De
ortu	ort	k1gInSc2	ort
<g/>
,	,	kIx,	,
progressu	progress	k1gInSc2	progress
et	et	k?	et
incolatu	incolat	k2eAgFnSc4d1	incolat
nationis	nationis	k1gFnSc4	nationis
Ruthenicae	Ruthenicae	k1gNnSc2	Ruthenicae
in	in	k?	in
Hungaria	Hungarium	k1gNnSc2	Hungarium
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc6	rozšíření
a	a	k8xC	a
osazení	osazení	k1gNnSc6	osazení
rusínské	rusínský	k2eAgFnSc2d1	rusínská
národnosti	národnost	k1gFnSc2	národnost
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
dějiny	dějiny	k1gFnPc1	dějiny
uherských	uherský	k2eAgInPc2d1	uherský
Rusínů	rusín	k1gInPc2	rusín
1772	[number]	k4	1772
–	–	k?	–
Jurium	Jurium	k1gNnSc1	Jurium
Hungariae	Hungaria	k1gInSc2	Hungaria
in	in	k?	in
Russiam	Russiam	k1gInSc1	Russiam
minorem	minor	k1gInSc7	minor
et	et	k?	et
Podoliam	Podoliam	k1gInSc1	Podoliam
<g/>
,	,	kIx,	,
Bohemiaeque	Bohemiaeque	k1gInSc1	Bohemiaeque
in	in	k?	in
Osvicensem	Osvicens	k1gInSc7	Osvicens
et	et	k?	et
Zatoriensem	Zatoriens	k1gInSc7	Zatoriens
ducatus	ducatus	k1gMnSc1	ducatus
explicatio	explicatio	k1gMnSc1	explicatio
(	(	kIx(	(
<g/>
Výklad	výklad	k1gInSc1	výklad
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
Uher	Uhry	k1gFnPc2	Uhry
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Rusi	Rus	k1gFnSc6	Rus
a	a	k8xC	a
Podolii	Podolie	k1gFnSc6	Podolie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
i	i	k9	i
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
Osvicenském	Osvicenský	k2eAgNnSc6d1	Osvicenský
a	a	k8xC	a
Zatorském	Zatorský	k2eAgNnSc6d1	Zatorský
hrabství	hrabství	k1gNnSc6	hrabství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
;	;	kIx,	;
také	také	k9	také
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
1777	[number]	k4	1777
–	–	k?	–
Ratio	Ratio	k1gNnSc1	Ratio
educationis	educationis	k1gFnSc1	educationis
(	(	kIx(	(
<g/>
Výchovný	výchovný	k2eAgInSc1d1	výchovný
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagogické	pedagogický	k2eAgFnPc1d1	pedagogická
práce	práce	k1gFnPc1	práce
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
školní	školní	k2eAgFnSc4d1	školní
reformu	reforma	k1gFnSc4	reforma
1783	[number]	k4	1783
–	–	k?	–
Historiae	Historia	k1gInSc2	Historia
jurisque	jurisque	k1gFnSc4	jurisque
publici	publice	k1gFnSc4	publice
regni	regn	k1gMnPc1	regn
Ungariae	Ungariae	k1gNnSc2	Ungariae
amoenitates	amoenitatesa	k1gFnPc2	amoenitatesa
<g/>
,	,	kIx,	,
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zajímavosti	zajímavost	k1gFnSc3	zajímavost
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
veřejného	veřejný	k2eAgNnSc2d1	veřejné
práva	právo	k1gNnSc2	právo
uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
</s>
