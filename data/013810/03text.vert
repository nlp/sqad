<s>
FC	FC	kA
Koper	kopra	k1gFnPc2
</s>
<s>
FC	FC	kA
Luka	luka	k1gNnPc4
Koper	kopra	k1gFnPc2
Země	země	k1gFnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Koper	kopra	k1gFnPc2
Založen	založen	k2eAgMnSc1d1
</s>
<s>
1955	#num#	k4
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Prva	Prva	k1gFnSc1
slovenska	slovensko	k1gNnSc2
nogometna	nogometen	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Stadion	stadion	k1gInSc1
</s>
<s>
Bonifika	Bonifika	k1gFnSc1
Stadium	stadium	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Vedení	vedení	k1gNnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Miran	Miran	k1gMnSc1
Srebrnič	Srebrnič	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Koper	kopra	k1gFnPc2
je	být	k5eAaImIp3nS
slovinský	slovinský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
z	z	k7c2
města	město	k1gNnSc2
Koper	kopra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1955	#num#	k4
sloučením	sloučení	k1gNnSc7
klubů	klub	k1gInPc2
Aurora	Aurora	k1gFnSc1
Koper	kopra	k1gFnPc2
a	a	k8xC
Meduza	Meduza	k1gFnSc1
Koper	kopra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesl	nést	k5eAaImAgInS
tehdy	tehdy	k6eAd1
název	název	k1gInSc1
NK	NK	kA
Koper	kopra	k1gFnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c6
FC	FC	kA
Koper	kopra	k1gFnPc2
Capodistria	Capodistrium	k1gNnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2002	#num#	k4
na	na	k7c6
FC	FC	kA
Koper	kopra	k1gFnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2003	#num#	k4
na	na	k7c6
FC	FC	kA
Anet	Aneta	k1gFnPc2
Koper	kopra	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
2008	#num#	k4
na	na	k7c6
FC	FC	kA
Luka	luka	k1gNnPc1
Koper	kopra	k1gFnPc2
podle	podle	k7c2
hlavního	hlavní	k2eAgMnSc2d1
sponzora	sponzor	k1gMnSc2
-	-	kIx~
slovinského	slovinský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
Koper	kopra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
slovinské	slovinský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
slovinského	slovinský	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
slovinského	slovinský	k2eAgInSc2d1
Superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledky	výsledek	k1gInPc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
účinkování	účinkování	k1gNnSc1
v	v	k7c6
Intertoto	Intertota	k1gFnSc5
Cupu	cup	k1gInSc2
2002	#num#	k4
a	a	k8xC
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Kolo	kolo	k1gNnSc1
</s>
<s>
Soupeř	soupeř	k1gMnSc1
</s>
<s>
Skóre	skóre	k1gNnSc1
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Litex	Litex	k1gInSc1
Loveč	Loveč	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
NK	NK	kA
Široki	Širok	k1gFnPc1
Brijeg	Brijeg	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Vllaznia	Vllaznium	k1gNnPc4
Shkodër	Shkodër	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Dinamo	Dinamo	k6eAd1
Zagreb	Zagreb	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Šachter	Šachter	k1gInSc1
Karagandy	Karaganda	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
FK	FK	kA
Čelik	Čelik	k1gMnSc1
Nikšić	Nikšić	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Neftçi	Neftçi	k1gNnSc1
Baku	Baku	k1gNnSc2
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovenia	Slovenium	k1gNnPc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Champions	Champions	k1gInSc1
<g/>
,	,	kIx,
rsssf	rsssf	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Slovenia	Slovenium	k1gNnSc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finals	k1gInSc1
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Slovenia	Slovenium	k1gNnSc2
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Super	super	k2eAgInSc1d1
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Kamenár	Kamenár	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
od	od	k7c2
Göteborgu	Göteborg	k1gInSc2
tri	tri	k?
góly	gól	k1gInPc4
<g/>
,	,	kIx,
Profutbal	Profutbal	k1gInSc4
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ďuriš	Ďuriš	k1gInSc1
dvoma	dvoma	k1gFnSc1
gólmi	gól	k1gFnPc7
pomohol	pomoholit	k5eAaPmRp2nS
Mladej	Mladej	k?
Boleslavi	Boleslaev	k1gFnSc6
k	k	k7c3
postupu	postup	k1gInSc3
<g/>
,	,	kIx,
Profutbal	Profutbal	k1gInSc4
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
klubu	klub	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Prva	Prv	k2eAgNnPc1d1
slovenska	slovensko	k1gNnPc1
nogometna	nogometen	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
FC	FC	kA
Koper	kopra	k1gFnPc2
•	•	k?
ND	ND	kA
Gorica	Gorica	k1gMnSc1
•	•	k?
NK	NK	kA
Aluminij	Aluminij	k1gMnSc3
•	•	k?
NK	NK	kA
Bravo	bravo	k1gMnSc1
•	•	k?
NK	NK	kA
Celje	Celj	k1gFnSc2
•	•	k?
NK	NK	kA
Domžale	Domžala	k1gFnSc3
•	•	k?
NK	NK	kA
Maribor	Maribor	k1gInSc1
•	•	k?
NŠ	NŠ	kA
Mura	mura	k1gFnSc1
•	•	k?
NK	NK	kA
Olimpija	Olimpij	k2eAgFnSc1d1
Lublaň	Lublaň	k1gFnSc1
•	•	k?
Tabor	Tabor	k1gInSc4
Sežana	Sežan	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovinsko	Slovinsko	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
