<p>
<s>
Brabenčák	Brabenčák	k1gMnSc1	Brabenčák
<g/>
,	,	kIx,	,
Brabenec	Brabenec	k1gMnSc1	Brabenec
či	či	k8xC	či
Na	na	k7c6	na
Brabenci	Brabenec	k1gMnSc6	Brabenec
je	být	k5eAaImIp3nS	být
kopec	kopec	k1gInSc1	kopec
v	v	k7c6	v
Radlicích	radlice	k1gFnPc6	radlice
a	a	k8xC	a
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Zastavěné	zastavěný	k2eAgNnSc1d1	zastavěné
východní	východní	k2eAgNnSc1d1	východní
úbočí	úbočí	k1gNnSc1	úbočí
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
též	též	k6eAd1	též
například	například	k6eAd1	například
názvem	název	k1gInSc7	název
Kesnerka	Kesnerka	k1gFnSc1	Kesnerka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
někdejších	někdejší	k2eAgFnPc2d1	někdejší
usedlostí	usedlost	k1gFnPc2	usedlost
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
partie	partie	k1gFnSc1	partie
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
mapách	mapa	k1gFnPc6	mapa
kótována	kótován	k2eAgFnSc1d1	kótována
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
297	[number]	k4	297
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
vrstevnic	vrstevnice	k1gFnPc2	vrstevnice
Základní	základní	k2eAgFnSc2d1	základní
mapy	mapa	k1gFnSc2	mapa
ČR	ČR	kA	ČR
1	[number]	k4	1
:	:	kIx,	:
10	[number]	k4	10
000	[number]	k4	000
se	se	k3xPyFc4	se
vrchol	vrchol	k1gInSc1	vrchol
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	daleko	k6eNd1	daleko
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Brabenci	Brabenec	k1gMnSc6	Brabenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podlouhlý	podlouhlý	k2eAgInSc1d1	podlouhlý
vrchol	vrchol	k1gInSc1	vrchol
kopce	kopec	k1gInSc2	kopec
tvoří	tvořit	k5eAaImIp3nS	tvořit
slepá	slepý	k2eAgFnSc1d1	slepá
ulice	ulice	k1gFnSc1	ulice
Na	na	k7c6	na
Brabenci	Brabenec	k1gMnSc6	Brabenec
(	(	kIx(	(
<g/>
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
a	a	k8xC	a
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
místního	místní	k2eAgInSc2d1	místní
lidového	lidový	k2eAgInSc2d1	lidový
názvu	název	k1gInSc2	název
Brabenčák	Brabenčák	k1gInSc1	Brabenčák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
severním	severní	k2eAgNnSc6d1	severní
úbočí	úbočí	k1gNnSc6	úbočí
ulice	ulice	k1gFnSc2	ulice
U	u	k7c2	u
starého	starý	k2eAgInSc2d1	starý
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
východním	východní	k2eAgInSc7d1	východní
koncem	konec	k1gInSc7	konec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
starý	starý	k2eAgInSc1d1	starý
smíchovský	smíchovský	k2eAgInSc1d1	smíchovský
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
radlický	radlický	k2eAgInSc1d1	radlický
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
partie	partie	k1gFnSc1	partie
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
strmější	strmý	k2eAgFnSc1d2	strmější
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
Radlic	Radlice	k1gInPc2	Radlice
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
kopce	kopec	k1gInSc2	kopec
na	na	k7c4	na
Smíchov	Smíchov	k1gInSc4	Smíchov
<g/>
:	:	kIx,	:
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nP	tvořit
u	u	k7c2	u
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
partie	partie	k1gFnSc2	partie
ulice	ulice	k1gFnSc2	ulice
U	u	k7c2	u
starého	starý	k2eAgInSc2d1	starý
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
východozápadním	východozápadní	k2eAgInSc7d1	východozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
a	a	k8xC	a
ulice	ulice	k1gFnSc1	ulice
Kroupova	Kroupův	k2eAgFnSc1d1	Kroupova
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
již	již	k6eAd1	již
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
smíchovské	smíchovský	k2eAgFnSc3d1	Smíchovská
části	část	k1gFnSc3	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radlické	radlický	k2eAgInPc1d1	radlický
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
nezastavěné	zastavěný	k2eNgInPc1d1	nezastavěný
<g/>
,	,	kIx,	,
lesnaté	lesnatý	k2eAgInPc1d1	lesnatý
<g/>
,	,	kIx,	,
protkané	protkaný	k2eAgInPc1d1	protkaný
vycházkovými	vycházkový	k2eAgFnPc7d1	vycházková
stezkami	stezka	k1gFnPc7	stezka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
severní	severní	k2eAgInSc4d1	severní
smíchovský	smíchovský	k2eAgInSc4d1	smíchovský
svah	svah	k1gInSc4	svah
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
smíchovskou	smíchovský	k2eAgFnSc4d1	Smíchovská
stranu	strana	k1gFnSc4	strana
kopce	kopec	k1gInSc2	kopec
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vilová	vilový	k2eAgFnSc1d1	vilová
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
vrstevnicovém	vrstevnicový	k2eAgInSc6d1	vrstevnicový
směru	směr	k1gInSc6	směr
kopec	kopec	k1gInSc1	kopec
úbočím	úboč	k1gFnPc3	úboč
obcházejí	obcházet	k5eAaImIp3nP	obcházet
ulice	ulice	k1gFnPc1	ulice
Nad	nad	k7c7	nad
Kesnerkou	Kesnerka	k1gFnSc7	Kesnerka
<g/>
,	,	kIx,	,
K	k	k7c3	k
Závěrce	závěrka	k1gFnSc3	závěrka
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Děvínem	Děvín	k1gInSc7	Děvín
<g/>
,	,	kIx,	,
Kesnerka	Kesnerka	k1gFnSc1	Kesnerka
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Závěrkou	závěrka	k1gFnSc7	závěrka
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Konvářce	Konvářka	k1gFnSc6	Konvářka
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Kesnerkou	Kesnerka	k1gFnSc7	Kesnerka
a	a	k8xC	a
Franty	Franta	k1gMnSc2	Franta
Kocourka	Kocourek	k1gMnSc2	Kocourek
<g/>
,	,	kIx,	,
šikmo	šikmo	k6eAd1	šikmo
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
od	od	k7c2	od
severovýchodu	severovýchod	k1gInSc2	severovýchod
ulice	ulice	k1gFnSc2	ulice
Pajerova	Pajerův	k2eAgMnSc2d1	Pajerův
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
hřbetnici	hřbetnice	k1gFnSc6	hřbetnice
schodiště	schodiště	k1gNnSc2	schodiště
Koulka	Koulko	k1gNnSc2	Koulko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesíku	lesík	k1gInSc6	lesík
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Pajerova	Pajerův	k2eAgMnSc2d1	Pajerův
a	a	k8xC	a
Pod	pod	k7c7	pod
Kesnerkou	Kesnerka	k1gFnSc7	Kesnerka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
studánka	studánka	k1gFnSc1	studánka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Pod	pod	k7c7	pod
Kesnerkou	Kesnerka	k1gFnSc7	Kesnerka
<g/>
,	,	kIx,	,
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
aktem	akt	k1gInSc7	akt
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
přáteli	přítel	k1gMnPc7	přítel
Ivana	Ivan	k1gMnSc2	Ivan
Martina	Martin	k1gMnSc2	Martin
Jirouse	Jirouse	k1gFnSc2	Jirouse
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nedaleko	daleko	k6eNd1	daleko
žil	žít	k5eAaImAgMnS	žít
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Magorova	magorův	k2eAgFnSc1d1	Magorova
studánka	studánka	k1gFnSc1	studánka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k8xC	jako
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
nechali	nechat	k5eAaPmAgMnP	nechat
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
údajně	údajně	k6eAd1	údajně
zapsat	zapsat	k5eAaPmF	zapsat
do	do	k7c2	do
katastrálního	katastrální	k2eAgInSc2d1	katastrální
výměru	výměr	k1gInSc2	výměr
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
kopce	kopec	k1gInSc2	kopec
byla	být	k5eAaImAgFnS	být
zalesněna	zalesněn	k2eAgFnSc1d1	zalesněna
postupně	postupně	k6eAd1	postupně
až	až	k9	až
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
partie	partie	k1gFnSc1	partie
byla	být	k5eAaImAgFnS	být
zastavěna	zastavět	k5eAaPmNgFnS	zastavět
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
smíchovském	smíchovský	k2eAgNnSc6d1	Smíchovské
úbočí	úbočí	k1gNnSc6	úbočí
stálo	stát	k5eAaImAgNnS	stát
či	či	k8xC	či
dosud	dosud	k6eAd1	dosud
stojí	stát	k5eAaImIp3nS	stát
několik	několik	k4yIc1	několik
historických	historický	k2eAgFnPc2d1	historická
usedlostí	usedlost	k1gFnPc2	usedlost
<g/>
:	:	kIx,	:
horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
Kesnerka	Kesnerka	k1gFnSc1	Kesnerka
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
postupně	postupně	k6eAd1	postupně
zbourány	zbourat	k5eAaPmNgFnP	zbourat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
Koulka	Koulka	k1gFnSc1	Koulka
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
stojí	stát	k5eAaImIp3nS	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k9	již
na	na	k7c4	na
úbočí	úbočí	k1gNnPc4	úbočí
Dívčích	dívčí	k2eAgInPc2d1	dívčí
hradů	hrad	k1gInPc2	hrad
též	též	k9	též
Konvářka	Konvářka	k1gFnSc1	Konvářka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
kopec	kopec	k1gInSc1	kopec
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
zhruba	zhruba	k6eAd1	zhruba
ulice	ulice	k1gFnSc1	ulice
Kutvirtova	Kutvirtův	k2eAgFnSc1d1	Kutvirtova
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
122	[number]	k4	122
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k6eAd1	naproti
přes	přes	k7c4	přes
údolí	údolí	k1gNnPc4	údolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kopec	kopec	k1gInSc1	kopec
Děvín	Děvín	k1gInSc1	Děvín
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dívčí	dívčí	k2eAgInPc4d1	dívčí
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
kopec	kopec	k1gInSc1	kopec
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
radlického	radlický	k2eAgNnSc2d1	Radlické
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
němu	on	k3xPp3gNnSc3	on
stojí	stát	k5eAaImIp3nS	stát
Paví	paví	k2eAgInSc1d1	paví
vrch	vrch	k1gInSc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úpatí	úpatí	k1gNnSc3	úpatí
kopce	kopec	k1gInSc2	kopec
lze	lze	k6eAd1	lze
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
smíchovskou	smíchovský	k2eAgFnSc4d1	Smíchovská
zástavbu	zástavba	k1gFnSc4	zástavba
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Radlické	radlický	k2eAgFnSc2d1	Radlická
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ulice	ulice	k1gFnSc1	ulice
Nad	nad	k7c7	nad
Koulkou	Koulka	k1gFnSc7	Koulka
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Neklance	Neklanec	k1gMnSc4	Neklanec
<g/>
,	,	kIx,	,
Holubova	Holubův	k2eAgMnSc4d1	Holubův
(	(	kIx(	(
<g/>
v	v	k7c6	v
Radlicích	Radlice	k1gInPc6	Radlice
Pechlátova	Pechlátův	k2eAgFnSc1d1	Pechlátova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Barvířkou	barvířka	k1gFnSc7	barvířka
<g/>
,	,	kIx,	,
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
Na	na	k7c4	na
Laurové	laurový	k2eAgNnSc4d1	laurový
<g/>
,	,	kIx,	,
Braunova	Braunův	k2eAgMnSc4d1	Braunův
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Brentovou	Brentův	k2eAgFnSc7d1	Brentova
a	a	k8xC	a
Ke	k	k7c3	k
Koulce	Koulka	k1gFnSc3	Koulka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
východu	východ	k1gInSc3	východ
kopec	kopec	k1gInSc1	kopec
strmě	strmě	k6eAd1	strmě
spadá	spadat	k5eAaImIp3nS	spadat
k	k	k7c3	k
nivě	niva	k1gFnSc3	niva
vltavského	vltavský	k2eAgNnSc2d1	Vltavské
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
úpatí	úpatí	k1gNnSc6	úpatí
vede	vést	k5eAaImIp3nS	vést
ulice	ulice	k1gFnSc1	ulice
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Dobříšská	dobříšský	k2eAgFnSc1d1	Dobříšská
(	(	kIx(	(
<g/>
Městský	městský	k2eAgInSc4d1	městský
okruh	okruh	k1gInSc4	okruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
svazek	svazek	k1gInSc4	svazek
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
Praha-Smíchov	Praha-Smíchov	k1gInSc1	Praha-Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
západu	západ	k1gInSc3	západ
kopec	kopec	k1gInSc1	kopec
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
jako	jako	k9	jako
ostroh	ostroh	k1gInSc1	ostroh
proti	proti	k7c3	proti
radlickému	radlický	k2eAgNnSc3d1	Radlické
údolí	údolí	k1gNnSc3	údolí
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Jinonicům	Jinonice	k1gInPc3	Jinonice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
západního	západní	k2eAgNnSc2d1	západní
úpatí	úpatí	k1gNnSc2	úpatí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sportcentrum	sportcentrum	k1gNnSc1	sportcentrum
Radlice	radlice	k1gFnSc2	radlice
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
obestavěné	obestavěný	k2eAgInPc1d1	obestavěný
třípatrovou	třípatrový	k2eAgFnSc7d1	třípatrová
kancelářskou	kancelářský	k2eAgFnSc7d1	kancelářská
budovou	budova	k1gFnSc7	budova
čp.	čp.	k?	čp.
339	[number]	k4	339
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Hrušňová	hrušňový	k2eAgFnSc1d1	hrušňová
alej	alej	k1gFnSc1	alej
pod	pod	k7c7	pod
Brabenčákem	Brabenčák	k1gInSc7	Brabenčák
<g/>
,	,	kIx,	,
Pražská	pražský	k2eAgFnSc1d1	Pražská
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
Ovocné	ovocný	k2eAgFnPc1d1	ovocná
sady	sada	k1gFnPc1	sada
a	a	k8xC	a
aleje	alej	k1gFnPc1	alej
<g/>
,	,	kIx,	,
Radlice	Radlice	k1gInPc1	Radlice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Petice	petice	k1gFnSc1	petice
<g/>
:	:	kIx,	:
Nesouhlas	nesouhlas	k1gInSc1	nesouhlas
s	s	k7c7	s
pokračující	pokračující	k2eAgFnSc7d1	pokračující
ignorací	ignorace	k1gFnSc7	ignorace
potřeb	potřeba	k1gFnPc2	potřeba
obyvatel	obyvatel	k1gMnPc2	obyvatel
oblasti	oblast	k1gFnSc3	oblast
Dívčích	dívčí	k2eAgInPc2d1	dívčí
Hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
Brabence	Brabenec	k1gMnPc4	Brabenec
<g/>
,	,	kIx,	,
Parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
</s>
</p>
