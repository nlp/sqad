<p>
<s>
Édouard	Édouard	k1gMnSc1	Édouard
Balladur	Balladur	k1gMnSc1	Balladur
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
politik	politik	k1gMnSc1	politik
arménského	arménský	k2eAgInSc2d1	arménský
původu	původ	k1gInSc2	původ
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
premiérem	premiér	k1gMnSc7	premiér
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Jacquese	Jacques	k1gMnSc2	Jacques
Chiraca	Chiracus	k1gMnSc2	Chiracus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
představitelem	představitel	k1gMnSc7	představitel
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
Rassemblement	Rassemblement	k1gMnSc1	Rassemblement
pour	pour	k1gMnSc1	pour
la	la	k1gNnSc2	la
République	République	k1gNnSc2	République
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
spoluvytvořila	spoluvytvořit	k5eAaPmAgFnS	spoluvytvořit
nový	nový	k2eAgInSc4d1	nový
pravicový	pravicový	k2eAgInSc4d1	pravicový
subjekt	subjekt	k1gInSc4	subjekt
Union	union	k1gInSc1	union
pour	pour	k1gInSc1	pour
un	un	k?	un
Mouvement	Mouvement	k1gInSc1	Mouvement
Populaire	Populair	k1gInSc5	Populair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
politika	politik	k1gMnSc4	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
pravici	pravice	k1gFnSc6	pravice
oslabil	oslabit	k5eAaPmAgInS	oslabit
gaullismus	gaullismus	k1gInSc1	gaullismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
začínal	začínat	k5eAaImAgMnS	začínat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Georgese	Georgese	k1gFnSc2	Georgese
Pompidoua	Pompidou	k1gInSc2	Pompidou
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
poradcem	poradce	k1gMnSc7	poradce
<g/>
,	,	kIx,	,
když	když	k8xS	když
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Pompidou	Pompida	k1gFnSc7	Pompida
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Balladur	Balladur	k1gMnSc1	Balladur
zástupcem	zástupce	k1gMnSc7	zástupce
jeho	on	k3xPp3gMnSc2	on
kancléře	kancléř	k1gMnSc2	kancléř
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k8xC	i
kancléřem	kancléř	k1gMnSc7	kancléř
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
angažmá	angažmá	k1gNnSc6	angažmá
v	v	k7c6	v
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
paláci	palác	k1gInSc6	palác
se	se	k3xPyFc4	se
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
načas	načas	k6eAd1	načas
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
jako	jako	k9	jako
podporovatel	podporovatel	k1gMnSc1	podporovatel
a	a	k8xC	a
poradce	poradce	k1gMnSc1	poradce
Jacquese	Jacques	k1gMnSc2	Jacques
Chiraca	Chiracus	k1gMnSc2	Chiracus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
formoval	formovat	k5eAaImAgMnS	formovat
novou	nový	k2eAgFnSc4d1	nová
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
první	první	k4xOgFnSc6	první
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Chirac	Chirac	k1gMnSc1	Chirac
sestavil	sestavit	k5eAaPmAgMnS	sestavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
vlády	vláda	k1gFnPc1	vláda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
vládnout	vládnout	k5eAaImF	vládnout
pod	pod	k7c7	pod
levicovým	levicový	k2eAgMnSc7d1	levicový
prezidentem	prezident	k1gMnSc7	prezident
François	François	k1gFnPc2	François
Mitterrandem	Mitterrand	k1gInSc7	Mitterrand
a	a	k8xC	a
Balladur	Balladur	k1gMnSc1	Balladur
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
specialistu	specialista	k1gMnSc4	specialista
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
premiér	premiéra	k1gFnPc2	premiéra
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
umírněným	umírněný	k2eAgInSc7d1	umírněný
přístupem	přístup	k1gInSc7	přístup
<g/>
,	,	kIx,	,
zpětně	zpětně	k6eAd1	zpětně
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
především	především	k6eAd1	především
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
při	při	k7c6	při
genocidě	genocida	k1gFnSc6	genocida
ve	v	k7c6	v
Rwandě	Rwanda	k1gFnSc6	Rwanda
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
politickém	politický	k2eAgInSc6d1	politický
osudu	osud	k1gInSc6	osud
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
především	především	k9	především
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
Chiracem	Chiraec	k1gInSc7	Chiraec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Balladur	Balladur	k1gMnSc1	Balladur
vedl	vést	k5eAaImAgMnS	vést
vládu	vláda	k1gFnSc4	vláda
jako	jako	k8xS	jako
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
Chiracovi	Chiraec	k1gMnSc3	Chiraec
slíbil	slíbit	k5eAaPmAgMnS	slíbit
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Chirac	Chirac	k1gMnSc1	Chirac
však	však	k9	však
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
a	a	k8xC	a
Balladura	Balladura	k1gFnSc1	Balladura
ihned	ihned	k6eAd1	ihned
odvolal	odvolat	k5eAaPmAgInS	odvolat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Chirac	Chirac	k1gMnSc1	Chirac
si	se	k3xPyFc3	se
spor	spor	k1gInSc1	spor
vzal	vzít	k5eAaPmAgInS	vzít
velmi	velmi	k6eAd1	velmi
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
s	s	k7c7	s
Balladurem	Balladur	k1gMnSc7	Balladur
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
pravicové	pravicový	k2eAgFnSc2d1	pravicová
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
podpodporovali	podpodporovat	k5eAaPmAgMnP	podpodporovat
Balladura	Balladur	k1gMnSc4	Balladur
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Balladuriens	Balladuriens	k1gInSc1	Balladuriens
(	(	kIx(	(
<g/>
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patřil	patřit	k5eAaImAgMnS	patřit
mj.	mj.	kA	mj.
i	i	k9	i
Nicolas	Nicolas	k1gInSc1	Nicolas
Sarkozy	Sarkoz	k1gInPc1	Sarkoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odstavil	odstavit	k5eAaPmAgInS	odstavit
z	z	k7c2	z
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Édouard	Édouarda	k1gFnPc2	Édouarda
Balladur	Balladura	k1gFnPc2	Balladura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgInPc1d1	encyklopedický
zdroje	zdroj	k1gInPc1	zdroj
na	na	k7c4	na
Answers	Answers	k1gInSc4	Answers
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
Monsieur-Biographie	Monsieur-Biographie	k1gFnPc4	Monsieur-Biographie
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
