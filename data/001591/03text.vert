<s>
Jukawa	Jukawa	k1gFnSc1	Jukawa
Hideki	Hidek	k1gFnSc2	Hidek
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
湯	湯	k?	湯
秀	秀	k?	秀
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Yukawa	Yukaw	k2eAgNnPc4d1	Yukaw
Hideki	Hideki	k1gNnPc4	Hideki
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1907	[number]	k4	1907
Tokio	Tokio	k1gNnSc1	Tokio
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
Kjóto	Kjóto	k1gNnSc1	Kjóto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
japonský	japonský	k2eAgMnSc1d1	japonský
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
předpověď	předpověď	k1gFnSc4	předpověď
existence	existence	k1gFnSc2	existence
mezonů	mezon	k1gInPc2	mezon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
teoretického	teoretický	k2eAgInSc2d1	teoretický
výzkumu	výzkum	k1gInSc2	výzkum
jaderných	jaderný	k2eAgFnPc2d1	jaderná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Jukawův	Jukawův	k2eAgInSc1d1	Jukawův
potenciál	potenciál	k1gInSc1	potenciál
silné	silný	k2eAgFnSc2d1	silná
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hideki	Hidek	k1gFnSc2	Hidek
Jukawa	Jukawum	k1gNnSc2	Jukawum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
astro	astra	k1gFnSc5	astra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Official	Official	k1gMnSc1	Official
Nobel	Nobel	k1gMnSc1	Nobel
site	sitat	k5eAaPmIp3nS	sitat
životopis	životopis	k1gInSc4	životopis
-	-	kIx~	-
en	en	k?	en
Jeho	jeho	k3xOp3gFnSc4	jeho
předpověď	předpověď	k1gFnSc4	předpověď
mezonu	mezon	k1gInSc2	mezon
-	-	kIx~	-
en	en	k?	en
</s>
