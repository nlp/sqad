<s>
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
je	být	k5eAaImIp3nS	být
tragická	tragický	k2eAgFnSc1d1	tragická
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Friedricha	Friedrich	k1gMnSc2	Friedrich
Dürrenmatta	Dürrenmatt	k1gMnSc2	Dürrenmatt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
premiéra	premiéra	k1gFnSc1	premiéra
s	s	k7c7	s
Therese	Therese	k1gFnSc1	Therese
Giehse	Giehse	k1gFnPc4	Giehse
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1956	[number]	k4	1956
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
světového	světový	k2eAgInSc2d1	světový
úspěchu	úspěch	k1gInSc2	úspěch
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
autorovi	autor	k1gMnSc3	autor
přinesla	přinést	k5eAaPmAgFnS	přinést
finanční	finanční	k2eAgFnSc4d1	finanční
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
Claire	Clair	k1gInSc5	Clair
Zachanassian	Zachanassian	k1gMnSc1	Zachanassian
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Klara	Klara	k1gFnSc1	Klara
Wäscher	Wäschra	k1gFnPc2	Wäschra
(	(	kIx(	(
<g/>
multimiliardářka	multimiliardářka	k1gFnSc1	multimiliardářka
<g/>
;	;	kIx,	;
Armenian-Oil	Armenian-Oil	k1gInSc1	Armenian-Oil
<g/>
)	)	kIx)	)
Její	její	k3xOp3gMnPc1	její
manželé	manžel	k1gMnPc1	manžel
VII	VII	kA	VII
<g/>
–	–	k?	–
<g/>
IX	IX	kA	IX
Komorník	komorník	k1gMnSc1	komorník
Toby	Toba	k1gMnSc2	Toba
(	(	kIx(	(
<g/>
se	s	k7c7	s
žvýkačkou	žvýkačka	k1gFnSc7	žvýkačka
<g/>
)	)	kIx)	)
Roby	roba	k1gFnPc1	roba
(	(	kIx(	(
<g/>
se	s	k7c7	s
žvýkačkou	žvýkačka	k1gFnSc7	žvýkačka
<g/>
)	)	kIx)	)
Koby	Koby	k?	Koby
(	(	kIx(	(
<g/>
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
)	)	kIx)	)
Loby	lob	k1gInPc1	lob
(	(	kIx(	(
<g/>
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
)	)	kIx)	)
Navštívení	navštívení	k1gNnSc1	navštívení
Ill	Ill	k1gFnSc2	Ill
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
<g />
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
Starosta	Starosta	k1gMnSc1	Starosta
Farář	farář	k1gMnSc1	farář
Učitel	učitel	k1gMnSc1	učitel
Doktor	doktor	k1gMnSc1	doktor
Policista	policista	k1gMnSc1	policista
První	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Druhý	druhý	k4xOgInSc4	druhý
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgFnSc1	třetí
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
(	(	kIx(	(
<g/>
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Malíř	malíř	k1gMnSc1	malíř
První	první	k4xOgMnSc1	první
žena	žena	k1gFnSc1	žena
Druhá	druhý	k4xOgFnSc1	druhý
žena	žena	k1gFnSc1	žena
Třetí	třetí	k4xOgFnSc2	třetí
žena	žena	k1gFnSc1	žena
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
Přednosta	přednosta	k1gMnSc1	přednosta
stanice	stanice	k1gFnSc2	stanice
Strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
Průvodčí	průvodčí	k1gMnSc1	průvodčí
Exekutor	exekutor	k1gMnSc1	exekutor
Otravní	otravný	k2eAgMnPc1d1	otravný
Novinář	novinář	k1gMnSc1	novinář
I	i	k8xC	i
Novinář	novinář	k1gMnSc1	novinář
II	II	kA	II
Radioreportér	Radioreportér	k1gMnSc1	Radioreportér
Kameraman	kameraman	k1gMnSc1	kameraman
Miliardářka	miliardářka	k1gFnSc1	miliardářka
Claire	Clair	k1gInSc5	Clair
Zachanassian	Zachanassiana	k1gFnPc2	Zachanassiana
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
titulní	titulní	k2eAgFnSc1d1	titulní
"	"	kIx"	"
<g/>
stará	starý	k2eAgFnSc1d1	stará
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prijíždí	prijíždit	k5eAaPmIp3nS	prijíždit
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
zchudlého	zchudlý	k2eAgNnSc2d1	zchudlé
maloměsta	maloměsto	k1gNnSc2	maloměsto
Güllen	Güllna	k1gFnPc2	Güllna
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
kdysi	kdysi	k6eAd1	kdysi
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
jako	jako	k9	jako
Klárka	Klárka	k1gFnSc1	Klárka
Wäscherová	Wäscherová	k1gFnSc1	Wäscherová
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
městečka	městečko	k1gNnSc2	městečko
si	se	k3xPyFc3	se
od	od	k7c2	od
návštěvy	návštěva	k1gFnSc2	návštěva
slibují	slibovat	k5eAaImIp3nP	slibovat
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
Claire	Clair	k1gInSc5	Clair
zase	zase	k9	zase
pomstu	pomsta	k1gFnSc4	pomsta
za	za	k7c4	za
dávnou	dávný	k2eAgFnSc4d1	dávná
křivdu	křivda	k1gFnSc4	křivda
<g/>
:	:	kIx,	:
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
s	s	k7c7	s
Gülleňanem	Gülleňan	k1gMnSc7	Gülleňan
Alfrédem	Alfréd	k1gMnSc7	Alfréd
Illem	Ill	k1gMnSc7	Ill
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
otcovství	otcovství	k1gNnSc4	otcovství
popřel	popřít	k5eAaPmAgMnS	popřít
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
falešných	falešný	k2eAgMnPc2d1	falešný
svědků	svědek	k1gMnPc2	svědek
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
následný	následný	k2eAgInSc1d1	následný
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
musela	muset	k5eAaImAgFnS	muset
v	v	k7c6	v
hanbě	hanba	k1gFnSc6	hanba
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
opustit	opustit	k5eAaPmF	opustit
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k9	ale
díky	díky	k7c3	díky
sňatku	sňatek	k1gInSc3	sňatek
s	s	k7c7	s
ropným	ropný	k2eAgMnSc7d1	ropný
magnátem	magnát	k1gMnSc7	magnát
a	a	k8xC	a
několika	několik	k4yIc7	několik
dalším	další	k2eAgNnSc7d1	další
manželstvím	manželství	k1gNnSc7	manželství
získala	získat	k5eAaPmAgFnS	získat
obrovský	obrovský	k2eAgInSc4d1	obrovský
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
vysoce	vysoce	k6eAd1	vysoce
vážená	vážený	k2eAgFnSc1d1	Vážená
"	"	kIx"	"
<g/>
stará	starý	k2eAgFnSc1d1	stará
dáma	dáma	k1gFnSc1	dáma
<g/>
"	"	kIx"	"
učiní	učinit	k5eAaPmIp3nS	učinit
Güllenským	Güllenský	k2eAgInSc7d1	Güllenský
nehoráznou	nehorázný	k2eAgFnSc7d1	nehorázná
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
:	:	kIx,	:
věnuje	věnovat	k5eAaPmIp3nS	věnovat
městu	město	k1gNnSc3	město
miliardu	miliarda	k4xCgFnSc4	miliarda
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
Illa	Ill	k1gInSc2	Ill
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
obyvatel	obyvatel	k1gMnPc2	obyvatel
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nejprve	nejprve	k6eAd1	nejprve
návrh	návrh	k1gInSc4	návrh
jednoznačně	jednoznačně	k6eAd1	jednoznačně
odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
začnou	začít	k5eAaPmIp3nP	začít
podezřele	podezřele	k6eAd1	podezřele
utrácet	utrácet	k5eAaImF	utrácet
a	a	k8xC	a
dělat	dělat	k5eAaImF	dělat
dluhy	dluh	k1gInPc4	dluh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
počítali	počítat	k5eAaImAgMnP	počítat
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
brzo	brzo	k6eAd1	brzo
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Ill	Ill	k?	Ill
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
přemožený	přemožený	k2eAgInSc4d1	přemožený
strachem	strach	k1gInSc7	strach
a	a	k8xC	a
pocitem	pocit	k1gInSc7	pocit
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnPc3	svůj
sousedům	soused	k1gMnPc3	soused
vydá	vydat	k5eAaPmIp3nS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
informuje	informovat	k5eAaBmIp3nS	informovat
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
že	že	k8xS	že
paní	paní	k1gFnSc1	paní
Zachanassian	Zachanassiana	k1gFnPc2	Zachanassiana
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
městu	město	k1gNnSc3	město
miliardový	miliardový	k2eAgInSc4d1	miliardový
příspěvek	příspěvek	k1gInSc4	příspěvek
díky	díky	k7c3	díky
Alfrédu	Alfréd	k1gMnSc3	Alfréd
Illovi	Illa	k1gMnSc3	Illa
<g/>
,	,	kIx,	,
jejímu	její	k3xOp3gMnSc3	její
příteli	přítel	k1gMnSc3	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Gülleňané	Gülleňan	k1gMnPc1	Gülleňan
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
uličku	ulička	k1gFnSc4	ulička
a	a	k8xC	a
Ill	Ill	k1gFnSc1	Ill
jí	on	k3xPp3gFnSc3	on
projde	projít	k5eAaPmIp3nS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
rozestoupí	rozestoupit	k5eAaPmIp3nS	rozestoupit
<g/>
,	,	kIx,	,
Ill	Ill	k1gMnPc1	Ill
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Infarkt	infarkt	k1gInSc1	infarkt
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
samou	samý	k3xTgFnSc7	samý
radostí	radost	k1gFnSc7	radost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
v	v	k7c6	v
komentářích	komentář	k1gInPc6	komentář
lékaře	lékař	k1gMnSc2	lékař
i	i	k8xC	i
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Claire	Clair	k1gMnSc5	Clair
nechá	nechat	k5eAaPmIp3nS	nechat
mrtvého	mrtvý	k1gMnSc4	mrtvý
uložit	uložit	k5eAaPmF	uložit
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
přivezla	přivézt	k5eAaPmAgFnS	přivézt
<g/>
,	,	kIx,	,
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
obdrží	obdržet	k5eAaPmIp3nS	obdržet
miliardový	miliardový	k2eAgInSc4d1	miliardový
šek	šek	k1gInSc4	šek
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
další	další	k2eAgNnPc1d1	další
Dürrenmattova	Dürrenmattův	k2eAgNnPc1d1	Dürrenmattův
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
i	i	k9	i
Návštěva	návštěva	k1gFnSc1	návštěva
dá	dát	k5eAaPmIp3nS	dát
interpretovat	interpretovat	k5eAaBmF	interpretovat
mnoha	mnoho	k4c2	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zřejmé	zřejmý	k2eAgFnSc2d1	zřejmá
kritiky	kritika	k1gFnSc2	kritika
chamtivosti	chamtivost	k1gFnSc2	chamtivost
a	a	k8xC	a
povrchnosti	povrchnost	k1gFnSc2	povrchnost
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
úvahu	úvaha	k1gFnSc4	úvaha
i	i	k8xC	i
problém	problém	k1gInSc4	problém
sklonů	sklon	k1gInPc2	sklon
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
žití	žití	k1gNnSc3	žití
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
obcházení	obcházení	k1gNnSc4	obcházení
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
světlých	světlý	k2eAgInPc2d1	světlý
zítřků	zítřek	k1gInPc2	zítřek
či	či	k8xC	či
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
vzít	vzít	k5eAaPmF	vzít
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Dürrenmatt	Dürrenmatt	k1gInSc1	Dürrenmatt
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
spojuje	spojovat	k5eAaImIp3nS	spojovat
motivy	motiv	k1gInPc4	motiv
tragédie	tragédie	k1gFnSc2	tragédie
i	i	k8xC	i
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k6eAd1	tak
klasickou	klasický	k2eAgFnSc4d1	klasická
tragikomedii	tragikomedie	k1gFnSc4	tragikomedie
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
též	též	k9	též
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
modelové	modelový	k2eAgNnSc1d1	modelové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
realističnost	realističnost	k1gFnSc4	realističnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autor	autor	k1gMnSc1	autor
vykonstruuje	vykonstruovat	k5eAaPmIp3nS	vykonstruovat
určitou	určitý	k2eAgFnSc4d1	určitá
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dovede	dovést	k5eAaPmIp3nS	dovést
do	do	k7c2	do
krajnosti	krajnost	k1gFnSc2	krajnost
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
jejím	její	k3xOp3gNnSc7	její
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
společenský	společenský	k2eAgInSc4d1	společenský
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
mnoho	mnoho	k4c1	mnoho
odkazů	odkaz	k1gInPc2	odkaz
k	k	k7c3	k
antickým	antický	k2eAgFnPc3d1	antická
tragédiím	tragédie	k1gFnPc3	tragédie
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
interpretace	interpretace	k1gFnSc2	interpretace
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
v	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
postava	postava	k1gFnSc1	postava
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
<g/>
:	:	kIx,	:
Claire	Clair	k1gInSc5	Clair
Zachanassian	Zachanassian	k1gInSc1	Zachanassian
čeká	čekat	k5eAaImIp3nS	čekat
45	[number]	k4	45
let	léto	k1gNnPc2	léto
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pomstu	pomsta	k1gFnSc4	pomsta
-	-	kIx~	-
touto	tento	k3xDgFnSc7	tento
urputností	urputnost	k1gFnSc7	urputnost
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
Médee	Médee	k1gFnSc1	Médee
<g/>
.	.	kIx.	.
</s>
<s>
Pomsta	pomsta	k1gFnSc1	pomsta
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
skrze	skrze	k?	skrze
bohatství	bohatství	k1gNnSc1	bohatství
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
z	z	k7c2	z
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
udělá	udělat	k5eAaPmIp3nS	udělat
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
majetek	majetek	k1gInSc4	majetek
jí	on	k3xPp3gFnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
"	"	kIx"	"
<g/>
jednat	jednat	k5eAaImF	jednat
jako	jako	k9	jako
hrdinka	hrdinka	k1gFnSc1	hrdinka
řecké	řecký	k2eAgFnSc2d1	řecká
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
absolutně	absolutně	k6eAd1	absolutně
<g/>
,	,	kIx,	,
strašlivě	strašlivě	k6eAd1	strašlivě
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Médea	Médea	k1gFnSc1	Médea
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ale	ale	k9	ale
můžeme	moct	k5eAaImIp1nP	moct
ji	on	k3xPp3gFnSc4	on
přirovnat	přirovnat	k5eAaPmF	přirovnat
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
řecké	řecký	k2eAgFnSc3d1	řecká
hrdince	hrdinka	k1gFnSc3	hrdinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
bohyni	bohyně	k1gFnSc3	bohyně
pomsty	pomsta	k1gFnSc2	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
moci	moc	k1gFnSc3	moc
může	moct	k5eAaImIp3nS	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
mnoha	mnoho	k4c6	mnoho
osudech	osud	k1gInPc6	osud
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dojem	dojem	k1gInSc1	dojem
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
zdůrazněn	zdůraznit	k5eAaPmNgInS	zdůraznit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dürrenmatt	Dürrenmatt	k1gInSc1	Dürrenmatt
Kláru	Klára	k1gFnSc4	Klára
vybavil	vybavit	k5eAaPmAgInS	vybavit
jakousi	jakýsi	k3yIgFnSc7	jakýsi
pseudonesmrtelností	pseudonesmrtelnost	k1gFnSc7	pseudonesmrtelnost
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
přežila	přežít	k5eAaPmAgFnS	přežít
leteckou	letecký	k2eAgFnSc4d1	letecká
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
co	co	k8xS	co
jí	on	k3xPp3gFnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
auru	aura	k1gFnSc4	aura
zázračnosti	zázračnost	k1gFnSc2	zázračnost
a	a	k8xC	a
nadpozemskosti	nadpozemskost	k1gFnSc2	nadpozemskost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xC	jako
nedosažitelná	dosažitelný	k2eNgFnSc1d1	nedosažitelná
modla	modla	k1gFnSc1	modla
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vládkyní	vládkyně	k1gFnSc7	vládkyně
nad	nad	k7c7	nad
životem	život	k1gInSc7	život
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tragickým	tragický	k2eAgMnSc7d1	tragický
hrdinou	hrdina	k1gMnSc7	hrdina
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
hokynář	hokynář	k1gMnSc1	hokynář
Alfréd	Alfréd	k1gMnSc1	Alfréd
Ill	Ill	k1gMnSc1	Ill
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vlastně	vlastně	k9	vlastně
viník	viník	k1gMnSc1	viník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
nevyhnutelnému	vyhnutelný	k2eNgInSc3d1	nevyhnutelný
osudu	osud	k1gInSc3	osud
vzdoruje	vzdorovat	k5eAaImIp3nS	vzdorovat
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
názorem	názor	k1gInSc7	názor
a	a	k8xC	a
chováním	chování	k1gNnSc7	chování
získává	získávat	k5eAaImIp3nS	získávat
tragickou	tragický	k2eAgFnSc4d1	tragická
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
evokuje	evokovat	k5eAaBmIp3nS	evokovat
žánr	žánr	k1gInSc4	žánr
antické	antický	k2eAgFnSc2d1	antická
tragédie	tragédie	k1gFnSc2	tragédie
především	především	k9	především
chór	chór	k1gInSc1	chór
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
přednáší	přednášet	k5eAaImIp3nS	přednášet
cynický	cynický	k2eAgInSc4d1	cynický
komentář	komentář	k1gInSc4	komentář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
velebí	velebit	k5eAaImIp3nS	velebit
"	"	kIx"	"
<g/>
svaté	svatý	k2eAgNnSc1d1	svaté
dobro	dobro	k1gNnSc1	dobro
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
propojuje	propojovat	k5eAaImIp3nS	propojovat
motiv	motiv	k1gInSc1	motiv
"	"	kIx"	"
<g/>
peníze	peníz	k1gInPc1	peníz
znamenají	znamenat	k5eAaImIp3nP	znamenat
moc	moc	k6eAd1	moc
<g/>
"	"	kIx"	"
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
dürrenmattovskou	dürrenmattovský	k2eAgFnSc7d1	dürrenmattovský
"	"	kIx"	"
<g/>
kritikou	kritika	k1gFnSc7	kritika
západní	západní	k2eAgFnSc2d1	západní
společnosti	společnost	k1gFnSc2	společnost
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
"	"	kIx"	"
a	a	k8xC	a
topoi	topoi	k1gInSc4	topoi
řeckých	řecký	k2eAgFnPc2d1	řecká
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Osud	osud	k1gInSc1	osud
a	a	k8xC	a
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
vina	vina	k1gFnSc1	vina
a	a	k8xC	a
trest	trest	k1gInSc1	trest
<g/>
,	,	kIx,	,
pomsta	pomsta	k1gFnSc1	pomsta
a	a	k8xC	a
oběť	oběť	k1gFnSc1	oběť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
téma	téma	k1gNnSc1	téma
prodejnosti	prodejnost	k1gFnSc2	prodejnost
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
stává	stávat	k5eAaImIp3nS	stávat
sarkastickou	sarkastický	k2eAgFnSc7d1	sarkastická
groteskou	groteska	k1gFnSc7	groteska
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
představeni	představen	k2eAgMnPc1d1	představen
jakožto	jakožto	k8xS	jakožto
"	"	kIx"	"
<g/>
počestní	počestný	k2eAgMnPc1d1	počestný
občané	občan	k1gMnPc1	občan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
začnou	začít	k5eAaPmIp3nP	začít
podléhat	podléhat	k5eAaImF	podléhat
lákání	lákání	k1gNnSc4	lákání
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
ukážou	ukázat	k5eAaPmIp3nP	ukázat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
směšné	směšný	k2eAgFnPc1d1	směšná
figurky	figurka	k1gFnPc1	figurka
<g/>
.	.	kIx.	.
</s>
<s>
Lháři	lhář	k1gMnPc1	lhář
<g/>
,	,	kIx,	,
hamižní	hamižný	k2eAgMnPc1d1	hamižný
podvodníci	podvodník	k1gMnPc1	podvodník
a	a	k8xC	a
pustí	pustý	k2eAgMnPc1d1	pustý
žvanilové	žvanil	k1gMnPc1	žvanil
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
typickým	typický	k2eAgFnPc3d1	typická
postavám	postava	k1gFnPc3	postava
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
nim	on	k3xPp3gInPc3	on
stojí	stát	k5eAaImIp3nS	stát
osud	osud	k1gInSc1	osud
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
uznává	uznávat	k5eAaImIp3nS	uznávat
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
nést	nést	k5eAaImF	nést
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Ill	Ill	k1gMnSc1	Ill
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
vůči	vůči	k7c3	vůči
bezduché	bezduchý	k2eAgFnSc3d1	bezduchá
mase	masa	k1gFnSc3	masa
jediným	jediný	k2eAgMnSc7d1	jediný
morálním	morální	k2eAgMnSc7d1	morální
člověkem	člověk	k1gMnSc7	člověk
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
náhodou	náhodou	k6eAd1	náhodou
se	se	k3xPyFc4	se
městečko	městečko	k1gNnSc1	městečko
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
Güllen	Güllen	k1gInSc4	Güllen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Gülle	Gülle	k1gFnSc1	Gülle
-	-	kIx~	-
kejda	kejda	k1gFnSc1	kejda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
slova	slovo	k1gNnPc1	slovo
skrývají	skrývat	k5eAaImIp3nP	skrývat
další	další	k2eAgInPc1d1	další
významy	význam	k1gInPc1	význam
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
samotné	samotný	k2eAgNnSc1d1	samotné
jméno	jméno	k1gNnSc1	jméno
Zachanassian	Zachanassiana	k1gFnPc2	Zachanassiana
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
díla	dílo	k1gNnSc2	dílo
známého	známý	k1gMnSc2	známý
miliardáře	miliardář	k1gMnSc2	miliardář
Basil	Basil	k1gMnSc1	Basil
Zaharoffa	Zaharoff	k1gMnSc2	Zaharoff
<g/>
,	,	kIx,	,
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
Onassise	Onassise	k1gFnSc2	Onassise
a	a	k8xC	a
Calouste	Calouste	k1gMnSc2	Calouste
Gulbenkiana	Gulbenkian	k1gMnSc2	Gulbenkian
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Návštěva	návštěva	k1gFnSc1	návštěva
<g/>
"	"	kIx"	"
–	–	k?	–
hollywoodské	hollywoodský	k2eAgNnSc1d1	hollywoodské
zpracování	zpracování	k1gNnSc1	zpracování
režiséra	režisér	k1gMnSc2	režisér
Bernharda	Bernhard	k1gMnSc2	Bernhard
Wickiho	Wicki	k1gMnSc2	Wicki
z	z	k7c2	z
r.	r.	kA	r.
1963	[number]	k4	1963
s	s	k7c7	s
Ingrid	Ingrid	k1gFnSc7	Ingrid
Bergman	Bergman	k1gMnSc1	Bergman
a	a	k8xC	a
Anthony	Anthon	k1gMnPc4	Anthon
Quinnem	Quinno	k1gNnSc7	Quinno
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dürrenmatt	Dürrenmatt	k1gMnSc1	Dürrenmatt
se	se	k3xPyFc4	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
údajně	údajně	k6eAd1	údajně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podívat	podívat	k5eAaPmF	podívat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Opera	opera	k1gFnSc1	opera
–	–	k?	–
libreto	libreto	k1gNnSc1	libreto
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Dürrenmatt	Dürrenmatt	k1gInSc4	Dürrenmatt
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgInS	složit
Gottfried	Gottfried	k1gInSc1	Gottfried
von	von	k1gInSc1	von
Einem	Einem	k1gInSc1	Einem
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc1	inscenace
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
J.	J.	kA	J.
Bohdalovou	Bohdalová	k1gFnSc4	Bohdalová
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Z.	Z.	kA	Z.
Zelenka	zelenka	k1gFnSc1	zelenka
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Inscenace	inscenace	k1gFnSc1	inscenace
Klicperova	Klicperův	k2eAgNnSc2d1	Klicperovo
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Morávka	Morávek	k1gMnSc2	Morávek
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
hradecká	hradecký	k2eAgFnSc1d1	hradecká
rodačka	rodačka	k1gFnSc1	rodačka
<g/>
,	,	kIx,	,
světová	světový	k2eAgFnSc1d1	světová
operní	operní	k2eAgFnSc1d1	operní
diva	diva	k1gFnSc1	diva
Soňa	Soňa	k1gFnSc1	Soňa
Červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
autorsky	autorsky	k6eAd1	autorsky
upravená	upravený	k2eAgFnSc1d1	upravená
divadelní	divadelní	k2eAgFnSc1d1	divadelní
inscenace	inscenace	k1gFnSc1	inscenace
souboru	soubor	k1gInSc2	soubor
Depresivní	depresivní	k2eAgFnPc1d1	depresivní
děti	dítě	k1gFnPc1	dítě
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
penězích	peníze	k1gInPc6	peníze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
Güllen	Güllen	k1gInSc4	Güllen
přejmenován	přejmenován	k2eAgInSc4d1	přejmenován
na	na	k7c4	na
Dalskabáty	Dalskabáta	k1gFnPc4	Dalskabáta
a	a	k8xC	a
představení	představení	k1gNnPc4	představení
nese	nést	k5eAaImIp3nS	nést
podtitul	podtitul	k1gInSc4	podtitul
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
güllenské	güllenský	k2eAgFnPc1d1	güllenský
postavy	postava	k1gFnPc1	postava
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
svými	svůj	k3xOyFgInPc7	svůj
českými	český	k2eAgInPc7d1	český
vzory	vzor	k1gInPc7	vzor
(	(	kIx(	(
<g/>
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
např.	např.	kA	např.
Dorota	Dorota	k1gFnSc1	Dorota
Máchalová	Máchalová	k1gFnSc1	Máchalová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Sepéši	Sepéše	k1gFnSc4	Sepéše
<g/>
,	,	kIx,	,
Deana	Deaen	k2eAgFnSc1d1	Deana
Jakubisková-Horváthová	Jakubisková-Horváthová	k1gFnSc1	Jakubisková-Horváthová
nebo	nebo	k8xC	nebo
Antonín	Antonín	k1gMnSc1	Antonín
Koniáš	Koniáš	k1gMnSc1	Koniáš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
je	být	k5eAaImIp3nS	být
inscenováno	inscenovat	k5eAaBmNgNnS	inscenovat
ve	v	k7c6	v
zchátralé	zchátralý	k2eAgFnSc6d1	zchátralá
funkcionalistické	funkcionalistický	k2eAgFnSc6d1	funkcionalistická
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Čelákovicích	Čelákovice	k1gFnPc6	Čelákovice
<g/>
.	.	kIx.	.
</s>
<s>
Goertz	Goertz	k1gMnSc1	Goertz
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dürrenmatt	Dürrenmatt	k1gInSc1	Dürrenmatt
</s>
