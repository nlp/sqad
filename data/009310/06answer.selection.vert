<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
ve	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
název	název	k1gInSc4	název
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
státní	státní	k2eAgFnSc4d1	státní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
formu	forma	k1gFnSc4	forma
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
