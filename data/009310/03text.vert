<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
ve	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
název	název	k1gInSc4	název
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
státní	státní	k2eAgFnSc4d1	státní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
formu	forma	k1gFnSc4	forma
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
==	==	k?	==
</s>
</p>
<p>
<s>
Doklady	doklad	k1gInPc1	doklad
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
středověku	středověk	k1gInSc2	středověk
i	i	k8xC	i
novověkého	novověký	k2eAgInSc2d1	novověký
feudalismu	feudalismus	k1gInSc2	feudalismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1234	[number]	k4	1234
a	a	k8xC	a
1250	[number]	k4	1250
jsou	být	k5eAaImIp3nP	být
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
zubrů	zubr	k1gMnPc2	zubr
evropských	evropský	k2eAgMnPc2d1	evropský
v	v	k7c6	v
Badinském	Badinský	k2eAgInSc6d1	Badinský
pralese	prales	k1gInSc6	prales
u	u	k7c2	u
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
i	i	k8xC	i
evropské	evropský	k2eAgNnSc1d1	Evropské
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uváděn	k2eAgInSc1d1	uváděn
Žofínský	žofínský	k2eAgInSc1d1	žofínský
prales	prales	k1gInSc1	prales
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Hojnou	hojný	k2eAgFnSc4d1	hojná
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
panství	panství	k1gNnSc6	panství
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
Buquoyové	Buquoyová	k1gFnSc2	Buquoyová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Schwarzenbergové	Schwarzenberg	k1gMnPc1	Schwarzenberg
Boubínský	boubínský	k2eAgInSc4d1	boubínský
prales	prales	k1gInSc4	prales
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
první	první	k4xOgFnSc1	první
chráněná	chráněný	k2eAgFnSc1d1	chráněná
území	území	k1gNnSc6	území
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Kvetnica	Kvetnica	k1gMnSc1	Kvetnica
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
Ponická	Ponický	k2eAgFnSc1d1	Ponická
dúbrava	dúbrava	k1gFnSc1	dúbrava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Szabóovy	Szabóův	k2eAgFnSc2d1	Szabóův
skály	skála	k1gFnSc2	skála
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
a	a	k8xC	a
Dobročský	Dobročský	k2eAgInSc1d1	Dobročský
prales	prales	k1gInSc1	prales
a	a	k8xC	a
Badínský	Badínský	k2eAgInSc1d1	Badínský
prales	prales	k1gInSc1	prales
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
speciálním	speciální	k2eAgInSc7d1	speciální
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zákon	zákon	k1gInSc1	zákon
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1955	[number]	k4	1955
Zb	Zb	k1gFnPc2	Zb
<g/>
.	.	kIx.	.
</s>
<s>
SNR	SNR	kA	SNR
<g/>
,	,	kIx,	,
o	o	k7c6	o
štátnej	štátnat	k5eAaImRp2nS	štátnat
ochrane	ochran	k1gInSc5	ochran
prírody	príroda	k1gFnSc2	príroda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
novelizován	novelizovat	k5eAaBmNgInS	novelizovat
zákony	zákon	k1gInPc4	zákon
SNR	SNR	kA	SNR
č.	č.	k?	č.
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
Zb	Zb	k1gFnPc2	Zb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
Zb	Zb	k1gFnPc2	Zb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
1986	[number]	k4	1986
Zb	Zb	k1gFnPc2	Zb
<g/>
.	.	kIx.	.
a	a	k8xC	a
128	[number]	k4	128
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Zb	Zb	k1gFnPc2	Zb
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgInPc4d1	český
kraje	kraj	k1gInPc4	kraj
Československa	Československo	k1gNnSc2	Československo
poté	poté	k6eAd1	poté
přijalo	přijmout	k5eAaPmAgNnS	přijmout
československé	československý	k2eAgNnSc4d1	Československé
Národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
československý	československý	k2eAgInSc1d1	československý
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
federalizaci	federalizace	k1gFnSc6	federalizace
státu	stát	k1gInSc2	stát
novelizován	novelizován	k2eAgInSc1d1	novelizován
zákony	zákon	k1gInPc7	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
146	[number]	k4	146
<g/>
/	/	kIx~	/
<g/>
1971	[number]	k4	1971
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
137	[number]	k4	137
<g/>
/	/	kIx~	/
<g/>
1982	[number]	k4	1982
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
č.	č.	k?	č.
96	[number]	k4	96
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
č.	č.	k?	č.
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1986	[number]	k4	1986
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
byly	být	k5eAaImAgInP	být
prvními	první	k4xOgFnPc7	první
speciálními	speciální	k2eAgFnPc7d1	speciální
normami	norma	k1gFnPc7	norma
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
proto	proto	k8xC	proto
nerušily	rušit	k5eNaImAgFnP	rušit
žádnou	žádný	k3yNgFnSc4	žádný
dřívější	dřívější	k2eAgFnSc4d1	dřívější
právní	právní	k2eAgFnSc4d1	právní
úpravu	úprava	k1gFnSc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
přírody	příroda	k1gFnSc2	příroda
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
chráněny	chráněn	k2eAgInPc4d1	chráněn
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
státu	stát	k1gInSc2	stát
s	s	k7c7	s
vlastníkem	vlastník	k1gMnSc7	vlastník
či	či	k8xC	či
uživatelem	uživatel	k1gMnSc7	uživatel
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnSc4	území
přešla	přejít	k5eAaPmAgFnS	přejít
ochrana	ochrana	k1gFnSc1	ochrana
podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
zavedly	zavést	k5eAaPmAgInP	zavést
formální	formální	k2eAgNnSc4d1	formální
rozlišení	rozlišení	k1gNnSc4	rozlišení
typů	typ	k1gInPc2	typ
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
velkoplošná	velkoplošný	k2eAgNnPc1d1	velkoplošné
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
jako	jako	k8xC	jako
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
a	a	k8xC	a
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
přijala	přijmout	k5eAaPmAgFnS	přijmout
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
nový	nový	k2eAgInSc1d1	nový
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Československé	československý	k2eAgFnSc2d1	Československá
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
úprava	úprava	k1gFnSc1	úprava
přijata	přijmout	k5eAaPmNgFnS	přijmout
zákonem	zákon	k1gInSc7	zákon
287	[number]	k4	287
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Z.	Z.	kA	Z.
z.	z.	k?	z.
(	(	kIx(	(
<g/>
novely	novela	k1gFnSc2	novela
222	[number]	k4	222
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
,	,	kIx,	,
211	[number]	k4	211
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
,	,	kIx,	,
416	[number]	k4	416
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
,	,	kIx,	,
553	[number]	k4	553
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
,	,	kIx,	,
237	[number]	k4	237
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Z.	Z.	kA	Z.
z.	z.	k?	z.
a	a	k8xC	a
261	[number]	k4	261
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
543	[number]	k4	543
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Z.	Z.	kA	Z.
z.	z.	k?	z.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochrane	ochran	k1gInSc5	ochran
prírody	príroda	k1gFnSc2	príroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
různé	různý	k2eAgFnPc1d1	různá
kategorie	kategorie	k1gFnPc1	kategorie
a	a	k8xC	a
zóny	zóna	k1gFnPc1	zóna
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
stupňů	stupeň	k1gInPc2	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
spadá	spadat	k5eAaImIp3nS	spadat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
vyšší	vysoký	k2eAgInSc1d2	vyšší
stupeň	stupeň	k1gInSc1	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
zákon	zákon	k1gInSc1	zákon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nese	nést	k5eAaImIp3nS	nést
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
a	a	k8xC	a
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jej	on	k3xPp3gMnSc4	on
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
několikrát	několikrát	k6eAd1	několikrát
novelizoval	novelizovat	k5eAaBmAgMnS	novelizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
zákon	zákon	k1gInSc1	zákon
určuje	určovat	k5eAaImIp3nS	určovat
jednak	jednak	k8xC	jednak
obecné	obecný	k2eAgNnSc4d1	obecné
zásady	zásada	k1gFnPc4	zásada
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
-	-	kIx~	-
ochranu	ochrana	k1gFnSc4	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
geologických	geologický	k2eAgMnPc2d1	geologický
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paleontologických	paleontologický	k2eAgInPc2d1	paleontologický
nálezů	nález	k1gInPc2	nález
i	i	k9	i
ochranu	ochrana	k1gFnSc4	ochrana
krajinného	krajinný	k2eAgInSc2d1	krajinný
rázu	ráz	k1gInSc2	ráz
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
definuje	definovat	k5eAaBmIp3nS	definovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
zvláště	zvláště	k6eAd1	zvláště
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
-	-	kIx~	-
např.	např.	kA	např.
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
povinnosti	povinnost	k1gFnPc4	povinnost
fyzických	fyzický	k2eAgFnPc2d1	fyzická
a	a	k8xC	a
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
orgány	orgán	k1gInPc4	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
část	část	k1gFnSc1	část
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
i	i	k9	i
soustavě	soustava	k1gFnSc3	soustava
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
jeho	jeho	k3xOp3gNnPc1	jeho
ustanovení	ustanovení	k1gNnPc1	ustanovení
mohou	moct	k5eAaImIp3nP	moct
kolidovat	kolidovat	k5eAaImF	kolidovat
s	s	k7c7	s
Listinou	listina	k1gFnSc7	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
citelně	citelně	k6eAd1	citelně
omezuje	omezovat	k5eAaImIp3nS	omezovat
například	například	k6eAd1	například
vlastnická	vlastnický	k2eAgNnPc4d1	vlastnické
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rozsah	rozsah	k1gInSc4	rozsah
vlastnických	vlastnický	k2eAgNnPc2d1	vlastnické
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
formy	forma	k1gFnSc2	forma
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
(	(	kIx(	(
<g/>
právnická	právnický	k2eAgFnSc1d1	právnická
versus	versus	k7c1	versus
fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
nález	nález	k1gInSc1	nález
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
§	§	k?	§
68	[number]	k4	68
<g/>
,	,	kIx,	,
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
3	[number]	k4	3
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
názor	názor	k1gInSc4	názor
předkladatelů	předkladatel	k1gMnPc2	předkladatel
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
že	že	k8xS	že
napadená	napadený	k2eAgNnPc1d1	napadené
ustanovení	ustanovení	k1gNnPc1	ustanovení
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
přes	přes	k7c4	přes
možnost	možnost	k1gFnSc4	možnost
jejich	jejich	k3xOp3gInSc2	jejich
ústavně	ústavně	k6eAd1	ústavně
konformního	konformní	k2eAgInSc2d1	konformní
výkladu	výklad	k1gInSc2	výklad
formulována	formulovat	k5eAaImNgFnS	formulovat
příliš	příliš	k6eAd1	příliš
nekonkrétně	konkrétně	k6eNd1	konkrétně
a	a	k8xC	a
široce	široko	k6eAd1	široko
<g/>
,	,	kIx,	,
a	a	k8xC	a
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
změněna	změněn	k2eAgFnSc1d1	změněna
při	při	k7c6	při
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
budoucích	budoucí	k2eAgFnPc2d1	budoucí
úprav	úprava	k1gFnPc2	úprava
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhová	druhový	k2eAgFnSc1d1	druhová
ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
chráněny	chráněn	k2eAgInPc4d1	chráněn
před	před	k7c7	před
zničením	zničení	k1gNnSc7	zničení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
uvedené	uvedený	k2eAgInPc4d1	uvedený
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
platí	platit	k5eAaImIp3nS	platit
konkrétnější	konkrétní	k2eAgFnSc1d2	konkrétnější
a	a	k8xC	a
přísnější	přísný	k2eAgFnSc1d2	přísnější
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgInPc1d1	chráněný
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
přílohy	příloha	k1gFnSc2	příloha
této	tento	k3xDgFnSc2	tento
vyhlášky	vyhláška	k1gFnSc2	vyhláška
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgMnPc4d1	ohrožený
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
a	a	k8xC	a
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kategorie	kategorie	k1gFnPc1	kategorie
nemusí	muset	k5eNaImIp3nP	muset
vždy	vždy	k6eAd1	vždy
odpovídat	odpovídat	k5eAaImF	odpovídat
stupni	stupeň	k1gInSc3	stupeň
ohrožení	ohrožení	k1gNnSc2	ohrožení
v	v	k7c6	v
aktuálním	aktuální	k2eAgInSc6d1	aktuální
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územní	územní	k2eAgFnSc1d1	územní
ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
zvláště	zvláště	k6eAd1	zvláště
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
na	na	k7c4	na
velkoplošná	velkoplošný	k2eAgNnPc4d1	velkoplošné
a	a	k8xC	a
maloplošná	maloplošný	k2eAgNnPc4d1	maloplošné
<g/>
.	.	kIx.	.
</s>
<s>
Velkoplošná	velkoplošný	k2eAgNnPc1d1	velkoplošné
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kategorie	kategorie	k1gFnPc1	kategorie
národní	národní	k2eAgFnPc1d1	národní
park	park	k1gInSc4	park
a	a	k8xC	a
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
maloplošná	maloplošný	k2eAgFnSc1d1	maloplošná
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
stupeň	stupeň	k1gInSc4	stupeň
ochrany	ochrana	k1gFnSc2	ochrana
každé	každý	k3xTgFnSc2	každý
kategorie	kategorie	k1gFnSc2	kategorie
a	a	k8xC	a
podmínky	podmínka	k1gFnPc1	podmínka
jejich	jejich	k3xOp3gNnSc2	jejich
využívání	využívání	k1gNnSc2	využívání
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
<g/>
,	,	kIx,	,
CHKO	CHKO	kA	CHKO
<g/>
,	,	kIx,	,
NPR	NPR	kA	NPR
a	a	k8xC	a
NPP	NPP	kA	NPP
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
PP	PP	kA	PP
a	a	k8xC	a
PR	pr	k0	pr
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
velkoplošných	velkoplošný	k2eAgInPc2d1	velkoplošný
ZCHÚ	ZCHÚ	kA	ZCHÚ
je	být	k5eAaImIp3nS	být
péče	péče	k1gFnSc1	péče
o	o	k7c6	o
ZCHÚ	ZCHÚ	kA	ZCHÚ
svěřena	svěřit	k5eAaPmNgFnS	svěřit
jejich	jejich	k3xOp3gFnPc3	jejich
správám	správa	k1gFnPc3	správa
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gMnPc4	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
NPR	NPR	kA	NPR
a	a	k8xC	a
NPP	NPP	kA	NPP
je	být	k5eAaImIp3nS	být
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Agentura	agentura	k1gFnSc1	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
u	u	k7c2	u
PP	PP	kA	PP
a	a	k8xC	a
PR	pr	k0	pr
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Management	management	k1gInSc1	management
a	a	k8xC	a
využívání	využívání	k1gNnSc1	využívání
ZCHÚ	ZCHÚ	kA	ZCHÚ
jsou	být	k5eAaImIp3nP	být
prováděny	provádět	k5eAaImNgInP	provádět
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schvalovány	schvalován	k2eAgFnPc1d1	schvalována
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ZCHÚ	ZCHÚ	kA	ZCHÚ
je	být	k5eAaImIp3nS	být
také	také	k9	také
do	do	k7c2	do
územní	územní	k2eAgFnSc2d1	územní
ochrany	ochrana	k1gFnSc2	ochrana
řazen	řazen	k2eAgInSc4d1	řazen
územní	územní	k2eAgInSc4d1	územní
systém	systém	k1gInSc4	systém
ekologické	ekologický	k2eAgFnSc2d1	ekologická
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
krajinný	krajinný	k2eAgInSc1d1	krajinný
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
přechodně	přechodně	k6eAd1	přechodně
chráněná	chráněný	k2eAgFnSc1d1	chráněná
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
dřeviny	dřevina	k1gFnPc1	dřevina
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
mimo	mimo	k7c4	mimo
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc1	jeskyně
a	a	k8xC	a
paleontologický	paleontologický	k2eAgInSc1d1	paleontologický
nález	nález	k1gInSc1	nález
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
chráněným	chráněný	k2eAgInSc7d1	chráněný
zájmem	zájem	k1gInSc7	zájem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
a	a	k8xC	a
garantuje	garantovat	k5eAaBmIp3nS	garantovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
veřejná	veřejný	k2eAgFnSc1d1	veřejná
přístupnost	přístupnost	k1gFnSc1	přístupnost
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
tak	tak	k6eAd1	tak
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
základní	základní	k2eAgFnSc4d1	základní
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
prostupnost	prostupnost	k1gFnSc4	prostupnost
zejména	zejména	k9	zejména
přírodního	přírodní	k2eAgNnSc2d1	přírodní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaného	využívaný	k2eAgNnSc2d1	využívané
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ukládá	ukládat	k5eAaImIp3nS	ukládat
orgánům	orgán	k1gMnPc3	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
povinnost	povinnost	k1gFnSc1	povinnost
evidovat	evidovat	k5eAaImF	evidovat
stezky	stezka	k1gFnSc2	stezka
a	a	k8xC	a
pěšiny	pěšina	k1gFnSc2	pěšina
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
regulovat	regulovat	k5eAaImF	regulovat
jejich	jejich	k3xOp3gNnSc4	jejich
zřizování	zřizování	k1gNnSc4	zřizování
či	či	k8xC	či
rušení	rušení	k1gNnSc4	rušení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Orgány	orgán	k1gInPc1	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
===	===	k?	===
</s>
</p>
<p>
<s>
Těmito	tento	k3xDgInPc7	tento
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
pověřený	pověřený	k2eAgInSc1d1	pověřený
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgInSc1d1	obecní
úřad	úřad	k1gInSc1	úřad
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
a	a	k8xC	a
chráněných	chráněný	k2eAgFnPc2d1	chráněná
krajinných	krajinný	k2eAgFnPc2d1	krajinná
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
inspekce	inspekce	k1gFnSc1	inspekce
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
újezd	újezd	k1gInSc1	újezd
a	a	k8xC	a
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
MŽP	MŽP	kA	MŽP
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
mj.	mj.	kA	mj.
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
vrchní	vrchní	k2eAgInSc1d1	vrchní
dozor	dozor	k1gInSc1	dozor
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
ČIŽP	ČIŽP	kA	ČIŽP
mj.	mj.	kA	mj.
dozírá	dozírat	k5eAaImIp3nS	dozírat
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
rozhodování	rozhodování	k1gNnSc2	rozhodování
správních	správní	k2eAgInPc2d1	správní
orgánů	orgán	k1gInPc2	orgán
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oprávněna	oprávnit	k5eAaPmNgFnS	oprávnit
ukládat	ukládat	k5eAaImF	ukládat
opatření	opatření	k1gNnSc4	opatření
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
škod	škoda	k1gFnPc2	škoda
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
na	na	k7c6	na
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
zákonům	zákon	k1gInPc3	zákon
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
zákonům	zákon	k1gInPc3	zákon
o	o	k7c6	o
lesích	lese	k1gFnPc6	lese
<g/>
,	,	kIx,	,
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
územním	územní	k2eAgNnSc6d1	územní
plánování	plánování	k1gNnSc6	plánování
a	a	k8xC	a
stavebním	stavební	k2eAgInSc6d1	stavební
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
myslivosti	myslivost	k1gFnSc2	myslivost
a	a	k8xC	a
rybářství	rybářství	k1gNnSc2	rybářství
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
předpisy	předpis	k1gInPc4	předpis
vydané	vydaný	k2eAgInPc4d1	vydaný
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
provádění	provádění	k1gNnSc2	provádění
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
např.	např.	kA	např.
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
orgánu	orgán	k1gInSc2	orgán
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
nelze	lze	k6eNd1	lze
vydat	vydat	k5eAaPmF	vydat
povolení	povolení	k1gNnSc4	povolení
ke	k	k7c3	k
stavební	stavební	k2eAgFnSc3d1	stavební
činnosti	činnost	k1gFnSc3	činnost
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
k	k	k7c3	k
větším	veliký	k2eAgInPc3d2	veliký
zásahům	zásah	k1gInPc3	zásah
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
k	k	k7c3	k
zásahu	zásah	k1gInSc3	zásah
do	do	k7c2	do
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
půdního	půdní	k2eAgInSc2d1	půdní
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
k	k	k7c3	k
myslivosti	myslivost	k1gFnSc3	myslivost
a	a	k8xC	a
rybářství	rybářství	k1gNnSc3	rybářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČESKO	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
123	[number]	k4	123
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
zákonů	zákon	k1gInPc2	zákon
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
částka	částka	k1gFnSc1	částka
44	[number]	k4	44
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1258	[number]	k4	1258
<g/>
–	–	k?	–
<g/>
1288	[number]	k4	1288
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1211	[number]	k4	1211
<g/>
-	-	kIx~	-
<g/>
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
https://www.zakonyprolidi.cz/cs/2017-123	[url]	k4	https://www.zakonyprolidi.cz/cs/2017-123
</s>
</p>
<p>
<s>
ČESKO	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
114	[number]	k4	114
ze	z	k7c2	z
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
zákonů	zákon	k1gInPc2	zákon
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
částka	částka	k1gFnSc1	částka
28	[number]	k4	28
<g/>
,	,	kIx,	,
s.	s.	k?	s.
666	[number]	k4	666
<g/>
–	–	k?	–
<g/>
692	[number]	k4	692
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1211	[number]	k4	1211
<g/>
-	-	kIx~	-
<g/>
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
https://www.zakonyprolidi.cz/cs/1992-114	[url]	k4	https://www.zakonyprolidi.cz/cs/1992-114
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Směrnice	směrnice	k1gFnSc1	směrnice
o	o	k7c6	o
stanovištích	stanoviště	k1gNnPc6	stanoviště
</s>
</p>
<p>
<s>
Směrnice	směrnice	k1gFnPc1	směrnice
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Platné	platný	k2eAgNnSc1d1	platné
a	a	k8xC	a
účinné	účinný	k2eAgNnSc1d1	účinné
znění	znění	k1gNnSc1	znění
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
na	na	k7c6	na
ZákonyProLidi	ZákonyProLid	k1gMnPc1	ZákonyProLid
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
znění	znění	k1gNnSc1	znění
zákona	zákon	k1gInSc2	zákon
na	na	k7c6	na
Portálu	portál	k1gInSc6	portál
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
stejnopis	stejnopis	k1gInSc1	stejnopis
Sbírky	sbírka	k1gFnSc2	sbírka
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
PDF	PDF	kA	PDF
na	na	k7c6	na
MVČR	MVČR	kA	MVČR
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
na	na	k7c4	na
PSP	PSP	kA	PSP
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
