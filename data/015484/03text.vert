<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc5
</s>
<s>
Logo	logo	k1gNnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc1
<g/>
,	,	kIx,
GNU	GNU	kA
GPL	GPL	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
„	„	k?
<g/>
obecná	obecný	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
licence	licence	k1gFnSc1
GNU	gnu	k1gNnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
licence	licence	k1gFnSc1
pro	pro	k7c4
svobodný	svobodný	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
,	,	kIx,
původně	původně	k6eAd1
napsaná	napsaný	k2eAgFnSc1d1
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
pro	pro	k7c4
projekt	projekt	k1gInSc4
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPL	GPL	kA
je	být	k5eAaImIp3nS
nejpopulárnějším	populární	k2eAgNnSc7d3
a	a	k8xC
dobře	dobře	k6eAd1
známým	známý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
silně	silně	k6eAd1
copyleftové	copyleftový	k2eAgFnSc2d1
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgNnP
odvozená	odvozený	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
dostupná	dostupný	k2eAgFnSc1d1
pod	pod	k7c7
toutéž	týž	k3xTgFnSc7
licencí	licence	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
filosofie	filosofie	k1gFnSc2
je	být	k5eAaImIp3nS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
poskytuje	poskytovat	k5eAaImIp3nS
uživatelům	uživatel	k1gMnPc3
počítačového	počítačový	k2eAgInSc2d1
programu	program	k1gInSc2
práva	právo	k1gNnSc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
a	a	k8xC
používá	používat	k5eAaImIp3nS
copyleft	copyleft	k5eAaPmF
k	k	k7c3
zajištění	zajištění	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
svobody	svoboda	k1gFnPc1
ochráněny	ochráněn	k2eAgFnPc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
dílo	dílo	k1gNnSc4
změněno	změnit	k5eAaPmNgNnS
nebo	nebo	k8xC
k	k	k7c3
něčemu	něco	k3yInSc3
přidáno	přidán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
rozdíl	rozdíl	k1gInSc4
oproti	oproti	k7c3
permisivním	permisivní	k2eAgFnPc3d1
licencím	licence	k1gFnPc3
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
typickým	typický	k2eAgInSc7d1
případem	případ	k1gInSc7
jsou	být	k5eAaImIp3nP
BSD	BSD	kA
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc1
(	(	kIx(
<g/>
LGPL	LGPL	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
upravená	upravený	k2eAgFnSc1d1
<g/>
,	,	kIx,
permisivnější	permisivný	k2eAgFnSc1d2
verze	verze	k1gFnSc1
GPL	GPL	kA
<g/>
,	,	kIx,
původně	původně	k6eAd1
zamýšlená	zamýšlený	k2eAgFnSc1d1
pro	pro	k7c4
některé	některý	k3yIgFnPc4
knihovny	knihovna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
také	také	k9
GNU	gnu	k1gNnSc1
Free	Fre	k1gInSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
dokumentaci	dokumentace	k1gFnSc4
k	k	k7c3
softwaru	software	k1gInSc3
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ale	ale	k9
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
použita	použít	k5eAaPmNgFnS
i	i	k9
jinde	jinde	k6eAd1
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
projektu	projekt	k1gInSc6
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Affero	Affero	k1gNnSc1
General	General	k1gFnSc2
Public	publicum	k1gNnPc2
License	License	k1gFnSc1
(	(	kIx(
<g/>
GNU	gnu	k1gMnSc1
AGPL	AGPL	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
licence	licence	k1gFnSc1
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
síťový	síťový	k2eAgInSc4d1
serverový	serverový	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNU	gnu	k1gNnSc1
AGPL	AGPL	kA
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
GNU	gnu	k1gNnSc1
General	General	k1gFnSc2
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
však	však	k9
pokrývá	pokrývat	k5eAaImIp3nS
také	také	k9
užití	užití	k1gNnSc1
softwaru	software	k1gInSc2
přes	přes	k7c4
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
požaduje	požadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
síťovému	síťový	k2eAgMnSc3d1
uživateli	uživatel	k1gMnSc3
zpřístupněn	zpřístupněn	k2eAgInSc4d1
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
díla	dílo	k1gNnSc2
licencovaného	licencovaný	k2eAgNnSc2d1
pod	pod	k7c7
AGPL	AGPL	kA
<g/>
,	,	kIx,
například	například	k6eAd1
webové	webový	k2eAgFnSc6d1
aplikaci	aplikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
doporučuje	doporučovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
použití	použití	k1gNnSc1
této	tento	k3xDgFnSc2
licence	licence	k1gFnSc2
zvažováno	zvažovat	k5eAaImNgNnS
pro	pro	k7c4
jakýkoli	jakýkoli	k3yIgInSc4
software	software	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
používá	používat	k5eAaImIp3nS
přes	přes	k7c4
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Licence	licence	k1gFnSc1
GPL	GPL	kA
byla	být	k5eAaImAgFnS
napsána	napsat	k5eAaBmNgFnS,k5eAaPmNgFnS
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
pro	pro	k7c4
použití	použití	k1gNnSc4
s	s	k7c7
programy	program	k1gInPc7
poskytovanými	poskytovaný	k2eAgInPc7d1
jako	jako	k9
součást	součást	k1gFnSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
GPL	GPL	kA
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c4
unifikaci	unifikace	k1gFnSc4
podobných	podobný	k2eAgFnPc2d1
licencí	licence	k1gFnPc2
používaných	používaný	k2eAgFnPc2d1
pro	pro	k7c4
časné	časný	k2eAgFnPc4d1
verze	verze	k1gFnPc4
programů	program	k1gInPc2
GNU	gnu	k1gNnSc2
Emacs	Emacsa	k1gFnPc2
<g/>
,	,	kIx,
GNU	gnu	k1gMnSc1
Debugger	Debugger	k1gMnSc1
a	a	k8xC
GNU	gnu	k1gMnSc1
Compiler	Compiler	k1gMnSc1
Collection	Collection	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
licence	licence	k1gFnPc1
obsahovaly	obsahovat	k5eAaImAgFnP
podobné	podobný	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
jako	jako	k8xS,k8xC
moderní	moderní	k2eAgInPc1d1
GPL	GPL	kA
<g/>
,	,	kIx,
ale	ale	k8xC
byly	být	k5eAaImAgFnP
specifické	specifický	k2eAgFnPc1d1
pro	pro	k7c4
každý	každý	k3xTgInSc4
program	program	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
dělalo	dělat	k5eAaImAgNnS
nekompatibilní	kompatibilní	k2eNgInSc1d1
<g/>
,	,	kIx,
byť	byť	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
stejné	stejný	k2eAgFnPc4d1
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stallmanovým	Stallmanův	k2eAgInSc7d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
jedinou	jediný	k2eAgFnSc4d1
licenci	licence	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
použila	použít	k5eAaPmAgFnS
pro	pro	k7c4
jakýkoli	jakýkoli	k3yIgInSc4
projekt	projekt	k1gInSc4
a	a	k8xC
tedy	tedy	k9
by	by	k9
mnoho	mnoho	k4c1
projektů	projekt	k1gInPc2
mohlo	moct	k5eAaImAgNnS
sdílet	sdílet	k5eAaImF
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1
rozhodnutí	rozhodnutí	k1gNnSc1
ohledně	ohledně	k7c2
důvěry	důvěra	k1gFnSc2
v	v	k7c6
GPL	GPL	kA
přišlo	přijít	k5eAaPmAgNnS
od	od	k7c2
Linuse	Linuse	k1gFnSc2
Torvaldse	Torvalds	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
použil	použít	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
licenci	licence	k1gFnSc4
pro	pro	k7c4
linuxové	linuxový	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
přechodem	přechod	k1gInSc7
z	z	k7c2
dřívější	dřívější	k2eAgFnSc2d1
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zakazovala	zakazovat	k5eAaImAgFnS
komerční	komerční	k2eAgFnSc4d1
distribuci	distribuce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2007	#num#	k4
licenci	licence	k1gFnSc4
GPL	GPL	kA
používá	používat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
65	#num#	k4
%	%	kIx~
z	z	k7c2
43	#num#	k4
442	#num#	k4
projektů	projekt	k1gInPc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
v	v	k7c6
seznamu	seznam	k1gInSc6
na	na	k7c6
serveru	server	k1gInSc6
Freshmeat	Freshmeat	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
2006	#num#	k4
mělo	mít	k5eAaImAgNnS
tuto	tento	k3xDgFnSc4
licenci	licence	k1gFnSc4
okolo	okolo	k7c2
68	#num#	k4
%	%	kIx~
projektů	projekt	k1gInPc2
na	na	k7c4
SourceForge	SourceForge	k1gFnSc4
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
průzkum	průzkum	k1gInSc1
Red	Red	k1gFnSc2
Hat	hat	k0
Linuxu	linux	k1gInSc2
7.1	7.1	k4
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
50	#num#	k4
%	%	kIx~
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
je	být	k5eAaImIp3nS
licencováno	licencovat	k5eAaBmNgNnS
pod	pod	k7c4
GPL	GPL	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
průzkum	průzkum	k1gInSc1
MetaLab	MetaLaba	k1gFnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
největšího	veliký	k2eAgInSc2d3
archivu	archiv	k1gInSc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
projektů	projekt	k1gInPc2
má	mít	k5eAaImIp3nS
právě	právě	k9
tuto	tento	k3xDgFnSc4
licenci	licence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
průzkum	průzkum	k1gInSc1
velkého	velký	k2eAgInSc2d1
repozitáře	repozitář	k1gInSc2
open	open	k1gMnSc1
source	source	k1gMnSc1
software	software	k1gInSc4
ohlásil	ohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
červenci	červenec	k1gInSc6
1997	#num#	k4
je	být	k5eAaImIp3nS
okolo	okolo	k7c2
poloviny	polovina	k1gFnSc2
softwarových	softwarový	k2eAgInPc2d1
balíků	balík	k1gInPc2
s	s	k7c7
explicitními	explicitní	k2eAgFnPc7d1
licenčními	licenční	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
licencováno	licencovat	k5eAaBmNgNnS
pod	pod	k7c4
GPL	GPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
významné	významný	k2eAgInPc4d1
svobodné	svobodný	k2eAgInPc4d1
programy	program	k1gInPc4
pod	pod	k7c7
GPL	GPL	kA
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
linuxové	linuxový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
nebo	nebo	k8xC
GNU	gnu	k1gMnSc1
Compiler	Compiler	k1gMnSc1
Collection	Collection	k1gInSc1
(	(	kIx(
<g/>
GCC	GCC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některý	některý	k3yIgInSc1
další	další	k2eAgInSc1d1
software	software	k1gInSc1
je	být	k5eAaImIp3nS
duálně	duálně	k6eAd1
licencován	licencovat	k5eAaBmNgInS
s	s	k7c7
více	hodně	k6eAd2
různými	různý	k2eAgFnPc7d1
licencemi	licence	k1gFnPc7
<g/>
,	,	kIx,
často	často	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
bývá	bývat	k5eAaImIp3nS
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
pozorovatelé	pozorovatel	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
silný	silný	k2eAgInSc1d1
copyleft	copyleft	k1gInSc1
poskytovaný	poskytovaný	k2eAgInSc1d1
GPL	GPL	kA
byl	být	k5eAaImAgInS
klíčovým	klíčový	k2eAgMnSc7d1
pro	pro	k7c4
úspěch	úspěch	k1gInSc4
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
programátorům	programátor	k1gMnPc3
do	do	k7c2
něj	on	k3xPp3gNnSc2
přispívajícím	přispívající	k2eAgFnPc3d1
dal	dát	k5eAaPmAgMnS
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gNnSc2
díla	dílo	k1gNnSc2
přinesou	přinést	k5eAaPmIp3nP
užitek	užitek	k1gInSc1
celému	celý	k2eAgInSc3d1
světu	svět	k1gInSc3
a	a	k8xC
zůstanou	zůstat	k5eAaPmIp3nP
svobodná	svobodný	k2eAgNnPc1d1
<g/>
,	,	kIx,
místo	místo	k6eAd1
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
vytěžována	vytěžovat	k5eAaImNgFnS
softwarovými	softwarový	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
něco	něco	k6eAd1
vracely	vracet	k5eAaImAgInP
zpět	zpět	k6eAd1
komunitě	komunita	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
verze	verze	k1gFnSc1
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
verze	verze	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
15	#num#	k4
let	léto	k1gNnPc2
někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
komunity	komunita	k1gFnSc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
nabyli	nabýt	k5eAaPmAgMnP
přesvědčení	přesvědčený	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
někteří	některý	k3yIgMnPc1
dodavatelé	dodavatel	k1gMnPc1
softwaru	software	k1gInSc2
a	a	k8xC
hardwaru	hardware	k1gInSc2
našli	najít	k5eAaPmAgMnP
v	v	k7c6
GPL	GPL	kA
skuliny	skulina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jim	on	k3xPp3gMnPc3
umožňují	umožňovat	k5eAaImIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
software	software	k1gInSc4
licencovaný	licencovaný	k2eAgInSc4d1
pod	pod	k7c4
GPL	GPL	kA
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
se	s	k7c7
záměry	záměr	k1gInPc7
programátorů	programátor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
takové	takový	k3xDgFnPc4
metody	metoda	k1gFnPc4
patří	patřit	k5eAaImIp3nS
tivoizace	tivoizace	k1gFnSc1
(	(	kIx(
<g/>
začlenění	začlenění	k1gNnSc1
GPL	GPL	kA
softwaru	software	k1gInSc2
do	do	k7c2
hardwaru	hardware	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odmítá	odmítat	k5eAaImIp3nS
spouštět	spouštět	k5eAaImF
modifikované	modifikovaný	k2eAgFnPc4d1
verze	verze	k1gFnPc4
tohoto	tento	k3xDgInSc2
softwaru	software	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používání	používání	k1gNnSc1
nepublikovaných	publikovaný	k2eNgFnPc2d1
modifikovaných	modifikovaný	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
GPL	GPL	kA
softwaru	software	k1gInSc3
za	za	k7c7
webovým	webový	k2eAgNnSc7d1
rozhraním	rozhraní	k1gNnSc7
<g/>
,	,	kIx,
patentové	patentový	k2eAgInPc1d1
obchody	obchod	k1gInPc1
mezi	mezi	k7c7
Microsoftem	Microsoft	k1gMnSc7
a	a	k8xC
distributory	distributor	k1gMnPc7
Linuxu	linux	k1gInSc2
a	a	k8xC
Unixu	Unix	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mohou	moct	k5eAaImIp3nP
zkusit	zkusit	k5eAaPmF
využít	využít	k5eAaPmF
patenty	patent	k1gInPc4
jako	jako	k8xC,k8xS
zbraň	zbraň	k1gFnSc4
proti	proti	k7c3
konkurenci	konkurence	k1gFnSc3
z	z	k7c2
Linuxu	linux	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
3	#num#	k4
byla	být	k5eAaImAgFnS
vyvinuta	vyvinout	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
tyto	tento	k3xDgInPc4
problémy	problém	k1gInPc4
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
vydána	vydán	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
</s>
<s>
Verze	verze	k1gFnSc1
1	#num#	k4
</s>
<s>
Verze	verze	k1gFnSc1
1	#num#	k4
licence	licence	k1gFnSc1
GNU	gnu	k1gNnSc2
GPL	GPL	kA
<g/>
,	,	kIx,
vydaná	vydaný	k2eAgFnSc1d1
v	v	k7c6
lednu	leden	k1gInSc6
1989	#num#	k4
<g/>
,	,	kIx,
chránila	chránit	k5eAaImAgFnS
proti	proti	k7c3
dvěma	dva	k4xCgInPc3
způsobům	způsob	k1gInPc3
<g/>
,	,	kIx,
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
softwaroví	softwarový	k2eAgMnPc1d1
distributoři	distributor	k1gMnPc1
omezovali	omezovat	k5eAaImAgMnP
svobody	svoboda	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
definují	definovat	k5eAaBmIp3nP
svobodný	svobodný	k2eAgInSc4d1
software	software	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
distributoři	distributor	k1gMnPc1
mohou	moct	k5eAaImIp3nP
publikovat	publikovat	k5eAaBmF
jen	jen	k9
binární	binární	k2eAgInPc1d1
soubory	soubor	k1gInPc1
–	–	k?
spustitelné	spustitelný	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nikoli	nikoli	k9
čitelné	čitelný	k2eAgFnSc2d1
nebo	nebo	k8xC
upravitelné	upravitelný	k2eAgFnSc2d1
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPLv	GPLv	k1gInSc1
<g/>
1	#num#	k4
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
mu	on	k3xPp3gMnSc3
brání	bránit	k5eAaImIp3nS
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
dodavatel	dodavatel	k1gMnSc1
šířící	šířící	k2eAgInPc4d1
binární	binární	k2eAgInSc4d1
soubory	soubor	k1gInPc4
musí	muset	k5eAaImIp3nS
pod	pod	k7c7
stejnými	stejný	k2eAgFnPc7d1
licenčními	licenční	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
poskytnout	poskytnout	k5eAaPmF
také	také	k9
lidsky	lidsky	k6eAd1
čitelný	čitelný	k2eAgInSc1d1
zdrojový	zdrojový	k2eAgInSc1d1
kód	kód	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgInSc7
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
distributoři	distributor	k1gMnPc1
mohou	moct	k5eAaImIp3nP
přidávat	přidávat	k5eAaImF
dodatečná	dodatečný	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
<g/>
,	,	kIx,
buď	buď	k8xC
přidáváním	přidávání	k1gNnSc7
restrikcí	restrikce	k1gFnSc7
k	k	k7c3
licenci	licence	k1gFnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
kombinováním	kombinování	k1gNnSc7
softwaru	software	k1gInSc2
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
softwarem	software	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
další	další	k2eAgFnPc4d1
restrikce	restrikce	k1gFnPc4
ohledně	ohledně	k7c2
své	svůj	k3xOyFgFnSc2
distribuce	distribuce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
udělá	udělat	k5eAaPmIp3nS
<g/>
,	,	kIx,
na	na	k7c4
kombinované	kombinovaný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nP
obě	dva	k4xCgFnPc1
sady	sada	k1gFnPc1
restrikcí	restrikce	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přidána	přidán	k2eAgNnPc1d1
neakceptovatelná	akceptovatelný	k2eNgNnPc1d1
omezení	omezení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPLv	GPLv	k1gInSc1
<g/>
1	#num#	k4
tomu	ten	k3xDgNnSc3
zamezuje	zamezovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
modifikované	modifikovaný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
šířeny	šířit	k5eAaImNgInP
pod	pod	k7c4
GPLv	GPLv	k1gInSc4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
software	software	k1gInSc1
šířený	šířený	k2eAgInSc1d1
pod	pod	k7c7
podmínkami	podmínka	k1gFnPc7
GPLv	GPLvum	k1gNnPc2
<g/>
1	#num#	k4
lze	lze	k6eAd1
kombinovat	kombinovat	k5eAaImF
se	s	k7c7
softwarem	software	k1gInSc7
pod	pod	k7c7
permisivnějšími	permisivný	k2eAgFnPc7d2
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
nezmění	změnit	k5eNaPmIp3nP
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
pod	pod	k7c7
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
se	se	k3xPyFc4
distribuuje	distribuovat	k5eAaBmIp3nS
výsledný	výsledný	k2eAgInSc1d1
celek	celek	k1gInSc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
se	se	k3xPyFc4
softwarem	software	k1gInSc7
šířeným	šířený	k2eAgNnSc7d1
pod	pod	k7c7
restriktivnější	restriktivní	k2eAgFnSc7d2
licencí	licence	k1gFnSc7
ho	on	k3xPp3gMnSc2
kombinovat	kombinovat	k5eAaImF
nelze	lze	k6eNd1
<g/>
,	,	kIx,
protože	protože	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
požadavkem	požadavek	k1gInSc7
GPLv	GPLv	k1gInSc4
<g/>
1	#num#	k4
na	na	k7c4
šíření	šíření	k1gNnSc4
pod	pod	k7c7
touto	tento	k3xDgFnSc7
licencí	licence	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
2	#num#	k4
</s>
<s>
Podle	podle	k7c2
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
v	v	k7c4
GPLv	GPLv	k1gInSc4
<g/>
2	#num#	k4
klauzule	klauzule	k1gFnSc1
"	"	kIx"
<g/>
svobodu	svoboda	k1gFnSc4
nebo	nebo	k8xC
život	život	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ji	on	k3xPp3gFnSc4
nazval	nazvat	k5eAaPmAgInS,k5eAaBmAgInS
–	–	k?
sekce	sekce	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
sekce	sekce	k1gFnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
někdo	někdo	k3yInSc1
přidá	přidat	k5eAaPmIp3nS
omezení	omezení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
mu	on	k3xPp3gMnSc3
brání	bránit	k5eAaImIp3nS
šířit	šířit	k5eAaImF
software	software	k1gInSc4
pod	pod	k7c7
licencí	licence	k1gFnSc7
GPL	GPL	kA
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
respektoval	respektovat	k5eAaImAgMnS
svobodu	svoboda	k1gFnSc4
ostatních	ostatní	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
pokud	pokud	k8xS
právní	právní	k2eAgNnSc1d1
ustanovení	ustanovení	k1gNnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
software	software	k1gInSc1
smí	smět	k5eAaImIp3nS
distribuovat	distribuovat	k5eAaBmF
jen	jen	k9
v	v	k7c6
binární	binární	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nesmí	smět	k5eNaImIp3nS
ho	on	k3xPp3gMnSc4
šířit	šířit	k5eAaImF
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vycházelo	vycházet	k5eAaImAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
méně	málo	k6eAd2
restriktivní	restriktivní	k2eAgFnSc1d1
licence	licence	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
strategicky	strategicky	k6eAd1
užitečná	užitečný	k2eAgFnSc1d1
pro	pro	k7c4
některé	některý	k3yIgFnPc4
softwarové	softwarový	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
<g/>
;	;	kIx,
verze	verze	k1gFnSc1
2	#num#	k4
licence	licence	k1gFnSc1
GPL	GPL	kA
(	(	kIx(
<g/>
GPLv	GPLv	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
červnu	červen	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
proto	proto	k8xC
druhá	druhý	k4xOgFnSc1
licence	licence	k1gFnSc1
–	–	k?
Library	Librar	k1gInPc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
(	(	kIx(
<g/>
LGPL	LGPL	kA
<g/>
)	)	kIx)
–	–	k?
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
označena	označit	k5eAaPmNgFnS
číslem	číslo	k1gNnSc7
2	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
licence	licence	k1gFnPc1
vzájemně	vzájemně	k6eAd1
doplňují	doplňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslování	číslování	k1gNnSc1
verzí	verze	k1gFnPc2
se	se	k3xPyFc4
rozešlo	rozejít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
verze	verze	k1gFnSc1
2.1	2.1	k4
licence	licence	k1gFnSc1
LGPL	LGPL	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ji	on	k3xPp3gFnSc4
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c6
GNU	gnu	k1gNnPc6
Lesser	Lesser	k1gInSc4
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
lépe	dobře	k6eAd2
odrážela	odrážet	k5eAaImAgFnS
její	její	k3xOp3gNnSc4
místo	místo	k1gNnSc4
ve	v	k7c6
filosofii	filosofie	k1gFnSc6
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
3	#num#	k4
</s>
<s>
Na	na	k7c6
sklonku	sklonek	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
Free	Free	k1gFnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c4
verzi	verze	k1gFnSc4
3	#num#	k4
licence	licence	k1gFnSc2
GPL	GPL	kA
(	(	kIx(
<g/>
GPLv	GPLv	k1gInSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
byl	být	k5eAaImAgInS
publikován	publikovat	k5eAaBmNgInS
první	první	k4xOgInSc4
"	"	kIx"
<g/>
diskusní	diskusní	k2eAgInSc4d1
draft	draft	k1gInSc4
<g/>
"	"	kIx"
této	tento	k3xDgFnSc2
licence	licence	k1gFnSc2
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
veřejné	veřejný	k2eAgFnPc4d1
konzultace	konzultace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
původně	původně	k6eAd1
plánovány	plánován	k2eAgFnPc1d1
na	na	k7c4
devět	devět	k4xCc4
až	až	k9
patnáct	patnáct	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
protáhly	protáhnout	k5eAaPmAgFnP
na	na	k7c4
osmnáct	osmnáct	k4xCc4
měsíců	měsíc	k1gInPc2
a	a	k8xC
byly	být	k5eAaImAgInP
publikovány	publikovat	k5eAaBmNgInP
čtyři	čtyři	k4xCgInPc1
koncepty	koncept	k1gInPc1
textu	text	k1gInSc2
licence	licence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgInSc4d1
GPLv	GPLv	k1gInSc4
<g/>
3	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPLv	GPLv	k1gInSc1
<g/>
3	#num#	k4
byla	být	k5eAaImAgFnS
napsána	napsat	k5eAaPmNgFnS,k5eAaBmNgFnS
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
za	za	k7c2
právní	právní	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
Ebena	Ebena	k1gFnSc1
Moglena	Moglena	k1gFnSc1
a	a	k8xC
Software	software	k1gInSc1
Freedom	Freedom	k1gInSc1
Law	Law	k1gFnSc4
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
Stallmana	Stallman	k1gMnSc2
se	se	k3xPyFc4
nejdůležitější	důležitý	k2eAgFnPc1d3
změny	změna	k1gFnPc1
týkají	týkat	k5eAaImIp3nP
vztahu	vztah	k1gInSc3
k	k	k7c3
softwarovým	softwarový	k2eAgInPc3d1
patentům	patent	k1gInPc3
<g/>
,	,	kIx,
kompatibilitě	kompatibilita	k1gFnSc3
svobodných	svobodný	k2eAgFnPc2d1
licencí	licence	k1gFnPc2
<g/>
,	,	kIx,
definice	definice	k1gFnSc1
"	"	kIx"
<g/>
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
"	"	kIx"
a	a	k8xC
hardwarových	hardwarový	k2eAgNnPc2d1
omezení	omezení	k1gNnPc2
modifikace	modifikace	k1gFnSc2
software	software	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
tivoizace	tivoizace	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnPc1d1
změny	změna	k1gFnPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
internacionalizaci	internacionalizace	k1gFnSc3
<g/>
,	,	kIx,
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
řeší	řešit	k5eAaImIp3nS
porušení	porušení	k1gNnSc1
licence	licence	k1gFnSc2
a	a	k8xC
jak	jak	k6eAd1
může	moct	k5eAaImIp3nS
držitel	držitel	k1gMnSc1
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
přidávat	přidávat	k5eAaImF
k	k	k7c3
licenci	licence	k1gFnSc3
další	další	k2eAgNnSc1d1
povolení	povolení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
dalšími	další	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
stojí	stát	k5eAaImIp3nP
za	za	k7c4
zmínku	zmínka	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oprávnění	oprávnění	k1gNnSc4
autorů	autor	k1gMnPc2
přidávat	přidávat	k5eAaImF
další	další	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
nebo	nebo	k8xC
požadavky	požadavek	k1gInPc4
na	na	k7c4
distribuci	distribuce	k1gFnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
takových	takový	k3xDgFnPc2
volitelných	volitelný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Affero	Affero	k1gNnSc1
klauzule	klauzule	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zamýšlena	zamýšlen	k2eAgFnSc1d1
ke	k	k7c3
splnění	splnění	k1gNnSc3
požadavků	požadavek	k1gInPc2
vzhledem	vzhledem	k7c3
k	k	k7c3
softwaru	software	k1gInSc3
jako	jako	k8xS,k8xC
službě	služba	k1gFnSc3
<g/>
;	;	kIx,
povolení	povolení	k1gNnSc1
přidat	přidat	k5eAaPmF
tento	tento	k3xDgInSc4
požadavek	požadavek	k1gInSc4
činí	činit	k5eAaImIp3nS
GPLv	GPLv	k1gInSc4
<g/>
3	#num#	k4
kompatibilní	kompatibilní	k2eAgFnSc1d1
s	s	k7c7
licencí	licence	k1gFnSc7
Affero	Affero	k1gNnSc1
General	General	k1gFnSc6
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Proces	proces	k1gInSc1
veřejných	veřejný	k2eAgFnPc2d1
konzultací	konzultace	k1gFnPc2
byl	být	k5eAaImAgInS
koordinován	koordinován	k2eAgInSc1d1
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
za	za	k7c2
asistence	asistence	k1gFnSc2
Software	software	k1gInSc1
Freedom	Freedom	k1gInSc1
Law	Law	k1gFnPc2
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dalších	další	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
zabývajících	zabývající	k2eAgFnPc2d1
se	se	k3xPyFc4
svobodným	svobodný	k2eAgInSc7d1
softwarem	software	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentáře	komentář	k1gInSc2
od	od	k7c2
veřejnosti	veřejnost	k1gFnSc2
byly	být	k5eAaImAgInP
shromažďovány	shromažďován	k2eAgInPc1d1
přes	přes	k7c4
webový	webový	k2eAgInSc4d1
portál	portál	k1gInSc4
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
portál	portál	k1gInSc1
běží	běžet	k5eAaImIp3nS
na	na	k7c6
softwaru	software	k1gInSc6
stet	stet	k5eAaPmF
<g/>
,	,	kIx,
vytvořeném	vytvořený	k2eAgMnSc6d1
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentáře	komentář	k1gInSc2
byly	být	k5eAaImAgFnP
předávány	předávat	k5eAaImNgFnP
čtyřem	čtyři	k4xCgInPc3
výborům	výbor	k1gInPc3
čítajícím	čítající	k2eAgInPc3d1
přibližně	přibližně	k6eAd1
130	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
příznivců	příznivec	k1gMnPc2
i	i	k8xC
odpůrců	odpůrce	k1gMnPc2
cílů	cíl	k1gInPc2
FSF	FSF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
výbory	výbor	k1gInPc4
zkoumaly	zkoumat	k5eAaImAgInP
komentáře	komentář	k1gInPc1
zaslané	zaslaný	k2eAgInPc1d1
veřejností	veřejnost	k1gFnSc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
shrnutí	shrnutí	k1gNnSc4
předávaly	předávat	k5eAaImAgFnP
Stallmanovi	Stallmanův	k2eAgMnPc1d1
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bude	být	k5eAaImBp3nS
licence	licence	k1gFnSc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
procesu	proces	k1gInSc2
veřejných	veřejný	k2eAgFnPc2d1
konzultací	konzultace	k1gFnPc2
bylo	být	k5eAaImAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
návrhu	návrh	k1gInSc3
zasláno	zaslat	k5eAaPmNgNnS
962	#num#	k4
komentářů	komentář	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
konce	konec	k1gInSc2
procesu	proces	k1gInSc2
přišlo	přijít	k5eAaPmAgNnS
celkem	celkem	k6eAd1
2	#num#	k4
636	#num#	k4
komentářů	komentář	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třetí	třetí	k4xOgInSc1
koncept	koncept	k1gInSc1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Zahrnoval	zahrnovat	k5eAaImAgMnS
formulace	formulace	k1gFnPc4
zamýšlené	zamýšlený	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
ochrana	ochrana	k1gFnSc1
proti	proti	k7c3
patentovému	patentový	k2eAgNnSc3d1
křížovému	křížový	k2eAgNnSc3d1
licencování	licencování	k1gNnSc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
kontroverzní	kontroverzní	k2eAgFnSc1d1
patentová	patentový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
Microsoft-Novell	Microsoft-Novella	k1gFnPc2
a	a	k8xC
omezuje	omezovat	k5eAaImIp3nS
protitivoizační	protitivoizační	k2eAgFnSc1d1
klauzule	klauzule	k1gFnSc1
na	na	k7c4
právní	právní	k2eAgFnSc4d1
definici	definice	k1gFnSc4
"	"	kIx"
<g/>
uživatele	uživatel	k1gMnPc4
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
spotřebitelského	spotřebitelský	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
explicitně	explicitně	k6eAd1
odstraňuje	odstraňovat	k5eAaImIp3nS
sekci	sekce	k1gFnSc4
o	o	k7c6
"	"	kIx"
<g/>
geografických	geografický	k2eAgNnPc6d1
omezeních	omezení	k1gNnPc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
odstranění	odstranění	k1gNnSc1
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
na	na	k7c6
začátku	začátek	k1gInSc6
veřejných	veřejný	k2eAgFnPc2d1
konzultací	konzultace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgMnSc1
diskusní	diskusní	k2eAgInSc1d1
draft	draft	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
poslední	poslední	k2eAgMnSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedl	zavést	k5eAaPmAgMnS
kompatibilitu	kompatibilita	k1gFnSc4
s	s	k7c7
licencí	licence	k1gFnSc7
Apache	Apach	k1gInSc2
<g/>
,	,	kIx,
vyjasnil	vyjasnit	k5eAaPmAgInS
roli	role	k1gFnSc4
vnějších	vnější	k2eAgMnPc2d1
kontrahentů	kontrahent	k1gMnPc2
a	a	k8xC
udělal	udělat	k5eAaPmAgMnS
výjimku	výjimka	k1gFnSc4
k	k	k7c3
povolení	povolení	k1gNnSc3
dohody	dohoda	k1gFnSc2
Microsoft-Novell	Microsoft-Novell	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
sekci	sekce	k1gFnSc6
11	#num#	k4
<g/>
,	,	kIx,
paragrafu	paragraf	k1gInSc2
6	#num#	k4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Nesmíte	smět	k5eNaImIp2nP
poskytnout	poskytnout	k5eAaPmF
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jste	být	k5eAaImIp2nP
ve	v	k7c6
smluvním	smluvní	k2eAgInSc6d1
vztahu	vztah	k1gInSc6
se	se	k3xPyFc4
třetí	třetí	k4xOgFnSc7
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
šíření	šíření	k1gNnSc1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yQgNnSc2,k3yRgNnSc2,k3yIgNnSc2
zaplatíte	zaplatit	k5eAaPmIp2nP
této	tento	k3xDgFnSc3
třetí	třetí	k4xOgFnSc3
osobě	osoba	k1gFnSc3
za	za	k7c4
vaši	váš	k3xOp2gFnSc4
aktivitu	aktivita	k1gFnSc4
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
šíření	šíření	k1gNnSc6
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
tato	tento	k3xDgFnSc1
třetí	třetí	k4xOgFnSc1
osoba	osoba	k1gFnSc1
zaručuje	zaručovat	k5eAaImIp3nS
všem	všecek	k3xTgFnPc3
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
od	od	k7c2
vás	vy	k3xPp2nPc2
přijmou	přijmout	k5eAaPmIp3nP
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
diskriminační	diskriminační	k2eAgFnSc4d1
patentovou	patentový	k2eAgFnSc4d1
licenci	licence	k1gFnSc4
[	[	kIx(
<g/>
...	...	k?
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
takové	takový	k3xDgFnSc2
dohody	dohoda	k1gFnSc2
neúčinné	účinný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Licence	licence	k1gFnSc1
je	být	k5eAaImIp3nS
míněna	mínit	k5eAaImNgFnS
také	také	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
způsobila	způsobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Microsoft	Microsoft	kA
rozšíří	rozšířit	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
patentové	patentový	k2eAgFnPc4d1
licence	licence	k1gFnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
poskytl	poskytnout	k5eAaPmAgMnS
zákazníkům	zákazník	k1gMnPc3
společnosti	společnost	k1gFnSc2
Novell	Novell	kA
pro	pro	k7c4
užití	užití	k1gNnSc4
softwaru	software	k1gInSc2
licencovaného	licencovaný	k2eAgInSc2d1
pod	pod	k7c4
GPLv	GPLv	k1gInSc4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
všechny	všechen	k3xTgMnPc4
uživatele	uživatel	k1gMnPc4
tohoto	tento	k3xDgInSc2
softwaru	software	k1gInSc2
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jen	jen	k9
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
Microsoft	Microsoft	kA
právně	právně	k6eAd1
"	"	kIx"
<g/>
šiřitelem	šiřitel	k1gMnSc7
<g/>
"	"	kIx"
softwaru	software	k1gInSc2
licencovaného	licencovaný	k2eAgInSc2d1
pod	pod	k7c4
GPLv	GPLv	k1gInSc4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
vysoce	vysoce	k6eAd1
profilovaní	profilovaný	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
komentovali	komentovat	k5eAaBmAgMnP
pro	pro	k7c4
masmédia	masmédium	k1gNnPc4
návrhy	návrh	k1gInPc4
licence	licence	k1gFnSc2
a	a	k8xC
zveřejňovali	zveřejňovat	k5eAaImAgMnP
prohlášení	prohlášení	k1gNnSc4
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
námitkách	námitka	k1gFnPc6
k	k	k7c3
částem	část	k1gFnPc3
diskusních	diskusní	k2eAgInPc2d1
draftů	draft	k1gInPc2
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podmínky	podmínka	k1gFnPc1
licence	licence	k1gFnSc2
</s>
<s>
Podmínky	podmínka	k1gFnPc4
licence	licence	k1gFnSc2
GPL	GPL	kA
jsou	být	k5eAaImIp3nP
komukoli	kdokoli	k3yInSc3
dostupné	dostupný	k2eAgInPc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
získá	získat	k5eAaPmIp3nS
text	text	k1gInSc4
licence	licence	k1gFnSc2
s	s	k7c7
dílem	dílo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
pod	pod	k7c7
touto	tento	k3xDgFnSc7
licencí	licence	k1gFnSc7
poskytováno	poskytovat	k5eAaImNgNnS
(	(	kIx(
<g/>
"	"	kIx"
<g/>
nabyvatel	nabyvatel	k1gMnSc1
licence	licence	k1gFnSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
nabyvatel	nabyvatel	k1gMnSc1
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
akceptuje	akceptovat	k5eAaBmIp3nS
její	její	k3xOp3gFnPc4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
získává	získávat	k5eAaImIp3nS
právo	právo	k1gNnSc4
modifikovat	modifikovat	k5eAaBmF
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
kopírovat	kopírovat	k5eAaImF
ho	on	k3xPp3gMnSc4
a	a	k8xC
dál	daleko	k6eAd2
rozšiřovat	rozšiřovat	k5eAaImF
toto	tento	k3xDgNnSc4
dílo	dílo	k1gNnSc4
i	i	k8xC
jakoukoli	jakýkoli	k3yIgFnSc4
odvozenou	odvozený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabyvatel	nabyvatel	k1gMnSc1
licence	licence	k1gFnSc2
může	moct	k5eAaImIp3nS
provádět	provádět	k5eAaImF
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
za	za	k7c4
úplatu	úplata	k1gFnSc4
či	či	k8xC
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
bod	bod	k1gInSc4
odlišuje	odlišovat	k5eAaImIp3nS
GPL	GPL	kA
od	od	k7c2
licencí	licence	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zakazují	zakazovat	k5eAaImIp3nP
komerční	komerční	k2eAgFnSc3d1
redistribuci	redistribuce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSF	FSF	kA
argumentuje	argumentovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
omezovat	omezovat	k5eAaImF
komerční	komerční	k2eAgNnSc4d1
užití	užití	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
GPL	GPL	kA
tedy	tedy	k9
explicitně	explicitně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
díla	dílo	k1gNnPc1
pod	pod	k7c4
GPL	GPL	kA
lze	lze	k6eAd1
"	"	kIx"
<g/>
prodávat	prodávat	k5eAaImF
<g/>
"	"	kIx"
za	za	k7c4
libovolnou	libovolný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
GPL	GPL	kA
dále	daleko	k6eAd2
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
distributor	distributor	k1gMnSc1
nesmí	smět	k5eNaImIp3nS
zavést	zavést	k5eAaPmF
"	"	kIx"
<g/>
další	další	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
na	na	k7c4
práva	právo	k1gNnPc4
zaručená	zaručený	k2eAgFnSc1d1
GPL	GPL	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zakazuje	zakazovat	k5eAaImIp3nS
aktivity	aktivita	k1gFnPc4
jako	jako	k8xC,k8xS
distribuci	distribuce	k1gFnSc4
softwaru	software	k1gInSc2
pod	pod	k7c7
závazkem	závazek	k1gInSc7
nebo	nebo	k8xC
smlouvou	smlouva	k1gFnSc7
mlčenlivosti	mlčenlivost	k1gFnSc2
(	(	kIx(
<g/>
NDA	NDA	kA
<g/>
,	,	kIx,
non-disclosure	non-disclosur	k1gMnSc5
agreement	agreement	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distributoři	distributor	k1gMnPc1
softwarového	softwarový	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
pod	pod	k7c7
GPL	GPL	kA
také	také	k6eAd1
zaručují	zaručovat	k5eAaImIp3nP
poskytnutí	poskytnutí	k1gNnSc4
licence	licence	k1gFnSc2
pro	pro	k7c4
jakékoli	jakýkoli	k3yIgInPc4
patenty	patent	k1gInPc4
použité	použitý	k2eAgInPc4d1
v	v	k7c6
tomto	tento	k3xDgInSc6
softwaru	software	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
tyto	tento	k3xDgInPc1
patenty	patent	k1gInPc1
v	v	k7c6
díle	dílo	k1gNnSc6
užívány	užíván	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Sekce	sekce	k1gFnSc1
3	#num#	k4
licence	licence	k1gFnSc1
požaduje	požadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
k	k	k7c3
programům	program	k1gInPc3
šířeným	šířený	k2eAgInPc3d1
jako	jako	k8xS,k8xC
předkompilované	předkompilovaný	k2eAgInPc1d1
binární	binární	k2eAgInPc1d1
soubory	soubor	k1gInPc1
přikládaly	přikládat	k5eAaImAgInP
zdrojové	zdrojový	k2eAgInPc1d1
kódy	kód	k1gInPc1
<g/>
,	,	kIx,
písemná	písemný	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
distribuce	distribuce	k1gFnSc2
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
stejným	stejný	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
se	se	k3xPyFc4
poskytly	poskytnout	k5eAaPmAgInP
binární	binární	k2eAgInPc1d1
soubory	soubor	k1gInPc1
<g/>
,	,	kIx,
anebo	anebo	k8xC
písemná	písemný	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
k	k	k7c3
poskytnutí	poskytnutí	k1gNnSc3
zdrojových	zdrojový	k2eAgInPc2d1
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Copyleft	Copyleft	k1gMnSc1
</s>
<s>
Distribuční	distribuční	k2eAgNnPc4d1
práva	právo	k1gNnPc4
zaručená	zaručený	k2eAgNnPc4d1
GPL	GPL	kA
pro	pro	k7c4
modifikované	modifikovaný	k2eAgFnPc4d1
verze	verze	k1gFnPc4
díla	dílo	k1gNnSc2
nejsou	být	k5eNaImIp3nP
bez	bez	k7c2
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
někdo	někdo	k3yInSc1
šíří	šířit	k5eAaImIp3nS
GPL	GPL	kA
dílo	dílo	k1gNnSc1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
vlastními	vlastní	k2eAgFnPc7d1
modifikacemi	modifikace	k1gFnPc7
<g/>
,	,	kIx,
požadavky	požadavek	k1gInPc7
na	na	k7c4
distribuci	distribuce	k1gFnSc4
celého	celý	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
jakkoli	jakkoli	k6eAd1
zvětšeny	zvětšen	k2eAgFnPc1d1
oproti	oproti	k7c3
požadavkům	požadavek	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
požadavek	požadavek	k1gInSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
copyleft	copyleft	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
právní	právní	k2eAgFnSc4d1
moc	moc	k1gFnSc4
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
aplikace	aplikace	k1gFnSc2
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
na	na	k7c4
počítačové	počítačový	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
dílo	dílo	k1gNnSc1
šířené	šířený	k2eAgFnSc2d1
pod	pod	k7c7
GPL	GPL	kA
je	být	k5eAaImIp3nS
chráněno	chránit	k5eAaImNgNnS
autorským	autorský	k2eAgNnSc7d1
právem	právo	k1gNnSc7
<g/>
,	,	kIx,
nabyvatel	nabyvatel	k1gMnSc1
licence	licence	k1gFnSc2
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc4
oprávnění	oprávnění	k1gNnSc4
k	k	k7c3
šíření	šíření	k1gNnSc3
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
ani	ani	k8xC
v	v	k7c6
modifikované	modifikovaný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
(	(	kIx(
<g/>
kromě	kromě	k7c2
případu	případ	k1gInSc2
fair	fair	k6eAd1
use	usus	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
mu	on	k3xPp3gNnSc3
určují	určovat	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc4
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
chce	chtít	k5eAaImIp3nS
vykonávat	vykonávat	k5eAaImF
práva	práv	k2eAgFnSc1d1
normálně	normálně	k6eAd1
omezená	omezený	k2eAgFnSc1d1
autorským	autorský	k2eAgNnSc7d1
právem	právo	k1gNnSc7
(	(	kIx(
<g/>
např.	např.	kA
šíření	šíření	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
přijmout	přijmout	k5eAaPmF
podmínky	podmínka	k1gFnPc4
licence	licence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
někdo	někdo	k3yInSc1
distribuuje	distribuovat	k5eAaBmIp3nS
kopie	kopie	k1gFnPc4
díla	dílo	k1gNnSc2
bez	bez	k7c2
přijetí	přijetí	k1gNnSc2
podmínek	podmínka	k1gFnPc2
GPL	GPL	kA
(	(	kIx(
<g/>
například	například	k6eAd1
s	s	k7c7
udržením	udržení	k1gNnSc7
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
v	v	k7c6
utajení	utajení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
postižen	postihnout	k5eAaPmNgMnS
(	(	kIx(
<g/>
žalován	žalovat	k5eAaImNgMnS
původním	původní	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
<g/>
,	,	kIx,
trestně	trestně	k6eAd1
stíhán	stíhat	k5eAaImNgMnS
<g/>
)	)	kIx)
za	za	k7c2
porušení	porušení	k1gNnSc2
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Copyleft	Copyleft	k1gInSc1
tedy	tedy	k9
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
autorské	autorský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
(	(	kIx(
<g/>
copyright	copyright	k1gNnSc2
<g/>
)	)	kIx)
k	k	k7c3
zajištění	zajištění	k1gNnSc3
opaku	opak	k1gInSc2
obvyklého	obvyklý	k2eAgInSc2d1
účelu	účel	k1gInSc2
<g/>
:	:	kIx,
namísto	namísto	k7c2
zavádění	zavádění	k1gNnSc2
omezení	omezení	k1gNnSc2
zaručuje	zaručovat	k5eAaImIp3nS
práva	právo	k1gNnPc4
ostatním	ostatní	k2eAgMnPc3d1
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
následně	následně	k6eAd1
odebrána	odebrán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
zaručena	zaručen	k2eAgNnPc1d1
neomezená	omezený	k2eNgNnPc1d1
distribuční	distribuční	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
v	v	k7c6
prohlášení	prohlášení	k1gNnSc6
copyleftu	copyleft	k1gInSc2
byla	být	k5eAaImAgFnS
právní	právní	k2eAgFnSc1d1
vada	vada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
distributorů	distributor	k1gMnPc2
GPL	GPL	kA
programů	program	k1gInPc2
přikládá	přikládat	k5eAaImIp3nS
zdrojový	zdrojový	k2eAgInSc1d1
kód	kód	k1gInSc1
ke	k	k7c3
spustitelným	spustitelný	k2eAgInPc3d1
souborům	soubor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alternativní	alternativní	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
vyhovět	vyhovět	k5eAaPmF
copyleftu	copylefta	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
poskytnout	poskytnout	k5eAaPmF
písemnou	písemný	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
na	na	k7c4
poskytnutí	poskytnutí	k1gNnSc4
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
na	na	k7c6
fyzickém	fyzický	k2eAgNnSc6d1
médiu	médium	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
CD	CD	kA
<g/>
)	)	kIx)
na	na	k7c4
vyžádání	vyžádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
programů	program	k1gInPc2
pod	pod	k7c7
GPL	GPL	kA
distribuováno	distribuován	k2eAgNnSc1d1
přes	přes	k7c4
Internet	Internet	k1gInSc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc1
zdrojové	zdrojový	k2eAgInPc1d1
kódy	kód	k1gInPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
protokolem	protokol	k1gInSc7
FTP	FTP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
internetové	internetový	k2eAgFnSc6d1
distribuci	distribuce	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
licencí	licence	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Copyleft	Copyleft	k1gInSc1
se	se	k3xPyFc4
uplatní	uplatnit	k5eAaPmIp3nS
jen	jen	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
člověk	člověk	k1gMnSc1
program	program	k1gInSc4
šířit	šířit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
soukromé	soukromý	k2eAgFnSc2d1
modifikované	modifikovaný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
povinnost	povinnost	k1gFnSc4
šířit	šířit	k5eAaImF
modifikace	modifikace	k1gFnPc4
–	–	k?
do	do	k7c2
okamžiku	okamžik	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
začne	začít	k5eAaPmIp3nS
modifikovaný	modifikovaný	k2eAgInSc4d1
software	software	k1gInSc4
distribuovat	distribuovat	k5eAaBmF
někomu	někdo	k3yInSc3
dalšímu	další	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všimněte	všimnout	k5eAaPmRp2nP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
copyleft	copyleft	k1gInSc1
uplatňuje	uplatňovat	k5eAaImIp3nS
jen	jen	k9
na	na	k7c4
software	software	k1gInSc4
a	a	k8xC
ne	ne	k9
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
výstup	výstup	k1gInSc4
(	(	kIx(
<g/>
pokud	pokud	k8xS
výstup	výstup	k1gInSc4
není	být	k5eNaImIp3nS
sám	sám	k3xTgMnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
odvozenou	odvozený	k2eAgFnSc4d1
verzí	verze	k1gFnSc7
programu	program	k1gInSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
například	například	k6eAd1
veřejný	veřejný	k2eAgInSc4d1
webový	webový	k2eAgInSc4d1
portál	portál	k1gInSc4
běžící	běžící	k2eAgInSc4d1
na	na	k7c6
modifikované	modifikovaný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
CMS	CMS	kA
šířeného	šířený	k2eAgNnSc2d1
pod	pod	k7c7
GPL	GPL	kA
nemusí	muset	k5eNaImIp3nS
šířit	šířit	k5eAaImF
změny	změna	k1gFnPc4
tohoto	tento	k3xDgInSc2
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Licencování	licencování	k1gNnSc1
a	a	k8xC
smluvní	smluvní	k2eAgFnPc1d1
záležitosti	záležitost	k1gFnPc1
</s>
<s>
GPL	GPL	kA
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
jako	jako	k9
licence	licence	k1gFnSc1
<g/>
,	,	kIx,
ne	ne	k9
jako	jako	k9
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
některých	některý	k3yIgFnPc6
jurisdikcích	jurisdikce	k1gFnPc6
nepsaného	psaný	k2eNgNnSc2d1,k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
anglo-americký	anglo-americký	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
právní	právní	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
licencí	licence	k1gFnSc7
a	a	k8xC
smlouvou	smlouva	k1gFnSc7
tento	tento	k3xDgInSc4
<g/>
:	:	kIx,
smlouvy	smlouva	k1gFnPc1
jsou	být	k5eAaImIp3nP
vymahatelné	vymahatelný	k2eAgInPc4d1
prostřednictvím	prostřednictvím	k7c2
smluvního	smluvní	k2eAgNnSc2d1
(	(	kIx(
<g/>
obchodního	obchodní	k2eAgNnSc2d1
<g/>
)	)	kIx)
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
licence	licence	k1gFnPc1
pomocí	pomocí	k7c2
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozlišení	rozlišení	k1gNnSc1
však	však	k9
nemá	mít	k5eNaImIp3nS
význam	význam	k1gInSc4
v	v	k7c6
mnoha	mnoho	k4c6
jurisdikcích	jurisdikce	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
není	být	k5eNaImIp3nS
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
smlouvami	smlouva	k1gFnPc7
a	a	k8xC
licencemi	licence	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
systémech	systém	k1gInPc6
římského	římský	k2eAgNnSc2d1
(	(	kIx(
<g/>
kontinentálního	kontinentální	k2eAgMnSc2d1
<g/>
)	)	kIx)
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
upravuje	upravovat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
problematiku	problematika	k1gFnSc4
zákon	zákon	k1gInSc1
č.	č.	k?
121	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
autorský	autorský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
používá	používat	k5eAaImIp3nS
pojem	pojem	k1gInSc1
licence	licence	k1gFnSc2
ve	v	k7c6
smyslu	smysl	k1gInSc6
"	"	kIx"
<g/>
oprávnění	oprávnění	k1gNnPc4
k	k	k7c3
výkonu	výkon	k1gInSc3
práva	právo	k1gNnSc2
dílo	dílo	k1gNnSc1
užít	užít	k5eAaPmF
<g/>
"	"	kIx"
(	(	kIx(
<g/>
§	§	k?
12	#num#	k4
<g/>
,	,	kIx,
odst	odst	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
licenční	licenční	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
je	být	k5eAaImIp3nS
dle	dle	k7c2
§	§	k?
2358	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
89	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
právní	právní	k2eAgInSc4d1
úkon	úkon	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
autor	autor	k1gMnSc1
poskytuje	poskytovat	k5eAaImIp3nS
nabyvateli	nabyvatel	k1gMnSc3
licenci	licence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
podání	podání	k1gNnSc6
návrhu	návrh	k1gInSc2
na	na	k7c6
uzavření	uzavření	k1gNnSc6
licenční	licenční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
jde	jít	k5eAaImIp3nS
i	i	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
směřuje	směřovat	k5eAaImIp3nS
<g/>
-li	-li	k?
projev	projev	k1gInSc4
vůle	vůle	k1gFnSc2
i	i	k9
vůči	vůči	k7c3
neurčitému	určitý	k2eNgInSc3d1
okruhu	okruh	k1gInSc3
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
obsahu	obsah	k1gInSc3
návrhu	návrh	k1gInSc2
nebo	nebo	k8xC
k	k	k7c3
praxi	praxe	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
strany	strana	k1gFnSc2
mezi	mezi	k7c7
sebou	se	k3xPyFc7
zavedly	zavést	k5eAaPmAgInP
<g/>
,	,	kIx,
nebo	nebo	k8xC
zvyklostem	zvyklost	k1gFnPc3
může	moct	k5eAaImIp3nS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
návrh	návrh	k1gInSc1
určen	určen	k2eAgInSc1d1
<g/>
,	,	kIx,
vyjádřit	vyjádřit	k5eAaPmF
souhlas	souhlas	k1gInSc4
s	s	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c4
uzavření	uzavření	k1gNnSc4
smlouvy	smlouva	k1gFnSc2
provedením	provedení	k1gNnSc7
určitého	určitý	k2eAgInSc2d1
úkonu	úkon	k1gInSc2
bez	bez	k7c2
vyrozumění	vyrozumění	k1gNnSc2
navrhovatele	navrhovatel	k1gMnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
zachová	zachovat	k5eAaPmIp3nS
<g/>
,	,	kIx,
zejména	zejména	k9
že	že	k8xS
poskytne	poskytnout	k5eAaPmIp3nS
nebo	nebo	k8xC
přijme	přijmout	k5eAaPmIp3nS
plnění	plnění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
přijetí	přijetí	k1gNnSc1
návrhu	návrh	k1gInSc2
účinné	účinný	k2eAgNnSc1d1
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
úkon	úkon	k1gInSc1
učiněn	učinit	k5eAaImNgInS,k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Text	text	k1gInSc1
licence	licence	k1gFnSc2
GPL	GPL	kA
je	být	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
českého	český	k2eAgNnSc2d1
práva	právo	k1gNnSc2
tedy	tedy	k8xC
vlastně	vlastně	k9
podmínkami	podmínka	k1gFnPc7
licenční	licenční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
části	část	k1gFnSc2
GPL	GPL	kA
(	(	kIx(
<g/>
například	například	k6eAd1
o	o	k7c4
vzdání	vzdání	k1gNnSc4
se	se	k3xPyFc4
odpovědnosti	odpovědnost	k1gFnSc2
<g/>
)	)	kIx)
považují	považovat	k5eAaImIp3nP
někteří	některý	k3yIgMnPc1
právníci	právník	k1gMnPc1
v	v	k7c6
kontextu	kontext	k1gInSc6
tohoto	tento	k3xDgNnSc2
práva	právo	k1gNnSc2
za	za	k7c4
neplatné	platný	k2eNgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
nesouhlasí	souhlasit	k5eNaImIp3nS
s	s	k7c7
podmínkami	podmínka	k1gFnPc7
GPL	GPL	kA
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
oprávněni	oprávnit	k5eAaPmNgMnP
kopírovat	kopírovat	k5eAaImF
nebo	nebo	k8xC
distribuovat	distribuovat	k5eAaBmF
tento	tento	k3xDgInSc4
software	software	k1gInSc4
nebo	nebo	k8xC
odvozená	odvozený	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
však	však	k9
software	software	k1gInSc4
používat	používat	k5eAaImF
dle	dle	k7c2
libosti	libost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Držitelé	držitel	k1gMnPc1
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
</s>
<s>
Text	text	k1gInSc1
licence	licence	k1gFnSc2
GPL	GPL	kA
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
o	o	k7c6
sobě	se	k3xPyFc3
autorským	autorský	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
<g/>
,	,	kIx,
autorská	autorský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
drží	držet	k5eAaImIp3nP
Free	Free	k1gInSc4
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSF	FSF	kA
však	však	k9
nemá	mít	k5eNaImIp3nS
práva	právo	k1gNnPc4
k	k	k7c3
dílu	díl	k1gInSc3
vydanému	vydaný	k2eAgInSc3d1
pod	pod	k7c4
GPL	GPL	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
autor	autor	k1gMnSc1
explicitně	explicitně	k6eAd1
nepřevede	převést	k5eNaPmIp3nS
práva	právo	k1gNnPc4
na	na	k7c4
FSF	FSF	kA
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
zřídka	zřídka	k6eAd1
<g/>
,	,	kIx,
výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
části	část	k1gFnPc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
podávat	podávat	k5eAaImF
žalobu	žaloba	k1gFnSc4
při	při	k7c6
porušení	porušení	k1gNnSc6
podmínek	podmínka	k1gFnPc2
licence	licence	k1gFnSc2
mají	mít	k5eAaImIp3nP
jen	jen	k9
přímo	přímo	k6eAd1
jednotliví	jednotlivý	k2eAgMnPc1d1
držitelé	držitel	k1gMnPc1
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
FSF	FSF	kA
povoluje	povolovat	k5eAaImIp3nS
lidem	člověk	k1gMnPc3
vytvářet	vytvářet	k5eAaImF
si	se	k3xPyFc3
nové	nový	k2eAgFnPc4d1
licence	licence	k1gFnPc4
založené	založený	k2eAgFnPc4d1
na	na	k7c6
GPL	GPL	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tyto	tento	k3xDgFnPc1
odvozené	odvozený	k2eAgFnPc1d1
licence	licence	k1gFnPc1
nesmějí	smát	k5eNaImIp3nP
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
používat	používat	k5eAaImF
preambuli	preambule	k1gFnSc4
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ale	ale	k9
nedoporučuje	doporučovat	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
takové	takový	k3xDgFnPc1
licence	licence	k1gFnPc1
jsou	být	k5eAaImIp3nP
obecně	obecně	k6eAd1
nekompatibilní	kompatibilní	k2eNgInPc1d1
s	s	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Pro	pro	k7c4
více	hodně	k6eAd2
informací	informace	k1gFnPc2
viz	vidět	k5eAaImRp2nS
GPL	GPL	kA
FAQ	FAQ	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
licence	licence	k1gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
v	v	k7c6
projektu	projekt	k1gInSc6
GNU	gnu	k1gMnSc1
patří	patřit	k5eAaImIp3nS
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
a	a	k8xC
GNU	gnu	k1gNnSc2
Free	Fre	k1gInSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
GPL	GPL	kA
u	u	k7c2
soudu	soud	k1gInSc2
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1
sporem	spor	k1gInSc7
souvisejícím	související	k2eAgMnSc7d1
s	s	k7c7
GPL	GPL	kA
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
ne-GPL	ne-GPL	k?
software	software	k1gInSc1
smí	smět	k5eAaImIp3nS
dynamicky	dynamicky	k6eAd1
linkovat	linkovat	k5eAaImF
GPL	GPL	kA
knihovny	knihovna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPL	GPL	kA
jasně	jasně	k6eAd1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
veškerá	veškerý	k3xTgNnPc4
odvozená	odvozený	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
kódu	kód	k1gInSc2
pod	pod	k7c4
GPL	GPL	kA
musí	muset	k5eAaImIp3nS
sama	sám	k3xTgFnSc1
být	být	k5eAaImF
také	také	k6eAd1
pod	pod	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
však	však	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
spustitelný	spustitelný	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
dynamicky	dynamicky	k6eAd1
linkuje	linkovat	k5eAaImIp3nS
GPL	GPL	kA
kód	kód	k1gInSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
považován	považován	k2eAgMnSc1d1
za	za	k7c4
odvozené	odvozený	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunita	komunita	k1gFnSc1
okolo	okolo	k7c2
svobodného	svobodný	k2eAgNnSc2d1
a	a	k8xC
open	open	k1gNnSc4
source	sourec	k1gInSc2
softwaru	software	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgNnSc6
rozdělená	rozdělený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSF	FSF	kA
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
spustitelný	spustitelný	k2eAgInSc1d1
soubor	soubor	k1gInSc1
je	být	k5eAaImIp3nS
odvozeným	odvozený	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
GPL	GPL	kA
kód	kód	k1gInSc4
a	a	k8xC
spustitelný	spustitelný	k2eAgInSc4d1
soubor	soubor	k1gInSc4
"	"	kIx"
<g/>
vzájemně	vzájemně	k6eAd1
volají	volat	k5eAaImIp3nP
funkce	funkce	k1gFnPc4
a	a	k8xC
sdílejí	sdílet	k5eAaImIp3nP
datové	datový	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
čímž	což	k3yRnSc7,k3yQnSc7
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
souhlasí	souhlasit	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
kdežto	kdežto	k8xS
někteří	některý	k3yIgMnPc1
(	(	kIx(
<g/>
např.	např.	kA
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
<g/>
)	)	kIx)
souhlasí	souhlasit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dynamické	dynamický	k2eAgNnSc1d1
linkování	linkování	k1gNnSc1
může	moct	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
odvozené	odvozený	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
nesouhlasí	souhlasit	k5eNaImIp3nS
s	s	k7c7
danou	daný	k2eAgFnSc7d1
definicí	definice	k1gFnSc7
takových	takový	k3xDgInPc2
případů	případ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
experti	expert	k1gMnPc1
argumentují	argumentovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
otázka	otázka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
zůstává	zůstávat	k5eAaImIp3nS
otevřená	otevřený	k2eAgFnSc1d1
<g/>
:	:	kIx,
jeden	jeden	k4xCgMnSc1
právník	právník	k1gMnSc1
firmy	firma	k1gFnSc2
Novell	Novell	kA
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nevytvoření	nevytvoření	k1gNnSc1
odvozeného	odvozený	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
dynamickým	dynamický	k2eAgNnSc7d1
linkováním	linkování	k1gNnSc7
"	"	kIx"
<g/>
dává	dávat	k5eAaImIp3nS
smysl	smysl	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ale	ale	k8xC
nejde	jít	k5eNaImIp3nS
o	o	k7c4
"	"	kIx"
<g/>
čistý	čistý	k2eAgInSc4d1
řez	řez	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Lawrence	Lawrenec	k1gInSc2
Rosen	rosen	k2eAgMnSc1d1
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
soud	soud	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
by	by	kYmCp3nS
"	"	kIx"
<g/>
pravděpodobně	pravděpodobně	k6eAd1
<g/>
"	"	kIx"
vyloučil	vyloučit	k5eAaPmAgMnS
dynamické	dynamický	k2eAgNnSc1d1
linkování	linkování	k1gNnSc1
z	z	k7c2
odvozených	odvozený	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
"	"	kIx"
<g/>
existují	existovat	k5eAaImIp3nP
také	také	k9
dobré	dobrý	k2eAgInPc4d1
argumenty	argument	k1gInPc4
<g/>
"	"	kIx"
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
a	a	k8xC
"	"	kIx"
<g/>
výsledek	výsledek	k1gInSc1
není	být	k5eNaImIp3nS
jasný	jasný	k2eAgInSc1d1
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
při	při	k7c6
pozdější	pozdní	k2eAgFnSc6d2
příležitosti	příležitost	k1gFnSc6
argumentoval	argumentovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
tržní	tržní	k2eAgInPc4d1
<g/>
"	"	kIx"
faktory	faktor	k1gInPc4
jsou	být	k5eAaImIp3nP
důležitější	důležitý	k2eAgMnPc1d2
než	než	k8xS
technika	technika	k1gFnSc1
linkování	linkování	k1gNnSc2
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jednoznačně	jednoznačně	k6eAd1
není	být	k5eNaImIp3nS
otázka	otázka	k1gFnSc1
GPL	GPL	kA
jako	jako	k8xC,k8xS
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jak	jak	k8xS,k8xC
autorské	autorský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
definuje	definovat	k5eAaBmIp3nS
odvozená	odvozený	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sporu	spor	k1gInSc6
Galoob	Galooba	k1gFnPc2
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nintendo	Nintendo	k6eAd1
odvolací	odvolací	k2eAgInSc1d1
soud	soud	k1gInSc1
devátého	devátý	k4xOgInSc2
obvodu	obvod	k1gInSc2
definoval	definovat	k5eAaBmAgMnS
odvozené	odvozený	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
jako	jako	k8xS,k8xC
vztah	vztah	k1gInSc4
mající	mající	k2eAgInSc4d1
"	"	kIx"
<g/>
'	'	kIx"
<g/>
formu	forma	k1gFnSc4
<g/>
'	'	kIx"
nebo	nebo	k8xC
trvalost	trvalost	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
a	a	k8xC
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
porušující	porušující	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
musí	muset	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc7d1
formou	forma	k1gFnSc7
začleňovat	začleňovat	k5eAaImF
část	část	k1gFnSc4
chráněného	chráněný	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ale	ale	k8xC
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
jasnému	jasný	k2eAgNnSc3d1
soudnímu	soudní	k2eAgNnSc3d1
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
by	by	kYmCp3nS
vyřešilo	vyřešit	k5eAaPmAgNnS
tento	tento	k3xDgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
záznam	záznam	k1gInSc1
o	o	k7c6
někom	někdo	k3yInSc6
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
obcházel	obcházet	k5eAaImAgInS
GPL	GPL	kA
pomocí	pomocí	k7c2
dynamického	dynamický	k2eAgNnSc2d1
linkování	linkování	k1gNnSc2
a	a	k8xC
hrozil	hrozit	k5eAaImAgInS
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
spor	spor	k1gInSc1
s	s	k7c7
držitelem	držitel	k1gMnSc7
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
omezení	omezení	k1gNnSc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
jako	jako	k9
vymahatelné	vymahatelný	k2eAgNnSc1d1
de	de	k?
facto	facto	k1gNnSc1
<g/>
,	,	kIx,
byť	byť	k8xS
to	ten	k3xDgNnSc1
zatím	zatím	k6eAd1
nebylo	být	k5eNaImAgNnS
prokázáno	prokázat	k5eAaPmNgNnS
de	de	k?
jure	jure	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
žalovala	žalovat	k5eAaImAgFnS
firma	firma	k1gFnSc1
MySQL	MySQL	k1gFnSc3
firmu	firma	k1gFnSc4
Progress	Progress	k1gInSc1
NuSphere	NuSpher	k1gInSc5
kvůli	kvůli	k7c3
porušení	porušení	k1gNnSc3
autorského	autorský	k2eAgNnSc2d1
a	a	k8xC
známkového	známkový	k2eAgNnSc2d1
práva	právo	k1gNnSc2
u	u	k7c2
massachutetského	massachutetský	k2eAgInSc2d1
okresního	okresní	k2eAgInSc2d1
soudu	soud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
NuSphere	NuSpher	k1gInSc5
údajně	údajně	k6eAd1
porušila	porušit	k5eAaPmAgNnP
autorská	autorský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
k	k	k7c3
MySQL	MySQL	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
slinkovala	slinkovat	k5eAaPmAgFnS
kód	kód	k1gInSc4
tabulkového	tabulkový	k2eAgInSc2d1
typu	typ	k1gInSc2
Gemini	Gemin	k2eAgMnPc1d1
se	se	k3xPyFc4
serverem	server	k1gInSc7
MySQL	MySQL	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
předběžném	předběžný	k2eAgNnSc6d1
slyšení	slyšení	k1gNnSc6
před	před	k7c7
soudkyní	soudkyně	k1gFnSc7
Patti	Patť	k1gFnSc2
Saris	Saris	k1gInSc1
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2002	#num#	k4
vstoupily	vstoupit	k5eAaPmAgFnP
strany	strana	k1gFnPc1
sporu	spor	k1gInSc2
do	do	k7c2
jednání	jednání	k1gNnSc2
o	o	k7c4
vyrovnání	vyrovnání	k1gNnSc4
a	a	k8xC
možná	možná	k9
se	se	k3xPyFc4
vyrovnaly	vyrovnat	k5eAaBmAgInP,k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
slyšení	slyšení	k1gNnSc6
Patti	Patť	k1gFnSc2
Saris	Saris	k1gFnSc2
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
nevidí	vidět	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
důvod	důvod	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
proč	proč	k6eAd1
by	by	kYmCp3nS
GPL	GPL	kA
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
vymahatelná	vymahatelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2003	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
SCO	SCO	kA
Group	Group	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
GPL	GPL	kA
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
právní	právní	k2eAgFnSc4d1
platnost	platnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
zamýšlí	zamýšlet	k5eAaImIp3nS
vést	vést	k5eAaImF
právní	právní	k2eAgInPc4d1
spory	spor	k1gInPc4
ohledně	ohledně	k7c2
částí	část	k1gFnPc2
kódu	kód	k1gInSc2
zřejmě	zřejmě	k6eAd1
zkopírovaných	zkopírovaný	k2eAgFnPc2d1
ze	z	k7c2
systému	systém	k1gInSc2
SCO	SCO	kA
Unix	Unix	k1gInSc1
do	do	k7c2
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
firmu	firma	k1gFnSc4
problematické	problematický	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
šířila	šířit	k5eAaImAgFnS
Linux	linux	k1gInSc4
a	a	k8xC
další	další	k2eAgInSc4d1
GPL	GPL	kA
kód	kód	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
distribuci	distribuce	k1gFnSc6
Caldera	Caldera	k1gFnSc1
OpenLinux	OpenLinux	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
těžko	těžko	k6eAd1
dokazatelné	dokazatelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
měla	mít	k5eAaImAgFnS
jiné	jiný	k2eAgNnSc4d1
právo	právo	k1gNnSc4
tak	tak	k6eAd1
činit	činit	k5eAaImF
<g/>
,	,	kIx,
než	než	k8xS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
poskytuje	poskytovat	k5eAaImIp3nS
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
viz	vidět	k5eAaImRp2nS
SCO-Linux	SCO-Linux	k1gInSc1
kontroverze	kontroverze	k1gFnPc1
a	a	k8xC
SCO	SCO	kA
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
IBM	IBM	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2004	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
projekt	projekt	k1gInSc1
netfilter	netfilter	k1gInSc1
<g/>
/	/	kIx~
<g/>
iptables	iptables	k1gInSc1
u	u	k7c2
okresního	okresní	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
vydání	vydání	k1gNnSc2
předběžného	předběžný	k2eAgNnSc2d1
opatření	opatření	k1gNnSc2
proti	proti	k7c3
společnosti	společnost	k1gFnSc3
Sitecom	Sitecom	k1gInSc4
Germany	German	k1gInPc7
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
tato	tento	k3xDgFnSc1
firma	firma	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
upustit	upustit	k5eAaPmF
od	od	k7c2
šíření	šíření	k1gNnSc2
softwaru	software	k1gInSc2
netfilteru	netfilter	k1gInSc2
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
podmínkami	podmínka	k1gFnPc7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2004	#num#	k4
německý	německý	k2eAgInSc1d1
soud	soud	k1gInSc1
potvrdil	potvrdit	k5eAaPmAgInS
toto	tento	k3xDgNnSc4
opatření	opatření	k1gNnSc4
konečným	konečný	k2eAgInSc7d1
rozsudkem	rozsudek	k1gInSc7
proti	proti	k7c3
firmě	firma	k1gFnSc3
Sitecom	Sitecom	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Zdůvodnění	zdůvodnění	k1gNnSc1
rozsudku	rozsudek	k1gInSc2
přesně	přesně	k6eAd1
odráží	odrážet	k5eAaImIp3nS
predikci	predikce	k1gFnSc4
poskytnutou	poskytnutý	k2eAgFnSc4d1
dříve	dříve	k6eAd2
FSF	FSF	kA
(	(	kIx(
<g/>
Ebenem	eben	k1gInSc7
Moglenem	Moglen	k1gInSc7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Žalovaný	žalovaný	k1gMnSc1
porušil	porušit	k5eAaPmAgMnS
autorská	autorský	k2eAgNnPc4d1
práv	právo	k1gNnPc2
žalobce	žalobce	k1gMnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nabízel	nabízet	k5eAaImAgInS
software	software	k1gInSc1
'	'	kIx"
<g/>
netfilter	netfilter	k1gInSc1
<g/>
/	/	kIx~
<g/>
iptables	iptables	k1gInSc1
<g/>
'	'	kIx"
ke	k	k7c3
stažení	stažení	k1gNnSc3
a	a	k8xC
inzerování	inzerování	k1gNnSc4
této	tento	k3xDgFnSc2
své	svůj	k3xOyFgFnSc2
distribuce	distribuce	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
dodržel	dodržet	k5eAaPmAgMnS
podmínky	podmínka	k1gFnPc4
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedená	uvedený	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
přípustná	přípustný	k2eAgFnSc1d1
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
žalovaný	žalovaný	k1gMnSc1
získal	získat	k5eAaPmAgMnS
licenci	licence	k1gFnSc4
<g/>
...	...	k?
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
nezávislé	závislý	k2eNgNnSc1d1
na	na	k7c6
otázce	otázka	k1gFnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
byly	být	k5eAaImAgFnP
podmínky	podmínka	k1gFnPc1
GPL	GPL	kA
účinně	účinně	k6eAd1
dohodnuty	dohodnout	k5eAaPmNgFnP
mezi	mezi	k7c4
žalobce	žalobce	k1gMnPc4
a	a	k8xC
žalovaným	žalovaný	k1gMnPc3
<g/>
,	,	kIx,
či	či	k8xC
nikoli	nikoli	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nebyla	být	k5eNaImAgFnS
GPL	GPL	kA
dohodnuta	dohodnout	k5eAaPmNgFnS
mezi	mezi	k7c7
stranami	strana	k1gFnPc7
<g/>
,	,	kIx,
žalovaný	žalovaný	k1gMnSc1
by	by	kYmCp3nS
přesto	přesto	k6eAd1
postrádal	postrádat	k5eAaImAgInS
potřebná	potřebný	k2eAgNnPc4d1
práva	právo	k1gNnPc4
pro	pro	k7c4
kopírování	kopírování	k1gNnSc4
<g/>
,	,	kIx,
šíření	šíření	k1gNnSc4
a	a	k8xC
zveřejňování	zveřejňování	k1gNnSc4
softwaru	software	k1gInSc2
'	'	kIx"
<g/>
netfilter	netfilter	k1gInSc1
<g/>
/	/	kIx~
<g/>
iptables	iptables	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
rozsudek	rozsudek	k1gInSc4
byl	být	k5eAaImAgInS
důležitý	důležitý	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
soud	soud	k1gInSc1
potvrdil	potvrdit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
porušení	porušení	k1gNnSc1
podmínek	podmínka	k1gFnPc2
GPL	GPL	kA
je	být	k5eAaImIp3nS
porušením	porušení	k1gNnSc7
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
tento	tento	k3xDgInSc1
případ	případ	k1gInSc1
nebyl	být	k5eNaImAgInS
tak	tak	k6eAd1
rozhodujícím	rozhodující	k2eAgInSc7d1
testem	test	k1gInSc7
GPL	GPL	kA
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
někteří	některý	k3yIgMnPc1
vyvodili	vyvodit	k5eAaPmAgMnP,k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
nebyla	být	k5eNaImAgFnS
napadena	napaden	k2eAgFnSc1d1
samotná	samotný	k2eAgFnSc1d1
vynutitelnost	vynutitelnost	k1gFnSc1
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
soud	soud	k1gInSc1
toliko	toliko	k6eAd1
pokusil	pokusit	k5eAaPmAgInS
rozpoznat	rozpoznat	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
byla	být	k5eAaImAgFnS
samotná	samotný	k2eAgFnSc1d1
licence	licence	k1gFnSc1
vůbec	vůbec	k9
v	v	k7c6
účinnosti	účinnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2005	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Wallace	Wallace	k1gFnSc2
spor	spora	k1gFnPc2
proti	proti	k7c3
Free	Free	k1gNnSc3
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
u	u	k7c2
soudu	soud	k1gInSc2
v	v	k7c6
Jižním	jižní	k2eAgInSc6d1
okresu	okres	k1gInSc6
Indiany	Indiana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
GPL	GPL	kA
je	být	k5eAaImIp3nS
ilegální	ilegální	k2eAgInSc4d1
pokus	pokus	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
fixovat	fixovat	k5eAaImF
ceny	cena	k1gFnPc4
na	na	k7c4
nulu	nula	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žaloba	žaloba	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
2006	#num#	k4
zamítnuta	zamítnout	k5eAaPmNgFnS
na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Wallace	Wallace	k1gFnSc1
neprokázal	prokázat	k5eNaPmAgInS
platný	platný	k2eAgInSc1d1
antitrustový	antitrustový	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
<g/>
;	;	kIx,
soud	soud	k1gInSc1
prohlásil	prohlásit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
GPL	GPL	kA
podporuje	podporovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nikoli	nikoli	k9
omezuje	omezovat	k5eAaImIp3nS
<g/>
,	,	kIx,
svobodnou	svobodný	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
a	a	k8xC
distribuci	distribuce	k1gFnSc4
počítačových	počítačový	k2eAgInPc2d1
operačních	operační	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
plynou	plynout	k5eAaImIp3nP
přímé	přímý	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
pro	pro	k7c4
spotřebitele	spotřebitel	k1gMnPc4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Wallace	Wallace	k1gFnSc2
odmítl	odmítnout	k5eAaPmAgMnS
možnost	možnost	k1gFnSc4
další	další	k2eAgFnSc2d1
změny	změna	k1gFnSc2
své	svůj	k3xOyFgFnSc2
stížnosti	stížnost	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
zaplatit	zaplatit	k5eAaPmF
FSF	FSF	kA
náklady	náklad	k1gInPc4
na	na	k7c4
tento	tento	k3xDgInSc4
spor	spor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2005	#num#	k4
soud	soud	k1gInSc1
centrálního	centrální	k2eAgInSc2d1
okresu	okres	k1gInSc2
Soulu	Soul	k1gInSc2
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
GPL	GPL	kA
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
právní	právní	k2eAgFnSc4d1
váhu	váha	k1gFnSc4
vzhledem	vzhledem	k7c3
k	k	k7c3
obchodnímu	obchodní	k2eAgNnSc3d1
tajemství	tajemství	k1gNnSc3
odvozeného	odvozený	k2eAgNnSc2d1
od	od	k7c2
díla	dílo	k1gNnSc2
licencovaného	licencovaný	k2eAgNnSc2d1
pod	pod	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Žalovaní	žalovaný	k1gMnPc1
argumentovali	argumentovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nemožné	možný	k2eNgNnSc1d1
dodržet	dodržet	k5eAaPmF
obchodní	obchodní	k2eAgNnSc4d1
tajemství	tajemství	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nP
postupovali	postupovat	k5eAaImAgMnP
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
GPL	GPL	kA
a	a	k8xC
šířili	šířit	k5eAaImAgMnP
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
obchodní	obchodní	k2eAgNnSc4d1
tajemství	tajemství	k1gNnSc4
neporušují	porušovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
argument	argument	k1gInSc1
byl	být	k5eAaImAgInS
posouzen	posoudit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
nepodložený	podložený	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2006	#num#	k4
projekt	projekt	k1gInSc1
gpl-violations	gpl-violations	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
soudním	soudní	k2eAgInSc6d1
sporu	spor	k1gInSc6
proti	proti	k7c3
D-Link	D-Link	k1gInSc1
Germany	German	k1gMnPc4
GmbH	GmbH	k1gMnSc2
<g/>
,	,	kIx,
vedeného	vedený	k2eAgInSc2d1
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
D-Link	D-Link	k1gInSc1
používal	používat	k5eAaImAgInS
linuxové	linuxový	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
nepřípustným	přípustný	k2eNgMnSc7d1
a	a	k8xC
autorské	autorský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
porušujícím	porušující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Rozsudek	rozsudek	k1gInSc1
konečně	konečně	k6eAd1
poskytl	poskytnout	k5eAaPmAgInS
"	"	kIx"
<g/>
černě	černě	k6eAd1
na	na	k7c6
bílém	bílé	k1gNnSc6
<g/>
"	"	kIx"
pravomocné	pravomocný	k2eAgNnSc4d1
soudní	soudní	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
odůvodnění	odůvodnění	k1gNnSc2
je	být	k5eAaImIp3nS
GPL	GPL	kA
platná	platný	k2eAgFnSc1d1
a	a	k8xC
právně	právně	k6eAd1
závazná	závazný	k2eAgNnPc1d1
<g/>
;	;	kIx,
v	v	k7c6
dalších	další	k2eAgInPc6d1
sporech	spor	k1gInPc6
lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
jiné	jiný	k2eAgInPc1d1
německé	německý	k2eAgInPc1d1
soudy	soud	k1gInPc1
se	se	k3xPyFc4
přikloní	přiklonit	k5eAaPmIp3nP
k	k	k7c3
tomuto	tento	k3xDgInSc3
právnímu	právní	k2eAgInSc3d1
názoru	názor	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
zahájili	zahájit	k5eAaPmAgMnP
vývojáři	vývojář	k1gMnPc1
projektu	projekt	k1gInSc2
BusyBox	BusyBox	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
Software	software	k1gInSc1
Freedom	Freedom	k1gInSc1
Law	Law	k1gFnPc2
Center	centrum	k1gNnPc2
program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
distributoři	distributor	k1gMnPc1
BusyBoxu	BusyBox	k1gInSc2
v	v	k7c4
embedded	embedded	k1gInSc4
systémech	systém	k1gInPc6
dodržovali	dodržovat	k5eAaImAgMnP
GPL	GPL	kA
<g/>
,	,	kIx,
a	a	k8xC
ti	ten	k3xDgMnPc1
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
ji	on	k3xPp3gFnSc4
dodržovat	dodržovat	k5eAaImF
nebudou	být	k5eNaImBp3nP
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
žalováni	žalovat	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
označeno	označit	k5eAaPmNgNnS
za	za	k7c4
první	první	k4xOgNnSc4
americké	americký	k2eAgNnSc4d1
použití	použití	k1gNnSc4
soudů	soud	k1gInPc2
pro	pro	k7c4
vymáhání	vymáhání	k1gNnSc4
povinností	povinnost	k1gFnPc2
plynoucích	plynoucí	k2eAgFnPc2d1
z	z	k7c2
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
Soudní	soudní	k2eAgFnPc1d1
spory	spor	k1gInPc4
ohledně	ohledně	k7c2
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s>
Kompatibilita	kompatibilita	k1gFnSc1
a	a	k8xC
vícenásobné	vícenásobný	k2eAgNnSc1d1
licencování	licencování	k1gNnSc1
</s>
<s>
Mnoho	mnoho	k4c1
nejběžnějších	běžný	k2eAgFnPc2d3
licencí	licence	k1gFnPc2
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
původní	původní	k2eAgMnSc1d1
MIT	MIT	kA
<g/>
/	/	kIx~
<g/>
X	X	kA
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
BSD	BSD	kA
licence	licence	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
současné	současný	k2eAgFnSc6d1
tříklauzulové	tříklauzulový	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
LGPL	LGPL	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
kompatibilní	kompatibilní	k2eAgInPc1d1
s	s	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kód	kód	k1gInSc4
pod	pod	k7c7
těmito	tento	k3xDgFnPc7
licencemi	licence	k1gFnPc7
lze	lze	k6eAd1
bez	bez	k7c2
konfliktu	konflikt	k1gInSc2
kombinovat	kombinovat	k5eAaImF
s	s	k7c7
programem	program	k1gInSc7
pod	pod	k7c4
GPL	GPL	kA
(	(	kIx(
<g/>
na	na	k7c4
celou	celý	k2eAgFnSc4d1
nově	nově	k6eAd1
vzniklou	vzniklý	k2eAgFnSc4d1
kombinaci	kombinace	k1gFnSc4
by	by	kYmCp3nP
se	se	k3xPyFc4
pak	pak	k6eAd1
vztahovala	vztahovat	k5eAaImAgFnS
GPL	GPL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
licence	licence	k1gFnPc1
svobodného	svobodný	k2eAgNnSc2d1
a	a	k8xC
open	open	k1gNnSc4
source	sourec	k1gInSc2
softwaru	software	k1gInSc2
však	však	k9
nejsou	být	k5eNaImIp3nP
kompatibilní	kompatibilní	k2eAgInPc1d1
s	s	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
příznivců	příznivec	k1gMnPc2
GPL	GPL	kA
silně	silně	k6eAd1
obhajuje	obhajovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vývojáři	vývojář	k1gMnPc7
svobodného	svobodný	k2eAgNnSc2d1
a	a	k8xC
open	open	k1gNnSc4
source	sourec	k1gInSc2
softwaru	software	k1gInSc2
používali	používat	k5eAaImAgMnP
pouze	pouze	k6eAd1
licence	licence	k1gFnPc4
kompatibilní	kompatibilní	k2eAgFnPc4d1
s	s	k7c7
GPL	GPL	kA
<g/>
,	,	kIx,
protože	protože	k8xS
pokud	pokud	k8xS
by	by	kYmCp3nP
činili	činit	k5eAaImAgMnP
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
by	by	kYmCp3nS
složité	složitý	k2eAgNnSc1d1
používat	používat	k5eAaImF
takový	takový	k3xDgInSc4
software	software	k1gInSc4
ve	v	k7c6
větších	veliký	k2eAgInPc6d2
celcích	celek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všimněte	všimnout	k5eAaPmRp2nP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
vyvstává	vyvstávat	k5eAaImIp3nS
jen	jen	k9
u	u	k7c2
současného	současný	k2eAgNnSc2d1
použití	použití	k1gNnSc2
licencí	licence	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
stanovují	stanovovat	k5eAaImIp3nP
podmínky	podmínka	k1gFnPc4
dané	daný	k2eAgFnPc4d1
způsobem	způsob	k1gInSc7
kombinace	kombinace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
BSD	BSD	kA
<g/>
,	,	kIx,
žádné	žádný	k3yNgFnPc1
takové	takový	k3xDgFnPc1
podmínky	podmínka	k1gFnPc1
nestanovují	stanovovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
seznam	seznam	k1gInSc4
licencí	licence	k1gFnPc2
schválených	schválený	k2eAgInPc2d1
FSF	FSF	kA
na	na	k7c4
příklady	příklad	k1gInPc4
kompatibilních	kompatibilní	k2eAgFnPc2d1
a	a	k8xC
nekompatibilních	kompatibilní	k2eNgFnPc2d1
licencí	licence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
obchodních	obchodní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
používá	používat	k5eAaImIp3nS
duální	duální	k2eAgNnSc4d1
licencování	licencování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíří	šířit	k5eAaImIp3nS
GPL	GPL	kA
verzi	verze	k1gFnSc4
a	a	k8xC
prodávají	prodávat	k5eAaImIp3nP
proprietární	proprietární	k2eAgFnPc1d1
licence	licence	k1gFnPc1
firmám	firma	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
chtějí	chtít	k5eAaImIp3nP
kombinovat	kombinovat	k5eAaImF
tento	tento	k3xDgInSc4
software	software	k1gInSc4
s	s	k7c7
proprietárním	proprietární	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
používají	používat	k5eAaImIp3nP
dynamické	dynamický	k2eAgNnSc4d1
linkování	linkování	k1gNnSc4
či	či	k8xC
nikoli	nikoli	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
příklady	příklad	k1gInPc4
takových	takový	k3xDgFnPc2
společností	společnost	k1gFnPc2
patří	patřit	k5eAaImIp3nP
MySQL	MySQL	k1gMnPc1
AB	AB	kA
<g/>
,	,	kIx,
Qt	Qt	k1gFnSc1
Software	software	k1gInSc1
(	(	kIx(
<g/>
Qt	Qt	k1gMnSc1
toolkit	toolkit	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Namesys	Namesys	k1gInSc1
(	(	kIx(
<g/>
ReiserFS	ReiserFS	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Red	Red	k1gFnSc1
Hat	hat	k0
(	(	kIx(
<g/>
Cygwin	Cygwina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přijetí	přijetí	k1gNnSc1
GPL	GPL	kA
</s>
<s>
Open	Open	k1gNnSc1
Source	Sourka	k1gFnSc3
License	Licensa	k1gFnSc3
Resource	Resourka	k1gFnSc3
Center	centrum	k1gNnPc2
spravované	spravovaný	k2eAgInPc1d1
Black	Black	k1gInSc4
Duck	Duck	k1gInSc1
Software	software	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
licenci	licence	k1gFnSc4
GPL	GPL	kA
používá	používat	k5eAaImIp3nS
okolo	okolo	k7c2
60	#num#	k4
%	%	kIx~
veškerého	veškerý	k3xTgNnSc2
open	open	k1gInSc4
source	sourec	k1gInPc1
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Podstatná	podstatný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
projektů	projekt	k1gInPc2
je	být	k5eAaImIp3nS
vydávána	vydávat	k5eAaImNgFnS,k5eAaPmNgFnS
pod	pod	k7c4
GPL	GPL	kA
2	#num#	k4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
projektů	projekt	k1gInPc2
již	již	k6eAd1
migrovaly	migrovat	k5eAaImAgFnP
na	na	k7c6
GPL	GPL	kA
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
označil	označit	k5eAaPmAgMnS
Steve	Steve	k1gMnSc1
Ballmer	Ballmer	k1gMnSc1
<g/>
,	,	kIx,
CEO	CEO	kA
společnosti	společnost	k1gFnSc2
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Linux	Linux	kA
za	za	k7c4
"	"	kIx"
<g/>
rakovinu	rakovina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
uchytí	uchytit	k5eAaPmIp3nS
na	na	k7c6
veškerém	veškerý	k3xTgNnSc6
intelektuálním	intelektuální	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
<g/>
,	,	kIx,
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
dotkne	dotknout	k5eAaPmIp3nS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Kritikové	kritik	k1gMnPc1
Microsoftu	Microsoft	k1gInSc2
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
skutečným	skutečný	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
proč	proč	k6eAd1
Microsoft	Microsoft	kA
nemá	mít	k5eNaImIp3nS
rád	rád	k2eAgMnSc1d1
GPL	GPL	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
GPL	GPL	kA
brání	bránit	k5eAaImIp3nP
pokusům	pokus	k1gInPc3
dodavatelů	dodavatel	k1gMnPc2
proprietárního	proprietární	k2eAgInSc2d1
softwaru	software	k1gInSc2
"	"	kIx"
<g/>
uchopit	uchopit	k5eAaPmF
<g/>
,	,	kIx,
rozšířit	rozšířit	k5eAaPmF
<g/>
,	,	kIx,
zničit	zničit	k5eAaPmF
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Microsoft	Microsoft	kA
vydal	vydat	k5eAaPmAgMnS
Microsoft	Microsoft	kA
Windows	Windows	kA
Services	Services	k1gMnSc1
for	forum	k1gNnPc2
UNIX	UNIX	kA
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInSc1d1
kód	kód	k1gInSc1
licencovaný	licencovaný	k2eAgInSc1d1
pod	pod	k7c7
GPL	GPL	kA
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
útoky	útok	k1gInPc4
Microsoftu	Microsoft	k1gInSc2
proti	proti	k7c3
GPL	GPL	kA
<g/>
,	,	kIx,
vydali	vydat	k5eAaPmAgMnP
mnozí	mnohý	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
a	a	k8xC
obhájci	obhájce	k1gMnPc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
společné	společný	k2eAgNnSc4d1
prohlášení	prohlášení	k1gNnSc4
podporující	podporující	k2eAgFnSc4d1
licenci	licence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
GPL	GPL	kA
je	být	k5eAaImIp3nS
mnoha	mnoho	k4c3
kritiky	kritika	k1gFnSc2
označována	označovat	k5eAaImNgFnS
jako	jako	k9
"	"	kIx"
<g/>
virální	virální	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
protože	protože	k8xS
GPL	GPL	kA
povoluje	povolovat	k5eAaImIp3nS
vydávat	vydávat	k5eAaImF,k5eAaPmF
pod	pod	k7c7
touto	tento	k3xDgFnSc7
licencí	licence	k1gFnSc7
pouze	pouze	k6eAd1
celé	celý	k2eAgInPc1d1
programy	program	k1gInPc1
–	–	k?
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
programátoři	programátor	k1gMnPc1
nesmí	smět	k5eNaImIp3nP
vydávat	vydávat	k5eAaImF,k5eAaPmF
programy	program	k1gInPc4
<g/>
,	,	kIx,
linkující	linkující	k2eAgInSc4d1
GPL	GPL	kA
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
nekompatibilními	kompatibilní	k2eNgFnPc7d1
licencemi	licence	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takzvaný	takzvaný	k2eAgInSc1d1
"	"	kIx"
<g/>
virální	virální	k2eAgInSc1d1
<g/>
"	"	kIx"
efekt	efekt	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
nelze	lze	k6eNd1
různě	různě	k6eAd1
licencovaný	licencovaný	k2eAgInSc1d1
software	software	k1gInSc1
kombinovat	kombinovat	k5eAaImF
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
licencí	licence	k1gFnPc2
změněna	změnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byť	byť	k8xS
lze	lze	k6eAd1
teoreticky	teoreticky	k6eAd1
změnit	změnit	k5eAaPmF
kteroukoli	kterýkoli	k3yIgFnSc4
z	z	k7c2
licencí	licence	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
"	"	kIx"
<g/>
virálním	virální	k2eAgInSc6d1
<g/>
"	"	kIx"
scénáři	scénář	k1gInSc6
nelze	lze	k6eNd1
prakticky	prakticky	k6eAd1
změnit	změnit	k5eAaPmF
GPL	GPL	kA
(	(	kIx(
<g/>
protože	protože	k8xS
software	software	k1gInSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
mnoho	mnoho	k4c4
přispěvatelů	přispěvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
někteří	některý	k3yIgMnPc1
změnu	změna	k1gFnSc4
téměř	téměř	k6eAd1
jistě	jistě	k6eAd1
odmítnou	odmítnout	k5eAaPmIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
licence	licence	k1gFnPc4
druhého	druhý	k4xOgInSc2
softwaru	software	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
praxi	praxe	k1gFnSc6
změněna	změnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
filosofické	filosofický	k2eAgFnSc2d1
odlišnosti	odlišnost	k1gFnSc2
mezi	mezi	k7c7
GPL	GPL	kA
a	a	k8xC
permisivními	permisivní	k2eAgFnPc7d1
licencemi	licence	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
BSD	BSD	kA
licence	licence	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nemají	mít	k5eNaImIp3nP
na	na	k7c4
modifikované	modifikovaný	k2eAgMnPc4d1
verze	verze	k1gFnSc1
takové	takový	k3xDgInPc4
požadavky	požadavek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznivci	příznivec	k1gMnPc1
GPL	GPL	kA
zastávají	zastávat	k5eAaImIp3nP
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
zajistit	zajistit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc2
svobody	svoboda	k1gFnSc2
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgInP
po	po	k7c4
celou	celý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
od	od	k7c2
vývojáře	vývojář	k1gMnSc2
k	k	k7c3
uživateli	uživatel	k1gMnSc3
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
jiní	jiný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
zase	zase	k9
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
prostředníci	prostředník	k1gMnPc1
mezi	mezi	k7c7
vývojáři	vývojář	k1gMnPc7
a	a	k8xC
uživateli	uživatel	k1gMnPc7
by	by	kYmCp3nS
měli	mít	k5eAaImAgMnP
mít	mít	k5eAaImF
svobodu	svoboda	k1gFnSc4
šířit	šířit	k5eAaImF
tento	tento	k3xDgInSc4
software	software	k1gInSc4
jako	jako	k8xC,k8xS
nesvobodný	svobodný	k2eNgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specifičtěji	specificky	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
GPL	GPL	kA
vyžaduje	vyžadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
redistribuce	redistribuce	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
pod	pod	k7c4
GPL	GPL	kA
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
permisivnější	permisivný	k2eAgFnPc1d2
licence	licence	k1gFnPc1
povolují	povolovat	k5eAaImIp3nP
redistribuci	redistribuce	k1gFnSc4
pod	pod	k7c7
restriktivnějšími	restriktivní	k2eAgFnPc7d2
licencemi	licence	k1gFnPc7
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
původní	původní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
GPL	GPL	kA
povoluje	povolovat	k5eAaImIp3nS
komerční	komerční	k2eAgFnSc4d1
distribuci	distribuce	k1gFnSc4
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
tržní	tržní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
bude	být	k5eAaImBp3nS
stlačena	stlačit	k5eAaPmNgFnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
ceny	cena	k1gFnSc2
distribuce	distribuce	k1gFnSc1
—	—	k?
blízkou	blízký	k2eAgFnSc7d1
nule	nula	k1gFnSc3
—	—	k?
<g/>
,	,	kIx,
protože	protože	k8xS
zákazníci	zákazník	k1gMnPc1
mohou	moct	k5eAaImIp3nP
dále	daleko	k6eAd2
šířit	šířit	k5eAaImF
tento	tento	k3xDgInSc4
software	software	k1gInSc4
a	a	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
za	za	k7c4
náklady	náklad	k1gInPc4
redistribuce	redistribuce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
toto	tento	k3xDgNnSc4
by	by	kYmCp3nP
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
nahlížet	nahlížet	k5eAaImF
jako	jako	k9
na	na	k7c4
překážku	překážka	k1gFnSc4
v	v	k7c6
komerčním	komerční	k2eAgNnSc6d1
užití	užití	k1gNnSc6
GPL	GPL	kA
softwaru	software	k1gInSc2
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
chtějí	chtít	k5eAaImIp3nP
tento	tento	k3xDgInSc4
kód	kód	k1gInSc4
použít	použít	k5eAaPmF
pro	pro	k7c4
proprietární	proprietární	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nechtějí	chtít	k5eNaImIp3nP
využít	využít	k5eAaPmF
kódu	kód	k1gInSc2
pod	pod	k7c4
GPL	GPL	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
reimplementovat	reimplementovat	k5eAaImF,k5eAaBmF,k5eAaPmF
sami	sám	k3xTgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
zahrnul	zahrnout	k5eAaPmAgInS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
open	open	k1gInSc4
source	sourec	k1gInPc1
softwaru	software	k1gInSc2
podmínky	podmínka	k1gFnSc2
proti	proti	k7c3
GPL	GPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Navíc	navíc	k6eAd1
projekt	projekt	k1gInSc4
FreeBSD	FreeBSD	k1gFnSc2
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
méně	málo	k6eAd2
veřejně	veřejně	k6eAd1
známou	známý	k2eAgFnSc7d1
a	a	k8xC
nezamýšlenou	zamýšlený	k2eNgFnSc7d1
funkcí	funkce	k1gFnSc7
GPL	GPL	kA
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
příznivá	příznivý	k2eAgFnSc1d1
pro	pro	k7c4
velké	velká	k1gFnPc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
chtějí	chtít	k5eAaImIp3nP
podkopávat	podkopávat	k5eAaImF
jiné	jiný	k2eAgFnPc4d1
softwarové	softwarový	k2eAgFnPc4d1
firmy	firma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
GPL	GPL	kA
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vhodná	vhodný	k2eAgFnSc1d1
k	k	k7c3
použití	použití	k1gNnSc3
jako	jako	k8xS,k8xC
marketingová	marketingový	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
<g/>
,	,	kIx,
potenciálně	potenciálně	k6eAd1
omezující	omezující	k2eAgInSc1d1
celkový	celkový	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
přínos	přínos	k1gInSc1
a	a	k8xC
přispívající	přispívající	k2eAgFnPc1d1
k	k	k7c3
monopolistickém	monopolistický	k2eAgNnSc6d1
chování	chování	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Není	být	k5eNaImIp3nS
však	však	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
nějaké	nějaký	k3yIgInPc1
takové	takový	k3xDgInPc1
případy	případ	k1gInPc1
nastaly	nastat	k5eAaPmAgInP
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
GPL	GPL	kA
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
odškodňovací	odškodňovací	k2eAgFnSc4d1
klauzuli	klauzule	k1gFnSc4
explicitně	explicitně	k6eAd1
chránící	chránící	k2eAgMnPc4d1
správce	správce	k1gMnPc4
a	a	k8xC
vývojáře	vývojář	k1gMnPc4
proti	proti	k7c3
sporům	spor	k1gInPc3
vycházejícím	vycházející	k2eAgInPc3d1
z	z	k7c2
bezohledného	bezohledný	k2eAgNnSc2d1
přispívání	přispívání	k1gNnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Pokud	pokud	k8xS
vývojář	vývojář	k1gMnSc1
vloží	vložit	k5eAaPmIp3nP
existující	existující	k2eAgFnPc4d1
patentované	patentovaný	k2eAgFnPc4d1
nebo	nebo	k8xC
autorsky	autorsky	k6eAd1
chráněné	chráněný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
do	do	k7c2
GPL	GPL	kA
projektu	projekt	k1gInSc2
s	s	k7c7
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vlastní	vlastní	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
správci	správce	k1gMnPc1
projektu	projekt	k1gInSc2
i	i	k9
další	další	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
právně	právně	k6eAd1
odpovědni	odpověden	k2eAgMnPc1d1
za	za	k7c2
škody	škoda	k1gFnSc2
vzniklé	vzniklý	k2eAgFnSc2d1
držitelům	držitel	k1gMnPc3
autorských	autorský	k2eAgNnPc2d1
nebo	nebo	k8xC
patentových	patentový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Absence	absence	k1gFnSc1
odškodnění	odškodnění	k1gNnSc4
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
kritik	kritika	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vedla	vést	k5eAaImAgFnS
Mozillu	Mozilla	k1gFnSc4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
Mozilla	Mozilla	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
namísto	namísto	k7c2
použití	použití	k1gNnSc2
GPL	GPL	kA
nebo	nebo	k8xC
LGPL	LGPL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
však	však	k9
Mozilla	Mozilla	k1gFnSc1
přelicencovala	přelicencovat	k5eAaImAgFnS,k5eAaPmAgFnS,k5eAaBmAgFnS
svá	svůj	k3xOyFgNnPc4
díla	dílo	k1gNnPc4
pod	pod	k7c4
trojitou	trojitý	k2eAgFnSc4d1
licenci	licence	k1gFnSc4
GPL	GPL	kA
<g/>
/	/	kIx~
<g/>
LGPL	LGPL	kA
<g/>
/	/	kIx~
<g/>
MPL	MPL	kA
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
s	s	k7c7
nekompatibilitou	nekompatibilita	k1gFnSc7
MPL	MPL	kA
s	s	k7c7
GPL	GPL	kA
<g/>
.	.	kIx.
</s>
<s>
Někteří	některý	k3yIgMnPc1
softwaroví	softwarový	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
shledali	shledat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
širším	široký	k2eAgInSc6d2
pohledu	pohled	k1gInSc6
je	být	k5eAaImIp3nS
GPL	GPL	kA
příliš	příliš	k6eAd1
restriktivní	restriktivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Bjø	Bjø	k1gInSc4
Reese	Reese	k1gFnSc2
a	a	k8xC
Daniel	Daniel	k1gMnSc1
Stenberg	Stenberg	k1gMnSc1
cítili	cítit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
volba	volba	k1gFnSc1
GPL	GPL	kA
pro	pro	k7c4
software	software	k1gInSc4
vytvořila	vytvořit	k5eAaPmAgFnS
"	"	kIx"
<g/>
quodque	quodque	k1gFnSc4
pro	pro	k7c4
quo	quo	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
lat.	lat.	k?
"	"	kIx"
<g/>
všechno	všechen	k3xTgNnSc1
na	na	k7c4
oplátku	oplátka	k1gFnSc4
za	za	k7c4
něco	něco	k3yInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
pro	pro	k7c4
vývojáře	vývojář	k1gMnSc4
jiného	jiný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
s	s	k7c7
tím	ten	k3xDgNnSc7
jejich	jejich	k3xOp3gNnSc7
linkuje	linkovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
ukončili	ukončit	k5eAaPmAgMnP
použití	použití	k1gNnSc4
GPLv	GPLv	k1gInSc1
<g/>
2	#num#	k4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
méně	málo	k6eAd2
restriktivních	restriktivní	k2eAgFnPc2d1
copyleftových	copyleftův	k2eAgFnPc2d1
licencí	licence	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xS,k8xC
specifický	specifický	k2eAgInSc1d1
příklad	příklad	k1gInSc1
nekompatibility	nekompatibilita	k1gFnSc2
licencí	licence	k1gFnPc2
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
ZFS	ZFS	kA
firmy	firma	k1gFnSc2
Sun	Sun	kA
Microsystems	Microsystems	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
licencován	licencován	k2eAgMnSc1d1
pod	pod	k7c7
licencí	licence	k1gFnSc7
CDDL	CDDL	kA
nekompatibilní	kompatibilní	k2eNgInPc4d1
s	s	k7c7
GPL	GPL	kA
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc4
patentů	patent	k1gInPc2
firmy	firma	k1gFnSc2
Sun	Sun	kA
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
jej	on	k3xPp3gInSc4
nelze	lze	k6eNd1
linkovat	linkovat	k5eAaImF
s	s	k7c7
linuxovým	linuxový	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
také	také	k9
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
GPL	GPL	kA
mohla	moct	k5eAaImAgFnS
a	a	k8xC
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
kratší	krátký	k2eAgFnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licensa	k1gFnSc6
</s>
<s>
Affero	Affero	k1gNnSc1
General	General	k1gFnSc2
Public	publicum	k1gNnPc2
License	Licensa	k1gFnSc6
</s>
<s>
Softwarové	softwarový	k2eAgFnPc1d1
licence	licence	k1gFnPc1
</s>
<s>
Copyleft	Copyleft	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
GNU	gnu	k1gNnSc2
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Prezentace	prezentace	k1gFnPc1
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
na	na	k7c6
druhé	druhý	k4xOgFnSc6
mezinárodní	mezinárodní	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
GPLv	GPLva	k1gFnPc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímý	přímý	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
na	na	k7c6
sekci	sekce	k1gFnSc6
o	o	k7c6
prehistorii	prehistorie	k1gFnSc6
GPL	GPL	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Freshmeat	Freshmeat	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
statistics	statistics	k6eAd1
page	page	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SourceForge	SourceForge	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
:	:	kIx,
Software	software	k1gInSc1
Map	mapa	k1gFnPc2
<g/>
↑	↑	k?
David	David	k1gMnSc1
A.	A.	kA
Wheeler	Wheeler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Estimating	Estimating	k1gInSc1
Linux	Linux	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Size	Size	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Eric	Eric	k1gInSc1
S.	S.	kA
Raymond	Raymond	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Homesteading	Homesteading	k1gInSc1
the	the	k?
Noosphere	Noospher	k1gMnSc5
<g/>
,	,	kIx,
<g/>
"	"	kIx"
odkazováno	odkazovat	k5eAaImNgNnS
v	v	k7c6
Make	Make	k1gFnSc6
Your	Your	k1gMnSc1
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
Software	software	k1gInSc4
GPL-Compatible	GPL-Compatible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Or	Or	k1gFnSc1
Else	Elsa	k1gFnSc6
By	by	k9
David	David	k1gMnSc1
A.	A.	kA
Wheeler	Wheeler	k1gMnSc1
<g/>
↑	↑	k?
proč	proč	k6eAd1
GPL	GPL	kA
odstartovala	odstartovat	k5eAaPmAgFnS
Linux	linux	k1gInSc4
k	k	k7c3
úspěchu	úspěch	k1gInSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Prezentace	prezentace	k1gFnPc1
Richarda	Richard	k1gMnSc2
Stallmana	Stallman	k1gMnSc2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
na	na	k7c6
druhé	druhý	k4xOgFnSc6
mezinárodní	mezinárodní	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
GPLv	GPLva	k1gFnPc2
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímý	přímý	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
na	na	k7c6
sekci	sekce	k1gFnSc6
o	o	k7c4
klauzuli	klauzule	k1gFnSc4
"	"	kIx"
<g/>
svobodu	svoboda	k1gFnSc4
nebo	nebo	k8xC
život	život	k1gInSc4
<g/>
"	"	kIx"
<g/>
.1	.1	k4
2	#num#	k4
Prezentace	prezentace	k1gFnSc1
Richarda	Richard	k1gMnSc4
Stallmana	Stallman	k1gMnSc4
25.2	25.2	k4
<g/>
.2006	.2006	k4
<g/>
,	,	kIx,
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc1
–	–	k?
první	první	k4xOgInSc4
den	den	k1gInSc4
každoroční	každoroční	k2eAgFnSc2d1
konference	konference	k1gFnSc2
FOSDEM	FOSDEM	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Interview	interview	k1gNnSc1
s	s	k7c7
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gInSc7
Archivováno	archivován	k2eAgNnSc4d1
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Free	Free	k1gNnPc7
Software	software	k1gInSc1
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GPLv	GPLv	k1gInSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
Drafting	Drafting	k1gInSc1
version	version	k1gInSc1
3	#num#	k4
of	of	k?
the	the	k?
GNU	gnu	k1gNnSc2
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
Europe	Europ	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
komentáře	komentář	k1gInPc1
k	k	k7c3
diskusnímu	diskusní	k2eAgInSc3d1
draftu	draft	k1gInSc3
4	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
komentáře	komentář	k1gInPc1
k	k	k7c3
draftu	draft	k1gInSc3
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
komentáře	komentář	k1gInPc1
k	k	k7c3
draftu	draft	k1gInSc3
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
komentáře	komentář	k1gInPc1
k	k	k7c3
draftu	draft	k1gInSc3
3	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
gplv	gplv	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
fsf	fsf	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
komentáře	komentář	k1gInPc1
k	k	k7c3
draftu	draft	k1gInSc3
4	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
third	thirdo	k1gNnPc2
draft	draft	k1gInSc1
of	of	k?
GPLv	GPLv	k1gInSc1
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Final	Final	k1gInSc1
Discussion	Discussion	k1gInSc1
Draft	draft	k1gInSc4
<g/>
,	,	kIx,
ověřeno	ověřen	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
↑	↑	k?
GPLv	GPLv	k1gMnSc1
<g/>
3	#num#	k4
FAQ	FAQ	kA
<g/>
,	,	kIx,
ověřeno	ověřen	k2eAgNnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
čitelnost	čitelnost	k1gFnSc4
z	z	k7c2
FAQ	FAQ	kA
<g/>
,	,	kIx,
nikoli	nikoli	k9
z	z	k7c2
licence	licence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fourth	Fourth	k1gInSc1
Discussion	Discussion	k1gInSc1
Draft	draft	k1gInSc4
Rationale	Rationale	k1gFnSc2
<g/>
,	,	kIx,
ověřeno	ověřen	k2eAgNnSc1d1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Postoj	postoj	k1gInSc1
vývojářů	vývojář	k1gMnPc2
jádra	jádro	k1gNnSc2
ke	k	k7c3
GPLv	GPLvum	k1gNnPc2
<g/>
3	#num#	k4
--	--	k?
http://lwn.net/Articles/200422/,	http://lwn.net/Articles/200422/,	k4
ověřeno	ověřen	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
↑	↑	k?
Selling	Selling	k1gInSc1
Free	Free	k1gInSc1
Software	software	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Essay	Essaa	k1gFnSc2
by	by	kYmCp3nS
Stallman	Stallman	k1gMnSc1
explaining	explaining	k1gInSc1
why	why	k?
a	a	k8xC
licence	licence	k1gFnSc1
is	is	k?
more	mor	k1gInSc5
suitable	suitable	k6eAd1
than	thana	k1gFnPc2
a	a	k8xC
contract	contracta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Eben	eben	k1gInSc1
Moglen	Moglen	k1gInSc1
explaining	explaining	k1gInSc1
why	why	k?
the	the	k?
GPL	GPL	kA
is	is	k?
a	a	k8xC
licence	licence	k1gFnSc2
and	and	k?
why	why	k?
it	it	k?
matters	matters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
GUADAMUZ-GONZALEZ	GUADAMUZ-GONZALEZ	k?
<g/>
,	,	kIx,
Andres	Andres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viral	Viral	k1gInSc1
contracts	contracts	k1gInSc4
or	or	k?
unenforceable	unenforceable	k6eAd1
documents	documents	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Contractual	Contractual	k1gInSc1
validity	validita	k1gFnSc2
of	of	k?
copyleft	copyleft	k1gMnSc1
licences	licences	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	European	k1gMnSc1
Intellectual	Intellectual	k1gMnSc1
Property	Propert	k1gMnPc4
Review	Review	k1gFnPc2
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
121	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
právu	právo	k1gNnSc6
autorském	autorský	k2eAgNnSc6d1
<g/>
,	,	kIx,
o	o	k7c6
právech	právo	k1gNnPc6
souvisejících	související	k2eAgFnPc2d1
s	s	k7c7
právem	právo	k1gNnSc7
autorským	autorský	k2eAgNnSc7d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
INFO@AION.CZ	INFO@AION.CZ	k1gMnSc1
<g/>
,	,	kIx,
AION	AION	kA
CS-	CS-	k1gFnSc1
<g/>
.	.	kIx.
89	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
(	(	kIx(
<g/>
nový	nový	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Živý	živý	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
<g/>
:	:	kIx,
s	s	k7c7
Josefem	Josef	k1gMnSc7
Aujezdským	Aujezdský	k2eAgMnSc7d1
o	o	k7c6
právní	právní	k2eAgFnSc6d1
problematice	problematika	k1gFnSc6
související	související	k2eAgInPc1d1
s	s	k7c7
IT	IT	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GPL	GPL	kA
FAQ	FAQ	kA
<g/>
:	:	kIx,
Can	Can	k1gFnPc1
I	i	k8xC
modify	modif	k1gInPc1
the	the	k?
GPL	GPL	kA
and	and	k?
make	make	k1gInSc1
a	a	k8xC
modified	modified	k1gInSc1
licence	licence	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Can	Can	k1gFnSc2
I	i	k8xC
apply	appla	k1gFnSc2
the	the	k?
GPL	GPL	kA
when	when	k1gInSc1
writing	writing	k1gInSc1
a	a	k8xC
plug-in	plug-in	k1gInSc1
for	forum	k1gNnPc2
a	a	k8xC
non-free	non-freat	k5eAaPmIp3nS
program	program	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
GPL	GPL	kA
FAQ	FAQ	kA
<g/>
,	,	kIx,
Free	Free	k1gFnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jerry	Jerra	k1gFnSc2
Epplin	Epplina	k1gFnPc2
<g/>
,	,	kIx,
Using	Using	k1gInSc1
GPL	GPL	kA
software	software	k1gInSc1
in	in	k?
embedded	embedded	k1gInSc1
applications	applications	k1gInSc1
<g/>
,	,	kIx,
LinuxDevices	LinuxDevices	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
4	#num#	k4
March	March	k1gInSc1
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Linus	Linus	k1gInSc1
Torvalds	Torvalds	k1gInSc1
<g/>
,	,	kIx,
GPL	GPL	kA
only	onla	k1gFnPc1
modules	modules	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
linux-kernel	linux-kernet	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
mailing	mailing	k1gInSc1
list	list	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
December	December	k1gInSc1
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Matt	Matt	k1gInSc1
Asay	Asaa	k1gMnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc2
GPL	GPL	kA
<g/>
:	:	kIx,
Understanding	Understanding	k1gInSc1
the	the	k?
License	Licens	k1gMnSc2
that	that	k2eAgInSc4d1
Governs	Governs	k1gInSc4
Linux	Linux	kA
<g/>
,	,	kIx,
Novel	novela	k1gFnPc2
Cool	Coola	k1gFnPc2
Solutions	Solutions	k1gInSc1
Feature	Featur	k1gMnSc5
(	(	kIx(
<g/>
16	#num#	k4
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lawrence	Lawrenec	k1gInPc1
Rosen	rosen	k2eAgInSc1d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Derivative	Derivativ	k1gInSc5
Works	Works	kA
<g/>
,	,	kIx,
Linux	Linux	kA
Journal	Journal	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
Jan	Jana	k1gFnPc2
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lawrence	Lawrenec	k1gInSc2
Rosen	rosit	k5eAaImNgMnS
<g/>
,	,	kIx,
Derivative	Derivativ	k1gInSc5
Works	Works	kA
<g/>
,	,	kIx,
rosenlaw	rosenlaw	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
25	#num#	k4
May	May	k1gMnSc1
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
See	See	k1gFnSc1
Progress	Progress	k1gInSc1
Software	software	k1gInSc1
Corporation	Corporation	k1gInSc1
v.	v.	k?
MySQL	MySQL	k1gFnSc2
AB	AB	kA
<g/>
,	,	kIx,
195	#num#	k4
F.	F.	kA
Supp	Supp	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
d	d	k?
328	#num#	k4
(	(	kIx(
<g/>
D.	D.	kA
Mass	Mass	k1gInSc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
defendant	defendant	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
motion	motion	k1gInSc1
for	forum	k1gNnPc2
preliminary	preliminara	k1gFnSc2
injunction	injunction	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Harald	Harald	k1gMnSc1
Welte	Welt	k1gInSc5
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sitecom	Sitecom	k1gInSc1
<g/>
,	,	kIx,
final	final	k1gMnSc1
order	order	k1gMnSc1
<g/>
,	,	kIx,
translated	translated	k1gMnSc1
from	from	k1gMnSc1
German	German	k1gMnSc1
by	by	kYmCp3nS
Jens	Jens	k1gInSc1
Maurer	Maurer	k1gInSc4
<g/>
↑	↑	k?
Dismissal	Dismissal	k1gFnSc2
of	of	k?
Wallace	Wallace	k1gFnSc2
v.	v.	k?
FSF	FSF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	From	k1gInSc1
this	this	k6eAd1
article	article	k6eAd1
on	on	k3xPp3gMnSc1
Groklaw	Groklaw	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Seoul	Seoul	k1gInSc1
Central	Central	k1gFnSc2
District	District	k2eAgInSc1d1
Court	Court	k1gInSc1
ruling	ruling	k1gInSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
(	(	kIx(
<g/>
in	in	k?
Korean	Korean	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
korea	korea	k1gMnSc1
<g/>
.	.	kIx.
<g/>
gnu	gnu	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
18	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://gpl-violations.org/news/20060922-dlink-judgement_frankfurt.html.	http://gpl-violations.org/news/20060922-dlink-judgement_frankfurt.html.	k4
gpl-violations	gpl-violations	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
D-Link	D-Link	k1gInSc1
Judgement	Judgement	k1gInSc1
<g/>
.	.	kIx.
www.jbb.de	www.jbb.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Open	Open	k1gInSc1
Source	Source	k1gMnSc2
License	Licens	k1gMnSc2
Resource	Resourka	k1gFnSc6
Center	centrum	k1gNnPc2
|	|	kIx~
Black	Black	k1gInSc1
Duck	Duck	k1gMnSc1
Software	software	k1gInSc1
<g/>
.	.	kIx.
www.blackducksoftware.com	www.blackducksoftware.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NEWBART	NEWBART	kA
<g/>
,	,	kIx,
Dave	Dav	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
CEO	CEO	kA
takes	takesa	k1gFnPc2
launch	launcha	k1gFnPc2
break	break	k1gInSc1
with	with	k1gMnSc1
the	the	k?
Sun-Times	Sun-Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
Sun-Times	Sun-Timesa	k1gFnPc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
(	(	kIx(
<g/>
Internet	Internet	k1gInSc1
archive	archiv	k1gInSc5
link	linka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Deadly	Deadly	k1gMnPc4
embrace	embrace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Leaders	Leaders	k1gInSc1
Stand	Standa	k1gFnPc2
Together	Togethra	k1gFnPc2
<g/>
.	.	kIx.
www.perens.com	www.perens.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Speech	speech	k1gInSc4
Transcript	Transcript	k2eAgInSc4d1
–	–	k?
Craig	Craig	k1gInSc4
Mundie	Mundie	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
University	universita	k1gFnSc2
Stern	sternum	k1gNnPc2
School	School	k1gInSc1
of	of	k?
Business	business	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
"	"	kIx"
<g/>
,	,	kIx,
Prepared	Prepared	k1gInSc1
Text	text	k1gInSc1
of	of	k?
Remarks	Remarks	k1gInSc1
by	by	kYmCp3nS
Craig	Craig	k1gInSc4
Mundie	Mundie	k1gFnSc2
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
Senior	senior	k1gMnSc1
Vice	vika	k1gFnSc3
President	president	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Commercial	Commercial	k1gInSc1
Software	software	k1gInSc1
Model	modla	k1gFnPc2
The	The	k1gFnSc2
New	New	k1gFnPc1
York	York	k1gInSc4
University	universita	k1gFnSc2
Stern	sternum	k1gNnPc2
School	School	k1gInSc1
of	of	k?
Business	business	k1gInSc1
May	May	k1gMnSc1
3	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
↑	↑	k?
Microsoft	Microsoft	kA
anti-GPL	anti-GPL	k?
fine	fine	k1gInSc1
print	print	k1gInSc1
threatens	threatens	k1gInSc1
competition	competition	k1gInSc4
|	|	kIx~
The	The	k1gFnPc2
Register	registrum	k1gNnPc2
<g/>
↑	↑	k?
GPL	GPL	kA
Advantages	Advantages	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
and	and	k?
Disadvantages	Disadvantages	k1gMnSc1
<g/>
,	,	kIx,
FreeBSD	FreeBSD	k1gMnSc1
<g/>
:	:	kIx,
doc	doc	kA
<g/>
/	/	kIx~
<g/>
en_US	en_US	k?
<g/>
.	.	kIx.
<g/>
ISO	ISO	kA
<g/>
8859	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
articles	articles	k1gInSc1
<g/>
/	/	kIx~
<g/>
bsdl-gpl	bsdl-gpnout	k5eAaPmAgInS
<g/>
/	/	kIx~
<g/>
article	article	k1gInSc1
<g/>
.	.	kIx.
<g/>
sgml	sgml	k1gInSc1
<g/>
,	,	kIx,
<g/>
v	v	k7c6
1.5	1.5	k4
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
16	#num#	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
keramida	keramid	k1gMnSc2
Exp	exp	kA
<g/>
↑	↑	k?
Bjø	Bjø	k1gMnSc1
Reese	Reese	k1gFnSc2
and	and	k?
Daniel	Daniel	k1gMnSc1
Stenberg	Stenberg	k1gMnSc1
<g/>
,	,	kIx,
Working	Working	k1gInSc1
Without	Without	k1gMnSc1
Copyleft	Copyleft	k1gMnSc1
(	(	kIx(
<g/>
December	December	k1gInSc1
19	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Linux	Linux	kA
<g/>
:	:	kIx,
ZFS	ZFS	kA
<g/>
,	,	kIx,
Licenses	Licenses	k1gInSc1
and	and	k?
Patents	Patents	k1gInSc1
<g/>
.	.	kIx.
kerneltrap	kerneltrap	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
25	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Allison	Allison	k1gMnSc1
Randal	Randal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPLv	GPLv	k1gInSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Clarity	Clarita	k1gFnPc1
and	and	k?
Simplicity	simplicita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
15	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
GNU	gnu	k1gMnSc1
General	General	k1gFnSc4
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
1.0	1.0	k4
–	–	k?
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
zavržena	zavrhnout	k5eAaPmNgFnS
FSF	FSF	kA
<g/>
.	.	kIx.
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
2.0	2.0	k4
–	–	k?
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
zavržena	zavržen	k2eAgNnPc4d1
FSF	FSF	kA
<g/>
,	,	kIx,
ale	ale	k8xC
mnoho	mnoho	k4c4
projektů	projekt	k1gInPc2
–	–	k?
včetně	včetně	k7c2
Linuxu	linux	k1gInSc2
a	a	k8xC
GNU	gnu	k1gNnSc6
–	–	k?
ji	on	k3xPp3gFnSc4
stále	stále	k6eAd1
používá	používat	k5eAaImIp3nS
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
3.0	3.0	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
3.0	3.0	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
3.0	3.0	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
Practical	Practical	k1gFnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
GPL	GPL	kA
Compliance	Compliance	k1gFnSc1
(	(	kIx(
<g/>
Covers	Covers	k1gInSc1
GPLv	GPLv	k1gInSc1
<g/>
2	#num#	k4
and	and	k?
v	v	k7c6
<g/>
3	#num#	k4
<g/>
)	)	kIx)
–	–	k?
from	from	k1gInSc1
the	the	k?
Software	software	k1gInSc1
Freedom	Freedom	k1gInSc1
Law	Law	k1gFnPc2
Center	centrum	k1gNnPc2
</s>
<s>
A	a	k9
paper	paper	k1gMnSc1
on	on	k3xPp3gMnSc1
enforcing	enforcing	k1gInSc4
the	the	k?
GPL	GPL	kA
</s>
<s>
Free	Free	k6eAd1
Software	software	k1gInSc1
Leaders	Leaders	k1gInSc1
Stand	Standa	k1gFnPc2
Together	Togethra	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
joint	joint	k1gMnSc1
statement	statement	k1gMnSc1
in	in	k?
support	support	k1gInSc1
of	of	k?
the	the	k?
GPL	GPL	kA
</s>
<s>
Frequently	Frequentnout	k5eAaPmAgInP
Asked	Asked	k1gInSc4
Questions	Questionsa	k1gFnPc2
about	about	k1gMnSc1
the	the	k?
GPL	GPL	kA
</s>
<s>
GPL	GPL	kA
<g/>
,	,	kIx,
BSD	BSD	kA
<g/>
,	,	kIx,
and	and	k?
NetBSD	NetBSD	k1gFnSc1
–	–	k?
why	why	k?
the	the	k?
GPL	GPL	kA
rocketed	rocketed	k1gInSc1
Linux	linux	k1gInSc1
to	ten	k3xDgNnSc4
success	success	k6eAd1
by	by	k9
David	David	k1gMnSc1
A.	A.	kA
Wheeler	Wheeler	k1gMnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
and	and	k?
Commentaries	Commentaries	k1gMnSc1
–	–	k?
Edited	Edited	k1gMnSc1
by	by	kYmCp3nS
Robert	Robert	k1gMnSc1
Chassell	Chassell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
v	v	k7c6
<g/>
2.1	2.1	k4
</s>
<s>
History	Histor	k1gInPc1
of	of	k?
the	the	k?
GPL	GPL	kA
</s>
<s>
Information	Information	k1gInSc1
Site	Site	k1gNnSc1
Tracking	Tracking	k1gInSc1
Rate	Rate	k1gFnSc1
of	of	k?
GPL	GPL	kA
v	v	k7c6
<g/>
3	#num#	k4
License	Licens	k1gMnSc2
Adoption	Adoption	k1gInSc4
</s>
<s>
List	list	k1gInSc1
of	of	k?
presentation	presentation	k1gInSc1
transcripts	transcripts	k1gInSc1
about	about	k1gInSc4
the	the	k?
GPL	GPL	kA
and	and	k?
free	freat	k5eAaPmIp3nS
software	software	k1gInSc1
licenses	licensesa	k1gFnPc2
</s>
<s>
Make	Make	k5eAaImRp2nP
Your	Your	k1gInSc4
Open	Open	k1gMnSc1
Source	Sourec	k1gInSc2
Software	software	k1gInSc1
GPL-Compatible	GPL-Compatible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Or	Or	k1gFnSc1
Else	Elsa	k1gFnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
David	David	k1gMnSc1
A.	A.	kA
Wheeler	Wheeler	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
April	April	k1gInSc1
2004	#num#	k4
<g/>
)	)	kIx)
—	—	k?
why	why	k?
a	a	k8xC
GPL-compatible	GPL-compatible	k1gMnSc1
licence	licence	k1gFnSc2
is	is	k?
important	important	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
health	health	k1gInSc1
of	of	k?
a	a	k8xC
project	project	k1gMnSc1
</s>
<s>
The	The	k?
Emacs	Emacs	k1gInSc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
February	Februara	k1gFnPc1
1988	#num#	k4
version	version	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
direct	direct	k2eAgInSc1d1
predecessor	predecessor	k1gInSc1
of	of	k?
the	the	k?
GNU	gnu	k1gNnPc2
GPL	GPL	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
Historie	historie	k1gFnSc1
</s>
<s>
GNU	gnu	k1gNnSc1
Manifest	manifest	k1gInSc1
•	•	k?
Projekt	projekt	k1gInSc1
GNU	gnu	k1gMnSc1
•	•	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
•	•	k?
Historie	historie	k1gFnSc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
Affero	Affero	k1gNnSc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Free	Fre	k1gFnSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc2
•	•	k?
GPL	GPL	kA
linking	linking	k1gInSc1
exception	exception	k1gInSc1
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
GNU	gnu	k1gMnSc1
•	•	k?
GNU	gnu	k1gMnSc1
Health	Health	k1gMnSc1
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
NG	NG	kA
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
•	•	k?
GNOME	GNOME	kA
•	•	k?
Gnuzilla	Gnuzilla	k1gMnSc1
•	•	k?
IceCat	IceCat	k1gInSc1
•	•	k?
Gnash	Gnash	k1gInSc1
•	•	k?
Bash	Bash	k1gInSc1
•	•	k?
GCC	GCC	kA
•	•	k?
GNU	gnu	k1gNnSc2
Emacs	Emacsa	k1gFnPc2
•	•	k?
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librara	k1gFnPc1
•	•	k?
Coreutils	Coreutils	k1gInSc1
•	•	k?
GNU	gnu	k1gMnSc1
build	build	k1gMnSc1
system	syst	k1gMnSc7
•	•	k?
gettext	gettext	k1gMnSc1
•	•	k?
Ostatní	ostatní	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
balíčky	balíček	k1gInPc4
a	a	k8xC
programy	program	k1gInPc4
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
J.	J.	kA
Chassell	Chassell	k1gMnSc1
•	•	k?
Loï	Loï	k1gFnSc1
Dachary	Dachara	k1gFnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Galli	Galle	k1gFnSc3
•	•	k?
Georg	Georg	k1gInSc1
C.	C.	kA
F.	F.	kA
Greve	Greev	k1gFnPc1
•	•	k?
Federico	Federico	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Mako	mako	k1gNnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Bradley	Bradlea	k1gFnSc2
M.	M.	kA
Kuhn	Kuhn	k1gMnSc1
•	•	k?
Eben	eben	k1gInSc1
Moglen	Moglen	k1gInSc1
•	•	k?
Brett	Brett	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Leonard	Leonard	k1gMnSc1
H.	H.	kA
Tower	Tower	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
-	-	kIx~
spor	spor	k1gInSc1
o	o	k7c4
jméno	jméno	k1gNnSc4
•	•	k?
Revolution	Revolution	k1gInSc1
OS	OS	kA
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Code	Code	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
