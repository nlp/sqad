<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1754	[number]	k4	1754
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
vládnoucí	vládnoucí	k2eAgInSc4d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
1774	[number]	k4	1774
–	–	k?	–
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vnukem	vnuk	k1gMnSc7	vnuk
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Antonie	Antonie	k1gFnSc1	Antonie
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
