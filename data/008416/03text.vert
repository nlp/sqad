<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1754	[number]	k4	1754
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
vládnoucí	vládnoucí	k2eAgInSc4d1	vládnoucí
v	v	k7c6	v
letech	let	k1gInPc6	let
1774	[number]	k4	1774
–	–	k?	–
1792	[number]	k4	1792
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vnukem	vnuk	k1gMnSc7	vnuk
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Antonie	Antonie	k1gFnSc1	Antonie
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Ludvíkovým	Ludvíkův	k2eAgMnSc7d1	Ludvíkův
předchůdcem	předchůdce	k1gMnSc7	předchůdce
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
trůnu	trůn	k1gInSc6	trůn
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1	Ludvíkův
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
dauphin	dauphin	k1gMnSc1	dauphin
Ludvík	Ludvík	k1gMnSc1	Ludvík
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Bourbonský	bourbonský	k2eAgMnSc1d1	bourbonský
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
–	–	k?	–
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohl	moct	k5eAaImAgMnS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
Saská	saský	k2eAgFnSc1d1	saská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Augusta	Augusta	k1gMnSc1	Augusta
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
a	a	k8xC	a
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
saského	saský	k2eAgMnSc2d1	saský
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
vládl	vládnout	k5eAaImAgInS	vládnout
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Fridrich	Fridrich	k1gMnSc1	Fridrich
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1754	[number]	k4	1754
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
jako	jako	k8xS	jako
třetí	třetí	k4xOgMnSc1	třetí
potomek	potomek	k1gMnSc1	potomek
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
sedmi	sedm	k4xCc2	sedm
dětí	dítě	k1gFnPc2	dítě
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
obdržel	obdržet	k5eAaPmAgMnS	obdržet
titul	titul	k1gInSc4	titul
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Ludvík	Ludvík	k1gMnSc1	Ludvík
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
o	o	k7c4	o
výchovu	výchova	k1gFnSc4	výchova
jeho	jeho	k3xOp3gMnPc2	jeho
sourozenců	sourozenec	k1gMnPc2	sourozenec
i	i	k9	i
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
výchovu	výchova	k1gFnSc4	výchova
se	se	k3xPyFc4	se
starali	starat	k5eAaImAgMnP	starat
k	k	k7c3	k
údivu	údiv	k1gInSc3	údiv
celého	celý	k2eAgInSc2d1	celý
francouzského	francouzský	k2eAgInSc2d1	francouzský
dvora	dvůr	k1gInSc2	dvůr
osobně	osobně	k6eAd1	osobně
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Ludvík	Ludvík	k1gMnSc1	Ludvík
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
plesy	ples	k1gInPc4	ples
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
ani	ani	k8xC	ani
lov	lov	k1gInSc4	lov
mu	on	k3xPp3gMnSc3	on
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
blízký	blízký	k2eAgMnSc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
dauphinem	dauphin	k1gMnSc7	dauphin
a	a	k8xC	a
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
panovaly	panovat	k5eAaImAgFnP	panovat
velké	velký	k2eAgInPc4d1	velký
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
občas	občas	k6eAd1	občas
vůči	vůči	k7c3	vůči
veřejnosti	veřejnost	k1gFnSc3	veřejnost
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
následníci	následník	k1gMnPc1	následník
trůnu	trůn	k1gInSc2	trůn
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
usednout	usednout	k5eAaPmF	usednout
a	a	k8xC	a
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgMnS	najít
oblibu	obliba	k1gFnSc4	obliba
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
chorálů	chorál	k1gInPc2	chorál
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
saská	saský	k2eAgFnSc1d1	saská
princezna	princezna	k1gFnSc1	princezna
Marie	Marie	k1gFnSc1	Marie
Josefa	Josefa	k1gFnSc1	Josefa
byla	být	k5eAaImAgFnS	být
svědomitá	svědomitý	k2eAgFnSc1d1	svědomitá
<g/>
,	,	kIx,	,
taktní	taktní	k2eAgFnSc1d1	taktní
<g/>
,	,	kIx,	,
zbožná	zbožný	k2eAgFnSc1d1	zbožná
a	a	k8xC	a
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
nástupcem	nástupce	k1gMnSc7	nástupce
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
i	i	k9	i
otec	otec	k1gMnSc1	otec
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1765	[number]	k4	1765
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
přímým	přímý	k2eAgMnSc7d1	přímý
následníkem	následník	k1gMnSc7	následník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1770	[number]	k4	1770
se	se	k3xPyFc4	se
patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
o	o	k7c4	o
rok	rok	k1gInSc4	rok
mladší	mladý	k2eAgFnSc1d2	mladší
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
princeznou	princezna	k1gFnSc7	princezna
Marií	Maria	k1gFnSc7	Maria
Antonií	Antonie	k1gFnSc7	Antonie
(	(	kIx(	(
<g/>
Marií	Maria	k1gFnSc7	Maria
Antoinettou	Antoinetta	k1gFnSc7	Antoinetta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
císařského	císařský	k2eAgInSc2d1	císařský
páru	pár	k1gInSc2	pár
Františka	Františka	k1gFnSc1	Františka
I.	I.	kA	I.
Štěpána	Štěpána	k1gFnSc1	Štěpána
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k9	tak
upevnili	upevnit	k5eAaPmAgMnP	upevnit
francouzsko-rakouské	francouzskoakouský	k2eAgInPc4d1	francouzsko-rakouský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
manželství	manželství	k1gNnSc2	manželství
vzešly	vzejít	k5eAaPmAgFnP	vzejít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
Šarlota	Šarlota	k1gFnSc1	Šarlota
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
–	–	k?	–
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
1781	[number]	k4	1781
–	–	k?	–
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Karel	Karel	k1gMnSc1	Karel
Bourbonský	bourbonský	k2eAgMnSc1d1	bourbonský
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
–	–	k?	–
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
též	též	k9	též
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
</s>
</p>
<p>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
Helena	Helena	k1gFnSc1	Helena
Beatrice	Beatrice	k1gFnSc1	Beatrice
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
–	–	k?	–
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Dynastický	dynastický	k2eAgInSc1d1	dynastický
sňatek	sňatek	k1gInSc1	sňatek
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
zemřela	zemřít	k5eAaPmAgFnS	zemřít
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
Leszczynská	Leszczynská	k1gFnSc1	Leszczynská
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
vyvíjen	vyvíjet	k5eAaImNgMnS	vyvíjet
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jeho	jeho	k3xOp3gMnPc2	jeho
ministrů	ministr	k1gMnPc2	ministr
nátlak	nátlak	k1gInSc4	nátlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
nové	nový	k2eAgNnSc4d1	nové
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
svého	svůj	k1gMnSc4	svůj
šestnáctiletého	šestnáctiletý	k2eAgMnSc2d1	šestnáctiletý
vnuka	vnuk	k1gMnSc2	vnuk
a	a	k8xC	a
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tak	tak	k6eAd1	tak
19	[number]	k4	19
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1770	[number]	k4	1770
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
princeznou	princezna	k1gFnSc7	princezna
Marií	Maria	k1gFnSc7	Maria
Antoinettou	Antoinetta	k1gFnSc7	Antoinetta
<g/>
.	.	kIx.	.
</s>
<s>
Nemluvný	mluvný	k2eNgMnSc1d1	nemluvný
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
a	a	k8xC	a
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
Ludvík	Ludvík	k1gMnSc1	Ludvík
si	se	k3xPyFc3	se
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
hledal	hledat	k5eAaImAgMnS	hledat
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
veselé	veselá	k1gFnSc3	veselá
<g/>
,	,	kIx,	,
nepříliš	příliš	k6eNd1	příliš
vzdělané	vzdělaný	k2eAgFnSc3d1	vzdělaná
a	a	k8xC	a
povrchní	povrchní	k2eAgFnSc3d1	povrchní
manželce	manželka	k1gFnSc3	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Manželský	manželský	k2eAgInSc4d1	manželský
vztah	vztah	k1gInSc4	vztah
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
zkomplikován	zkomplikovat	k5eAaPmNgMnS	zkomplikovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedošel	dojít	k5eNaPmAgInS	dojít
naplnění	naplnění	k1gNnSc4	naplnění
nejen	nejen	k6eAd1	nejen
během	během	k7c2	během
svatební	svatební	k2eAgFnSc2d1	svatební
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
iniciativy	iniciativa	k1gFnSc2	iniciativa
bratr	bratr	k1gMnSc1	bratr
Marie	Maria	k1gFnSc2	Maria
Antoinetty	Antoinetta	k1gFnSc2	Antoinetta
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
švagrem	švagr	k1gMnSc7	švagr
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
promluvil	promluvit	k5eAaPmAgMnS	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
tedy	tedy	k8xC	tedy
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
konečně	konečně	k6eAd1	konečně
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
malou	malý	k2eAgFnSc4d1	malá
a	a	k8xC	a
nenáročnou	náročný	k2eNgFnSc4d1	nenáročná
operaci	operace	k1gFnSc4	operace
(	(	kIx(	(
<g/>
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
trpěl	trpět	k5eAaImAgMnS	trpět
fimózou	fimóza	k1gFnSc7	fimóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1777	[number]	k4	1777
bylo	být	k5eAaImAgNnS	být
manželství	manželství	k1gNnSc1	manželství
naplněno	naplnit	k5eAaPmNgNnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1778	[number]	k4	1778
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
královna	královna	k1gFnSc1	královna
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1778	[number]	k4	1778
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
první	první	k4xOgFnSc2	první
potomek	potomek	k1gMnSc1	potomek
královských	královský	k2eAgMnPc2d1	královský
manželů	manžel	k1gMnPc2	manžel
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
Charlotta	Charlotta	k1gFnSc1	Charlotta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
ještě	ještě	k9	ještě
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
reformátor	reformátor	k1gMnSc1	reformátor
==	==	k?	==
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
ujímá	ujímat	k5eAaImIp3nS	ujímat
trůnu	trůn	k1gInSc2	trůn
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
děda	děd	k1gMnSc2	děd
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
finančně	finančně	k6eAd1	finančně
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
v	v	k7c4	v
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
roční	roční	k2eAgInSc4d1	roční
deficit	deficit	k1gInSc4	deficit
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
milionových	milionový	k2eAgFnPc6d1	milionová
částkách	částka	k1gFnPc6	částka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
břímě	břímě	k1gNnSc1	břímě
daní	daň	k1gFnPc2	daň
nese	nést	k5eAaImIp3nS	nést
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
bedrech	bedra	k1gNnPc6	bedra
produktivní	produktivní	k2eAgNnSc1d1	produktivní
část	část	k1gFnSc4	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vlastníci	vlastník	k1gMnPc1	vlastník
majetků	majetek	k1gInPc2	majetek
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
dávek	dávka	k1gFnPc2	dávka
osvobozeni	osvobodit	k5eAaPmNgMnP	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Buržoazie	buržoazie	k1gFnSc1	buržoazie
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
šlechtou	šlechta	k1gFnSc7	šlechta
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
větším	veliký	k2eAgInSc6d2	veliký
podílu	podíl	k1gInSc6	podíl
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
všechny	všechen	k3xTgFnPc4	všechen
vrstvy	vrstva	k1gFnPc4	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mírumilovný	mírumilovný	k2eAgMnSc1d1	mírumilovný
<g/>
,	,	kIx,	,
dobromyslný	dobromyslný	k2eAgMnSc1d1	dobromyslný
a	a	k8xC	a
nerozhodný	rozhodný	k2eNgMnSc1d1	nerozhodný
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
před	před	k7c7	před
sebou	se	k3xPyFc7	se
těžký	těžký	k2eAgInSc4d1	těžký
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
řešení	řešení	k1gNnSc6	řešení
se	se	k3xPyFc4	se
ale	ale	k9	ale
vrhl	vrhnout	k5eAaImAgMnS	vrhnout
s	s	k7c7	s
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
horlivostí	horlivost	k1gFnSc7	horlivost
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
idealismem	idealismus	k1gInSc7	idealismus
<g/>
.	.	kIx.	.
</s>
<s>
Rádcem	rádce	k1gMnSc7	rádce
mladého	mladý	k2eAgMnSc2d1	mladý
krále	král	k1gMnSc2	král
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
již	již	k6eAd1	již
třiasedmdesátiletý	třiasedmdesátiletý	k2eAgMnSc1d1	třiasedmdesátiletý
hrabě	hrabě	k1gMnSc1	hrabě
Jean-Frederic	Jean-Frederic	k1gMnSc1	Jean-Frederic
Phelippeauxe	Phelippeauxe	k1gFnSc2	Phelippeauxe
de	de	k?	de
Maurepas	Maurepas	k1gMnSc1	Maurepas
a	a	k8xC	a
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
generálního	generální	k2eAgMnSc2d1	generální
kontrolora	kontrolor	k1gMnSc2	kontrolor
financí	finance	k1gFnPc2	finance
získává	získávat	k5eAaImIp3nS	získávat
Anne	Anne	k1gFnPc2	Anne
Robert	Robert	k1gMnSc1	Robert
Jacques	Jacques	k1gMnSc1	Jacques
Turgot	Turgot	k1gMnSc1	Turgot
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nasadil	nasadit	k5eAaPmAgMnS	nasadit
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
linii	linie	k1gFnSc4	linie
reforem	reforma	k1gFnPc2	reforma
státní	státní	k2eAgFnSc2d1	státní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
–	–	k?	–
žádné	žádný	k3yNgNnSc4	žádný
zvyšování	zvyšování	k1gNnSc4	zvyšování
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
půjčky	půjčka	k1gFnPc4	půjčka
<g/>
,	,	kIx,	,
nekompromisní	kompromisní	k2eNgNnPc4d1	nekompromisní
úsporná	úsporný	k2eAgNnPc4d1	úsporné
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Turgot	Turgot	k1gMnSc1	Turgot
se	se	k3xPyFc4	se
také	také	k9	také
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
rušení	rušení	k1gNnSc2	rušení
neužitečných	užitečný	k2eNgInPc2d1	neužitečný
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
omezil	omezit	k5eAaPmAgMnS	omezit
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
výdaje	výdaj	k1gInPc4	výdaj
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
plánoval	plánovat	k5eAaImAgMnS	plánovat
zdanění	zdanění	k1gNnSc4	zdanění
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc4	zrušení
roboty	robota	k1gFnSc2	robota
<g/>
,	,	kIx,	,
cechů	cech	k1gInPc2	cech
a	a	k8xC	a
cla	clo	k1gNnSc2	clo
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zároveň	zároveň	k6eAd1	zároveň
obnovil	obnovit	k5eAaPmAgInS	obnovit
pařížský	pařížský	k2eAgInSc1d1	pařížský
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
rozpuštěný	rozpuštěný	k2eAgInSc1d1	rozpuštěný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
ale	ale	k9	ale
opakovaně	opakovaně	k6eAd1	opakovaně
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zablokovat	zablokovat	k5eAaPmF	zablokovat
veškeré	veškerý	k3xTgFnPc4	veškerý
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
král	král	k1gMnSc1	král
parlament	parlament	k1gInSc1	parlament
opět	opět	k6eAd1	opět
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
ho	on	k3xPp3gMnSc4	on
opět	opět	k6eAd1	opět
svolal	svolat	k5eAaPmAgMnS	svolat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
u	u	k7c2	u
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
osvícenců	osvícenec	k1gMnPc2	osvícenec
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
narážely	narážet	k5eAaPmAgInP	narážet
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
u	u	k7c2	u
šlechty	šlechta	k1gFnSc2	šlechta
i	i	k8xC	i
členů	člen	k1gMnPc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
nakonec	nakonec	k6eAd1	nakonec
většinu	většina	k1gFnSc4	většina
reforem	reforma	k1gFnPc2	reforma
schválil	schválit	k5eAaPmAgMnS	schválit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Turgot	Turgot	k1gMnSc1	Turgot
už	už	k6eAd1	už
měl	mít	k5eAaImAgMnS	mít
mezi	mezi	k7c7	mezi
aristokracií	aristokracie	k1gFnSc7	aristokracie
a	a	k8xC	a
ve	v	k7c6	v
finančních	finanční	k2eAgInPc6d1	finanční
kruzích	kruh	k1gInPc6	kruh
tolik	tolik	k6eAd1	tolik
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
zbavil	zbavit	k5eAaPmAgInS	zbavit
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
dále	daleko	k6eAd2	daleko
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Anglické	anglický	k2eAgFnSc2d1	anglická
kolonie	kolonie	k1gFnSc2	kolonie
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
koloniemi	kolonie	k1gFnPc7	kolonie
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
obranný	obranný	k2eAgInSc4d1	obranný
a	a	k8xC	a
útočný	útočný	k2eAgInSc4d1	útočný
pakt	pakt	k1gInSc4	pakt
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
finančně	finančně	k6eAd1	finančně
náročného	náročný	k2eAgInSc2d1	náročný
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
boj	boj	k1gInSc4	boj
o	o	k7c4	o
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
situace	situace	k1gFnSc2	situace
země	zem	k1gFnSc2	zem
zatím	zatím	k6eAd1	zatím
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
převzal	převzít	k5eAaPmAgMnS	převzít
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c2	za
státní	státní	k2eAgFnSc2d1	státní
finance	finance	k1gFnSc2	finance
bankéř	bankéř	k1gMnSc1	bankéř
a	a	k8xC	a
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
obchodník	obchodník	k1gMnSc1	obchodník
Jacques	Jacques	k1gMnSc1	Jacques
Necker	Necker	k1gMnSc1	Necker
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
prosadil	prosadit	k5eAaPmAgMnS	prosadit
další	další	k2eAgFnPc4d1	další
půjčky	půjčka	k1gFnPc4	půjčka
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ještě	ještě	k6eAd1	ještě
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
jeho	jeho	k3xOp3gNnSc4	jeho
zadlužení	zadlužení	k1gNnSc4	zadlužení
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
úsporná	úsporný	k2eAgNnPc4d1	úsporné
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc4d1	nová
organizaci	organizace	k1gFnSc4	organizace
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
propagoval	propagovat	k5eAaImAgInS	propagovat
zlepšení	zlepšení	k1gNnSc4	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
mučení	mučení	k1gNnPc2	mučení
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
otroctví	otroctví	k1gNnSc1	otroctví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
praktikovalo	praktikovat	k5eAaImAgNnS	praktikovat
v	v	k7c6	v
zámořských	zámořský	k2eAgFnPc6d1	zámořská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
Necker	Necker	k1gMnSc1	Necker
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Neckerovi	Neckerův	k2eAgMnPc1d1	Neckerův
nástupci	nástupce	k1gMnPc1	nástupce
<g/>
,	,	kIx,	,
Joly	jola	k1gFnPc1	jola
de	de	k?	de
Fleury	Fleura	k1gFnSc2	Fleura
a	a	k8xC	a
Lefévre	Lefévr	k1gInSc5	Lefévr
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ormesson	Ormesson	k1gMnSc1	Ormesson
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc3	úřad
ujali	ujmout	k5eAaPmAgMnP	ujmout
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
o	o	k7c6	o
ozdravění	ozdravění	k1gNnSc6	ozdravění
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Charles	Charles	k1gMnSc1	Charles
Alexandre	Alexandr	k1gInSc5	Alexandr
de	de	k?	de
Colonne	Colonn	k1gMnSc5	Colonn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zavedení	zavedení	k1gNnSc4	zavedení
daně	daň	k1gFnSc2	daň
pro	pro	k7c4	pro
šlechtu	šlechta	k1gFnSc4	šlechta
a	a	k8xC	a
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
tedy	tedy	k8xC	tedy
svolal	svolat	k5eAaPmAgMnS	svolat
shromáždění	shromáždění	k1gNnPc4	shromáždění
notáblů	notábl	k1gMnPc2	notábl
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
144	[number]	k4	144
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgFnPc2d1	postavená
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
princů	princ	k1gMnPc2	princ
<g/>
,	,	kIx,	,
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
starostů	starosta	k1gMnPc2	starosta
a	a	k8xC	a
zástupců	zástupce	k1gMnPc2	zástupce
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
zavedení	zavedení	k1gNnSc4	zavedení
těchto	tento	k3xDgFnPc2	tento
nových	nový	k2eAgFnPc2d1	nová
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Notáblové	notábl	k1gMnPc1	notábl
reformy	reforma	k1gFnSc2	reforma
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
Colonne	Colonn	k1gInSc5	Colonn
musel	muset	k5eAaImAgMnS	muset
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
odstoupit	odstoupit	k5eAaPmF	odstoupit
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
toulouský	toulouský	k2eAgMnSc1d1	toulouský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Loménie	Loménie	k1gFnSc2	Loménie
de	de	k?	de
Brienne	Brienn	k1gInSc5	Brienn
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
shromáždění	shromáždění	k1gNnSc2	shromáždění
notáblů	notábl	k1gMnPc2	notábl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
králem	král	k1gMnSc7	král
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
a	a	k8xC	a
rezignovaný	rezignovaný	k2eAgMnSc1d1	rezignovaný
a	a	k8xC	a
zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
parlament	parlament	k1gInSc4	parlament
za	za	k7c4	za
rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vzpoury	vzpoura	k1gFnSc2	vzpoura
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
požadovali	požadovat	k5eAaImAgMnP	požadovat
svolání	svolání	k1gNnSc4	svolání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
svolání	svolání	k1gNnSc3	svolání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
u	u	k7c2	u
lidu	lid	k1gInSc2	lid
bouři	bouře	k1gFnSc4	bouře
nadšení	nadšení	k1gNnSc1	nadšení
a	a	k8xC	a
popularita	popularita	k1gFnSc1	popularita
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
správce	správce	k1gMnSc2	správce
financí	finance	k1gFnSc7	finance
znovu	znovu	k6eAd1	znovu
dosazen	dosazen	k2eAgMnSc1d1	dosazen
Jacques	Jacques	k1gMnSc1	Jacques
Necker	Necker	k1gMnSc1	Necker
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
horní	horní	k2eAgFnSc1d1	horní
vrstva	vrstva	k1gFnSc1	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
požadovala	požadovat	k5eAaImAgFnS	požadovat
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
moci	moc	k1gFnSc6	moc
<g/>
,	,	kIx,	,
což	což	k9	což
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
odmítal	odmítat	k5eAaImAgMnS	odmítat
vzít	vzít	k5eAaPmF	vzít
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
vládce	vládce	k1gMnSc4	vládce
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
si	se	k3xPyFc3	se
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
vlády	vláda	k1gFnSc2	vláda
už	už	k6eAd1	už
pominula	pominout	k5eAaPmAgFnS	pominout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1789	[number]	k4	1789
svolal	svolat	k5eAaPmAgMnS	svolat
Ludvík	Ludvík	k1gMnSc1	Ludvík
generální	generální	k2eAgInPc4d1	generální
stavy	stav	k1gInPc4	stav
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
reformy	reforma	k1gFnSc2	reforma
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
zástupců	zástupce	k1gMnPc2	zástupce
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
odstartovaly	odstartovat	k5eAaPmAgFnP	odstartovat
Velkou	velký	k2eAgFnSc4d1	velká
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Bastily	Bastila	k1gFnSc2	Bastila
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
formálně	formálně	k6eAd1	formálně
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
zároveň	zároveň	k6eAd1	zároveň
hledal	hledat	k5eAaImAgInS	hledat
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
k	k	k7c3	k
městečku	městečko	k1gNnSc3	městečko
Varennes	Varennesa	k1gFnPc2	Varennesa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
útěk	útěk	k1gInSc1	útěk
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
odhalen	odhalit	k5eAaPmNgInS	odhalit
poštmistrem	poštmistr	k1gMnSc7	poštmistr
Jeanem-Baptistem	Jeanem-Baptist	k1gMnSc7	Jeanem-Baptist
Drouetem	Drouet	k1gMnSc7	Drouet
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
odvezena	odvézt	k5eAaPmNgFnS	odvézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
vlastizrádce	vlastizrádce	k1gMnPc4	vlastizrádce
<g/>
,	,	kIx,	,
sesazen	sesadit	k5eAaPmNgMnS	sesadit
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
po	po	k7c6	po
procesu	proces	k1gInSc6	proces
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
veřejně	veřejně	k6eAd1	veřejně
sťat	stnout	k5eAaPmNgInS	stnout
gilotinou	gilotina	k1gFnSc7	gilotina
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
statečnost	statečnost	k1gFnSc1	statečnost
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
chvílích	chvíle	k1gFnPc6	chvíle
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
úctu	úcta	k1gFnSc4	úcta
mnoha	mnoho	k4c2	mnoho
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robespierrovy	Robespierrův	k2eAgInPc1d1	Robespierrův
argumenty	argument	k1gInPc1	argument
pro	pro	k7c4	pro
popravu	poprava	k1gFnSc4	poprava
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
projev	projev	k1gInSc1	projev
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
konventu	konvent	k1gInSc6	konvent
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nevede	vést	k5eNaImIp3nS	vést
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
není	být	k5eNaImIp3nS	být
obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
nejste	být	k5eNaImIp2nP	být
žádní	žádný	k3yNgMnPc1	žádný
soudcové	soudce	k1gMnPc1	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Jste	být	k5eAaImIp2nP	být
pouze	pouze	k6eAd1	pouze
politikové	politik	k1gMnPc1	politik
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
národa	národ	k1gInSc2	národ
...	...	k?	...
Vaším	váš	k3xOp2gInSc7	váš
úkolem	úkol	k1gInSc7	úkol
není	být	k5eNaImIp3nS	být
vyslovit	vyslovit	k5eAaPmF	vyslovit
nad	nad	k7c7	nad
někým	někdo	k3yInSc7	někdo
soudní	soudní	k2eAgInSc1d1	soudní
rozsudek	rozsudek	k1gInSc1	rozsudek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijmout	přijmout	k5eAaPmF	přijmout
opatření	opatření	k1gNnPc4	opatření
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
veřejného	veřejný	k2eAgNnSc2d1	veřejné
blaha	blaho	k1gNnSc2	blaho
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mne	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
příčí	příčit	k5eAaImIp3nS	příčit
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
k	k	k7c3	k
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
necítím	cítit	k5eNaImIp1nS	cítit
lásku	láska	k1gFnSc4	láska
ani	ani	k8xC	ani
nenávist	nenávist	k1gFnSc4	nenávist
...	...	k?	...
Ale	ale	k9	ale
sesazený	sesazený	k2eAgMnSc1d1	sesazený
král	král	k1gMnSc1	král
v	v	k7c6	v
lůně	lůno	k1gNnSc6	lůno
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
pevně	pevně	k6eAd1	pevně
zakotvena	zakotven	k2eAgFnSc1d1	zakotvena
...	...	k?	...
takový	takový	k3xDgInSc1	takový
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zneškodněn	zneškodnit	k5eAaPmNgMnS	zneškodnit
uvězněním	uvěznění	k1gNnSc7	uvěznění
nebo	nebo	k8xC	nebo
vypovězením	vypovězení	k1gNnSc7	vypovězení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bolestí	bolest	k1gFnSc7	bolest
vyslovuji	vyslovovat	k5eAaImIp1nS	vyslovovat
osudnou	osudný	k2eAgFnSc4d1	osudná
pravdu	pravda	k1gFnSc4	pravda
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
poctivých	poctivý	k2eAgMnPc2d1	poctivý
občanů	občan	k1gMnPc2	občan
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
konventu	konvent	k1gInSc6	konvent
byla	být	k5eAaImAgFnS	být
smrt	smrt	k1gFnSc4	smrt
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
odhlasována	odhlasován	k2eAgFnSc1d1	odhlasována
nejtěsnější	těsný	k2eAgFnSc7d3	nejtěsnější
možnou	možný	k2eAgFnSc7d1	možná
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
revolucionář	revolucionář	k1gMnSc1	revolucionář
Ludvík	Ludvík	k1gMnSc1	Ludvík
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
orleánský	orleánský	k2eAgMnSc1d1	orleánský
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíka	Ludvík	k1gMnSc4	Ludvík
tak	tak	k6eAd1	tak
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
i	i	k8xC	i
hlas	hlas	k1gInSc4	hlas
jeho	jeho	k3xOp3gMnSc2	jeho
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc4	osobnost
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
pohledy	pohled	k1gInPc1	pohled
na	na	k7c4	na
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
historikové	historik	k1gMnPc1	historik
jej	on	k3xPp3gMnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c2	za
čestného	čestný	k2eAgMnSc2d1	čestný
<g/>
,	,	kIx,	,
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
a	a	k8xC	a
zbožného	zbožný	k2eAgMnSc2d1	zbožný
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
dobrými	dobrý	k2eAgInPc7d1	dobrý
úmysly	úmysl	k1gInPc7	úmysl
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
nebylo	být	k5eNaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
nezbytném	nezbytný	k2eAgNnSc6d1	nezbytný
měřítku	měřítko	k1gNnSc6	měřítko
zreformovat	zreformovat	k5eAaPmF	zreformovat
monarchii	monarchie	k1gFnSc4	monarchie
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětním	obětní	k2eAgMnSc7d1	obětní
beránkem	beránek	k1gMnSc7	beránek
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
jako	jako	k8xC	jako
panovník	panovník	k1gMnSc1	panovník
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
často	často	k6eAd1	často
nerozhodně	rozhodně	k6eNd1	rozhodně
a	a	k8xC	a
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
řešit	řešit	k5eAaImF	řešit
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
reformovat	reformovat	k5eAaBmF	reformovat
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
feudální	feudální	k2eAgNnSc4d1	feudální
uspořádání	uspořádání	k1gNnSc4	uspořádání
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gFnSc3	jeho
vládě	vláda	k1gFnSc3	vláda
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgMnS	jevit
především	především	k9	především
jako	jako	k9	jako
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
-	-	kIx~	-
v	v	k7c6	v
době	doba	k1gFnSc6	doba
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
nouze	nouze	k1gFnSc2	nouze
žil	žít	k5eAaImAgMnS	žít
totiž	totiž	k9	totiž
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
chotí	choť	k1gFnSc7	choť
v	v	k7c6	v
přepychu	přepych	k1gInSc6	přepych
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dráždil	dráždit	k5eAaImAgInS	dráždit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Tituly	titul	k1gInPc1	titul
a	a	k8xC	a
oslovení	oslovení	k1gNnSc1	oslovení
==	==	k?	==
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1754	[number]	k4	1754
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1765	[number]	k4	1765
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
Královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1765	[number]	k4	1765
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1774	[number]	k4	1774
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc1	jeho
Královská	královský	k2eAgFnSc1d1	královská
Výsost	výsost	k1gFnSc1	výsost
dauphin	dauphin	k1gMnSc1	dauphin
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1774	[number]	k4	1774
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1792	[number]	k4	1792
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gNnSc1	jeho
Nejkřesťanštější	křesťanský	k2eAgNnSc1d3	křesťanský
Veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
král	král	k1gMnSc1	král
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1792	[number]	k4	1792
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1793	[number]	k4	1793
<g/>
:	:	kIx,	:
občan	občan	k1gMnSc1	občan
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kapet	Kapet	k1gMnSc1	Kapet
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
FURET	FURET	kA	FURET
<g/>
,	,	kIx,	,
Francois	Francois	k1gMnSc1	Francois
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Turgota	Turgot	k1gMnSc2	Turgot
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
1770	[number]	k4	1770
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
452	[number]	k4	452
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HARTMANN	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
králové	král	k1gMnPc1	král
a	a	k8xC	a
císaři	císař	k1gMnPc1	císař
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
:	:	kIx,	:
od	od	k7c2	od
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XII	XII	kA	XII
<g/>
.	.	kIx.	.
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1498	[number]	k4	1498
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
517	[number]	k4	517
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
Cronin	Cronin	k2eAgMnSc1d1	Cronin
<g/>
:	:	kIx,	:
Ludwig	Ludwig	k1gMnSc1	Ludwig
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
und	und	k?	und
Marie-Antoinette	Marie-Antoinett	k1gInSc5	Marie-Antoinett
-	-	kIx~	-
Eine	Einus	k1gMnSc5	Einus
Biographie	Biographius	k1gMnSc5	Biographius
<g/>
.	.	kIx.	.
</s>
<s>
Claassen	Claassen	k1gInSc1	Claassen
<g/>
,	,	kIx,	,
Düsseldorf	Düsseldorf	k1gInSc1	Düsseldorf
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Fay	Fay	k1gMnSc1	Fay
<g/>
:	:	kIx,	:
Ludwig	Ludwig	k1gMnSc1	Ludwig
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
-	-	kIx~	-
Der	drát	k5eAaImRp2nS	drát
Sturz	Sturz	k1gInSc1	Sturz
der	drát	k5eAaImRp2nS	drát
französischen	französischna	k1gFnPc2	französischna
Monarchie	monarchie	k1gFnPc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Heyne	Heyn	k1gInSc5	Heyn
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Evelyne	Evelynout	k5eAaPmIp3nS	Evelynout
Lever	Lever	k1gInSc1	Lever
<g/>
:	:	kIx,	:
Ludwig	Ludwig	k1gInSc1	Ludwig
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Klett-Cotta	Klett-Cotta	k1gFnSc1	Klett-Cotta
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Helga	Helga	k1gFnSc1	Helga
Thoma	Thoma	k1gFnSc1	Thoma
<g/>
:	:	kIx,	:
Z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
,	,	kIx,	,
Ikar	Ikar	k1gInSc1	Ikar
Praha	Praha	k1gFnSc1	Praha
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
