<s>
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Sarah	Sarah	k1gFnSc1
Magdalena	Magdalena	k1gFnSc1
Biasini	Biasin	k2eAgMnPc5d1
Narození	narození	k1gNnSc6
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Gassin	Gassin	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1
univerzitaLee	univerzitaLee	k1gFnSc1
Strasberg	Strasberg	k1gInSc1
Theatre	Theatr	k1gInSc5
and	and	k?
Film	film	k1gInSc1
Institute	institut	k1gInSc5
Povolání	povolání	k1gNnPc4
</s>
<s>
divadelní	divadelní	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
a	a	k8xC
herečka	herečka	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Biasini	Biasin	k2eAgMnPc1d1
a	a	k8xC
Romy	Rom	k1gMnPc4
Schneider	Schneider	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
pradědeček	pradědeček	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Retty	Retta	k1gFnSc2
</s>
<s>
prababička	prababička	k1gFnSc1
</s>
<s>
Rosa	Rosa	k1gFnSc1
Albach-Retty	Albach-Retta	k1gFnSc2
</s>
<s>
dědeček	dědeček	k1gMnSc1
</s>
<s>
Wolf	Wolf	k1gMnSc1
Albach-Retty	Albach-Retta	k1gFnSc2
</s>
<s>
babička	babička	k1gFnSc1
</s>
<s>
Magda	Magda	k1gFnSc1
Schneiderová	Schneiderová	k1gFnSc1
</s>
<s>
otec	otec	k1gMnSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Biasini	Biasin	k2eAgMnPc1d1
</s>
<s>
matka	matka	k1gFnSc1
</s>
<s>
Romy	Rom	k1gMnPc4
Schneider	Schneider	k1gMnSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Magdalena	Magdalena	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1977	#num#	k4
v	v	k7c4
Gassinu	Gassina	k1gFnSc4
<g/>
,	,	kIx,
Var	var	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
herečky	hereček	k1gMnPc4
Romy	Romy	k1gFnSc4	
Schneiderové	Schneiderová	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gMnSc2
druhého	druhý	k4xOgMnSc2
manžela	manžel	k1gMnSc2
a	a	k8xC
osobního	osobní	k2eAgMnSc2d1
sekretáře	sekretář	k1gMnSc2
Daniela	Daniel	k1gMnSc2
Biasiniho	Biasini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS
nejprve	nejprve	k6eAd1
historii	historie	k1gFnSc4
umění	umění	k1gNnSc2
na	na	k7c6
pařížské	pařížský	k2eAgFnSc6d1
Sorbonně	Sorbonna	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
i	i	k9
divadlo	divadlo	k1gNnSc1
a	a	k8xC
herectví	herectví	k1gNnSc1
na	na	k7c6
Strasbergově	Strasbergův	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
a	a	k8xC
divadelním	divadelní	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Los	los	k1gInSc1
Angeles	Angelesa	k1gFnPc2
a	a	k8xC
v	v	k7c6
Actors	Actorsa	k1gFnPc2
<g/>
'	'	kIx"
Studiu	studio	k1gNnSc3
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
úspěšně	úspěšně	k6eAd1
debutovala	debutovat	k5eAaBmAgFnS
ve	v	k7c6
francouzské	francouzský	k2eAgFnSc6d1
minisérii	minisérie	k1gFnSc6
Julie	Julie	k1gFnSc2
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
s	s	k7c7
tajemstvím	tajemství	k1gNnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
byla	být	k5eAaImAgFnS
nominována	nominován	k2eAgFnSc1d1
na	na	k7c4
cenu	cena	k1gFnSc4
Emmy	Emma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
2004	#num#	k4
-	-	kIx~
Julie	Julie	k1gFnSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
s	s	k7c7
tajemstvím	tajemství	k1gNnSc7
</s>
<s>
2005	#num#	k4
-	-	kIx~
Dům	dům	k1gInSc1
u	u	k7c2
kanálu	kanál	k1gInSc2
</s>
<s>
2008	#num#	k4
-	-	kIx~
Muž	muž	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
pes	pes	k1gMnSc1
</s>
<s>
2012	#num#	k4
-	-	kIx~
Associés	Associés	k1gInSc1
contre	contr	k1gInSc5
le	le	k?
crime	crim	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Julie	Julie	k1gFnSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
s	s	k7c7
tajemstvím	tajemství	k1gNnSc7
-	-	kIx~
recenze	recenze	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
web	web	k1gInSc1
fanoušků	fanoušek	k1gMnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
web	web	k1gInSc1
fanoušků	fanoušek	k1gMnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sarah	Sarah	k1gFnSc1
Biasiniová	Biasiniový	k2eAgFnSc1d1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Sarah	Sarah	k1gFnSc2
Biasini	Biasin	k2eAgMnPc1d1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
137300905	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7844	#num#	k4
071X	071X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2012073621	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
19931145	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2012073621	#num#	k4
</s>
