<s>
Mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nS	dědit
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
maternálně	maternálně	k6eAd1	maternálně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
ze	z	k7c2	z
spermie	spermie	k1gFnSc2	spermie
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
obvykle	obvykle	k6eAd1	obvykle
veškerá	veškerý	k3xTgFnSc1	veškerý
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
zárodku	zárodek	k1gInSc2	zárodek
<g/>
.	.	kIx.	.
</s>
