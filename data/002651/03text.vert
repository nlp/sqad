<s>
J.A.	J.A.	k?	J.A.
<g/>
R.	R.	kA	R.
(	(	kIx(	(
<g/>
Jednotka	jednotka	k1gFnSc1	jednotka
Akademického	akademický	k2eAgInSc2d1	akademický
Rapu	rapa	k1gFnSc4	rapa
nebo	nebo	k8xC	nebo
také	také	k9	také
Jaromír	Jaromír	k1gMnSc1	Jaromír
a	a	k8xC	a
Radomír	Radomír	k1gMnSc1	Radomír
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
česká	český	k2eAgFnSc1d1	Česká
funkově-rockově-rapová	funkověockověapový	k2eAgFnSc1d1	funkově-rockově-rapový
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
J.A.	J.A.	k1gFnSc2	J.A.
<g/>
R.	R.	kA	R.
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
symbolicky	symbolicky	k6eAd1	symbolicky
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
sestavu	sestava	k1gFnSc4	sestava
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
autor	autor	k1gMnSc1	autor
hudby	hudba	k1gFnSc2	hudba
Roman	Roman	k1gMnSc1	Roman
Holý	Holý	k1gMnSc1	Holý
a	a	k8xC	a
duo	duo	k1gNnSc1	duo
"	"	kIx"	"
<g/>
akademických	akademický	k2eAgMnPc2d1	akademický
<g/>
"	"	kIx"	"
rapperů	rapper	k1gMnPc2	rapper
Oto	Oto	k1gMnSc1	Oto
Klempíř	Klempíř	k1gMnSc1	Klempíř
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Viktořík	Viktořík	k1gMnSc1	Viktořík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
rockovějších	rockový	k2eAgNnPc6d2	rockovější
albech	album	k1gNnPc6	album
(	(	kIx(	(
<g/>
Frtka	Frtka	k1gFnSc1	Frtka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
a	a	k8xC	a
Mydli	mydlit	k5eAaImRp2nS	mydlit
to	ten	k3xDgNnSc1	ten
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
ukázala	ukázat	k5eAaPmAgFnS	ukázat
kapela	kapela	k1gFnSc1	kapela
svoji	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
potenciál	potenciál	k1gInSc4	potenciál
především	především	k6eAd1	především
na	na	k7c6	na
revolučním	revoluční	k2eAgNnSc6d1	revoluční
třetím	třetí	k4xOgNnSc6	třetí
albu	album	k1gNnSc6	album
Mein	Mein	k1gMnSc1	Mein
Kampfunk	Kampfunk	k1gMnSc1	Kampfunk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
zpívá	zpívat	k5eAaImIp3nS	zpívat
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
s	s	k7c7	s
říznou	řízný	k2eAgFnSc7d1	řízná
dechovou	dechový	k2eAgFnSc7d1	dechová
sekcí	sekce	k1gFnSc7	sekce
a	a	k8xC	a
výpravným	výpravný	k2eAgInSc7d1	výpravný
bookletem	booklet	k1gInSc7	booklet
od	od	k7c2	od
Lely	Lela	k1gFnSc2	Lela
Geislerové	Geislerová	k1gFnSc2	Geislerová
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgInPc1	první
velké	velký	k2eAgInPc1d1	velký
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
Maksimig	Maksimig	k1gInSc1	Maksimig
nebo	nebo	k8xC	nebo
Hnědojed	Hnědojed	k1gInSc1	Hnědojed
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
J.A.	J.A.	k1gMnSc3	J.A.
<g/>
R.	R.	kA	R.
svoje	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
řadové	řadový	k2eAgNnSc4d1	řadové
album	album	k1gNnSc4	album
Homo	Homo	k1gMnSc1	Homo
Fonkianz	Fonkianz	k1gMnSc1	Fonkianz
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
labelu	label	k1gInSc6	label
Sony	Sony	kA	Sony
Music	Music	k1gMnSc1	Music
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
již	již	k6eAd1	již
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
desetičlenném	desetičlenný	k2eAgNnSc6d1	desetičlenné
složení	složení	k1gNnSc6	složení
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
Viktořík	Viktořík	k1gMnSc1	Viktořík
<g/>
,	,	kIx,	,
Klempíř	Klempíř	k1gMnSc1	Klempíř
<g/>
,	,	kIx,	,
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Zbořil	Zbořil	k1gMnSc1	Zbořil
<g/>
,	,	kIx,	,
Balzar	Balzar	k1gMnSc1	Balzar
<g/>
,	,	kIx,	,
Chyška	chyška	k1gFnSc1	chyška
<g/>
,	,	kIx,	,
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Kaspar	Kaspar	k1gMnSc1	Kaspar
a	a	k8xC	a
Kop	kop	k1gInSc1	kop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydávají	vydávat	k5eAaImIp3nP	vydávat
také	také	k9	také
videokazetu	videokazeta	k1gFnSc4	videokazeta
s	s	k7c7	s
názvem	název	k1gInSc7	název
V	v	k7c6	v
deseti	deset	k4xCc7	deset
letí	letý	k2eAgMnPc1d1	letý
desetiletím	desetiletí	k1gNnSc7	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
řadové	řadový	k2eAgNnSc4d1	řadové
album	album	k1gNnSc4	album
si	se	k3xPyFc3	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
museli	muset	k5eAaImAgMnP	muset
počkat	počkat	k5eAaPmF	počkat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Nervák	nervák	k1gInSc1	nervák
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
rockových	rockový	k2eAgInPc2d1	rockový
prvků	prvek	k1gInPc2	prvek
více	hodně	k6eAd2	hodně
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
k	k	k7c3	k
popu	pop	k1gInSc3	pop
a	a	k8xC	a
disku	disk	k1gInSc3	disk
-	-	kIx~	-
samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
převažujícím	převažující	k2eAgInSc7d1	převažující
funkem	funk	k1gInSc7	funk
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
prezentovalo	prezentovat	k5eAaBmAgNnS	prezentovat
především	především	k6eAd1	především
singly	singl	k1gInPc1	singl
Metamegamastítko	Metamegamastítko	k1gNnSc1	Metamegamastítko
a	a	k8xC	a
Jsem	být	k5eAaImIp1nS	být
pohodlný	pohodlný	k2eAgMnSc1d1	pohodlný
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
byly	být	k5eAaImAgInP	být
natočeny	natočen	k2eAgInPc1d1	natočen
videoklipy	videoklip	k1gInPc1	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
Jsem	být	k5eAaImIp1nS	být
pohodlný	pohodlný	k2eAgMnSc1d1	pohodlný
režíroval	režírovat	k5eAaImAgMnS	režírovat
osvědčený	osvědčený	k2eAgMnSc1d1	osvědčený
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
klipů	klip	k1gInPc2	klip
pro	pro	k7c4	pro
Monkey	Monke	k2eAgInPc4d1	Monke
Business	business	k1gInSc4	business
nebo	nebo	k8xC	nebo
Dana	Dan	k1gMnSc4	Dan
Bártu	Bárta	k1gMnSc4	Bárta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
Armáda	armáda	k1gFnSc1	armáda
špásu	špás	k1gInSc2	špás
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	s	k7c7	s
stylem	styl	k1gInSc7	styl
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
k	k	k7c3	k
rockovějším	rockový	k2eAgInPc3d2	rockovější
opusům	opus	k1gInPc3	opus
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
singly	singl	k1gInPc4	singl
<g/>
:	:	kIx,	:
Superpéro	Superpéro	k1gNnSc1	Superpéro
a	a	k8xC	a
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
klipovou	klipový	k2eAgFnSc4d1	klipová
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
J.A.	J.A.	k?	J.A.
<g/>
R.	R.	kA	R.
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
nefunguje	fungovat	k5eNaImIp3nS	fungovat
jako	jako	k9	jako
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
jako	jako	k9	jako
příležitostné	příležitostný	k2eAgNnSc1d1	příležitostné
setkání	setkání	k1gNnSc1	setkání
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
jazzmanů	jazzman	k1gMnPc2	jazzman
a	a	k8xC	a
hudebních	hudební	k2eAgMnPc2d1	hudební
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
J.A.	J.A.	k1gFnPc2	J.A.
<g/>
R.	R.	kA	R.
figurují	figurovat	k5eAaImIp3nP	figurovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
vlastních	vlastní	k2eAgFnPc6d1	vlastní
kapelách	kapela	k1gFnPc6	kapela
a	a	k8xC	a
věnují	věnovat	k5eAaImIp3nP	věnovat
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInPc3	svůj
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
každoročně	každoročně	k6eAd1	každoročně
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
tradiční	tradiční	k2eAgInSc1d1	tradiční
koncert	koncert	k1gInSc1	koncert
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
sále	sál	k1gInSc6	sál
Lucerny	lucerna	k1gFnSc2	lucerna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tak	tak	k6eAd1	tak
slaví	slavit	k5eAaImIp3nP	slavit
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letošním	letošní	k2eAgInSc6d1	letošní
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
oslavila	oslavit	k5eAaPmAgFnS	oslavit
skupina	skupina	k1gFnSc1	skupina
již	již	k9	již
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydali	vydat	k5eAaPmAgMnP	vydat
J.A.R	J.A.R	k1gFnSc6	J.A.R
sedmé	sedmý	k4xOgNnSc4	sedmý
řadové	řadový	k2eAgNnSc4d1	řadové
album	album	k1gNnSc4	album
Dlouhohrající	dlouhohrající	k2eAgNnPc1d1	dlouhohrající
děcka	děcko	k1gNnPc1	děcko
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Holý	Holý	k1gMnSc1	Holý
-	-	kIx~	-
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Michael	Michael	k1gMnSc1	Michael
Viktořík	Viktořík	k1gMnSc1	Viktořík
-	-	kIx~	-
rap	rap	k1gMnSc1	rap
Oto	Oto	k1gMnSc1	Oto
Klempíř	Klempíř	k1gMnSc1	Klempíř
-	-	kIx~	-
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
tamburína	tamburína	k1gFnSc1	tamburína
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
Mirek	Mirek	k1gMnSc1	Mirek
Chyška	chyška	k1gFnSc1	chyška
-	-	kIx~	-
kytary	kytara	k1gFnSc2	kytara
Pavel	Pavel	k1gMnSc1	Pavel
Bady	Bada	k1gFnSc2	Bada
Zbořil	Zbořil	k1gMnSc1	Zbořil
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Robert	Robert	k1gMnSc1	Robert
Balzar	Balzar	k1gMnSc1	Balzar
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Filip	Filip	k1gMnSc1	Filip
Jelínek	Jelínek	k1gMnSc1	Jelínek
-	-	kIx~	-
trombón	trombón	k1gInSc1	trombón
<g/>
,	,	kIx,	,
dechová	dechový	k2eAgFnSc1d1	dechová
sekce	sekce	k1gFnSc1	sekce
Splash	Splash	k1gInSc4	Splash
Hornz	Hornz	k1gInSc1	Hornz
Radka	Radka	k1gFnSc1	Radka
Kaspar	Kaspar	k1gMnSc1	Kaspar
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
dechová	dechový	k2eAgFnSc1d1	dechová
sekce	sekce	k1gFnSc1	sekce
Splash	Splash	k1gMnSc1	Splash
Hornz	Hornz	k1gMnSc1	Hornz
František	František	k1gMnSc1	František
Kop	kop	k1gInSc1	kop
-	-	kIx~	-
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
dechová	dechový	k2eAgFnSc1d1	dechová
sekce	sekce	k1gFnSc1	sekce
Splash	Splash	k1gMnSc1	Splash
Hornz	Hornz	k1gMnSc1	Hornz
Vít	Vít	k1gMnSc1	Vít
Kučaj	Kučaj	k1gMnSc1	Kučaj
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
,	,	kIx,	,
1990	[number]	k4	1990
Marek	Marek	k1gMnSc1	Marek
Minárik	Minárik	k1gMnSc1	Minárik
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Jiří	Jiří	k1gMnSc1	Jiří
Šíma	Šíma	k1gMnSc1	Šíma
(	(	kIx(	(
<g/>
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
)	)	kIx)	)
-	-	kIx~	-
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
Karel	Karel	k1gMnSc1	Karel
Růžička	Růžička	k1gMnSc1	Růžička
-	-	kIx~	-
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
1992	[number]	k4	1992
Frtka	Frtka	k1gFnSc1	Frtka
(	(	kIx(	(
<g/>
Monitor	monitor	k1gInSc1	monitor
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
Mydli-to	Mydlio	k1gNnSc1	Mydli-to
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
BMG	BMG	kA	BMG
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
Mein	Mein	k1gMnSc1	Mein
Kampfunk	Kampfunk	k1gMnSc1	Kampfunk
(	(	kIx(	(
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
Homo	Homo	k1gMnSc1	Homo
Fonkianz	Fonkianz	k1gMnSc1	Fonkianz
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
V	v	k7c6	v
deseti	deset	k4xCc2	deset
letí	letět	k5eAaImIp3nS	letět
desetiletím	desetiletí	k1gNnSc7	desetiletí
-	-	kIx~	-
VHS	VHS	kA	VHS
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Ťo	Ťo	k1gFnSc2	Ťo
ti	ten	k3xDgMnPc1	ten
ťo	ťo	k?	ťo
jemixes	jemixes	k1gMnSc1	jemixes
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
Frtka	Frtko	k1gNnSc2	Frtko
<g/>
/	/	kIx~	/
<g/>
Mydli-to	Mydlio	k1gNnSc1	Mydli-to
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
-	-	kIx~	-
reedice	reedice	k1gFnSc1	reedice
<g/>
,	,	kIx,	,
2	[number]	k4	2
CD	CD	kA	CD
2002	[number]	k4	2002
Nervák	nervák	k1gInSc1	nervák
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
Music	Musice	k1gInPc2	Musice
<g/>
/	/	kIx~	/
<g/>
Bonton	bonton	k1gInSc1	bonton
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Armáda	armáda	k1gFnSc1	armáda
špásu	špás	k1gInSc2	špás
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
DVD	DVD	kA	DVD
&	&	k?	&
CD	CD	kA	CD
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
Dlouhohrající	dlouhohrající	k2eAgNnPc1d1	dlouhohrající
děcka	děcko	k1gNnPc1	děcko
(	(	kIx(	(
<g/>
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
</s>
