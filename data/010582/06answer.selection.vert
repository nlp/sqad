<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
jej	on	k3xPp3gNnSc4	on
norský	norský	k2eAgMnSc1d1	norský
režisér	režisér	k1gMnSc1	režisér
Morten	Morten	k2eAgInSc4d1	Morten
Tyldum	Tyldum	k1gInSc4	Tyldum
a	a	k8xC	a
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
matematikovi	matematik	k1gMnSc6	matematik
a	a	k8xC	a
kryptoanalytikovi	kryptoanalytik	k1gMnSc6	kryptoanalytik
Alanu	Alan	k1gMnSc6	Alan
Turingovi	Turing	k1gMnSc6	Turing
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
hraje	hrát	k5eAaImIp3nS	hrát
Benedict	Benedict	k2eAgInSc1d1	Benedict
Cumberbatch	Cumberbatch	k1gInSc1	Cumberbatch
<g/>
.	.	kIx.	.
</s>
