<p>
<s>
Kód	kód	k1gInSc1	kód
Enigmy	enigma	k1gFnSc2	enigma
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gFnSc1	The
Imitation	Imitation	k1gInSc1	Imitation
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
thriller	thriller	k1gInSc1	thriller
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgMnS	natočit
jej	on	k3xPp3gNnSc4	on
norský	norský	k2eAgMnSc1d1	norský
režisér	režisér	k1gMnSc1	režisér
Morten	Morten	k2eAgInSc4d1	Morten
Tyldum	Tyldum	k1gInSc4	Tyldum
a	a	k8xC	a
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
matematikovi	matematik	k1gMnSc6	matematik
a	a	k8xC	a
kryptoanalytikovi	kryptoanalytik	k1gMnSc6	kryptoanalytik
Alanu	Alan	k1gMnSc6	Alan
Turingovi	Turing	k1gMnSc6	Turing
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
hraje	hrát	k5eAaImIp3nS	hrát
Benedict	Benedict	k2eAgInSc1d1	Benedict
Cumberbatch	Cumberbatch	k1gInSc1	Cumberbatch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
představili	představit	k5eAaPmAgMnP	představit
například	například	k6eAd1	například
Keira	Keira	k1gMnSc1	Keira
Knightley	Knightlea	k1gFnSc2	Knightlea
(	(	kIx(	(
<g/>
Joan	Joan	k1gInSc1	Joan
Clarke	Clark	k1gInSc2	Clark
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matthew	Matthew	k1gMnSc5	Matthew
Goode	Good	k1gMnSc5	Good
(	(	kIx(	(
<g/>
Hugh	Hugha	k1gFnPc2	Hugha
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
)	)	kIx)	)
či	či	k8xC	či
Mark	Mark	k1gMnSc1	Mark
Strong	Strong	k1gMnSc1	Strong
(	(	kIx(	(
<g/>
Stewart	Stewart	k1gMnSc1	Stewart
Menzies	Menzies	k1gMnSc1	Menzies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Telluride	tellurid	k1gInSc5	tellurid
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
prolínají	prolínat	k5eAaImIp3nP	prolínat
dvě	dva	k4xCgFnPc1	dva
časové	časový	k2eAgFnPc1d1	časová
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1	vyprávění
začíná	začínat	k5eAaImIp3nS	začínat
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc4	Turing
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
policistovi	policistův	k2eAgMnPc1d1	policistův
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
z	z	k7c2	z
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
přesahy	přesah	k1gInPc7	přesah
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
sousedem	soused	k1gMnSc7	soused
nahlášeno	nahlásit	k5eAaPmNgNnS	nahlásit
vloupání	vloupání	k1gNnSc4	vloupání
do	do	k7c2	do
Turingova	Turingův	k2eAgInSc2d1	Turingův
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
Turing	Turing	k1gInSc1	Turing
sám	sám	k3xTgInSc1	sám
bagatelizuje	bagatelizovat	k5eAaImIp3nS	bagatelizovat
a	a	k8xC	a
policii	policie	k1gFnSc3	policie
ironicky	ironicky	k6eAd1	ironicky
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
detektiv	detektiv	k1gMnSc1	detektiv
Nock	Nock	k1gMnSc1	Nock
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Turing	Turing	k1gInSc1	Turing
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
cambridských	cambridský	k2eAgMnPc2d1	cambridský
špionů	špion	k1gMnPc2	špion
<g/>
.	.	kIx.	.
</s>
<s>
Pátráním	pátrání	k1gNnSc7	pátrání
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turing	Turing	k1gInSc1	Turing
není	být	k5eNaImIp3nS	být
špion	špion	k1gMnSc1	špion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
homosexuál	homosexuál	k1gMnSc1	homosexuál
a	a	k8xC	a
jako	jako	k8xC	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
předveden	předvést	k5eAaPmNgInS	předvést
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
policistovi	policista	k1gMnSc3	policista
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
matematické	matematický	k2eAgFnPc4d1	matematická
schopnosti	schopnost	k1gFnPc4	schopnost
najat	najat	k2eAgInSc1d1	najat
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k1gNnPc7	další
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
prolomení	prolomení	k1gNnSc6	prolomení
kódu	kód	k1gInSc2	kód
šifrovacího	šifrovací	k2eAgInSc2d1	šifrovací
stroje	stroj	k1gInSc2	stroj
Enigma	enigma	k1gFnSc1	enigma
používaným	používaný	k2eAgNnSc7d1	používané
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
úsilí	úsilí	k1gNnSc6	úsilí
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
stroji	stroj	k1gInSc3	stroj
kód	kód	k1gInSc1	kód
objevit	objevit	k5eAaPmF	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
stihne	stihnout	k5eAaPmIp3nS	stihnout
zasnoubit	zasnoubit	k5eAaPmF	zasnoubit
s	s	k7c7	s
Joan	Joano	k1gNnPc2	Joano
Clarkovou	Clarková	k1gFnSc4	Clarková
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
okolnostem	okolnost	k1gFnPc3	okolnost
zasnoubení	zasnoubení	k1gNnSc2	zasnoubení
opět	opět	k6eAd1	opět
zruší	zrušit	k5eAaPmIp3nS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
s	s	k7c7	s
pokynem	pokyn	k1gInSc7	pokyn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
o	o	k7c6	o
tajném	tajný	k2eAgInSc6d1	tajný
projektu	projekt	k1gInSc6	projekt
nemluvil	mluvit	k5eNaImAgMnS	mluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
a	a	k8xC	a
namísto	namísto	k7c2	namísto
dvouletého	dvouletý	k2eAgInSc2d1	dvouletý
nepodmíněného	podmíněný	k2eNgInSc2d1	nepodmíněný
trestu	trest	k1gInSc2	trest
zvolí	zvolit	k5eAaPmIp3nS	zvolit
hormonální	hormonální	k2eAgFnSc4d1	hormonální
terapii	terapie	k1gFnSc4	terapie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
chemickou	chemický	k2eAgFnSc4d1	chemická
kastraci	kastrace	k1gFnSc4	kastrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
ho	on	k3xPp3gMnSc4	on
Joana	Joan	k1gMnSc4	Joan
Clarková	Clarková	k1gFnSc1	Clarková
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
provdaná	provdaný	k2eAgFnSc1d1	provdaná
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
však	však	k9	však
nevidí	vidět	k5eNaImIp3nS	vidět
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
východisko	východisko	k1gNnSc1	východisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Alexandre	Alexandr	k1gInSc5	Alexandr
Desplat	Desple	k1gNnPc2	Desple
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
skladby	skladba	k1gFnSc2	skladba
nahrál	nahrát	k5eAaPmAgInS	nahrát
Londýnský	londýnský	k2eAgInSc1d1	londýnský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
studiu	studio	k1gNnSc6	studio
Abbey	Abbea	k1gFnSc2	Abbea
Road	Road	k1gMnSc1	Road
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Desplat	Desple	k1gNnPc2	Desple
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
ale	ale	k9	ale
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
jiný	jiný	k2eAgInSc4d1	jiný
film	film	k1gInSc4	film
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Grandhotel	grandhotel	k1gInSc1	grandhotel
Budapešť	Budapešť	k1gFnSc4	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
zazněla	zaznít	k5eAaPmAgFnS	zaznít
také	také	k9	také
skladba	skladba	k1gFnSc1	skladba
Coffee	Coffe	k1gInSc2	Coffe
Meditation	Meditation	k1gInSc1	Meditation
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Milana	Milan	k1gMnSc2	Milan
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
byla	být	k5eAaImAgFnS	být
nominována	nominován	k2eAgFnSc1d1	nominována
také	také	k9	také
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudební	hudební	k2eAgFnSc4d1	hudební
složku	složka	k1gFnSc4	složka
k	k	k7c3	k
vizuálnímu	vizuální	k2eAgNnSc3d1	vizuální
dílu	dílo	k1gNnSc3	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
Grandhotelem	grandhotel	k1gInSc7	grandhotel
Budapešť	Budapešť	k1gFnSc4	Budapešť
a	a	k8xC	a
Birdmanem	Birdman	k1gInSc7	Birdman
<g/>
,	,	kIx,	,
oběma	dva	k4xCgMnPc7	dva
s	s	k7c7	s
9	[number]	k4	9
nominacemi	nominace	k1gFnPc7	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Nominován	nominován	k2eAgMnSc1d1	nominován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
kategorii	kategorie	k1gFnSc6	kategorie
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Benedict	Benedict	k2eAgInSc4d1	Benedict
Cumberbatch	Cumberbatch	k1gInSc4	Cumberbatch
na	na	k7c4	na
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
Keira	Keir	k1gMnSc2	Keir
Knightley	Knightlea	k1gMnSc2	Knightlea
na	na	k7c4	na
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
herečku	herečka	k1gFnSc4	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
Morten	Morten	k2eAgInSc1d1	Morten
Tyldum	Tyldum	k1gInSc1	Tyldum
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Goldenberg	Goldenberg	k1gInSc4	Goldenberg
za	za	k7c4	za
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
Alexandre	Alexandr	k1gInSc5	Alexandr
Desplat	Desple	k1gNnPc2	Desple
za	za	k7c4	za
původní	původní	k2eAgFnSc4d1	původní
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Djurkovičová	Djurkovičová	k1gFnSc1	Djurkovičová
a	a	k8xC	a
Tatiana	Tatiana	k1gFnSc1	Tatiana
Macdonaldová	Macdonaldová	k1gFnSc1	Macdonaldová
za	za	k7c4	za
výpravu	výprava	k1gFnSc4	výprava
a	a	k8xC	a
Graham	graham	k1gInSc4	graham
Moore	Moor	k1gInSc5	Moor
za	za	k7c4	za
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kód	kód	k1gInSc1	kód
Enigmy	enigma	k1gFnSc2	enigma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
filmu	film	k1gInSc2	film
</s>
</p>
