<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jedinou	jediný	k2eAgFnSc4d1
planetu	planeta	k1gFnSc4
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
ženě	žena	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
římské	římský	k2eAgFnSc6d1
bohyni	bohyně	k1gFnSc6
lásky	láska	k1gFnSc2
a	a	k8xC
krásy	krása	k1gFnSc2
Venuši	Venuše	k1gFnSc6
<g/>
.	.	kIx.
</s>