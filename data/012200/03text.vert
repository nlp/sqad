<p>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ἐ	Ἐ	k?	Ἐ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Epicurus	Epicurus	k1gInSc1	Epicurus
<g/>
,	,	kIx,	,
také	také	k9	také
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
nebo	nebo	k8xC	nebo
Epikur	Epikur	k1gMnSc1	Epikur
<g/>
;	;	kIx,	;
341	[number]	k4	341
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Samu	Samos	k1gInSc2	Samos
–	–	k?	–
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Athény	Athéna	k1gFnSc2	Athéna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
starověký	starověký	k2eAgMnSc1d1	starověký
řecký	řecký	k2eAgMnSc1d1	řecký
hédonistický	hédonistický	k2eAgMnSc1d1	hédonistický
filosof	filosof	k1gMnSc1	filosof
období	období	k1gNnSc2	období
helénismu	helénismus	k1gInSc2	helénismus
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
představitel	představitel	k1gMnSc1	představitel
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
směru	směr	k1gInSc2	směr
–	–	k?	–
epikúreismu	epikúreismus	k1gInSc2	epikúreismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nauce	nauka	k1gFnSc6	nauka
vycházel	vycházet	k5eAaImAgMnS	vycházet
především	především	k9	především
z	z	k7c2	z
Démokritova	Démokritův	k2eAgInSc2d1	Démokritův
atomismu	atomismus	k1gInSc2	atomismus
<g/>
,	,	kIx,	,
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
však	však	k9	však
jeho	jeho	k3xOp3gFnSc4	jeho
představu	představa	k1gFnSc4	představa
absolutního	absolutní	k2eAgInSc2d1	absolutní
determinismu	determinismus	k1gInSc2	determinismus
připuštěním	připuštění	k1gNnSc7	připuštění
existence	existence	k1gFnSc2	existence
samovolné	samovolný	k2eAgFnSc2d1	samovolná
odchylky	odchylka	k1gFnSc2	odchylka
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
jinak	jinak	k6eAd1	jinak
zcela	zcela	k6eAd1	zcela
mechanicky	mechanicky	k6eAd1	mechanicky
se	se	k3xPyFc4	se
pohybujících	pohybující	k2eAgInPc2d1	pohybující
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
usiloval	usilovat	k5eAaImAgMnS	usilovat
"	"	kIx"	"
<g/>
usnadnit	usnadnit	k5eAaPmF	usnadnit
výklad	výklad	k1gInSc4	výklad
bohatství	bohatství	k1gNnSc2	bohatství
proměn	proměna	k1gFnPc2	proměna
světa	svět	k1gInSc2	svět
a	a	k8xC	a
především	především	k9	především
lidské	lidský	k2eAgFnSc2d1	lidská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zásadně	zásadně	k6eAd1	zásadně
odmítal	odmítat	k5eAaImAgMnS	odmítat
koncepci	koncepce	k1gFnSc4	koncepce
osudové	osudový	k2eAgFnSc2d1	osudová
nutnosti	nutnost	k1gFnSc2	nutnost
a	a	k8xC	a
teleologické	teleologický	k2eAgNnSc1d1	teleologické
pojetí	pojetí	k1gNnSc1	pojetí
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
cíl	cíl	k1gInSc4	cíl
své	svůj	k3xOyFgFnSc2	svůj
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
soudobou	soudobý	k2eAgFnSc7d1	soudobá
tradicí	tradice	k1gFnSc7	tradice
přikládal	přikládat	k5eAaImAgMnS	přikládat
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
etice	etika	k1gFnSc3	etika
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgInS	považovat
dosažení	dosažení	k1gNnSc4	dosažení
blaženého	blažený	k2eAgInSc2d1	blažený
života	život	k1gInSc2	život
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
duševního	duševní	k2eAgInSc2d1	duševní
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vyplývajícího	vyplývající	k2eAgNnSc2d1	vyplývající
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
Epikúra	Epikúr	k1gInSc2	Epikúr
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jen	jen	k9	jen
skrze	skrze	k?	skrze
slast	slast	k1gFnSc1	slast
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
rozumí	rozumět	k5eAaImIp3nS	rozumět
uspokojení	uspokojení	k1gNnSc4	uspokojení
přirozených	přirozený	k2eAgFnPc2d1	přirozená
potřeb	potřeba	k1gFnPc2	potřeba
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
bolesti	bolest	k1gFnSc2	bolest
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
neklidu	neklid	k1gInSc2	neklid
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
protiklad	protiklad	k1gInSc1	protiklad
slasti	slast	k1gFnSc2	slast
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
hlavní	hlavní	k2eAgFnSc1d1	hlavní
překážka	překážka	k1gFnSc1	překážka
–	–	k?	–
strast	strast	k1gFnSc1	strast
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
slast	slast	k1gFnSc4	slast
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
podstatou	podstata	k1gFnSc7	podstata
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
především	především	k9	především
negativně	negativně	k6eAd1	negativně
jako	jako	k8xC	jako
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
strasti	strast	k1gFnSc2	strast
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
blaženosti	blaženost	k1gFnSc6	blaženost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
trvalé	trvalý	k2eAgFnSc3d1	trvalá
slasti	slast	k1gFnSc3	slast
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výhradně	výhradně	k6eAd1	výhradně
vyhýbáním	vyhýbání	k1gNnSc7	vyhýbání
se	se	k3xPyFc4	se
strasti	strast	k1gFnPc4	strast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úsilí	úsilí	k1gNnSc6	úsilí
o	o	k7c4	o
blaženost	blaženost	k1gFnSc4	blaženost
klíčové	klíčový	k2eAgNnSc1d1	klíčové
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
skutečných	skutečný	k2eAgFnPc2d1	skutečná
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
uspokojení	uspokojení	k1gNnSc4	uspokojení
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
strasti	strast	k1gFnSc2	strast
<g/>
,	,	kIx,	,
od	od	k7c2	od
jen	jen	k6eAd1	jen
domněle	domněle	k6eAd1	domněle
opodstatněných	opodstatněný	k2eAgMnPc2d1	opodstatněný
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
naplnění	naplnění	k1gNnSc1	naplnění
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
není	být	k5eNaImIp3nS	být
či	či	k8xC	či
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
samo	sám	k3xTgNnSc1	sám
zdrojem	zdroj	k1gInSc7	zdroj
strasti	strast	k1gFnPc1	strast
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zdržet	zdržet	k5eAaPmF	zdržet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
liché	lichý	k2eAgFnPc1d1	lichá
potřeby	potřeba	k1gFnPc1	potřeba
podle	podle	k7c2	podle
Epikúra	Epikúr	k1gInSc2	Epikúr
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
několika	několik	k4yIc2	několik
druhů	druh	k1gInPc2	druh
strachu	strach	k1gInSc2	strach
–	–	k?	–
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
nemožnosti	nemožnost	k1gFnSc2	nemožnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
spoutáván	spoutávat	k5eAaImNgMnS	spoutávat
a	a	k8xC	a
vzdalován	vzdalovat	k5eAaImNgMnS	vzdalovat
od	od	k7c2	od
blaženosti	blaženost	k1gFnSc2	blaženost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
přirozené	přirozený	k2eAgFnPc4d1	přirozená
potřeby	potřeba	k1gFnPc4	potřeba
od	od	k7c2	od
potřeb	potřeba	k1gFnPc2	potřeba
lichých	lichý	k2eAgFnPc2d1	lichá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
slasti	slast	k1gFnSc2	slast
nejprve	nejprve	k6eAd1	nejprve
nutné	nutný	k2eAgNnSc1d1	nutné
osvobození	osvobození	k1gNnSc1	osvobození
od	od	k7c2	od
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
cestou	cestou	k7c2	cestou
rozumnosti	rozumnost	k1gFnSc2	rozumnost
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
lze	lze	k6eAd1	lze
pochopit	pochopit	k5eAaPmF	pochopit
bezdůvodnost	bezdůvodnost	k1gFnSc4	bezdůvodnost
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
správně	správně	k6eAd1	správně
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
vyhovění	vyhovění	k1gNnSc1	vyhovění
kterým	který	k3yRgFnPc3	který
potřebám	potřeba	k1gFnPc3	potřeba
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
usilování	usilování	k1gNnSc2	usilování
o	o	k7c6	o
které	který	k3yRgFnSc6	který
slasti	slast	k1gFnSc6	slast
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
duševnímu	duševní	k2eAgInSc3d1	duševní
klidu	klid	k1gInSc3	klid
a	a	k8xC	a
kterým	který	k3yQgNnSc7	který
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
utrpení	utrpení	k1gNnSc1	utrpení
lidí	člověk	k1gMnPc2	člověk
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k9	především
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
lichých	lichý	k2eAgInPc2d1	lichý
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
duše	duše	k1gFnSc2	duše
<g/>
"	"	kIx"	"
a	a	k8xC	a
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
své	svůj	k3xOyFgFnSc3	svůj
nauce	nauka	k1gFnSc3	nauka
dává	dávat	k5eAaImIp3nS	dávat
až	až	k9	až
"	"	kIx"	"
<g/>
terapeutický	terapeutický	k2eAgInSc4d1	terapeutický
<g/>
"	"	kIx"	"
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejím	její	k3xOp3gInSc7	její
nejvlastnějším	vlastní	k2eAgInSc7d3	nejvlastnější
cílem	cíl	k1gInSc7	cíl
nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
především	především	k9	především
praktický	praktický	k2eAgInSc4d1	praktický
návod	návod	k1gInSc4	návod
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
trvalého	trvalý	k2eAgInSc2d1	trvalý
stavu	stav	k1gInSc2	stav
slasti	slast	k1gFnSc2	slast
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nalézá	nalézat	k5eAaImIp3nS	nalézat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
uvědomělém	uvědomělý	k2eAgNnSc6d1	uvědomělé
a	a	k8xC	a
důsledném	důsledný	k2eAgNnSc6d1	důsledné
vyhýbání	vyhýbání	k1gNnSc6	vyhýbání
se	se	k3xPyFc4	se
strastem	strast	k1gFnPc3	strast
a	a	k8xC	a
v	v	k7c6	v
uvolněném	uvolněný	k2eAgNnSc6d1	uvolněné
<g/>
,	,	kIx,	,
nenásilném	násilný	k2eNgNnSc6d1	nenásilné
<g/>
,	,	kIx,	,
víceméně	víceméně	k9	víceméně
samovolně	samovolně	k6eAd1	samovolně
se	se	k3xPyFc4	se
dostavujícím	dostavující	k2eAgNnSc6d1	dostavující
dosahování	dosahování	k1gNnSc6	dosahování
slasti	slast	k1gFnSc2	slast
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
askeze	askeze	k1gFnSc2	askeze
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
maximálním	maximální	k2eAgNnSc6d1	maximální
omezování	omezování	k1gNnSc6	omezování
potřeb	potřeba	k1gFnPc2	potřeba
lichých	lichý	k2eAgInPc2d1	lichý
a	a	k8xC	a
v	v	k7c6	v
přiměřeném	přiměřený	k2eAgNnSc6d1	přiměřené
uspokojování	uspokojování	k1gNnSc6	uspokojování
potřeb	potřeba	k1gFnPc2	potřeba
přirozených	přirozený	k2eAgFnPc2d1	přirozená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
školy	škola	k1gFnSc2	škola
byl	být	k5eAaImAgInS	být
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
obdivován	obdivovat	k5eAaImNgMnS	obdivovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
od	od	k7c2	od
strachu	strach	k1gInSc2	strach
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
spasitel	spasitel	k1gMnSc1	spasitel
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nositel	nositel	k1gMnSc1	nositel
světla	světlo	k1gNnSc2	světlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nauka	nauka	k1gFnSc1	nauka
často	často	k6eAd1	často
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
protispolečenská	protispolečenský	k2eAgNnPc1d1	protispolečenské
a	a	k8xC	a
zejména	zejména	k9	zejména
–	–	k?	–
protože	protože	k8xS	protože
odmítal	odmítat	k5eAaImAgMnS	odmítat
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
zájmu	zájem	k1gInSc6	zájem
bohů	bůh	k1gMnPc2	bůh
o	o	k7c4	o
běh	běh	k1gInSc4	běh
světa	svět	k1gInSc2	svět
či	či	k8xC	či
o	o	k7c4	o
životy	život	k1gInPc4	život
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
bezbožná	bezbožný	k2eAgFnSc1d1	bezbožná
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
také	také	k9	také
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kritiky	kritik	k1gMnPc7	kritik
i	i	k8xC	i
vulgarizujícími	vulgarizující	k2eAgMnPc7d1	vulgarizující
následovníky	následovník	k1gMnPc7	následovník
<g/>
,	,	kIx,	,
desinterpretována	desinterpretovat	k5eAaImNgFnS	desinterpretovat
jako	jako	k8xC	jako
přitakávající	přitakávající	k2eAgFnPc1d1	přitakávající
nevázané	vázaný	k2eNgFnPc1d1	nevázaná
poživačnosti	poživačnost	k1gFnPc1	poživačnost
či	či	k8xC	či
prostopášnosti	prostopášnost	k1gFnPc1	prostopášnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
odmítal	odmítat	k5eAaImAgInS	odmítat
poukazem	poukaz	k1gInSc7	poukaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozumnost	rozumnost	k1gFnSc1	rozumnost
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
usilování	usilování	k1gNnSc3	usilování
o	o	k7c6	o
střídmé	střídmý	k2eAgFnSc6d1	střídmá
a	a	k8xC	a
neokázalé	okázalý	k2eNgFnSc6d1	neokázalá
slasti	slast	k1gFnSc6	slast
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jen	jen	k9	jen
jimi	on	k3xPp3gMnPc7	on
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
strasti	strast	k1gFnSc2	strast
zcela	zcela	k6eAd1	zcela
spolehlivě	spolehlivě	k6eAd1	spolehlivě
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
badatelé	badatel	k1gMnPc1	badatel
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
sobectví	sobectví	k1gNnSc2	sobectví
a	a	k8xC	a
až	až	k9	až
asociálně	asociálně	k6eAd1	asociálně
vypjatý	vypjatý	k2eAgInSc4d1	vypjatý
individualismus	individualismus	k1gInSc4	individualismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
však	však	k9	však
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
pouze	pouze	k6eAd1	pouze
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
soupis	soupis	k1gInSc1	soupis
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
etických	etický	k2eAgFnPc2d1	etická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
snad	snad	k9	snad
sám	sám	k3xTgMnSc1	sám
sestavil	sestavit	k5eAaPmAgMnS	sestavit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
dopisy	dopis	k1gInPc1	dopis
určené	určený	k2eAgInPc1d1	určený
žákům	žák	k1gMnPc3	žák
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nauka	nauka	k1gFnSc1	nauka
známa	znám	k2eAgFnSc1d1	známa
jen	jen	k9	jen
ze	z	k7c2	z
sekundárních	sekundární	k2eAgInPc2d1	sekundární
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
na	na	k7c6	na
egejském	egejský	k2eAgInSc6d1	egejský
ostrově	ostrov	k1gInSc6	ostrov
Samu	Samos	k1gInSc2	Samos
v	v	k7c6	v
roce	rok	k1gInSc6	rok
341	[number]	k4	341
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Den	dna	k1gFnPc2	dna
jeho	jeho	k3xOp3gFnPc2	jeho
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
epikúrejské	epikúrejský	k2eAgFnSc6d1	epikúrejský
škole	škola	k1gFnSc6	škola
později	pozdě	k6eAd2	pozdě
oslavován	oslavovat	k5eAaImNgMnS	oslavovat
<g/>
,	,	kIx,	,
připadal	připadat	k5eAaImAgMnS	připadat
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
gaméliónu	gamélión	k1gInSc2	gamélión
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
narodil	narodit	k5eAaPmAgInS	narodit
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Neoklés	Neoklésa	k1gFnPc2	Neoklésa
byl	být	k5eAaImAgMnS	být
athénský	athénský	k2eAgMnSc1d1	athénský
občan	občan	k1gMnSc1	občan
a	a	k8xC	a
na	na	k7c4	na
Samos	Samos	k1gInSc4	Samos
přišel	přijít	k5eAaPmAgMnS	přijít
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
roce	rok	k1gInSc6	rok
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jako	jako	k8xC	jako
klérúchos	klérúchos	k1gInSc1	klérúchos
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Chairestrata	Chairestrata	k1gFnSc1	Chairestrata
<g/>
.	.	kIx.	.
</s>
<s>
Známi	znám	k2eAgMnPc1d1	znám
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
Neoklés	Neoklésa	k1gFnPc2	Neoklésa
<g/>
,	,	kIx,	,
Chairedémos	Chairedémosa	k1gFnPc2	Chairedémosa
a	a	k8xC	a
Aristobúlos	Aristobúlosa	k1gFnPc2	Aristobúlosa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
Epikúrův	Epikúrův	k2eAgInSc4d1	Epikúrův
podnět	podnět	k1gInSc4	podnět
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
také	také	k6eAd1	také
měli	mít	k5eAaImAgMnP	mít
věnovat	věnovat	k5eAaPmF	věnovat
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
<g/>
Základy	základ	k1gInPc1	základ
vzdělání	vzdělání	k1gNnSc2	vzdělání
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
nepochybně	pochybně	k6eNd1	pochybně
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
venkovský	venkovský	k2eAgMnSc1d1	venkovský
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
filosofií	filosofie	k1gFnSc7	filosofie
se	se	k3xPyFc4	se
snad	snad	k9	snad
seznámil	seznámit	k5eAaPmAgMnS	seznámit
u	u	k7c2	u
samského	samský	k1gMnSc2	samský
platónika	platónik	k1gMnSc2	platónik
Pamfila	Pamfil	k1gMnSc2	Pamfil
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
poslán	poslán	k2eAgInSc1d1	poslán
do	do	k7c2	do
iónského	iónský	k2eAgNnSc2d1	Iónské
města	město	k1gNnSc2	město
Teós	Teósa	k1gFnPc2	Teósa
studovat	studovat	k5eAaImF	studovat
k	k	k7c3	k
Démokritovu	Démokritův	k2eAgMnSc3d1	Démokritův
žáku	žák	k1gMnSc3	žák
Nausifanovi	Nausifan	k1gMnSc3	Nausifan
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
zde	zde	k6eAd1	zde
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jako	jako	k8xS	jako
18	[number]	k4	18
<g/>
letý	letý	k2eAgMnSc1d1	letý
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
2	[number]	k4	2
roky	rok	k1gInPc1	rok
trvající	trvající	k2eAgInPc1d1	trvající
službu	služba	k1gFnSc4	služba
eféba	eféb	k1gMnSc2	eféb
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zcela	zcela	k6eAd1	zcela
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
průběhu	průběh	k1gInSc6	průběh
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c4	na
Samos	Samos	k1gInSc4	Samos
už	už	k6eAd1	už
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
322	[number]	k4	322
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
totiž	totiž	k9	totiž
Perdikkás	Perdikkás	k1gInSc1	Perdikkás
tamním	tamní	k2eAgMnPc3d1	tamní
klérúchům	klérúch	k1gMnPc3	klérúch
nařídil	nařídit	k5eAaPmAgMnS	nařídit
opustit	opustit	k5eAaPmF	opustit
přidělenou	přidělený	k2eAgFnSc4d1	přidělená
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
ji	on	k3xPp3gFnSc4	on
původním	původní	k2eAgMnPc3d1	původní
vlastníkům	vlastník	k1gMnPc3	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Neoklés	Neoklés	k6eAd1	Neoklés
proto	proto	k8xC	proto
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Kolofónu	Kolofón	k1gInSc2	Kolofón
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
jako	jako	k9	jako
20	[number]	k4	20
<g/>
letý	letý	k2eAgInSc1d1	letý
přišel	přijít	k5eAaPmAgInS	přijít
i	i	k9	i
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
<g/>
.	.	kIx.	.
</s>
<s>
Nuceným	nucený	k2eAgNnSc7d1	nucené
opuštěním	opuštění	k1gNnSc7	opuštění
pozemků	pozemek	k1gInPc2	pozemek
na	na	k7c6	na
Samu	Samos	k1gInSc6	Samos
ovšem	ovšem	k9	ovšem
rodina	rodina	k1gFnSc1	rodina
ztratila	ztratit	k5eAaPmAgFnS	ztratit
hlavní	hlavní	k2eAgInSc4d1	hlavní
zdroj	zdroj	k1gInSc4	zdroj
příjmu	příjem	k1gInSc2	příjem
a	a	k8xC	a
upadla	upadnout	k5eAaPmAgFnS	upadnout
tím	ten	k3xDgNnSc7	ten
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
nouze	nouze	k1gFnSc2	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Epikúra	Epikúr	k1gMnSc4	Epikúr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
možná	možná	k9	možná
sám	sám	k3xTgMnSc1	sám
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
začal	začít	k5eAaPmAgMnS	začít
vyučovat	vyučovat	k5eAaImF	vyučovat
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
nemohl	moct	k5eNaImAgMnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
obvykle	obvykle	k6eAd1	obvykle
nákladném	nákladný	k2eAgNnSc6d1	nákladné
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
renomovaných	renomovaný	k2eAgFnPc2d1	renomovaná
filosofických	filosofický	k2eAgFnPc2d1	filosofická
škol	škola	k1gFnPc2	škola
<g/>
;	;	kIx,	;
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
samostudiu	samostudium	k1gNnSc3	samostudium
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
vypořádávát	vypořádávát	k5eAaPmF	vypořádávát
se	s	k7c7	s
skromnými	skromný	k2eAgInPc7d1	skromný
poměry	poměr	k1gInPc7	poměr
znemožňujícími	znemožňující	k2eAgInPc7d1	znemožňující
mu	on	k3xPp3gMnSc3	on
systematické	systematický	k2eAgNnSc1d1	systematické
studium	studium	k1gNnSc1	studium
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
učitelů	učitel	k1gMnPc2	učitel
(	(	kIx(	(
<g/>
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
pak	pak	k6eAd1	pak
později	pozdě	k6eAd2	pozdě
vždy	vždy	k6eAd1	vždy
hovořil	hovořit	k5eAaImAgMnS	hovořit
s	s	k7c7	s
patrnou	patrný	k2eAgFnSc7d1	patrná
jízlivostí	jízlivost	k1gFnSc7	jízlivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zůstal	zůstat	k5eAaPmAgMnS	zůstat
autodidaktem	autodidakt	k1gMnSc7	autodidakt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
celoživotními	celoživotní	k2eAgFnPc7d1	celoživotní
útrapami	útrapa	k1gFnPc7	útrapa
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
vynucujícími	vynucující	k2eAgFnPc7d1	vynucující
si	se	k3xPyFc3	se
přísný	přísný	k2eAgInSc1d1	přísný
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
podnětů	podnět	k1gInPc2	podnět
vedoucích	vedoucí	k1gFnPc2	vedoucí
ke	k	k7c3	k
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
povaze	povaha	k1gFnSc3	povaha
Epikúra	Epikúr	k1gInSc2	Epikúr
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc2	jeho
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
jistě	jistě	k9	jistě
byl	být	k5eAaImAgInS	být
svými	svůj	k3xOyFgMnPc7	svůj
oponenty	oponent	k1gMnPc7	oponent
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
i	i	k9	i
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
vysmíván	vysmíván	k2eAgMnSc1d1	vysmíván
a	a	k8xC	a
pomlouván	pomlouván	k2eAgMnSc1d1	pomlouván
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
Epikúrově	Epikúrův	k2eAgInSc6d1	Epikúrův
životě	život	k1gInSc6	život
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
32	[number]	k4	32
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
zejména	zejména	k9	zejména
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
samostudiu	samostudium	k1gNnSc6	samostudium
filosofie	filosofie	k1gFnSc2	filosofie
nejsou	být	k5eNaImIp3nP	být
bezpečně	bezpečně	k6eAd1	bezpečně
známé	známý	k2eAgFnPc1d1	známá
žádné	žádný	k3yNgFnPc1	žádný
podrobnosti	podrobnost	k1gFnPc1	podrobnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mohl	moct	k5eAaImAgMnS	moct
studovat	studovat	k5eAaImF	studovat
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgInPc2d1	jiný
zejména	zejména	k9	zejména
nauku	nauka	k1gFnSc4	nauka
Aristippovu	Aristippův	k2eAgFnSc4d1	Aristippův
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
jej	on	k3xPp3gInSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
řada	řada	k1gFnSc1	řada
podobností	podobnost	k1gFnPc2	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc4d1	vlastní
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
školu	škola	k1gFnSc4	škola
založil	založit	k5eAaPmAgMnS	založit
snad	snad	k9	snad
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
310	[number]	k4	310
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Mytiléně	Mytiléna	k1gFnSc6	Mytiléna
na	na	k7c4	na
Lesbu	lesba	k1gFnSc4	lesba
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Lampsaku	Lampsak	k1gInSc2	Lampsak
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgInS	získat
řadu	řada	k1gFnSc4	řada
oddaných	oddaný	k2eAgMnPc2d1	oddaný
žáků	žák	k1gMnPc2	žák
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hermarcha	Hermarcha	k1gMnSc1	Hermarcha
nebo	nebo	k8xC	nebo
Metrodóra	Metrodóra	k1gMnSc1	Metrodóra
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepochybně	pochybně	k6eNd1	pochybně
také	také	k9	také
už	už	k6eAd1	už
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
svou	svůj	k3xOyFgFnSc4	svůj
nauku	nauka	k1gFnSc4	nauka
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
uceleného	ucelený	k2eAgInSc2d1	ucelený
filosofického	filosofický	k2eAgInSc2d1	filosofický
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
následné	následný	k2eAgNnSc4d1	následné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
přenést	přenést	k5eAaPmF	přenést
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
jistotu	jistota	k1gFnSc4	jistota
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
způsobilosti	způsobilost	k1gFnSc6	způsobilost
obstát	obstát	k5eAaPmF	obstát
v	v	k7c6	v
náročné	náročný	k2eAgFnSc6d1	náročná
<g />
.	.	kIx.	.
</s>
<s>
konkurenci	konkurence	k1gFnSc4	konkurence
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
středisku	středisko	k1gNnSc6	středisko
soudobé	soudobý	k2eAgFnSc2d1	soudobá
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
nové	nový	k2eAgMnPc4d1	nový
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
306	[number]	k4	306
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
mezi	mezi	k7c7	mezi
Athénami	Athéna	k1gFnPc7	Athéna
a	a	k8xC	a
Peiraem	Peira	k1gInSc7	Peira
koupil	koupit	k5eAaPmAgMnS	koupit
dům	dům	k1gInSc4	dům
se	s	k7c7	s
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
velmi	velmi	k6eAd1	velmi
početnými	početný	k2eAgMnPc7d1	početný
žáky	žák	k1gMnPc7	žák
společně	společně	k6eAd1	společně
žil	žít	k5eAaImAgMnS	žít
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
také	také	k9	také
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
názvu	název	k1gInSc3	název
Epikúrovy	Epikúrův	k2eAgFnSc2d1	Epikúrova
školy	škola	k1gFnSc2	škola
–	–	k?	–
"	"	kIx"	"
<g/>
Zahrada	zahrada	k1gFnSc1	zahrada
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Képos	Képos	k1gInSc1	Képos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Athény	Athéna	k1gFnSc2	Athéna
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
cest	cesta	k1gFnPc2	cesta
za	za	k7c7	za
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
do	do	k7c2	do
Iónie	Iónie	k1gFnSc2	Iónie
neopustil	opustit	k5eNaPmAgInS	opustit
<g/>
;	;	kIx,	;
text	text	k1gInSc1	text
jeho	jeho	k3xOp3gFnSc2	jeho
závěti	závěť	k1gFnSc2	závěť
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
usiloval	usilovat	k5eAaImAgMnS	usilovat
především	především	k9	především
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
finančně	finančně	k6eAd1	finančně
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
intenzivním	intenzivní	k2eAgInSc6d1	intenzivní
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
ke	k	k7c3	k
společenství	společenství	k1gNnSc3	společenství
svých	svůj	k3xOyFgMnPc2	svůj
následovníků	následovník	k1gMnPc2	následovník
jakož	jakož	k8xC	jakož
i	i	k9	i
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
členům	člen	k1gMnPc3	člen
měl	mít	k5eAaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
naukou	nauka	k1gFnSc7	nauka
se	se	k3xPyFc4	se
také	také	k9	také
spíše	spíše	k9	spíše
stranil	stranit	k5eAaImAgMnS	stranit
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
nebránilo	bránit	k5eNaImAgNnS	bránit
v	v	k7c6	v
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
nejvýznamnějších	významný	k2eAgFnPc6d3	nejvýznamnější
athénských	athénský	k2eAgFnPc6d1	Athénská
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
vedl	vést	k5eAaImAgMnS	vést
velmi	velmi	k6eAd1	velmi
skromný	skromný	k2eAgMnSc1d1	skromný
až	až	k8xS	až
asketický	asketický	k2eAgInSc1d1	asketický
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
nemocemi	nemoc	k1gFnPc7	nemoc
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
urputnými	urputný	k2eAgFnPc7d1	urputná
bolestmi	bolest	k1gFnPc7	bolest
<g/>
;	;	kIx,	;
nejspíše	nejspíše	k9	nejspíše
trpěl	trpět	k5eAaImAgMnS	trpět
ledvinovými	ledvinový	k2eAgInPc7d1	ledvinový
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l	l	kA	l
</s>
</p>
<p>
<s>
===	===	k?	===
Zahrada	zahrada	k1gFnSc1	zahrada
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
komuny	komuna	k1gFnSc2	komuna
přijímal	přijímat	k5eAaImAgMnS	přijímat
i	i	k9	i
manžele	manžel	k1gMnPc4	manžel
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
otroky	otrok	k1gMnPc4	otrok
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
patrně	patrně	k6eAd1	patrně
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
výstřednostech	výstřednost	k1gFnPc6	výstřednost
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
smyšlené	smyšlený	k2eAgFnPc1d1	smyšlená
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
Zahrady	zahrada	k1gFnSc2	zahrada
prý	prý	k9	prý
stálo	stát	k5eAaImAgNnS	stát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cizinče	cizinec	k1gMnSc5	cizinec
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
ti	ten	k3xDgMnPc1	ten
bude	být	k5eAaImBp3nS	být
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
dobrem	dobro	k1gNnSc7	dobro
rozkoš	rozkoš	k1gFnSc1	rozkoš
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
Zahrada	zahrada	k1gFnSc1	zahrada
sice	sice	k8xC	sice
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
stoik	stoik	k1gMnSc1	stoik
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Díogena	Díogeno	k1gNnSc2	Díogeno
byl	být	k5eAaImAgInS	být
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
plodný	plodný	k2eAgMnSc1d1	plodný
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
převyšoval	převyšovat	k5eAaImAgMnS	převyšovat
všechny	všechen	k3xTgInPc4	všechen
množstvím	množství	k1gNnSc7	množství
knih	kniha	k1gFnPc2	kniha
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
totiž	totiž	k9	totiž
k	k	k7c3	k
300	[number]	k4	300
svitků	svitek	k1gInPc2	svitek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
názvů	název	k1gInPc2	název
"	"	kIx"	"
<g/>
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tematicky	tematicky	k6eAd1	tematicky
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
oblast	oblast	k1gFnSc4	oblast
přírodovědy	přírodověda	k1gFnSc2	přírodověda
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc2d1	přírodní
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
např.	např.	kA	např.
O	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
sestávající	sestávající	k2eAgInSc4d1	sestávající
ze	z	k7c2	z
37	[number]	k4	37
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
O	o	k7c6	o
atomech	atom	k1gInPc6	atom
a	a	k8xC	a
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
noetiky	noetika	k1gFnSc2	noetika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
O	o	k7c6	o
kritériu	kritérion	k1gNnSc6	kritérion
neboli	neboli	k8xC	neboli
Kánon	kánon	k1gInSc1	kánon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etiky	etika	k1gFnSc2	etika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
O	o	k7c6	o
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc6d1	ostatní
ctnostech	ctnost	k1gFnPc6	ctnost
<g/>
,	,	kIx,	,
O	o	k7c6	o
konečném	konečný	k2eAgInSc6d1	konečný
cíli	cíl	k1gInSc6	cíl
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
spisy	spis	k1gInPc1	spis
polemické	polemický	k2eAgInPc1d1	polemický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Proti	proti	k7c3	proti
přírodním	přírodní	k2eAgMnPc3d1	přírodní
filosofům	filosof	k1gMnPc3	filosof
<g/>
,	,	kIx,	,
Proti	proti	k7c3	proti
megarikům	megarik	k1gMnPc3	megarik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
texty	text	k1gInPc1	text
tří	tři	k4xCgFnPc2	tři
Epikurových	Epikurův	k2eAgFnPc2d1	Epikurova
dopisů	dopis	k1gInPc2	dopis
jeho	on	k3xPp3gInSc4	on
žákům	žák	k1gMnPc3	žák
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
Díogenés	Díogenés	k1gInSc4	Díogenés
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
Epikúrovy	Epikúrův	k2eAgFnSc2d1	Epikúrova
Závěti	závěť	k1gFnSc2	závěť
<g/>
)	)	kIx)	)
cituje	citovat	k5eAaBmIp3nS	citovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
referenci	reference	k1gFnSc6	reference
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
sám	sám	k3xTgMnSc1	sám
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
"	"	kIx"	"
<g/>
podal	podat	k5eAaPmAgMnS	podat
výtah	výtah	k1gInSc4	výtah
z	z	k7c2	z
celé	celá	k1gFnSc2	celá
své	svůj	k3xOyFgFnSc2	svůj
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
List	list	k1gInSc4	list
Hérodotovi	Hérodotův	k2eAgMnPc1d1	Hérodotův
–	–	k?	–
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
hlavní	hlavní	k2eAgInPc4d1	hlavní
principy	princip	k1gInPc4	princip
atomismu	atomismus	k1gInSc2	atomismus
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
List	list	k1gInSc4	list
Pýthokleovi	Pýthokleův	k2eAgMnPc1d1	Pýthokleův
–	–	k?	–
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
astronomických	astronomický	k2eAgInPc6d1	astronomický
a	a	k8xC	a
meteorologických	meteorologický	k2eAgInPc6d1	meteorologický
úkazech	úkaz	k1gInPc6	úkaz
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
List	list	k1gInSc4	list
Menoikeovi	Menoikeův	k2eAgMnPc1d1	Menoikeův
–	–	k?	–
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
otázkách	otázka	k1gFnPc6	otázka
etických	etický	k2eAgMnPc2d1	etický
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
dochovány	dochován	k2eAgFnPc1d1	dochována
dvě	dva	k4xCgFnPc1	dva
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
články	článek	k1gInPc1	článek
–	–	k?	–
soubor	soubor	k1gInSc4	soubor
40	[number]	k4	40
hlavních	hlavní	k2eAgInPc2d1	hlavní
článků	článek	k1gInPc2	článek
učení	učení	k1gNnSc2	učení
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
Díogenem	Díogen	k1gInSc7	Díogen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
snad	snad	k9	snad
sestavil	sestavit	k5eAaPmAgMnS	sestavit
sám	sám	k3xTgMnSc1	sám
Epikuros	Epikuros	k1gMnSc1	Epikuros
jako	jako	k9	jako
pomůcku	pomůcka	k1gFnSc4	pomůcka
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
zapamatování	zapamatování	k1gNnSc2	zapamatování
si	se	k3xPyFc3	se
základů	základ	k1gInPc2	základ
epikurejské	epikurejský	k2eAgFnSc2d1	epikurejská
nauky	nauka	k1gFnSc2	nauka
či	či	k8xC	či
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
memorování	memorování	k1gNnSc4	memorování
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Vaticanae	Vaticanae	k6eAd1	Vaticanae
sententiae	sententiaat	k5eAaPmIp3nS	sententiaat
–	–	k?	–
<g/>
Zdrojem	zdroj	k1gInSc7	zdroj
dalších	další	k2eAgFnPc2d1	další
znalostí	znalost	k1gFnPc2	znalost
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
jeho	jeho	k3xOp3gMnPc1	jeho
pozdější	pozdní	k2eAgMnPc1d2	pozdější
přívrženci	přívrženec	k1gMnPc1	přívrženec
(	(	kIx(	(
<g/>
Lucretius	Lucretius	k1gMnSc1	Lucretius
Carus	Carus	k1gMnSc1	Carus
<g/>
,	,	kIx,	,
Horatius	Horatius	k1gMnSc1	Horatius
Flaccus	Flaccus	k1gMnSc1	Flaccus
<g/>
,	,	kIx,	,
Plinius	Plinius	k1gMnSc1	Plinius
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
)	)	kIx)	)
a	a	k8xC	a
znalci	znalec	k1gMnPc1	znalec
(	(	kIx(	(
<g/>
Diogenés	Diogenés	k1gInSc1	Diogenés
Laertios	Laertios	k1gInSc1	Laertios
<g/>
,	,	kIx,	,
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
,	,	kIx,	,
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epikúrova	Epikúrův	k2eAgFnSc1d1	Epikúrova
etická	etický	k2eAgFnSc1d1	etická
filosofie	filosofie	k1gFnSc1	filosofie
==	==	k?	==
</s>
</p>
<p>
<s>
Epikúrova	Epikúrův	k2eAgFnSc1d1	Epikúrova
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
protikladem	protiklad	k1gInSc7	protiklad
filosofie	filosofie	k1gFnSc2	filosofie
platónské	platónský	k2eAgFnSc2d1	platónská
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
na	na	k7c4	na
smyslově	smyslově	k6eAd1	smyslově
vnímaný	vnímaný	k2eAgInSc4d1	vnímaný
svět	svět	k1gInSc4	svět
běžné	běžný	k2eAgFnSc2d1	běžná
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
idejích	idea	k1gFnPc6	idea
i	i	k8xC	i
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
hédonismus	hédonismus	k1gInSc1	hédonismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
slast	slast	k1gFnSc4	slast
jediným	jediný	k2eAgNnSc7d1	jediné
dobrem	dobro	k1gNnSc7	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
výrazy	výraz	k1gInPc1	výraz
"	"	kIx"	"
<g/>
epikurejec	epikurejec	k1gMnSc1	epikurejec
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
epikurejský	epikurejský	k2eAgMnSc1d1	epikurejský
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglické	anglický	k2eAgFnSc2d1	anglická
epicure	epicur	k1gMnSc5	epicur
<g/>
)	)	kIx)	)
znamenají	znamenat	k5eAaImIp3nP	znamenat
rozkošník	rozkošník	k1gMnSc1	rozkošník
<g/>
,	,	kIx,	,
labužník	labužník	k1gMnSc1	labužník
<g/>
,	,	kIx,	,
požitkář	požitkář	k1gMnSc1	požitkář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
libuje	libovat	k5eAaImIp3nS	libovat
ve	v	k7c6	v
vybraných	vybraný	k2eAgNnPc6d1	vybrané
jídlech	jídlo	k1gNnPc6	jídlo
a	a	k8xC	a
víně	vína	k1gFnSc6	vína
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
epikurejské	epikurejský	k2eAgInPc4d1	epikurejský
hody	hod	k1gInPc4	hod
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgNnPc2d1	jiné
starých	starý	k2eAgNnPc2d1	staré
a	a	k8xC	a
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
použití	použití	k1gNnSc1	použití
slova	slovo	k1gNnSc2	slovo
epikurejec	epikurejec	k1gMnSc1	epikurejec
dosti	dosti	k6eAd1	dosti
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
samotného	samotný	k2eAgInSc2d1	samotný
Epikúra	Epikúr	k1gInSc2	Epikúr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ten	ten	k3xDgMnSc1	ten
prý	prý	k9	prý
totiž	totiž	k9	totiž
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
trpěl	trpět	k5eAaImAgMnS	trpět
žaludečními	žaludeční	k2eAgFnPc7d1	žaludeční
potížemi	potíž	k1gFnPc7	potíž
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
labužníkem	labužník	k1gMnSc7	labužník
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
pil	pít	k5eAaImAgMnS	pít
jen	jen	k9	jen
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
jedl	jíst	k5eAaImAgMnS	jíst
velmi	velmi	k6eAd1	velmi
střídmě	střídmě	k6eAd1	střídmě
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
žil	žít	k5eAaImAgInS	žít
odříkavým	odříkavý	k2eAgInSc7d1	odříkavý
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svém	své	k1gNnSc6	své
dopisu	dopis	k1gInSc2	dopis
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
naplněn	naplnit	k5eAaPmNgInS	naplnit
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
slastmi	slast	k1gFnPc7	slast
<g/>
,	,	kIx,	,
žiju	žít	k5eAaImIp1nS	žít
<g/>
-li	i	k?	-li
o	o	k7c6	o
chlebu	chléb	k1gInSc6	chléb
a	a	k8xC	a
o	o	k7c6	o
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
přehlížím	přehlížet	k5eAaImIp1nS	přehlížet
přepych	přepych	k1gInSc4	přepych
a	a	k8xC	a
rozkoše	rozkoš	k1gFnPc4	rozkoš
<g/>
,	,	kIx,	,
ne	ne	k9	ne
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
samé	samý	k3xTgNnSc4	samý
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pro	pro	k7c4	pro
nepříjemnosti	nepříjemnost	k1gFnPc4	nepříjemnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
je	on	k3xPp3gNnSc4	on
provázejí	provázet	k5eAaImIp3nP	provázet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
filosofie	filosofie	k1gFnSc2	filosofie
Platónovy	Platónův	k2eAgFnSc2d1	Platónova
<g/>
,	,	kIx,	,
Aristotelovy	Aristotelův	k2eAgFnSc2d1	Aristotelova
nebo	nebo	k8xC	nebo
stoické	stoický	k2eAgNnSc1d1	stoické
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
filosofie	filosofie	k1gFnSc1	filosofie
"	"	kIx"	"
<g/>
egoistická	egoistický	k2eAgFnSc1d1	egoistická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c6	na
hledání	hledání	k1gNnSc6	hledání
osobního	osobní	k2eAgNnSc2d1	osobní
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
záležitostech	záležitost	k1gFnPc6	záležitost
veřejných	veřejný	k2eAgFnPc2d1	veřejná
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
soudil	soudit	k5eAaImAgMnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
snažit	snažit	k5eAaImF	snažit
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nezdaří	zdařit	k5eNaPmIp3nS	zdařit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
stáhnout	stáhnout	k5eAaPmF	stáhnout
a	a	k8xC	a
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prožij	prožít	k5eAaPmRp2nS	prožít
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
v	v	k7c6	v
skrytu	skryt	k1gInSc6	skryt
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Etická	etický	k2eAgFnSc1d1	etická
filozofie	filozofie	k1gFnSc1	filozofie
Epikúrova	Epikúrov	k1gInSc2	Epikúrov
spočívá	spočívat	k5eAaImIp3nS	spočívat
zejména	zejména	k9	zejména
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
<g/>
,	,	kIx,	,
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
strastí	strast	k1gFnPc2	strast
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
žít	žít	k5eAaImF	žít
umírněně	umírněně	k6eAd1	umírněně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
příjemně	příjemně	k6eAd1	příjemně
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
považoval	považovat	k5eAaImAgMnS	považovat
slast	slast	k1gFnSc4	slast
za	za	k7c4	za
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
současně	současně	k6eAd1	současně
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
:	:	kIx,	:
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
-li	i	k?	-li
někdo	někdo	k3yInSc1	někdo
slast	slast	k1gFnSc4	slast
příliš	příliš	k6eAd1	příliš
úporně	úporně	k6eAd1	úporně
<g/>
,	,	kIx,	,
dostaví	dostavit	k5eAaPmIp3nS	dostavit
se	se	k3xPyFc4	se
strast	strast	k1gFnSc1	strast
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
příliš	příliš	k6eAd1	příliš
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
bolest	bolest	k1gFnSc4	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Správná	správný	k2eAgFnSc1d1	správná
cesta	cesta	k1gFnSc1	cesta
životem	život	k1gInSc7	život
znamená	znamenat	k5eAaImIp3nS	znamenat
žít	žít	k5eAaImF	žít
příjemně	příjemně	k6eAd1	příjemně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
strasti	strast	k1gFnSc2	strast
z	z	k7c2	z
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
takového	takový	k3xDgInSc2	takový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
považovat	považovat	k5eAaImF	považovat
Epikúrovu	Epikúrův	k2eAgFnSc4d1	Epikúrova
filozofii	filozofie	k1gFnSc4	filozofie
za	za	k7c4	za
návod	návod	k1gInSc4	návod
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
slastí	slast	k1gFnPc2	slast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
k	k	k7c3	k
vyhýbání	vyhýbání	k1gNnSc3	vyhýbání
se	se	k3xPyFc4	se
strasti	strast	k1gFnPc4	strast
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
<g/>
-li	i	k?	-li
někdo	někdo	k3yInSc1	někdo
v	v	k7c6	v
životě	život	k1gInSc6	život
požitky	požitek	k1gInPc1	požitek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
bolesti	bolest	k1gFnSc3	bolest
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
podle	podle	k7c2	podle
Epikúra	Epikúro	k1gNnSc2	Epikúro
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
slasti	slast	k1gFnPc1	slast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
provázeny	provázet	k5eAaImNgInP	provázet
strastí	strast	k1gFnSc7	strast
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
tělesná	tělesný	k2eAgFnSc1d1	tělesná
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc1d1	sexuální
láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přináší	přinášet	k5eAaImIp3nS	přinášet
únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
výčitky	výčitka	k1gFnPc4	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
melancholii	melancholie	k1gFnSc4	melancholie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
"	"	kIx"	"
<g/>
dynamickými	dynamický	k2eAgFnPc7d1	dynamická
<g/>
"	"	kIx"	"
slastmi	slast	k1gFnPc7	slast
jsou	být	k5eAaImIp3nP	být
nenasytnost	nenasytnost	k1gFnSc4	nenasytnost
<g/>
,	,	kIx,	,
sláva	sláva	k1gFnSc1	sláva
dosažená	dosažený	k2eAgFnSc1d1	dosažená
výkonem	výkon	k1gInSc7	výkon
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
opilství	opilství	k1gNnSc2	opilství
a	a	k8xC	a
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
špatné	špatný	k2eAgFnPc1d1	špatná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gFnPc4	on
provázejí	provázet	k5eAaImIp3nP	provázet
strasti	strast	k1gFnPc4	strast
<g/>
:	:	kIx,	:
nenasytnost	nenasytnost	k1gFnSc1	nenasytnost
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nevolnosti	nevolnost	k1gFnSc3	nevolnost
<g/>
,	,	kIx,	,
sláva	sláva	k1gFnSc1	sláva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provázena	provázet	k5eAaImNgFnS	provázet
všemožnými	všemožný	k2eAgFnPc7d1	všemožná
nesnázemi	nesnáz	k1gFnPc7	nesnáz
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc4	pití
přináší	přinášet	k5eAaImIp3nS	přinášet
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
...	...	k?	...
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
uznával	uznávat	k5eAaImAgInS	uznávat
(	(	kIx(	(
<g/>
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
<g/>
)	)	kIx)	)
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bychom	by	kYmCp1nP	by
dnes	dnes	k6eAd1	dnes
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přátelství	přátelství	k1gNnSc1	přátelství
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
žádné	žádný	k3yNgFnPc4	žádný
strasti	strast	k1gFnPc4	strast
nedoprovázejí	doprovázet	k5eNaImIp3nP	doprovázet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
vysoce	vysoce	k6eAd1	vysoce
cenil	cenit	k5eAaImAgMnS	cenit
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgMnS	podporovat
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
<g/>
.	.	kIx.	.
</s>
<s>
Příteli	přítel	k1gMnSc3	přítel
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
všechno	všechen	k3xTgNnSc4	všechen
obětovat	obětovat	k5eAaBmF	obětovat
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
psychologického	psychologický	k2eAgMnSc4d1	psychologický
hédonika	hédonik	k1gMnSc4	hédonik
<g/>
.	.	kIx.	.
</s>
<s>
Očividně	očividně	k6eAd1	očividně
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
motivováni	motivován	k2eAgMnPc1d1	motivován
snahou	snaha	k1gFnSc7	snaha
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
slasti	slast	k1gFnPc4	slast
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
také	také	k6eAd1	také
etickým	etický	k2eAgMnSc7d1	etický
hédonikem	hédonik	k1gMnSc7	hédonik
(	(	kIx(	(
<g/>
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
významnými	významný	k2eAgFnPc7d1	významná
výhradami	výhrada	k1gFnPc7	výhrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etický	etický	k2eAgInSc1d1	etický
hédonismus	hédonismus	k1gInSc1	hédonismus
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
slast	slast	k1gFnSc4	slast
nejen	nejen	k6eAd1	nejen
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
ji	on	k3xPp3gFnSc4	on
mají	mít	k5eAaImIp3nP	mít
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sama	sám	k3xTgFnSc1	sám
slast	slast	k1gFnSc1	slast
je	být	k5eAaImIp3nS	být
dobrem	dobro	k1gNnSc7	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
umírněnou	umírněný	k2eAgFnSc4d1	umírněná
formu	forma	k1gFnSc4	forma
etického	etický	k2eAgInSc2d1	etický
i	i	k8xC	i
psychologického	psychologický	k2eAgInSc2d1	psychologický
hédonismu	hédonismus	k1gInSc2	hédonismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epikurejská	epikurejský	k2eAgFnSc1d1	epikurejská
metafyzika	metafyzika	k1gFnSc1	metafyzika
==	==	k?	==
</s>
</p>
<p>
<s>
Epikúrejská	Epikúrejský	k2eAgFnSc1d1	Epikúrejský
metafyzika	metafyzika	k1gFnSc1	metafyzika
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
materialistickém	materialistický	k2eAgInSc6d1	materialistický
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
monistickém	monistický	k2eAgInSc6d1	monistický
názoru	názor	k1gInSc6	názor
Démokritovu	Démokritův	k2eAgFnSc4d1	Démokritova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Démokritovy	Démokritův	k2eAgFnSc2d1	Démokritova
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
atomech	atom	k1gInPc6	atom
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
atomy	atom	k1gInPc4	atom
podle	podle	k7c2	podle
Epikura	Epikur	k1gMnSc2	Epikur
nadány	nadán	k2eAgFnPc1d1	nadána
jakousi	jakýsi	k3yIgFnSc7	jakýsi
svobodnou	svobodný	k2eAgFnSc7d1	svobodná
vůlí	vůle	k1gFnSc7	vůle
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
samovolné	samovolný	k2eAgFnPc1d1	samovolná
odchylky	odchylka	k1gFnPc1	odchylka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
clinamen	clinamen	k1gInSc1	clinamen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Víří	vířit	k5eAaImIp3nS	vířit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
háčků	háček	k1gMnPc2	háček
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
ve	v	k7c4	v
větší	veliký	k2eAgInPc4d2	veliký
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
dané	daný	k2eAgFnSc2d1	daná
dráhy	dráha	k1gFnSc2	dráha
odchýlit	odchýlit	k5eAaPmF	odchýlit
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
nedělitelného	dělitelný	k2eNgInSc2d1	nedělitelný
atomu	atom	k1gInSc2	atom
jako	jako	k8xC	jako
základního	základní	k2eAgInSc2d1	základní
prvku	prvek	k1gInSc2	prvek
univerza	univerzum	k1gNnSc2	univerzum
podle	podle	k7c2	podle
epikúrejců	epikúrejec	k1gMnPc2	epikúrejec
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Epikúrova	Epikúrův	k2eAgFnSc1d1	Epikúrova
atomistická	atomistický	k2eAgFnSc1d1	atomistická
představa	představa	k1gFnSc1	představa
bezúčelného	bezúčelný	k2eAgNnSc2d1	bezúčelné
univerza	univerzum	k1gNnSc2	univerzum
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
bohové	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
však	však	k9	však
člověku	člověk	k1gMnSc3	člověk
nevládnou	vládnout	k5eNaImIp3nP	vládnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
metafyzických	metafyzický	k2eAgInPc2d1	metafyzický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
materialistický	materialistický	k2eAgInSc1d1	materialistický
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
deterministický	deterministický	k2eAgMnSc1d1	deterministický
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
a	a	k8xC	a
postuluje	postulovat	k5eAaImIp3nS	postulovat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
nevládne	vládnout	k5eNaImIp3nS	vládnout
osud	osud	k1gInSc1	osud
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
může	moct	k5eAaImIp3nS	moct
sám	sám	k3xTgMnSc1	sám
řídit	řídit	k5eAaImF	řídit
a	a	k8xC	a
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c4	o
něm.	něm.	k?	něm.
Lidskou	lidský	k2eAgFnSc4d1	lidská
duši	duše	k1gFnSc4	duše
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Platóna	Platón	k1gMnSc2	Platón
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
o	o	k7c4	o
svět	svět	k1gInSc4	svět
a	a	k8xC	a
osud	osud	k1gInSc4	osud
člověka	člověk	k1gMnSc2	člověk
nezajímají	zajímat	k5eNaImIp3nP	zajímat
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
slavný	slavný	k2eAgInSc1d1	slavný
výrok	výrok	k1gInSc1	výrok
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
tak	tak	k6eAd1	tak
nejobávanější	obávaný	k2eAgNnSc4d3	nejobávanější
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
ničím	ničit	k5eAaImIp1nS	ničit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
tu	tu	k6eAd1	tu
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
nejsme	být	k5eNaImIp1nP	být
tu	tu	k6eAd1	tu
již	již	k9	již
my	my	k3xPp1nPc1	my
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Epikúrovo	Epikúrův	k2eAgNnSc4d1	Epikúrův
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Epikúrova	Epikúrův	k2eAgFnSc1d1	Epikúrova
filosofie	filosofie	k1gFnSc1	filosofie
tak	tak	k6eAd1	tak
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
protipól	protipól	k1gInSc4	protipól
předchozích	předchozí	k2eAgInPc2d1	předchozí
velkých	velký	k2eAgInPc2d1	velký
metafyzických	metafyzický	k2eAgInPc2d1	metafyzický
systémů	systém	k1gInPc2	systém
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
zkoumání	zkoumání	k1gNnSc4	zkoumání
tohoto	tento	k3xDgInSc2	tento
smyslově	smyslově	k6eAd1	smyslově
přístupného	přístupný	k2eAgInSc2d1	přístupný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
tak	tak	k9	tak
dodala	dodat	k5eAaPmAgFnS	dodat
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc4d2	veliký
vážnost	vážnost	k1gFnSc4	vážnost
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc4d1	klasická
podobu	podoba	k1gFnSc4	podoba
epikurejství	epikurejství	k1gNnSc2	epikurejství
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
básni	báseň	k1gFnSc6	báseň
"	"	kIx"	"
<g/>
O	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
<g/>
"	"	kIx"	"
jeho	jeho	k3xOp3gNnSc1	jeho
patrně	patrně	k6eAd1	patrně
největší	veliký	k2eAgMnSc1d3	veliký
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
filosof	filosof	k1gMnSc1	filosof
Lucretius	Lucretius	k1gMnSc1	Lucretius
Carus	Carus	k1gMnSc1	Carus
<g/>
.	.	kIx.	.
</s>
<s>
Epikúrovi	Epikúrův	k2eAgMnPc1d1	Epikúrův
žáci	žák	k1gMnPc1	žák
sice	sice	k8xC	sice
působili	působit	k5eAaImAgMnP	působit
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
propagovali	propagovat	k5eAaImAgMnP	propagovat
spíše	spíše	k9	spíše
fyzické	fyzický	k2eAgNnSc4d1	fyzické
pojetí	pojetí	k1gNnSc4	pojetí
slasti	slast	k1gFnSc2	slast
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
antickými	antický	k2eAgFnPc7d1	antická
filosofiemi	filosofie	k1gFnPc7	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
mimořádného	mimořádný	k2eAgInSc2d1	mimořádný
rozmachu	rozmach	k1gInSc2	rozmach
náboženství	náboženství	k1gNnSc2	náboženství
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
epikureismus	epikureismus	k1gInSc1	epikureismus
ostře	ostro	k6eAd1	ostro
odmítán	odmítat	k5eAaImNgInS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
novověké	novověký	k2eAgFnSc2d1	novověká
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
a	a	k8xC	a
sedmnáctém	sedmnáctý	k4xOgInSc6	sedmnáctý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
starověkou	starověký	k2eAgFnSc4d1	starověká
atomistickou	atomistický	k2eAgFnSc4d1	atomistická
metafyziku	metafyzika	k1gFnSc4	metafyzika
výrazně	výrazně	k6eAd1	výrazně
oživil	oživit	k5eAaPmAgMnS	oživit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
a	a	k8xC	a
epikureismus	epikureismus	k1gInSc1	epikureismus
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
řadu	řada	k1gFnSc4	řada
vědců	vědec	k1gMnPc2	vědec
(	(	kIx(	(
<g/>
Galilei	Galilei	k1gNnSc1	Galilei
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
Gassendi	Gassend	k1gMnPc1	Gassend
<g/>
,	,	kIx,	,
Newton	Newton	k1gMnSc1	Newton
<g/>
)	)	kIx)	)
i	i	k8xC	i
politických	politický	k2eAgMnPc2d1	politický
myslitelů	myslitel	k1gMnPc2	myslitel
(	(	kIx(	(
<g/>
Bodin	Bodin	k1gMnSc1	Bodin
<g/>
,	,	kIx,	,
Hobbes	Hobbes	k1gMnSc1	Hobbes
<g/>
,	,	kIx,	,
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
)	)	kIx)	)
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
objevili	objevit	k5eAaPmAgMnP	objevit
myšlenku	myšlenka	k1gFnSc4	myšlenka
společenské	společenský	k2eAgFnSc2d1	společenská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
i	i	k9	i
pro	pro	k7c4	pro
filosofii	filosofie	k1gFnSc4	filosofie
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
zabýval	zabývat	k5eAaImAgMnS	zabývat
například	například	k6eAd1	například
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
oceňoval	oceňovat	k5eAaImAgMnS	oceňovat
Epikúra	Epikúr	k1gMnSc4	Epikúr
jako	jako	k9	jako
objevitele	objevitel	k1gMnSc4	objevitel
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
dohodě	dohoda	k1gFnSc6	dohoda
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
na	na	k7c6	na
společenské	společenský	k2eAgFnSc6d1	společenská
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
Epikurovo	Epikurův	k2eAgNnSc1d1	Epikurovo
poučení	poučení	k1gNnSc1	poučení
o	o	k7c6	o
slasti	slast	k1gFnSc6	slast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
komentovaná	komentovaný	k2eAgNnPc1d1	komentované
vydání	vydání	k1gNnPc1	vydání
===	===	k?	===
</s>
</p>
<p>
<s>
Díogenés	Díogenés	k1gInSc1	Díogenés
Laertios	Laertios	k1gInSc1	Laertios
<g/>
:	:	kIx,	:
Životy	život	k1gInPc1	život
<g/>
,	,	kIx,	,
názory	názor	k1gInPc1	názor
a	a	k8xC	a
výroky	výrok	k1gInPc1	výrok
proslulých	proslulý	k2eAgMnPc2d1	proslulý
filosofů	filosof	k1gMnPc2	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Antonín	Antonín	k1gMnSc1	Antonín
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
473	[number]	k4	473
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901916	[number]	k4	901916
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
X.	X.	kA	X.
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
385	[number]	k4	385
<g/>
–	–	k?	–
<g/>
427	[number]	k4	427
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
EPIKUROS	Epikuros	k1gMnSc1	Epikuros
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šťastnom	šťastnom	k1gInSc1	šťastnom
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Nakladateľstvo	Nakladateľstvo	k1gNnSc1	Nakladateľstvo
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
453	[number]	k4	453
s.	s.	k?	s.
</s>
</p>
<p>
<s>
LUDVÍKOVSKÝ	ludvíkovský	k2eAgMnSc1d1	ludvíkovský
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
učení	učení	k1gNnSc1	učení
filosofa	filosof	k1gMnSc2	filosof
Epikura	Epikur	k1gMnSc2	Epikur
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
117	[number]	k4	117
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Titus	Titus	k1gMnSc1	Titus
Lucretius	Lucretius	k1gMnSc1	Lucretius
Carus	Carus	k1gMnSc1	Carus
<g/>
:	:	kIx,	:
O	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
De	De	k?	De
rerum	rerum	k1gInSc1	rerum
natura	natura	k1gFnSc1	natura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Julie	Julie	k1gFnSc1	Julie
Nováková	Nováková	k1gFnSc1	Nováková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
135830	[number]	k4	135830
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CETL	CETL	kA	CETL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
dějinami	dějiny	k1gFnPc7	dějiny
evropského	evropský	k2eAgNnSc2d1	Evropské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
27282	[number]	k4	27282
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Kapitola	kapitola	k1gFnSc1	kapitola
"	"	kIx"	"
<g/>
Epikuros	Epikuros	k1gMnSc1	Epikuros
–	–	k?	–
dovršitel	dovršitel	k1gMnSc1	dovršitel
atomismu	atomismus	k1gInSc2	atomismus
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
76	[number]	k4	76
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
Radislav	Radislav	k1gMnSc1	Radislav
Hošek	Hošek	k1gMnSc1	Hošek
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
ČERVENKA	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Pesimistický	pesimistický	k2eAgInSc4d1	pesimistický
motiv	motiv	k1gInSc4	motiv
v	v	k7c6	v
Epikurově	Epikurův	k2eAgFnSc6d1	Epikurova
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
mysl	mysl	k1gFnSc1	mysl
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
filosofický	filosofický	k2eAgInSc1d1	filosofický
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
82	[number]	k4	82
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FESTUGIÈ	FESTUGIÈ	k?	FESTUGIÈ
<g/>
,	,	kIx,	,
André-Jean	André-Jean	k1gInSc1	André-Jean
<g/>
.	.	kIx.	.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Radislav	Radislav	k1gMnSc1	Radislav
Hošek	Hošek	k1gMnSc1	Hošek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
125	[number]	k4	125
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86005	[number]	k4	86005
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
doby	doba	k1gFnSc2	doba
hellenistické	hellenistický	k2eAgFnSc2d1	hellenistický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
Společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
161	[number]	k4	161
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
749169	[number]	k4	749169
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
Epikuros	Epikuros	k1gMnSc1	Epikuros
a	a	k8xC	a
epikureismus	epikureismus	k1gInSc1	epikureismus
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
127	[number]	k4	127
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1	ludvíkovský
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
FINK	FINK	kA	FINK
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
<g/>
.	.	kIx.	.
</s>
<s>
Oáza	oáza	k1gFnSc1	oáza
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Věra	Věra	k1gFnSc1	Věra
Koubová	Koubová	k1gFnSc1	Koubová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
61	[number]	k4	61
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
224	[number]	k4	224
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
56	[number]	k4	56
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LONG	LONG	kA	LONG
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnSc2	Anthona
Arthur	Arthura	k1gFnPc2	Arthura
<g/>
.	.	kIx.	.
</s>
<s>
Hellénistická	Hellénistický	k2eAgFnSc1d1	Hellénistický
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
:	:	kIx,	:
stoikové	stoik	k1gMnPc1	stoik
<g/>
,	,	kIx,	,
epikurejci	epikurejec	k1gMnPc1	epikurejec
<g/>
,	,	kIx,	,
skeptikové	skeptik	k1gMnPc1	skeptik
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Kolev	Kolev	k1gFnSc1	Kolev
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
OIKOYMENH	OIKOYMENH	kA	OIKOYMENH
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
341	[number]	k4	341
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7298	[number]	k4	7298
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCGREAL	MCGREAL	kA	MCGREAL
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
Philip	Philip	k1gMnSc1	Philip
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
postavy	postava	k1gFnPc1	postava
západního	západní	k2eAgNnSc2d1	západní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
:	:	kIx,	:
slovník	slovník	k1gInSc1	slovník
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
707	[number]	k4	707
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85190	[number]	k4	85190
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
59	[number]	k4	59
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MERTL	Mertl	k1gMnSc1	Mertl
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
politického	politický	k2eAgNnSc2d1	politické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
214	[number]	k4	214
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
761018	[number]	k4	761018
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Kapitola	kapitola	k1gFnSc1	kapitola
"	"	kIx"	"
<g/>
Epikurejci	epikurejec	k1gMnPc1	epikurejec
a	a	k8xC	a
stoikové	stoik	k1gMnPc1	stoik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
:	:	kIx,	:
illustrovaná	illustrovaný	k2eAgFnSc1d1	illustrovaná
encyklopaedie	encyklopaedie	k1gFnSc1	encyklopaedie
obecných	obecný	k2eAgFnPc2d1	obecná
vědomostí	vědomost	k1gFnPc2	vědomost
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
1039	[number]	k4	1039
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
277218	[number]	k4	277218
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
665	[number]	k4	665
<g/>
–	–	k?	–
<g/>
667	[number]	k4	667
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ateismus	ateismus	k1gInSc1	ateismus
</s>
</p>
<p>
<s>
Atomismus	atomismus	k1gInSc1	atomismus
</s>
</p>
<p>
<s>
Epikureismus	epikureismus	k1gInSc1	epikureismus
</s>
</p>
<p>
<s>
Hédonismus	hédonismus	k1gInSc1	hédonismus
</s>
</p>
<p>
<s>
Helénismus	helénismus	k1gInSc1	helénismus
</s>
</p>
<p>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
filosofie	filosofie	k1gFnSc1	filosofie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
1911	[number]	k4	1911
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannica	k1gMnSc1	Britannica
<g/>
/	/	kIx~	/
<g/>
Epicurus	Epicurus	k1gMnSc1	Epicurus
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Lives_of_the_Eminent_Philosophers	Lives_of_the_Eminent_Philosophersa	k1gFnPc2	Lives_of_the_Eminent_Philosophersa
<g/>
/	/	kIx~	/
<g/>
Book_X	Book_X	k1gFnSc1	Book_X
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Díogena	Díogen	k1gMnSc2	Díogen
Laertského	Laertský	k2eAgMnSc2d1	Laertský
a	a	k8xC	a
článek	článek	k1gInSc1	článek
o	o	k7c6	o
Epikúrově	Epikúrův	k2eAgInSc6d1	Epikúrův
životopise	životopis	k1gInSc6	životopis
</s>
</p>
