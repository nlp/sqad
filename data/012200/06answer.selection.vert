<s>
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
byl	být	k5eAaImAgInS	být
autorem	autor	k1gMnSc7	autor
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
však	však	k9	však
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
