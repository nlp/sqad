<s>
Plynový	plynový	k2eAgInSc1d1	plynový
zapalovač	zapalovač	k1gInSc1	zapalovač
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
spalování	spalování	k1gNnSc6	spalování
plynného	plynný	k2eAgInSc2d1	plynný
butanu	butan	k1gInSc2	butan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
teplotu	teplota	k1gFnSc4	teplota
až	až	k9	až
okolo	okolo	k7c2	okolo
750	[number]	k4	750
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
