<p>
<s>
Zapalovač	zapalovač	k1gInSc1	zapalovač
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
respektive	respektive	k9	respektive
zapálí	zapálit	k5eAaPmIp3nS	zapálit
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
buď	buď	k8xC	buď
na	na	k7c6	na
principu	princip	k1gInSc6	princip
mechanického	mechanický	k2eAgInSc2d1	mechanický
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ručních	ruční	k2eAgInPc2d1	ruční
respektive	respektive	k9	respektive
kapesních	kapesní	k2eAgInPc2d1	kapesní
zapalovačů	zapalovač	k1gInPc2	zapalovač
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ale	ale	k9	ale
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
elektrický	elektrický	k2eAgInSc4d1	elektrický
spotřebič	spotřebič	k1gInSc4	spotřebič
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
běžných	běžný	k2eAgMnPc2d1	běžný
domácích	domácí	k1gMnPc2	domácí
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
kuchyňských	kuchyňská	k1gFnPc2	kuchyňská
<g/>
)	)	kIx)	)
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kapesní	kapesní	k2eAgInPc4d1	kapesní
zapalovače	zapalovač	k1gInPc4	zapalovač
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
zapalovačů	zapalovač	k1gMnPc2	zapalovač
–	–	k?	–
plynové	plynový	k2eAgNnSc4d1	plynové
a	a	k8xC	a
benzínové	benzínový	k2eAgNnSc4d1	benzínové
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
ještě	ještě	k9	ještě
podle	podle	k7c2	podle
možnosti	možnost	k1gFnSc2	možnost
doplňování	doplňování	k1gNnSc2	doplňování
hořlaviny	hořlavina	k1gFnSc2	hořlavina
na	na	k7c4	na
zapalovače	zapalovač	k1gInPc4	zapalovač
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
a	a	k8xC	a
plnící	plnící	k2eAgFnSc4d1	plnící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zapalovač	zapalovač	k1gInSc1	zapalovač
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jsou	být	k5eAaImIp3nP	být
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
hořlavinou	hořlavina	k1gFnSc7	hořlavina
<g/>
,	,	kIx,	,
tryska	tryska	k1gFnSc1	tryska
či	či	k8xC	či
knot	knot	k1gInSc1	knot
a	a	k8xC	a
mechanismem	mechanismus	k1gInSc7	mechanismus
který	který	k3yQgInSc4	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jiskru	jiskra	k1gFnSc4	jiskra
<g/>
.	.	kIx.	.
</s>
<s>
Jiskrový	jiskrový	k2eAgInSc1d1	jiskrový
mechanismus	mechanismus	k1gInSc1	mechanismus
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
buď	buď	k8xC	buď
piezoelektrickým	piezoelektrický	k2eAgInSc7d1	piezoelektrický
krystalem	krystal	k1gInSc7	krystal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
stisknutí	stisknutí	k1gNnSc6	stisknutí
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
jiskru	jiskra	k1gFnSc4	jiskra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zapálí	zapálit	k5eAaPmIp3nS	zapálit
hořlavinu	hořlavina	k1gFnSc4	hořlavina
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
mechanismu	mechanismus	k1gInSc2	mechanismus
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
podobné	podobný	k2eAgNnSc1d1	podobné
křesadlu	křesadlo	k1gNnSc3	křesadlo
–	–	k?	–
ozubený	ozubený	k2eAgInSc4d1	ozubený
válec	válec	k1gInSc4	válec
s	s	k7c7	s
drážkováním	drážkování	k1gNnSc7	drážkování
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tře	třít	k5eAaImIp3nS	třít
o	o	k7c4	o
"	"	kIx"	"
<g/>
kamínek	kamínek	k1gInSc4	kamínek
do	do	k7c2	do
zapalovače	zapalovač	k1gInSc2	zapalovač
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jiskry	jiskra	k1gFnSc2	jiskra
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
k	k	k7c3	k
zapálení	zapálení	k1gNnSc3	zapálení
hořlaviny	hořlavina	k1gFnSc2	hořlavina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Benzinový	benzinový	k2eAgInSc1d1	benzinový
zapalovač	zapalovač	k1gInSc1	zapalovač
===	===	k?	===
</s>
</p>
<p>
<s>
Benzinové	benzinový	k2eAgInPc1d1	benzinový
zapalovače	zapalovač	k1gInPc1	zapalovač
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
kovové	kovový	k2eAgInPc1d1	kovový
a	a	k8xC	a
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
zasunuté	zasunutý	k2eAgNnSc1d1	zasunuté
těleso	těleso	k1gNnSc1	těleso
zapalovače	zapalovač	k1gInSc2	zapalovač
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
vatou	vata	k1gFnSc7	vata
<g/>
,	,	kIx,	,
či	či	k8xC	či
jinou	jiný	k2eAgFnSc7d1	jiná
sající	sající	k2eAgFnSc7d1	sající
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
hořlavinu	hořlavina	k1gFnSc4	hořlavina
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
technický	technický	k2eAgInSc1d1	technický
benzín	benzín	k1gInSc1	benzín
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
jí	jíst	k5eAaImIp3nS	jíst
před	před	k7c7	před
vypařením	vypaření	k1gNnSc7	vypaření
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tělesa	těleso	k1gNnSc2	těleso
zapalovače	zapalovač	k1gInSc2	zapalovač
je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
knot	knot	k1gInSc1	knot
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zvlhčen	zvlhčen	k2eAgInSc4d1	zvlhčen
hořlavinou	hořlavina	k1gFnSc7	hořlavina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
škrtnutí	škrtnutí	k1gNnSc6	škrtnutí
jsou	být	k5eAaImIp3nP	být
zapáleny	zapálen	k2eAgFnPc4d1	zapálena
hořlavé	hořlavý	k2eAgFnPc4d1	hořlavá
směsi	směs	k1gFnPc4	směs
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
plamen	plamen	k1gInSc1	plamen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
zapalovačů	zapalovač	k1gMnPc2	zapalovač
mají	mít	k5eAaImIp3nP	mít
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
knotu	knot	k1gInSc2	knot
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
speciální	speciální	k2eAgFnSc7d1	speciální
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
děrovaná	děrovaný	k2eAgFnSc1d1	děrovaná
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plamenu	plamen	k1gInSc2	plamen
hořet	hořet	k5eAaImF	hořet
i	i	k9	i
při	při	k7c6	při
prudkém	prudký	k2eAgNnSc6d1	prudké
foukání	foukání	k1gNnSc6	foukání
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
je	být	k5eAaImIp3nS	být
patentována	patentován	k2eAgFnSc1d1	patentována
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
benzínovými	benzínový	k2eAgMnPc7d1	benzínový
zapalovači	zapalovač	k1gMnPc7	zapalovač
jsou	být	k5eAaImIp3nP	být
výrobky	výrobek	k1gInPc1	výrobek
značky	značka	k1gFnSc2	značka
Zippo	Zippa	k1gFnSc5	Zippa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
uživatele	uživatel	k1gMnPc4	uživatel
zárukou	záruka	k1gFnSc7	záruka
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
na	na	k7c4	na
veškeré	veškerý	k3xTgInPc4	veškerý
svoje	svůj	k3xOyFgInPc4	svůj
zapalovače	zapalovač	k1gInPc4	zapalovač
doživotní	doživotní	k2eAgFnSc4d1	doživotní
záruku	záruka	k1gFnSc4	záruka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
značkou	značka	k1gFnSc7	značka
je	být	k5eAaImIp3nS	být
rakouské	rakouský	k2eAgFnSc3d1	rakouská
IMCO	IMCO	kA	IMCO
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
stoletou	stoletý	k2eAgFnSc7d1	stoletá
tradicí	tradice	k1gFnSc7	tradice
výroby	výroba	k1gFnSc2	výroba
benzínových	benzínový	k2eAgInPc2d1	benzínový
zapalovačů	zapalovač	k1gInPc2	zapalovač
vlastního	vlastní	k2eAgInSc2d1	vlastní
patentu	patent	k1gInSc2	patent
známých	známý	k1gMnPc2	známý
v	v	k7c6	v
ČR	ČR	kA	ČR
též	též	k9	též
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
rakušák	rakušák	k?	rakušák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plynové	plynový	k2eAgInPc4d1	plynový
zapalovače	zapalovač	k1gInPc4	zapalovač
===	===	k?	===
</s>
</p>
<p>
<s>
Plynový	plynový	k2eAgInSc1d1	plynový
zapalovač	zapalovač	k1gInSc1	zapalovač
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
spalování	spalování	k1gNnSc6	spalování
plynného	plynný	k2eAgInSc2d1	plynný
butanu	butan	k1gInSc2	butan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
teplotu	teplota	k1gFnSc4	teplota
až	až	k9	až
okolo	okolo	k7c2	okolo
750	[number]	k4	750
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Butan	butan	k1gInSc1	butan
je	být	k5eAaImIp3nS	být
skladován	skladovat	k5eAaImNgInS	skladovat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
nádržce	nádržka	k1gFnSc6	nádržka
pod	pod	k7c7	pod
křesadlem	křesadlo	k1gNnSc7	křesadlo
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
zapalovačů	zapalovač	k1gMnPc2	zapalovač
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nádržku	nádržka	k1gFnSc4	nádržka
pro	pro	k7c4	pro
plyn	plyn	k1gInSc4	plyn
doplňovat	doplňovat	k5eAaImF	doplňovat
z	z	k7c2	z
externího	externí	k2eAgInSc2d1	externí
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
životnost	životnost	k1gFnSc4	životnost
zapalovače	zapalovač	k1gInSc2	zapalovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
nádržky	nádržka	k1gFnSc2	nádržka
je	být	k5eAaImIp3nS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
trubička	trubička	k1gFnSc1	trubička
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
stisknutí	stisknutí	k1gNnSc6	stisknutí
plastového	plastový	k2eAgNnSc2d1	plastové
tlačítka	tlačítko	k1gNnSc2	tlačítko
regulován	regulovat	k5eAaImNgInS	regulovat
únik	únik	k1gInSc1	únik
plynu	plyn	k1gInSc2	plyn
z	z	k7c2	z
trysky	tryska	k1gFnSc2	tryska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
zapalovače	zapalovač	k1gInSc2	zapalovač
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
umístěno	umístěn	k2eAgNnSc4d1	umístěno
regulační	regulační	k2eAgNnSc4d1	regulační
kolečko	kolečko	k1gNnSc4	kolečko
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
regulovat	regulovat	k5eAaImF	regulovat
zaškrcení	zaškrcený	k2eAgMnPc1d1	zaškrcený
trysky	tryska	k1gFnSc2	tryska
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvětšování	zvětšování	k1gNnSc3	zvětšování
či	či	k8xC	či
zmenšování	zmenšování	k1gNnSc3	zmenšování
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
kolečko	kolečko	k1gNnSc1	kolečko
vytočí	vytočit	k5eAaPmIp3nS	vytočit
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
plyn	plyn	k1gInSc1	plyn
uniká	unikat	k5eAaImIp3nS	unikat
ze	z	k7c2	z
zapalovače	zapalovač	k1gInSc2	zapalovač
nekontrolovatelně	kontrolovatelně	k6eNd1	kontrolovatelně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
zapálení	zapálení	k1gNnSc3	zapálení
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
neovladatelný	ovladatelný	k2eNgInSc4d1	neovladatelný
zdroj	zdroj	k1gInSc4	zdroj
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plynové	plynový	k2eAgInPc1d1	plynový
zapalovače	zapalovač	k1gInPc1	zapalovač
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mohou	moct	k5eAaImIp3nP	moct
samovolně	samovolně	k6eAd1	samovolně
vybouchnout	vybouchnout	k5eAaPmF	vybouchnout
vlivem	vliv	k1gInSc7	vliv
expanze	expanze	k1gFnSc2	expanze
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejnebezpečnější	bezpečný	k2eNgNnPc4d3	nejnebezpečnější
místa	místo	k1gNnPc4	místo
patří	patřit	k5eAaImIp3nP	patřit
interiéry	interiér	k1gInPc1	interiér
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
slunečním	sluneční	k2eAgInPc3d1	sluneční
paprskům	paprsek	k1gInPc3	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektrické	elektrický	k2eAgInPc4d1	elektrický
zapalovače	zapalovač	k1gInPc4	zapalovač
==	==	k?	==
</s>
</p>
<p>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
k	k	k7c3	k
podpalování	podpalování	k1gNnSc3	podpalování
plynových	plynový	k2eAgInPc2d1	plynový
spotřebičů	spotřebič	k1gInPc2	spotřebič
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sporáky	sporák	k1gInPc1	sporák
<g/>
,	,	kIx,	,
karmy	karma	k1gFnPc1	karma
<g/>
,	,	kIx,	,
plynová	plynový	k2eAgNnPc1d1	plynové
kamna	kamna	k1gNnPc1	kamna
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Zapalovače	zapalovač	k1gInPc4	zapalovač
piezoelelektrické	piezoelelektrický	k2eAgInPc4d1	piezoelelektrický
===	===	k?	===
</s>
</p>
<p>
<s>
Pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
piezoelektrickém	piezoelektrický	k2eAgInSc6d1	piezoelektrický
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
stlačením	stlačení	k1gNnSc7	stlačení
krystalu	krystal	k1gInSc2	krystal
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
elektrické	elektrický	k2eAgNnSc4d1	elektrické
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
malý	malý	k2eAgInSc4d1	malý
elektrický	elektrický	k2eAgInSc4d1	elektrický
výboj	výboj	k1gInSc4	výboj
v	v	k7c6	v
jiskřišti	jiskřiště	k1gNnSc6	jiskřiště
<g/>
,	,	kIx,	,
jiskra	jiskra	k1gFnSc1	jiskra
výboje	výboj	k1gInSc2	výboj
pak	pak	k6eAd1	pak
podpálí	podpálit	k5eAaPmIp3nS	podpálit
plyn	plyn	k1gInSc4	plyn
ve	v	k7c6	v
sporáku	sporák	k1gInSc6	sporák
či	či	k8xC	či
v	v	k7c6	v
plynových	plynový	k2eAgNnPc6d1	plynové
kamnech	kamna	k1gNnPc6	kamna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zapalovače	zapalovač	k1gInPc1	zapalovač
síťové	síťový	k2eAgFnSc2d1	síťová
===	===	k?	===
</s>
</p>
<p>
<s>
Existuji	existovat	k5eAaImIp1nS	existovat
i	i	k9	i
zapalovací	zapalovací	k2eAgNnSc4d1	zapalovací
zařízení	zařízení	k1gNnSc4	zařízení
pracující	pracující	k1gFnSc2	pracující
s	s	k7c7	s
napájením	napájení	k1gNnSc7	napájení
z	z	k7c2	z
běžné	běžný	k2eAgFnSc2d1	běžná
elektrorozvodné	elektrorozvodný	k2eAgFnSc2d1	elektrorozvodná
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cigaretové	cigaretový	k2eAgInPc4d1	cigaretový
zapalovače	zapalovač	k1gInPc4	zapalovač
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
===	===	k?	===
</s>
</p>
<p>
<s>
Cigaretové	cigaretový	k2eAgInPc1d1	cigaretový
zapalovače	zapalovač	k1gInPc1	zapalovač
jsou	být	k5eAaImIp3nP	být
běžným	běžný	k2eAgNnSc7d1	běžné
vybavením	vybavení	k1gNnSc7	vybavení
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
však	však	k9	však
už	už	k6eAd1	už
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
ve	v	k7c6	v
zpřístupnění	zpřístupnění	k1gNnSc6	zpřístupnění
palubní	palubní	k2eAgFnSc2d1	palubní
sítě	síť	k1gFnSc2	síť
automobilu	automobil	k1gInSc2	automobil
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
účelům	účel	k1gInPc3	účel
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
pro	pro	k7c4	pro
nabíjení	nabíjení	k1gNnSc4	nabíjení
mobilů	mobil	k1gInPc2	mobil
nebo	nebo	k8xC	nebo
napájení	napájení	k1gNnSc2	napájení
navigací	navigace	k1gFnPc2	navigace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
automobilový	automobilový	k2eAgInSc1d1	automobilový
zapalovač	zapalovač	k1gInSc1	zapalovač
cigaret	cigareta	k1gFnPc2	cigareta
</s>
</p>
<p>
<s>
Autozapalovač	Autozapalovač	k1gMnSc1	Autozapalovač
</s>
</p>
<p>
<s>
CL	CL	kA	CL
konektor	konektor	k1gInSc1	konektor
</s>
</p>
<p>
<s>
auto	auto	k1gNnSc1	auto
zásuvka	zásuvka	k1gFnSc1	zásuvka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zapalovače	zapalovač	k1gInSc2	zapalovač
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Zapalovač	zapalovač	k1gInSc4	zapalovač
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zapalovač	zapalovač	k1gInSc1	zapalovač
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
