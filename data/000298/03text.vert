<s>
Klášter	klášter	k1gInSc1	klášter
řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Voršily	Voršila	k1gFnSc2	Voršila
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Voršilky	voršilka	k1gFnPc1	voršilka
do	do	k7c2	do
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
dorazily	dorazit	k5eAaPmAgInP	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
postavit	postavit	k5eAaPmF	postavit
zde	zde	k6eAd1	zde
nový	nový	k2eAgInSc4d1	nový
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
kvůli	kvůli	k7c3	kvůli
zdlouhavým	zdlouhavý	k2eAgNnPc3d1	zdlouhavé
jednáním	jednání	k1gNnPc3	jednání
s	s	k7c7	s
městským	městský	k2eAgInSc7d1	městský
magistrátem	magistrát	k1gInSc7	magistrát
trval	trvat	k5eAaImAgInS	trvat
celých	celý	k2eAgInPc2d1	celý
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
konečně	konečně	k6eAd1	konečně
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
stavba	stavba	k1gFnSc1	stavba
kláštera	klášter	k1gInSc2	klášter
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Kiliána	Kilián	k1gMnSc2	Kilián
Ignáce	Ignác	k1gMnSc2	Ignác
Dientzenhofera	Dientzenhofer	k1gMnSc2	Dientzenhofer
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1735	[number]	k4	1735
<g/>
-	-	kIx~	-
<g/>
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
na	na	k7c6	na
polygonním	polygonní	k2eAgInSc6d1	polygonní
půdorysu	půdorys	k1gInSc6	půdorys
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
centrální	centrální	k2eAgFnSc7d1	centrální
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
jen	jen	k6eAd1	jen
tři	tři	k4xCgNnPc4	tři
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
B.	B.	kA	B.
Ohmana	Ohman	k1gMnSc2	Ohman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
uliční	uliční	k2eAgFnSc2d1	uliční
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byly	být	k5eAaImAgFnP	být
řádové	řádový	k2eAgFnPc1d1	řádová
sestry	sestra	k1gFnPc1	sestra
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
vystěhovány	vystěhován	k2eAgInPc1d1	vystěhován
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
sídlí	sídlet	k5eAaImIp3nS	sídlet
archiv	archiv	k1gInSc1	archiv
a	a	k8xC	a
církevní	církevní	k2eAgFnSc1d1	církevní
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Voršily	Voršila	k1gFnPc1	Voršila
<g/>
,	,	kIx,	,
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
města	město	k1gNnSc2	město
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
</s>
