<s>
Staroruština	Staroruština	k1gFnSc1	Staroruština
či	či	k8xC	či
stará	starý	k2eAgFnSc1d1	stará
ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
д	д	k?	д
я	я	k?	я
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
д	д	k?	д
м	м	k?	м
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
с	с	k?	с
м	м	k?	м
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
používali	používat	k5eAaImAgMnP	používat
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
východní	východní	k2eAgMnPc1d1	východní
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
několika	několik	k4yIc2	několik
východních	východní	k2eAgNnPc2d1	východní
vojvodství	vojvodství	k1gNnPc2	vojvodství
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Východoslovanské	východoslovanský	k2eAgInPc1d1	východoslovanský
kmeny	kmen	k1gInPc1	kmen
zpočátku	zpočátku	k6eAd1	zpočátku
hovořily	hovořit	k5eAaImAgInP	hovořit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
slabých	slabý	k2eAgInPc2d1	slabý
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
kontaktů	kontakt	k1gInPc2	kontakt
kmenovými	kmenový	k2eAgInPc7d1	kmenový
dialekty	dialekt	k1gInPc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
naddialektální	naddialektální	k2eAgFnSc2d1	naddialektální
podoby	podoba	k1gFnSc2	podoba
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vznik	vznik	k1gInSc1	vznik
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
začalo	začít	k5eAaPmAgNnS	začít
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
rozkvětu	rozkvět	k1gInSc3	rozkvět
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Moudrého	moudrý	k2eAgMnSc2d1	moudrý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
postupně	postupně	k6eAd1	postupně
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdrobeným	rozdrobený	k2eAgNnSc7d1	rozdrobené
ruským	ruský	k2eAgNnSc7d1	ruské
knížectvím	knížectví	k1gNnSc7	knížectví
zasadil	zasadit	k5eAaPmAgInS	zasadit
těžký	těžký	k2eAgInSc1d1	těžký
úder	úder	k1gInSc1	úder
mongolský	mongolský	k2eAgInSc4d1	mongolský
vpád	vpád	k1gInSc4	vpád
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
východoslovanské	východoslovanský	k2eAgNnSc4d1	východoslovanské
území	území	k1gNnPc4	území
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
izolované	izolovaný	k2eAgFnPc4d1	izolovaná
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
nového	nový	k2eAgNnSc2d1	nové
politického	politický	k2eAgNnSc2d1	politické
uspořádání	uspořádání	k1gNnSc2	uspořádání
se	se	k3xPyFc4	se
západoruská	západoruský	k2eAgNnPc1d1	západoruský
nářečí	nářečí	k1gNnPc1	nářečí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
východních	východní	k2eAgInPc2d1	východní
dialektů	dialekt	k1gInPc2	dialekt
budoucí	budoucí	k2eAgFnSc2d1	budoucí
velkoruské	velkoruský	k2eAgFnSc2d1	velkoruská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
jednotný	jednotný	k2eAgInSc1d1	jednotný
staroruský	staroruský	k2eAgInSc1d1	staroruský
jazyk	jazyk	k1gInSc1	jazyk
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
tak	tak	k6eAd1	tak
vzniknout	vzniknout	k5eAaPmF	vzniknout
svým	svůj	k3xOyFgMnPc3	svůj
následníkům	následník	k1gMnPc3	následník
-	-	kIx~	-
východní	východní	k2eAgFnSc6d1	východní
ruštině	ruština	k1gFnSc6	ruština
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
staroběloruštině	staroběloruština	k1gFnSc6	staroběloruština
a	a	k8xC	a
jejím	její	k3xOp3gMnPc3	její
následníkům	následník	k1gMnPc3	následník
běloruštině	běloruština	k1gFnSc6	běloruština
<g/>
,	,	kIx,	,
ukrajinštině	ukrajinština	k1gFnSc6	ukrajinština
a	a	k8xC	a
rusínštině	rusínština	k1gFnSc6	rusínština
<g/>
.	.	kIx.	.
východoslovanština	východoslovanština	k1gFnSc1	východoslovanština
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
–	–	k?	–
Šíření	šíření	k1gNnSc4	šíření
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
řeči	řeč	k1gFnSc2	řeč
na	na	k7c6	na
území	území	k1gNnSc6	území
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
neslovanskými	slovanský	k2eNgInPc7d1	neslovanský
jazykovými	jazykový	k2eAgInPc7d1	jazykový
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
raná	raný	k2eAgFnSc1d1	raná
staroruština	staroruština	k1gFnSc1	staroruština
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
–	–	k?	–
Utváření	utváření	k1gNnSc1	utváření
jazyka	jazyk	k1gInSc2	jazyk
staroruského	staroruský	k2eAgNnSc2d1	staroruské
etnika	etnikum	k1gNnSc2	etnikum
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
pozdní	pozdní	k2eAgFnSc1d1	pozdní
staroruština	staroruština	k1gFnSc1	staroruština
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
konec	konec	k1gInSc1	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
–	–	k?	–
Vyčleňování	vyčleňování	k1gNnSc4	vyčleňování
velkých	velký	k2eAgFnPc2d1	velká
jazykových	jazykový	k2eAgFnPc2d1	jazyková
zón	zóna	k1gFnPc2	zóna
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
konvergenčních	konvergenční	k2eAgInPc2d1	konvergenční
i	i	k8xC	i
divergenčních	divergenční	k2eAgInPc2d1	divergenční
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
výhradně	výhradně	k6eAd1	výhradně
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgFnPc4d1	literární
památky	památka	k1gFnPc4	památka
psané	psaný	k2eAgFnPc4d1	psaná
hlaholicí	hlaholice	k1gFnSc7	hlaholice
na	na	k7c4	na
území	území	k1gNnSc4	území
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
však	však	k9	však
hlaholské	hlaholský	k2eAgNnSc4d1	hlaholské
graffiti	graffiti	k1gNnSc4	graffiti
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
zlomky	zlomek	k1gInPc4	zlomek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
novgorodském	novgorodský	k2eAgInSc6d1	novgorodský
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
vznikala	vznikat	k5eAaImAgFnS	vznikat
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
alfabety	alfabeta	k1gFnSc2	alfabeta
doplňováním	doplňování	k1gNnSc7	doplňování
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
řečtině	řečtina	k1gFnSc3	řečtina
cizí	cizí	k2eAgFnPc1d1	cizí
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
abecedy	abeceda	k1gFnSc2	abeceda
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
krocích	krok	k1gInPc6	krok
–	–	k?	–
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
písmena	písmeno	k1gNnPc4	písmeno
б	б	k?	б
ж	ж	k?	ж
ш	ш	k?	ш
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ѕ	ѕ	k?	ѕ
ц	ц	k?	ц
ч	ч	k?	ч
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ъ	ъ	k?	ъ
ѣ	ѣ	k?	ѣ
ѫ	ѫ	k?	ѫ
ѭ	ѭ	k?	ѭ
ю	ю	k?	ю
ѧ	ѧ	k?	ѧ
Nová	nový	k2eAgFnSc1d1	nová
souhláska	souhláska	k1gFnSc1	souhláska
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
zpravidla	zpravidla	k6eAd1	zpravidla
místo	místo	k1gNnSc4	místo
před	před	k7c7	před
foneticky	foneticky	k6eAd1	foneticky
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
souhláskou	souhláska	k1gFnSc7	souhláska
a	a	k8xC	a
samohláska	samohláska	k1gFnSc1	samohláska
byla	být	k5eAaImAgFnS	být
přidávána	přidávat	k5eAaImNgFnS	přidávat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
pak	pak	k6eAd1	pak
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
vypouštění	vypouštění	k1gNnSc3	vypouštění
řeckých	řecký	k2eAgNnPc2d1	řecké
písmen	písmeno	k1gNnPc2	písmeno
nemajících	mající	k2eNgNnPc2d1	nemající
v	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
řeči	řeč	k1gFnSc6	řeč
přímý	přímý	k2eAgInSc4d1	přímý
protějšek	protějšek	k1gInSc4	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
varianta	varianta	k1gFnSc1	varianta
cyrilské	cyrilský	k2eAgFnSc2d1	Cyrilská
abecedy	abeceda	k1gFnSc2	abeceda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
kyjevského	kyjevský	k2eAgInSc2d1	kyjevský
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Sofie	Sofia	k1gFnSc2	Sofia
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
27	[number]	k4	27
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
pořadí	pořadí	k1gNnSc1	pořadí
přesně	přesně	k6eAd1	přesně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
řecké	řecký	k2eAgFnSc3d1	řecká
abecedě	abeceda	k1gFnSc3	abeceda
(	(	kIx(	(
<g/>
а	а	k?	а
б	б	k?	б
в	в	k?	в
г	г	k?	г
д	д	k?	д
є	є	k?	є
ж	ж	k?	ж
з	з	k?	з
и	и	k?	и
ѳ	ѳ	k?	ѳ
ı	ı	k?	ı
к	к	k?	к
л	л	k?	л
м	м	k?	м
н	н	k?	н
ѯ	ѯ	k?	ѯ
о	о	k?	о
п	п	k?	п
р	р	k?	р
с	с	k?	с
т	т	k?	т
у	у	k?	у
ф	ф	k?	ф
х	х	k?	х
ш	ш	k?	ш
щ	щ	k?	щ
ѡ	ѡ	k?	ѡ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
řecké	řecký	k2eAgNnSc1d1	řecké
písmeno	písmeno	k1gNnSc1	písmeno
ψ	ψ	k?	ψ
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
přehodnoceno	přehodnotit	k5eAaPmNgNnS	přehodnotit
jako	jako	k9	jako
щ	щ	k?	щ
a	a	k8xC	a
grafém	grafém	k1gInSc1	grafém
у	у	k?	у
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
ligaturou	ligatura	k1gFnSc7	ligatura
ѹ	ѹ	k?	ѹ
Slova	slovo	k1gNnPc4	slovo
se	se	k3xPyFc4	se
v	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
obvykle	obvykle	k6eAd1	obvykle
neoddělují	oddělovat	k5eNaImIp3nP	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
období	období	k1gNnSc6	období
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
účelu	účel	k1gInSc2	účel
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tři	tři	k4xCgInPc1	tři
grafické	grafický	k2eAgInPc1d1	grafický
systémy	systém	k1gInPc1	systém
<g/>
:	:	kIx,	:
jednojerové	jednojerový	k2eAgNnSc4d1	jednojerový
psaní	psaní	k1gNnSc4	psaní
–	–	k?	–
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
ruského	ruský	k2eAgNnSc2d1	ruské
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dvou	dva	k4xCgInPc2	dva
jerů	jer	k1gInPc2	jer
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
pouze	pouze	k6eAd1	pouze
jer	jer	k1gInSc1	jer
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
ъ	ъ	k?	ъ
V	v	k7c6	v
památkách	památka	k1gFnPc6	památka
knižní	knižní	k2eAgFnSc2d1	knižní
povahy	povaha	k1gFnSc2	povaha
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
grafický	grafický	k2eAgInSc1d1	grafický
systém	systém	k1gInSc1	systém
nahrazen	nahradit	k5eAaPmNgInS	nahradit
už	už	k6eAd1	už
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
knižním	knižní	k2eAgNnSc7d1	knižní
psaním	psaní	k1gNnSc7	psaní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
lépe	dobře	k6eAd2	dobře
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
fonologii	fonologie	k1gFnSc4	fonologie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neformální	formální	k2eNgFnSc6d1	neformální
korespondenci	korespondence	k1gFnSc6	korespondence
se	se	k3xPyFc4	se
jednojerový	jednojerový	k2eAgInSc1d1	jednojerový
pravopis	pravopis	k1gInSc1	pravopis
udržel	udržet	k5eAaPmAgInS	udržet
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vytlačen	vytlačen	k2eAgInSc1d1	vytlačen
běžným	běžný	k2eAgNnSc7d1	běžné
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
knižní	knižní	k2eAgNnSc1d1	knižní
psaní	psaní	k1gNnSc1	psaní
–	–	k?	–
nejznámější	známý	k2eAgInSc1d3	nejznámější
grafický	grafický	k2eAgInSc1d1	grafický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
knižních	knižní	k2eAgFnPc6d1	knižní
písemných	písemný	k2eAgFnPc6d1	písemná
památkách	památka	k1gFnPc6	památka
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Výběr	výběr	k1gInSc1	výběr
písmen	písmeno	k1gNnPc2	písmeno
zde	zde	k6eAd1	zde
podléhá	podléhat	k5eAaImIp3nS	podléhat
přísným	přísný	k2eAgNnPc3d1	přísné
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
variace	variace	k1gFnPc4	variace
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
způsoby	způsob	k1gInPc4	způsob
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
běžné	běžný	k2eAgNnSc1d1	běžné
psaní	psaní	k1gNnSc1	psaní
–	–	k?	–
používané	používaný	k2eAgNnSc1d1	používané
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
pro	pro	k7c4	pro
neformální	formální	k2eNgFnSc4d1	neformální
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
tohoto	tento	k3xDgMnSc2	tento
psaní	psaní	k1gNnSc3	psaní
jsou	být	k5eAaImIp3nP	být
páry	pár	k1gInPc1	pár
o	o	k7c4	o
a	a	k8xC	a
ъ	ъ	k?	ъ
resp.	resp.	kA	resp.
є	є	k?	є
a	a	k8xC	a
ь	ь	k?	ь
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
rovnocenné	rovnocenný	k2eAgFnPc4d1	rovnocenná
varianty	varianta	k1gFnPc4	varianta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
jejich	jejich	k3xOp3gFnSc1	jejich
libovolná	libovolný	k2eAgFnSc1d1	libovolná
záměna	záměna	k1gFnSc1	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
jať	jať	k1gNnSc2	jať
ѣ	ѣ	k?	ѣ
lze	lze	k6eAd1	lze
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
taktéž	taktéž	k?	taktéž
zaměnit	zaměnit	k5eAaPmF	zaměnit
za	za	k7c4	za
є	є	k?	є
(	(	kIx(	(
<g/>
či	či	k8xC	či
ь	ь	k?	ь
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
volnost	volnost	k1gFnSc1	volnost
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
dopisy	dopis	k1gInPc4	dopis
na	na	k7c6	na
březové	březový	k2eAgFnSc6d1	Březová
kůře	kůra	k1gFnSc6	kůra
od	od	k7c2	od
knižních	knižní	k2eAgInPc2d1	knižní
dokumentů	dokument	k1gInPc2	dokument
staré	starý	k2eAgFnSc2d1	stará
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
záznamem	záznam	k1gInSc7	záznam
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
Gnězdovský	Gnězdovský	k2eAgInSc1d1	Gnězdovský
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
datovat	datovat	k5eAaImF	datovat
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
na	na	k7c6	na
džbánu	džbán	k1gInSc6	džbán
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
cyrilský	cyrilský	k2eAgInSc1d1	cyrilský
nápis	nápis	k1gInSc1	nápis
г	г	k?	г
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
čtení	čtení	k1gNnSc1	čtení
i	i	k8xC	i
interpretace	interpretace	k1gFnSc1	interpretace
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hořčici	hořčice	k1gFnSc4	hořčice
či	či	k8xC	či
hořlavinu	hořlavina	k1gFnSc4	hořlavina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
vlastníka	vlastník	k1gMnSc2	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nález	nález	k1gInSc1	nález
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
písmo	písmo	k1gNnSc1	písmo
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
nacházelo	nacházet	k5eAaImAgNnS	nacházet
mnohostranné	mnohostranný	k2eAgNnSc1d1	mnohostranné
využití	využití	k1gNnSc1	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
praslovanský	praslovanský	k2eAgInSc1d1	praslovanský
zákon	zákon	k1gInSc1	zákon
otevřených	otevřený	k2eAgFnPc2d1	otevřená
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
existenci	existence	k1gFnSc4	existence
pouze	pouze	k6eAd1	pouze
těch	ten	k3xDgFnPc2	ten
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončit	k5eAaPmNgFnP	zakončit
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
zákon	zákon	k1gInSc1	zákon
dal	dát	k5eAaPmAgInS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
tzv.	tzv.	kA	tzv.
plnohlasí	plnohlasí	k1gNnSc4	plnohlasí
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
změně	změna	k1gFnSc6	změna
*	*	kIx~	*
<g/>
or	or	k?	or
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ol	ol	k?	ol
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
er	er	k?	er
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
el	ela	k1gFnPc2	ela
→	→	k?	→
oro	oro	k?	oro
<g/>
,	,	kIx,	,
olo	olo	k?	olo
<g/>
,	,	kIx,	,
ere	ere	k?	ere
<g/>
,	,	kIx,	,
olo	olo	k?	olo
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
mezi	mezi	k7c7	mezi
souhláskami	souhláska	k1gFnPc7	souhláska
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
korva	korva	k6eAd1	korva
→	→	k?	→
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
plnohlasí	plnohlasí	k1gNnPc1	plnohlasí
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
ještě	ještě	k9	ještě
nestačilo	stačit	k5eNaBmAgNnS	stačit
plně	plně	k6eAd1	plně
zformovat	zformovat	k5eAaPmF	zformovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgInSc1d1	pozdní
vznik	vznik	k1gInSc1	vznik
plnohlasí	plnohlasí	k1gNnSc2	plnohlasí
dnes	dnes	k6eAd1	dnes
připomínají	připomínat	k5eAaImIp3nP	připomínat
především	především	k9	především
nářeční	nářeční	k2eAgNnPc1d1	nářeční
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některá	některý	k3yIgNnPc1	některý
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgNnP	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
i	i	k9	i
ve	v	k7c6	v
spisovném	spisovný	k2eAgInSc6d1	spisovný
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
п	п	k?	п
<g/>
́	́	k?	́
<g/>
л	л	k?	л
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
před	před	k7c7	před
souhláskou	souhláska	k1gFnSc7	souhláska
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
působil	působit	k5eAaImAgInS	působit
změnu	změna	k1gFnSc4	změna
náslovného	náslovný	k2eAgMnSc2d1	náslovný
*	*	kIx~	*
<g/>
or	or	k?	or
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ol	ol	k?	ol
s	s	k7c7	s
intonací	intonace	k1gFnSc7	intonace
akutovou	akutový	k2eAgFnSc7d1	akutový
→	→	k?	→
ra	ra	k0	ra
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
ò	ò	k?	ò
→	→	k?	→
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
р	р	k?	р
<g/>
́	́	k?	́
<g/>
л	л	k?	л
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
*	*	kIx~	*
<g/>
or	or	k?	or
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ol	ol	k?	ol
s	s	k7c7	s
intonací	intonace	k1gFnSc7	intonace
cirkumflexovou	cirkumflexový	k2eAgFnSc7d1	cirkumflexový
→	→	k?	→
ro	ro	k?	ro
<g/>
,	,	kIx,	,
lo	lo	k?	lo
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
ȍ	ȍ	k?	ȍ
→	→	k?	→
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
ˉ	ˉ	k?	ˉ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východoslovanské	východoslovanský	k2eAgFnSc6d1	východoslovanská
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
temné	temný	k2eAgNnSc1d1	temné
/	/	kIx~	/
<g/>
ɫ	ɫ	k?	ɫ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gFnSc6	jehož
existenci	existence	k1gFnSc6	existence
svědčí	svědčit	k5eAaImIp3nS	svědčit
změna	změna	k1gFnSc1	změna
kvality	kvalita	k1gFnSc2	kvalita
samohlásky	samohláska	k1gFnSc2	samohláska
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
*	*	kIx~	*
<g/>
Tь	Tь	k1gFnSc1	Tь
→	→	k?	→
Tъ	Tъ	k1gFnPc2	Tъ
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
motivovaný	motivovaný	k2eAgInSc4d1	motivovaný
rozvoj	rozvoj	k1gInSc4	rozvoj
plnohlasí	plnohlasí	k1gNnSc2	plnohlasí
*	*	kIx~	*
<g/>
TelT	TelT	k1gFnSc1	TelT
→	→	k?	→
*	*	kIx~	*
<g/>
TolT	TolT	k1gMnSc1	TolT
→	→	k?	→
ToloT	ToloT	k1gMnSc1	ToloT
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
souhláskami	souhláska	k1gFnPc7	souhláska
chyběl	chybět	k5eAaImAgInS	chybět
zvuk	zvuk	k1gInSc1	zvuk
/	/	kIx~	/
<g/>
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
a	a	k8xC	a
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
byl	být	k5eAaImAgInS	být
zaměňován	zaměňovat	k5eAaImNgInS	zaměňovat
za	za	k7c4	za
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
/	/	kIx~	/
<g/>
x	x	k?	x
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
bylo	být	k5eAaImAgNnS	být
labiodentální	labiodentální	k2eAgNnSc1d1	labiodentální
/	/	kIx~	/
<g/>
v	v	k7c4	v
<g/>
/	/	kIx~	/
ve	v	k7c6	v
výpůjčkách	výpůjčka	k1gFnPc6	výpůjčka
často	často	k6eAd1	často
nahrazováno	nahrazovat	k5eAaImNgNnS	nahrazovat
hláskou	hláska	k1gFnSc7	hláska
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
souhlásky	souhláska	k1gFnPc1	souhláska
rozlišovaly	rozlišovat	k5eAaImAgFnP	rozlišovat
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgFnPc1d1	měkká
a	a	k8xC	a
poloměkké	poloměkký	k2eAgFnPc1d1	poloměkká
<g/>
.	.	kIx.	.
</s>
<s>
Poloměkce	Poloměkce	k1gFnPc1	Poloměkce
se	se	k3xPyFc4	se
vyslovovaly	vyslovovat	k5eAaImAgFnP	vyslovovat
původní	původní	k2eAgFnPc1d1	původní
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
souhlásky	souhláska	k1gFnPc1	souhláska
před	před	k7c7	před
předními	přední	k2eAgFnPc7d1	přední
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
splynutí	splynutí	k1gNnSc3	splynutí
poloměkkých	poloměkký	k2eAgFnPc2d1	poloměkká
souhlásek	souhláska	k1gFnPc2	souhláska
s	s	k7c7	s
měkkými	měkký	k2eAgMnPc7d1	měkký
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
byly	být	k5eAaImAgFnP	být
veláry	velára	k1gFnPc1	velára
/	/	kIx~	/
<g/>
k	k	k7c3	k
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
x	x	k?	x
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
alveoláry	alveolára	k1gFnSc2	alveolára
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
d	d	k?	d
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
změkčené	změkčený	k2eAgInPc1d1	změkčený
protějšky	protějšek	k1gInPc1	protějšek
podlehly	podlehnout	k5eAaPmAgInP	podlehnout
změně	změna	k1gFnSc3	změna
*	*	kIx~	*
<g/>
tj	tj	kA	tj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dj	dj	k?	dj
→	→	k?	→
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
ž	ž	k?	ž
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
a	a	k8xC	a
labiály	labiála	k1gFnPc1	labiála
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
w	w	k?	w
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejichž	jejichž	k3xOyRp3gFnPc2	jejichž
měkkých	měkký	k2eAgFnPc2d1	měkká
variant	varianta	k1gFnPc2	varianta
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
tzv.	tzv.	kA	tzv.
epentetické	epentetický	k2eAgInPc1d1	epentetický
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
:	:	kIx,	:
*	*	kIx~	*
<g/>
pj	pj	k?	pj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
bj	bj	k?	bj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
vj	vj	k?	vj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
mj	mj	kA	mj
→	→	k?	→
pľ	pľ	k?	pľ
<g/>
,	,	kIx,	,
bľ	bľ	k?	bľ
<g/>
,	,	kIx,	,
vľ	vľ	k?	vľ
<g/>
,	,	kIx,	,
mľ	mľ	k?	mľ
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgNnSc4d1	měkké
bez	bez	k7c2	bez
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
protějšku	protějšek	k1gInSc2	protějšek
byly	být	k5eAaImAgFnP	být
souhlásky	souhláska	k1gFnPc1	souhláska
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
ž	ž	k?	ž
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Párovou	párový	k2eAgFnSc7d1	párová
měkkostí	měkkost	k1gFnSc7	měkkost
byly	být	k5eAaImAgFnP	být
spjaty	spjat	k2eAgFnPc1d1	spjata
alveoláry	alveolára	k1gFnPc1	alveolára
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
z	z	k7c2	z
<g/>
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
<g/>
z	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
ɫ	ɫ	k?	ɫ
<g/>
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
<g/>
r	r	kA	r
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
souhlásek	souhláska	k1gFnPc2	souhláska
staroruského	staroruský	k2eAgInSc2d1	staroruský
jazyka	jazyk	k1gInSc2	jazyk
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
lze	lze	k6eAd1	lze
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
1	[number]	k4	1
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
z	z	k7c2	z
raženého	ražený	k2eAgInSc2d1	ražený
ražené	ražený	k2eAgInPc1d1	ražený
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
frikativní	frikativní	k2eAgMnSc1d1	frikativní
/	/	kIx~	/
<g/>
ɣ	ɣ	k?	ɣ
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
odlišnost	odlišnost	k1gFnSc1	odlišnost
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
dialektálních	dialektální	k2eAgInPc2d1	dialektální
rysů	rys	k1gInPc2	rys
výchoslovanských	výchoslovanský	k2eAgNnPc2d1	výchoslovanský
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
2	[number]	k4	2
Tato	tento	k3xDgFnSc1	tento
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
/	/	kIx~	/
<g/>
žd	žd	k?	žd
<g/>
͡	͡	k?	͡
<g/>
ž	ž	k?	ž
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
zachycené	zachycený	k2eAgNnSc4d1	zachycené
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
jako	jako	k8xC	jako
ж	ж	k?	ж
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
fonologickou	fonologický	k2eAgFnSc7d1	fonologická
inovací	inovace	k1gFnSc7	inovace
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgMnS	být
zánik	zánik	k1gInSc4	zánik
opozice	opozice	k1gFnSc2	opozice
krátkých	krátká	k1gFnPc2	krátká
a	a	k8xC	a
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
však	však	k9	však
již	již	k6eAd1	již
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc4	žádný
věrohodné	věrohodný	k2eAgInPc4d1	věrohodný
doklady	doklad	k1gInPc4	doklad
existence	existence	k1gFnSc2	existence
této	tento	k3xDgFnSc2	tento
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předhistorickém	předhistorický	k2eAgNnSc6d1	předhistorické
období	období	k1gNnSc6	období
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
nosovky	nosovka	k1gFnPc1	nosovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nabyly	nabýt	k5eAaPmAgFnP	nabýt
nové	nový	k2eAgFnPc4d1	nová
hláskové	hláskový	k2eAgFnPc4d1	hlásková
kvality	kvalita	k1gFnPc4	kvalita
ѫ	ѫ	k?	ѫ
/	/	kIx~	/
<g/>
ǫ	ǫ	k?	ǫ
<g/>
/	/	kIx~	/
→	→	k?	→
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
ѧ	ѧ	k?	ѧ
/	/	kIx~	/
<g/>
ę	ę	k?	ę
<g/>
/	/	kIx~	/
→	→	k?	→
/	/	kIx~	/
<g/>
ä	ä	k?	ä
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
východoslovanských	východoslovanský	k2eAgFnPc6d1	východoslovanská
literárních	literární	k2eAgFnPc6d1	literární
památkách	památka	k1gFnPc6	památka
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
běžná	běžný	k2eAgFnSc1d1	běžná
záměna	záměna	k1gFnSc1	záměna
písmen	písmeno	k1gNnPc2	písmeno
ѫ	ѫ	k?	ѫ
a	a	k8xC	a
ѹ	ѹ	k?	ѹ
resp.	resp.	kA	resp.
ѧ	ѧ	k?	ѧ
a	a	k8xC	a
ı	ı	k?	ı
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
omezené	omezený	k2eAgFnSc2d1	omezená
skupiny	skupina	k1gFnSc2	skupina
slov	slovo	k1gNnPc2	slovo
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
změna	změna	k1gFnSc1	změna
náslovného	náslovný	k2eAgNnSc2d1	náslovné
*	*	kIx~	*
<g/>
(	(	kIx(	(
<g/>
j	j	k?	j
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
→	→	k?	→
о	о	k?	о
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
jezero	jezero	k1gNnSc1	jezero
→	→	k?	→
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
také	také	k9	také
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jména	jméno	k1gNnPc4	jméno
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
stsev	stsev	k1gFnSc1	stsev
<g/>
.	.	kIx.	.
</s>
<s>
Helga	Helga	k1gFnSc1	Helga
→	→	k?	→
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanské	praslovanský	k2eAgFnPc1d1	praslovanská
samohlásky	samohláska	k1gFnPc1	samohláska
*	*	kIx~	*
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ъ	ъ	k?	ъ
resp.	resp.	kA	resp.
*	*	kIx~	*
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ь	ь	k?	ь
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
hlásky	hlásek	k1gInPc1	hlásek
-j-	-	k?	-j-
splynuly	splynout	k5eAaPmAgInP	splynout
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
tzv.	tzv.	kA	tzv.
napjaté	napjatý	k2eAgInPc1d1	napjatý
jery	jer	k1gInPc1	jer
*	*	kIx~	*
<g/>
ъ	ъ	k?	ъ
<g/>
̂	̂	k?	̂
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ь	ь	k?	ь
<g/>
̂	̂	k?	̂
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
staroruských	staroruský	k2eAgInPc6d1	staroruský
dialektech	dialekt	k1gInPc6	dialekt
dále	daleko	k6eAd2	daleko
<g />
.	.	kIx.	.
</s>
<s>
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
*	*	kIx~	*
<g/>
yj	yj	k?	yj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ъ	ъ	k?	ъ
→	→	k?	→
*	*	kIx~	*
<g/>
ъ	ъ	k?	ъ
<g/>
̂	̂	k?	̂
<g/>
j	j	k?	j
→	→	k?	→
dial.	dial.	k?	dial.
yj	yj	k?	yj
<g/>
,	,	kIx,	,
oj	oj	k1gInSc1	oj
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
moldъ	moldъ	k?	moldъ
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
myjǫ	myjǫ	k?	myjǫ
→	→	k?	→
ukr	ukr	k?	ukr
<g/>
.	.	kIx.	.
м	м	k?	м
<g/>
́	́	k?	́
<g />
.	.	kIx.	.
</s>
<s>
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
́	́	k?	́
<g/>
ю	ю	k?	ю
×	×	k?	×
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
м	м	k?	м
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
́	́	k?	́
<g/>
ю	ю	k?	ю
×	×	k?	×
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
dial.	dial.	k?	dial.
м	м	k?	м
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
́	́	k?	́
<g/>
ю	ю	k?	ю
(	(	kIx(	(
<g/>
Pskov	Pskov	k1gInSc1	Pskov
<g/>
))	))	k?	))
*	*	kIx~	*
<g/>
ij	ij	k?	ij
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ь	ь	k?	ь
→	→	k?	→
*	*	kIx~	*
<g/>
ь	ь	k?	ь
<g/>
̂	̂	k?	̂
<g/>
j	j	k?	j
→	→	k?	→
dial.	dial.	k?	dial.
ij	ij	k?	ij
<g/>
,	,	kIx,	,
ej	ej	k0	ej
<g/>
,	,	kIx,	,
ь	ь	k?	ь
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
šija	šija	k1gFnSc1	šija
→	→	k?	→
ukr	ukr	k?	ukr
<g/>
.	.	kIx.	.
ш	ш	k?	ш
<g/>
́	́	k?	́
<g/>
я	я	k?	я
×	×	k?	×
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
ш	ш	k?	ш
<g/>
́	́	k?	́
<g/>
я	я	k?	я
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dial.	dial.	k?	dial.
ш	ш	k?	ш
(	(	kIx(	(
<g/>
Žití	žití	k1gNnSc1	žití
sv.	sv.	kA	sv.
Nifonta	Nifont	k1gInSc2	Nifont
<g/>
))	))	k?	))
*	*	kIx~	*
<g/>
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
jь	jь	k?	jь
→	→	k?	→
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
̂	̂	k?	̂
→	→	k?	→
dial.	dial.	k?	dial.
jь	jь	k?	jь
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
psl	psl	k?	psl
<g/>
.	.	kIx.	.
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
5	[number]	k4	5
ajь	ajь	k?	ajь
→	→	k?	→
ukr	ukr	k?	ukr
<g/>
.	.	kIx.	.
й	й	k?	й
<g/>
,	,	kIx,	,
5	[number]	k4	5
я	я	k?	я
<g/>
́	́	k?	́
<g/>
ц	ц	k?	ц
×	×	k?	×
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
и	и	k?	и
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
5	[number]	k4	5
я	я	k?	я
<g/>
́	́	k?	́
<g/>
ц	ц	k?	ц
<g/>
)	)	kIx)	)
Změkčení	změkčení	k1gNnSc1	změkčení
poloměkkých	poloměkký	k2eAgFnPc2d1	poloměkká
souhlásek	souhláska	k1gFnPc2	souhláska
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
těsné	těsný	k2eAgNnSc1d1	těsné
sblížení	sblížení	k1gNnSc1	sblížení
barvy	barva	k1gFnSc2	barva
souhlásky	souhláska	k1gFnSc2	souhláska
a	a	k8xC	a
samohlásky	samohláska	k1gFnSc2	samohláska
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
slabiky	slabika	k1gFnSc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Protiklad	protiklad	k1gInSc1	protiklad
předního	přední	k2eAgMnSc2d1	přední
a	a	k8xC	a
nepředního	přední	k2eNgNnSc2d1	přední
místa	místo	k1gNnSc2	místo
artikulace	artikulace	k1gFnSc2	artikulace
samohlásek	samohláska	k1gFnPc2	samohláska
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
opozici	opozice	k1gFnSc4	opozice
předních	přední	k2eAgFnPc2d1	přední
a	a	k8xC	a
nepředních	přední	k2eNgFnPc2d1	přední
slabik	slabika	k1gFnPc2	slabika
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
*	*	kIx~	*
<g/>
ťi	ťi	k?	ťi
~	~	kIx~	~
*	*	kIx~	*
<g/>
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ťü	ťü	k?	ťü
~	~	kIx~	~
*	*	kIx~	*
<g/>
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ťe	ťe	k?	ťe
~	~	kIx~	~
*	*	kIx~	*
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ťь	ťь	k?	ťь
~	~	kIx~	~
*	*	kIx~	*
<g/>
tъ	tъ	k?	tъ
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ťä	ťä	k?	ťä
~	~	kIx~	~
*	*	kIx~	*
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Inventář	inventář	k1gInSc1	inventář
samohlásek	samohláska	k1gFnPc2	samohláska
staroruského	staroruský	k2eAgInSc2d1	staroruský
jazyka	jazyk	k1gInSc2	jazyk
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
odpovídající	odpovídající	k2eAgInPc4d1	odpovídající
grafémy	grafém	k1gInPc4	grafém
knižního	knižní	k2eAgInSc2d1	knižní
typu	typ	k1gInSc2	typ
lze	lze	k6eAd1	lze
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
1	[number]	k4	1
Na	na	k7c4	na
výslovnost	výslovnost	k1gFnSc4	výslovnost
blízkou	blízký	k2eAgFnSc4d1	blízká
k	k	k7c3	k
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
baltofinské	baltofinský	k2eAgFnPc1d1	baltofinský
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
(	(	kIx(	(
<g/>
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
к	к	k?	к
→	→	k?	→
fin.	fin.	k?	fin.
risti	risť	k1gFnSc2	risť
<g/>
;	;	kIx,	;
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
б	б	k?	б
→	→	k?	→
fin.	fin.	k?	fin.
pulvana	pulvan	k1gMnSc2	pulvan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
raného	raný	k2eAgNnSc2d1	rané
a	a	k8xC	a
pozdního	pozdní	k2eAgNnSc2d1	pozdní
období	období	k1gNnSc2	období
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vokalizaci	vokalizace	k1gFnSc3	vokalizace
jerů	jer	k1gInPc2	jer
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičního	tradiční	k2eAgNnSc2d1	tradiční
schématu	schéma	k1gNnSc2	schéma
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
sudé	sudý	k2eAgNnSc1d1	sudé
jery	jery	k1gNnSc1	jery
ъ	ъ	k?	ъ
→	→	k?	→
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
resp.	resp.	kA	resp.
ь	ь	k?	ь
→	→	k?	→
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
2	[number]	k4	2
Realizace	realizace	k1gFnSc1	realizace
písmene	písmeno	k1gNnSc2	písmeno
jať	jať	k1gNnPc2	jať
ѣ	ѣ	k?	ѣ
byla	být	k5eAaImAgFnS	být
úzká	úzký	k2eAgFnSc1d1	úzká
/	/	kIx~	/
<g/>
ê	ê	k?	ê
<g/>
/	/	kIx~	/
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
východoslovanského	východoslovanský	k2eAgNnSc2d1	východoslovanské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
pozdním	pozdní	k2eAgNnSc7d1	pozdní
obdobím	období	k1gNnSc7	období
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dialektech	dialekt	k1gInPc6	dialekt
dále	daleko	k6eAd2	daleko
zúžila	zúžit	k5eAaPmAgFnS	zúžit
na	na	k7c4	na
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
části	část	k1gFnSc6	část
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
dialektů	dialekt	k1gInPc2	dialekt
naopak	naopak	k6eAd1	naopak
existovala	existovat	k5eAaImAgFnS	existovat
široká	široký	k2eAgFnSc1d1	široká
realizace	realizace	k1gFnSc1	realizace
/	/	kIx~	/
<g/>
ä	ä	k?	ä
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
baltofinských	baltofinský	k2eAgFnPc6d1	baltofinský
přejímkách	přejímka	k1gFnPc6	přejímka
(	(	kIx(	(
<g/>
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
г	г	k?	г
→	→	k?	→
fin.	fin.	k?	fin.
räähka	räähek	k1gInSc2	räähek
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
moderních	moderní	k2eAgNnPc6d1	moderní
ruských	ruský	k2eAgNnPc6d1	ruské
nářečích	nářečí	k1gNnPc6	nářečí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
я	я	k?	я
"	"	kIx"	"
<g/>
jedla	jíst	k5eAaImAgFnS	jíst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
к	к	k?	к
"	"	kIx"	"
<g/>
cep	cep	k1gInSc1	cep
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
pod	pod	k7c7	pod
autonomním	autonomní	k2eAgInSc7d1	autonomní
přízvukem	přízvuk	k1gInSc7	přízvuk
či	či	k8xC	či
v	v	k7c6	v
zavřené	zavřený	k2eAgFnSc6d1	zavřená
slabice	slabika	k1gFnSc6	slabika
úží	úžit	k5eAaImIp3nS	úžit
v	v	k7c6	v
/	/	kIx~	/
<g/>
ô	ô	k?	ô
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
literárních	literární	k2eAgFnPc6d1	literární
památkách	památka	k1gFnPc6	památka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
hláska	hláska	k1gFnSc1	hláska
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
pomocí	pomocí	k7c2	pomocí
písmene	písmeno	k1gNnSc2	písmeno
omega	omega	k1gFnSc1	omega
ѡ	ѡ	k?	ѡ
Nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
fonetické	fonetický	k2eAgFnPc4d1	fonetická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
chronologicky	chronologicky	k6eAd1	chronologicky
níže	nízce	k6eAd2	nízce
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
zánik	zánik	k1gInSc4	zánik
nosovek	nosovka	k1gFnPc2	nosovka
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
změkčení	změkčení	k1gNnSc4	změkčení
poloměkkých	poloměkký	k2eAgInPc2d1	poloměkký
souhlásek1	souhlásek1	k4	souhlásek1
~	~	kIx~	~
pol.	pol.	k?	pol.
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
vokalizace	vokalizace	k1gFnSc1	vokalizace
<g />
.	.	kIx.	.
</s>
<s>
jerů	jer	k1gInPc2	jer
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
změna	změna	k1gFnSc1	změna
/	/	kIx~	/
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
→	→	k?	→
/	/	kIx~	/
<g/>
'	'	kIx"	'
<g/>
o	o	k7c4	o
<g/>
/	/	kIx~	/
před	před	k7c7	před
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
souhláskou	souhláska	k1gFnSc7	souhláska
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
změkčení	změkčení	k1gNnSc1	změkčení
velár	velára	k1gFnPc2	velára
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
vznik	vznik	k1gInSc1	vznik
akání2	akání2	k4	akání2
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
-	-	kIx~	-
ztvrdnutí	ztvrdnutí	k1gNnSc1	ztvrdnutí
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
ž	ž	k?	ž
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
1	[number]	k4	1
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
Rusi	Rus	k1gFnSc2	Rus
existovaly	existovat	k5eAaImAgInP	existovat
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
se	se	k3xPyFc4	se
změkčení	změkčení	k1gNnSc1	změkčení
vyhnulo	vyhnout	k5eAaPmAgNnS	vyhnout
<g/>
.	.	kIx.	.
2	[number]	k4	2
Akání	Akání	k1gNnPc2	Akání
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
přízvuků	přízvuk	k1gInPc2	přízvuk
rané	raný	k2eAgFnSc2d1	raná
fáze	fáze	k1gFnSc2	fáze
staroruštiny	staroruština	k1gFnSc2	staroruština
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
podobný	podobný	k2eAgInSc1d1	podobný
systému	systém	k1gInSc3	systém
praslovanskému	praslovanský	k2eAgInSc3d1	praslovanský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
tři	tři	k4xCgFnPc4	tři
slabičné	slabičný	k2eAgFnPc4d1	slabičná
intonace	intonace	k1gFnPc4	intonace
(	(	kIx(	(
<g/>
akut	akut	k1gInSc1	akut
<g/>
,	,	kIx,	,
neoakut	neoakut	k1gInSc1	neoakut
<g/>
,	,	kIx,	,
cirkumflex	cirkumflex	k1gInSc1	cirkumflex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
však	však	k9	však
na	na	k7c6	na
východoslovanské	východoslovanský	k2eAgFnSc6d1	východoslovanská
půdě	půda	k1gFnSc6	půda
neexistovala	existovat	k5eNaImAgFnS	existovat
fonologická	fonologický	k2eAgFnSc1d1	fonologická
opozice	opozice	k1gFnSc1	opozice
akut	akut	k1gInSc1	akut
<g/>
–	–	k?	–
<g/>
neoakut	neoakut	k1gInSc1	neoakut
a	a	k8xC	a
systém	systém	k1gInSc1	systém
tak	tak	k6eAd1	tak
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
autonomní	autonomní	k2eAgInSc1d1	autonomní
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
akutu	akut	k1gInSc2	akut
i	i	k8xC	i
neoakutu	neoakut	k1gInSc2	neoakut
a	a	k8xC	a
automatický	automatický	k2eAgInSc4d1	automatický
přízvuk	přízvuk	k1gInSc4	přízvuk
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
cirkumflexu	cirkumflex	k1gInSc3	cirkumflex
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fonologického	fonologický	k2eAgNnSc2d1	fonologické
hlediska	hledisko	k1gNnSc2	hledisko
spadaly	spadat	k5eAaPmAgInP	spadat
všechny	všechen	k3xTgFnPc4	všechen
plnovýznamové	plnovýznamový	k2eAgInPc1d1	plnovýznamový
slovní	slovní	k2eAgInPc1d1	slovní
tvary	tvar	k1gInPc1	tvar
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
ortotona	ortotona	k1gFnSc1	ortotona
<g/>
,	,	kIx,	,
provázená	provázený	k2eAgNnPc4d1	provázené
autonomním	autonomní	k2eAgInSc7d1	autonomní
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
,	,	kIx,	,
spjatým	spjatý	k2eAgInSc7d1	spjatý
neodlučitelně	odlučitelně	k6eNd1	odlučitelně
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
slabik	slabika	k1gFnPc2	slabika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
г	г	k?	г
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
enklinomena	enklinomen	k2eAgFnSc1d1	enklinomen
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vlastní	vlastní	k2eAgInSc4d1	vlastní
přízvuk	přízvuk	k1gInSc4	přízvuk
neměla	mít	k5eNaImAgFnS	mít
a	a	k8xC	a
v	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
pozici	pozice	k1gFnSc6	pozice
nabývala	nabývat	k5eAaImAgFnS	nabývat
určitého	určitý	k2eAgNnSc2d1	určité
prozodického	prozodický	k2eAgNnSc2d1	prozodický
zesílení	zesílení	k1gNnSc2	zesílení
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
realizace	realizace	k1gFnSc1	realizace
fonetického	fonetický	k2eAgNnSc2d1	fonetické
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
od	od	k7c2	od
autonomního	autonomní	k2eAgInSc2d1	autonomní
přízvuku	přízvuk	k1gInSc2	přízvuk
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
lišila	lišit	k5eAaImAgFnS	lišit
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
automatický	automatický	k2eAgInSc1d1	automatický
přízvuk	přízvuk	k1gInSc1	přízvuk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ˉ	ˉ	k?	ˉ
<g/>
,	,	kIx,	,
ˉ	ˉ	k?	ˉ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
ohebných	ohebný	k2eAgNnPc2d1	ohebné
slov	slovo	k1gNnPc2	slovo
lze	lze	k6eAd1	lze
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uvedeným	uvedený	k2eAgInSc7d1	uvedený
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
akcentuační	akcentuační	k2eAgNnPc4d1	akcentuační
paradigmata	paradigma	k1gNnPc4	paradigma
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
ortotonické	ortotonický	k2eAgInPc1d1	ortotonický
tvary	tvar	k1gInPc1	tvar
se	s	k7c7	s
stálým	stálý	k2eAgInSc7d1	stálý
přízvukem	přízvuk	k1gInSc7	přízvuk
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
v	v	k7c6	v
odvozených	odvozený	k2eAgNnPc6d1	odvozené
slovech	slovo	k1gNnPc6	slovo
<g/>
:	:	kIx,	:
с	с	k?	с
<g/>
́	́	k?	́
<g/>
<g />
.	.	kIx.	.
</s>
<s>
т	т	k?	т
-а	-а	k?	-а
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
́	́	k?	́
<g/>
т	т	k?	т
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
с	с	k?	с
<g/>
́	́	k?	́
<g/>
т	т	k?	т
с	с	k?	с
в	в	k?	в
<g/>
́	́	k?	́
<g/>
р	р	k?	р
-ъ	-ъ	k?	-ъ
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
́	́	k?	́
<g/>
р	р	k?	р
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
в	в	k?	в
<g/>
́	́	k?	́
<g/>
р	р	k?	р
ч	ч	k?	ч
<g/>
́	́	k?	́
<g/>
д	д	k?	д
-а	-а	k?	-а
<g/>
,	,	kIx,	,
ч	ч	k?	ч
<g/>
́	́	k?	́
<g />
.	.	kIx.	.
</s>
<s>
<g/>
д	д	k?	д
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
ч	ч	k?	ч
<g/>
́	́	k?	́
<g/>
д	д	k?	д
с	с	k?	с
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
ortotonické	ortotonický	k2eAgInPc1d1	ortotonický
tvary	tvar	k1gInPc1	tvar
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
koncovky	koncovka	k1gFnSc2	koncovka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
slabice	slabika	k1gFnSc6	slabika
kmene	kmen	k1gInSc2	kmen
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
couvnutí	couvnutí	k1gNnSc3	couvnutí
přízvuku	přízvuk	k1gInSc2	přízvuk
z	z	k7c2	z
lichého	lichý	k2eAgInSc2d1	lichý
jeru	jer	k1gInSc2	jer
či	či	k8xC	či
nepočátečního	počáteční	k2eNgInSc2d1	počáteční
vokálu	vokál	k1gInSc2	vokál
s	s	k7c7	s
cirkumflexovou	cirkumflexový	k2eAgFnSc7d1	cirkumflexový
intonací	intonace	k1gFnSc7	intonace
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
к	к	k?	к
<g/>
́	́	k?	́
<g/>
л	л	k?	л
-ѧ	-ѧ	k?	-ѧ
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
́	́	k?	́
<g/>
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
́	́	k?	́
<g/>
в	в	k?	в
ж	ж	k?	ж
<g/>
́	́	k?	́
-ъ	-ъ	k?	-ъ
<g/>
́	́	k?	́
<g/>
ı	ı	k?	ı
<g/>
,	,	kIx,	,
ж	ж	k?	ж
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
,	,	kIx,	,
ж	ж	k?	ж
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
ж	ж	k?	ж
<g/>
́	́	k?	́
<g/>
т	т	k?	т
с	с	k?	с
п	п	k?	п
<g/>
́	́	k?	́
-а	-а	k?	-а
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
́	́	k?	́
<g/>
р	р	k?	р
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
́	́	k?	́
<g/>
р	р	k?	р
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
-	-	kIx~	-
Některé	některý	k3yIgInPc1	některý
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
ortotonické	ortotonický	k2eAgFnPc1d1	ortotonický
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
enklinomena	enklinomen	k2eAgMnSc4d1	enklinomen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
dominance	dominance	k1gFnSc1	dominance
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
morfů	morf	k1gInPc2	morf
<g/>
:	:	kIx,	:
ˉ	ˉ	k?	ˉ
-а	-а	k?	-а
<g/>
,	,	kIx,	,
ˉ	ˉ	k?	ˉ
г	г	k?	г
<g/>
,	,	kIx,	,
г	г	k?	г
<g/>
́	́	k?	́
<g/>
к	к	k?	к
<g/>
,	,	kIx,	,
г	г	k?	г
<g/>
́	́	k?	́
<g/>
ı	ı	k?	ı
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
г	г	k?	г
<g/>
́	́	k?	́
<g/>
т	т	k?	т
в	в	k?	в
<g/>
́	́	k?	́
-ъ	-ъ	k?	-ъ
<g/>
́	́	k?	́
<g/>
ı	ı	k?	ı
<g/>
,	,	kIx,	,
ˉ	ˉ	k?	ˉ
в	в	k?	в
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
́	́	k?	́
<g/>
д	д	k?	д
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
́	́	k?	́
<g/>
ı	ı	k?	ı
ˉ	ˉ	k?	ˉ
-а	-а	k?	-а
<g/>
,	,	kIx,	,
ˉ	ˉ	k?	ˉ
ѹ	ѹ	k?	ѹ
<g/>
,	,	kIx,	,
ѹ	ѹ	k?	ѹ
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
ѹ	ѹ	k?	ѹ
<g/>
́	́	k?	́
<g/>
ı	ı	k?	ı
Zánik	zánik	k1gInSc1	zánik
rozdílnosti	rozdílnost	k1gFnSc2	rozdílnost
autonomního	autonomní	k2eAgInSc2d1	autonomní
a	a	k8xC	a
automatického	automatický	k2eAgInSc2d1	automatický
přízvuku	přízvuk	k1gInSc2	přízvuk
vedl	vést	k5eAaImAgInS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jediného	jediný	k2eAgInSc2d1	jediný
dynamického	dynamický	k2eAgInSc2d1	dynamický
přízvuku	přízvuk	k1gInSc2	přízvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
známe	znát	k5eAaImIp1nP	znát
ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
východoslovanských	východoslovanský	k2eAgInPc2d1	východoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
enklinomen	enklinomen	k2eAgInSc4d1	enklinomen
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
tvary	tvar	k1gInPc1	tvar
s	s	k7c7	s
přízvukem	přízvuk	k1gInSc7	přízvuk
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
fonetického	fonetický	k2eAgNnSc2d1	fonetické
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ˉ	ˉ	k?	ˉ
б	б	k?	б
→	→	k?	→
н	н	k?	н
<g/>
́	́	k?	́
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jakém	jaký	k3yQgInSc6	jaký
období	období	k1gNnSc4	období
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
nastala	nastat	k5eAaPmAgFnS	nastat
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
současně	současně	k6eAd1	současně
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
a	a	k8xC	a
časově	časově	k6eAd1	časově
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
od	od	k7c2	od
zániku	zánik	k1gInSc2	zánik
jerů	jer	k1gInPc2	jer
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
vymezena	vymezit	k5eAaPmNgFnS	vymezit
vznikem	vznik	k1gInSc7	vznik
akání	akání	k2eAgNnSc1d1	akání
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
už	už	k6eAd1	už
se	se	k3xPyFc4	se
původní	původní	k2eAgInPc1d1	původní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
přízvuky	přízvuk	k1gInPc7	přízvuk
neodráží	odrážet	k5eNaImIp3nS	odrážet
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
slovesa	sloveso	k1gNnSc2	sloveso
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
kategorii	kategorie	k1gFnSc4	kategorie
gramatického	gramatický	k2eAgNnSc2d1	gramatické
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
<g/>
,	,	kIx,	,
duál	duál	k1gInSc1	duál
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
moderním	moderní	k2eAgInPc3d1	moderní
východoslovanským	východoslovanský	k2eAgInPc3d1	východoslovanský
jazykům	jazyk	k1gInPc3	jazyk
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
dvojného	dvojný	k2eAgNnSc2d1	dvojné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mizí	mizet	k5eAaImIp3nS	mizet
teprve	teprve	k6eAd1	teprve
počátkem	počátkem	k7c2	počátkem
pozdního	pozdní	k2eAgNnSc2d1	pozdní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
ruština	ruština	k1gFnSc1	ruština
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
trojí	trojí	k4xRgInSc1	trojí
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
maskulinum	maskulinum	k1gNnSc1	maskulinum
<g/>
,	,	kIx,	,
femininum	femininum	k1gNnSc1	femininum
<g/>
,	,	kIx,	,
neutrum	neutrum	k1gNnSc1	neutrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
7	[number]	k4	7
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
<g/>
,	,	kIx,	,
lokativ	lokativ	k1gInSc1	lokativ
<g/>
,	,	kIx,	,
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
zakončení	zakončení	k1gNnSc1	zakončení
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
na	na	k7c6	na
etymologickém	etymologický	k2eAgInSc6d1	etymologický
základě	základ	k1gInSc6	základ
podle	podle	k7c2	podle
starých	starý	k2eAgInPc2d1	starý
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
ā	ā	k?	ā
<g/>
,	,	kIx,	,
o-kmeny	omen	k1gInPc1	o-kmen
<g/>
,	,	kIx,	,
u-kmeny	umen	k1gInPc1	u-kmen
<g/>
,	,	kIx,	,
i-kmeny	imen	k1gInPc1	i-kmen
<g/>
,	,	kIx,	,
konsonantické	konsonantický	k2eAgInPc1d1	konsonantický
kmeny	kmen	k1gInPc1	kmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
deklinačních	deklinační	k2eAgInPc2d1	deklinační
typů	typ	k1gInPc2	typ
substantiv	substantivum	k1gNnPc2	substantivum
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
1	[number]	k4	1
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
první	první	k4xOgFnSc2	první
palatalizace	palatalizace	k1gFnSc2	palatalizace
před	před	k7c7	před
-e	-e	k?	-e
vok.	vok.	k?	vok.
sg.	sg.	k?	sg.
mužských	mužský	k2eAgInPc2d1	mužský
o-kmenů	omen	k1gInPc2	o-kmen
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
kmene	kmen	k1gInSc2	kmen
zakončeného	zakončený	k2eAgInSc2d1	zakončený
velárou	velára	k1gFnSc7	velára
-k-	-	k?	-k-
<g/>
,	,	kIx,	,
<g/>
-g-	-	k?	-g-
<g/>
,	,	kIx,	,
<g/>
-ch-	h-	k?	-ch-
→	→	k?	→
-č-	-č-	k?	-č-
<g/>
,	,	kIx,	,
<g/>
-ž-	-ž-	k?	-ž-
<g/>
,	,	kIx,	,
<g/>
-š-	-š-	k?	-š-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
б	б	k?	б
→	→	k?	→
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
následkem	následkem	k7c2	následkem
druhé	druhý	k4xOgFnSc2	druhý
palatalizace	palatalizace	k1gFnSc2	palatalizace
před	před	k7c7	před
-ě	-ě	k?	-ě
nebo	nebo	k8xC	nebo
-i	-i	k?	-i
diftongického	diftongický	k2eAgInSc2d1	diftongický
původu	původ	k1gInSc2	původ
mění	měnit	k5eAaImIp3nS	měnit
kmen	kmen	k1gInSc1	kmen
zakončený	zakončený	k2eAgInSc1d1	zakončený
-k-	-	k?	-k-
<g/>
,	,	kIx,	,
<g/>
-g-	-	k?	-g-
<g/>
,	,	kIx,	,
<g/>
-ch-	h-	k?	-ch-
→	→	k?	→
-c-	-	k?	-c-
<g/>
,	,	kIx,	,
<g/>
-z-	-	k?	-z-
<g/>
,	,	kIx,	,
<g/>
-s-	-	k?	-s-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
→	→	k?	→
н	н	k?	н
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
Pro	pro	k7c4	pro
o-kmeny	omen	k1gInPc4	o-kmen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
kategorii	kategorie	k1gFnSc4	kategorie
jmen	jméno	k1gNnPc2	jméno
osobních	osobní	k2eAgNnPc2d1	osobní
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
provázejí	provázet	k5eAaImIp3nP	provázet
jisté	jistý	k2eAgFnPc1d1	jistá
tvaroslovné	tvaroslovný	k2eAgFnPc1d1	tvaroslovná
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
písemných	písemný	k2eAgFnPc6d1	písemná
památkách	památka	k1gFnPc6	památka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
projevuje	projevovat	k5eAaImIp3nS	projevovat
splýváním	splývání	k1gNnSc7	splývání
tvaru	tvar	k1gInSc2	tvar
akuz	akuza	k1gFnPc2	akuza
<g/>
.	.	kIx.	.
a	a	k8xC	a
gen.	gen.	kA	gen.
sg.	sg.	k?	sg.
u	u	k7c2	u
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
apelativ	apelativum	k1gNnPc2	apelativum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
označují	označovat	k5eAaImIp3nP	označovat
osobu	osoba	k1gFnSc4	osoba
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
těchto	tento	k3xDgNnPc2	tento
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
nezřídka	nezřídka	k6eAd1	nezřídka
objevuje	objevovat	k5eAaImIp3nS	objevovat
koncovka	koncovka	k1gFnSc1	koncovka
-ovi	v	k1gFnSc2	-ov
(	(	kIx(	(
<g/>
či	či	k8xC	či
-evi	-evi	k6eAd1	-evi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
Konsonantická	konsonantický	k2eAgFnSc1d1	konsonantický
deklinace	deklinace	k1gFnSc1	deklinace
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
označujících	označující	k2eAgFnPc2d1	označující
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
přežívaly	přežívat	k5eAaImAgInP	přežívat
také	také	k9	také
v	v	k7c6	v
gen.	gen.	kA	gen.
pl.	pl.	k?	pl.
maskulin	maskulinum	k1gNnPc2	maskulinum
л	л	k?	л
<g/>
,	,	kIx,	,
н	н	k?	н
<g/>
,	,	kIx,	,
п	п	k?	п
a	a	k8xC	a
д	д	k?	д
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
dial.	dial.	k?	dial.
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
konsonantické	konsonantický	k2eAgInPc1d1	konsonantický
kmeny	kmen	k1gInPc1	kmen
zpravidla	zpravidla	k6eAd1	zpravidla
přizpůsobily	přizpůsobit	k5eAaPmAgInP	přizpůsobit
skloňování	skloňování	k1gNnPc4	skloňování
jiným	jiný	k2eAgInPc3d1	jiný
deklinačním	deklinační	k2eAgInPc3d1	deklinační
typům	typ	k1gInPc3	typ
<g/>
;	;	kIx,	;
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
koncovkou	koncovka	k1gFnSc7	koncovka
gen.	gen.	kA	gen.
a	a	k8xC	a
lok.	lok.	k?	lok.
sg.	sg.	k?	sg.
-e	-e	k?	-e
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
knižních	knižní	k2eAgInPc6d1	knižní
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toponyma	toponymum	k1gNnPc4	toponymum
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
užívání	užívání	k1gNnSc1	užívání
bezpředložkového	bezpředložkový	k2eAgInSc2d1	bezpředložkový
lokativu	lokativ	k1gInSc2	lokativ
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
setrvávání	setrvávání	k1gNnSc2	setrvávání
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
bezpředložkového	bezpředložkový	k2eAgInSc2d1	bezpředložkový
dativu	dativ	k1gInSc2	dativ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
pohybu	pohyb	k1gInSc2	pohyb
dovnitř	dovnitř	k6eAd1	dovnitř
(	(	kIx(	(
<g/>
např.	např.	kA	např.
К	К	k?	К
→	→	k?	→
К	К	k?	К
"	"	kIx"	"
<g/>
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
"	"	kIx"	"
×	×	k?	×
К	К	k?	К
"	"	kIx"	"
<g/>
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předložkové	předložkový	k2eAgFnPc1d1	předložková
vazby	vazba	k1gFnPc1	vazba
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
ā	ā	k?	ā
(	(	kIx(	(
<g/>
např	např	kA	např
Р	Р	k?	Р
→	→	k?	→
в	в	k?	в
Р	Р	k?	Р
×	×	k?	×
в	в	k?	в
Р	Р	k?	Р
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
к	к	k?	к
є	є	k?	є
р	р	k?	р
п	п	k?	п
–	–	k?	–
"	"	kIx"	"
<g/>
Koupil	koupit	k5eAaPmAgInS	koupit
jsi	být	k5eAaImIp2nS	být
nevolnici	nevolnice	k1gFnSc4	nevolnice
ve	v	k7c6	v
Pskově	Pskov	k1gInSc6	Pskov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
109	[number]	k4	109
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
]	]	kIx)	]
ш	ш	k?	ш
т	т	k?	т
є	є	k?	є
к	к	k?	к
–	–	k?	–
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
do	do	k7c2	do
Kučkova	Kučkov	k1gInSc2	Kučkov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
723	[number]	k4	723
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Jména	jméno	k1gNnPc4	jméno
hromadná	hromadný	k2eAgNnPc4d1	hromadné
označující	označující	k2eAgFnSc7d1	označující
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
р	р	k?	р
<g/>
,	,	kIx,	,
г	г	k?	г
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
přísudkem	přísudek	k1gInSc7	přísudek
shodují	shodovat	k5eAaImIp3nP	shodovat
zpravidla	zpravidla	k6eAd1	zpravidla
ad	ad	k7c4	ad
sensum	sensum	k1gInSc4	sensum
(	(	kIx(	(
<g/>
přísudek	přísudek	k1gInSc1	přísudek
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
п	п	k?	п
д	д	k?	д
с	с	k?	с
ч	ч	k?	ч
–	–	k?	–
"	"	kIx"	"
<g/>
Pojede	jet	k5eAaImIp3nS	jet
družina	družina	k1gFnSc1	družina
<g/>
,	,	kIx,	,
Sávovi	Sávův	k2eAgMnPc1d1	Sávův
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
69	[number]	k4	69
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
к	к	k?	к
с	с	k?	с
г	г	k?	г
м	м	k?	м
п	п	k?	п
и	и	k?	и
м	м	k?	м
д	д	k?	д
–	–	k?	–
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
postaráte	postarat	k5eAaPmIp2nP	postarat
o	o	k7c4	o
mne	já	k3xPp1nSc4	já
a	a	k8xC	a
mé	můj	k3xOp1gFnPc4	můj
děti	dítě	k1gFnPc4	dítě
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
49	[number]	k4	49
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Výrazné	výrazný	k2eAgNnSc4d1	výrazné
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
distribuce	distribuce	k1gFnSc2	distribuce
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
deklinační	deklinační	k2eAgInPc4d1	deklinační
typy	typ	k1gInPc4	typ
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
už	už	k9	už
v	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
<g/>
.	.	kIx.	.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
náleží	náležet	k5eAaImIp3nP	náležet
pouze	pouze	k6eAd1	pouze
skupině	skupina	k1gFnSc3	skupina
původních	původní	k2eAgInPc2d1	původní
o-kmenů	omen	k1gInPc2	o-kmen
a	a	k8xC	a
ā	ā	k?	ā
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
tvoření	tvoření	k1gNnSc6	tvoření
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
výhradně	výhradně	k6eAd1	výhradně
hledisko	hledisko	k1gNnSc1	hledisko
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
adjektiv	adjektivum	k1gNnPc2	adjektivum
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
složené	složený	k2eAgInPc1d1	složený
a	a	k8xC	a
jmenné	jmenný	k2eAgInPc1d1	jmenný
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
významový	významový	k2eAgInSc1d1	významový
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
písemnictví	písemnictví	k1gNnSc2	písemnictví
začal	začít	k5eAaPmAgInS	začít
stírat	stírat	k5eAaImF	stírat
<g/>
.	.	kIx.	.
</s>
<s>
Jmenné	jmenný	k2eAgInPc1d1	jmenný
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
téměř	téměř	k6eAd1	téměř
neodlišují	odlišovat	k5eNaImIp3nP	odlišovat
od	od	k7c2	od
podstatných	podstatný	k2eAgFnPc2d1	podstatná
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
morfologickým	morfologický	k2eAgInSc7d1	morfologický
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
nahrazení	nahrazení	k1gNnSc4	nahrazení
původního	původní	k2eAgInSc2d1	původní
tvaru	tvar	k1gInSc2	tvar
vokativu	vokativ	k1gInSc2	vokativ
nominativem	nominativ	k1gInSc7	nominativ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
začíná	začínat	k5eAaImIp3nS	začínat
sbližování	sbližování	k1gNnSc1	sbližování
se	s	k7c7	s
složenými	složený	k2eAgInPc7d1	složený
tvary	tvar	k1gInPc7	tvar
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
pádech	pád	k1gInPc6	pád
kromě	kromě	k7c2	kromě
nom.	nom.	k?	nom.
a	a	k8xC	a
akuz	akuz	k1gInSc1	akuz
<g/>
.	.	kIx.	.
</s>
<s>
Složené	složený	k2eAgInPc1d1	složený
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
o	o	k7c4	o
zájmeno	zájmeno	k1gNnSc4	zájmeno
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
platnost	platnost	k1gFnSc4	platnost
určitého	určitý	k2eAgInSc2d1	určitý
členu	člen	k1gInSc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
prošly	projít	k5eAaPmAgInP	projít
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
skloňování	skloňování	k1gNnSc4	skloňování
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
:	:	kIx,	:
1	[number]	k4	1
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
v	v	k7c6	v
nepřímých	přímý	k2eNgInPc6d1	nepřímý
pádech	pád	k1gInPc6	pád
plurálu	plurál	k1gInSc2	plurál
objevují	objevovat	k5eAaImIp3nP	objevovat
stažené	stažený	k2eAgInPc1d1	stažený
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
<g/>
,	,	kIx,	,
н	н	k?	н
<g/>
,	,	kIx,	,
н	н	k?	н
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komparativ	komparativ	k1gInSc1	komparativ
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
připojením	připojení	k1gNnSc7	připojení
přípony	přípona	k1gFnSc2	přípona
-ějь	-ějь	k?	-ějь
(	(	kIx(	(
<g/>
-ajь	-ajь	k?	-ajь
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
ь	ь	k?	ь
k	k	k7c3	k
adjektivnímu	adjektivní	k2eAgInSc3d1	adjektivní
kmeni	kmen	k1gInSc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
tvar	tvar	k1gInSc4	tvar
nom.	nom.	k?	nom.
<g/>
-akuz	-akuz	k1gInSc1	-akuz
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
mužského	mužský	k2eAgInSc2d1	mužský
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chybí	chybět	k5eAaImIp3nS	chybět
-š	-š	k?	-š
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
<g/>
,	,	kIx,	,
х	х	k?	х
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
komparativu	komparativ	k1gInSc2	komparativ
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
též	též	k9	též
funkci	funkce	k1gFnSc4	funkce
superlativu	superlativ	k1gInSc2	superlativ
<g/>
:	:	kIx,	:
а	а	k?	а
х	х	k?	х
т	т	k?	т
в	в	k?	в
в	в	k?	в
т	т	k?	т
л	л	k?	л
н	н	k?	н
–	–	k?	–
"	"	kIx"	"
<g/>
Hodlám	hodlat	k5eAaImIp1nS	hodlat
kvůli	kvůli	k7c3	kvůli
tobě	ty	k3xPp2nSc3	ty
zabavit	zabavit	k5eAaPmF	zabavit
zboží	zboží	k1gNnSc2	zboží
nejváženějšího	vážený	k2eAgMnSc2d3	nejváženější
Novgoroďana	Novgoroďan	k1gMnSc2	Novgoroďan
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
246	[number]	k4	246
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Staroruská	staroruský	k2eAgNnPc1d1	staroruské
zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
2	[number]	k4	2
velké	velký	k2eAgFnSc2d1	velká
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
bezrodá	bezrodý	k2eAgFnSc1d1	bezrodý
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
rodová	rodový	k2eAgFnSc1d1	rodová
<g/>
,	,	kIx,	,
rozlišující	rozlišující	k2eAgInSc1d1	rozlišující
trojí	trojit	k5eAaImIp3nS	trojit
rod	rod	k1gInSc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnPc4d1	osobní
zájmena	zájmeno	k1gNnPc4	zájmeno
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc1	osoba
a	a	k8xC	a
zájmeno	zájmeno	k1gNnSc1	zájmeno
zvratné	zvratný	k2eAgNnSc1d1	zvratné
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
bezrodá	bezrodý	k2eAgNnPc4d1	bezrodý
zájmena	zájmeno	k1gNnPc4	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
zájmen	zájmeno	k1gNnPc2	zájmeno
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
v	v	k7c6	v
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
a	a	k8xC	a
akuz	akuz	k1gInSc4	akuz
<g/>
.	.	kIx.	.
příklonné	příklonný	k2eAgInPc4d1	příklonný
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgInPc4d1	krátký
<g/>
)	)	kIx)	)
a	a	k8xC	a
plnopřízvučné	plnopřízvučný	k2eAgInPc4d1	plnopřízvučný
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
<g/>
)	)	kIx)	)
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Příklonné	příklonný	k2eAgInPc1d1	příklonný
tvary	tvar	k1gInPc1	tvar
dativu	dativ	k1gInSc2	dativ
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
nepřízvučné	přízvučný	k2eNgFnSc6d1	nepřízvučná
pozici	pozice	k1gFnSc6	pozice
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
tvary	tvar	k1gInPc1	tvar
při	při	k7c6	při
logickém	logický	k2eAgInSc6d1	logický
důrazu	důraz	k1gInSc6	důraz
nebo	nebo	k8xC	nebo
po	po	k7c6	po
předložce	předložka	k1gFnSc6	předložka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
krátkých	krátký	k2eAgInPc2d1	krátký
tvarů	tvar	k1gInPc2	tvar
dativních	dativní	k2eAgInPc2d1	dativní
se	se	k3xPyFc4	se
krátké	krátký	k2eAgInPc1d1	krátký
akuzativní	akuzativní	k2eAgInPc1d1	akuzativní
tvary	tvar	k1gInPc1	tvar
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
po	po	k7c6	po
předložce	předložka	k1gFnSc6	předložka
<g/>
.	.	kIx.	.
</s>
<s>
Původními	původní	k2eAgInPc7d1	původní
akuzativními	akuzativní	k2eAgInPc7d1	akuzativní
tvary	tvar	k1gInPc7	tvar
byly	být	k5eAaImAgFnP	být
totiž	totiž	k9	totiž
tvary	tvar	k1gInPc4	tvar
krátké	krátký	k2eAgInPc4d1	krátký
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
i	i	k9	i
bez	bez	k7c2	bez
důrazu	důraz	k1gInSc2	důraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
předliterárním	předliterární	k2eAgNnSc6d1	předliterární
období	období	k1gNnSc6	období
začaly	začít	k5eAaPmAgFnP	začít
do	do	k7c2	do
akuzativu	akuzativ	k1gInSc2	akuzativ
pronikat	pronikat	k5eAaImF	pronikat
tvary	tvar	k1gInPc4	tvar
genitivu	genitiv	k1gInSc2	genitiv
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgNnSc1d1	považované
za	za	k7c4	za
nositele	nositel	k1gMnSc4	nositel
důrazu	důraz	k1gInSc2	důraz
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
tvarů	tvar	k1gInPc2	tvar
bezrodých	bezrodý	k2eAgNnPc2d1	bezrodý
zájmen	zájmeno	k1gNnPc2	zájmeno
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
1	[number]	k4	1
Funkci	funkce	k1gFnSc4	funkce
příklonných	příklonný	k2eAgFnPc2d1	příklonná
variant	varianta	k1gFnPc2	varianta
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
plní	plnit	k5eAaImIp3nS	plnit
odpovídající	odpovídající	k2eAgInPc4d1	odpovídající
tvary	tvar	k1gInPc4	tvar
prézentu	prézens	k1gInSc3	prézens
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
<g/>
.	.	kIx.	.
2	[number]	k4	2
Zvratné	zvratný	k2eAgNnSc1d1	zvratné
zájmeno	zájmeno	k1gNnSc1	zájmeno
с	с	k?	с
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
předhistorické	předhistorický	k2eAgFnSc6d1	předhistorická
době	doba	k1gFnSc6	doba
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
částici	částice	k1gFnSc6	částice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
funkci	funkce	k1gFnSc6	funkce
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
zvratných	zvratný	k2eAgNnPc2d1	zvratné
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnSc1d1	osobní
zájmeno	zájmeno	k1gNnSc1	zájmeno
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
podmětu	podmět	k1gInSc2	podmět
v	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
textech	text	k1gInPc6	text
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
spojkami	spojka	k1gFnPc7	spojka
ж	ж	k?	ж
<g/>
,	,	kIx,	,
а	а	k?	а
<g/>
,	,	kIx,	,
и	и	k?	и
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
ukazovacím	ukazovací	k2eAgNnSc7d1	ukazovací
zájmenem	zájmeno	k1gNnSc7	zájmeno
о	о	k?	о
nebo	nebo	k8xC	nebo
т	т	k?	т
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepřímých	přímý	k2eNgInPc6d1	nepřímý
pádech	pád	k1gInPc6	pád
plní	plnit	k5eAaImIp3nP	plnit
funkci	funkce	k1gFnSc4	funkce
osobního	osobní	k2eAgNnSc2d1	osobní
zájmena	zájmeno	k1gNnSc2	zájmeno
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
zájmeno	zájmeno	k1gNnSc1	zájmeno
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
předložkách	předložka	k1gFnPc6	předložka
variantní	variantní	k2eAgInPc1d1	variantní
tvary	tvar	k1gInPc1	tvar
začínající	začínající	k2eAgInPc1d1	začínající
na	na	k7c4	na
n-	n-	k?	n-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
<g/>
,	,	kIx,	,
н	н	k?	н
<g/>
,	,	kIx,	,
н	н	k?	н
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
zájmenného	zájmenný	k2eAgNnSc2d1	zájmenné
skloňování	skloňování	k1gNnSc2	skloňování
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
níže	nízce	k6eAd2	nízce
<g/>
:	:	kIx,	:
Ukazovací	ukazovací	k2eAgNnSc1d1	ukazovací
–	–	k?	–
odrážejí	odrážet	k5eAaImIp3nP	odrážet
tři	tři	k4xCgInPc1	tři
stupně	stupeň	k1gInPc1	stupeň
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Zájmeno	zájmeno	k1gNnSc1	zájmeno
с	с	k?	с
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
předmět	předmět	k1gInSc4	předmět
blízký	blízký	k2eAgInSc4d1	blízký
k	k	k7c3	k
mluvčímu	mluvčí	k1gMnSc3	mluvčí
<g/>
,	,	kIx,	,
т	т	k?	т
na	na	k7c4	na
předmět	předmět	k1gInSc4	předmět
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
od	od	k7c2	od
mluvčího	mluvčí	k1gMnSc2	mluvčí
ale	ale	k9	ale
blízký	blízký	k2eAgInSc1d1	blízký
k	k	k7c3	k
posluchači	posluchač	k1gMnSc3	posluchač
a	a	k8xC	a
о	о	k?	о
na	na	k7c4	na
předmět	předmět	k1gInSc4	předmět
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
od	od	k7c2	od
mluvčího	mluvčí	k1gMnSc2	mluvčí
i	i	k8xC	i
posluchače	posluchač	k1gMnSc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
jednoslabičné	jednoslabičný	k2eAgInPc1d1	jednoslabičný
tvary	tvar	k1gInPc1	tvar
nom.	nom.	k?	nom.
sg.	sg.	k?	sg.
m.	m.	k?	m.
с	с	k?	с
<g/>
,	,	kIx,	,
т	т	k?	т
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
jerů	jer	k1gInPc2	jer
vytlačovány	vytlačován	k2eAgInPc4d1	vytlačován
tvary	tvar	k1gInPc4	tvar
s	s	k7c7	s
připojeným	připojený	k2eAgNnSc7d1	připojené
zájmenem	zájmeno	k1gNnSc7	zájmeno
*	*	kIx~	*
<g/>
jь	jь	k?	jь
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
sь	sь	k?	sь
→	→	k?	→
с	с	k?	с
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
tъ	tъ	k?	tъ
→	→	k?	→
т	т	k?	т
<g/>
)	)	kIx)	)
či	či	k8xC	či
tvary	tvar	k1gInPc7	tvar
zdvojenými	zdvojený	k2eAgInPc7d1	zdvojený
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
sь	sь	k?	sь
→	→	k?	→
с	с	k?	с
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
tъ	tъ	k?	tъ
→	→	k?	→
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přivlastňovací	přivlastňovací	k2eAgInSc1d1	přivlastňovací
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
indoevropskou	indoevropský	k2eAgFnSc7d1	indoevropská
přivlastňovací	přivlastňovací	k2eAgFnSc7d1	přivlastňovací
příponou	přípona	k1gFnSc7	přípona
*	*	kIx~	*
<g/>
-ios	-ios	k6eAd1	-ios
jednak	jednak	k8xC	jednak
od	od	k7c2	od
pronominálních	pronominální	k2eAgInPc2d1	pronominální
základů	základ	k1gInPc2	základ
*	*	kIx~	*
<g/>
m-	m-	k?	m-
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
tw-	tw-	k?	tw-
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
sw-	sw-	k?	sw-
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
м	м	k?	м
<g/>
,	,	kIx,	,
т	т	k?	т
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
od	od	k7c2	od
genitivních	genitivní	k2eAgInPc2d1	genitivní
tvarů	tvar	k1gInPc2	tvar
*	*	kIx~	*
<g/>
nasъ	nasъ	k?	nasъ
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
vasъ	vasъ	k?	vasъ
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
н	н	k?	н
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tázací	tázací	k2eAgInSc1d1	tázací
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
od	od	k7c2	od
základu	základ	k1gInSc2	základ
*	*	kIx~	*
<g/>
k-	k-	k?	k-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
<g/>
,	,	kIx,	,
ч	ч	k?	ч
<g/>
,	,	kIx,	,
ч	ч	k?	ч
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
nominativu	nominativ	k1gInSc2	nominativ
zájmena	zájmeno	k1gNnPc1	zájmeno
к	к	k?	к
<g/>
,	,	kIx,	,
ч	ч	k?	ч
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
složením	složení	k1gNnSc7	složení
tvarů	tvar	k1gInPc2	tvar
*	*	kIx~	*
<g/>
kъ	kъ	k?	kъ
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
čь	čь	k?	čь
a	a	k8xC	a
zesilující	zesilující	k2eAgFnSc1d1	zesilující
částice	částice	k1gFnSc1	částice
*	*	kIx~	*
<g/>
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zájmena	zájmeno	k1gNnPc1	zájmeno
к	к	k?	к
<g/>
,	,	kIx,	,
ч	ч	k?	ч
připojením	připojení	k1gNnSc7	připojení
zájmena	zájmeno	k1gNnSc2	zájmeno
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
.	.	kIx.	.
</s>
<s>
Vztažná	vztažný	k2eAgFnSc1d1	vztažná
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc1	dva
synonymní	synonymní	k2eAgInPc1d1	synonymní
typy	typ	k1gInPc1	typ
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zájmena	zájmeno	k1gNnPc1	zájmeno
od	od	k7c2	od
základu	základ	k1gInSc2	základ
*	*	kIx~	*
<g/>
j-	j-	k?	j-
s	s	k7c7	s
připojenou	připojený	k2eAgFnSc7d1	připojená
částicí	částice	k1gFnSc7	částice
*	*	kIx~	*
<g/>
-že	-že	k?	-že
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c6	o
částici	částice	k1gFnSc6	částice
*	*	kIx~	*
<g/>
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
např.	např.	kA	např.
и	и	k?	и
<g/>
,	,	kIx,	,
и	и	k?	и
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
tvary	tvar	k1gInPc1	tvar
od	od	k7c2	od
základu	základ	k1gInSc2	základ
*	*	kIx~	*
<g/>
k-	k-	k?	k-
(	(	kIx(	(
<g/>
shodné	shodný	k2eAgFnPc1d1	shodná
s	s	k7c7	s
tázacími	tázací	k2eAgNnPc7d1	tázací
zájmeny	zájmeno	k1gNnPc7	zájmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
o	o	k7c6	o
částici	částice	k1gFnSc6	částice
*	*	kIx~	*
<g/>
že	že	k8xS	že
nebo	nebo	k8xC	nebo
*	*	kIx~	*
<g/>
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
např.	např.	kA	např.
к	к	k?	к
<g/>
,	,	kIx,	,
к	к	k?	к
ж	ж	k?	ж
<g/>
,	,	kIx,	,
к	к	k?	к
т	т	k?	т
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záporná	záporný	k2eAgFnSc1d1	záporná
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
rozšířením	rozšíření	k1gNnSc7	rozšíření
tvaru	tvar	k1gInSc2	tvar
tázacího	tázací	k2eAgNnSc2d1	tázací
zájmena	zájmeno	k1gNnSc2	zájmeno
o	o	k7c6	o
částici	částice	k1gFnSc6	částice
*	*	kIx~	*
<g/>
ni-	ni-	k?	ni-
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
předložkových	předložkový	k2eAgNnPc6d1	předložkové
spojeních	spojení	k1gNnPc6	spojení
nachází	nacházet	k5eAaImIp3nS	nacházet
před	před	k7c7	před
celým	celý	k2eAgNnSc7d1	celé
spojením	spojení	k1gNnSc7	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc1	tento
zájmena	zájmeno	k1gNnPc1	zájmeno
dále	daleko	k6eAd2	daleko
provázena	provázet	k5eAaImNgFnS	provázet
částicí	částice	k1gFnSc7	částice
*	*	kIx~	*
<g/>
že	že	k8xS	že
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
ж	ж	k?	ж
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
н	н	k?	н
н	н	k?	н
ч	ч	k?	ч
ж	ж	k?	ж
"	"	kIx"	"
<g/>
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neurčitá	určitý	k2eNgFnSc1d1	neurčitá
–	–	k?	–
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
libovolnosti	libovolnost	k1gFnSc2	libovolnost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tvary	tvar	k1gInPc1	tvar
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
tázacími	tázací	k2eAgNnPc7d1	tázací
zájmeny	zájmeno	k1gNnPc7	zájmeno
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
o	o	k7c4	o
částici	částice	k1gFnSc4	částice
*	*	kIx~	*
<g/>
ljubo	ljuba	k1gMnSc5	ljuba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
к	к	k?	к
л	л	k?	л
"	"	kIx"	"
<g/>
kdokoli	kdokoli	k3yInSc1	kdokoli
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
významu	význam	k1gInSc6	význam
vědomé	vědomý	k2eAgFnSc2d1	vědomá
neurčitosti	neurčitost	k1gFnSc2	neurčitost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tvary	tvar	k1gInPc1	tvar
tázacích	tázací	k2eAgNnPc2d1	tázací
zájmen	zájmeno	k1gNnPc2	zájmeno
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
*	*	kIx~	*
<g/>
ně-	ně-	k?	ně-
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
předložkových	předložkový	k2eAgNnPc6d1	předložkové
spojeních	spojení	k1gNnPc6	spojení
nachází	nacházet	k5eAaImIp3nS	nacházet
před	před	k7c7	před
celým	celý	k2eAgNnSc7d1	celé
spojením	spojení	k1gNnSc7	spojení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
н	н	k?	н
"	"	kIx"	"
<g/>
kdosi	kdosi	k3yInSc1	kdosi
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
н	н	k?	н
к	к	k?	к
к	к	k?	к
"	"	kIx"	"
<g/>
k	k	k7c3	k
někomu	někdo	k3yInSc3	někdo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
významu	význam	k1gInSc2	význam
a	a	k8xC	a
užití	užití	k1gNnSc2	užití
lze	lze	k6eAd1	lze
číslovky	číslovka	k1gFnPc1	číslovka
staré	starý	k2eAgFnSc2d1	stará
ruštiny	ruština	k1gFnSc2	ruština
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
základní	základní	k2eAgNnSc4d1	základní
<g/>
,	,	kIx,	,
řadové	řadový	k2eAgNnSc4d1	řadové
<g/>
,	,	kIx,	,
druhové	druhový	k2eAgNnSc4d1	druhové
a	a	k8xC	a
násobné	násobný	k2eAgNnSc4d1	násobné
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
pomocí	pomocí	k7c2	pomocí
cyrilských	cyrilský	k2eAgNnPc2d1	cyrilské
písmen	písmeno	k1gNnPc2	písmeno
opatřených	opatřený	k2eAgMnPc2d1	opatřený
titlem	titl	k1gInSc7	titl
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
různorodou	různorodý	k2eAgFnSc4d1	různorodá
skupinu	skupina	k1gFnSc4	skupina
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tvarosloví	tvarosloví	k1gNnSc2	tvarosloví
i	i	k8xC	i
skladby	skladba	k1gFnSc2	skladba
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc1	jejich
přehled	přehled	k1gInSc1	přehled
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
Číslovky	číslovka	k1gFnSc2	číslovka
о	о	k?	о
<g/>
,	,	kIx,	,
д	д	k?	д
a	a	k8xC	a
о	о	k?	о
se	se	k3xPyFc4	se
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
podle	podle	k7c2	podle
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
zájmenného	zájmenný	k2eAgInSc2d1	zájmenný
vzoru	vzor	k1gInSc2	vzor
(	(	kIx(	(
<g/>
gen.	gen.	kA	gen.
о	о	k?	о
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslovka	číslovka	k1gFnSc1	číslovka
д	д	k?	д
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
skloňovat	skloňovat	k5eAaImF	skloňovat
také	také	k9	také
jako	jako	k9	jako
o-kmenové	omenový	k2eAgNnSc1d1	o-kmenový
substantivum	substantivum	k1gNnSc1	substantivum
(	(	kIx(	(
<g/>
gen.	gen.	kA	gen.
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
koncovka	koncovka	k1gFnSc1	koncovka
nom.	nom.	k?	nom.
<g/>
-akuz	-akuz	k1gInSc1	-akuz
<g/>
.	.	kIx.	.
duálu	duál	k1gInSc6	duál
neuter	neutrum	k1gNnPc2	neutrum
-a	-a	k?	-a
začíná	začínat	k5eAaImIp3nS	začínat
pronikat	pronikat	k5eAaImF	pronikat
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
д	д	k?	д
л	л	k?	л
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
číslovky	číslovka	k1gFnSc2	číslovka
т	т	k?	т
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
i-kmenové	imenový	k2eAgNnSc1d1	i-kmenový
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
též	též	k9	též
alternativní	alternativní	k2eAgInPc4d1	alternativní
tvary	tvar	k1gInPc4	tvar
v	v	k7c6	v
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
trь	trь	k?	trь
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
akuz	akuz	k1gMnSc1	akuz
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
trь	trь	k?	trь
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
lok.	lok.	k?	lok.
/	/	kIx~	/
<g/>
trь	trь	k?	trь
<g/>
/	/	kIx~	/
a	a	k8xC	a
instr.	instr.	k?	instr.
/	/	kIx~	/
<g/>
trь	trь	k?	trь
<g/>
/	/	kIx~	/
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
podle	podle	k7c2	podle
analogie	analogie	k1gFnSc2	analogie
s	s	k7c7	s
číslovkou	číslovka	k1gFnSc7	číslovka
ч	ч	k?	ч
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
číslovka	číslovka	k1gFnSc1	číslovka
ч	ч	k?	ч
má	mít	k5eAaImIp3nS	mít
nepravidelné	pravidelný	k2eNgNnSc4d1	nepravidelné
skloňování	skloňování	k1gNnSc4	skloňování
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
podle	podle	k7c2	podle
i-kmenové	imenová	k1gFnSc2	i-kmenová
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
podle	podle	k7c2	podle
konsonantické	konsonantický	k2eAgFnSc2d1	konsonantický
deklinace	deklinace	k1gFnSc2	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Číslovky	číslovka	k1gFnPc1	číslovka
5-10	[number]	k4	5-10
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
i-kmenová	imenový	k2eAgNnPc1d1	i-kmenový
substantiva	substantivum	k1gNnPc1	substantivum
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
rodě	rod	k1gInSc6	rod
s	s	k7c7	s
rozvíjejícím	rozvíjející	k2eAgInSc7d1	rozvíjející
přívlastkem	přívlastek	k1gInSc7	přívlastek
(	(	kIx(	(
<g/>
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
též	též	k9	též
s	s	k7c7	s
příčestím	příčestí	k1gNnSc7	příčestí
perfekta	perfektum	k1gNnSc2	perfektum
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
б	б	k?	б
м	м	k?	м
–	–	k?	–
"	"	kIx"	"
<g/>
6	[number]	k4	6
mých	můj	k3xOp1gFnPc2	můj
veverek	veverka	k1gFnPc2	veverka
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
754	[number]	k4	754
–	–	k?	–
14	[number]	k4	14
<g />
.	.	kIx.	.
</s>
<s>
<g/>
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
о	о	k?	о
в	в	k?	в
–	–	k?	–
"	"	kIx"	"
<g/>
osm	osm	k4xCc1	osm
/	/	kIx~	/
<g/>
vojáků	voják	k1gMnPc2	voják
<g/>
/	/	kIx~	/
vyklouzlo	vyklouznout	k5eAaPmAgNnS	vyklouznout
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
724	[number]	k4	724
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Počítaný	počítaný	k2eAgInSc1d1	počítaný
předmět	předmět	k1gInSc1	předmět
stojí	stát	k5eAaImIp3nS	stát
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
již	již	k9	již
v	v	k7c6	v
nejstarším	starý	k2eAgNnSc6d3	nejstarší
období	období	k1gNnSc6	období
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
číslovka	číslovka	k1gFnSc1	číslovka
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
pádě	pád	k1gInSc6	pád
s	s	k7c7	s
počítaným	počítaný	k2eAgInSc7d1	počítaný
předmětem	předmět	k1gInSc7	předmět
<g/>
:	:	kIx,	:
<g/>
н	н	k?	н
д	д	k?	д
г	г	k?	г
–	–	k?	–
"	"	kIx"	"
<g/>
za	za	k7c4	za
deset	deset	k4xCc4	deset
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
420	[number]	k4	420
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
40	[number]	k4	40
<g/>
м	м	k?	м
р	р	k?	р
–	–	k?	–
"	"	kIx"	"
<g/>
ohledně	ohledně	k7c2	ohledně
40	[number]	k4	40
rezan	rezana	k1gFnPc2	rezana
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
247	[number]	k4	247
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Číslovka	číslovka	k1gFnSc1	číslovka
д	д	k?	д
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
v	v	k7c6	v
ustrnulých	ustrnulý	k2eAgNnPc6d1	ustrnulé
spojeních	spojení	k1gNnPc6	spojení
archaické	archaický	k2eAgFnPc1d1	archaická
tvary	tvar	k1gInPc4	tvar
podle	podle	k7c2	podle
konsonantické	konsonantický	k2eAgFnSc2d1	konsonantický
deklinace	deklinace	k1gFnSc2	deklinace
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dřívější	dřívější	k2eAgFnSc4d1	dřívější
příslušnost	příslušnost	k1gFnSc4	příslušnost
mezi	mezi	k7c4	mezi
maskulina	maskulinum	k1gNnPc4	maskulinum
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c6	o
gen.	gen.	kA	gen.
sg.	sg.	k?	sg.
д	д	k?	д
v	v	k7c6	v
číselných	číselný	k2eAgInPc6d1	číselný
výrazech	výraz	k1gInPc6	výraz
pro	pro	k7c4	pro
poloviny	polovina	k1gFnPc4	polovina
desítek	desítka	k1gFnPc2	desítka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
п	п	k?	п
в	в	k?	в
д	д	k?	д
<g/>
,	,	kIx,	,
п	п	k?	п
т	т	k?	т
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lok.	lok.	k?	lok.
sg.	sg.	k?	sg.
д	д	k?	д
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
pro	pro	k7c4	pro
11-19	[number]	k4	11-19
(	(	kIx(	(
<g/>
např.	např.	kA	např.
о	о	k?	о
н	н	k?	н
д	д	k?	д
<g/>
,	,	kIx,	,
д	д	k?	д
н	н	k?	н
д	д	k?	д
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvary	tvar	k1gInPc1	tvar
nom.	nom.	k?	nom.
pl.	pl.	k?	pl.
д	д	k?	д
<g/>
,	,	kIx,	,
gen.	gen.	kA	gen.
pl.	pl.	k?	pl.
д	д	k?	д
ve	v	k7c6	v
složených	složený	k2eAgInPc6d1	složený
tvarech	tvar	k1gInPc6	tvar
desítek	desítka	k1gFnPc2	desítka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ч	ч	k?	ч
д	д	k?	д
<g/>
,	,	kIx,	,
п	п	k?	п
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
с	с	k?	с
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
čísla	číslo	k1gNnSc2	číslo
40	[number]	k4	40
doloženo	doložen	k2eAgNnSc1d1	doloženo
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
i	i	k9	i
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
měnové	měnový	k2eAgFnSc2d1	měnová
jednotky	jednotka	k1gFnSc2	jednotka
"	"	kIx"	"
<g/>
svazek	svazek	k1gInSc1	svazek
40	[number]	k4	40
sobolích	sobolí	k2eAgFnPc2d1	sobolí
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Číslovka	číslovka	k1gFnSc1	číslovka
с	с	k?	с
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
o-kmenová	omenový	k2eAgNnPc4d1	o-kmenový
neutra	neutrum	k1gNnPc4	neutrum
(	(	kIx(	(
<g/>
složené	složený	k2eAgInPc1d1	složený
tvary	tvar	k1gInPc1	tvar
д	д	k?	д
с	с	k?	с
<g/>
,	,	kIx,	,
т	т	k?	т
с	с	k?	с
<g/>
,	,	kIx,	,
п	п	k?	п
с	с	k?	с
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
д	д	k?	д
<g/>
,	,	kIx,	,
doložené	doložená	k1gFnPc1	doložená
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Skloňování	skloňování	k1gNnSc4	skloňování
číslovky	číslovka	k1gFnSc2	číslovka
т	т	k?	т
je	být	k5eAaImIp3nS	být
jā	jā	k?	jā
(	(	kIx(	(
<g/>
složené	složený	k2eAgInPc1d1	složený
tvary	tvar	k1gInPc1	tvar
д	д	k?	д
т	т	k?	т
<g/>
,	,	kIx,	,
т	т	k?	т
т	т	k?	т
<g/>
,	,	kIx,	,
п	п	k?	п
т	т	k?	т
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílové	dílový	k2eAgInPc1d1	dílový
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nP	tvořit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
podskupinu	podskupina	k1gFnSc4	podskupina
číslovek	číslovka	k1gFnPc2	číslovka
základních	základní	k2eAgFnPc2d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
patří	patřit	k5eAaImIp3nS	patřit
п	п	k?	п
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
,	,	kIx,	,
т	т	k?	т
a	a	k8xC	a
ч	ч	k?	ч
<g/>
.	.	kIx.	.
</s>
<s>
Číselné	číselný	k2eAgInPc4d1	číselný
výrazy	výraz	k1gInPc4	výraz
obsahující	obsahující	k2eAgNnSc4d1	obsahující
slovo	slovo	k1gNnSc4	slovo
п	п	k?	п
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
složených	složený	k2eAgFnPc2d1	složená
číslovek	číslovka	k1gFnPc2	číslovka
označujících	označující	k2eAgFnPc2d1	označující
poloviny	polovina	k1gFnPc4	polovina
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
desítek	desítka	k1gFnPc2	desítka
a	a	k8xC	a
stovek	stovka	k1gFnPc2	stovka
<g/>
:	:	kIx,	:
п	п	k?	п
т	т	k?	т
р	р	k?	р
–	–	k?	–
"	"	kIx"	"
<g/>
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
ruble	rubl	k1gInSc2	rubl
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
689	[number]	k4	689
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
п	п	k?	п
п	п	k?	п
д	д	k?	д
г	г	k?	г
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
čtyřicet	čtyřicet	k4xCc1	čtyřicet
pět	pět	k4xCc1	pět
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
902	[number]	k4	902
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
и	и	k?	и
и	и	k?	и
п	п	k?	п
с	с	k?	с
–	–	k?	–
"	"	kIx"	"
<g/>
Zabili	zabít	k5eAaPmAgMnP	zabít
jich	on	k3xPp3gMnPc2	on
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc1	padesát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
Synodální	synodální	k2eAgInSc4d1	synodální
rukopis	rukopis	k1gInSc4	rukopis
<g/>
:	:	kIx,	:
Novgorodský	novgorodský	k2eAgInSc4d1	novgorodský
první	první	k4xOgInSc4	první
letopis	letopis	k1gInSc4	letopis
<g/>
,	,	kIx,	,
1142	[number]	k4	1142
<g/>
]	]	kIx)	]
Číslovky	číslovka	k1gFnSc2	číslovka
<g />
.	.	kIx.	.
</s>
<s>
vyššího	vysoký	k2eAgInSc2d2	vyšší
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
příponou	přípona	k1gFnSc7	přípona
-ina	-ina	k1gInSc2	-in
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
řídce	řídce	k6eAd1	řídce
a	a	k8xC	a
zvl.	zvl.	kA	zvl.
s	s	k7c7	s
posunutým	posunutý	k2eAgInSc7d1	posunutý
významem	význam	k1gInSc7	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ч	ч	k?	ч
"	"	kIx"	"
<g/>
1⁄	1⁄	k?	1⁄
kádě	káď	k1gFnSc2	káď
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
dutá	dutý	k2eAgFnSc1d1	dutá
míra	míra	k1gFnSc1	míra
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
о	о	k?	о
"	"	kIx"	"
<g/>
1⁄	1⁄	k?	1⁄
kádě	káď	k1gFnSc2	káď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
д	д	k?	д
"	"	kIx"	"
<g/>
desátek	desátek	k1gInSc1	desátek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadové	řadový	k2eAgInPc1d1	řadový
–	–	k?	–
adjektiva	adjektivum	k1gNnSc2	adjektivum
výhradně	výhradně	k6eAd1	výhradně
se	s	k7c7	s
složenými	složený	k2eAgInPc7d1	složený
tvary	tvar	k1gInPc7	tvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
п	п	k?	п
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
,	,	kIx,	,
т	т	k?	т
<g/>
,	,	kIx,	,
ч	ч	k?	ч
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
,	,	kIx,	,
ш	ш	k?	ш
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
,	,	kIx,	,
о	о	k?	о
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
číslovky	číslovka	k1gFnSc2	číslovka
druhé	druhý	k4xOgFnPc4	druhý
desítky	desítka	k1gFnPc4	desítka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
podobná	podobný	k2eAgNnPc1d1	podobné
spojení	spojení	k1gNnPc1	spojení
jako	jako	k9	jako
u	u	k7c2	u
základních	základní	k2eAgFnPc2d1	základní
číslovek	číslovka	k1gFnPc2	číslovka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
о	о	k?	о
н	н	k?	н
д	д	k?	д
<g/>
,	,	kIx,	,
в	в	k?	в
н	н	k?	н
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tvary	tvar	k1gInPc7	tvar
jmennými	jmenný	k2eAgInPc7d1	jmenný
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
ustrnulých	ustrnulý	k2eAgNnPc6d1	ustrnulé
spojeních	spojení	k1gNnPc6	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druh	k1gMnPc1	druh
–	–	k?	–
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
se	se	k3xPyFc4	se
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
číslovky	číslovka	k1gFnSc2	číslovka
příponou	přípona	k1gFnSc7	přípona
-er-	r-	k?	-er-
a	a	k8xC	a
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
adjektiva	adjektivum	k1gNnSc2	adjektivum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ч	ч	k?	ч
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
,	,	kIx,	,
ш	ш	k?	ш
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
,	,	kIx,	,
о	о	k?	о
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
tvary	tvar	k1gInPc1	tvar
д	д	k?	д
<g/>
,	,	kIx,	,
о	о	k?	о
<g/>
,	,	kIx,	,
т	т	k?	т
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
příponou	přípona	k1gFnSc7	přípona
neodvozují	odvozovat	k5eNaImIp3nP	odvozovat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
zájmennou	zájmenný	k2eAgFnSc4d1	zájmenná
flexi	flexe	k1gFnSc4	flexe
<g/>
.	.	kIx.	.
</s>
<s>
Násobné	násobný	k2eAgInPc1d1	násobný
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
tvaru	tvar	k1gInSc2	tvar
-šь	-šь	k?	-šь
buď	buď	k8xC	buď
ke	k	k7c3	k
kmeni	kmen	k1gInSc3	kmen
přes	přes	k7c4	přes
spojovací	spojovací	k2eAgFnSc4d1	spojovací
samohlásku	samohláska	k1gFnSc4	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
д	д	k?	д
<g/>
,	,	kIx,	,
т	т	k?	т
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
,	,	kIx,	,
ш	ш	k?	ш
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
,	,	kIx,	,
о	о	k?	о
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
nominativnímu	nominativní	k2eAgInSc3d1	nominativní
tvaru	tvar	k1gInSc3	tvar
základní	základní	k2eAgFnSc2d1	základní
číslovky	číslovka	k1gFnSc2	číslovka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
д	д	k?	д
<g/>
,	,	kIx,	,
ч	ч	k?	ч
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
tvoření	tvoření	k1gNnSc2	tvoření
stojí	stát	k5eAaImIp3nS	stát
tvar	tvar	k1gInSc4	tvar
о	о	k?	о
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morfologie	morfologie	k1gFnSc2	morfologie
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
slovesa	sloveso	k1gNnPc4	sloveso
kmen	kmen	k1gInSc1	kmen
prézentní	prézentní	k2eAgFnSc2d1	prézentní
a	a	k8xC	a
infinitivní	infinitivní	k2eAgFnSc2d1	infinitivní
<g/>
.	.	kIx.	.
</s>
<s>
Prézentní	prézentní	k2eAgInSc1d1	prézentní
kmen	kmen	k1gInSc1	kmen
končí	končit	k5eAaImIp3nS	končit
vždy	vždy	k6eAd1	vždy
souhláskou	souhláska	k1gFnSc7	souhláska
a	a	k8xC	a
kromě	kromě	k7c2	kromě
prézenta	prézento	k1gNnSc2	prézento
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
imperativ	imperativ	k1gInSc1	imperativ
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
infinitivního	infinitivní	k2eAgInSc2d1	infinitivní
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
souhláskou	souhláska	k1gFnSc7	souhláska
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prézentního	prézentní	k2eAgInSc2d1	prézentní
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
slovesa	sloveso	k1gNnPc1	sloveso
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
atematická	atematický	k2eAgNnPc4d1	atematický
a	a	k8xC	a
tematická	tematický	k2eAgNnPc4d1	tematické
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
atematických	atematický	k2eAgNnPc2d1	atematický
sloves	sloveso	k1gNnPc2	sloveso
jsou	být	k5eAaImIp3nP	být
koncovky	koncovka	k1gFnPc1	koncovka
prézentu	prézens	k1gInSc2	prézens
připojeny	připojit	k5eAaPmNgInP	připojit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ke	k	k7c3	k
kořeni	kořen	k1gInSc3	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
slovesa	sloveso	k1gNnPc1	sloveso
jsou	být	k5eAaImIp3nP	být
tematická	tematický	k2eAgNnPc1d1	tematické
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mají	mít	k5eAaImIp3nP	mít
kmenotvornou	kmenotvorný	k2eAgFnSc4d1	kmenotvorná
příponu	přípona	k1gFnSc4	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
prézentu	prézens	k1gInSc2	prézens
je	být	k5eAaImIp3nS	být
těsně	těsně	k6eAd1	těsně
svázán	svázat	k5eAaPmNgMnS	svázat
s	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
vidu	vid	k1gInSc2	vid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
staroruštině	staroruština	k1gFnSc6	staroruština
již	již	k6eAd1	již
samotné	samotný	k2eAgNnSc1d1	samotné
rozlišování	rozlišování	k1gNnSc1	rozlišování
dokonavého	dokonavý	k2eAgInSc2d1	dokonavý
a	a	k8xC	a
nedokonavého	dokonavý	k2eNgInSc2d1	nedokonavý
vidu	vid	k1gInSc2	vid
prokazatelně	prokazatelně	k6eAd1	prokazatelně
existovalo	existovat	k5eAaImAgNnS	existovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zatím	zatím	k6eAd1	zatím
nemělo	mít	k5eNaImAgNnS	mít
tak	tak	k6eAd1	tak
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
slovesa	sloveso	k1gNnPc1	sloveso
(	(	kIx(	(
<g/>
především	především	k9	především
bezpředponová	bezpředponový	k2eAgFnSc1d1	bezpředponový
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
obouvidová	obouvidový	k2eAgFnSc1d1	obouvidový
a	a	k8xC	a
čistě	čistě	k6eAd1	čistě
vidové	vidový	k2eAgFnPc1d1	vidová
dvojice	dvojice	k1gFnPc1	dvojice
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
způsobem	způsob	k1gInSc7	způsob
tvoření	tvoření	k1gNnSc2	tvoření
dokonavého	dokonavý	k2eAgInSc2d1	dokonavý
vidu	vid	k1gInSc2	vid
byla	být	k5eAaImAgFnS	být
prefixace	prefixace	k1gFnSc1	prefixace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dávala	dávat	k5eAaImAgFnS	dávat
předpona	předpona	k1gFnSc1	předpona
slovesu	sloveso	k1gNnSc3	sloveso
nový	nový	k2eAgInSc4d1	nový
význam	význam	k1gInSc4	význam
či	či	k8xC	či
významový	významový	k2eAgInSc1d1	významový
odstín	odstín	k1gInSc1	odstín
(	(	kIx(	(
<g/>
např.	např.	kA	např.
к	к	k?	к
→	→	k?	→
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvozování	odvozování	k1gNnSc1	odvozování
sekundárních	sekundární	k2eAgNnPc2d1	sekundární
imperfektiv	imperfektivum	k1gNnPc2	imperfektivum
se	se	k3xPyFc4	se
provádělo	provádět	k5eAaImAgNnS	provádět
nejčastěji	často	k6eAd3	často
příponou	přípona	k1gFnSc7	přípona
-ъ	-ъ	k?	-ъ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
с	с	k?	с
→	→	k?	→
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgNnPc1d1	sekundární
imperfektiva	imperfektivum	k1gNnPc1	imperfektivum
na	na	k7c6	na
-jati	ať	k1gFnSc6	-jať
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
od	od	k7c2	od
imperfekta	imperfektum	k1gNnSc2	imperfektum
sloves	sloveso	k1gNnPc2	sloveso
na	na	k7c6	na
-iti	-iti	k1gInSc6	-it
(	(	kIx(	(
<g/>
např.	např.	kA	např.
п	п	k?	п
→	→	k?	→
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atematická	atematický	k2eAgNnPc1d1	atematický
slovesa	sloveso	k1gNnPc1	sloveso
tvoří	tvořit	k5eAaImIp3nP	tvořit
prézens	prézens	k1gInSc4	prézens
připojením	připojení	k1gNnSc7	připojení
koncovek	koncovka	k1gFnPc2	koncovka
-mь	ь	k?	-mь
<g/>
,	,	kIx,	,
-si	i	k?	-si
<g/>
,	,	kIx,	,
-tь	ь	k?	-tь
<g/>
,	,	kIx,	,
-vě	ě	k?	-vě
<g/>
,	,	kIx,	,
-ta	a	k?	-ta
<g/>
,	,	kIx,	,
-ta	a	k?	-ta
<g/>
,	,	kIx,	,
-my	y	k?	-my
<g/>
,	,	kIx,	,
-te	e	k?	-te
<g/>
,	,	kIx,	,
-jatь	atь	k?	-jatь
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ke	k	k7c3	k
kořeni	kořen	k1gInSc3	kořen
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
o	o	k7c6	o
kombinaci	kombinace	k1gFnSc6	kombinace
hlásek	hláska	k1gFnPc2	hláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nP	patřit
slovesa	sloveso	k1gNnPc1	sloveso
б	б	k?	б
(	(	kIx(	(
<g/>
kořen	kořen	k1gInSc4	kořen
*	*	kIx~	*
<g/>
es-	es-	k?	es-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
в	в	k?	в
(	(	kIx(	(
<g/>
kořen	kořen	k1gInSc4	kořen
*	*	kIx~	*
<g/>
věd-	věd-	k?	věd-
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
д	д	k?	д
(	(	kIx(	(
<g/>
kořen	kořen	k1gInSc4	kořen
*	*	kIx~	*
<g/>
dad-	dad-	k?	dad-
<g/>
)	)	kIx)	)
a	a	k8xC	a
ѣ	ѣ	k?	ѣ
(	(	kIx(	(
<g/>
kořen	kořen	k1gInSc4	kořen
*	*	kIx~	*
<g/>
ěd-	ěd-	k?	ěd-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
atematické	atematický	k2eAgNnSc4d1	atematický
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
knižní	knižní	k2eAgInSc4d1	knižní
tvary	tvar	k1gInPc4	tvar
slovesa	sloveso	k1gNnSc2	sloveso
и	и	k?	и
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
и	и	k?	и
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
tvary	tvar	k1gInPc1	tvar
tematické	tematický	k2eAgFnSc2d1	tematická
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
и	и	k?	и
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
в	в	k?	в
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
doloženo	doložit	k5eAaPmNgNnS	doložit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
je	být	k5eAaImIp3nS	být
vytlačováno	vytlačovat	k5eAaImNgNnS	vytlačovat
tematickým	tematický	k2eAgNnSc7d1	tematické
slovesem	sloveso	k1gNnSc7	sloveso
в	в	k?	в
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
prézentu	prézens	k1gInSc3	prézens
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
є	є	k?	є
<g/>
)	)	kIx)	)
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
spony	spona	k1gFnSc2	spona
(	(	kIx(	(
<g/>
a	a	k8xC	a
též	též	k9	též
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
perfekta	perfektum	k1gNnSc2	perfektum
vyjma	vyjma	k7c2	vyjma
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
všech	všecek	k3xTgNnPc2	všecek
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
příklonné	příklonný	k2eAgFnPc1d1	příklonná
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
nevyjádřeném	vyjádřený	k2eNgInSc6d1	nevyjádřený
podmětu	podmět	k1gInSc6	podmět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
existují	existovat	k5eAaImIp3nP	existovat
plnopřízvučné	plnopřízvučný	k2eAgInPc1d1	plnopřízvučný
tvary	tvar	k1gInPc1	tvar
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
nacházet	nacházet	k5eAaImF	nacházet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
existovat	existovat	k5eAaImF	existovat
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
ч	ч	k?	ч
н	н	k?	н
в	в	k?	в
ч	ч	k?	ч
т	т	k?	т
є	є	k?	є
в	в	k?	в
к	к	k?	к
–	–	k?	–
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
neposíláš	posílat	k5eNaImIp2nS	posílat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
ti	ty	k3xPp2nSc3	ty
dala	dát	k5eAaPmAgFnS	dát
vykovat	vykovat	k5eAaPmF	vykovat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
644	[number]	k4	644
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
є	є	k?	є
г	г	k?	г
м	м	k?	м
н	н	k?	н
и	и	k?	и
з	з	k?	з
–	–	k?	–
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
10	[number]	k4	10
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Tematická	tematický	k2eAgNnPc1d1	tematické
slovesa	sloveso	k1gNnPc1	sloveso
tvoří	tvořit	k5eAaImIp3nP	tvořit
prézens	prézens	k1gInSc4	prézens
připojením	připojení	k1gNnSc7	připojení
koncovek	koncovka	k1gFnPc2	koncovka
-u	-u	k?	-u
<g/>
,	,	kIx,	,
-ši	-ši	k?	-ši
<g/>
,	,	kIx,	,
-tь	-tь	k?	-tь
<g/>
,	,	kIx,	,
-vě	-vě	k?	-vě
<g/>
,	,	kIx,	,
-ta	-ta	k?	-ta
<g/>
,	,	kIx,	,
-ta	-ta	k?	-ta
<g/>
,	,	kIx,	,
-mъ	-mъ	k?	-mъ
<g/>
,	,	kIx,	,
-te	-te	k?	-te
<g/>
,	,	kIx,	,
-utь	-utь	k?	-utь
<g/>
/	/	kIx~	/
<g/>
-jatь	-jatь	k?	-jatь
ke	k	k7c3	k
kmeni	kmen	k1gInSc3	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
podléhat	podléhat	k5eAaImF	podléhat
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
časování	časování	k1gNnSc2	časování
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
1	[number]	k4	1
Nová	nový	k2eAgFnSc1d1	nová
tematická	tematický	k2eAgFnSc1d1	tematická
koncovka	koncovka	k1gFnSc1	koncovka
2	[number]	k4	2
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
sg.	sg.	k?	sg.
-šь	-šь	k?	-šь
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Koncem	koncem	k7c2	koncem
pozdního	pozdní	k2eAgNnSc2d1	pozdní
období	období	k1gNnSc2	období
proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
do	do	k7c2	do
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
atematických	atematický	k2eAgInPc2d1	atematický
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
д	д	k?	д
→	→	k?	→
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
V	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
též	též	k9	též
tvary	tvar	k1gInPc1	tvar
bez	bez	k7c2	bez
-tь	ь	k?	-tь
/	/	kIx~	/
-stь	tь	k?	-stь
(	(	kIx(	(
<g/>
např.	např.	kA	např.
є	є	k?	є
<g/>
,	,	kIx,	,
х	х	k?	х
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
systému	systém	k1gInSc2	systém
staroruských	staroruský	k2eAgInPc2d1	staroruský
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
jasná	jasný	k2eAgNnPc1d1	jasné
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
nejdiskutovanějším	diskutovaný	k2eAgInSc7d3	nejdiskutovanější
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgNnSc2	jaký
postavení	postavení	k1gNnSc2	postavení
nabyly	nabýt	k5eAaPmAgInP	nabýt
prosté	prostý	k2eAgInPc1d1	prostý
minulé	minulý	k2eAgInPc1d1	minulý
časy	čas	k1gInPc1	čas
–	–	k?	–
aorist	aorist	k1gInSc1	aorist
a	a	k8xC	a
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol	stol	k1gInSc1	stol
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Rusi	Rus	k1gFnPc4	Rus
možné	možný	k2eAgNnSc1d1	možné
předpokládat	předpokládat	k5eAaImF	předpokládat
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
aorist	aorist	k1gInSc1	aorist
sice	sice	k8xC	sice
chybí	chybit	k5eAaPmIp3nS	chybit
v	v	k7c6	v
hovorovém	hovorový	k2eAgInSc6d1	hovorový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
mluvčí	mluvčit	k5eAaImIp3nS	mluvčit
mu	on	k3xPp3gNnSc3	on
bez	bez	k7c2	bez
obtíží	obtíž	k1gFnPc2	obtíž
rozumějí	rozumět	k5eAaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
později	pozdě	k6eAd2	pozdě
zaniká	zanikat	k5eAaImIp3nS	zanikat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mizí	mizet	k5eAaImIp3nS	mizet
pasivní	pasivní	k2eAgFnSc4d1	pasivní
znalost	znalost	k1gFnSc4	znalost
aoristu	aorist	k1gInSc2	aorist
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgInSc4	týž
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dříve	dříve	k6eAd2	dříve
završený	završený	k2eAgInSc1d1	završený
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
i	i	k9	i
pro	pro	k7c4	pro
imperfektum	imperfektum	k1gNnSc4	imperfektum
<g/>
.	.	kIx.	.
</s>
<s>
Aorist	aorist	k1gInSc1	aorist
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgInSc1d1	prostý
minulý	minulý	k2eAgInSc1d1	minulý
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
pouze	pouze	k6eAd1	pouze
aoristové	aoristový	k2eAgInPc1d1	aoristový
tvary	tvar	k1gInPc1	tvar
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zakončení	zakončení	k1gNnSc2	zakončení
infinitivního	infinitivní	k2eAgInSc2d1	infinitivní
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
tvoření	tvoření	k1gNnSc2	tvoření
<g/>
:	:	kIx,	:
Imperfektum	imperfektum	k1gNnSc1	imperfektum
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgInSc4d1	prostý
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
stažení	stažení	k1gNnSc2	stažení
skupiny	skupina	k1gFnSc2	skupina
-ē	-ē	k?	-ē
→	→	k?	→
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
a-	a-	k?	a-
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
distribuce	distribuce	k1gFnSc2	distribuce
jeho	jeho	k3xOp3gFnPc2	jeho
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
pouze	pouze	k6eAd1	pouze
kmeny	kmen	k1gInPc1	kmen
s	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-a-	-a-	k?	-a-
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staženém	stažený	k2eAgInSc6d1	stažený
tvaru	tvar	k1gInSc6	tvar
-aa-	-aa-	k?	-aa-
→	→	k?	→
-a-	-a-	k?	-a-
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Perfektum	perfektum	k1gNnSc1	perfektum
je	být	k5eAaImIp3nS	být
opisný	opisný	k2eAgInSc4d1	opisný
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
platný	platný	k2eAgInSc4d1	platný
stav	stav	k1gInSc4	stav
vzešlý	vzešlý	k2eAgInSc4d1	vzešlý
z	z	k7c2	z
děje	děj	k1gInSc2	děj
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorovém	hovorový	k2eAgInSc6d1	hovorový
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgInS	stát
univerzální	univerzální	k2eAgInSc1d1	univerzální
tvar	tvar	k1gInSc1	tvar
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
perfekta	perfektum	k1gNnSc2	perfektum
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pomocného	pomocný	k2eAgNnSc2d1	pomocné
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
v	v	k7c6	v
přítomném	přítomný	k2eAgInSc6d1	přítomný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
fakticky	fakticky	k6eAd1	fakticky
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
nevyjádřeném	vyjádřený	k2eNgInSc6d1	nevyjádřený
podmětu	podmět	k1gInSc6	podmět
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
příčestí	příčestí	k1gNnSc2	příčestí
perfekta	perfektum	k1gNnSc2	perfektum
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
perfektum	perfektum	k1gNnSc1	perfektum
velice	velice	k6eAd1	velice
zřídka	zřídka	k6eAd1	zřídka
v	v	k7c6	v
knižních	knižní	k2eAgInPc6d1	knižní
textech	text	k1gInPc6	text
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
nejčastěji	často	k6eAd3	často
označuje	označovat	k5eAaImIp3nS	označovat
stav	stav	k1gInSc4	stav
minulý	minulý	k2eAgInSc4d1	minulý
<g/>
,	,	kIx,	,
vzešlý	vzešlý	k2eAgInSc4d1	vzešlý
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
děje	děj	k1gInSc2	děj
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
pluskvamperfektum	pluskvamperfektum	k1gNnSc1	pluskvamperfektum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pluskvamperfektum	pluskvamperfektum	k1gNnSc1	pluskvamperfektum
je	být	k5eAaImIp3nS	být
opisný	opisný	k2eAgInSc4d1	opisný
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
perfekta	perfektum	k1gNnSc2	perfektum
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
a	a	k8xC	a
příčestí	příčestí	k1gNnSc1	příčestí
perfekta	perfektum	k1gNnSc2	perfektum
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgMnS	sloužit
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
předčasnosti	předčasnost	k1gFnSc2	předčasnost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
perfektum	perfektum	k1gNnSc4	perfektum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
řeči	řeč	k1gFnSc6	řeč
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
událost	událost	k1gFnSc1	událost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jako	jako	k9	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
včera	včera	k6eAd1	včera
(	(	kIx(	(
<g/>
a	a	k8xC	a
ne	ne	k9	ne
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loni	loni	k6eAd1	loni
(	(	kIx(	(
<g/>
a	a	k8xC	a
ne	ne	k9	ne
letos	letos	k6eAd1	letos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
(	(	kIx(	(
<g/>
a	a	k8xC	a
ne	ne	k9	ne
nyní	nyní	k6eAd1	nyní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
chybí	chybět	k5eAaImIp3nS	chybět
sémantický	sémantický	k2eAgInSc1d1	sémantický
element	element	k1gInSc1	element
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
předčasnost	předčasnost	k1gFnSc4	předčasnost
vůči	vůči	k7c3	vůči
jiné	jiný	k2eAgFnSc3d1	jiná
události	událost	k1gFnSc3	událost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
imperativu	imperativ	k1gInSc2	imperativ
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
prézentního	prézentní	k2eAgInSc2d1	prézentní
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Příponou	přípona	k1gFnSc7	přípona
-ě-	-ě-	k?	-ě-
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
imperativ	imperativ	k1gInSc1	imperativ
od	od	k7c2	od
sloves	sloveso	k1gNnPc2	sloveso
I.	I.	kA	I.
časování	časování	k1gNnSc1	časování
s	s	k7c7	s
kmenem	kmen	k1gInSc7	kmen
zakončeným	zakončený	k2eAgInSc7d1	zakončený
na	na	k7c4	na
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Přípona	přípona	k1gFnSc1	přípona
-i-	-i-	k?	-i-
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
II	II	kA	II
<g/>
.	.	kIx.	.
časování	časování	k1gNnPc2	časování
a	a	k8xC	a
sloves	sloveso	k1gNnPc2	sloveso
I.	I.	kA	I.
časování	časování	k1gNnPc4	časování
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
měkkou	měkký	k2eAgFnSc7d1	měkká
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Diftongický	diftongický	k2eAgInSc1d1	diftongický
původ	původ	k1gInSc1	původ
těchto	tento	k3xDgFnPc2	tento
přípon	přípona	k1gFnPc2	přípona
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
změnu	změna	k1gFnSc4	změna
kmene	kmen	k1gInSc2	kmen
zakončeného	zakončený	k2eAgInSc2d1	zakončený
velárou	velára	k1gFnSc7	velára
-k-	-	k?	-k-
<g/>
,	,	kIx,	,
<g/>
-g-	-	k?	-g-
<g/>
,	,	kIx,	,
<g/>
-ch-	h-	k?	-ch-
→	→	k?	→
-c-	-	k?	-c-
<g/>
,	,	kIx,	,
<g/>
-z-	-	k?	-z-
<g/>
,	,	kIx,	,
<g/>
-s-	-	k?	-s-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pomogu	pomoga	k1gFnSc4	pomoga
→	→	k?	→
pomozi	pomoze	k1gFnSc3	pomoze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
obvykle	obvykle	k6eAd1	obvykle
existuje	existovat	k5eAaImIp3nS	existovat
jeden	jeden	k4xCgInSc4	jeden
společný	společný	k2eAgInSc4d1	společný
tvar	tvar	k1gInSc4	tvar
na	na	k7c4	na
-	-	kIx~	-
<g/>
i.	i.	k?	i.
Pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
sloves	sloveso	k1gNnPc2	sloveso
(	(	kIx(	(
<g/>
zvl.	zvl.	kA	zvl.
atematických	atematický	k2eAgMnPc2d1	atematický
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
tvar	tvar	k1gInSc4	tvar
imperativu	imperativ	k1gInSc2	imperativ
na	na	k7c4	na
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
ž	ž	k?	ž
<g/>
)	)	kIx)	)
<g/>
ь	ь	k?	ь
1	[number]	k4	1
Koncové	koncový	k2eAgNnSc1d1	koncové
-i	-i	k?	-i
odpadá	odpadat	k5eAaImIp3nS	odpadat
od	od	k7c2	od
kmenů	kmen	k1gInPc2	kmen
zakončených	zakončený	k2eAgInPc2d1	zakončený
na	na	k7c6	na
-j-	-j-	k?	-j-
nejpozději	pozdě	k6eAd3	pozdě
počátkem	počátkem	k7c2	počátkem
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Není	být	k5eNaImIp3nS	být
<g/>
-li	-li	k?	-li
pod	pod	k7c7	pod
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
koncem	koncem	k7c2	koncem
tohoto	tento	k3xDgInSc2	tento
stol.	stol.	k?	stol.
odpadat	odpadat	k5eAaImF	odpadat
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
přechodníků	přechodník	k1gInPc2	přechodník
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nesklonné	sklonný	k2eNgFnPc4d1	nesklonná
a	a	k8xC	a
shodují	shodovat	k5eAaImIp3nP	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
řídícím	řídící	k2eAgNnSc7d1	řídící
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
rodě	rod	k1gInSc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oslabování	oslabování	k1gNnSc3	oslabování
shody	shoda	k1gFnSc2	shoda
a	a	k8xC	a
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
období	období	k1gNnSc6	období
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
tvar	tvar	k1gInSc1	tvar
sg.	sg.	k?	sg.
m.	m.	k?	m.
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
osoby	osoba	k1gFnPc4	osoba
i	i	k8xC	i
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Přechodník	přechodník	k1gInSc1	přechodník
přítomný	přítomný	k1gMnSc1	přítomný
je	být	k5eAaImIp3nS	být
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
současnosti	současnost	k1gFnSc2	současnost
dvou	dva	k4xCgInPc2	dva
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
od	od	k7c2	od
prézentního	prézentní	k2eAgInSc2d1	prézentní
kmene	kmen	k1gInSc2	kmen
koncovkami	koncovka	k1gFnPc7	koncovka
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-uči	či	k1gNnPc1	-uči
<g/>
,	,	kIx,	,
-uče	č	k1gInPc1	-uč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
м	м	k?	м
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
,	,	kIx,	,
м	м	k?	м
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
variantě	varianta	k1gFnSc6	varianta
-ja	a	k?	-ja
<g/>
,	,	kIx,	,
-jači	ač	k1gInSc6	-jač
<g/>
,	,	kIx,	,
-jače	ače	k1gNnSc1	-jače
(	(	kIx(	(
<g/>
např.	např.	kA	např.
х	х	k?	х
<g/>
,	,	kIx,	,
х	х	k?	х
<g/>
,	,	kIx,	,
х	х	k?	х
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
а	а	k?	а
н	н	k?	н
в	в	k?	в
н	н	k?	н
ж	ж	k?	ж
а	а	k?	а
м	м	k?	м
н	н	k?	н
в	в	k?	в
н	н	k?	н
ж	ж	k?	ж
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k9	a
nyní	nyní	k6eAd1	nyní
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
novou	nový	k2eAgFnSc4d1	nová
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
mně	já	k3xPp1nSc3	já
nedá	dát	k5eNaPmIp3nS	dát
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
9	[number]	k4	9
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Přechodník	přechodník	k1gInSc4	přechodník
minulý	minulý	k2eAgInSc4d1	minulý
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
předčasnosti	předčasnost	k1gFnSc2	předčasnost
jednoho	jeden	k4xCgInSc2	jeden
děje	děj	k1gInSc2	děj
před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
od	od	k7c2	od
infinitivního	infinitivní	k2eAgInSc2d1	infinitivní
kmene	kmen	k1gInSc2	kmen
koncovkami	koncovka	k1gFnPc7	koncovka
-ъ	-ъ	k?	-ъ
<g/>
,	,	kIx,	,
-ъ	-ъ	k?	-ъ
<g/>
,	,	kIx,	,
-ъ	-ъ	k?	-ъ
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
,	,	kIx,	,
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
hiátové	hiátový	k2eAgFnSc2d1	hiátová
-v-	-	k?	-v-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
б	б	k?	б
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
п	п	k?	п
д	д	k?	д
и	и	k?	и
ж	ж	k?	ж
с	с	k?	с
–	–	k?	–
"	"	kIx"	"
<g/>
Prodejte	prodat	k5eAaPmRp2nP	prodat
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
pojďte	jít	k5eAaImRp2nP	jít
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
424	[number]	k4	424
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Příčestí	příčestí	k1gNnSc1	příčestí
činné	činný	k2eAgFnPc1d1	činná
(	(	kIx(	(
<g/>
perfekta	perfektum	k1gNnPc1	perfektum
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
složených	složený	k2eAgInPc2d1	složený
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
a	a	k8xC	a
kondicionálu	kondicionál	k1gInSc2	kondicionál
<g/>
.	.	kIx.	.
</s>
<s>
Odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
se	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-l-	-	k?	-l-
od	od	k7c2	od
infinitivního	infinitivní	k2eAgInSc2d1	infinitivní
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
rodě	rod	k1gInSc6	rod
(	(	kIx(	(
<g/>
sg.	sg.	k?	sg.
m.	m.	k?	m.
п	п	k?	п
<g/>
,	,	kIx,	,
sg.	sg.	k?	sg.
f.	f.	k?	f.
п	п	k?	п
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
m.	m.	k?	m.
п	п	k?	п
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
а	а	k?	а
п	п	k?	п
є	є	k?	є
о	о	k?	о
з	з	k?	з
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k8xC	a
přišli	přijít	k5eAaPmAgMnP	přijít
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvonili	zvonit	k5eAaImAgMnP	zvonit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
605	[number]	k4	605
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Příčestí	příčestí	k1gNnSc1	příčestí
trpné	trpný	k2eAgNnSc1d1	trpné
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
opisného	opisný	k2eAgNnSc2d1	opisné
pasiva	pasivum	k1gNnSc2	pasivum
<g/>
.	.	kIx.	.
</s>
<s>
Odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
se	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
-n-	-	k?	-n-
nebo	nebo	k8xC	nebo
-t-	-	k?	-t-
od	od	k7c2	od
infinitivního	infinitivní	k2eAgInSc2d1	infinitivní
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
rodě	rod	k1gInSc6	rod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
п	п	k?	п
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
в	в	k?	в
к	к	k?	к
3	[number]	k4	3
п	п	k?	п
–	–	k?	–
"	"	kIx"	"
<g/>
Vosku	vosk	k1gInSc2	vosk
jsou	být	k5eAaImIp3nP	být
koupeny	koupen	k2eAgFnPc1d1	koupena
3	[number]	k4	3
kapi	kapi	k1gNnPc2	kapi
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
439	[number]	k4	439
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Od	od	k7c2	od
uvedených	uvedený	k2eAgInPc2d1	uvedený
participiálních	participiální	k2eAgInPc2d1	participiální
tvarů	tvar	k1gInPc2	tvar
lze	lze	k6eAd1	lze
tvořit	tvořit	k5eAaImF	tvořit
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
slovesná	slovesný	k2eAgFnSc1d1	slovesná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
м	м	k?	м
→	→	k?	→
м	м	k?	м
<g/>
,	,	kIx,	,
б	б	k?	б
→	→	k?	→
б	б	k?	б
<g/>
,	,	kIx,	,
д	д	k?	д
→	→	k?	→
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc1	přívlastek
může	moct	k5eAaImIp3nS	moct
zaujímat	zaujímat	k5eAaImF	zaujímat
postavení	postavení	k1gNnSc4	postavení
jak	jak	k8xS	jak
před	před	k7c7	před
řídícím	řídící	k2eAgNnSc7d1	řídící
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
případné	případný	k2eAgFnSc2d1	případná
mnohoznačnosti	mnohoznačnost	k1gFnSc2	mnohoznačnost
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
výrazně	výrazně	k6eAd1	výrazně
omezen	omezit	k5eAaPmNgInS	omezit
pravidly	pravidlo	k1gNnPc7	pravidlo
tzv.	tzv.	kA	tzv.
opakování	opakování	k1gNnSc3	opakování
předložek	předložka	k1gFnPc2	předložka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
předložka	předložka	k1gFnSc1	předložka
opakuje	opakovat	k5eAaImIp3nS	opakovat
před	před	k7c7	před
všemi	všecek	k3xTgInPc7	všecek
shodnými	shodný	k2eAgInPc7d1	shodný
přívlastky	přívlastek	k1gInPc7	přívlastek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
neužívá	užívat	k5eNaImIp3nS	užívat
po	po	k7c6	po
přívlastku	přívlastek	k1gInSc6	přívlastek
v	v	k7c6	v
kontaktní	kontaktní	k2eAgFnSc6d1	kontaktní
prepozici	prepozice	k1gFnSc6	prepozice
před	před	k7c7	před
řídícím	řídící	k2eAgNnSc7d1	řídící
jménem	jméno	k1gNnSc7	jméno
<g/>
:	:	kIx,	:
ѹ	ѹ	k?	ѹ
м	м	k?	м
р	р	k?	р
–	–	k?	–
"	"	kIx"	"
<g/>
u	u	k7c2	u
mého	můj	k3xOp1gInSc2	můj
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
748	[number]	k4	748
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
к	к	k?	к
п	п	k?	п
к	к	k?	к
в	в	k?	в
–	–	k?	–
"	"	kIx"	"
<g/>
k	k	k7c3	k
velkému	velký	k2eAgMnSc3d1	velký
posadnikovi	posadnik	k1gMnSc3	posadnik
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
831	[number]	k4	831
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
з	з	k?	з
о	о	k?	о
з	з	k?	з
м	м	k?	м
з	з	k?	з
с	с	k?	с
–	–	k?	–
"	"	kIx"	"
<g/>
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
mého	můj	k1gMnSc2	můj
otčíma	otčím	k1gMnSc2	otčím
Semjona	Semjon	k1gMnSc2	Semjon
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
závěť	závěť	k1gFnSc1	závěť
Matfeje	Matfej	k1gMnSc2	Matfej
Vasiljeviče	Vasiljevič	k1gMnSc2	Vasiljevič
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
a	a	k8xC	a
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
bez	bez	k7c2	bez
předložky	předložka	k1gFnSc2	předložka
také	také	k9	také
v	v	k7c6	v
kontaktní	kontaktní	k2eAgFnSc6d1	kontaktní
postpozici	postpozice	k1gFnSc6	postpozice
<g/>
.	.	kIx.	.
</s>
<s>
Předložka	předložka	k1gFnSc1	předložka
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
neopakuje	opakovat	k5eNaImIp3nS	opakovat
ani	ani	k8xC	ani
před	před	k7c7	před
neshodným	shodný	k2eNgInSc7d1	neshodný
přívlastkem	přívlastek	k1gInSc7	přívlastek
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
případy	případ	k1gInPc7	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
roztržené	roztržený	k2eAgNnSc4d1	roztržené
–	–	k?	–
dovnitř	dovnitř	k6eAd1	dovnitř
je	být	k5eAaImIp3nS	být
vklíněno	vklíněn	k2eAgNnSc1d1	vklíněno
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
či	či	k8xC	či
skupina	skupina	k1gFnSc1	skupina
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovním	slovní	k2eAgNnSc6d1	slovní
spojení	spojení	k1gNnSc6	spojení
nezávislé	závislý	k2eNgInPc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc1d3	nejčastější
příčina	příčina	k1gFnSc1	příčina
narušení	narušení	k1gNnSc2	narušení
projektivity	projektivita	k1gFnSc2	projektivita
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
Wackernagelova	Wackernagelův	k2eAgInSc2d1	Wackernagelův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnSc4	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
taktovou	taktový	k2eAgFnSc4d1	taktová
skupinu	skupina	k1gFnSc4	skupina
jsou	být	k5eAaImIp3nP	být
čistě	čistě	k6eAd1	čistě
mechanicky	mechanicky	k6eAd1	mechanicky
vsouvány	vsouván	k2eAgFnPc4d1	vsouván
příklonky	příklonka	k1gFnPc4	příklonka
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
existuje	existovat	k5eAaImIp3nS	existovat
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
vazba	vazba	k1gFnSc1	vazba
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
:	:	kIx,	:
м	м	k?	м
ж	ж	k?	ж
в	в	k?	в
л	л	k?	л
–	–	k?	–
"	"	kIx"	"
<g/>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
léta	léto	k1gNnPc1	léto
vám	vy	k3xPp2nPc3	vy
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
503	[number]	k4	503
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
с	с	k?	с
ж	ж	k?	ж
т	т	k?	т
ѡ	ѡ	k?	ѡ
н	н	k?	н
н	н	k?	н
–	–	k?	–
"	"	kIx"	"
<g/>
Nepotřebuješ	potřebovat	k5eNaImIp2nS	potřebovat
Svjatoslava	Svjatoslava	k1gFnSc1	Svjatoslava
Olgoviče	Olgovič	k1gMnSc2	Olgovič
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
Ipaťjevský	Ipaťjevský	k2eAgInSc1d1	Ipaťjevský
letopis	letopis	k1gInSc1	letopis
<g/>
:	:	kIx,	:
Kyjevský	kyjevský	k2eAgInSc1d1	kyjevský
letopis	letopis	k1gInSc1	letopis
<g/>
,	,	kIx,	,
1151	[number]	k4	1151
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
zachovávání	zachovávání	k1gNnSc2	zachovávání
Wackernagelova	Wackernagelův	k2eAgInSc2d1	Wackernagelův
zákona	zákon	k1gInSc2	zákon
lze	lze	k6eAd1	lze
příklonky	příklonka	k1gFnPc1	příklonka
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
silné	silný	k2eAgFnSc2d1	silná
–	–	k?	–
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
Wackernagelův	Wackernagelův	k2eAgInSc4d1	Wackernagelův
zákon	zákon	k1gInSc4	zákon
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
narušení	narušení	k1gNnSc2	narušení
principu	princip	k1gInSc2	princip
členské	členský	k2eAgFnSc2d1	členská
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
(	(	kIx(	(
<g/>
ж	ж	k?	ж
<g/>
,	,	kIx,	,
л	л	k?	л
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
т	т	k?	т
<g/>
,	,	kIx,	,
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
slabé	slabý	k2eAgFnPc4d1	slabá
–	–	k?	–
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
Wackernagelova	Wackernagelův	k2eAgInSc2d1	Wackernagelův
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgFnPc1d1	častá
(	(	kIx(	(
<g/>
zájmena	zájmeno	k1gNnSc2	zájmeno
v	v	k7c6	v
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c4	v
akuz	akuz	k1gInSc4	akuz
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
prézens	prézens	k1gNnSc7	prézens
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Příklonné	příklonný	k2eAgInPc4d1	příklonný
tvary	tvar	k1gInPc4	tvar
pro	pro	k7c4	pro
3	[number]	k4	3
<g/>
.	.	kIx.	.
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
akuzativních	akuzativní	k2eAgInPc2d1	akuzativní
tvarů	tvar	k1gInPc2	tvar
zájmena	zájmeno	k1gNnSc2	zájmeno
*	*	kIx~	*
<g/>
jь	jь	k?	jь
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rod	rod	k1gInSc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Plnopřízvučnou	Plnopřízvučný	k2eAgFnSc4d1	Plnopřízvučný
funkci	funkce	k1gFnSc4	funkce
zde	zde	k6eAd1	zde
přebraly	přebrat	k5eAaPmAgInP	přebrat
původní	původní	k2eAgInSc4d1	původní
tvary	tvar	k1gInPc4	tvar
genitivu	genitiv	k1gInSc2	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytlačení	vytlačení	k1gNnSc3	vytlačení
slabých	slabý	k2eAgFnPc2d1	slabá
příklonek	příklonka	k1gFnPc2	příklonka
plnopřízvučnými	plnopřízvučný	k2eAgInPc7d1	plnopřízvučný
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
předhistorické	předhistorický	k2eAgFnSc6d1	předhistorická
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
částici	částice	k1gFnSc6	částice
změnilo	změnit	k5eAaPmAgNnS	změnit
zvratné	zvratný	k2eAgNnSc1d1	zvratné
zájmeno	zájmeno	k1gNnSc1	zájmeno
с	с	k?	с
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
původní	původní	k2eAgFnSc6d1	původní
funkci	funkce	k1gFnSc6	funkce
doloženo	doložit	k5eAaPmNgNnS	doložit
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brzké	brzký	k2eAgFnSc6d1	brzká
době	doba	k1gFnSc6	doba
začíná	začínat	k5eAaImIp3nS	začínat
též	též	k9	též
velmi	velmi	k6eAd1	velmi
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
proces	proces	k1gInSc1	proces
připojování	připojování	k1gNnSc2	připojování
zájmena	zájmeno	k1gNnSc2	zájmeno
с	с	k?	с
k	k	k7c3	k
řídícímu	řídící	k2eAgNnSc3d1	řídící
slovesu	sloveso	k1gNnSc3	sloveso
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
dokončen	dokončit	k5eAaPmNgInS	dokončit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
začaly	začít	k5eAaPmAgInP	začít
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
mizet	mizet	k5eAaImF	mizet
příklonky	příklonka	k1gFnPc1	příklonka
dativu	dativ	k1gInSc2	dativ
plurálu	plurál	k1gInSc2	plurál
a	a	k8xC	a
duálu	duál	k1gInSc2	duál
patrně	patrně	k6eAd1	patrně
kvůli	kvůli	k7c3	kvůli
homonymii	homonymie	k1gFnSc3	homonymie
s	s	k7c7	s
akuzativními	akuzativní	k2eAgInPc7d1	akuzativní
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
zanikají	zanikat	k5eAaImIp3nP	zanikat
zájmenné	zájmenný	k2eAgFnPc4d1	zájmenná
příklonky	příklonka	k1gFnPc4	příklonka
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
a	a	k8xC	a
duálu	duál	k1gInSc6	duál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
přestávají	přestávat	k5eAaImIp3nP	přestávat
mít	mít	k5eAaImF	mít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
živém	živý	k2eAgInSc6d1	živý
jazyce	jazyk	k1gInSc6	jazyk
i	i	k8xC	i
příklonná	příklonný	k2eAgNnPc1d1	příklonné
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c6	v
singuláru	singulár	k1gInSc6	singulár
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
být	být	k5eAaImF	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
knižní	knižní	k2eAgInSc4d1	knižní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
zánik	zánik	k1gInSc1	zánik
těchto	tento	k3xDgFnPc2	tento
příklonek	příklonka	k1gFnPc2	příklonka
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
vymizely	vymizet	k5eAaPmAgInP	vymizet
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
také	také	k9	také
příklonné	příklonný	k2eAgInPc4d1	příklonný
tvary	tvar	k1gInPc4	tvar
prézentu	prézens	k1gInSc3	prézens
slovesa	sloveso	k1gNnSc2	sloveso
б	б	k?	б
<g/>
.	.	kIx.	.
</s>
<s>
Projektivita	Projektivita	k1gFnSc1	Projektivita
konstrukcí	konstrukce	k1gFnPc2	konstrukce
bývá	bývat	k5eAaImIp3nS	bývat
nezřídka	nezřídka	k6eAd1	nezřídka
narušena	narušit	k5eAaPmNgFnS	narušit
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
upřesnění	upřesnění	k1gNnSc4	upřesnění
<g/>
:	:	kIx,	:
а	а	k?	а
с	с	k?	с
п	п	k?	п
н	н	k?	н
в	в	k?	в
н	н	k?	н
с	с	k?	с
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k8xC	a
syna	syn	k1gMnSc4	syn
Vsevoloda	Vsevolod	k1gMnSc4	Vsevolod
dosadil	dosadit	k5eAaPmAgMnS	dosadit
na	na	k7c4	na
stolec	stolec	k1gInSc4	stolec
v	v	k7c6	v
Novgorodě	Novgorod	k1gInSc6	Novgorod
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
Synodální	synodální	k2eAgInSc1d1	synodální
rukopis	rukopis	k1gInSc1	rukopis
<g/>
:	:	kIx,	:
Novgorodský	novgorodský	k2eAgInSc1d1	novgorodský
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgInSc1	první
letopis	letopis	k1gInSc1	letopis
<g/>
,	,	kIx,	,
1117	[number]	k4	1117
<g/>
]	]	kIx)	]
в	в	k?	в
ѹ	ѹ	k?	ѹ
т	т	k?	т
о	о	k?	о
н	н	k?	н
д	д	k?	д
г	г	k?	г
ѹ	ѹ	k?	ѹ
в	в	k?	в
ш	ш	k?	ш
–	–	k?	–
"	"	kIx"	"
<g/>
Vezmi	vzít	k5eAaPmRp2nS	vzít
od	od	k7c2	od
Timošky	Timoška	k1gFnSc2	Timoška
<g/>
,	,	kIx,	,
Vojcinova	Vojcinův	k2eAgMnSc2d1	Vojcinův
švagra	švagr	k1gMnSc2	švagr
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc4	jedenáct
hřiven	hřivna	k1gFnPc2	hřivna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
dopis	dopis	k1gInSc1	dopis
č.	č.	k?	č.
78	[number]	k4	78
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
]	]	kIx)	]
Ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
anomální	anomální	k2eAgInSc4d1	anomální
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
projektivity	projektivita	k1gFnSc2	projektivita
je	být	k5eAaImIp3nS	být
normou	norma	k1gFnSc7	norma
knižního	knižní	k2eAgInSc2d1	knižní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hovorová	hovorový	k2eAgFnSc1d1	hovorová
řeč	řeč	k1gFnSc1	řeč
dává	dávat	k5eAaImIp3nS	dávat
obvykle	obvykle	k6eAd1	obvykle
přednost	přednost	k1gFnSc4	přednost
jinému	jiný	k2eAgInSc3d1	jiný
principu	princip	k1gInSc3	princip
–	–	k?	–
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
upřesnění	upřesnění	k1gNnSc4	upřesnění
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
pochází	pocházet	k5eAaImIp3nS	pocházet
jádro	jádro	k1gNnSc1	jádro
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
staré	starý	k2eAgFnSc2d1	stará
ruštiny	ruština	k1gFnSc2	ruština
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
její	její	k3xOp3gNnPc1	její
slova	slovo	k1gNnSc2	slovo
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
archaický	archaický	k2eAgInSc4d1	archaický
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
в	в	k?	в
"	"	kIx"	"
<g/>
obytný	obytný	k2eAgInSc4d1	obytný
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
stan	stan	k1gInSc4	stan
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
с	с	k?	с
"	"	kIx"	"
<g/>
majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc1	peníz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
původní	původní	k2eAgMnPc1d1	původní
specificky	specificky	k6eAd1	specificky
východoslovanská	východoslovanský	k2eAgNnPc4d1	východoslovanské
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
б	б	k?	б
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
д	д	k?	д
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
)	)	kIx)	)
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
přejatá	přejatý	k2eAgFnSc1d1	přejatá
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
jazykových	jazykový	k2eAgFnPc2d1	jazyková
rodin	rodina	k1gFnPc2	rodina
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Kiparskij	Kiparskij	k1gMnSc1	Kiparskij
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
monografii	monografie	k1gFnSc6	monografie
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
historii	historie	k1gFnSc6	historie
ruské	ruský	k2eAgFnSc2d1	ruská
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruština	ruština	k1gFnSc1	ruština
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
454	[number]	k4	454
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
epochy	epocha	k1gFnSc2	epocha
<g/>
,	,	kIx,	,
cca	cca	kA	cca
300	[number]	k4	300
z	z	k7c2	z
baltoslovanské	baltoslovanský	k2eAgFnSc2d1	baltoslovanská
epochy	epocha	k1gFnSc2	epocha
a	a	k8xC	a
420	[number]	k4	420
z	z	k7c2	z
všeslovanské	všeslovanský	k2eAgFnSc2d1	všeslovanská
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
lexikální	lexikální	k2eAgFnPc4d1	lexikální
výpůjčky	výpůjčka	k1gFnPc4	výpůjčka
z	z	k7c2	z
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
–	–	k?	–
55	[number]	k4	55
germanismů	germanismus	k1gInPc2	germanismus
<g/>
,	,	kIx,	,
cca	cca	kA	cca
20	[number]	k4	20
íránismů	íránismus	k1gInPc2	íránismus
a	a	k8xC	a
23	[number]	k4	23
turcismy	turcismus	k1gInPc7	turcismus
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
nebereme	brát	k5eNaImIp1nP	brát
<g/>
-li	i	k?	-li
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
odvozená	odvozený	k2eAgFnSc1d1	odvozená
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
zdědila	zdědit	k5eAaPmAgFnS	zdědit
stará	starý	k2eAgFnSc1d1	stará
ruština	ruština	k1gFnSc1	ruština
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
přibližně	přibližně	k6eAd1	přibližně
1270	[number]	k4	1270
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
roku	rok	k1gInSc2	rok
988	[number]	k4	988
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
knižního	knižní	k2eAgInSc2d1	knižní
církevněslovanského	církevněslovanský	k2eAgInSc2d1	církevněslovanský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
staré	starý	k2eAgFnSc2d1	stará
ruštiny	ruština	k1gFnSc2	ruština
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
znaků	znak	k1gInPc2	znak
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
:	:	kIx,	:
mezisouhláskové	mezisouhlásek	k1gMnPc1	mezisouhlásek
*	*	kIx~	*
<g/>
or	or	k?	or
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ol	ol	k?	ol
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
er	er	k?	er
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
el	ela	k1gFnPc2	ela
→	→	k?	→
ra	ra	k0	ra
<g/>
,	,	kIx,	,
la	la	k0	la
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
re	re	k9	re
<g/>
,	,	kIx,	,
le	le	k?	le
(	(	kIx(	(
<g/>
csl.	csl.	k?	csl.
г	г	k?	г
<g/>
,	,	kIx,	,
с	с	k?	с
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
г	г	k?	г
<g/>
,	,	kIx,	,
с	с	k?	с
<g/>
)	)	kIx)	)
počáteční	počáteční	k2eAgFnSc4d1	počáteční
*	*	kIx~	*
<g/>
or-	or-	k?	or-
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
ol-	ol-	k?	ol-
→	→	k?	→
ra-	ra-	k?	ra-
<g/>
,	,	kIx,	,
la-	la-	k?	la-
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
intonaci	intonace	k1gFnSc6	intonace
(	(	kIx(	(
<g/>
csl.	csl.	k?	csl.
р	р	k?	р
<g/>
,	,	kIx,	,
л	л	k?	л
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
р	р	k?	р
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
л	л	k?	л
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
*	*	kIx~	*
<g/>
tj	tj	kA	tj
<g/>
,	,	kIx,	,
*	*	kIx~	*
<g/>
dj	dj	k?	dj
→	→	k?	→
šč	šč	k?	šč
<g/>
,	,	kIx,	,
žd	žd	k?	žd
(	(	kIx(	(
<g/>
csl.	csl.	k?	csl.
п	п	k?	п
<g/>
,	,	kIx,	,
в	в	k?	в
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
п	п	k?	п
<g/>
,	,	kIx,	,
в	в	k?	в
<g/>
)	)	kIx)	)
změna	změna	k1gFnSc1	změna
*	*	kIx~	*
<g/>
je	být	k5eAaImIp3nS	být
→	→	k?	→
o	o	k7c4	o
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
(	(	kIx(	(
<g/>
csl.	csl.	k?	csl.
ѥ	ѥ	k?	ѥ
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
о	о	k?	о
<g/>
)	)	kIx)	)
pádové	pádový	k2eAgFnSc2d1	pádová
koncovky	koncovka	k1gFnSc2	koncovka
*	*	kIx~	*
<g/>
ę	ę	k?	ę
<g/>
̆	̆	k?	̆
→	→	k?	→
ja	ja	k?	ja
(	(	kIx(	(
<g/>
csl.	csl.	k?	csl.
о	о	k?	о
з	з	k?	з
×	×	k?	×
strus	strusa	k1gFnPc2	strusa
<g/>
.	.	kIx.	.
о	о	k?	о
з	з	k?	з
<g/>
)	)	kIx)	)
používání	používání	k1gNnSc1	používání
prostých	prostý	k2eAgInPc2d1	prostý
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
zájmeno	zájmeno	k1gNnSc1	zájmeno
с	с	k?	с
za	za	k7c7	za
řídícím	řídící	k2eAgNnSc7d1	řídící
slovesem	sloveso	k1gNnSc7	sloveso
Určit	určit	k5eAaPmF	určit
vztah	vztah	k1gInSc4	vztah
knižního	knižní	k2eAgInSc2d1	knižní
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
mluvených	mluvený	k2eAgInPc2d1	mluvený
dialektů	dialekt	k1gInPc2	dialekt
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
složité	složitý	k2eAgNnSc1d1	složité
<g/>
,	,	kIx,	,
památky	památka	k1gFnPc1	památka
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
oba	dva	k4xCgInPc4	dva
elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgFnSc2d1	blízká
hovorové	hovorový	k2eAgFnSc2d1	hovorová
řeči	řeč	k1gFnSc2	řeč
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
dopisy	dopis	k1gInPc1	dopis
na	na	k7c6	na
březové	březový	k2eAgFnSc6d1	Březová
kůře	kůra	k1gFnSc6	kůra
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
řeč	řeč	k1gFnSc1	řeč
světských	světský	k2eAgFnPc2d1	světská
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
letopise	letopis	k1gInSc6	letopis
Kyjevském	kyjevský	k2eAgInSc6d1	kyjevský
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
#	#	kIx~	#
<g/>
Literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
DERKSEN	DERKSEN	kA	DERKSEN
<g/>
,	,	kIx,	,
Rick	Rick	k1gInSc1	Rick
<g/>
.	.	kIx.	.
</s>
<s>
Etymological	Etymologicat	k5eAaPmAgMnS	Etymologicat
Dictionary	Dictionar	k1gMnPc4	Dictionar
of	of	k?	of
the	the	k?	the
Slavic	slavice	k1gFnPc2	slavice
Inherited	Inherited	k1gMnSc1	Inherited
Lexicon	Lexicon	k1gMnSc1	Lexicon
<g/>
.	.	kIx.	.
</s>
<s>
Leiden	Leidna	k1gFnPc2	Leidna
:	:	kIx,	:
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
726	[number]	k4	726
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
15504	[number]	k4	15504
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
DURNOVO	DURNOVO	kA	DURNOVO
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
.	.	kIx.	.
</s>
<s>
Izbrannye	Izbrannye	k1gInSc1	Izbrannye
raboty	rabota	k1gFnSc2	rabota
po	po	k7c6	po
istorii	istorie	k1gFnSc6	istorie
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Jazyki	Jazyk	k1gFnPc1	Jazyk
slavjanskoj	slavjanskoj	k1gInSc4	slavjanskoj
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
811	[number]	k4	811
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7859	[number]	k4	7859
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
DYBO	DYBO	kA	DYBO
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Antonovič	Antonovič	k1gMnSc1	Antonovič
<g/>
.	.	kIx.	.
</s>
<s>
Slavjanskaja	Slavjanskaja	k1gMnSc1	Slavjanskaja
akcentologija	akcentologija	k1gMnSc1	akcentologija
<g/>
:	:	kIx,	:
Opyt	Opyt	k1gMnSc1	Opyt
rekonstrukcii	rekonstrukcie	k1gFnSc4	rekonstrukcie
sistemy	sistema	k1gFnSc2	sistema
akcentnych	akcentnycha	k1gFnPc2	akcentnycha
paradigm	paradig	k1gNnSc7	paradig
v	v	k7c4	v
praslavjanskom	praslavjanskom	k1gInSc4	praslavjanskom
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
FILIN	FILIN	kA	FILIN
<g/>
,	,	kIx,	,
Fedot	Fedot	k1gMnSc1	Fedot
Petrovič	Petrovič	k1gMnSc1	Petrovič
<g/>
.	.	kIx.	.
</s>
<s>
Problemy	Problema	k1gFnPc1	Problema
istoričeskoj	istoričeskoj	k1gInSc1	istoričeskoj
leksikologii	leksikologie	k1gFnSc3	leksikologie
russkogo	russkogo	k1gNnSc1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
GORŠKOVA	GORŠKOVA	kA	GORŠKOVA
<g/>
,	,	kIx,	,
Klavdija	Klavdij	k2eAgFnSc1d1	Klavdija
Vasiljevna	Vasiljevna	k1gFnSc1	Vasiljevna
<g/>
;	;	kIx,	;
CHABURGAJEV	CHABURGAJEV	kA	CHABURGAJEV
<g/>
,	,	kIx,	,
Georgij	Georgij	k1gMnSc1	Georgij
Aleksandrovič	Aleksandrovič	k1gMnSc1	Aleksandrovič
<g/>
.	.	kIx.	.
</s>
<s>
Istoričeskaja	Istoričeskaj	k2eAgFnSc1d1	Istoričeskaj
grammatika	grammatika	k1gFnSc1	grammatika
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Vysšaja	Vysšaj	k2eAgFnSc1d1	Vysšaj
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
359	[number]	k4	359
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
GRICKEVIČ	GRICKEVIČ	kA	GRICKEVIČ
<g/>
,	,	kIx,	,
Julija	Julij	k2eAgFnSc1d1	Julija
Nikolajevna	Nikolajevna	k1gFnSc1	Nikolajevna
<g/>
.	.	kIx.	.
</s>
<s>
Istoričeskaja	Istoričeskaj	k2eAgFnSc1d1	Istoričeskaj
grammatika	grammatika	k1gFnSc1	grammatika
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
posobije	posobít	k5eAaPmIp3nS	posobít
dlja	dlja	k1gFnSc1	dlja
studentov	studentov	k1gInSc1	studentov
zaočnogo	zaočnogo	k1gMnSc1	zaočnogo
otdelenija	otdelenija	k1gMnSc1	otdelenija
<g/>
.	.	kIx.	.
</s>
<s>
Pskov	Pskov	k1gInSc1	Pskov
:	:	kIx,	:
PGPI	PGPI	kA	PGPI
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
M.	M.	kA	M.
Kirova	Kirovo	k1gNnPc4	Kirovo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
100	[number]	k4	100
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
87854	[number]	k4	87854
<g/>
-	-	kIx~	-
<g/>
237	[number]	k4	237
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
CHABURGAJEV	CHABURGAJEV	kA	CHABURGAJEV
<g/>
,	,	kIx,	,
Georgij	Georgij	k1gMnSc1	Georgij
Aleksandrovič	Aleksandrovič	k1gMnSc1	Aleksandrovič
<g/>
.	.	kIx.	.
</s>
<s>
Stanovlenie	Stanovlenie	k1gFnSc1	Stanovlenie
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Vysšaja	Vysšaj	k2eAgFnSc1d1	Vysšaj
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Vsevolodovič	Vsevolodovič	k1gMnSc1	Vsevolodovič
<g/>
.	.	kIx.	.
</s>
<s>
Istoričeskaja	Istoričeskaj	k2eAgFnSc1d1	Istoričeskaj
grammatika	grammatika	k1gFnSc1	grammatika
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Prosveščenie	Prosveščenie	k1gFnSc1	Prosveščenie
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
400	[number]	k4	400
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
910	[number]	k4	910
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
IVANOVA	Ivanův	k2eAgFnSc1d1	Ivanova
<g/>
,	,	kIx,	,
T.	T.	kA	T.
A.	A.	kA	A.
<g/>
.	.	kIx.	.
</s>
<s>
Ob	Ob	k1gInSc1	Ob
azbuke	azbukat	k5eAaPmIp3nS	azbukat
na	na	k7c4	na
stene	sten	k1gInSc5	sten
sofijskogo	sofijskogo	k6eAd1	sofijskogo
sobora	sobora	k1gFnSc1	sobora
v	v	k7c4	v
Kieve	Kieev	k1gFnPc4	Kieev
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
118	[number]	k4	118
<g/>
-	-	kIx~	-
<g/>
122	[number]	k4	122
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
JANIN	Janin	k2eAgMnSc1d1	Janin
<g/>
,	,	kIx,	,
Valentin	Valentin	k1gMnSc1	Valentin
Lavrenťjevič	Lavrenťjevič	k1gMnSc1	Lavrenťjevič
<g/>
.	.	kIx.	.
</s>
<s>
Středověký	středověký	k2eAgInSc1d1	středověký
Novgorod	Novgorod	k1gInSc1	Novgorod
v	v	k7c6	v
nápisech	nápis	k1gInPc6	nápis
na	na	k7c6	na
březové	březový	k2eAgFnSc6d1	Březová
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jitka	Jitka	k1gFnSc1	Jitka
Komendová	Komendová	k1gFnSc1	Komendová
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Mervart	Mervarta	k1gFnPc2	Mervarta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
378	[number]	k4	378
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86818	[number]	k4	86818
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KRIVKO	KRIVKO	kA	KRIVKO
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
.	.	kIx.	.
</s>
<s>
Drevnerusskaja	Drevnerusskaj	k2eAgFnSc1d1	Drevnerusskaj
orfografija	orfografija	k1gFnSc1	orfografija
XI	XI	kA	XI
-	-	kIx~	-
načala	načít	k5eAaPmAgFnS	načít
XII	XII	kA	XII
veka	veka	k1gFnSc1	veka
v	v	k7c6	v
svete	svete	k5eAaPmIp2nP	svete
supersegmentnych	supersegmentnych	k1gMnSc1	supersegmentnych
tembrovych	tembrovych	k1gMnSc1	tembrovych
oppozicij	oppozicít	k5eAaPmRp2nS	oppozicít
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
LAMPRECHT	LAMPRECHT	kA	LAMPRECHT
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
266	[number]	k4	266
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
MALKOVA	MALKOVA	kA	MALKOVA
<g/>
,	,	kIx,	,
O.	O.	kA	O.
V.	V.	kA	V.
Imelo	Imelo	k1gNnSc4	Imelo
li	li	k8xS	li
mesto	mesto	k1gNnSc4	mesto
vtoričnoe	vtorično	k1gFnSc2	vtorično
smjagčenie	smjagčenie	k1gFnSc2	smjagčenie
soglasnych	soglasnych	k1gMnSc1	soglasnych
pered	pered	k1gMnSc1	pered
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
i	i	k8xC	i
<g/>
]	]	kIx)	]
v	v	k7c4	v
dialektach	dialektach	k1gInSc4	dialektach
južnoj	južnoj	k1gInSc4	južnoj
zony	zona	k1gFnSc2	zona
drevnerusskogo	drevnerusskogo	k1gNnSc4	drevnerusskogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
76	[number]	k4	76
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
MALKOVA	MALKOVA	kA	MALKOVA
<g/>
,	,	kIx,	,
O.	O.	kA	O.
V.	V.	kA	V.
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
istorii	istorie	k1gFnSc3	istorie
obrazovanija	obrazovanija	k1gFnSc1	obrazovanija
vostočnoslavjanskich	vostočnoslavjanskich	k1gMnSc1	vostočnoslavjanskich
jazykov	jazykov	k1gInSc1	jazykov
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
MICHEJEV	MICHEJEV	kA	MICHEJEV
<g/>
,	,	kIx,	,
Savva	Savva	k1gFnSc1	Savva
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
<g/>
.	.	kIx.	.
22	[number]	k4	22
drevnerusskich	drevnerusskich	k1gMnSc1	drevnerusskich
glagoličeskich	glagoličeskich	k1gMnSc1	glagoličeskich
nadpisi-graffiti	nadpisiraffit	k5eAaImF	nadpisi-graffit
XI-XII	XI-XII	k1gFnSc7	XI-XII
vekov	vekovo	k1gNnPc2	vekovo
iz	iz	k?	iz
Novgoroda	Novgoroda	k1gMnSc1	Novgoroda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
62	[number]	k4	62
<g/>
,	,	kIx,	,
s.	s.	k?	s.
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
NAZAROVA	NAZAROVA	kA	NAZAROVA
<g/>
,	,	kIx,	,
T.	T.	kA	T.
V.	V.	kA	V.
<g/>
.	.	kIx.	.
</s>
<s>
Akaňje	Akaňje	k1gFnSc1	Akaňje
v	v	k7c6	v
ukrainskich	ukrainski	k1gFnPc6	ukrainski
govorach	govoracha	k1gFnPc2	govoracha
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
211	[number]	k4	211
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
NIKOLAJEV	NIKOLAJEV	kA	NIKOLAJEV
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Lvovič	Lvovič	k1gMnSc1	Lvovič
<g/>
.	.	kIx.	.
</s>
<s>
Sledy	sled	k1gInPc1	sled
osobennostej	osobennostat	k5eAaPmRp2nS	osobennostat
vostočnoslavjanskich	vostočnoslavjanskich	k1gMnSc1	vostočnoslavjanskich
plemennych	plemennych	k1gMnSc1	plemennych
dialektov	dialektov	k1gInSc4	dialektov
v	v	k7c6	v
sovremennych	sovremenny	k1gFnPc6	sovremenny
velikorusskich	velikorusskich	k1gMnSc1	velikorusskich
govorach	govorach	k1gMnSc1	govorach
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
Kriviči	Krivič	k1gInPc7	Krivič
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Vsevolodovič	Vsevolodovič	k1gMnSc1	Vsevolodovič
<g/>
.	.	kIx.	.
</s>
<s>
Balto-slavjanskije	Baltolavjanskít	k5eAaPmIp3nS	Balto-slavjanskít
issledovanija	issledovanija	k6eAd1	issledovanija
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
10896	[number]	k4	10896
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
115	[number]	k4	115
<g/>
-	-	kIx~	-
<g/>
154	[number]	k4	154
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
NIKOLAJEV	NIKOLAJEV	kA	NIKOLAJEV
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Lvovič	Lvovič	k1gMnSc1	Lvovič
<g/>
.	.	kIx.	.
</s>
<s>
Ranneje	Rannej	k1gInPc1	Rannej
dialektnoje	dialektnoj	k1gInSc2	dialektnoj
členenie	členenie	k1gFnSc2	členenie
i	i	k8xC	i
vnešnie	vnešnie	k1gFnSc2	vnešnie
svjazi	svjah	k1gMnPc1	svjah
vostočnoslavjanskich	vostočnoslavjanskicha	k1gFnPc2	vostočnoslavjanskicha
dialektov	dialektovo	k1gNnPc2	dialektovo
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
SCHRAMM	SCHRAMM	kA	SCHRAMM
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gInSc1	Gottfried
<g/>
.	.	kIx.	.
</s>
<s>
Altrusslands	Altrusslands	k6eAd1	Altrusslands
Anfang	Anfang	k1gInSc1	Anfang
<g/>
.	.	kIx.	.
</s>
<s>
Historische	Historische	k1gFnSc1	Historische
Schlüsse	Schlüsse	k1gFnSc2	Schlüsse
aus	aus	k?	aus
Namen	Namen	k1gInSc1	Namen
<g/>
,	,	kIx,	,
Wörtern	Wörtern	k1gInSc1	Wörtern
und	und	k?	und
Texten	Texten	k2eAgInSc1d1	Texten
zum	zum	k?	zum
9	[number]	k4	9
<g/>
.	.	kIx.	.
und	und	k?	und
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhundert	Jahrhundert	k1gMnSc1	Jahrhundert
<g/>
.	.	kIx.	.
</s>
<s>
Freiburg	Freiburg	k1gMnSc1	Freiburg
im	im	k?	im
Breisgau	Breisgaa	k1gMnSc4	Breisgaa
:	:	kIx,	:
Rombach	Rombach	k1gMnSc1	Rombach
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
569	[number]	k4	569
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3793092681	[number]	k4	3793092681
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Slovar	Slovar	k1gInSc4	Slovar
drevnerusskogo	drevnerusskogo	k6eAd1	drevnerusskogo
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
XI	XI	kA	XI
-	-	kIx~	-
XIV	XIV	kA	XIV
vv	vv	k?	vv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
I-	I-	k1gMnSc2	I-
<g/>
X.	X.	kA	X.
Redakce	redakce	k1gFnSc2	redakce
R.	R.	kA	R.
I.	I.	kA	I.
Avanesov	Avanesov	k1gInSc1	Avanesov
<g/>
,	,	kIx,	,
I.	I.	kA	I.
S.	S.	kA	S.
Uluchanov	Uluchanov	k1gInSc1	Uluchanov
<g/>
,	,	kIx,	,
V.	V.	kA	V.
B.	B.	kA	B.
Krysko	Krysko	k1gNnSc1	Krysko
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Russkij	Russkij	k1gFnSc1	Russkij
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
STANG	STANG	kA	STANG
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
<g/>
.	.	kIx.	.
</s>
<s>
Slavonic	Slavonice	k1gFnPc2	Slavonice
accentuation	accentuation	k1gInSc1	accentuation
<g/>
.	.	kIx.	.
</s>
<s>
Oslo	Oslo	k1gNnSc1	Oslo
:	:	kIx,	:
Universitetsforlaget	Universitetsforlaget	k1gMnSc1	Universitetsforlaget
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
194	[number]	k4	194
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6078	[number]	k4	6078
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
ŠACHMATOV	ŠACHMATOV	kA	ŠACHMATOV
<g/>
,	,	kIx,	,
Aleksej	Aleksej	k1gMnSc1	Aleksej
Aleksandrovič	Aleksandrovič	k1gMnSc1	Aleksandrovič
<g/>
.	.	kIx.	.
</s>
<s>
Istoričeskaja	Istoričeskaj	k2eAgFnSc1d1	Istoričeskaj
morfologia	morfologia	k1gFnSc1	morfologia
russkogo	russkogo	k6eAd1	russkogo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Učpedgiz	Učpedgiz	k1gMnSc1	Učpedgiz
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
401	[number]	k4	401
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ŠAULSKIJ	ŠAULSKIJ	kA	ŠAULSKIJ
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Valerjevič	Valerjevič	k1gMnSc1	Valerjevič
<g/>
;	;	kIx,	;
KŇAZEV	KŇAZEV	kA	KŇAZEV
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
<g/>
.	.	kIx.	.
</s>
<s>
Russkaja	Russkaj	k2eAgFnSc1d1	Russkaja
dialektologija	dialektologija	k1gFnSc1	dialektologija
<g/>
:	:	kIx,	:
fonetika	fonetika	k1gFnSc1	fonetika
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
PGPI	PGPI	kA	PGPI
im	im	k?	im
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
M.	M.	kA	M.
Kirova	Kirovo	k1gNnPc4	Kirovo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
20	[number]	k4	20
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ŠILOV	ŠILOV	kA	ŠILOV
<g/>
,	,	kIx,	,
Aleksej	Aleksej	k1gMnSc1	Aleksej
Lvovič	Lvovič	k1gMnSc1	Lvovič
<g/>
.	.	kIx.	.
</s>
<s>
Pribaltijsko-finskaja	Pribaltijskoinskaj	k2eAgFnSc1d1	Pribaltijsko-finskaj
leksika	leksika	k1gFnSc1	leksika
i	i	k9	i
vostočnoslavjanskoje	vostočnoslavjanskoj	k1gInSc2	vostočnoslavjanskoj
jazykoznanie	jazykoznanie	k1gFnSc2	jazykoznanie
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
TRUBAČOV	TRUBAČOV	kA	TRUBAČOV
<g/>
,	,	kIx,	,
Oleg	Oleg	k1gMnSc1	Oleg
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poiskach	poiska	k1gFnPc6	poiska
jedinstva	jedinstvo	k1gNnSc2	jedinstvo
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
286	[number]	k4	286
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-	[number]	k4	978-
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
33259	[number]	k4	33259
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
VASMER	VASMER	kA	VASMER
<g/>
,	,	kIx,	,
Max	max	kA	max
<g/>
.	.	kIx.	.
Russisches	Russisches	k1gMnSc1	Russisches
etymologisches	etymologisches	k1gMnSc1	etymologisches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
<g/>
.	.	kIx.	.
</s>
<s>
Bd	Bd	k?	Bd
I-III	I-III	k1gFnSc1	I-III
<g/>
.	.	kIx.	.
</s>
<s>
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
:	:	kIx,	:
Winter	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
VEČERKA	Večerka	k1gMnSc1	Večerka
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
<g/>
.	.	kIx.	.
</s>
<s>
Altkirchenslavische	Altkirchenslavische	k1gFnSc1	Altkirchenslavische
Syntax	syntax	k1gFnSc1	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Bd	Bd	k?	Bd
I-IV	I-IV	k1gFnSc1	I-IV
<g/>
.	.	kIx.	.
</s>
<s>
Freiburg	Freiburg	k1gMnSc1	Freiburg
i.	i.	k?	i.
Br.	Br.	k1gMnSc1	Br.
:	:	kIx,	:
U.	U.	kA	U.
W.	W.	kA	W.
Weiher	Weihra	k1gFnPc2	Weihra
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
WORTH	WORTH	kA	WORTH
<g/>
,	,	kIx,	,
Dean	Dean	k1gMnSc1	Dean
S.	S.	kA	S.
<g/>
.	.	kIx.	.
</s>
<s>
Očerki	Očerki	k6eAd1	Očerki
po	po	k7c6	po
russkij	russkít	k5eAaPmRp2nS	russkít
filologii	filologie	k1gFnSc4	filologie
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
K.	K.	kA	K.
K.	K.	kA	K.
Bogatyrjov	Bogatyrjov	k1gInSc1	Bogatyrjov
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Indrik	Indrik	k1gMnSc1	Indrik
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
85759	[number]	k4	85759
<g/>
-	-	kIx~	-
<g/>
362	[number]	k4	362
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Protivopostavlenie	Protivopostavlenie	k1gFnSc1	Protivopostavlenie
otnositelnych	otnositelnycha	k1gFnPc2	otnositelnycha
i	i	k8xC	i
voprositelnych	voprositelnycha	k1gFnPc2	voprositelnycha
mestoimenij	mestoimenít	k5eAaPmRp2nS	mestoimenít
v	v	k7c4	v
drevnerusskom	drevnerusskom	k1gInSc4	drevnerusskom
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
IVANOV	IVANOV	kA	IVANOV
<g/>
,	,	kIx,	,
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Vsevolodovič	Vsevolodovič	k1gMnSc1	Vsevolodovič
<g/>
.	.	kIx.	.
</s>
<s>
Balto-slavjanskije	Baltolavjanskít	k5eAaPmIp3nS	Balto-slavjanskít
issledovanija	issledovanija	k6eAd1	issledovanija
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
107	[number]	k4	107
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Ot	ot	k1gMnSc1	ot
praslavjanskoj	praslavjanskoj	k1gInSc4	praslavjanskoj
akcentuacii	akcentuacie	k1gFnSc3	akcentuacie
k	k	k7c3	k
russkoj	russkojit	k5eAaPmRp2nS	russkojit
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
428	[number]	k4	428
s.	s.	k?	s.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Merilo	Merít	k5eAaPmAgNnS	Merít
Pravednoe	Pravednoe	k1gNnSc1	Pravednoe
XIV	XIV	kA	XIV
veka	veka	k1gFnSc1	veka
kak	kak	k?	kak
akcentologičeskij	akcentologičeskít	k5eAaPmRp2nS	akcentologičeskít
istočnik	istočnik	k1gInSc4	istočnik
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
Sagner	Sagner	k1gMnSc1	Sagner
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
87690	[number]	k4	87690
<g/>
-	-	kIx~	-
<g/>
482	[number]	k4	482
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
;	;	kIx,	;
JANIN	Janin	k2eAgMnSc1d1	Janin
<g/>
,	,	kIx,	,
Valentin	Valentin	k1gMnSc1	Valentin
Lavrenťjevič	Lavrenťjevič	k1gMnSc1	Lavrenťjevič
<g/>
.	.	kIx.	.
</s>
<s>
Novgorodskij	Novgorodskít	k5eAaPmRp2nS	Novgorodskít
kodeks	kodeks	k6eAd1	kodeks
pervoj	pervoj	k1gInSc4	pervoj
četverti	četvert	k1gMnPc1	četvert
XI	XI	kA	XI
v.	v.	k?	v.
–	–	k?	–
drevnejšaja	drevnejšaj	k2eAgFnSc1d1	drevnejšaj
kniga	kniga	k1gFnSc1	kniga
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Drevnejšaja	Drevnejšaj	k2eAgFnSc1d1	Drevnejšaj
kirilličeskaja	kirilličeskaj	k2eAgFnSc1d1	kirilličeskaj
azbuka	azbuka	k1gFnSc1	azbuka
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Drevnenovgorodskij	Drevnenovgorodskít	k5eAaPmRp2nS	Drevnenovgorodskít
dialekt	dialekt	k1gInSc1	dialekt
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Jazyki	Jazyki	k1gNnSc1	Jazyki
slavjanskich	slavjanskicha	k1gFnPc2	slavjanskicha
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
872	[number]	k4	872
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
94457	[number]	k4	94457
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
;	;	kIx,	;
JANIN	Janin	k2eAgMnSc1d1	Janin
<g/>
,	,	kIx,	,
Valentin	Valentin	k1gMnSc1	Valentin
Lavrenťjevič	Lavrenťjevič	k1gMnSc1	Lavrenťjevič
<g/>
.	.	kIx.	.
</s>
<s>
Beresťanyje	Beresťanýt	k5eAaPmIp3nS	Beresťanýt
gramoty	gramota	k1gFnPc4	gramota
iz	iz	k?	iz
novgorodskich	novgorodskich	k1gInSc1	novgorodskich
raskopok	raskopok	k1gInSc1	raskopok
2006	[number]	k4	2006
g.	g.	k?	g.
<g/>
.	.	kIx.	.
</s>
<s>
Voprosy	Vopros	k1gInPc1	Vopros
jazykoznanija	jazykoznanijum	k1gNnSc2	jazykoznanijum
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Drevnerusskie	Drevnerusskie	k1gFnSc1	Drevnerusskie
enklitiki	enklitik	k1gFnSc2	enklitik
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Jazyki	Jazyki	k1gNnSc1	Jazyki
slavjanskich	slavjanskicha	k1gFnPc2	slavjanskicha
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9551	[number]	k4	9551
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
232	[number]	k4	232
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
ZALIZŇAK	ZALIZŇAK	kA	ZALIZŇAK
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Anatoljevič	Anatoljevič	k1gMnSc1	Anatoljevič
<g/>
.	.	kIx.	.
</s>
<s>
Drevnerusskij	Drevnerusskít	k5eAaPmRp2nS	Drevnerusskít
i	i	k8xC	i
starovelikorusskij	starovelikorusskít	k5eAaPmRp2nS	starovelikorusskít
slovar	slovar	k1gInSc4	slovar
ukazatel	ukazatel	k1gInSc1	ukazatel
(	(	kIx(	(
<g/>
XIV-XVII	XIV-XVII	k1gFnSc1	XIV-XVII
vv	vv	k?	vv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
:	:	kIx,	:
Jazyki	Jazyki	k1gNnSc1	Jazyki
slavjanskich	slavjanskicha	k1gFnPc2	slavjanskicha
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9551	[number]	k4	9551
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
377	[number]	k4	377
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc4	Rus
Staronovgorodský	Staronovgorodský	k2eAgInSc4d1	Staronovgorodský
dialekt	dialekt	k1gInSc4	dialekt
</s>
