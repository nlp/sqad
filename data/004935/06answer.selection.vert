<s>
Staroruština	Staroruština	k1gFnSc1	Staroruština
či	či	k8xC	či
stará	starý	k2eAgFnSc1d1	stará
ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
д	д	k?	д
я	я	k?	я
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
д	д	k?	д
м	м	k?	м
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
с	с	k?	с
м	м	k?	м
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
používali	používat	k5eAaImAgMnP	používat
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
východní	východní	k2eAgMnPc1d1	východní
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
několika	několik	k4yIc2	několik
východních	východní	k2eAgNnPc2d1	východní
vojvodství	vojvodství	k1gNnPc2	vojvodství
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
