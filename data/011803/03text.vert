<p>
<s>
Sedm	sedm	k4xCc1	sedm
chlebů	chléb	k1gInPc2	chléb
je	být	k5eAaImIp3nS	být
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
formace	formace	k1gFnSc1	formace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kokořínska	Kokořínsko	k1gNnSc2	Kokořínsko
ležící	ležící	k2eAgFnSc1d1	ležící
nedaleko	nedaleko	k7c2	nedaleko
jeskyně	jeskyně	k1gFnSc2	jeskyně
Mordloch	Mordloch	k1gInSc1	Mordloch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skalní	skalní	k2eAgInSc1d1	skalní
útvar	útvar	k1gInSc1	útvar
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
žlutě	žlutě	k6eAd1	žlutě
turistické	turistický	k2eAgFnSc2d1	turistická
trasy	trasa	k1gFnSc2	trasa
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
ze	z	k7c2	z
Stračí	stračí	k2eAgFnSc2d1	stračí
do	do	k7c2	do
Želíz	Želízy	k1gInPc2	Želízy
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
vedena	veden	k2eAgFnSc1d1	vedena
též	též	k9	též
žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
odbočka	odbočka	k1gFnSc1	odbočka
<g/>
.	.	kIx.	.
</s>
<s>
Formace	formace	k1gFnSc1	formace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Sedm	sedm	k4xCc1	sedm
chlebů	chléb	k1gInPc2	chléb
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
připomíná	připomínat	k5eAaImIp3nS	připomínat
sedm	sedm	k4xCc4	sedm
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
postavených	postavený	k2eAgInPc2d1	postavený
bochníků	bochník	k1gInPc2	bochník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sedm	sedm	k4xCc1	sedm
chlebů	chléb	k1gInPc2	chléb
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mordloch	Mordloch	k1gMnSc1	Mordloch
</s>
</p>
<p>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Kokořínsko	Kokořínsko	k1gNnSc1	Kokořínsko
</s>
</p>
