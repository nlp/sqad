<p>
<s>
Habib	Habib	k1gInSc4	Habib
Koité	Koita	k1gMnPc1	Koita
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známý	známý	k2eAgMnSc1d1	známý
malijský	malijský	k2eAgMnSc1d1	malijský
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
západoafrickou	západoafrický	k2eAgFnSc4d1	západoafrická
hudební	hudební	k2eAgFnSc4d1	hudební
tradici	tradice	k1gFnSc4	tradice
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
rockem	rock	k1gInSc7	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
skupina	skupina	k1gFnSc1	skupina
Bamada	Bamada	k1gFnSc1	Bamada
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
talentovaných	talentovaný	k2eAgMnPc2d1	talentovaný
západoafrických	západoafrický	k2eAgMnPc2d1	západoafrický
hudebníků	hudebník	k1gMnPc2	hudebník
(	(	kIx(	(
<g/>
další	další	k2eAgNnSc1d1	další
velkou	velký	k2eAgFnSc7d1	velká
hvězdou	hvězda	k1gFnSc7	hvězda
je	být	k5eAaImIp3nS	být
balafonista	balafonista	k1gMnSc1	balafonista
Kélétigui	Kélétigu	k1gFnSc2	Kélétigu
Diabaté	Diabatá	k1gFnSc2	Diabatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koité	Koita	k1gMnPc1	Koita
je	on	k3xPp3gMnPc4	on
známý	známý	k1gMnSc1	známý
svou	svůj	k3xOyFgFnSc7	svůj
unikátní	unikátní	k2eAgFnSc7d1	unikátní
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
pentatonické	pentatonický	k2eAgFnPc4d1	pentatonická
stupnice	stupnice	k1gFnPc4	stupnice
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
skladby	skladba	k1gFnPc1	skladba
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
blues	blues	k1gNnSc7	blues
a	a	k8xC	a
flamencem	flamenco	k1gNnSc7	flamenco
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
známým	známý	k2eAgMnPc3d1	známý
malijským	malijský	k2eAgMnPc3d1	malijský
hudebníkům	hudebník	k1gMnPc3	hudebník
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Salif	Salif	k1gInSc4	Salif
Keita	Keito	k1gNnSc2	Keito
nebo	nebo	k8xC	nebo
Oumou	Ouma	k1gFnSc7	Ouma
Sangaré	Sangarý	k2eAgFnSc2d1	Sangarý
<g/>
)	)	kIx)	)
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
<g/>
,	,	kIx,	,
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
<g/>
,	,	kIx,	,
tichý	tichý	k2eAgInSc4d1	tichý
a	a	k8xC	a
koncentrovaný	koncentrovaný	k2eAgInSc4d1	koncentrovaný
zpěv	zpěv	k1gInSc4	zpěv
před	před	k7c7	před
technickou	technický	k2eAgFnSc7d1	technická
dokonalostí	dokonalost	k1gFnSc7	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Bamada	Bamada	k1gFnSc1	Bamada
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c4	na
harmoniku	harmonika	k1gFnSc4	harmonika
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
calabasu	calabasa	k1gFnSc4	calabasa
a	a	k8xC	a
koru	kora	k1gFnSc4	kora
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
píše	psát	k5eAaImIp3nS	psát
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
bambarštině	bambarština	k1gFnSc6	bambarština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Muso	Musa	k1gFnSc5	Musa
ko	ko	k?	ko
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Ma	Ma	k?	Ma
Ya	Ya	k1gMnSc1	Ya
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Baro	Baro	k1gMnSc1	Baro
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Live	Live	k6eAd1	Live
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Afriki	Afriki	k1gNnSc1	Afriki
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Habib	Habiba	k1gFnPc2	Habiba
Koité	Koitý	k2eAgFnPc4d1	Koitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
-	-	kIx~	-
Habib	Habib	k1gInSc1	Habib
Koité	Koitý	k2eAgFnSc2d1	Koitý
</s>
</p>
