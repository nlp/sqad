<s>
Zároveň	zároveň	k6eAd1
si	se	k3xPyFc3
změnil	změnit	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
na	na	k7c4
Jimi	on	k3xPp3gInPc7
Hendrix	Hendrix	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
Marshall	Marshall	k1gMnSc1
Hendrix	Hendrix	k1gInSc4
<g/>
,	,	kIx,
rozený	rozený	k2eAgInSc4d1
jako	jako	k8xS
Johnny	Johnna	k1gFnSc2
Allen	Allen	k1gMnSc1
Hendrix	Hendrix	k1gInSc1
(	(	kIx(
<g/>
27	[number]	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1942	[number]	k4
Seattle	Seattle	k1gFnSc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
-	-	kIx~
18	[number]	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1970	[number]	k4
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
kytarista	kytarista	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>