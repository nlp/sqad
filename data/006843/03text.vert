<s>
Bnej	Bnej	k1gFnSc1	Bnej
Menaše	Menaše	k1gFnSc2	Menaše
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ב	ב	k?	ב
מ	מ	k?	מ
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
Manasese	Manasese	k1gFnPc1	Manasese
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9000	[number]	k4	9000
indických	indický	k2eAgMnPc2d1	indický
Židů	Žid	k1gMnPc2	Žid
ze	z	k7c2	z
severovýchodních	severovýchodní	k2eAgInPc2d1	severovýchodní
indických	indický	k2eAgInPc2d1	indický
svazových	svazový	k2eAgInPc2d1	svazový
států	stát	k1gInPc2	stát
Manipur	Manipura	k1gFnPc2	Manipura
a	a	k8xC	a
Mizoram	Mizoram	k1gInSc1	Mizoram
ležících	ležící	k2eAgMnPc2d1	ležící
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Barmou	Barma	k1gFnSc7	Barma
a	a	k8xC	a
Bangladéšem	Bangladéš	k1gInSc7	Bangladéš
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc4	potomek
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
ztracených	ztracený	k2eAgInPc2d1	ztracený
kmenů	kmen	k1gInPc2	kmen
Izraelitů	izraelita	k1gMnPc2	izraelita
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
Manases	Manases	k1gMnSc1	Manases
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2700	[number]	k4	2700
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
devíti	devět	k4xCc7	devět
izraelitskými	izraelitský	k2eAgInPc7d1	izraelitský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
odveden	odveden	k2eAgInSc1d1	odveden
Asyřany	Asyřan	k1gMnPc7	Asyřan
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
provedlo	provést	k5eAaPmAgNnS	provést
díky	díky	k7c3	díky
organizaci	organizace	k1gFnSc3	organizace
Šavej	Šavej	k1gInSc1	Šavej
Jisra	Jisro	k1gNnSc2	Jisro
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
Židy	Žid	k1gMnPc4	Žid
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
aliju	aliju	k5eAaPmIp1nS	aliju
na	na	k7c4	na
1500	[number]	k4	1500
příslušníků	příslušník	k1gMnPc2	příslušník
Bnej	Bnej	k1gMnSc1	Bnej
Menaše	Menaš	k1gInSc2	Menaš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
žijí	žít	k5eAaImIp3nP	žít
tito	tento	k3xDgMnPc1	tento
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
zejména	zejména	k9	zejména
v	v	k7c6	v
osadách	osada	k1gFnPc6	osada
Kirjat	Kirjat	k1gInSc1	Kirjat
Arba	arba	k1gFnSc1	arba
<g/>
,	,	kIx,	,
Ofra	Ofra	k1gFnSc1	Ofra
a	a	k8xC	a
Bejt	Bejt	k?	Bejt
El.	El.	k1gFnSc1	El.
</s>
