<s>
Bnej	Bnej	k1gFnSc1	Bnej
Menaše	Menaše	k1gFnSc2	Menaše
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ב	ב	k?	ב
מ	מ	k?	מ
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
Manasese	Manasese	k1gFnPc1	Manasese
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9000	[number]	k4	9000
indických	indický	k2eAgMnPc2d1	indický
Židů	Žid	k1gMnPc2	Žid
ze	z	k7c2	z
severovýchodních	severovýchodní	k2eAgInPc2d1	severovýchodní
indických	indický	k2eAgInPc2d1	indický
svazových	svazový	k2eAgInPc2d1	svazový
států	stát	k1gInPc2	stát
Manipur	Manipura	k1gFnPc2	Manipura
a	a	k8xC	a
Mizoram	Mizoram	k1gInSc1	Mizoram
ležících	ležící	k2eAgMnPc2d1	ležící
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Barmou	Barma	k1gFnSc7	Barma
a	a	k8xC	a
Bangladéšem	Bangladéš	k1gInSc7	Bangladéš
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc4	potomek
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
ztracených	ztracený	k2eAgInPc2d1	ztracený
kmenů	kmen	k1gInPc2	kmen
Izraelitů	izraelita	k1gMnPc2	izraelita
<g/>
.	.	kIx.	.
</s>
