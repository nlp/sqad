<s>
Burušaskí	Burušaskí	k1gFnSc1	Burušaskí
neboli	neboli	k8xC	neboli
chadžuná	chadžuná	k1gFnSc1	chadžuná
je	být	k5eAaImIp3nS	být
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
Hunzové	Hunzový	k2eAgNnSc1d1	Hunzový
(	(	kIx(	(
<g/>
také	také	k9	také
zvaní	zvaní	k1gNnSc1	zvaní
Burúšové	Burúšová	k1gFnSc2	Burúšová
<g/>
)	)	kIx)	)
v	v	k7c6	v
horských	horský	k2eAgNnPc6d1	horské
údolích	údolí	k1gNnPc6	údolí
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Karákóram	Karákóram	k1gInSc1	Karákóram
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
dvacítkové	dvacítkový	k2eAgFnSc2d1	dvacítková
početní	početní	k2eAgFnSc2d1	početní
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
ergativu	ergativ	k1gInSc2	ergativ
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
burušaskí	burušask	k1gFnSc7	burušask
společné	společný	k2eAgFnSc2d1	společná
baskičtina	baskičtina	k1gFnSc1	baskičtina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
společných	společný	k2eAgInPc6d1	společný
kořenech	kořen	k1gInPc6	kořen
<g/>
,	,	kIx,	,
sahajících	sahající	k2eAgInPc2d1	sahající
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
deseti	deset	k4xCc7	deset
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
Dené-kavkazské	Denéavkazský	k2eAgFnSc6d1	Dené-kavkazský
jazykové	jazykový	k2eAgFnSc6d1	jazyková
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
kromě	kromě	k7c2	kromě
burušaskí	burušaskí	k1gFnSc2	burušaskí
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
Basků	Bask	k1gMnPc2	Bask
také	také	k9	také
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
adygejština	adygejština	k1gFnSc1	adygejština
<g/>
,	,	kIx,	,
ketština	ketština	k1gFnSc1	ketština
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
severoamerické	severoamerický	k2eAgInPc1d1	severoamerický
jazyky	jazyk	k1gInPc1	jazyk
na-dené	naený	k2eAgInPc1d1	na-dený
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
vědci	vědec	k1gMnPc1	vědec
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
původ	původ	k1gInSc4	původ
burušaskí	burušaskí	k6eAd1	burušaskí
od	od	k7c2	od
jazyka	jazyk	k1gInSc2	jazyk
starověkých	starověký	k2eAgInPc2d1	starověký
Frygů	Fryg	k1gInPc2	Fryg
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
burušaskí	burušaskí	k1gFnSc6	burušaskí
čtyři	čtyři	k4xCgInPc1	čtyři
rody	rod	k1gInPc1	rod
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
abstraktní	abstraktní	k2eAgInPc4d1	abstraktní
pojmy	pojem	k1gInPc4	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
používáním	používání	k1gNnSc7	používání
přivlastňovacích	přivlastňovací	k2eAgFnPc2d1	přivlastňovací
předpon	předpona	k1gFnPc2	předpona
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
38	[number]	k4	38
forem	forma	k1gFnPc2	forma
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
údolí	údolí	k1gNnPc2	údolí
–	–	k?	–
Hunza	Hunz	k1gMnSc2	Hunz
<g/>
,	,	kIx,	,
Nágar	Nágar	k1gInSc1	Nágar
a	a	k8xC	a
Veršikvár	Veršikvár	k1gInSc1	Veršikvár
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgNnSc1d1	zvané
také	také	k9	také
Jasin	Jasin	k1gInSc4	Jasin
<g/>
)	)	kIx)	)
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
dialekt	dialekt	k1gInSc4	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Burušaskí	Burušaskí	k6eAd1	Burušaskí
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
urdská	urdský	k2eAgFnSc1d1	urdský
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Omniglot	Omniglot	k1gInSc1	Omniglot
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Ethnologue	Ethnologue	k1gInSc1	Ethnologue
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Píseň	píseň	k1gFnSc1	píseň
v	v	k7c6	v
burušaskí	burušaskí	k1gFnSc6	burušaskí
</s>
