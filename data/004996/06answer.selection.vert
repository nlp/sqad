<s>
Burušaskí	Burušaskí	k1gFnSc1	Burušaskí
neboli	neboli	k8xC	neboli
chadžuná	chadžuná	k1gFnSc1	chadžuná
je	být	k5eAaImIp3nS	být
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
Hunzové	Hunzový	k2eAgNnSc1d1	Hunzový
(	(	kIx(	(
<g/>
také	také	k9	také
zvaní	zvaní	k1gNnSc1	zvaní
Burúšové	Burúšová	k1gFnSc2	Burúšová
<g/>
)	)	kIx)	)
v	v	k7c6	v
horských	horský	k2eAgNnPc6d1	horské
údolích	údolí	k1gNnPc6	údolí
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Karákóram	Karákóram	k1gInSc1	Karákóram
<g/>
.	.	kIx.	.
</s>
