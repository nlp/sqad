<s>
Býk	býk	k1gMnSc1	býk
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
astrologické	astrologický	k2eAgNnSc1d1	astrologické
znamení	znamení	k1gNnSc1	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
s	s	k7c7	s
původem	původ	k1gInSc7	původ
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Býka	býk	k1gMnSc2	býk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Býk	býk	k1gMnSc1	býk
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
negativní	negativní	k2eAgNnSc4d1	negativní
(	(	kIx(	(
<g/>
introvertní	introvertní	k2eAgNnSc4d1	introvertní
<g/>
)	)	kIx)	)
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zemské	zemský	k2eAgNnSc4d1	zemské
znamení	znamení	k1gNnSc4	znamení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
pevných	pevný	k2eAgNnPc2d1	pevné
znamení	znamení	k1gNnPc2	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Býk	býk	k1gMnSc1	býk
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
ovládán	ovládat	k5eAaImNgInS	ovládat
planetou	planeta	k1gFnSc7	planeta
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
konstelaci	konstelace	k1gFnSc6	konstelace
Býka	býk	k1gMnSc2	býk
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sideralistické	sideralistický	k2eAgFnSc6d1	sideralistický
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Taurus	Taurus	k1gInSc1	Taurus
(	(	kIx(	(
<g/>
astrology	astrolog	k1gMnPc7	astrolog
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
