<s>
Okresy	okres	k1gInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
jsou	být	k5eAaImIp3nP
územní	územní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
středního	střední	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
území	území	k1gNnSc1
státu	stát	k1gInSc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>