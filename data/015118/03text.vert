<s>
Okresy	okres	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Okresy	okres	k1gInPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2007	#num#	k4
v	v	k7c6
hranicích	hranice	k1gFnPc6
samosprávných	samosprávný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
okresem	okres	k1gInSc7
není	být	k5eNaImIp3nS
a	a	k8xC
na	na	k7c4
okresy	okres	k1gInPc4
se	se	k3xPyFc4
nečlení	členit	k5eNaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Značka	značka	k1gFnSc1
oznamující	oznamující	k2eAgFnSc1d1
motoristům	motorista	k1gMnPc3
vjezd	vjezd	k1gInSc1
do	do	k7c2
okresu	okres	k1gInSc2
Brno-venkov	Brno-venkov	k1gInSc1
</s>
<s>
Okresy	okres	k1gInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
jsou	být	k5eAaImIp3nP
územní	územní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
středního	střední	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
území	území	k1gNnSc1
státu	stát	k1gInSc2
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
menšími	malý	k2eAgFnPc7d2
správními	správní	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
každého	každý	k3xTgInSc2
okresu	okres	k1gInSc2
je	být	k5eAaImIp3nS
jedinečný	jedinečný	k2eAgInSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
celé	celý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
má	mít	k5eAaImIp3nS
(	(	kIx(
<g/>
od	od	k7c2
vzniku	vznik	k1gInSc2
okresu	okres	k1gInSc2
Jeseník	Jeseník	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1996	#num#	k4
<g/>
)	)	kIx)
celkem	celkem	k6eAd1
76	#num#	k4
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
nepatří	patřit	k5eNaImIp3nS
do	do	k7c2
žádného	žádný	k3yNgInSc2
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresy	okres	k1gInPc1
jsou	být	k5eAaImIp3nP
zákonem	zákon	k1gInSc7
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
vymezeny	vymezit	k5eAaPmNgInP
prostřednictvím	prostřednictvím	k7c2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresy	okres	k1gInPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
obce	obec	k1gFnPc4
a	a	k8xC
vojenské	vojenský	k2eAgInPc4d1
újezdy	újezd	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
okresu	okres	k1gInSc2
Brno-město	Brno-města	k1gMnSc5
tvoří	tvořit	k5eAaImIp3nP
statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
byly	být	k5eAaImAgFnP
podobnými	podobný	k2eAgInPc7d1
případy	případ	k1gInPc7
i	i	k8xC
okresy	okres	k1gInPc7
Plzeň-město	Plzeň-města	k1gMnSc5
a	a	k8xC
Ostrava-město	Ostrava-města	k1gMnSc5
a	a	k8xC
dokud	dokud	k8xS
existovaly	existovat	k5eAaImAgInP
okresní	okresní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
předtím	předtím	k6eAd1
okresní	okresní	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc4
působnost	působnost	k1gFnSc1
vykonávaly	vykonávat	k5eAaImAgInP
pro	pro	k7c4
území	území	k1gNnSc4
okresu	okres	k1gInSc2
magistráty	magistrát	k1gInPc4
těchto	tento	k3xDgNnPc2
měst	město	k1gNnPc2
a	a	k8xC
orgány	orgán	k1gInPc1
samosprávných	samosprávný	k2eAgFnPc2d1
částí	část	k1gFnPc2
těchto	tento	k3xDgNnPc2
měst	město	k1gNnPc2
(	(	kIx(
<g/>
§	§	k?
10	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
147	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
není	být	k5eNaImIp3nS
a	a	k8xC
nebyla	být	k5eNaImAgFnS
okresem	okres	k1gInSc7
a	a	k8xC
její	její	k3xOp3gNnSc1
území	území	k1gNnSc1
se	se	k3xPyFc4
nedělí	dělit	k5eNaImIp3nS
na	na	k7c4
okresy	okres	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
deset	deset	k4xCc4
číslovaných	číslovaný	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
měla	mít	k5eAaImAgFnS
podle	podle	k7c2
přílohy	příloha	k1gFnSc2
č.	č.	k?
1	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
147	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
pravomoc	pravomoc	k1gFnSc4
okresního	okresní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
pro	pro	k7c4
svá	svůj	k3xOyFgNnPc4
území	území	k1gNnPc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zčásti	zčásti	k6eAd1
přenášela	přenášet	k5eAaImAgFnS
statutem	statut	k1gInSc7
na	na	k7c4
orgány	orgán	k1gInPc4
svých	svůj	k3xOyFgMnPc2
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
vybraných	vybraný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obvody	obvod	k1gInPc1
okresů	okres	k1gInPc2
i	i	k8xC
po	po	k7c6
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
okresní	okresní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
zrušeny	zrušit	k5eAaPmNgInP
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgInP
zachovány	zachovat	k5eAaPmNgInP
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
soudů	soud	k1gInPc2
<g/>
,	,	kIx,
policie	policie	k1gFnSc2
<g/>
,	,	kIx,
archivů	archiv	k1gInPc2
<g/>
,	,	kIx,
úřadů	úřad	k1gInPc2
práce	práce	k1gFnSc2
atd.	atd.	kA
Dále	daleko	k6eAd2
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
jako	jako	k8xC,k8xS
statistická	statistický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
jako	jako	k9
rozlišovací	rozlišovací	k2eAgInSc4d1
údaj	údaj	k1gInSc4
k	k	k7c3
názvu	název	k1gInSc3
obce	obec	k1gFnSc2
a	a	k8xC
některých	některý	k3yIgFnPc2
nestátních	státní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číselník	číselník	k1gInSc4
okresů	okres	k1gInPc2
je	být	k5eAaImIp3nS
vytvářen	vytvářet	k5eAaImNgInS,k5eAaPmNgInS
a	a	k8xC
udržován	udržovat	k5eAaImNgInS
Českým	český	k2eAgInSc7d1
statistickým	statistický	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změny	změna	k1gFnSc2
v	v	k7c6
okresech	okres	k1gInPc6
(	(	kIx(
<g/>
vznik	vznik	k1gInSc1
<g/>
,	,	kIx,
změnu	změna	k1gFnSc4
vymezení	vymezení	k1gNnSc2
<g/>
)	)	kIx)
schvaluje	schvalovat	k5eAaImIp3nS
vláda	vláda	k1gFnSc1
ČR	ČR	kA
<g/>
;	;	kIx,
k	k	k7c3
větším	veliký	k2eAgFnPc3d2
změnám	změna	k1gFnPc3
ve	v	k7c6
vymezení	vymezení	k1gNnSc6
okresů	okres	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
zejména	zejména	k9
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
za	za	k7c7
účelem	účel	k1gInSc7
obnovení	obnovení	k1gNnSc1
skladebnosti	skladebnost	k1gFnSc2
územního	územní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc2
okresů	okres	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
</s>
<s>
Okresní	okresní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ukončily	ukončit	k5eAaPmAgFnP
svoji	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2002	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
druhé	druhý	k4xOgFnSc2
fáze	fáze	k1gFnSc2
reformy	reforma	k1gFnSc2
územní	územní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
už	už	k9
okresy	okres	k1gInPc1
přestaly	přestat	k5eAaPmAgInP
mít	mít	k5eAaImF
zákonem	zákon	k1gInSc7
stanoveno	stanovit	k5eAaPmNgNnS
své	svůj	k3xOyFgNnSc1
sídlo	sídlo	k1gNnSc1
(	(	kIx(
<g/>
okresní	okresní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
byly	být	k5eAaImAgInP
okresy	okres	k1gInPc1
jednotkou	jednotka	k1gFnSc7
NUTS	NUTS	kA
4	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
tato	tento	k3xDgFnSc1
úroveň	úroveň	k1gFnSc1
převedena	převést	k5eAaPmNgFnS
do	do	k7c2
soustavy	soustava	k1gFnSc2
LAU	LAU	kA
<g/>
,	,	kIx,
okresy	okres	k1gInPc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
jednotkou	jednotka	k1gFnSc7
úrovně	úroveň	k1gFnSc2
LAU	LAU	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obvody	obvod	k1gInPc4
úřadů	úřad	k1gInPc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
okresní	okresní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
byly	být	k5eAaImAgInP
zachovány	zachovat	k5eAaPmNgInP
v	v	k7c6
plném	plný	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemkové	pozemkový	k2eAgInPc1d1
úřady	úřad	k1gInPc1
je	on	k3xPp3gMnPc4
využívaly	využívat	k5eAaPmAgFnP,k5eAaImAgFnP
jen	jen	k6eAd1
v	v	k7c6
tom	ten	k3xDgInSc6
rozsahu	rozsah	k1gInSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
byly	být	k5eAaImAgInP
vymezeny	vymezen	k2eAgInPc1d1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
těchto	tento	k3xDgInPc2
zbytků	zbytek	k1gInPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c6
okresní	okresní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
hrají	hrát	k5eAaImIp3nP
okresy	okres	k1gInPc1
i	i	k9
nadále	nadále	k6eAd1
roli	role	k1gFnSc4
pro	pro	k7c4
fungování	fungování	k1gNnSc4
profesní	profesní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
pro	pro	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
okresní	okresní	k2eAgInSc4d1
sdružení	sdružení	k1gNnSc1
lékařů	lékař	k1gMnPc2
<g/>
,	,	kIx,
stomatologů	stomatolog	k1gMnPc2
<g/>
,	,	kIx,
lékárníků	lékárník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
veterinárních	veterinární	k2eAgMnPc2d1
lékařů	lékař	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
soudů	soud	k1gInPc2
a	a	k8xC
státních	státní	k2eAgNnPc2d1
zastupitelství	zastupitelství	k1gNnPc2
sice	sice	k8xC
zachoval	zachovat	k5eAaPmAgInS
uspořádání	uspořádání	k1gNnSc4
vycházející	vycházející	k2eAgNnSc4d1
z	z	k7c2
dosavadních	dosavadní	k2eAgInPc2d1
krajů	kraj	k1gInPc2
a	a	k8xC
okresů	okres	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc2
sídel	sídlo	k1gNnPc2
i	i	k8xC
názvy	název	k1gInPc1
„	„	k?
<g/>
krajský	krajský	k2eAgInSc1d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
okresní	okresní	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
avšak	avšak	k8xC
soudní	soudní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
těchto	tento	k3xDgInPc2
soudů	soud	k1gInPc2
byly	být	k5eAaImAgFnP
redefinovány	redefinován	k2eAgMnPc4d1
speciálním	speciální	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
postupem	postup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
nich	on	k3xPp3gNnPc2
vymezení	vymezení	k1gNnPc2
okresů	okres	k1gInPc2
dle	dle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
odchýlilo	odchýlit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Kompetence	kompetence	k1gFnSc1
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
převzaly	převzít	k5eAaPmAgInP
zčásti	zčásti	k6eAd1
krajské	krajský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
a	a	k8xC
zčásti	zčásti	k6eAd1
obecní	obecní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgMnSc1d2
než	než	k8xS
okresy	okres	k1gInPc1
a	a	k8xC
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
přesahovaly	přesahovat	k5eAaImAgInP
i	i	k9
přes	přes	k7c4
hranice	hranice	k1gFnPc4
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
i	i	k9
když	když	k8xS
pouze	pouze	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
kraje	kraj	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
nastal	nastat	k5eAaPmAgInS
u	u	k7c2
152	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
narušil	narušit	k5eAaPmAgMnS
tak	tak	k6eAd1
skladebnost	skladebnost	k1gFnSc4
územního	územní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
reformy	reforma	k1gFnSc2
územní	územní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
skladebnosti	skladebnost	k1gFnSc2
na	na	k7c6
těchto	tento	k3xDgFnPc6
úrovních	úroveň	k1gFnPc6
dosáhnout	dosáhnout	k5eAaPmF
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
k	k	k7c3
faktickému	faktický	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
působností	působnost	k1gFnPc2
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
nezbytné	nezbytný	k2eAgNnSc1d1,k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
realizovat	realizovat	k5eAaBmF
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
změnou	změna	k1gFnSc7
okresní	okresní	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
119	#num#	k4
obcí	obec	k1gFnPc2
vydáním	vydání	k1gNnSc7
vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
513	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nabyla	nabýt	k5eAaPmAgFnS
účinnosti	účinnost	k1gFnPc4
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
bylo	být	k5eAaImAgNnS
33	#num#	k4
obcí	obec	k1gFnPc2
spadajících	spadající	k2eAgFnPc2d1
do	do	k7c2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Český	český	k2eAgInSc1d1
Brod	Brod	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
obce	obec	k1gFnPc1
leží	ležet	k5eAaImIp3nP
v	v	k7c6
okrese	okres	k1gInSc6
Nymburk	Nymburk	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
obec	obec	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stod	Stod	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
obcí	obec	k1gFnPc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Domažlice	Domažlice	k1gFnPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
Tanvald	Tanvald	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
obec	obec	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Semily	Semily	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Turnov	Turnov	k1gInSc1
(	(	kIx(
<g/>
13	#num#	k4
obcí	obec	k1gFnPc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Liberec	Liberec	k1gInSc1
a	a	k8xC
3	#num#	k4
obce	obec	k1gFnPc1
leží	ležet	k5eAaImIp3nP
v	v	k7c6
okrese	okres	k1gInSc6
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
obce	obec	k1gFnPc1
leží	ležet	k5eAaImIp3nP
v	v	k7c6
okrese	okres	k1gInSc6
Vsetín	Vsetín	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novou	nový	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
územního	územní	k2eAgNnSc2d1
vymezení	vymezení	k1gNnSc2
okresů	okres	k1gInPc2
zavedl	zavést	k5eAaPmAgInS
zákon	zákon	k1gInSc1
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
převedl	převést	k5eAaPmAgInS
obce	obec	k1gFnPc4
mezi	mezi	k7c7
okresy	okres	k1gInPc7
a	a	k8xC
změna	změna	k1gFnSc1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
nedotkla	dotknout	k5eNaPmAgFnS
například	například	k6eAd1
příslušnosti	příslušnost	k1gFnSc3
k	k	k7c3
soudům	soud	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
vlastními	vlastní	k2eAgInPc7d1
okresy	okres	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
dalších	další	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
změnil	změnit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
příslušnost	příslušnost	k1gFnSc4
k	k	k7c3
jiné	jiný	k2eAgFnSc3d1
obci	obec	k1gFnSc3
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
sladil	sladit	k5eAaImAgInS,k5eAaPmAgInS
většinu	většina	k1gFnSc4
zbývajících	zbývající	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresy	okres	k1gInPc1
podle	podle	k7c2
samosprávných	samosprávný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kraje	kraj	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
347	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
č.	č.	k?
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
stanovil	stanovit	k5eAaPmAgMnS
názvy	název	k1gInPc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
(	(	kIx(
<g/>
krajů	kraj	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
vymezil	vymezit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
výčtem	výčet	k1gInSc7
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
území	území	k1gNnSc1
okresů	okres	k1gInPc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
definováno	definovat	k5eAaBmNgNnS
prováděcím	prováděcí	k2eAgInSc7d1
předpisem	předpis	k1gInSc7
k	k	k7c3
zákonu	zákon	k1gInSc3
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
-	-	kIx~
vyhláškou	vyhláška	k1gFnSc7
č.	č.	k?
564	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
stanovení	stanovení	k1gNnSc4
území	území	k1gNnSc2
okresů	okres	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc6
obvodů	obvod	k1gInPc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okresy	okres	k1gInPc7
jsou	být	k5eAaImIp3nP
jako	jako	k9
části	část	k1gFnSc2
krajů	kraj	k1gInPc2
nově	nově	k6eAd1
vymezeny	vymezen	k2eAgInPc1d1
zákonem	zákon	k1gInSc7
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
347	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
také	také	k6eAd1
jmenováno	jmenovat	k5eAaBmNgNnS,k5eAaImNgNnS
vyšším	vysoký	k2eAgInSc7d2
územním	územní	k2eAgInSc7d1
samosprávným	samosprávný	k2eAgInSc7d1
celkem	celek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Prahu	práh	k1gInSc2
se	se	k3xPyFc4
nevztahuje	vztahovat	k5eNaImIp3nS
zákon	zákon	k1gInSc1
o	o	k7c6
krajích	kraj	k1gInPc6
<g/>
,	,	kIx,
Praze	Praha	k1gFnSc6
přiznává	přiznávat	k5eAaImIp3nS
postavení	postavení	k1gNnSc4
a	a	k8xC
pravomoci	pravomoc	k1gFnPc4
obce	obec	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
kraje	kraj	k1gInPc1
zákon	zákon	k1gInSc1
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
147	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
okresních	okresní	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
plnilo	plnit	k5eAaImAgNnS
v	v	k7c6
plném	plný	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
i	i	k9
funkce	funkce	k1gFnSc1
okresního	okresní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
okresem	okres	k1gInSc7
není	být	k5eNaImIp3nS
a	a	k8xC
nebylo	být	k5eNaImAgNnS
a	a	k8xC
na	na	k7c4
okresy	okres	k1gInPc4
se	se	k3xPyFc4
nečlení	členit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
okresní	okresní	k2eAgInPc1d1
soudy	soud	k1gInPc1
zde	zde	k6eAd1
působí	působit	k5eAaImIp3nP
obvodní	obvodní	k2eAgInPc1d1
soudy	soud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Benešov	Benešov	k1gInSc1
</s>
<s>
Beroun	Beroun	k1gInSc1
</s>
<s>
Kladno	Kladno	k1gNnSc1
</s>
<s>
Kolín	Kolín	k1gInSc1
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Mělník	Mělník	k1gInSc1
</s>
<s>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
Nymburk	Nymburk	k1gInSc1
</s>
<s>
Praha-východ	Praha-východ	k1gInSc1
</s>
<s>
Praha-západ	Praha-západ	k6eAd1
</s>
<s>
Příbram	Příbram	k1gFnSc1
</s>
<s>
Rakovník	Rakovník	k1gInSc1
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
</s>
<s>
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
</s>
<s>
Písek	Písek	k1gInSc1
</s>
<s>
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
Tábor	Tábor	k1gInSc1
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Domažlice	Domažlice	k1gFnPc1
</s>
<s>
Klatovy	Klatovy	k1gInPc1
</s>
<s>
Plzeň-jih	Plzeň-jih	k1gMnSc1
</s>
<s>
Plzeň-město	Plzeň-město	k6eAd1
</s>
<s>
Plzeň-sever	Plzeň-sever	k1gMnSc1
</s>
<s>
Rokycany	Rokycany	k1gInPc1
</s>
<s>
Tachov	Tachov	k1gInSc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Cheb	Cheb	k1gInSc1
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
</s>
<s>
Chomutov	Chomutov	k1gInSc1
</s>
<s>
Litoměřice	Litoměřice	k1gInPc1
</s>
<s>
Louny	Louny	k1gInPc1
</s>
<s>
Most	most	k1gInSc1
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
Liberec	Liberec	k1gInSc1
</s>
<s>
Semily	Semily	k1gInPc1
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Jičín	Jičín	k1gInSc1
</s>
<s>
Náchod	Náchod	k1gInSc1
</s>
<s>
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
</s>
<s>
Trutnov	Trutnov	k1gInSc1
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Chrudim	Chrudim	k1gFnSc1
</s>
<s>
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Svitavy	Svitava	k1gFnPc1
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
</s>
<s>
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Pelhřimov	Pelhřimov	k1gInSc1
</s>
<s>
Třebíč	Třebíč	k1gFnSc1
</s>
<s>
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Blansko	Blansko	k1gNnSc1
</s>
<s>
Brno-město	Brno-město	k6eAd1
</s>
<s>
Brno-venkov	Brno-venkov	k1gInSc1
</s>
<s>
Břeclav	Břeclav	k1gFnSc1
</s>
<s>
Hodonín	Hodonín	k1gInSc1
</s>
<s>
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Jeseník	Jeseník	k1gInSc1
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Přerov	Přerov	k1gInSc1
</s>
<s>
Šumperk	Šumperk	k1gInSc1
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Bruntál	Bruntál	k1gInSc1
</s>
<s>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
</s>
<s>
Karviná	Karviná	k1gFnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Opava	Opava	k1gFnSc1
</s>
<s>
Ostrava-město	Ostrava-město	k6eAd1
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Vsetín	Vsetín	k1gInSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOUDELKA	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodka	k1gFnSc6
územní	územní	k2eAgFnSc6d1
samosprávou	samospráva	k1gFnSc7
po	po	k7c4
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Linde	Lind	k1gMnSc5
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7201	#num#	k4
<g/>
-	-	kIx~
<g/>
403	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
76	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
4	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
51	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Čl	čl	kA
<g/>
.	.	kIx.
CXVII	CXVII	kA
odstavec	odstavec	k1gInSc4
1	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
320	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
zrušení	zrušení	k1gNnSc4
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
ukončením	ukončení	k1gNnSc7
činnosti	činnost	k1gFnSc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
↑	↑	k?
§	§	k?
7	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
435	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
§	§	k?
6	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
582	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Příloha	příloha	k1gFnSc1
zákona	zákon	k1gInSc2
č.	č.	k?
139	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
§	§	k?
10	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
220	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
§	§	k?
8	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
381	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
<g/>
↑	↑	k?
§	§	k?
9	#num#	k4
odst	odstum	k1gNnPc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
a	a	k8xC
přílohy	příloh	k1gInPc1
č.	č.	k?
3	#num#	k4
a	a	k8xC
4	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
soudech	soud	k1gInPc6
a	a	k8xC
soudcích	soudek	k1gInPc6
<g/>
,	,	kIx,
§	§	k?
7	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
283	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státním	státní	k2eAgNnSc6d1
zastupitelství	zastupitelství	k1gNnSc6
<g/>
↑	↑	k?
§	§	k?
4	#num#	k4
a	a	k8xC
§	§	k?
12	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
↑	↑	k?
mld.	mld.	k?
Posunuly	posunout	k5eAaPmAgFnP
se	se	k3xPyFc4
hranice	hranice	k1gFnPc1
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
musejí	muset	k5eAaImIp3nP
na	na	k7c4
úřady	úřad	k1gInPc4
pro	pro	k7c4
občanské	občanský	k2eAgInPc4d1
průkazy	průkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-05	2021-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
4	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
okresů	okres	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Česka	Česko	k1gNnSc2
</s>
<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
NUTS	NUTS	kA
</s>
<s>
Mikroregion	mikroregion	k1gInSc1
</s>
<s>
Region	region	k1gInSc1
</s>
<s>
Mezoregion	Mezoregion	k1gInSc1
</s>
<s>
Makroregion	Makroregion	k1gInSc1
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
Okresní	okresní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
okres	okres	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
(	(	kIx(
<g/>
č.	č.	k?
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
o	o	k7c6
okresních	okresní	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
<g/>
,	,	kIx,
úpravě	úprava	k1gFnSc3
jejich	jejich	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
a	a	k8xC
o	o	k7c6
některých	některý	k3yIgNnPc6
dalších	další	k2eAgNnPc6d1
opatřeních	opatření	k1gNnPc6
s	s	k7c7
tím	ten	k3xDgInSc7
souvisejících	související	k2eAgInPc2d1
(	(	kIx(
<g/>
č.	č.	k?
425	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
okresních	okresní	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
(	(	kIx(
<g/>
č.	č.	k?
147	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
(	(	kIx(
<g/>
č.	č.	k?
347	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
)	)	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
zrušení	zrušení	k1gNnSc4
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
ukončením	ukončení	k1gNnSc7
činnosti	činnost	k1gFnSc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
</s>
<s>
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
na	na	k7c4
Zákony	zákon	k1gInPc4
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
Evropy	Evropa	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Athos	Athos	k1gMnSc1
(	(	kIx(
<g/>
GR	GR	kA
<g/>
)	)	kIx)
</s>
<s>
Alandy	Alanda	k1gFnPc1
(	(	kIx(
<g/>
FI	fi	k0
<g/>
)	)	kIx)
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Guernsey	Guernsea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Mayen	Mayna	k1gFnPc2
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
</s>
<s>
Jersey	Jersea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Špicberky	Špicberky	k1gFnPc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
Území	území	k1gNnSc1
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
