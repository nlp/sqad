<s>
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Red	Red	k1gMnSc1	Red
Dwarf	Dwarf	k1gMnSc1	Dwarf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgInSc1d1	britský
sitcom	sitcom	k1gInSc1	sitcom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
natáčený	natáčený	k2eAgInSc1d1	natáčený
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
a	a	k8xC	a
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
sci-fi	scii	k1gFnSc2	sci-fi
coby	coby	k?	coby
parodie	parodie	k1gFnSc2	parodie
<g/>
.	.	kIx.	.
</s>
