<p>
<s>
ČSN	ČSN	kA	ČSN
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc1d1	chráněné
označení	označení	k1gNnSc1	označení
českých	český	k2eAgFnPc2d1	Česká
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
ČSN	ČSN	kA	ČSN
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
československých	československý	k2eAgFnPc2d1	Československá
státních	státní	k2eAgFnPc2d1	státní
norem	norma	k1gFnPc2	norma
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
československých	československý	k2eAgFnPc2d1	Československá
norem	norma	k1gFnPc2	norma
(	(	kIx(	(
<g/>
československých	československý	k2eAgFnPc2d1	Československá
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
vydávání	vydávání	k1gNnSc4	vydávání
ČSN	ČSN	kA	ČSN
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označování	označování	k1gNnSc1	označování
norem	norma	k1gFnPc2	norma
==	==	k?	==
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
ČSN	ČSN	kA	ČSN
původně	původně	k6eAd1	původně
znamenala	znamenat	k5eAaImAgFnS	znamenat
Československá	československý	k2eAgFnSc1d1	Československá
státní	státní	k2eAgFnSc1d1	státní
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Československá	československý	k2eAgFnSc1d1	Československá
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
existovaly	existovat	k5eAaImAgFnP	existovat
ještě	ještě	k9	ještě
centrálně	centrálně	k6eAd1	centrálně
vydávané	vydávaný	k2eAgFnPc1d1	vydávaná
oborové	oborový	k2eAgFnPc1d1	oborová
normy	norma	k1gFnPc1	norma
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ON	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslované	číslovaný	k2eAgNnSc1d1	číslované
shodným	shodný	k2eAgInSc7d1	shodný
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
úsekové	úsekový	k2eAgFnPc1d1	úseková
normy	norma	k1gFnPc1	norma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
podnikové	podnikový	k2eAgFnPc1d1	podniková
normy	norma	k1gFnPc1	norma
(	(	kIx(	(
<g/>
PN	PN	kA	PN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
ČSN	ČSN	kA	ČSN
zachováno	zachovat	k5eAaPmNgNnS	zachovat
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
závazný	závazný	k2eAgInSc1d1	závazný
výklad	výklad	k1gInSc1	výklad
zkratky	zkratka	k1gFnSc2	zkratka
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiálně	oficiálně	k6eNd1	oficiálně
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
vykládá	vykládat	k5eAaImIp3nS	vykládat
slovy	slovo	k1gNnPc7	slovo
Česká	český	k2eAgFnSc1d1	Česká
soustava	soustava	k1gFnSc1	soustava
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
chráněné	chráněný	k2eAgNnSc1d1	chráněné
výlučné	výlučný	k2eAgNnSc1d1	výlučné
slovní	slovní	k2eAgNnSc1d1	slovní
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
písmennou	písmenný	k2eAgFnSc7d1	písmenná
značkou	značka	k1gFnSc7	značka
normy	norma	k1gFnSc2	norma
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
šestimístné	šestimístný	k2eAgNnSc1d1	šestimístné
třídicí	třídicí	k2eAgNnSc1d1	třídicí
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
první	první	k4xOgNnSc4	první
dvojčíslí	dvojčíslí	k1gNnSc4	dvojčíslí
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
mezerou	mezera	k1gFnSc7	mezera
a	a	k8xC	a
</s>
</p>
<p>
<s>
značí	značit	k5eAaImIp3nS	značit
třídu	třída	k1gFnSc4	třída
norem	norma	k1gFnPc2	norma
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
99	[number]	k4	99
udává	udávat	k5eAaImIp3nS	udávat
širší	široký	k2eAgInSc4d2	širší
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
obor	obor	k1gInSc4	obor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
číslice	číslice	k1gFnSc1	číslice
označuje	označovat	k5eAaImIp3nS	označovat
skupinu	skupina	k1gFnSc4	skupina
a	a	k8xC	a
podskupinu	podskupina	k1gFnSc4	podskupina
norem	norma	k1gFnPc2	norma
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
dvojčíslí	dvojčíslí	k1gNnSc4	dvojčíslí
představuje	představovat	k5eAaImIp3nS	představovat
pořadové	pořadový	k2eAgNnSc4d1	pořadové
číslo	číslo	k1gNnSc4	číslo
normy	norma	k1gFnSc2	norma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Převzaté	převzatý	k2eAgFnPc1d1	převzatá
(	(	kIx(	(
<g/>
harmonizované	harmonizovaný	k2eAgFnPc1d1	harmonizovaná
<g/>
)	)	kIx)	)
Evropské	evropský	k2eAgFnPc1d1	Evropská
normy	norma	k1gFnPc1	norma
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
původním	původní	k2eAgNnSc7d1	původní
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
před	před	k7c4	před
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
přidána	přidán	k2eAgFnSc1d1	přidána
zkratka	zkratka	k1gFnSc1	zkratka
ČSN	ČSN	kA	ČSN
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označena	označit	k5eAaPmNgFnS	označit
například	například	k6eAd1	například
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
12899	[number]	k4	12899
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9001	[number]	k4	9001
<g/>
,	,	kIx,	,
ČSN	ČSN	kA	ČSN
IEC	IEC	kA	IEC
61713	[number]	k4	61713
<g/>
,	,	kIx,	,
ČSN	ČSN	kA	ČSN
ETS	ETS	kA	ETS
300	[number]	k4	300
976	[number]	k4	976
apod.	apod.	kA	apod.
Normě	Norma	k1gFnSc3	Norma
bývá	bývat	k5eAaImIp3nS	bývat
zároveň	zároveň	k6eAd1	zároveň
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
třídicí	třídicí	k2eAgInSc1d1	třídicí
znak	znak	k1gInSc1	znak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tradičního	tradiční	k2eAgNnSc2d1	tradiční
šesticiferného	šesticiferný	k2eAgNnSc2d1	šesticiferné
označení	označení	k1gNnSc2	označení
podle	podle	k7c2	podle
třídy	třída	k1gFnSc2	třída
ČSN	ČSN	kA	ČSN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Evropských	evropský	k2eAgFnPc2d1	Evropská
norem	norma	k1gFnPc2	norma
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
i	i	k9	i
praxe	praxe	k1gFnSc1	praxe
označovat	označovat	k5eAaImF	označovat
za	za	k7c7	za
dvojtečkou	dvojtečka	k1gFnSc7	dvojtečka
normu	norma	k1gFnSc4	norma
rokem	rok	k1gInSc7	rok
vydání	vydání	k1gNnPc2	vydání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9000	[number]	k4	9000
<g/>
:	:	kIx,	:
<g/>
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
udán	udat	k5eAaPmNgInS	udat
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
norma	norma	k1gFnSc1	norma
v	v	k7c6	v
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
znění	znění	k1gNnSc6	znění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legislativní	legislativní	k2eAgInSc4d1	legislativní
rámec	rámec	k1gInSc4	rámec
tvorby	tvorba	k1gFnSc2	tvorba
ČSN	ČSN	kA	ČSN
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
===	===	k?	===
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Československá	československý	k2eAgFnSc1d1	Československá
normalisační	normalisační	k2eAgFnSc1d1	normalisační
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
ČSN	ČSN	kA	ČSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byly	být	k5eAaImAgInP	být
velké	velký	k2eAgInPc1d1	velký
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
<g/>
Vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
311	[number]	k4	311
<g/>
/	/	kIx~	/
<g/>
1940	[number]	k4	1940
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
závaznosti	závaznost	k1gFnSc6	závaznost
českomoravských	českomoravský	k2eAgInPc2d1	českomoravský
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
československých	československý	k2eAgFnPc2d1	Československá
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
při	při	k7c6	při
dodávkách	dodávka	k1gFnPc6	dodávka
a	a	k8xC	a
pracích	prak	k1gInPc6	prak
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgInPc4d1	veřejný
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
ústavy	ústav	k1gInPc4	ústav
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
fondy	fond	k1gInPc4	fond
<g/>
,	,	kIx,	,
ukládalo	ukládat	k5eAaImAgNnS	ukládat
zadavatelům	zadavatel	k1gMnPc3	zadavatel
dodávek	dodávka	k1gFnPc2	dodávka
a	a	k8xC	a
prací	práce	k1gFnPc2	práce
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgInPc4d1	veřejný
subjekty	subjekt	k1gInPc4	subjekt
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
podporou	podpora	k1gFnSc7	podpora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dodavatelům	dodavatel	k1gMnPc3	dodavatel
a	a	k8xC	a
objednatelům	objednatel	k1gMnPc3	objednatel
ukládali	ukládat	k5eAaImAgMnP	ukládat
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
práce	práce	k1gFnSc1	práce
musí	muset	k5eAaImIp3nS	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
českomoravským	českomoravský	k2eAgFnPc3d1	Českomoravská
technickým	technický	k2eAgFnPc3d1	technická
normám	norma	k1gFnPc3	norma
<g/>
.	.	kIx.	.
</s>
<s>
Výtisky	výtisk	k1gInPc4	výtisk
norem	norma	k1gFnPc2	norma
dodával	dodávat	k5eAaImAgInS	dodávat
Normalisační	Normalisační	k2eAgInSc1d1	Normalisační
úřad	úřad	k1gInSc1	úřad
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
439	[number]	k4	439
<g/>
/	/	kIx~	/
<g/>
1941	[number]	k4	1941
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
závaznosti	závaznost	k1gFnSc6	závaznost
českomoravských	českomoravský	k2eAgFnPc2d1	Českomoravská
norem	norma	k1gFnPc2	norma
požárně-technických	požárněechnický	k2eAgFnPc2d1	požárně-technický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
201	[number]	k4	201
<g/>
/	/	kIx~	/
<g/>
1942	[number]	k4	1942
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
závaznosti	závaznost	k1gFnSc6	závaznost
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgFnPc2d1	obchodní
a	a	k8xC	a
dodacích	dodací	k2eAgFnPc2d1	dodací
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
předpisů	předpis	k1gInPc2	předpis
o	o	k7c6	o
jakosti	jakost	k1gFnSc6	jakost
a	a	k8xC	a
označování	označování	k1gNnSc4	označování
<g/>
,	,	kIx,	,
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhláškou	vyhláška	k1gFnSc7	vyhláška
v	v	k7c6	v
Úředním	úřední	k2eAgInSc6d1	úřední
listu	list	k1gInSc6	list
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
obecnou	obecný	k2eAgFnSc4d1	obecná
závaznost	závaznost	k1gFnSc4	závaznost
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
podmínek	podmínka	k1gFnPc2	podmínka
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československé	československý	k2eAgFnSc2d1	Československá
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
===	===	k?	===
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
84	[number]	k4	84
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
závaznosti	závaznost	k1gFnSc6	závaznost
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
ústředním	ústřední	k2eAgInPc3d1	ústřední
orgánům	orgán	k1gInPc3	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Úředním	úřední	k2eAgInSc6d1	úřední
listu	list	k1gInSc6	list
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
Úředním	úřední	k2eAgInSc6d1	úřední
věstníku	věstník	k1gInSc6	věstník
<g/>
,	,	kIx,	,
vyhláškou	vyhláška	k1gFnSc7	vyhláška
stanovily	stanovit	k5eAaPmAgInP	stanovit
závaznost	závaznost	k1gFnSc4	závaznost
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
nebo	nebo	k8xC	nebo
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
Československá	československý	k2eAgFnSc1d1	Československá
společnost	společnost	k1gFnSc1	společnost
normalizační	normalizační	k2eAgFnSc1d1	normalizační
nebo	nebo	k8xC	nebo
Elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
svaz	svaz	k1gInSc1	svaz
československý	československý	k2eAgInSc1d1	československý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
45	[number]	k4	45
<g/>
/	/	kIx~	/
<g/>
1951	[number]	k4	1951
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
normalisaci	normalisace	k1gFnSc6	normalisace
<g/>
,	,	kIx,	,
zavádělo	zavádět	k5eAaImAgNnS	zavádět
státní	státní	k2eAgNnSc1d1	státní
<g/>
,	,	kIx,	,
úsekové	úsekový	k2eAgFnPc1d1	úseková
a	a	k8xC	a
podnikové	podnikový	k2eAgFnPc1d1	podniková
normy	norma	k1gFnPc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
norma	norma	k1gFnSc1	norma
nebyla	být	k5eNaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k9	jako
směrná	směrná	k1gFnSc1	směrná
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
závazná	závazný	k2eAgFnSc1d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
Vydávání	vydávání	k1gNnSc1	vydávání
norem	norma	k1gFnPc2	norma
řídil	řídit	k5eAaImAgInS	řídit
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
normalisaci	normalisace	k1gFnSc4	normalisace
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc4	vydání
normy	norma	k1gFnSc2	norma
bylo	být	k5eAaImAgNnS	být
oznamováno	oznamovat	k5eAaImNgNnS	oznamovat
v	v	k7c6	v
Úředním	úřední	k2eAgInSc6d1	úřední
listu	list	k1gInSc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zavedená	zavedený	k2eAgFnSc1d1	zavedená
zkratka	zkratka	k1gFnSc1	zkratka
ČSN	ČSN	kA	ČSN
měla	mít	k5eAaImAgFnS	mít
jednoznačně	jednoznačně	k6eAd1	jednoznačně
stanovený	stanovený	k2eAgInSc4d1	stanovený
význam	význam	k1gInSc4	význam
Československá	československý	k2eAgFnSc1d1	Československá
státní	státní	k2eAgFnSc1d1	státní
norma	norma	k1gFnSc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
normalisaci	normalisace	k1gFnSc4	normalisace
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
propracovávat	propracovávat	k5eAaImF	propracovávat
metody	metoda	k1gFnPc1	metoda
přechodu	přechod	k1gInSc2	přechod
na	na	k7c4	na
sovětské	sovětský	k2eAgFnPc4d1	sovětská
státní	státní	k2eAgFnPc4d1	státní
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
35	[number]	k4	35
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
normalisaci	normalisace	k1gFnSc6	normalisace
doplnilo	doplnit	k5eAaPmAgNnS	doplnit
vládní	vládní	k2eAgNnSc1d1	vládní
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
46	[number]	k4	46
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
normalisaci	normalisace	k1gFnSc6	normalisace
<g/>
,	,	kIx,	,
Směrnice	směrnice	k1gFnSc1	směrnice
č.	č.	k?	č.
79	[number]	k4	79
<g/>
/	/	kIx~	/
<g/>
1958	[number]	k4	1958
Ú.	Ú.	kA	Ú.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
o	o	k7c4	o
vykládání	vykládání	k1gNnSc4	vykládání
návrhů	návrh	k1gInPc2	návrh
státních	státní	k2eAgFnPc2d1	státní
norem	norma	k1gFnPc2	norma
a	a	k8xC	a
podávání	podávání	k1gNnSc1	podávání
připomínek	připomínka	k1gFnPc2	připomínka
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
návrhům	návrh	k1gInPc3	návrh
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
156	[number]	k4	156
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
(	(	kIx(	(
<g/>
oborové	oborový	k2eAgFnSc2d1	oborová
normy	norma	k1gFnSc2	norma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technickými	technický	k2eAgFnPc7d1	technická
normami	norma	k1gFnPc7	norma
byly	být	k5eAaImAgFnP	být
nejen	nejen	k6eAd1	nejen
státní	státní	k2eAgFnPc1d1	státní
<g/>
,	,	kIx,	,
úsekové	úsekový	k2eAgFnPc1d1	úseková
a	a	k8xC	a
podnikové	podnikový	k2eAgFnPc1d1	podniková
normy	norma	k1gFnPc1	norma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sjednané	sjednaný	k2eAgFnPc4d1	sjednaná
technické	technický	k2eAgFnPc4d1	technická
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Státní	státní	k2eAgInSc1d1	státní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
vynálezy	vynález	k1gInPc4	vynález
a	a	k8xC	a
normalisaci	normalisace	k1gFnSc4	normalisace
jako	jako	k8xC	jako
technické	technický	k2eAgFnPc4d1	technická
normy	norma	k1gFnPc4	norma
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
nebyla	být	k5eNaImAgFnS	být
označená	označený	k2eAgFnSc1d1	označená
jako	jako	k8xC	jako
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
závazná	závazný	k2eAgFnSc1d1	závazná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
96	[number]	k4	96
<g/>
/	/	kIx~	/
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
normalizaci	normalizace	k1gFnSc6	normalizace
a	a	k8xC	a
vyhláška	vyhláška	k1gFnSc1	vyhláška
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
č.	č.	k?	č.
97	[number]	k4	97
<g/>
/	/	kIx~	/
<g/>
1964	[number]	k4	1964
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
technické	technický	k2eAgFnSc6d1	technická
normalizaci	normalizace	k1gFnSc6	normalizace
<g/>
,	,	kIx,	,
zavedly	zavést	k5eAaPmAgInP	zavést
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
norem	norma	k1gFnPc2	norma
<g/>
:	:	kIx,	:
státní	státní	k2eAgFnSc1d1	státní
norma	norma	k1gFnSc1	norma
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oborová	oborový	k2eAgFnSc1d1	oborová
norma	norma	k1gFnSc1	norma
(	(	kIx(	(
<g/>
ON	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
a	a	k8xC	a
podniková	podnikový	k2eAgFnSc1d1	podniková
norma	norma	k1gFnSc1	norma
(	(	kIx(	(
<g/>
PN	PN	kA	PN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc2d1	státní
normy	norma	k1gFnSc2	norma
schvaloval	schvalovat	k5eAaImAgInS	schvalovat
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
<g/>
,	,	kIx,	,
oborové	oborový	k2eAgFnPc4d1	oborová
normy	norma	k1gFnPc4	norma
příslušný	příslušný	k2eAgInSc4d1	příslušný
ústřední	ústřední	k2eAgInSc4d1	ústřední
orgán	orgán	k1gInSc4	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
byly	být	k5eAaImAgFnP	být
závazné	závazný	k2eAgFnPc1d1	závazná
pro	pro	k7c4	pro
organizace	organizace	k1gFnPc1	organizace
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
příslušné	příslušný	k2eAgInPc4d1	příslušný
výrobky	výrobek	k1gInPc4	výrobek
nebo	nebo	k8xC	nebo
provádějící	provádějící	k2eAgFnSc4d1	provádějící
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
též	též	k9	též
pro	pro	k7c4	pro
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
byli	být	k5eAaImAgMnP	být
seznámeni	seznámit	k5eAaPmNgMnP	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
zákon	zákon	k1gInSc1	zákon
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
právní	právní	k2eAgInPc4d1	právní
předpisy	předpis	k1gInPc4	předpis
konstatováním	konstatování	k1gNnSc7	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
nesměla	smět	k5eNaImAgFnS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
technickou	technický	k2eAgFnSc7d1	technická
normou	norma	k1gFnSc7	norma
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
mohl	moct	k5eAaImAgInS	moct
povolit	povolit	k5eAaPmF	povolit
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
normu	norma	k1gFnSc4	norma
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
orgán	orgán	k1gInSc1	orgán
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
uvedený	uvedený	k2eAgInSc1d1	uvedený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
upravoval	upravovat	k5eAaImAgInS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
142	[number]	k4	142
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
československých	československý	k2eAgFnPc6d1	Československá
technických	technický	k2eAgFnPc6d1	technická
normách	norma	k1gFnPc6	norma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
632	[number]	k4	632
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
byla	být	k5eAaImAgNnP	být
obecně	obecně	k6eAd1	obecně
závazná	závazný	k2eAgNnPc1d1	závazné
ta	ten	k3xDgNnPc1	ten
ustanovení	ustanovení	k1gNnPc1	ustanovení
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
byl	být	k5eAaImAgInS	být
Federální	federální	k2eAgInSc1d1	federální
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novely	novela	k1gFnSc2	novela
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
pozbyly	pozbýt	k5eAaPmAgFnP	pozbýt
platnosti	platnost	k1gFnSc6	platnost
všechny	všechen	k3xTgFnPc1	všechen
oborové	oborový	k2eAgFnPc1d1	oborová
normy	norma	k1gFnPc1	norma
(	(	kIx(	(
<g/>
ON	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1994	[number]	k4	1994
pozbyly	pozbýt	k5eAaPmAgFnP	pozbýt
závaznosti	závaznost	k1gFnPc1	závaznost
všechny	všechen	k3xTgFnPc1	všechen
starší	starý	k2eAgMnSc1d2	starší
ČSN	ČSN	kA	ČSN
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
nebyla	být	k5eNaImAgFnS	být
závaznost	závaznost	k1gFnSc1	závaznost
výslovně	výslovně	k6eAd1	výslovně
uvedena	uvést	k5eAaPmNgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
ČSN	ČSN	kA	ČSN
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nenazývají	nazývat	k5eNaImIp3nP	nazývat
"	"	kIx"	"
<g/>
státní	státní	k2eAgFnPc1d1	státní
normy	norma	k1gFnPc1	norma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
československé	československý	k2eAgFnSc2d1	Československá
normy	norma	k1gFnSc2	norma
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
československé	československý	k2eAgFnSc2d1	Československá
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
===	===	k?	===
</s>
</p>
<p>
<s>
Právní	právní	k2eAgInSc1d1	právní
rámec	rámec	k1gInSc1	rámec
technické	technický	k2eAgFnSc2d1	technická
normalizace	normalizace	k1gFnSc2	normalizace
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
technických	technický	k2eAgInPc6d1	technický
požadavcích	požadavek	k1gInPc6	požadavek
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
vydáváním	vydávání	k1gNnSc7	vydávání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
také	také	k9	také
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
technické	technický	k2eAgFnPc1d1	technická
normy	norma	k1gFnPc1	norma
nejsou	být	k5eNaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
právně	právně	k6eAd1	právně
závazné	závazný	k2eAgNnSc1d1	závazné
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
závaznost	závaznost	k1gFnSc1	závaznost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
nebo	nebo	k8xC	nebo
vyplynout	vyplynout	k5eAaPmF	vyplynout
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
právního	právní	k2eAgInSc2d1	právní
předpisu	předpis	k1gInSc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
povolit	povolit	k5eAaPmF	povolit
tzv.	tzv.	kA	tzv.
neopomenutelný	opomenutelný	k2eNgMnSc1d1	neopomenutelný
účastník	účastník	k1gMnSc1	účastník
uvedený	uvedený	k2eAgMnSc1d1	uvedený
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
je	být	k5eAaImIp3nS	být
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
rovněž	rovněž	k9	rovněž
zakázal	zakázat	k5eAaPmAgInS	zakázat
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
českých	český	k2eAgFnPc2d1	Česká
norem	norma	k1gFnPc2	norma
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
zavádí	zavádět	k5eAaImIp3nS	zavádět
pojmy	pojem	k1gInPc4	pojem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
technický	technický	k2eAgInSc1d1	technický
předpis	předpis	k1gInSc1	předpis
–	–	k?	–
právní	právní	k2eAgInSc4d1	právní
předpis	předpis	k1gInSc4	předpis
obsahující	obsahující	k2eAgInPc4d1	obsahující
technické	technický	k2eAgInPc4d1	technický
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
výrobek	výrobek	k1gInSc4	výrobek
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
<g/>
)	)	kIx)	)
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
–	–	k?	–
norma	norma	k1gFnSc1	norma
přijatá	přijatý	k2eAgFnSc1d1	přijatá
postupem	postup	k1gInSc7	postup
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
oznámená	oznámený	k2eAgFnSc1d1	oznámená
ve	v	k7c6	v
věstníku	věstník	k1gInSc6	věstník
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
</s>
</p>
<p>
<s>
jiná	jiný	k2eAgFnSc1d1	jiná
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
–	–	k?	–
pojem	pojem	k1gInSc1	pojem
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
definován	definovat	k5eAaBmNgInS	definovat
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
zejména	zejména	k9	zejména
technické	technický	k2eAgFnPc4d1	technická
normy	norma	k1gFnPc4	norma
přijaté	přijatý	k2eAgNnSc1d1	přijaté
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
nebo	nebo	k8xC	nebo
nadnárodních	nadnárodní	k2eAgFnPc6d1	nadnárodní
institucích	instituce	k1gFnPc6	instituce
</s>
</p>
<p>
<s>
technický	technický	k2eAgInSc1d1	technický
dokument	dokument	k1gInSc1	dokument
–	–	k?	–
jiný	jiný	k2eAgInSc4d1	jiný
dokument	dokument	k1gInSc4	dokument
obsahující	obsahující	k2eAgInPc4d1	obsahující
technické	technický	k2eAgInPc4d1	technický
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
technickým	technický	k2eAgInSc7d1	technický
předpisem	předpis	k1gInSc7	předpis
ani	ani	k8xC	ani
technickou	technický	k2eAgFnSc4d1	technická
normouZávaznost	normouZávaznost	k1gFnSc4	normouZávaznost
nebo	nebo	k8xC	nebo
doporučení	doporučení	k1gNnSc4	doporučení
normy	norma	k1gFnSc2	norma
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc2	její
části	část	k1gFnSc2	část
může	moct	k5eAaImIp3nS	moct
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
právní	právní	k2eAgInSc1d1	právní
předpis	předpis	k1gInSc1	předpis
(	(	kIx(	(
<g/>
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stanoví	stanovit	k5eAaPmIp3nP	stanovit
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
povinnost	povinnost	k1gFnSc4	povinnost
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
technickými	technický	k2eAgFnPc7d1	technická
normami	norma	k1gFnPc7	norma
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
výlučný	výlučný	k2eAgInSc1d1	výlučný
odkaz	odkaz	k1gInSc1	odkaz
–	–	k?	–
závazné	závazný	k2eAgInPc1d1	závazný
tj.	tj.	kA	tj.
naplnění	naplnění	k1gNnSc1	naplnění
požadavku	požadavek	k1gInSc2	požadavek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
postupem	postup	k1gInSc7	postup
dle	dle	k7c2	dle
normy	norma	k1gFnSc2	norma
</s>
</p>
<p>
<s>
indikativní	indikativní	k2eAgInSc1d1	indikativní
odkaz	odkaz	k1gInSc1	odkaz
–	–	k?	–
doporučení	doporučení	k1gNnSc1	doporučení
tj.	tj.	kA	tj.
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
naplnění	naplnění	k1gNnSc2	naplnění
požadavku	požadavek	k1gInSc2	požadavek
je	být	k5eAaImIp3nS	být
postupovat	postupovat	k5eAaImF	postupovat
dle	dle	k7c2	dle
normy	norma	k1gFnSc2	norma
</s>
</p>
<p>
<s>
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c4	o
dílo	dílo	k1gNnSc4	dílo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pokyn	pokyn	k1gInSc1	pokyn
nadřízeného	nadřízený	k2eAgMnSc2d1	nadřízený
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prací	práce	k1gFnPc2	práce
podniku	podnik	k1gInSc2	podnik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
správního	správní	k2eAgInSc2d1	správní
orgánu	orgán	k1gInSc2	orgán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dotčený	dotčený	k2eAgInSc1d1	dotčený
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
v	v	k7c6	v
územním	územní	k2eAgNnSc6d1	územní
řízení	řízení	k1gNnSc6	řízení
dle	dle	k7c2	dle
Stavebního	stavební	k2eAgInSc2d1	stavební
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vydavatel	vydavatel	k1gMnSc1	vydavatel
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
vydalo	vydat	k5eAaPmAgNnS	vydat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
č.	č.	k?	č.
203	[number]	k4	203
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
pověřen	pověřit	k5eAaPmNgMnS	pověřit
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
vydáváním	vydávání	k1gNnSc7	vydávání
českých	český	k2eAgFnPc2d1	Česká
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
Český	český	k2eAgInSc1d1	český
normalizační	normalizační	k2eAgInSc1d1	normalizační
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
vykonával	vykonávat	k5eAaImAgInS	vykonávat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ministra	ministr	k1gMnSc2	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
tak	tak	k6eAd1	tak
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
vydávání	vydávání	k1gNnSc4	vydávání
ČSN	ČSN	kA	ČSN
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právní	právní	k2eAgFnSc1d1	právní
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Jakožto	jakožto	k8xS	jakožto
úřední	úřední	k2eAgNnPc1d1	úřední
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
normy	norma	k1gFnPc1	norma
ČSN	ČSN	kA	ČSN
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
z	z	k7c2	z
ochrany	ochrana	k1gFnSc2	ochrana
autorským	autorský	k2eAgInSc7d1	autorský
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Práva	práv	k2eAgFnSc1d1	práva
obdobná	obdobný	k2eAgFnSc1d1	obdobná
autorským	autorský	k2eAgNnSc7d1	autorské
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
chráněna	chránit	k5eAaImNgFnS	chránit
speciálním	speciální	k2eAgNnSc7d1	speciální
ustanovením	ustanovení	k1gNnSc7	ustanovení
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
22	[number]	k4	22
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
§	§	k?	§
5	[number]	k4	5
<g/>
,	,	kIx,	,
odstavec	odstavec	k1gInSc1	odstavec
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
českých	český	k2eAgFnPc2d1	Česká
norem	norma	k1gFnPc2	norma
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
183	[number]	k4	183
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
§	§	k?	§
196	[number]	k4	196
uváděl	uvádět	k5eAaImAgInS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
právní	právní	k2eAgInSc1d1	právní
předpis	předpis	k1gInSc1	předpis
vydaný	vydaný	k2eAgInSc1d1	vydaný
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
provedení	provedení	k1gNnSc3	provedení
stanoví	stanovit	k5eAaPmIp3nS	stanovit
povinnost	povinnost	k1gFnSc1	povinnost
postupovat	postupovat	k5eAaImF	postupovat
podle	podle	k7c2	podle
technické	technický	k2eAgFnSc2d1	technická
normy	norma	k1gFnSc2	norma
(	(	kIx(	(
<g/>
ČSN	ČSN	kA	ČSN
<g/>
,	,	kIx,	,
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
<g />
.	.	kIx.	.
</s>
<s>
technická	technický	k2eAgFnSc1d1	technická
norma	norma	k1gFnSc1	norma
bezplatně	bezplatně	k6eAd1	bezplatně
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
ÚNMZ	ÚNMZ	kA	ÚNMZ
toto	tento	k3xDgNnSc4	tento
ustanovení	ustanovení	k1gNnSc4	ustanovení
vykládal	vykládat	k5eAaImAgMnS	vykládat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačí	stačit	k5eAaBmIp3nS	stačit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Národní	národní	k2eAgFnSc6d1	národní
knihovně	knihovna	k1gFnSc6	knihovna
umístit	umístit	k5eAaPmF	umístit
jeden	jeden	k4xCgInSc4	jeden
výtisk	výtisk	k1gInSc4	výtisk
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
nesmí	smět	k5eNaImIp3nS	smět
nic	nic	k3yNnSc1	nic
okopírovat	okopírovat	k5eAaPmF	okopírovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
poskytovat	poskytovat	k5eAaImF	poskytovat
úplné	úplný	k2eAgNnSc4d1	úplné
znění	znění	k1gNnSc4	znění
norem	norma	k1gFnPc2	norma
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnPc1	žádost
o	o	k7c4	o
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
úřadu	úřad	k1gInSc2	úřad
nařídil	nařídit	k5eAaPmAgMnS	nařídit
normy	norma	k1gFnPc4	norma
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
informaci	informace	k1gFnSc4	informace
poskytnout	poskytnout	k5eAaPmF	poskytnout
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
Jan	Jan	k1gMnSc1	Jan
Mládek	Mládek	k1gMnSc1	Mládek
následně	následně	k6eAd1	následně
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
nechal	nechat	k5eAaPmAgMnS	nechat
přílepkem	přílepek	k1gInSc7	přílepek
k	k	k7c3	k
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
technických	technický	k2eAgInPc6d1	technický
požadavcích	požadavek	k1gInPc6	požadavek
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
příslušné	příslušný	k2eAgNnSc4d1	příslušné
ustanovení	ustanovení	k1gNnSc4	ustanovení
stavebního	stavební	k2eAgInSc2d1	stavební
zákona	zákon	k1gInSc2	zákon
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
tříd	třída	k1gFnPc2	třída
==	==	k?	==
</s>
</p>
<p>
<s>
01	[number]	k4	01
Obecná	obecný	k2eAgFnSc1d1	obecná
třída	třída	k1gFnSc1	třída
</s>
</p>
<p>
<s>
02	[number]	k4	02
Strojní	strojní	k2eAgFnPc4d1	strojní
součásti	součást	k1gFnPc4	součást
</s>
</p>
<p>
<s>
03	[number]	k4	03
Strojní	strojní	k2eAgFnPc4d1	strojní
součásti	součást	k1gFnPc4	součást
-	-	kIx~	-
koroze	koroze	k1gFnSc1	koroze
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
materiálu	materiál	k1gInSc2	materiál
</s>
</p>
<p>
<s>
04	[number]	k4	04
Slévárenství	slévárenství	k1gNnSc1	slévárenství
</s>
</p>
<p>
<s>
05	[number]	k4	05
Svařování	svařování	k1gNnSc1	svařování
<g/>
,	,	kIx,	,
pájení	pájení	k1gNnSc1	pájení
<g/>
,	,	kIx,	,
řezání	řezání	k1gNnSc1	řezání
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
plastů	plast	k1gInPc2	plast
</s>
</p>
<p>
<s>
06	[number]	k4	06
Topení	topení	k1gNnSc1	topení
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
pece	pec	k1gFnPc1	pec
<g/>
,	,	kIx,	,
vařidla	vařidlo	k1gNnPc1	vařidlo
a	a	k8xC	a
topidla	topidlo	k1gNnPc1	topidlo
</s>
</p>
<p>
<s>
07	[number]	k4	07
Kotle	kotel	k1gInSc2	kotel
</s>
</p>
<p>
<s>
08	[number]	k4	08
Turbíny	turbína	k1gFnSc2	turbína
</s>
</p>
<p>
<s>
09	[number]	k4	09
Spalovací	spalovací	k2eAgInPc1d1	spalovací
motory	motor	k1gInPc1	motor
pístové	pístový	k2eAgInPc1d1	pístový
</s>
</p>
<p>
<s>
10	[number]	k4	10
Kompresory	kompresor	k1gInPc1	kompresor
<g/>
,	,	kIx,	,
vakuová	vakuový	k2eAgFnSc1d1	vakuová
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
pneumatická	pneumatický	k2eAgNnPc1d1	pneumatické
zařízení	zařízení	k1gNnPc1	zařízení
</s>
</p>
<p>
<s>
11	[number]	k4	11
Čerpadla	čerpadlo	k1gNnPc4	čerpadlo
<g/>
,	,	kIx,	,
hydraulická	hydraulický	k2eAgNnPc4d1	hydraulické
zařízení	zařízení	k1gNnPc4	zařízení
</s>
</p>
<p>
<s>
12	[number]	k4	12
Vzduchotechnická	vzduchotechnický	k2eAgFnSc1d1	vzduchotechnická
zařízení	zařízení	k1gNnPc1	zařízení
</s>
</p>
<p>
<s>
13	[number]	k4	13
Armatury	armatura	k1gFnSc2	armatura
a	a	k8xC	a
potrubí	potrubí	k1gNnSc2	potrubí
</s>
</p>
<p>
<s>
14	[number]	k4	14
Chladicí	chladicí	k2eAgFnSc1d1	chladicí
technika	technika	k1gFnSc1	technika
</s>
</p>
<p>
<s>
15	[number]	k4	15
Výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
drátu	drát	k1gInSc2	drát
</s>
</p>
<p>
<s>
16	[number]	k4	16
Výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
drátu	drát	k1gInSc2	drát
</s>
</p>
<p>
<s>
17	[number]	k4	17
Jemná	jemný	k2eAgFnSc1d1	jemná
mechanika	mechanika	k1gFnSc1	mechanika
</s>
</p>
<p>
<s>
18	[number]	k4	18
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
automatizace	automatizace	k1gFnSc1	automatizace
</s>
</p>
<p>
<s>
19	[number]	k4	19
Optické	optický	k2eAgInPc1d1	optický
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
kinematografii	kinematografie	k1gFnSc4	kinematografie
a	a	k8xC	a
reprografii	reprografie	k1gFnSc4	reprografie
</s>
</p>
<p>
<s>
20	[number]	k4	20
Obráběcí	obráběcí	k2eAgInPc1d1	obráběcí
stroje	stroj	k1gInPc1	stroj
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
</s>
</p>
<p>
<s>
21	[number]	k4	21
Tvářecí	tvářecí	k2eAgInPc1d1	tvářecí
stroje	stroj	k1gInPc1	stroj
</s>
</p>
<p>
<s>
22	[number]	k4	22
Nástroje	nástroj	k1gInSc2	nástroj
</s>
</p>
<p>
<s>
23	[number]	k4	23
Nářadí	nářadí	k1gNnSc1	nářadí
</s>
</p>
<p>
<s>
24	[number]	k4	24
Upínací	upínací	k2eAgInSc4d1	upínací
nářadí	nářadí	k1gNnSc1	nářadí
</s>
</p>
<p>
<s>
25	[number]	k4	25
Měřicí	měřicí	k2eAgInSc4d1	měřicí
a	a	k8xC	a
kontrolní	kontrolní	k2eAgNnSc4d1	kontrolní
nářadí	nářadí	k1gNnSc4	nářadí
a	a	k8xC	a
přístroje	přístroj	k1gInPc4	přístroj
</s>
</p>
<p>
<s>
26	[number]	k4	26
Zařízení	zařízení	k1gNnSc1	zařízení
dopravní	dopravní	k2eAgFnSc2d1	dopravní
a	a	k8xC	a
pro	pro	k7c4	pro
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
</s>
</p>
<p>
<s>
27	[number]	k4	27
Zdvihací	zdvihací	k2eAgInSc1d1	zdvihací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
pro	pro	k7c4	pro
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
těžbu	těžba	k1gFnSc4	těžba
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
zemní	zemní	k2eAgFnPc4d1	zemní
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnPc4d1	stavební
a	a	k8xC	a
silniční	silniční	k2eAgFnPc4d1	silniční
práce	práce	k1gFnPc4	práce
</s>
</p>
<p>
<s>
28	[number]	k4	28
Kolejová	kolejový	k2eAgFnSc1d1	kolejová
vozidla	vozidlo	k1gNnPc1	vozidlo
</s>
</p>
<p>
<s>
29	[number]	k4	29
Kolejová	kolejový	k2eAgFnSc1d1	kolejová
vozidla	vozidlo	k1gNnPc1	vozidlo
</s>
</p>
<p>
<s>
30	[number]	k4	30
Silniční	silniční	k2eAgInSc1d1	silniční
vozidla	vozidlo	k1gNnPc1	vozidlo
</s>
</p>
<p>
<s>
31	[number]	k4	31
Letectví	letectví	k1gNnSc1	letectví
a	a	k8xC	a
kosmonautika	kosmonautika	k1gFnSc1	kosmonautika
</s>
</p>
<p>
<s>
32	[number]	k4	32
Lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
plovoucí	plovoucí	k2eAgNnSc1d1	plovoucí
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
33	[number]	k4	33
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
-	-	kIx~	-
elektrotechnické	elektrotechnický	k2eAgInPc1d1	elektrotechnický
předpisy	předpis	k1gInPc1	předpis
</s>
</p>
<p>
<s>
34	[number]	k4	34
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
</s>
</p>
<p>
<s>
35	[number]	k4	35
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
</s>
</p>
<p>
<s>
36	[number]	k4	36
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
</s>
</p>
<p>
<s>
37	[number]	k4	37
Elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
-	-	kIx~	-
energetika	energetika	k1gFnSc1	energetika
</s>
</p>
<p>
<s>
38	[number]	k4	38
Energetika	energetika	k1gFnSc1	energetika
-	-	kIx~	-
požární	požární	k2eAgFnSc1d1	požární
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
</s>
</p>
<p>
<s>
39	[number]	k4	39
Zbraně	zbraň	k1gFnPc4	zbraň
pro	pro	k7c4	pro
civilní	civilní	k2eAgFnSc4d1	civilní
potřebu	potřeba	k1gFnSc4	potřeba
</s>
</p>
<p>
<s>
40	[number]	k4	40
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
technika	technika	k1gFnSc1	technika
</s>
</p>
<p>
<s>
41	[number]	k4	41
Hutnictví	hutnictví	k1gNnSc1	hutnictví
-	-	kIx~	-
materiálové	materiálový	k2eAgInPc1d1	materiálový
listy	list	k1gInPc1	list
ocelí	ocelit	k5eAaImIp3nP	ocelit
</s>
</p>
<p>
<s>
42	[number]	k4	42
Hutnictví	hutnictví	k1gNnSc1	hutnictví
</s>
</p>
<p>
<s>
43	[number]	k4	43
Hutnictví	hutnictví	k1gNnSc1	hutnictví
-	-	kIx~	-
strojní	strojní	k2eAgNnSc1d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
44	[number]	k4	44
Hornictví	hornictví	k1gNnSc1	hornictví
</s>
</p>
<p>
<s>
45	[number]	k4	45
Hlubinné	hlubinný	k2eAgNnSc4d1	hlubinné
vrtání	vrtání	k1gNnSc4	vrtání
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
nafty	nafta	k1gFnSc2	nafta
</s>
</p>
<p>
<s>
46	[number]	k4	46
Zemědělství	zemědělství	k1gNnSc1	zemědělství
</s>
</p>
<p>
<s>
47	[number]	k4	47
Zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
a	a	k8xC	a
lesnické	lesnický	k2eAgInPc1d1	lesnický
stroje	stroj	k1gInPc1	stroj
</s>
</p>
<p>
<s>
48	[number]	k4	48
Lesnictví	lesnictví	k1gNnSc1	lesnictví
</s>
</p>
<p>
<s>
49	[number]	k4	49
Průmysl	průmysl	k1gInSc1	průmysl
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
</s>
</p>
<p>
<s>
50	[number]	k4	50
Výrobky	výrobek	k1gInPc1	výrobek
průmyslu	průmysl	k1gInSc2	průmysl
papírenského	papírenský	k2eAgInSc2d1	papírenský
</s>
</p>
<p>
<s>
51	[number]	k4	51
Strojní	strojní	k2eAgInSc4d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
52	[number]	k4	52
Strojní	strojní	k2eAgInSc4d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
53	[number]	k4	53
Strojní	strojní	k2eAgInSc4d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
56	[number]	k4	56
Výrobky	výrobek	k1gInPc4	výrobek
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
57	[number]	k4	57
Výrobky	výrobek	k1gInPc4	výrobek
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
58	[number]	k4	58
Výrobky	výrobek	k1gInPc4	výrobek
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
62	[number]	k4	62
Průmysl	průmysl	k1gInSc1	průmysl
gumárenský	gumárenský	k2eAgInSc1d1	gumárenský
<g/>
,	,	kIx,	,
pryž	pryž	k1gFnSc1	pryž
</s>
</p>
<p>
<s>
63	[number]	k4	63
Průmysl	průmysl	k1gInSc1	průmysl
gumárenský	gumárenský	k2eAgInSc1d1	gumárenský
<g/>
,	,	kIx,	,
pryžové	pryžový	k2eAgInPc1d1	pryžový
výrobky	výrobek	k1gInPc1	výrobek
</s>
</p>
<p>
<s>
64	[number]	k4	64
Plasty	plast	k1gInPc7	plast
</s>
</p>
<p>
<s>
65	[number]	k4	65
Výrobky	výrobek	k1gInPc4	výrobek
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
66	[number]	k4	66
Výrobky	výrobek	k1gInPc4	výrobek
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
67	[number]	k4	67
Výrobky	výrobek	k1gInPc4	výrobek
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
68	[number]	k4	68
Výrobky	výrobek	k1gInPc4	výrobek
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
69	[number]	k4	69
Strojní	strojní	k2eAgInSc4d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
70	[number]	k4	70
Výrobky	výrobek	k1gInPc7	výrobek
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
tavených	tavený	k2eAgFnPc2d1	tavená
hornin	hornina	k1gFnPc2	hornina
</s>
</p>
<p>
<s>
71	[number]	k4	71
Sklo	sklo	k1gNnSc1	sklo
a	a	k8xC	a
tavené	tavený	k2eAgFnPc1d1	tavená
horniny	hornina	k1gFnPc1	hornina
-	-	kIx~	-
materiálové	materiálový	k2eAgInPc1d1	materiálový
listy	list	k1gInPc1	list
a	a	k8xC	a
výrobní	výrobní	k2eAgNnPc1d1	výrobní
zařízení	zařízení	k1gNnPc1	zařízení
</s>
</p>
<p>
<s>
72	[number]	k4	72
Stavební	stavební	k2eAgFnPc4d1	stavební
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
výrobky	výrobek	k1gInPc4	výrobek
</s>
</p>
<p>
<s>
73	[number]	k4	73
Navrhování	navrhování	k1gNnSc1	navrhování
a	a	k8xC	a
provádění	provádění	k1gNnSc1	provádění
staveb	stavba	k1gFnPc2	stavba
</s>
</p>
<p>
<s>
74	[number]	k4	74
Části	část	k1gFnSc2	část
staveb	stavba	k1gFnPc2	stavba
</s>
</p>
<p>
<s>
75	[number]	k4	75
Vodní	vodní	k2eAgInSc4d1	vodní
hospodářství	hospodářství	k1gNnSc1	hospodářství
</s>
</p>
<p>
<s>
76	[number]	k4	76
Služby	služba	k1gFnPc1	služba
</s>
</p>
<p>
<s>
77	[number]	k4	77
Obaly	obal	k1gInPc1	obal
a	a	k8xC	a
obalová	obalový	k2eAgFnSc1d1	obalová
technika	technika	k1gFnSc1	technika
</s>
</p>
<p>
<s>
79	[number]	k4	79
Průmysl	průmysl	k1gInSc1	průmysl
kožedělný	kožedělný	k2eAgInSc1d1	kožedělný
</s>
</p>
<p>
<s>
80	[number]	k4	80
Textilní	textilní	k2eAgFnPc4d1	textilní
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
výrobky	výrobek	k1gInPc4	výrobek
</s>
</p>
<p>
<s>
81	[number]	k4	81
Strojní	strojní	k2eAgInSc4d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
82	[number]	k4	82
Stroje	stroj	k1gInSc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
povrchu	povrch	k1gInSc2	povrch
</s>
</p>
<p>
<s>
83	[number]	k4	83
Ochrana	ochrana	k1gFnSc1	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnSc1d1	pracovní
a	a	k8xC	a
osobní	osobní	k2eAgFnSc1d1	osobní
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
strojních	strojní	k2eAgNnPc2d1	strojní
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
ergonomie	ergonomie	k1gFnSc2	ergonomie
</s>
</p>
<p>
<s>
84	[number]	k4	84
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
</s>
</p>
<p>
<s>
85	[number]	k4	85
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
</s>
</p>
<p>
<s>
86	[number]	k4	86
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
</s>
</p>
<p>
<s>
87	[number]	k4	87
Telekomunikace	telekomunikace	k1gFnSc1	telekomunikace
</s>
</p>
<p>
<s>
88	[number]	k4	88
Průmysl	průmysl	k1gInSc1	průmysl
polygrafický	polygrafický	k2eAgInSc1d1	polygrafický
</s>
</p>
<p>
<s>
89	[number]	k4	89
Hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
</s>
</p>
<p>
<s>
90	[number]	k4	90
Kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
<g/>
,	,	kIx,	,
školní	školní	k2eAgFnPc4d1	školní
a	a	k8xC	a
kreslicí	kreslicí	k2eAgFnPc4d1	kreslicí
potřeby	potřeba	k1gFnPc4	potřeba
</s>
</p>
<p>
<s>
91	[number]	k4	91
Vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
93	[number]	k4	93
Výstrojné	výstrojný	k2eAgInPc1d1	výstrojný
zboží	zboží	k1gNnSc2	zboží
</s>
</p>
<p>
<s>
94	[number]	k4	94
Výstrojné	výstrojný	k2eAgFnPc4d1	výstrojný
zboží	zboží	k1gNnSc2	zboží
</s>
</p>
<p>
<s>
95	[number]	k4	95
Výstrojné	výstrojný	k2eAgInPc1d1	výstrojný
zboží	zboží	k1gNnSc2	zboží
</s>
</p>
<p>
<s>
96	[number]	k4	96
Výstrojné	výstrojný	k2eAgInPc1d1	výstrojný
zboží	zboží	k1gNnSc2	zboží
</s>
</p>
<p>
<s>
97	[number]	k4	97
Výměna	výměna	k1gFnSc1	výměna
dat	datum	k1gNnPc2	datum
</s>
</p>
<p>
<s>
98	[number]	k4	98
Zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
informatika	informatika	k1gFnSc1	informatika
</s>
</p>
<p>
<s>
99	[number]	k4	99
Metrologie	metrologie	k1gFnSc1	metrologie
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ČSN	ČSN	kA	ČSN
ve	v	k7c4	v
WikislovníkuÚřad	WikislovníkuÚřad	k1gInSc4	WikislovníkuÚřad
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
metrologii	metrologie	k1gFnSc4	metrologie
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
zkušebnictví	zkušebnictví	k1gNnSc4	zkušebnictví
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
84	[number]	k4	84
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
o	o	k7c6	o
závaznosti	závaznost	k1gFnSc6	závaznost
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
na	na	k7c6	na
ZákonyProLidi	ZákonyProLid	k1gMnPc1	ZákonyProLid
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
částky	částka	k1gFnSc2	částka
na	na	k7c6	na
Portálu	portál	k1gInSc6	portál
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
výpis	výpis	k1gInSc4	výpis
částek	částka	k1gFnPc2	částka
Sbírky	sbírka	k1gFnSc2	sbírka
zákonů	zákon	k1gInPc2	zákon
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
na	na	k7c6	na
MVČR	MVČR	kA	MVČR
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
