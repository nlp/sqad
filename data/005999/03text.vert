<s>
Bišónen	Bišónen	k1gInSc1	Bišónen
(	(	kIx(	(
<g/>
美	美	k?	美
<g/>
,	,	kIx,	,
Hepburnovým	Hepburnův	k2eAgInSc7d1	Hepburnův
přepisem	přepis	k1gInSc7	přepis
bishō	bishō	k?	bishō
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
"	"	kIx"	"
<g/>
krásný	krásný	k2eAgMnSc1d1	krásný
mladý	mladý	k2eAgMnSc1d1	mladý
hoch	hoch	k1gMnSc1	hoch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
idealizovaný	idealizovaný	k2eAgInSc4d1	idealizovaný
typ	typ	k1gInSc4	typ
krásného	krásný	k2eAgMnSc2d1	krásný
mladého	mladý	k2eAgMnSc2d1	mladý
muže	muž	k1gMnSc2	muž
ve	v	k7c6	v
východoasijské	východoasijský	k2eAgFnSc6d1	východoasijská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
především	především	k9	především
v	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
mangy	mango	k1gNnPc7	mango
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Ženským	ženský	k2eAgInSc7d1	ženský
protějškem	protějšek	k1gInSc7	protějšek
bišónena	bišóneno	k1gNnSc2	bišóneno
je	být	k5eAaImIp3nS	být
bišódžo	bišódžo	k6eAd1	bišódžo
(	(	kIx(	(
<g/>
美	美	k?	美
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
