<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
seriálu	seriál	k1gInSc2	seriál
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Bigger	Bigger	k1gMnSc1	Bigger
<g/>
,	,	kIx,	,
Longer	Longer	k1gMnSc1	Longer
&	&	k?	&
Uncut	Uncut	k1gInSc1	Uncut
(	(	kIx(	(
<g/>
přesný	přesný	k2eAgInSc1d1	přesný
překlad	překlad	k1gInSc1	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
Delší	dlouhý	k2eAgNnSc1d2	delší
&	&	k?	&
Nesestříhaný	sestříhaný	k2eNgMnSc1d1	nesestříhaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
uveden	uvést	k5eAaPmNgMnS	uvést
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
South	South	k1gMnSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Peklo	peklo	k1gNnSc1	peklo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
