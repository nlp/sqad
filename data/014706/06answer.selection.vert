<s>
Burdž	k1gMnSc1	k6eAd1
Chalífa	chalífa	k1gMnSc1
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
ب	ب	k?
خ	خ	k?
–	–	k?
„	„	k?
<g/>
Chalífova	chalífův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
anglicky	anglicky	k6eAd1
Burj	Burj	k1gFnSc1
Khalifa	Khalif	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
mrakodrap	mrakodrap	k1gInSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
2004	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
v	v	k7c6
městě	město	k1gNnSc6
Dubaj	Dubaj	k1gFnSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
<g/>
.	.	kIx.
</s>