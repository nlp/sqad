<s>
Burdž	Burdž	k6eAd1
Chalífa	chalífa	k1gMnSc1
</s>
<s>
Burdž	Burdž	k1gFnSc1
Chalífaب	Chalífaب	k1gFnSc2
خ	خ	k?
Účel	účel	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
Kanceláře	kancelář	k1gFnPc1
<g/>
,	,	kIx,
hotel	hotel	k1gInSc1
<g/>
,	,	kIx,
vyhlídka	vyhlídka	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
high-tech	high-t	k1gInPc6
architektura	architektura	k1gFnSc1
a	a	k8xC
novofuturismus	novofuturismus	k1gInSc4
Architekti	architekt	k1gMnPc1
</s>
<s>
Skidmore	Skidmor	k1gMnSc5
<g/>
,	,	kIx,
Owings	Owings	k1gInSc1
and	and	k?
Merrill	Merrill	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
Materiály	materiál	k1gInPc4
</s>
<s>
železobeton	železobeton	k1gInSc1
<g/>
,	,	kIx,
ocel	ocel	k1gFnSc1
<g/>
,	,	kIx,
hliník	hliník	k1gInSc1
a	a	k8xC
sklo	sklo	k1gNnSc1
Stavitel	stavitel	k1gMnSc1
</s>
<s>
Samsung	Samsung	kA
C	C	kA
<g/>
&	&	k?
<g/>
T	T	kA
Corporation	Corporation	k1gInSc4
<g/>
,	,	kIx,
Arabtec	Arabtec	k1gInSc4
a	a	k8xC
Besix	Besix	k1gInSc4
Pojmenováno	pojmenován	k2eAgNnSc1d1
po	po	k7c6
</s>
<s>
Chalífa	chalífa	k1gMnSc1
bin	bin	k?
Saíd	Saíd	k1gMnSc1
Ál	Ál	k1gMnSc1
Nahján	Nahján	k1gMnSc1
Technické	technický	k2eAgInPc4d1
parametry	parametr	k1gInPc4
Nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
</s>
<s>
829,8	829,8	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výška	výška	k1gFnSc1
střechy	střecha	k1gFnSc2
</s>
<s>
828	#num#	k4
m	m	kA
Počet	počet	k1gInSc1
podlaží	podlaží	k1gNnSc2
</s>
<s>
189	#num#	k4
(	(	kIx(
<g/>
163	#num#	k4
<g/>
)	)	kIx)
Podlahová	podlahový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
464	#num#	k4
511	#num#	k4
m	m	kA
<g/>
2	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
1	#num#	k4
Sheikh	Sheikh	k1gMnSc1
Mohammed	Mohammed	k1gMnSc1
bin	bin	k?
Rashid	Rashid	k1gInSc1
Boulevard	Boulevard	k1gInSc1
<g/>
,	,	kIx,
Dubaj	Dubaj	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
25	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
49,7	49,7	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
55	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
26,8	26,8	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
www.burjkhalifa.ae	www.burjkhalifa.ae	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Burdž	k1gMnSc1	k6eAd1
Chalífa	chalífa	k1gMnSc1
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
ب	ب	k?
خ	خ	k?
–	–	k?
„	„	k?
<g/>
Chalífova	chalífův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
anglicky	anglicky	k6eAd1
Burj	Burj	k1gFnSc1
Khalifa	Khalif	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
mrakodrap	mrakodrap	k1gInSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
2004	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
v	v	k7c6
městě	město	k1gNnSc6
Dubaj	Dubaj	k1gFnSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
byl	být	k5eAaImAgInS
znám	znám	k2eAgInSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
Burdž	Burdž	k1gFnSc1
Dubaj	Dubaj	k1gMnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
ب	ب	k?
د	د	k?
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Burj	Burj	k1gFnSc1
Dubai	Duba	k1gFnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
„	„	k?
<g/>
Dubajská	Dubajský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
krátce	krátce	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
zprovozněním	zprovoznění	k1gNnSc7
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
dubajský	dubajský	k2eAgMnSc1d1
emír	emír	k1gMnSc1
Muhammad	Muhammad	k1gInSc1
bin	bin	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Rášid	Rášid	k1gInSc1
Ál	Ál	k1gFnSc2
Maktúm	Maktúma	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
stavba	stavba	k1gFnSc1
bude	být	k5eAaImBp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
prezidentovi	prezident	k1gMnSc6
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
<g/>
,	,	kIx,
vládci	vládce	k1gMnPc1
sousedního	sousední	k2eAgInSc2d1
emirátu	emirát	k1gInSc2
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
plné	plný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
zní	znět	k5eAaImIp3nS
Chalífa	chalífa	k1gMnSc1
bin	bin	k?
Saíd	Saíd	k1gMnSc1
Ál	Ál	k1gMnSc1
Nahján	Nahján	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Celkové	celkový	k2eAgInPc1d1
náklady	náklad	k1gInPc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
činily	činit	k5eAaImAgInP
1,5	1,5	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
výstavby	výstavba	k1gFnSc2
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
této	tento	k3xDgFnSc2
jedinečné	jedinečný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
byl	být	k5eAaImAgMnS
Adrian	Adrian	k1gMnSc1
Smith	Smith	k1gMnSc1
z	z	k7c2
renomované	renomovaný	k2eAgFnSc2d1
chicagské	chicagský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Skidmore	Skidmor	k1gInSc5
<g/>
,	,	kIx,
Owings	Owings	k1gInSc1
and	and	k?
Merrill	Merrill	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
návrhu	návrh	k1gInSc2
téže	týž	k3xTgFnSc2,k3xDgFnSc2
společnosti	společnost	k1gFnSc2
pro	pro	k7c4
přístavní	přístavní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
v	v	k7c6
australském	australský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Melbourne	Melbourne	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrakodrap	mrakodrap	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Grollo	Grollo	k1gNnSc4
Tower	Towra	k1gFnPc2
však	však	k9
nebyl	být	k5eNaImAgInS
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
postaven	postaven	k2eAgMnSc1d1
a	a	k8xC
projektová	projektový	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
byla	být	k5eAaImAgFnS
přepracována	přepracovat	k5eAaPmNgFnS
pro	pro	k7c4
Dubaj	Dubaj	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Developerem	developer	k1gMnSc7
celého	celý	k2eAgInSc2d1
projektu	projekt	k1gInSc2
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Emaar	Emaar	k1gMnSc1
Properties	Properties	k1gMnSc1
z	z	k7c2
emirátu	emirát	k1gInSc2
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
získal	získat	k5eAaPmAgInS
emirát	emirát	k1gInSc1
Dubai	Dubae	k1gFnSc4
32	#num#	k4
procentní	procentní	k2eAgInSc4d1
akciový	akciový	k2eAgInSc4d1
podíl	podíl	k1gInSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
pozemky	pozemek	k1gInPc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
28	#num#	k4
miliard	miliarda	k4xCgFnPc2
dirhamů	dirham	k1gInPc2
(	(	kIx(
<g/>
AED	AED	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stavbě	stavba	k1gFnSc6
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgInP
americká	americký	k2eAgFnSc1d1
firma	firma	k1gFnSc1
CBM	CBM	kA
Engineers	Engineersa	k1gFnPc2
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
a	a	k8xC
dále	daleko	k6eAd2
mj.	mj.	kA
stavební	stavební	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Samsung	Samsung	kA
Constructions	Constructions	k1gInSc1
<g/>
,	,	kIx,
BESIX	BESIX	kA
a	a	k8xC
Arabtec	Arabtec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
OTIS	OTIS	kA
pro	pro	k7c4
budovu	budova	k1gFnSc4
dodala	dodat	k5eAaPmAgFnS
54	#num#	k4
výtahů	výtah	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
dodávkách	dodávka	k1gFnPc6
výtahů	výtah	k1gInPc2
spolupracovala	spolupracovat	k5eAaImAgFnS
také	také	k9
pardubická	pardubický	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Pega	Pega	k1gFnSc1
Hoist	Hoist	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Samotnou	samotný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
provádělo	provádět	k5eAaImAgNnS
celkově	celkově	k6eAd1
asi	asi	k9
10	#num#	k4
000	#num#	k4
pracovníků	pracovník	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
byli	být	k5eAaImAgMnP
převážně	převážně	k6eAd1
dělníci	dělník	k1gMnPc1
z	z	k7c2
Pákistánu	Pákistán	k1gInSc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
Bangladéše	Bangladéš	k1gInSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
a	a	k8xC
Filipín	Filipíny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1
se	se	k3xPyFc4
stavěl	stavět	k5eAaImAgInS
od	od	k7c2
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2004	#num#	k4
a	a	k8xC
ještě	ještě	k6eAd1
před	před	k7c7
dokončením	dokončení	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejvyšší	vysoký	k2eAgFnSc7d3
stavbou	stavba	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
dosažena	dosáhnout	k5eAaPmNgFnS
konečná	konečný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
828	#num#	k4
metrů	metr	k1gInPc2
včetně	včetně	k7c2
antény	anténa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc4d1
otevření	otevření	k1gNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výtahy	výtah	k1gInPc7
vedou	vést	k5eAaImIp3nP
až	až	k9
do	do	k7c2
189	#num#	k4
<g/>
.	.	kIx.
patra	patro	k1gNnSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
638	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
běžné	běžný	k2eAgNnSc4d1
užití	užití	k1gNnSc4
slouží	sloužit	k5eAaImIp3nS
163	#num#	k4
pater	patro	k1gNnPc2
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlídková	vyhlídkový	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
zvaná	zvaný	k2eAgFnSc1d1
At	At	k1gFnSc1
the	the	k?
Top	topit	k5eAaImRp2nS
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
124	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
a	a	k8xC
naskýtá	naskýtat	k5eAaImIp3nS
pohled	pohled	k1gInSc4
na	na	k7c4
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
města	město	k1gNnSc2
Dubaje	Dubaj	k1gInSc2
s	s	k7c7
jeho	jeho	k3xOp3gInPc7
dalšími	další	k2eAgInPc7d1
mrakodrapy	mrakodrap	k1gInPc7
a	a	k8xC
dálnicemi	dálnice	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgFnSc1d1
podlahová	podlahový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
464	#num#	k4
511	#num#	k4
metrů	metr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celém	celý	k2eAgInSc6d1
mrakodrapu	mrakodrap	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
57	#num#	k4
výtahů	výtah	k1gInPc2
a	a	k8xC
8	#num#	k4
eskalátorů	eskalátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
výstavby	výstavba	k1gFnSc2
nebyla	být	k5eNaImAgFnS
konečná	konečný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
mrakodrapu	mrakodrap	k1gInSc2
dlouho	dlouho	k6eAd1
přesně	přesně	k6eAd1
známa	znám	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
byla	být	k5eAaImAgFnS
přísně	přísně	k6eAd1
tajena	tajen	k2eAgFnSc1d1
kvůli	kvůli	k7c3
konkurenci	konkurence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
předběžných	předběžný	k2eAgInPc2d1
odhadů	odhad	k1gInPc2
se	se	k3xPyFc4
předpokládala	předpokládat	k5eAaImAgFnS
výška	výška	k1gFnSc1
asi	asi	k9
808	#num#	k4
metrů	metr	k1gInPc2
se	s	k7c7
162	#num#	k4
podlažími	podlaží	k1gNnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
samotná	samotný	k2eAgFnSc1d1
stavební	stavební	k2eAgFnSc1d1
firma	firma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Firma	firma	k1gFnSc1
OTIS	OTIS	kA
uvádí	uvádět	k5eAaImIp3nS
výšku	výška	k1gFnSc4
stavby	stavba	k1gFnSc2
800	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Projekty	projekt	k1gInPc1
na	na	k7c6
oficiální	oficiální	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
budovy	budova	k1gFnSc2
naznačovaly	naznačovat	k5eAaImAgFnP
až	až	k9
195	#num#	k4
podlaží	podlaží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
se	se	k3xPyFc4
spekulovalo	spekulovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
výška	výška	k1gFnSc1
mrakodrapu	mrakodrap	k1gInSc2
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
i	i	k9
více	hodně	k6eAd2
než	než	k8xS
940	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
až	až	k9
1	#num#	k4
011	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
rozsáhlého	rozsáhlý	k2eAgInSc2d1
projektu	projekt	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
Downtown	Downtown	k1gNnSc1
Dubai	Duba	k1gFnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
20	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
zahrnujícího	zahrnující	k2eAgInSc2d1
i	i	k8xC
několik	několik	k4yIc1
dalších	další	k2eAgInPc2d1
mrakodrapů	mrakodrap	k1gInPc2
<g/>
,	,	kIx,
obrovské	obrovský	k2eAgNnSc1d1
nákupní	nákupní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Dubai	Duba	k1gFnSc2
Mall	Malla	k1gFnPc2
a	a	k8xC
umělé	umělý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
s	s	k7c7
vodotrysky	vodotrysk	k1gInPc7
uprostřed	uprostřed	k7c2
komplexu	komplex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rekordy	rekord	k1gInPc1
</s>
<s>
Burdž	Burdž	k6eAd1
Chalífa	chalífa	k1gMnSc1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
nejvyššími	vysoký	k2eAgFnPc7d3
stavbami	stavba	k1gFnPc7
světa	svět	k1gInSc2
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2007	#num#	k4
předstihl	předstihnout	k5eAaPmAgInS
skelet	skelet	k1gInSc4
budovy	budova	k1gFnSc2
mrakodrap	mrakodrap	k1gInSc1
Willis	Willis	k1gFnSc4
Tower	Towero	k1gNnPc2
i	i	k9
tehdy	tehdy	k6eAd1
teprve	teprve	k6eAd1
rozestavěný	rozestavěný	k2eAgMnSc1d1
One	One	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Center	centrum	k1gNnPc2
jako	jako	k9
budovu	budova	k1gFnSc4
s	s	k7c7
největším	veliký	k2eAgInSc7d3
počtem	počet	k1gInSc7
podlaží	podlaží	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2008	#num#	k4
developerská	developerský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
EMAAR	EMAAR	kA
Properties	Properties	k1gInSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
je	být	k5eAaImIp3nS
se	s	k7c7
160	#num#	k4
kompletními	kompletní	k2eAgNnPc7d1
podlažími	podlaží	k1gNnPc7
a	a	k8xC
výškou	výška	k1gFnSc7
629	#num#	k4
metru	metr	k1gInSc2
již	již	k9
nejvyšší	vysoký	k2eAgFnSc7d3
stavbou	stavba	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dostavění	dostavění	k1gNnSc6
drží	držet	k5eAaImIp3nS
Burdž	Burdž	k1gFnSc4
Chalífa	chalífa	k1gMnSc1
kolem	kolem	k7c2
15	#num#	k4
prestižních	prestižní	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
(	(	kIx(
<g/>
mj.	mj.	kA
nejvyšší	vysoký	k2eAgFnSc1d3
nekotvená	kotvený	k2eNgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
obytná	obytný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
kategorie	kategorie	k1gFnPc4
nejvyšší	vysoký	k2eAgFnPc4d3
kdy	kdy	k6eAd1
postavené	postavený	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
světa	svět	k1gInSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
stožárů	stožár	k1gInPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
mrakodrap	mrakodrap	k1gInSc1
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
podstatně	podstatně	k6eAd1
překonal	překonat	k5eAaPmAgMnS
již	již	k6eAd1
neexistující	existující	k2eNgInSc4d1
televizní	televizní	k2eAgInSc4d1
a	a	k8xC
rozhlasový	rozhlasový	k2eAgInSc4d1
Vysílač	vysílač	k1gInSc4
Konstantynow	Konstantynow	k1gFnSc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
výšku	výška	k1gFnSc4
646,38	646,38	k4
m	m	kA
<g/>
,	,	kIx,
avšak	avšak	k8xC
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1991	#num#	k4
se	se	k3xPyFc4
zhroutil	zhroutit	k5eAaPmAgInS
a	a	k8xC
již	již	k6eAd1
nebyl	být	k5eNaImAgInS
obnoven	obnovit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
je	být	k5eAaImIp3nS
Burdž	Burdž	k1gFnSc4
Chalífa	chalífa	k1gMnSc1
budovou	budova	k1gFnSc7
s	s	k7c7
největším	veliký	k2eAgInSc7d3
počtem	počet	k1gInSc7
pater	patro	k1gNnPc2
<g/>
,	,	kIx,
nejmohutnější	mohutný	k2eAgFnSc7d3
výtahovou	výtahový	k2eAgFnSc7d1
instalací	instalace	k1gFnSc7
s	s	k7c7
výtahy	výtah	k1gInPc7
jezdícími	jezdící	k2eAgInPc7d1
maximální	maximální	k2eAgInSc4d1
rychlostí	rychlost	k1gFnSc7
10	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
tj.	tj.	kA
36	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
–	–	k?
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
moderními	moderní	k2eAgMnPc7d1
mrakodrapy	mrakodrap	k1gInPc4
průměrná	průměrný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc7d3
betonovou	betonový	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
či	či	k8xC
stavbou	stavba	k1gFnSc7
s	s	k7c7
nejvýše	vysoce	k6eAd3,k6eAd1
položenou	položený	k2eAgFnSc7d1
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1
plošinou	plošina	k1gFnSc7
(	(	kIx(
<g/>
ve	v	k7c4
124	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
nejvýše	vysoce	k6eAd3,k6eAd1
položenou	položený	k2eAgFnSc7d1
mešitou	mešita	k1gFnSc7
(	(	kIx(
<g/>
ve	v	k7c4
158	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
instalací	instalace	k1gFnSc7
hliníkové	hliníkový	k2eAgFnSc2d1
nebo	nebo	k8xC
skleněné	skleněný	k2eAgFnSc2d1
fasády	fasáda	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
výšky	výška	k1gFnSc2
512	#num#	k4
m	m	kA
<g/>
)	)	kIx)
či	či	k8xC
nejvýše	nejvýše	k6eAd1,k6eAd3
položeným	položený	k2eAgInSc7d1
bazénem	bazén	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
76	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
19	#num#	k4
<g/>
]	]	kIx)
Kuriózním	kuriózní	k2eAgInSc7d1
rekordem	rekord	k1gInSc7
je	být	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgInSc1d3
dosud	dosud	k6eAd1
učiněný	učiněný	k2eAgInSc1d1
BASE	basa	k1gFnSc3
jump	jump	k1gInSc1
(	(	kIx(
<g/>
skok	skok	k1gInSc1
z	z	k7c2
výšky	výška	k1gFnSc2
s	s	k7c7
padákem	padák	k1gInSc7
–	–	k?
jeden	jeden	k4xCgInSc4
z	z	k7c2
adrenalinových	adrenalinový	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
)	)	kIx)
z	z	k7c2
nejvyššího	vysoký	k2eAgInSc2d3
bodu	bod	k1gInSc2
Burdž	Burdž	k1gFnSc1
Chalífy	chalífa	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
též	též	k9
první	první	k4xOgFnSc1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zobrazena	zobrazit	k5eAaPmNgFnS
v	v	k7c6
Google	Googl	k1gInSc6
Maps	Mapsa	k1gFnPc2
(	(	kIx(
<g/>
Street	Street	k1gMnSc1
View	View	k1gMnSc1
<g/>
)	)	kIx)
i	i	k8xC
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
chodeb	chodba	k1gFnPc2
<g/>
,	,	kIx,
výhledu	výhled	k1gInSc2
z	z	k7c2
vybraných	vybraný	k2eAgNnPc2d1
pater	patro	k1gNnPc2
a	a	k8xC
veřejných	veřejný	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
uvnitř	uvnitř	k7c2
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
a	a	k8xC
design	design	k1gInSc1
budovy	budova	k1gFnSc2
</s>
<s>
Developerem	developer	k1gMnSc7
celého	celý	k2eAgInSc2d1
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
arabská	arabský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Emaar	Emaar	k1gMnSc1
Properties	Properties	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budovu	budova	k1gFnSc4
projektovalo	projektovat	k5eAaBmAgNnS
architektonické	architektonický	k2eAgNnSc1d1
studio	studio	k1gNnSc1
Skidmore	Skidmor	k1gInSc5
<g/>
,	,	kIx,
Owings	Owings	k1gInSc1
and	and	k?
Merrill	Merrill	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
spolupracovalo	spolupracovat	k5eAaImAgNnS
i	i	k9
na	na	k7c6
projektech	projekt	k1gInPc6
Willis	Willis	k1gInSc4
Tower	Towra	k1gFnPc2
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
One	One	k1gMnSc1
World	World	k1gMnSc1
Trade	Trad	k1gInSc5
Center	centrum	k1gNnPc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Ťin	Ťin	k1gMnSc1
Mao	Mao	k1gMnSc1
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
a	a	k8xC
mnoha	mnoho	k4c2
dalších	další	k2eAgFnPc2d1
výškových	výškový	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
byl	být	k5eAaImAgMnS
Adrian	Adrian	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architekti	architekt	k1gMnPc1
se	se	k3xPyFc4
inspirovali	inspirovat	k5eAaBmAgMnP
návrhy	návrh	k1gInPc4
Franka	Frank	k1gMnSc2
Lloyda	Lloyd	k1gMnSc2
Wrighta	Wright	k1gMnSc2
i	i	k8xC
islámskou	islámský	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strukturálním	strukturální	k2eAgMnSc7d1
inženýrem	inženýr	k1gMnSc7
stavby	stavba	k1gFnSc2
byl	být	k5eAaImAgMnS
William	William	k1gInSc4
F.	F.	kA
Baker	Baker	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Členitý	členitý	k2eAgInSc1d1
půdorys	půdorys	k1gInSc1
budovy	budova	k1gFnSc2
má	mít	k5eAaImIp3nS
připomínat	připomínat	k5eAaImF
rozvinutý	rozvinutý	k2eAgInSc1d1
květ	květ	k1gInSc1
květiny	květina	k1gFnSc2
hymenocallis	hymenocallis	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
Dubaji	Dubaj	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
světa	svět	k1gInSc2
tak	tak	k6eAd1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
základu	základ	k1gInSc6
tzv.	tzv.	kA
semene	semeno	k1gNnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
geometrický	geometrický	k2eAgInSc1d1
útvar	útvar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
lze	lze	k6eAd1
běžně	běžně	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Základová	základový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
má	mít	k5eAaImIp3nS
plochu	plocha	k1gFnSc4
7000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
850	#num#	k4
pilířů	pilíř	k1gInPc2
<g/>
,	,	kIx,
širokých	široký	k2eAgFnPc2d1
až	až	k9
1,5	1,5	k4
m	m	kA
a	a	k8xC
hlubokých	hluboký	k2eAgFnPc2d1
až	až	k9
50	#num#	k4
m	m	kA
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
bylo	být	k5eAaImAgNnS
na	na	k7c4
základy	základ	k1gInPc4
spotřebováno	spotřebován	k2eAgNnSc4d1
45	#num#	k4
000	#num#	k4
m³	m³	k?
betonu	beton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
kostry	kostra	k1gFnSc2
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
betonu	beton	k1gInSc2
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgFnSc1d1
čtvrtina	čtvrtina	k1gFnSc1
je	být	k5eAaImIp3nS
ocelová	ocelový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnější	vnější	k2eAgInPc1d1
obklady	obklad	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
vyztuženého	vyztužený	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
,	,	kIx,
speciálně	speciálně	k6eAd1
vybraného	vybraný	k2eAgInSc2d1
do	do	k7c2
extrémních	extrémní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
(	(	kIx(
<g/>
vysoké	vysoký	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
<g/>
,	,	kIx,
písečné	písečný	k2eAgFnPc1d1
bouře	bouř	k1gFnPc1
<g/>
,	,	kIx,
silný	silný	k2eAgInSc1d1
vítr	vítr	k1gInSc1
ve	v	k7c6
vrchní	vrchní	k2eAgFnSc6d1
části	část	k1gFnSc6
budovy	budova	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
návrhu	návrh	k1gInSc6
interiéru	interiér	k1gInSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
módní	módní	k2eAgMnSc1d1
návrhář	návrhář	k1gMnSc1
Giorgio	Giorgio	k1gMnSc1
Armani	Arman	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Panorama	panorama	k1gNnSc1
města	město	k1gNnSc2
Dubaje	Dubaj	k1gInSc2
s	s	k7c7
mrakodrapem	mrakodrap	k1gInSc7
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
</s>
<s>
Nasvícení	nasvícení	k1gNnSc1
budovy	budova	k1gFnSc2
</s>
<s>
Burdž	Burdž	k6eAd1
Chalífa	chalífa	k1gMnSc1
</s>
<s>
Budova	budova	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
příležitostně	příležitostně	k6eAd1
speciálně	speciálně	k6eAd1
nasvícena	nasvícen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
takto	takto	k6eAd1
vyjádřují	vyjádřovat	k5eAaImIp3nP
gratulaci	gratulace	k1gFnSc4
či	či	k8xC
solidaritu	solidarita	k1gFnSc4
některým	některý	k3yIgFnPc3
zemím	zem	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k6eAd1
např.	např.	kA
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
budovu	budova	k1gFnSc4
překryla	překrýt	k5eAaPmAgFnS
projekce	projekce	k1gFnSc1
britské	britský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
na	na	k7c4
památku	památka	k1gFnSc4
obětí	oběť	k1gFnPc2
teroristického	teroristický	k2eAgInSc2d1
útoku	útok	k1gInSc2
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
barvami	barva	k1gFnPc7
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
si	se	k3xPyFc3
připomínalo	připomínat	k5eAaImAgNnS
potvrzení	potvrzení	k1gNnSc1
své	svůj	k3xOyFgFnSc2
neutrality	neutralita	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
nasvícena	nasvítit	k5eAaPmNgFnS
českou	český	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
vzniku	vznik	k1gInSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Využití	využití	k1gNnSc1
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
smíšené	smíšený	k2eAgNnSc1d1
<g/>
.	.	kIx.
37	#num#	k4
pater	patro	k1gNnPc2
mrakodrapu	mrakodrap	k1gInSc2
zabírá	zabírat	k5eAaImIp3nS
Armani	Armaň	k1gFnSc3
Hotel	hotel	k1gInSc4
<g/>
,	,	kIx,
první	první	k4xOgInSc4
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
63	#num#	k4
patrech	patro	k1gNnPc6
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
700	#num#	k4
luxusních	luxusní	k2eAgInPc2d1
bytů	byt	k1gInPc2
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
budovy	budova	k1gFnSc2
patří	patřit	k5eAaImIp3nS
kancelářím	kancelář	k1gFnPc3
a	a	k8xC
technologickým	technologický	k2eAgInPc3d1
prostorům	prostor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlídková	vyhlídkový	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
zvaná	zvaný	k2eAgFnSc1d1
At	At	k1gFnSc1
the	the	k?
Top	topit	k5eAaImRp2nS
je	on	k3xPp3gInPc4
na	na	k7c4
124	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc4
budovy	budova	k1gFnSc2
slouží	sloužit	k5eAaImIp3nS
telekomunikacím	telekomunikace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
78	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
je	být	k5eAaImIp3nS
položen	položen	k2eAgInSc1d1
plavecký	plavecký	k2eAgInSc1d1
bazén	bazén	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
stavebních	stavební	k2eAgFnPc2d1
firem	firma	k1gFnPc2
od	od	k7c2
dělníků	dělník	k1gMnPc2
</s>
<s>
Na	na	k7c6
výstavbě	výstavba	k1gFnSc6
mrakodrapu	mrakodrap	k1gInSc2
se	se	k3xPyFc4
podílelo	podílet	k5eAaImAgNnS
zhruba	zhruba	k6eAd1
10	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
z	z	k7c2
mnoha	mnoho	k4c2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavební	stavební	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
však	však	k9
pocházeli	pocházet	k5eAaImAgMnP
převážně	převážně	k6eAd1
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Ti	ten	k3xDgMnPc1
si	se	k3xPyFc3
opakovaně	opakovaně	k6eAd1
stěžovali	stěžovat	k5eAaImAgMnP
na	na	k7c4
nízké	nízký	k2eAgFnPc4d1
mzdy	mzda	k1gFnPc4
a	a	k8xC
špatné	špatný	k2eAgFnPc4d1
pracovní	pracovní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výroků	výrok	k1gInPc2
dotázaných	dotázaný	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
uvedených	uvedený	k2eAgFnPc2d1
anglickým	anglický	k2eAgInSc7d1
deníkem	deník	k1gInSc7
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
v	v	k7c6
březnu	březen	k1gInSc6
2006	#num#	k4
si	se	k3xPyFc3
tehdy	tehdy	k6eAd1
zkušený	zkušený	k2eAgMnSc1d1
tesař	tesař	k1gMnSc1
vydělal	vydělat	k5eAaPmAgMnS
4,34	4,34	k4
britských	britský	k2eAgFnPc2d1
liber	libra	k1gFnPc2
za	za	k7c4
den	den	k1gInSc4
a	a	k8xC
nekvalifikovaný	kvalifikovaný	k2eNgMnSc1d1
dělník	dělník	k1gMnSc1
2,84	2,84	k4
britských	britský	k2eAgFnPc2d1
liber	libra	k1gFnPc2
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Časté	častý	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
také	také	k9
případy	případ	k1gInPc4
pozdní	pozdní	k2eAgFnSc2d1
výplaty	výplata	k1gFnSc2
mezd	mzda	k1gFnPc2
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
zaměstnavatelé	zaměstnavatel	k1gMnPc1
také	také	k9
zadržovali	zadržovat	k5eAaImAgMnP
pasy	pas	k1gInPc4
svých	svůj	k3xOyFgMnPc2
zaměstnanců	zaměstnanec	k1gMnPc2
až	až	k9
do	do	k7c2
skončení	skončení	k1gNnSc2
zaměstnání	zaměstnání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Odbory	odbor	k1gInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
arabských	arabský	k2eAgInPc6d1
emirátech	emirát	k1gInPc6
zatím	zatím	k6eAd1
zakázané	zakázaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
vypukly	vypuknout	k5eAaPmAgFnP
na	na	k7c6
stavbě	stavba	k1gFnSc6
nepokoje	nepokoj	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
nepřijely	přijet	k5eNaPmAgInP
autobusy	autobus	k1gInPc1
odvážející	odvážející	k2eAgInPc1d1
dělníky	dělník	k1gMnPc4
na	na	k7c4
ubytovny	ubytovna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojení	spokojený	k2eNgMnPc1d1
dělníci	dělník	k1gMnPc1
ničili	ničit	k5eAaImAgMnP
auta	auto	k1gNnPc4
<g/>
,	,	kIx,
vybavení	vybavení	k1gNnSc4
kanceláří	kancelář	k1gFnPc2
i	i	k8xC
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
úředníků	úředník	k1gMnPc2
dubajského	dubajský	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
způsobili	způsobit	k5eAaPmAgMnP
škody	škoda	k1gFnPc4
za	za	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
US-	US-	k1gFnPc2
<g/>
$	$	kIx~
(	(	kIx(
<g/>
asi	asi	k9
20	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
den	den	k1gInSc1
většina	většina	k1gFnSc1
dělníků	dělník	k1gMnPc2
odmítla	odmítnout	k5eAaPmAgFnS
nastoupit	nastoupit	k5eAaPmF
do	do	k7c2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
stávce	stávka	k1gFnSc3
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
i	i	k9
dělníci	dělník	k1gMnPc1
pracující	pracující	k2eAgMnPc1d1
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
Mezinárodního	mezinárodní	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
Dubaj	Dubaj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyjednávání	vyjednávání	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
dni	den	k1gInSc6
dělníci	dělník	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2006	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2006	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2007	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
snímek	snímek	k1gInSc1
staveniště	staveniště	k1gNnSc2
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
fontánu	fontána	k1gFnSc4
The	The	k1gMnSc4
Dubai	Dubai	k1gNnSc7
Fountain	Fountain	k1gMnSc1
z	z	k7c2
vyhlídkové	vyhlídkový	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
ve	v	k7c6
124	#num#	k4
<g/>
.	.	kIx.
patře	patro	k1gNnSc6
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
vyhlídkové	vyhlídkový	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
na	na	k7c4
městské	městský	k2eAgFnPc4d1
dálnice	dálnice	k1gFnPc4
</s>
<s>
Nepřehlédnutelný	přehlédnutelný	k2eNgInSc1d1
Burdž	Burdž	k1gFnSc4
Chalífa	chalífa	k1gMnSc1
</s>
<s>
Dubaj	Dubaj	k1gFnSc1
při	při	k7c6
západu	západ	k1gInSc6
slunce	slunce	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Burj	Burj	k1gInSc1
Khalifa	Khalif	k1gMnSc2
<g/>
,	,	kIx,
ctbuh	ctbuh	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
↑	↑	k?
Nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
světa	svět	k1gInSc2
-	-	kIx~
orchidej	orchidej	k1gFnSc1
sahá	sahat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
nebes	nebesa	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ct	Ct	k1gFnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2010-01-04	2010-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
STANGLIN	STANGLIN	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gInSc1
<g/>
:	:	kIx,
Dubai	Dubai	k1gNnSc1
opens	opensa	k1gFnPc2
world	worlda	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tallest	tallest	k1gInSc1
building	building	k1gInSc1
(	(	kIx(
<g/>
Dubaj	Dubaj	k1gInSc1
otvírá	otvírat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
budovu	budova	k1gFnSc4
světa	svět	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
Today	Todaa	k1gFnPc1
<g/>
,	,	kIx,
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
světa	svět	k1gInSc2
slaví	slavit	k5eAaImIp3nS
10	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
vlastně	vlastně	k9
stavěla	stavět	k5eAaImAgFnS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoom	Zoom	k1gMnSc1
magazin	magazin	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chalífův	chalífův	k2eAgInSc4d1
hrad	hrad	k1gInSc4
stojí	stát	k5eAaImIp3nS
už	už	k6eAd1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořadí	pořadí	k1gNnSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
komu	kdo	k3yQnSc3,k3yInSc3,k3yRnSc3
bude	být	k5eAaImBp3nS
patřit	patřit	k5eAaImF
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-04	2020-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Burj	Burj	k1gInSc1
Khalifa	Khalif	k1gMnSc2
<g/>
.	.	kIx.
www.cad.cz	www.cad.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ROBINSON	Robinson	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
:	:	kIx,
Grollo	Grollo	k1gNnSc1
tower	towra	k1gFnPc2
to	ten	k3xDgNnSc1
go	go	k?
ahead	ahead	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
Dubai	Duba	k1gFnSc2
(	(	kIx(
<g/>
Věž	věžit	k5eAaImRp2nS
Grollo	Grollo	k1gNnSc4
bude	být	k5eAaImBp3nS
postavena	postaven	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
Dubaji	Dubaj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Age	Age	k1gFnSc1
<g/>
,	,	kIx,
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
staženo	stažen	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.1	.1	k4
2	#num#	k4
Projektová	projektový	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
firmy	firma	k1gFnSc2
OTIS	OTIS	kA
<g/>
,	,	kIx,
http://www.otis.com/corp/c1-projects.html	http://www.otis.com/corp/c1-projects.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
světa	svět	k1gInSc2
v	v	k7c6
Dubaji	Dubaj	k1gFnSc6
vyrůstá	vyrůstat	k5eAaImIp3nS
pomocí	pomocí	k7c2
českých	český	k2eAgInPc2d1
výtahů	výtah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavební	stavební	k2eAgInSc1d1
fórum	fórum	k1gNnSc4
<g/>
,	,	kIx,
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Jak	jak	k6eAd1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
svět	svět	k1gInSc4
ze	z	k7c2
148	#num#	k4
<g/>
.	.	kIx.
patra	patro	k1gNnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Dubajský	Dubajský	k2eAgInSc1d1
mrakodrap	mrakodrap	k1gInSc1
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
nabízí	nabízet	k5eAaImIp3nS
luxus	luxus	k1gInSc4
v	v	k7c6
oblacích	oblak	k1gInPc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-10	2018-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.doka.com/doka/en_global/references/constructionmethods/climbingsystems/selfclimbing/pages/03274/index.php	http://www.doka.com/doka/en_global/references/constructionmethods/climbingsystems/selfclimbing/pages/03274/index.php	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
13	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
www.doka.com	www.doka.com	k1gInSc4
The	The	k1gFnSc1
Formwork	Formwork	k1gInSc1
Experts	Experts	k1gInSc1
<g/>
↑	↑	k?
http://www.dubaimegaprojects.com/	http://www.dubaimegaprojects.com/	k?
www.dubaimegaprojects.com	www.dubaimegaprojects.com	k1gInSc1
<g/>
↑	↑	k?
Fact	Fact	k2eAgInSc1d1
Sheet	Sheet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
At	At	k1gFnSc1
the	the	k?
top	topit	k5eAaImRp2nS
<g/>
;	;	kIx,
Burj	Burj	k1gMnSc1
Khalifa	Khalif	k1gMnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.burjkhalifa.ae	www.burjkhalifa.ae	k6eAd1
<g/>
↑	↑	k?
Burj	Burj	k1gInSc4
Khalifa	Khalif	k1gMnSc2
<g/>
,	,	kIx,
Dubai	Duba	k1gFnSc2
|	|	kIx~
EMPORIS	EMPORIS	kA
<g/>
↑	↑	k?
10	#num#	k4
nejrychlejších	rychlý	k2eAgInPc2d3
výtahů	výtah	k1gInPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VÝTAHY	výtah	k1gInPc4
SERVER	server	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-04	2020-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jak	jak	k8xS,k8xC
rychlé	rychlý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
výtahy	výtah	k1gInPc1
v	v	k7c6
mrakodrapech	mrakodrap	k1gInPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nejrychlejší	rychlý	k2eAgInSc4d3
jezdí	jezdit	k5eAaImIp3nS
60	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-07	2014-11-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.putzmeister.de/cps/rde/xchg/SID-3C6E00FC-8269E805/pm_online/hs.xsl/5933_ENU_HTML.htm	http://www.putzmeister.de/cps/rde/xchg/SID-3C6E00FC-8269E805/pm_online/hs.xsl/5933_ENU_HTML.htm	k1gInSc1
<g/>
↑	↑	k?
www.tallest-building-in-the-world.com	www.tallest-building-in-the-world.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
22	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ndtv.com/news/world/dubais_skyscraper_has_worlds_highest_mosque.php	http://www.ndtv.com/news/world/dubais_skyscraper_has_worlds_highest_mosque.php	k1gMnSc1
<g/>
↑	↑	k?
http://news.bbc.co.uk/2/hi/8448411.stm	http://news.bbc.co.uk/2/hi/8448411.stm	k1gMnSc1
<g/>
↑	↑	k?
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
ve	v	k7c4
Street	Street	k1gInSc4
View	View	k1gFnSc2
<g/>
↑	↑	k?
OBARHUA	OBARHUA	kA
<g/>
,	,	kIx,
Emmanuel	Emmanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dubai	Dubai	k1gNnSc1
Tour	Toura	k1gFnPc2
Pro	pro	k7c4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-10-29	2018-10-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejvyšší	vysoký	k2eAgInSc1d3
mrakodrap	mrakodrap	k1gInSc1
světa	svět	k1gInSc2
v	v	k7c6
Dubaji	Dubaj	k1gFnSc6
rozsvítila	rozsvítit	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
trikolóra	trikolóra	k1gFnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
WHITAKER	WHITAKER	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
:	:	kIx,
Riot	Riot	k1gMnSc1
by	by	kYmCp3nS
migrant	migrant	k1gMnSc1
workers	workers	k6eAd1
halts	halts	k6eAd1
construction	construction	k1gInSc1
of	of	k?
Dubai	Dubai	k1gNnSc1
skyscraper	skyscrapero	k1gNnPc2
(	(	kIx(
<g/>
Pozdvižení	pozdvižení	k1gNnPc2
přistěhovalých	přistěhovalý	k2eAgMnPc2d1
dělníků	dělník	k1gMnPc2
pozdrželo	pozdržet	k5eAaPmAgNnS
stavbu	stavba	k1gFnSc4
mrakodrapu	mrakodrap	k1gInSc2
v	v	k7c6
Dubaji	Dubaj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
staženo	stažen	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Laborers	Laborers	k1gInSc1
riot	riot	k1gMnSc1
over	overa	k1gFnPc2
low	low	k?
Dubai	Duba	k1gMnPc1
wages	wages	k1gMnSc1
<g/>
,	,	kIx,
MSNBC	MSNBC	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
staveb	stavba	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Burdž	Burdž	k1gFnSc1
Chalífa	chalífa	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Novinky	novinka	k1gFnPc1
ze	z	k7c2
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
fotografií	fotografia	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Burj	Burj	k1gMnSc1
Dubaj	Dubaj	k1gMnSc1
na	na	k7c6
Emporis	Emporis	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Článek	článek	k1gInSc1
na	na	k7c6
arciweb	arciwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Novinky	novinka	k1gFnPc1
<g/>
,	,	kIx,
aktuality	aktualita	k1gFnPc1
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
fotografií	fotografia	k1gFnPc2
<g/>
,	,	kIx,
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
videa	video	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1110912366	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2010000040	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
1461154983529267860004	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2010000040	#num#	k4
</s>
