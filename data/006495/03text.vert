<s>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1820	[number]	k4	1820
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
klasický	klasický	k2eAgMnSc1d1	klasický
britský	britský	k2eAgMnSc1d1	britský
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zastánce	zastánce	k1gMnSc4	zastánce
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
organicistického	organicistický	k2eAgInSc2d1	organicistický
proudu	proud	k1gInSc2	proud
<g/>
"	"	kIx"	"
v	v	k7c6	v
sociologii	sociologie	k1gFnSc6	sociologie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
podobnost	podobnost	k1gFnSc4	podobnost
rysů	rys	k1gInPc2	rys
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
biologického	biologický	k2eAgInSc2d1	biologický
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
myšlenek	myšlenka	k1gFnPc2	myšlenka
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
,	,	kIx,	,
jen	jen	k9	jen
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
však	však	k9	však
dnes	dnes	k6eAd1	dnes
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
původním	původní	k2eAgMnSc7d1	původní
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1820	[number]	k4	1820
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
městě	město	k1gNnSc6	město
Derby	derby	k1gNnPc2	derby
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
však	však	k9	však
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
nikdy	nikdy	k6eAd1	nikdy
nenavštěvoval	navštěvovat	k5eNaImAgInS	navštěvovat
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
zprvu	zprvu	k6eAd1	zprvu
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
strýc	strýc	k1gMnSc1	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
Spencer	Spencer	k1gMnSc1	Spencer
nikdy	nikdy	k6eAd1	nikdy
nenavštěvoval	navštěvovat	k5eNaImAgMnS	navštěvovat
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
typickým	typický	k2eAgMnSc7d1	typický
samoukem	samouk	k1gMnSc7	samouk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zástupcem	zástupce	k1gMnSc7	zástupce
vydavatele	vydavatel	k1gMnSc2	vydavatel
časopisu	časopis	k1gInSc2	časopis
Economist	Economist	k1gMnSc1	Economist
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
osobnostmi	osobnost	k1gFnPc7	osobnost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
britské	britský	k2eAgFnSc2d1	britská
vědecké	vědecký	k2eAgFnSc2d1	vědecká
elity	elita	k1gFnSc2	elita
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
zde	zde	k6eAd1	zde
též	též	k6eAd1	též
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Social	Social	k1gInSc1	Social
Statics	Statics	k1gInSc1	Statics
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Spencerův	Spencerův	k2eAgMnSc1d1	Spencerův
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
svému	svůj	k3xOyFgMnSc3	svůj
synovci	synovec	k1gMnSc3	synovec
značné	značný	k2eAgNnSc1d1	značné
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zanechat	zanechat	k5eAaPmF	zanechat
publicistické	publicistický	k2eAgFnPc4d1	publicistická
činnosti	činnost	k1gFnPc4	činnost
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
soukromým	soukromý	k2eAgMnSc7d1	soukromý
učencem	učenec	k1gMnSc7	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
Spencerova	Spencerův	k2eAgFnSc1d1	Spencerova
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Principles	Principles	k1gMnSc1	Principles
of	of	k?	of
Psychology	psycholog	k1gMnPc7	psycholog
<g/>
"	"	kIx"	"
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k8xC	tak
vřele	vřele	k6eAd1	vřele
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
prvotina	prvotina	k1gFnSc1	prvotina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
Spencer	Spencer	k1gInSc1	Spencer
zahájil	zahájit	k5eAaPmAgInS	zahájit
snahu	snaha	k1gFnSc4	snaha
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
univerzálního	univerzální	k2eAgInSc2d1	univerzální
filozofického	filozofický	k2eAgInSc2d1	filozofický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
schopného	schopný	k2eAgMnSc2d1	schopný
zajistit	zajistit	k5eAaPmF	zajistit
společný	společný	k2eAgInSc4d1	společný
teoretický	teoretický	k2eAgInSc4d1	teoretický
základ	základ	k1gInSc4	základ
lidského	lidský	k2eAgNnSc2d1	lidské
vědění	vědění	k1gNnSc2	vědění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
publikoval	publikovat	k5eAaBmAgMnS	publikovat
řadu	řada	k1gFnSc4	řada
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
pokrýt	pokrýt	k5eAaPmF	pokrýt
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
poznání	poznání	k1gNnSc2	poznání
<g/>
:	:	kIx,	:
First	First	k1gFnSc1	First
Principles	Principles	k1gMnSc1	Principles
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Biology	biolog	k1gMnPc7	biolog
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Sociology	sociolog	k1gMnPc7	sociolog
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Ethics	Ethics	k1gInSc1	Ethics
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Jako	jako	k8xS	jako
vědec	vědec	k1gMnSc1	vědec
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
Spencer	Spencer	k1gMnSc1	Spencer
uznáván	uznáván	k2eAgMnSc1d1	uznáván
až	až	k6eAd1	až
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestože	přestože	k8xS	přestože
jeho	on	k3xPp3gInSc4	on
věhlas	věhlas	k1gInSc4	věhlas
nabyl	nabýt	k5eAaPmAgMnS	nabýt
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedoceněn	docenit	k5eNaPmNgInS	docenit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gInSc1	Spencer
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
úvahách	úvaha	k1gFnPc6	úvaha
navazoval	navazovat	k5eAaImAgInS	navazovat
jen	jen	k6eAd1	jen
na	na	k7c4	na
nemnoho	mnoho	k6eNd1	mnoho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patřil	patřit	k5eAaImAgInS	patřit
evolucionista	evolucionista	k1gMnSc1	evolucionista
a	a	k8xC	a
Spencerův	Spencerův	k2eAgMnSc1d1	Spencerův
přítel	přítel	k1gMnSc1	přítel
Thomas	Thomas	k1gMnSc1	Thomas
Huxley	Huxlea	k1gMnSc2	Huxlea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
posléze	posléze	k6eAd1	posléze
Spencer	Spencer	k1gMnSc1	Spencer
navázal	navázat	k5eAaPmAgMnS	navázat
dlouholeté	dlouholetý	k2eAgInPc4d1	dlouholetý
korespondenční	korespondenční	k2eAgInPc4d1	korespondenční
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gMnSc1	Spencer
vědecké	vědecký	k2eAgFnSc2d1	vědecká
knihy	kniha	k1gFnSc2	kniha
takřka	takřka	k6eAd1	takřka
vůbec	vůbec	k9	vůbec
nečetl	číst	k5eNaImAgInS	číst
a	a	k8xC	a
vědomosti	vědomost	k1gFnPc4	vědomost
získával	získávat	k5eAaImAgMnS	získávat
především	především	k9	především
z	z	k7c2	z
periodik	periodikum	k1gNnPc2	periodikum
<g/>
,	,	kIx,	,
diskuzí	diskuze	k1gFnPc2	diskuze
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
a	a	k8xC	a
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
asistentů	asistent	k1gMnPc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInPc4	jeho
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
kořeny	kořen	k1gInPc4	kořen
třeba	třeba	k6eAd1	třeba
hledat	hledat	k5eAaImF	hledat
zejména	zejména	k9	zejména
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
intelektuálním	intelektuální	k2eAgNnSc6d1	intelektuální
prostředí	prostředí	k1gNnSc6	prostředí
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
přírodovědeckých	přírodovědecký	k2eAgInPc6d1	přírodovědecký
kruzích	kruh	k1gInPc6	kruh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
ekonomii	ekonomie	k1gFnSc6	ekonomie
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
v	v	k7c6	v
sociologických	sociologický	k2eAgFnPc6d1	sociologická
koncepcích	koncepce	k1gFnPc6	koncepce
Augusta	August	k1gMnSc2	August
Comta	Comt	k1gMnSc2	Comt
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc4d1	celé
Spencerovo	Spencerův	k2eAgNnSc4d1	Spencerův
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
filozofické	filozofický	k2eAgNnSc4d1	filozofické
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
užíval	užívat	k5eAaImAgMnS	užívat
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
syntetická	syntetický	k2eAgFnSc1d1	syntetická
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
propojeno	propojen	k2eAgNnSc1d1	propojeno
nemnoha	nemnoh	k1gMnSc4	nemnoh
jednotícími	jednotící	k2eAgNnPc7d1	jednotící
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jedním	jeden	k4xCgInSc7	jeden
je	být	k5eAaImIp3nS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
evolucionismu	evolucionismus	k1gInSc2	evolucionismus
<g/>
.	.	kIx.	.
</s>
<s>
Evoluci	evoluce	k1gFnSc4	evoluce
Spencer	Spencra	k1gFnPc2	Spencra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
First	First	k1gMnSc1	First
Principles	Principles	k1gMnSc1	Principles
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
změnu	změna	k1gFnSc4	změna
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
relativně	relativně	k6eAd1	relativně
nevymezeného	vymezený	k2eNgInSc2d1	nevymezený
<g/>
,	,	kIx,	,
nekoherentního	koherentní	k2eNgInSc2d1	nekoherentní
<g/>
,	,	kIx,	,
homogenního	homogenní	k2eAgInSc2d1	homogenní
ke	k	k7c3	k
stavu	stav	k1gInSc2	stav
relativně	relativně	k6eAd1	relativně
vymezenému	vymezený	k2eAgMnSc3d1	vymezený
<g/>
,	,	kIx,	,
koherentnímu	koherentní	k2eAgMnSc3d1	koherentní
a	a	k8xC	a
heterogennímu	heterogenní	k2eAgMnSc3d1	heterogenní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Keller	Keller	k1gMnSc1	Keller
2005	[number]	k4	2005
:	:	kIx,	:
146	[number]	k4	146
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
regrese	regrese	k1gFnSc1	regrese
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
komplexních	komplexní	k2eAgFnPc2d1	komplexní
a	a	k8xC	a
diferencovaných	diferencovaný	k2eAgFnPc2d1	diferencovaná
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vysoce	vysoce	k6eAd1	vysoce
abstraktní	abstraktní	k2eAgInSc4d1	abstraktní
model	model	k1gInSc4	model
evoluce	evoluce	k1gFnSc2	evoluce
používá	používat	k5eAaImIp3nS	používat
Spencer	Spencer	k1gInSc4	Spencer
i	i	k9	i
pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
vývoje	vývoj	k1gInSc2	vývoj
společnosti	společnost	k1gFnSc2	společnost
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
The	The	k1gFnSc2	The
Study	stud	k1gInPc4	stud
of	of	k?	of
Sociology	sociolog	k1gMnPc4	sociolog
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
zde	zde	k6eAd1	zde
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
sociální	sociální	k2eAgFnSc1d1	sociální
podléhá	podléhat	k5eAaImIp3nS	podléhat
podobným	podobný	k2eAgInPc3d1	podobný
zákonům	zákon	k1gInPc3	zákon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pole	pole	k1gNnSc1	pole
biologické	biologický	k2eAgFnSc2d1	biologická
či	či	k8xC	či
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
nárůst	nárůst	k1gInSc1	nárůst
velikosti	velikost	k1gFnSc2	velikost
subsystémů	subsystém	k1gInPc2	subsystém
k	k	k7c3	k
tlaku	tlak	k1gInSc3	tlak
na	na	k7c4	na
diferenciaci	diferenciace	k1gFnSc4	diferenciace
jejich	jejich	k3xOp3gFnPc2	jejich
funkcí	funkce	k1gFnPc2	funkce
<g/>
;	;	kIx,	;
funkčně	funkčně	k6eAd1	funkčně
diferenciované	diferenciovaný	k2eAgInPc1d1	diferenciovaný
subsystémy	subsystém	k1gInPc1	subsystém
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zpětně	zpětně	k6eAd1	zpětně
s	s	k7c7	s
celkem	celkem	k6eAd1	celkem
propojeny	propojit	k5eAaPmNgFnP	propojit
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
nenahraditelnému	nahraditelný	k2eNgInSc3d1	nenahraditelný
významu	význam	k1gInSc3	význam
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
přežití	přežití	k1gNnSc4	přežití
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zpětná	zpětný	k2eAgFnSc1d1	zpětná
integrace	integrace	k1gFnSc1	integrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
společnosti	společnost	k1gFnPc1	společnost
procházejí	procházet	k5eAaImIp3nP	procházet
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
vývojem	vývoj	k1gInSc7	vývoj
od	od	k7c2	od
homogenních	homogenní	k2eAgInPc2d1	homogenní
a	a	k8xC	a
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
útvarů	útvar	k1gInPc2	útvar
k	k	k7c3	k
vysoce	vysoce	k6eAd1	vysoce
komplexním	komplexní	k2eAgFnPc3d1	komplexní
a	a	k8xC	a
diferencovaným	diferencovaný	k2eAgFnPc3d1	diferencovaná
strukturám	struktura	k1gFnPc3	struktura
-	-	kIx~	-
zkoumání	zkoumání	k1gNnSc3	zkoumání
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
směr	směr	k1gInSc4	směr
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dle	dle	k7c2	dle
Spencera	Spencero	k1gNnSc2	Spencero
skutečným	skutečný	k2eAgInSc7d1	skutečný
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
úkolem	úkol	k1gInSc7	úkol
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
Comtův	Comtův	k2eAgInSc1d1	Comtův
sociologický	sociologický	k2eAgInSc1d1	sociologický
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
a	a	k8xC	a
zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
odkrytí	odkrytí	k1gNnSc1	odkrytí
faktorů	faktor	k1gInPc2	faktor
ovlivňujících	ovlivňující	k2eAgInPc2d1	ovlivňující
evoluci	evoluce	k1gFnSc4	evoluce
společnosti	společnost	k1gFnSc2	společnost
nás	my	k3xPp1nPc4	my
neopravňuje	opravňovat	k5eNaImIp3nS	opravňovat
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
modifikaci	modifikace	k1gFnSc3	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
Spencer	Spencer	k1gInSc1	Spencer
sociologickým	sociologický	k2eAgMnSc7d1	sociologický
realistou	realista	k1gMnSc7	realista
(	(	kIx(	(
<g/>
považuje	považovat	k5eAaImIp3nS	považovat
společnost	společnost	k1gFnSc1	společnost
za	za	k7c4	za
skutečnost	skutečnost	k1gFnSc4	skutečnost
nadosobní	nadosobní	k2eAgFnSc1d1	nadosobní
a	a	k8xC	a
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
členech	člen	k1gInPc6	člen
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
určitý	určitý	k2eAgInSc4d1	určitý
individualismus	individualismus	k1gInSc4	individualismus
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
sloužící	sloužící	k1gFnSc2	sloužící
svým	svůj	k3xOyFgMnPc3	svůj
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
členům	člen	k1gMnPc3	člen
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
řadu	řada	k1gFnSc4	řada
analogií	analogie	k1gFnSc7	analogie
mezi	mezi	k7c7	mezi
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
organismem	organismus	k1gInSc7	organismus
<g/>
:	:	kIx,	:
růst	růst	k1gInSc1	růst
co	co	k3yQnSc1	co
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
i	i	k9	i
co	co	k3yInSc4	co
do	do	k7c2	do
komplexity	komplexita	k1gFnSc2	komplexita
diferenciace	diferenciace	k1gFnSc2	diferenciace
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
funkcí	funkce	k1gFnPc2	funkce
zpětná	zpětný	k2eAgFnSc1d1	zpětná
integrace	integrace	k1gFnSc1	integrace
Poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
však	však	k9	však
i	i	k9	i
na	na	k7c4	na
nezanedbatelné	zanedbatelný	k2eNgFnPc4d1	nezanedbatelná
rozpory	rozpora	k1gFnPc4	rozpora
mezi	mezi	k7c7	mezi
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
organismem	organismus	k1gInSc7	organismus
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc1d2	veliký
volnost	volnost	k1gFnSc1	volnost
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
organismu	organismus	k1gInSc2	organismus
jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgFnPc1d1	určitá
části	část	k1gFnPc1	část
specializovány	specializován	k2eAgFnPc1d1	specializována
k	k	k7c3	k
myšlení	myšlení	k1gNnSc3	myšlení
a	a	k8xC	a
cítění	cítění	k1gNnSc4	cítění
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
tohoto	tento	k3xDgNnSc2	tento
schopny	schopen	k2eAgFnPc1d1	schopna
všechny	všechen	k3xTgFnPc4	všechen
její	její	k3xOp3gFnPc4	její
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
existují	existovat	k5eAaImIp3nP	existovat
všechny	všechen	k3xTgFnPc1	všechen
jednotky	jednotka	k1gFnPc1	jednotka
jen	jen	k9	jen
pro	pro	k7c4	pro
blaho	blaho	k1gNnSc4	blaho
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
naopak	naopak	k6eAd1	naopak
Podle	podle	k7c2	podle
Spencera	Spencer	k1gMnSc2	Spencer
plní	plnit	k5eAaImIp3nP	plnit
sociální	sociální	k2eAgFnPc1d1	sociální
instituce	instituce	k1gFnPc1	instituce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
podobnou	podobný	k2eAgFnSc4d1	podobná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
orgány	orgán	k1gInPc4	orgán
v	v	k7c6	v
živém	živý	k2eAgInSc6d1	živý
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
pozornost	pozornost	k1gFnSc4	pozornost
např.	např.	kA	např.
institucím	instituce	k1gFnPc3	instituce
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
obřadním	obřadní	k2eAgFnPc3d1	obřadní
<g/>
,	,	kIx,	,
politickým	politický	k2eAgFnPc3d1	politická
<g/>
,	,	kIx,	,
církevním	církevní	k2eAgFnPc3d1	církevní
<g/>
,	,	kIx,	,
profesním	profesní	k2eAgFnPc3d1	profesní
či	či	k8xC	či
průmyslovým	průmyslový	k2eAgFnPc3d1	průmyslová
<g/>
.	.	kIx.	.
</s>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
dvě	dva	k4xCgFnPc4	dva
dosti	dosti	k6eAd1	dosti
odlišná	odlišný	k2eAgNnPc4d1	odlišné
pojetí	pojetí	k1gNnPc4	pojetí
společnosti	společnost	k1gFnSc2	společnost
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc7	první
typologií	typologie	k1gFnSc7	typologie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozlišování	rozlišování	k1gNnSc1	rozlišování
společností	společnost	k1gFnPc2	společnost
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc4d1	složené
<g/>
,	,	kIx,	,
dvojitě	dvojitě	k6eAd1	dvojitě
složené	složený	k2eAgInPc1d1	složený
a	a	k8xC	a
trojitě	trojitě	k6eAd1	trojitě
složené	složený	k2eAgInPc1d1	složený
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nárůstu	nárůst	k1gInSc2	nárůst
komplexity	komplexita	k1gFnSc2	komplexita
a	a	k8xC	a
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
typologií	typologie	k1gFnSc7	typologie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
označit	označit	k5eAaPmF	označit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dichotomie	dichotomie	k1gFnSc1	dichotomie
společností	společnost	k1gFnPc2	společnost
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
(	(	kIx(	(
<g/>
industriálních	industriální	k2eAgFnPc2d1	industriální
<g/>
,	,	kIx,	,
organických	organický	k2eAgFnPc2d1	organická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
způsobu	způsob	k1gInSc2	způsob
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
aplikován	aplikován	k2eAgInSc1d1	aplikován
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gMnSc1	Spencer
se	se	k3xPyFc4	se
obsáhle	obsáhle	k6eAd1	obsáhle
zabývá	zabývat	k5eAaImIp3nS	zabývat
tematikou	tematika	k1gFnSc7	tematika
lidského	lidský	k2eAgNnSc2d1	lidské
individua	individuum	k1gNnSc2	individuum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jedině	jedině	k6eAd1	jedině
skrze	skrze	k?	skrze
jeho	jeho	k3xOp3gFnSc4	jeho
povahu	povaha	k1gFnSc4	povaha
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dopracovat	dopracovat	k5eAaPmF	dopracovat
k	k	k7c3	k
pravdivé	pravdivý	k2eAgFnSc3d1	pravdivá
teorii	teorie	k1gFnSc3	teorie
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
Spencerově	Spencerův	k2eAgNnSc6d1	Spencerův
pojetí	pojetí	k1gNnSc6	pojetí
člověka	člověk	k1gMnSc2	člověk
sehrál	sehrát	k5eAaPmAgMnS	sehrát
jeho	jeho	k3xOp3gInSc4	jeho
sociální	sociální	k2eAgInSc4d1	sociální
darwinismus	darwinismus	k1gInSc4	darwinismus
a	a	k8xC	a
koncepce	koncepce	k1gFnSc1	koncepce
přežití	přežití	k1gNnSc2	přežití
nejsilnějšího	silný	k2eAgNnSc2d3	nejsilnější
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
převzal	převzít	k5eAaPmAgMnS	převzít
od	od	k7c2	od
Malthuse	Malthuse	k1gFnSc2	Malthuse
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
tříbí	tříbit	k5eAaImIp3nS	tříbit
a	a	k8xC	a
zdokonaluje	zdokonalovat	k5eAaImIp3nS	zdokonalovat
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nevyhnutelný	vyhnutelný	k2eNgInSc1d1	nevyhnutelný
a	a	k8xC	a
Spencer	Spencer	k1gInSc1	Spencer
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stavět	stavět	k5eAaImF	stavět
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
"	"	kIx"	"
moderní	moderní	k2eAgFnSc1d1	moderní
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
rovnostářská	rovnostářský	k2eAgFnSc1d1	rovnostářská
a	a	k8xC	a
mírumilovná	mírumilovný	k2eAgFnSc1d1	mírumilovná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nezakládá	zakládat	k5eNaImIp3nS	zakládat
na	na	k7c6	na
regulačních	regulační	k2eAgInPc6d1	regulační
státních	státní	k2eAgInPc6d1	státní
mechanismech	mechanismus	k1gInPc6	mechanismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
kontraktech	kontrakt	k1gInPc6	kontrakt
mezi	mezi	k7c7	mezi
svobodnými	svobodný	k2eAgNnPc7d1	svobodné
individui	individuum	k1gNnPc7	individuum
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
jejich	jejich	k3xOp3gInPc2	jejich
osobních	osobní	k2eAgInPc2d1	osobní
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
lidstva	lidstvo	k1gNnSc2	lidstvo
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
Spencer	Spencer	k1gInSc1	Spencer
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
individualizace	individualizace	k1gFnSc2	individualizace
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc1	svůj
úvahy	úvaha	k1gFnPc1	úvaha
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
později	pozdě	k6eAd2	pozdě
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
typologií	typologie	k1gFnPc2	typologie
společnosti	společnost	k1gFnSc2	společnost
<g/>
:	:	kIx,	:
vojenská	vojenský	k2eAgFnSc1d1	vojenská
společnost	společnost	k1gFnSc1	společnost
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
povahy	povaha	k1gFnSc2	povaha
přímo	přímo	k6eAd1	přímo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
co	co	k9	co
možná	možná	k9	možná
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
unifikaci	unifikace	k1gFnSc4	unifikace
individuí	individuum	k1gNnPc2	individuum
a	a	k8xC	a
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
odklání	odklánět	k5eAaImIp3nS	odklánět
od	od	k7c2	od
závazné	závazný	k2eAgFnSc2d1	závazná
normy	norma	k1gFnSc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
industriální	industriální	k2eAgFnSc1d1	industriální
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgInSc3	ten
podporuje	podporovat	k5eAaImIp3nS	podporovat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
svobodu	svoboda	k1gFnSc4	svoboda
individua	individuum	k1gNnSc2	individuum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
právních	právní	k2eAgFnPc2d1	právní
mezí	mez	k1gFnPc2	mez
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gMnSc1	Spencer
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
nehodnotící	hodnotící	k2eNgFnSc2d1	nehodnotící
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
odmítal	odmítat	k5eAaImAgMnS	odmítat
odsuzování	odsuzování	k1gNnSc4	odsuzování
určitých	určitý	k2eAgFnPc2d1	určitá
zvyklostí	zvyklost	k1gFnPc2	zvyklost
praktikovaných	praktikovaný	k2eAgFnPc2d1	praktikovaná
v	v	k7c6	v
archaických	archaický	k2eAgFnPc6d1	archaická
společnostech	společnost	k1gFnPc6	společnost
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
naší	náš	k3xOp1gFnSc2	náš
současné	současný	k2eAgFnSc2d1	současná
evropské	evropský	k2eAgFnSc2d1	Evropská
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klíčová	klíčový	k2eAgFnSc1d1	klíčová
je	být	k5eAaImIp3nS	být
funkčnost	funkčnost	k1gFnSc1	funkčnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
společenských	společenský	k2eAgInPc2d1	společenský
jevů	jev	k1gInPc2	jev
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencra	k1gFnPc2	Spencra
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
např.	např.	kA	např.
Comta	Comta	k1gFnSc1	Comta
<g/>
)	)	kIx)	)
také	také	k9	také
zabýval	zabývat	k5eAaImAgInS	zabývat
důsledky	důsledek	k1gInPc7	důsledek
plynoucími	plynoucí	k2eAgInPc7d1	plynoucí
z	z	k7c2	z
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
badatel	badatel	k1gMnSc1	badatel
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
součástí	součást	k1gFnSc7	součást
objektu	objekt	k1gInSc2	objekt
svého	svůj	k3xOyFgInSc2	svůj
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnSc2	jeho
sociologického	sociologický	k2eAgNnSc2d1	sociologické
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
podobným	podobný	k2eAgFnPc3d1	podobná
metodologickým	metodologický	k2eAgFnPc3d1	metodologická
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Spencer	Spencer	k1gMnSc1	Spencer
usiloval	usilovat	k5eAaImAgMnS	usilovat
také	také	k9	také
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
sociologie	sociologie	k1gFnSc1	sociologie
stala	stát	k5eAaPmAgFnS	stát
empirickou	empirický	k2eAgFnSc7d1	empirická
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
začal	začít	k5eAaPmAgInS	začít
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
a	a	k8xC	a
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Descriptive	Descriptiv	k1gInSc5	Descriptiv
Sociology	sociolog	k1gMnPc7	sociolog
publikovat	publikovat	k5eAaBmF	publikovat
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
empirický	empirický	k2eAgInSc4d1	empirický
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
dokumentující	dokumentující	k2eAgInSc4d1	dokumentující
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgFnPc2d1	světová
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
