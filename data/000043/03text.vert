<s>
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gInSc1	máj
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
dělnický	dělnický	k2eAgInSc1d1	dělnický
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
slaví	slavit	k5eAaImIp3nS	slavit
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
zavedla	zavést	k5eAaPmAgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
II	II	kA	II
<g/>
.	.	kIx.	.
internacionála	internacionála	k1gFnSc1	internacionála
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
stávky	stávka	k1gFnSc2	stávka
amerických	americký	k2eAgMnPc2d1	americký
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
Haymarketský	Haymarketský	k2eAgInSc4d1	Haymarketský
masakr	masakr	k1gInSc4	masakr
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
na	na	k7c6	na
Střeleckém	střelecký	k2eAgInSc6d1	střelecký
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
svátek	svátek	k1gInSc1	svátek
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
Labor	Labor	k1gMnSc1	Labor
Day	Day	k1gMnSc1	Day
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nS	slavit
první	první	k4xOgNnSc4	první
pondělí	pondělí	k1gNnSc4	pondělí
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1886	[number]	k4	1886
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
odborů	odbor	k1gInPc2	odbor
a	a	k8xC	a
anarchistů	anarchista	k1gMnPc2	anarchista
celodenní	celodenní	k2eAgFnSc1d1	celodenní
stávka	stávka	k1gFnSc1	stávka
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
anarchistickým	anarchistický	k2eAgInSc7d1	anarchistický
deníkem	deník	k1gInSc7	deník
The	The	k1gFnSc2	The
Alarm	alarm	k1gInSc1	alarm
<g/>
,	,	kIx,	,
usilující	usilující	k2eAgMnSc1d1	usilující
o	o	k7c4	o
osmihodinovou	osmihodinový	k2eAgFnSc4d1	osmihodinová
pracovní	pracovní	k2eAgFnSc4d1	pracovní
dobu	doba	k1gFnSc4	doba
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
stávkovalo	stávkovat	k5eAaImAgNnS	stávkovat
asi	asi	k9	asi
300	[number]	k4	300
000	[number]	k4	000
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
demonstrantů	demonstrant	k1gMnPc2	demonstrant
s	s	k7c7	s
pořádkovými	pořádkový	k2eAgFnPc7d1	pořádková
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
několik	několik	k4yIc1	několik
stávkujících	stávkující	k1gMnPc2	stávkující
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
policejní	policejní	k2eAgFnSc2d1	policejní
palby	palba	k1gFnSc2	palba
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
při	při	k7c6	při
demonstraci	demonstrace	k1gFnSc6	demonstrace
na	na	k7c4	na
Haymarket	Haymarket	k1gInSc4	Haymarket
Square	square	k1gInSc4	square
kvůli	kvůli	k7c3	kvůli
výbuchu	výbuch	k1gInSc3	výbuch
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byli	být	k5eAaImAgMnP	být
obviněni	obvinit	k5eAaPmNgMnP	obvinit
a	a	k8xC	a
následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
důkazů	důkaz	k1gInPc2	důkaz
<g/>
)	)	kIx)	)
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
anarchisté	anarchista	k1gMnPc1	anarchista
August	August	k1gMnSc1	August
Spies	Spies	k1gMnSc1	Spies
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Parsons	Parsons	k1gInSc1	Parsons
<g/>
,	,	kIx,	,
Adolph	Adolph	k1gInSc1	Adolph
Fisher	Fishra	k1gFnPc2	Fishra
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Engel	Engel	k1gMnSc1	Engel
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Lingg	Lingg	k1gMnSc1	Lingg
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Lingg	Lingg	k1gMnSc1	Lingg
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
již	již	k9	již
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
Svátek	svátek	k1gInSc1	svátek
práce	práce	k1gFnSc2	práce
celostátně	celostátně	k6eAd1	celostátně
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
slavil	slavit	k5eAaImAgInS	slavit
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhaly	probíhat	k5eAaImAgFnP	probíhat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
stávky	stávka	k1gFnSc2	stávka
a	a	k8xC	a
demonstrace	demonstrace	k1gFnSc2	demonstrace
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
starých	starý	k2eAgFnPc2d1	stará
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
přijala	přijmout	k5eAaPmAgFnS	přijmout
II	II	kA	II
<g/>
.	.	kIx.	.
internacionála	internacionála	k1gFnSc1	internacionála
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
francouzských	francouzský	k2eAgMnPc2d1	francouzský
socialistů	socialist	k1gMnPc2	socialist
1	[number]	k4	1
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gInSc1	máj
<g/>
)	)	kIx)	)
za	za	k7c4	za
oficiální	oficiální	k2eAgInSc4d1	oficiální
svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
tohoto	tento	k3xDgInSc2	tento
svátku	svátek	k1gInSc2	svátek
přidávaly	přidávat	k5eAaImAgInP	přidávat
další	další	k2eAgInPc1d1	další
politické	politický	k2eAgInPc1d1	politický
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
nacismus	nacismus	k1gInSc1	nacismus
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
trval	trvat	k5eAaImAgMnS	trvat
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
na	na	k7c6	na
zákazu	zákaz	k1gInSc6	zákaz
tohoto	tento	k3xDgInSc2	tento
"	"	kIx"	"
<g/>
marxisticko-anarchistického	marxistickonarchistický	k2eAgInSc2d1	marxisticko-anarchistický
výmyslu	výmysl	k1gInSc2	výmysl
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
sv.	sv.	kA	sv.
Josefu	Josef	k1gMnSc3	Josef
dělníkovi	dělník	k1gMnSc3	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
socialistických	socialistický	k2eAgFnPc6d1	socialistická
zemích	zem	k1gFnPc6	zem
patřil	patřit	k5eAaImAgInS	patřit
tento	tento	k3xDgInSc4	tento
svátek	svátek	k1gInSc4	svátek
mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
<g/>
,	,	kIx,	,
organizovaly	organizovat	k5eAaBmAgInP	organizovat
se	se	k3xPyFc4	se
masové	masový	k2eAgInPc1d1	masový
prvomájové	prvomájový	k2eAgInPc1d1	prvomájový
průvody	průvod	k1gInPc1	průvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
procházely	procházet	k5eAaImAgFnP	procházet
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
před	před	k7c7	před
tribunou	tribuna	k1gFnSc7	tribuna
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
představiteli	představitel	k1gMnPc7	představitel
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
oslavách	oslava	k1gFnPc6	oslava
prvního	první	k4xOgNnSc2	první
máje	máj	k1gInSc2	máj
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
nepsaně	psaně	k6eNd1	psaně
povinná	povinný	k2eAgFnSc1d1	povinná
a	a	k8xC	a
nadřízení	nadřízený	k1gMnPc1	nadřízený
a	a	k8xC	a
učitelé	učitel	k1gMnPc1	učitel
měli	mít	k5eAaImAgMnP	mít
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
účast	účast	k1gFnSc4	účast
svých	svůj	k3xOyFgMnPc2	svůj
podřízených	podřízený	k1gMnPc2	podřízený
a	a	k8xC	a
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
bral	brát	k5eAaImAgInS	brát
jako	jako	k8xC	jako
májová	májový	k2eAgFnSc1d1	Májová
veselice	veselice	k1gFnSc1	veselice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průvodu	průvod	k1gInSc6	průvod
byly	být	k5eAaImAgFnP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
národní	národní	k2eAgFnSc7d1	národní
a	a	k8xC	a
družstevní	družstevní	k2eAgInPc4d1	družstevní
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc4	škola
i	i	k8xC	i
různé	různý	k2eAgFnPc4d1	různá
zájmové	zájmový	k2eAgFnPc4d1	zájmová
organizace	organizace	k1gFnPc4	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
organizace	organizace	k1gFnSc1	organizace
Svazarmu	Svazarm	k1gInSc2	Svazarm
či	či	k8xC	či
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pěších	pěší	k2eAgMnPc2d1	pěší
účastníků	účastník	k1gMnPc2	účastník
městem	město	k1gNnSc7	město
projížděly	projíždět	k5eAaImAgInP	projíždět
i	i	k9	i
alegorické	alegorický	k2eAgInPc1d1	alegorický
vozy	vůz	k1gInPc1	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
průvodů	průvod	k1gInPc2	průvod
byly	být	k5eAaImAgFnP	být
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
transparenty	transparent	k1gInPc4	transparent
a	a	k8xC	a
mávátka	mávátko	k1gNnPc4	mávátko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
prostředí	prostředí	k1gNnSc6	prostředí
nahrával	nahrávat	k5eAaImAgInS	nahrávat
propagaci	propagace	k1gFnSc4	propagace
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
i	i	k8xC	i
ten	ten	k3xDgInSc1	ten
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
5	[number]	k4	5
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
dny	den	k1gInPc1	den
z	z	k7c2	z
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
časově	časově	k6eAd1	časově
blízké	blízký	k2eAgInPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
Užívalo	užívat	k5eAaImAgNnS	užívat
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
propagační	propagační	k2eAgNnSc1d1	propagační
sousloví	sousloví	k1gNnSc1	sousloví
"	"	kIx"	"
<g/>
slavné	slavný	k2eAgInPc1d1	slavný
májové	májový	k2eAgInPc1d1	májový
dny	den	k1gInPc1	den
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
letácích	leták	k1gInPc6	leták
a	a	k8xC	a
plakátech	plakát	k1gInPc6	plakát
bývaly	bývat	k5eAaImAgFnP	bývat
uvedeny	uvést	k5eAaPmNgFnP	uvést
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
vzpomínkové	vzpomínkový	k2eAgFnPc1d1	vzpomínková
akce	akce	k1gFnPc1	akce
na	na	k7c4	na
padlé	padlý	k1gMnPc4	padlý
sovětské	sovětský	k2eAgMnPc4d1	sovětský
a	a	k8xC	a
československé	československý	k2eAgMnPc4d1	československý
vojáky	voják	k1gMnPc4	voják
ve	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
konala	konat	k5eAaImAgFnS	konat
tradiční	tradiční	k2eAgFnSc1d1	tradiční
oslava	oslava	k1gFnSc1	oslava
Svátku	svátek	k1gInSc2	svátek
práce	práce	k1gFnSc2	práce
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
navštěvovaná	navštěvovaný	k2eAgFnSc1d1	navštěvovaná
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
sympatizanty	sympatizant	k1gMnPc7	sympatizant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
konala	konat	k5eAaImAgFnS	konat
jiná	jiný	k2eAgFnSc1d1	jiná
akce	akce	k1gFnSc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
setkání	setkání	k1gNnSc4	setkání
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
si	se	k3xPyFc3	se
předem	předem	k6eAd1	předem
zamluvila	zamluvit	k5eAaPmAgFnS	zamluvit
Konfederace	konfederace	k1gFnSc1	konfederace
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
na	na	k7c6	na
akci	akce	k1gFnSc6	akce
s	s	k7c7	s
názvem	název	k1gInSc7	název
První	první	k4xOgFnSc1	první
máj	máj	k1gFnSc1	máj
bez	bez	k7c2	bez
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
ale	ale	k8xC	ale
oslavila	oslavit	k5eAaPmAgFnS	oslavit
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gFnSc1	máj
u	u	k7c2	u
Křižíkovy	Křižíkův	k2eAgFnSc2d1	Křižíkova
fontány	fontána	k1gFnSc2	fontána
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
pořadatele	pořadatel	k1gMnPc4	pořadatel
prvomájových	prvomájový	k2eAgNnPc2d1	prvomájové
setkání	setkání	k1gNnSc2	setkání
patří	patřit	k5eAaImIp3nS	patřit
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
snažící	snažící	k2eAgFnSc1d1	snažící
se	se	k3xPyFc4	se
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
někdejší	někdejší	k2eAgNnSc4d1	někdejší
svobodné	svobodný	k2eAgNnSc4d1	svobodné
socialistické	socialistický	k2eAgNnSc4d1	socialistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšli	vyjít	k5eAaPmAgMnP	vyjít
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
i	i	k9	i
aktivisté	aktivista	k1gMnPc1	aktivista
Národního	národní	k2eAgInSc2d1	národní
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
anarchistickou	anarchistický	k2eAgFnSc7d1	anarchistická
Antifašistickou	antifašistický	k2eAgFnSc7d1	Antifašistická
akcí	akce	k1gFnSc7	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
na	na	k7c6	na
Střeleckém	střelecký	k2eAgInSc6d1	střelecký
ostrově	ostrov	k1gInSc6	ostrov
sešlo	sejít	k5eAaPmAgNnS	sejít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
anarchistů	anarchista	k1gMnPc2	anarchista
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
pochod	pochod	k1gInSc4	pochod
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
se	se	k3xPyFc4	se
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
sešla	sejít	k5eAaPmAgFnS	sejít
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
300	[number]	k4	300
osob	osoba	k1gFnPc2	osoba
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
kolem	kolem	k7c2	kolem
tisícovky	tisícovka	k1gFnSc2	tisícovka
příslušníků	příslušník	k1gMnPc2	příslušník
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
POKORNÝ	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
máj	máj	k1gFnSc1	máj
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2004-05-01	[number]	k4	2004-05-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
