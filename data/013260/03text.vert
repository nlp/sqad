<p>
<s>
Elán	elán	k1gInSc1	elán
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
(	(	kIx(	(
<g/>
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
<g/>
)	)	kIx)	)
pop-rocková	popockový	k2eAgFnSc1d1	pop-rocková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
baskytaristou	baskytarista	k1gMnSc7	baskytarista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Jozefem	Jozef	k1gMnSc7	Jozef
"	"	kIx"	"
<g/>
Jožem	Jož	k1gInSc7	Jož
<g/>
"	"	kIx"	"
Rážem	Ráž	k1gMnSc7	Ráž
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
další	další	k2eAgFnSc7d1	další
výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
slovenské	slovenský	k2eAgFnSc2d1	slovenská
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Václavem	Václav	k1gMnSc7	Václav
"	"	kIx"	"
<g/>
Vašem	váš	k3xOp2gInSc6	váš
<g/>
"	"	kIx"	"
Patejdlem	Patejdlo	k1gNnSc7	Patejdlo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
tvořil	tvořit	k5eAaImAgInS	tvořit
s	s	k7c7	s
Rážem	Ráž	k1gMnSc7	Ráž
neodmyslitelnou	odmyslitelný	k2eNgFnSc4d1	neodmyslitelná
dvojici	dvojice	k1gFnSc4	dvojice
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1985	[number]	k4	1985
Patejdl	Patejdl	k1gFnSc2	Patejdl
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
soukromou	soukromý	k2eAgFnSc4d1	soukromá
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
o	o	k7c6	o
11	[number]	k4	11
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Elánu	elán	k1gInSc2	elán
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Partička	partička	k1gFnSc1	partička
pubertálních	pubertální	k2eAgInPc2d1	pubertální
14	[number]	k4	14
<g/>
letých	letý	k2eAgMnPc2d1	letý
kluků	kluk	k1gMnPc2	kluk
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
po	po	k7c6	po
různých	různý	k2eAgInPc6d1	různý
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
restauracích	restaurace	k1gFnPc6	restaurace
a	a	k8xC	a
získávala	získávat	k5eAaImAgFnS	získávat
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
renomé	renomé	k1gNnSc4	renomé
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
zač	zač	k6eAd1	zač
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
největší	veliký	k2eAgFnSc6d3	veliký
česko-slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
hudební	hudební	k2eAgFnSc6d1	hudební
soutěži	soutěž	k1gFnSc6	soutěž
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
lyra	lyra	k1gFnSc1	lyra
'	'	kIx"	'
<g/>
80	[number]	k4	80
získala	získat	k5eAaPmAgFnS	získat
Stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
lyru	lyra	k1gFnSc4	lyra
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
skladbou	skladba	k1gFnSc7	skladba
Kaskadér	kaskadér	k1gMnSc1	kaskadér
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
vydal	vydat	k5eAaPmAgInS	vydat
Elán	elán	k1gInSc4	elán
10	[number]	k4	10
velice	velice	k6eAd1	velice
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
alb	album	k1gNnPc2	album
a	a	k8xC	a
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
se	se	k3xPyFc4	se
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
skupin	skupina	k1gFnPc2	skupina
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nP	patřit
písně	píseň	k1gFnPc1	píseň
Láska	láska	k1gFnSc1	láska
moja	moja	k?	moja
<g/>
,	,	kIx,	,
Neviem	Nevius	k1gMnSc7	Nevius
byť	byť	k8xS	byť
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
Kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
Aj	aj	kA	aj
keď	keď	k?	keď
bez	bez	k7c2	bez
peňazí	peňazí	k1gNnSc2	peňazí
<g/>
,	,	kIx,	,
Zaľúbil	Zaľúbil	k1gMnSc1	Zaľúbil
sa	sa	k?	sa
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
Amnestia	Amnestia	k1gFnSc1	Amnestia
na	na	k7c4	na
neveru	nevera	k1gFnSc4	nevera
<g/>
,	,	kIx,	,
Nie	Nie	k1gFnSc4	Nie
sme	sme	k?	sme
zlí	zlít	k5eAaImIp3nS	zlít
<g/>
,	,	kIx,	,
Jedenáste	Jedenást	k1gMnSc5	Jedenást
prikazanie	prikazanie	k1gFnSc1	prikazanie
<g/>
,	,	kIx,	,
Smrtka	Smrtka	k1gFnSc1	Smrtka
na	na	k7c4	na
pražskom	pražskom	k1gInSc4	pražskom
orloji	orloj	k1gInPc7	orloj
<g/>
,	,	kIx,	,
Tanečnice	tanečnice	k1gFnPc4	tanečnice
z	z	k7c2	z
Lúčnice	Lúčnice	k1gFnSc2	Lúčnice
<g/>
,	,	kIx,	,
Sestrička	Sestrička	k1gFnSc1	Sestrička
z	z	k7c2	z
Kramárov	Kramárovo	k1gNnPc2	Kramárovo
<g/>
,	,	kIx,	,
Stužková	stužkový	k2eAgNnPc1d1	stužkový
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
písním	píseň	k1gFnPc3	píseň
byly	být	k5eAaImAgFnP	být
natočeny	natočen	k2eAgFnPc1d1	natočena
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nevšední	všední	k2eNgInPc4d1	nevšední
videoklipy	videoklip	k1gInPc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
byl	být	k5eAaImAgInS	být
i	i	k9	i
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
grafický	grafický	k2eAgInSc1d1	grafický
styl	styl	k1gInSc1	styl
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
loga	logo	k1gNnSc2	logo
skupiny	skupina	k1gFnSc2	skupina
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bubáka	bubák	k1gMnSc2	bubák
<g/>
"	"	kIx"	"
s	s	k7c7	s
vyplazeným	vyplazený	k2eAgInSc7d1	vyplazený
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jej	on	k3xPp3gMnSc4	on
karikaturista	karikaturista	k1gMnSc1	karikaturista
Alan	Alan	k1gMnSc1	Alan
Lesyk	Lesyk	k1gMnSc1	Lesyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
vážnou	vážný	k2eAgFnSc4d1	vážná
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
J.	J.	kA	J.
Ráž	Ráž	k1gMnSc1	Ráž
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
r.	r.	kA	r.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Elán	elán	k1gInSc1	elán
dodnes	dodnes	k6eAd1	dodnes
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
vyprodává	vyprodávat	k5eAaImIp3nS	vyprodávat
koncerty	koncert	k1gInPc4	koncert
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
účastnilo	účastnit	k5eAaImAgNnS	účastnit
obřího	obří	k2eAgInSc2d1	obří
koncertu	koncert	k1gInSc2	koncert
cca	cca	kA	cca
75	[number]	k4	75
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
úplně	úplně	k6eAd1	úplně
vyprodána	vyprodat	k5eAaPmNgFnS	vyprodat
O2	O2	k1gFnSc7	O2
arena	aren	k1gMnSc2	aren
(	(	kIx(	(
<g/>
cca	cca	kA	cca
11	[number]	k4	11
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
V	v	k7c6	v
září	září	k1gNnSc6	září
2007	[number]	k4	2007
se	se	k3xPyFc4	se
skupině	skupina	k1gFnSc3	skupina
podařilo	podařit	k5eAaPmAgNnS	podařit
prorazit	prorazit	k5eAaPmF	prorazit
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
když	když	k8xS	když
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c4	v
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Elán	elán	k1gInSc1	elán
zpočátku	zpočátku	k6eAd1	zpočátku
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
volná	volný	k2eAgFnSc1d1	volná
formace	formace	k1gFnSc1	formace
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Hrávali	hrávat	k5eAaImAgMnP	hrávat
ve	v	k7c6	v
středoškolských	středoškolský	k2eAgInPc6d1	středoškolský
a	a	k8xC	a
vysokoškolských	vysokoškolský	k2eAgInPc6d1	vysokoškolský
klubech	klub	k1gInPc6	klub
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
:	:	kIx,	:
v	v	k7c6	v
Tunisu	Tunis	k1gInSc6	Tunis
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
známé	známý	k2eAgFnPc1d1	známá
původní	původní	k2eAgFnPc1d1	původní
skladby	skladba	k1gFnPc1	skladba
skupiny	skupina	k1gFnPc1	skupina
Elán	elán	k1gInSc4	elán
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
Semafór	Semafór	k1gInSc1	Semafór
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dám	dát	k5eAaPmIp1nS	dát
ti	ty	k3xPp2nSc3	ty
všetko	všetka	k1gFnSc5	všetka
čo	čo	k?	čo
mám	mít	k5eAaImIp1nS	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ponúkam	Ponúkam	k1gInSc1	Ponúkam
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
pokus	pokus	k1gInSc1	pokus
Vaša	Vaša	k?	Vaša
Patejdla	Patejdla	k1gFnSc1	Patejdla
<g/>
,	,	kIx,	,
skladba	skladba	k1gFnSc1	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Bláznivé	bláznivý	k2eAgFnPc4d1	bláznivá
hry	hra	k1gFnPc4	hra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
Bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
lyře	lyra	k1gFnSc6	lyra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Vašo	Vašo	k1gMnSc1	Vašo
Patejdl	Patejdl	k1gMnSc1	Patejdl
tehdy	tehdy	k6eAd1	tehdy
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
aranžmá	aranžmá	k1gNnSc4	aranžmá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
na	na	k7c6	na
Bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
lyře	lyra	k1gFnSc6	lyra
získala	získat	k5eAaPmAgFnS	získat
stříbrné	stříbrná	k1gFnPc4	stříbrná
ocenění	ocenění	k1gNnSc2	ocenění
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Kaskadér	kaskadér	k1gMnSc1	kaskadér
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
kapela	kapela	k1gFnSc1	kapela
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
publikum	publikum	k1gNnSc4	publikum
hlavně	hlavně	k6eAd1	hlavně
svou	svůj	k3xOyFgFnSc7	svůj
původní	původní	k2eAgFnSc7d1	původní
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
byl	být	k5eAaImAgInS	být
příchod	příchod	k1gInSc1	příchod
Jána	Ján	k1gMnSc2	Ján
Baláže	Baláž	k1gMnSc2	Baláž
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Modus	modus	k1gInSc1	modus
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc1	spolupráce
na	na	k7c6	na
koncipovaných	koncipovaný	k2eAgNnPc6d1	koncipované
studiových	studiový	k2eAgNnPc6d1	studiové
albech	album	k1gNnPc6	album
s	s	k7c7	s
textaři	textař	k1gMnPc7	textař
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Boris	Boris	k1gMnSc1	Boris
Filan	Filan	k1gMnSc1	Filan
<g/>
;	;	kIx,	;
výtvarná	výtvarný	k2eAgNnPc1d1	výtvarné
zpracování	zpracování	k1gNnSc4	zpracování
vizualizace	vizualizace	k1gFnSc2	vizualizace
alb	album	k1gNnPc2	album
a	a	k8xC	a
hudebních	hudební	k2eAgInPc2d1	hudební
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
loga	logo	k1gNnSc2	logo
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
Bubáka	bubák	k1gMnSc2	bubák
s	s	k7c7	s
vyplazeným	vyplazený	k2eAgInSc7d1	vyplazený
jazykem	jazyk	k1gInSc7	jazyk
od	od	k7c2	od
Alana	Alan	k1gMnSc2	Alan
Lesyka	Lesyek	k1gMnSc2	Lesyek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
debutovým	debutový	k2eAgNnSc7d1	debutové
albem	album	k1gNnSc7	album
byl	být	k5eAaImAgMnS	být
Ôsmy	Ôsma	k1gMnSc2	Ôsma
svetadiel	svetadiel	k1gMnSc1	svetadiel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
následovaly	následovat	k5eAaImAgFnP	následovat
alba	alba	k1gFnSc1	alba
Nie	Nie	k1gFnSc1	Nie
sme	sme	k?	sme
zlí	zlít	k5eAaImIp3nS	zlít
<g/>
,	,	kIx,	,
Elán	elán	k1gInSc1	elán
3	[number]	k4	3
<g/>
,	,	kIx,	,
Hodina	hodina	k1gFnSc1	hodina
slovenčiny	slovenčina	k1gFnSc2	slovenčina
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Tuláci	tulák	k1gMnPc1	tulák
v	v	k7c6	v
podchodoch	podchodoch	k1gMnSc1	podchodoch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Človečina	Človečina	k1gFnSc1	Človečina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Stužková	stužkový	k2eAgFnSc1d1	Stužková
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zaľúbil	Zaľúbil	k1gMnSc1	Zaľúbil
sa	sa	k?	sa
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Kráľovná	Kráľovné	k1gNnPc1	Kráľovné
bielych	bielych	k1gMnSc1	bielych
tenisiek	tenisiek	k1gMnSc1	tenisiek
<g/>
"	"	kIx"	"
patří	patřit	k5eAaImIp3nS	patřit
stále	stále	k6eAd1	stále
do	do	k7c2	do
repertoáru	repertoár	k1gInSc2	repertoár
nejen	nejen	k6eAd1	nejen
koncertů	koncert	k1gInPc2	koncert
skupiny	skupina	k1gFnSc2	skupina
Elán	elán	k1gInSc4	elán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
repertoáru	repertoár	k1gInSc2	repertoár
hudebníků	hudebník	k1gMnPc2	hudebník
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
společenských	společenský	k2eAgFnPc2d1	společenská
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
odcházejí	odcházet	k5eAaImIp3nP	odcházet
Zdeno	Zdena	k1gFnSc5	Zdena
Baláž	Baláž	k1gMnSc1	Baláž
<g/>
,	,	kIx,	,
Vašo	Vašo	k1gMnSc1	Vašo
Patejdl	Patejdl	k1gMnSc1	Patejdl
a	a	k8xC	a
Juraj	Juraj	k1gMnSc1	Juraj
Farkaš	Farkaš	k1gMnSc1	Farkaš
<g/>
.	.	kIx.	.
</s>
<s>
Ján	Ján	k1gMnSc1	Ján
Baláž	Baláž	k1gMnSc1	Baláž
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jožom	Jožom	k1gInSc1	Jožom
Rážem	Ráž	k1gMnSc7	Ráž
doplnili	doplnit	k5eAaPmAgMnP	doplnit
sestavu	sestava	k1gFnSc4	sestava
skupiny	skupina	k1gFnSc2	skupina
bubeníkem	bubeník	k1gMnSc7	bubeník
Gabrielem	Gabriel	k1gMnSc7	Gabriel
Szabó	Szabó	k1gMnSc7	Szabó
a	a	k8xC	a
klávesistou	klávesista	k1gMnSc7	klávesista
Martinem	Martin	k1gMnSc7	Martin
Karvašem	Karvaš	k1gMnSc7	Karvaš
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
zahájila	zahájit	k5eAaPmAgFnS	zahájit
druhou	druhý	k4xOgFnSc4	druhý
etapu	etapa	k1gFnSc4	etapa
působení	působení	k1gNnSc2	působení
kapely	kapela	k1gFnSc2	kapela
od	od	k7c2	od
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Elán	elán	k1gInSc1	elán
vydala	vydat	k5eAaPmAgFnS	vydat
tři	tři	k4xCgNnPc4	tři
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
Detektivka	detektivka	k1gFnSc1	detektivka
<g/>
,	,	kIx,	,
Nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
náklad	náklad	k1gInSc1	náklad
(	(	kIx(	(
<g/>
se	s	k7c7	s
sérií	série	k1gFnSc7	série
videoklipů	videoklip	k1gInPc2	videoklip
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rabaka	Rabak	k1gMnSc4	Rabak
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
scénáři	scénář	k1gInSc6	scénář
se	se	k3xPyFc4	se
s	s	k7c7	s
Borisem	Boris	k1gMnSc7	Boris
Filanem	Filan	k1gMnSc7	Filan
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
režisér	režisér	k1gMnSc1	režisér
Dušan	Dušan	k1gMnSc1	Dušan
Rapoš	Rapoš	k1gMnSc1	Rapoš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pocházejí	pocházet	k5eAaImIp3nP	pocházet
hity	hit	k1gInPc1	hit
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Tanečnice	tanečnice	k1gFnSc1	tanečnice
z	z	k7c2	z
Lúčnice	Lúčnice	k1gFnSc2	Lúčnice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Detektívka	Detektívka	k1gFnSc1	Detektívka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ver	Ver	k1gMnSc1	Ver
mi	já	k3xPp1nSc3	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
náklad	náklad	k1gInSc1	náklad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
singlu	singl	k1gInSc6	singl
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Smrtka	Smrtka	k1gFnSc1	Smrtka
na	na	k7c4	na
Pražskom	Pražskom	k1gInSc4	Pražskom
orloji	orloj	k1gInPc7	orloj
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dresy	dres	k1gInPc1	dres
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jožo	Joža	k1gMnSc5	Joža
Ráž	Ráž	k1gMnSc1	Ráž
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Nagym	Nagym	k1gInSc4	Nagym
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Psi	pes	k1gMnPc1	pes
sa	sa	k?	sa
bránia	bránium	k1gNnSc2	bránium
útokom	útokom	k1gInSc4	útokom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Rabaka	Rabak	k1gMnSc2	Rabak
vzešly	vzejít	k5eAaPmAgFnP	vzejít
skladby	skladba	k1gFnPc1	skladba
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Od	od	k7c2	od
Tatier	Tatira	k1gFnPc2	Tatira
k	k	k7c3	k
Dunaju	Dunaju	k1gFnPc3	Dunaju
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Van	van	k1gInSc4	van
Goghovo	Goghův	k2eAgNnSc4d1	Goghovo
ucho	ucho	k1gNnSc4	ucho
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Čaba	Čaba	k1gFnSc1	Čaba
neblázni	bláznit	k5eNaImRp2nS	bláznit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešli	odejít	k5eAaPmAgMnP	odejít
Szabó	Szabó	k1gMnPc1	Szabó
s	s	k7c7	s
Karvašem	Karvaš	k1gInSc7	Karvaš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Jožo	Joža	k1gFnSc5	Joža
Ráž	Ráž	k1gMnSc1	Ráž
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Baláž	Baláž	k1gMnSc1	Baláž
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Farnbauer	Farnbauer	k1gMnSc1	Farnbauer
<g/>
,	,	kIx,	,
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Horňák	Horňák	k1gMnSc1	Horňák
a	a	k8xC	a
Juraj	Juraj	k1gMnSc1	Juraj
Kuchárek	Kuchárek	k1gMnSc1	Kuchárek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
nahrála	nahrát	k5eAaBmAgFnS	nahrát
alba	alba	k1gFnSc1	alba
Netvor	netvora	k1gFnPc2	netvora
z	z	k7c2	z
čiernej	čiernat	k5eAaPmRp2nS	čiernat
hviezdy	hviezd	k1gInPc4	hviezd
Q7A	Q7A	k1gMnPc1	Q7A
a	a	k8xC	a
několik	několik	k4yIc1	několik
reedic	reedice	k1gFnPc2	reedice
svých	svůj	k3xOyFgInPc2	svůj
hitů	hit	k1gInPc2	hit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pocházejí	pocházet	k5eAaImIp3nP	pocházet
hity	hit	k1gInPc4	hit
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Bosorka	bosorka	k1gFnSc1	bosorka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sestrička	Sestrička	k1gFnSc1	Sestrička
z	z	k7c2	z
Kramárov	Kramárovo	k1gNnPc2	Kramárovo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hostia	Hostia	k1gFnSc1	Hostia
z	z	k7c2	z
inej	inej	k1gMnSc1	inej
planéty	planéta	k1gMnSc2	planéta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pištoľ	Pištoľ	k1gFnSc1	Pištoľ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Modlitba	modlitba	k1gFnSc1	modlitba
pre	pre	k?	pre
dva	dva	k4xCgInPc4	dva
hlasy	hlas	k1gInPc4	hlas
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Amnestia	Amnestia	k1gFnSc1	Amnestia
na	na	k7c6	na
neveru	never	k1gInSc6	never
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
vrací	vracet	k5eAaImIp3nS	vracet
Vašo	Vašo	k6eAd1	Vašo
Patejdl	Patejdl	k1gFnSc1	Patejdl
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
nahrávání	nahrávání	k1gNnSc4	nahrávání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hodina	hodina	k1gFnSc1	hodina
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
Anna	Anna	k1gFnSc1	Anna
Mária	Mária	k1gFnSc1	Mária
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Odkazovač	Odkazovač	k1gInSc1	Odkazovač
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hey	Hey	k1gMnSc1	Hey
<g/>
,	,	kIx,	,
hey	hey	k?	hey
zlato	zlato	k1gNnSc4	zlato
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
motocyklové	motocyklový	k2eAgFnSc6d1	motocyklová
havárii	havárie	k1gFnSc6	havárie
Joža	Joža	k1gFnSc1	Joža
Ráže	ráže	k1gFnSc1	ráže
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
a	a	k8xC	a
2003	[number]	k4	2003
vydána	vydán	k2eAgFnSc1d1	vydána
další	další	k2eAgFnSc1d1	další
dvě	dva	k4xCgFnPc4	dva
komerčně	komerčně	k6eAd1	komerčně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
alba	alba	k1gFnSc1	alba
Elán	elán	k1gInSc1	elán
3000	[number]	k4	3000
<g/>
'	'	kIx"	'
a	a	k8xC	a
Tretie	Tretie	k1gFnSc2	Tretie
oko	oko	k1gNnSc1	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
vydáno	vydán	k2eAgNnSc4d1	vydáno
další	další	k2eAgNnSc4d1	další
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Anjelská	Anjelský	k2eAgFnSc1d1	Anjelská
daň	daň	k1gFnSc1	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elán	elán	k1gInSc1	elán
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
hudební	hudební	k2eAgFnSc2d1	hudební
scény	scéna	k1gFnSc2	scéna
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
řadu	řada	k1gFnSc4	řada
hitů	hit	k1gInPc2	hit
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
i	i	k9	i
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
anketách	anketa	k1gFnPc6	anketa
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jen	jen	k9	jen
slovenské	slovenský	k2eAgFnSc2d1	slovenská
ankety	anketa	k1gFnSc2	anketa
Slávik	Slávika	k1gFnPc2	Slávika
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
Elán	elán	k1gInSc1	elán
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
dodnes	dodnes	k6eAd1	dodnes
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vyprodat	vyprodat	k5eAaPmF	vyprodat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
největší	veliký	k2eAgNnPc4d3	veliký
koncertní	koncertní	k2eAgNnPc4d1	koncertní
pódia	pódium	k1gNnPc4	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Letné	Letná	k1gFnSc6	Letná
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
publiku	publikum	k1gNnSc6	publikum
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
pronajaté	pronajatý	k2eAgFnSc6d1	pronajatá
hale	hala	k1gFnSc6	hala
newyorské	newyorský	k2eAgFnSc2d1	newyorská
Carnegie	Carnegie	k1gFnSc2	Carnegie
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
přes	přes	k7c4	přes
20	[number]	k4	20
slovenských	slovenský	k2eAgMnPc2d1	slovenský
a	a	k8xC	a
5	[number]	k4	5
anglických	anglický	k2eAgNnPc2d1	anglické
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vydali	vydat	k5eAaPmAgMnP	vydat
mnoho	mnoho	k4c4	mnoho
výběrových	výběrový	k2eAgInPc2d1	výběrový
disků	disk	k1gInPc2	disk
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
milióny	milión	k4xCgInPc4	milión
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Ôsmy	Ôsmum	k1gNnPc7	Ôsmum
svetadiel	svetadiet	k5eAaImAgMnS	svetadiet
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Nie	Nie	k1gFnSc1	Nie
sme	sme	k?	sme
zlí	zlít	k5eAaImIp3nS	zlít
<g/>
,	,	kIx,	,
Kamikadze	kamikadze	k1gMnSc1	kamikadze
lover	lover	k1gMnSc1	lover
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Elán	elán	k1gInSc1	elán
3	[number]	k4	3
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Nightshift	Nightshift	k1gInSc1	Nightshift
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Hodina	hodina	k1gFnSc1	hodina
slovenčiny	slovenčina	k1gFnSc2	slovenčina
<g/>
,	,	kIx,	,
Schoolparty	Schoolparta	k1gFnSc2	Schoolparta
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Detektívka	Detektívka	k1gFnSc1	Detektívka
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Neviem	Nevium	k1gNnSc7	Nevium
byť	byť	k8xS	byť
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
Missing	Missing	k1gInSc4	Missing
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
náklad	náklad	k1gInSc1	náklad
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Rabaka	Rabak	k1gMnSc2	Rabak
<g/>
,	,	kIx,	,
Midnight	Midnight	k1gInSc1	Midnight
in	in	k?	in
the	the	k?	the
city	city	k1gFnSc7	city
<g/>
1991	[number]	k4	1991
Netvor	netvora	k1gFnPc2	netvora
z	z	k7c2	z
čiernej	čiernat	k5eAaImRp2nS	čiernat
hviezdy	hviezd	k1gInPc1	hviezd
Q7A	Q7A	k1gMnPc6	Q7A
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Legenda	legenda	k1gFnSc1	legenda
1	[number]	k4	1
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
2	[number]	k4	2
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Hodina	hodina	k1gFnSc1	hodina
angličtiny	angličtina	k1gFnSc2	angličtina
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Hodina	hodina	k1gFnSc1	hodina
nehy	neha	k1gFnSc2	neha
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Hodina	hodina	k1gFnSc1	hodina
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
3	[number]	k4	3
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Legenda	legenda	k1gFnSc1	legenda
4	[number]	k4	4
<g/>
,	,	kIx,	,
Elán	elán	k1gInSc1	elán
Unplugged	Unplugged	k1gInSc1	Unplugged
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Jožo	Joža	k1gFnSc5	Joža
<g/>
...	...	k?	...
<g/>
2000	[number]	k4	2000
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
stvorená	stvorená	k1gFnSc1	stvorená
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
5	[number]	k4	5
-	-	kIx~	-
Posledná	Posledný	k2eAgNnPc4d1	Posledný
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Otazníky	otazník	k1gInPc4	otazník
<g/>
/	/	kIx~	/
<g/>
Všetko	Všetka	k1gFnSc5	Všetka
čo	čo	k?	čo
máš	mít	k5eAaImIp2nS	mít
<g/>
,	,	kIx,	,
Neviem	Nevius	k1gMnSc7	Nevius
byť	byť	k8xS	byť
sám	sám	k3xTgMnSc1	sám
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Elán	elán	k1gInSc1	elán
3000	[number]	k4	3000
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Tretie	Tretie	k1gFnSc1	Tretie
oko	oko	k1gNnSc1	oko
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Elán	elán	k1gInSc1	elán
<g/>
:	:	kIx,	:
Megakoncert	Megakoncert	k1gInSc1	Megakoncert
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Unplugged	Unplugged	k1gInSc1	Unplugged
-	-	kIx~	-
NY	NY	kA	NY
Carnagie	Carnagie	k1gFnSc1	Carnagie
hall	hall	k1gMnSc1	hall
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Anjelska	Anjelska	k1gFnSc1	Anjelska
daň	daň	k1gFnSc1	daň
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Živých	živá	k1gFnPc2	živá
nás	my	k3xPp1nPc2	my
nedostanú	nedostanú	k?	nedostanú
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Tesař	Tesař	k1gMnSc1	Tesař
<g/>
:	:	kIx,	:
Elán	elán	k1gInSc1	elán
Rock	rock	k1gInSc1	rock
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
Champagne	Champagn	k1gMnSc5	Champagn
Avantgarde	Avantgard	k1gMnSc5	Avantgard
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7150	[number]	k4	7150
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
53	[number]	k4	53
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ingrid	Ingrid	k1gFnSc1	Ingrid
Mareková	Mareková	k1gFnSc1	Mareková
<g/>
:	:	kIx,	:
Posledné	Posledný	k2eAgFnPc4d1	Posledná
tri	tri	k?	tri
hodiny	hodina	k1gFnPc4	hodina
Elánu	elán	k1gInSc2	elán
<g/>
,	,	kIx,	,
alebo	aleba	k1gFnSc5	aleba
legenda	legenda	k1gFnSc1	legenda
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
Slovakia	Slovakium	k1gNnSc2	Slovakium
GT	GT	kA	GT
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Marcela	Marcela	k1gFnSc1	Marcela
Titzlová	Titzlová	k1gFnSc1	Titzlová
<g/>
:	:	kIx,	:
Jožo	Joža	k1gFnSc5	Joža
Ráž	Ráž	k1gMnSc1	Ráž
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
neba	neb	k1gInSc2	neb
<g/>
,	,	kIx,	,
Rybka	Rybka	k1gMnSc1	Rybka
Publisher	Publishra	k1gFnPc2	Publishra
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ľuboš	Ľuboš	k1gMnSc1	Ľuboš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
Trištvrte	Trištvrt	k1gInSc5	Trištvrt
na	na	k7c6	na
jeseň	jeseň	k1gFnSc4	jeseň
<g/>
,	,	kIx,	,
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
551	[number]	k4	551
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
140	[number]	k4	140
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byli	být	k5eAaImAgMnP	být
použité	použitý	k2eAgFnPc4d1	použitá
jako	jako	k8xC	jako
písně	píseň	k1gFnPc4	píseň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marcela	Marcela	k1gFnSc1	Marcela
Titzlová	Titzlová	k1gFnSc1	Titzlová
<g/>
:	:	kIx,	:
Elán	elán	k1gInSc1	elán
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
,	,	kIx,	,
Carnegie	Carnegie	k1gFnSc1	Carnegie
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
MC	MC	kA	MC
production	production	k1gInSc1	production
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
+	+	kIx~	+
CD	CD	kA	CD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Elán	elán	k1gInSc1	elán
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
lyra	lyra	k1gFnSc1	lyra
</s>
</p>
