<s>
Leptospiróza	Leptospiróza	k1gFnSc1	Leptospiróza
je	být	k5eAaImIp3nS	být
horečnaté	horečnatý	k2eAgNnSc4d1	horečnaté
bakteriální	bakteriální	k2eAgNnSc4d1	bakteriální
onemocnění	onemocnění	k1gNnSc4	onemocnění
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zoonotické	zoonotický	k2eAgNnSc4d1	zoonotický
onemocnění	onemocnění	k1gNnSc4	onemocnění
způsobené	způsobený	k2eAgNnSc4d1	způsobené
spirochétami	spirochéta	k1gFnPc7	spirochéta
rodu	rod	k1gInSc3	rod
Leptospira	Leptospira	k1gMnSc1	Leptospira
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
Adolf	Adolf	k1gMnSc1	Adolf
Weil	Weil	k1gMnSc1	Weil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
způsob	způsob	k1gInSc1	způsob
nakažení	nakažení	k1gNnSc2	nakažení
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
kontakt	kontakt	k1gInSc4	kontakt
poranění	poranění	k1gNnSc2	poranění
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
očí	oko	k1gNnPc2	oko
nebo	nebo	k8xC	nebo
sliznic	sliznice	k1gFnPc2	sliznice
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
znečištěnou	znečištěný	k2eAgFnSc7d1	znečištěná
močí	moč	k1gFnSc7	moč
nakaženého	nakažený	k2eAgNnSc2d1	nakažené
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Leptospiróza	Leptospiróza	k1gFnSc1	Leptospiróza
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
také	také	k9	také
"	"	kIx"	"
<g/>
krysí	krysí	k2eAgFnSc1d1	krysí
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
infekcí	infekce	k1gFnSc7	infekce
mikroba	mikrob	k1gMnSc4	mikrob
jménem	jméno	k1gNnSc7	jméno
leptospira	leptospira	k1gMnSc1	leptospira
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bakterii	bakterie	k1gFnSc4	bakterie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
spirálovitý	spirálovitý	k2eAgInSc4d1	spirálovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
se	s	k7c7	s
spirochetami	spirocheta	k1gFnPc7	spirocheta
a	a	k8xC	a
borreliemi	borrelie	k1gFnPc7	borrelie
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgInPc2	tento
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
leptospiry	leptospira	k1gFnPc1	leptospira
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Leptospira	Leptospiro	k1gNnSc2	Leptospiro
interrogans	interrogansa	k1gFnPc2	interrogansa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
choroba	choroba	k1gFnSc1	choroba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
antropozoonózy	antropozoonóza	k1gFnSc2	antropozoonóza
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
přenosná	přenosný	k2eAgNnPc1d1	přenosné
ze	z	k7c2	z
zvířete	zvíře	k1gNnSc2	zvíře
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
borrelióza	borrelióza	k1gFnSc1	borrelióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemocné	nemocný	k2eAgNnSc1d1	nemocný
zvíře	zvíře	k1gNnSc1	zvíře
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hlodavce	hlodavec	k1gMnPc4	hlodavec
-	-	kIx~	-
potkany	potkan	k1gMnPc4	potkan
<g/>
,	,	kIx,	,
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
vepře	vepřít	k5eAaPmIp3nS	vepřít
<g/>
)	)	kIx)	)
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
tyto	tento	k3xDgInPc4	tento
mikroby	mikrob	k1gInPc4	mikrob
močí	močit	k5eAaImIp3nS	močit
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
infekční	infekční	k2eAgFnSc7d1	infekční
močí	moč	k1gFnSc7	moč
zamoří	zamořit	k5eAaPmIp3nS	zamořit
(	(	kIx(	(
<g/>
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
zdrojem	zdroj	k1gInSc7	zdroj
nákazy	nákaza	k1gFnSc2	nákaza
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
možných	možný	k2eAgInPc2d1	možný
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
nakazit	nakazit	k5eAaPmF	nakazit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
člověk	člověk	k1gMnSc1	člověk
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
půdou	půda	k1gFnSc7	půda
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nemocným	mocný	k2eNgNnSc7d1	nemocné
zvířetem	zvíře	k1gNnSc7	zvíře
znečištěna	znečistit	k5eAaPmNgFnS	znečistit
(	(	kIx(	(
<g/>
kontaminována	kontaminovat	k5eAaBmNgFnS	kontaminovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
leptospiry	leptospira	k1gFnPc1	leptospira
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
trávícího	trávící	k2eAgInSc2d1	trávící
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
neporušenou	porušený	k2eNgFnSc7d1	neporušená
sliznicí	sliznice	k1gFnSc7	sliznice
do	do	k7c2	do
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
znečištěné	znečištěný	k2eAgFnSc2d1	znečištěná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vám	vy	k3xPp2nPc3	vy
"	"	kIx"	"
<g/>
šplouchne	šplouchnout	k5eAaPmIp3nS	šplouchnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
mikroby	mikrob	k1gInPc1	mikrob
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
spojivkou	spojivka	k1gFnSc7	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
nevědomky	nevědomky	k6eAd1	nevědomky
loknete	loknout	k5eAaPmIp2nP	loknout
vody	voda	k1gFnPc1	voda
při	při	k7c6	při
koupání	koupání	k1gNnSc6	koupání
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
bahnem	bahno	k1gNnSc7	bahno
nebo	nebo	k8xC	nebo
vlhkou	vlhký	k2eAgFnSc7d1	vlhká
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
máte	mít	k5eAaImIp2nP	mít
na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
oděrky	oděrka	k1gFnSc2	oděrka
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
porušenou	porušený	k2eAgFnSc4d1	porušená
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
možný	možný	k2eAgInSc1d1	možný
způsob	způsob	k1gInSc1	způsob
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vás	vy	k3xPp2nPc4	vy
pokouše	pokousat	k5eAaPmIp3nS	pokousat
nebo	nebo	k8xC	nebo
poškrábe	poškrábat	k5eAaPmIp3nS	poškrábat
nemocné	mocný	k2eNgNnSc4d1	nemocné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
choroba	choroba	k1gFnSc1	choroba
převážně	převážně	k6eAd1	převážně
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
jako	jako	k9	jako
profesionální	profesionální	k2eAgFnSc1d1	profesionální
nákaza	nákaza	k1gFnSc1	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
ti	ten	k3xDgMnPc1	ten
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
díky	díky	k7c3	díky
charakteru	charakter	k1gInSc3	charakter
svého	svůj	k3xOyFgNnSc2	svůj
povolání	povolání	k1gNnSc2	povolání
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
leptospirami	leptospira	k1gFnPc7	leptospira
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
-	-	kIx~	-
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
zootechnici	zootechnik	k1gMnPc1	zootechnik
<g/>
,	,	kIx,	,
zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
,	,	kIx,	,
pracovníci	pracovník	k1gMnPc1	pracovník
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
<g/>
..	..	k?	..
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
velmi	velmi	k6eAd1	velmi
oblíbeného	oblíbený	k2eAgNnSc2d1	oblíbené
stanování	stanování	k1gNnSc2	stanování
<g/>
,	,	kIx,	,
chalupaření	chalupaření	k1gNnSc2	chalupaření
<g/>
,	,	kIx,	,
rybaření	rybařený	k2eAgMnPc1d1	rybařený
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc1	chování
různých	různý	k2eAgNnPc2d1	různé
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
apod.	apod.	kA	apod.
může	moct	k5eAaImIp3nS	moct
leptospiróza	leptospiróza	k1gFnSc1	leptospiróza
postihnout	postihnout	k5eAaPmF	postihnout
prakticky	prakticky	k6eAd1	prakticky
každého	každý	k3xTgNnSc2	každý
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgFnPc2d3	nejčastější
infekcí	infekce	k1gFnPc2	infekce
přenosných	přenosný	k2eAgFnPc2d1	přenosná
ze	z	k7c2	z
zvířete	zvíře	k1gNnSc2	zvíře
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
prokázán	prokázán	k2eAgMnSc1d1	prokázán
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
ve	v	k7c6	v
vašem	váš	k3xOp2gInSc6	váš
okolí	okolí	k1gNnSc6	okolí
má	mít	k5eAaImIp3nS	mít
leptospirózu	leptospiróza	k1gFnSc4	leptospiróza
<g/>
,	,	kIx,	,
nemusíte	muset	k5eNaImIp2nP	muset
se	se	k3xPyFc4	se
bát	bát	k5eAaImF	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
chytnete	chytnout	k5eAaPmIp2nP	chytnout
<g/>
.	.	kIx.	.
</s>
<s>
Leptospirózou	Leptospiróza	k1gFnSc7	Leptospiróza
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
ohroženi	ohrožen	k2eAgMnPc1d1	ohrožen
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
leptospiry	leptospira	k1gFnPc4	leptospira
vylučovat	vylučovat	k5eAaImF	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
zemědělci	zemědělec	k1gMnPc1	zemědělec
a	a	k8xC	a
zootechnici	zootechnik	k1gMnPc1	zootechnik
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
vodáci	vodák	k1gMnPc1	vodák
<g/>
,	,	kIx,	,
<g/>
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
českých	český	k2eAgInPc6d1	český
tocích	tok	k1gInPc6	tok
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
vodou	voda	k1gFnSc7	voda
docela	docela	k6eAd1	docela
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
časté	častý	k2eAgMnPc4d1	častý
přenašeče	přenašeč	k1gMnPc4	přenašeč
této	tento	k3xDgFnSc2	tento
choroby	choroba	k1gFnSc2	choroba
patří	patřit	k5eAaImIp3nP	patřit
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
všudypřítomní	všudypřítomný	k2eAgMnPc1d1	všudypřítomný
potkani	potkan	k1gMnPc1	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
pak	pak	k6eAd1	pak
mezi	mezi	k7c4	mezi
další	další	k1gNnSc4	další
ohrožené	ohrožený	k2eAgFnSc2d1	ohrožená
profesní	profesní	k2eAgFnSc2d1	profesní
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nP	patřit
deratizátoři	deratizátor	k1gMnPc1	deratizátor
a	a	k8xC	a
pracovníci	pracovník	k1gMnPc1	pracovník
kanalizací	kanalizace	k1gFnPc2	kanalizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
obyvatelé	obyvatel	k1gMnPc1	obyvatel
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potkani	potkan	k1gMnPc1	potkan
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Leptospirózu	Leptospiróza	k1gFnSc4	Leptospiróza
ale	ale	k8xC	ale
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
jen	jen	k9	jen
"	"	kIx"	"
<g/>
divocí	divoký	k2eAgMnPc1d1	divoký
<g/>
"	"	kIx"	"
potkani	potkan	k1gMnPc1	potkan
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
nakazit	nakazit	k5eAaPmF	nakazit
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
ošetřovatelé	ošetřovatel	k1gMnPc1	ošetřovatel
laboratorních	laboratorní	k2eAgInPc2d1	laboratorní
"	"	kIx"	"
<g/>
čistých	čistý	k2eAgInPc2d1	čistý
<g/>
"	"	kIx"	"
potkanů	potkan	k1gMnPc2	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Nakažené	nakažený	k2eAgNnSc1d1	nakažené
zvíře	zvíře	k1gNnSc1	zvíře
může	moct	k5eAaImIp3nS	moct
svou	svůj	k3xOyFgFnSc7	svůj
močí	moč	k1gFnSc7	moč
znečistit	znečistit	k5eAaPmF	znečistit
prakticky	prakticky	k6eAd1	prakticky
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	čí	k3xOyRgNnSc7	čí
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Leptospiry	Leptospira	k1gFnPc1	Leptospira
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
přežívají	přežívat	k5eAaImIp3nP	přežívat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pobýváte	pobývat	k5eAaImIp2nP	pobývat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
byste	by	kYmCp2nP	by
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
pití	pití	k1gNnSc3	pití
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
studánek	studánka	k1gFnPc2	studánka
<g/>
,	,	kIx,	,
koupání	koupání	k1gNnSc1	koupání
ve	v	k7c6	v
stojatých	stojatý	k2eAgFnPc6d1	stojatá
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
jezení	jezenit	k5eAaPmIp3nS	jezenit
neumytého	umytý	k2eNgNnSc2d1	neumyté
ovoce	ovoce	k1gNnSc2	ovoce
čí	čí	k3xOyQgFnSc2	čí
zeleniny	zelenina	k1gFnSc2	zelenina
ze	z	k7c2	z
zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
,	,	kIx,	,
chození	chozený	k2eAgMnPc1d1	chozený
naboso	naboso	k6eAd1	naboso
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
nebo	nebo	k8xC	nebo
vlhké	vlhký	k2eAgFnSc6d1	vlhká
půdě	půda	k1gFnSc6	půda
apod.	apod.	kA	apod.
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
hubení	hubení	k1gNnSc6	hubení
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
objektech	objekt	k1gInPc6	objekt
(	(	kIx(	(
<g/>
deratizaci	deratizace	k1gFnSc6	deratizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rizikových	rizikový	k2eAgNnPc6d1	rizikové
pracovištích	pracoviště	k1gNnPc6	pracoviště
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
zachovávání	zachovávání	k1gNnSc1	zachovávání
předpisů	předpis	k1gInPc2	předpis
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
důsledně	důsledně	k6eAd1	důsledně
používat	používat	k5eAaImF	používat
pracovních	pracovní	k2eAgFnPc2d1	pracovní
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
byste	by	kYmCp2nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
řídit	řídit	k5eAaImF	řídit
základními	základní	k2eAgInPc7d1	základní
hygienickými	hygienický	k2eAgInPc7d1	hygienický
pravidly	pravidlo	k1gNnPc7	pravidlo
-	-	kIx~	-
používat	používat	k5eAaImF	používat
nezávadnou	závadný	k2eNgFnSc4d1	nezávadná
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
se	se	k3xPyFc4	se
pití	pití	k1gNnSc2	pití
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
loknete	loknout	k5eAaPmIp2nP	loknout
při	při	k7c6	při
koupání	koupání	k1gNnSc6	koupání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
koupat	koupat	k5eAaImF	koupat
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodních	přírodní	k2eAgNnPc6d1	přírodní
koupalištích	koupaliště	k1gNnPc6	koupaliště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dobře	dobře	k6eAd1	dobře
umýt	umýt	k5eAaPmF	umýt
všechny	všechen	k3xTgFnPc4	všechen
pochutiny	pochutina	k1gFnPc4	pochutina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jste	být	k5eAaImIp2nP	být
nasbírali	nasbírat	k5eAaPmAgMnP	nasbírat
nebo	nebo	k8xC	nebo
natrhali	natrhat	k5eAaPmAgMnP	natrhat
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Leptospiróza	Leptospiróza	k1gFnSc1	Leptospiróza
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc4	onemocnění
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
zcela	zcela	k6eAd1	zcela
skrytě	skrytě	k6eAd1	skrytě
(	(	kIx(	(
<g/>
inaparentně	inaparentně	k6eAd1	inaparentně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
příznaků	příznak	k1gInPc2	příznak
leptospirózy	leptospiróza	k1gFnSc2	leptospiróza
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
běžná	běžný	k2eAgNnPc4d1	běžné
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
opravdu	opravdu	k6eAd1	opravdu
těžké	těžký	k2eAgNnSc1d1	těžké
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
správné	správný	k2eAgFnSc3d1	správná
diagnóze	diagnóza	k1gFnSc3	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
onemocnění	onemocnění	k1gNnSc1	onemocnění
projeví	projevit	k5eAaPmIp3nS	projevit
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
věku	věk	k1gInSc6	věk
a	a	k8xC	a
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
na	na	k7c6	na
typu	typ	k1gInSc6	typ
leptospiry	leptospira	k1gFnSc2	leptospira
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
organismus	organismus	k1gInSc4	organismus
infikovala	infikovat	k5eAaBmAgFnS	infikovat
a	a	k8xC	a
množství	množství	k1gNnPc1	množství
mikrobů	mikrob	k1gInPc2	mikrob
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
pronikly	proniknout	k5eAaPmAgFnP	proniknout
<g/>
.	.	kIx.	.
</s>
<s>
Leptospiróza	Leptospiróza	k1gFnSc1	Leptospiróza
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
lehce	lehko	k6eAd1	lehko
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
trvajícími	trvající	k2eAgInPc7d1	trvající
jen	jen	k6eAd1	jen
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
velmi	velmi	k6eAd1	velmi
připomínají	připomínat	k5eAaImIp3nP	připomínat
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dvoufázový	dvoufázový	k2eAgInSc1d1	dvoufázový
průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
blaťácká	blaťácký	k2eAgFnSc1d1	blaťácký
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
polní	polní	k2eAgFnSc1d1	polní
<g/>
,	,	kIx,	,
žňová	žňový	k2eAgFnSc1d1	žňová
horečka	horečka	k1gFnSc1	horečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
inkubační	inkubační	k2eAgFnSc6d1	inkubační
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uplyne	uplynout	k5eAaPmIp3nS	uplynout
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
nakažení	nakažení	k1gNnSc2	nakažení
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
projevům	projev	k1gInPc3	projev
infekce	infekce	k1gFnSc2	infekce
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
nemocného	nemocný	k1gMnSc2	nemocný
náhle	náhle	k6eAd1	náhle
objeví	objevit	k5eAaPmIp3nS	objevit
horečka	horečka	k1gFnSc1	horečka
až	až	k9	až
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
se	s	k7c7	s
zimnicí	zimnice	k1gFnSc7	zimnice
a	a	k8xC	a
třesavkou	třesavka	k1gFnSc7	třesavka
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
trpí	trpět	k5eAaImIp3nS	trpět
krutými	krutý	k2eAgFnPc7d1	krutá
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
-	-	kIx~	-
svaly	sval	k1gInPc1	sval
bolí	bolet	k5eAaImIp3nP	bolet
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
lýtkách	lýtko	k1gNnPc6	lýtko
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgFnPc4d1	přítomna
bolesti	bolest	k1gFnPc4	bolest
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
ke	k	k7c3	k
zvracení	zvracení	k1gNnSc3	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k1gMnSc1	nemocný
má	mít	k5eAaImIp3nS	mít
špatnou	špatný	k2eAgFnSc4d1	špatná
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
blouzní	blouznit	k5eAaImIp3nS	blouznit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zmatený	zmatený	k2eAgMnSc1d1	zmatený
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
psychotické	psychotický	k2eAgInPc1d1	psychotický
projevy	projev	k1gInPc1	projev
-	-	kIx~	-
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
onemocnění	onemocnění	k1gNnPc2	onemocnění
probíhá	probíhat	k5eAaImIp3nS	probíhat
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
a	a	k8xC	a
najednou	najednou	k6eAd1	najednou
veškeré	veškerý	k3xTgInPc4	veškerý
příznaky	příznak	k1gInPc4	příznak
vymizí	vymizet	k5eAaPmIp3nP	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k1gMnSc1	nemocný
se	se	k3xPyFc4	se
jakoby	jakoby	k8xS	jakoby
náhle	náhle	k6eAd1	náhle
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
ani	ani	k8xC	ani
nemusí	muset	k5eNaImIp3nP	muset
rozvinout	rozvinout	k5eAaPmF	rozvinout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
infekce	infekce	k1gFnSc1	infekce
závažná	závažný	k2eAgFnSc1d1	závažná
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
znovu	znovu	k6eAd1	znovu
vrací	vracet	k5eAaImIp3nS	vracet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
původními	původní	k2eAgInPc7d1	původní
projevy	projev	k1gInPc7	projev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
porucha	porucha	k1gFnSc1	porucha
funkce	funkce	k1gFnSc2	funkce
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
psychického	psychický	k2eAgInSc2d1	psychický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
tuhnutí	tuhnutí	k1gNnSc3	tuhnutí
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
zvrací	zvracet	k5eAaImIp3nS	zvracet
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
vyjádřeny	vyjádřen	k2eAgInPc4d1	vyjádřen
znaky	znak	k1gInPc4	znak
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	on	k3xPp3gNnPc4	on
onemocnění	onemocnění	k1gNnPc4	onemocnění
poměrně	poměrně	k6eAd1	poměrně
závažné	závažný	k2eAgFnPc1d1	závažná
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
léčbě	léčba	k1gFnSc6	léčba
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc4	takovýto
průběh	průběh	k1gInSc4	průběh
mají	mít	k5eAaImIp3nP	mít
leptospirózy	leptospiróza	k1gFnPc1	leptospiróza
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
Leptospirou	Leptospirý	k2eAgFnSc7d1	Leptospirý
grippotyphosou	grippotyphosa	k1gFnSc7	grippotyphosa
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
organismus	organismus	k1gInSc1	organismus
napaden	napaden	k2eAgInSc1d1	napaden
zvlášť	zvlášť	k6eAd1	zvlášť
agresivní	agresivní	k2eAgFnSc7d1	agresivní
formou	forma	k1gFnSc7	forma
leptospiry	leptospira	k1gFnSc2	leptospira
tzv.	tzv.	kA	tzv.
Leptospirrou	Leptospirrý	k2eAgFnSc4d1	Leptospirrý
icterohaemorhagiae	icterohaemorhagiae	k1gFnSc4	icterohaemorhagiae
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oslabeného	oslabený	k2eAgMnSc4d1	oslabený
jedince	jedinec	k1gMnSc4	jedinec
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc4	onemocnění
nabírá	nabírat	k5eAaImIp3nS	nabírat
rychlý	rychlý	k2eAgInSc1d1	rychlý
spád	spád	k1gInSc1	spád
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
u	u	k7c2	u
lehkého	lehký	k2eAgInSc2d1	lehký
průběhu	průběh	k1gInSc2	průběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
postižení	postižení	k1gNnSc2	postižení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
orgánu	orgán	k1gInSc6	orgán
<g/>
:	:	kIx,	:
jater	játra	k1gNnPc2	játra
(	(	kIx(	(
<g/>
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
(	(	kIx(	(
<g/>
nemocný	nemocný	k1gMnSc1	nemocný
přestává	přestávat	k5eAaImIp3nS	přestávat
močit	močit	k5eAaImF	močit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
odpadních	odpadní	k2eAgFnPc2d1	odpadní
látek	látka	k1gFnPc2	látka
-	-	kIx~	-
močoviny	močovina	k1gFnSc2	močovina
a	a	k8xC	a
kreatininu	kreatinin	k1gInSc2	kreatinin
-	-	kIx~	-
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
srážení	srážení	k1gNnSc2	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
krvácení	krvácení	k1gNnSc2	krvácení
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
spojivky	spojivka	k1gFnPc4	spojivka
-	-	kIx~	-
takže	takže	k8xS	takže
bělma	bělmo	k1gNnPc1	bělmo
očí	oko	k1gNnPc2	oko
jsou	být	k5eAaImIp3nP	být
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
červeně	červeň	k1gFnPc1	červeň
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sliznic	sliznice	k1gFnPc2	sliznice
-	-	kIx~	-
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
krvácení	krvácení	k1gNnSc3	krvácení
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
probíhající	probíhající	k2eAgFnSc1d1	probíhající
leptospiróza	leptospiróza	k1gFnSc1	leptospiróza
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Weilova	Weilův	k2eAgFnSc1d1	Weilova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
závažný	závažný	k2eAgInSc1d1	závažný
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
léčbu	léčba	k1gFnSc4	léčba
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nemocný	nemocný	k1gMnSc1	nemocný
přijde	přijít	k5eAaPmIp3nS	přijít
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
lékařem	lékař	k1gMnSc7	lékař
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
zmíněnými	zmíněný	k2eAgInPc7d1	zmíněný
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
onemocnění	onemocnění	k1gNnSc4	onemocnění
alespoň	alespoň	k9	alespoň
zpočátku	zpočátku	k6eAd1	zpočátku
zaměněno	zaměněn	k2eAgNnSc1d1	zaměněno
za	za	k7c4	za
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
styku	styk	k1gInSc6	styk
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
rozvoje	rozvoj	k1gInSc2	rozvoj
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
nakazí	nakazit	k5eAaPmIp3nS	nakazit
často	často	k6eAd1	často
při	při	k7c6	při
koupání	koupání	k1gNnSc1	koupání
nebo	nebo	k8xC	nebo
napití	napití	k1gNnSc1	napití
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
pomůže	pomoct	k5eAaPmIp3nS	pomoct
lékaři	lékař	k1gMnPc1	lékař
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
onemocnění	onemocnění	k1gNnSc4	onemocnění
vůbec	vůbec	k9	vůbec
pomyslet	pomyslet	k5eAaPmF	pomyslet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
chřipkou	chřipka	k1gFnSc7	chřipka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejné	stejný	k2eAgInPc1d1	stejný
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
leptospiróza	leptospiróza	k1gFnSc1	leptospiróza
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jste	být	k5eAaImIp2nP	být
tedy	tedy	k9	tedy
pobývali	pobývat	k5eAaImAgMnP	pobývat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
se	se	k3xPyFc4	se
koupali	koupat	k5eAaImAgMnP	koupat
<g/>
,	,	kIx,	,
rybařili	rybařit	k5eAaImAgMnP	rybařit
nebo	nebo	k8xC	nebo
pili	pít	k5eAaImAgMnP	pít
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
studánek	studánka	k1gFnPc2	studánka
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
nebo	nebo	k8xC	nebo
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
nezapomeňte	zapomnět	k5eNaImRp2nP	zapomnět
to	ten	k3xDgNnSc4	ten
svému	svůj	k3xOyFgMnSc3	svůj
lékaři	lékař	k1gMnPc1	lékař
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
!	!	kIx.	!
</s>
<s>
Pro	pro	k7c4	pro
průkaz	průkaz	k1gInSc4	průkaz
leptospirózy	leptospiróza	k1gFnSc2	leptospiróza
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
speciální	speciální	k2eAgInPc4d1	speciální
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
testy	test	k1gInPc4	test
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
zjištění	zjištění	k1gNnSc4	zjištění
přítomnosti	přítomnost	k1gFnSc2	přítomnost
typických	typický	k2eAgFnPc2d1	typická
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bylo	být	k5eAaImAgNnS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
prvních	první	k4xOgInPc2	první
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
leptospiry	leptospira	k1gFnPc4	leptospira
tzv.	tzv.	kA	tzv.
vykultivovat	vykultivovat	k5eAaPmF	vykultivovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
odebere	odebrat	k5eAaPmIp3nS	odebrat
vzorek	vzorek	k1gInSc1	vzorek
moči	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
nebo	nebo	k8xC	nebo
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
přenese	přenést	k5eAaPmIp3nS	přenést
do	do	k7c2	do
speciálního	speciální	k2eAgNnSc2d1	speciální
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
kultivační	kultivační	k2eAgFnSc1d1	kultivační
půda	půda	k1gFnSc1	půda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
leptospirám	leptospirat	k5eAaImIp1nS	leptospirat
dobře	dobře	k6eAd1	dobře
daří	dařit	k5eAaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
leptospiry	leptospira	k1gFnPc1	leptospira
na	na	k7c6	na
kultivační	kultivační	k2eAgFnSc6d1	kultivační
půdě	půda	k1gFnSc6	půda
pomnoží	pomnožit	k5eAaPmIp3nS	pomnožit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
(	(	kIx(	(
<g/>
lékař	lékař	k1gMnSc1	lékař
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
rozpoznáváním	rozpoznávání	k1gNnSc7	rozpoznávání
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
)	)	kIx)	)
schopen	schopen	k2eAgMnSc1d1	schopen
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaké	jaký	k3yQgInPc4	jaký
mikroby	mikrob	k1gInPc4	mikrob
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řada	řada	k1gFnSc1	řada
testů	test	k1gInPc2	test
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
informaci	informace	k1gFnSc3	informace
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
tělních	tělní	k2eAgInPc2d1	tělní
sekretů	sekret	k1gInPc2	sekret
<g/>
,	,	kIx,	,
především	především	k9	především
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
funkce	funkce	k1gFnSc1	funkce
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
postiženy	postihnout	k5eAaPmNgFnP	postihnout
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
onemocnění	onemocnění	k1gNnPc2	onemocnění
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
závažnosti	závažnost	k1gFnSc6	závažnost
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
leptospirózu	leptospiróza	k1gFnSc4	leptospiróza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
lehký	lehký	k2eAgInSc4d1	lehký
průběh	průběh	k1gInSc4	průběh
<g/>
,	,	kIx,	,
léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
symptomaticky	symptomaticky	k6eAd1	symptomaticky
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemocnému	nemocný	k1gMnSc3	nemocný
podají	podat	k5eAaPmIp3nP	podat
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ho	on	k3xPp3gInSc4	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
zbavit	zbavit	k5eAaPmF	zbavit
nepříjemných	příjemný	k2eNgInPc2d1	nepříjemný
a	a	k8xC	a
odstranitelných	odstranitelný	k2eAgInPc2d1	odstranitelný
příznaků	příznak	k1gInPc2	příznak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
horečky	horečka	k1gFnSc2	horečka
a	a	k8xC	a
úlevy	úleva	k1gFnSc2	úleva
od	od	k7c2	od
zimnice	zimnice	k1gFnSc2	zimnice
a	a	k8xC	a
třesavky	třesavka	k1gFnSc2	třesavka
se	se	k3xPyFc4	se
nasadí	nasadit	k5eAaPmIp3nS	nasadit
antipyretika	antipyretikum	k1gNnSc2	antipyretikum
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
analgetika	analgetikum	k1gNnSc2	analgetikum
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
podají	podat	k5eAaPmIp3nP	podat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
tabletové	tabletový	k2eAgFnSc6d1	tabletová
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	nemocný	k1gMnSc1	nemocný
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nS	léčit
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
doporučen	doporučit	k5eAaPmNgInS	doporučit
klid	klid	k1gInSc1	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
,	,	kIx,	,
přijímání	přijímání	k1gNnSc4	přijímání
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
nemoc	nemoc	k1gFnSc1	nemoc
probíhá	probíhat	k5eAaImIp3nS	probíhat
těžce	těžce	k6eAd1	těžce
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
příznaků	příznak	k1gInPc2	příznak
z	z	k7c2	z
poškození	poškození	k1gNnSc2	poškození
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
žloutenka	žloutenka	k1gFnSc1	žloutenka
<g/>
,	,	kIx,	,
nemocný	nemocný	k2eAgMnSc1d1	nemocný
přestává	přestávat	k5eAaImIp3nS	přestávat
močit	močit	k5eAaImF	močit
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
úroveň	úroveň	k1gFnSc1	úroveň
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytně	nezbytně	k6eAd1	nezbytně
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
přijat	přijmout	k5eAaPmNgInS	přijmout
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
podávány	podáván	k2eAgFnPc4d1	podávána
jednak	jednak	k8xC	jednak
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
,	,	kIx,	,
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
kauzální	kauzální	k2eAgFnSc1d1	kauzální
terapie	terapie	k1gFnSc1	terapie
(	(	kIx(	(
<g/>
léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
příčinu	příčina	k1gFnSc4	příčina
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
infekci	infekce	k1gFnSc4	infekce
leptospirou	leptospirý	k2eAgFnSc4d1	leptospirý
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
řady	řada	k1gFnSc2	řada
léků	lék	k1gInPc2	lék
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
i	i	k8xC	i
přístrojů	přístroj	k1gInPc2	přístroj
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
podpůrné	podpůrný	k2eAgFnSc6d1	podpůrná
terapii	terapie	k1gFnSc6	terapie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
dobré	dobrý	k2eAgFnSc2d1	dobrá
funkce	funkce	k1gFnSc2	funkce
poškozených	poškozený	k2eAgInPc2d1	poškozený
orgánů	orgán	k1gInPc2	orgán
např.	např.	kA	např.
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
ledvin	ledvina	k1gFnPc2	ledvina
dialýza	dialýza	k1gFnSc1	dialýza
<g/>
,	,	kIx,	,
při	při	k7c6	při
krvácivých	krvácivý	k2eAgInPc6d1	krvácivý
projevech	projev	k1gInPc6	projev
podání	podání	k1gNnSc2	podání
krevních	krevní	k2eAgFnPc2d1	krevní
náhrad	náhrada	k1gFnPc2	náhrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
selhávání	selhávání	k1gNnSc2	selhávání
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
umělá	umělý	k2eAgFnSc1d1	umělá
plicní	plicní	k2eAgFnSc1d1	plicní
ventilace	ventilace	k1gFnSc1	ventilace
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemocný	nemocný	k2eAgMnSc1d1	nemocný
kritické	kritický	k2eAgNnSc4d1	kritické
období	období	k1gNnSc4	období
překoná	překonat	k5eAaPmIp3nS	překonat
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
poškozených	poškozený	k2eAgInPc2d1	poškozený
orgánů	orgán	k1gInPc2	orgán
se	se	k3xPyFc4	se
obnoví	obnovit	k5eAaPmIp3nS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napomoci	napomoct	k5eAaPmF	napomoct
léčbě	léčba	k1gFnSc3	léčba
leptospirózy	leptospiróza	k1gFnSc2	leptospiróza
Jestliže	jestliže	k8xS	jestliže
jste	být	k5eAaImIp2nP	být
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
leptospirózou	leptospiróza	k1gFnSc7	leptospiróza
<g/>
,	,	kIx,	,
kolikrát	kolikrát	k6eAd1	kolikrát
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
ani	ani	k9	ani
nemusíte	muset	k5eNaImIp2nP	muset
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
onemocnění	onemocnění	k1gNnSc1	onemocnění
probíhá	probíhat	k5eAaImIp3nS	probíhat
mírně	mírně	k6eAd1	mírně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gInSc4	on
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
chřipkou	chřipka	k1gFnSc7	chřipka
a	a	k8xC	a
tak	tak	k9	tak
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
i	i	k9	i
přistupovat	přistupovat	k5eAaImF	přistupovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
horší	horšit	k5eAaImIp3nS	horšit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezbytně	nezbytně	k6eAd1	nezbytně
nutné	nutný	k2eAgNnSc1d1	nutné
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
řádně	řádně	k6eAd1	řádně
ho	on	k3xPp3gMnSc4	on
informovat	informovat	k5eAaBmF	informovat
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
příznacích	příznak	k1gInPc6	příznak
a	a	k8xC	a
možných	možný	k2eAgFnPc6d1	možná
souvislostech	souvislost	k1gFnPc6	souvislost
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
pomyslet	pomyslet	k5eAaPmF	pomyslet
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
diagnózu	diagnóza	k1gFnSc4	diagnóza
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
léčbu	léčba	k1gFnSc4	léčba
přiměřenou	přiměřený	k2eAgFnSc4d1	přiměřená
závažnosti	závažnost	k1gFnSc2	závažnost
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
lehce	lehko	k6eAd1	lehko
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
nezanechá	zanechat	k5eNaPmIp3nS	zanechat
vůbec	vůbec	k9	vůbec
žádné	žádný	k3yNgInPc1	žádný
následky	následek	k1gInPc1	následek
a	a	k8xC	a
nemocný	nemocný	k1gMnSc1	nemocný
je	být	k5eAaImIp3nS	být
brzy	brzy	k6eAd1	brzy
"	"	kIx"	"
<g/>
fit	fit	k2eAgFnSc1d1	fit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc1d2	delší
doba	doba	k1gFnSc1	doba
zotavování	zotavování	k1gNnSc2	zotavování
(	(	kIx(	(
<g/>
rekonvalescence	rekonvalescence	k1gFnSc1	rekonvalescence
<g/>
)	)	kIx)	)
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
těžkém	těžký	k2eAgInSc6d1	těžký
průběhu	průběh	k1gInSc6	průběh
leptospirózy	leptospiróza	k1gFnSc2	leptospiróza
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
bakterie	bakterie	k1gFnSc1	bakterie
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
nasazení	nasazení	k1gNnSc6	nasazení
antibiotické	antibiotický	k2eAgFnSc2d1	antibiotická
léčby	léčba	k1gFnSc2	léčba
rychle	rychle	k6eAd1	rychle
vymýcena	vymýcen	k2eAgFnSc1d1	vymýcena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uzdravení	uzdravení	k1gNnSc3	uzdravení
všech	všecek	k3xTgInPc2	všecek
postižených	postižený	k2eAgInPc2d1	postižený
orgánů	orgán	k1gInPc2	orgán
-	-	kIx~	-
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gNnPc4	on
onemocnění	onemocnění	k1gNnPc4	onemocnění
opravdu	opravdu	k6eAd1	opravdu
těžké	těžký	k2eAgFnPc1d1	těžká
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
funkce	funkce	k1gFnSc1	funkce
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemocní	nemocný	k1gMnPc1	nemocný
trpí	trpět	k5eAaImIp3nP	trpět
častými	častý	k2eAgFnPc7d1	častá
změnami	změna	k1gFnPc7	změna
nálad	nálada	k1gFnPc2	nálada
a	a	k8xC	a
depresí	deprese	k1gFnPc2	deprese
<g/>
.	.	kIx.	.
</s>
