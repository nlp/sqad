<s>
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1875	[number]	k4	1875
Kesswil	Kesswila	k1gFnPc2	Kesswila
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
Küsnacht	Küsnachta	k1gFnPc2	Küsnachta
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
psychoterapeut	psychoterapeut	k1gMnSc1	psychoterapeut
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
analytické	analytický	k2eAgFnSc2d1	analytická
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
přínos	přínos	k1gInSc1	přínos
psychologii	psychologie	k1gFnSc4	psychologie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
pochopení	pochopení	k1gNnSc4	pochopení
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
světa	svět	k1gInSc2	svět
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
zkoumání	zkoumání	k1gNnSc4	zkoumání
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
léčbě	léčba	k1gFnSc3	léčba
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
protestantského	protestantský	k2eAgMnSc2d1	protestantský
faráře	farář	k1gMnSc2	farář
prožíval	prožívat	k5eAaImAgMnS	prožívat
všechny	všechen	k3xTgFnPc4	všechen
otcovy	otcův	k2eAgFnPc4d1	otcova
pochybnosti	pochybnost	k1gFnPc4	pochybnost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgNnSc1d1	citlivé
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
často	často	k6eAd1	často
oddával	oddávat	k5eAaImAgMnS	oddávat
osobní	osobní	k2eAgFnSc4d1	osobní
imaginaci	imaginace	k1gFnSc4	imaginace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
navázat	navázat	k5eAaPmF	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikému	veliký	k2eAgNnSc3d1	veliké
množství	množství	k1gNnSc3	množství
duchovních	duchovní	k2eAgMnPc2d1	duchovní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
půjde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
otcových	otcův	k2eAgFnPc6d1	otcova
stopách	stopa	k1gFnPc6	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
dospělosti	dospělost	k1gFnSc6	dospělost
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
rodinné	rodinný	k2eAgFnSc2d1	rodinná
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
zapsat	zapsat	k5eAaPmF	zapsat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Curychu	Curych	k1gInSc2	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Curyšská	curyšský	k2eAgFnSc1d1	curyšská
univerzita	univerzita	k1gFnSc1	univerzita
měla	mít	k5eAaImAgFnS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
klinickou	klinický	k2eAgFnSc4d1	klinická
bázi	báze	k1gFnSc4	báze
v	v	k7c6	v
Burghölzli	Burghölzli	k1gFnSc6	Burghölzli
a	a	k8xC	a
Jung	Jung	k1gMnSc1	Jung
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
lékařem	lékař	k1gMnSc7	lékař
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
vedl	vést	k5eAaImAgInS	vést
Eugen	Eugen	k2eAgInSc1d1	Eugen
Bleuler	Bleuler	k1gInSc1	Bleuler
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
spíše	spíše	k9	spíše
tradičním	tradiční	k2eAgNnSc7d1	tradiční
popisným	popisný	k2eAgNnSc7d1	popisné
zkoumáním	zkoumání	k1gNnSc7	zkoumání
duševních	duševní	k2eAgFnPc2d1	duševní
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
k	k	k7c3	k
filosofické	filosofický	k2eAgFnSc3d1	filosofická
psychologii	psychologie	k1gFnSc3	psychologie
se	se	k3xPyFc4	se
Jung	Jung	k1gMnSc1	Jung
zabýval	zabývat	k5eAaImAgMnS	zabývat
experimentální	experimentální	k2eAgFnSc7d1	experimentální
psychologií	psychologie	k1gFnSc7	psychologie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
zákony	zákon	k1gInPc4	zákon
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
platností	platnost	k1gFnSc7	platnost
jako	jako	k8xS	jako
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Franzem	Franz	k1gMnSc7	Franz
Riklinem	Riklin	k1gInSc7	Riklin
významně	významně	k6eAd1	významně
přepracovali	přepracovat	k5eAaPmAgMnP	přepracovat
Bleulerem	Bleuler	k1gInSc7	Bleuler
používaný	používaný	k2eAgInSc4d1	používaný
asociační	asociační	k2eAgInSc4d1	asociační
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
škálu	škála	k1gFnSc4	škála
podnětných	podnětný	k2eAgNnPc2d1	podnětné
slov	slovo	k1gNnPc2	slovo
i	i	k8xC	i
asociačních	asociační	k2eAgInPc2d1	asociační
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Asociační	asociační	k2eAgInSc1d1	asociační
test	test	k1gInSc1	test
tak	tak	k9	tak
pro	pro	k7c4	pro
Junga	Jung	k1gMnSc4	Jung
představoval	představovat	k5eAaImAgMnS	představovat
bránu	brána	k1gFnSc4	brána
k	k	k7c3	k
obsahům	obsah	k1gInPc3	obsah
nevědomých	vědomý	k2eNgInPc2d1	nevědomý
komplexů	komplex	k1gInPc2	komplex
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
psychice	psychika	k1gFnSc6	psychika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fabulacích	fabulace	k1gFnPc6	fabulace
duševně	duševně	k6eAd1	duševně
nemocných	nemocný	k1gMnPc2	nemocný
nacházel	nacházet	k5eAaImAgInS	nacházet
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
mýty	mýtus	k1gInPc7	mýtus
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
shodu	shoda	k1gFnSc4	shoda
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
společné	společný	k2eAgFnSc2d1	společná
základny	základna	k1gFnSc2	základna
individuální	individuální	k2eAgFnSc2d1	individuální
imaginace	imaginace	k1gFnSc2	imaginace
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
celé	celý	k2eAgFnSc2d1	celá
lidské	lidský	k2eAgFnSc2d1	lidská
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
mytologii	mytologie	k1gFnSc4	mytologie
a	a	k8xC	a
sbíral	sbírat	k5eAaImAgInS	sbírat
materiál	materiál	k1gInSc1	materiál
přeludů	přelud	k1gInPc2	přelud
<g/>
,	,	kIx,	,
halucinací	halucinace	k1gFnPc2	halucinace
a	a	k8xC	a
snů	sen	k1gInPc2	sen
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdál	zdát	k5eAaImAgInS	zdát
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
relevantní	relevantní	k2eAgFnSc1d1	relevantní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
žáka	žák	k1gMnSc2	žák
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spolupráci	spolupráce	k1gFnSc4	spolupráce
Junga	Jung	k1gMnSc2	Jung
s	s	k7c7	s
Freudem	Freud	k1gInSc7	Freud
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
především	především	k9	především
důvěra	důvěra	k1gFnSc1	důvěra
a	a	k8xC	a
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Freudem	Freud	k1gInSc7	Freud
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Freuda	Freud	k1gMnSc4	Freud
znamenal	znamenat	k5eAaImAgMnS	znamenat
Jung	Jung	k1gMnSc1	Jung
nejinspirativnější	inspirativní	k2eAgInSc4d3	nejinspirativnější
prvek	prvek	k1gInSc4	prvek
svého	svůj	k3xOyFgNnSc2	svůj
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
se	se	k3xPyFc4	se
shodovali	shodovat	k5eAaImAgMnP	shodovat
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hysterie	hysterie	k1gFnSc2	hysterie
<g/>
.	.	kIx.	.
</s>
<s>
Freudovská	freudovský	k2eAgFnSc1d1	freudovská
psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
na	na	k7c4	na
sexuální	sexuální	k2eAgInSc4d1	sexuální
původ	původ	k1gInSc4	původ
neurózy	neuróza	k1gFnSc2	neuróza
<g/>
,	,	kIx,	,
však	však	k9	však
připadala	připadat	k5eAaImAgFnS	připadat
Jungovi	Jung	k1gMnSc6	Jung
příliš	příliš	k6eAd1	příliš
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
trajektorií	trajektorie	k1gFnSc7	trajektorie
<g/>
"	"	kIx"	"
neurózy	neuróza	k1gFnPc1	neuróza
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
implikacemi	implikace	k1gFnPc7	implikace
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
pojetí	pojetí	k1gNnSc1	pojetí
komplexu	komplex	k1gInSc2	komplex
jako	jako	k8xS	jako
jakéhosi	jakýsi	k3yIgNnSc2	jakýsi
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
rezidua	reziduum	k1gNnSc2	reziduum
z	z	k7c2	z
psychických	psychický	k2eAgInPc2d1	psychický
prožitků	prožitek	k1gInPc2	prožitek
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
mu	on	k3xPp3gMnSc3	on
připadalo	připadat	k5eAaImAgNnS	připadat
silně	silně	k6eAd1	silně
omezující	omezující	k2eAgNnSc1d1	omezující
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
fantazií	fantazie	k1gFnPc2	fantazie
americké	americký	k2eAgFnSc2d1	americká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Frank	frank	k1gInSc4	frank
Millerové	Millerová	k1gFnSc2	Millerová
jej	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgInS	přivést
k	k	k7c3	k
podvědomým	podvědomý	k2eAgFnPc3d1	podvědomá
tvořivým	tvořivý	k2eAgFnPc3d1	tvořivá
imaginacím	imaginace	k1gFnPc3	imaginace
<g/>
,	,	kIx,	,
mytologickým	mytologický	k2eAgInPc3d1	mytologický
snům	sen	k1gInPc3	sen
a	a	k8xC	a
fantaziím	fantazie	k1gFnPc3	fantazie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
veskrze	veskrze	k6eAd1	veskrze
neosobní	osobní	k2eNgInSc4d1	neosobní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Subjekt	subjekt	k1gInSc1	subjekt
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
archaické	archaický	k2eAgFnSc3d1	archaická
mimoslovní	mimoslovní	k2eAgFnSc3d1	mimoslovní
formě	forma	k1gFnSc3	forma
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
Freuda	Freud	k1gMnSc4	Freud
byl	být	k5eAaImAgInS	být
symbolismus	symbolismus	k1gInSc1	symbolismus
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
důsledkem	důsledek	k1gInSc7	důsledek
historického	historický	k2eAgInSc2d1	historický
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
Jung	Jung	k1gMnSc1	Jung
kladl	klást	k5eAaImAgMnS	klást
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
faktory	faktor	k1gInPc4	faktor
minimální	minimální	k2eAgInSc4d1	minimální
důraz	důraz	k1gInSc4	důraz
a	a	k8xC	a
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
neosobní	osobní	k2eNgFnSc4d1	neosobní
vrstvu	vrstva	k1gFnSc4	vrstva
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
nevědomí	nevědomí	k1gNnSc4	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
společné	společný	k2eAgFnPc1d1	společná
cesty	cesta	k1gFnPc1	cesta
rozcházet	rozcházet	k5eAaImF	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
vydal	vydat	k5eAaPmAgMnS	vydat
Jung	Jung	k1gMnSc1	Jung
studii	studie	k1gFnSc6	studie
Symboly	symbol	k1gInPc1	symbol
proměny	proměna	k1gFnSc2	proměna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Freudův	Freudův	k2eAgInSc1d1	Freudův
přílišný	přílišný	k2eAgInSc1d1	přílišný
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
sexualitu	sexualita	k1gFnSc4	sexualita
a	a	k8xC	a
na	na	k7c6	na
příkladě	příklad	k1gInSc6	příklad
symbolismu	symbolismus	k1gInSc2	symbolismus
vizí	vize	k1gFnPc2	vize
Frank	Frank	k1gMnSc1	Frank
Millerové	Millerová	k1gFnSc2	Millerová
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
pojetí	pojetí	k1gNnSc4	pojetí
libida	libido	k1gNnSc2	libido
jako	jako	k8xC	jako
obecné	obecný	k2eAgFnSc2d1	obecná
psychické	psychický	k2eAgFnSc2d1	psychická
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
energie	energie	k1gFnSc2	energie
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přijmout	přijmout	k5eAaPmF	přijmout
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
neshodě	neshoda	k1gFnSc6	neshoda
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
už	už	k6eAd1	už
mezitím	mezitím	k6eAd1	mezitím
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
dogma	dogma	k1gNnSc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
Freuda	Freud	k1gMnSc2	Freud
a	a	k8xC	a
Junga	Jung	k1gMnSc2	Jung
neměl	mít	k5eNaImAgInS	mít
jen	jen	k9	jen
vědecké	vědecký	k2eAgInPc4d1	vědecký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
osobní	osobní	k2eAgNnSc4d1	osobní
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Freud	Freud	k1gInSc1	Freud
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Junga	Jung	k1gMnSc4	Jung
adoptovat	adoptovat	k5eAaPmF	adoptovat
a	a	k8xC	a
"	"	kIx"	"
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
patriarchů	patriarcha	k1gMnPc2	patriarcha
jej	on	k3xPp3gMnSc4	on
pomazat	pomazat	k5eAaPmF	pomazat
in	in	k?	in
partibus	partibus	k1gMnSc1	partibus
infidelium	infidelium	k1gNnSc1	infidelium
–	–	k?	–
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nevěřících	nevěřící	k1gFnPc2	nevěřící
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
později	pozdě	k6eAd2	pozdě
založilo	založit	k5eAaPmAgNnS	založit
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
otcovské	otcovský	k2eAgFnSc2d1	otcovská
autority	autorita	k1gFnSc2	autorita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
vztah	vztah	k1gInSc1	vztah
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Junga	Jung	k1gMnSc4	Jung
zlomový	zlomový	k2eAgInSc1d1	zlomový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
apokalyptických	apokalyptický	k2eAgInPc2d1	apokalyptický
snů	sen	k1gInPc2	sen
a	a	k8xC	a
vizí	vize	k1gFnSc7	vize
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
přednášky	přednáška	k1gFnPc4	přednáška
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
psychoanalytické	psychoanalytický	k2eAgFnSc2d1	psychoanalytická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
převážně	převážně	k6eAd1	převážně
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
teoretických	teoretický	k2eAgFnPc6d1	teoretická
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
úzký	úzký	k2eAgInSc4d1	úzký
kroužek	kroužek	k1gInSc4	kroužek
Psychologického	psychologický	k2eAgInSc2d1	psychologický
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
soukromé	soukromý	k2eAgFnSc3d1	soukromá
terapeutické	terapeutický	k2eAgFnSc3d1	terapeutická
praxi	praxe	k1gFnSc3	praxe
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
především	především	k6eAd1	především
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
svoji	svůj	k3xOyFgFnSc4	svůj
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
promlouvat	promlouvat	k5eAaImF	promlouvat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
nevědomím	nevědomí	k1gNnSc7	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
tak	tak	k9	tak
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
rané	raný	k2eAgInPc4d1	raný
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
dětské	dětský	k2eAgFnSc2d1	dětská
imaginace	imaginace	k1gFnSc2	imaginace
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
personifikovat	personifikovat	k5eAaBmF	personifikovat
fenomény	fenomén	k1gInPc4	fenomén
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
,	,	kIx,	,
naslouchat	naslouchat	k5eAaImF	naslouchat
jejich	jejich	k3xOp3gInSc2	jejich
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
vydělovat	vydělovat	k5eAaImF	vydělovat
z	z	k7c2	z
bezbřehosti	bezbřehost	k1gFnSc2	bezbřehost
a	a	k8xC	a
činit	činit	k5eAaImF	činit
je	on	k3xPp3gInPc4	on
uchopitelnými	uchopitelný	k2eAgFnPc7d1	uchopitelná
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
postulátu	postulát	k1gInSc3	postulát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
individuální	individuální	k2eAgFnSc6d1	individuální
mysli	mysl	k1gFnSc6	mysl
existují	existovat	k5eAaImIp3nP	existovat
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jí	on	k3xPp3gFnSc7	on
nepatří	patřit	k5eNaImIp3nP	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
popsat	popsat	k5eAaPmF	popsat
nějakou	nějaký	k3yIgFnSc4	nějaký
praktickou	praktický	k2eAgFnSc4d1	praktická
metodiku	metodika	k1gFnSc4	metodika
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
nevědomím	nevědomí	k1gNnSc7	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboženské	náboženský	k2eAgFnPc1d1	náboženská
filosofie	filosofie	k1gFnPc1	filosofie
<g/>
,	,	kIx,	,
metafyzické	metafyzický	k2eAgInPc1d1	metafyzický
systémy	systém	k1gInPc1	systém
nebo	nebo	k8xC	nebo
symbolická	symbolický	k2eAgFnSc1d1	symbolická
imaginace	imaginace	k1gFnSc1	imaginace
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
faktem	fakt	k1gInSc7	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Psychologické	psychologický	k2eAgFnPc1d1	psychologická
výpovědi	výpověď	k1gFnPc1	výpověď
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
stejnou	stejný	k2eAgFnSc4d1	stejná
váhu	váha	k1gFnSc4	váha
jako	jako	k8xC	jako
výpovědi	výpověď	k1gFnPc4	výpověď
vědeckého	vědecký	k2eAgInSc2d1	vědecký
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgFnPc7	jaký
disponuje	disponovat	k5eAaBmIp3nS	disponovat
například	například	k6eAd1	například
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
zavedl	zavést	k5eAaPmAgInS	zavést
pojem	pojem	k1gInSc1	pojem
psychické	psychický	k2eAgFnSc2d1	psychická
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
moderní	moderní	k2eAgFnSc4d1	moderní
osvícené	osvícený	k2eAgNnSc1d1	osvícené
pojetí	pojetí	k1gNnSc1	pojetí
člověka	člověk	k1gMnSc2	člověk
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
mimo	mimo	k7c4	mimo
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
empirickou	empirický	k2eAgFnSc4d1	empirická
povahu	povaha	k1gFnSc4	povaha
duševních	duševní	k2eAgFnPc2d1	duševní
výpovědí	výpověď	k1gFnPc2	výpověď
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
zároveň	zároveň	k6eAd1	zároveň
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
moderní	moderní	k2eAgInSc4d1	moderní
vědecký	vědecký	k2eAgInSc4d1	vědecký
názor	názor	k1gInSc4	názor
nebude	být	k5eNaImBp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
plynoucí	plynoucí	k2eAgNnSc4d1	plynoucí
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
psychického	psychický	k2eAgInSc2d1	psychický
světa	svět	k1gInSc2	svět
od	od	k7c2	od
racionalisticky	racionalisticky	k6eAd1	racionalisticky
chápané	chápaný	k2eAgFnSc2d1	chápaná
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
předtuchu	předtucha	k1gFnSc4	předtucha
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
vlivu	vliv	k1gInSc2	vliv
nezpracovaných	zpracovaný	k2eNgInPc2d1	nezpracovaný
nevědomých	vědomý	k2eNgInPc2d1	nevědomý
archetypů	archetyp	k1gInPc2	archetyp
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
splnila	splnit	k5eAaPmAgFnS	splnit
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
úvahách	úvaha	k1gFnPc6	úvaha
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Jung	Jung	k1gMnSc1	Jung
k	k	k7c3	k
podstatnému	podstatný	k2eAgNnSc3d1	podstatné
zjištění	zjištění	k1gNnSc3	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědomí	vědomí	k1gNnSc1	vědomí
a	a	k8xC	a
nevědomí	nevědomí	k1gNnSc1	nevědomí
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
chovají	chovat	k5eAaImIp3nP	chovat
komplementárně	komplementárně	k6eAd1	komplementárně
a	a	k8xC	a
kompenzačně	kompenzačně	k6eAd1	kompenzačně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
matematické	matematický	k2eAgFnSc2d1	matematická
vědy	věda	k1gFnSc2	věda
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pojem	pojem	k1gInSc1	pojem
transcendentní	transcendentní	k2eAgFnSc2d1	transcendentní
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
faktor	faktor	k1gInSc1	faktor
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
faktor	faktor	k1gInSc1	faktor
nevědomí	nevědomí	k1gNnPc2	nevědomí
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
psychickou	psychický	k2eAgFnSc4d1	psychická
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
určuje	určovat	k5eAaImIp3nS	určovat
poměr	poměr	k1gInSc1	poměr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
komponent	komponenta	k1gFnPc2	komponenta
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
varianty	variant	k1gInPc1	variant
vztahu	vztah	k1gInSc2	vztah
vědomých	vědomý	k2eAgInPc2d1	vědomý
a	a	k8xC	a
nevědomých	vědomý	k2eNgInPc2d1	nevědomý
faktorů	faktor	k1gInPc2	faktor
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
duševního	duševní	k2eAgNnSc2d1	duševní
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
koncepci	koncepce	k1gFnSc4	koncepce
psychologických	psychologický	k2eAgInPc2d1	psychologický
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
dvojí	dvojí	k4xRgFnSc4	dvojí
kategorii	kategorie	k1gFnSc4	kategorie
povahy	povaha	k1gFnSc2	povaha
podle	podle	k7c2	podle
zaměření	zaměření	k1gNnSc2	zaměření
libida	libido	k1gNnSc2	libido
<g/>
,	,	kIx,	,
introvertní	introvertní	k2eAgFnSc1d1	introvertní
(	(	kIx(	(
<g/>
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
nitra	nitro	k1gNnSc2	nitro
<g/>
)	)	kIx)	)
a	a	k8xC	a
extrovertní	extrovertní	k2eAgFnSc1d1	extrovertní
(	(	kIx(	(
<g/>
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
vnější	vnější	k2eAgInSc4d1	vnější
svět	svět	k1gInSc4	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
klasifikaci	klasifikace	k1gFnSc4	klasifikace
doplnil	doplnit	k5eAaPmAgMnS	doplnit
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
(	(	kIx(	(
<g/>
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
cit	cit	k1gInSc1	cit
<g/>
,	,	kIx,	,
vnímání	vnímání	k1gNnSc1	vnímání
<g/>
,	,	kIx,	,
intuice	intuice	k1gFnSc1	intuice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c4	o
diferenciaci	diferenciace	k1gFnSc4	diferenciace
(	(	kIx(	(
<g/>
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
)	)	kIx)	)
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejméně	málo	k6eAd3	málo
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
psychická	psychický	k2eAgFnSc1d1	psychická
funkce	funkce	k1gFnSc1	funkce
patří	patřit	k5eAaImIp3nS	patřit
u	u	k7c2	u
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
osobního	osobní	k2eAgNnSc2d1	osobní
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Jung	Jung	k1gMnSc1	Jung
stavbu	stavba	k1gFnSc4	stavba
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Bollingenu	Bollingen	k1gInSc6	Bollingen
<g/>
,	,	kIx,	,
obydlí	obydlí	k1gNnSc6	obydlí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
živoucím	živoucí	k2eAgInSc7d1	živoucí
důkazem	důkaz	k1gInSc7	důkaz
jeho	on	k3xPp3gInSc2	on
dalšího	další	k2eAgInSc2d1	další
objevu	objev	k1gInSc2	objev
<g/>
,	,	kIx,	,
psychického	psychický	k2eAgInSc2d1	psychický
středu	střed	k1gInSc2	střed
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
duši	duše	k1gFnSc6	duše
–	–	k?	–
Self	Self	k1gInSc1	Self
neboli	neboli	k8xC	neboli
bytostného	bytostný	k2eAgInSc2d1	bytostný
Já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
Bollingenská	Bollingenský	k2eAgFnSc1d1	Bollingenský
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
psycholog	psycholog	k1gMnSc1	psycholog
neustále	neustále	k6eAd1	neustále
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
jen	jen	k9	jen
místem	místo	k1gNnSc7	místo
setkávání	setkávání	k1gNnSc2	setkávání
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
symbolickým	symbolický	k2eAgNnSc7d1	symbolické
vyjádřením	vyjádření	k1gNnSc7	vyjádření
osobního	osobní	k2eAgInSc2d1	osobní
růstu	růst	k1gInSc2	růst
k	k	k7c3	k
celistvosti	celistvost	k1gFnSc3	celistvost
kombinací	kombinace	k1gFnPc2	kombinace
praktické	praktický	k2eAgFnSc2d1	praktická
životní	životní	k2eAgFnSc2d1	životní
orientace	orientace	k1gFnSc2	orientace
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
snové	snový	k2eAgFnSc2d1	snová
imaginativní	imaginativní	k2eAgFnSc2d1	imaginativní
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
svého	svůj	k3xOyFgNnSc2	svůj
zjištění	zjištění	k1gNnSc2	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
projekcí	projekce	k1gFnSc7	projekce
nevědomých	vědomý	k2eNgInPc2d1	nevědomý
obsahů	obsah	k1gInPc2	obsah
do	do	k7c2	do
reality	realita	k1gFnSc2	realita
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
obsahy	obsah	k1gInPc1	obsah
viditelné	viditelný	k2eAgInPc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
tímto	tento	k3xDgInSc7	tento
překračovala	překračovat	k5eAaImAgFnS	překračovat
svůj	svůj	k3xOyFgInSc4	svůj
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgMnS	začít
Jung	Jung	k1gMnSc1	Jung
studia	studio	k1gNnSc2	studio
alchymistického	alchymistický	k2eAgInSc2d1	alchymistický
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
,	,	kIx,	,
obracel	obracet	k5eAaImAgMnS	obracet
se	se	k3xPyFc4	se
ke	k	k7c3	k
starým	starý	k2eAgMnPc3d1	starý
filosofickým	filosofický	k2eAgMnPc3d1	filosofický
mistrům	mistr	k1gMnPc3	mistr
(	(	kIx(	(
<g/>
Mistr	mistr	k1gMnSc1	mistr
Eckhart	Eckharta	k1gFnPc2	Eckharta
<g/>
,	,	kIx,	,
Paracelsus	Paracelsus	k1gInSc1	Paracelsus
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Augustin	Augustin	k1gMnSc1	Augustin
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
křesťanství	křesťanství	k1gNnPc4	křesťanství
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc1	jeho
heretická	heretický	k2eAgNnPc1d1	heretické
hnutí	hnutí	k1gNnPc1	hnutí
a	a	k8xC	a
symbolismus	symbolismus	k1gInSc1	symbolismus
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
objevení	objevení	k1gNnSc4	objevení
gnostických	gnostický	k2eAgInPc2d1	gnostický
svitků	svitek	k1gInPc2	svitek
z	z	k7c2	z
egyptského	egyptský	k2eAgInSc2d1	egyptský
Nag	Nag	k1gFnSc7	Nag
Hammádí	Hammádí	k1gNnPc2	Hammádí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
religionistického	religionistický	k2eAgInSc2d1	religionistický
spolku	spolek	k1gInSc2	spolek
Eranos	Eranosa	k1gFnPc2	Eranosa
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Mirceou	Mircea	k1gFnSc7	Mircea
Eliadem	Eliad	k1gInSc7	Eliad
<g/>
,	,	kIx,	,
Károlem	Károl	k1gInSc7	Károl
Kerényim	Kerényima	k1gFnPc2	Kerényima
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
interpretaci	interpretace	k1gFnSc4	interpretace
Tajemství	tajemství	k1gNnSc2	tajemství
zlatého	zlatý	k2eAgInSc2d1	zlatý
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
esoterického	esoterický	k2eAgInSc2d1	esoterický
textu	text	k1gInSc2	text
čínského	čínský	k2eAgInSc2d1	čínský
taoismu	taoismus	k1gInSc2	taoismus
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
předmluvu	předmluva	k1gFnSc4	předmluva
k	k	k7c3	k
čínské	čínský	k2eAgFnSc3d1	čínská
knize	kniha	k1gFnSc3	kniha
proměn	proměna	k1gFnPc2	proměna
I-ťing	I-ťing	k1gInSc1	I-ťing
<g/>
.	.	kIx.	.
</s>
<s>
Veškerou	veškerý	k3xTgFnSc4	veškerý
získanou	získaný	k2eAgFnSc4d1	získaná
látku	látka	k1gFnSc4	látka
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
trvale	trvale	k6eAd1	trvale
konfrontoval	konfrontovat	k5eAaBmAgInS	konfrontovat
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
praxí	praxe	k1gFnSc7	praxe
psychiatra	psychiatr	k1gMnSc2	psychiatr
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
spektrum	spektrum	k1gNnSc1	spektrum
klientely	klientela	k1gFnSc2	klientela
se	se	k3xPyFc4	se
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
od	od	k7c2	od
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
pacientů	pacient	k1gMnPc2	pacient
psychiatrického	psychiatrický	k2eAgInSc2d1	psychiatrický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Küsnachtu	Küsnacht	k1gInSc6	Küsnacht
až	až	k9	až
po	po	k7c4	po
známé	známý	k2eAgFnPc4d1	známá
i	i	k8xC	i
zámožné	zámožný	k2eAgFnPc4d1	zámožná
osobnosti	osobnost	k1gFnPc4	osobnost
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
Jamese	Jamese	k1gFnSc1	Jamese
Joyce	Joyce	k1gFnSc1	Joyce
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Hermann	Hermann	k1gMnSc1	Hermann
Hesse	Hesse	k1gFnSc1	Hesse
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Johna	John	k1gMnSc2	John
D.	D.	kA	D.
Rockefellera	Rockefeller	k1gMnSc2	Rockefeller
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pořádal	pořádat	k5eAaImAgMnS	pořádat
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
články	článek	k1gInPc4	článek
do	do	k7c2	do
odborných	odborný	k2eAgInPc2d1	odborný
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
myšlení	myšlení	k1gNnSc3	myšlení
primitivních	primitivní	k2eAgMnPc2d1	primitivní
afrických	africký	k2eAgMnPc2d1	africký
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Basilejské	basilejský	k2eAgFnSc6d1	Basilejská
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
stal	stát	k5eAaPmAgMnS	stát
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
lékařské	lékařský	k2eAgFnSc2d1	lékařská
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
přerušil	přerušit	k5eAaPmAgMnS	přerušit
svou	svůj	k3xOyFgFnSc4	svůj
psychiatrickou	psychiatrický	k2eAgFnSc4d1	psychiatrická
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
nadmíru	nadmíra	k1gFnSc4	nadmíra
široký	široký	k2eAgInSc4d1	široký
záběr	záběr	k1gInSc4	záběr
vědomostí	vědomost	k1gFnPc2	vědomost
mu	on	k3xPp3gMnSc3	on
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
se	se	k3xPyFc4	se
fundovaně	fundovaně	k6eAd1	fundovaně
k	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
dění	dění	k1gNnSc3	dění
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
ke	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
textům	text	k1gInPc3	text
a	a	k8xC	a
přepracovával	přepracovávat	k5eAaImAgInS	přepracovávat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
nově	nově	k6eAd1	nově
nabyté	nabytý	k2eAgFnPc4d1	nabytá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
religiózními	religiózní	k2eAgInPc7d1	religiózní
a	a	k8xC	a
esoterními	esoterní	k2eAgInPc7d1	esoterní
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
alchymistických	alchymistický	k2eAgFnPc2d1	alchymistická
obrazových	obrazový	k2eAgFnPc2d1	obrazová
sérií	série	k1gFnPc2	série
jej	on	k3xPp3gMnSc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
ke	k	k7c3	k
koncepci	koncepce	k1gFnSc3	koncepce
dvojice	dvojice	k1gFnSc2	dvojice
archetypů	archetyp	k1gInPc2	archetyp
anima	animo	k1gNnSc2	animo
a	a	k8xC	a
animus	animus	k1gInSc1	animus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mužskému	mužský	k2eAgInSc3d1	mužský
světu	svět	k1gInSc3	svět
tvoří	tvořit	k5eAaImIp3nS	tvořit
nevědomý	vědomý	k2eNgInSc4d1	nevědomý
protějšek	protějšek	k1gInSc4	protějšek
anima	animo	k1gNnSc2	animo
(	(	kIx(	(
<g/>
ženská	ženský	k2eAgFnSc1d1	ženská
část	část	k1gFnSc1	část
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjením	rozvíjení	k1gNnSc7	rozvíjení
symboliky	symbolika	k1gFnSc2	symbolika
dochází	docházet	k5eAaImIp3nP	docházet
adepti	adept	k1gMnPc1	adept
alchymie	alchymie	k1gFnSc2	alchymie
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
nevědomím	nevědomí	k1gNnSc7	nevědomí
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
psychický	psychický	k2eAgInSc1d1	psychický
střed	střed	k1gInSc1	střed
ve	v	k7c6	v
vědomém	vědomý	k2eAgInSc6d1	vědomý
já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Self	Self	k1gMnSc1	Self
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
charakteristiku	charakteristika	k1gFnSc4	charakteristika
našel	najít	k5eAaPmAgMnS	najít
Jung	Jung	k1gMnSc1	Jung
u	u	k7c2	u
snové	snový	k2eAgFnSc2d1	snová
imaginace	imaginace	k1gFnSc2	imaginace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jen	jen	k9	jen
podtrhlo	podtrhnout	k5eAaPmAgNnS	podtrhnout
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
zaměřenosti	zaměřenost	k1gFnSc6	zaměřenost
lidské	lidský	k2eAgFnSc2d1	lidská
psychiky	psychika	k1gFnSc2	psychika
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
(	(	kIx(	(
<g/>
teleologie	teleologie	k1gFnSc1	teleologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
procesem	proces	k1gInSc7	proces
individuace	individuace	k1gFnSc2	individuace
a	a	k8xC	a
ztotožnil	ztotožnit	k5eAaPmAgInS	ztotožnit
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
psychického	psychický	k2eAgInSc2d1	psychický
středu	střed	k1gInSc2	střed
v	v	k7c6	v
bytostném	bytostný	k2eAgInSc6d1	bytostný
Já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
takto	takto	k6eAd1	takto
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
terapie	terapie	k1gFnSc2	terapie
použil	použít	k5eAaPmAgInS	použít
aktivní	aktivní	k2eAgFnSc4d1	aktivní
imaginaci	imaginace	k1gFnSc4	imaginace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
klienti	klient	k1gMnPc1	klient
často	často	k6eAd1	často
zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
své	svůj	k3xOyFgFnPc4	svůj
vize	vize	k1gFnPc4	vize
do	do	k7c2	do
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
mandal	mandat	k5eAaImAgMnS	mandat
východu	východ	k1gInSc3	východ
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
psychickou	psychický	k2eAgFnSc4d1	psychická
celost	celost	k1gFnSc4	celost
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
neodděloval	oddělovat	k5eNaImAgInS	oddělovat
od	od	k7c2	od
biologické	biologický	k2eAgFnSc2d1	biologická
evoluce	evoluce	k1gFnSc2	evoluce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
však	však	k9	však
samostatně	samostatně	k6eAd1	samostatně
netematizoval	tematizovat	k5eNaImAgMnS	tematizovat
evoluční	evoluční	k2eAgFnSc3d1	evoluční
teorii	teorie	k1gFnSc3	teorie
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgMnS	držet
svého	svůj	k3xOyFgInSc2	svůj
psychologického	psychologický	k2eAgInSc2d1	psychologický
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
nutnost	nutnost	k1gFnSc4	nutnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vývoje	vývoj	k1gInSc2	vývoj
lidského	lidský	k2eAgNnSc2d1	lidské
myšlení	myšlení	k1gNnSc2	myšlení
viděl	vidět	k5eAaImAgInS	vidět
například	například	k6eAd1	například
přijetí	přijetí	k1gNnSc4	přijetí
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
nevědomé	vědomý	k2eNgFnSc2d1	nevědomá
psychické	psychický	k2eAgFnSc2d1	psychická
funkce	funkce	k1gFnSc2	funkce
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
psychologie	psychologie	k1gFnSc2	psychologie
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gNnSc4	on
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
dogmatu	dogma	k1gNnSc2	dogma
o	o	k7c4	o
nanebevzetí	nanebevzetí	k1gNnSc4	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1950	[number]	k4	1950
nijak	nijak	k6eAd1	nijak
nepřekvapilo	překvapit	k5eNaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
synchronicity	synchronicita	k1gFnSc2	synchronicita
(	(	kIx(	(
<g/>
nepříčinné	příčinný	k2eNgNnSc1d1	příčinný
spojení	spojení	k1gNnSc1	spojení
dějů	děj	k1gInPc2	děj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dotazován	dotazovat	k5eAaImNgInS	dotazovat
fyzikem	fyzik	k1gMnSc7	fyzik
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
Paulim	Paulim	k1gInSc4	Paulim
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
analogie	analogie	k1gFnSc2	analogie
při	při	k7c6	při
přechodových	přechodový	k2eAgInPc6d1	přechodový
dějích	děj	k1gInPc6	děj
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
překročil	překročit	k5eAaPmAgMnS	překročit
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
vědecká	vědecký	k2eAgNnPc4d1	vědecké
paradigmata	paradigma	k1gNnPc4	paradigma
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
synchronicitou	synchronicita	k1gFnSc7	synchronicita
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
psýché	psýché	k1gFnSc1	psýché
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vázaná	vázané	k1gNnPc4	vázané
na	na	k7c4	na
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
lokalizovaná	lokalizovaný	k2eAgFnSc1d1	lokalizovaná
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nebo	nebo	k8xC	nebo
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
psychicky	psychicky	k6eAd1	psychicky
relativní	relativní	k2eAgInSc1d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
Jungova	Jungův	k2eAgFnSc1d1	Jungova
práce	práce	k1gFnSc1	práce
nebyla	být	k5eNaImAgFnS	být
přijímána	přijímat	k5eAaImNgFnS	přijímat
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
moderní	moderní	k2eAgFnSc7d1	moderní
vědou	věda	k1gFnSc7	věda
musel	muset	k5eAaImAgInS	muset
trvale	trvale	k6eAd1	trvale
dokazovat	dokazovat	k5eAaImF	dokazovat
empirickou	empirický	k2eAgFnSc4d1	empirická
povahu	povaha	k1gFnSc4	povaha
svých	svůj	k3xOyFgNnPc2	svůj
zjištění	zjištění	k1gNnPc2	zjištění
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
obviňován	obviňovat	k5eAaImNgInS	obviňovat
z	z	k7c2	z
esoterismu	esoterismus	k1gInSc2	esoterismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
opačné	opačný	k2eAgFnSc2d1	opačná
strany	strana	k1gFnSc2	strana
jej	on	k3xPp3gMnSc4	on
například	například	k6eAd1	například
židovský	židovský	k2eAgMnSc1d1	židovský
filosof	filosof	k1gMnSc1	filosof
Martin	Martin	k1gMnSc1	Martin
Buber	Buber	k1gMnSc1	Buber
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
za	za	k7c4	za
přílišné	přílišný	k2eAgNnSc4d1	přílišné
zatemňování	zatemňování	k1gNnSc4	zatemňování
Boha	bůh	k1gMnSc4	bůh
gnostickou	gnostický	k2eAgFnSc7d1	gnostická
spekulací	spekulace	k1gFnSc7	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
výtka	výtka	k1gFnSc1	výtka
teologů	teolog	k1gMnPc2	teolog
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
psychologizujícímu	psychologizující	k2eAgNnSc3d1	psychologizující
pojetí	pojetí	k1gNnSc3	pojetí
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k9	jako
autonomní	autonomní	k2eAgInSc1d1	autonomní
psychický	psychický	k2eAgInSc1d1	psychický
obsah	obsah	k1gInSc1	obsah
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
realitu	realita	k1gFnSc4	realita
mimo	mimo	k7c4	mimo
lidskou	lidský	k2eAgFnSc4d1	lidská
psýché	psýché	k1gFnSc4	psýché
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
výtce	výtka	k1gFnSc3	výtka
se	se	k3xPyFc4	se
Jung	Jung	k1gMnSc1	Jung
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
mnohoznačně	mnohoznačně	k6eAd1	mnohoznačně
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
psychologické	psychologický	k2eAgNnSc4d1	psychologické
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
dominikánem	dominikán	k1gMnSc7	dominikán
Victorem	Victor	k1gMnSc7	Victor
Whitem	Whit	k1gMnSc7	Whit
došlo	dojít	k5eAaPmAgNnS	dojít
navzdory	navzdory	k7c3	navzdory
veškerým	veškerý	k3xTgFnPc3	veškerý
sympatiím	sympatie	k1gFnPc3	sympatie
k	k	k7c3	k
neshodě	neshoda	k1gFnSc3	neshoda
okolo	okolo	k7c2	okolo
otázky	otázka	k1gFnSc2	otázka
privatio	privatio	k6eAd1	privatio
boni	boni	k6eAd1	boni
<g/>
.	.	kIx.	.
</s>
<s>
Zlo	zlo	k1gNnSc1	zlo
podle	podle	k7c2	podle
Junga	Jung	k1gMnSc2	Jung
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
celku	celek	k1gInSc2	celek
skutečnosti	skutečnost	k1gFnSc2	skutečnost
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
redukovat	redukovat	k5eAaBmF	redukovat
na	na	k7c4	na
pouhý	pouhý	k2eAgInSc4d1	pouhý
nedostatek	nedostatek	k1gInSc4	nedostatek
dobra	dobro	k1gNnSc2	dobro
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
zde	zde	k6eAd1	zde
nacházel	nacházet	k5eAaImAgMnS	nacházet
pro	pro	k7c4	pro
tvrzení	tvrzení	k1gNnSc4	tvrzení
psychologické	psychologický	k2eAgNnSc1d1	psychologické
zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
<g/>
.	.	kIx.	.
</s>
<s>
Jungova	Jungův	k2eAgFnSc1d1	Jungova
analytická	analytický	k2eAgFnSc1d1	analytická
psychologie	psychologie	k1gFnSc1	psychologie
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
použitelná	použitelný	k2eAgFnSc1d1	použitelná
a	a	k8xC	a
neomezuje	omezovat	k5eNaImIp3nS	omezovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
psychologické	psychologický	k2eAgInPc4d1	psychologický
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
respektuje	respektovat	k5eAaImIp3nS	respektovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
povahu	povaha	k1gFnSc4	povaha
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
si	se	k3xPyFc3	se
celou	celá	k1gFnSc4	celá
řadu	řad	k1gInSc2	řad
ctitelů	ctitel	k1gMnPc2	ctitel
a	a	k8xC	a
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
Jungově	Jungův	k2eAgNnSc6d1	Jungovo
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Institut	institut	k1gInSc1	institut
C.	C.	kA	C.
G.	G.	kA	G.
Junga	Jung	k1gMnSc4	Jung
jako	jako	k8xC	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
vzrůstající	vzrůstající	k2eAgInSc4d1	vzrůstající
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c6	na
myšlenkách	myšlenka	k1gFnPc6	myšlenka
analytické	analytický	k2eAgFnSc2d1	analytická
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Jung	Jung	k1gMnSc1	Jung
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
objevy	objev	k1gInPc1	objev
jej	on	k3xPp3gMnSc4	on
překročily	překročit	k5eAaPmAgFnP	překročit
a	a	k8xC	a
z	z	k7c2	z
analytické	analytický	k2eAgFnSc2d1	analytická
psychologie	psychologie	k1gFnSc2	psychologie
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
institut	institut	k1gInSc1	institut
orientuje	orientovat	k5eAaBmIp3nS	orientovat
jako	jako	k9	jako
tréninkové	tréninkový	k2eAgNnSc4d1	tréninkové
centrum	centrum	k1gNnSc4	centrum
analytiků	analytik	k1gMnPc2	analytik
a	a	k8xC	a
psychoterapeutů	psychoterapeut	k1gMnPc2	psychoterapeut
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
setkávání	setkávání	k1gNnSc3	setkávání
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
analytickou	analytický	k2eAgFnSc4d1	analytická
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zas	zas	k6eAd1	zas
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
analytickou	analytický	k2eAgFnSc4d1	analytická
psychologii	psychologie	k1gFnSc4	psychologie
(	(	kIx(	(
<g/>
ČSAP	ČSAP	kA	ČSAP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgFnPc4d1	lokální
společnosti	společnost	k1gFnPc4	společnost
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
The	The	k1gFnPc7	The
International	International	k1gFnSc1	International
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
Analytical	Analytical	k1gFnPc2	Analytical
Psychology	psycholog	k1gMnPc4	psycholog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejcitovanějších	citovaný	k2eAgMnPc2d3	nejcitovanější
psychologů	psycholog	k1gMnPc2	psycholog
mu	on	k3xPp3gMnSc3	on
autor	autor	k1gMnSc1	autor
pořadí	pořadí	k1gNnSc6	pořadí
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1903	[number]	k4	1903
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
Emmu	Emma	k1gFnSc4	Emma
Rauschenbachovou	Rauschenbachová	k1gFnSc4	Rauschenbachová
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1882	[number]	k4	1882
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
průmyslnické	průmyslnický	k2eAgFnSc2d1	průmyslnická
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c4	v
Schaffhausenu	Schaffhausen	k2eAgFnSc4d1	Schaffhausen
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
Agathe	Agathe	k1gFnSc1	Agathe
Jungová-Niehusová	Jungová-Niehusový	k2eAgFnSc1d1	Jungová-Niehusový
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gret	Gret	k1gInSc1	Gret
Jungová-Baumanová	Jungová-Baumanová	k1gFnSc1	Jungová-Baumanová
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Jung	Jung	k1gMnSc1	Jung
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1908	[number]	k4	1908
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marianne	Mariann	k1gInSc5	Mariann
Jungová-Niehusová	Jungová-Niehusový	k2eAgFnSc1d1	Jungová-Niehusový
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Helene	Helen	k1gInSc5	Helen
Jungová-Hoerniová	Jungová-Hoerniový	k2eAgFnSc1d1	Jungová-Hoerniový
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Burghölzli	Burghölzli	k1gFnSc6	Burghölzli
navázal	navázat	k5eAaPmAgMnS	navázat
Jung	Jung	k1gMnSc1	Jung
mimomanželský	mimomanželský	k2eAgInSc1d1	mimomanželský
poměr	poměr	k1gInSc1	poměr
se	s	k7c7	s
Sabinou	Sabina	k1gFnSc7	Sabina
Spielreinovou	Spielreinová	k1gFnSc7	Spielreinová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
ruská	ruský	k2eAgFnSc1d1	ruská
židovka	židovka	k1gFnSc1	židovka
studovala	studovat	k5eAaImAgFnS	studovat
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
medicínu	medicína	k1gFnSc4	medicína
a	a	k8xC	a
k	k	k7c3	k
Jungovi	Jung	k1gMnSc3	Jung
si	se	k3xPyFc3	se
během	během	k7c2	během
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
(	(	kIx(	(
<g/>
přenosu	přenos	k1gInSc2	přenos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
silný	silný	k2eAgInSc4d1	silný
citový	citový	k2eAgInSc4d1	citový
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
až	až	k9	až
bizarní	bizarní	k2eAgInSc4d1	bizarní
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
neetický	etický	k2eNgInSc4d1	neetický
poměr	poměr	k1gInSc4	poměr
přišel	přijít	k5eAaPmAgInS	přijít
Eugen	Eugen	k2eAgInSc1d1	Eugen
Bleuler	Bleuler	k1gInSc1	Bleuler
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jung	Jung	k1gMnSc1	Jung
z	z	k7c2	z
léčebny	léčebna	k1gFnSc2	léčebna
v	v	k7c6	v
Burghölzli	Burghölzli	k1gFnSc6	Burghölzli
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
zemřel	zemřít	k5eAaPmAgMnS	zemřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
v	v	k7c6	v
Küsnachtu	Küsnacht	k1gInSc6	Küsnacht
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
hrobě	hrob	k1gInSc6	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Životopisné	životopisný	k2eAgInPc1d1	životopisný
filmy	film	k1gInPc1	film
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
natočil	natočit	k5eAaBmAgMnS	natočit
italský	italský	k2eAgMnSc1d1	italský
režisér	režisér	k1gMnSc1	režisér
Roberto	Roberta	k1gFnSc5	Roberta
Faenza	Faenza	k1gFnSc1	Faenza
film	film	k1gInSc1	film
Strážce	strážce	k1gMnSc1	strážce
duše	duše	k1gFnSc1	duše
(	(	kIx(	(
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Prendimi	Prendi	k1gFnPc7	Prendi
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
anima	animo	k1gNnPc1	animo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Junga	Jung	k1gMnSc2	Jung
Iain	Iain	k1gMnSc1	Iain
Glen	Glen	k1gMnSc1	Glen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
film	film	k1gInSc1	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
životě	život	k1gInSc6	život
Carla	Carl	k1gMnSc2	Carl
Gustava	Gustav	k1gMnSc2	Gustav
Junga	Jung	k1gMnSc2	Jung
v	v	k7c6	v
letech	let	k1gInPc6	let
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Junga	Jung	k1gMnSc2	Jung
Michael	Michael	k1gMnSc1	Michael
Fassbender	Fassbender	k1gMnSc1	Fassbender
<g/>
.	.	kIx.	.
</s>
