<s>
JCDecaux	JCDecaux	k1gInSc1
</s>
<s>
JCDecaux	JCDecaux	k1gInSc1
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1964	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Jean-Claude	Jean-Claude	k6eAd1
Decaux	Decaux	k1gInSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Neuilly-sur-Seine	Neuilly-sur-Seinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnPc1
Produkty	produkt	k1gInPc1
</s>
<s>
street	street	k1gMnSc1
furniture	furnitur	k1gMnSc5
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.jcdecaux.com	www.jcdecaux.com	k1gInSc1
ISIN	ISIN	kA
</s>
<s>
FR0000077919	FR0000077919	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reklamní	reklamní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
v	v	k7c6
Enschede	Ensched	k1gMnSc5
v	v	k7c6
Holandsku	Holandsko	k1gNnSc6
</s>
<s>
Skupina	skupina	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
[	[	kIx(
<g/>
žisedeko	žisedeko	k6eAd1
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
se	se	k3xPyFc4
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
společností	společnost	k1gFnSc7
zaměřenou	zaměřený	k2eAgFnSc7d1
na	na	k7c4
venkovní	venkovní	k2eAgFnSc4d1
reklamu	reklama	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
rozvíjí	rozvíjet	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
ve	v	k7c6
třech	tři	k4xCgInPc6
hlavních	hlavní	k2eAgInPc6d1
segmentech	segment	k1gInPc6
<g/>
:	:	kIx,
městský	městský	k2eAgInSc1d1
mobiliář	mobiliář	k1gInSc1
<g/>
,	,	kIx,
velkoformátová	velkoformátový	k2eAgFnSc1d1
reklama	reklama	k1gFnSc1
a	a	k8xC
reklama	reklama	k1gFnSc1
v	v	k7c6
systémech	systém	k1gInPc6
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
JCDecaux	JCDecaux	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
provozuje	provozovat	k5eAaImIp3nS
vybraný	vybraný	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
mobiliář	mobiliář	k1gInSc1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
jména	jméno	k1gNnSc2
zakladatele	zakladatel	k1gMnSc2
Jeana	Jean	k1gMnSc2
Clauda	Clauda	k1gFnSc1
Decauxe	Decauxe	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k9
„	„	k?
<g/>
miliardář	miliardář	k1gMnSc1
ze	z	k7c2
zastávky	zastávka	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
od	od	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
právě	právě	k9
v	v	k7c6
zastávkových	zastávkový	k2eAgInPc6d1
přístřešcích	přístřešek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Koncept	koncept	k1gInSc1
"	"	kIx"
<g/>
městského	městský	k2eAgInSc2d1
mobiliáře	mobiliář	k1gInSc2
<g/>
"	"	kIx"
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
reklamního	reklamní	k2eAgNnSc2d1
využití	využití	k1gNnSc2
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
investorem	investor	k1gMnSc7
byl	být	k5eAaImAgMnS
zakladatel	zakladatel	k1gMnSc1
společnosti	společnost	k1gFnSc2
Jean-Claude	Jean-Claud	k1gInSc5
Decaux	Decaux	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
myšlence	myšlenka	k1gFnSc6
<g/>
,	,	kIx,
nabídnout	nabídnout	k5eAaPmF
městu	město	k1gNnSc3
prvky	prvek	k1gInPc7
městského	městský	k2eAgInSc2d1
mobiliáře	mobiliář	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
výstavba	výstavba	k1gFnSc1
i	i	k9
následná	následný	k2eAgFnSc1d1
údržba	údržba	k1gFnSc1
je	být	k5eAaImIp3nS
financována	financovat	k5eAaBmNgFnS
skrze	skrze	k?
reklamu	reklama	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Prvním	první	k4xOgNnSc7
městem	město	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
autobusové	autobusový	k2eAgFnPc1d1
zastávky	zastávka	k1gFnPc1
s	s	k7c7
reklamou	reklama	k1gFnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Lyon	Lyon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Služby	služba	k1gFnPc1
a	a	k8xC
obrat	obrat	k1gInSc1
</s>
<s>
JCDecaux	JCDecaux	k1gInSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
firmou	firma	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
reklamy	reklama	k1gFnSc2
na	na	k7c6
městském	městský	k2eAgInSc6d1
mobiliáři	mobiliář	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
reklamě	reklama	k1gFnSc6
na	na	k7c6
letištích	letiště	k1gNnPc6
(	(	kIx(
<g/>
184	#num#	k4
letišť	letiště	k1gNnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
systému	systém	k1gInSc6
samoobslužných	samoobslužný	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
firmou	firma	k1gFnSc7
poskytující	poskytující	k2eAgFnSc4d1
reklamu	reklama	k1gFnSc4
na	na	k7c6
billboardech	billboard	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
zaměstnává	zaměstnávat	k5eAaImIp3nS
celkem	celkem	k6eAd1
9.940	9.940	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
skupina	skupina	k1gFnSc1
je	být	k5eAaImIp3nS
přítomna	přítomen	k2eAgFnSc1d1
v	v	k7c6
56	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
a	a	k8xC
3600	#num#	k4
městech	město	k1gNnPc6
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
10.000	10.000	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
obratu	obrat	k1gInSc3
€	€	k?
2.350	2.350	k4
mio	mio	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
provozuje	provozovat	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
české	český	k2eAgFnSc2d1
dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
JCDecaux	JCDecaux	k1gInSc1
<g/>
,	,	kIx,
Městský	městský	k2eAgInSc1d1
mobiliář	mobiliář	k1gInSc1
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
na	na	k7c4
dobu	doba	k1gFnSc4
25	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
prodloužena	prodloužit	k5eAaPmNgFnS
do	do	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
městský	městský	k2eAgInSc1d1
mobiliář	mobiliář	k1gInSc1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
síť	síť	k1gFnSc1
samostatně	samostatně	k6eAd1
stojících	stojící	k2eAgFnPc2d1
reklamních	reklamní	k2eAgFnPc2d1
vitrín	vitrína	k1gFnPc2
<g/>
,	,	kIx,
zastávkových	zastávkový	k2eAgInPc2d1
přístřešků	přístřešek	k1gInPc2
a	a	k8xC
také	také	k9
velkých	velký	k2eAgInPc2d1
reklamních	reklamní	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yRgFnPc6,k3yQgFnPc6
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
integrováno	integrovat	k5eAaBmNgNnS
WC	WC	kA
nebo	nebo	k8xC
telefonní	telefonní	k2eAgFnSc1d1
budka	budka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
poskytuje	poskytovat	k5eAaImIp3nS
městu	město	k1gNnSc3
také	také	k9
služby	služba	k1gFnPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
budování	budování	k1gNnSc1
a	a	k8xC
údržba	údržba	k1gFnSc1
přístřešků	přístřešek	k1gInPc2
zastávek	zastávka	k1gFnPc2
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
odpadkových	odpadkový	k2eAgInPc2d1
košů	koš	k1gInPc2
či	či	k8xC
veřejných	veřejný	k2eAgFnPc2d1
toalet	toaleta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
reklamní	reklamní	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
a	a	k8xC
reklamní	reklamní	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
s	s	k7c7
celoplošným	celoplošný	k2eAgInSc7d1
zásahem	zásah	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
nástupu	nástup	k1gInSc6
přebrala	přebrat	k5eAaPmAgFnS
do	do	k7c2
správy	správa	k1gFnSc2
a	a	k8xC
vlastnictví	vlastnictví	k1gNnSc1
mobiliář	mobiliář	k1gInSc4
společnosti	společnost	k1gFnSc2
Dambach	Dambacha	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zde	zde	k6eAd1
působila	působit	k5eAaImAgFnS
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Přístřešky	přístřešek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
JCDecaux	JCDecaux	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
instaloval	instalovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
Norman	Norman	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Část	část	k1gFnSc1
mobiliáře	mobiliář	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
JCDecaux	JCDecaux	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
zřídila	zřídit	k5eAaPmAgNnP
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
architekt	architekt	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Špaček	Špaček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Útvaru	útvar	k1gInSc2
hlavního	hlavní	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
<g/>
,	,	kIx,
zaštítěné	zaštítěný	k2eAgInPc4d1
JCDecaux	JCDecaux	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
reklamní	reklamní	k2eAgInPc4d1
a	a	k8xC
informační	informační	k2eAgInPc4d1
sloupy	sloup	k1gInPc4
a	a	k8xC
pouliční	pouliční	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
navázané	navázaný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
navrhl	navrhnout	k5eAaPmAgMnS
prodejní	prodejní	k2eAgInPc4d1
stánky	stánek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
památkáři	památkář	k1gMnPc1
stánkařům	stánkař	k1gMnPc3
řekli	říct	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
povolí	povolit	k5eAaPmIp3nP
jen	jen	k9
stánky	stánek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
budou	být	k5eAaImBp3nP
vycházet	vycházet	k5eAaImF
z	z	k7c2
tvarosloví	tvarosloví	k1gNnSc2
JCDecaux	JCDecaux	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
napodobenin	napodobenina	k1gFnPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
soudním	soudní	k2eAgInPc3d1
sporům	spor	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Špaček	Špaček	k1gMnSc1
dále	daleko	k6eAd2
pro	pro	k7c4
JCDexaux	JCDexaux	k1gInSc4
navrhoval	navrhovat	k5eAaImAgMnS
drobnosti	drobnost	k1gFnSc3
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
tvarování	tvarování	k1gNnSc1
výdechů	výdech	k1gInPc2
z	z	k7c2
podchodů	podchod	k1gInPc2
na	na	k7c6
Václavském	václavský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
kapotáž	kapotáž	k1gFnSc1
měřící	měřící	k2eAgFnSc2d1
meteorologické	meteorologický	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
na	na	k7c6
náměstí	náměstí	k1gNnSc6
Republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Hodiny	hodina	k1gFnSc2
typu	typ	k1gInSc2
JCDecaux	JCDecaux	k1gInSc1
od	od	k7c2
firmy	firma	k1gFnSc2
SPEL	SPEL	kA
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architekta	architekt	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
Špačka	Špaček	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
první	první	k4xOgInPc1
exempláře	exemplář	k1gInPc1
byly	být	k5eAaImAgInP
umístěny	umístit	k5eAaPmNgInP
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
v	v	k7c6
ulici	ulice	k1gFnSc6
Újezd	Újezd	k1gInSc1
na	na	k7c6
křižovatkách	křižovatka	k1gFnPc6
s	s	k7c7
Všehrdovou	Všehrdová	k1gFnSc7
a	a	k8xC
s	s	k7c7
Vítěznou	vítězný	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
a	a	k8xC
celkově	celkově	k6eAd1
jich	on	k3xPp3gMnPc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
instalováno	instalovat	k5eAaBmNgNnS
40	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
JCDecaux	JCDecaux	k1gInSc1
nevlastnil	vlastnit	k5eNaImAgInS
a	a	k8xC
neprovozoval	provozovat	k5eNaImAgInS
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnSc7
správcem	správce	k1gMnSc7
byli	být	k5eAaImAgMnP
správci	správce	k1gMnPc7
městského	městský	k2eAgNnSc2d1
veřejného	veřejný	k2eAgNnSc2d1
osvětlení	osvětlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
JCDecaux	JCDecaux	k1gInSc1
vlastnila	vlastnit	k5eAaImAgFnS
a	a	k8xC
provozovala	provozovat	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
900	#num#	k4
přístřešků	přístřešek	k1gInPc2
na	na	k7c6
zastávkách	zastávka	k1gFnPc6
MHD	MHD	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
300	#num#	k4
z	z	k7c2
nich	on	k3xPp3gInPc2
bylo	být	k5eAaImAgNnS
bez	bez	k7c2
reklamní	reklamní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
provozuje	provozovat	k5eAaImIp3nS
344	#num#	k4
prosvětlených	prosvětlený	k2eAgNnPc2d1
city	city	k1gNnPc2
light	lighta	k1gFnPc2
vitrín	vitrína	k1gFnPc2
<g/>
,	,	kIx,
148	#num#	k4
velkoformátových	velkoformátový	k2eAgFnPc2d1
vitrín	vitrína	k1gFnPc2
<g/>
,	,	kIx,
1288	#num#	k4
metrů	metr	k1gInPc2
reklamního	reklamní	k2eAgNnSc2d1
zábradlí	zábradlí	k1gNnSc2
a	a	k8xC
68	#num#	k4
informačních	informační	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
konkurenční	konkurenční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
BigBoard	BigBoarda	k1gFnPc2
a	a	k8xC
Svaz	svaz	k1gInSc1
provozovatelů	provozovatel	k1gMnPc2
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
spustily	spustit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
kampaň	kampaň	k1gFnSc1
„	„	k?
<g/>
Pražane	Pražan	k1gMnSc5
<g/>
,	,	kIx,
i	i	k8xC
ty	ten	k3xDgMnPc4
přicházíš	přicházet	k5eAaImIp2nS
o	o	k7c4
peníze	peníz	k1gInPc4
<g/>
“	“	k?
proti	proti	k7c3
smlouvě	smlouva	k1gFnSc3
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
se	se	k3xPyFc4
společností	společnost	k1gFnPc2
JCDecaux	JCDecaux	k1gInSc1
<g/>
,	,	kIx,
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
web	web	k1gInSc1
okradenapraha	okradenapraha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
vedly	vést	k5eAaImAgFnP
kampaň	kampaň	k1gFnSc4
proti	proti	k7c3
TOP09	TOP09	k1gMnSc3
a	a	k8xC
primátorovi	primátor	k1gMnSc3
Tomáši	Tomáš	k1gMnSc3
Hudečkovi	Hudeček	k1gMnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
kampaně	kampaň	k1gFnSc2
byly	být	k5eAaImAgFnP
Hudečkem	Hudeček	k1gMnSc7
podporované	podporovaný	k2eAgInPc4d1
stavební	stavební	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
zakázaly	zakázat	k5eAaPmAgFnP
reklamu	reklama	k1gFnSc4
větší	veliký	k2eAgFnPc1d2
než	než	k8xS
šest	šest	k4xCc1
metrů	metr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Asociace	asociace	k1gFnSc2
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
Stanislav	Stanislav	k1gMnSc1
Lazar	Lazar	k1gMnSc1
poté	poté	k6eAd1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kvůli	kvůli	k7c3
sporu	spor	k1gInSc3
ohledně	ohledně	k7c2
velikosti	velikost	k1gFnSc2
billboardů	billboard	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
se	se	k3xPyFc4
asociace	asociace	k1gFnPc1
po	po	k7c6
třech	tři	k4xCgInPc6
letech	let	k1gInPc6
existence	existence	k1gFnSc2
s	s	k7c7
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
2014	#num#	k4
rozpadne	rozpadnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zánik	zánik	k1gInSc4
iniciovala	iniciovat	k5eAaBmAgFnS
společnost	společnost	k1gFnSc1
BigBoard	BigBoard	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
Praha	Praha	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
firmou	firma	k1gFnSc7
JCDecaux	JCDecaux	k1gInSc4
neprodlouží	prodloužit	k5eNaPmIp3nS
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
smlouva	smlouva	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
přístřešky	přístřešek	k1gInPc4
provozovat	provozovat	k5eAaImF
20	#num#	k4
let	léto	k1gNnPc2
jiná	jiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgFnSc7d1
ředitel	ředitel	k1gMnSc1
JCDecaux	JCDecaux	k1gInSc1
Pavel	Pavel	k1gMnSc1
Slabý	Slabý	k1gMnSc1
vyjádřil	vyjádřit	k5eAaPmAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgInPc4
podklady	podklad	k1gInPc4
připravené	připravený	k2eAgInPc4d1
pro	pro	k7c4
radu	rada	k1gFnSc4
města	město	k1gNnSc2
magistrátním	magistrátní	k2eAgMnSc7d1
úředníkem	úředník	k1gMnSc7
panem	pan	k1gMnSc7
Matesem	Mates	k1gMnSc7
jsou	být	k5eAaImIp3nP
zjevně	zjevně	k6eAd1
ušity	ušít	k5eAaPmNgInP
na	na	k7c4
míru	míra	k1gFnSc4
skupině	skupina	k1gFnSc3
BigBoard	BigBoarda	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Praha	Praha	k1gFnSc1
dle	dle	k7c2
svého	svůj	k3xOyFgNnSc2
vyjádření	vyjádření	k1gNnSc2
do	do	k7c2
výběrového	výběrový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
s	s	k7c7
účastí	účast	k1gFnSc7
JCDecaux	JCDecaux	k1gInSc1
ani	ani	k8xC
nepočítá	počítat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Deník	deník	k1gInSc1
Právo	právo	k1gNnSc4
přišel	přijít	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
bývalý	bývalý	k2eAgMnSc1d1
magistrátní	magistrátní	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
Ivo	Ivo	k1gMnSc1
Mates	Mates	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgMnS
jako	jako	k9
zpracovatel	zpracovatel	k1gMnSc1
tisku	tisk	k1gInSc2
ohledně	ohledně	k7c2
přístřešků	přístřešek	k1gInPc2
<g/>
,	,	kIx,
používal	používat	k5eAaImAgInS
několik	několik	k4yIc4
let	léto	k1gNnPc2
vůz	vůz	k1gInSc4
Volvo	Volvo	k1gNnSc1
patřící	patřící	k2eAgInPc4d1
firmám	firma	k1gFnPc3
kolem	kolem	k7c2
skupiny	skupina	k1gFnSc2
BigBoard	BigBoard	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
komisi	komise	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
další	další	k2eAgInSc4d1
postup	postup	k1gInSc4
připravila	připravit	k5eAaPmAgFnS
<g/>
,	,	kIx,
seděl	sedět	k5eAaImAgMnS
Stanislav	Stanislav	k1gMnSc1
Lazar	Lazar	k1gMnSc1
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
výkonný	výkonný	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Svazu	svaz	k1gInSc2
provozovatelů	provozovatel	k1gMnPc2
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
podle	podle	k7c2
zástupců	zástupce	k1gMnPc2
JCDecaux	JCDecaux	k1gInSc4
vznikl	vzniknout	k5eAaPmAgInS
pod	pod	k7c7
taktovkou	taktovka	k1gFnSc7
BigBoardu	BigBoard	k1gInSc2
a	a	k8xC
hájí	hájit	k5eAaImIp3nS
jeho	jeho	k3xOp3gInPc4
zájmy	zájem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupci	zástupce	k1gMnPc7
firmy	firma	k1gFnSc2
BigBoard	BigBoard	k1gInSc4
obě	dva	k4xCgNnPc4
nařčení	nařčení	k1gNnPc4
ostře	ostro	k6eAd1
odmítli	odmítnout	k5eAaPmAgMnP
a	a	k8xC
označili	označit	k5eAaPmAgMnP
je	on	k3xPp3gFnPc4
za	za	k7c4
nepravdivá	pravdivý	k2eNgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Mluvčí	mluvčí	k1gMnSc1
Prahy	Praha	k1gFnSc2
Vít	Vít	k1gMnSc1
Hofmann	Hofmann	k1gMnSc1
označil	označit	k5eAaPmAgMnS
nařčení	nařčení	k1gNnSc4
z	z	k7c2
korupce	korupce	k1gFnSc2
za	za	k7c4
pouhý	pouhý	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
kvůli	kvůli	k7c3
končící	končící	k2eAgFnSc3d1
smlouvě	smlouva	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
JCDecaux	JCDecaux	k1gInSc4
poslala	poslat	k5eAaPmAgFnS
<g />
.	.	kIx.
</s>
<s hack="1">
primátorce	primátorka	k1gFnSc6
Adrianě	Adriana	k1gFnSc6
Krnáčové	Krnáčová	k1gFnSc2
otevřený	otevřený	k2eAgInSc4d1
dopis	dopis	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c4
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
vylepila	vylepit	k5eAaPmAgFnS
též	též	k9
na	na	k7c6
některých	některý	k3yIgInPc6
zastávkových	zastávkový	k2eAgInPc6d1
přístřešcích	přístřešek	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
prodlouženého	prodloužený	k2eAgInSc2d1
víkendu	víkend	k1gInSc2
v	v	k7c6
červenci	červenec	k1gInSc6
2018	#num#	k4
asi	asi	k9
na	na	k7c4
20	#num#	k4
přístřešků	přístřešek	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vylepen	vylepen	k2eAgInSc4d1
tento	tento	k3xDgInSc4
dopis	dopis	k1gInSc4
<g/>
,	,	kIx,
zaútočil	zaútočit	k5eAaPmAgMnS
neznámý	známý	k2eNgMnSc1d1
vandal	vandal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autor	autor	k1gMnSc1
designu	design	k1gInSc2
přístřešků	přístřešek	k1gInPc2
Norman	Norman	k1gMnSc1
Foster	Foster	k1gMnSc1
údajně	údajně	k6eAd1
svůj	svůj	k3xOyFgInSc4
design	design	k1gInSc1
zkorumpovaným	zkorumpovaný	k2eAgMnSc7d1
pražským	pražský	k2eAgMnSc7d1
úředníkům	úředník	k1gMnPc3
odmítá	odmítat	k5eAaImIp3nS
prodat	prodat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přístřešek	přístřešek	k1gInSc1
navržený	navržený	k2eAgInSc1d1
Normanem	Norman	k1gMnSc7
Fosterem	Foster	k1gMnSc7
</s>
<s>
Hodiny	hodina	k1gFnPc1
podle	podle	k7c2
návrhu	návrh	k1gInSc2
Jiřího	Jiří	k1gMnSc2
Špačka	Špaček	k1gMnSc2
</s>
<s>
Technický	technický	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
JCDecaux	JCDecaux	k1gInSc4
Tomáš	Tomáš	k1gMnSc1
Tenzer	Tenzer	k1gMnSc1
byl	být	k5eAaImAgMnS
podle	podle	k7c2
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
obviněn	obvinit	k5eAaPmNgMnS
z	z	k7c2
korupce	korupce	k1gFnSc2
a	a	k8xC
manipulace	manipulace	k1gFnSc2
s	s	k7c7
veřejnými	veřejný	k2eAgFnPc7d1
zakázkami	zakázka	k1gFnPc7
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
firma	firma	k1gFnSc1
podle	podle	k7c2
obžaloby	obžaloba	k1gFnSc2
vyplácela	vyplácet	k5eAaImAgFnS
statisícové	statisícový	k2eAgFnPc4d1
částky	částka	k1gFnPc4
někdejšímu	někdejší	k2eAgMnSc3d1
starostovi	starosta	k1gMnSc3
Prahy	Praha	k1gFnSc2
1	#num#	k4
Michalu	Michal	k1gMnSc3
Valentovi	Valenta	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
zajistit	zajistit	k5eAaPmF
hladký	hladký	k2eAgInSc4d1
průběh	průběh	k1gInSc4
tendru	tendr	k1gInSc2
na	na	k7c4
reklamu	reklama	k1gFnSc4
v	v	k7c6
CLV	CLV	kA
vitrínách	vitrína	k1gFnPc6
v	v	k7c6
podchodu	podchod	k1gInSc6
metra	metro	k1gNnSc2
stanice	stanice	k1gFnSc2
Staroměstská	staroměstský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
telefonních	telefonní	k2eAgInPc2d1
odposlechů	odposlech	k1gInPc2
se	se	k3xPyFc4
Tenzer	Tenzer	k1gInSc1
s	s	k7c7
Valentou	Valenta	k1gMnSc7
předem	předem	k6eAd1
domlouvali	domlouvat	k5eAaImAgMnP
<g/>
,	,	kIx,
jaké	jaký	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
parametry	parametr	k1gInPc4
má	mít	k5eAaImIp3nS
mít	mít	k5eAaImF
výběrové	výběrový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
na	na	k7c4
pronájem	pronájem	k1gInSc4
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
při	při	k7c6
druhém	druhý	k4xOgInSc6
pokusu	pokus	k1gInSc6
o	o	k7c4
výběrové	výběrový	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
skutečně	skutečně	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
provizi	provize	k1gFnSc4
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
fakturovala	fakturovat	k5eAaImAgFnS
jako	jako	k9
konzultační	konzultační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JCDecaux	JCDecaux	k1gInSc1
k	k	k7c3
tomu	ten	k3xDgMnSc3
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgFnPc4
spekulace	spekulace	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
možného	možný	k2eAgNnSc2d1
korupčního	korupční	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
jsou	být	k5eAaImIp3nP
nepravdivé	pravdivý	k2eNgFnPc1d1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
mediální	mediální	k2eAgFnSc4d1
manipulaci	manipulace	k1gFnSc4
a	a	k8xC
snahu	snaha	k1gFnSc4
konkurence	konkurence	k1gFnSc2
společnost	společnost	k1gFnSc1
poškodit	poškodit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
se	se	k3xPyFc4
hodlá	hodlat	k5eAaImIp3nS
proti	proti	k7c3
těmto	tento	k3xDgNnPc3
nařčením	nařčení	k1gNnPc3
všemi	všecek	k3xTgFnPc7
prostředky	prostředek	k1gInPc4
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michal	Michal	k1gMnSc1
Valenta	Valenta	k1gMnSc1
k	k	k7c3
tomu	ten	k3xDgNnSc3
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
konzultace	konzultace	k1gFnPc1
skutečně	skutečně	k6eAd1
společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc4
poskytovala	poskytovat	k5eAaImAgFnS
a	a	k8xC
smlouva	smlouva	k1gFnSc1
se	se	k3xPyFc4
nevztahovala	vztahovat	k5eNaImAgFnS
k	k	k7c3
diskutovanému	diskutovaný	k2eAgInSc3d1
případu	případ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Tenzer	Tenzer	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
kandidoval	kandidovat	k5eAaImAgMnS
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
za	za	k7c4
Pirátskou	pirátský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
zažalovala	zažalovat	k5eAaPmAgFnS
TV	TV	kA
Barrandov	Barrandov	k1gInSc4
a	a	k8xC
jejího	její	k3xOp3gMnSc2
majitele	majitel	k1gMnSc2
Jaromíra	Jaromír	k1gMnSc4
Soukupa	Soukup	k1gMnSc2
o	o	k7c4
100	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
jako	jako	k8xS,k8xC
odškodné	odškodné	k1gNnSc4
za	za	k7c4
šíření	šíření	k1gNnSc4
nepravdivých	pravdivý	k2eNgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
kauze	kauza	k1gFnSc6
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepravdivost	Nepravdivost	k1gFnSc1
měla	mít	k5eAaImAgFnS
spočívat	spočívat	k5eAaImF
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
televize	televize	k1gFnSc1
označila	označit	k5eAaPmAgFnS
případ	případ	k1gInSc4
za	za	k7c4
korupci	korupce	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
obžaloba	obžaloba	k1gFnSc1
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
jiném	jiný	k2eAgInSc6d1
trestném	trestný	k2eAgInSc6d1
činu	čin	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
o	o	k7c6
sjednání	sjednání	k1gNnSc6
výhody	výhoda	k1gFnSc2
při	při	k7c6
veřejné	veřejný	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
firmy	firma	k1gFnSc2
Soukup	Soukup	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
zjevně	zjevně	k6eAd1
účelově	účelově	k6eAd1
a	a	k8xC
záměrně	záměrně	k6eAd1
i	i	k9
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
nepravdivých	pravdivý	k2eNgFnPc2d1
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
se	se	k3xPyFc4
chtěla	chtít	k5eAaImAgFnS
obrátit	obrátit	k5eAaPmF
také	také	k9
na	na	k7c4
Radu	rada	k1gFnSc4
pro	pro	k7c4
rozhlasové	rozhlasový	k2eAgNnSc4d1
a	a	k8xC
televizní	televizní	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Předběžným	předběžný	k2eAgNnSc7d1
opatřením	opatření	k1gNnSc7
obvodní	obvodní	k2eAgInSc4d1
soud	soud	k1gInSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
1	#num#	k4
v	v	k7c6
dubnu	duben	k1gInSc6
2019	#num#	k4
televizi	televize	k1gFnSc6
TV	TV	kA
Barrandov	Barrandov	k1gInSc1
zakázal	zakázat	k5eAaPmAgInS
vysílat	vysílat	k5eAaImF
o	o	k7c4
JCDecaux	JCDecaux	k1gInSc4
kvůli	kvůli	k7c3
kauze	kauza	k1gFnSc3
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgNnSc1d1
stání	stání	k1gNnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
konat	konat	k5eAaImF
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
ovládla	ovládnout	k5eAaPmAgFnS
také	také	k6eAd1
reklamní	reklamní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Rencar	Rencar	k1gInSc1
Praha	Praha	k1gFnSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
dceřinou	dceřin	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obchodním	obchodní	k2eAgInSc6d1
rejstříku	rejstřík	k1gInSc6
nejsou	být	k5eNaImIp3nP
vlastníci	vlastník	k1gMnPc1
Rencaru	Rencar	k1gInSc2
či	či	k8xC
vlastnické	vlastnický	k2eAgInPc1d1
podíly	podíl	k1gInPc1
uvedeni	uvést	k5eAaPmNgMnP
(	(	kIx(
<g/>
společnost	společnost	k1gFnSc1
má	mít	k5eAaImIp3nS
akcie	akcie	k1gFnPc4
na	na	k7c4
jméno	jméno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rencar	Rencar	k1gInSc1
Praha	Praha	k1gFnSc1
a.s.	a.s.	k?
se	se	k3xPyFc4
však	však	k9
sama	sám	k3xTgFnSc1
prohlašuje	prohlašovat	k5eAaImIp3nS
za	za	k7c4
součást	součást	k1gFnSc4
skupiny	skupina	k1gFnSc2
JCDecaux	JCDecaux	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Rencar	Rencara	k1gFnPc2
Praha	Praha	k1gFnSc1
a.s.	a.s.	k?
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1990	#num#	k4
jako	jako	k8xC,k8xS
dceřiná	dceřiný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
DP	DP	kA
hl.	hl.	k?
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
je	být	k5eAaImIp3nS
majoritním	majoritní	k2eAgMnSc7d1
akcionářem	akcionář	k1gMnSc7
společnost	společnost	k1gFnSc1
Europlakat	Europlakat	k1gMnSc1
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
velkoplošnou	velkoplošný	k2eAgFnSc4d1
reklamu	reklama	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
skupiny	skupina	k1gFnSc2
JCDecaux	JCDecaux	k1gInSc1
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Rencar	Rencar	k1gInSc1
Praha	Praha	k1gFnSc1
a.s.	a.s.	k?
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
polovičním	poloviční	k2eAgMnSc7d1
vlastníkem	vlastník	k1gMnSc7
společnosti	společnost	k1gFnSc2
CLV	CLV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
provozuje	provozovat	k5eAaImIp3nS
city-light	city-light	k1gInSc4
vitríny	vitrína	k1gFnSc2
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Samoobslužné	samoobslužný	k2eAgFnPc1d1
půjčovny	půjčovna	k1gFnPc1
kol	kolo	k1gNnPc2
</s>
<s>
Společnost	společnost	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
provozuje	provozovat	k5eAaImIp3nS
systém	systém	k1gInSc4
samoobslužných	samoobslužný	k2eAgFnPc2d1
kol	kola	k1gFnPc2
Cyklocity	Cyklocit	k1gInPc1
v	v	k7c4
70	#num#	k4
městech	město	k1gNnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
i	i	k9
mimo	mimo	k7c4
ni	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
sdílení	sdílení	k1gNnSc2
kol	kola	k1gFnPc2
je	být	k5eAaImIp3nS
automatizovaný	automatizovaný	k2eAgInSc4d1
systém	systém	k1gInSc4
půjčování	půjčování	k1gNnSc2
kol	kolo	k1gNnPc2
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojany	stojan	k1gInPc4
s	s	k7c7
koly	kolo	k1gNnPc7
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgFnPc1d1
24	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
vloží	vložit	k5eAaPmIp3nP
do	do	k7c2
terminálu	terminál	k1gInSc2
předplacenou	předplacený	k2eAgFnSc7d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
platební	platební	k2eAgFnSc4d1
<g/>
)	)	kIx)
kartu	karta	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
uvolní	uvolnit	k5eAaPmIp3nS
kolo	kolo	k1gNnSc1
připevněné	připevněný	k2eAgNnSc1d1
ke	k	k7c3
stojanu	stojan	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
půjčené	půjčený	k2eAgNnSc1d1
kola	kolo	k1gNnPc1
mohou	moct	k5eAaImIp3nP
vrátit	vrátit	k5eAaPmF
do	do	k7c2
jakéhokoliv	jakýkoliv	k3yIgInSc2
jiného	jiný	k2eAgInSc2d1
stojanu	stojan	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
JCDecaux	JCDecaux	k1gInSc1
ve	v	k7c6
světě	svět	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
JCDecaux	JCDecaux	k1gInSc1
(	(	kIx(
<g/>
stránka	stránka	k1gFnSc1
již	již	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
,	,	kIx,
datum	datum	k1gNnSc1
zveřejnění	zveřejnění	k1gNnSc2
i	i	k8xC
přístupu	přístup	k1gInSc2
neznámé	neznámá	k1gFnSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Advertising	Advertising	k1gInSc1
bus	bus	k1gInSc1
shelters	shelters	k1gInSc1
<g/>
,	,	kIx,
JCDecaux	JCDecaux	k1gInSc1
(	(	kIx(
<g/>
stránka	stránka	k1gFnSc1
již	již	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
,	,	kIx,
datum	datum	k1gNnSc1
zveřejnění	zveřejnění	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
i	i	k8xC
přístupu	přístup	k1gInSc2
neznámé	neznámá	k1gFnSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
O	o	k7c6
společnosti	společnost	k1gFnSc6
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
JCDecaux	JCDecaux	k1gInSc1
(	(	kIx(
<g/>
stránka	stránka	k1gFnSc1
již	již	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
,	,	kIx,
datum	datum	k1gNnSc1
zveřejnění	zveřejnění	k1gNnSc2
i	i	k8xC
přístupu	přístup	k1gInSc2
neznámé	neznámá	k1gFnSc2
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Praha	Praha	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
ráda	rád	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tu	tu	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
JCDecaux	JCDecaux	k1gInSc1
funguje	fungovat	k5eAaImIp3nS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
architekt	architekt	k1gMnSc1
pražského	pražský	k2eAgInSc2d1
mobiliáře	mobiliář	k1gInSc2
<g/>
,	,	kIx,
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
↑	↑	k?
Venkovní	venkovní	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
JCDecaux	JCDecaux	k1gInSc1
<g/>
,	,	kIx,
SPEL	SPEL	kA
<g/>
↑	↑	k?
JCDecaux	JCDecaux	k1gInSc1
instaloval	instalovat	k5eAaBmAgInS
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
hodin	hodina	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
MaM	mámit	k5eAaImRp2nS
(	(	kIx(
<g/>
Marketing	marketing	k1gInSc1
<g/>
&	&	k?
<g/>
Media	medium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
-pda-	-pda-	k?
<g/>
1	#num#	k4
2	#num#	k4
Praha	Praha	k1gFnSc1
neprodlouží	prodloužit	k5eNaPmIp3nS
s	s	k7c7
JCDecaux	JCDecaux	k1gInSc4
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
zastávky	zastávka	k1gFnPc4
MHD	MHD	kA
<g/>
,	,	kIx,
MediaGuru	MediaGura	k1gFnSc4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
ČTK1	ČTK1	k1gFnSc1
2	#num#	k4
3	#num#	k4
4	#num#	k4
Firma	firma	k1gFnSc1
JCDecaux	JCDecaux	k1gInSc1
píše	psát	k5eAaImIp3nS
o	o	k7c6
korupci	korupce	k1gFnSc6
na	na	k7c6
magistrátu	magistrát	k1gInSc6
<g/>
,	,	kIx,
polepila	polepit	k5eAaPmAgFnS
zastávky	zastávka	k1gFnSc2
otevřeným	otevřený	k2eAgInSc7d1
dopisem	dopis	k1gInSc7
<g/>
,	,	kIx,
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
↑	↑	k?
Okradená	okradený	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Svaz	svaz	k1gInSc1
provozovatelů	provozovatel	k1gMnPc2
venkovní	venkovní	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
z.	z.	k?
<g/>
s.	s.	k?
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Reklamní	reklamní	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
spustily	spustit	k5eAaPmAgFnP
další	další	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
proti	proti	k7c3
pražskému	pražský	k2eAgMnSc3d1
primátorovi	primátor	k1gMnSc3
<g/>
,	,	kIx,
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
,	,	kIx,
čap	čap	k1gInSc1
<g/>
↑	↑	k?
Za	za	k7c4
otevřený	otevřený	k2eAgInSc4d1
dopis	dopis	k1gInSc4
míří	mířit	k5eAaImIp3nS
na	na	k7c4
JCDexaux	JCDexaux	k1gInSc4
trestní	trestní	k2eAgNnSc4d1
oznámení	oznámení	k1gNnSc4
<g/>
,	,	kIx,
podala	podat	k5eAaPmAgFnS
ho	on	k3xPp3gInSc4
konkurence	konkurence	k1gFnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
rsr	rsr	k?
<g/>
↑	↑	k?
Vandal	Vandal	k1gMnSc1
zničil	zničit	k5eAaPmAgMnS
zastávky	zastávka	k1gFnPc4
s	s	k7c7
dopisem	dopis	k1gInSc7
pro	pro	k7c4
Krnáčovou	Krnáčův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
JCDecaux	JCDecaux	k1gInSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
kampaň	kampaň	k1gFnSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
rsr	rsr	k?
<g/>
↑	↑	k?
František	František	k1gMnSc1
Strnad	Strnad	k1gMnSc1
<g/>
:	:	kIx,
Manažer	manažer	k1gInSc1
JCDecaux	JCDecaux	k1gInSc1
ladil	ladit	k5eAaImAgInS
noty	nota	k1gFnPc4
s	s	k7c7
politikem	politik	k1gMnSc7
ODS	ODS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
mu	on	k3xPp3gMnSc3
platila	platit	k5eAaImAgFnS
statisíce	statisíce	k1gInPc4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
20191	#num#	k4
2	#num#	k4
JCDecaux	JCDecaux	k1gInSc1
zažaluje	zažalovat	k5eAaPmIp3nS
TV	TV	kA
Barrandov	Barrandov	k1gInSc1
kvůli	kvůli	k7c3
kauze	kauza	k1gFnSc3
pronájmu	pronájem	k1gInSc2
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
,	,	kIx,
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
ČTK1	ČTK1	k1gFnSc1
2	#num#	k4
Soud	soud	k1gInSc1
zakázal	zakázat	k5eAaPmAgInS
TV	TV	kA
Barrandov	Barrandov	k1gInSc1
vysílat	vysílat	k5eAaImF
o	o	k7c4
JCDecaux	JCDecaux	k1gInSc4
kvůli	kvůli	k7c3
kauze	kauza	k1gFnSc3
reklamních	reklamní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
,	,	kIx,
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
,	,	kIx,
jpl	jpl	k?
(	(	kIx(
<g/>
Jitka	Jitka	k1gFnSc1
Venturová	Venturový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Pohyblivá	pohyblivý	k2eAgFnSc1d1
reklamní	reklamní	k2eAgFnSc1d1
<g/>
,	,	kIx,
Rencar	Rencar	k1gInSc1
Praha	Praha	k1gFnSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
v	v	k7c6
lednu	leden	k1gInSc6
2019	#num#	k4
<g/>
↑	↑	k?
http://www.jcdecaux.com/en/content/view/full/991	http://www.jcdecaux.com/en/content/view/full/991	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
JCDecaux	JCDecaux	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
JCDecaux	JCDecaux	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Web	web	k1gInSc4
společnosti	společnost	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
128059996	#num#	k4
</s>
