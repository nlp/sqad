<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
(	(	kIx(	(
<g/>
tibetsky	tibetsky	k6eAd1	tibetsky
ར	ར	k?	ར
<g/>
ྨ	ྨ	k?	ྨ
<g/>
་	་	k?	་
<g/>
ཆ	ཆ	k?	ཆ
<g/>
ུ	ུ	k?	ུ
<g/>
་	་	k?	་
<g/>
,	,	kIx,	,
Mačhu	Mačha	k1gMnSc4	Mačha
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Chuang-che	Chuanghe	k1gNnSc2	Chuang-che
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Huáng	Huáng	k1gInSc1	Huáng
Hé	hé	k0	hé
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
黄	黄	k?	黄
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
黃	黃	k?	黃
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
5464	[number]	k4	5464
km	km	kA	km
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
tok	tok	k1gInSc4	tok
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
752	[number]	k4	752
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Žlutá	žlutat	k5eAaImIp3nS	žlutat
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
doložený	doložený	k2eAgInSc1d1	doložený
před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
tisíciletími	tisíciletí	k1gNnPc7	tisíciletí
v	v	k7c6	v
období	období	k1gNnSc6	období
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
výstižně	výstižně	k6eAd1	výstižně
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
zbarvení	zbarvení	k1gNnSc4	zbarvení
řeky	řeka	k1gFnSc2	řeka
způsobené	způsobený	k2eAgInPc1d1	způsobený
unášenou	unášený	k2eAgFnSc7d1	unášená
zeminou	zemina	k1gFnSc7	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnPc1d1	střední
a	a	k8xC	a
dolní	dolní	k2eAgNnPc1d1	dolní
povodí	povodí	k1gNnPc1	povodí
Žluté	žlutý	k2eAgFnSc2d1	žlutá
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
kolébkou	kolébka	k1gFnSc7	kolébka
čínské	čínský	k2eAgFnSc2d1	čínská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
počíná	počínat	k5eAaImIp3nS	počínat
svůj	svůj	k3xOyFgInSc4	svůj
tok	tok	k1gInSc4	tok
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Čching-chaj	Čchinghaj	k1gFnSc1	Čching-chaj
na	na	k7c6	na
Tibetské	tibetský	k2eAgFnSc6d1	tibetská
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
4800	[number]	k4	4800
m	m	kA	m
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Jagra	Jagro	k1gNnSc2	Jagro
Dagze	Dagze	k1gFnSc2	Dagze
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
úbočí	úbočí	k1gNnSc6	úbočí
pohoří	pohoří	k1gNnSc4	pohoří
Bajan	bajan	k1gMnSc1	bajan
Char	Char	k1gMnSc1	Char
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
část	část	k1gFnSc4	část
horského	horský	k2eAgNnSc2d1	horské
pásma	pásmo	k1gNnSc2	pásmo
Kchun-lun	Kchununa	k1gFnPc2	Kchun-luna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
protíná	protínat	k5eAaImIp3nS	protínat
kotlinu	kotlina	k1gFnSc4	kotlina
Odon	Odona	k1gFnPc2	Odona
Tala	Tal	k1gInSc2	Tal
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
velkými	velký	k2eAgNnPc7d1	velké
jezery	jezero	k1gNnPc7	jezero
Ngoring	Ngoring	k1gInSc1	Ngoring
a	a	k8xC	a
Gyaring	Gyaring	k1gInSc1	Gyaring
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměrného	průměrný	k2eAgInSc2d1	průměrný
spádu	spád	k1gInSc2	spád
10	[number]	k4	10
‰	‰	k?	‰
a	a	k8xC	a
klikatí	klikatit	k5eAaImIp3nS	klikatit
se	s	k7c7	s
soutěskami	soutěska	k1gFnPc7	soutěska
mezi	mezi	k7c7	mezi
jihovýchodními	jihovýchodní	k2eAgInPc7d1	jihovýchodní
výběžky	výběžek	k1gInPc7	výběžek
Kchun-lunu	Kchunun	k1gInSc2	Kchun-lun
a	a	k8xC	a
Nanyšanu	Nanyšan	k1gInSc2	Nanyšan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
peřeje	peřej	k1gInPc4	peřej
a	a	k8xC	a
vodopády	vodopád	k1gInPc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
Lan-čou	Lan-čá	k1gFnSc4	Lan-čá
je	být	k5eAaImIp3nS	být
zadržena	zadržet	k5eAaPmNgFnS	zadržet
přehradou	přehrada	k1gFnSc7	přehrada
Liou-ťia-sia	Liou-ťiaius	k1gMnSc2	Liou-ťia-sius
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
zmnožuje	zmnožovat	k5eAaImIp3nS	zmnožovat
svůj	svůj	k3xOyFgInSc4	svůj
průtok	průtok	k1gInSc4	průtok
o	o	k7c4	o
vody	voda	k1gFnPc4	voda
řek	řeka	k1gFnPc2	řeka
Tchao	Tchao	k1gNnSc4	Tchao
a	a	k8xC	a
Chuang	Chuang	k1gInSc4	Chuang
a	a	k8xC	a
opouští	opouštět	k5eAaImIp3nP	opouštět
hory	hora	k1gFnPc1	hora
jako	jako	k8xS	jako
velká	velký	k2eAgFnSc1d1	velká
horská	horský	k2eAgFnSc1d1	horská
řeka	řeka	k1gFnSc1	řeka
s	s	k7c7	s
bouřlivým	bouřlivý	k2eAgInSc7d1	bouřlivý
tokem	tok	k1gInSc7	tok
<g/>
.	.	kIx.	.
</s>
<s>
Stáčí	stáčet	k5eAaImIp3nS	stáčet
se	se	k3xPyFc4	se
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
mohutný	mohutný	k2eAgInSc1d1	mohutný
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
sever	sever	k1gInSc4	sever
po	po	k7c6	po
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
pusté	pustý	k2eAgFnPc1d1	pustá
plošiny	plošina	k1gFnPc1	plošina
Ordos	Ordosa	k1gFnPc2	Ordosa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
východ	východ	k1gInSc4	východ
přes	přes	k7c4	přes
rovinu	rovina	k1gFnSc4	rovina
Che-tao	Cheao	k6eAd1	Che-tao
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
za	za	k7c7	za
Pao-tchou	Paocha	k1gFnSc7	Pao-tcha
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
jih	jih	k1gInSc4	jih
přes	přes	k7c4	přes
planinu	planina	k1gFnSc4	planina
Löss	Lössa	k1gFnPc2	Lössa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
silně	silně	k6eAd1	silně
eroduje	erodovat	k5eAaImIp3nS	erodovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
prodírá	prodírat	k5eAaImIp3nS	prodírat
až	až	k9	až
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
mocnými	mocný	k2eAgFnPc7d1	mocná
sprašovými	sprašový	k2eAgFnPc7d1	sprašová
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hluboká	hluboký	k2eAgNnPc4d1	hluboké
úzká	úzký	k2eAgNnPc4d1	úzké
údolí	údolí	k1gNnPc4	údolí
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
přiléhavě	přiléhavě	k6eAd1	přiléhavě
zvaném	zvaný	k2eAgNnSc6d1	zvané
Chu-kchou	Chuchý	k2eAgFnSc4d1	Chu-kchý
neboli	neboli	k8xC	neboli
Hrdlo	hrdlo	k1gNnSc1	hrdlo
konvice	konvice	k1gFnSc2	konvice
(	(	kIx(	(
<g/>
壶	壶	k?	壶
<g/>
,	,	kIx,	,
Húkǒ	Húkǒ	k1gFnSc1	Húkǒ
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
působivé	působivý	k2eAgFnPc4d1	působivá
peřeje	peřej	k1gFnPc4	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mezi	mezi	k7c7	mezi
skalami	skála	k1gFnPc7	skála
zužuje	zužovat	k5eAaImIp3nS	zužovat
z	z	k7c2	z
500	[number]	k4	500
na	na	k7c4	na
necelých	celý	k2eNgInPc2d1	necelý
50	[number]	k4	50
m	m	kA	m
a	a	k8xC	a
propadá	propadat	k5eAaPmIp3nS	propadat
se	se	k3xPyFc4	se
vodopádem	vodopád	k1gInSc7	vodopád
o	o	k7c4	o
17	[number]	k4	17
m	m	kA	m
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
polopouštních	polopouštní	k2eAgFnPc6d1	polopouštní
a	a	k8xC	a
stepním	stepní	k2eAgFnPc3d1	stepní
úsecích	úsek	k1gInPc6	úsek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
km	km	kA	km
se	se	k3xPyFc4	se
vodnost	vodnost	k1gFnSc1	vodnost
řeky	řeka	k1gFnSc2	řeka
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jen	jen	k9	jen
nevýznamně	významně	k6eNd1	významně
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
průsaky	průsak	k1gInPc4	průsak
dokonce	dokonce	k9	dokonce
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
spád	spád	k1gInSc4	spád
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
0,74	[number]	k4	0,74
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Přijímá	přijímat	k5eAaImIp3nS	přijímat
Wu-ding-che	Wuingh	k1gFnPc4	Wu-ding-ch
<g/>
,	,	kIx,	,
Wej-che	Wejh	k1gInPc4	Wej-ch
a	a	k8xC	a
Fen-che	Fenh	k1gInPc4	Fen-ch
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
také	také	k9	také
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnoho	mnoho	k4c4	mnoho
nánosů	nános	k1gInPc2	nános
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Tchung-kuanu	Tchunguan	k1gInSc2	Tchung-kuan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijímá	přijímat	k5eAaImIp3nS	přijímat
řeku	řeka	k1gFnSc4	řeka
Wej	Wej	k1gFnPc2	Wej
a	a	k8xC	a
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Šan-sia	Šania	k1gFnSc1	Šan-sia
údolím	údolí	k1gNnSc7	údolí
San-men-sia	Sanenium	k1gNnSc2	San-men-sium
<g/>
,	,	kIx,	,
tj	tj	kA	tj
Tři	tři	k4xCgFnPc1	tři
brány	brána	k1gFnPc1	brána
(	(	kIx(	(
<g/>
三	三	k?	三
Sā	Sā	k1gFnSc1	Sā
<g/>
)	)	kIx)	)
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
přehradou	přehrada	k1gFnSc7	přehrada
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
nová	nový	k2eAgFnSc1d1	nová
přehrada	přehrada	k1gFnSc1	přehrada
Siao-lang-ti	Siaoang-ť	k1gFnSc2	Siao-lang-ť
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
700	[number]	k4	700
<g/>
km	km	kA	km
protéká	protékat	k5eAaImIp3nS	protékat
Velkou	velký	k2eAgFnSc7d1	velká
čínskou	čínský	k2eAgFnSc7d1	čínská
rovinou	rovina	k1gFnSc7	rovina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
míjí	míjet	k5eAaImIp3nS	míjet
starobylé	starobylý	k2eAgNnSc1d1	starobylé
město	město	k1gNnSc1	město
Luo-jang	Luoanga	k1gFnPc2	Luo-janga
<g/>
,	,	kIx,	,
od	od	k7c2	od
Čeng-čou	Čeng-ča	k1gMnSc7	Čeng-ča
a	a	k8xC	a
Kchaj-fengu	Kchajeng	k1gInSc3	Kchaj-feng
pak	pak	k6eAd1	pak
pozvolna	pozvolna	k6eAd1	pozvolna
(	(	kIx(	(
<g/>
spád	spád	k1gInSc1	spád
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
0,12	[number]	k4	0,12
‰	‰	k?	‰
<g/>
)	)	kIx)	)
teče	téct	k5eAaImIp3nS	téct
širou	širý	k2eAgFnSc7d1	širá
náplavovou	náplavový	k2eAgFnSc7d1	náplavová
rovinou	rovina	k1gFnSc7	rovina
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
skrze	skrze	k?	skrze
město	město	k1gNnSc1	město
Ťi-nan	Ťian	k1gMnSc1	Ťi-nan
a	a	k8xC	a
vylévá	vylévat	k5eAaImIp3nS	vylévat
se	se	k3xPyFc4	se
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
deltou	delta	k1gFnSc7	delta
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Pochaj	Pochaj	k1gFnSc1	Pochaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc1	změna
průběhu	průběh	k1gInSc2	průběh
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ploché	plochý	k2eAgFnSc6d1	plochá
krajině	krajina	k1gFnSc6	krajina
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
protržení	protržení	k1gNnSc6	protržení
hrází	hráz	k1gFnPc2	hráz
řeka	řeka	k1gFnSc1	řeka
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
přibližně	přibližně	k6eAd1	přibližně
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
dramaticky	dramaticky	k6eAd1	dramaticky
svůj	svůj	k3xOyFgInSc4	svůj
běh	běh	k1gInSc4	běh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sedm	sedm	k4xCc1	sedm
povodní	povodeň	k1gFnPc2	povodeň
mělo	mít	k5eAaImAgNnS	mít
katastrofální	katastrofální	k2eAgInPc4d1	katastrofální
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Hoře	hora	k1gFnSc3	hora
Číny	Čína	k1gFnSc2	Čína
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
602	[number]	k4	602
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tekla	téct	k5eAaImAgFnS	téct
od	od	k7c2	od
dnešního	dnešní	k2eAgInSc2d1	dnešní
Čeng-čou	Čeng-čý	k2eAgFnSc4d1	Čeng-čý
podél	podél	k7c2	podél
východního	východní	k2eAgNnSc2d1	východní
úpatí	úpatí	k1gNnSc2	úpatí
hor	hora	k1gFnPc2	hora
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
měnila	měnit	k5eAaImAgFnS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
tok	tok	k1gInSc4	tok
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
11	[number]	k4	11
<g/>
,	,	kIx,	,
893	[number]	k4	893
<g/>
,	,	kIx,	,
1048	[number]	k4	1048
a	a	k8xC	a
1194	[number]	k4	1194
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
něco	něco	k3yInSc4	něco
severněji	severně	k6eAd2	severně
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgFnPc1d1	výrazná
změny	změna	k1gFnPc1	změna
nastaly	nastat	k5eAaPmAgFnP	nastat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1289	[number]	k4	1289
a	a	k8xC	a
1324	[number]	k4	1324
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
obrátila	obrátit	k5eAaPmAgFnS	obrátit
od	od	k7c2	od
Kchaj-fengu	Kchajeng	k1gInSc2	Kchaj-feng
svůj	svůj	k3xOyFgInSc4	svůj
tok	tok	k1gInSc4	tok
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Chuaj	Chuaj	k1gFnSc1	Chuaj
se	se	k3xPyFc4	se
vlévala	vlévat	k5eAaImAgFnS	vlévat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Šantungského	šantungský	k2eAgInSc2d1	šantungský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
nynějším	nynější	k2eAgNnSc6d1	nynější
korytě	koryto	k1gNnSc6	koryto
vyjma	vyjma	k7c2	vyjma
krátkého	krátký	k2eAgNnSc2d1	krátké
období	období	k1gNnSc2	období
let	let	k1gInSc4	let
1938	[number]	k4	1938
až	až	k9	až
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyhození	vyhození	k1gNnSc4	vyhození
hrází	hráz	k1gFnPc2	hráz
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
za	za	k7c2	za
války	válka	k1gFnSc2	válka
vtékala	vtékat	k5eAaImAgFnS	vtékat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Šantungského	šantungský	k2eAgInSc2d1	šantungský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
v	v	k7c6	v
horské	horský	k2eAgFnSc6d1	horská
části	část	k1gFnSc6	část
povodí	povodit	k5eAaPmIp3nP	povodit
i	i	k9	i
sněhové	sněhový	k2eAgFnPc1d1	sněhová
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
není	být	k5eNaImIp3nS	být
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
kratší	krátký	k2eAgInSc4d2	kratší
než	než	k8xS	než
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-ťiang	-ťiang	k1gInSc1	-ťiang
<g/>
,	,	kIx,	,
vodností	vodnost	k1gFnSc7	vodnost
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
zdaleka	zdaleka	k6eAd1	zdaleka
rovnat	rovnat	k5eAaImF	rovnat
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
ze	z	k7c2	z
752	[number]	k4	752
000	[number]	k4	000
km2	km2	k4	km2
rozlehlého	rozlehlý	k2eAgNnSc2d1	rozlehlé
povodí	povodí	k1gNnSc2	povodí
Žluté	žlutý	k2eAgFnSc2d1	žlutá
řeky	řeka	k1gFnSc2	řeka
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
oblasti	oblast	k1gFnPc4	oblast
neoplývající	oplývající	k2eNgFnPc1d1	neoplývající
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Průtok	průtok	k1gInSc1	průtok
Žluté	žlutý	k2eAgFnSc2d1	žlutá
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
<g/>
,	,	kIx,	,
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
až	až	k9	až
2000	[number]	k4	2000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Postupně	postupně	k6eAd1	postupně
podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
u	u	k7c2	u
Lan-čou	Lan-čá	k1gFnSc4	Lan-čá
1105	[number]	k4	1105
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
u	u	k7c2	u
Pao-tchou	Paocha	k1gFnSc7	Pao-tcha
818	[number]	k4	818
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
u	u	k7c2	u
Le-kchou	Lecha	k1gFnSc7	Le-kcha
1500	[number]	k4	1500
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
vodních	vodní	k2eAgInPc2d1	vodní
stavů	stav	k1gInPc2	stav
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
v	v	k7c6	v
období	období	k1gNnSc6	období
monzunů	monzun	k1gInPc2	monzun
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
průtok	průtok	k1gInSc1	průtok
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
běžného	běžný	k2eAgInSc2d1	běžný
roku	rok	k1gInSc2	rok
22	[number]	k4	22
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
při	při	k7c6	při
katastrofálních	katastrofální	k2eAgFnPc6d1	katastrofální
povodních	povodeň	k1gFnPc6	povodeň
až	až	k9	až
30	[number]	k4	30
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Roční	roční	k2eAgInSc1d1	roční
odtok	odtok	k1gInSc1	odtok
činí	činit	k5eAaImIp3nS	činit
50	[number]	k4	50
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povodní	povodeň	k1gFnPc2	povodeň
stoupá	stoupat	k5eAaImIp3nS	stoupat
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
soutěskách	soutěska	k1gFnPc6	soutěska
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
a	a	k8xC	a
na	na	k7c6	na
rovinách	rovina	k1gFnPc6	rovina
o	o	k7c4	o
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
m.	m.	k?	m.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
vody	voda	k1gFnSc2	voda
nejméně	málo	k6eAd3	málo
a	a	k8xC	a
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
na	na	k7c4	na
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
na	na	k7c4	na
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
vody	voda	k1gFnSc2	voda
řeka	řeka	k1gFnSc1	řeka
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
vysychá	vysychat	k5eAaImIp3nS	vysychat
800	[number]	k4	800
kilometrů	kilometr	k1gInPc2	kilometr
před	před	k7c7	před
ústím	ústí	k1gNnSc7	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
fenoménem	fenomén	k1gInSc7	fenomén
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dal	dát	k5eAaPmAgInS	dát
řece	řeka	k1gFnSc3	řeka
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
však	však	k9	však
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
částic	částice	k1gFnPc2	částice
zeminy	zemina	k1gFnSc2	zemina
unášených	unášený	k2eAgFnPc2d1	unášená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
plavenin	plavenina	k1gFnPc2	plavenina
činí	činit	k5eAaImIp3nS	činit
36	[number]	k4	36
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
světových	světový	k2eAgFnPc2d1	světová
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Nárazovitě	nárazovitě	k6eAd1	nárazovitě
přitom	přitom	k6eAd1	přitom
tyto	tento	k3xDgFnPc4	tento
hodnoty	hodnota	k1gFnPc4	hodnota
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
600	[number]	k4	600
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
!	!	kIx.	!
</s>
<s>
Každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
tak	tak	k6eAd1	tak
řeka	řeka	k1gFnSc1	řeka
přemístí	přemístit	k5eAaPmIp3nS	přemístit
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnSc2	miliarda
tun	tuna	k1gFnPc2	tuna
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
odplývají	odplývat	k5eAaImIp3nP	odplývat
do	do	k7c2	do
Pochajského	Pochajský	k2eAgInSc2d1	Pochajský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
o	o	k7c4	o
290	[number]	k4	290
m.	m.	k?	m.
Zbylá	zbylý	k2eAgFnSc1d1	zbylá
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
plavenin	plavenina	k1gFnPc2	plavenina
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednak	jednak	k8xC	jednak
zanáší	zanášet	k5eAaImIp3nP	zanášet
přehrady	přehrada	k1gFnPc4	přehrada
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
úroveň	úroveň	k1gFnSc1	úroveň
říčního	říční	k2eAgNnSc2d1	říční
dna	dno	k1gNnSc2	dno
o	o	k7c4	o
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeka	řeka	k1gFnSc1	řeka
sevřená	sevřený	k2eAgFnSc1d1	sevřená
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc6	staletí
navyšovanými	navyšovaný	k2eAgFnPc7d1	navyšovaná
ochrannými	ochranný	k2eAgFnPc7d1	ochranná
hrázemi	hráz	k1gFnPc7	hráz
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
3	[number]	k4	3
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Hráze	hráze	k1gFnSc1	hráze
vysoké	vysoká	k1gFnSc2	vysoká
5	[number]	k4	5
až	až	k9	až
12	[number]	k4	12
m	m	kA	m
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
včetně	včetně	k7c2	včetně
přítoků	přítok	k1gInPc2	přítok
délky	délka	k1gFnSc2	délka
5000	[number]	k4	5000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povodně	povodeň	k1gFnPc1	povodeň
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
vytržení	vytržení	k1gNnSc3	vytržení
se	se	k3xPyFc4	se
z	z	k7c2	z
hrází	hráz	k1gFnPc2	hráz
<g/>
,	,	kIx,	,
škody	škoda	k1gFnPc1	škoda
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
a	a	k8xC	a
majetku	majetek	k1gInSc6	majetek
jsou	být	k5eAaImIp3nP	být
nedozírné	dozírný	k2eNgFnPc1d1	nedozírná
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
povodně	povodně	k6eAd1	povodně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
a	a	k8xC	a
1931	[number]	k4	1931
si	se	k3xPyFc3	se
každá	každý	k3xTgFnSc1	každý
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
činí	činit	k5eAaImIp3nS	činit
nejhoršími	zlý	k2eAgFnPc7d3	nejhorší
živelními	živelní	k2eAgFnPc7d1	živelní
pohromami	pohroma	k1gFnPc7	pohroma
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
dalo	dát	k5eAaPmAgNnS	dát
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
kuomintangské	kuomintangský	k2eAgNnSc4d1	kuomintangský
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
zoufalém	zoufalý	k2eAgInSc6d1	zoufalý
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
zastavení	zastavení	k1gNnSc6	zastavení
japonského	japonský	k2eAgInSc2d1	japonský
postupu	postup	k1gInSc2	postup
vyhodit	vyhodit	k5eAaPmF	vyhodit
hráze	hráz	k1gFnPc4	hráz
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
přes	přes	k7c4	přes
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
na	na	k7c6	na
rovině	rovina	k1gFnSc6	rovina
Che-tchao	Chechao	k6eAd1	Che-tchao
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
790	[number]	k4	790
km	km	kA	km
především	především	k6eAd1	především
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
čínské	čínský	k2eAgFnSc6d1	čínská
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
přehradní	přehradní	k2eAgFnPc1d1	přehradní
nádrže	nádrž	k1gFnPc1	nádrž
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
elektrárnami	elektrárna	k1gFnPc7	elektrárna
(	(	kIx(	(
<g/>
San-men-sia	Sanenia	k1gFnSc1	San-men-sia
<g/>
,	,	kIx,	,
Liou-ťia-sia	Liou-ťiaia	k1gFnSc1	Liou-ťia-sia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
hustě	hustě	k6eAd1	hustě
osídleným	osídlený	k2eAgFnPc3d1	osídlená
oblastem	oblast	k1gFnPc3	oblast
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnSc2	město
Lan-čou	Lan-čá	k1gFnSc4	Lan-čá
<g/>
,	,	kIx,	,
Jin-čchuan	Jin-čchuan	k1gInSc4	Jin-čchuan
<g/>
,	,	kIx,	,
Pao-tchou	Paochý	k2eAgFnSc4d1	Pao-tchý
<g/>
,	,	kIx,	,
Čeng-čou	Čeng-čý	k2eAgFnSc4d1	Čeng-čý
<g/>
,	,	kIx,	,
Kaj-feng	Kajeng	k1gInSc1	Kaj-feng
<g/>
,	,	kIx,	,
Ťi-nan	Ťian	k1gInSc1	Ťi-nan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
polohy	poloha	k1gFnSc2	poloha
při	při	k7c6	při
Žluté	žlutý	k2eAgFnSc6d1	žlutá
řece	řeka	k1gFnSc6	řeka
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
provincie	provincie	k1gFnSc2	provincie
Che-pej	Cheej	k1gFnSc1	Che-pej
a	a	k8xC	a
Che-nan	Chean	k1gInSc1	Che-nan
–	–	k?	–
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Severně	severně	k6eAd1	severně
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jižně	jižně	k6eAd1	jižně
řeky	řeka	k1gFnSc2	řeka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Х	Х	k?	Х
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
řek	řeka	k1gFnPc2	řeka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Žlutá	žlutat	k5eAaImIp3nS	žlutat
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Žlutá	žlutat	k5eAaImIp3nS	žlutat
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
