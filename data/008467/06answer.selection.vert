<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
řeka	řeka	k1gFnSc1	řeka
(	(	kIx(	(
<g/>
tibetsky	tibetsky	k6eAd1	tibetsky
ར	ར	k?	ར
<g/>
ྨ	ྨ	k?	ྨ
<g/>
་	་	k?	་
<g/>
ཆ	ཆ	k?	ཆ
<g/>
ུ	ུ	k?	ུ
<g/>
་	་	k?	་
<g/>
,	,	kIx,	,
Mačhu	Mačha	k1gMnSc4	Mačha
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Chuang-che	Chuanghe	k1gNnSc2	Chuang-che
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Huáng	Huáng	k1gInSc1	Huáng
Hé	hé	k0	hé
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
黄	黄	k?	黄
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
黃	黃	k?	黃
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
5464	[number]	k4	5464
km	km	kA	km
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
tok	tok	k1gInSc4	tok
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
