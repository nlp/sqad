<s>
Kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
76	[number]	k4	76
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
–	–	k?	–
okres	okres	k1gInSc1	okres
Jeseník	Jeseník	k1gInSc1	Jeseník
–	–	k?	–
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
