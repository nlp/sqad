<s>
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
В	В	k?	В
(	(	kIx(	(
<g/>
Volga	Volga	k1gFnSc1	Volga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tatarsky	tatarsky	k6eAd1	tatarsky
İ	İ	k?	İ
<g/>
,	,	kIx,	,
İ	İ	k?	İ
nebo	nebo	k8xC	nebo
İ	İ	k?	İ
<g/>
,	,	kIx,	,
mordvinsky	mordvinsky	k6eAd1	mordvinsky
Р	Р	k?	Р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veletok	veletok	k1gInSc1	veletok
protékající	protékající	k2eAgInSc1d1	protékající
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
evropskou	evropský	k2eAgFnSc7d1	Evropská
částí	část	k1gFnSc7	část
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
3534	[number]	k4	3534
km	km	kA	km
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
a	a	k8xC	a
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
řeka	řeka	k1gFnSc1	řeka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
odtok	odtok	k1gInSc4	odtok
do	do	k7c2	do
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>

