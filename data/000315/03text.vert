<s>
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
В	В	k?	В
(	(	kIx(	(
<g/>
Volga	Volga	k1gFnSc1	Volga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tatarsky	tatarsky	k6eAd1	tatarsky
İ	İ	k?	İ
<g/>
,	,	kIx,	,
İ	İ	k?	İ
nebo	nebo	k8xC	nebo
İ	İ	k?	İ
<g/>
,	,	kIx,	,
mordvinsky	mordvinsky	k6eAd1	mordvinsky
Р	Р	k?	Р
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veletok	veletok	k1gInSc1	veletok
protékající	protékající	k2eAgInSc1d1	protékající
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
evropskou	evropský	k2eAgFnSc7d1	Evropská
částí	část	k1gFnSc7	část
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
3534	[number]	k4	3534
km	km	kA	km
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
a	a	k8xC	a
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
řeka	řeka	k1gFnSc1	řeka
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
odtok	odtok	k1gInSc4	odtok
do	do	k7c2	do
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
velkých	velký	k2eAgFnPc2d1	velká
přehrad	přehrada	k1gFnPc2	přehrada
a	a	k8xC	a
plavební	plavební	k2eAgInPc1d1	plavební
kanály	kanál	k1gInPc1	kanál
ji	on	k3xPp3gFnSc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
Baltským	baltský	k2eAgMnSc7d1	baltský
<g/>
,	,	kIx,	,
Bílým	bílý	k2eAgMnSc7d1	bílý
<g/>
,	,	kIx,	,
Azovským	azovský	k2eAgNnSc7d1	Azovské
i	i	k8xC	i
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
stavbou	stavba	k1gFnSc7	stavba
přehrad	přehrada	k1gFnPc2	přehrada
činila	činit	k5eAaImAgFnS	činit
její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
3690	[number]	k4	3690
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
380	[number]	k4	380
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Volha	Volha	k1gFnSc1	Volha
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
spád	spád	k1gInSc4	spád
<g/>
,	,	kIx,	,
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
toku	tok	k1gInSc6	tok
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
0,07	[number]	k4	0,07
promile	promile	k1gNnPc2	promile
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
má	mít	k5eAaImIp3nS	mít
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
Rusy	Rus	k1gMnPc4	Rus
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Matka	matka	k1gFnSc1	matka
Volha	Volha	k1gFnSc1	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
-	-	kIx~	-
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Oky	oka	k1gFnSc2	oka
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
-	-	kIx~	-
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Oky	oka	k1gFnSc2	oka
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Kamy	Kamy	k?	Kamy
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
-	-	kIx~	-
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Kamy	Kamy	k?	Kamy
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pramen	pramen	k1gInSc1	pramen
Volhy	Volha	k1gFnSc2	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Volha	Volha	k1gFnSc1	Volha
pramení	pramenit	k5eAaImIp3nS	pramenit
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
ve	v	k7c6	v
Valdajské	Valdajský	k2eAgFnSc6d1	Valdajská
vrchovině	vrchovina	k1gFnSc6	vrchovina
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Volgo-Verchovje	Volgo-Verchovj	k1gFnSc2	Volgo-Verchovj
v	v	k7c6	v
Tverské	Tverský	k2eAgFnSc6d1	Tverská
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
228	[number]	k4	228
m.	m.	k?	m.
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
nevelkých	velký	k2eNgNnPc2d1	nevelké
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Verchit	Verchit	k1gInSc1	Verchit
<g/>
,	,	kIx,	,
Stěrž	Stěrž	k1gFnSc1	Stěrž
<g/>
,	,	kIx,	,
Vselug	Vselug	k1gMnSc1	Vselug
<g/>
,	,	kIx,	,
Peno	Peno	k1gMnSc1	Peno
<g/>
,	,	kIx,	,
Volgo	Volgo	k1gMnSc1	Volgo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odtoku	odtok	k1gInSc2	odtok
z	z	k7c2	z
posledního	poslední	k2eAgMnSc2d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Hornovolžská	Hornovolžský	k2eAgFnSc1d1	Hornovolžský
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
řeka	řeka	k1gFnSc1	řeka
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
obtéká	obtékat	k5eAaImIp3nS	obtékat
Moskvu	Moskva	k1gFnSc4	Moskva
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
historickým	historický	k2eAgNnSc7d1	historické
městem	město	k1gNnSc7	město
Tver	Tvera	k1gFnPc2	Tvera
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
Dubna	duben	k1gInSc2	duben
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
Ústavem	ústav	k1gInSc7	ústav
jaderných	jaderný	k2eAgInPc2d1	jaderný
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
Moskevský	moskevský	k2eAgInSc4d1	moskevský
průplav	průplav	k1gInSc4	průplav
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc4d1	spojující
řeku	řeka	k1gFnSc4	řeka
s	s	k7c7	s
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Tverem	Tvero	k1gNnSc7	Tvero
a	a	k8xC	a
Rybinskem	Rybinsko	k1gNnSc7	Rybinsko
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
tři	tři	k4xCgFnPc4	tři
přehrady	přehrada	k1gFnPc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Volžská	volžský	k2eAgFnSc1d1	Volžská
s	s	k7c7	s
hrází	hráz	k1gFnSc7	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
u	u	k7c2	u
Ivankova	Ivankův	k2eAgNnSc2d1	Ivankův
<g/>
,	,	kIx,	,
Ugličská	Ugličský	k2eAgFnSc1d1	Ugličský
s	s	k7c7	s
hrází	hráz	k1gFnSc7	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
u	u	k7c2	u
Ugliče	Uglič	k1gInSc2	Uglič
a	a	k8xC	a
Rybinská	Rybinský	k2eAgFnSc1d1	Rybinský
s	s	k7c7	s
hrází	hráz	k1gFnSc7	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
u	u	k7c2	u
Rybinsku	Rybinsko	k1gNnSc6	Rybinsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
ústí	ústit	k5eAaImIp3nS	ústit
Volžsko-baltský	volžskoaltský	k2eAgInSc1d1	volžsko-baltský
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Rybinsk	Rybinsk	k1gInSc1	Rybinsk
a	a	k8xC	a
Jaroslavl	Jaroslavl	k1gFnSc1	Jaroslavl
a	a	k8xC	a
pod	pod	k7c4	pod
Kostromou	Kostromý	k2eAgFnSc4d1	Kostromý
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
dolině	dolina	k1gFnSc6	dolina
mezi	mezi	k7c7	mezi
vysokými	vysoký	k2eAgInPc7d1	vysoký
břehy	břeh	k1gInPc7	břeh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
protíná	protínat	k5eAaImIp3nS	protínat
Ugličsko-danplovskou	Ugličskoanplovský	k2eAgFnSc4d1	Ugličsko-danplovský
a	a	k8xC	a
Galičsko-čuchlomskou	Galičsko-čuchlomský	k2eAgFnSc4d1	Galičsko-čuchlomský
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přes	přes	k7c4	přes
Unženskou	Unženský	k2eAgFnSc4d1	Unženský
a	a	k8xC	a
Balachninskou	Balachninský	k2eAgFnSc4d1	Balachninský
nížinu	nížina	k1gFnSc4	nížina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Gorodce	Gorodce	k1gMnSc2	Gorodce
nad	nad	k7c7	nad
Nižným	nižný	k2eAgInSc7d1	nižný
Novgorodem	Novgorod	k1gInSc7	Novgorod
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Gorkovské	gorkovský	k2eAgFnSc2d1	gorkovský
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Okou	oka	k1gFnSc7	oka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
vodnější	vodný	k2eAgNnSc1d2	vodný
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
podél	podél	k7c2	podél
severního	severní	k2eAgInSc2d1	severní
okraje	okraj	k1gInSc2	okraj
Povolžské	povolžský	k2eAgFnSc2d1	Povolžská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
břeh	břeh	k1gInSc1	břeh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
levý	levý	k2eAgMnSc1d1	levý
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Čeboksar	Čeboksara	k1gFnPc2	Čeboksara
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Čeboksarské	Čeboksarský	k2eAgFnSc2d1	Čeboksarský
přehrady	přehrada	k1gFnSc2	přehrada
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
největšího	veliký	k2eAgInSc2d3	veliký
přítoku	přítok	k1gInSc2	přítok
Kamy	Kamy	k?	Kamy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stává	stávat	k5eAaImIp3nS	stávat
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
Povolžskou	povolžský	k2eAgFnSc7d1	Povolžská
vrchovinou	vrchovina	k1gFnSc7	vrchovina
a	a	k8xC	a
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tatarstánu	Tatarstán	k1gInSc2	Tatarstán
Kazaně	Kazaň	k1gFnSc2	Kazaň
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Togliatti	Togliatť	k1gFnSc2	Togliatť
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
automobilkou	automobilka	k1gFnSc7	automobilka
nad	nad	k7c7	nad
Samarskou	samarský	k2eAgFnSc7d1	Samarská
lukou	luka	k1gFnSc7	luka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
,	,	kIx,	,
když	když	k8xS	když
obtéká	obtékat	k5eAaImIp3nS	obtékat
Žiguljovské	Žiguljovský	k2eAgFnPc4d1	Žiguljovský
hory	hora	k1gFnPc4	hora
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
Volžská	volžský	k2eAgFnSc1d1	Volžská
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kujbyševské	kujbyševský	k2eAgFnSc2d1	Kujbyševská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vzdutí	vzdutí	k1gNnSc1	vzdutí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
550	[number]	k4	550
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
tok	tok	k1gInSc1	tok
Kamy	Kamy	k?	Kamy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
město	město	k1gNnSc1	město
Uljanovsk	Uljanovsk	k1gInSc1	Uljanovsk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
Balakovo	Balakův	k2eAgNnSc4d1	Balakovo
pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
hráz	hráz	k1gFnSc1	hráz
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Saratovské	Saratovský	k2eAgFnSc2d1	Saratovská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
řeky	řeka	k1gFnSc2	řeka
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc4	město
Samara	Samara	k1gFnSc1	Samara
a	a	k8xC	a
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
toku	tok	k1gInSc6	tok
Volhy	Volha	k1gFnSc2	Volha
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
měst	město	k1gNnPc2	město
Marx	Marx	k1gMnSc1	Marx
a	a	k8xC	a
Engels	Engels	k1gMnSc1	Engels
sídlili	sídlit	k5eAaImAgMnP	sídlit
němečtí	německý	k2eAgMnPc1d1	německý
kolonisté	kolonista	k1gMnPc1	kolonista
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
povolžští	povolžský	k2eAgMnPc1d1	povolžský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přesídlení	přesídlení	k1gNnSc2	přesídlení
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc1	přítok
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
vodné	vodný	k2eAgNnSc1d1	vodné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
naproti	naproti	k7c3	naproti
městu	město	k1gNnSc3	město
Engels	Engels	k1gMnSc1	Engels
leží	ležet	k5eAaImIp3nS	ležet
Saratov	Saratov	k1gInSc4	Saratov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
600	[number]	k4	600
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Volgogradská	volgogradský	k2eAgFnSc1d1	Volgogradská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
přehradou	přehrada	k1gFnSc7	přehrada
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Volgograd	Volgograd	k1gInSc1	Volgograd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
rozhodujících	rozhodující	k2eAgFnPc2d1	rozhodující
bitev	bitva	k1gFnPc2	bitva
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
21	[number]	k4	21
km	km	kA	km
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
první	první	k4xOgNnSc1	první
východní	východní	k2eAgNnSc1d1	východní
rameno	rameno	k1gNnSc1	rameno
Achtuba	Achtuba	k1gFnSc1	Achtuba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
537	[number]	k4	537
km	km	kA	km
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
níže	nízce	k6eAd2	nízce
ústí	ústit	k5eAaImIp3nS	ústit
100	[number]	k4	100
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
Volžsko-donský	volžskoonský	k2eAgInSc1d1	volžsko-donský
průplav	průplav	k1gInSc1	průplav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Volhu	Volha	k1gFnSc4	Volha
se	s	k7c7	s
západnějším	západní	k2eAgMnSc7d2	západnější
Donem	Don	k1gMnSc7	Don
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k9	i
s	s	k7c7	s
Azovským	azovský	k2eAgNnSc7d1	Azovské
mořem	moře	k1gNnSc7	moře
<g/>
;	;	kIx,	;
na	na	k7c6	na
volžské	volžský	k2eAgFnSc6d1	Volžská
straně	strana	k1gFnSc6	strana
překonává	překonávat	k5eAaImIp3nS	překonávat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
88	[number]	k4	88
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
donské	donský	k2eAgFnSc6d1	Donská
straně	strana	k1gFnSc6	strana
44	[number]	k4	44
m.	m.	k?	m.
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
delta	delta	k1gFnSc1	delta
Volhy	Volha	k1gFnSc2	Volha
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
46	[number]	k4	46
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Astrachaně	Astrachaň	k1gFnSc2	Astrachaň
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
rameno	rameno	k1gNnSc1	rameno
Buzan	Buzana	k1gFnPc2	Buzana
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
průtoků	průtok	k1gInPc2	průtok
a	a	k8xC	a
mělkých	mělký	k2eAgFnPc2d1	mělká
říček	říčka	k1gFnPc2	říčka
<g/>
.	.	kIx.	.
</s>
<s>
Deltou	delta	k1gFnSc7	delta
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ústí	ústí	k1gNnSc1	ústí
leží	ležet	k5eAaImIp3nS	ležet
28	[number]	k4	28
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celkový	celkový	k2eAgInSc1d1	celkový
spád	spád	k1gInSc1	spád
činí	činit	k5eAaImIp3nS	činit
256	[number]	k4	256
m.	m.	k?	m.
Obrovské	obrovský	k2eAgNnSc1d1	obrovské
území	území	k1gNnSc1	území
delty	delta	k1gFnSc2	delta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
hnízdištěm	hnízdiště	k1gNnSc7	hnízdiště
stěhovavých	stěhovavý	k2eAgMnPc2d1	stěhovavý
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
je	být	k5eAaImIp3nS	být
také	také	k9	také
střediskem	středisko	k1gNnSc7	středisko
výroby	výroba	k1gFnSc2	výroba
kaviáru	kaviár	k1gInSc2	kaviár
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
200	[number]	k4	200
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Levých	levá	k1gFnPc2	levá
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vodnější	vodný	k2eAgInPc1d2	vodný
než	než	k8xS	než
pravé	pravý	k2eAgInPc1d1	pravý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
pod	pod	k7c7	pod
Kamyšinem	Kamyšin	k1gInSc7	Kamyšin
již	již	k6eAd1	již
žádné	žádný	k3yNgInPc4	žádný
přítoky	přítok	k1gInPc4	přítok
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
<g/>
.	.	kIx.	.
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
-	-	kIx~	-
Seližarovka	Seližarovka	k1gFnSc1	Seližarovka
<g/>
,	,	kIx,	,
Tverca	Tverca	k1gFnSc1	Tverca
<g/>
,	,	kIx,	,
Mologa	Mologa	k1gFnSc1	Mologa
<g/>
,	,	kIx,	,
Šeksna	Šeksna	k1gFnSc1	Šeksna
<g/>
,	,	kIx,	,
Unža	Unža	k1gFnSc1	Unža
střední	střední	k1gMnSc1	střední
tok	tok	k1gInSc1	tok
-	-	kIx~	-
Oka	oka	k1gFnSc1	oka
<g/>
,	,	kIx,	,
Sura	Sura	k1gFnSc1	Sura
<g/>
,	,	kIx,	,
Vetluga	Vetluga	k1gFnSc1	Vetluga
<g/>
,	,	kIx,	,
Svijaga	Svijaga	k1gFnSc1	Svijaga
dolní	dolní	k2eAgFnSc1d1	dolní
tok	tok	k1gInSc1	tok
-	-	kIx~	-
Kama	Kama	k1gFnSc1	Kama
<g/>
,	,	kIx,	,
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
<g />
.	.	kIx.	.
</s>
<s>
Irgiz	Irgiz	k1gMnSc1	Irgiz
<g/>
,	,	kIx,	,
Jeruslan	Jeruslan	k1gMnSc1	Jeruslan
delta	delta	k1gNnSc2	delta
(	(	kIx(	(
<g/>
ramena	rameno	k1gNnSc2	rameno
<g/>
)	)	kIx)	)
-	-	kIx~	-
Bachtěmir	Bachtěmir	k1gMnSc1	Bachtěmir
<g/>
,	,	kIx,	,
Kamyzjak	Kamyzjak	k1gMnSc1	Kamyzjak
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Volha	Volha	k1gFnSc1	Volha
<g/>
,	,	kIx,	,
Bolda	Bolda	k1gMnSc1	Bolda
<g/>
,	,	kIx,	,
Buzan	Buzan	k1gMnSc1	Buzan
<g/>
,	,	kIx,	,
Achtuba	Achtuba	k1gMnSc1	Achtuba
Základními	základní	k2eAgInPc7d1	základní
zdroji	zdroj	k1gInPc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podzemní	podzemní	k2eAgFnPc1d1	podzemní
vody	voda	k1gFnPc1	voda
(	(	kIx(	(
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
charakterizovaný	charakterizovaný	k2eAgMnSc1d1	charakterizovaný
jarním	jarní	k2eAgInSc7d1	jarní
vzestupem	vzestup	k1gInSc7	vzestup
hladiny	hladina	k1gFnSc2	hladina
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
až	až	k8xS	až
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc4d1	malá
vodnost	vodnost	k1gFnSc4	vodnost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
dešti	dešť	k1gInSc6	dešť
způsobující	způsobující	k2eAgFnSc2d1	způsobující
povodně	povodeň	k1gFnSc2	povodeň
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc4	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgNnSc1d1	roční
kolísání	kolísání	k1gNnSc1	kolísání
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
kaskády	kaskáda	k1gFnSc2	kaskáda
přehrad	přehrada	k1gFnPc2	přehrada
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
u	u	k7c2	u
Tveru	Tver	k1gInSc2	Tver
11	[number]	k4	11
m	m	kA	m
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
ústím	ústit	k5eAaImIp1nS	ústit
Kamy	Kamy	k?	Kamy
15	[number]	k4	15
až	až	k9	až
17	[number]	k4	17
m	m	kA	m
a	a	k8xC	a
u	u	k7c2	u
Astrachaně	Astrachaň	k1gFnSc2	Astrachaň
3	[number]	k4	3
m.	m.	k?	m.
Po	po	k7c6	po
zregulování	zregulování	k1gNnSc6	zregulování
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
kolísání	kolísání	k1gNnSc1	kolísání
prudce	prudko	k6eAd1	prudko
snížilo	snížit	k5eAaPmAgNnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
průtoku	průtok	k1gInSc2	průtok
až	až	k6eAd1	až
k	k	k7c3	k
Volgogradu	Volgograd	k1gInSc3	Volgograd
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Pod	pod	k7c7	pod
Volgogradem	Volgograd	k1gInSc7	Volgograd
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
%	%	kIx~	%
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vypařování	vypařování	k1gNnSc2	vypařování
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInPc1d1	maximální
průtoky	průtok	k1gInPc1	průtok
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Kamy	Kamy	k?	Kamy
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
67	[number]	k4	67
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
u	u	k7c2	u
Volgogradu	Volgograd	k1gInSc2	Volgograd
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozlití	rozlití	k1gNnSc2	rozlití
do	do	k7c2	do
úvalu	úval	k1gInSc2	úval
nepřevyšovaly	převyšovat	k5eNaImAgFnP	převyšovat
52	[number]	k4	52
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Po	po	k7c6	po
regulaci	regulace	k1gFnSc6	regulace
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hodnoty	hodnota	k1gFnSc2	hodnota
průtoků	průtok	k1gInPc2	průtok
snížily	snížit	k5eAaPmAgFnP	snížit
a	a	k8xC	a
nejnižší	nízký	k2eAgFnPc1d3	nejnižší
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
bilance	bilance	k1gFnPc1	bilance
řeky	řeka	k1gFnSc2	řeka
nad	nad	k7c7	nad
Volgogradem	Volgograd	k1gInSc7	Volgograd
za	za	k7c4	za
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
přehrad	přehrada	k1gFnPc2	přehrada
řeka	řek	k1gMnSc2	řek
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
přinesla	přinést	k5eAaPmAgFnS	přinést
za	za	k7c4	za
rok	rok	k1gInSc4	rok
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
Mt	Mt	k1gFnPc2	Mt
nánosů	nános	k1gInPc2	nános
a	a	k8xC	a
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
Mt	Mt	k1gMnPc2	Mt
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
uprostřed	uprostřed	k7c2	uprostřed
léta	léto	k1gNnSc2	léto
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
20	[number]	k4	20
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
С	С	k?	С
Rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
u	u	k7c2	u
Astrachaně	Astrachaň	k1gFnSc2	Astrachaň
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
pod	pod	k7c7	pod
Kamyšinem	Kamyšin	k1gInSc7	Kamyšin
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
uprostřed	uprostřed	k7c2	uprostřed
dubna	duben	k1gInSc2	duben
na	na	k7c6	na
zbývajícím	zbývající	k2eAgInSc6d1	zbývající
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ledu	led	k1gInSc2	led
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
dní	den	k1gInPc2	den
a	a	k8xC	a
u	u	k7c2	u
Astrachaně	Astrachaň	k1gFnSc2	Astrachaň
260	[number]	k4	260
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
přehrad	přehrada	k1gFnPc2	přehrada
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
teplotní	teplotní	k2eAgInSc1d1	teplotní
režim	režim	k1gInSc1	režim
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
a	a	k8xC	a
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
význam	význam	k1gInSc1	význam
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
přehrad	přehrada	k1gFnPc2	přehrada
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Volžské	volžský	k2eAgFnSc2d1	Volžská
kaskády	kaskáda	k1gFnSc2	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Přehrady	přehrada	k1gFnPc1	přehrada
a	a	k8xC	a
hydroelektrárny	hydroelektrárna	k1gFnPc1	hydroelektrárna
na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
<g/>
:	:	kIx,	:
Volžská	volžský	k2eAgFnSc1d1	Volžská
(	(	kIx(	(
<g/>
Ivankovská	Ivankovský	k2eAgFnSc1d1	Ivankovský
<g/>
)	)	kIx)	)
přehrada	přehrada	k1gFnSc1	přehrada
Ugličská	Ugličský	k2eAgFnSc1d1	Ugličský
přehrada	přehrada	k1gFnSc1	přehrada
Rybinská	Rybinský	k2eAgFnSc1d1	Rybinský
přehrada	přehrada	k1gFnSc1	přehrada
Gorkovská	gorkovský	k2eAgFnSc1d1	gorkovský
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
názvu	název	k1gInSc2	název
Nižního	nižní	k2eAgInSc2d1	nižní
Novgorodu	Novgorod	k1gInSc2	Novgorod
<g/>
)	)	kIx)	)
Čeboksarská	Čeboksarský	k2eAgFnSc1d1	Čeboksarský
přehrada	přehrada	k1gFnSc1	přehrada
Kujbyševská	kujbyševský	k2eAgFnSc1d1	Kujbyševská
přehrada	přehrada	k1gFnSc1	přehrada
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
-	-	kIx~	-
6450	[number]	k4	6450
km2	km2	k4	km2
<g/>
)	)	kIx)	)
Saratovská	Saratovský	k2eAgFnSc1d1	Saratovská
přehrada	přehrada	k1gFnSc1	přehrada
Volgogradská	volgogradský	k2eAgFnSc1d1	Volgogradská
přehrada	přehrada	k1gFnSc1	přehrada
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
zatopená	zatopený	k2eAgFnSc1d1	zatopená
volžskými	volžský	k2eAgFnPc7d1	Volžská
přehradami	přehrada	k1gFnPc7	přehrada
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
20	[number]	k4	20
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
říčními	říční	k2eAgInPc7d1	říční
systémy	systém	k1gInPc7	systém
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vodních	vodní	k2eAgInPc2d1	vodní
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
Volžsko-baltská	volžskoaltský	k2eAgFnSc1d1	volžsko-baltský
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
Vyšněvolocký	Vyšněvolocký	k1gMnSc1	Vyšněvolocký
kanál	kanál	k1gInSc1	kanál
Tichvinský	Tichvinský	k2eAgInSc4d1	Tichvinský
kanál	kanál	k1gInSc4	kanál
s	s	k7c7	s
Bílým	bílý	k2eAgNnSc7d1	bílé
mořem	moře	k1gNnSc7	moře
Severodvinský	Severodvinský	k2eAgInSc4d1	Severodvinský
kanál	kanál	k1gInSc4	kanál
Bělomořsko-baltský	bělomořskoaltský	k2eAgInSc4d1	bělomořsko-baltský
kanál	kanál	k1gInSc4	kanál
s	s	k7c7	s
Azovským	azovský	k2eAgMnSc7d1	azovský
a	a	k8xC	a
s	s	k7c7	s
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
Volžsko-donský	volžskoonský	k2eAgInSc4d1	volžsko-donský
průplav	průplav	k1gInSc4	průplav
Plánované	plánovaný	k2eAgNnSc1d1	plánované
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Ural	Ural	k1gInSc4	Ural
(	(	kIx(	(
<g/>
Volžsko-uralský	volžskoralský	k2eAgInSc4d1	volžsko-uralský
kanál	kanál	k1gInSc4	kanál
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Volha	Volha	k1gFnSc1	Volha
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
přítoky	přítok	k1gInPc7	přítok
a	a	k8xC	a
průplavy	průplav	k1gInPc1	průplav
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
následujícími	následující	k2eAgNnPc7d1	následující
městy	město	k1gNnPc7	město
(	(	kIx(	(
<g/>
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Koupání	koupání	k1gNnSc1	koupání
u	u	k7c2	u
Volhy	Volha	k1gFnSc2	Volha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
příjemné	příjemný	k2eAgInPc1d1	příjemný
<g/>
,	,	kIx,	,
písek	písek	k1gInSc1	písek
je	být	k5eAaImIp3nS	být
rozpálený	rozpálený	k2eAgMnSc1d1	rozpálený
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
chladivá	chladivý	k2eAgFnSc1d1	chladivá
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koupání	koupání	k1gNnSc1	koupání
"	"	kIx"	"
<g/>
na	na	k7c4	na
Adama	Adam	k1gMnSc4	Adam
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
západních	západní	k2eAgNnPc2d1	západní
letovisek	letovisko	k1gNnPc2	letovisko
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
používat	používat	k5eAaImF	používat
oplocených	oplocený	k2eAgFnPc2d1	oplocená
nudistických	nudistický	k2eAgFnPc2d1	nudistická
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
40	[number]	k4	40
má	mít	k5eAaImIp3nS	mít
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
plotice	plotice	k1gFnSc1	plotice
obecná	obecná	k1gFnSc1	obecná
kaspická	kaspický	k2eAgFnSc1d1	kaspická
<g/>
,	,	kIx,	,
sledi	sleď	k1gMnPc1	sleď
<g/>
,	,	kIx,	,
cejn	cejn	k1gMnSc1	cejn
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
candáti	candát	k1gMnPc1	candát
<g/>
,	,	kIx,	,
kapr	kapr	k1gMnSc1	kapr
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
sumec	sumec	k1gMnSc1	sumec
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc4	štika
<g/>
,	,	kIx,	,
jeseteři	jeseter	k1gMnPc1	jeseter
(	(	kIx(	(
<g/>
jeseter	jeseter	k1gMnSc1	jeseter
malý	malý	k1gMnSc1	malý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úrodná	úrodný	k2eAgFnSc1d1	úrodná
oblast	oblast	k1gFnSc1	oblast
podél	podél	k7c2	podél
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
Volhy	Volha	k1gFnSc2	Volha
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
osídlena	osídlit	k5eAaPmNgFnS	osídlit
už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
pokládají	pokládat	k5eAaImIp3nP	pokládat
dolní	dolní	k2eAgNnPc4d1	dolní
Povolží	Povolží	k1gNnPc4	Povolží
za	za	k7c4	za
pravěkou	pravěký	k2eAgFnSc4d1	pravěká
kolébku	kolébka	k1gFnSc4	kolébka
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sloužila	sloužit	k5eAaImAgFnS	sloužit
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
přítoky	přítok	k1gInPc7	přítok
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
tržní	tržní	k2eAgFnSc2d1	tržní
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
dovážely	dovážet	k5eAaImAgFnP	dovážet
tkaniny	tkanina	k1gFnPc1	tkanina
<g/>
,	,	kIx,	,
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
kožešiny	kožešina	k1gFnSc2	kožešina
<g/>
,	,	kIx,	,
vosk	vosk	k1gInSc4	vosk
a	a	k8xC	a
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
ležela	ležet	k5eAaImAgFnS	ležet
i	i	k9	i
hlavní	hlavní	k2eAgNnPc4d1	hlavní
sídla	sídlo	k1gNnPc4	sídlo
říše	říš	k1gFnSc2	říš
Chazarů	Chazar	k1gMnPc2	Chazar
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
významná	významný	k2eAgNnPc4d1	významné
obchodní	obchodní	k2eAgNnPc4d1	obchodní
a	a	k8xC	a
politická	politický	k2eAgNnPc4d1	politické
centra	centrum	k1gNnPc4	centrum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Velikij	Velikij	k1gFnSc4	Velikij
golgat	golgat	k5eAaPmF	golgat
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Bolgary	Bolgar	k1gInPc4	Bolgar
v	v	k7c4	v
tatarštinu	tatarština	k1gFnSc4	tatarština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
sídlo	sídlo	k1gNnSc1	sídlo
Volžských	volžský	k2eAgMnPc2d1	volžský
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Saraj	Saraj	k1gFnSc1	Saraj
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hordy	horda	k1gFnSc2	horda
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Volgogradu	Volgograd	k1gInSc2	Volgograd
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Itil	Itil	k1gInSc1	Itil
<g/>
/	/	kIx~	/
<g/>
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
centra	centrum	k1gNnPc1	centrum
byla	být	k5eAaImAgNnP	být
Novgorod	Novgorod	k1gInSc4	Novgorod
<g/>
,	,	kIx,	,
Rostov	Rostov	k1gInSc4	Rostov
<g/>
,	,	kIx,	,
Sundal	sundat	k5eAaPmAgMnS	sundat
<g/>
,	,	kIx,	,
Murom	Murom	k1gInSc1	Murom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
význam	význam	k1gInSc1	význam
trhů	trh	k1gInPc2	trh
slábnul	slábnout	k5eAaPmAgInS	slábnout
a	a	k8xC	a
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
mongolsko-tatarské	mongolskoatarský	k2eAgInPc1d1	mongolsko-tatarský
nájezdy	nájezd	k1gInPc1	nájezd
narušily	narušit	k5eAaPmAgInP	narušit
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
vztahy	vztah	k1gInPc4	vztah
kromě	kromě	k7c2	kromě
povodí	povodí	k1gNnSc2	povodí
horního	horní	k2eAgInSc2d1	horní
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrály	hrát	k5eAaImAgInP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Tver	Tver	k1gInSc1	Tver
a	a	k8xC	a
města	město	k1gNnSc2	město
Vladimiro-Suzdalské	Vladimiro-Suzdalský	k2eAgFnSc2d1	Vladimiro-Suzdalský
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
význam	význam	k1gInSc1	význam
trhů	trh	k1gInPc2	trh
opět	opět	k6eAd1	opět
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
centra	centr	k1gMnSc4	centr
Kazaň	Kazaň	k1gFnSc1	Kazaň
<g/>
,	,	kIx,	,
Nižnij	Nižnij	k1gFnSc1	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Ivanem	Ivan	k1gMnSc7	Ivan
Hrozným	hrozný	k2eAgMnSc7d1	hrozný
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rusko	Rusko	k1gNnSc4	Rusko
postupně	postupně	k6eAd1	postupně
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
i	i	k8xC	i
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
Volhy	Volha	k1gFnSc2	Volha
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
expanzi	expanze	k1gFnSc4	expanze
do	do	k7c2	do
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
této	tento	k3xDgFnSc2	tento
expanze	expanze	k1gFnSc2	expanze
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
líčí	líčit	k5eAaImIp3nS	líčit
i	i	k9	i
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
kozáckém	kozácký	k2eAgMnSc6d1	kozácký
náčelníkovi	náčelník	k1gMnSc6	náčelník
Stěnkovi	Stěnek	k1gMnSc6	Stěnek
Razinovi	Razin	k1gMnSc6	Razin
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
populární	populární	k2eAgFnSc1d1	populární
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Volga	Volga	k1gFnSc1	Volga
<g/>
,	,	kIx,	,
Volga	Volga	k1gFnSc1	Volga
<g/>
,	,	kIx,	,
mať	matit	k5eAaImRp2nS	matit
rodnaja	rodnaja	k1gMnSc1	rodnaja
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
vodní	vodní	k2eAgInSc1d1	vodní
systém	systém	k1gInSc1	systém
Volhy	Volha	k1gFnSc2	Volha
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Kazaňského	kazaňský	k2eAgInSc2d1	kazaňský
a	a	k8xC	a
Astrachaňského	astrachaňský	k2eAgInSc2d1	astrachaňský
chanátu	chanát	k1gInSc2	chanát
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Trhy	trh	k1gInPc1	trh
vzkvétají	vzkvétat	k5eAaImIp3nP	vzkvétat
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
nová	nový	k2eAgNnPc1d1	nové
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Saratov	Saratov	k1gInSc1	Saratov
<g/>
,	,	kIx,	,
Caricyn	Caricyn	k1gInSc1	Caricyn
<g/>
)	)	kIx)	)
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
význam	význam	k1gInSc4	význam
stávajících	stávající	k2eAgInPc2d1	stávající
(	(	kIx(	(
<g/>
Jaroslavl	Jaroslavl	k1gFnSc1	Jaroslavl
<g/>
,	,	kIx,	,
Kostroma	Kostroma	k1gFnSc1	Kostroma
<g/>
,	,	kIx,	,
Nižnij	Nižnij	k1gFnSc1	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pluly	plout	k5eAaImAgFnP	plout
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
karavany	karavana	k1gFnPc1	karavana
lodí	loď	k1gFnPc2	loď
čítající	čítající	k2eAgFnSc2d1	čítající
až	až	k8xS	až
500	[number]	k4	500
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
doznal	doznat	k5eAaPmAgMnS	doznat
trh	trh	k1gInSc4	trh
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
velkého	velký	k2eAgInSc2d1	velký
rozvoje	rozvoj	k1gInSc2	rozvoj
na	na	k7c4	na
což	což	k3yRnSc4	což
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
dokončení	dokončení	k1gNnSc2	dokončení
Mariinské	Mariinský	k2eAgFnSc2d1	Mariinský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spojila	spojit	k5eAaPmAgFnS	spojit
povodí	povodí	k1gNnSc4	povodí
Volhy	Volha	k1gFnSc2	Volha
a	a	k8xC	a
Něvy	Něva	k1gFnSc2	Něva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
velké	velký	k2eAgFnSc2d1	velká
říční	říční	k2eAgFnSc2d1	říční
flotily	flotila	k1gFnSc2	flotila
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
parník	parník	k1gInSc1	parník
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
a	a	k8xC	a
práce	práce	k1gFnSc1	práce
300000	[number]	k4	300000
burlaků	burlak	k1gInPc2	burlak
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Převáželo	převážet	k5eAaImAgNnS	převážet
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
měly	mít	k5eAaImAgFnP	mít
jarmarky	jarmarky	k?	jarmarky
v	v	k7c6	v
Nižném	nižný	k2eAgInSc6d1	nižný
Novgorodě	Novgorod	k1gInSc6	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc2	několik
bitvám	bitva	k1gFnPc3	bitva
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
řeka	řeka	k1gFnSc1	řeka
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
bitvy	bitva	k1gFnPc1	bitva
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
s	s	k7c7	s
československými	československý	k2eAgMnPc7d1	československý
legionáři	legionář	k1gMnPc7	legionář
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Lipjag	Lipjaga	k1gFnPc2	Lipjaga
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
bělogvardějci	bělogvardějec	k1gMnPc7	bělogvardějec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
s	s	k7c7	s
Kolčakem	Kolčak	k1gMnSc7	Kolčak
a	a	k8xC	a
Děnikinem	Děnikin	k1gMnSc7	Děnikin
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
.	.	kIx.	.
</s>
