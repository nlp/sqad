<s>
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1852	[number]	k4	1852
Mirotice	Mirotice	k1gFnSc1	Mirotice
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
součást	součást	k1gFnSc1	součást
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
dekoratér	dekoratér	k1gMnSc1	dekoratér
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
Generace	generace	k1gFnSc2	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
klasik	klasik	k1gMnSc1	klasik
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
