<p>
<s>
Irena	Irena	k1gFnSc1	Irena
"	"	kIx"	"
<g/>
Inka	Inka	k1gFnSc1	Inka
<g/>
"	"	kIx"	"
Bernášková	Bernášková	k1gFnSc1	Bernášková
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
1904	[number]	k4	1904
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1942	[number]	k4	1942
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
odbojářka	odbojářka	k1gFnSc1	odbojářka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
položila	položit	k5eAaPmAgFnS	položit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
německé	německý	k2eAgFnSc3d1	německá
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dcer	dcera	k1gFnPc2	dcera
grafika	grafik	k1gMnSc2	grafik
a	a	k8xC	a
malíře	malíř	k1gMnSc2	malíř
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Preissiga	Preissig	k1gMnSc2	Preissig
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Preissigovi	Preissigův	k2eAgMnPc1d1	Preissigův
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
Irena	Irena	k1gFnSc1	Irena
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvěma	dva	k4xCgFnPc7	dva
sestrami	sestra	k1gFnPc7	sestra
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
Františka	František	k1gMnSc2	František
Bernáška	Bernášek	k1gMnSc2	Bernášek
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
sňatkem	sňatek	k1gInSc7	sňatek
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
s	s	k7c7	s
Irenou	Irena	k1gFnSc7	Irena
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
přerušil	přerušit	k5eAaPmAgMnS	přerušit
veškeré	veškerý	k3xTgInPc4	veškerý
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
na	na	k7c6	na
Spořilově	spořilův	k2eAgNnSc6d1	spořilův
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
mobilizaci	mobilizace	k1gFnSc6	mobilizace
v	v	k7c6	v
období	období	k1gNnSc6	období
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
krize	krize	k1gFnSc2	krize
se	se	k3xPyFc4	se
Bernášková	Bernášková	k1gFnSc1	Bernášková
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
jako	jako	k8xS	jako
sestra	sestra	k1gFnSc1	sestra
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
ošetřovala	ošetřovat	k5eAaImAgFnS	ošetřovat
uprchlíky	uprchlík	k1gMnPc4	uprchlík
z	z	k7c2	z
obsazeného	obsazený	k2eAgNnSc2d1	obsazené
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Československa	Československo	k1gNnSc2	Československo
německými	německý	k2eAgInPc7d1	německý
vojsky	vojsky	k6eAd1	vojsky
nejdříve	dříve	k6eAd3	dříve
kolportovala	kolportovat	k5eAaImAgFnS	kolportovat
letáky	leták	k1gInPc4	leták
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
také	také	k9	také
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
ilegálního	ilegální	k2eAgInSc2d1	ilegální
časopisu	časopis	k1gInSc2	časopis
V	v	k7c4	v
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
převáděla	převádět	k5eAaImAgFnS	převádět
ohrožené	ohrožený	k2eAgMnPc4d1	ohrožený
lidi	člověk	k1gMnPc4	člověk
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pátralo	pátrat	k5eAaImAgNnS	pátrat
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
unikala	unikat	k5eAaImAgFnS	unikat
zatčení	zatčení	k1gNnSc3	zatčení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
vydávání	vydávání	k1gNnSc6	vydávání
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Zatčena	zatčen	k2eAgFnSc1d1	zatčena
byla	být	k5eAaImAgFnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1940	[number]	k4	1940
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
Poříčí	Poříčí	k1gNnSc6	Poříčí
s	s	k7c7	s
falešnými	falešný	k2eAgInPc7d1	falešný
doklady	doklad	k1gInPc7	doklad
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
statečností	statečnost	k1gFnSc7	statečnost
při	při	k7c6	při
výsleších	výslech	k1gInPc6	výslech
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
veškerou	veškerý	k3xTgFnSc4	veškerý
vinu	vina	k1gFnSc4	vina
vzala	vzít	k5eAaPmAgFnS	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
zachránila	zachránit	k5eAaPmAgFnS	zachránit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
zatčena	zatknout	k5eAaPmNgFnS	zatknout
její	její	k3xOp3gFnSc1	její
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Buchenwald	Buchenwald	k1gInSc1	Buchenwald
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Bernášková	Bernášková	k1gFnSc1	Bernášková
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
německým	německý	k2eAgInSc7d1	německý
soudem	soud	k1gInSc7	soud
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
<g/>
)	)	kIx)	)
a	a	k8xC	a
popravena	popravit	k5eAaPmNgFnS	popravit
gilotinou	gilotina	k1gFnSc7	gilotina
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irena	Irena	k1gFnSc1	Irena
Bernášková	Bernášková	k1gFnSc1	Bernášková
byla	být	k5eAaImAgFnS	být
vyznamenána	vyznamenán	k2eAgFnSc1d1	vyznamenána
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
prezidentem	prezident	k1gMnSc7	prezident
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
medailí	medaile	k1gFnPc2	medaile
Za	za	k7c4	za
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
V.	V.	kA	V.
Preissig	Preissig	k1gInSc4	Preissig
a	a	k8xC	a
I.	I.	kA	I.
Bernášková	Bernášková	k1gFnSc1	Bernášková
==	==	k?	==
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
domě	dům	k1gInSc6	dům
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
<g/>
:	:	kIx,	:
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
VIII	VIII	kA	VIII
944	[number]	k4	944
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
141	[number]	k4	141
00	[number]	k4	00
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Záběhlice	Záběhlice	k1gFnPc1	Záběhlice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
se	se	k3xPyFc4	se
ulice	ulice	k1gFnSc1	ulice
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Südoststrasse	Südoststrass	k1gMnSc4	Südoststrass
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
bydlel	bydlet	k5eAaImAgMnS	bydlet
zde	zde	k6eAd1	zde
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Preissig	Preissig	k1gMnSc1	Preissig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
bytě	byt	k1gInSc6	byt
se	se	k3xPyFc4	se
připravovala	připravovat	k5eAaImAgFnS	připravovat
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
vydávala	vydávat	k5eAaImAgFnS	vydávat
-	-	kIx~	-
od	od	k7c2	od
čísla	číslo	k1gNnSc2	číslo
28	[number]	k4	28
-	-	kIx~	-
"	"	kIx"	"
<g/>
spořilovská	spořilovský	k2eAgFnSc1d1	Spořilovská
<g/>
"	"	kIx"	"
varianta	varianta	k1gFnSc1	varianta
ilegálního	ilegální	k2eAgInSc2d1	ilegální
časopisu	časopis	k1gInSc2	časopis
V	v	k7c4	v
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
centrálního	centrální	k2eAgInSc2d1	centrální
horního	horní	k2eAgInSc2d1	horní
nápisu	nápis	k1gInSc2	nápis
<g/>
:	:	kIx,	:
VOJTĚCH	Vojtěch	k1gMnSc1	Vojtěch
PREISSIG	PREISSIG	kA	PREISSIG
A	a	k9	a
JEHO	jeho	k3xOp3gFnSc1	jeho
DCERA	dcera	k1gFnSc1	dcera
IRENA	Irena	k1gFnSc1	Irena
"	"	kIx"	"
<g/>
PANÍ	paní	k1gFnSc1	paní
INKA	Inka	k1gMnSc1	Inka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
reliéf	reliéf	k1gInSc1	reliéf
dvou	dva	k4xCgFnPc2	dva
tváří	tvář	k1gFnPc2	tvář
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
;	;	kIx,	;
oba	dva	k4xCgInPc1	dva
hledí	hledět	k5eAaImIp3nS	hledět
směrem	směr	k1gInSc7	směr
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
V.	V.	kA	V.
Preissiga	Preissig	k1gMnSc2	Preissig
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
pak	pak	k6eAd1	pak
reliéf	reliéf	k1gInSc4	reliéf
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
I.	I.	kA	I.
Bernáškové	Bernášková	k1gFnSc2	Bernášková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podél	podél	k7c2	podél
levého	levý	k2eAgInSc2d1	levý
svislého	svislý	k2eAgInSc2d1	svislý
okraje	okraj	k1gInSc2	okraj
reliéfu	reliéf	k1gInSc2	reliéf
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Vojtěchu	Vojtěch	k1gMnSc6	Vojtěch
Preissigovi	Preissig	k1gMnSc6	Preissig
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
</s>
</p>
<p>
<s>
Podél	podél	k7c2	podél
pravé	pravý	k2eAgFnSc2d1	pravá
svislé	svislý	k2eAgFnSc2d1	svislá
hrany	hrana	k1gFnSc2	hrana
centrálního	centrální	k2eAgInSc2d1	centrální
reliéfu	reliéf	k1gInSc2	reliéf
jsou	být	k5eAaImIp3nP	být
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
dceři	dcera	k1gFnSc6	dcera
Ireně	Irena	k1gFnSc3	Irena
Bernáškové	Bernášková	k1gFnSc3	Bernášková
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VRZALOVÁ	Vrzalová	k1gFnSc1	Vrzalová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Zasnoubena	zasnouben	k2eAgFnSc1d1	zasnoubena
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
333	[number]	k4	333
<g/>
+	+	kIx~	+
<g/>
36	[number]	k4	36
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7565	[number]	k4	7565
<g/>
-	-	kIx~	-
<g/>
186	[number]	k4	186
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Vrzalová	Vrzalová	k1gFnSc1	Vrzalová
<g/>
:	:	kIx,	:
Odbojářka	Odbojářka	k1gFnSc1	Odbojářka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zemřela	zemřít	k5eAaPmAgFnS	zemřít
pod	pod	k7c7	pod
gilotinou	gilotina	k1gFnSc7	gilotina
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
s.	s.	k?	s.
20	[number]	k4	20
<g/>
.	.	kIx.	.
</ref>
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Irena	Irena	k1gFnSc1	Irena
Bernášková	Bernášková	k1gFnSc1	Bernášková
</s>
</p>
<p>
<s>
Pobytové	pobytový	k2eAgFnPc1d1	pobytová
přihlášky	přihláška	k1gFnPc1	přihláška
pražského	pražský	k2eAgNnSc2d1	Pražské
policejního	policejní	k2eAgNnSc2d1	policejní
ředitelství	ředitelství	k1gNnSc2	ředitelství
(	(	kIx(	(
<g/>
konskripce	konskripce	k1gFnSc1	konskripce
<g/>
)	)	kIx)	)
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Preissiga	Preissig	k1gMnSc2	Preissig
*	*	kIx~	*
<g/>
1873	[number]	k4	1873
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Spořilovské	spořilovský	k2eAgFnPc1d1	Spořilovská
noviny	novina	k1gFnPc1	novina
<g/>
:	:	kIx,	:
Inka	Inka	k1gFnSc1	Inka
Bernášková	Bernášková	k1gFnSc1	Bernášková
-	-	kIx~	-
statečná	statečný	k2eAgFnSc1d1	statečná
žena	žena	k1gFnSc1	žena
ze	z	k7c2	z
Spořilova	spořilův	k2eAgInSc2d1	spořilův
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Info	Info	k6eAd1	Info
o	o	k7c6	o
TV	TV	kA	TV
dokumentu	dokument	k1gInSc2	dokument
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
</s>
</p>
