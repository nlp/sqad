<s>
Irena	Irena	k1gFnSc1	Irena
Bernášková	Bernášková	k1gFnSc1	Bernášková
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
německým	německý	k2eAgInSc7d1	německý
soudem	soud	k1gInSc7	soud
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
Češka	Češka	k1gFnSc1	Češka
<g/>
)	)	kIx)	)
a	a	k8xC	a
popravena	popravit	k5eAaPmNgFnS	popravit
gilotinou	gilotina	k1gFnSc7	gilotina
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
