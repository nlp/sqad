<s>
Prvním	první	k4xOgMnSc7	první
nositelem	nositel	k1gMnSc7	nositel
čestného	čestný	k2eAgInSc2d1	čestný
doktorátu	doktorát	k1gInSc2	doktorát
byl	být	k5eAaImAgMnS	být
skladatel	skladatel	k1gMnSc1	skladatel
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
také	také	k9	také
syn	syn	k1gMnSc1	syn
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Masarykův	Masarykův	k2eAgMnSc1d1	Masarykův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
a	a	k8xC	a
nástupce	nástupce	k1gMnSc1	nástupce
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
funkci	funkce	k1gFnSc6	funkce
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
nebo	nebo	k8xC	nebo
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
