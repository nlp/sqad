<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1869	[number]	k4	1869
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1938	[number]	k4	1938
Starý	Starý	k1gMnSc1	Starý
Smokovec	Smokovec	k1gMnSc1	Smokovec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnPc1d1	pokroková
a	a	k8xC	a
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
poslanec	poslanec	k1gMnSc1	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
pražskou	pražský	k2eAgFnSc4d1	Pražská
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
civilní	civilní	k2eAgMnSc1d1	civilní
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Augusta	August	k1gMnSc2	August
Zátky	zátka	k1gFnSc2	zátka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
přemístil	přemístit	k5eAaPmAgMnS	přemístit
sídlo	sídlo	k1gNnSc4	sídlo
své	svůj	k3xOyFgFnSc2	svůj
firmy	firma	k1gFnSc2	firma
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
realistické	realistický	k2eAgFnSc2d1	realistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
blízké	blízký	k2eAgInPc4d1	blízký
T.	T.	kA	T.
G.	G.	kA	G.
Masarykovi	Masaryk	k1gMnSc6	Masaryk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
profesní	profesní	k2eAgFnSc6d1	profesní
i	i	k8xC	i
politické	politický	k2eAgFnSc6d1	politická
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
protirakouském	protirakouský	k2eAgInSc6d1	protirakouský
odboji	odboj	k1gInSc6	odboj
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgInS	podporovat
rodinu	rodina	k1gFnSc4	rodina
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Profiloval	profilovat	k5eAaImAgInS	profilovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
slovakofil	slovakofil	k1gMnSc1	slovakofil
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
realistickou	realistický	k2eAgFnSc7d1	realistická
stranou	strana	k1gFnSc7	strana
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozešel	rozejít	k5eAaPmAgInS	rozejít
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
Revolučním	revoluční	k2eAgNnSc6d1	revoluční
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
za	za	k7c4	za
slovenský	slovenský	k2eAgInSc4d1	slovenský
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Profesí	profes	k1gFnPc2	profes
byl	být	k5eAaImAgMnS	být
autorizovaný	autorizovaný	k2eAgMnSc1d1	autorizovaný
civilní	civilní	k2eAgMnSc1d1	civilní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
listu	list	k1gInSc2	list
Česká	český	k2eAgFnSc1d1	Česká
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
skupinou	skupina	k1gFnSc7	skupina
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
k	k	k7c3	k
Československé	československý	k2eAgFnSc3d1	Československá
straně	strana	k1gFnSc3	strana
socialistické	socialistický	k2eAgFnSc2d1	socialistická
(	(	kIx(	(
<g/>
národně	národně	k6eAd1	národně
sociální	sociální	k2eAgNnSc1d1	sociální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
některých	některý	k3yIgFnPc2	některý
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
budovu	budova	k1gFnSc4	budova
české	český	k2eAgFnSc2d1	Česká
státní	státní	k2eAgFnSc2d1	státní
školy	škola	k1gFnSc2	škola
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
projekt	projekt	k1gInSc1	projekt
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc4	budova
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
československého	československý	k2eAgInSc2d1	československý
průmyslu	průmysl	k1gInSc2	průmysl
cukrovarnického	cukrovarnický	k2eAgInSc2d1	cukrovarnický
<g/>
,	,	kIx,	,
Průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
tiskárny	tiskárna	k1gFnSc2	tiskárna
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
a	a	k8xC	a
Státního	státní	k2eAgInSc2d1	státní
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Ořechovce	ořechovka	k1gFnSc6	ořechovka
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
projektování	projektování	k1gNnSc4	projektování
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
československého	československý	k2eAgNnSc2d1	Československé
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
zájmem	zájem	k1gInSc7	zájem
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byla	být	k5eAaImAgFnS	být
astronomie	astronomie	k1gFnSc1	astronomie
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Československé	československý	k2eAgFnSc2d1	Československá
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Projektoval	projektovat	k5eAaBmAgMnS	projektovat
dalekohledy	dalekohled	k1gInPc4	dalekohled
a	a	k8xC	a
observatoř	observatoř	k1gFnSc1	observatoř
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgMnSc6d1	Ondřejův
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Quido	Quido	k1gNnSc1	Quido
Záruba	Záruba	k1gMnSc1	Záruba
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
inženýrským	inženýrský	k2eAgMnSc7d1	inženýrský
geologem	geolog	k1gMnSc7	geolog
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Libor	Libor	k1gMnSc1	Libor
Záruba	Záruba	k1gMnSc1	Záruba
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
stavitelem	stavitel	k1gMnSc7	stavitel
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Slapy	slap	k1gInPc1	slap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zeť	zeť	k1gMnSc1	zeť
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Krch	Krch	k1gMnSc1	Krch
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
architektem	architekt	k1gMnSc7	architekt
<g/>
,	,	kIx,	,
zeť	zeť	k1gMnSc1	zeť
Pavel	Pavel	k1gMnSc1	Pavel
Bořkovec	Bořkovec	k1gMnSc1	Bořkovec
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Československá	československý	k2eAgFnSc1d1	Československá
vlajka	vlajka	k1gFnSc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
ústavního	ústavní	k2eAgInSc2d1	ústavní
výboru	výbor	k1gInSc2	výbor
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
vybrat	vybrat	k5eAaPmF	vybrat
vhodný	vhodný	k2eAgInSc4d1	vhodný
návrh	návrh	k1gInSc4	návrh
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
předkladatelem	předkladatel	k1gMnSc7	předkladatel
několika	několik	k4yIc2	několik
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k9	ale
nebyly	být	k5eNaImAgFnP	být
komisí	komise	k1gFnSc7	komise
doporučeny	doporučen	k2eAgFnPc1d1	doporučena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Projekční	projekční	k2eAgFnSc1d1	projekční
a	a	k8xC	a
stavební	stavební	k2eAgFnSc1d1	stavební
firma	firma	k1gFnSc1	firma
==	==	k?	==
</s>
</p>
<p>
<s>
Projekční	projekční	k2eAgFnSc1d1	projekční
a	a	k8xC	a
stavební	stavební	k2eAgFnSc1d1	stavební
firma	firma	k1gFnSc1	firma
Ing.	ing.	kA	ing.
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
provedla	provést	k5eAaPmAgFnS	provést
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
Areál	areál	k1gInSc1	areál
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
cukrovarnického	cukrovarnický	k2eAgInSc2d1	cukrovarnický
Střešovice	Střešovice	k1gFnPc4	Střešovice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc1	sídlo
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc1	sídlo
Ústavu	ústav	k1gInSc2	ústav
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
biochemie	biochemie	k1gFnSc2	biochemie
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.	v.	k?	v.
v.	v.	k?	v.
i.	i.	k?	i.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
letecký	letecký	k2eAgInSc1d1	letecký
hangár	hangár	k1gInSc1	hangár
ve	v	k7c6	v
Vajnorech	Vajnor	k1gInPc6	Vajnor
</s>
</p>
<p>
<s>
budova	budova	k1gFnSc1	budova
Ústředí	ústředí	k1gNnSc2	ústředí
tabákové	tabákový	k2eAgFnSc2d1	tabáková
režie	režie	k1gFnSc2	režie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
</s>
</p>
<p>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
úsek	úsek	k1gInSc1	úsek
železniční	železniční	k2eAgFnSc2d1	železniční
dráhy	dráha	k1gFnSc2	dráha
Púchov	Púchov	k1gInSc1	Púchov
-	-	kIx~	-
Horní	horní	k2eAgInSc1d1	horní
Lideč	Lideč	k1gInSc1	Lideč
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
úsek	úsek	k1gInSc1	úsek
železniční	železniční	k2eAgFnSc2d1	železniční
dráhy	dráha	k1gFnSc2	dráha
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
-	-	kIx~	-
Diviaky	Diviak	k1gInPc7	Diviak
včetně	včetně	k7c2	včetně
stavby	stavba	k1gFnSc2	stavba
několika	několik	k4yIc2	několik
tunelů	tunel	k1gInPc2	tunel
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
úsek	úsek	k1gInSc1	úsek
železniční	železniční	k2eAgFnSc2d1	železniční
dráhy	dráha	k1gFnSc2	dráha
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Německý	německý	k2eAgInSc1d1	německý
BrodFirma	BrodFirm	k1gMnSc4	BrodFirm
se	se	k3xPyFc4	se
také	také	k9	také
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
ústavy	ústav	k1gInPc1	ústav
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Sanatorium	sanatorium	k1gNnSc1	sanatorium
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
sociální	sociální	k2eAgFnSc2d1	sociální
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
ve	v	k7c6	v
Vyšných	vyšný	k2eAgNnPc6d1	Vyšné
Hágách	Hága	k1gNnPc6	Hága
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Elektrických	elektrický	k2eAgInPc2d1	elektrický
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
</s>
</p>
<p>
<s>
Opevňovací	opevňovací	k2eAgFnSc1d1	opevňovací
práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
národní	národní	k2eAgFnSc1d1	národní
obranyFirma	obranyFirma	k1gFnSc1	obranyFirma
projektovala	projektovat	k5eAaBmAgFnS	projektovat
a	a	k8xC	a
provedla	provést	k5eAaPmAgFnS	provést
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
odvodňovací	odvodňovací	k2eAgFnPc4d1	odvodňovací
a	a	k8xC	a
kanalizační	kanalizační	k2eAgFnPc4d1	kanalizační
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Prováděla	provádět	k5eAaImAgFnS	provádět
i	i	k9	i
průzkumné	průzkumný	k2eAgFnPc4d1	průzkumná
práce	práce	k1gFnPc4	práce
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
projekt	projekt	k1gInSc4	projekt
pražské	pražský	k2eAgFnSc2d1	Pražská
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gNnSc4	Záruba-Pfeffermann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
v	v	k7c6	v
Revolučním	revoluční	k2eAgNnSc6d1	revoluční
národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
</s>
</p>
<p>
<s>
Nekrolog	nekrolog	k1gInSc1	nekrolog
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
politice	politika	k1gFnSc6	politika
</s>
</p>
<p>
<s>
Pobytové	pobytový	k2eAgFnPc1d1	pobytová
přihlášky	přihláška	k1gFnPc1	přihláška
pražského	pražský	k2eAgNnSc2d1	Pražské
policejního	policejní	k2eAgNnSc2d1	policejní
ředitelství	ředitelství	k1gNnSc2	ředitelství
(	(	kIx(	(
<g/>
konskripce	konskripce	k1gFnSc1	konskripce
<g/>
)	)	kIx)	)
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Pfeffermann	Pfeffermann	k1gMnSc1	Pfeffermann
Josef	Josef	k1gMnSc1	Josef
1869	[number]	k4	1869
</s>
</p>
