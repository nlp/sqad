<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnPc1d1	pokroková
a	a	k8xC	a
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
poslanec	poslanec	k1gMnSc1	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
?	?	kIx.	?
</s>
