<s>
Josef	Josef	k1gMnSc1	Josef
Záruba-Pfeffermann	Záruba-Pfeffermann	k1gMnSc1	Záruba-Pfeffermann
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1869	[number]	k4	1869
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1938	[number]	k4	1938
Starý	Starý	k1gMnSc1	Starý
Smokovec	Smokovec	k1gMnSc1	Smokovec
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
pokrokové	pokrokový	k2eAgFnPc1d1	pokroková
a	a	k8xC	a
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
poslanec	poslanec	k1gMnSc1	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
