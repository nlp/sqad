<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
1998	#num#	k4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorůZákladní	juniorůZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Datum	datum	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
Místo	místo	k7c2
</s>
<s>
Mora	mora	k1gFnSc1
a	a	k8xC
Malung	Malung	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
vítěz	vítěz	k1gMnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
druhý	druhý	k4xOgInSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
třetí	třetí	k4xOgMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
1998	#num#	k4
byl	být	k5eAaImAgInS
31	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
poslední	poslední	k2eAgInSc1d1
ročník	ročník	k1gInSc1
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turnaj	turnaj	k1gInSc1
hostila	hostit	k5eAaImAgFnS
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
švédská	švédský	k2eAgNnPc1d1
města	město	k1gNnPc1
Mora	mora	k1gFnSc1
a	a	k8xC
Malung	Malung	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráli	hrát	k5eAaImAgMnP
na	na	k7c6
něm	on	k3xPp3gMnSc6
hokejisté	hokejista	k1gMnPc1
narození	narození	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
a	a	k8xC
mladší	mladý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
soutěž	soutěž	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
mistrovstvím	mistrovství	k1gNnSc7
světa	svět	k1gInSc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Základní	základní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
</s>
<s>
Skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
TýmRUSFINCZENORZV	TýmRUSFINCZENORZV	k1gMnSc1
R	R	kA
P	P	kA
Skóre	skóre	k1gNnPc2
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
<g/>
—	—	k?
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
1321011	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finsko	Finsko	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
—	—	k?
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
320116	#num#	k4
<g/>
:	:	kIx,
<g/>
64	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
<g/>
—	—	k?
<g/>
12	#num#	k4
<g/>
:	:	kIx,
<g/>
131117	#num#	k4
<g/>
:	:	kIx,
<g/>
83	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norsko	Norsko	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
:	:	kIx,
<g/>
81	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
<g/>
—	—	k?
<g/>
30032	#num#	k4
<g/>
:	:	kIx,
<g/>
250	#num#	k4
</s>
<s>
Skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
TýmSWESUISVKUKRZ	TýmSWESUISVKUKRZ	k1gMnSc2
V	v	k7c6
R	R	kA
P	P	kA
Skóre	skóre	k1gNnPc2
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
<g/>
—	—	k?
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
313	#num#	k4
<g/>
:	:	kIx,
<g/>
1330024	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
—	—	k?
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
132019	#num#	k4
<g/>
:	:	kIx,
<g/>
84	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensko	Slovensko	k1gNnSc1
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
—	—	k?
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
131029	#num#	k4
<g/>
:	:	kIx,
<g/>
102	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajina	Ukrajina	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
131	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
—	—	k?
<g/>
30033	#num#	k4
<g/>
:	:	kIx,
<g/>
230	#num#	k4
</s>
<s>
Finálová	finálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
se	se	k3xPyFc4
započetly	započíst	k5eAaPmAgFnP
postupujícím	postupující	k2eAgNnSc7d1
i	i	k8xC
do	do	k7c2
finálové	finálový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístění	umístění	k1gNnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
skupině	skupina	k1gFnSc6
bylo	být	k5eAaImAgNnS
konečným	konečný	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
mužstev	mužstvo	k1gNnPc2
na	na	k7c6
turnaji	turnaj	k1gInSc6
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
TýmSWEFINRUSCZESUISVKZV	TýmSWEFINRUSCZESUISVKZV	k1gMnSc1
R	R	kA
P	P	kA
Skóre	skóre	k1gNnPc2
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
<g/>
—	—	k?
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
531122	#num#	k4
<g/>
:	:	kIx,
<g/>
127	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finsko	Finsko	k1gNnSc1
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
—	—	k?
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2531117	#num#	k4
<g/>
:	:	kIx,
<g/>
107	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
—	—	k?
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1531113	#num#	k4
<g/>
:	:	kIx,
<g/>
97	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
—	—	k?
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
3531121	#num#	k4
<g/>
:	:	kIx,
<g/>
177	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
<g/>
—	—	k?
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
51047	#num#	k4
<g/>
:	:	kIx,
<g/>
202	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensko	Slovensko	k1gNnSc1
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
—	—	k?
<g/>
500510	#num#	k4
<g/>
:	:	kIx,
<g/>
200	#num#	k4
</s>
<s>
O	o	k7c6
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Norsko	Norsko	k1gNnSc1
-	-	kIx~
Ukrajina	Ukrajina	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c4
zápasy	zápas	k1gInPc4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Všichni	všechen	k3xTgMnPc1
účastníci	účastník	k1gMnPc1
získali	získat	k5eAaPmAgMnP
právo	právo	k1gNnSc4
startovat	startovat	k5eAaBmF
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
1999	#num#	k4
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
nesestoupil	sestoupit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Turnajová	turnajový	k2eAgNnPc4d1
ocenění	ocenění	k1gNnPc4
</s>
<s>
Brankář	brankář	k1gMnSc1
</s>
<s>
Obránce	obránce	k1gMnSc1
</s>
<s>
Útočníci	útočník	k1gMnPc1
</s>
<s>
Ceny	cena	k1gFnPc1
direktoriátu	direktoriát	k1gInSc2
IIHF	IIHF	kA
</s>
<s>
Kristian	Kristian	k1gMnSc1
Antila	Antil	k1gMnSc2
</s>
<s>
Mikko	Mikko	k6eAd1
Jokela	Jokela	k1gFnSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Sedin	Sedin	k1gMnSc1
</s>
<s>
All-Star	All-Star	k1gInSc1
Tým	tým	k1gInSc1
(	(	kIx(
<g/>
podle	podle	k7c2
médií	médium	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Kristian	Kristian	k1gMnSc1
Antila	Antil	k1gMnSc2
</s>
<s>
Mikko	Mikko	k6eAd1
Jokela	Jokela	k1gFnSc1
<g/>
,	,	kIx,
Dmitrij	Dmitrij	k1gFnSc1
Kalinin	Kalinin	k2eAgInSc4d1
</s>
<s>
Denis	Denisa	k1gFnPc2
Švidkij	Švidkij	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Sedin	Sedin	k1gMnSc1
<g/>
,	,	kIx,
Marko	Marko	k1gMnSc1
Ahosilta	Ahosilta	k1gMnSc1
</s>
<s>
Produktivita	produktivita	k1gFnSc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Asistence	asistence	k1gFnSc1
</s>
<s>
Bbody	Bbody	k6eAd1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Sedin	Sedin	k1gMnSc1
<g/>
3811	#num#	k4
</s>
<s>
Denis	Denisa	k1gFnPc2
Švidkij	Švidkij	k1gFnSc4
<g/>
2911	#num#	k4
</s>
<s>
Michal	Michal	k1gMnSc1
Sivek	sivka	k1gFnPc2
<g/>
4610	#num#	k4
</s>
<s>
Henrik	Henrik	k1gMnSc1
Sedin	Sedin	k1gMnSc1
<g/>
549	#num#	k4
</s>
<s>
Milan	Milan	k1gMnSc1
Kraft	Kraft	k1gMnSc1
<g/>
538	#num#	k4
</s>
<s>
Mistři	mistr	k1gMnPc1
Evropy	Evropa	k1gFnSc2
-	-	kIx~
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
<g/>
:	:	kIx,
Johan	Johan	k1gMnSc1
Asplund	Asplund	k1gMnSc1
<g/>
,	,	kIx,
Tobias	Tobias	k1gMnSc1
Bössfall	Bössfall	k1gMnSc1
</s>
<s>
Obránci	obránce	k1gMnPc1
<g/>
:	:	kIx,
Christian	Christian	k1gMnSc1
Bäckman	Bäckman	k1gMnSc1
<g/>
,	,	kIx,
Gabriel	Gabriel	k1gMnSc1
Karlsson	Karlsson	k1gMnSc1
<g/>
,	,	kIx,
Rickard	Rickard	k1gMnSc1
Borgqvist	Borgqvist	k1gMnSc1
<g/>
,	,	kIx,
Patrik	Patrik	k1gMnSc1
Lindfors	Lindfors	k1gInSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Messa	Messa	k1gFnSc1
<g/>
,	,	kIx,
Jonas	Jonas	k1gMnSc1
Westerholm	Westerholm	k1gMnSc1
<g/>
,	,	kIx,
Kristofer	Kristofer	k1gMnSc1
Gustavsson	Gustavsson	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Nyström	Nyström	k1gMnSc1
<g/>
,	,	kIx,
Niklas	Niklas	k1gMnSc1
Kronwall	Kronwall	k1gMnSc1
</s>
<s>
Útočníci	útočník	k1gMnPc1
<g/>
:	:	kIx,
Henrik	Henrik	k1gMnSc1
Sedin	Sedin	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Sedin	Sedin	k1gMnSc1
<g/>
,	,	kIx,
Jonas	Jonas	k1gMnSc1
Frögren	Frögrna	k1gFnPc2
<g/>
,	,	kIx,
Per	pero	k1gNnPc2
Hallin	Hallin	k2eAgMnSc1d1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Svee	Sve	k1gFnSc2
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
Må	Må	k1gMnSc1
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
Berglund	Berglund	k1gMnSc1
<g/>
,	,	kIx,
Göran	Göran	k1gMnSc1
Lindbom	Lindbom	k1gInSc1
<g/>
,	,	kIx,
Rickard	Rickard	k1gInSc1
Wallin	Wallin	k1gInSc1
<g/>
,	,	kIx,
Henrik	Henrik	k1gMnSc1
Zetterberg	Zetterberg	k1gMnSc1
<g/>
,	,	kIx,
Mattias	Mattias	k1gMnSc1
Weinhandl	Weinhandl	k1gMnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
<g/>
:	:	kIx,
Michal	Michal	k1gMnSc1
Láníček	Láníček	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
</s>
<s>
Obránci	obránce	k1gMnPc1
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Hájek	Hájek	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Kutlák	Kutlák	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Jindra	Jindra	k1gMnSc1
<g/>
,	,	kIx,
Angel	angel	k1gMnSc1
Krstev	Krstev	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Sičák	Sičák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Chotěborký	Chotěborký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Vilášek	Vilášek	k1gMnSc1
</s>
<s>
Útočníci	útočník	k1gMnPc1
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Kristek	Kristka	k1gFnPc2
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Kraft	Kraft	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Sivek	sivka	k1gFnPc2
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
Irgl	Irgl	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Sochor	Sochor	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Boháč	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Brendl	Brendl	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Trávníček	Trávníček	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Padělek	padělek	k1gInSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Martinák	Martinák	k1gMnSc1
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2
skupiny	skupina	k1gFnPc1
</s>
<s>
B	B	kA
skupina	skupina	k1gFnSc1
</s>
<s>
Šampionát	šampionát	k1gInSc1
B	B	kA
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgMnS
ve	v	k7c4
Füssena	Füssen	k2eAgNnPc4d1
a	a	k8xC
Memmingenu	Memmingen	k1gInSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
postup	postup	k1gInSc4
na	na	k7c4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
1999	#num#	k4
si	se	k3xPyFc3
vybojovala	vybojovat	k5eAaPmAgFnS
domácí	domácí	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
C	C	kA
skupina	skupina	k1gFnSc1
</s>
<s>
Šampionát	šampionát	k1gInSc1
C	C	kA
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
Záhřebu	Záhřeb	k1gInSc6
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
vyhráli	vyhrát	k5eAaPmAgMnP
jej	on	k3xPp3gNnSc4
Rakušané	Rakušan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Jugoslávie	Jugoslávie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
D	D	kA
skupina	skupina	k1gFnSc1
</s>
<s>
Šampionát	šampionát	k1gInSc1
D	D	kA
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
Lucemburku	Lucemburk	k1gInSc6
v	v	k7c6
Lucembursku	Lucembursko	k1gNnSc6
<g/>
,	,	kIx,
vyhráli	vyhrát	k5eAaPmAgMnP
jej	on	k3xPp3gNnSc4
Kazaši	Kazach	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
Do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
1967	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
Do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
