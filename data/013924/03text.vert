<s>
Avaři	Avar	k1gMnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
historickém	historický	k2eAgNnSc6d1
etniku	etnikum	k1gNnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
kavkazských	kavkazský	k2eAgMnPc6d1
Avarech	Avar	k1gMnPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Avarové	Avarové	k?
(	(	kIx(
<g/>
Kavkaz	Kavkaz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
</s>
<s>
←	←	k?
←	←	k?
←	←	k?
</s>
<s>
567	#num#	k4
<g/>
–	–	k?
<g/>
822	#num#	k4
</s>
<s>
→	→	k?
→	→	k?
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
mezi	mezi	k7c7
lety	let	k1gInPc7
582	#num#	k4
<g/>
–	–	k?
<g/>
612	#num#	k4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
žádné	žádný	k3yNgInPc1
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
praslovanština	praslovanština	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
lingua	lingu	k2eAgFnSc1d1
franca	franca	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
turkický	turkický	k2eAgInSc4d1
<g/>
,	,	kIx,
mongolský	mongolský	k2eAgInSc4d1
<g/>
,	,	kIx,
tuguzský	tuguzský	k2eAgInSc4d1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
původně	původně	k6eAd1
šamanistické	šamanistický	k2eAgFnPc1d1
<g/>
,	,	kIx,
animistické	animistický	k2eAgFnPc1d1
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgFnPc1d1
(	(	kIx(
<g/>
po	po	k7c6
796	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
kmenová	kmenový	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
<g/>
,	,	kIx,
monarchie	monarchie	k1gFnSc1
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Langobardi	Langobard	k1gMnPc1
</s>
<s>
Gepidové	Gepid	k1gMnPc1
</s>
<s>
Hunská	hunský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Franská	franský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
bulharská	bulharský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Avaři	Avar	k1gMnPc1
byli	být	k5eAaImAgMnP
kočovné	kočovný	k2eAgNnSc4d1
etnikum	etnikum	k1gNnSc4
nejasného	jasný	k2eNgNnSc2d1
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
mongolského	mongolský	k2eAgInSc2d1
<g/>
,	,	kIx,
turkického	turkický	k2eAgInSc2d1
nebo	nebo	k8xC
indoevropského	indoevropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
nejspíše	nejspíše	k9
pocházelo	pocházet	k5eAaImAgNnS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jejich	jejich	k3xOp3gMnPc1
možní	možný	k2eAgMnPc1d1
příbuzní	příbuzný	k1gMnPc1
Hunové	Hun	k1gMnPc1
ze	z	k7c2
stepí	step	k1gFnPc2
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Avaři	Avar	k1gMnPc1
byli	být	k5eAaImAgMnP
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
možné	možný	k2eAgNnSc1d1
u	u	k7c2
Hunů	Hun	k1gMnPc2
<g/>
,	,	kIx,
směs	směs	k1gFnSc1
mongolských	mongolský	k2eAgInPc2d1
<g/>
,	,	kIx,
turkických	turkický	k2eAgInPc2d1
<g/>
,	,	kIx,
indoevropských	indoevropský	k2eAgInPc2d1
<g/>
,	,	kIx,
uralských	uralský	k2eAgInPc2d1
<g/>
,	,	kIx,
možná	možná	k9
i	i	k8xC
tunguzských	tunguzský	k2eAgNnPc2d1
a	a	k8xC
dalších	další	k2eAgNnPc2d1
kočovných	kočovný	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
svého	svůj	k3xOyFgInSc2
pobytu	pobyt	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
mísili	mísit	k5eAaImAgMnP
také	také	k9
se	s	k7c7
Slovany	Slovan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
od	od	k7c2
poloviny	polovina	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
počátku	počátek	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
hráli	hrát	k5eAaImAgMnP
značně	značně	k6eAd1
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
politickém	politický	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
Balkánu	Balkán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
Avarů	Avar	k1gMnPc2
</s>
<s>
Uměle	uměle	k6eAd1
prodloužená	prodloužený	k2eAgFnSc1d1
avarská	avarský	k2eAgFnSc1d1
lebka	lebka	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
Avarů	Avar	k1gMnPc2
je	být	k5eAaImIp3nS
zastřen	zastřít	k5eAaPmNgMnS
tajemstvím	tajemství	k1gNnSc7
a	a	k8xC
rozporuplnými	rozporuplný	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
mongolského	mongolský	k2eAgInSc2d1
nebo	nebo	k8xC
turkického	turkický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantský	byzantský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Menander	Menander	k1gMnSc1
Protector	Protector	k1gMnSc1
je	on	k3xPp3gInPc4
pokládal	pokládat	k5eAaImAgMnS
za	za	k7c4
příbuzné	příbuzný	k1gMnPc4
Hunů	Hun	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc4
původ	původ	k1gInSc4
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
pokládaný	pokládaný	k2eAgInSc1d1
za	za	k7c4
turkický	turkický	k2eAgInSc4d1
<g/>
,	,	kIx,
také	také	k9
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasný	jasný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Avaři	Avar	k1gMnPc1
s	s	k7c7
Turky	Turek	k1gMnPc7
sdíleli	sdílet	k5eAaImAgMnP
některé	některý	k3yIgInPc4
kulturní	kulturní	k2eAgInPc4d1
zvyky	zvyk	k1gInPc4
a	a	k8xC
výrazy	výraz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jejich	jejich	k3xOp3gMnSc7
jmenovci	jmenovec	k1gMnPc1
Avary	Avar	k1gMnPc7
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
je	být	k5eAaImIp3nS
spojují	spojovat	k5eAaImIp3nP
podobné	podobný	k2eAgInPc4d1
znaky	znak	k1gInPc4
na	na	k7c6
jejich	jejich	k3xOp3gNnPc6
pohřebištích	pohřebiště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kavkazští	kavkazský	k2eAgMnPc5d1
Avarové	Avarové	k?
však	však	k9
nemluví	mluvit	k5eNaImIp3nS
jazykem	jazyk	k1gInSc7
z	z	k7c2
rodiny	rodina	k1gFnSc2
altajské	altajský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
jazykem	jazyk	k1gInSc7
ze	z	k7c2
severovýchodní	severovýchodní	k2eAgFnSc2d1
kavkazské	kavkazský	k2eAgFnSc2d1
jazykové	jazykový	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staří	starý	k2eAgMnPc1d1
Turkové	Turek	k1gMnPc1
(	(	kIx(
<g/>
Gökturkové	Gökturkové	k2eAgInSc2d1
<g/>
)	)	kIx)
roku	rok	k1gInSc2
567	#num#	k4
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
požádali	požádat	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	k9
nové	nový	k2eAgMnPc4d1
nájezdníky	nájezdník	k1gMnPc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
pseudo-Avary	pseudo-Avar	k1gInPc4
a	a	k8xC
nazýval	nazývat	k5eAaImAgMnS
je	on	k3xPp3gInPc4
Varchonity	Varchonit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobé	soudobý	k2eAgInPc4d1
byzantské	byzantský	k2eAgInPc4d1
prameny	pramen	k1gInPc4
psaly	psát	k5eAaImAgFnP
o	o	k7c6
těchto	tento	k3xDgInPc6
Varchonitech	Varchonit	k1gInPc6
jako	jako	k8xC,k8xS
o	o	k7c6
někdejších	někdejší	k2eAgMnPc6d1
otrocích	otrok	k1gMnPc6
Turků	Turek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
jen	jen	k9
Avaři	Avar	k1gMnPc1
začali	začít	k5eAaPmAgMnP
říkat	říkat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
s	s	k7c7
tímto	tento	k3xDgNnSc7
jménem	jméno	k1gNnSc7
byla	být	k5eAaImAgFnS
spjata	spjat	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
prestiž	prestiž	k1gFnSc1
obávaných	obávaný	k2eAgMnPc2d1
válečníků	válečník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	k9
tvrzení	tvrzení	k1gNnSc1
Gökturků	Gökturk	k1gInPc2
bylo	být	k5eAaImAgNnS
pravdivé	pravdivý	k2eAgNnSc1d1
(	(	kIx(
<g/>
ti	ten	k3xDgMnPc1
však	však	k9
mohli	moct	k5eAaImAgMnP
Byzantince	Byzantinec	k1gMnSc4
záměrně	záměrně	k6eAd1
klamat	klamat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
snížili	snížit	k5eAaPmAgMnP
prestiž	prestiž	k1gFnSc4
Turkům	Turek	k1gMnPc3
nepřátelských	přátelský	k2eNgFnPc2d1
Avarů	Avar	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znamenalo	znamenat	k5eAaImAgNnS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
evropští	evropský	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
(	(	kIx(
<g/>
pseudo-Avaři	pseudo-Avař	k1gMnPc1
<g/>
)	)	kIx)
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
kmenů	kmen	k1gInPc2
Var	Vary	k1gInPc2
(	(	kIx(
<g/>
Uar	Uar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
War	War	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
a	a	k8xC
Hunni	Hunň	k1gMnSc3
(	(	kIx(
<g/>
Chionité	Chionitý	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
tvořily	tvořit	k5eAaImAgFnP
významnou	významný	k2eAgFnSc4d1
součást	součást	k1gFnSc4
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Hefthalitů	Hefthalit	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Bílých	bílý	k2eAgMnPc2d1
Hunů	Hun	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
byly	být	k5eAaImAgFnP
převážně	převážně	k6eAd1
indoevropského	indoevropský	k2eAgInSc2d1
původu	původ	k1gInSc2
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
Chionité	Chionitý	k2eAgNnSc4d1
byli	být	k5eAaImAgMnP
pravděpodobně	pravděpodobně	k6eAd1
míšenci	míšenec	k1gMnPc1
Hunů	Hun	k1gMnPc2
a	a	k8xC
početnějších	početní	k2eAgInPc2d2
indoevropských	indoevropský	k2eAgInPc2d1
–	–	k?
íránských	íránský	k2eAgInPc2d1
kočovných	kočovný	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
příslušníci	příslušník	k1gMnPc1
zřejmě	zřejmě	k6eAd1
vládnoucího	vládnoucí	k2eAgInSc2d1
kmene	kmen	k1gInSc2
Var	var	k1gInSc1
byli	být	k5eAaImAgMnP
patrně	patrně	k6eAd1
mongolského	mongolský	k2eAgInSc2d1
původu	původ	k1gInSc2
–	–	k?
v	v	k7c6
tomto	tento	k3xDgInSc6
kmenovém	kmenový	k2eAgInSc6d1
svazu	svaz	k1gInSc6
však	však	k9
etnicky	etnicky	k6eAd1
převládali	převládat	k5eAaImAgMnP
indoevropané	indoevropaný	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
prospěch	prospěch	k1gInSc4
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
hovoří	hovořit	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
lebky	lebka	k1gFnPc4
nalezené	nalezený	k2eAgFnPc4d1
v	v	k7c6
avarských	avarský	k2eAgInPc6d1
hrobech	hrob	k1gInPc6
jsou	být	k5eAaImIp3nP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
europoidního	europoidní	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
platí	platit	k5eAaImIp3nS
zejména	zejména	k9
pro	pro	k7c4
rané	raný	k2eAgNnSc4d1
období	období	k1gNnSc4
avarské	avarský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
europoidní	europoidní	k2eAgInSc1d1
charakter	charakter	k1gInSc1
Avarů	Avar	k1gMnPc2
tak	tak	k9
ještě	ještě	k9
nemohl	moct	k5eNaImAgMnS
být	být	k5eAaImF
příliš	příliš	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgMnS
jejich	jejich	k3xOp3gNnSc7
mísením	mísení	k1gNnSc7
se	s	k7c7
Slovany	Slovan	k1gInPc7
<g/>
)	)	kIx)
<g/>
;	;	kIx,
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
obdobích	období	k1gNnPc6
byl	být	k5eAaImAgInS
podíl	podíl	k1gInSc1
mongoloidních	mongoloidní	k2eAgInPc2d1
typů	typ	k1gInPc2
lebek	lebka	k1gFnPc2
v	v	k7c6
avarských	avarský	k2eAgInPc6d1
hrobech	hrob	k1gInPc6
o	o	k7c4
něco	něco	k3yInSc4
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
neboť	neboť	k8xC
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
přílivu	příliv	k1gInSc3
středoasijských	středoasijský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
uherské	uherský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
Avaři	Avar	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
pouze	pouze	k6eAd1
pseudo-Avaři	pseudo-Avař	k1gMnPc1
(	(	kIx(
<g/>
Varchonité	Varchonitý	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
se	se	k3xPyFc4
otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
koho	kdo	k3yInSc4,k3yQnSc4,k3yRnSc4
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
považovat	považovat	k5eAaImF
za	za	k7c4
skutečné	skutečný	k2eAgMnPc4d1
Avary	Avar	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
úvahu	úvaha	k1gFnSc4
připadají	připadat	k5eAaImIp3nP,k5eAaPmIp3nP
zejména	zejména	k9
příslušníci	příslušník	k1gMnPc1
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Rouranů	Rouran	k1gInPc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
též	též	k9
Žuan	Žuan	k1gNnSc1
–	–	k?
Žuanů	Žuan	k1gInPc2
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc1
pojmenování	pojmenování	k1gNnSc1
je	být	k5eAaImIp3nS
čínské	čínský	k2eAgNnSc1d1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
jak	jak	k8xS,k8xC
nazývali	nazývat	k5eAaImAgMnP
sami	sám	k3xTgMnPc1
sebe	sebe	k3xPyFc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vládnoucí	vládnoucí	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
tohoto	tento	k3xDgInSc2
svazu	svaz	k1gInSc2
byly	být	k5eAaImAgFnP
(	(	kIx(
<g/>
proto	proto	k6eAd1
<g/>
)	)	kIx)
<g/>
mongolského	mongolský	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
dle	dle	k7c2
čínských	čínský	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
byly	být	k5eAaImAgFnP
potomky	potomek	k1gMnPc4
starodávných	starodávný	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
Číně	Čína	k1gFnSc6
označovaných	označovaný	k2eAgMnPc2d1
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
Wuhuan	Wuhuan	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
dle	dle	k7c2
tehdejších	tehdejší	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
čínského	čínský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
vyslovováno	vyslovovat	k5eAaImNgNnS
jako	jako	k9
"	"	kIx"
<g/>
Avar	Avar	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
čínských	čínský	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
však	však	k9
i	i	k9
vládnoucí	vládnoucí	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Hefthalitů	Hefthalit	k1gInPc2
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
Rouranů	Rouran	k1gInPc2
(	(	kIx(
<g/>
potažmo	potažmo	k6eAd1
z	z	k7c2
kmenů	kmen	k1gInPc2
Wuhuan	Wuhuana	k1gFnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
tedy	tedy	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jak	jak	k6eAd1
Rourané	Rouraný	k2eAgInPc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
Hefthalité	Hefthalitý	k2eAgFnSc2d1
se	se	k3xPyFc4
označovali	označovat	k5eAaImAgMnP
za	za	k7c4
Avary	Avar	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gökturkové	Gökturkové	k2eAgNnSc7d1
(	(	kIx(
<g/>
bývalí	bývalý	k2eAgMnPc1d1
poddaní	poddaný	k1gMnPc1
Rouranů	Rouran	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
polovině	polovina	k1gFnSc6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyvrátili	vyvrátit	k5eAaPmAgMnP
říši	říše	k1gFnSc4
Rouranů	Rouran	k1gInPc2
a	a	k8xC
posléze	posléze	k6eAd1
(	(	kIx(
<g/>
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
Peršany	Peršan	k1gMnPc7
<g/>
)	)	kIx)
i	i	k9
říši	říše	k1gFnSc4
Hefthalitů	Hefthalit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
obou	dva	k4xCgFnPc6
zaniklých	zaniklý	k2eAgFnPc2d1
říší	říš	k1gFnPc2
prchala	prchat	k5eAaImAgFnS
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
jejich	jejich	k3xOp3gFnSc2
cesty	cesta	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
ke	k	k7c3
spojení	spojení	k1gNnSc3
těchto	tento	k3xDgInPc2
kmenů	kmen	k1gInPc2
v	v	k7c4
jeden	jeden	k4xCgInSc4
celek	celek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
evropských	evropský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
pod	pod	k7c7
názvem	název	k1gInSc7
"	"	kIx"
<g/>
Avaři	Avar	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
Avarům	Avar	k1gMnPc3
se	se	k3xPyFc4
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
cestě	cesta	k1gFnSc6
na	na	k7c4
západ	západ	k1gInSc4
připojili	připojit	k5eAaPmAgMnP
také	také	k9
turkičtí	turkický	k2eAgMnPc1d1
Oghurové	Oghur	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
mongolskými	mongolský	k2eAgInPc7d1
Rourany	Rouran	k1gInPc7
a	a	k8xC
převážně	převážně	k6eAd1
indoevropskými	indoevropský	k2eAgInPc7d1
Hefthality	Hefthalit	k1gInPc7
existovaly	existovat	k5eAaImAgFnP
již	již	k9
v	v	k7c6
Asii	Asie	k1gFnSc6
úzké	úzký	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
politické	politický	k2eAgFnSc6d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgFnSc3d1
a	a	k8xC
zřejmě	zřejmě	k6eAd1
i	i	k9
etnické	etnický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říše	říše	k1gFnSc1
Hefthalitů	Hefthalit	k1gInPc2
byla	být	k5eAaImAgFnS
(	(	kIx(
<g/>
alespoň	alespoň	k9
po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
)	)	kIx)
vazalem	vazal	k1gMnSc7
říše	říš	k1gFnSc2
Rouranů	Rouran	k1gInPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
za	za	k7c4
její	její	k3xOp3gNnSc4
západní	západní	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antropologická	antropologický	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
evropských	evropský	k2eAgMnPc2d1
Avarů	Avar	k1gMnPc2
naznačuje	naznačovat	k5eAaImIp3nS
vliv	vliv	k1gInSc4
indoevropských	indoevropský	k2eAgInPc2d1
a	a	k8xC
mongolských	mongolský	k2eAgInPc2d1
(	(	kIx(
<g/>
ev.	ev.	k?
turkických	turkický	k2eAgInPc2d1
<g/>
)	)	kIx)
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
například	například	k6eAd1
používali	používat	k5eAaImAgMnP
obrněnou	obrněný	k2eAgFnSc4d1
těžkou	těžký	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
u	u	k7c2
nich	on	k3xPp3gMnPc2
často	často	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
symbolika	symbolika	k1gFnSc1
s	s	k7c7
motivem	motiv	k1gInSc7
gryfa	gryf	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
typické	typický	k2eAgNnSc1d1
pro	pro	k7c4
íránské	íránský	k2eAgMnPc4d1
(	(	kIx(
<g/>
sarmatské	sarmatský	k2eAgMnPc4d1
<g/>
)	)	kIx)
kočovníky	kočovník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
účesy	účes	k1gInPc4
(	(	kIx(
<g/>
dva	dva	k4xCgInPc4
copy	cop	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
způsob	způsob	k1gInSc1
pohřbívání	pohřbívání	k1gNnSc2
a	a	k8xC
používání	používání	k1gNnSc2
titulů	titul	k1gInPc2
(	(	kIx(
<g/>
hodností	hodnost	k1gFnPc2
<g/>
)	)	kIx)
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
mongolské	mongolský	k2eAgInPc4d1
Rourany	Rouran	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochované	dochovaný	k2eAgInPc1d1
fragmenty	fragment	k1gInPc1
avarského	avarský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
zase	zase	k9
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
turkický	turkický	k2eAgInSc4d1
původ	původ	k1gInSc4
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
hunštinou	hunština	k1gFnSc7
<g/>
,	,	kIx,
protobulharštinou	protobulharština	k1gFnSc7
a	a	k8xC
dnešní	dnešní	k2eAgFnSc7d1
čuvašštinou	čuvašština	k1gFnSc7
bývá	bývat	k5eAaImIp3nS
stará	starý	k2eAgFnSc1d1
avarština	avarština	k1gFnSc1
řazena	řadit	k5eAaImNgFnS
do	do	k7c2
tzv.	tzv.	kA
oghurské	oghurský	k2eAgFnSc2d1
větve	větev	k1gFnSc2
turkických	turkický	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
;	;	kIx,
ony	onen	k3xDgInPc1,k3xPp3gInPc1
fragmenty	fragment	k1gInPc1
se	se	k3xPyFc4
ale	ale	k9
nemusí	muset	k5eNaImIp3nS
vztahovat	vztahovat	k5eAaImF
k	k	k7c3
avarštině	avarština	k1gFnSc3
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
k	k	k7c3
protobulharštině	protobulharština	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
však	však	k9
vyloučeno	vyloučit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
evropští	evropský	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
byli	být	k5eAaImAgMnP
výhradně	výhradně	k6eAd1
Hefthalité	Hefthalitý	k2eAgInPc4d1
(	(	kIx(
<g/>
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
již	již	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
středoasijské	středoasijský	k2eAgFnSc6d1
domovině	domovina	k1gFnSc6
přijali	přijmout	k5eAaPmAgMnP
řadu	řada	k1gFnSc4
prvků	prvek	k1gInPc2
od	od	k7c2
Rouranů	Rouran	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
Oghurů	Oghur	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
nikoliv	nikoliv	k9
nějaká	nějaký	k3yIgFnSc1
aliance	aliance	k1gFnSc1
Hefthalitů	Hefthalit	k1gInPc2
a	a	k8xC
Rouranů	Rouran	k1gInPc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
Oghurů	Oghur	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
také	také	k9
jméno	jméno	k1gNnSc1
významného	významný	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
Hefhtalitů	Hefhtalit	k1gInPc2
–	–	k?
Kušnavar	Kušnavar	k1gInSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
název	název	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Hefthalitů	Hefthalit	k1gInPc2
–	–	k?
Warvaliz	Warvaliza	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
patrně	patrně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
"	"	kIx"
<g/>
Město	město	k1gNnSc1
Avarů	Avar	k1gMnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
nacházelo	nacházet	k5eAaImAgNnS
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Afghánistánu	Afghánistán	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Avaři	Avar	k1gMnPc1
přivedli	přivést	k5eAaPmAgMnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
domestikované	domestikovaný	k2eAgMnPc4d1
buvoly	buvol	k1gMnPc4
indické	indický	k2eAgMnPc4d1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
chováni	chovat	k5eAaImNgMnP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
plemeno	plemeno	k1gNnSc1
buvola	buvol	k1gMnSc2
lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
spíše	spíše	k9
u	u	k7c2
Hefthalitů	Hefthalit	k1gInPc2
(	(	kIx(
<g/>
jejichž	jejichž	k3xOyRp3gFnSc1
říše	říše	k1gFnSc1
zasahovala	zasahovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
Indie	Indie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
Rouranů	Rouran	k1gInPc2
<g/>
,	,	kIx,
žijících	žijící	k2eAgMnPc2d1
na	na	k7c4
území	území	k1gNnSc4
dnešního	dnešní	k2eAgNnSc2d1
Mongolska	Mongolsko	k1gNnSc2
a	a	k8xC
přilehlých	přilehlý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Oghurů	Oghur	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
jezerem	jezero	k1gNnSc7
Balchaš	Balchaš	k1gInSc1
a	a	k8xC
pohořím	pohořet	k5eAaPmIp1nS
Altaj	Altaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimochodem	mimochodem	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
600	#num#	k4
n.	n.	k?
<g/>
l.	l.	k?
daroval	darovat	k5eAaPmAgMnS
avarský	avarský	k2eAgMnSc1d1
kagan	kagan	k1gMnSc1
(	(	kIx(
<g/>
chán	chán	k1gMnSc1
<g/>
)	)	kIx)
stádo	stádo	k1gNnSc1
těchto	tento	k3xDgMnPc2
buvolů	buvol	k1gMnPc2
králi	král	k1gMnSc3
Langobardů	Langobard	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
sídlili	sídlit	k5eAaImAgMnP
v	v	k7c6
Itálii	Itálie	k1gFnSc6
–	–	k?
také	také	k9
zde	zde	k6eAd1
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
plemeno	plemeno	k1gNnSc1
buvolů	buvol	k1gMnPc2
chová	chovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Výše	vysoce	k6eAd2
uvedené	uvedený	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
shrnout	shrnout	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
Avaři	Avar	k1gMnPc1
<g/>
"	"	kIx"
lze	lze	k6eAd1
v	v	k7c6
širším	široký	k2eAgInSc6d2
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc2
chápat	chápat	k5eAaImF
jako	jako	k8xS,k8xC
kmenový	kmenový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
tvořený	tvořený	k2eAgInSc1d1
příslušníky	příslušník	k1gMnPc7
různých	různý	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
významnou	významný	k2eAgFnSc4d1
(	(	kIx(
<g/>
a	a	k8xC
možná	možná	k9
většinovou	většinový	k2eAgFnSc4d1
<g/>
)	)	kIx)
část	část	k1gFnSc4
tohoto	tento	k3xDgInSc2
svazu	svaz	k1gInSc2
tvořili	tvořit	k5eAaImAgMnP
převážně	převážně	k6eAd1
indoevropští	indoevropský	k2eAgMnPc1d1
Hefthalité	Hefthalitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládnoucí	vládnoucí	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
tohoto	tento	k3xDgInSc2
svazu	svaz	k1gInSc2
tvořili	tvořit	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
kmene	kmen	k1gInSc2
Avarů	Avar	k1gMnPc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
tedy	tedy	k9
Var	var	k1gInSc1
<g/>
/	/	kIx~
<g/>
Uar	Uar	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
patrně	patrně	k6eAd1
mongolského	mongolský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
již	již	k6eAd1
v	v	k7c6
Asii	Asie	k1gFnSc6
ovládal	ovládat	k5eAaImAgInS
Rourany	Rouran	k1gInPc7
i	i	k8xC
Hefthality	Hefthalit	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
avarského	avarský	k2eAgInSc2d1
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
také	také	k9
turkičtí	turkický	k2eAgMnPc1d1
Oghurové	Oghur	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
Avary	Avar	k1gMnPc7
pak	pak	k6eAd1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
uherské	uherský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
i	i	k9
Kutrigurové	Kutrigurový	k2eAgInPc1d1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
lze	lze	k6eAd1
řadit	řadit	k5eAaImF
k	k	k7c3
turkickým	turkický	k2eAgMnPc3d1
Bulharům	Bulhar	k1gMnPc3
(	(	kIx(
<g/>
Hunům	Hun	k1gMnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
další	další	k2eAgInPc4d1
turkické	turkický	k2eAgInPc4d1
a	a	k8xC
možná	možná	k9
i	i	k9
ugrofinské	ugrofinský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
uherské	uherský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
se	se	k3xPyFc4
Avaři	Avar	k1gMnPc1
a	a	k8xC
Kutrigurové	Kutrigur	k1gMnPc1
dále	daleko	k6eAd2
mísili	mísit	k5eAaImAgMnP
se	s	k7c7
Slovany	Slovan	k1gMnPc7
a	a	k8xC
germánskými	germánský	k2eAgMnPc7d1
Gepidy	Gepid	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
kavkazských	kavkazský	k2eAgMnPc2d1
Avarů	Avar	k1gMnPc2
také	také	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
odvozován	odvozovat	k5eAaImNgInS
od	od	k7c2
kmene	kmen	k1gInSc2
Var	var	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ovládl	ovládnout	k5eAaPmAgMnS
Hefthality	Hefthalita	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tito	tento	k3xDgMnPc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
460	#num#	k4
n.	n.	k?
<g/>
l.	l.	k?
podnikli	podniknout	k5eAaPmAgMnP
invazi	invaze	k1gFnSc4
do	do	k7c2
oblasti	oblast	k1gFnSc2
severního	severní	k2eAgInSc2d1
Kavkazu	Kavkaz	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
část	část	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
usadila	usadit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
se	se	k3xPyFc4
Avaři	Avar	k1gMnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
objevili	objevit	k5eAaPmAgMnP
až	až	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
6	#num#	k4
stol.	stol.	k?
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
přesunem	přesun	k1gInSc7
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
usazení	usazení	k1gNnPc2
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
později	pozdě	k6eAd2
přestali	přestat	k5eAaPmAgMnP
používat	používat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
původní	původní	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
a	a	k8xC
převzali	převzít	k5eAaPmAgMnP
jazyk	jazyk	k1gInSc4
kavkazských	kavkazský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Příchod	příchod	k1gInSc1
Avarů	Avar	k1gMnPc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
Turkických	turkický	k2eAgInPc2d1
národů	národ	k1gInPc2
Historie	historie	k1gFnSc2
turkických	turkický	k2eAgInPc2d1
národůdo	národůdo	k1gNnSc4
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Turkucký	Turkucký	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
552	#num#	k4
<g/>
–	–	k?
<g/>
744	#num#	k4
</s>
<s>
Turkuti	Turkut	k1gMnPc1
</s>
<s>
Západní	západní	k2eAgMnPc1d1
Turkuti	Turkut	k1gMnPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Turkuti	Turkuť	k1gFnPc1
</s>
<s>
Modří	modrý	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
564	#num#	k4
<g/>
–	–	k?
<g/>
804	#num#	k4
</s>
<s>
Chazarský	chazarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
618	#num#	k4
<g/>
–	–	k?
<g/>
1048	#num#	k4
</s>
<s>
Sı	Sı	k2eAgMnSc1d1
628	#num#	k4
<g/>
–	–	k?
<g/>
646	#num#	k4
</s>
<s>
Onogurie	Onogurie	k1gFnSc1
632	#num#	k4
<g/>
–	–	k?
<g/>
668	#num#	k4
</s>
<s>
Dunajské	dunajský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Volžské	volžský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Kangarská	Kangarský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
659	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
</s>
<s>
Türgešský	Türgešský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
699	#num#	k4
<g/>
–	–	k?
<g/>
766	#num#	k4
</s>
<s>
Ujgurský	Ujgurský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
744	#num#	k4
<g/>
–	–	k?
<g/>
840	#num#	k4
</s>
<s>
Karlucký	Karlucký	k2eAgInSc1d1
jabguluk	jabguluk	k1gInSc1
756	#num#	k4
<g/>
–	–	k?
<g/>
940	#num#	k4
</s>
<s>
Karachánský	Karachánský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
840	#num#	k4
<g/>
–	–	k?
<g/>
1212	#num#	k4
</s>
<s>
Západní	západní	k2eAgMnPc1d1
Karachánové	Karachán	k1gMnPc1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
Karachánové	Karachánové	k2eAgFnSc1d1
</s>
<s>
Kan-čouské	Kan-čouský	k2eAgNnSc1d1
království	království	k1gNnSc1
848-1036	848-1036	k4
</s>
<s>
Kao-čchangské	Kao-čchangský	k2eAgNnSc1d1
království	království	k1gNnSc1
856-1335	856-1335	k4
</s>
<s>
Pečeněžský	Pečeněžský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
860	#num#	k4
<g/>
–	–	k?
<g/>
1091	#num#	k4
</s>
<s>
Kimäcký	Kimäcký	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
743	#num#	k4
<g/>
–	–	k?
<g/>
1035	#num#	k4
</s>
<s>
Kumánie	Kumánie	k1gFnSc1
<g/>
1067	#num#	k4
<g/>
–	–	k?
<g/>
1239	#num#	k4
</s>
<s>
Oguzský	Oguzský	k2eAgInSc1d1
jabguluk	jabguluk	k1gInSc1
<g/>
750	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
</s>
<s>
Ghaznovská	Ghaznovský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
963	#num#	k4
<g/>
–	–	k?
<g/>
1186	#num#	k4
</s>
<s>
Seldžucká	Seldžucký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1037	#num#	k4
<g/>
–	–	k?
<g/>
1194	#num#	k4
</s>
<s>
Chórezmská	Chórezmský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1077	#num#	k4
<g/>
–	–	k?
<g/>
1231	#num#	k4
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Seldžuků	Seldžuk	k1gInPc2
1092	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
</s>
<s>
Dillíský	dillíský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
1206	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
</s>
<s>
Dynastie	dynastie	k1gFnSc1
otroků	otrok	k1gMnPc2
(	(	kIx(
<g/>
Dillí	Dillí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Chaldží	Chaldž	k1gFnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Tughlakovců	Tughlakovec	k1gMnPc2
</s>
<s>
Kypčacký	Kypčacký	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
1240	#num#	k4
<g/>
–	–	k?
<g/>
1502	#num#	k4
</s>
<s>
Mamlúcký	mamlúcký	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
1250	#num#	k4
<g/>
–	–	k?
<g/>
1517	#num#	k4
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Bahríovců	Bahríovec	k1gMnPc2
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1299	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
turkické	turkický	k2eAgFnPc1d1
dynastie	dynastie	k1gFnPc1
</s>
<s>
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Artúkovců	Artúkovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Saltúkovců	Saltúkovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Ázerbájdžánu	Ázerbájdžán	k1gInSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Íldenizovců	Íldenizovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Egyptu	Egypt	k1gInSc3
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Tulúnovců	Tulúnovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Íchšídovců	Íchšídovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Levantě	Levanta	k1gFnSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Búríjovců	Búríjovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Zengíjovců	Zengíjovec	k1gMnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
•	•	k?
editovat	editovat	k5eAaImF
</s>
<s>
Birituální	Birituální	k2eAgFnSc1d1
pohřebište	pohřebišit	k5eAaPmRp2nP,k5eAaImRp2nP
středního	střední	k2eAgNnSc2d1
a	a	k8xC
pozdního	pozdní	k2eAgNnSc2d1
období	období	k1gNnSc2
kaganátu	kaganát	k1gInSc2
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
hranice	hranice	k1gFnSc1
pozdně	pozdně	k6eAd1
avarského	avarský	k2eAgInSc2d1
kaganátu	kaganát	k1gInSc2
</s>
<s>
Do	do	k7c2
Evropy	Evropa	k1gFnSc2
přišli	přijít	k5eAaPmAgMnP
Avaři	Avar	k1gMnPc1
roku	rok	k1gInSc2
558	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
kmenový	kmenový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
nebyl	být	k5eNaImAgInS
příliš	příliš	k6eAd1
početný	početný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
záhy	záhy	k6eAd1
se	se	k3xPyFc4
rozrostl	rozrůst	k5eAaPmAgInS
o	o	k7c4
kmeny	kmen	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
připojeny	připojit	k5eAaPmNgInP
dobrovolně	dobrovolně	k6eAd1
i	i	k9
násilím	násilí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřily	patřit	k5eAaImAgFnP
mezi	mezi	k7c4
ně	on	k3xPp3gInPc4
zbytky	zbytek	k1gInPc4
Hunů	Hun	k1gMnPc2
<g/>
,	,	kIx,
bulharští	bulharský	k2eAgMnPc1d1
Utigurové	Utigur	k1gMnPc1
a	a	k8xC
především	především	k6eAd1
Kutrigurové	Kutrigur	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
zaujali	zaujmout	k5eAaPmAgMnP
v	v	k7c6
avarském	avarský	k2eAgNnSc6d1
společenství	společenství	k1gNnSc6
mocensky	mocensky	k6eAd1
významné	významný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
už	už	k6eAd1
tehdy	tehdy	k6eAd1
objevili	objevit	k5eAaPmAgMnP
v	v	k7c6
řadách	řada	k1gFnPc6
kočovných	kočovný	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
také	také	k9
Slované	Slovan	k1gMnPc1
<g/>
,	,	kIx,
především	především	k9
Antové	Ant	k1gMnPc1
<g/>
,	,	kIx,
sídlící	sídlící	k2eAgMnPc1d1
mezi	mezi	k7c7
Dněstrem	Dněstr	k1gInSc7
a	a	k8xC
Dněprem	Dněpr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
byzantského	byzantský	k2eAgMnSc2d1
autora	autor	k1gMnSc2
Theofylakta	Theofylakt	k1gMnSc2
Simokatta	Simokatt	k1gMnSc2
to	ten	k3xDgNnSc1
byli	být	k5eAaImAgMnP
právě	právě	k6eAd1
Avaři	Avar	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
po	po	k7c6
roce	rok	k1gInSc6
602	#num#	k4
podrobili	podrobit	k5eAaPmAgMnP
mocný	mocný	k2eAgInSc4d1
antský	antský	k2eAgInSc4d1
kmenový	kmenový	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
vedením	vedení	k1gNnSc7
avarského	avarský	k2eAgInSc2d1
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
byzantskou	byzantský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
však	však	k9
nebyly	být	k5eNaImAgInP
zpočátku	zpočátku	k6eAd1
nepřátelské	přátelský	k2eNgInPc1d1
<g/>
,	,	kIx,
ba	ba	k9
právě	právě	k9
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
panují	panovat	k5eAaImIp3nP
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
mocenské	mocenský	k2eAgFnSc2d1
poměry	poměra	k1gFnSc2
a	a	k8xC
hodlali	hodlat	k5eAaImAgMnP
je	on	k3xPp3gInPc4
využít	využít	k5eAaPmF
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
byla	být	k5eAaImAgFnS
finančně	finančně	k6eAd1
vyčerpaná	vyčerpaný	k2eAgFnSc1d1
dlouhodobými	dlouhodobý	k2eAgFnPc7d1
válkami	válka	k1gFnPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vedla	vést	k5eAaImAgFnS
na	na	k7c6
několika	několik	k4yIc6
frontách	fronta	k1gFnPc6
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císaře	Císař	k1gMnSc4
Justiniána	Justinián	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
jemuž	jenž	k3xRgNnSc3
táhlo	táhnout	k5eAaImAgNnS
na	na	k7c4
osmdesátku	osmdesátka	k1gFnSc4
<g/>
,	,	kIx,
zmáhala	zmáhat	k5eAaImAgFnS
nechuť	nechuť	k1gFnSc4
k	k	k7c3
rozhodným	rozhodný	k2eAgNnPc3d1
opatřením	opatření	k1gNnPc3
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
mu	on	k3xPp3gMnSc3
často	často	k6eAd1
chyběly	chybět	k5eAaImAgInP
prostředky	prostředek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáků	voják	k1gMnPc2
bylo	být	k5eAaImAgNnS
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
demoralizovaná	demoralizovaný	k2eAgFnSc1d1
a	a	k8xC
pokud	pokud	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
nedostávalo	dostávat	k5eNaImAgNnS
zásob	zásoba	k1gFnPc2
potravin	potravina	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
potřebných	potřebný	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
neváhali	váhat	k5eNaImAgMnP
velitelé	velitel	k1gMnPc1
dát	dát	k5eAaPmF
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
vyplenění	vyplenění	k1gNnSc3
vlastní	vlastní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
si	se	k3xPyFc3
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
vědomi	vědom	k2eAgMnPc1d1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
svými	svůj	k3xOyFgInPc7
útoky	útok	k1gInPc7
váží	vážit	k5eAaImIp3nS
síly	síla	k1gFnPc1
nepřátelské	přátelský	k2eNgFnPc1d1
Byzanci	Byzanc	k1gFnSc3
(	(	kIx(
<g/>
především	především	k6eAd1
Slovany	Slovan	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
toho	ten	k3xDgNnSc2
využít	využít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
roku	rok	k1gInSc2
558	#num#	k4
přijal	přijmout	k5eAaPmAgMnS
Justinián	Justinián	k1gMnSc1
I.	I.	kA
avarské	avarský	k2eAgMnPc4d1
posly	posel	k1gMnPc4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
dojednal	dojednat	k5eAaPmAgMnS
placení	placení	k1gNnSc2
tributu	tribut	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bohatě	bohatě	k6eAd1
obdaroval	obdarovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
se	se	k3xPyFc4
nezdráhal	zdráhat	k5eNaImAgMnS
vyplácet	vyplácet	k5eAaImF
Avarům	Avar	k1gMnPc3
tribut	tribut	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
tím	ten	k3xDgNnSc7
až	až	k9
na	na	k7c4
drobné	drobný	k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
podařilo	podařit	k5eAaPmAgNnS
zabránit	zabránit	k5eAaPmF
jejich	jejich	k3xOp3gNnSc4
vpádům	vpád	k1gInPc3
na	na	k7c4
byzantské	byzantský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
562	#num#	k4
vypravil	vypravit	k5eAaPmAgInS
avarský	avarský	k2eAgInSc1d1
kagan	kagan	k1gInSc1
k	k	k7c3
Justiniánovi	Justinián	k1gMnSc3
poselstvo	poselstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ho	on	k3xPp3gMnSc4
požádalo	požádat	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
poskytl	poskytnout	k5eAaPmAgMnS
Avarům	Avar	k1gMnPc3
k	k	k7c3
osídlení	osídlení	k1gNnSc3
tzv.	tzv.	kA
Malou	malý	k2eAgFnSc4d1
Skythii	Skythie	k1gFnSc4
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc4
rozkládající	rozkládající	k2eAgFnSc2d1
se	se	k3xPyFc4
mezi	mezi	k7c7
dolním	dolní	k2eAgInSc7d1
tokem	tok	k1gInSc7
Dunaje	Dunaj	k1gInSc2
a	a	k8xC
Černým	černý	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
rumunská	rumunský	k2eAgFnSc1d1
Dobrudža	Dobrudža	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
dobře	dobře	k6eAd1
ovládat	ovládat	k5eAaImF
Anty	Ant	k1gMnPc4
i	i	k9
expandovat	expandovat	k5eAaImF
do	do	k7c2
nitra	nitro	k1gNnSc2
balkánské	balkánský	k2eAgFnSc2d1
části	část	k1gFnSc2
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	on	k3xPp3gInPc4
lákala	lákat	k5eAaImAgFnS
k	k	k7c3
výbojům	výboj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justinián	Justinián	k1gMnSc1
však	však	k9
nehodlal	hodlat	k5eNaImAgMnS
na	na	k7c4
tento	tento	k3xDgInSc4
návrh	návrh	k1gInSc4
přistoupit	přistoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posílil	posílit	k5eAaPmAgMnS
posádky	posádka	k1gFnPc4
<g/>
,	,	kIx,
chránící	chránící	k2eAgFnSc4d1
dunajskou	dunajský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
měly	mít	k5eAaImAgInP
Avarům	Avar	k1gMnPc3
zabránit	zabránit	k5eAaPmF
v	v	k7c6
překročení	překročení	k1gNnSc6
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obratným	obratný	k2eAgNnSc7d1
jednáním	jednání	k1gNnSc7
se	se	k3xPyFc4
navíc	navíc	k6eAd1
byzantské	byzantský	k2eAgFnSc3d1
diplomacii	diplomacie	k1gFnSc3
podařilo	podařit	k5eAaPmAgNnS
vzbudit	vzbudit	k5eAaPmF
u	u	k7c2
Avarů	Avar	k1gMnPc2
zájem	zájem	k1gInSc1
o	o	k7c6
tažení	tažení	k1gNnSc6
proti	proti	k7c3
franské	franský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Pronikání	pronikání	k1gNnSc1
Avarů	Avar	k1gMnPc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Avarské	avarský	k2eAgFnPc1d1
hordy	horda	k1gFnPc1
pronikly	proniknout	k5eAaPmAgFnP
rychle	rychle	k6eAd1
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankům	Frank	k1gMnPc3
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
Avary	Avar	k1gMnPc4
odrazit	odrazit	k5eAaPmF
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
stepí	step	k1gFnPc2
poblíž	poblíž	k7c2
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantský	byzantský	k2eAgInSc1d1
tribut	tribut	k1gInSc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
stále	stále	k6eAd1
ještě	ještě	k9
představoval	představovat	k5eAaImAgInS
dobrý	dobrý	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
roku	rok	k1gInSc2
565	#num#	k4
císař	císař	k1gMnSc1
Justinián	Justinián	k1gMnSc1
I.	I.	kA
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
synovec	synovec	k1gMnSc1
a	a	k8xC
nástupce	nástupce	k1gMnSc1
Justinus	Justinus	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
565	#num#	k4
<g/>
-	-	kIx~
<g/>
578	#num#	k4
<g/>
)	)	kIx)
vzápětí	vzápětí	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
vykupovat	vykupovat	k5eAaImF
si	se	k3xPyFc3
nadále	nadále	k6eAd1
mír	mír	k1gInSc4
tributem	tribut	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
se	se	k3xPyFc4
proto	proto	k8xC
pokusili	pokusit	k5eAaPmAgMnP
o	o	k7c4
nový	nový	k2eAgInSc4d1
výboj	výboj	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
franské	franský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
uspěli	uspět	k5eAaPmAgMnP
a	a	k8xC
navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
v	v	k7c6
Panonii	Panonie	k1gFnSc6
usídlený	usídlený	k2eAgInSc1d1
germánský	germánský	k2eAgInSc1d1
kmen	kmen	k1gInSc1
Langobardů	Langobard	k1gMnPc2
požádal	požádat	k5eAaPmAgInS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
sousední	sousední	k2eAgFnSc3d1
říši	říš	k1gFnSc3
Gepidů	Gepid	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgMnPc7
byl	být	k5eAaImAgMnS
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
zaměřili	zaměřit	k5eAaPmAgMnP
svoji	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
bývalou	bývalý	k2eAgFnSc4d1
římskou	římský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
Langobardi	Langobard	k1gMnPc1
porazili	porazit	k5eAaPmAgMnP
nakonec	nakonec	k6eAd1
Gepidy	Gepida	k1gFnPc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
sami	sám	k3xTgMnPc1
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
Avaři	Avar	k1gMnPc1
velkou	velký	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
i	i	k8xC
území	území	k1gNnSc4
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
obývané	obývaný	k2eAgFnSc2d1
Gepidy	Gepida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
568	#num#	k4
pak	pak	k6eAd1
odtáhli	odtáhnout	k5eAaPmAgMnP
Langobardi	Langobard	k1gMnPc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
přestalo	přestat	k5eAaPmAgNnS
bezprostřední	bezprostřední	k2eAgNnSc4d1
sousedství	sousedství	k1gNnSc4
bojovných	bojovný	k2eAgMnPc2d1
nomádů	nomád	k1gMnPc2
brzy	brzy	k6eAd1
vyhovovat	vyhovovat	k5eAaImF
<g/>
,	,	kIx,
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dodnes	dodnes	k6eAd1
nese	nést	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
(	(	kIx(
<g/>
Lombardie	Lombardie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Avaři	Avar	k1gMnPc1
poté	poté	k6eAd1
ovládli	ovládnout	k5eAaPmAgMnP
celou	celý	k2eAgFnSc4d1
Karpatskou	karpatský	k2eAgFnSc4d1
kotlinu	kotlina	k1gFnSc4
i	i	k8xC
zdejší	zdejší	k2eAgNnSc4d1
slovanské	slovanský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
panství	panství	k1gNnSc1
sahalo	sahat	k5eAaImAgNnS
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
až	až	k9
do	do	k7c2
benátské	benátský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stýkalo	stýkat	k5eAaImAgNnS
s	s	k7c7
územím	území	k1gNnSc7
ovládaným	ovládaný	k2eAgNnSc7d1
Franky	Franky	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
po	po	k7c4
Sirmium	Sirmium	k1gNnSc4
<g/>
,	,	kIx,
významné	významný	k2eAgNnSc4d1
obchodní	obchodní	k2eAgNnSc4d1
a	a	k8xC
vojenské	vojenský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
Dunaje	Dunaj	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
Byzanc	Byzanc	k1gFnSc1
sice	sice	k8xC
nejprve	nejprve	k6eAd1
udržela	udržet	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
582	#num#	k4
po	po	k7c6
nástupu	nástup	k1gInSc6
císaře	císař	k1gMnSc2
Maurikia	Maurikius	k1gMnSc2
ho	on	k3xPp3gInSc4
definitivně	definitivně	k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
strategicky	strategicky	k6eAd1
výhodným	výhodný	k2eAgNnSc7d1
nástupištěm	nástupiště	k1gNnSc7
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
expanzi	expanze	k1gFnSc3
jak	jak	k8xS,k8xC
proti	proti	k7c3
franské	franský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
tak	tak	k8xS,k8xC
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slované	Slovan	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
hojně	hojně	k6eAd1
podíleli	podílet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
podmanění	podmanění	k1gNnSc4
<g/>
,	,	kIx,
konali	konat	k5eAaImAgMnP
při	při	k7c6
taženích	tažení	k1gNnPc6
různé	různý	k2eAgFnSc2d1
pomocné	pomocný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
stavěli	stavět	k5eAaImAgMnP
mosty	most	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
slovanské	slovanský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
staly	stát	k5eAaPmAgFnP
avarskými	avarský	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
kočovníky	kočovník	k1gMnPc7
podnikaly	podnikat	k5eAaImAgFnP
kořistnické	kořistnický	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
byli	být	k5eAaImAgMnP
Avaři	Avar	k1gMnPc1
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
neporazitelné	porazitelný	k2eNgFnPc4d1
<g/>
,	,	kIx,
alespoň	alespoň	k9
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avarskou	avarský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
tvořila	tvořit	k5eAaImAgFnS
slovanská	slovanský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
<g/>
,	,	kIx,
bulharská	bulharský	k2eAgFnSc1d1
lehká	lehký	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
a	a	k8xC
avarská	avarský	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avarští	avarský	k2eAgMnPc1d1
těžkooděnci	těžkooděnec	k1gMnPc1
–	–	k?
obrnění	obrnění	k1gNnSc2
jezdci	jezdec	k1gMnPc1
–	–	k?
výrazně	výrazně	k6eAd1
ovlivnili	ovlivnit	k5eAaPmAgMnP
pozdější	pozdní	k2eAgInSc4d2
vývoj	vývoj	k1gInSc4
evropského	evropský	k2eAgNnSc2d1
válečnictví	válečnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimochodem	mimochodem	k6eAd1
byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
patrně	patrně	k6eAd1
Avaři	Avar	k1gMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
jako	jako	k9
první	první	k4xOgFnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
používal	používat	k5eAaImAgMnS
jezdecké	jezdecký	k2eAgInPc4d1
třmeny	třmen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
novinka	novinka	k1gFnSc1
poskytovala	poskytovat	k5eAaImAgFnS
avarským	avarský	k2eAgMnPc3d1
jezdcům	jezdec	k1gMnPc3
v	v	k7c6
boji	boj	k1gInSc6
značnou	značný	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
vůči	vůči	k7c3
protivníkům	protivník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
takto	takto	k6eAd1
vybaveni	vybaven	k2eAgMnPc1d1
nebyli	být	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
už	už	k6eAd1
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
nezvládala	zvládat	k5eNaImAgFnS
platit	platit	k5eAaImF
tribut	tribut	k1gInSc4
<g/>
,	,	kIx,
Avaři	Avar	k1gMnPc1
podporovaní	podporovaný	k2eAgMnPc1d1
Slovany	Slovan	k1gInPc4
začali	začít	k5eAaPmAgMnP
znovu	znovu	k6eAd1
pořádat	pořádat	k5eAaImF
nájezdy	nájezd	k1gInPc4
na	na	k7c4
římské	římský	k2eAgFnPc4d1
komunity	komunita	k1gFnPc4
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
útoky	útok	k1gInPc1
odrážela	odrážet	k5eAaImAgFnS
a	a	k8xC
dokonce	dokonce	k9
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
odvetnou	odvetný	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
za	za	k7c4
Dunaj	Dunaj	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
rozhodnutí	rozhodnutí	k1gNnSc1
Maurikia	Maurikium	k1gNnSc2
zde	zde	k6eAd1
i	i	k9
přezimovat	přezimovat	k5eAaBmF
vedlo	vést	k5eAaImAgNnS
roku	rok	k1gInSc2
602	#num#	k4
k	k	k7c3
revoltě	revolta	k1gFnSc3
a	a	k8xC
svržení	svržení	k1gNnSc3
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Fóky	Fóka	k1gMnSc2
bylo	být	k5eAaImAgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
610	#num#	k4
zápolení	zápolení	k1gNnSc4
vyrovnané	vyrovnaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
v	v	k7c6
Byzanci	Byzanc	k1gFnSc6
ale	ale	k8xC
zuřila	zuřit	k5eAaImAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
využila	využít	k5eAaPmAgFnS
zejména	zejména	k9
Persie	Persie	k1gFnPc1
k	k	k7c3
získání	získání	k1gNnSc3
území	území	k1gNnSc2
na	na	k7c6
východě	východ	k1gInSc6
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
Avarům	Avar	k1gMnPc3
značně	značně	k6eAd1
uvolnilo	uvolnit	k5eAaPmAgNnS
ruce	ruka	k1gFnPc4
na	na	k7c6
nebráněném	bráněný	k2eNgInSc6d1
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejného	stejný	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
610	#num#	k4
<g/>
)	)	kIx)
vtrhli	vtrhnout	k5eAaPmAgMnP
i	i	k9
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zhruba	zhruba	k6eAd1
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
franský	franský	k2eAgMnSc1d1
kupec	kupec	k1gMnSc1
Sámo	Sámo	k1gMnSc1
vedl	vést	k5eAaImAgMnS
Slovany	Slovan	k1gMnPc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
v	v	k7c4
povstání	povstání	k1gNnSc4
proti	proti	k7c3
Avarům	Avar	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanům	Slovan	k1gMnPc3
se	se	k3xPyFc4
už	už	k6eAd1
nelíbila	líbit	k5eNaImAgFnS
poroba	poroba	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
násilné	násilný	k2eAgInPc4d1
svazky	svazek	k1gInPc4
Avarů	Avar	k1gMnPc2
se	s	k7c7
slovanskými	slovanský	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Kdykoli	kdykoli	k6eAd1
Avaři	Avar	k1gMnPc1
vojensky	vojensky	k6eAd1
útočili	útočit	k5eAaImAgMnP
na	na	k7c4
některý	některý	k3yIgInSc4
národ	národ	k1gInSc4
<g/>
,	,	kIx,
stáli	stát	k5eAaImAgMnP
s	s	k7c7
celým	celý	k2eAgInSc7d1
svým	svůj	k3xOyFgInSc7
vojskem	vojsko	k1gNnSc7
před	před	k7c7
táborem	tábor	k1gInSc7
<g/>
,	,	kIx,
Slované	Slovan	k1gMnPc1
pak	pak	k6eAd1
bojovali	bojovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
nabývali	nabývat	k5eAaImAgMnP
vrchu	vrch	k1gInSc3
a	a	k8xC
vítězili	vítězit	k5eAaImAgMnP
<g/>
,	,	kIx,
tu	ten	k3xDgFnSc4
Avaři	Avar	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
zmocnili	zmocnit	k5eAaPmAgMnP
kořisti	kořist	k1gFnSc3
<g/>
;	;	kIx,
jestliže	jestliže	k8xS
však	však	k9
byli	být	k5eAaImAgMnP
Slované	Slovan	k1gMnPc1
přemáháni	přemáhat	k5eAaImNgMnP
<g/>
,	,	kIx,
opřeli	opřít	k5eAaPmAgMnP
se	se	k3xPyFc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
Avarů	Avar	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
nabrali	nabrat	k5eAaPmAgMnP
nových	nový	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
...	...	k?
Avaři	Avar	k1gMnPc1
přicházeli	přicházet	k5eAaImAgMnP
každoročně	každoročně	k6eAd1
ke	k	k7c3
Slovanům	Slovan	k1gMnPc3
přezimovat	přezimovat	k5eAaBmF
<g/>
,	,	kIx,
brali	brát	k5eAaImAgMnP
si	se	k3xPyFc3
do	do	k7c2
lože	lože	k1gNnSc2
manželky	manželka	k1gFnSc2
Slovanů	Slovan	k1gMnPc2
i	i	k8xC
jejich	jejich	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
jiných	jiný	k2eAgInPc2d1
projevů	projev	k1gInPc2
útlaku	útlak	k1gInSc2
platili	platit	k5eAaImAgMnP
Slované	Slovan	k1gMnPc1
Avarům	Avar	k1gMnPc3
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
synové	syn	k1gMnPc1
Avarů	Avar	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
zplodili	zplodit	k5eAaPmAgMnP
s	s	k7c7
manželkami	manželka	k1gFnPc7
a	a	k8xC
dcerami	dcera	k1gFnPc7
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
nechtěli	chtít	k5eNaImAgMnP
již	již	k6eAd1
snášet	snášet	k5eAaImF
křivdy	křivda	k1gFnPc4
a	a	k8xC
útisk	útisk	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
odmítajíce	odmítat	k5eAaImSgFnP
nadvládu	nadvláda	k1gFnSc4
Avarů	Avar	k1gMnPc2
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
bouřit	bouřit	k5eAaImF
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Fredegarova	Fredegarův	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
626	#num#	k4
podnikli	podniknout	k5eAaPmAgMnP
Avaři	Avar	k1gMnPc1
spolu	spolu	k6eAd1
s	s	k7c7
balkánskými	balkánský	k2eAgMnPc7d1
Slovany	Slovan	k1gMnPc7
společný	společný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nezdařený	zdařený	k2eNgInSc1d1
útok	útok	k1gInSc1
s	s	k7c7
Peršany	peršan	k1gInPc7
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
neúspěchu	neúspěch	k1gInSc2
bylo	být	k5eAaImAgNnS
byzantské	byzantský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
slovanské	slovanský	k2eAgInPc1d1
dlabané	dlabaný	k2eAgInPc1d1
čluny	člun	k1gInPc1
nebyly	být	k5eNaImAgInP
velkým	velký	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
armády	armáda	k1gFnPc1
se	se	k3xPyFc4
proto	proto	k6eAd1
nedokázaly	dokázat	k5eNaPmAgFnP
spojit	spojit	k5eAaPmF
a	a	k8xC
koordinovat	koordinovat	k5eAaBmF
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Panonie	Panonie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
většinu	většina	k1gFnSc4
Balkánu	Balkán	k1gInSc2
ponechali	ponechat	k5eAaPmAgMnP
slovanským	slovanský	k2eAgInPc3d1
kmenům	kmen	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
ani	ani	k9
Avaři	Avar	k1gMnPc1
<g/>
,	,	kIx,
ani	ani	k8xC
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
nedokázali	dokázat	k5eNaPmAgMnP
ovládat	ovládat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
650	#num#	k4
na	na	k7c6
mapě	mapa	k1gFnSc6
Evropy	Evropa	k1gFnSc2
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
630	#num#	k4
<g/>
–	–	k?
<g/>
635	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
avarské	avarský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Avary	Avar	k1gMnPc7
a	a	k8xC
Bulhary	Bulhar	k1gMnPc7
(	(	kIx(
<g/>
Kutrigury	Kutrigur	k1gMnPc7
a	a	k8xC
Onogury	Onogur	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
sice	sice	k8xC
uhájili	uhájit	k5eAaPmAgMnP
kaganský	kaganský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
Bulhaři	Bulhar	k1gMnPc1
žijící	žijící	k2eAgMnPc1d1
ve	v	k7c6
stepích	step	k1gFnPc6
severně	severně	k6eAd1
od	od	k7c2
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
získali	získat	k5eAaPmAgMnP
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
avarské	avarský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
670	#num#	k4
však	však	k9
Bulhaři	Bulhar	k1gMnPc1
(	(	kIx(
<g/>
Onogurové	Onogur	k1gMnPc1
<g/>
)	)	kIx)
začínají	začínat	k5eAaImIp3nP
z	z	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
prchat	prchat	k5eAaImF
pod	pod	k7c7
tlakem	tlak	k1gInSc7
Chazarů	Chazar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
skupina	skupina	k1gFnSc1
Bulharů	Bulhar	k1gMnPc2
našla	najít	k5eAaPmAgFnS
útočiště	útočiště	k1gNnSc4
právě	právě	k6eAd1
u	u	k7c2
Avarů	Avar	k1gMnPc2
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gFnSc1
další	další	k2eAgFnSc1d1
část	část	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
dnešní	dnešní	k2eAgNnSc4d1
Bulharsko	Bulharsko	k1gNnSc4
<g/>
,	,	kIx,
jiná	jiný	k2eAgNnPc1d1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
dnešního	dnešní	k2eAgInSc2d1
Tatarstánu	Tatarstán	k1gInSc2
založila	založit	k5eAaPmAgFnS
Volžské	volžský	k2eAgNnSc4d1
Bulharsko	Bulharsko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Onogurové	Onogur	k1gMnPc1
výrazně	výrazně	k6eAd1
ovlivnili	ovlivnit	k5eAaPmAgMnP
etnický	etnický	k2eAgInSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
avarské	avarský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
o	o	k7c6
avarské	avarský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
poměrně	poměrně	k6eAd1
málo	málo	k4c1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgInPc1d1
výboje	výboj	k1gInPc1
na	na	k7c6
území	území	k1gNnSc6
sousedních	sousední	k2eAgInPc2d1
států	stát	k1gInPc2
se	se	k3xPyFc4
již	již	k6eAd1
patrně	patrně	k6eAd1
nekonaly	konat	k5eNaImAgFnP
<g/>
,	,	kIx,
Avaři	Avar	k1gMnPc1
zřejmě	zřejmě	k6eAd1
spíše	spíše	k9
těžili	těžit	k5eAaImAgMnP
z	z	k7c2
ovládání	ovládání	k1gNnSc2
obchodních	obchodní	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jejich	jejich	k3xOp3gInSc3
říší	říš	k1gFnSc7
procházely	procházet	k5eAaImAgFnP
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
postupně	postupně	k6eAd1
přecházeli	přecházet	k5eAaImAgMnP
k	k	k7c3
zemědělství	zemědělství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
Dolní	dolní	k2eAgFnPc1d1
Dunajovice	Dunajovice	k1gFnPc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
slovansko-avarské	slovansko-avarský	k2eAgNnSc1d1
pohřebiště	pohřebiště	k1gNnSc1
z	z	k7c2
doby	doba	k1gFnSc2
předvelkomoravské	předvelkomoravský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Bylo	být	k5eAaImAgNnS
by	by	kYmCp3nS
chybou	chyba	k1gFnSc7
považovat	považovat	k5eAaImF
vztah	vztah	k1gInSc4
Avarů	Avar	k1gMnPc2
ke	k	k7c3
Slovanům	Slovan	k1gMnPc3
za	za	k7c4
pouhou	pouhý	k2eAgFnSc4d1
tyranii	tyranie	k1gFnSc4
<g/>
;	;	kIx,
tento	tento	k3xDgInSc4
vztah	vztah	k1gInSc4
byl	být	k5eAaImAgMnS
komplexní	komplexní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
slovanské	slovanský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
žily	žít	k5eAaImAgFnP
v	v	k7c6
avarské	avarský	k2eAgFnSc6d1
porobě	poroba	k1gFnSc6
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
uzavřely	uzavřít	k5eAaPmAgFnP
s	s	k7c7
Avary	Avar	k1gMnPc4
spojenectví	spojenectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časem	časem	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
společné	společný	k2eAgFnSc2d1
avarsko-slovanské	avarsko-slovanský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
genetickému	genetický	k2eAgNnSc3d1
smísení	smísení	k1gNnSc3
obou	dva	k4xCgInPc2
etnik	etnikum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
také	také	k9
výrazně	výrazně	k6eAd1
napomohli	napomoct	k5eAaPmAgMnP
rozšíření	rozšíření	k1gNnSc3
Slovanů	Slovan	k1gInPc2
do	do	k7c2
jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
možná	možná	k9
i	i	k9
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
–	–	k?
bez	bez	k7c2
vojensky	vojensky	k6eAd1
zdatných	zdatný	k2eAgMnPc2d1
Avarů	Avar	k1gMnPc2
schopných	schopný	k2eAgMnPc2d1
prosadit	prosadit	k5eAaPmF
se	se	k3xPyFc4
vůči	vůči	k7c3
Frankům	Franky	k1gInPc3
a	a	k8xC
Byzanci	Byzanc	k1gFnSc6
by	by	kYmCp3nS
slovanská	slovanský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
těchto	tento	k3xDgNnPc2
území	území	k1gNnPc2
byla	být	k5eAaImAgFnS
obtížná	obtížný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kolonizace	kolonizace	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Avaři	Avar	k1gMnPc1
a	a	k8xC
Slované	Slovan	k1gMnPc1
podnikli	podniknout	k5eAaPmAgMnP
společné	společný	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
a	a	k8xC
zlikvidovali	zlikvidovat	k5eAaPmAgMnP
domácí	domácí	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
se	se	k3xPyFc4
následně	následně	k6eAd1
s	s	k7c7
kořistí	kořist	k1gFnSc7
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
uherské	uherský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Slované	Slovan	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c6
dobytém	dobytý	k2eAgNnSc6d1
území	území	k1gNnSc6
usadili	usadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
náboženství	náboženství	k1gNnSc4
Avarů	Avar	k1gMnPc2
toho	ten	k3xDgNnSc2
není	být	k5eNaImIp3nS
mnoho	mnoho	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
vyznávali	vyznávat	k5eAaImAgMnP
tengrismus	tengrismus	k1gInSc4
<g/>
,	,	kIx,
možná	možná	k9
také	také	k9
manicheismus	manicheismus	k1gInSc1
<g/>
,	,	kIx,
vyloučen	vyloučen	k2eAgInSc1d1
není	být	k5eNaImIp3nS
ani	ani	k9
buddhismus	buddhismus	k1gInSc1
nebo	nebo	k8xC
zoroastriánství	zoroastriánství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušné	příslušný	k2eAgInPc4d1
archeologické	archeologický	k2eAgInPc4d1
nálezy	nález	k1gInPc4
obsahují	obsahovat	k5eAaImIp3nP
pozoruhodně	pozoruhodně	k6eAd1
vysoký	vysoký	k2eAgInSc4d1
počet	počet	k1gInSc4
artefaktů	artefakt	k1gInPc2
judaistického	judaistický	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
názoru	názor	k1gInSc2
Byzantinců	Byzantinec	k1gMnPc2
byli	být	k5eAaImAgMnP
Avaři	Avar	k1gMnPc1
vyspělejší	vyspělý	k2eAgMnPc1d2
než	než	k8xS
jiní	jiný	k2eAgMnPc1d1
stepní	stepní	k2eAgMnPc1d1
nomádi	nomád	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
zapříčiněno	zapříčiněn	k2eAgNnSc1d1
jejich	jejich	k3xOp3gNnSc4
kontakty	kontakt	k1gInPc1
s	s	k7c7
perskou	perský	k2eAgFnSc7d1
<g/>
,	,	kIx,
indickou	indický	k2eAgFnSc7d1
a	a	k8xC
čínskou	čínský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Byzantské	byzantský	k2eAgInPc1d1
a	a	k8xC
franské	franský	k2eAgInPc1d1
prameny	pramen	k1gInPc1
však	však	k9
popisují	popisovat	k5eAaImIp3nP
Avary	Avar	k1gMnPc4
také	také	k9
jako	jako	k9
agresivní	agresivní	k2eAgMnPc4d1
barbary	barbar	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vynikali	vynikat	k5eAaImAgMnP
lstivostí	lstivost	k1gFnPc2
a	a	k8xC
praktikovali	praktikovat	k5eAaImAgMnP
černou	černý	k2eAgFnSc4d1
magii	magie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
zprávy	zpráva	k1gFnPc1
však	však	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
tendenční	tendenční	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
Avarů	Avar	k1gMnPc2
</s>
<s>
Avarské	avarský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Roku	rok	k1gInSc2
791	#num#	k4
vedl	vést	k5eAaImAgInS
společně	společně	k6eAd1
s	s	k7c7
panonským	panonský	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
Vojnomírem	Vojnomír	k1gMnSc7
proti	proti	k7c3
Avarům	Avar	k1gMnPc3
vojenskou	vojenský	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
Karel	Karel	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
avarská	avarský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
dekonsolidovaná	dekonsolidovaný	k2eAgFnSc1d1
vytrvalou	vytrvalý	k2eAgFnSc7d1
vnitřní	vnitřní	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
avarské	avarský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
totiž	totiž	k9
vypukla	vypuknout	k5eAaPmAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
stoupenci	stoupenec	k1gMnPc7
kagana	kagan	k1gMnSc2
(	(	kIx(
<g/>
chána	chán	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
jugura	jugur	k1gMnSc2
(	(	kIx(
<g/>
nejvyššího	vysoký	k2eAgMnSc2d3
kněze	kněz	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
této	tento	k3xDgFnSc2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
neshoda	neshoda	k1gFnSc1
v	v	k7c6
otázce	otázka	k1gFnSc6
zaujetí	zaujetí	k1gNnSc2
postoje	postoj	k1gInSc2
k	k	k7c3
franské	franský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
kagan	kagan	k1gMnSc1
realisticky	realisticky	k6eAd1
prosazoval	prosazovat	k5eAaImAgMnS
zaujetí	zaujetí	k1gNnSc4
vazalského	vazalský	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
Avarů	Avar	k1gMnPc2
vůči	vůči	k7c3
mocnějším	mocný	k2eAgInPc3d2
Frankům	Franky	k1gInPc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jugur	jugur	k1gMnSc1
toto	tento	k3xDgNnSc4
odmítal	odmítat	k5eAaImAgMnS
(	(	kIx(
<g/>
Avaři	Avar	k1gMnPc1
by	by	kYmCp3nS
museli	muset	k5eAaImAgMnP
přijmout	přijmout	k5eAaPmF
křest	křest	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
frakce	frakce	k1gFnPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
prakticky	prakticky	k6eAd1
zlikvidovaly	zlikvidovat	k5eAaPmAgInP
<g/>
,	,	kIx,
v	v	k7c6
bojích	boj	k1gInPc6
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
kagan	kagan	k1gInSc1
i	i	k9
jugur	jugur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmíněná	zmíněný	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
Karla	Karel	k1gMnSc2
Velikého	veliký	k2eAgInSc2d1
z	z	k7c2
roku	rok	k1gInSc2
791	#num#	k4
neuspěla	uspět	k5eNaPmAgFnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
další	další	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
796	#num#	k4
již	již	k6eAd1
ano	ano	k9
a	a	k8xC
Frankové	Frank	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
avarské	avarský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylí	zbylý	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
před	před	k7c7
Karlem	Karel	k1gMnSc7
Velikým	veliký	k2eAgMnSc7d1
ustoupili	ustoupit	k5eAaPmAgMnP
za	za	k7c4
řeku	řeka	k1gFnSc4
Tisu	tis	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
roku	rok	k1gInSc2
822	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
o	o	k7c6
nich	on	k3xPp3gMnPc6
poslední	poslední	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
v	v	k7c6
písemných	písemný	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc1
říše	říše	k1gFnSc1
úplně	úplně	k6eAd1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
za	za	k7c2
významného	významný	k2eAgNnSc2d1
přispění	přispění	k1gNnSc2
slovanských	slovanský	k2eAgNnPc2d1
knížat	kníže	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
využila	využít	k5eAaPmAgFnS
fransko-avarských	fransko-avarský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylí	zbylý	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
zřejmě	zřejmě	k6eAd1
splývali	splývat	k5eAaImAgMnP
se	s	k7c7
slovanským	slovanský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zde	zde	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
nitranské	nitranský	k2eAgNnSc4d1
a	a	k8xC
blatenské	blatenský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
vtrhli	vtrhnout	k5eAaPmAgMnP
Maďaři	Maďar	k1gMnPc1
mluvící	mluvící	k2eAgFnSc4d1
uralským	uralský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
prý	prý	k9
setkali	setkat	k5eAaPmAgMnP
se	s	k7c7
zbytky	zbytek	k1gInPc7
Hunů	Hun	k1gMnPc2
<g/>
,	,	kIx,
což	což	k9
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
zbytky	zbytek	k1gInPc4
Avarů	Avar	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Frankové	Franková	k1gFnPc1
na	na	k7c6
přelomu	přelom	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
stol	stol	k1gInSc1
ovládli	ovládnout	k5eAaPmAgMnP
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
avarské	avarský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
bulharští	bulharský	k2eAgMnPc1d1
Onogurové	Onogur	k1gMnPc1
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
mimochodem	mimochodem	k9
název	název	k1gInSc1
Hungarus	Hungarus	k1gInSc1
<g/>
)	)	kIx)
ovládli	ovládnout	k5eAaPmAgMnP
její	její	k3xOp3gFnPc4
jihovýchodní	jihovýchodní	k2eAgFnPc4d1
část	část	k1gFnSc4
a	a	k8xC
začlenili	začlenit	k5eAaPmAgMnP
Avary	Avar	k1gMnPc4
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zdokumentováno	zdokumentován	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
811	#num#	k4
se	se	k3xPyFc4
Avaři	Avar	k1gMnPc1
účastnili	účastnit	k5eAaImAgMnP
útoku	útok	k1gInSc3
Bulharů	Bulhar	k1gMnPc2
na	na	k7c4
Byzanc	Byzanc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
dokazují	dokazovat	k5eAaImIp3nP
významnou	významný	k2eAgFnSc4d1
avarskou	avarský	k2eAgFnSc4d1
přítomnost	přítomnost	k1gFnSc4
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
ještě	ještě	k9
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
toto	tento	k3xDgNnSc4
území	území	k1gNnSc4
již	již	k6eAd1
ovládali	ovládat	k5eAaImAgMnP
Maďaři	Maďar	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definitivní	definitivní	k2eAgInSc1d1
zánik	zánik	k1gInSc1
Avarů	Avar	k1gMnPc2
patrně	patrně	k6eAd1
proběhl	proběhnout	k5eAaPmAgInS
jejich	jejich	k3xOp3gFnSc7
asimilací	asimilace	k1gFnSc7
s	s	k7c7
Maďary	Maďar	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Avarů	Avar	k1gMnPc2
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
odvozován	odvozován	k2eAgInSc1d1
původ	původ	k1gInSc1
maďarského	maďarský	k2eAgNnSc2d1
etnika	etnikum	k1gNnSc2
Székely	Székela	k1gFnSc2
(	(	kIx(
<g/>
Sikulové	Sikulový	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avaři	Avar	k1gMnPc1
dále	daleko	k6eAd2
dle	dle	k7c2
některých	některý	k3yIgInPc2
názorů	názor	k1gInPc2
mohli	moct	k5eAaImAgMnP
ovlivnit	ovlivnit	k5eAaPmF
etnogenezi	etnogeneze	k1gFnSc4
Chorvatů	Chorvat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
také	také	k9
spekulace	spekulace	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
vládnoucí	vládnoucí	k2eAgInSc1d1
rod	rod	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
byl	být	k5eAaImAgInS
tvořen	tvořit	k5eAaImNgInS
potomky	potomek	k1gMnPc4
avarské	avarský	k2eAgFnSc2d1
aristokracie	aristokracie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
dnešního	dnešní	k2eAgNnSc2d1
Maďarska	Maďarsko	k1gNnSc2
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
mnoho	mnoho	k4c1
avarských	avarský	k2eAgFnPc2d1
ruin	ruina	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
pohřebiště	pohřebiště	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
nalezené	nalezený	k2eAgInPc1d1
symboly	symbol	k1gInPc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
shodují	shodovat	k5eAaImIp3nP
se	s	k7c7
symboly	symbol	k1gInPc7
používanými	používaný	k2eAgMnPc7d1
kavkazskými	kavkazský	k2eAgMnPc7d1
Avary	Avar	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Avaři	Avar	k1gMnPc1
ve	v	k7c6
slovanských	slovanský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
Avaři	Avar	k1gMnPc1
byli	být	k5eAaImAgMnP
tak	tak	k6eAd1
výrazným	výrazný	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
slovanských	slovanský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
většině	většina	k1gFnSc6
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
se	se	k3xPyFc4
slovo	slovo	k1gNnSc1
Avar	Avar	k1gMnSc1
stalo	stát	k5eAaPmAgNnS
synonymem	synonymum	k1gNnSc7
pro	pro	k7c4
velkého	velký	k2eAgMnSc4d1
nebo	nebo	k8xC
mocného	mocný	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
–	–	k?
obra	obr	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Avarští	avarský	k2eAgMnPc1d1
kaganové	kagan	k1gMnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
důkladnou	důkladný	k2eAgFnSc4d1
jazykovou	jazykový	k2eAgFnSc4d1
korekturu	korektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspiraci	inspirace	k1gFnSc4
k	k	k7c3
vylepšení	vylepšení	k1gNnSc3
můžete	moct	k5eAaImIp2nP
hledat	hledat	k5eAaImF
v	v	k7c6
radách	rada	k1gFnPc6
na	na	k7c6
stránce	stránka	k1gFnSc6
Pravopisné	pravopisný	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
pořadítituljménopřibližnýrok	pořadítituljménopřibližnýrok	k1gInSc1
vládnutízemřelpoznámky	vládnutízemřelpoznámka	k1gFnSc2
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
králSarošu	králSarosat	k5eAaPmIp1nS
nebo	nebo	k8xC
Sarosios	Sarosios	k1gInSc1
<g/>
(	(	kIx(
<g/>
časné	časný	k2eAgInPc1d1
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
-první	-první	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
-	-	kIx~
Sarodius	Sarodius	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
dědeček	dědeček	k1gMnSc1
byl	být	k5eAaImAgMnS
Nothuliu	Nothulius	k1gMnSc3
<g/>
,	,	kIx,
<g/>
zatím	zatím	k6eAd1
ještě	ještě	k9
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
rozkvět	rozkvět	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existovalv	existovalv	k1gMnSc1
uvedené	uvedený	k2eAgFnSc3d1
době	doba	k1gFnSc3
</s>
<s>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
kaganAnakuej	kaganAnakuej	k1gInSc1
<g/>
522522	#num#	k4
<g/>
Jü-ťiou-lü	Jü-ťiou-lü	k1gFnPc2
A-na-kuej	A-na-kuej	k1gInSc1
(	(	kIx(
<g/>
郁	郁	k?
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
-Anločen--Jü-ťiou-lü	-Anločen--Jü-ťiou-lü	k?
An-luo-čchen	An-luo-čchen	k1gInSc1
(	(	kIx(
<g/>
郁	郁	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
Anakuejův	Anakuejův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
-Kanadík	-Kanadík	k1gInSc1
<g/>
(	(	kIx(
<g/>
Kuti	kut	k2eAgMnPc1d1
<g/>
,	,	kIx,
Kangti	Kangt	k2eAgMnPc1d1
nebo	nebo	k8xC
Kazarig	Kazarig	k1gMnSc1
<g/>
)	)	kIx)
<g/>
552554	#num#	k4
<g/>
duben	duben	k1gInSc1
552	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
poražených	poražený	k2eAgMnPc2d1
Žuan-Žuanů	Žuan-Žuan	k1gMnPc2
uprchla	uprchnout	k5eAaPmAgFnS
na	na	k7c6
území	území	k1gNnSc6
Severní	severní	k2eAgFnSc2d1
Čchi	Čch	k1gFnSc2
(	(	kIx(
<g/>
550	#num#	k4
<g/>
–	–	k?
<g/>
577	#num#	k4
<g/>
)	)	kIx)
<g/>
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Období	období	k1gNnSc1
jižních	jižní	k2eAgFnPc2d1
a	a	k8xC
severních	severní	k2eAgFnPc2d1
dynystií	dynystie	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kaganem	kagan	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Ku-tchi	Ku-tch	k1gFnSc3
(	(	kIx(
<g/>
古	古	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pozdějiznámý	pozdějiznámý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Jü-ťiou-lü	Jü-ťiou-lü	k1gMnSc1
Kchang-tchi	Kchang-tch	k1gFnSc2
(	(	kIx(
<g/>
郁	郁	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
východní	východní	k2eAgMnPc1d1
Hunobulhaři	Hunobulhař	k1gMnPc1
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
bylijeho	bylijeze	k6eAd1
vazaly	vazal	k1gMnPc7
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
známi	znám	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
Kazarigové	Kazarig	k1gMnPc1
(	(	kIx(
<g/>
Kutrigurové	Kutrigur	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
-Bat-bajan	-Bat-bajan	k1gInSc1
nebo	nebo	k8xC
Bajar	Bajar	k1gInSc1
<g/>
562	#num#	k4
<g/>
-založil	-založit	k5eAaPmAgInS,k5eAaImAgInS
Avarskou	avarský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
soustředěnou	soustředěný	k2eAgFnSc4d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
dnešnímRuskem	dnešnímRusek	k1gInSc7
a	a	k8xC
Maďarskem	Maďarsko	k1gNnSc7
a	a	k8xC
pozdější	pozdní	k2eAgFnSc7d2
Besarábií	Besarábie	k1gFnSc7
</s>
<s>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s desamb="1">
<g/>
---kagan	---kagan	k1gInSc1
s	s	k7c7
neznámým	známý	k2eNgNnSc7d1
jménem	jméno	k1gNnSc7
</s>
<s>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
----kagan	----kagan	k1gInSc1
s	s	k7c7
neznámým	známý	k2eNgNnSc7d1
jménem	jméno	k1gNnSc7
</s>
<s>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
?	?	kIx.
</s>
<s desamb="1">
<g/>
--druhá	--druhý	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
-	-	kIx~
(	(	kIx(
<g/>
635	#num#	k4
<g/>
-	-	kIx~
<g/>
685	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
?	?	kIx.
</s>
<s desamb="1">
<g/>
--třetí	--třetit	k5eAaPmIp3nS
dynastie	dynastie	k1gFnSc1
-	-	kIx~
(	(	kIx(
<g/>
685	#num#	k4
<g/>
-	-	kIx~
<g/>
795	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
?	?	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
časné	časný	k2eAgFnSc2d1
790	#num#	k4
<g/>
)	)	kIx)
<g/>
-rozkvět	-rozkvět	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existoval	existovat	k5eAaImAgInS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
,	,	kIx,
<g/>
v	v	k7c6
opozici	opozice	k1gFnSc6
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
kaganJugurus	kaganJugurus	k1gInSc1
<g/>
(	(	kIx(
<g/>
časné	časný	k2eAgInPc1d1
790	#num#	k4
<g/>
)	)	kIx)
<g/>
-rozkvět	-rozkvět	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existoval	existovat	k5eAaImAgInS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
době	doba	k1gFnSc6
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
-Kajd	-Kajd	k1gMnSc1
Tudun	Tudun	k1gInSc4
<g/>
795803	#num#	k4
<g/>
čtvrtá	čtvrtá	k1gFnSc1
dynastie	dynastie	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
(	(	kIx(
<g/>
795	#num#	k4
<g/>
-	-	kIx~
<g/>
899	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
-Zodan	-Zodan	k1gInSc1
<g/>
803805	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s desamb="1">
<g/>
Teodor	Teodora	k1gFnPc2
<g/>
805814	#num#	k4
<g/>
od	od	k7c2
roku	rok	k1gInSc2
805	#num#	k4
subjekty	subjekt	k1gInPc7
Karlovské	Karlovský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
-Abraham	-Abraham	k6eAd1
<g/>
810	#num#	k4
<g/>
-rozkvět	-rozkvět	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existoval	existovat	k5eAaImAgInS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
době	doba	k1gFnSc6
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
-Isaac	-Isaac	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
Tudun	Tudun	k1gInSc4
<g/>
826	#num#	k4
<g/>
-rozkvět	-rozkvět	k5eAaPmF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existoval	existovat	k5eAaImAgInS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
době	doba	k1gFnSc6
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
--	--	k?
<g/>
844	#num#	k4
<g/>
-jméno	-jméno	k6eAd1
neznámé	známý	k2eNgNnSc1d1
<g/>
,	,	kIx,
rozkvět	rozkvět	k1gInSc1
<g/>
,	,	kIx,
<g/>
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
že	že	k8xS
existoval	existovat	k5eAaImAgInS
v	v	k7c6
uvedené	uvedený	k2eAgFnSc6d1
době	doba	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CAVENDISH	CAVENDISH	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peoples	Peoples	k1gInSc1
of	of	k?
Western	Western	kA
Asia	Asia	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Marshall	Marshall	k1gInSc1
Cavendish	Cavendisha	k1gFnPc2
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7614	#num#	k4
<g/>
-	-	kIx~
<g/>
7677	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7614	#num#	k4
<g/>
-	-	kIx~
<g/>
7677	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOSWORTH	BOSWORTH	kA
<g/>
,	,	kIx,
Clifford	Clifford	k1gMnSc1
Edmund	Edmund	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historic	Historic	k1gMnSc1
Cities	Cities	k1gMnSc1
of	of	k?
the	the	k?
Islamic	Islamic	k1gMnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
:	:	kIx,
Brill	Brill	k1gInSc1
NV	NV	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
280	#num#	k4
s.	s.	k?
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15388	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BORRERO	BORRERO	kA
<g/>
,	,	kIx,
Mauricio	Mauricio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russia	Russium	k1gNnPc1
<g/>
:	:	kIx,
A	a	k9
Reference	reference	k1gFnPc1
Guide	Guid	k1gInSc5
from	fro	k1gNnSc7
the	the	k?
Renaissance	Renaissance	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Present	Present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Facts	Facts	k1gInSc1
On	on	k3xPp3gInSc1
File	File	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
162	#num#	k4
s.	s.	k?
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8160	#num#	k4
<g/>
-	-	kIx~
<g/>
4454	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Praotec	praotec	k1gMnSc1
Sámo	Sámo	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Slované	Slovan	k1gMnPc1
a	a	k8xC
Avaři	Avar	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kočovníci	kočovník	k1gMnPc1
stepi	step	k1gFnSc2
V	V	kA
<g/>
:	:	kIx,
Regnal	Regnal	k1gMnSc1
Chronologies	Chronologies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Obsidian	Obsidian	k1gMnSc1
Globe	globus	k1gInSc5
Forum	forum	k1gNnSc1
Index	index	k1gInSc1
(	(	kIx(
<g/>
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannic	k1gInSc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hunové	Hun	k1gMnPc1
</s>
<s>
Protobulhaři	Protobulhař	k1gMnPc1
</s>
<s>
Turkické	turkický	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Insignie	insignie	k1gFnPc1
prezidenta	prezident	k1gMnSc2
Turecka	Turecko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
jsou	být	k5eAaImIp3nP
Avaři	Avar	k1gMnPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Avaři	Avar	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Avarové	Avarové	k?
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Avarové	Avarové	k?
ve	v	k7c6
Vlastenském	vlastenský	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
historickém	historický	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Turkické	turkický	k2eAgInPc1d1
národy	národ	k1gInPc1
Kurzíva	kurzíva	k1gFnSc1
s	s	k7c7
†	†	k?
ukazují	ukazovat	k5eAaImIp3nP
vymřelé	vymřelý	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
–	–	k?
znak	znak	k1gInSc1
†	†	k?
ukazuje	ukazovat	k5eAaImIp3nS
vymírající	vymírající	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
Historické	historický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
kmenů	kmen	k1gInPc2
a	a	k8xC
klanů	klan	k1gInPc2
</s>
<s>
Turkuti	Turkut	k1gMnPc1
†	†	k?
•	•	k?
Jenisejští	jenisejský	k2eAgMnPc1d1
Kyrgyzové	Kyrgyz	k1gMnPc1
†	†	k?
•	•	k?
Kanglyové	Kanglyová	k1gFnSc2
†	†	k?
•	•	k?
Karlukové	Karlukový	k2eAgFnSc2d1
•	•	k?
Kimakové	Kimakový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Kumáni	Kumán	k1gMnPc1
(	(	kIx(
<g/>
Polovci	Polovec	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Kypčaci	Kypčace	k1gFnSc6
•	•	k?
Kı	Kı	k1gFnSc2
•	•	k?
Ogurští	Ogurský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
–	–	k?
Avaři	Avar	k1gMnPc1
†	†	k?
•	•	k?
Hunové	Hun	k1gMnPc1
†	†	k?
•	•	k?
Chazaři	Chazar	k1gMnPc1
†	†	k?
•	•	k?
Prabulhaři	Prabulhař	k1gMnSc3
†	†	k?
•	•	k?
Oguzští	Oguzský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Türgešové	Türgešové	k2eAgMnSc4d1
†	†	k?
•	•	k?
Siungnuové	Siungnuová	k1gFnSc2
†	†	k?
•	•	k?
Šatoové	Šatoová	k1gFnSc2
†	†	k?
•	•	k?
Tinglingové	Tinglingový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Wusunové	Wusunový	k2eAgFnSc2d1
†	†	k?
Současné	současný	k2eAgInPc4d1
národy	národ	k1gInPc4
a	a	k8xC
národnosti	národnost	k1gFnPc4
</s>
<s>
Altajové	Altajový	k2eAgFnPc4d1
•	•	k?
Azerové	Azerová	k1gFnPc4
•	•	k?
Balkaři	Balkař	k1gMnSc3
•	•	k?
Baškirové	Baškir	k1gMnPc1
•	•	k?
Chakasové	Chakasové	k2eAgInPc2d1
<g/>
•	•	k?
Chaladžové	Chaladžový	k2eAgFnSc2d1
•	•	k?
Chorásánští	chorásánský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Čulymci	Čulymec	k1gMnSc3
•	•	k?
Čuvaši	Čuvaš	k1gMnSc3
•	•	k?
Dolgani	Dolgaň	k1gFnSc6
†	†	k?
•	•	k?
Gagauzové	Gagauzová	k1gFnSc2
•	•	k?
Iráčtí	irácký	k2eAgMnPc1d1
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Jakuti	Jakut	k1gMnPc1
•	•	k?
Karačajové	Karačajový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Karaité	Karaitý	k2eAgNnSc1d1
†	†	k?
•	•	k?
Karakalpaci	Karakalpace	k1gFnSc4
•	•	k?
Karamanliové	Karamanliový	k2eAgFnSc2d1
•	•	k?
Karapapachové	Karapapachová	k1gFnSc2
†	†	k?
•	•	k?
Kaškajové	Kaškajový	k2eAgFnSc2d1
•	•	k?
Kazaši	Kazach	k1gMnPc1
•	•	k?
Krymčáci	Krymčák	k1gMnPc1
†	†	k?
•	•	k?
Krymští	krymský	k2eAgMnPc1d1
Tataři	Tatar	k1gMnPc1
•	•	k?
Kumandynci	Kumandynec	k1gMnSc3
†	†	k?
•	•	k?
Kumykové	Kumyková	k1gFnSc2
•	•	k?
Kyrgyzové	Kyrgyzová	k1gFnSc2
•	•	k?
Meschetští	Meschetský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Najmani	Najman	k1gMnPc1
•	•	k?
Nogajové	Nogajový	k2eAgFnSc2d1
•	•	k?
Salarové	Salarová	k1gFnSc2
•	•	k?
Syrští	syrský	k2eAgMnPc1d1
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Šorci	šorc	k1gInSc6
•	•	k?
Tataři	Tatar	k1gMnPc1
•	•	k?
Telengiti	Telengit	k2eAgMnPc1d1
•	•	k?
Teleuti	Teleuť	k1gFnPc4
•	•	k?
Tofalaři	Tofalař	k1gMnSc3
†	†	k?
•	•	k?
Turci	Turek	k1gMnPc1
•	•	k?
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Tuvinci	Tuvinec	k1gInSc3
•	•	k?
Ujgurové	Ujgurový	k2eAgFnSc2d1
•	•	k?
Urumové	Urumová	k1gFnSc2
•	•	k?
Uzbeci	Uzbek	k1gMnPc1
•	•	k?
Jugurové	Jugurový	k2eAgNnSc1d1
†	†	k?
Západoevropští	západoevropský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Turci	Turek	k1gMnPc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
Balkánští	balkánský	k2eAgMnPc1d1
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Turci	Turek	k1gMnPc1
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Egyptu	Egypt	k1gInSc3
•	•	k?
Turečtí	turecký	k2eAgMnPc1d1
Kyprioti	Kypriot	k1gMnPc1
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c4
Kosovu	Kosův	k2eAgFnSc4d1
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Thrákii	Thrákie	k1gFnSc6
•	•	k?
Muslimové	muslim	k1gMnPc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
117370	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4004021-5	4004021-5	k4
</s>
