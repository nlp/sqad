<p>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Oberstlandrichter	Oberstlandrichter	k1gInSc1	Oberstlandrichter
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
iudex	iudex	k1gInSc1	iudex
terrae	terra	k1gFnSc2	terra
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
supremus	supremus	k1gInSc1	supremus
iudex	iudex	k1gInSc1	iudex
terrae	terrae	k1gInSc1	terrae
nebo	nebo	k8xC	nebo
iudex	iudex	k1gInSc1	iudex
Bohemiae	Bohemia	k1gInSc2	Bohemia
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dvanáctičlenném	dvanáctičlenný	k2eAgNnSc6d1	dvanáctičlenné
kolegiu	kolegium	k1gNnSc6	kolegium
pátým	pátý	k4xOgNnSc7	pátý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zemským	zemský	k2eAgMnSc7d1	zemský
stavovským	stavovský	k2eAgMnSc7d1	stavovský
úředníkem	úředník	k1gMnSc7	úředník
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
zemských	zemský	k2eAgMnPc2d1	zemský
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Připravoval	připravovat	k5eAaImAgMnS	připravovat
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
jednání	jednání	k1gNnSc4	jednání
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Böhmisches	Böhmisches	k1gMnSc1	Böhmisches
/	/	kIx~	/
Mährisches	Mährisches	k1gMnSc1	Mährisches
Landrecht	Landrecht	k1gMnSc1	Landrecht
<g/>
,	,	kIx,	,
iudicium	iudicium	k1gNnSc1	iudicium
terrae	terrae	k1gFnPc2	terrae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnPc2	jehož
kompetencí	kompetence	k1gFnPc2	kompetence
spadaly	spadat	k5eAaPmAgFnP	spadat
majetkové	majetkový	k2eAgFnPc1d1	majetková
a	a	k8xC	a
trestní	trestní	k2eAgFnPc1d1	trestní
záležitosti	záležitost	k1gFnPc1	záležitost
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
dvorský	dvorský	k2eAgMnSc1d1	dvorský
sudí	sudí	k1gMnSc1	sudí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
úředníka	úředník	k1gMnSc4	úředník
čistě	čistě	k6eAd1	čistě
stavovského	stavovský	k2eAgMnSc4d1	stavovský
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
(	(	kIx(	(
<g/>
iudex	iudex	k1gInSc1	iudex
terrae	terra	k1gInSc2	terra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
(	(	kIx(	(
<g/>
králem	král	k1gMnSc7	král
1230	[number]	k4	1230
<g/>
–	–	k?	–
<g/>
1253	[number]	k4	1253
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
kompetence	kompetence	k1gFnSc1	kompetence
dvorského	dvorský	k2eAgMnSc2d1	dvorský
sudího	sudí	k1gMnSc2	sudí
zmenšovaly	zmenšovat	k5eAaImAgInP	zmenšovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
agenda	agenda	k1gFnSc1	agenda
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
zemského	zemský	k2eAgMnSc2d1	zemský
sudího	sudí	k1gMnSc2	sudí
narůstala	narůstat	k5eAaImAgFnS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
soudcem	soudce	k1gMnSc7	soudce
nad	nad	k7c7	nad
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
předsedal	předsedat	k5eAaImAgMnS	předsedat
zemskému	zemský	k2eAgInSc3d1	zemský
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
předsedal	předsedat	k5eAaImAgMnS	předsedat
soudu	soud	k1gInSc2	soud
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
komorník	komorník	k1gMnSc1	komorník
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
skutečně	skutečně	k6eAd1	skutečně
vedl	vést	k5eAaImAgInS	vést
jednání	jednání	k1gNnSc4	jednání
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
(	(	kIx(	(
<g/>
tak	tak	k9	tak
v	v	k7c6	v
Ordo	orda	k1gFnSc5	orda
iudicii	iudicium	k1gNnPc7	iudicium
terrae	terrae	k1gNnSc1	terrae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
zemských	zemský	k2eAgInPc2d1	zemský
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
beneficiarii	beneficiarie	k1gFnSc4	beneficiarie
supremi	supre	k1gFnPc7	supre
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obsazován	obsazovat	k5eAaImNgMnS	obsazovat
pány	pan	k1gMnPc7	pan
(	(	kIx(	(
<g/>
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1437	[number]	k4	1437
a	a	k8xC	a
1497	[number]	k4	1497
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
Obnovené	obnovený	k2eAgNnSc1d1	obnovené
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgNnSc1d1	zemské
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předbělohorském	předbělohorský	k2eAgNnSc6d1	předbělohorské
období	období	k1gNnSc6	období
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Bílou	bílý	k2eAgFnSc7d1	bílá
horou	hora	k1gFnSc7	hora
přísahal	přísahat	k5eAaImAgMnS	přísahat
králi	král	k1gMnSc3	král
a	a	k8xC	a
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
pouze	pouze	k6eAd1	pouze
králi	král	k1gMnSc3	král
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
dědicům	dědic	k1gMnPc3	dědic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tereziánských	tereziánský	k2eAgFnPc2d1	Tereziánská
reforem	reforma	k1gFnPc2	reforma
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
místodržitelské	místodržitelský	k2eAgNnSc1d1	místodržitelský
kolegium	kolegium	k1gNnSc1	kolegium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
řádným	řádný	k2eAgInSc7d1	řádný
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
omezeny	omezen	k2eAgFnPc1d1	omezena
pravomoci	pravomoc	k1gFnPc1	pravomoc
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
zbyrokratizován	zbyrokratizován	k2eAgMnSc1d1	zbyrokratizován
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
však	však	k9	však
název	název	k1gInSc1	název
úřadu	úřad	k1gInSc2	úřad
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
hodnost	hodnost	k1gFnSc4	hodnost
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
zemského	zemský	k2eAgMnSc2d1	zemský
sudího	sudí	k1gMnSc2	sudí
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
zemských	zemský	k2eAgFnPc2d1	zemská
sudí	sudí	k1gFnPc4	sudí
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Královský	královský	k2eAgInSc1d1	královský
úřad	úřad	k1gInSc1	úřad
====	====	k?	====
</s>
</p>
<p>
<s>
1170	[number]	k4	1170
Zvěst	zvěst	k1gFnSc1	zvěst
</s>
</p>
<p>
<s>
1175	[number]	k4	1175
<g/>
–	–	k?	–
<g/>
1178	[number]	k4	1178
Čéč	Čéč	k1gFnSc1	Čéč
</s>
</p>
<p>
<s>
1183	[number]	k4	1183
<g/>
–	–	k?	–
<g/>
1187	[number]	k4	1187
Jan	Jan	k1gMnSc1	Jan
</s>
</p>
<p>
<s>
1189	[number]	k4	1189
Předota	Předota	k1gFnSc1	Předota
</s>
</p>
<p>
<s>
1192	[number]	k4	1192
<g/>
–	–	k?	–
<g/>
1199	[number]	k4	1199
Ratibor	Ratibora	k1gFnPc2	Ratibora
</s>
</p>
<p>
<s>
1205	[number]	k4	1205
<g/>
–	–	k?	–
<g/>
1212	[number]	k4	1212
Budivoj	Budivoj	k1gInSc1	Budivoj
</s>
</p>
<p>
<s>
1212	[number]	k4	1212
Ctibor	Ctibor	k1gMnSc1	Ctibor
</s>
</p>
<p>
<s>
1216	[number]	k4	1216
<g/>
–	–	k?	–
<g/>
1222	[number]	k4	1222
Dalibor	Dalibor	k1gMnSc1	Dalibor
</s>
</p>
<p>
<s>
1222	[number]	k4	1222
<g/>
–	–	k?	–
<g/>
1229	[number]	k4	1229
Záviš	Záviš	k1gMnSc1	Záviš
</s>
</p>
<p>
<s>
1234	[number]	k4	1234
<g/>
–	–	k?	–
<g/>
1239	[number]	k4	1239
Albert	Alberta	k1gFnPc2	Alberta
ze	z	k7c2	z
Slivna	Slivno	k1gNnSc2	Slivno
</s>
</p>
<p>
<s>
1234	[number]	k4	1234
Aldik	Aldik	k1gInSc1	Aldik
</s>
</p>
<p>
<s>
1238	[number]	k4	1238
Albert	Albert	k1gMnSc1	Albert
</s>
</p>
<p>
<s>
1240	[number]	k4	1240
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
</s>
</p>
<p>
<s>
1240	[number]	k4	1240
<g/>
–	–	k?	–
<g/>
1241	[number]	k4	1241
Zbislav	Zbislava	k1gFnPc2	Zbislava
</s>
</p>
<p>
<s>
1241	[number]	k4	1241
Sulislav	Sulislav	k1gMnSc1	Sulislav
</s>
</p>
<p>
<s>
1242	[number]	k4	1242
Čéč	Čéč	k1gFnSc1	Čéč
</s>
</p>
<p>
<s>
====	====	k?	====
Postupná	postupný	k2eAgFnSc1d1	postupná
proměna	proměna	k1gFnSc1	proměna
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
úřad	úřad	k1gInSc4	úřad
====	====	k?	====
</s>
</p>
<p>
<s>
1244	[number]	k4	1244
Ctibor	Ctibor	k1gMnSc1	Ctibor
</s>
</p>
<p>
<s>
1249	[number]	k4	1249
<g/>
–	–	k?	–
<g/>
1255	[number]	k4	1255
Pomněn	pomněn	k2eAgInSc4d1	pomněn
</s>
</p>
<p>
<s>
1256	[number]	k4	1256
<g/>
–	–	k?	–
<g/>
1264	[number]	k4	1264
Čéč	Čéč	k1gFnSc1	Čéč
</s>
</p>
<p>
<s>
1267	[number]	k4	1267
<g/>
–	–	k?	–
<g/>
1269	[number]	k4	1269
Drslav	Drslav	k1gFnSc1	Drslav
</s>
</p>
<p>
<s>
1277	[number]	k4	1277
Děpolt	Děpolt	k1gInSc1	Děpolt
z	z	k7c2	z
Rýzmberka	Rýzmberka	k1gFnSc1	Rýzmberka
</s>
</p>
<p>
<s>
1277	[number]	k4	1277
<g/>
–	–	k?	–
<g/>
1279	[number]	k4	1279
Oneš	Oneš	k1gInSc1	Oneš
</s>
</p>
<p>
<s>
1284	[number]	k4	1284
<g/>
–	–	k?	–
<g/>
1287	[number]	k4	1287
Boleslav	Boleslav	k1gMnSc1	Boleslav
ze	z	k7c2	z
Smečna	Smečno	k1gNnSc2	Smečno
</s>
</p>
<p>
<s>
1295	[number]	k4	1295
Sezema	Sezema	k1gNnSc1	Sezema
</s>
</p>
<p>
<s>
1309	[number]	k4	1309
<g/>
–	–	k?	–
<g/>
1324	[number]	k4	1324
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Říčan	Říčany	k1gInPc2	Říčany
</s>
</p>
<p>
<s>
1328	[number]	k4	1328
<g/>
–	–	k?	–
<g/>
1340	[number]	k4	1340
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pluh	pluh	k1gInSc1	pluh
z	z	k7c2	z
Rabštejna	Rabštejn	k1gInSc2	Rabštejn
</s>
</p>
<p>
<s>
1343	[number]	k4	1343
<g/>
–	–	k?	–
<g/>
1394	[number]	k4	1394
Ondřej	Ondřej	k1gMnSc1	Ondřej
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
</s>
</p>
<p>
<s>
1396	[number]	k4	1396
<g/>
–	–	k?	–
<g/>
1397	[number]	k4	1397
Hynek	Hynek	k1gMnSc1	Hynek
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
z	z	k7c2	z
Hohenštejna	Hohenštejno	k1gNnSc2	Hohenštejno
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1398	[number]	k4	1398
<g/>
–	–	k?	–
<g/>
1399	[number]	k4	1399
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
a	a	k8xC	a
z	z	k7c2	z
Krasíkova	Krasíkův	k2eAgInSc2d1	Krasíkův
(	(	kIx(	(
<g/>
†	†	k?	†
1425	[number]	k4	1425
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1402	[number]	k4	1402
<g/>
–	–	k?	–
<g/>
1417	[number]	k4	1417
Hynek	Hynek	k1gMnSc1	Hynek
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
z	z	k7c2	z
Hohenštejna	Hohenštejno	k1gNnSc2	Hohenštejno
(	(	kIx(	(
<g/>
podruhé	podruhé	k6eAd1	podruhé
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1437	[number]	k4	1437
<g/>
–	–	k?	–
<g/>
1457	[number]	k4	1457
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Zajíc	Zajíc	k1gMnSc1	Zajíc
z	z	k7c2	z
Hazmbruka	Hazmbruk	k1gMnSc2	Hazmbruk
na	na	k7c6	na
Kosti	kost	k1gFnSc6	kost
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1463	[number]	k4	1463
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1464	[number]	k4	1464
Lev	Lev	k1gMnSc1	Lev
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
na	na	k7c4	na
Blatné	blatný	k2eAgNnSc4d1	Blatné
</s>
</p>
<p>
<s>
1467	[number]	k4	1467
Dětřich	Dětřich	k1gMnSc1	Dětřich
z	z	k7c2	z
Janovic	Janovice	k1gFnPc2	Janovice
na	na	k7c6	na
Chlumci	Chlumec	k1gInSc6	Chlumec
</s>
</p>
<p>
<s>
1472	[number]	k4	1472
<g/>
–	–	k?	–
<g/>
1474	[number]	k4	1474
Jiří	Jiří	k1gMnSc1	Jiří
ze	z	k7c2	z
Stráže	stráž	k1gFnSc2	stráž
</s>
</p>
<p>
<s>
1475	[number]	k4	1475
<g/>
–	–	k?	–
<g/>
1479	[number]	k4	1479
Jan	Jan	k1gMnSc1	Jan
Tovačovský	Tovačovský	k1gMnSc1	Tovačovský
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
</s>
</p>
<p>
<s>
1479	[number]	k4	1479
<g/>
–	–	k?	–
<g/>
1504	[number]	k4	1504
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Půta	Půta	k1gMnSc1	Půta
Švihovský	Švihovský	k2eAgMnSc1d1	Švihovský
z	z	k7c2	z
Rýzmberka	Rýzmberka	k1gFnSc1	Rýzmberka
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1504	[number]	k4	1504
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1504	[number]	k4	1504
<g/>
–	–	k?	–
<g/>
1507	[number]	k4	1507
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Lev	Lev	k1gMnSc1	Lev
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
(	(	kIx(	(
<g/>
před	před	k7c7	před
1470	[number]	k4	1470
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1535	[number]	k4	1535
Blatná	blatný	k2eAgFnSc1d1	Blatná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1508	[number]	k4	1508
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1510	[number]	k4	1510
Petr	Petr	k1gMnSc1	Petr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Holický	holický	k2eAgInSc1d1	holický
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
na	na	k7c6	na
Leštně	Leštně	k1gFnSc6	Leštně
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1514	[number]	k4	1514
Líšno	Líšno	k1gNnSc1	Líšno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1512	[number]	k4	1512
<g/>
–	–	k?	–
<g/>
1516	[number]	k4	1516
Jiří	Jiří	k1gMnSc1	Jiří
Bezdružický	Bezdružický	k2eAgMnSc1d1	Bezdružický
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
na	na	k7c6	na
Buštěhradě	Buštěhrad	k1gInSc6	Buštěhrad
</s>
</p>
<p>
<s>
1522	[number]	k4	1522
<g/>
–	–	k?	–
<g/>
1523	[number]	k4	1523
Hynek	Hynek	k1gMnSc1	Hynek
Bořita	Bořita	k1gMnSc1	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
na	na	k7c6	na
Smečně	Smečně	k1gFnSc6	Smečně
</s>
</p>
<p>
<s>
1523	[number]	k4	1523
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1534	[number]	k4	1534
Zdislav	Zdislava	k1gFnPc2	Zdislava
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
Lipé	Lipá	k1gFnSc2	Lipá
na	na	k7c6	na
Zákupech	zákup	k1gInPc6	zákup
(	(	kIx(	(
<g/>
1467	[number]	k4	1467
<g/>
/	/	kIx~	/
<g/>
1470	[number]	k4	1470
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1553	[number]	k4	1553
Zákupy	zákup	k1gInPc4	zákup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1534	[number]	k4	1534
<g/>
–	–	k?	–
<g/>
1541	[number]	k4	1541
Jindřich	Jindřich	k1gMnSc1	Jindřich
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
na	na	k7c6	na
Dřevenicích	Dřevenice	k1gFnPc6	Dřevenice
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1541	[number]	k4	1541
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1541	[number]	k4	1541
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1554	[number]	k4	1554
Jan	Jana	k1gFnPc2	Jana
III	III	kA	III
<g/>
.	.	kIx.	.
starší	starý	k2eAgInSc1d2	starší
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
na	na	k7c6	na
Zbiroze	Zbiroh	k1gInSc6wO	Zbiroh
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1569	[number]	k4	1569
Libochovice	Libochovice	k1gFnPc1	Libochovice
<g/>
;	;	kIx,	;
chlumecká	chlumecký	k2eAgFnSc1d1	Chlumecká
linie	linie	k1gFnPc1	linie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1554	[number]	k4	1554
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
</s>
</p>
<p>
<s>
1570	[number]	k4	1570
<g/>
–	–	k?	–
<g/>
1576	[number]	k4	1576
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Felix	Felix	k1gMnSc1	Felix
Hasištejnský	Hasištejnský	k1gMnSc1	Hasištejnský
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1517	[number]	k4	1517
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1583	[number]	k4	1583
Líčkov	Líčkov	k1gInSc1	Líčkov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1577	[number]	k4	1577
<g/>
–	–	k?	–
<g/>
1581	[number]	k4	1581
Adam	Adam	k1gMnSc1	Adam
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
na	na	k7c6	na
Přimdě	Přimda	k1gFnSc6	Přimda
</s>
</p>
<p>
<s>
1582	[number]	k4	1582
<g/>
–	–	k?	–
<g/>
1584	[number]	k4	1584
Jiří	Jiří	k1gMnSc1	Jiří
(	(	kIx(	(
<g/>
starší	starší	k1gMnSc1	starší
<g/>
)	)	kIx)	)
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
na	na	k7c6	na
Libochovicích	Libochovice	k1gFnPc6	Libochovice
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1607	[number]	k4	1607
Loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1585	[number]	k4	1585
<g/>
–	–	k?	–
<g/>
1597	[number]	k4	1597
Jiří	Jiří	k1gMnSc2	Jiří
Bořita	Bořit	k1gMnSc2	Bořit
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
na	na	k7c6	na
Smečně	Smečně	k1gFnSc6	Smečně
(	(	kIx(	(
<g/>
1532	[number]	k4	1532
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1598	[number]	k4	1598
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1597	[number]	k4	1597
<g/>
–	–	k?	–
<g/>
1599	[number]	k4	1599
Václav	Václav	k1gMnSc1	Václav
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
na	na	k7c6	na
Rychmburce	Rychmburka	k1gFnSc6	Rychmburka
</s>
</p>
<p>
<s>
1599	[number]	k4	1599
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1603	[number]	k4	1603
Adam	Adam	k1gMnSc1	Adam
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1623	[number]	k4	1623
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1603	[number]	k4	1603
<g/>
–	–	k?	–
<g/>
1608	[number]	k4	1608
Volf	Volf	k1gMnSc1	Volf
Novohradský	novohradský	k2eAgMnSc1d1	novohradský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1609	[number]	k4	1609
Lnáře	lnář	k1gMnPc4	lnář
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1611	[number]	k4	1611
Adam	Adam	k1gMnSc1	Adam
ml.	ml.	kA	ml.
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
1569	[number]	k4	1569
<g/>
/	/	kIx~	/
<g/>
1570	[number]	k4	1570
<g/>
–	–	k?	–
<g/>
1638	[number]	k4	1638
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1613	[number]	k4	1613
<g/>
–	–	k?	–
<g/>
1617	[number]	k4	1617
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Talmberka	Talmberka	k1gFnSc1	Talmberka
na	na	k7c6	na
Jankově	Jankův	k2eAgFnSc6d1	Jankova
</s>
</p>
<p>
<s>
1617	[number]	k4	1617
<g/>
–	–	k?	–
<g/>
1623	[number]	k4	1623
Vilém	Viléma	k1gFnPc2	Viléma
Slavata	Slavat	k1gMnSc2	Slavat
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
a	a	k8xC	a
Košumberka	Košumberka	k1gFnSc1	Košumberka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1572	[number]	k4	1572
Čestín	Čestín	k1gInSc1	Čestín
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1652	[number]	k4	1652
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1619	[number]	k4	1619
<g/>
–	–	k?	–
<g/>
1620	[number]	k4	1620
Jáchym	Jáchym	k1gMnSc1	Jáchym
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šlik	šlika	k1gFnPc2	šlika
na	na	k7c6	na
Svijanech	Svijan	k1gInPc6	Svijan
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1569	[number]	k4	1569
Ostrov	ostrov	k1gInSc1	ostrov
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1621	[number]	k4	1621
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1624	[number]	k4	1624
<g/>
–	–	k?	–
<g/>
1625	[number]	k4	1625
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bořita	Bořita	k1gMnSc1	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1582	[number]	k4	1582
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1649	[number]	k4	1649
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1625	[number]	k4	1625
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Talmberka	Talmberka	k1gFnSc1	Talmberka
na	na	k7c6	na
Vlašimi	Vlašim	k1gFnSc6	Vlašim
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1627	[number]	k4	1627
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
–	–	k?	–
1638	[number]	k4	1638
Jindřich	Jindřich	k1gMnSc1	Jindřich
Libštejnský	Libštejnský	k1gMnSc1	Libštejnský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
1570	[number]	k4	1570
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1638	[number]	k4	1638
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1644	[number]	k4	1644
Sezima	Sezima	k1gFnSc1	Sezima
z	z	k7c2	z
Vrtby	vrtba	k1gFnSc2	vrtba
(	(	kIx(	(
<g/>
1578	[number]	k4	1578
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1644	[number]	k4	1644
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1648	[number]	k4	1648
Bernard	Bernard	k1gMnSc1	Bernard
Ignác	Ignác	k1gMnSc1	Ignác
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1614	[number]	k4	1614
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1685	[number]	k4	1685
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1648	[number]	k4	1648
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Matyáš	Matyáš	k1gMnSc1	Matyáš
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1612	[number]	k4	1612
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1648	[number]	k4	1648
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1649	[number]	k4	1649
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
Heřman	Heřman	k1gMnSc1	Heřman
Czernin	Czernin	k1gInSc1	Czernin
z	z	k7c2	z
Chudenic	Chudenice	k1gFnPc2	Chudenice
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1576	[number]	k4	1576
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1651	[number]	k4	1651
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1650	[number]	k4	1650
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1651	[number]	k4	1651
Jan	Jan	k1gMnSc1	Jan
Hartvík	Hartvík	k1gMnSc1	Hartvík
z	z	k7c2	z
Nostic	Nostice	k1gFnPc2	Nostice
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
Kunzendorf	Kunzendorf	k1gInSc1	Kunzendorf
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1683	[number]	k4	1683
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1651	[number]	k4	1651
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1652	[number]	k4	1652
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1622	[number]	k4	1622
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1656	[number]	k4	1656
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1652	[number]	k4	1652
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1656	[number]	k4	1656
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
Valentin	Valentina	k1gFnPc2	Valentina
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
(	(	kIx(	(
<g/>
1612	[number]	k4	1612
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1677	[number]	k4	1677
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1656	[number]	k4	1656
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1678	[number]	k4	1678
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vilém	Vilém	k1gMnSc1	Vilém
Albrecht	Albrecht	k1gMnSc1	Albrecht
Krakowský	Krakowský	k1gMnSc1	Krakowský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1600	[number]	k4	1600
Týnec	Týnec	k1gInSc1	Týnec
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1688	[number]	k4	1688
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1678	[number]	k4	1678
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1685	[number]	k4	1685
Adolf	Adolf	k1gMnSc1	Adolf
Vratislav	Vratislav	k1gMnSc1	Vratislav
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
Postoloprty	Postoloprta	k1gFnSc2	Postoloprta
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1703	[number]	k4	1703
Zásmuky	Zásmuky	k1gInPc4	Zásmuky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1685	[number]	k4	1685
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1688	[number]	k4	1688
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Jáchym	Jáchym	k1gMnSc1	Jáchym
Slavata	Slavata	k1gFnSc1	Slavata
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
a	a	k8xC	a
Košumberka	Košumberka	k1gFnSc1	Košumberka
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
/	/	kIx~	/
<g/>
1637	[number]	k4	1637
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1689	[number]	k4	1689
Velvary	Velvar	k1gInPc4	Velvar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1688	[number]	k4	1688
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1689	[number]	k4	1689
Jan	Jan	k1gMnSc1	Jan
František	František	k1gMnSc1	František
Bruntálský	bruntálský	k2eAgMnSc1d1	bruntálský
z	z	k7c2	z
Vrbna	Vrbno	k1gNnSc2	Vrbno
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1634	[number]	k4	1634
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1705	[number]	k4	1705
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1690	[number]	k4	1690
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1696	[number]	k4	1696
Václav	Václav	k1gMnSc1	Václav
Norbert	Norbert	k1gMnSc1	Norbert
Oktavián	Oktavián	k1gMnSc1	Oktavián
Kinský	Kinský	k1gMnSc1	Kinský
(	(	kIx(	(
<g/>
1642	[number]	k4	1642
<g/>
–	–	k?	–
<g/>
1719	[number]	k4	1719
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1696	[number]	k4	1696
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1704	[number]	k4	1704
Václav	Václav	k1gMnSc1	Václav
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1643	[number]	k4	1643
Bechyně	Bechyně	k1gFnSc2	Bechyně
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1708	[number]	k4	1708
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1704	[number]	k4	1704
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1711	[number]	k4	1711
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Vratislav	Vratislav	k1gMnSc1	Vratislav
z	z	k7c2	z
Mitrovic	Mitrovice	k1gFnPc2	Mitrovice
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1670	[number]	k4	1670
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1712	[number]	k4	1712
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
agendu	agenda	k1gFnSc4	agenda
reálně	reálně	k6eAd1	reálně
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Přehořovský	Přehořovský	k1gMnSc1	Přehořovský
z	z	k7c2	z
Kvasejovic	Kvasejovice	k1gFnPc2	Kvasejovice
</s>
</p>
<p>
<s>
1705	[number]	k4	1705
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1717	[number]	k4	1717
František	František	k1gMnSc1	František
Karel	Karel	k1gMnSc1	Karel
Přehořovský	Přehořovský	k1gMnSc1	Přehořovský
z	z	k7c2	z
Kvasejovic	Kvasejovice	k1gFnPc2	Kvasejovice
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1723	[number]	k4	1723
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1717	[number]	k4	1717
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1721	[number]	k4	1721
Jan	Jan	k1gMnSc1	Jan
Arnošt	Arnošt	k1gMnSc1	Arnošt
ze	z	k7c2	z
Schaffgotsche	Schaffgotsch	k1gFnSc2	Schaffgotsch
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1675	[number]	k4	1675
Dobromierz	Dobromierz	k1gInSc1	Dobromierz
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1747	[number]	k4	1747
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1721	[number]	k4	1721
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1747	[number]	k4	1747
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Bruntálský	bruntálský	k2eAgMnSc1d1	bruntálský
z	z	k7c2	z
Vrbna	Vrbno	k1gNnSc2	Vrbno
(	(	kIx(	(
<g/>
1675	[number]	k4	1675
<g/>
–	–	k?	–
<g/>
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1747	[number]	k4	1747
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1748	[number]	k4	1748
Filip	Filip	k1gMnSc1	Filip
Nerius	Nerius	k1gMnSc1	Nerius
Krakovský	krakovský	k2eAgMnSc1d1	krakovský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1686	[number]	k4	1686
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1773	[number]	k4	1773
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1748	[number]	k4	1748
<g/>
–	–	k?	–
<g/>
1757	[number]	k4	1757
Karel	Karel	k1gMnSc1	Karel
Gotthard	Gotthard	k1gMnSc1	Gotthard
Schaffgotsch	Schaffgotsch	k1gMnSc1	Schaffgotsch
(	(	kIx(	(
<g/>
1706	[number]	k4	1706
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1757	[number]	k4	1757
<g/>
–	–	k?	–
<g/>
1760	[number]	k4	1760
Josef	Josef	k1gMnSc1	Josef
Vilém	Vilém	k1gMnSc1	Vilém
Nostic	Nostice	k1gFnPc2	Nostice
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1706	[number]	k4	1706
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1787	[number]	k4	1787
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
)	)	kIx)	)
</s>
</p>
<p>
<s>
1760	[number]	k4	1760
<g/>
–	–	k?	–
<g/>
1773	[number]	k4	1773
Prokop	Prokop	k1gMnSc1	Prokop
Jan	Jan	k1gMnSc1	Jan
Krakowský	Krakowský	k1gMnSc1	Krakowský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1718	[number]	k4	1718
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1774	[number]	k4	1774
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1789	[number]	k4	1789
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Pachta	Pachta	k1gMnSc1	Pachta
z	z	k7c2	z
Rájova	Rájův	k2eAgInSc2d1	Rájův
(	(	kIx(	(
<g/>
1710	[number]	k4	1710
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1789	[number]	k4	1789
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1792	[number]	k4	1792
Prokop	prokop	k1gInSc1	prokop
Lažanský	Lažanský	k2eAgInSc1d1	Lažanský
z	z	k7c2	z
Bukové	bukový	k2eAgFnSc2d1	Buková
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1792	[number]	k4	1792
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1802	[number]	k4	1802
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Běšin	Běšin	k1gMnSc1	Běšin
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1802	[number]	k4	1802
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Wallis	Wallis	k1gFnSc2	Wallis
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1767	[number]	k4	1767
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1818	[number]	k4	1818
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1804	[number]	k4	1804
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1808	[number]	k4	1808
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
František	františek	k1gInSc4	františek
Karg	kargo	k1gNnPc2	kargo
z	z	k7c2	z
Bebenburka	Bebenburka	k1gFnSc1	Bebenburka
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1813	[number]	k4	1813
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
Auersperg	Auersperg	k1gMnSc1	Auersperg
(	(	kIx(	(
<g/>
1767	[number]	k4	1767
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1813	[number]	k4	1813
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1830	[number]	k4	1830
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Lažanský	Lažanský	k2eAgMnSc1d1	Lažanský
z	z	k7c2	z
Bukové	bukový	k2eAgFnSc2d1	Buková
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1830	[number]	k4	1830
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
1832	[number]	k4	1832
Fridrich	Fridrich	k1gMnSc1	Fridrich
Mořic	Mořic	k1gMnSc1	Mořic
Wagemann	Wagemann	k1gMnSc1	Wagemann
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Moravské	moravský	k2eAgNnSc1d1	Moravské
markrabství	markrabství	k1gNnSc1	markrabství
==	==	k?	==
</s>
</p>
<p>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
markrabětem	markrabě	k1gMnSc7	markrabě
1333	[number]	k4	1333
<g/>
–	–	k?	–
<g/>
1349	[number]	k4	1349
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
soudy	soud	k1gInPc4	soud
(	(	kIx(	(
<g/>
cúdy	cúda	k1gFnPc4	cúda
<g/>
)	)	kIx)	)
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
zasedal	zasedat	k5eAaImAgInS	zasedat
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
sudí	sudí	k1gMnPc1	sudí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sněmovního	sněmovní	k2eAgNnSc2d1	sněmovní
usnesení	usnesení	k1gNnSc2	usnesení
potvrzeného	potvrzený	k2eAgNnSc2d1	potvrzené
listinou	listina	k1gFnSc7	listina
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
(	(	kIx(	(
<g/>
markrabětem	markrabě	k1gMnSc7	markrabě
1490	[number]	k4	1490
<g/>
–	–	k?	–
<g/>
1516	[number]	k4	1516
<g/>
)	)	kIx)	)
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1493	[number]	k4	1493
byl	být	k5eAaImAgInS	být
úřad	úřad	k1gInSc1	úřad
sudího	sudí	k1gMnSc2	sudí
sloučen	sloučen	k2eAgMnSc1d1	sloučen
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k9	tak
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Obdobně	obdobně	k6eAd1	obdobně
byl	být	k5eAaImAgInS	být
sloučen	sloučit	k5eAaPmNgInS	sloučit
i	i	k9	i
úřad	úřad	k1gInSc1	úřad
komorníka	komorník	k1gMnSc2	komorník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1523	[number]	k4	1523
za	za	k7c2	za
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
(	(	kIx(	(
<g/>
markrabětem	markrabě	k1gMnSc7	markrabě
1516	[number]	k4	1516
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
úřad	úřad	k1gInSc1	úřad
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
pánů	pan	k1gMnPc2	pan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
zemských	zemský	k2eAgFnPc2d1	zemská
sudí	sudí	k1gFnPc4	sudí
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
sudí	sudí	k1gMnSc1	sudí
====	====	k?	====
</s>
</p>
<p>
<s>
1203	[number]	k4	1203
Veliš	Veliš	k1gInSc1	Veliš
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1207	[number]	k4	1207
Jindřich	Jindřich	k1gMnSc1	Jindřich
</s>
</p>
<p>
<s>
1208	[number]	k4	1208
<g/>
–	–	k?	–
<g/>
1214	[number]	k4	1214
Doben	Dobna	k1gFnPc2	Dobna
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1215	[number]	k4	1215
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
1222	[number]	k4	1222
<g/>
–	–	k?	–
<g/>
1225	[number]	k4	1225
Veliš	Veliš	k1gInSc1	Veliš
</s>
</p>
<p>
<s>
1230	[number]	k4	1230
Jan	Jan	k1gMnSc1	Jan
</s>
</p>
<p>
<s>
1232	[number]	k4	1232
<g/>
–	–	k?	–
<g/>
1235	[number]	k4	1235
Přebor	přebor	k1gInSc1	přebor
</s>
</p>
<p>
<s>
1234	[number]	k4	1234
<g/>
–	–	k?	–
<g/>
1236	[number]	k4	1236
Vecen	Vecna	k1gFnPc2	Vecna
</s>
</p>
<p>
<s>
1240	[number]	k4	1240
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
</s>
</p>
<p>
<s>
1250	[number]	k4	1250
<g/>
–	–	k?	–
<g/>
1251	[number]	k4	1251
Sláva	Sláva	k1gFnSc1	Sláva
</s>
</p>
<p>
<s>
1253	[number]	k4	1253
<g/>
–	–	k?	–
<g/>
1269	[number]	k4	1269
Jan	Jan	k1gMnSc1	Jan
</s>
</p>
<p>
<s>
1270	[number]	k4	1270
Lideř	Lideř	k1gFnSc1	Lideř
z	z	k7c2	z
Horky	horka	k1gFnSc2	horka
</s>
</p>
<p>
<s>
1275	[number]	k4	1275
<g/>
–	–	k?	–
<g/>
1278	[number]	k4	1278
Vojslav	Vojslava	k1gFnPc2	Vojslava
z	z	k7c2	z
Ludslavic	Ludslavice	k1gFnPc2	Ludslavice
</s>
</p>
<p>
<s>
1286	[number]	k4	1286
Předbor	Předbor	k1gInSc1	Předbor
(	(	kIx(	(
<g/>
Přešek	Přešek	k1gInSc1	Přešek
<g/>
)	)	kIx)	)
z	z	k7c2	z
Bolelouče	Bolelouč	k1gFnSc2	Bolelouč
</s>
</p>
<p>
<s>
1287	[number]	k4	1287
Kadold	Kadold	k1gInSc1	Kadold
z	z	k7c2	z
Polomi	Polo	k1gFnPc7	Polo
</s>
</p>
<p>
<s>
1297	[number]	k4	1297
Příbek	Příbek	k1gInSc1	Příbek
</s>
</p>
<p>
<s>
1303	[number]	k4	1303
Vítek	Vítek	k1gMnSc1	Vítek
ze	z	k7c2	z
Švabenic	Švabenice	k1gFnPc2	Švabenice
</s>
</p>
<p>
<s>
1303	[number]	k4	1303
Jan	Jan	k1gMnSc1	Jan
</s>
</p>
<p>
<s>
1314	[number]	k4	1314
Heřman	Heřman	k1gMnSc1	Heřman
</s>
</p>
<p>
<s>
1318	[number]	k4	1318
Štěpán	Štěpán	k1gMnSc1	Štěpán
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
</s>
</p>
<p>
<s>
1323	[number]	k4	1323
Diviš	Diviš	k1gMnSc1	Diviš
ze	z	k7c2	z
Štenberka	Štenberka	k1gFnSc1	Štenberka
</s>
</p>
<p>
<s>
1330	[number]	k4	1330
Lucek	Lucka	k1gFnPc2	Lucka
</s>
</p>
<p>
<s>
1348	[number]	k4	1348
<g/>
–	–	k?	–
<g/>
1358	[number]	k4	1358
Hereš	Hereš	k1gFnSc1	Hereš
z	z	k7c2	z
Lelekovic	Lelekovice	k1gFnPc2	Lelekovice
</s>
</p>
<p>
<s>
1358	[number]	k4	1358
Štěpán	Štěpán	k1gMnSc1	Štěpán
</s>
</p>
<p>
<s>
1359	[number]	k4	1359
<g/>
–	–	k?	–
<g/>
1365	[number]	k4	1365
Buzez	Buzez	k1gInSc1	Buzez
z	z	k7c2	z
Lelekovic	Lelekovice	k1gFnPc2	Lelekovice
</s>
</p>
<p>
<s>
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1377	[number]	k4	1377
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
z	z	k7c2	z
Knenic	Knenice	k1gFnPc2	Knenice
jinak	jinak	k6eAd1	jinak
z	z	k7c2	z
Longberka	Longberka	k1gFnSc1	Longberka
(	(	kIx(	(
<g/>
Lamberk	Lamberk	k1gInSc1	Lamberk
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1382	[number]	k4	1382
Hereš	Hereš	k1gFnSc1	Hereš
z	z	k7c2	z
Roketnice	Roketnice	k1gFnSc2	Roketnice
</s>
</p>
<p>
<s>
1383	[number]	k4	1383
<g/>
–	–	k?	–
<g/>
1387	[number]	k4	1387
Unka	Unka	k1gFnSc1	Unka
z	z	k7c2	z
Majetína	Majetín	k1gInSc2	Majetín
</s>
</p>
<p>
<s>
1389	[number]	k4	1389
Sulík	Sulík	k1gInSc1	Sulík
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
</s>
</p>
<p>
<s>
1406	[number]	k4	1406
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
</s>
</p>
<p>
<s>
1407	[number]	k4	1407
<g/>
–	–	k?	–
<g/>
1408	[number]	k4	1408
Sulík	Sulík	k1gInSc1	Sulík
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
</s>
</p>
<p>
<s>
1409	[number]	k4	1409
Erhard	Erhard	k1gMnSc1	Erhard
Puška	puška	k1gFnSc1	puška
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
</s>
</p>
<p>
<s>
1412	[number]	k4	1412
Sulík	Sulík	k1gInSc1	Sulík
z	z	k7c2	z
Konice	Konice	k1gFnSc2	Konice
</s>
</p>
<p>
<s>
1414	[number]	k4	1414
<g/>
–	–	k?	–
<g/>
1417	[number]	k4	1417
Milota	milota	k1gFnSc1	milota
z	z	k7c2	z
Tvorkova	tvorkův	k2eAgInSc2d1	tvorkův
</s>
</p>
<p>
<s>
1418	[number]	k4	1418
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
z	z	k7c2	z
Krumsina	Krumsin	k2eAgInSc2d1	Krumsin
</s>
</p>
<p>
<s>
1420	[number]	k4	1420
Jan	Jan	k1gMnSc1	Jan
ze	z	k7c2	z
Sovince	Sovinec	k1gInSc2	Sovinec
na	na	k7c6	na
Pňovicích	Pňovice	k1gFnPc6	Pňovice
</s>
</p>
<p>
<s>
1426	[number]	k4	1426
Vaněk	Vaněk	k1gMnSc1	Vaněk
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
</s>
</p>
<p>
<s>
1437	[number]	k4	1437
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Dúbravka	Dúbravka	k1gFnSc1	Dúbravka
z	z	k7c2	z
Dúbravic	Dúbravice	k1gFnPc2	Dúbravice
</s>
</p>
<p>
<s>
1446	[number]	k4	1446
<g/>
–	–	k?	–
<g/>
1466	[number]	k4	1466
Mikuláš	mikuláš	k1gInSc1	mikuláš
z	z	k7c2	z
Bystřice	Bystřice	k1gFnSc2	Bystřice
z	z	k7c2	z
Ojnic	ojnice	k1gFnPc2	ojnice
a	a	k8xC	a
na	na	k7c6	na
Milonicích	Milonice	k1gFnPc6	Milonice
</s>
</p>
<p>
<s>
1480	[number]	k4	1480
<g/>
–	–	k?	–
<g/>
1482	[number]	k4	1482
Jan	Jan	k1gMnSc1	Jan
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
</s>
</p>
<p>
<s>
1486	[number]	k4	1486
<g/>
–	–	k?	–
<g/>
1496	[number]	k4	1496
Jan	Jan	k1gMnSc1	Jan
Pňovský	Pňovský	k2eAgInSc1d1	Pňovský
ze	z	k7c2	z
Sovince	Sovinec	k1gInSc2	Sovinec
</s>
</p>
<p>
<s>
====	====	k?	====
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
sudí	sudí	k1gMnSc1	sudí
====	====	k?	====
</s>
</p>
<p>
<s>
1213	[number]	k4	1213
<g/>
–	–	k?	–
<g/>
1214	[number]	k4	1214
Lev	lev	k1gInSc1	lev
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
i	i	k9	i
komorník	komorník	k1gMnSc1	komorník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1222	[number]	k4	1222
<g/>
–	–	k?	–
<g/>
1226	[number]	k4	1226
Ratiboř	Ratiboř	k1gFnSc1	Ratiboř
z	z	k7c2	z
Deblína	Deblín	k1gInSc2	Deblín
</s>
</p>
<p>
<s>
1234	[number]	k4	1234
<g/>
–	–	k?	–
<g/>
136	[number]	k4	136
Jakub	Jakub	k1gMnSc1	Jakub
</s>
</p>
<p>
<s>
1238	[number]	k4	1238
<g/>
–	–	k?	–
<g/>
1240	[number]	k4	1240
Vecen	Vecna	k1gFnPc2	Vecna
</s>
</p>
<p>
<s>
1240	[number]	k4	1240
<g/>
–	–	k?	–
<g/>
1245	[number]	k4	1245
Rubín	rubín	k1gInSc1	rubín
</s>
</p>
<p>
<s>
1255	[number]	k4	1255
Lambert	Lambert	k1gInSc1	Lambert
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
</s>
</p>
<p>
<s>
1262	[number]	k4	1262
Matěj	Matěj	k1gMnSc1	Matěj
z	z	k7c2	z
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
z	z	k7c2	z
Kobylic	Kobylice	k1gFnPc2	Kobylice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1281	[number]	k4	1281
<g/>
–	–	k?	–
<g/>
1283	[number]	k4	1283
Vecemil	Vecemil	k1gFnSc1	Vecemil
</s>
</p>
<p>
<s>
1283	[number]	k4	1283
Drzek	Drzek	k1gInSc1	Drzek
</s>
</p>
<p>
<s>
1288	[number]	k4	1288
<g/>
–	–	k?	–
<g/>
1297	[number]	k4	1297
Pravík	Pravík	k1gInSc1	Pravík
</s>
</p>
<p>
<s>
1297	[number]	k4	1297
Skoch	Skoch	k1gInSc1	Skoch
z	z	k7c2	z
Hvězdlic	Hvězdlice	k1gFnPc2	Hvězdlice
</s>
</p>
<p>
<s>
1317	[number]	k4	1317
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Ronberku	Ronberk	k1gInSc2	Ronberk
</s>
</p>
<p>
<s>
1324	[number]	k4	1324
<g/>
–	–	k?	–
<g/>
1327	[number]	k4	1327
Přibyslav	Přibyslav	k1gFnSc1	Přibyslav
z	z	k7c2	z
Pěrkova	Pěrkov	k1gInSc2	Pěrkov
</s>
</p>
<p>
<s>
1329	[number]	k4	1329
<g/>
–	–	k?	–
<g/>
1355	[number]	k4	1355
Dětřich	Dětřich	k1gMnSc1	Dětřich
ze	z	k7c2	z
Spranu	Spran	k1gInSc2	Spran
</s>
</p>
<p>
<s>
1356	[number]	k4	1356
<g/>
–	–	k?	–
<g/>
1365	[number]	k4	1365
Blud	blud	k1gInSc1	blud
z	z	k7c2	z
Kralic	Kralice	k1gFnPc2	Kralice
</s>
</p>
<p>
<s>
1366	[number]	k4	1366
<g/>
–	–	k?	–
<g/>
1371	[number]	k4	1371
Frank	frank	k1gInSc1	frank
z	z	k7c2	z
Paměnic	Paměnice	k1gFnPc2	Paměnice
jinak	jinak	k6eAd1	jinak
z	z	k7c2	z
Kunovic	Kunovice	k1gFnPc2	Kunovice
</s>
</p>
<p>
<s>
1373	[number]	k4	1373
<g/>
–	–	k?	–
<g/>
1379	[number]	k4	1379
Unka	Unka	k1gFnSc1	Unka
z	z	k7c2	z
Majetína	Majetín	k1gInSc2	Majetín
</s>
</p>
<p>
<s>
1383	[number]	k4	1383
<g/>
–	–	k?	–
<g/>
1392	[number]	k4	1392
Bohunek	Bohunka	k1gFnPc2	Bohunka
z	z	k7c2	z
Třtěnic	Třtěnice	k1gFnPc2	Třtěnice
</s>
</p>
<p>
<s>
1398	[number]	k4	1398
<g/>
–	–	k?	–
<g/>
1412	[number]	k4	1412
Erhard	Erhard	k1gInSc1	Erhard
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
ze	z	k7c2	z
Skály	skála	k1gFnSc2	skála
</s>
</p>
<p>
<s>
1415	[number]	k4	1415
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
Jošt	Jošt	k1gInSc1	Jošt
Hecht	Hecht	k2eAgInSc1d1	Hecht
z	z	k7c2	z
Rosic	Rosice	k1gFnPc2	Rosice
</s>
</p>
<p>
<s>
1446	[number]	k4	1446
<g/>
–	–	k?	–
<g/>
1448	[number]	k4	1448
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
ze	z	k7c2	z
Šelnberku	Šelnberk	k1gInSc2	Šelnberk
</s>
</p>
<p>
<s>
1464	[number]	k4	1464
<g/>
–	–	k?	–
<g/>
1466	[number]	k4	1466
Hynek	Hynek	k1gMnSc1	Hynek
z	z	k7c2	z
Ludanic	Ludanice	k1gFnPc2	Ludanice
</s>
</p>
<p>
<s>
1480	[number]	k4	1480
<g/>
–	–	k?	–
<g/>
1482	[number]	k4	1482
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Ludanic	Ludanice	k1gFnPc2	Ludanice
</s>
</p>
<p>
<s>
1492	[number]	k4	1492
<g/>
–	–	k?	–
<g/>
1498	[number]	k4	1498
Lipolt	Lipolt	k1gInSc1	Lipolt
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
na	na	k7c4	na
Cornštejně	Cornštejně	k1gFnSc4	Cornštejně
</s>
</p>
<p>
<s>
====	====	k?	====
Znojemský	znojemský	k2eAgMnSc1d1	znojemský
sudí	sudí	k1gMnSc1	sudí
====	====	k?	====
</s>
</p>
<p>
<s>
před	před	k7c7	před
1213	[number]	k4	1213
Jindřich	Jindřich	k1gMnSc1	Jindřich
</s>
</p>
<p>
<s>
1222	[number]	k4	1222
Dětřich	Dětřich	k1gMnSc1	Dětřich
</s>
</p>
<p>
<s>
1226	[number]	k4	1226
Pavel	Pavel	k1gMnSc1	Pavel
</s>
</p>
<p>
<s>
1227	[number]	k4	1227
Bartoš	Bartoš	k1gMnSc1	Bartoš
(	(	kIx(	(
<g/>
Bítov	Bítov	k1gInSc1	Bítov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1234	[number]	k4	1234
Ostoj	Ostoj	k1gInSc1	Ostoj
</s>
</p>
<p>
<s>
1260	[number]	k4	1260
<g/>
–	–	k?	–
<g/>
1268	[number]	k4	1268
Ranožíř	Ranožíř	k1gInSc1	Ranožíř
</s>
</p>
<p>
<s>
1269	[number]	k4	1269
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
–	–	k?	–
1278	[number]	k4	1278
Cuzkraj	Cuzkraj	k1gFnSc1	Cuzkraj
</s>
</p>
<p>
<s>
1279	[number]	k4	1279
Volfram	Volfram	k1gInSc1	Volfram
</s>
</p>
<p>
<s>
1281	[number]	k4	1281
Cuzkraj	Cuzkraj	k1gFnSc1	Cuzkraj
</s>
</p>
<p>
<s>
1298	[number]	k4	1298
Držislav	Držislav	k1gMnSc1	Držislav
z	z	k7c2	z
Boleradic	Boleradice	k1gFnPc2	Boleradice
</s>
</p>
<p>
<s>
1345	[number]	k4	1345
Blud	blud	k1gInSc1	blud
z	z	k7c2	z
Kralic	Kralice	k1gFnPc2	Kralice
</s>
</p>
<p>
<s>
====	====	k?	====
Břeclavský	břeclavský	k2eAgMnSc1d1	břeclavský
sudí	sudí	k1gMnSc1	sudí
====	====	k?	====
</s>
</p>
<p>
<s>
1255	[number]	k4	1255
<g/>
–	–	k?	–
<g/>
1259	[number]	k4	1259
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
</s>
</p>
<p>
<s>
1295	[number]	k4	1295
Přebor	přebor	k1gInSc1	přebor
</s>
</p>
<p>
<s>
1297	[number]	k4	1297
<g/>
–	–	k?	–
<g/>
1317	[number]	k4	1317
Velislav	Velislava	k1gFnPc2	Velislava
z	z	k7c2	z
Ořechové	ořechový	k2eAgFnSc2d1	ořechová
</s>
</p>
<p>
<s>
1320	[number]	k4	1320
Pardus	pardus	k1gInSc1	pardus
ze	z	k7c2	z
Šardic	Šardice	k1gFnPc2	Šardice
na	na	k7c6	na
Horké	Horká	k1gFnSc6	Horká
</s>
</p>
<p>
<s>
1322	[number]	k4	1322
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Blišic	Blišice	k1gFnPc2	Blišice
</s>
</p>
<p>
<s>
====	====	k?	====
Nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
sudí	sudí	k1gMnPc1	sudí
====	====	k?	====
</s>
</p>
<p>
<s>
1486	[number]	k4	1486
<g/>
–	–	k?	–
<g/>
1496	[number]	k4	1496
Jan	Jan	k1gMnSc1	Jan
Pňovský	Pňovský	k2eAgInSc1d1	Pňovský
ze	z	k7c2	z
Sovince	Sovinec	k1gInSc2	Sovinec
</s>
</p>
<p>
<s>
1496	[number]	k4	1496
<g/>
–	–	k?	–
<g/>
1504	[number]	k4	1504
Lipolt	Lipolt	k1gInSc1	Lipolt
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
</s>
</p>
<p>
<s>
1505	[number]	k4	1505
<g/>
–	–	k?	–
<g/>
1507	[number]	k4	1507
Jan	Jan	k1gMnSc1	Jan
Pňovský	Pňovský	k2eAgInSc1d1	Pňovský
ze	z	k7c2	z
Sovince	Sovinec	k1gInSc2	Sovinec
</s>
</p>
<p>
<s>
1508	[number]	k4	1508
<g/>
–	–	k?	–
<g/>
1517	[number]	k4	1517
Znáta	Znáta	k1gFnSc1	Znáta
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
</s>
</p>
<p>
<s>
1518	[number]	k4	1518
<g/>
–	–	k?	–
<g/>
1524	[number]	k4	1524
Vok	Vok	k1gFnSc2	Vok
Pňovský	Pňovský	k2eAgInSc4d1	Pňovský
ze	z	k7c2	z
Sovince	Sovinec	k1gInSc2	Sovinec
</s>
</p>
<p>
<s>
1526	[number]	k4	1526
Dobeš	Dobeš	k1gMnSc1	Dobeš
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
a	a	k8xC	a
na	na	k7c6	na
Rosicích	Rosice	k1gFnPc6	Rosice
</s>
</p>
<p>
<s>
1527	[number]	k4	1527
<g/>
–	–	k?	–
<g/>
1536	[number]	k4	1536
Jan	Jan	k1gMnSc1	Jan
st.	st.	kA	st.
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
na	na	k7c4	na
Kvasicích	kvasicí	k2eAgNnPc2d1	kvasicí
(	(	kIx(	(
<g/>
†	†	k?	†
1536	[number]	k4	1536
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1536	[number]	k4	1536
<g/>
–	–	k?	–
<g/>
1548	[number]	k4	1548
Jan	Jan	k1gMnSc1	Jan
Zajímač	zajímač	k1gMnSc1	zajímač
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
na	na	k7c6	na
Jevišovicích	Jevišovice	k1gFnPc6	Jevišovice
a	a	k8xC	a
Tavíkovicích	Tavíkovice	k1gFnPc6	Tavíkovice
</s>
</p>
<p>
<s>
1548	[number]	k4	1548
<g/>
–	–	k?	–
<g/>
1552	[number]	k4	1552
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc3	Liechtenstein
na	na	k7c4	na
Lednici	Lednice	k1gFnSc4	Lednice
</s>
</p>
<p>
<s>
1552	[number]	k4	1552
<g/>
–	–	k?	–
<g/>
1554	[number]	k4	1554
Václav	Václav	k1gMnSc1	Václav
Černohorský	Černohorský	k1gMnSc1	Černohorský
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
</s>
</p>
<p>
<s>
1554	[number]	k4	1554
<g/>
–	–	k?	–
<g/>
1563	[number]	k4	1563
Václav	Václav	k1gMnSc1	Václav
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
</s>
</p>
<p>
<s>
1563	[number]	k4	1563
<g/>
–	–	k?	–
<g/>
1567	[number]	k4	1567
Albrecht	Albrecht	k1gMnSc1	Albrecht
Černohorský	Černohorský	k1gMnSc1	Černohorský
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
</s>
</p>
<p>
<s>
1580	[number]	k4	1580
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
Jan	Jana	k1gFnPc2	Jana
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
na	na	k7c6	na
Třebové	Třebová	k1gFnSc6	Třebová
</s>
</p>
<p>
<s>
1581	[number]	k4	1581
<g/>
–	–	k?	–
<g/>
1582	[number]	k4	1582
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Hynek	Hynek	k1gMnSc1	Hynek
Brtnický	brtnický	k2eAgMnSc1d1	brtnický
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
</s>
</p>
<p>
<s>
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1594	[number]	k4	1594
Protas	Protas	k1gInSc1	Protas
z	z	k7c2	z
Lomnice	Lomnice	k1gFnSc2	Lomnice
na	na	k7c4	na
Mezeříčí	Mezeříčí	k1gNnSc4	Mezeříčí
a	a	k8xC	a
Jemnici	Jemnice	k1gFnSc4	Jemnice
</s>
</p>
<p>
<s>
1596	[number]	k4	1596
<g/>
–	–	k?	–
<g/>
1599	[number]	k4	1599
Jáchym	Jáchym	k1gMnSc1	Jáchym
Haugwitz	Haugwitz	k1gMnSc1	Haugwitz
z	z	k7c2	z
Biskupic	Biskupic	k1gMnSc1	Biskupic
na	na	k7c6	na
Rokytnici	Rokytnice	k1gFnSc6	Rokytnice
a	a	k8xC	a
Kralicích	Kralice	k1gFnPc6	Kralice
</s>
</p>
<p>
<s>
1599	[number]	k4	1599
<g/>
–	–	k?	–
<g/>
1601	[number]	k4	1601
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Liechtensteinu	Liechtenstein	k1gMnSc6	Liechtenstein
na	na	k7c6	na
Mikulově	Mikulov	k1gInSc6	Mikulov
</s>
</p>
<p>
<s>
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Lev	Lev	k1gMnSc1	Lev
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
</s>
</p>
<p>
<s>
1613	[number]	k4	1613
<g/>
–	–	k?	–
<g/>
1616	[number]	k4	1616
Vilém	Viléma	k1gFnPc2	Viléma
z	z	k7c2	z
Roupova	roupův	k2eAgInSc2d1	roupův
</s>
</p>
<p>
<s>
1617	[number]	k4	1617
<g/>
–	–	k?	–
<g/>
1618	[number]	k4	1618
Lev	Lev	k1gMnSc1	Lev
Burian	Burian	k1gMnSc1	Burian
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1586	[number]	k4	1586
<g/>
/	/	kIx~	/
<g/>
1590	[number]	k4	1590
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
</s>
</p>
<p>
<s>
1619	[number]	k4	1619
Václav	Václav	k1gMnSc1	Václav
Bítovský	Bítovský	k1gMnSc1	Bítovský
ze	z	k7c2	z
Slavikovic	Slavikovice	k1gFnPc2	Slavikovice
</s>
</p>
<p>
<s>
1623	[number]	k4	1623
<g/>
–	–	k?	–
<g/>
1625	[number]	k4	1625
Lev	Lev	k1gMnSc1	Lev
Burian	Burian	k1gMnSc1	Burian
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1586	[number]	k4	1586
<g/>
/	/	kIx~	/
<g/>
1590	[number]	k4	1590
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
–	–	k?	–
podruhé	podruhé	k6eAd1	podruhé
</s>
</p>
<p>
<s>
1629	[number]	k4	1629
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Náchoda	Náchod	k1gInSc2	Náchod
</s>
</p>
<p>
<s>
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
Julius	Julius	k1gMnSc1	Julius
ze	z	k7c2	z
Salm-Neuburgu	Salm-Neuburg	k1gInSc2	Salm-Neuburg
</s>
</p>
<p>
<s>
1637	[number]	k4	1637
<g/>
–	–	k?	–
<g/>
1642	[number]	k4	1642
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Rottalu	Rottal	k1gMnSc6	Rottal
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1674	[number]	k4	1674
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1643	[number]	k4	1643
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
Baltazar	Baltazar	k1gMnSc1	Baltazar
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
</s>
</p>
<p>
<s>
1644	[number]	k4	1644
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
Gabriel	Gabriela	k1gFnPc2	Gabriela
Serenyi	Serenyi	k1gNnSc2	Serenyi
</s>
</p>
<p>
<s>
1649	[number]	k4	1649
<g/>
–	–	k?	–
<g/>
1651	[number]	k4	1651
František	františek	k1gInSc1	františek
Magnis	Magnis	k1gFnSc2	Magnis
(	(	kIx(	(
<g/>
1598	[number]	k4	1598
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1652	[number]	k4	1652
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1651	[number]	k4	1651
<g/>
–	–	k?	–
<g/>
1652	[number]	k4	1652
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
Valentin	Valentina	k1gFnPc2	Valentina
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
(	(	kIx(	(
<g/>
1612	[number]	k4	1612
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1677	[number]	k4	1677
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1652	[number]	k4	1652
<g/>
–	–	k?	–
<g/>
1655	[number]	k4	1655
Lev	Lev	k1gMnSc1	Lev
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Kounic	Kounice	k1gFnPc2	Kounice
</s>
</p>
<p>
<s>
1655	[number]	k4	1655
<g/>
–	–	k?	–
<g/>
1659	[number]	k4	1659
Michael	Michael	k1gMnSc1	Michael
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
z	z	k7c2	z
Althannu	Althann	k1gInSc2	Althann
</s>
</p>
<p>
<s>
1659	[number]	k4	1659
<g/>
–	–	k?	–
<g/>
1660	[number]	k4	1660
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
František	František	k1gMnSc1	František
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
z	z	k7c2	z
Heisenštejna	Heisenštejn	k1gInSc2	Heisenštejn
</s>
</p>
<p>
<s>
1660	[number]	k4	1660
<g/>
–	–	k?	–
<g/>
1664	[number]	k4	1664
Jiří	Jiří	k1gMnSc1	Jiří
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bruntálský	bruntálský	k2eAgMnSc1d1	bruntálský
z	z	k7c2	z
Vrbna	Vrbno	k1gNnSc2	Vrbno
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1615	[number]	k4	1615
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1682	[number]	k4	1682
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
1675	[number]	k4	1675
Antonín	Antonín	k1gMnSc1	Antonín
František	František	k1gMnSc1	František
Collalto	Collalto	k1gNnSc4	Collalto
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1630	[number]	k4	1630
Mantova	Mantova	k1gFnSc1	Mantova
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1675	[number]	k4	1675
<g/>
–	–	k?	–
<g/>
1679	[number]	k4	1679
Michael	Michael	k1gMnSc1	Michael
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Althannu	Althann	k1gInSc2	Althann
</s>
</p>
<p>
<s>
1679	[number]	k4	1679
<g/>
–	–	k?	–
<g/>
1697	[number]	k4	1697
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Oppersdorffu	Oppersdorff	k1gInSc2	Oppersdorff
</s>
</p>
<p>
<s>
1697	[number]	k4	1697
<g/>
–	–	k?	–
<g/>
1701	[number]	k4	1701
Karel	Karel	k1gMnSc1	Karel
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Thurn-Valsássina	Thurn-Valsássina	k1gFnSc1	Thurn-Valsássina
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1643	[number]	k4	1643
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1701	[number]	k4	1701
<g/>
–	–	k?	–
<g/>
1702	[number]	k4	1702
Walter	Walter	k1gMnSc1	Walter
Xaver	Xaver	k1gMnSc1	Xaver
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1664	[number]	k4	1664
Brno	Brno	k1gNnSc4	Brno
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1738	[number]	k4	1738
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1702	[number]	k4	1702
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
František	František	k1gMnSc1	František
Dominik	Dominik	k1gMnSc1	Dominik
Podstatský	Podstatský	k2eAgMnSc1d1	Podstatský
z	z	k7c2	z
Prusinovic	Prusinovice	k1gFnPc2	Prusinovice
</s>
</p>
<p>
<s>
1714	[number]	k4	1714
<g/>
–	–	k?	–
<g/>
1721	[number]	k4	1721
Michael	Michael	k1gMnSc1	Michael
Heřman	Heřman	k1gMnSc1	Heřman
z	z	k7c2	z
Althannu	Althann	k1gInSc2	Althann
</s>
</p>
<p>
<s>
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1726	[number]	k4	1726
Leopold	Leopold	k1gMnSc1	Leopold
Antonín	Antonín	k1gMnSc1	Antonín
Sak	sak	k1gInSc4	sak
z	z	k7c2	z
Bohuňovic	Bohuňovice	k1gFnPc2	Bohuňovice
</s>
</p>
<p>
<s>
1726	[number]	k4	1726
<g/>
–	–	k?	–
<g/>
1737	[number]	k4	1737
František	František	k1gMnSc1	František
Michal	Michal	k1gMnSc1	Michal
Šubíř	Šubíř	k1gMnSc1	Šubíř
z	z	k7c2	z
Chobyně	Chobyně	k1gFnSc2	Chobyně
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1682	[number]	k4	1682
Jaroměřice	Jaroměřice	k1gFnPc4	Jaroměřice
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1738	[number]	k4	1738
Jaroměřice	Jaroměřice	k1gFnPc1	Jaroměřice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1737	[number]	k4	1737
Leopold	Leopold	k1gMnSc1	Leopold
z	z	k7c2	z
Dietrichsteinu	Dietrichstein	k1gInSc2	Dietrichstein
</s>
</p>
<p>
<s>
1738	[number]	k4	1738
<g/>
–	–	k?	–
<g/>
1748	[number]	k4	1748
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Heissler	Heissler	k1gMnSc1	Heissler
z	z	k7c2	z
Heitersheimu	Heitersheim	k1gInSc2	Heitersheim
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1748	[number]	k4	1748
<g/>
–	–	k?	–
<g/>
1753	[number]	k4	1753
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Schrattenbach	Schrattenbach	k1gMnSc1	Schrattenbach
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1712	[number]	k4	1712
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1783	[number]	k4	1783
Baden	Badno	k1gNnPc2	Badno
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1753	[number]	k4	1753
<g/>
–	–	k?	–
<g/>
1755	[number]	k4	1755
Václav	Václav	k1gMnSc1	Václav
Michal	Michal	k1gMnSc1	Michal
z	z	k7c2	z
Vrbna	Vrbn	k1gInSc2	Vrbn
</s>
</p>
<p>
<s>
1755	[number]	k4	1755
<g/>
–	–	k?	–
<g/>
1763	[number]	k4	1763
Karel	Karel	k1gMnSc1	Karel
Otto	Otto	k1gMnSc1	Otto
ze	z	k7c2	z
Salm-Neuburgu	Salm-Neuburg	k1gInSc2	Salm-Neuburg
</s>
</p>
<p>
<s>
1763	[number]	k4	1763
Mikuláš	mikuláš	k1gInSc1	mikuláš
Hamilton	Hamilton	k1gInSc4	Hamilton
</s>
</p>
<p>
<s>
1766	[number]	k4	1766
František	František	k1gMnSc1	František
Reinolt	Reinolt	k1gMnSc1	Reinolt
Andlern	Andlern	k1gMnSc1	Andlern
Vitten	Vitten	k2eAgMnSc1d1	Vitten
(	(	kIx(	(
<g/>
†	†	k?	†
1766	[number]	k4	1766
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1766	[number]	k4	1766
<g/>
–	–	k?	–
<g/>
1772	[number]	k4	1772
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Widmann	Widmann	k1gMnSc1	Widmann
</s>
</p>
<p>
<s>
1772	[number]	k4	1772
<g/>
–	–	k?	–
<g/>
1773	[number]	k4	1773
Leopold	Leopolda	k1gFnPc2	Leopolda
Clary-Aldringen	Clary-Aldringen	k1gInSc4	Clary-Aldringen
</s>
</p>
<p>
<s>
1773	[number]	k4	1773
<g/>
–	–	k?	–
<g/>
1776	[number]	k4	1776
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
</s>
</p>
<p>
<s>
1776	[number]	k4	1776
<g/>
–	–	k?	–
<g/>
1781	[number]	k4	1781
Jan	Jan	k1gMnSc1	Jan
Baltazar	Baltazar	k1gMnSc1	Baltazar
Mitrovský	Mitrovský	k2eAgMnSc1d1	Mitrovský
z	z	k7c2	z
Nemyšle	Nemyšle	k1gNnSc2	Nemyšle
</s>
</p>
<p>
<s>
1781	[number]	k4	1781
<g/>
–	–	k?	–
<g/>
1783	[number]	k4	1783
Jan	Jan	k1gMnSc1	Jan
Zikmund	Zikmund	k1gMnSc1	Zikmund
Bukůvka	Bukůvka	k1gFnSc1	Bukůvka
z	z	k7c2	z
Bukůvky	Bukůvka	k1gFnSc2	Bukůvka
</s>
</p>
<p>
<s>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
Petr	Petr	k1gMnSc1	Petr
Alcantara	Alcantara	k1gFnSc1	Alcantara
Blümegen	Blümegen	k1gInSc1	Blümegen
</s>
</p>
<p>
<s>
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1812	[number]	k4	1812
František	František	k1gMnSc1	František
Dubský	Dubský	k1gMnSc1	Dubský
z	z	k7c2	z
Třebomyslic	Třebomyslice	k1gFnPc2	Třebomyslice
</s>
</p>
<p>
<s>
1812	[number]	k4	1812
<g/>
–	–	k?	–
<g/>
1816	[number]	k4	1816
Heřman	Heřman	k1gMnSc1	Heřman
František	František	k1gMnSc1	František
Hess	Hess	k1gInSc4	Hess
</s>
</p>
<p>
<s>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
Josef	Josef	k1gMnSc1	Josef
Bubna	Bubn	k1gInSc2	Bubn
z	z	k7c2	z
Litic	Litice	k1gFnPc2	Litice
</s>
</p>
<p>
<s>
1829	[number]	k4	1829
<g/>
–	–	k?	–
<g/>
1837	[number]	k4	1837
Antonín	Antonín	k1gMnSc1	Antonín
Seblnický	Seblnický	k2eAgMnSc5d1	Seblnický
</s>
</p>
<p>
<s>
1837	[number]	k4	1837
Rudolf	Rudolf	k1gMnSc1	Rudolf
z	z	k7c2	z
Tannenberka	Tannenberka	k1gFnSc1	Tannenberka
</s>
</p>
<p>
<s>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Volkenštejna	Volkenštejn	k1gInSc2	Volkenštejn
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOČEK	Boček	k1gMnSc1	Boček
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc4	přehled
Knížat	kníže	k1gNnPc2	kníže
a	a	k8xC	a
Markrabat	markrabě	k1gNnPc2	markrabě
i	i	k8xC	i
jiných	jiný	k2eAgMnPc2d1	jiný
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
důstojníků	důstojník	k1gMnPc2	důstojník
zemských	zemský	k2eAgMnPc2d1	zemský
v	v	k7c6	v
markrabství	markrabství	k1gNnSc6	markrabství
moravském	moravský	k2eAgNnSc6d1	Moravské
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jednota	jednota	k1gFnSc1	jednota
národní	národní	k2eAgFnSc2d1	národní
s.	s.	k?	s.
Cyrilla	Cyrilla	k1gMnSc1	Cyrilla
a	a	k8xC	a
Methuda	Methuda	k1gMnSc1	Methuda
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČAPKA	Čapka	k1gMnSc1	Čapka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Akademické	akademický	k2eAgNnSc1d1	akademické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
CERM	CERM	kA	CERM
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
434	[number]	k4	434
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7204	[number]	k4	7204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
225	[number]	k4	225
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLEDÍKOVÁ	HLEDÍKOVÁ	kA	HLEDÍKOVÁ
<g/>
,	,	kIx,	,
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
<g/>
;	;	kIx,	;
JANÁK	Janák	k1gMnSc1	Janák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
DOBEŠ	Dobeš	k1gMnSc1	Dobeš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
:	:	kIx,	:
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
státu	stát	k1gInSc2	stát
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
570	[number]	k4	570
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
709	[number]	k4	709
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
53	[number]	k4	53
<g/>
,	,	kIx,	,
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
124	[number]	k4	124
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MALÝ	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
československého	československý	k2eAgNnSc2d1	Československé
práva	právo	k1gNnSc2	právo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Linde	Lind	k1gInSc5	Lind
Praha	Praha	k1gFnSc1	Praha
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7201	[number]	k4	7201
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PALACKÝ	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
I.	I.	kA	I.
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Přehled	přehled	k1gInSc4	přehled
současný	současný	k2eAgInSc1d1	současný
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
s.	s.	k?	s.
321	[number]	k4	321
<g/>
–	–	k?	–
<g/>
417	[number]	k4	417
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RAMEŠ	Rameš	k1gMnSc1	Rameš
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
pro	pro	k7c4	pro
historiky	historik	k1gMnPc4	historik
a	a	k8xC	a
návštěvníky	návštěvník	k1gMnPc4	návštěvník
archivů	archiv	k1gInPc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
176	[number]	k4	176
<g/>
–	–	k?	–
<g/>
177	[number]	k4	177
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VYKOUPIL	vykoupit	k5eAaPmAgMnS	vykoupit
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgInPc1d1	přepracovaný
a	a	k8xC	a
doplněné	doplněný	k2eAgInPc1d1	doplněný
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
772	[number]	k4	772
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902782	[number]	k4	902782
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
396	[number]	k4	396
<g/>
-	-	kIx~	-
<g/>
397	[number]	k4	397
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
</s>
</p>
<p>
<s>
Místosudí	místosudí	k1gMnSc1	místosudí
</s>
</p>
<p>
<s>
Dvorský	dvorský	k2eAgInSc1d1	dvorský
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
dvorský	dvorský	k2eAgMnSc1d1	dvorský
sudí	sudí	k1gMnSc1	sudí
</s>
</p>
