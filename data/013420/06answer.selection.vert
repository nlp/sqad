<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Oberstlandrichter	Oberstlandrichter	k1gInSc1	Oberstlandrichter
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
iudex	iudex	k1gInSc1	iudex
terrae	terra	k1gFnSc2	terra
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
supremus	supremus	k1gInSc1	supremus
iudex	iudex	k1gInSc1	iudex
terrae	terrae	k1gInSc1	terrae
nebo	nebo	k8xC	nebo
iudex	iudex	k1gInSc1	iudex
Bohemiae	Bohemia	k1gInSc2	Bohemia
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dvanáctičlenném	dvanáctičlenný	k2eAgNnSc6d1	dvanáctičlenné
kolegiu	kolegium	k1gNnSc6	kolegium
pátým	pátý	k4xOgNnSc7	pátý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zemským	zemský	k2eAgMnSc7d1	zemský
stavovským	stavovský	k2eAgMnSc7d1	stavovský
úředníkem	úředník	k1gMnSc7	úředník
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
