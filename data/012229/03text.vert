<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Křetínský	Křetínský	k1gMnSc1	Křetínský
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1975	[number]	k4	1975
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
spoluvlastník	spoluvlastník	k1gMnSc1	spoluvlastník
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
desítku	desítka	k1gFnSc4	desítka
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
dolarové	dolarový	k2eAgMnPc4d1	dolarový
miliardáře	miliardář	k1gMnPc4	miliardář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
(	(	kIx(	(
<g/>
94	[number]	k4	94
%	%	kIx~	%
<g/>
)	)	kIx)	)
české	český	k2eAgFnSc2d1	Česká
energetické	energetický	k2eAgFnSc2d1	energetická
skupiny	skupina	k1gFnSc2	skupina
Energetický	energetický	k2eAgMnSc1d1	energetický
a	a	k8xC	a
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
EPH	EPH	kA	EPH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
šesti	šest	k4xCc6	šest
evropských	evropský	k2eAgInPc6d1	evropský
státech	stát	k1gInPc6	stát
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
profesor	profesor	k1gMnSc1	profesor
RNDr.	RNDr.	kA	RNDr.
Mojmír	Mojmír	k1gMnSc1	Mojmír
Křetínský	Křetínský	k1gMnSc1	Křetínský
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
Katedry	katedra	k1gFnSc2	katedra
teorie	teorie	k1gFnSc2	teorie
programování	programování	k1gNnSc2	programování
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
doc.	doc.	kA	doc.
JUDr.	JUDr.	kA	JUDr.
Michaela	Michaela	k1gFnSc1	Michaela
Židlická	Židlická	k1gFnSc1	Židlická
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
soudkyní	soudkyně	k1gFnPc2	soudkyně
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přednáší	přednášet	k5eAaImIp3nS	přednášet
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Křetínský	Křetínský	k1gMnSc1	Křetínský
má	mít	k5eAaImIp3nS	mít
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
titul	titul	k1gInSc4	titul
z	z	k7c2	z
politických	politický	k2eAgFnPc2d1	politická
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
titul	titul	k1gInSc1	titul
magistra	magister	k1gMnSc2	magister
a	a	k8xC	a
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
pražskou	pražský	k2eAgFnSc4d1	Pražská
vilu	vila	k1gFnSc4	vila
u	u	k7c2	u
Mrázovky	Mrázovka	k1gFnSc2	Mrázovka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bydlel	bydlet	k5eAaImAgMnS	bydlet
průmyslník	průmyslník	k1gMnSc1	průmyslník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Preiss	Preiss	k1gInSc1	Preiss
a	a	k8xC	a
za	za	k7c2	za
totality	totalita	k1gFnSc2	totalita
komunistický	komunistický	k2eAgMnSc1d1	komunistický
pohlavár	pohlavár	k1gMnSc1	pohlavár
Vasil	Vasil	k1gMnSc1	Vasil
Biľak	Biľak	k1gMnSc1	Biľak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
hraje	hrát	k5eAaImIp3nS	hrát
golf	golf	k1gInSc4	golf
a	a	k8xC	a
tenis	tenis	k1gInSc4	tenis
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sběratelem	sběratel	k1gMnSc7	sběratel
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
byla	být	k5eAaImAgFnS	být
Klára	Klára	k1gFnSc1	Klára
Cetlová	Cetlová	k1gFnSc1	Cetlová
<g/>
,	,	kIx,	,
členka	členka	k1gFnSc1	členka
rozkladové	rozkladový	k2eAgFnSc2d1	rozkladová
komise	komise	k1gFnSc2	komise
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
Anna	Anna	k1gFnSc1	Anna
Kellnerová	Kellnerová	k1gFnSc1	Kellnerová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
známého	známý	k2eAgMnSc2d1	známý
podnikatele	podnikatel	k1gMnSc2	podnikatel
Petra	Petr	k1gMnSc2	Petr
Kellnera	Kellner	k1gMnSc2	Kellner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
koncipient	koncipient	k1gMnSc1	koncipient
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
advokátní	advokátní	k2eAgFnSc3d1	advokátní
kanceláři	kancelář	k1gFnSc3	kancelář
Gottweis	Gottweis	k1gFnSc3	Gottweis
&	&	k?	&
Partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
investiční	investiční	k2eAgFnSc4d1	investiční
skupinu	skupina	k1gFnSc4	skupina
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
partnerem	partner	k1gMnSc7	partner
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
ziscích	zisk	k1gInPc6	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
Energetického	energetický	k2eAgInSc2d1	energetický
a	a	k8xC	a
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
holdingu	holding	k1gInSc2	holding
(	(	kIx(	(
<g/>
EPH	EPH	kA	EPH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
jeho	jeho	k3xOp3gNnSc2	jeho
představenstva	představenstvo	k1gNnSc2	představenstvo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
94	[number]	k4	94
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
majoritním	majoritní	k2eAgMnSc7d1	majoritní
vlastníkem	vlastník	k1gMnSc7	vlastník
EP	EP	kA	EP
Industries	Industries	k1gMnSc1	Industries
<g/>
,	,	kIx,	,
odštěpené	odštěpený	k2eAgFnSc6d1	odštěpená
z	z	k7c2	z
EPH	EPH	kA	EPH
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
firmu	firma	k1gFnSc4	firma
AVE	ave	k1gNnSc2	ave
CZ	CZ	kA	CZ
nebo	nebo	k8xC	nebo
SOR	SOR	kA	SOR
Libchavy	Libchava	k1gFnPc4	Libchava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
několika	několik	k4yIc7	několik
holdingu	holding	k1gInSc2	holding
přidružených	přidružený	k2eAgFnPc2d1	přidružená
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
společnost	společnost	k1gFnSc1	společnost
NAFTA	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
Eustream	Eustream	k1gInSc1	Eustream
a	a	k8xC	a
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
EPH	EPH	kA	EPH
–	–	k?	–
EP	EP	kA	EP
Investment	Investment	k1gInSc1	Investment
Advisors	Advisors	k1gInSc1	Advisors
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
i	i	k9	i
funkce	funkce	k1gFnSc1	funkce
ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nejsou	být	k5eNaImIp3nP	být
spřízněny	spřízněn	k2eAgInPc1d1	spřízněn
s	s	k7c7	s
EPH	EPH	kA	EPH
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
šéfem	šéf	k1gMnSc7	šéf
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
akcionáři	akcionář	k1gMnPc7	akcionář
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
<g/>
.	.	kIx.	.
<g/>
Křetínský	Křetínský	k1gMnSc1	Křetínský
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Ringier	Ringier	k1gMnSc1	Ringier
<g />
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
Blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
Reflex	reflex	k1gInSc1	reflex
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
Sport	sport	k1gInSc1	sport
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
Czech	Czech	k1gInSc4	Czech
News	News	k1gInSc4	News
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Křetínský	Křetínský	k1gMnSc1	Křetínský
a	a	k8xC	a
slovenský	slovenský	k2eAgMnSc1d1	slovenský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Patrik	Patrik	k1gMnSc1	Patrik
Tkáč	tkáč	k1gMnSc1	tkáč
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
skupinou	skupina	k1gFnSc7	skupina
Haniel	Haniel	k1gMnSc1	Haniel
Holding	holding	k1gInSc1	holding
na	na	k7c6	na
koupi	koupě	k1gFnSc6	koupě
7,3	[number]	k4	7,3
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
Metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
burzovní	burzovní	k2eAgFnSc3d1	burzovní
kapitalizaci	kapitalizace	k1gFnSc3	kapitalizace
Metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
do	do	k7c2	do
indexu	index	k1gInSc2	index
M-Dax	M-Dax	k1gInSc1	M-Dax
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
možnou	možný	k2eAgFnSc4d1	možná
kupní	kupní	k2eAgFnSc4d1	kupní
cenu	cena	k1gFnSc4	cena
balíku	balík	k1gInSc2	balík
akcií	akcie	k1gFnPc2	akcie
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
360	[number]	k4	360
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
asi	asi	k9	asi
9,2	[number]	k4	9,2
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Metro	metro	k1gNnSc4	metro
provozuje	provozovat	k5eAaImIp3nS	provozovat
763	[number]	k4	763
tzv.	tzv.	kA	tzv.
cash-and-carry	cashndarra	k1gFnSc2	cash-and-carra
obchodů	obchod	k1gInPc2	obchod
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
zákazníky	zákazník	k1gMnPc4	zákazník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
podnikatelů	podnikatel	k1gMnPc2	podnikatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
činná	činný	k2eAgFnSc1d1	činná
ve	v	k7c6	v
25	[number]	k4	25
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Křetínský	Křetínský	k1gMnSc1	Křetínský
a	a	k8xC	a
Tkáč	tkáč	k1gMnSc1	tkáč
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Metra	metr	k1gMnSc2	metr
Olafa	Olaf	k1gMnSc2	Olaf
Kocha	Koch	k1gMnSc2	Koch
nadto	nadto	k6eAd1	nadto
tzv.	tzv.	kA	tzv.
předkupní	předkupní	k2eAgNnSc1d1	předkupní
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
15,2	[number]	k4	15,2
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
deníkem	deník	k1gInSc7	deník
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Allgemeine	Allgemein	k1gInSc5	Allgemein
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Koch	Koch	k1gMnSc1	Koch
velký	velký	k2eAgInSc4d1	velký
respekt	respekt	k1gInSc4	respekt
před	před	k7c7	před
podnikatelskými	podnikatelský	k2eAgInPc7d1	podnikatelský
výsledky	výsledek	k1gInPc7	výsledek
Křetínského	Křetínského	k2eAgInPc7d1	Křetínského
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Křetínský	Křetínský	k1gMnSc1	Křetínský
<g/>
,	,	kIx,	,
interview	interview	k1gNnSc1	interview
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
</s>
</p>
