<s>
Vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
nebo	nebo	k8xC	nebo
také	také	k9	také
ruské	ruský	k2eAgNnSc1d1	ruské
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
vertikálně	vertikálně	k6eAd1	vertikálně
umístěného	umístěný	k2eAgNnSc2d1	umístěné
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
obvodu	obvod	k1gInSc6	obvod
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístěn	k2eAgInPc1d1	rozmístěn
vagóny	vagón	k1gInPc1	vagón
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
