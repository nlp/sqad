<p>
<s>
Vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
nebo	nebo	k8xC	nebo
také	také	k9	také
ruské	ruský	k2eAgNnSc1d1	ruské
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
vertikálně	vertikálně	k6eAd1	vertikálně
umístěného	umístěný	k2eAgNnSc2d1	umístěné
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
obvodu	obvod	k1gInSc6	obvod
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístěn	k2eAgInPc1d1	rozmístěn
vagóny	vagón	k1gInPc1	vagón
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zábavní	zábavní	k2eAgFnSc1d1	zábavní
lunaparková	lunaparkový	k2eAgFnSc1d1	lunaparkový
atrakce	atrakce	k1gFnSc1	atrakce
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc4	původ
vyhlídkového	vyhlídkový	k2eAgNnSc2d1	vyhlídkové
/	/	kIx~	/
ruského	ruský	k2eAgNnSc2d1	ruské
kola	kolo	k1gNnSc2	kolo
bývá	bývat	k5eAaImIp3nS	bývat
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
popsal	popsat	k5eAaPmAgMnS	popsat
Angličan	Angličan	k1gMnSc1	Angličan
Peter	Peter	k1gMnSc1	Peter
Mundy	Munda	k1gFnSc2	Munda
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
cestopisu	cestopis	k1gInSc6	cestopis
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kolotoč	kolotoč	k1gInSc4	kolotoč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
viděl	vidět	k5eAaImAgInS	vidět
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
moderní	moderní	k2eAgFnSc1d1	moderní
obří	obří	k2eAgNnSc4d1	obří
kolo	kolo	k1gNnSc4	kolo
postavil	postavit	k5eAaPmAgMnS	postavit
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
americký	americký	k2eAgMnSc1d1	americký
inženýr	inženýr	k1gMnSc1	inženýr
George	Georg	k1gFnSc2	Georg
Washington	Washington	k1gInSc1	Washington
Gale	Gale	k1gMnSc1	Gale
Ferris	Ferris	k1gFnPc2	Ferris
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
atrakci	atrakce	k1gFnSc3	atrakce
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
říká	říkat	k5eAaImIp3nS	říkat
Ferris	Ferris	k1gFnSc4	Ferris
wheel	wheela	k1gFnPc2	wheela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známá	známý	k2eAgFnSc1d1	známá
obří	obří	k2eAgFnSc1d1	obří
kola	kola	k1gFnSc1	kola
==	==	k?	==
</s>
</p>
<p>
<s>
Obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
parku	park	k1gInSc6	park
Prátr	Prátr	k1gInSc1	Prátr
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
65	[number]	k4	65
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
London	London	k1gMnSc1	London
Eye	Eye	k1gMnSc1	Eye
(	(	kIx(	(
<g/>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
135	[number]	k4	135
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
60	[number]	k4	60
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
(	(	kIx(	(
<g/>
53	[number]	k4	53
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Singapore	Singapor	k1gMnSc5	Singapor
Flyer	Flyer	k1gMnSc1	Flyer
(	(	kIx(	(
<g/>
165	[number]	k4	165
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
High	High	k1gMnSc1	High
Roller	Roller	k1gMnSc1	Roller
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
167,6	[number]	k4	167,6
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obří	obří	k2eAgNnSc1d1	obří
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
–	–	k?	–
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
největším	veliký	k2eAgMnSc7d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
208	[number]	k4	208
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
zahájena	zahájen	k2eAgFnSc1d1	zahájena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
plánem	plán	k1gInSc7	plán
otevřít	otevřít	k5eAaPmF	otevřít
kolo	kolo	k1gNnSc4	kolo
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
konání	konání	k1gNnSc2	konání
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ain	Ain	k?	Ain
Dubai	Dubai	k1gNnSc1	Dubai
v	v	k7c6	v
Dubai	Duba	k1gInSc6	Duba
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
bude	být	k5eAaImBp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
210,0	[number]	k4	210,0
m	m	kA	m
<g/>
)	)	kIx)	)
Ain	Ain	k1gMnSc1	Ain
Dubai	Duba	k1gFnSc2	Duba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ain	Ain	k1gFnSc1	Ain
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
arabsky	arabsky	k6eAd1	arabsky
"	"	kIx"	"
<g/>
oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
pojme	pojmout	k5eAaPmIp3nS	pojmout
najednou	najednou	k6eAd1	najednou
1400	[number]	k4	1400
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
/	/	kIx~	/
ruské	ruský	k2eAgNnSc1d1	ruské
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
lunaparku	lunapark	k1gInSc6	lunapark
na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
(	(	kIx(	(
<g/>
35	[number]	k4	35
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
/	/	kIx~	/
ruské	ruský	k2eAgNnSc1d1	ruské
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
ukrajinském	ukrajinský	k2eAgInSc6d1	ukrajinský
zábavním	zábavní	k2eAgInSc6d1	zábavní
park	park	k1gInSc1	park
v	v	k7c6	v
Pripjati	Pripjat	k2eAgMnPc1d1	Pripjat
(	(	kIx(	(
<g/>
26	[number]	k4	26
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Pripjať	Pripjať	k1gFnSc2	Pripjať
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
opustili	opustit	k5eAaPmAgMnP	opustit
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lunapark	lunapark	k1gInSc1	lunapark
</s>
</p>
<p>
<s>
Cirkus	cirkus	k1gInSc1	cirkus
</s>
</p>
<p>
<s>
Varieté	varieté	k1gNnSc1	varieté
</s>
</p>
<p>
<s>
Kabaret	kabaret	k1gInSc1	kabaret
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ruské	ruský	k2eAgFnSc2d1	ruská
kolo	kolo	k1gNnSc4	kolo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
