<s>
Egyptské	egyptský	k2eAgFnPc1d1	egyptská
pyramidy	pyramida	k1gFnPc1	pyramida
(	(	kIx(	(
<g/>
v	v	k7c6	v
egyptštině	egyptština	k1gFnSc6	egyptština
<g/>
:	:	kIx,	:
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mer	mer	k?	mer
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
stavby	stavba	k1gFnPc1	stavba
typu	typ	k1gInSc2	typ
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
budovány	budovat	k5eAaImNgFnP	budovat
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
panovníka	panovník	k1gMnSc2	panovník
Džosera	Džosero	k1gNnSc2	Džosero
ze	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
dynastie	dynastie	k1gFnSc2	dynastie
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
prvního	první	k4xOgNnSc2	první
krále	král	k1gMnSc2	král
18	[number]	k4	18
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
Ahmose	Ahmosa	k1gFnSc3	Ahmosa
I.	I.	kA	I.
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
po	po	k7c6	po
<g />
.	.	kIx.	.
</s>
<s>
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
přibližně	přibližně	k6eAd1	přibližně
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hrobky	hrobek	k1gInPc1	hrobek
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
kenotafy	kenotaf	k1gInPc1	kenotaf
<g/>
)	)	kIx)	)
králů	král	k1gMnPc2	král
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
některých	některý	k3yIgFnPc2	některý
jejich	jejich	k3xOp3gFnPc2	jejich
významných	významný	k2eAgFnPc2d1	významná
manželek	manželka	k1gFnPc2	manželka
a	a	k8xC	a
matek	matka	k1gFnPc2	matka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
prozatím	prozatím	k6eAd1	prozatím
neprokázané	prokázaný	k2eNgFnSc2d1	neprokázaná
domněnky	domněnka	k1gFnSc2	domněnka
rakouského	rakouský	k2eAgMnSc2d1	rakouský
egyptologa	egyptolog	k1gMnSc2	egyptolog
Petera	Peter	k1gMnSc2	Peter
Jánosiho	Jánosi	k1gMnSc2	Jánosi
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
dynastie	dynastie	k1gFnSc2	dynastie
hrobka	hrobka	k1gFnSc1	hrobka
královny	královna	k1gFnSc2	královna
podobu	podoba	k1gFnSc4	podoba
pyramidy	pyramida	k1gFnSc2	pyramida
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc3	její
syn	syn	k1gMnSc1	syn
panovníkem	panovník	k1gMnSc7	panovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
pyramid	pyramida	k1gFnPc2	pyramida
byla	být	k5eAaImAgFnS	být
nikoli	nikoli	k9	nikoli
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
izolovanou	izolovaný	k2eAgFnSc7d1	izolovaná
stavbou	stavba	k1gFnSc7	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
součástí	součást	k1gFnSc7	součást
celého	celý	k2eAgInSc2d1	celý
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
plánu	plán	k1gInSc2	plán
funkčně	funkčně	k6eAd1	funkčně
(	(	kIx(	(
<g/>
především	především	k9	především
nábožensky	nábožensky	k6eAd1	nábožensky
<g/>
)	)	kIx)	)
propojených	propojený	k2eAgFnPc2d1	propojená
budov	budova	k1gFnPc2	budova
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
přesnější	přesný	k2eAgMnSc1d2	přesnější
hovořit	hovořit	k5eAaImF	hovořit
ne	ne	k9	ne
o	o	k7c6	o
pyramidách	pyramida	k1gFnPc6	pyramida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
pyramidových	pyramidový	k2eAgInPc6d1	pyramidový
komplexech	komplex	k1gInPc6	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
standardní	standardní	k2eAgFnSc7d1	standardní
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
pyramidy	pyramida	k1gFnSc2	pyramida
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
údolní	údolní	k2eAgInSc1d1	údolní
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
vzestupná	vzestupný	k2eAgFnSc1d1	vzestupná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
zádušní	zádušní	k2eAgInSc1d1	zádušní
chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
satelitní	satelitní	k2eAgFnSc1d1	satelitní
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stavby	stavba	k1gFnPc1	stavba
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
sebe	se	k3xPyFc2	se
mohou	moct	k5eAaImIp3nP	moct
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
architektonických	architektonický	k2eAgFnPc6d1	architektonická
jednotlivostech	jednotlivost	k1gFnPc6	jednotlivost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
koncepčně	koncepčně	k6eAd1	koncepčně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zpravidla	zpravidla	k6eAd1	zpravidla
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
proměně	proměna	k1gFnSc6	proměna
náboženských	náboženský	k2eAgFnPc2d1	náboženská
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
zejména	zejména	k9	zejména
za	za	k7c2	za
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
vznikaly	vznikat	k5eAaImAgFnP	vznikat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
nekropole	nekropole	k1gFnPc1	nekropole
s	s	k7c7	s
hrobkami	hrobka	k1gFnPc7	hrobka
členů	člen	k1gMnPc2	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
dvorních	dvorní	k2eAgMnPc2d1	dvorní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
příslušníků	příslušník	k1gMnPc2	příslušník
společenské	společenský	k2eAgFnSc2d1	společenská
elity	elita	k1gFnSc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
motiv	motiv	k1gInSc1	motiv
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
pohřební	pohřební	k2eAgFnSc2d1	pohřební
architektury	architektura	k1gFnSc2	architektura
soukromých	soukromý	k2eAgFnPc2d1	soukromá
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Pyramidy	pyramida	k1gFnPc1	pyramida
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
ve	v	k7c6	v
Starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
panovníka	panovník	k1gMnSc2	panovník
Džosera	Džoser	k1gMnSc2	Džoser
(	(	kIx(	(
<g/>
Džoserova	Džoserův	k2eAgFnSc1d1	Džoserova
pyramida	pyramida	k1gFnSc1	pyramida
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
provinční	provinční	k2eAgFnPc1d1	provinční
stupňovité	stupňovitý	k2eAgFnPc1d1	stupňovitá
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
)	)	kIx)	)
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Ahmose	Ahmosa	k1gFnSc6	Ahmosa
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Stavěly	stavět	k5eAaImAgFnP	stavět
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Používaly	používat	k5eAaImAgInP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
hrobky	hrobka	k1gFnPc1	hrobka
různých	různý	k2eAgMnPc2d1	různý
králů	král	k1gMnPc2	král
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
významných	významný	k2eAgFnPc2d1	významná
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
celého	celý	k2eAgInSc2d1	celý
pyramidového	pyramidový	k2eAgInSc2d1	pyramidový
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stavby	stavba	k1gFnPc1	stavba
mohou	moct	k5eAaImIp3nP	moct
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
hypotéz	hypotéza	k1gFnPc2	hypotéza
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Egypťané	Egypťan	k1gMnPc1	Egypťan
stavěli	stavět	k5eAaImAgMnP	stavět
pyramidy	pyramid	k1gInPc4	pyramid
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgInPc1d1	velký
kamenné	kamenný	k2eAgInPc1d1	kamenný
bloky	blok	k1gInPc1	blok
byly	být	k5eAaImAgInP	být
měděnými	měděný	k2eAgInPc7d1	měděný
nástroji	nástroj	k1gInPc7	nástroj
vytesány	vytesán	k2eAgInPc4d1	vytesán
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přesunuty	přesunut	k2eAgInPc1d1	přesunut
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
často	často	k6eAd1	často
lodí	loď	k1gFnPc2	loď
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
vyzdviženy	vyzdvižen	k2eAgInPc4d1	vyzdvižen
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
dnešní	dnešní	k2eAgFnSc2d1	dnešní
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
badatelé	badatel	k1gMnPc1	badatel
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stavby	stavba	k1gFnPc1	stavba
neobešly	obešt	k5eNaBmAgFnP	obešt
bez	bez	k7c2	bez
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
ale	ale	k9	ale
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramidy	pyramid	k1gInPc1	pyramid
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
desítkami	desítka	k1gFnPc7	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
zručných	zručný	k2eAgMnPc2d1	zručný
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
brali	brát	k5eAaImAgMnP	brát
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
doby	doba	k1gFnSc2	doba
stavitelů	stavitel	k1gMnPc2	stavitel
pyramid	pyramid	k1gInSc1	pyramid
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
vláda	vláda	k1gFnSc1	vláda
4	[number]	k4	4
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nejznámější	známý	k2eAgInPc1d3	nejznámější
z	z	k7c2	z
pyramid	pyramida	k1gFnPc2	pyramida
–	–	k?	–
Chufuova	Chufuův	k2eAgInSc2d1	Chufuův
<g/>
,	,	kIx,	,
Rachefova	Rachefův	k2eAgInSc2d1	Rachefův
a	a	k8xC	a
Menkaureova	Menkaureův	k2eAgInSc2d1	Menkaureův
v	v	k7c4	v
Gíze	Gíze	k1gInSc4	Gíze
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pokládány	pokládat	k5eAaImNgInP	pokládat
za	za	k7c4	za
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
zachovaný	zachovaný	k2eAgMnSc1d1	zachovaný
<g/>
)	)	kIx)	)
z	z	k7c2	z
tradičních	tradiční	k2eAgInPc2d1	tradiční
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
ony	onen	k3xDgInPc4	onen
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
obzvláště	obzvláště	k6eAd1	obzvláště
pyramida	pyramida	k1gFnSc1	pyramida
panovníka	panovník	k1gMnSc2	panovník
Chufua	Chufuus	k1gMnSc2	Chufuus
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
někdy	někdy	k6eAd1	někdy
jako	jako	k8xS	jako
Velká	velký	k2eAgFnSc1d1	velká
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
jedinečná	jedinečný	k2eAgNnPc4d1	jedinečné
a	a	k8xC	a
odlišující	odlišující	k2eAgNnSc4d1	odlišující
se	se	k3xPyFc4	se
ostatních	ostatní	k2eAgFnPc2d1	ostatní
staveb	stavba	k1gFnPc2	stavba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
předmětem	předmět	k1gInSc7	předmět
pozornosti	pozornost	k1gFnSc2	pozornost
autorů	autor	k1gMnPc2	autor
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pyramidy	pyramida	k1gFnPc1	pyramida
jsou	být	k5eAaImIp3nP	být
obrovské	obrovský	k2eAgFnPc1d1	obrovská
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Badatelé	badatel	k1gMnPc1	badatel
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kolem	kolem	k6eAd1	kolem
sta	sto	k4xCgNnPc1	sto
<g/>
.	.	kIx.	.
</s>
<s>
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
komory	komora	k1gFnPc4	komora
<g/>
:	:	kIx,	:
Královu	Králův	k2eAgFnSc4d1	Králova
a	a	k8xC	a
Královninu	královnin	k2eAgFnSc4d1	Královnina
síň	síň	k1gFnSc4	síň
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Velkou	velký	k2eAgFnSc4d1	velká
Galerii	galerie	k1gFnSc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tajná	tajný	k2eAgFnSc1d1	tajná
chodba	chodba	k1gFnSc1	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
pyramidami	pyramida	k1gFnPc7	pyramida
jsou	být	k5eAaImIp3nP	být
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
a	a	k8xC	a
Rachefova	Rachefův	k2eAgFnSc1d1	Rachefova
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
poblíž	poblíž	k6eAd1	poblíž
Gízy	Gíz	k2eAgFnPc1d1	Gíza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
Rachefova	Rachefův	k2eAgFnSc1d1	Rachefova
pyramida	pyramida	k1gFnSc1	pyramida
obložena	obložit	k5eAaPmNgFnS	obložit
bílým	bílý	k2eAgInSc7d1	bílý
vápencem	vápenec	k1gInSc7	vápenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
odrážely	odrážet	k5eAaImAgInP	odrážet
jasné	jasný	k2eAgInPc1d1	jasný
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
skutečně	skutečně	k6eAd1	skutečně
krásná	krásný	k2eAgFnSc1d1	krásná
podívaná	podívaná	k1gFnSc1	podívaná
<g/>
.	.	kIx.	.
</s>
<s>
Vápenec	vápenec	k1gInSc1	vápenec
však	však	k9	však
bohužel	bohužel	k9	bohužel
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
století	století	k1gNnSc2	století
rozebrali	rozebrat	k5eAaPmAgMnP	rozebrat
Egypťané	Egypťan	k1gMnPc1	Egypťan
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
stavby	stavba	k1gFnPc4	stavba
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
pyramida	pyramida	k1gFnSc1	pyramida
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
povrch	povrch	k1gInSc1	povrch
takového	takový	k3xDgInSc2	takový
rozměru	rozměr	k1gInSc2	rozměr
jako	jako	k9	jako
13	[number]	k4	13
fotbalových	fotbalový	k2eAgNnPc2d1	fotbalové
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
současná	současný	k2eAgFnSc1d1	současná
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
137,5	[number]	k4	137,5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Sfinga	sfinga	k1gFnSc1	sfinga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
pyramidami	pyramida	k1gFnPc7	pyramida
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
má	mít	k5eAaImIp3nS	mít
hlídat	hlídat	k5eAaImF	hlídat
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tří	tři	k4xCgFnPc2	tři
největších	veliký	k2eAgFnPc2d3	veliký
pyramid	pyramida	k1gFnPc2	pyramida
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgFnPc1	tři
malé	malá	k1gFnPc1	malá
tzv.	tzv.	kA	tzv.
satelitní	satelitní	k2eAgFnPc1d1	satelitní
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
hrobky	hrobka	k1gFnPc1	hrobka
manželek	manželka	k1gFnPc2	manželka
faraona	faraon	k1gMnSc4	faraon
Chufua	Chufuus	k1gMnSc4	Chufuus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rachefova	Rachefův	k2eAgFnSc1d1	Rachefova
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
optický	optický	k2eAgInSc4d1	optický
klam	klam	k1gInSc4	klam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Rachefova	Rachefův	k2eAgFnSc1d1	Rachefova
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšším	vysoký	k2eAgNnSc6d2	vyšší
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mastaba	mastaba	k1gFnSc1	mastaba
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
jako	jako	k9	jako
hrobka	hrobka	k1gFnSc1	hrobka
vládnoucích	vládnoucí	k2eAgFnPc2d1	vládnoucí
vrstev	vrstva	k1gFnPc2	vrstva
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
nízký	nízký	k2eAgInSc1d1	nízký
komolý	komolý	k2eAgInSc1d1	komolý
čtyřboký	čtyřboký	k2eAgInSc1d1	čtyřboký
jehlan	jehlan	k1gInSc1	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
údolní	údolní	k2eAgInSc4d1	údolní
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhala	probíhat	k5eAaImAgFnS	probíhat
mumifikace	mumifikace	k1gFnSc1	mumifikace
<g/>
,	,	kIx,	,
vzestupnou	vzestupný	k2eAgFnSc4d1	vzestupná
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
zádušní	zádušní	k2eAgInSc4d1	zádušní
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
obnovující	obnovující	k2eAgFnSc2d1	obnovující
síly	síla	k1gFnSc2	síla
panovníka	panovník	k1gMnSc2	panovník
(	(	kIx(	(
<g/>
vzestupná	vzestupný	k2eAgFnSc1d1	vzestupná
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stoupala	stoupat	k5eAaImAgFnS	stoupat
k	k	k7c3	k
pyramidě	pyramida	k1gFnSc3	pyramida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
Džoserova	Džoserův	k2eAgFnSc1d1	Džoserova
v	v	k7c4	v
Sákkaře	Sákkař	k1gMnPc4	Sákkař
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
je	být	k5eAaImIp3nS	být
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
je	být	k5eAaImIp3nS	být
Ahmoseho	Ahmose	k1gMnSc4	Ahmose
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
bohužel	bohužel	k6eAd1	bohužel
zřítila	zřítit	k5eAaPmAgFnS	zřítit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
stojící	stojící	k2eAgFnSc1d1	stojící
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
Chendžerova	Chendžerův	k2eAgFnSc1d1	Chendžerův
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
Menkaureova	Menkaureův	k2eAgFnSc1d1	Menkaureova
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
egyptských	egyptský	k2eAgFnPc2d1	egyptská
pyramid	pyramida	k1gFnPc2	pyramida
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
:	:	kIx,	:
některé	některý	k3yIgNnSc1	některý
jsou	být	k5eAaImIp3nP	být
doloženy	doložen	k2eAgInPc1d1	doložen
písemnými	písemný	k2eAgInPc7d1	písemný
prameny	pramen	k1gInPc7	pramen
a	a	k8xC	a
nebyly	být	k5eNaImAgInP	být
doposud	doposud	k6eAd1	doposud
objeveny	objevit	k5eAaPmNgInP	objevit
či	či	k8xC	či
identifikovány	identifikován	k2eAgInPc1d1	identifikován
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
už	už	k9	už
zcela	zcela	k6eAd1	zcela
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
;	;	kIx,	;
badatelé	badatel	k1gMnPc1	badatel
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jich	on	k3xPp3gMnPc2	on
existuje	existovat	k5eAaImIp3nS	existovat
přibližně	přibližně	k6eAd1	přibližně
sto	sto	k4xCgNnSc1	sto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
orientačního	orientační	k2eAgInSc2d1	orientační
počtu	počet	k1gInSc2	počet
jsou	být	k5eAaImIp3nP	být
zahrnovány	zahrnovat	k5eAaImNgFnP	zahrnovat
jak	jak	k6eAd1	jak
samotné	samotný	k2eAgFnPc1d1	samotná
pyramidy	pyramida	k1gFnPc1	pyramida
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
satelitních	satelitní	k2eAgFnPc2d1	satelitní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
byly	být	k5eAaImAgFnP	být
jako	jako	k8xS	jako
pyramidy	pyramida	k1gFnPc1	pyramida
plánovány	plánovat	k5eAaImNgFnP	plánovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jiné	jiná	k1gFnSc2	jiná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xC	jako
mastaba	mastaba	k1gFnSc1	mastaba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Raneferefovy	Raneferefův	k2eAgFnSc2d1	Raneferefova
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgInPc1d1	královský
pyramidové	pyramidový	k2eAgInPc1d1	pyramidový
komplexy	komplex	k1gInPc1	komplex
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
měly	mít	k5eAaImAgInP	mít
nejen	nejen	k6eAd1	nejen
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
náboženský	náboženský	k2eAgInSc1d1	náboženský
a	a	k8xC	a
ideologický	ideologický	k2eAgInSc1d1	ideologický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
staveb	stavba	k1gFnPc2	stavba
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
existenci	existence	k1gFnSc4	existence
přísně	přísně	k6eAd1	přísně
organizované	organizovaný	k2eAgFnPc4d1	organizovaná
byrokracie	byrokracie	k1gFnPc4	byrokracie
ovládající	ovládající	k2eAgFnPc4d1	ovládající
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
centra	centrum	k1gNnSc2	centrum
všechny	všechen	k3xTgInPc4	všechen
zdroje	zdroj	k1gInPc4	zdroj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
pyramidových	pyramidový	k2eAgInPc6d1	pyramidový
projektech	projekt	k1gInPc6	projekt
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
zainteresována	zainteresovat	k5eAaPmNgFnS	zainteresovat
až	až	k6eAd1	až
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zde	zde	k6eAd1	zde
provozovaným	provozovaný	k2eAgInSc7d1	provozovaný
posmrtným	posmrtný	k2eAgInSc7d1	posmrtný
kultem	kult	k1gInSc7	kult
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
vznikala	vznikat	k5eAaImAgNnP	vznikat
tzv.	tzv.	kA	tzv.
pyramidová	pyramidový	k2eAgNnPc4d1	pyramidové
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
a	a	k8xC	a
personální	personální	k2eAgNnSc1d1	personální
zázemí	zázemí	k1gNnSc1	zázemí
kultu	kult	k1gInSc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zánikem	zánik	k1gInSc7	zánik
staroegyptské	staroegyptský	k2eAgFnSc2d1	staroegyptská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
hieroglyfického	hieroglyfický	k2eAgNnSc2d1	hieroglyfické
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
vytratilo	vytratit	k5eAaPmAgNnS	vytratit
i	i	k9	i
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
původním	původní	k2eAgInSc6d1	původní
účelu	účel	k1gInSc6	účel
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
se	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
tyto	tento	k3xDgFnPc4	tento
stavby	stavba	k1gFnPc4	stavba
začlenit	začlenit	k5eAaPmF	začlenit
a	a	k8xC	a
interpretovat	interpretovat	k5eAaBmF	interpretovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
určitého	určitý	k2eAgNnSc2d1	určité
historického	historický	k2eAgNnSc2d1	historické
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
biblického	biblický	k2eAgInSc2d1	biblický
<g/>
)	)	kIx)	)
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k6eAd1	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
živý	živý	k2eAgInSc1d1	živý
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
proud	proud	k1gInSc1	proud
reflektující	reflektující	k2eAgFnSc4d1	reflektující
staroegyptskou	staroegyptský	k2eAgFnSc4d1	staroegyptská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
zejména	zejména	k9	zejména
pyramidy	pyramida	k1gFnPc1	pyramida
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
označení	označení	k1gNnSc1	označení
pyramidologie	pyramidologie	k1gFnSc2	pyramidologie
<g/>
)	)	kIx)	)
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
hermetismu	hermetismus	k1gInSc2	hermetismus
a	a	k8xC	a
esoteriky	esoterik	k1gMnPc4	esoterik
jako	jako	k8xS	jako
nositele	nositel	k1gMnPc4	nositel
odvěké	odvěký	k2eAgFnSc2d1	odvěká
moudrosti	moudrost	k1gFnSc2	moudrost
či	či	k8xC	či
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
tajemství	tajemství	k1gNnPc2	tajemství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
"	"	kIx"	"
<g/>
zašifrována	zašifrován	k2eAgFnSc1d1	zašifrována
<g/>
"	"	kIx"	"
a	a	k8xC	a
která	který	k3yRgFnSc1	který
třeba	třeba	k6eAd1	třeba
"	"	kIx"	"
<g/>
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejnovější	nový	k2eAgFnSc6d3	nejnovější
době	doba	k1gFnSc6	doba
bývají	bývat	k5eAaImIp3nP	bývat
nejrůznějšími	různý	k2eAgInPc7d3	nejrůznější
způsoby	způsob	k1gInPc7	způsob
dávány	dáván	k2eAgInPc4d1	dáván
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
údajnou	údajný	k2eAgFnSc7d1	údajná
existencí	existence	k1gFnSc7	existence
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
civilizací	civilizace	k1gFnPc2	civilizace
nebo	nebo	k8xC	nebo
s	s	k7c7	s
dávnými	dávný	k2eAgNnPc7d1	dávné
a	a	k8xC	a
současné	současný	k2eAgFnSc6d1	současná
vědě	věda	k1gFnSc6	věda
neznámými	známý	k2eNgFnPc7d1	neznámá
lidskými	lidský	k2eAgFnPc7d1	lidská
kulturami	kultura	k1gFnPc7	kultura
disponujícími	disponující	k2eAgFnPc7d1	disponující
pokročilými	pokročilý	k2eAgFnPc7d1	pokročilá
technologiemi	technologie	k1gFnPc7	technologie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítají	odmítat	k5eAaImIp3nP	odmítat
akceptovat	akceptovat	k5eAaBmF	akceptovat
nejen	nejen	k6eAd1	nejen
primární	primární	k2eAgInSc4d1	primární
účel	účel	k1gInSc4	účel
pyramid	pyramida	k1gFnPc2	pyramida
jako	jako	k8xS	jako
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
datování	datování	k1gNnSc2	datování
egyptologií	egyptologie	k1gFnPc2	egyptologie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
posouvají	posouvat	k5eAaImIp3nP	posouvat
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
představy	představ	k1gInPc1	představ
ovšem	ovšem	k9	ovšem
stojí	stát	k5eAaImIp3nP	stát
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
egyptologií	egyptologie	k1gFnSc7	egyptologie
akceptovány	akceptován	k2eAgInPc1d1	akceptován
<g/>
.	.	kIx.	.
</s>
