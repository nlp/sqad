<p>
<s>
Wolf	Wolf	k1gMnSc1	Wolf
Hoffman	Hoffman	k1gMnSc1	Hoffman
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1959	[number]	k4	1959
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
,	,	kIx,	,
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
německé	německý	k2eAgFnSc2d1	německá
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Accept	Accepta	k1gFnPc2	Accepta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jeho	jeho	k3xOp3gNnSc1	jeho
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
Classical	Classical	k1gFnSc2	Classical
s	s	k7c7	s
rockovými	rockový	k2eAgFnPc7d1	rocková
verzemi	verze	k1gFnPc7	verze
klasických	klasický	k2eAgFnPc2d1	klasická
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
druhé	druhý	k4xOgNnSc1	druhý
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Headbangers	Headbangers	k1gInSc4	Headbangers
Symphony	Symphona	k1gFnSc2	Symphona
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
člena	člen	k1gMnSc2	člen
skupiny	skupina	k1gFnSc2	skupina
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gFnSc1	Row
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
Bring	Bring	k1gMnSc1	Bring
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
Bach	Bacha	k1gFnPc2	Bacha
Alive	Aliev	k1gFnSc2	Aliev
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
skupiny	skupina	k1gFnSc2	skupina
Accept	Accepta	k1gFnPc2	Accepta
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Wolf	Wolf	k1gMnSc1	Wolf
Hoffman	Hoffman	k1gMnSc1	Hoffman
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
profesionální	profesionální	k2eAgMnSc1d1	profesionální
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Nashville	Nashvilla	k1gFnSc6	Nashvilla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Gaby	Gaba	k1gFnPc4	Gaba
byla	být	k5eAaImAgFnS	být
manažerkou	manažerka	k1gFnSc7	manažerka
Accept	Accepta	k1gFnPc2	Accepta
a	a	k8xC	a
napsala	napsat	k5eAaPmAgFnS	napsat
i	i	k9	i
několik	několik	k4yIc4	několik
textů	text	k1gInPc2	text
k	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
písním	píseň	k1gFnPc3	píseň
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Deaffy	Deaff	k1gInPc4	Deaff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sólová	sólový	k2eAgFnSc1d1	sólová
diskografie	diskografie	k1gFnSc1	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Classical	Classicat	k5eAaPmAgMnS	Classicat
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Headbangers	Headbangers	k1gInSc1	Headbangers
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wolf	Wolf	k1gMnSc1	Wolf
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wolf	Wolf	k1gMnSc1	Wolf
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
