<s>
Euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
€	€	k?	€
<g/>
,	,	kIx,	,
měnový	měnový	k2eAgInSc1d1	měnový
kód	kód	k1gInSc1	kód
ISO	ISO	kA	ISO
4217	[number]	k4	4217
<g/>
:	:	kIx,	:
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měna	měna	k1gFnSc1	měna
eurozóny	eurozóna	k1gFnSc2	eurozóna
a	a	k8xC	a
po	po	k7c6	po
americkém	americký	k2eAgInSc6d1	americký
dolaru	dolar	k1gInSc6	dolar
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
)	)	kIx)	)
druhý	druhý	k4xOgMnSc1	druhý
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
reprezentant	reprezentant	k1gMnSc1	reprezentant
ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
měnovém	měnový	k2eAgInSc6d1	měnový
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
eurozóny	eurozóna	k1gFnSc2	eurozóna
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
Evropskou	evropský	k2eAgFnSc7d1	Evropská
centrální	centrální	k2eAgFnSc7d1	centrální
bankou	banka	k1gFnSc7	banka
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
platidlem	platidlo	k1gNnSc7	platidlo
v	v	k7c4	v
19	[number]	k4	19
z	z	k7c2	z
28	[number]	k4	28
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
šesti	šest	k4xCc6	šest
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
mimo	mimo	k7c4	mimo
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
také	také	k9	také
Česko	Česko	k1gNnSc1	Česko
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
přijmout	přijmout	k5eAaPmF	přijmout
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
evropskou	evropský	k2eAgFnSc4d1	Evropská
měnu	měna	k1gFnSc4	měna
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
maastrichtských	maastrichtský	k2eAgNnPc2d1	maastrichtské
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
zda	zda	k8xS	zda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kdy	kdy	k6eAd1	kdy
euro	euro	k1gNnSc1	euro
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
předcházející	předcházející	k2eAgInSc4d1	předcházející
příklad	příklad	k1gInSc4	příklad
vytvoření	vytvoření	k1gNnSc2	vytvoření
evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
bývá	bývat	k5eAaImIp3nS	bývat
považována	považován	k2eAgFnSc1d1	považována
Latinská	latinský	k2eAgFnSc1d1	Latinská
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
Belgické	belgický	k2eAgNnSc1d1	Belgické
království	království	k1gNnSc1	království
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Druhým	druhý	k4xOgNnSc7	druhý
Francouzským	francouzský	k2eAgNnSc7d1	francouzské
císařstvím	císařství	k1gNnSc7	císařství
<g/>
,	,	kIx,	,
Italským	italský	k2eAgNnSc7d1	italské
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
Řecké	řecký	k2eAgNnSc1d1	řecké
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Latinské	latinský	k2eAgFnSc2d1	Latinská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
nepřímo	přímo	k6eNd1	přímo
zapojily	zapojit	k5eAaPmAgInP	zapojit
i	i	k9	i
Španělské	španělský	k2eAgNnSc1d1	španělské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Rumunské	rumunský	k2eAgNnSc1d1	rumunské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Srbské	srbský	k2eAgNnSc1d1	srbské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
a	a	k8xC	a
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
atd.	atd.	kA	atd.
Unie	unie	k1gFnSc1	unie
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
stanovení	stanovení	k1gNnSc6	stanovení
obsahu	obsah	k1gInSc2	obsah
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
minci	mince	k1gFnSc6	mince
každé	každý	k3xTgFnSc2	každý
členské	členský	k2eAgFnSc2d1	členská
národní	národní	k2eAgFnSc2d1	národní
měny	měna	k1gFnSc2	měna
na	na	k7c4	na
4,5	[number]	k4	4,5
gramu	gram	k1gInSc2	gram
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obsahu	obsah	k1gInSc2	obsah
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
minci	mince	k1gFnSc6	mince
každé	každý	k3xTgFnSc2	každý
členské	členský	k2eAgFnSc2d1	členská
národní	národní	k2eAgFnSc2d1	národní
měny	měna	k1gFnSc2	měna
na	na	k7c4	na
0,290322	[number]	k4	0,290322
gramu	gram	k1gInSc2	gram
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
volná	volný	k2eAgFnSc1d1	volná
směnitelnost	směnitelnost	k1gFnSc1	směnitelnost
všech	všecek	k3xTgFnPc2	všecek
členských	členský	k2eAgFnPc2d1	členská
měn	měna	k1gFnPc2	měna
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
byla	být	k5eAaImAgFnS	být
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
Skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
devizové	devizový	k2eAgFnSc6d1	devizová
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnSc6d1	virtuální
<g/>
)	)	kIx)	)
podobě	podoba	k1gFnSc6	podoba
platit	platit	k5eAaImF	platit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ve	v	k7c6	v
valutové	valutový	k2eAgFnSc6d1	valutová
podobě	podoba	k1gFnSc6	podoba
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
a	a	k8xC	a
tak	tak	k6eAd1	tak
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
dříve	dříve	k6eAd2	dříve
platné	platný	k2eAgFnPc4d1	platná
měny	měna	k1gFnPc4	měna
používané	používaný	k2eAgFnPc4d1	používaná
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
košovou	košový	k2eAgFnSc4d1	košová
jednotku	jednotka	k1gFnSc4	jednotka
ECU	ECU	kA	ECU
v	v	k7c6	v
symbolickém	symbolický	k2eAgInSc6d1	symbolický
kurzu	kurz	k1gInSc6	kurz
1	[number]	k4	1
EUR	euro	k1gNnPc2	euro
=	=	kIx~	=
1	[number]	k4	1
ECU	ECU	kA	ECU
<g/>
.	.	kIx.	.
</s>
<s>
Výchozí	výchozí	k2eAgFnSc1d1	výchozí
externí	externí	k2eAgFnSc1d1	externí
hodnota	hodnota	k1gFnSc1	hodnota
eura	euro	k1gNnSc2	euro
vůči	vůči	k7c3	vůči
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
činila	činit	k5eAaImAgFnS	činit
1	[number]	k4	1
EUR	euro	k1gNnPc2	euro
=	=	kIx~	=
1,178	[number]	k4	1,178
<g/>
9	[number]	k4	9
USD	USD	kA	USD
a	a	k8xC	a
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
propočtu	propočet	k1gInSc2	propočet
kurzu	kurz	k1gInSc2	kurz
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
ECU	ECU	kA	ECU
v	v	k7c4	v
poslední	poslední	k2eAgInSc4d1	poslední
obchodní	obchodní	k2eAgInSc4d1	obchodní
den	den	k1gInSc4	den
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
eura	euro	k1gNnSc2	euro
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
svého	svůj	k3xOyFgInSc2	svůj
historicky	historicky	k6eAd1	historicky
nejsilnějšího	silný	k2eAgInSc2d3	nejsilnější
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
představoval	představovat	k5eAaImAgInS	představovat
1,599	[number]	k4	1,599
<g/>
0	[number]	k4	0
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
za	za	k7c4	za
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c6	na
devizovém	devizový	k2eAgInSc6d1	devizový
trhu	trh	k1gInSc6	trh
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
propad	propad	k1gInSc4	propad
pak	pak	k6eAd1	pak
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
při	při	k7c6	při
kurzu	kurz	k1gInSc6	kurz
0,8252	[number]	k4	0,8252
USD	USD	kA	USD
za	za	k7c4	za
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
kurz	kurz	k1gInSc1	kurz
eura	euro	k1gNnSc2	euro
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
1,1	[number]	k4	1,1
USD	USD	kA	USD
za	za	k7c4	za
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
eura	euro	k1gNnSc2	euro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
podíl	podíl	k1gInSc4	podíl
eura	euro	k1gNnSc2	euro
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
devizových	devizový	k2eAgFnPc6d1	devizová
rezervách	rezerva	k1gFnPc6	rezerva
a	a	k8xC	a
euro	euro	k1gNnSc1	euro
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
světovou	světový	k2eAgFnSc7d1	světová
rezervní	rezervní	k2eAgFnSc7d1	rezervní
měnou	měna	k1gFnSc7	měna
rovnocennou	rovnocenný	k2eAgFnSc4d1	rovnocenná
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
předpověď	předpověď	k1gFnSc1	předpověď
se	se	k3xPyFc4	se
však	však	k9	však
nenaplnila	naplnit	k5eNaPmAgFnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
eura	euro	k1gNnSc2	euro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podíl	podíl	k1gInSc1	podíl
eura	euro	k1gNnSc2	euro
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
devizových	devizový	k2eAgFnPc6d1	devizová
rezervách	rezerva	k1gFnPc6	rezerva
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
ze	z	k7c2	z
17,9	[number]	k4	17,9
%	%	kIx~	%
na	na	k7c4	na
27,6	[number]	k4	27,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
ale	ale	k8xC	ale
podíl	podíl	k1gInSc1	podíl
eura	euro	k1gNnSc2	euro
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
devizových	devizový	k2eAgFnPc6d1	devizová
rezervách	rezerva	k1gFnPc6	rezerva
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
2015	[number]	k4	2015
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
pouze	pouze	k6eAd1	pouze
20,5	[number]	k4	20,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
tvořil	tvořit	k5eAaImAgInS	tvořit
71	[number]	k4	71
%	%	kIx~	%
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
62,1	[number]	k4	62,1
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
2015	[number]	k4	2015
63,8	[number]	k4	63,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
euromince	eurominec	k1gInSc2	eurominec
a	a	k8xC	a
eurobankovky	eurobankovka	k1gFnSc2	eurobankovka
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
euro	euro	k1gNnSc1	euro
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
100	[number]	k4	100
centů	cent	k1gInPc2	cent
(	(	kIx(	(
<g/>
eurocentů	eurocent	k1gInPc2	eurocent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Euromince	Eurominko	k6eAd1	Eurominko
mají	mít	k5eAaImIp3nP	mít
nominální	nominální	k2eAgFnPc4d1	nominální
hodnoty	hodnota	k1gFnPc4	hodnota
1	[number]	k4	1
c	c	k0	c
<g/>
,	,	kIx,	,
2	[number]	k4	2
c	c	k0	c
<g/>
,	,	kIx,	,
5	[number]	k4	5
c	c	k0	c
<g/>
,	,	kIx,	,
10	[number]	k4	10
c	c	k0	c
<g/>
,	,	kIx,	,
20	[number]	k4	20
c	c	k0	c
<g/>
,	,	kIx,	,
50	[number]	k4	50
c	c	k0	c
<g/>
,	,	kIx,	,
1	[number]	k4	1
€	€	k?	€
a	a	k8xC	a
2	[number]	k4	2
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
se	se	k3xPyFc4	se
jednocentové	jednocentový	k2eAgFnSc2d1	jednocentová
a	a	k8xC	a
dvoucentové	dvoucentový	k2eAgFnSc2d1	dvoucentová
mince	mince	k1gFnSc2	mince
běžně	běžně	k6eAd1	běžně
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
razí	razit	k5eAaImIp3nP	razit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
sběratele	sběratel	k1gMnPc4	sběratel
<g/>
.	.	kIx.	.
</s>
<s>
Euromince	Eurominko	k6eAd1	Eurominko
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
nominálních	nominální	k2eAgFnPc2d1	nominální
hodnot	hodnota	k1gFnPc2	hodnota
mají	mít	k5eAaImIp3nP	mít
shodnou	shodný	k2eAgFnSc4d1	shodná
lícovou	lícový	k2eAgFnSc4d1	lícová
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
mapa	mapa	k1gFnSc1	mapa
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
národní	národní	k2eAgFnSc4d1	národní
rubovou	rubový	k2eAgFnSc4d1	rubová
stranu	strana	k1gFnSc4	strana
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
členský	členský	k2eAgInSc4d1	členský
stát	stát	k1gInSc4	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
(	(	kIx(	(
<g/>
podobný	podobný	k2eAgInSc1d1	podobný
systém	systém	k1gInSc1	systém
stejné	stejný	k2eAgFnSc2d1	stejná
lícové	lícový	k2eAgFnSc2d1	lícová
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
rubových	rubový	k2eAgFnPc2d1	rubová
stran	strana	k1gFnPc2	strana
mincí	mince	k1gFnPc2	mince
používá	používat	k5eAaImIp3nS	používat
kapverdské	kapverdský	k2eAgNnSc4d1	Kapverdské
escudo	escudo	k1gNnSc4	escudo
a	a	k8xC	a
CFP	CFP	kA	CFP
frank	frank	k1gInSc1	frank
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
euromince	eurominec	k1gInPc1	eurominec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používán	k2eAgInPc1d1	používán
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
členských	členský	k2eAgFnPc6d1	členská
zemích	zem	k1gFnPc6	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Eurobankovky	Eurobankovka	k1gFnPc1	Eurobankovka
mají	mít	k5eAaImIp3nP	mít
nominální	nominální	k2eAgFnPc1d1	nominální
hodnoty	hodnota	k1gFnPc1	hodnota
5	[number]	k4	5
€	€	k?	€
<g/>
,	,	kIx,	,
10	[number]	k4	10
€	€	k?	€
<g/>
,	,	kIx,	,
20	[number]	k4	20
€	€	k?	€
<g/>
,	,	kIx,	,
50	[number]	k4	50
€	€	k?	€
<g/>
,	,	kIx,	,
100	[number]	k4	100
€	€	k?	€
<g/>
,	,	kIx,	,
200	[number]	k4	200
€	€	k?	€
a	a	k8xC	a
500	[number]	k4	500
€	€	k?	€
<g/>
,	,	kIx,	,
bankovky	bankovka	k1gFnPc1	bankovka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
nominálních	nominální	k2eAgFnPc2d1	nominální
hodnot	hodnota	k1gFnPc2	hodnota
mají	mít	k5eAaImIp3nP	mít
design	design	k1gInSc4	design
na	na	k7c6	na
lícové	lícový	k2eAgFnSc6d1	lícová
i	i	k8xC	i
rubové	rubový	k2eAgFnSc6d1	rubová
straně	strana	k1gFnSc6	strana
shodný	shodný	k2eAgInSc1d1	shodný
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eurozóna	Eurozóna	k1gFnSc1	Eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
eurozóny	eurozóna	k1gFnSc2	eurozóna
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
oficiální	oficiální	k2eAgNnSc1d1	oficiální
platidlo	platidlo	k1gNnSc1	platidlo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nejvzdálenějších	vzdálený	k2eAgInPc6d3	nejvzdálenější
regionech	region	k1gInPc6	region
EU	EU	kA	EU
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gFnSc1	Guadeloupe
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gInSc1	Martinik
<g/>
,	,	kIx,	,
Réunion	Réunion	k1gInSc1	Réunion
<g/>
,	,	kIx,	,
Mayotte	Mayott	k1gMnSc5	Mayott
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Azory	azor	k1gInPc1	azor
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Kanárské	kanárský	k2eAgInPc1d1	kanárský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
používají	používat	k5eAaImIp3nP	používat
euro	euro	k1gNnSc4	euro
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
a	a	k8xC	a
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
členy	člen	k1gInPc4	člen
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
mají	mít	k5eAaImIp3nP	mít
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
eura	euro	k1gNnSc2	euro
uzavřeny	uzavřen	k2eAgFnPc1d1	uzavřena
dohody	dohoda	k1gFnPc1	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
jiné	jiná	k1gFnPc1	jiná
zavedly	zavést	k5eAaPmAgFnP	zavést
euro	euro	k1gNnSc4	euro
jednostranně	jednostranně	k6eAd1	jednostranně
bez	bez	k7c2	bez
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
Andorra	Andorra	k1gFnSc1	Andorra
používají	používat	k5eAaImIp3nP	používat
euro	euro	k1gNnSc4	euro
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Euro	euro	k1gNnSc1	euro
jako	jako	k8xS	jako
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
zavedly	zavést	k5eAaPmAgInP	zavést
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Monaka	Monako	k1gNnSc2	Monako
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
a	a	k8xC	a
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
)	)	kIx)	)
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
euro	euro	k1gNnSc4	euro
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
dříve	dříve	k6eAd2	dříve
obíhající	obíhající	k2eAgInSc4d1	obíhající
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
a	a	k8xC	a
monacký	monacký	k2eAgInSc4d1	monacký
frank	frank	k1gInSc4	frank
<g/>
,	,	kIx,	,
v	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
italskou	italský	k2eAgFnSc4d1	italská
liru	lira	k1gFnSc4	lira
a	a	k8xC	a
sanmarinskou	sanmarinský	k2eAgFnSc4d1	sanmarinská
liru	lira	k1gFnSc4	lira
a	a	k8xC	a
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
italskou	italský	k2eAgFnSc4d1	italská
liru	lira	k1gFnSc4	lira
a	a	k8xC	a
vatikánskou	vatikánský	k2eAgFnSc4d1	Vatikánská
liru	lira	k1gFnSc4	lira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
euro	euro	k1gNnSc1	euro
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
a	a	k8xC	a
španělskou	španělský	k2eAgFnSc4d1	španělská
pesetu	peseta	k1gFnSc4	peseta
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
emitují	emitovat	k5eAaBmIp3nP	emitovat
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
euromince	eurominec	k1gInPc4	eurominec
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
získala	získat	k5eAaPmAgFnS	získat
právo	právo	k1gNnSc4	právo
razit	razit	k5eAaImF	razit
andorrské	andorrský	k2eAgInPc4d1	andorrský
euromince	eurominec	k1gInPc4	eurominec
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
mince	mince	k1gFnSc1	mince
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Andorry	Andorra	k1gFnSc2	Andorra
dostaly	dostat	k5eAaPmAgInP	dostat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
s	s	k7c7	s
EU	EU	kA	EU
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
eura	euro	k1gNnSc2	euro
mají	mít	k5eAaImIp3nP	mít
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
rovněž	rovněž	k9	rovněž
francouzská	francouzský	k2eAgNnPc4d1	francouzské
zámořská	zámořský	k2eAgNnPc4d1	zámořské
společenství	společenství	k1gNnPc4	společenství
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc4	Miquelon
a	a	k8xC	a
Saint-Barthelémy	Saint-Barthelém	k1gInPc4	Saint-Barthelém
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc1	území
sice	sice	k8xC	sice
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
ani	ani	k9	ani
eurozóny	eurozón	k1gInPc1	eurozón
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
používají	používat	k5eAaImIp3nP	používat
euro	euro	k1gNnSc4	euro
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
povoleno	povolen	k2eAgNnSc1d1	povoleno
vydávat	vydávat	k5eAaPmF	vydávat
euromince	eurominko	k6eAd1	eurominko
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rubovou	rubový	k2eAgFnSc7d1	rubová
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jednostranně	jednostranně	k6eAd1	jednostranně
bez	bez	k7c2	bez
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
euro	euro	k1gNnSc4	euro
zavedla	zavést	k5eAaPmAgFnS	zavést
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
euro	euro	k1gNnSc1	euro
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
německou	německý	k2eAgFnSc4d1	německá
marku	marka	k1gFnSc4	marka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
eura	euro	k1gNnSc2	euro
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
používají	používat	k5eAaImIp3nP	používat
tuto	tento	k3xDgFnSc4	tento
měnu	měna	k1gFnSc4	měna
i	i	k8xC	i
britské	britský	k2eAgFnSc2d1	britská
suverénní	suverénní	k2eAgFnSc2d1	suverénní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
základny	základna	k1gFnSc2	základna
Akrotiri	Akrotir	k1gFnSc2	Akrotir
a	a	k8xC	a
Dhekelia	Dhekelium	k1gNnSc2	Dhekelium
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
formální	formální	k2eAgFnSc2d1	formální
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
EU	EU	kA	EU
se	se	k3xPyFc4	se
euro	euro	k1gNnSc1	euro
jako	jako	k8xC	jako
měna	měna	k1gFnSc1	měna
de	de	k?	de
iure	iure	k6eAd1	iure
používá	používat	k5eAaImIp3nS	používat
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
neobydlených	obydlený	k2eNgNnPc6d1	neobydlené
francouzských	francouzský	k2eAgNnPc6d1	francouzské
zámořských	zámořský	k2eAgNnPc6d1	zámořské
územích	území	k1gNnPc6	území
(	(	kIx(	(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
antarktická	antarktický	k2eAgNnPc1d1	antarktické
území	území	k1gNnPc1	území
a	a	k8xC	a
Clippertonův	Clippertonův	k2eAgInSc1d1	Clippertonův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
omezilo	omezit	k5eAaPmAgNnS	omezit
užívání	užívání	k1gNnSc1	užívání
domácí	domácí	k2eAgFnSc2d1	domácí
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
povolilo	povolit	k5eAaPmAgNnS	povolit
užívání	užívání	k1gNnSc4	užívání
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
měn	měna	k1gFnPc2	měna
včetně	včetně	k7c2	včetně
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Maastrichtská	maastrichtský	k2eAgNnPc1d1	maastrichtské
konvergenční	konvergenční	k2eAgNnPc1d1	konvergenční
kritéria	kritérion	k1gNnPc1	kritérion
jsou	být	k5eAaImIp3nP	být
souborem	soubor	k1gInSc7	soubor
pěti	pět	k4xCc2	pět
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
ukazatelů	ukazatel	k1gInPc2	ukazatel
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
musí	muset	k5eAaImIp3nP	muset
splnit	splnit	k5eAaPmF	splnit
stát	stát	k1gInSc4	stát
usilující	usilující	k2eAgInSc4d1	usilující
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
jednotné	jednotný	k2eAgFnSc2d1	jednotná
evropské	evropský	k2eAgFnSc2d1	Evropská
měny	měna	k1gFnSc2	měna
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kritéria	kritérion	k1gNnPc4	kritérion
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Kritérium	kritérium	k1gNnSc1	kritérium
cenové	cenový	k2eAgFnSc2d1	cenová
stability	stabilita	k1gFnSc2	stabilita
–	–	k?	–
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
musí	muset	k5eAaImIp3nS	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržitelnou	udržitelný	k2eAgFnSc4d1	udržitelná
cenovou	cenový	k2eAgFnSc4d1	cenová
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
sledovanou	sledovaný	k2eAgFnSc4d1	sledovaná
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
před	před	k7c7	před
šetřením	šetření	k1gNnSc7	šetření
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
procentního	procentní	k2eAgInSc2d1	procentní
bodu	bod	k1gInSc2	bod
míru	mír	k1gInSc2	mír
inflace	inflace	k1gFnSc2	inflace
tří	tři	k4xCgInPc2	tři
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cenové	cenový	k2eAgFnSc2d1	cenová
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Kritérium	kritérium	k1gNnSc1	kritérium
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
–	–	k?	–
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
před	před	k7c7	před
šetřením	šetření	k1gNnSc7	šetření
průměrná	průměrný	k2eAgFnSc1d1	průměrná
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nominální	nominální	k2eAgFnSc1d1	nominální
úroková	úrokový	k2eAgFnSc1d1	úroková
sazba	sazba	k1gFnSc1	sazba
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
nesmí	smět	k5eNaImIp3nS	smět
přesahovat	přesahovat	k5eAaImF	přesahovat
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
procentní	procentní	k2eAgInPc4d1	procentní
body	bod	k1gInPc4	bod
úrokovou	úrokový	k2eAgFnSc4d1	úroková
sazbu	sazba	k1gFnSc4	sazba
tří	tři	k4xCgInPc2	tři
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cenové	cenový	k2eAgFnSc2d1	cenová
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Kritérium	kritérium	k1gNnSc1	kritérium
deficitu	deficit	k1gInSc2	deficit
veřejných	veřejný	k2eAgInPc2d1	veřejný
rozpočtů	rozpočet	k1gInPc2	rozpočet
–	–	k?	–
poměr	poměr	k1gInSc1	poměr
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
nebo	nebo	k8xC	nebo
skutečného	skutečný	k2eAgInSc2d1	skutečný
schodku	schodek	k1gInSc2	schodek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
k	k	k7c3	k
hrubému	hrubý	k2eAgInSc3d1	hrubý
domácímu	domácí	k2eAgInSc3d1	domácí
produktu	produkt	k1gInSc3	produkt
v	v	k7c6	v
tržních	tržní	k2eAgFnPc6d1	tržní
cenách	cena	k1gFnPc6	cena
nepřekročí	překročit	k5eNaPmIp3nS	překročit
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kritérium	kritérium	k1gNnSc1	kritérium
hrubého	hrubý	k2eAgInSc2d1	hrubý
veřejného	veřejný	k2eAgInSc2d1	veřejný
dluhu	dluh	k1gInSc2	dluh
–	–	k?	–
poměr	poměr	k1gInSc1	poměr
hrubého	hrubý	k2eAgInSc2d1	hrubý
veřejného	veřejný	k2eAgInSc2d1	veřejný
dluhu	dluh	k1gInSc2	dluh
k	k	k7c3	k
hrubému	hrubý	k2eAgInSc3d1	hrubý
domácímu	domácí	k2eAgInSc3d1	domácí
produktu	produkt	k1gInSc3	produkt
v	v	k7c6	v
tržních	tržní	k2eAgFnPc6d1	tržní
cenách	cena	k1gFnPc6	cena
nepřekročí	překročit	k5eNaPmIp3nS	překročit
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
snižuje	snižovat	k5eAaImIp3nS	snižovat
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
uspokojivým	uspokojivý	k2eAgNnSc7d1	uspokojivé
tempem	tempo	k1gNnSc7	tempo
referenční	referenční	k2eAgMnSc1d1	referenční
60	[number]	k4	60
<g/>
%	%	kIx~	%
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Kritérium	kritérium	k1gNnSc1	kritérium
stability	stabilita	k1gFnSc2	stabilita
měnového	měnový	k2eAgInSc2d1	měnový
kurzu	kurz	k1gInSc2	kurz
–	–	k?	–
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
normální	normální	k2eAgNnPc4d1	normální
kurzová	kurzový	k2eAgNnPc4d1	kurzové
rozpětí	rozpětí	k1gNnPc4	rozpětí
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
mechanizmem	mechanizmus	k1gInSc7	mechanizmus
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
II	II	kA	II
(	(	kIx(	(
<g/>
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
)	)	kIx)	)
Evropského	evropský	k2eAgInSc2d1	evropský
měnového	měnový	k2eAgInSc2d1	měnový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgNnPc2	dva
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
před	před	k7c7	před
šetřením	šetření	k1gNnSc7	šetření
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnSc2	zem
zapojené	zapojený	k2eAgFnSc6d1	zapojená
do	do	k7c2	do
ERM	ERM	kA	ERM
II	II	kA	II
musí	muset	k5eAaImIp3nS	muset
kurzy	kurz	k1gInPc4	kurz
svých	svůj	k3xOyFgFnPc2	svůj
měn	měna	k1gFnPc2	měna
udržovat	udržovat	k5eAaImF	udržovat
v	v	k7c6	v
povoleném	povolený	k2eAgNnSc6d1	povolené
fluktuačním	fluktuační	k2eAgNnSc6d1	fluktuační
pásmu	pásmo	k1gNnSc6	pásmo
±	±	k?	±
15	[number]	k4	15
%	%	kIx~	%
od	od	k7c2	od
stanoveného	stanovený	k2eAgInSc2d1	stanovený
středního	střední	k2eAgInSc2d1	střední
kurzu	kurz	k1gInSc2	kurz
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc2d1	centrální
parity	parita	k1gFnSc2	parita
<g/>
)	)	kIx)	)
k	k	k7c3	k
euru	euro	k1gNnSc3	euro
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
musí	muset	k5eAaImIp3nS	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
užší	úzký	k2eAgNnSc1d2	užší
fluktuační	fluktuační	k2eAgNnSc1d1	fluktuační
pásmo	pásmo	k1gNnSc1	pásmo
±	±	k?	±
2,25	[number]	k4	2,25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nesmí	smět	k5eNaImIp3nS	smět
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
devalvaci	devalvace	k1gFnSc3	devalvace
centrální	centrální	k2eAgFnSc2d1	centrální
parity	parita	k1gFnSc2	parita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měny	měna	k1gFnSc2	měna
navázané	navázaný	k2eAgFnSc2d1	navázaná
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
používaly	používat	k5eAaImAgFnP	používat
měny	měna	k1gFnPc1	měna
navázané	navázaný	k2eAgFnPc1d1	navázaná
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
1	[number]	k4	1
severoevropský	severoevropský	k2eAgInSc1d1	severoevropský
stát	stát	k1gInSc1	stát
zapojený	zapojený	k2eAgInSc1d1	zapojený
do	do	k7c2	do
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
,	,	kIx,	,
2	[number]	k4	2
balkánské	balkánský	k2eAgInPc1d1	balkánský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
3	[number]	k4	3
francouzská	francouzský	k2eAgFnSc1d1	francouzská
zámořská	zámořský	k2eAgFnSc1d1	zámořská
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
3	[number]	k4	3
malé	malý	k2eAgInPc4d1	malý
ostrovní	ostrovní	k2eAgInPc4d1	ostrovní
státy	stát	k1gInPc4	stát
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
14	[number]	k4	14
států	stát	k1gInPc2	stát
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Měna	měna	k1gFnSc1	měna
Dánska	Dánsko	k1gNnSc2	Dánsko
-	-	kIx~	-
koruna	koruna	k1gFnSc1	koruna
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
mechanismu	mechanismus	k1gInSc2	mechanismus
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
II	II	kA	II
(	(	kIx(	(
<g/>
ERM	ERM	kA	ERM
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
měny	měna	k1gFnPc4	měna
(	(	kIx(	(
<g/>
bosenská	bosenský	k2eAgFnSc1d1	bosenská
marka	marka	k1gFnSc1	marka
a	a	k8xC	a
bulharský	bulharský	k2eAgInSc1d1	bulharský
lev	lev	k1gInSc1	lev
<g/>
)	)	kIx)	)
navázané	navázaný	k2eAgFnSc2d1	navázaná
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
ve	v	k7c6	v
shodném	shodný	k2eAgInSc6d1	shodný
pevném	pevný	k2eAgInSc6d1	pevný
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
EUR	euro	k1gNnPc2	euro
=	=	kIx~	=
1,955	[number]	k4	1,955
<g/>
83	[number]	k4	83
BGN	BGN	kA	BGN
=	=	kIx~	=
1,955	[number]	k4	1,955
<g/>
83	[number]	k4	83
BAM	bam	k0	bam
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
a	a	k8xC	a
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
jsou	být	k5eAaImIp3nP	být
francouzská	francouzský	k2eAgNnPc4d1	francouzské
zámořská	zámořský	k2eAgNnPc4d1	zámořské
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
používají	používat	k5eAaImIp3nP	používat
CFP	CFP	kA	CFP
frank	frank	k1gInSc1	frank
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
CFA	CFA	kA	CFA
frank	frank	k1gInSc4	frank
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
kurzu	kurz	k1gInSc6	kurz
navázán	navázat	k5eAaPmNgMnS	navázat
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
CFP	CFP	kA	CFP
frank	frank	k1gInSc1	frank
pevně	pevně	k6eAd1	pevně
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Kapverdy	Kapverda	k1gFnSc2	Kapverda
<g/>
,	,	kIx,	,
Komory	komora	k1gFnSc2	komora
a	a	k8xC	a
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc4d1	princův
ostrov	ostrov	k1gInSc4	ostrov
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
měny	měna	k1gFnPc4	měna
-	-	kIx~	-
escudo	escudo	k1gNnSc1	escudo
<g/>
,	,	kIx,	,
frank	frank	k1gInSc1	frank
a	a	k8xC	a
dobra	dobro	k1gNnSc2	dobro
-	-	kIx~	-
pevně	pevně	k6eAd1	pevně
navázány	navázán	k2eAgInPc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Benin	Benin	k1gMnSc1	Benin
<g/>
,	,	kIx,	,
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
<g/>
,	,	kIx,	,
Gabon	Gabon	k1gMnSc1	Gabon
<g/>
,	,	kIx,	,
Guinea-Bissau	Guinea-Bissaus	k1gInSc2	Guinea-Bissaus
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
<g/>
,	,	kIx,	,
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Togo	Togo	k1gNnSc1	Togo
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
CFA	CFA	kA	CFA
frank	frank	k1gInSc1	frank
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
navázán	navázán	k2eAgInSc1d1	navázán
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
poměru	poměr	k1gInSc6	poměr
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
frank	frank	k1gInSc4	frank
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
CFA	CFA	kA	CFA
frank	frank	k1gInSc1	frank
pevně	pevně	k6eAd1	pevně
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
evropských	evropský	k2eAgInPc2d1	evropský
předpisů	předpis	k1gInPc2	předpis
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
úředních	úřední	k2eAgInPc6d1	úřední
nebo	nebo	k8xC	nebo
státních	státní	k2eAgInPc6d1	státní
jazycích	jazyk	k1gInPc6	jazyk
zemí	zem	k1gFnSc7	zem
EU	EU	kA	EU
užívá	užívat	k5eAaImIp3nS	užívat
tvar	tvar	k1gInSc1	tvar
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
používání	používání	k1gNnSc2	používání
názvu	název	k1gInSc2	název
euro	euro	k1gNnSc1	euro
plně	plně	k6eAd1	plně
podléhá	podléhat	k5eAaImIp3nS	podléhat
pravidlům	pravidlo	k1gNnPc3	pravidlo
úředních	úřední	k2eAgInPc2d1	úřední
nebo	nebo	k8xC	nebo
státních	státní	k2eAgInPc2d1	státní
jazyků	jazyk	k1gInPc2	jazyk
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tři	tři	k4xCgNnPc1	tři
různá	různý	k2eAgNnPc1d1	různé
písma	písmo	k1gNnPc1	písmo
–	–	k?	–
latinka	latinka	k1gFnSc1	latinka
<g/>
,	,	kIx,	,
cyrilice	cyrilice	k1gFnSc1	cyrilice
a	a	k8xC	a
řecké	řecký	k2eAgNnSc1d1	řecké
písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
společné	společný	k2eAgFnSc2d1	společná
měny	měna	k1gFnSc2	měna
států	stát	k1gInPc2	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
písmech	písmo	k1gNnPc6	písmo
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgFnSc4d1	následující
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kodifikovaných	kodifikovaný	k2eAgNnPc2d1	kodifikované
pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
euro	euro	k1gNnSc1	euro
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
ohebné	ohebný	k2eAgNnSc4d1	ohebné
<g/>
,	,	kIx,	,
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
skloňované	skloňovaný	k2eAgNnSc1d1	skloňované
standardně	standardně	k6eAd1	standardně
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Cent	cent	k1gInSc1	cent
(	(	kIx(	(
<g/>
eurocent	eurocent	k1gInSc1	eurocent
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
ohebné	ohebný	k2eAgNnSc1d1	ohebné
<g/>
,	,	kIx,	,
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Psaná	psaný	k2eAgFnSc1d1	psaná
podoba	podoba	k1gFnSc1	podoba
názvu	název	k1gInSc2	název
měny	měna	k1gFnSc2	měna
euro	euro	k1gNnSc1	euro
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
psané	psaný	k2eAgFnSc2d1	psaná
podoby	podoba	k1gFnSc2	podoba
názvu	název	k1gInSc2	název
kontinentu	kontinent	k1gInSc2	kontinent
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
předpona	předpona	k1gFnSc1	předpona
euro-	euro-	k?	euro-
standardně	standardně	k6eAd1	standardně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
záležitosti	záležitost	k1gFnPc4	záležitost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
psaných	psaný	k2eAgNnPc2d1	psané
slov	slovo	k1gNnPc2	slovo
euro	euro	k1gNnSc4	euro
a	a	k8xC	a
cent	cent	k1gInSc4	cent
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
/	/	kIx~	/
<g/>
eu	eu	k?	eu
<g/>
.	.	kIx.	.
<g/>
ro	ro	k?	ro
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
cent	cent	k1gInSc1	cent
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
slova	slovo	k1gNnSc2	slovo
euro	euro	k1gNnSc1	euro
platí	platit	k5eAaImIp3nS	platit
následující	následující	k2eAgFnPc4d1	následující
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
správná	správný	k2eAgNnPc1d1	správné
tato	tento	k3xDgNnPc1	tento
užití	užití	k1gNnSc3	užití
názvu	název	k1gInSc2	název
<g/>
/	/	kIx~	/
<g/>
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
/	/	kIx~	/
<g/>
symbolu	symbol	k1gInSc3	symbol
euro	euro	k1gNnSc4	euro
po	po	k7c6	po
číslovkách	číslovka	k1gFnPc6	číslovka
<g/>
:	:	kIx,	:
1	[number]	k4	1
euro	euro	k1gNnSc1	euro
/	/	kIx~	/
1	[number]	k4	1
EUR	euro	k1gNnPc2	euro
/	/	kIx~	/
1	[number]	k4	1
€	€	k?	€
/	/	kIx~	/
1	[number]	k4	1
Eur	euro	k1gNnPc2	euro
–	–	k?	–
obdoba	obdoba	k1gFnSc1	obdoba
výrazu	výraz	k1gInSc2	výraz
1	[number]	k4	1
koruna	koruna	k1gFnSc1	koruna
/	/	kIx~	/
1	[number]	k4	1
CZK	CZK	kA	CZK
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
1	[number]	k4	1
Kč	Kč	kA	Kč
2	[number]	k4	2
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
)	)	kIx)	)
eura	euro	k1gNnSc2	euro
/	/	kIx~	/
2	[number]	k4	2
EUR	euro	k1gNnPc2	euro
/	/	kIx~	/
2	[number]	k4	2
€	€	k?	€
/	/	kIx~	/
2	[number]	k4	2
Eur	euro	k1gNnPc2	euro
–	–	k?	–
obdoba	obdoba	k1gFnSc1	obdoba
výrazu	výraz	k1gInSc2	výraz
2	[number]	k4	2
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
)	)	kIx)	)
koruny	koruna	k1gFnSc2	koruna
/	/	kIx~	/
2	[number]	k4	2
CZK	CZK	kA	CZK
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
2	[number]	k4	2
Kč	Kč	kA	Kč
5	[number]	k4	5
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
eur	euro	k1gNnPc2	euro
/	/	kIx~	/
5	[number]	k4	5
EUR	euro	k1gNnPc2	euro
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
€	€	k?	€
/	/	kIx~	/
5	[number]	k4	5
Eur	euro	k1gNnPc2	euro
–	–	k?	–
obdoba	obdoba	k1gFnSc1	obdoba
výrazu	výraz	k1gInSc2	výraz
5	[number]	k4	5
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
korun	koruna	k1gFnPc2	koruna
/	/	kIx~	/
5	[number]	k4	5
CZK	CZK	kA	CZK
/	/	kIx~	/
–	–	k?	–
/	/	kIx~	/
5	[number]	k4	5
Kč	Kč	kA	Kč
Slovo	slovo	k1gNnSc1	slovo
euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
po	po	k7c6	po
číslovkách	číslovka	k1gFnPc6	číslovka
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
<g/>
:	:	kIx,	:
euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
N	N	kA	N
<g/>
)	)	kIx)	)
–	–	k?	–
eura	euro	k1gNnSc2	euro
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
–	–	k?	–
euru	euro	k1gNnSc6	euro
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
–	–	k?	–
euro	euro	k1gNnSc4	euro
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
–	–	k?	–
euru	euro	k1gNnSc6	euro
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
–	–	k?	–
eurem	euro	k1gNnSc7	euro
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
)	)	kIx)	)
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
<g/>
:	:	kIx,	:
eura	euro	k1gNnSc2	euro
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
–	–	k?	–
eur	euro	k1gNnPc2	euro
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
–	–	k?	–
eurům	euro	k1gNnPc3	euro
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
–	–	k?	–
eura	euro	k1gNnSc2	euro
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
–	–	k?	–
eurech	euro	k1gNnPc6	euro
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
–	–	k?	–
eury	euro	k1gNnPc7	euro
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
)	)	kIx)	)
Vysloveně	vysloveně	k6eAd1	vysloveně
nesprávné	správný	k2eNgFnPc1d1	nesprávná
jsou	být	k5eAaImIp3nP	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
tvary	tvar	k1gInPc1	tvar
<g/>
:	:	kIx,	:
1	[number]	k4	1
Euro	euro	k1gNnSc1	euro
<g/>
,	,	kIx,	,
2	[number]	k4	2
Euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
5	[number]	k4	5
Euro	euro	k1gNnSc4	euro
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
euro	euro	k1gNnSc1	euro
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
e	e	k0	e
a	a	k8xC	a
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
zkratka	zkratka	k1gFnSc1	zkratka
Eur	euro	k1gNnPc2	euro
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
názorů	názor	k1gInPc2	názor
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
zkratky	zkratka	k1gFnSc2	zkratka
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
EUR	euro	k1gNnPc2	euro
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
zkratky	zkratka	k1gFnSc2	zkratka
CZK	CZK	kA	CZK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
zkratka	zkratka	k1gFnSc1	zkratka
však	však	k9	však
není	být	k5eNaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
žádným	žádný	k3yNgInSc7	žádný
předpisem	předpis	k1gInSc7	předpis
oficiálně	oficiálně	k6eAd1	oficiálně
zavedená	zavedený	k2eAgNnPc1d1	zavedené
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
euro	euro	k1gNnSc4	euro
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
dost	dost	k6eAd1	dost
krátké	krátký	k2eAgFnSc6d1	krátká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
jako	jako	k9	jako
zkratka	zkratka	k1gFnSc1	zkratka
měny	měna	k1gFnSc2	měna
právně	právně	k6eAd1	právně
závazná	závazný	k2eAgFnSc1d1	závazná
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zkratky	zkratka	k1gFnSc2	zkratka
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
zkratka	zkratka	k1gFnSc1	zkratka
měny	měna	k1gFnSc2	měna
česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
právně	právně	k6eAd1	právně
závazná	závazný	k2eAgFnSc1d1	závazná
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
znak	znak	k1gInSc1	znak
€	€	k?	€
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
podle	podle	k7c2	podle
<g />
.	.	kIx.	.
</s>
<s>
zvyklostí	zvyklost	k1gFnPc2	zvyklost
–	–	k?	–
jako	jako	k8xS	jako
analogie	analogie	k1gFnPc1	analogie
umístění	umístění	k1gNnSc2	umístění
zkratek	zkratka	k1gFnPc2	zkratka
typu	typ	k1gInSc2	typ
EUR	euro	k1gNnPc2	euro
nebo	nebo	k8xC	nebo
znaků	znak	k1gInPc2	znak
typu	typ	k1gInSc2	typ
$	$	kIx~	$
–	–	k?	–
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
za	za	k7c4	za
číslovku	číslovka	k1gFnSc4	číslovka
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
před	před	k7c4	před
číslovku	číslovka	k1gFnSc4	číslovka
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
není	být	k5eNaImIp3nS	být
závazně	závazně	k6eAd1	závazně
předepsaná	předepsaný	k2eAgFnSc1d1	předepsaná
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
symbol	symbol	k1gInSc4	symbol
psát	psát	k5eAaImF	psát
i	i	k9	i
před	před	k7c4	před
částku	částka	k1gFnSc4	částka
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
začaly	začít	k5eAaPmAgInP	začít
hovory	hovor	k1gInPc1	hovor
o	o	k7c6	o
potenciální	potenciální	k2eAgFnSc6d1	potenciální
společné	společný	k2eAgFnSc6d1	společná
měnové	měnový	k2eAgFnSc6d1	měnová
unii	unie	k1gFnSc6	unie
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
Mexikem	Mexiko	k1gNnSc7	Mexiko
s	s	k7c7	s
perspektivou	perspektiva	k1gFnSc7	perspektiva
rozšíření	rozšíření	k1gNnSc2	rozšíření
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
nést	nést	k5eAaImF	nést
jméno	jméno	k1gNnSc4	jméno
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
měna	měna	k1gFnSc1	měna
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Amero	Amero	k1gNnSc1	Amero
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
<g/>
:	:	kIx,	:
1	[number]	k4	1
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
měna	měna	k1gFnSc1	měna
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
cílů	cíl	k1gInPc2	cíl
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
tolik	tolik	k6eAd1	tolik
prioritním	prioritní	k2eAgMnPc3d1	prioritní
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Realita	realita	k1gFnSc1	realita
společné	společný	k2eAgFnSc2d1	společná
africké	africký	k2eAgFnSc2d1	africká
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
Afro	Afro	k1gMnSc1	Afro
nebo	nebo	k8xC	nebo
Sheba	Sheba	k1gMnSc1	Sheba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vznikají	vznikat	k5eAaImIp3nP	vznikat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
regionální	regionální	k2eAgFnPc4d1	regionální
měnové	měnový	k2eAgFnPc4d1	měnová
unie	unie	k1gFnPc4	unie
částí	část	k1gFnPc2	část
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
již	již	k6eAd1	již
existující	existující	k2eAgFnSc2d1	existující
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
mezi	mezi	k7c7	mezi
14	[number]	k4	14
zeměmi	zem	k1gFnPc7	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
používajících	používající	k2eAgInPc2d1	používající
CFA	CFA	kA	CFA
frank	frank	k1gInSc4	frank
mají	mít	k5eAaImIp3nP	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
dvě	dva	k4xCgNnPc1	dva
další	další	k1gNnPc1	další
regionální	regionální	k2eAgFnSc2d1	regionální
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
Afriky	Afrika	k1gFnSc2	Afrika
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zaveden	zavést	k5eAaPmNgInS	zavést
východoafrický	východoafrický	k2eAgInSc1d1	východoafrický
šilink	šilink	k1gInSc1	šilink
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
kontinentu	kontinent	k1gInSc2	kontinent
měna	měna	k1gFnSc1	měna
eco	eco	k?	eco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
existuje	existovat	k5eAaImIp3nS	existovat
Eurasijská	Eurasijský	k2eAgFnSc1d1	Eurasijská
daňové	daňový	k2eAgFnPc4d1	daňová
unie	unie	k1gFnPc4	unie
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
i	i	k9	i
plán	plán	k1gInSc4	plán
společné	společný	k2eAgFnSc2d1	společná
měny	měna	k1gFnSc2	měna
Altynu	Altyn	k1gInSc2	Altyn
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
plány	plán	k1gInPc1	plán
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
společné	společný	k2eAgFnSc2d1	společná
měny	měna	k1gFnSc2	měna
mezi	mezi	k7c7	mezi
některými	některý	k3yIgInPc7	některý
státy	stát	k1gInPc7	stát
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
organizace	organizace	k1gFnSc1	organizace
Bolívarovský	Bolívarovský	k2eAgInSc1d1	Bolívarovský
svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
Ameriky	Amerika	k1gFnSc2	Amerika
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
členy	člen	k1gMnPc7	člen
virtuální	virtuální	k2eAgInSc4d1	virtuální
měnovou	měnový	k2eAgFnSc4d1	měnová
jednotku	jednotka	k1gFnSc4	jednotka
sucre	sucr	k1gMnSc5	sucr
<g/>
.	.	kIx.	.
</s>
