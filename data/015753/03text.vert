<s>
Joran	Joran	k1gMnSc1
</s>
<s>
Joran	Joran	k1gInSc1
[	[	kIx(
<g/>
ʒ	ʒ	k?
<g/>
.	.	kIx.
<g/>
ʁ	ʁ	k?
<g/>
̃	̃	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
juran	juran	k1gInSc1
[	[	kIx(
<g/>
ʒ	ʒ	k1gFnPc1
<g/>
.	.	kIx.
<g/>
ʁ	ʁ	k?
<g/>
̃	̃	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc1
horského	horský	k2eAgInSc2d1
studeného	studený	k2eAgInSc2d1
větru	vítr	k1gInSc2
vanoucího	vanoucí	k2eAgInSc2d1
ze	z	k7c2
severozápadu	severozápad	k1gInSc2
přes	přes	k7c4
pohoří	pohoří	k1gNnSc4
Jury	jura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
chladný	chladný	k2eAgInSc1d1
a	a	k8xC
nárazovitý	nárazovitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působení	působení	k1gNnSc1
joranu	joran	k1gInSc2
postihuje	postihovat	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
od	od	k7c2
Ženevy	Ženeva	k1gFnSc2
po	po	k7c4
Grenchen	Grenchen	k1gInSc4
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
oblast	oblast	k1gFnSc1
Ženevského	ženevský	k2eAgMnSc2d1
<g/>
,	,	kIx,
Neuchâtelského	Neuchâtelský	k2eAgNnSc2d1
a	a	k8xC
Bielského	Bielský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Bouët	Bouët	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
rozlišuje	rozlišovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
rozdílné	rozdílný	k2eAgInPc4d1
typy	typ	k1gInPc4
joranů	joran	k1gInPc2
vznikajících	vznikající	k2eAgInPc2d1
z	z	k7c2
různých	různý	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
joran	joran	k1gInSc1
dynamický	dynamický	k2eAgInSc1d1
(	(	kIx(
<g/>
též	též	k9
joran	joran	k1gMnSc1
studené	studený	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
‹	‹	k?
<g/>
Le	Le	k1gMnSc1
Joran	Joran	k1gMnSc1
de	de	k?
front	front	k1gInSc1
froid	froid	k1gInSc1
<g/>
›	›	k?
<g/>
)	)	kIx)
vzniká	vznikat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
oblast	oblast	k1gFnSc1
nízkého	nízký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
vzduchu	vzduch	k1gInSc2
přesouvá	přesouvat	k5eAaImIp3nS
od	od	k7c2
severu	sever	k1gInSc2
z	z	k7c2
Francie	Francie	k1gFnSc2
přes	přes	k7c4
region	region	k1gInSc4
Franche-Comté	Franche-Comtý	k2eAgFnSc2d1
a	a	k8xC
musí	muset	k5eAaImIp3nS
stoupat	stoupat	k5eAaImF
přes	přes	k7c4
pohoří	pohoří	k1gNnSc4
Jury	jura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
povětrnostní	povětrnostní	k2eAgFnSc1d1
situace	situace	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
většinou	většina	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
nad	nad	k7c7
severním	severní	k2eAgNnSc7d1
Španělskem	Španělsko	k1gNnSc7
leží	ležet	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
vysokého	vysoký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
oblasti	oblast	k1gFnSc2
Skotska	Skotsko	k1gNnSc2
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
jižní	jižní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
brázda	brázda	k1gFnSc1
nízkého	nízký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
joran	joran	k1gInSc4
statický	statický	k2eAgInSc4d1
(	(	kIx(
<g/>
též	též	k9
francouzsky	francouzsky	k6eAd1
‹	‹	k?
<g/>
Le	Le	k1gMnSc1
Joran	Joran	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
orage	orage	k1gInSc1
<g/>
›	›	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
katabatický	katabatický	k2eAgInSc1d1
padavý	padavý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
pozorován	pozorovat	k5eAaImNgInS
většinou	většinou	k6eAd1
v	v	k7c4
podvečer	podvečer	k1gInSc4
mezi	mezi	k7c7
květnem	květen	k1gInSc7
a	a	k8xC
červencem	červenec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc4
vedoucí	vedoucí	k1gFnSc2
ke	k	k7c3
statickému	statický	k2eAgInSc3d1
joranu	joran	k1gInSc3
jsou	být	k5eAaImIp3nP
zatím	zatím	k6eAd1
jen	jen	k9
málo	málo	k6eAd1
prozkoumány	prozkoumat	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
jsou	být	k5eAaImIp3nP
způsobeny	způsobit	k5eAaPmNgInP
v	v	k7c6
rozdílných	rozdílný	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
teploty	teplota	k1gFnSc2
a	a	k8xC
tlaku	tlak	k1gInSc2
vzduchu	vzduch	k1gInSc2
mezi	mezi	k7c7
severním	severní	k2eAgNnSc7d1
a	a	k8xC
jižním	jižní	k2eAgNnSc7d1
úpatím	úpatí	k1gNnSc7
Jury	jura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
rozdíl	rozdíl	k1gInSc1
teploty	teplota	k1gFnSc2
vzduchu	vzduch	k1gInSc2
mezi	mezi	k7c7
hřbetem	hřbet	k1gInSc7
a	a	k8xC
úpatím	úpatí	k1gNnSc7
má	mít	k5eAaImIp3nS
vliv	vliv	k1gInSc4
na	na	k7c4
rychlost	rychlost	k1gFnSc4
nárazů	náraz	k1gInPc2
větru	vítr	k1gInSc2
a	a	k8xC
na	na	k7c4
rychlost	rychlost	k1gFnSc4
větru	vítr	k1gInSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
teplot	teplota	k1gFnPc2
má	mít	k5eAaImIp3nS
vliv	vliv	k1gInSc1
zastínění	zastínění	k1gNnSc2
svahů	svah	k1gInPc2
Jury	jury	k1gFnPc2
při	při	k7c6
západu	západ	k1gInSc6
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
oblaky	oblak	k1gInPc1
a	a	k8xC
oblačnost	oblačnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BOUËT	BOUËT	kA
<g/>
,	,	kIx,
Max	max	kA
<g/>
.	.	kIx.
Climat	Climat	k1gInSc1
et	et	k?
météorologie	météorologie	k1gFnSc1
de	de	k?
la	la	k1gNnSc7
Suisse	Suisse	k1gFnSc2
romande	romand	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lausanne	Lausanne	k1gNnSc1
:	:	kIx,
Payot	Payot	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BOUËT	BOUËT	kA
<g/>
,	,	kIx,
Max	max	kA
<g/>
.	.	kIx.
Climat	Climat	k1gInSc1
et	et	k?
météorologie	météorologie	k1gFnSc1
de	de	k?
la	la	k1gNnSc7
Suisse	Suisse	k1gFnSc2
romande	romand	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lausanne	Lausanne	k1gNnSc1
:	:	kIx,
Payot	Payot	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vítr	vítr	k1gInSc1
vlastnosti	vlastnost	k1gFnSc2
</s>
<s>
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
•	•	k?
směr	směr	k1gInSc1
větru	vítr	k1gInSc2
•	•	k?
stáčení	stáčení	k1gNnSc1
větru	vítr	k1gInSc2
•	•	k?
střih	střih	k1gInSc1
větru	vítr	k1gInSc2
•	•	k?
síla	síla	k1gFnSc1
větru	vítr	k1gInSc2
měření	měření	k1gNnSc2
</s>
<s>
anemometr	anemometr	k1gInSc1
<g/>
,	,	kIx,
profiler	profiler	k1gInSc1
•	•	k?
anemograf	anemograf	k1gInSc1
•	•	k?
anemoskop	anemoskop	k1gInSc1
•	•	k?
větrná	větrný	k2eAgFnSc1d1
korouhev	korouhev	k1gFnSc1
•	•	k?
větrný	větrný	k2eAgInSc4d1
rukáv	rukáv	k1gInSc4
místní	místní	k2eAgInPc4d1
názvy	název	k1gInPc4
větrů	vítr	k1gInPc2
</s>
<s>
baguio	baguio	k6eAd1
•	•	k?
blizard	blizard	k1gInSc1
•	•	k?
bóra	bóra	k1gFnSc1
•	•	k?
breva	breva	k6eAd1
•	•	k?
bríza	bríza	k1gFnSc1
•	•	k?
buran	buran	k1gInSc1
•	•	k?
burga	burga	k1gFnSc1
•	•	k?
fén	fén	k1gInSc1
•	•	k?
fujavice	fujavice	k1gFnSc2
•	•	k?
fukéř	fukéř	k1gInSc1
•	•	k?
garmsil	garmsit	k5eAaImAgInS,k5eAaPmAgInS
•	•	k?
gibli	gible	k1gFnSc4
•	•	k?
haboob	habooba	k1gFnPc2
•	•	k?
halný	halný	k2eAgInSc1d1
•	•	k?
harmatán	harmatán	k2eAgInSc1d1
•	•	k?
húlava	húlava	k1gFnSc1
•	•	k?
hurikán	hurikán	k1gInSc1
•	•	k?
chamsin	chamsin	k1gInSc1
•	•	k?
chinook	chinook	k1gInSc1
•	•	k?
joran	joran	k1gInSc1
•	•	k?
jugo	jugo	k6eAd1
•	•	k?
košava	košava	k1gFnSc1
•	•	k?
leste	leste	k5eAaPmIp2nP
•	•	k?
levante	levant	k1gMnSc5
•	•	k?
leveche	leveche	k1gFnSc6
•	•	k?
maestral	maestrat	k5eAaPmAgMnS,k5eAaImAgMnS
•	•	k?
mistrál	mistrál	k1gMnSc1
•	•	k?
monzun	monzun	k1gInSc1
•	•	k?
nor	nora	k1gFnPc2
<g/>
'	'	kIx"
<g/>
easter	easter	k1gInSc1
•	•	k?
norte	nort	k1gMnSc5
<g/>
,	,	kIx,
norther	northra	k1gFnPc2
•	•	k?
ostria	ostrium	k1gNnSc2
•	•	k?
pamperos	pamperosa	k1gFnPc2
•	•	k?
papagajo	papagajo	k1gMnSc1
•	•	k?
polák	polák	k1gMnSc1
•	•	k?
puelche	puelche	k1gInSc1
•	•	k?
samum	samum	k1gInSc1
•	•	k?
tivano	tivana	k1gFnSc5
•	•	k?
touríello	touríello	k1gNnSc4
•	•	k?
tramontana	tramontana	k1gFnSc1
•	•	k?
uragán	uragán	k1gInSc1
•	•	k?
vardar	vardar	k1gInSc1
•	•	k?
willy-willy	willy-willa	k1gFnSc2
•	•	k?
zonda	zonda	k1gFnSc1
typy	typa	k1gFnSc2
větrů	vítr	k1gInPc2
</s>
<s>
ageostrofický	ageostrofický	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
anabatický	anabatický	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
geostrofický	geostrofický	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
horský	horský	k2eAgInSc1d1
a	a	k8xC
údolní	údolní	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
katabatický	katabatický	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
ledovcový	ledovcový	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
mořský	mořský	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
padavý	padavý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
pasát	pasát	k1gInSc1
•	•	k?
pobřežní	pobřežní	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
proměnlivý	proměnlivý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
protivítr	protivítr	k1gInSc1
•	•	k?
přízemní	přízemní	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
sestupný	sestupný	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
svahový	svahový	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
tornádo	tornádo	k1gNnSc1
•	•	k?
tromba	tromba	k1gFnSc1
•	•	k?
větrná	větrný	k2eAgFnSc1d1
smršť	smršť	k1gFnSc1
•	•	k?
vodní	vodní	k2eAgFnSc1d1
smršť	smršť	k1gFnSc1
•	•	k?
výstupný	výstupný	k2eAgInSc1d1
vítr	vítr	k1gInSc1
větrné	větrný	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
</s>
<s>
Beaufortova	Beaufortův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
(	(	kIx(
<g/>
bezvětří	bezvětří	k1gNnSc1
•	•	k?
vánek	vánek	k1gInSc1
•	•	k?
čerstvý	čerstvý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
bouřlivý	bouřlivý	k2eAgInSc1d1
vítr	vítr	k1gInSc1
•	•	k?
vichřice	vichřice	k1gFnSc1
•	•	k?
orkán	orkán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Saffirova	Saffirov	k1gInSc2
<g/>
–	–	k?
<g/>
Simpsonova	Simpsonův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
•	•	k?
Fujitova	Fujitův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
klima	klima	k1gNnSc1
</s>
<s>
větrná	větrný	k2eAgFnSc1d1
růžice	růžice	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Meteorologie	meteorologie	k1gFnSc1
|	|	kIx~
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
