<p>
<s>
Cornellova	Cornellův	k2eAgFnSc1d1	Cornellova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Cornell	Cornell	k1gMnSc1	Cornell
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ithaca	Ithac	k1gInSc2	Ithac
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
dvě	dva	k4xCgFnPc1	dva
lékařské	lékařský	k2eAgFnPc1d1	lékařská
fakulty	fakulta	k1gFnPc1	fakulta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Education	Education	k1gInSc1	Education
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
Katar	katar	k1gInSc1	katar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
Ezrou	Ezra	k1gMnSc7	Ezra
Cornellem	Cornell	k1gMnSc7	Cornell
a	a	k8xC	a
Andrew	Andrew	k1gMnSc7	Andrew
Dicksonem	Dickson	k1gMnSc7	Dickson
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
spolku	spolek	k1gInSc6	spolek
prestižních	prestižní	k2eAgFnPc2d1	prestižní
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
sdružených	sdružený	k2eAgFnPc2d1	sdružená
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Břečťanová	břečťanový	k2eAgFnSc1d1	Břečťanová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
profesoři	profesor	k1gMnPc1	profesor
a	a	k8xC	a
absolventi	absolvent	k1gMnPc1	absolvent
==	==	k?	==
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Bethe	Beth	k1gFnSc2	Beth
-	-	kIx~	-
laureát	laureát	k1gMnSc1	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vynálezců	vynálezce	k1gMnPc2	vynálezce
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
</s>
</p>
<p>
<s>
Jay	Jay	k?	Jay
Clark	Clark	k1gInSc1	Clark
Buckey	Buckea	k1gMnSc2	Buckea
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
astronaut	astronaut	k1gMnSc1	astronaut
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
<g/>
;	;	kIx,	;
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vynálezců	vynálezce	k1gMnPc2	vynálezce
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
</s>
</p>
<p>
<s>
Mae	Mae	k?	Mae
Jemisonová	Jemisonová	k1gFnSc1	Jemisonová
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
astronautka	astronautka	k1gFnSc1	astronautka
<g/>
,	,	kIx,	,
vystudovaný	vystudovaný	k2eAgInSc1d1	vystudovaný
obor	obor	k1gInSc1	obor
<g/>
:	:	kIx,	:
chemie	chemie	k1gFnSc1	chemie
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Kretzmann	Kretzmann	k1gMnSc1	Kretzmann
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
<g/>
;	;	kIx,	;
historik	historik	k1gMnSc1	historik
středověké	středověký	k2eAgFnSc2d1	středověká
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
logiky	logika	k1gFnSc2	logika
</s>
</p>
<p>
<s>
Jurij	Jurít	k5eAaPmRp2nS	Jurít
Feodorovič	Feodorovič	k1gMnSc1	Feodorovič
Orlov	Orlov	k1gInSc1	Orlov
-	-	kIx~	-
jaderný	jaderný	k2eAgMnSc1d1	jaderný
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
sovětský	sovětský	k2eAgMnSc1d1	sovětský
disident	disident	k1gMnSc1	disident
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Švejnar	Švejnar	k1gMnSc1	Švejnar
-	-	kIx~	-
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
obor	obor	k1gInSc1	obor
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
průmyslově-pracovních	průmyslověracovní	k2eAgInPc2d1	průmyslově-pracovní
vztahů	vztah	k1gInPc2	vztah
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cornellova	Cornellův	k2eAgFnSc1d1	Cornellova
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
