<s>
Cornellova	Cornellův	k2eAgFnSc1d1	Cornellova
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Cornell	Cornell	k1gMnSc1	Cornell
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ithaca	Ithac	k1gInSc2	Ithac
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
