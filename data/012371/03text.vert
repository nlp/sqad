<p>
<s>
Mihule	mihule	k1gFnSc1	mihule
potoční	potoční	k2eAgFnSc1d1	potoční
(	(	kIx(	(
<g/>
Lampetra	Lampetra	k1gFnSc1	Lampetra
planeri	planer	k1gFnSc2	planer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
mihule	mihule	k1gFnSc2	mihule
z	z	k7c2	z
nadtřídy	nadtřída	k1gFnSc2	nadtřída
kruhoústí	kruhoústí	k1gMnPc1	kruhoústí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Úhořovité	úhořovitý	k2eAgNnSc1d1	úhořovité
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
zakončené	zakončený	k2eAgNnSc1d1	zakončené
kruhovitými	kruhovitý	k2eAgNnPc7d1	kruhovité
ústy	ústa	k1gNnPc7	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hlavou	hlava	k1gFnSc7	hlava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
7	[number]	k4	7
žaberních	žaberní	k2eAgFnPc2d1	žaberní
štěrbin	štěrbina	k1gFnPc2	štěrbina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
ploutevní	ploutevní	k2eAgInSc4d1	ploutevní
lem	lem	k1gInSc4	lem
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
je	být	k5eAaImIp3nS	být
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
boky	boka	k1gFnPc4	boka
žlutavé	žlutavý	k2eAgNnSc4d1	žlutavé
a	a	k8xC	a
břicho	břicho	k1gNnSc4	břicho
stříbřité	stříbřitý	k2eAgNnSc4d1	stříbřité
<g/>
.	.	kIx.	.
</s>
<s>
Mihule	mihule	k1gFnSc1	mihule
potoční	potoční	k2eAgFnSc1d1	potoční
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
potoky	potok	k1gInPc4	potok
a	a	k8xC	a
menší	malý	k2eAgFnPc4d2	menší
řeky	řeka	k1gFnPc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
v	v	k7c6	v
úmořích	úmoří	k1gNnPc6	úmoří
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Larvy	larva	k1gFnPc1	larva
mihule	mihule	k1gFnSc2	mihule
potoční	potoční	k2eAgInPc1d1	potoční
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
detritem	detrit	k1gInSc7	detrit
a	a	k8xC	a
organickými	organický	k2eAgFnPc7d1	organická
usazeninami	usazenina	k1gFnPc7	usazenina
v	v	k7c6	v
písčitých	písčitý	k2eAgInPc6d1	písčitý
náplavech	náplav	k1gInPc6	náplav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obývá	obývat	k5eAaImIp3nS	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c4	v
dospělce	dospělec	k1gMnSc4	dospělec
zakrňuje	zakrňovat	k5eAaImIp3nS	zakrňovat
trávicí	trávicí	k2eAgInSc1d1	trávicí
trakt	trakt	k1gInSc1	trakt
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
dospělec	dospělec	k1gMnSc1	dospělec
potravu	potrava	k1gFnSc4	potrava
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Tření	tření	k1gNnSc1	tření
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
štěrkové	štěrkový	k2eAgNnSc4d1	štěrkové
dno	dno	k1gNnSc4	dno
asi	asi	k9	asi
1500	[number]	k4	1500
jiker	jikra	k1gFnPc2	jikra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
larvy	larva	k1gFnPc1	larva
–	–	k?	–
minohy	minoha	k1gFnSc2	minoha
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
zahrabávají	zahrabávat	k5eAaImIp3nP	zahrabávat
do	do	k7c2	do
bahna	bahno	k1gNnSc2	bahno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
před	před	k7c7	před
třením	tření	k1gNnSc7	tření
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c4	v
dospělce	dospělec	k1gMnPc4	dospělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Mihule	mihule	k1gFnPc1	mihule
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xC	jako
kulinářská	kulinářský	k2eAgFnSc1d1	kulinářská
delikatesa	delikatesa	k1gFnSc1	delikatesa
<g/>
.	.	kIx.	.
</s>
<s>
Jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
anglických	anglický	k2eAgFnPc2d1	anglická
dějin	dějiny	k1gFnPc2	dějiny
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pokrmem	pokrm	k1gInSc7	pokrm
z	z	k7c2	z
dušených	dušený	k2eAgFnPc2d1	dušená
mihulí	mihule	k1gFnPc2	mihule
udusil	udusit	k5eAaPmAgInS	udusit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1135	[number]	k4	1135
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
zanechal	zanechat	k5eAaPmAgMnS	zanechat
legitimního	legitimní	k2eAgMnSc4d1	legitimní
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
<g/>
;	;	kIx,	;
o	o	k7c4	o
anglickou	anglický	k2eAgFnSc4d1	anglická
korunu	koruna	k1gFnSc4	koruna
pak	pak	k6eAd1	pak
sváděli	svádět	k5eAaImAgMnP	svádět
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
vyčerpávající	vyčerpávající	k2eAgFnPc4d1	vyčerpávající
boje	boj	k1gInPc4	boj
Jindřichova	Jindřichův	k2eAgFnSc1d1	Jindřichova
dcera	dcera	k1gFnSc1	dcera
Matylda	Matylda	k1gFnSc1	Matylda
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
<g/>
)	)	kIx)	)
Štěpán	Štěpán	k1gMnSc1	Štěpán
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Blois	Blois	k1gFnSc2	Blois
<g/>
.	.	kIx.	.
</s>
<s>
Vleklý	vleklý	k2eAgInSc4d1	vleklý
spor	spor	k1gInSc4	spor
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
smrt	smrt	k1gFnSc1	smrt
Štěpánova	Štěpánův	k2eAgMnSc2d1	Štěpánův
syna	syn	k1gMnSc2	syn
Eustacha	Eustach	k1gMnSc2	Eustach
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1153	[number]	k4	1153
zemřel	zemřít	k5eAaPmAgInS	zemřít
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
;	;	kIx,	;
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
Matyldin	Matyldin	k2eAgMnSc1d1	Matyldin
syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mihule	mihule	k1gFnSc2	mihule
potoční	potoční	k2eAgMnPc1d1	potoční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mihule	mihule	k1gFnSc2	mihule
potoční	potoční	k2eAgFnSc2d1	potoční
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lampetra	Lampetrum	k1gNnSc2	Lampetrum
planeri	planer	k1gFnSc2	planer
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Mihule	mihule	k1gFnPc1	mihule
potoční	potoční	k2eAgFnPc1d1	potoční
-	-	kIx~	-
atlas	atlas	k1gInSc1	atlas
ryb	ryba	k1gFnPc2	ryba
on-line	onin	k1gInSc5	on-lin
</s>
</p>
