<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
znamená	znamenat	k5eAaImIp3nS	znamenat
předškolní	předškolní	k2eAgNnSc4d1	předškolní
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
kindergarten	kindergarten	k2eAgMnSc1d1	kindergarten
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
-	-	kIx~	-
"	"	kIx"	"
<g/>
zahrada	zahrada	k1gFnSc1	zahrada
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
(	(	kIx(	(
<g/>
nultá	nultý	k4xOgFnSc1	nultý
úroveň	úroveň	k1gFnSc1	úroveň
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
termín	termín	k1gInSc1	termín
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
označuje	označovat	k5eAaImIp3nS	označovat
součást	součást	k1gFnSc1	součást
formálního	formální	k2eAgInSc2d1	formální
školního	školní	k2eAgInSc2d1	školní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
předškolní	předškolní	k2eAgNnSc4d1	předškolní
zařízení	zařízení	k1gNnSc4	zařízení
nebo	nebo	k8xC	nebo
opatrovatelskou	opatrovatelský	k2eAgFnSc4d1	opatrovatelská
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
neoficiálně	oficiálně	k6eNd1	oficiálně
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
pojmem	pojem	k1gInSc7	pojem
mateřská	mateřský	k2eAgFnSc1d1	mateřská
školka	školka	k1gFnSc1	školka
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
školka	školka	k1gFnSc1	školka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mateřinka	mateřinka	k1gFnSc1	mateřinka
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Fröbel	Fröbel	k1gMnSc1	Fröbel
otevřel	otevřít	k5eAaPmAgMnS	otevřít
oficiálně	oficiálně	k6eAd1	oficiálně
první	první	k4xOgFnSc4	první
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
400	[number]	k4	400
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
objevení	objevení	k1gNnSc2	objevení
knihtisku	knihtisk	k1gInSc2	knihtisk
Gutenbergem	Gutenberg	k1gInSc7	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
USA	USA	kA	USA
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
Margarethe	Margarethe	k1gFnSc1	Margarethe
(	(	kIx(	(
<g/>
Margaretta	Margaretta	k1gFnSc1	Margaretta
<g/>
)	)	kIx)	)
Meyerová	Meyerový	k2eAgFnSc1d1	Meyerová
Schurzová	Schurzová	k1gFnSc1	Schurzová
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
státníka	státník	k1gMnSc4	státník
jménem	jméno	k1gNnSc7	jméno
Carl	Carl	k1gMnSc1	Carl
Schurz	Schurz	k1gMnSc1	Schurz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Watertown	Watertowna	k1gFnPc2	Watertowna
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
byla	být	k5eAaImAgFnS	být
provozována	provozován	k2eAgFnSc1d1	provozována
jako	jako	k8xC	jako
veřejná	veřejný	k2eAgFnSc1d1	veřejná
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
Opatrovny	opatrovna	k1gFnPc1	opatrovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
rozšíření	rozšíření	k1gNnSc4	rozšíření
manufakturní	manufakturní	k2eAgFnSc2d1	manufakturní
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
do	do	k7c2	do
pracovního	pracovní	k2eAgInSc2d1	pracovní
procesu	proces	k1gInSc2	proces
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
-	-	kIx~	-
matky	matka	k1gFnPc1	matka
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
předškolní	předškolní	k2eAgFnPc1d1	předškolní
děti	dítě	k1gFnPc1	dítě
potulovaly	potulovat	k5eAaImAgFnP	potulovat
bez	bez	k7c2	bez
dozoru	dozor	k1gInSc2	dozor
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
zařazovaly	zařazovat	k5eAaImAgInP	zařazovat
se	se	k3xPyFc4	se
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
žebráckých	žebrácký	k2eAgInPc2d1	žebrácký
gangů	gang	k1gInPc2	gang
nebo	nebo	k8xC	nebo
umíraly	umírat	k5eAaImAgFnP	umírat
na	na	k7c6	na
zranění	zranění	k1gNnSc6	zranění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
opatroven	opatrovna	k1gFnPc2	opatrovna
Jan	Jan	k1gMnSc1	Jan
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
-	-	kIx~	-
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
založil	založit	k5eAaPmAgMnS	založit
"	"	kIx"	"
<g/>
vzornou	vzorný	k2eAgFnSc4d1	vzorná
opatrovnu	opatrovna	k1gFnSc4	opatrovna
<g/>
"	"	kIx"	"
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
pod	pod	k7c7	pod
Emauzy	Emauzy	k1gInPc7	Emauzy
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
říkanek	říkanka	k1gFnPc2	říkanka
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
vštěpoval	vštěpovat	k5eAaBmAgInS	vštěpovat
tříletým	tříletý	k2eAgMnSc7d1	tříletý
až	až	k8xS	až
šestiletým	šestiletý	k2eAgMnSc7d1	šestiletý
dětem	dítě	k1gFnPc3	dítě
nenásilnou	násilný	k2eNgFnSc7d1	nenásilná
formou	forma	k1gFnSc7	forma
morální	morální	k2eAgFnSc2d1	morální
zásady	zásada	k1gFnSc2	zásada
a	a	k8xC	a
učil	učit	k5eAaImAgMnS	učit
je	být	k5eAaImIp3nS	být
základům	základ	k1gInPc3	základ
čtení	čtení	k1gNnSc2	čtení
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc2	psaní
i	i	k8xC	i
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
několik	několik	k4yIc4	několik
vlivných	vlivný	k2eAgFnPc2d1	vlivná
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Školka	školka	k1gFnSc1	školka
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
čtenář	čtenář	k1gMnSc1	čtenář
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
písař	písař	k1gMnSc1	písař
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
Čech	Čech	k1gMnSc1	Čech
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
německé	německý	k2eAgNnSc4d1	německé
Kindergarten	Kindergarten	k2eAgInSc4d1	Kindergarten
neboli	neboli	k8xC	neboli
dětské	dětský	k2eAgFnPc4d1	dětská
zahrádky	zahrádka	k1gFnPc4	zahrádka
podle	podle	k7c2	podle
Friedricha	Friedrich	k1gMnSc2	Friedrich
Fröebela	Fröebel	k1gMnSc2	Fröebel
-	-	kIx~	-
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
matky	matka	k1gFnPc4	matka
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
učily	učit	k5eAaImAgFnP	učit
správné	správný	k2eAgFnSc3d1	správná
výchově	výchova	k1gFnSc3	výchova
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
Kindergarten	Kindergartno	k1gNnPc2	Kindergartno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Spálené	spálený	k2eAgFnSc6d1	spálená
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Kindergarten	Kindergartno	k1gNnPc2	Kindergartno
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
českými	český	k2eAgMnPc7d1	český
Němci	Němec	k1gMnPc7	Němec
(	(	kIx(	(
<g/>
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
poněmčování	poněmčování	k1gNnSc3	poněmčování
českých	český	k2eAgFnPc2d1	Česká
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracovalo	pracovat	k5eAaImAgNnS	pracovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dárky	dárek	k1gInPc4	dárek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byly	být	k5eAaImAgInP	být
objekty	objekt	k1gInPc1	objekt
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
koule	koule	k1gFnPc1	koule
<g/>
,	,	kIx,	,
krychle	krychle	k1gFnPc1	krychle
<g/>
,	,	kIx,	,
hranol	hranol	k1gInSc1	hranol
<g/>
,	,	kIx,	,
kužel	kužel	k1gInSc1	kužel
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělily	dělit	k5eAaImAgInP	dělit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
první	první	k4xOgFnSc1	první
stavebnice	stavebnice	k1gFnSc1	stavebnice
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
myšlenkových	myšlenkový	k2eAgFnPc2d1	myšlenková
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
základy	základ	k1gInPc1	základ
počtů	počet	k1gInPc2	počet
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Jako	jako	k8xS	jako
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
německé	německý	k2eAgMnPc4d1	německý
Kindergarten	Kindergarten	k2eAgMnSc1d1	Kindergarten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Marie	Maria	k1gFnSc2	Maria
Riegrové	Riegrový	k2eAgFnSc2d1	Riegrový
Palacké	Palacká	k1gFnSc2	Palacká
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poslala	poslat	k5eAaPmAgFnS	poslat
dvě	dva	k4xCgFnPc4	dva
české	český	k2eAgFnPc4d1	Česká
opatrovnice	opatrovnice	k1gFnPc4	opatrovnice
(	(	kIx(	(
<g/>
Marii	Maria	k1gFnSc4	Maria
Mullerovou	Mullerová	k1gFnSc4	Mullerová
a	a	k8xC	a
Barboru	Barbora	k1gFnSc4	Barbora
Ledvinkovou	Ledvinková	k1gFnSc4	Ledvinková
<g/>
)	)	kIx)	)
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
přejaly	přejmout	k5eAaPmAgFnP	přejmout
francouzský	francouzský	k2eAgInSc4d1	francouzský
koncept	koncept	k1gInSc4	koncept
předškolních	předškolní	k2eAgFnPc2d1	předškolní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
fungovala	fungovat	k5eAaImAgFnS	fungovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
principu	princip	k1gInSc6	princip
charity	charita	k1gFnSc2	charita
(	(	kIx(	(
<g/>
něco	něco	k3yInSc1	něco
přispělo	přispět	k5eAaPmAgNnS	přispět
město	město	k1gNnSc4	město
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
byl	být	k5eAaImAgInS	být
financován	financovat	k5eAaBmNgInS	financovat
pomocí	pomoc	k1gFnSc7	pomoc
charitativních	charitativní	k2eAgFnPc2d1	charitativní
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
charitativním	charitativní	k2eAgFnPc3d1	charitativní
sbírkám	sbírka	k1gFnPc3	sbírka
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
provoz	provoz	k1gInSc4	provoz
MŠ	MŠ	kA	MŠ
celodenní	celodenní	k2eAgFnSc1d1	celodenní
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
nemusely	muset	k5eNaImAgFnP	muset
nosit	nosit	k5eAaImF	nosit
svačiny	svačina	k1gFnPc4	svačina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
poskytnuto	poskytnut	k2eAgNnSc1d1	poskytnuto
stravování	stravování	k1gNnSc1	stravování
a	a	k8xC	a
nejchudší	chudý	k2eAgMnPc1d3	nejchudší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
i	i	k9	i
ošaceny	ošatit	k5eAaPmNgInP	ošatit
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
přejala	přejmout	k5eAaPmAgFnS	přejmout
tři	tři	k4xCgFnPc4	tři
koncepce	koncepce	k1gFnPc4	koncepce
-	-	kIx~	-
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
(	(	kIx(	(
<g/>
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německou	německý	k2eAgFnSc7d1	německá
(	(	kIx(	(
<g/>
princip	princip	k1gInSc4	princip
"	"	kIx"	"
<g/>
dárků	dárek	k1gInPc2	dárek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
českou	český	k2eAgFnSc4d1	Česká
(	(	kIx(	(
<g/>
výuka	výuka	k1gFnSc1	výuka
trivia	trivium	k1gNnSc2	trivium
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
opatroven	opatrovna	k1gFnPc2	opatrovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
předškolní	předškolní	k2eAgFnSc1d1	předškolní
výchova	výchova	k1gFnSc1	výchova
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Reformním	reformní	k2eAgNnSc7d1	reformní
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
fröebelismus	fröebelismus	k1gInSc4	fröebelismus
(	(	kIx(	(
<g/>
zmechanizovaná	zmechanizovaný	k2eAgFnSc1d1	zmechanizovaná
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
dárky	dárek	k1gInPc7	dárek
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgNnSc1d1	absolutní
potlačení	potlačení	k1gNnSc1	potlačení
dětské	dětský	k2eAgFnSc2d1	dětská
individuality	individualita	k1gFnSc2	individualita
<g/>
,	,	kIx,	,
přeintelektualizovaný	přeintelektualizovaný	k2eAgInSc4d1	přeintelektualizovaný
rozvoj	rozvoj	k1gInSc4	rozvoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
proudy	proud	k1gInPc1	proud
-	-	kIx~	-
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
jako	jako	k8xS	jako
doplnění	doplnění	k1gNnSc1	doplnění
rodinné	rodinný	k2eAgFnSc2d1	rodinná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
příprava	příprava	k1gFnSc1	příprava
na	na	k7c4	na
základní	základní	k2eAgNnSc4d1	základní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
jednak	jednak	k8xC	jednak
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
praxe	praxe	k1gFnSc2	praxe
(	(	kIx(	(
<g/>
pedagogové	pedagog	k1gMnPc1	pedagog
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
realizovali	realizovat	k5eAaBmAgMnP	realizovat
své	svůj	k3xOyFgInPc4	svůj
předpoklady	předpoklad	k1gInPc4	předpoklad
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovina	rovina	k1gFnSc1	rovina
teorie	teorie	k1gFnSc2	teorie
(	(	kIx(	(
<g/>
řešilo	řešit	k5eAaImAgNnS	řešit
se	se	k3xPyFc4	se
pojetí	pojetí	k1gNnSc1	pojetí
pedagogiky	pedagogika	k1gFnSc2	pedagogika
jako	jako	k8xC	jako
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
školské	školský	k2eAgFnSc2d1	školská
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
jednotná	jednotný	k2eAgFnSc1d1	jednotná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
učitelů	učitel	k1gMnPc2	učitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
Reformní	reformní	k2eAgNnSc4d1	reformní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
potřeba	potřeba	k1gFnSc1	potřeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
školskou	školský	k2eAgFnSc4d1	školská
reformu	reforma	k1gFnSc4	reforma
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
ustanoveny	ustanovit	k5eAaPmNgFnP	ustanovit
dvě	dva	k4xCgFnPc1	dva
komise	komise	k1gFnPc1	komise
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
vedená	vedený	k2eAgFnSc1d1	vedená
Václavem	Václav	k1gMnSc7	Václav
Příhodou	Příhoda	k1gMnSc7	Příhoda
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
Janem	Jan	k1gMnSc7	Jan
Uherem	uher	k1gInSc7	uher
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Příhoda	Příhoda	k1gMnSc1	Příhoda
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
globální	globální	k2eAgInSc4d1	globální
model	model	k1gInSc4	model
<g/>
"	"	kIx"	"
z	z	k7c2	z
USA	USA	kA	USA
(	(	kIx(	(
<g/>
učení	učení	k1gNnSc1	učení
bylo	být	k5eAaImAgNnS	být
shrnuto	shrnout	k5eAaPmNgNnS	shrnout
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
-	-	kIx~	-
předměty	předmět	k1gInPc1	předmět
nebyly	být	k5eNaImAgInP	být
diferenciované	diferenciovaný	k2eAgInPc4d1	diferenciovaný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čtení	čtení	k1gNnSc1	čtení
se	se	k3xPyFc4	se
neučilo	učit	k5eNaImAgNnS	učit
pomocí	pomocí	k7c2	pomocí
hláskování	hláskování	k1gNnSc2	hláskování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomocí	pomocí	k7c2	pomocí
zapamatování	zapamatování	k1gNnSc2	zapamatování
si	se	k3xPyFc3	se
slova	slovo	k1gNnSc2	slovo
jako	jako	k8xS	jako
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nefungovalo	fungovat	k5eNaImAgNnS	fungovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nenaučilo	naučit	k5eNaPmAgNnS	naučit
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
pedagogy	pedagog	k1gMnPc7	pedagog
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pomyslnou	pomyslný	k2eAgFnSc4d1	pomyslná
"	"	kIx"	"
<g/>
globální	globální	k2eAgFnSc4d1	globální
válku	válka	k1gFnSc4	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příhoda	Příhoda	k1gMnSc1	Příhoda
také	také	k9	také
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pedagogika	pedagogika	k1gFnSc1	pedagogika
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
přírodovědnou	přírodovědný	k2eAgFnSc7d1	přírodovědná
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Uher	Uher	k1gMnSc1	Uher
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
především	především	k9	především
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
obsahem	obsah	k1gInSc7	obsah
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neurčoval	určovat	k5eNaImAgMnS	určovat
jeho	jeho	k3xOp3gInSc4	jeho
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odpůrcem	odpůrce	k1gMnSc7	odpůrce
Příhodovy	Příhodův	k2eAgFnSc2d1	Příhodova
globální	globální	k2eAgFnSc2d1	globální
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
pedagogiku	pedagogika	k1gFnSc4	pedagogika
zařazoval	zařazovat	k5eAaImAgMnS	zařazovat
dle	dle	k7c2	dle
svého	svůj	k3xOyFgInSc2	svůj
názoru	názor	k1gInSc2	názor
mezi	mezi	k7c4	mezi
humanitní	humanitní	k2eAgFnPc4d1	humanitní
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
školské	školský	k2eAgFnSc2d1	školská
reformy	reforma	k1gFnSc2	reforma
nebyla	být	k5eNaImAgFnS	být
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
spor	spor	k1gInSc1	spor
Jana	Jan	k1gMnSc2	Jan
Uhera	uher	k1gMnSc2	uher
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Příhody	příhoda	k1gFnSc2	příhoda
přerušila	přerušit	k5eAaPmAgFnS	přerušit
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
však	však	k9	však
přece	přece	k9	přece
jen	jen	k9	jen
něco	něco	k3yInSc1	něco
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
pedagogům	pedagog	k1gMnPc3	pedagog
"	"	kIx"	"
<g/>
pokusníkům	pokusník	k1gMnPc3	pokusník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ida	Ida	k1gFnSc1	Ida
Jarníková	Jarníková	k1gFnSc1	Jarníková
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
prvním	první	k4xOgNnSc7	první
rámcovým	rámcový	k2eAgNnSc7d1	rámcové
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
podobně	podobně	k6eAd1	podobně
jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc4	ten
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
-	-	kIx~	-
od	od	k7c2	od
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
,	,	kIx,	,
od	od	k7c2	od
Vánoc	Vánoce	k1gFnPc2	Vánoce
do	do	k7c2	do
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
a	a	k8xC	a
od	od	k7c2	od
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
do	do	k7c2	do
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
podle	podle	k7c2	podle
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
a	a	k8xC	a
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
témata	téma	k1gNnPc4	téma
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
týdenní	týdenní	k2eAgInPc4d1	týdenní
bloky	blok	k1gInPc4	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Süssová	Süssová	k1gFnSc1	Süssová
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
prostory	prostor	k1gInPc4	prostor
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
jejím	její	k3xOp3gNnSc7	její
vybavením	vybavení	k1gNnSc7	vybavení
(	(	kIx(	(
<g/>
MŠ	MŠ	kA	MŠ
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
3	[number]	k4	3
prostory	prostora	k1gFnPc1	prostora
-	-	kIx~	-
hernu	herna	k1gFnSc4	herna
<g/>
,	,	kIx,	,
učebnu	učebna	k1gFnSc4	učebna
se	s	k7c7	s
stolečky	stoleček	k1gInPc7	stoleček
a	a	k8xC	a
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Nábytek	nábytek	k1gInSc1	nábytek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
přizpůsoben	přizpůsoben	k2eAgInSc4d1	přizpůsoben
dětem	dítě	k1gFnPc3	dítě
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvládaly	zvládat	k5eAaImAgFnP	zvládat
sebeobsluhu	sebeobsluh	k1gInSc2	sebeobsluh
-	-	kIx~	-
malé	malý	k2eAgFnPc1d1	malá
židle	židle	k1gFnPc1	židle
<g/>
,	,	kIx,	,
stoly	stol	k1gInPc1	stol
<g/>
,	,	kIx,	,
toalety	toaleta	k1gFnPc1	toaleta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
-	-	kIx~	-
možnost	možnost	k1gFnSc1	možnost
změny	změna	k1gFnSc2	změna
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vydán	vydat	k5eAaPmNgInS	vydat
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
jednotné	jednotný	k2eAgFnSc6d1	jednotná
škole	škola	k1gFnSc6	škola
<g/>
"	"	kIx"	"
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
jeslí	jesle	k1gFnPc2	jesle
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
předškolní	předškolní	k2eAgNnSc1d1	předškolní
vzdělání	vzdělání	k1gNnSc1	vzdělání
do	do	k7c2	do
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
prozatím	prozatím	k6eAd1	prozatím
byla	být	k5eAaImAgFnS	být
MŠ	MŠ	kA	MŠ
nepovinná	povinný	k2eNgNnPc4d1	nepovinné
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
kvalita	kvalita	k1gFnSc1	kvalita
předškolního	předškolní	k2eAgNnSc2d1	předškolní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
(	(	kIx(	(
<g/>
rozvoj	rozvoj	k1gInSc1	rozvoj
estetické	estetický	k2eAgFnSc2d1	estetická
a	a	k8xC	a
dramatické	dramatický	k2eAgFnSc2d1	dramatická
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
řada	řada	k1gFnSc1	řada
publikací	publikace	k1gFnPc2	publikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1965-1975	[number]	k4	1965-1975
se	se	k3xPyFc4	se
však	však	k9	však
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
typický	typický	k2eAgInSc1d1	typický
socialistický	socialistický	k2eAgInSc1d1	socialistický
přístup	přístup	k1gInSc1	přístup
(	(	kIx(	(
<g/>
socialistická	socialistický	k2eAgFnSc1d1	socialistická
ideologie	ideologie	k1gFnSc1	ideologie
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
do	do	k7c2	do
obsahu	obsah	k1gInSc2	obsah
předškolní	předškolní	k2eAgFnSc2d1	předškolní
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
dětem	dítě	k1gFnPc3	dítě
jsou	být	k5eAaImIp3nP	být
předčítány	předčítat	k5eAaImNgFnP	předčítat
sovětské	sovětský	k2eAgFnPc1d1	sovětská
pohádky	pohádka	k1gFnPc1	pohádka
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
vzdělávací	vzdělávací	k2eAgInPc1d1	vzdělávací
programy	program	k1gInPc1	program
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
MŠ	MŠ	kA	MŠ
totožné	totožný	k2eAgFnPc1d1	totožná
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
se	se	k3xPyFc4	se
však	však	k9	však
zásadně	zásadně	k6eAd1	zásadně
mění	měnit	k5eAaImIp3nS	měnit
struktura	struktura	k1gFnSc1	struktura
školství	školství	k1gNnSc2	školství
<g/>
:	:	kIx,	:
dosud	dosud	k6eAd1	dosud
klasicky	klasicky	k6eAd1	klasicky
nepovinná	povinný	k2eNgFnSc1d1	nepovinná
MŠ	MŠ	kA	MŠ
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ZŠ	ZŠ	kA	ZŠ
povinná	povinný	k2eAgFnSc1d1	povinná
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
-	-	kIx~	-
5	[number]	k4	5
let	léto	k1gNnPc2	léto
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
a	a	k8xC	a
4	[number]	k4	4
roky	rok	k1gInPc7	rok
druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
<g/>
,	,	kIx,	,
SŠ	SŠ	kA	SŠ
3	[number]	k4	3
nebo	nebo	k8xC	nebo
4	[number]	k4	4
roky	rok	k1gInPc7	rok
zakončena	zakončit	k5eAaPmNgFnS	zakončit
výučním	výuční	k2eAgInSc7d1	výuční
listem	list	k1gInSc7	list
nebo	nebo	k8xC	nebo
maturitou	maturita	k1gFnSc7	maturita
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
mylnou	mylný	k2eAgFnSc4d1	mylná
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
představou	představa	k1gFnSc7	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
urychlíme	urychlit	k5eAaPmIp1nP	urychlit
tělesný	tělesný	k2eAgInSc4d1	tělesný
rozvoj	rozvoj	k1gInSc4	rozvoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
urychlí	urychlit	k5eAaPmIp3nS	urychlit
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
složky	složka	k1gFnPc1	složka
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
psychický	psychický	k2eAgInSc1d1	psychický
rozvoj	rozvoj	k1gInSc1	rozvoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
upravena	upravit	k5eAaPmNgFnS	upravit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poslední	poslední	k2eAgInPc4d1	poslední
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
jsou	být	k5eAaImIp3nP	být
středoškolského	středoškolský	k2eAgInSc2d1	středoškolský
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
sice	sice	k8xC	sice
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
středoškolské	středoškolský	k2eAgNnSc1d1	středoškolské
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
učivo	učivo	k1gNnSc1	učivo
probírané	probíraný	k2eAgNnSc1d1	probírané
na	na	k7c6	na
ZŠ	ZŠ	kA	ZŠ
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
muselo	muset	k5eAaImAgNnS	muset
vměstnat	vměstnat	k5eAaPmF	vměstnat
jen	jen	k9	jen
do	do	k7c2	do
8	[number]	k4	8
let	léto	k1gNnPc2	léto
-	-	kIx~	-
první	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
byl	být	k5eAaImAgInS	být
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
z	z	k7c2	z
5	[number]	k4	5
let	léto	k1gNnPc2	léto
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Učiva	učivo	k1gNnPc1	učivo
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
zaostával	zaostávat	k5eAaImAgMnS	zaostávat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
třída	třída	k1gFnSc1	třída
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
MŠ	MŠ	kA	MŠ
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jeden	jeden	k4xCgMnSc1	jeden
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
)	)	kIx)	)
rok	rok	k1gInSc1	rok
v	v	k7c6	v
MŠ	MŠ	kA	MŠ
povinný	povinný	k2eAgMnSc1d1	povinný
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
usazovány	usazovat	k5eAaImNgFnP	usazovat
do	do	k7c2	do
lavic	lavice	k1gFnPc2	lavice
a	a	k8xC	a
učily	učít	k5eAaPmAgFnP	učít
se	se	k3xPyFc4	se
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
a	a	k8xC	a
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
předškolního	předškolní	k2eAgInSc2d1	předškolní
věku	věk	k1gInSc2	věk
značně	značně	k6eAd1	značně
přetěžovaly	přetěžovat	k5eAaImAgFnP	přetěžovat
(	(	kIx(	(
<g/>
nebyly	být	k5eNaImAgFnP	být
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
zralé	zralý	k2eAgFnPc1d1	zralá
ani	ani	k8xC	ani
připravené	připravený	k2eAgFnPc1d1	připravená
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
projevovaly	projevovat	k5eAaImAgFnP	projevovat
značné	značný	k2eAgInPc4d1	značný
psychické	psychický	k2eAgInPc4d1	psychický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odchodem	odchod	k1gInSc7	odchod
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
navázala	navázat	k5eAaPmAgFnS	navázat
předškolní	předškolní	k2eAgFnSc1d1	předškolní
pedagogika	pedagogika	k1gFnSc1	pedagogika
na	na	k7c6	na
období	období	k1gNnSc6	období
Reformního	reformní	k2eAgNnSc2d1	reformní
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
osobnostně	osobnostně	k6eAd1	osobnostně
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
cíl	cíl	k1gInSc4	cíl
předškolní	předškolní	k2eAgFnSc2d1	předškolní
výchovy	výchova	k1gFnSc2	výchova
(	(	kIx(	(
<g/>
individuální	individuální	k2eAgInSc4d1	individuální
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
dítěti	dítě	k1gNnSc3	dítě
<g/>
,	,	kIx,	,
empatie	empatie	k1gFnPc1	empatie
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
učitelky	učitelka	k1gFnSc2	učitelka
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
integrace	integrace	k1gFnPc1	integrace
dětí	dítě	k1gFnPc2	dítě
se	s	k7c7	s
speciálními	speciální	k2eAgFnPc7d1	speciální
potřebami	potřeba	k1gFnPc7	potřeba
a	a	k8xC	a
interkulturní	interkulturní	k2eAgFnSc1d1	interkulturní
výchova	výchova	k1gFnSc1	výchova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
alternativních	alternativní	k2eAgInPc2d1	alternativní
pedagogických	pedagogický	k2eAgInPc2d1	pedagogický
směrů	směr	k1gInPc2	směr
které	který	k3yRgFnPc1	který
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
dnes	dnes	k6eAd1	dnes
-	-	kIx~	-
např.	např.	kA	např.
Montessori	Montessori	k1gNnSc1	Montessori
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Walfdorská	Walfdorský	k2eAgFnSc1d1	Walfdorská
pedagogika	pedagogika	k1gFnSc1	pedagogika
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc4	projekt
Začít	začít	k5eAaPmF	začít
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
Zdravá	zdravý	k2eAgFnSc1d1	zdravá
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Eko	Eko	k1gFnSc1	Eko
školky	školka	k1gFnSc2	školka
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInPc1d1	lesní
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
předškolním	předškolní	k2eAgNnSc6d1	předškolní
<g/>
,	,	kIx,	,
základním	základní	k2eAgNnSc6d1	základní
<g/>
,	,	kIx,	,
středním	střední	k2eAgNnSc6d1	střední
<g/>
,	,	kIx,	,
vyšším	vysoký	k2eAgNnSc6d2	vyšší
odborném	odborný	k2eAgNnSc6d1	odborné
a	a	k8xC	a
jiném	jiný	k2eAgNnSc6d1	jiné
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
(	(	kIx(	(
<g/>
školským	školský	k2eAgInSc7d1	školský
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
předškolní	předškolní	k2eAgNnSc1d1	předškolní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
stalo	stát	k5eAaPmAgNnS	stát
legitimní	legitimní	k2eAgFnSc7d1	legitimní
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
počáteční	počáteční	k2eAgInSc1d1	počáteční
stupeň	stupeň	k1gInSc1	stupeň
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
a	a	k8xC	a
řízeného	řízený	k2eAgNnSc2d1	řízené
požadavky	požadavek	k1gInPc7	požadavek
a	a	k8xC	a
pokyny	pokyn	k1gInPc7	pokyn
MŠMT	MŠMT	kA	MŠMT
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
předškolního	předškolní	k2eAgNnSc2d1	předškolní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
týchž	týž	k3xTgFnPc6	týž
zásadách	zásada	k1gFnPc6	zásada
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
obory	obor	k1gInPc1	obor
a	a	k8xC	a
úrovně	úroveň	k1gFnPc1	úroveň
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
společnými	společný	k2eAgInPc7d1	společný
cíli	cíl	k1gInPc7	cíl
<g/>
:	:	kIx,	:
orientuje	orientovat	k5eAaBmIp3nS	orientovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc4	dítě
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
osvojovalo	osvojovat	k5eAaImAgNnS	osvojovat
základy	základ	k1gInPc4	základ
klíčových	klíčový	k2eAgFnPc2d1	klíčová
kompetencí	kompetence	k1gFnPc2	kompetence
a	a	k8xC	a
získávalo	získávat	k5eAaImAgNnS	získávat
tak	tak	k6eAd1	tak
předpoklady	předpoklad	k1gInPc4	předpoklad
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
mu	on	k3xPp3gMnSc3	on
se	s	k7c7	s
snáze	snadno	k6eAd2	snadno
a	a	k8xC	a
spolehlivěji	spolehlivě	k6eAd2	spolehlivě
uplatnit	uplatnit	k5eAaPmF	uplatnit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Předškolní	předškolní	k2eAgNnSc1d1	předškolní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
je	být	k5eAaImIp3nS	být
institucionálně	institucionálně	k6eAd1	institucionálně
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
mateřskými	mateřský	k2eAgFnPc7d1	mateřská
školami	škola	k1gFnPc7	škola
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
s	s	k7c7	s
upraveným	upravený	k2eAgInSc7d1	upravený
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
programem	program	k1gInSc7	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
je	být	k5eAaImIp3nS	být
realizováno	realizován	k2eAgNnSc1d1	realizováno
v	v	k7c6	v
přípravných	přípravný	k2eAgFnPc6d1	přípravná
třídách	třída	k1gFnPc6	třída
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
legislativně	legislativně	k6eAd1	legislativně
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
soustavy	soustava	k1gFnSc2	soustava
jako	jako	k8xS	jako
druh	druh	k1gInSc4	druh
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
organizaci	organizace	k1gFnSc6	organizace
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
řídí	řídit	k5eAaImIp3nS	řídit
obdobnými	obdobný	k2eAgNnPc7d1	obdobné
pravidly	pravidlo	k1gNnPc7	pravidlo
jako	jako	k9	jako
školy	škola	k1gFnPc1	škola
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Předškolní	předškolní	k2eAgNnSc1d1	předškolní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
se	se	k3xPyFc4	se
organizuje	organizovat	k5eAaBmIp3nS	organizovat
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
šesti	šest	k4xCc2	šest
(	(	kIx(	(
<g/>
sedmi	sedm	k4xCc2	sedm
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přednostně	přednostně	k6eAd1	přednostně
jsou	být	k5eAaImIp3nP	být
přijímány	přijímán	k2eAgFnPc1d1	přijímána
děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
povinné	povinný	k2eAgFnSc2d1	povinná
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
se	se	k3xPyFc4	se
organizačně	organizačně	k6eAd1	organizačně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zařazovat	zařazovat	k5eAaImF	zařazovat
děti	dítě	k1gFnPc4	dítě
stejného	stejný	k2eAgInSc2d1	stejný
či	či	k8xC	či
různého	různý	k2eAgInSc2d1	různý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
třídy	třída	k1gFnPc4	třída
věkově	věkově	k6eAd1	věkově
homogenní	homogenní	k2eAgFnSc1d1	homogenní
či	či	k8xC	či
věkově	věkově	k6eAd1	věkově
heterogenní	heterogenní	k2eAgFnSc1d1	heterogenní
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
běžných	běžný	k2eAgFnPc2d1	běžná
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
zařazovat	zařazovat	k5eAaImF	zařazovat
děti	dítě	k1gFnPc4	dítě
se	s	k7c7	s
speciálními	speciální	k2eAgFnPc7d1	speciální
vzdělávacími	vzdělávací	k2eAgFnPc7d1	vzdělávací
potřebami	potřeba	k1gFnPc7	potřeba
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
třídy	třída	k1gFnSc2	třída
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
<g/>
.	.	kIx.	.
</s>
<s>
Poskytování	poskytování	k1gNnSc1	poskytování
předškolního	předškolní	k2eAgNnSc2d1	předškolní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Předškoláci	předškolák	k1gMnPc1	předškolák
dostávají	dostávat	k5eAaImIp3nP	dostávat
obvykle	obvykle	k6eAd1	obvykle
svačiny	svačina	k1gFnPc4	svačina
a	a	k8xC	a
obědy	oběd	k1gInPc4	oběd
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
jídelně	jídelna	k1gFnSc6	jídelna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jí	on	k3xPp3gFnSc2	on
školka	školka	k1gFnSc1	školka
disponuje	disponovat	k5eAaBmIp3nS	disponovat
a	a	k8xC	a
pokud	pokud	k8xS	pokud
předškoláky	předškolák	k1gMnPc7	předškolák
rodiče	rodič	k1gMnSc2	rodič
přihlásí	přihlásit	k5eAaPmIp3nS	přihlásit
ke	k	k7c3	k
kolektivnímu	kolektivní	k2eAgNnSc3d1	kolektivní
stravování	stravování	k1gNnSc3	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
nosí	nosit	k5eAaImIp3nS	nosit
jídlo	jídlo	k1gNnSc4	jídlo
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
obvykle	obvykle	k6eAd1	obvykle
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
systému	systém	k1gInSc2	systém
K-12	K-12	k1gFnPc2	K-12
(	(	kIx(	(
<g/>
od	od	k7c2	od
Kindergarten	Kindergartno	k1gNnPc2	Kindergartno
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
školy	škola	k1gFnSc2	škola
-	-	kIx~	-
ukončení	ukončení	k1gNnSc1	ukončení
středního	střední	k2eAgNnSc2d1	střední
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
formálního	formální	k2eAgNnSc2d1	formální
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Ontario	Ontario	k1gNnSc1	Ontario
a	a	k8xC	a
Wisconsin	Wisconsin	k1gInSc4	Wisconsin
existují	existovat	k5eAaImIp3nP	existovat
2	[number]	k4	2
ročníky	ročník	k1gInPc7	ročník
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školy	škola	k1gFnSc2	škola
-	-	kIx~	-
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Junior	junior	k1gMnSc1	junior
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Senior	senior	k1gMnSc1	senior
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dítě	dítě	k1gNnSc1	dítě
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídou	třída	k1gFnSc7	třída
(	(	kIx(	(
<g/>
ročníkem	ročník	k1gInSc7	ročník
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
grade	grad	k1gInSc5	grad
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
"	"	kIx"	"
<g/>
stupeň	stupeň	k1gInSc1	stupeň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nejsou	být	k5eNaImIp3nP	být
částí	část	k1gFnSc7	část
školního	školní	k2eAgInSc2d1	školní
systému	systém	k1gInSc2	systém
jako	jako	k8xS	jako
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
překlad	překlad	k1gInSc1	překlad
"	"	kIx"	"
<g/>
předškola	předškola	k1gFnSc1	předškola
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
předškolní	předškolní	k2eAgNnSc1d1	předškolní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Vorschule	Vorschule	k1gFnSc1	Vorschule
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
snahy	snaha	k1gFnPc4	snaha
v	v	k7c6	v
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
různě	různě	k6eAd1	různě
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
německém	německý	k2eAgInSc6d1	německý
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
6	[number]	k4	6
let	léto	k1gNnPc2	léto
a	a	k8xC	a
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
městskou	městský	k2eAgFnSc7d1	městská
nebo	nebo	k8xC	nebo
obecní	obecní	k2eAgFnSc7d1	obecní
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Docházka	docházka	k1gFnSc1	docházka
není	být	k5eNaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
ani	ani	k8xC	ani
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
rok	rok	k1gInSc1	rok
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Walesu	Wales	k1gInSc2	Wales
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Recepce	recepce	k1gFnSc1	recepce
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
Rok	rok	k1gInSc1	rok
0	[number]	k4	0
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Předškolní	předškolní	k2eAgFnSc1d1	předškolní
péče	péče	k1gFnSc1	péče
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
částí	část	k1gFnSc7	část
školního	školní	k2eAgInSc2d1	školní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
nebo	nebo	k8xC	nebo
příležitostně	příležitostně	k6eAd1	příležitostně
Kindergarten	Kindergarten	k2eAgMnSc1d1	Kindergarten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
především	především	k9	především
pro	pro	k7c4	pro
marketingové	marketingový	k2eAgInPc4d1	marketingový
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
rok	rok	k1gInSc1	rok
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
nazývá	nazývat	k5eAaImIp3nS	nazývat
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Victoria	Victorium	k1gNnSc2	Victorium
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc1	synonymum
pro	pro	k7c4	pro
předškolní	předškolní	k2eAgNnSc4d1	předškolní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc1	teritorium
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
jiný	jiný	k2eAgInSc4d1	jiný
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgInSc1d1	ekvivalentní
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
you	you	k?	you
er	er	k?	er
yuan	yuana	k1gFnPc2	yuana
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
předškolní	předškolní	k2eAgNnPc4d1	předškolní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
let	léto	k1gNnPc2	léto
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
naučily	naučit	k5eAaPmAgInP	naučit
"	"	kIx"	"
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
<g/>
"	"	kIx"	"
adekvátně	adekvátně	k6eAd1	adekvátně
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
interagovat	interagovat	k5eAaBmF	interagovat
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
různé	různý	k2eAgInPc4d1	různý
názorné	názorný	k2eAgInPc4d1	názorný
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
aktivity	aktivita	k1gFnPc4	aktivita
pro	pro	k7c4	pro
motivaci	motivace	k1gFnSc4	motivace
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
učily	učit	k5eAaImAgInP	učit
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
slovíčka	slovíčko	k1gNnPc4	slovíčko
<g/>
,	,	kIx,	,
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
společenské	společenský	k2eAgNnSc4d1	společenské
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předtím	předtím	k6eAd1	předtím
strávily	strávit	k5eAaPmAgFnP	strávit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
značit	značit	k5eAaImF	značit
trénink	trénink	k1gInSc4	trénink
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
být	být	k5eAaImF	být
bez	bez	k7c2	bez
strachu	strach	k1gInSc2	strach
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
též	též	k9	též
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rodičům	rodič	k1gMnPc3	rodič
(	(	kIx(	(
<g/>
zvláště	zvláště	k9	zvláště
matkám	matka	k1gFnPc3	matka
<g/>
)	)	kIx)	)
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgFnPc1d1	mateřská
školy	škola	k1gFnPc1	škola
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
polodenní	polodenní	k2eAgNnSc1d1	polodenní
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgMnPc2	jenž
dítě	dítě	k1gNnSc1	dítě
přichází	přicházet	k5eAaImIp3nS	přicházet
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
domů	domů	k6eAd1	domů
po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
po	po	k7c6	po
o	o	k0	o
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
celodenní	celodenní	k2eAgFnSc1d1	celodenní
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dítě	dítě	k1gNnSc1	dítě
přichází	přicházet	k5eAaImIp3nS	přicházet
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
domů	dům	k1gInPc2	dům
až	až	k6eAd1	až
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
někde	někde	k6eAd1	někde
již	již	k6eAd1	již
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
další	další	k2eAgFnSc7d1	další
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc7	první
třídou	třída	k1gFnSc7	třída
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
soukromých	soukromý	k2eAgFnPc2d1	soukromá
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
USA	USA	kA	USA
nazývá	nazývat	k5eAaImIp3nS	nazývat
svou	svůj	k3xOyFgFnSc4	svůj
opatrovatelskou	opatrovatelský	k2eAgFnSc4d1	opatrovatelská
péči	péče	k1gFnSc4	péče
'	'	kIx"	'
<g/>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
Kindergarden	Kindergardno	k1gNnPc2	Kindergardno
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Vžil	vžít	k5eAaPmAgInS	vžít
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
termín	termín	k1gInSc1	termín
'	'	kIx"	'
<g/>
nursery	nursera	k1gFnPc1	nursera
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
nursery	nurser	k1gInPc1	nurser
school	schoola	k1gFnPc2	schoola
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
