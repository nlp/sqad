<s>
Varix	varix	k1gInSc1
je	být	k5eAaImIp3nS
žilní	žilní	k2eAgFnSc1d1
městek	městek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k8
následek	následek	k1gInSc1
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
přetěžování	přetěžování	k1gNnSc2
žíly	žíla	k1gFnSc2
nadměrným	nadměrný	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
</s>