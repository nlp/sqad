<s>
Varix	varix	k1gInSc1
</s>
<s>
Jícnové	jícnový	k2eAgInPc1d1
varixy	varix	k1gInPc1
při	při	k7c6
endoskopickém	endoskopický	k2eAgNnSc6d1
vyšetření	vyšetření	k1gNnSc6
</s>
<s>
Varix	varix	k1gInSc1
je	být	k5eAaImIp3nS
žilní	žilní	k2eAgFnSc1d1
městek	městek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k8
následek	následek	k1gInSc1
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
přetěžování	přetěžování	k1gNnSc2
žíly	žíla	k1gFnSc2
nadměrným	nadměrný	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žilní	žilní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
ochabuje	ochabovat	k5eAaImIp3nS
<g/>
,	,	kIx,
žíla	žíla	k1gFnSc1
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
a	a	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
poškozování	poškozování	k1gNnSc3
jejích	její	k3xOp3gFnPc2
chlopní	chlopeň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
zhoršený	zhoršený	k2eAgInSc4d1
odtok	odtok	k1gInSc4
krve	krev	k1gFnSc2
z	z	k7c2
žíly	žíla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Varixy	varix	k1gInPc1
vznikají	vznikat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
na	na	k7c6
dolních	dolní	k2eAgFnPc6d1
končetinách	končetina	k1gFnPc6
tzv.	tzv.	kA
křečové	křečový	k2eAgFnSc2d1
žíly	žíla	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
vyskytovat	vyskytovat	k5eAaImF
také	také	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
jícnu	jícen	k1gInSc2
tzv.	tzv.	kA
jícnové	jícnový	k2eAgInPc1d1
varixy	varix	k1gInPc1
a	a	k8xC
</s>
<s>
žaludku	žaludek	k1gInSc2
jako	jako	k9
důsledek	důsledek	k1gInSc1
portální	portální	k2eAgFnSc2d1
hypertenze	hypertenze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Varix	varix	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1
neručí	ručit	k5eNaImIp3nS
za	za	k7c4
správnost	správnost	k1gFnSc4
lékařských	lékařský	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
vyhledejte	vyhledat	k5eAaPmRp2nP
lékaře	lékař	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
prosím	prosit	k5eAaImIp1nS
pokyny	pokyn	k1gInPc4
pro	pro	k7c4
využití	využití	k1gNnSc4
článků	článek	k1gInPc2
o	o	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
