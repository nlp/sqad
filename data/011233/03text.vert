<p>
<s>
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
I.	I.	kA	I.
Komnenos	Komnenos	k1gInSc1	Komnenos
Laskaris	Laskaris	k1gInSc1	Laskaris
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
Theodor	Theodora	k1gFnPc2	Theodora
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Θ	Θ	k?	Θ
Α	Α	k?	Α
<g/>
'	'	kIx"	'
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
Theodō	Theodō	k1gFnSc1	Theodō
I	i	k8xC	i
Laskaris	Laskaris	k1gFnSc1	Laskaris
<g/>
,	,	kIx,	,
1174	[number]	k4	1174
<g/>
?	?	kIx.	?
<g/>
–	–	k?	–
<g/>
1222	[number]	k4	1222
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
magnátské	magnátský	k2eAgFnSc2d1	magnátský
rodiny	rodina	k1gFnSc2	rodina
Laskaridů	Laskarid	k1gInPc2	Laskarid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nikájským	nikájský	k2eAgMnSc7d1	nikájský
císařem	císař	k1gMnSc7	císař
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1205	[number]	k4	1205
(	(	kIx(	(
<g/>
1208	[number]	k4	1208
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
<g/>
1222	[number]	k4	1222
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
pocházel	pocházet	k5eAaImAgInS	pocházet
ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Laskaridů	Laskarid	k1gInPc2	Laskarid
z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
spravoval	spravovat	k5eAaImAgMnS	spravovat
dědičné	dědičný	k2eAgNnSc4d1	dědičné
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Konstantinopole	Konstantinopol	k1gInSc6	Konstantinopol
křižáky	křižák	k1gInPc4	křižák
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1204	[number]	k4	1204
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Konstantinem	Konstantin	k1gMnSc7	Konstantin
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Nikaii	Nikaie	k1gFnSc6	Nikaie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
císařství	císařství	k1gNnSc2	císařství
latinského	latinský	k2eAgNnSc2d1	latinské
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
dobytím	dobytí	k1gNnSc7	dobytí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
tamějším	tamější	k2eAgInSc6d1	tamější
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
císaře	císař	k1gMnSc2	císař
Alexia	Alexium	k1gNnSc2	Alexium
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Annou	Anna	k1gFnSc7	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
organizátorem	organizátor	k1gMnSc7	organizátor
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
křižákům	křižák	k1gMnPc3	křižák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1208	[number]	k4	1208
ho	on	k3xPp3gInSc4	on
konstantinopolský	konstantinopolský	k2eAgMnSc1d1	konstantinopolský
patriarcha	patriarcha	k1gMnSc1	patriarcha
Michael	Michael	k1gMnSc1	Michael
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Autoreanos	Autoreanos	k1gInSc1	Autoreanos
korunoval	korunovat	k5eAaBmAgInS	korunovat
na	na	k7c4	na
císaře	císař	k1gMnSc4	císař
římského	římský	k2eAgInSc2d1	římský
(	(	kIx(	(
<g/>
byzantského	byzantský	k2eAgInSc2d1	byzantský
–	–	k?	–
basileus	basileus	k1gInSc1	basileus
ton	ton	k?	ton
Romanion	Romanion	k1gInSc1	Romanion
<g/>
)	)	kIx)	)
a	a	k8xC	a
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
,	,	kIx,	,
připravující	připravující	k2eAgFnSc6d1	připravující
se	se	k3xPyFc4	se
dobýt	dobýt	k5eAaPmF	dobýt
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
zpět	zpět	k6eAd1	zpět
(	(	kIx(	(
<g/>
byzantská	byzantský	k2eAgFnSc1d1	byzantská
reconquista	reconquista	k1gMnSc1	reconquista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikájští	Nikájský	k2eAgMnPc1d1	Nikájský
císařové	císař	k1gMnPc1	císař
postupně	postupně	k6eAd1	postupně
odnímali	odnímat	k5eAaImAgMnP	odnímat
maloasijská	maloasijský	k2eAgNnPc1d1	maloasijské
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
balkánská	balkánský	k2eAgNnPc4d1	balkánské
území	území	k1gNnPc4	území
Latinům	Latin	k1gMnPc3	Latin
(	(	kIx(	(
<g/>
křižákům	křižák	k1gMnPc3	křižák
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k9	tak
základnu	základna	k1gFnSc4	základna
pro	pro	k7c4	pro
dobytí	dobytí	k1gNnSc4	dobytí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Theodora	Theodora	k1gFnSc1	Theodora
však	však	k9	však
rovněž	rovněž	k9	rovněž
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
na	na	k7c6	na
východních	východní	k2eAgFnPc6d1	východní
hranicích	hranice	k1gFnPc6	hranice
Seldžukové	Seldžukový	k2eAgNnSc1d1	Seldžukový
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zabezpečil	zabezpečit	k5eAaPmAgInS	zabezpečit
vytyčením	vytyčení	k1gNnSc7	vytyčení
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Rúmským	Rúmský	k2eAgInSc7d1	Rúmský
sultanátem	sultanát	k1gInSc7	sultanát
a	a	k8xC	a
Nikajským	Nikajský	k2eAgNnSc7d1	Nikajské
císařstvím	císařství	k1gNnSc7	císařství
roku	rok	k1gInSc2	rok
1210	[number]	k4	1210
<g/>
–	–	k?	–
<g/>
1211	[number]	k4	1211
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Theodora	Theodora	k1gFnSc1	Theodora
I.	I.	kA	I.
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
část	část	k1gFnSc1	část
severního	severní	k2eAgNnSc2d1	severní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
panství	panství	k1gNnSc1	panství
tedy	tedy	k9	tedy
strategicky	strategicky	k6eAd1	strategicky
zaujímalo	zaujímat	k5eAaImAgNnS	zaujímat
významnou	významný	k2eAgFnSc4d1	významná
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Nikaia	Nikaia	k1gFnSc1	Nikaia
se	se	k3xPyFc4	se
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
Theodorovy	Theodorův	k2eAgFnSc2d1	Theodorova
vlády	vláda	k1gFnSc2	vláda
stala	stát	k5eAaPmAgFnS	stát
spojencem	spojenec	k1gMnSc7	spojenec
Bulharského	bulharský	k2eAgNnSc2d1	bulharské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
bojujícího	bojující	k2eAgMnSc2d1	bojující
proti	proti	k7c3	proti
Latinům	Latin	k1gMnPc3	Latin
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
cara	car	k1gMnSc2	car
Kalojana	Kalojan	k1gMnSc2	Kalojan
a	a	k8xC	a
nástupem	nástup	k1gInSc7	nástup
jeho	on	k3xPp3gMnSc2	on
synovce	synovec	k1gMnSc2	synovec
Borila	Boril	k1gMnSc2	Boril
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
–	–	k?	–
roku	rok	k1gInSc2	rok
1211	[number]	k4	1211
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Theodorovi	Theodor	k1gMnSc3	Theodor
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
i	i	k8xC	i
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
po	po	k7c6	po
boku	bok	k1gInSc6	bok
latinského	latinský	k2eAgMnSc2d1	latinský
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Flanderského	flanderský	k2eAgMnSc2d1	flanderský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
smlouvu	smlouva	k1gFnSc4	smlouva
podstatně	podstatně	k6eAd1	podstatně
omezující	omezující	k2eAgInSc1d1	omezující
vliv	vliv	k1gInSc1	vliv
Latinů	Latin	k1gMnPc2	Latin
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
politice	politika	k1gFnSc6	politika
prováděl	provádět	k5eAaImAgMnS	provádět
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
konfiskace	konfiskace	k1gFnSc2	konfiskace
majetků	majetek	k1gInPc2	majetek
konstantinopolského	konstantinopolský	k2eAgInSc2d1	konstantinopolský
patriarchátu	patriarchát	k1gInSc2	patriarchát
a	a	k8xC	a
také	také	k9	také
pozemkové	pozemkový	k2eAgFnSc2d1	pozemková
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
s	s	k7c7	s
císařovou	císařův	k2eAgFnSc7d1	císařova
politikou	politika	k1gFnSc7	politika
proti	proti	k7c3	proti
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Půdu	půda	k1gFnSc4	půda
propůjčoval	propůjčovat	k5eAaImAgMnS	propůjčovat
svým	svůj	k3xOyFgMnPc3	svůj
úředníkům	úředník	k1gMnPc3	úředník
<g/>
,	,	kIx,	,
vojákům	voják	k1gMnPc3	voják
sobě	se	k3xPyFc3	se
oddaným	oddaný	k2eAgInPc3d1	oddaný
či	či	k8xC	či
maloasijským	maloasijský	k2eAgInPc3d1	maloasijský
klášterům	klášter	k1gInPc3	klášter
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
opěrný	opěrný	k2eAgInSc4d1	opěrný
bod	bod	k1gInSc4	bod
své	svůj	k3xOyFgFnSc2	svůj
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
později	pozdě	k6eAd2	pozdě
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
manžel	manžel	k1gMnSc1	manžel
Theodorovy	Theodorův	k2eAgFnSc2d1	Theodorova
dcery	dcera	k1gFnSc2	dcera
Ireny	Irena	k1gFnSc2	Irena
Ioannes	Ioannes	k1gMnSc1	Ioannes
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dukas	Dukas	k1gInSc1	Dukas
Vatatzés	Vatatzés	k1gInSc1	Vatatzés
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
Theodorem	Theodor	k1gMnSc7	Theodor
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
skonu	skon	k1gInSc2	skon
zletilého	zletilý	k2eAgMnSc2d1	zletilý
syna	syn	k1gMnSc2	syn
<g/>
)	)	kIx)	)
určen	určit	k5eAaPmNgInS	určit
za	za	k7c4	za
nástupce	nástupce	k1gMnPc4	nástupce
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1222	[number]	k4	1222
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
císařem	císař	k1gMnSc7	císař
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ioannes	Ioannes	k1gInSc1	Ioannes
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
dráze	dráha	k1gFnSc6	dráha
nastoupené	nastoupený	k2eAgFnSc2d1	nastoupená
Theodorem	Theodor	k1gMnSc7	Theodor
nejen	nejen	k6eAd1	nejen
doma	doma	k6eAd1	doma
–	–	k?	–
spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
Bulhary	Bulhar	k1gMnPc7	Bulhar
a	a	k8xC	a
zmocňováním	zmocňování	k1gNnPc3	zmocňování
se	se	k3xPyFc4	se
území	území	k1gNnSc4	území
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
reconquistu	reconquist	k1gInSc2	reconquist
<g/>
,	,	kIx,	,
započatou	započatý	k2eAgFnSc4d1	započatá
císařem	císař	k1gMnSc7	císař
Theodorem	Theodor	k1gMnSc7	Theodor
I.	I.	kA	I.
Laskarisem	Laskaris	k1gInSc7	Laskaris
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
1199	[number]	k4	1199
Anna	Anna	k1gFnSc1	Anna
Komnena	Komnena	k1gFnSc1	Komnena
Angelina	Angelin	k2eAgFnSc1d1	Angelina
</s>
</p>
<p>
<s>
Nikolaos	Nikolaos	k1gInSc1	Nikolaos
Laskaris	Laskaris	k1gFnSc2	Laskaris
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1212	[number]	k4	1212
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ioannes	Ioannes	k1gInSc1	Ioannes
Laskaris	Laskaris	k1gFnSc2	Laskaris
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1212	[number]	k4	1212
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Irena	Irena	k1gFnSc1	Irena
Laskarina	Laskarina	k1gFnSc1	Laskarina
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1239	[number]	k4	1239
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikajská	nikajský	k2eAgFnSc1d1	nikajský
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
choť	choť	k1gFnSc1	choť
Ioanna	Ioanno	k1gNnSc2	Ioanno
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Duky	Duka	k1gFnPc1	Duka
Vatatza	Vatatz	k1gMnSc2	Vatatz
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Laskarina	Laskarina	k1gFnSc1	Laskarina
(	(	kIx(	(
<g/>
1206	[number]	k4	1206
<g/>
–	–	k?	–
<g/>
1270	[number]	k4	1270
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherská	uherský	k2eAgFnSc1d1	uherská
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
choť	choť	k1gMnSc1	choť
Bély	Béla	k1gMnSc2	Béla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eudokie	Eudokie	k1gFnSc1	Eudokie
Laskarina	Laskarina	k1gFnSc1	Laskarina
<g/>
,	,	kIx,	,
choť	choť	k1gMnSc1	choť
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Babenberského	babenberský	k2eAgNnSc2d1	babenberské
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
1214	[number]	k4	1214
Filipa	Filip	k1gMnSc2	Filip
Arménská	arménský	k2eAgNnPc4d1	arménské
</s>
</p>
<p>
<s>
Konstantinos	Konstantinos	k1gInSc1	Konstantinos
Laskaris	Laskaris	k1gFnSc2	Laskaris
<g/>
,	,	kIx,	,
thrácký	thrácký	k2eAgMnSc1d1	thrácký
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
manželství	manželství	k1gNnSc1	manželství
∞	∞	k?	∞
(	(	kIx(	(
<g/>
1219	[number]	k4	1219
<g/>
)	)	kIx)	)
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
Courtenay	Courtenaa	k1gFnSc2	Courtenaa
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DRŠKA	drška	k1gFnSc1	drška
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
PICKOVÁ	Picková	k1gFnSc1	Picková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Aleš	Aleš	k1gMnSc1	Aleš
Skřivan	Skřivan	k1gMnSc1	Skřivan
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86493	[number]	k4	86493
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZÁSTĚROVÁ	Zástěrová	k1gFnSc1	Zástěrová
<g/>
,	,	kIx,	,
Bohumila	Bohumila	k1gFnSc1	Bohumila
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
454	[number]	k4	454
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRADEČNÝ	HRADEČNÝ	kA	HRADEČNÝ
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
192	[number]	k4	192
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
I.	I.	kA	I.
Laskaris	Laskaris	k1gFnSc2	Laskaris
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Laskarové	Laskarové	k2eAgFnSc1d1	Laskarové
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
byzantských	byzantský	k2eAgMnPc2d1	byzantský
císařů	císař	k1gMnPc2	císař
</s>
</p>
<p>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
</s>
</p>
