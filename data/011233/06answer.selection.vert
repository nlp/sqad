<s>
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
I.	I.	kA	I.
Komnenos	Komnenos	k1gInSc1	Komnenos
Laskaris	Laskaris	k1gInSc1	Laskaris
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
Theodor	Theodora	k1gFnPc2	Theodora
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Θ	Θ	k?	Θ
Α	Α	k?	Α
<g/>
'	'	kIx"	'
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
Theodō	Theodō	k1gFnSc1	Theodō
I	i	k8xC	i
Laskaris	Laskaris	k1gFnSc1	Laskaris
<g/>
,	,	kIx,	,
1174	[number]	k4	1174
<g/>
?	?	kIx.	?
<g/>
–	–	k?	–
<g/>
1222	[number]	k4	1222
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
magnátské	magnátský	k2eAgFnSc2d1	magnátský
rodiny	rodina	k1gFnSc2	rodina
Laskaridů	Laskarid	k1gInPc2	Laskarid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nikájským	nikájský	k2eAgMnSc7d1	nikájský
císařem	císař	k1gMnSc7	císař
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1205	[number]	k4	1205
(	(	kIx(	(
<g/>
1208	[number]	k4	1208
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
<g/>
1222	[number]	k4	1222
<g/>
.	.	kIx.	.
</s>
