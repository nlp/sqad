<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Hejma	Hejmum	k1gNnSc2	Hejmum
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1951	[number]	k4	1951
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
Žlutý	žlutý	k2eAgMnSc1d1	žlutý
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
tlumočník	tlumočník	k1gMnSc1	tlumočník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
agentury	agentura	k1gFnSc2	agentura
Associated	Associated	k1gMnSc1	Associated
Press	Press	k1gInSc1	Press
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přispíval	přispívat	k5eAaImAgInS	přispívat
i	i	k9	i
do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
čtyři	čtyři	k4xCgFnPc4	čtyři
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
i	i	k8xC	i
z	z	k7c2	z
televizních	televizní	k2eAgFnPc2d1	televizní
soutěží	soutěž	k1gFnPc2	soutěž
jako	jako	k9	jako
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
nebo	nebo	k8xC	nebo
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
gymnáziu	gymnázium	k1gNnSc6	gymnázium
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
filologii	filologie	k1gFnSc4	filologie
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
čínština	čínština	k1gFnSc1	čínština
<g/>
)	)	kIx)	)
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
univerzitě	univerzita	k1gFnSc6	univerzita
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
reklamní	reklamní	k2eAgFnSc6d1	reklamní
agentuře	agentura	k1gFnSc6	agentura
a	a	k8xC	a
následně	následně	k6eAd1	následně
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
svobodného	svobodný	k2eAgNnSc2d1	svobodné
povolání	povolání	k1gNnSc2	povolání
jako	jako	k8xC	jako
tlumočník	tlumočník	k1gMnSc1	tlumočník
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pražským	pražský	k2eAgInSc7d1	pražský
zpravodajem	zpravodaj	k1gInSc7	zpravodaj
agentury	agentura	k1gFnSc2	agentura
Associated	Associated	k1gInSc1	Associated
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
Pokrýval	pokrývat	k5eAaImAgMnS	pokrývat
zejména	zejména	k9	zejména
klíčové	klíčový	k2eAgFnPc4d1	klíčová
chvíle	chvíle	k1gFnPc4	chvíle
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
základní	základní	k2eAgNnSc4d1	základní
období	období	k1gNnSc4	období
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
hudbě	hudba	k1gFnSc3	hudba
i	i	k8xC	i
psaní	psaní	k1gNnSc3	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Hejmová	Hejmový	k2eAgFnSc1d1	Hejmový
je	být	k5eAaImIp3nS	být
lékařka	lékařka	k1gFnSc1	lékařka
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
staršího	starý	k2eAgMnSc4d2	starší
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
dceru	dcera	k1gFnSc4	dcera
Terezu	Tereza	k1gFnSc4	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
Řevnicích	řevnice	k1gFnPc6	řevnice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
účinkovat	účinkovat	k5eAaImF	účinkovat
ve	v	k7c6	v
folkbluesové	folkbluesový	k2eAgFnSc6d1	folkbluesový
formaci	formace	k1gFnSc6	formace
Ivana	Ivan	k1gMnSc2	Ivan
Hlase	hlas	k1gInSc6	hlas
Žízeň	žízeň	k1gFnSc4	žízeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vzniká	vznikat	k5eAaImIp3nS	vznikat
skupina	skupina	k1gFnSc1	skupina
Žlutý	žlutý	k2eAgInSc4d1	žlutý
pes	pes	k1gMnSc1	pes
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
Hejma	Hejma	k1gFnSc1	Hejma
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nahrála	nahrát	k5eAaBmAgFnS	nahrát
kapela	kapela	k1gFnSc1	kapela
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
obsahujícíh	obsahujícíha	k1gFnPc2	obsahujícíha
řadu	řad	k1gInSc2	řad
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
evergreenů	evergreen	k1gInPc2	evergreen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Hudební	hudební	k2eAgFnSc1d1	hudební
akademie	akademie	k1gFnSc1	akademie
jako	jako	k8xS	jako
kapela	kapela	k1gFnSc1	kapela
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
Žlutým	žlutý	k2eAgMnSc7d1	žlutý
psem	pes	k1gMnSc7	pes
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
i	i	k9	i
v	v	k7c6	v
alternativním	alternativní	k2eAgInSc6d1	alternativní
projektu	projekt	k1gInSc6	projekt
L.	L.	kA	L.
L.	L.	kA	L.
Jetel	jetel	k1gInSc1	jetel
s	s	k7c7	s
Radimem	Radim	k1gMnSc7	Radim
Hladíkem	Hladík	k1gMnSc7	Hladík
a	a	k8xC	a
Ernoušem	Ernouš	k1gMnSc7	Ernouš
Šedivým	Šedivý	k1gMnSc7	Šedivý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
v	v	k7c6	v
akustickém	akustický	k2eAgNnSc6d1	akustické
sdružení	sdružení	k1gNnSc6	sdružení
Marush	Marusha	k1gFnPc2	Marusha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
a	a	k8xC	a
knihy	kniha	k1gFnPc1	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
Hejma	Hejmum	k1gNnSc2	Hejmum
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Štefanem	Štefan	k1gMnSc7	Štefan
Rybárem	Rybár	k1gMnSc7	Rybár
vydal	vydat	k5eAaPmAgMnS	vydat
cestopis	cestopis	k1gInSc4	cestopis
Autostopem	autostop	k1gInSc7	autostop
do	do	k7c2	do
Nepálu	Nepál	k1gInSc2	Nepál
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
tlumočník	tlumočník	k1gMnSc1	tlumočník
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
Ústřední	ústřední	k2eAgFnSc4d1	ústřední
půjčovnu	půjčovna	k1gFnSc4	půjčovna
filmů	film	k1gInPc2	film
opatřoval	opatřovat	k5eAaImAgMnS	opatřovat
anglicky	anglicky	k6eAd1	anglicky
mluvené	mluvený	k2eAgInPc4d1	mluvený
filmy	film	k1gInPc4	film
českými	český	k2eAgInPc7d1	český
titulky	titulek	k1gInPc7	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
samizdatové	samizdatový	k2eAgFnSc2d1	samizdatová
sítě	síť	k1gFnSc2	síť
filmových	filmový	k2eAgMnPc2d1	filmový
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
dovážené	dovážený	k2eAgFnSc2d1	dovážená
videokazety	videokazeta	k1gFnSc2	videokazeta
opatřoval	opatřovat	k5eAaImAgInS	opatřovat
amatérským	amatérský	k2eAgInSc7d1	amatérský
rychlodabingem	rychlodabing	k1gInSc7	rychlodabing
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
jako	jako	k8xC	jako
moderátor	moderátor	k1gInSc1	moderátor
televizních	televizní	k2eAgFnPc2d1	televizní
a	a	k8xC	a
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
politických	politický	k2eAgFnPc2d1	politická
debat	debata	k1gFnPc2	debata
v	v	k7c6	v
TV3	TV3	k1gFnSc6	TV3
(	(	kIx(	(
<g/>
politická	politický	k2eAgFnSc1d1	politická
debata	debata	k1gFnSc1	debata
Bez	bez	k7c2	bez
kravaty	kravata	k1gFnSc2	kravata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
(	(	kIx(	(
<g/>
pořad	pořad	k1gInSc1	pořad
Press	Press	k1gInSc1	Press
klub	klub	k1gInSc1	klub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Reflex	reflex	k1gInSc1	reflex
(	(	kIx(	(
<g/>
rubrika	rubrika	k1gFnSc1	rubrika
Psí	psí	k2eAgFnSc1d1	psí
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
uváděl	uvádět	k5eAaImAgInS	uvádět
pořad	pořad	k1gInSc1	pořad
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
porotce	porotce	k1gMnSc1	porotce
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
třetí	třetí	k4xOgFnSc3	třetí
řadě	řada	k1gFnSc3	řada
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
porotcem	porotce	k1gMnSc7	porotce
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc4	Česko
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Superstar	superstar	k1gFnSc1	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
Stream	Stream	k1gInSc1	Stream
kombinoval	kombinovat	k5eAaImAgInS	kombinovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
zprávy	zpráva	k1gFnPc4	zpráva
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Rap	rapa	k1gFnPc2	rapa
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
News	News	k1gInSc4	News
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
glosuje	glosovat	k5eAaBmIp3nS	glosovat
aktuální	aktuální	k2eAgFnPc4d1	aktuální
události	událost	k1gFnPc4	událost
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
XTV	XTV	kA	XTV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vydávat	vydávat	k5eAaPmF	vydávat
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
trilogii	trilogie	k1gFnSc4	trilogie
Fejsbuk	Fejsbuka	k1gFnPc2	Fejsbuka
<g/>
,	,	kIx,	,
Americký	americký	k2eAgInSc1d1	americký
Blues	blues	k1gInSc1	blues
a	a	k8xC	a
Srdce	srdce	k1gNnSc1	srdce
Zlomený	zlomený	k2eAgMnSc1d1	zlomený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Údajná	údajný	k2eAgFnSc1d1	údajná
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
StB	StB	k1gFnSc7	StB
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
komunistických	komunistický	k2eAgInPc6d1	komunistický
archivech	archiv	k1gInPc6	archiv
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
veden	vést	k5eAaImNgInS	vést
jako	jako	k8xS	jako
prověřovaná	prověřovaný	k2eAgFnSc1d1	prověřovaná
osoba	osoba	k1gFnSc1	osoba
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
Student	student	k1gMnSc1	student
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
jako	jako	k8xS	jako
agent	agent	k1gMnSc1	agent
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
Rony	ron	k1gInPc7	ron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
svazek	svazek	k1gInSc1	svazek
s	s	k7c7	s
patřičnými	patřičný	k2eAgInPc7d1	patřičný
důkazy	důkaz	k1gInPc7	důkaz
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nedochoval	dochovat	k5eNaPmAgMnS	dochovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
StB	StB	k1gFnSc1	StB
zničila	zničit	k5eAaPmAgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Hejma	Hejma	k1gFnSc1	Hejma
tuto	tento	k3xDgFnSc4	tento
archivní	archivní	k2eAgFnSc4d1	archivní
evidenci	evidence	k1gFnSc4	evidence
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
dezinformaci	dezinformace	k1gFnSc4	dezinformace
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
autobiografickém	autobiografický	k2eAgInSc6d1	autobiografický
románu	román	k1gInSc6	román
Fejsbuk	Fejsbuk	k1gInSc1	Fejsbuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Rybár	Rybár	k1gMnSc1	Rybár
<g/>
,	,	kIx,	,
Š.	Š.	kA	Š.
–	–	k?	–
Hejma	Hejma	k1gFnSc1	Hejma
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
:	:	kIx,	:
Autostopem	autostop	k1gInSc7	autostop
do	do	k7c2	do
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hejma	Hejma	k1gFnSc1	Hejma
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
:	:	kIx,	:
Fejsbuk	Fejsbuk	k1gMnSc1	Fejsbuk
<g/>
,	,	kIx,	,
EMINENT	EMINENT	kA	EMINENT
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hejma	Hejma	k1gFnSc1	Hejma
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
:	:	kIx,	:
Americký	americký	k2eAgInSc1d1	americký
blues	blues	k1gInSc1	blues
<g/>
,	,	kIx,	,
FRAGMENT	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hejma	Hejma	k1gFnSc1	Hejma
<g/>
,	,	kIx,	,
O.	O.	kA	O.
<g/>
:	:	kIx,	:
Srdce	srdce	k1gNnPc1	srdce
zlomený	zlomený	k2eAgInSc1d1	zlomený
<g/>
,	,	kIx,	,
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Žlutý	žlutý	k2eAgMnSc1d1	žlutý
pes	pes	k1gMnSc1	pes
–	–	k?	–
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
</s>
</p>
<p>
<s>
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
Soutěž	soutěž	k1gFnSc1	soutěž
</s>
</p>
<p>
<s>
Mullet	Mullet	k1gInSc1	Mullet
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ondřej	Ondřej	k1gMnSc1	Ondřej
Hejma	Hejma	k1gNnSc4	Hejma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ondřej	Ondřej	k1gMnSc1	Ondřej
Hejma	Hejma	k1gNnSc4	Hejma
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Žlutý	žlutý	k2eAgMnSc1d1	žlutý
pes	pes	k1gMnSc1	pes
</s>
</p>
