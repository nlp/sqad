<p>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
či	či	k8xC	či
kosmos	kosmos	k1gInSc1	kosmos
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
κ	κ	k?	κ
<g/>
,	,	kIx,	,
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
,	,	kIx,	,
šperk	šperk	k1gInSc1	šperk
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
také	také	k9	také
vše	všechen	k3xTgNnSc1	všechen
uspořádané	uspořádaný	k2eAgNnSc1d1	uspořádané
<g/>
,	,	kIx,	,
řádné	řádný	k2eAgNnSc1d1	řádné
a	a	k8xC	a
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
veškeré	veškerý	k3xTgFnSc2	veškerý
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	on	k3xPp3gNnSc4	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tedy	tedy	k9	tedy
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
,	,	kIx,	,
mezigalaktický	mezigalaktický	k2eAgInSc1d1	mezigalaktický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
temnou	temný	k2eAgFnSc4d1	temná
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
také	také	k9	také
někdy	někdy	k6eAd1	někdy
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
část	část	k1gFnSc1	část
<g/>
/	/	kIx~	/
<g/>
díl	díl	k1gInSc1	díl
vesmíru	vesmír	k1gInSc2	vesmír
mimo	mimo	k7c4	mimo
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Různými	různý	k2eAgInPc7d1	různý
názory	názor	k1gInPc7	názor
na	na	k7c4	na
svět	svět	k1gInSc4	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
zabývaly	zabývat	k5eAaImAgFnP	zabývat
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
náboženství	náboženství	k1gNnPc1	náboženství
a	a	k8xC	a
filosofie	filosofie	k1gFnPc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
vědě	věda	k1gFnSc6	věda
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
zabývá	zabývat	k5eAaImIp3nS	zabývat
hlavně	hlavně	k9	hlavně
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
,	,	kIx,	,
kosmologie	kosmologie	k1gFnSc1	kosmologie
a	a	k8xC	a
astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
měření	měření	k1gNnSc3	měření
evropského	evropský	k2eAgInSc2d1	evropský
kosmického	kosmický	k2eAgInSc2d1	kosmický
dalekohledu	dalekohled	k1gInSc2	dalekohled
Planck	Planck	k1gMnSc1	Planck
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2009	[number]	k4	2009
a	a	k8xC	a
2013	[number]	k4	2013
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zpřesnil	zpřesnit	k5eAaPmAgInS	zpřesnit
odhad	odhad	k1gInSc1	odhad
stáří	stáří	k1gNnSc2	stáří
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
13,799	[number]	k4	13,799
<g/>
±	±	k?	±
<g/>
0,021	[number]	k4	0,021
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
asi	asi	k9	asi
75	[number]	k4	75
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
na	na	k7c4	na
Mpc	Mpc	k1gMnPc4	Mpc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
historie	historie	k1gFnSc2	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
kosmologií	kosmologie	k1gFnPc2	kosmologie
a	a	k8xC	a
kosmogonií	kosmogonie	k1gFnPc2	kosmogonie
pro	pro	k7c4	pro
pozorovatelný	pozorovatelný	k2eAgInSc4d1	pozorovatelný
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
geocentrické	geocentrický	k2eAgInPc1d1	geocentrický
modely	model	k1gInPc1	model
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
konečný	konečný	k2eAgMnSc1d1	konečný
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
věčně	věčně	k6eAd1	věčně
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
soubor	soubor	k1gInSc1	soubor
soustředných	soustředný	k2eAgFnPc2d1	soustředná
sfér	sféra	k1gFnPc2	sféra
konečných	konečný	k2eAgFnPc2d1	konečná
velikostí	velikost	k1gFnPc2	velikost
-	-	kIx~	-
které	který	k3yQgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
stálicím	stálice	k1gFnPc3	stálice
<g/>
,	,	kIx,	,
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
různým	různý	k2eAgFnPc3d1	různá
planetám	planeta	k1gFnPc3	planeta
-	-	kIx~	-
rotujících	rotující	k2eAgMnPc2d1	rotující
kolem	kolem	k7c2	kolem
kulaté	kulatý	k2eAgFnSc2d1	kulatá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehybné	hybný	k2eNgFnPc1d1	nehybná
Země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
přesnějším	přesný	k2eAgNnPc3d2	přesnější
měřením	měření	k1gNnPc3	měření
a	a	k8xC	a
lepším	dobrý	k2eAgFnPc3d2	lepší
teoriím	teorie	k1gFnPc3	teorie
gravitace	gravitace	k1gFnSc2	gravitace
vedl	vést	k5eAaImAgInS	vést
vývoj	vývoj	k1gInSc1	vývoj
k	k	k7c3	k
heliocentrickému	heliocentrický	k2eAgInSc3d1	heliocentrický
modelu	model	k1gInSc3	model
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
a	a	k8xC	a
k	k	k7c3	k
modelu	model	k1gInSc3	model
vesmíru	vesmír	k1gInSc2	vesmír
Isaaca	Isaac	k1gInSc2	Isaac
Newtona	Newton	k1gMnSc2	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
astronomie	astronomie	k1gFnSc2	astronomie
přinesl	přinést	k5eAaPmAgInS	přinést
poznání	poznání	k1gNnSc4	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
galaxie	galaxie	k1gFnSc2	galaxie
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
miliard	miliarda	k4xCgFnPc2	miliarda
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
Mléčné	mléčný	k2eAgFnPc1d1	mléčná
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
mimo	mimo	k7c4	mimo
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
astronomických	astronomický	k2eAgInPc2d1	astronomický
přístrojů	přístroj	k1gInPc2	přístroj
jiné	jiný	k2eAgFnSc2d1	jiná
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Pečlivé	pečlivý	k2eAgNnSc1d1	pečlivé
studium	studium	k1gNnSc1	studium
rozložení	rozložení	k1gNnSc2	rozložení
těchto	tento	k3xDgFnPc2	tento
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
moderní	moderní	k2eAgFnSc2d1	moderní
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
Objevy	objev	k1gInPc1	objev
rudého	rudý	k2eAgInSc2d1	rudý
posuvu	posuv	k1gInSc2	posuv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Edwinem	Edwin	k1gMnSc7	Edwin
Hubblem	Hubbl	k1gMnSc7	Hubbl
a	a	k8xC	a
reliktního	reliktní	k2eAgNnSc2d1	reliktní
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
Arnem	Arne	k1gMnSc7	Arne
Penziasem	Penzias	k1gMnSc7	Penzias
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Wilsonem	Wilson	k1gMnSc7	Wilson
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
patrně	patrně	k6eAd1	patrně
svůj	svůj	k3xOyFgInSc4	svůj
počátek	počátek	k1gInSc4	počátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
dnes	dnes	k6eAd1	dnes
převládajícího	převládající	k2eAgInSc2d1	převládající
vědeckého	vědecký	k2eAgInSc2d1	vědecký
modelu	model	k1gInSc2	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
jako	jako	k8xS	jako
Velký	velký	k2eAgInSc4d1	velký
třesk	třesk	k1gInSc4	třesk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
začal	začít	k5eAaPmAgInS	začít
rozpínat	rozpínat	k5eAaImF	rozpínat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Planckově	Planckův	k2eAgInSc6d1	Planckův
čase	čas	k1gInSc6	čas
z	z	k7c2	z
extrémně	extrémně	k6eAd1	extrémně
horkého	horký	k2eAgInSc2d1	horký
a	a	k8xC	a
hustého	hustý	k2eAgInSc2d1	hustý
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
soustředěna	soustředěn	k2eAgFnSc1d1	soustředěna
veškerá	veškerý	k3xTgFnSc1	veškerý
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Planckova	Planckův	k2eAgInSc2d1	Planckův
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
po	po	k7c4	po
velice	velice	k6eAd1	velice
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
32	[number]	k4	32
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
trvala	trvat	k5eAaImAgFnS	trvat
kosmická	kosmický	k2eAgFnSc1d1	kosmická
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
nezávislých	závislý	k2eNgInPc2d1	nezávislý
experimentálních	experimentální	k2eAgInPc2d1	experimentální
měření	měření	k1gNnSc1	měření
tuto	tento	k3xDgFnSc4	tento
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
inflaci	inflace	k1gFnSc4	inflace
i	i	k8xC	i
teorii	teorie	k1gFnSc4	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Nedávná	dávný	k2eNgNnPc1d1	nedávné
pozorování	pozorování	k1gNnPc1	pozorování
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
temné	temný	k2eAgFnSc3d1	temná
energii	energie	k1gFnSc3	energie
(	(	kIx(	(
<g/>
energii	energie	k1gFnSc3	energie
vakua	vakuum	k1gNnSc2	vakuum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
první	první	k4xOgFnSc6	první
data	datum	k1gNnPc4	datum
získal	získat	k5eAaPmAgInS	získat
v	v	k7c4	v
1933	[number]	k4	1933
švýcarsko-americký	švýcarskomerický	k2eAgMnSc1d1	švýcarsko-americký
astronom	astronom	k1gMnSc1	astronom
Fritz	Fritz	k1gMnSc1	Fritz
Zwicky	Zwicka	k1gFnSc2	Zwicka
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
současnými	současný	k2eAgInPc7d1	současný
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
v	v	k7c6	v
současných	současný	k2eAgInPc6d1	současný
modelech	model	k1gInPc6	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesnosti	nepřesnost	k1gFnPc1	nepřesnost
současných	současný	k2eAgNnPc2d1	současné
pozorování	pozorování	k1gNnPc2	pozorování
vesmíru	vesmír	k1gInSc2	vesmír
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
předpovědět	předpovědět	k5eAaPmF	předpovědět
konečný	konečný	k2eAgInSc4d1	konečný
osud	osud	k1gInSc4	osud
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
výklad	výklad	k1gInSc1	výklad
astronomických	astronomický	k2eAgNnPc2d1	astronomické
pozorování	pozorování	k1gNnPc2	pozorování
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stáří	stáří	k1gNnSc1	stáří
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
13,799	[number]	k4	13,799
±	±	k?	±
<g/>
0,021	[number]	k4	0,021
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
že	že	k8xS	že
průměr	průměr	k1gInSc4	průměr
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
93	[number]	k4	93
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
čili	čili	k8xC	čili
8,80	[number]	k4	8,80
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
</s>
</p>
<p>
<s>
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
prostor	prostora	k1gFnPc2	prostora
může	moct	k5eAaImIp3nS	moct
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
rychlostí	rychlost	k1gFnSc7	rychlost
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
omezené	omezený	k2eAgFnSc2d1	omezená
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nemůžeme	moct	k5eNaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
prostor	prostor	k1gInSc4	prostor
ve	v	k7c4	v
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
uletět	uletět	k5eAaPmF	uletět
světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
či	či	k8xC	či
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
jiné	jiný	k2eAgNnSc4d1	jiné
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
)	)	kIx)	)
od	od	k7c2	od
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
velikost	velikost	k1gFnSc1	velikost
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
nebo	nebo	k8xC	nebo
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
"	"	kIx"	"
<g/>
náš	náš	k3xOp1gInSc1	náš
<g/>
"	"	kIx"	"
vesmír	vesmír	k1gInSc1	vesmír
součástí	součást	k1gFnPc2	součást
systému	systém	k1gInSc2	systém
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
vesmírů	vesmír	k1gInPc2	vesmír
zvaného	zvaný	k2eAgInSc2d1	zvaný
multivesmír	multivesmíra	k1gFnPc2	multivesmíra
nebo	nebo	k8xC	nebo
mnohovesmír	mnohovesmíra	k1gFnPc2	mnohovesmíra
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
multiverse	multiverse	k1gFnSc2	multiverse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jiné	jiný	k2eAgInPc1d1	jiný
vesmíry	vesmír	k1gInPc1	vesmír
přitom	přitom	k6eAd1	přitom
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
než	než	k8xS	než
ten	ten	k3xDgInSc1	ten
náš	náš	k3xOp1gInSc4	náš
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
mnohovesmír	mnohovesmíra	k1gFnPc2	mnohovesmíra
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
sci-fi	scii	k1gFnSc6	sci-fi
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
<g/>
,	,	kIx,	,
synonyma	synonymum	k1gNnSc2	synonymum
a	a	k8xC	a
definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
vesmír	vesmír	k1gInSc1	vesmír
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ruského	ruský	k2eAgNnSc2d1	ruské
slova	slovo	k1gNnSc2	slovo
в	в	k?	в
м	м	k?	м
(	(	kIx(	(
<g/>
ves	ves	k1gFnSc1	ves
mir	mir	k1gInSc1	mir
–	–	k?	–
"	"	kIx"	"
<g/>
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
místo	místo	k6eAd1	místo
staročeského	staročeský	k2eAgMnSc4d1	staročeský
vesvět	vesvět	k5eAaPmF	vesvět
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4	pro
vesmír	vesmír	k1gInSc4	vesmír
(	(	kIx(	(
<g/>
universe	universe	k1gFnSc1	universe
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starofrancouzského	starofrancouzský	k2eAgNnSc2d1	starofrancouzské
slova	slovo	k1gNnSc2	slovo
univers	universum	k1gNnPc2	universum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
universum	universum	k1gNnSc1	universum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
používal	používat	k5eAaImAgMnS	používat
Cicero	Cicero	k1gMnSc1	Cicero
i	i	k8xC	i
pozdější	pozdní	k2eAgMnPc1d2	pozdější
autoři	autor	k1gMnPc1	autor
latinských	latinský	k2eAgInPc2d1	latinský
textů	text	k1gInPc2	text
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
slovo	slovo	k1gNnSc1	slovo
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
poetického	poetický	k2eAgNnSc2d1	poetické
zkrácení	zkrácení	k1gNnSc2	zkrácení
slova	slovo	k1gNnSc2	slovo
unvorsum	unvorsum	k1gNnSc4	unvorsum
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použitého	použitý	k2eAgInSc2d1	použitý
v	v	k7c6	v
Lukreciově	Lukreciův	k2eAgFnSc6d1	Lukreciův
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
rerum	rerum	k1gInSc4	rerum
natura	natur	k1gMnSc4	natur
(	(	kIx(	(
<g/>
O	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
IV	IV	kA	IV
<g/>
.262	.262	k4	.262
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgInSc1d1	alternativní
výklad	výklad	k1gInSc1	výklad
slova	slovo	k1gNnSc2	slovo
unvorsum	unvorsum	k1gNnSc1	unvorsum
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
otáčeno	otáčen	k2eAgNnSc1d1	otáčeno
jedním	jeden	k4xCgInSc7	jeden
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slovo	slovo	k1gNnSc4	slovo
překladem	překlad	k1gInSc7	překlad
staršího	starý	k2eAgNnSc2d2	starší
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
π	π	k?	π
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
něco	něco	k3yInSc4	něco
přepravovat	přepravovat	k5eAaImF	přepravovat
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
roznášení	roznášení	k1gNnSc1	roznášení
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
časné	časný	k2eAgInPc4d1	časný
řecké	řecký	k2eAgInPc4d1	řecký
modely	model	k1gInPc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
Platónovy	Platónův	k2eAgFnSc2d1	Platónova
metafory	metafora	k1gFnSc2	metafora
o	o	k7c6	o
Slunci	slunce	k1gNnSc6	slunce
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
uvažoval	uvažovat	k5eAaImAgInS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rotace	rotace	k1gFnSc1	rotace
nejvzdálenější	vzdálený	k2eAgFnSc2d3	nejvzdálenější
sféry	sféra	k1gFnSc2	sféra
stálic	stálice	k1gFnPc2	stálice
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Slunce	slunce	k1gNnSc2	slunce
působí	působit	k5eAaImIp3nS	působit
pohyb	pohyb	k1gInSc1	pohyb
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
celkem	celkem	k6eAd1	celkem
přirozeně	přirozeně	k6eAd1	přirozeně
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
nebe	nebe	k1gNnSc1	nebe
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
důmyslná	důmyslný	k2eAgNnPc4d1	důmyslné
astronomická	astronomický	k2eAgNnPc4d1	astronomické
a	a	k8xC	a
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
měření	měření	k1gNnSc4	měření
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Foucaultovo	Foucaultův	k2eAgNnSc4d1	Foucaultovo
kyvadlo	kyvadlo	k1gNnSc4	kyvadlo
<g/>
)	)	kIx)	)
musela	muset	k5eAaImAgFnS	muset
prokázat	prokázat	k5eAaPmF	prokázat
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
používaný	používaný	k2eAgInSc4d1	používaný
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vesmír	vesmír	k1gInSc4	vesmír
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
starověkými	starověký	k2eAgMnPc7d1	starověký
řeckými	řecký	k2eAgMnPc7d1	řecký
filozofy	filozof	k1gMnPc7	filozof
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Pythagora	Pythagoras	k1gMnSc2	Pythagoras
byl	být	k5eAaImAgMnS	být
τ	τ	k?	τ
π	π	k?	π
(	(	kIx(	(
<g/>
všechno	všechen	k3xTgNnSc4	všechen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definované	definovaný	k2eAgInPc1d1	definovaný
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
(	(	kIx(	(
<g/>
τ	τ	k?	τ
ὅ	ὅ	k?	ὅ
<g/>
)	)	kIx)	)
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
(	(	kIx(	(
<g/>
τ	τ	k?	τ
κ	κ	k?	κ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
synonyma	synonymum	k1gNnSc2	synonymum
vesmíru	vesmír	k1gInSc2	vesmír
u	u	k7c2	u
starověkých	starověký	k2eAgMnPc2d1	starověký
řeckých	řecký	k2eAgMnPc2d1	řecký
filozofů	filozof	k1gMnPc2	filozof
byla	být	k5eAaImAgFnS	být
κ	κ	k?	κ
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
kosmos	kosmos	k1gInSc1	kosmos
<g/>
)	)	kIx)	)
a	a	k8xC	a
φ	φ	k?	φ
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
živou	živý	k2eAgFnSc4d1	živá
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
slovo	slovo	k1gNnSc1	slovo
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
synonyma	synonymum	k1gNnPc1	synonymum
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
(	(	kIx(	(
<g/>
totum	totum	k1gInSc1	totum
<g/>
,	,	kIx,	,
mundus	mundus	k1gInSc1	mundus
<g/>
,	,	kIx,	,
natura	natura	k1gFnSc1	natura
<g/>
)	)	kIx)	)
a	a	k8xC	a
přežila	přežít	k5eAaPmAgFnS	přežít
i	i	k9	i
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
německá	německý	k2eAgFnSc1d1	německá
slova	slovo	k1gNnPc1	slovo
Das	Das	k1gMnSc1	Das
All	All	k1gMnSc1	All
<g/>
,	,	kIx,	,
das	das	k?	das
Weltall	Weltall	k1gInSc1	Weltall
a	a	k8xC	a
die	die	k?	die
Natur	Natura	k1gFnPc2	Natura
pro	pro	k7c4	pro
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
synonyma	synonymum	k1gNnPc1	synonymum
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
everything	everything	k1gInSc1	everything
(	(	kIx(	(
<g/>
v	v	k7c4	v
teorii	teorie	k1gFnSc4	teorie
všeho	všecek	k3xTgNnSc2	všecek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cosmos	cosmos	k1gMnSc1	cosmos
(	(	kIx(	(
<g/>
v	v	k7c6	v
kosmologii	kosmologie	k1gFnSc6	kosmologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
world	world	k1gMnSc1	world
(	(	kIx(	(
<g/>
hypotéza	hypotéza	k1gFnSc1	hypotéza
mnoha	mnoho	k4c2	mnoho
světů	svět	k1gInPc2	svět
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nature	natur	k1gMnSc5	natur
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgInPc1d1	přírodní
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc1d1	přírodní
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejširší	široký	k2eAgFnSc1d3	nejširší
definice	definice	k1gFnSc1	definice
<g/>
:	:	kIx,	:
realita	realita	k1gFnSc1	realita
a	a	k8xC	a
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
===	===	k?	===
</s>
</p>
<p>
<s>
Nejširší	široký	k2eAgFnSc4d3	nejširší
definici	definice	k1gFnSc4	definice
vesmíru	vesmír	k1gInSc2	vesmír
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
ve	v	k7c6	v
spise	spis	k1gInSc6	spis
De	De	k?	De
Divisione	Division	k1gInSc5	Division
naturae	naturae	k1gNnPc6	naturae
(	(	kIx(	(
<g/>
O	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
středověkého	středověký	k2eAgMnSc2d1	středověký
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
teologa	teolog	k1gMnSc2	teolog
Jana	Jan	k1gMnSc2	Jan
Scota	Scot	k1gMnSc2	Scot
Eriugeny	Eriugen	k1gInPc4	Eriugen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
vesmír	vesmír	k1gInSc4	vesmír
jako	jako	k8xC	jako
prostě	prostě	k6eAd1	prostě
vše	všechen	k3xTgNnSc4	všechen
<g/>
:	:	kIx,	:
všechno	všechen	k3xTgNnSc1	všechen
stvořené	stvořený	k2eAgNnSc1d1	stvořené
i	i	k9	i
všechno	všechen	k3xTgNnSc1	všechen
nestvořené	stvořený	k2eNgNnSc1d1	nestvořené
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Feynmanově	Feynmanův	k2eAgInSc6d1	Feynmanův
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
kvantové	kvantový	k2eAgFnSc3d1	kvantová
mechanice	mechanika	k1gFnSc3	mechanika
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
dráhového	dráhový	k2eAgInSc2d1	dráhový
integrálu	integrál	k1gInSc2	integrál
jsou	být	k5eAaImIp3nP	být
amplitudy	amplituda	k1gFnPc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
různých	různý	k2eAgInPc2d1	různý
výsledků	výsledek	k1gInPc2	výsledek
určitého	určitý	k2eAgInSc2d1	určitý
pokusu	pokus	k1gInSc2	pokus
-	-	kIx~	-
za	za	k7c2	za
přesně	přesně	k6eAd1	přesně
definovaného	definovaný	k2eAgInSc2d1	definovaný
počátečního	počáteční	k2eAgInSc2d1	počáteční
stavu	stav	k1gInSc2	stav
systému	systém	k1gInSc2	systém
-	-	kIx~	-
určeny	určit	k5eAaPmNgInP	určit
sumací	sumace	k1gFnSc7	sumace
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
možných	možný	k2eAgFnPc6d1	možná
historiích	historie	k1gFnPc6	historie
(	(	kIx(	(
<g/>
cestách	cesta	k1gFnPc6	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
mohl	moct	k5eAaImAgInS	moct
systém	systém	k1gInSc1	systém
dospět	dospět	k5eAaPmF	dospět
z	z	k7c2	z
počátečního	počáteční	k2eAgMnSc2d1	počáteční
do	do	k7c2	do
konečného	konečný	k2eAgInSc2d1	konečný
stavu	stav	k1gInSc2	stav
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
pokus	pokus	k1gInSc1	pokus
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc4d1	možný
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
skutečný	skutečný	k2eAgInSc1d1	skutečný
výsledek	výsledek	k1gInSc1	výsledek
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
proces	proces	k1gInSc4	proces
kvantového	kvantový	k2eAgNnSc2d1	kvantové
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
kolaps	kolaps	k1gInSc1	kolaps
vlnové	vlnový	k2eAgFnSc2d1	vlnová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dobře	dobře	k6eAd1	dobře
definovaném	definovaný	k2eAgInSc6d1	definovaný
matematickém	matematický	k2eAgInSc6d1	matematický
významu	význam	k1gInSc6	význam
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
neexistuje	existovat	k5eNaImIp3nS	existovat
(	(	kIx(	(
<g/>
všemi	všecek	k3xTgFnPc7	všecek
možnými	možný	k2eAgFnPc7d1	možná
cestami	cesta	k1gFnPc7	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
skutečně	skutečně	k6eAd1	skutečně
existuje	existovat	k5eAaImIp3nS	existovat
(	(	kIx(	(
<g/>
experimentální	experimentální	k2eAgNnSc1d1	experimentální
měření	měření	k1gNnSc1	měření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
každý	každý	k3xTgInSc1	každý
elektron	elektron	k1gInSc1	elektron
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
jiným	jiný	k2eAgInSc7d1	jiný
elektronem	elektron	k1gInSc7	elektron
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jev	jev	k1gInSc1	jev
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
symetrie	symetrie	k1gFnSc1	symetrie
výměny	výměna	k1gFnSc2	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k9	jak
existujícího	existující	k2eAgInSc2d1	existující
<g/>
,	,	kIx,	,
tak	tak	k9	tak
neexistujícího	existující	k2eNgMnSc2d1	neexistující
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
volnou	volný	k2eAgFnSc4d1	volná
paralelu	paralela	k1gFnSc4	paralela
v	v	k7c6	v
buddhistické	buddhistický	k2eAgFnSc6d1	buddhistická
doktríně	doktrína	k1gFnSc6	doktrína
šúnjata	šúnjata	k1gFnSc1	šúnjata
o	o	k7c6	o
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
vývoji	vývoj	k1gInSc6	vývoj
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
představě	představa	k1gFnSc6	představa
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Leibnize	Leibnize	k1gFnSc2	Leibnize
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
nekompatibilního	kompatibilní	k2eNgInSc2d1	nekompatibilní
<g/>
,	,	kIx,	,
nekonzistentního	konzistentní	k2eNgInSc2d1	nekonzistentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnSc2	definice
reality	realita	k1gFnSc2	realita
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
existovalo	existovat	k5eAaImAgNnS	existovat
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
našeho	náš	k3xOp1gNnSc2	náš
současného	současný	k2eAgNnSc2d1	současné
chápání	chápání	k1gNnSc2	chápání
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
principů	princip	k1gInPc2	princip
<g/>
:	:	kIx,	:
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
známého	známý	k2eAgNnSc2d1	známé
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
časoprostor	časoprostor	k1gInSc1	časoprostor
nebo	nebo	k8xC	nebo
vakuum	vakuum	k1gNnSc1	vakuum
<g/>
;	;	kIx,	;
forem	forma	k1gFnPc2	forma
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	on	k3xPp3gNnSc4	on
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
definicí	definice	k1gFnSc7	definice
pojmu	pojem	k1gInSc2	pojem
vesmíru	vesmír	k1gInSc2	vesmír
souvisí	souviset	k5eAaImIp3nS	souviset
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
okamžiku	okamžik	k1gInSc6	okamžik
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
věta	věta	k1gFnSc1	věta
"	"	kIx"	"
<g/>
Vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vyplněn	vyplněn	k2eAgInSc1d1	vyplněn
jednotným	jednotný	k2eAgNnSc7d1	jednotné
mikrovlnným	mikrovlnný	k2eAgNnSc7d1	mikrovlnné
zářením	záření	k1gNnSc7	záření
na	na	k7c6	na
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
principy	princip	k1gInPc1	princip
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
,	,	kIx,	,
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
<g/>
)	)	kIx)	)
zhruba	zhruba	k6eAd1	zhruba
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
představám	představa	k1gFnPc3	představa
Aristotelovým	Aristotelův	k2eAgFnPc3d1	Aristotelova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Fyzika	fyzika	k1gFnSc1	fyzika
(	(	kIx(	(
<g/>
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
odvozujeme	odvozovat	k5eAaImIp1nP	odvozovat
slovo	slovo	k1gNnSc1	slovo
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
dělí	dělit	k5eAaImIp3nS	dělit
τ	τ	k?	τ
π	π	k?	π
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc4	všechen
<g/>
)	)	kIx)	)
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
zhruba	zhruba	k6eAd1	zhruba
analogických	analogický	k2eAgFnPc2d1	analogická
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc3	tvar
(	(	kIx(	(
<g/>
uspořádání	uspořádání	k1gNnSc3	uspořádání
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
)	)	kIx)	)
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
(	(	kIx(	(
<g/>
stvoření	stvoření	k1gNnSc1	stvoření
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc1	zničení
nebo	nebo	k8xC	nebo
změna	změna	k1gFnSc1	změna
jeho	jeho	k3xOp3gFnPc2	jeho
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
jeho	on	k3xPp3gInSc2	on
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
jsou	být	k5eAaImIp3nP	být
koncipovány	koncipovat	k5eAaBmNgInP	koncipovat
jako	jako	k8xS	jako
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
vlastnosti	vlastnost	k1gFnPc4	vlastnost
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gInSc2	její
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
filozofové	filozof	k1gMnPc1	filozof
jako	jako	k9	jako
například	například	k6eAd1	například
Lucretius	Lucretius	k1gMnSc1	Lucretius
<g/>
,	,	kIx,	,
Averroes	Averroes	k1gMnSc1	Averroes
<g/>
,	,	kIx,	,
Avicenna	Avicenna	k1gFnSc1	Avicenna
nebo	nebo	k8xC	nebo
Baruch	Baruch	k1gInSc1	Baruch
Spinoza	Spinoz	k1gMnSc2	Spinoz
změnili	změnit	k5eAaPmAgMnP	změnit
či	či	k8xC	či
upřesnili	upřesnit	k5eAaPmAgMnP	upřesnit
jejich	jejich	k3xOp3gNnSc4	jejich
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Averroes	Averroes	k1gInSc1	Averroes
a	a	k8xC	a
Spinoza	Spinoza	k1gFnSc1	Spinoza
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
natura	natura	k1gFnSc1	natura
naturans	naturans	k1gInSc4	naturans
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnSc4d1	tvořící
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc4d1	aktivní
přírodu	příroda	k1gFnSc4	příroda
<g/>
)	)	kIx)	)
od	od	k7c2	od
natura	natur	k1gMnSc2	natur
naturata	naturat	k1gMnSc2	naturat
<g/>
,	,	kIx,	,
stvořené	stvořený	k2eAgFnSc2d1	stvořená
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnPc4	definice
oddělených	oddělený	k2eAgInPc2d1	oddělený
časoprostorů	časoprostor	k1gInPc2	časoprostor
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
oddělené	oddělený	k2eAgInPc4d1	oddělený
časoprostory	časoprostor	k1gInPc4	časoprostor
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
existující	existující	k2eAgInSc1d1	existující
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
spolu	spolu	k6eAd1	spolu
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
jako	jako	k8xS	jako
metaforu	metafora	k1gFnSc4	metafora
skupinu	skupina	k1gFnSc4	skupina
samostatných	samostatný	k2eAgFnPc2d1	samostatná
mýdlových	mýdlový	k2eAgFnPc2d1	mýdlová
bublin	bublina	k1gFnPc2	bublina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
žijí	žít	k5eAaImIp3nP	žít
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
mýdlové	mýdlový	k2eAgFnSc2d1	mýdlová
bubliny	bublina	k1gFnSc2	bublina
nemohou	moct	k5eNaImIp3nP	moct
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
mýdlových	mýdlový	k2eAgFnPc6d1	mýdlová
bublinách	bublina	k1gFnPc6	bublina
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
terminologie	terminologie	k1gFnSc2	terminologie
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
"	"	kIx"	"
<g/>
mýdlová	mýdlový	k2eAgFnSc1d1	mýdlová
bublina	bublina	k1gFnSc1	bublina
<g/>
"	"	kIx"	"
časoprostoru	časoprostor	k1gInSc6	časoprostor
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
náš	náš	k3xOp1gInSc1	náš
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
prostor	prostor	k1gInSc1	prostor
a	a	k8xC	a
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
náš	náš	k3xOp1gInSc1	náš
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
my	my	k3xPp1nPc1	my
máme	mít	k5eAaImIp1nP	mít
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
náš	náš	k3xOp1gInSc4	náš
měsíc	měsíc	k1gInSc4	měsíc
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
kolekce	kolekce	k1gFnSc1	kolekce
těchto	tento	k3xDgInPc2	tento
oddělených	oddělený	k2eAgInPc2d1	oddělený
prostorů	prostor	k1gInPc2	prostor
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
mnohovesmír	mnohovesmír	k1gMnSc1	mnohovesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spolu	spolu	k6eAd1	spolu
nespojené	spojený	k2eNgInPc1d1	nespojený
vesmíry	vesmír	k1gInPc1	vesmír
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
mít	mít	k5eAaImF	mít
odlišné	odlišný	k2eAgFnSc2d1	odlišná
dimenze	dimenze	k1gFnSc2	dimenze
a	a	k8xC	a
topologie	topologie	k1gFnSc2	topologie
prostoročasu	prostoročas	k1gInSc2	prostoročas
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
takové	takový	k3xDgFnPc1	takový
možnosti	možnost	k1gFnPc1	možnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jen	jen	k6eAd1	jen
spekulativní	spekulativní	k2eAgMnPc1d1	spekulativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Definice	definice	k1gFnPc4	definice
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
reality	realita	k1gFnSc2	realita
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
omezující	omezující	k2eAgFnSc1d1	omezující
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
vesmír	vesmír	k1gInSc4	vesmír
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
časoprostoru	časoprostor	k1gInSc6	časoprostor
a	a	k8xC	a
s	s	k7c7	s
čím	co	k3yRnSc7	co
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
interagovat	interagovat	k5eAaBmF	interagovat
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
některé	některý	k3yIgInPc1	některý
regiony	region	k1gInPc1	region
prostoru	prostor	k1gInSc2	prostor
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
naší	náš	k3xOp1gFnSc7	náš
částí	část	k1gFnSc7	část
prostoru	prostor	k1gInSc2	prostor
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
konečné	konečná	k1gFnSc3	konečná
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
pokračující	pokračující	k2eAgFnSc4d1	pokračující
expanzi	expanze	k1gFnSc4	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rádiové	rádiový	k2eAgInPc1d1	rádiový
signály	signál	k1gInPc1	signál
vyslané	vyslaný	k2eAgInPc1d1	vyslaný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
nikdy	nikdy	k6eAd1	nikdy
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
určité	určitý	k2eAgFnPc4d1	určitá
oblasti	oblast	k1gFnPc4	oblast
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k8xC	i
kdyby	kdyby	kYmCp3nP	kdyby
vesmír	vesmír	k1gInSc1	vesmír
existoval	existovat	k5eAaImAgInS	existovat
navěky	navěky	k6eAd1	navěky
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
oblasti	oblast	k1gFnPc1	oblast
vesmíru	vesmír	k1gInSc2	vesmír
existují	existovat	k5eAaImIp3nP	existovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
reality	realita	k1gFnSc2	realita
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
jich	on	k3xPp3gFnPc2	on
nemůžeme	moct	k5eNaImIp1nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
a	a	k8xC	a
které	který	k3yQgFnPc1	který
nás	my	k3xPp1nPc4	my
mohou	moct	k5eAaImIp3nP	moct
kauzálně	kauzálně	k6eAd1	kauzálně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Přísně	přísně	k6eAd1	přísně
vzato	vzít	k5eAaPmNgNnS	vzít
<g/>
,	,	kIx,	,
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Cestováním	cestování	k1gNnSc7	cestování
může	moct	k5eAaImIp3nS	moct
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
oblasti	oblast	k1gFnSc6	oblast
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
než	než	k8xS	než
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
:	:	kIx,	:
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
prvního	první	k4xOgMnSc4	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
pro	pro	k7c4	pro
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ani	ani	k8xC	ani
nejrychlejší	rychlý	k2eAgMnSc1d3	nejrychlejší
cestovatel	cestovatel	k1gMnSc1	cestovatel
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
komunikovat	komunikovat	k5eAaImF	komunikovat
se	se	k3xPyFc4	se
všemi	všecek	k3xTgFnPc7	všecek
částmi	část	k1gFnPc7	část
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pozorovatelným	pozorovatelný	k2eAgInSc7d1	pozorovatelný
vesmírem	vesmír	k1gInSc7	vesmír
rozumí	rozumět	k5eAaImIp3nS	rozumět
vesmír	vesmír	k1gInSc1	vesmír
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc1	stáří
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
zákony	zákon	k1gInPc1	zákon
vesmíru	vesmír	k1gInSc2	vesmír
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
vesmíru	vesmír	k1gInSc2	vesmír
viditelná	viditelný	k2eAgFnSc1d1	viditelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
koule	koule	k1gFnSc1	koule
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
přibližně	přibližně	k6eAd1	přibližně
46	[number]	k4	46
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
odhadů	odhad	k1gInPc2	odhad
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
96	[number]	k4	96
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
z	z	k7c2	z
nejvzdálenějších	vzdálený	k2eAgInPc2d3	nejvzdálenější
viditelných	viditelný	k2eAgInPc2d1	viditelný
objektů	objekt	k1gInPc2	objekt
se	s	k7c7	s
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
typická	typický	k2eAgFnSc1d1	typická
galaxie	galaxie	k1gFnSc1	galaxie
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
a	a	k8xC	a
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
sousedními	sousední	k2eAgFnPc7d1	sousední
galaxiemi	galaxie	k1gFnPc7	galaxie
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
milióny	milión	k4xCgInPc7	milión
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
galaxie	galaxie	k1gFnPc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozorovaném	pozorovaný	k2eAgInSc6d1	pozorovaný
vesmíru	vesmír	k1gInSc6	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
britští	britský	k2eAgMnPc1d1	britský
astronomové	astronom	k1gMnPc1	astronom
odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
počet	počet	k1gInSc4	počet
galaxií	galaxie	k1gFnPc2	galaxie
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
bilion	bilion	k4xCgInSc4	bilion
<g/>
.	.	kIx.	.
</s>
<s>
Velikosti	velikost	k1gFnPc1	velikost
galaxií	galaxie	k1gFnPc2	galaxie
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
galaxií	galaxie	k1gFnPc2	galaxie
s	s	k7c7	s
méně	málo	k6eAd2	málo
než	než	k8xS	než
deseti	deset	k4xCc7	deset
miliony	milion	k4xCgInPc7	milion
(	(	kIx(	(
<g/>
107	[number]	k4	107
<g/>
)	)	kIx)	)
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
obří	obří	k2eAgFnPc4d1	obří
eliptické	eliptický	k2eAgFnPc4d1	eliptická
galaxie	galaxie	k1gFnPc4	galaxie
s	s	k7c7	s
biliónem	bilión	k4xCgInSc7	bilión
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
)	)	kIx)	)
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
se	se	k3xPyFc4	se
otáčejí	otáčet	k5eAaImIp3nP	otáčet
kolem	kolem	k7c2	kolem
těžiště	těžiště	k1gNnSc2	těžiště
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velice	velice	k6eAd1	velice
hrubého	hrubý	k2eAgInSc2d1	hrubý
odhadu	odhad	k1gInSc2	odhad
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pozorovatelném	pozorovatelný	k2eAgInSc6d1	pozorovatelný
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
triliardy	triliarda	k4xCgFnSc2	triliarda
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
1021	[number]	k4	1021
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
astronomové	astronom	k1gMnPc1	astronom
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
300	[number]	k4	300
triliard	triliarda	k4xCgFnPc2	triliarda
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
hmota	hmota	k1gFnSc1	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
(	(	kIx(	(
<g/>
homogenně	homogenně	k6eAd1	homogenně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
berou	brát	k5eAaImIp3nP	brát
průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
větších	veliký	k2eAgFnPc2d2	veliký
než	než	k8xS	než
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
měřítku	měřítko	k1gNnSc6	měřítko
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
hmota	hmota	k1gFnSc1	hmota
hierarchicky	hierarchicky	k6eAd1	hierarchicky
"	"	kIx"	"
<g/>
shlukuje	shlukovat	k5eAaImIp3nS	shlukovat
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
atomy	atom	k1gInPc7	atom
do	do	k7c2	do
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
do	do	k7c2	do
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
galaxií	galaxie	k1gFnPc2	galaxie
do	do	k7c2	do
kup	kupa	k1gFnPc2	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
kupy	kupa	k1gFnPc1	kupa
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
v	v	k7c6	v
nadkupách	nadkupa	k1gFnPc6	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
vlákna	vlákno	k1gNnPc1	vlákno
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
milionů	milion	k4xCgInPc2	milion
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnPc1d3	veliký
struktury	struktura	k1gFnPc1	struktura
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Sloanova	Sloanův	k2eAgFnSc1d1	Sloanova
velká	velký	k2eAgFnSc1d1	velká
zeď	zeď	k1gFnSc1	zeď
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
rozložena	rozložen	k2eAgFnSc1d1	rozložena
izotropně	izotropně	k6eAd1	izotropně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
pozorování	pozorování	k1gNnPc2	pozorování
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
směru	směr	k1gInSc6	směr
pozorování	pozorování	k1gNnSc2	pozorování
tedy	tedy	k9	tedy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc1d1	stejné
množství	množství	k1gNnSc1	množství
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
rovněž	rovněž	k9	rovněž
izotropní	izotropní	k2eAgNnSc1d1	izotropní
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tepelné	tepelný	k2eAgFnSc6d1	tepelná
rovnováze	rovnováha	k1gFnSc6	rovnováha
spektra	spektrum	k1gNnSc2	spektrum
záření	záření	k1gNnSc2	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
asi	asi	k9	asi
2,725	[number]	k4	2,725
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
homogenní	homogenní	k2eAgInSc1d1	homogenní
a	a	k8xC	a
izotropní	izotropní	k2eAgInSc1d1	izotropní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k9	jako
kosmologický	kosmologický	k2eAgInSc4d1	kosmologický
princip	princip	k1gInSc4	princip
a	a	k8xC	a
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
ji	on	k3xPp3gFnSc4	on
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
vesmír	vesmír	k1gInSc1	vesmír
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
celkovou	celkový	k2eAgFnSc4d1	celková
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
9,9	[number]	k4	9,9
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
30	[number]	k4	30
gramů	gram	k1gInPc2	gram
na	na	k7c4	na
centimetr	centimetr	k1gInSc4	centimetr
krychlový	krychlový	k2eAgInSc4d1	krychlový
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
74	[number]	k4	74
%	%	kIx~	%
temné	temný	k2eAgFnSc2d1	temná
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
22	[number]	k4	22
%	%	kIx~	%
chladné	chladný	k2eAgFnSc2d1	chladná
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
4	[number]	k4	4
%	%	kIx~	%
baryonové	baryonový	k2eAgFnSc2d1	baryonová
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgFnSc2d1	běžná
<g/>
)	)	kIx)	)
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
krychlové	krychlový	k2eAgInPc4d1	krychlový
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
temné	temný	k2eAgFnSc2d1	temná
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
temné	temný	k2eAgFnPc1d1	temná
hmoty	hmota	k1gFnPc1	hmota
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
neznámé	známý	k2eNgFnSc2d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Temná	temný	k2eAgFnSc1d1	temná
hmota	hmota	k1gFnSc1	hmota
podléhá	podléhat	k5eAaImIp3nS	podléhat
gravitaci	gravitace	k1gFnSc4	gravitace
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
expanzi	expanze	k1gFnSc4	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
temná	temný	k2eAgFnSc1d1	temná
energie	energie	k1gFnSc1	energie
toto	tento	k3xDgNnSc4	tento
rozpínání	rozpínání	k1gNnSc4	rozpínání
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
odhad	odhad	k1gInSc1	odhad
věku	věk	k1gInSc2	věk
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
13,799	[number]	k4	13,799
<g/>
±	±	k?	±
<g/>
0,021	[number]	k4	0,021
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
reliktního	reliktní	k2eAgNnSc2d1	reliktní
kosmického	kosmický	k2eAgNnSc2d1	kosmické
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislé	závislý	k2eNgInPc1d1	nezávislý
odhady	odhad	k1gInPc1	odhad
(	(	kIx(	(
<g/>
založené	založený	k2eAgNnSc1d1	založené
například	například	k6eAd1	například
na	na	k7c6	na
radioaktivním	radioaktivní	k2eAgNnSc6d1	radioaktivní
datování	datování	k1gNnSc6	datování
<g/>
)	)	kIx)	)
věku	věk	k1gInSc2	věk
vesmíru	vesmír	k1gInSc2	vesmír
mají	mít	k5eAaImIp3nP	mít
sice	sice	k8xC	sice
menší	malý	k2eAgFnSc4d2	menší
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
věk	věk	k1gInSc4	věk
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
či	či	k8xC	či
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
nebyl	být	k5eNaImAgInS	být
stejný	stejný	k2eAgInSc1d1	stejný
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
populacemi	populace	k1gFnPc7	populace
kvasarů	kvasar	k1gInPc2	kvasar
a	a	k8xC	a
galaxií	galaxie	k1gFnPc2	galaxie
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
expanze	expanze	k1gFnSc1	expanze
umožnila	umožnit	k5eAaPmAgFnS	umožnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozemští	pozemský	k2eAgMnPc1d1	pozemský
vědci	vědec	k1gMnPc1	vědec
mohou	moct	k5eAaImIp3nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
galaxie	galaxie	k1gFnSc2	galaxie
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
třicet	třicet	k4xCc1	třicet
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
cestovalo	cestovat	k5eAaImAgNnS	cestovat
pouhých	pouhý	k2eAgFnPc2d1	pouhá
třináct	třináct	k4xCc4	třináct
miliard	miliarda	k4xCgFnPc2	miliarda
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
expanze	expanze	k1gFnSc1	expanze
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotony	foton	k1gInPc1	foton
emitované	emitovaný	k2eAgInPc1d1	emitovaný
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
galaxií	galaxie	k1gFnPc2	galaxie
mají	mít	k5eAaImIp3nP	mít
posunutou	posunutý	k2eAgFnSc4d1	posunutá
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
do	do	k7c2	do
červeného	červený	k2eAgInSc2d1	červený
oboru	obor	k1gInSc2	obor
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
nižší	nízký	k2eAgFnSc4d2	nižší
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
prostorové	prostorový	k2eAgFnSc2d1	prostorová
expanze	expanze	k1gFnSc2	expanze
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
studium	studium	k1gNnSc1	studium
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
a	a	k8xC	a
expanze	expanze	k1gFnSc1	expanze
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
i	i	k9	i
dalším	další	k2eAgNnSc7d1	další
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInSc1d2	novější
výzkum	výzkum	k1gInSc1	výzkum
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
však	však	k9	však
zrychlenou	zrychlený	k2eAgFnSc4d1	zrychlená
expanzi	expanze	k1gFnSc4	expanze
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Relativní	relativní	k2eAgInPc1d1	relativní
podíly	podíl	k1gInPc1	podíl
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
nejlehčích	lehký	k2eAgInPc2d3	nejlehčí
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
deuterium	deuterium	k1gNnSc4	deuterium
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
stejné	stejný	k2eAgInPc1d1	stejný
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
jeho	jeho	k3xOp3gFnSc4	jeho
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
hmoty	hmota	k1gFnSc2	hmota
než	než	k8xS	než
antihmoty	antihmota	k1gFnSc2	antihmota
<g/>
,	,	kIx,	,
asymetrie	asymetrie	k1gFnSc1	asymetrie
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
narušením	narušení	k1gNnSc7	narušení
CP	CP	kA	CP
invariance	invariance	k1gFnSc2	invariance
při	při	k7c6	při
rozpadech	rozpad	k1gInPc6	rozpad
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	můj	k3xOp1gFnSc1	můj
gravitace	gravitace	k1gFnSc1	gravitace
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
kosmologických	kosmologický	k2eAgNnPc6d1	kosmologické
měřítkách	měřítko	k1gNnPc6	měřítko
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
souhrnnou	souhrnný	k2eAgFnSc4d1	souhrnná
hybnost	hybnost	k1gFnSc4	hybnost
či	či	k8xC	či
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
by	by	kYmCp3nP	by
vyplývaly	vyplývat	k5eAaImAgInP	vyplývat
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
Gaussova	Gaussův	k2eAgInSc2d1	Gaussův
zákona	zákon	k1gInSc2	zákon
elektrostatiky	elektrostatika	k1gFnSc2	elektrostatika
a	a	k8xC	a
z	z	k7c2	z
Landauova-Lifšicova	Landauova-Lifšicův	k2eAgInSc2d1	Landauova-Lifšicův
pseudotenzoru	pseudotenzor	k1gInSc2	pseudotenzor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
vesmír	vesmír	k1gInSc1	vesmír
byl	být	k5eAaImAgInS	být
konečný	konečný	k2eAgInSc1d1	konečný
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
tvoří	tvořit	k5eAaImIp3nS	tvořit
spojité	spojitý	k2eAgNnSc4d1	spojité
časoprostorové	časoprostorový	k2eAgNnSc4d1	časoprostorové
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
prostorových	prostorový	k2eAgFnPc2d1	prostorová
dimenzí	dimenze	k1gFnPc2	dimenze
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
časového	časový	k2eAgInSc2d1	časový
rozměru	rozměr	k1gInSc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
téměř	téměř	k6eAd1	téměř
plochý	plochý	k2eAgMnSc1d1	plochý
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
nulové	nulový	k2eAgNnSc1d1	nulové
zakřivení	zakřivení	k1gNnSc1	zakřivení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eukleidovská	eukleidovský	k2eAgFnSc1d1	eukleidovská
geometrie	geometrie	k1gFnSc1	geometrie
experimentálně	experimentálně	k6eAd1	experimentálně
platí	platit	k5eAaImIp3nS	platit
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
přesností	přesnost	k1gFnSc7	přesnost
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
časoprostor	časoprostor	k1gInSc1	časoprostor
má	mít	k5eAaImIp3nS	mít
souvislou	souvislý	k2eAgFnSc4d1	souvislá
topologii	topologie	k1gFnSc4	topologie
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
také	také	k9	také
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vesmír	vesmír	k1gInSc1	vesmír
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vícedimenzionální	vícedimenzionální	k2eAgInSc1d1	vícedimenzionální
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
časoprostor	časoprostor	k1gInSc1	časoprostor
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
provázanou	provázaný	k2eAgFnSc4d1	provázaná
globální	globální	k2eAgFnSc4d1	globální
topologii	topologie	k1gFnSc4	topologie
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
s	s	k7c7	s
válcovou	válcový	k2eAgFnSc7d1	válcová
nebo	nebo	k8xC	nebo
toroidní	toroidní	k2eAgFnSc7d1	toroidní
topologií	topologie	k1gFnSc7	topologie
dvourozměrných	dvourozměrný	k2eAgInPc2d1	dvourozměrný
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
pozorování	pozorování	k1gNnSc2	pozorování
řídí	řídit	k5eAaImIp3nP	řídit
souborem	soubor	k1gInSc7	soubor
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
převažujícího	převažující	k2eAgInSc2d1	převažující
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
fyziky	fyzika	k1gFnSc2	fyzika
se	se	k3xPyFc4	se
veškerá	veškerý	k3xTgFnSc1	veškerý
hmota	hmota	k1gFnSc1	hmota
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
generací	generace	k1gFnPc2	generace
leptonů	lepton	k1gInPc2	lepton
a	a	k8xC	a
kvarků	kvark	k1gInPc2	kvark
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
fermiony	fermion	k1gInPc1	fermion
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
spolu	spolu	k6eAd1	spolu
interagují	interagovat	k5eAaPmIp3nP	interagovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nejvýše	nejvýše	k6eAd1	nejvýše
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
:	:	kIx,	:
elektroslabé	elektroslabý	k2eAgFnSc2d1	elektroslabá
interakce	interakce	k1gFnSc2	interakce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
a	a	k8xC	a
slabou	slabý	k2eAgFnSc4d1	slabá
jadernou	jaderný	k2eAgFnSc4d1	jaderná
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc7d1	silná
jadernou	jaderný	k2eAgFnSc7d1	jaderná
sílou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
chromodynamika	chromodynamika	k1gFnSc1	chromodynamika
<g/>
,	,	kIx,	,
a	a	k8xC	a
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dá	dát	k5eAaPmIp3nS	dát
nejlépe	dobře	k6eAd3	dobře
popsat	popsat	k5eAaPmF	popsat
obecnou	obecný	k2eAgFnSc7d1	obecná
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
interakce	interakce	k1gFnPc4	interakce
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
popsány	popsat	k5eAaPmNgFnP	popsat
renormalizovanou	renormalizovaný	k2eAgFnSc7d1	renormalizovaný
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
teorií	teorie	k1gFnSc7	teorie
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
interakce	interakce	k1gFnPc1	interakce
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
kalibrační	kalibrační	k2eAgFnPc1d1	kalibrační
bosony	bosona	k1gFnPc1	bosona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
určité	určitý	k2eAgFnSc3d1	určitá
kalibrační	kalibrační	k2eAgFnSc3d1	kalibrační
symetrii	symetrie	k1gFnSc3	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Renormalizované	Renormalizovaný	k2eAgFnSc2d1	Renormalizovaný
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
teorie	teorie	k1gFnSc2	teorie
strun	struna	k1gFnPc2	struna
jsou	být	k5eAaImIp3nP	být
nadějné	nadějný	k2eAgInPc1d1	nadějný
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
má	mít	k5eAaImIp3nS	mít
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostorové	prostorový	k2eAgFnPc4d1	prostorová
a	a	k8xC	a
časové	časový	k2eAgFnPc4d1	časová
délky	délka	k1gFnPc4	délka
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgNnSc4	žádný
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
hodnoty	hodnota	k1gFnPc4	hodnota
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
h	h	k?	h
nebo	nebo	k8xC	nebo
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
G	G	kA	G
a	a	k8xC	a
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nP	platit
zákony	zákon	k1gInPc7	zákon
zachování	zachování	k1gNnSc2	zachování
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
momentu	moment	k1gInSc3	moment
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
;	;	kIx,	;
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
zachování	zachování	k1gNnSc2	zachování
souviset	souviset	k5eAaImF	souviset
se	s	k7c7	s
symetrií	symetrie	k1gFnSc7	symetrie
a	a	k8xC	a
matematickou	matematický	k2eAgFnSc7d1	matematická
identitou	identita	k1gFnSc7	identita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jemné	jemný	k2eAgNnSc1d1	jemné
vyladění	vyladění	k1gNnSc1	vyladění
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
vesmíru	vesmír	k1gInSc2	vesmír
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
hodnoty	hodnota	k1gFnPc1	hodnota
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvedené	uvedený	k2eAgFnPc1d1	uvedená
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
odchylují	odchylovat	k5eAaImIp3nP	odchylovat
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
komunitě	komunita	k1gFnSc6	komunita
neexistuje	existovat	k5eNaImIp3nS	existovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
toto	tento	k3xDgNnSc4	tento
jemné	jemný	k2eAgNnSc4d1	jemné
doladění	doladění	k1gNnSc4	doladění
vlastností	vlastnost	k1gFnPc2	vlastnost
vesmíru	vesmír	k1gInSc2	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
za	za	k7c2	za
jakých	jaký	k3yRgFnPc2	jaký
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
inteligentní	inteligentní	k2eAgInSc4d1	inteligentní
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jakých	jaký	k3yRgMnPc2	jaký
nabývá	nabývat	k5eAaImIp3nS	nabývat
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k6eAd1	jak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
má	mít	k5eAaImIp3nS	mít
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
konstatování	konstatování	k1gNnSc1	konstatování
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
diskusi	diskuse	k1gFnSc6	diskuse
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
existuje	existovat	k5eAaImIp3nS	existovat
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
doladěn	doladit	k5eAaPmNgInS	doladit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
podporovat	podporovat	k5eAaImF	podporovat
inteligentní	inteligentní	k2eAgInSc4d1	inteligentní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
pozorování	pozorování	k1gNnSc2	pozorování
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyladěný	vyladěný	k2eAgInSc1d1	vyladěný
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
inteligentního	inteligentní	k2eAgInSc2d1	inteligentní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poznatek	poznatek	k1gInSc1	poznatek
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
malý	malý	k2eAgInSc1d1	malý
antropický	antropický	k2eAgInSc1d1	antropický
princip	princip	k1gInSc1	princip
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
důležitý	důležitý	k2eAgInSc1d1	důležitý
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vzniku	vznik	k1gInSc2	vznik
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
pravděpodobnosti	pravděpodobnost	k1gFnPc4	pravděpodobnost
vzniku	vznik	k1gInSc2	vznik
jiných	jiný	k2eAgInPc2d1	jiný
vesmírů	vesmír	k1gInPc2	vesmír
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
od	od	k7c2	od
našeho	náš	k3xOp1gInSc2	náš
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velký	velký	k2eAgInSc4d1	velký
antropický	antropický	k2eAgInSc4d1	antropický
princip	princip	k1gInSc4	princip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vidí	vidět	k5eAaImIp3nS	vidět
stopu	stopa	k1gFnSc4	stopa
záměru	záměr	k1gInSc2	záměr
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
povahu	povaha	k1gFnSc4	povaha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
polemik	polemika	k1gFnPc2	polemika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
==	==	k?	==
</s>
</p>
<p>
<s>
Modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
kosmologie	kosmologie	k1gFnSc1	kosmologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
kosmogonie	kosmogonie	k1gFnSc2	kosmogonie
<g/>
)	)	kIx)	)
vznikaly	vznikat	k5eAaImAgInP	vznikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
kosmologie	kosmologie	k1gFnSc1	kosmologie
a	a	k8xC	a
kosmogonie	kosmogonie	k1gFnSc1	kosmogonie
příběhem	příběh	k1gInSc7	příběh
bohů	bůh	k1gMnPc2	bůh
vyprávěných	vyprávěný	k2eAgFnPc2d1	vyprávěná
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
odosobněného	odosobněný	k2eAgInSc2d1	odosobněný
vesmíru	vesmír	k1gInSc2	vesmír
řídícího	řídící	k2eAgInSc2d1	řídící
se	se	k3xPyFc4	se
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
zákony	zákon	k1gInPc1	zákon
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
navrhovány	navrhován	k2eAgFnPc1d1	navrhována
Řeky	řeka	k1gFnPc1	řeka
a	a	k8xC	a
Indy	Indus	k1gInPc1	Indus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
zlepšovala	zlepšovat	k5eAaImAgNnP	zlepšovat
astronomická	astronomický	k2eAgNnPc1d1	astronomické
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
teorie	teorie	k1gFnPc1	teorie
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
stále	stále	k6eAd1	stále
přesnějšímu	přesný	k2eAgInSc3d2	přesnější
popisu	popis	k1gInSc3	popis
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
éra	éra	k1gFnSc1	éra
kosmologie	kosmologie	k1gFnSc2	kosmologie
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
obecnou	obecný	k2eAgFnSc7d1	obecná
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
kvantitativně	kvantitativně	k6eAd1	kvantitativně
předpovědět	předpovědět	k5eAaPmF	předpovědět
vznik	vznik	k1gInSc4	vznik
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
konec	konec	k1gInSc4	konec
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
přijímaných	přijímaný	k2eAgFnPc2d1	přijímaná
kosmologických	kosmologický	k2eAgFnPc2d1	kosmologická
teorií	teorie	k1gFnPc2	teorie
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
z	z	k7c2	z
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
teorie	teorie	k1gFnSc2	teorie
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
ještě	ještě	k6eAd1	ještě
přesnější	přesný	k2eAgNnPc1d2	přesnější
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
určila	určit	k5eAaPmAgFnS	určit
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
kultury	kultura	k1gFnPc1	kultura
znají	znát	k5eAaImIp3nP	znát
příběhy	příběh	k1gInPc4	příběh
popisující	popisující	k2eAgNnSc4d1	popisující
stvoření	stvoření	k1gNnSc4	stvoření
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
se	se	k3xPyFc4	se
svět	svět	k1gInSc1	svět
rodí	rodit	k5eAaImIp3nS	rodit
z	z	k7c2	z
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
epické	epický	k2eAgFnSc6d1	epická
básni	báseň	k1gFnSc6	báseň
Kalevala	Kalevala	k1gFnSc6	Kalevala
<g/>
,	,	kIx,	,
čínském	čínský	k2eAgInSc6d1	čínský
příběhu	příběh	k1gInSc6	příběh
Pangu	Pang	k1gInSc6	Pang
nebo	nebo	k8xC	nebo
indickém	indický	k2eAgInSc6d1	indický
příběhu	příběh	k1gInSc6	příběh
Brahmánda	Brahmánd	k1gMnSc2	Brahmánd
Purana	Puran	k1gMnSc2	Puran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
příbězích	příběh	k1gInPc6	příběh
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stvoří	stvořit	k5eAaPmIp3nS	stvořit
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
z	z	k7c2	z
ní	on	k3xPp3gFnSc3	on
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
tibetského	tibetský	k2eAgInSc2d1	tibetský
buddhismu	buddhismus	k1gInSc2	buddhismus
o	o	k7c6	o
Ádi-buddhovi	Ádiuddh	k1gMnSc6	Ádi-buddh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
řeckém	řecký	k2eAgInSc6d1	řecký
příběhu	příběh	k1gInSc6	příběh
Gaii	Gaii	k1gNnSc1	Gaii
(	(	kIx(	(
<g/>
Matky	matka	k1gFnPc1	matka
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mýtu	mýto	k1gNnSc6	mýto
aztécké	aztécký	k2eAgFnSc2d1	aztécká
bohyně	bohyně	k1gFnSc2	bohyně
Coatlicue	Coatlicu	k1gFnSc2	Coatlicu
<g/>
,	,	kIx,	,
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
staroegyptského	staroegyptský	k2eAgMnSc2d1	staroegyptský
boha	bůh	k1gMnSc2	bůh
Atuma	Atum	k1gMnSc2	Atum
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
biblické	biblický	k2eAgFnSc2d1	biblická
knihy	kniha	k1gFnSc2	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
typu	typ	k1gInSc6	typ
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
stvořen	stvořit	k5eAaPmNgInS	stvořit
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
mužského	mužský	k2eAgNnSc2d1	mužské
a	a	k8xC	a
ženského	ženský	k2eAgNnSc2d1	ženské
božstva	božstvo	k1gNnSc2	božstvo
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
maorském	maorský	k2eAgInSc6d1	maorský
Rangi	Rang	k1gInSc6	Rang
a	a	k8xC	a
Papa	papa	k1gMnSc1	papa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
příbězích	příběh	k1gInPc6	příběh
je	být	k5eAaImIp3nS	být
vesmír	vesmír	k1gInSc1	vesmír
stvořen	stvořit	k5eAaPmNgInS	stvořit
ruční	ruční	k2eAgFnSc7d1	ruční
prací	práce	k1gFnSc7	práce
z	z	k7c2	z
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc4	tělo
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
bohyně	bohyně	k1gFnSc2	bohyně
Tiamat	Tiama	k1gNnPc2	Tiama
v	v	k7c6	v
babylónském	babylónský	k2eAgInSc6d1	babylónský
eposu	epos	k1gInSc6	epos
Enúma	Enúmum	k1gNnSc2	Enúmum
eliš	eliš	k5eAaPmIp2nS	eliš
nebo	nebo	k8xC	nebo
obra	obr	k1gMnSc2	obr
Ymira	Ymir	k1gMnSc2	Ymir
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
-	-	kIx~	-
nebo	nebo	k8xC	nebo
z	z	k7c2	z
chaosu	chaos	k1gInSc2	chaos
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Izanagi	Izanagi	k1gNnSc7	Izanagi
a	a	k8xC	a
Izanami	Izana	k1gFnPc7	Izana
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
bájesloví	bájesloví	k1gNnSc6	bájesloví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
příbězích	příběh	k1gInPc6	příběh
vychází	vycházet	k5eAaImIp3nS	vycházet
vesmír	vesmír	k1gInSc1	vesmír
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Brahman	Brahman	k1gMnSc1	Brahman
a	a	k8xC	a
Prakrti	Prakrt	k1gMnPc1	Prakrt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
filozofii	filozofie	k1gFnSc6	filozofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filozofické	filozofický	k2eAgInPc1d1	filozofický
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
raní	ranit	k5eAaPmIp3nP	ranit
řečtí	řecký	k2eAgMnPc1d1	řecký
filozofové	filozof	k1gMnPc1	filozof
<g/>
,	,	kIx,	,
předsókratici	předsókratik	k1gMnPc1	předsókratik
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
první	první	k4xOgInPc4	první
známé	známý	k2eAgInPc4d1	známý
filosofické	filosofický	k2eAgInPc4d1	filosofický
modely	model	k1gInPc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdání	zdání	k1gNnSc1	zdání
může	moct	k5eAaImIp3nS	moct
klamat	klamat	k5eAaImF	klamat
<g/>
,	,	kIx,	,
a	a	k8xC	a
usilovali	usilovat	k5eAaImAgMnP	usilovat
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
základních	základní	k2eAgFnPc2d1	základní
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
schopností	schopnost	k1gFnSc7	schopnost
věci	věc	k1gFnSc2	věc
změnit	změnit	k5eAaPmF	změnit
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
páru	pára	k1gFnSc4	pára
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc1	několik
filozofů	filozof	k1gMnPc2	filozof
si	se	k3xPyFc3	se
začalo	začít	k5eAaPmAgNnS	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
očividně	očividně	k6eAd1	očividně
různé	různý	k2eAgFnPc4d1	různá
látky	látka	k1gFnPc4	látka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
jednoho	jeden	k4xCgInSc2	jeden
praelementu	praelement	k1gInSc2	praelement
<g/>
,	,	kIx,	,
arché	arché	k1gFnSc1	arché
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byl	být	k5eAaImAgInS	být
Thalés	Thalés	k1gInSc1	Thalés
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
pralátku	pralátka	k1gFnSc4	pralátka
považoval	považovat	k5eAaImAgMnS	považovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
Anaximenés	Anaximenés	k1gInSc4	Anaximenés
za	za	k7c4	za
prvopočátek	prvopočátek	k1gInSc4	prvopočátek
považoval	považovat	k5eAaImAgMnS	považovat
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nP	muset
existovat	existovat	k5eAaImF	existovat
přitažlivé	přitažlivý	k2eAgFnPc1d1	přitažlivá
a	a	k8xC	a
odpudivé	odpudivý	k2eAgFnPc1d1	odpudivá
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
že	že	k8xS	že
arché	arché	k1gFnSc1	arché
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Empedoklés	Empedoklés	k1gInSc1	Empedoklés
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
základní	základní	k2eAgFnSc2d1	základní
látky	látka	k1gFnSc2	látka
čtyři	čtyři	k4xCgInPc4	čtyři
živly	živel	k1gInPc4	živel
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
jeho	jeho	k3xOp3gFnSc2	jeho
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
(	(	kIx(	(
<g/>
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc4	oheň
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kombinacích	kombinace	k1gFnPc6	kombinace
a	a	k8xC	a
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
čtyř	čtyři	k4xCgInPc2	čtyři
elementů	element	k1gInPc2	element
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgMnPc2d1	další
filozofů	filozof	k1gMnPc2	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
filozofové	filozof	k1gMnPc1	filozof
před	před	k7c7	před
Empedoklem	Empedokl	k1gInSc7	Empedokl
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
méně	málo	k6eAd2	málo
hmotné	hmotný	k2eAgFnPc4d1	hmotná
věci	věc	k1gFnPc4	věc
pro	pro	k7c4	pro
arché	arché	k1gFnSc4	arché
<g/>
,	,	kIx,	,
Hérakleitos	Hérakleitos	k1gMnSc1	Hérakleitos
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
logos	logos	k1gInSc4	logos
"	"	kIx"	"
<g/>
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
veškerenstvo	veškerenstvo	k1gNnSc4	veškerenstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgMnPc4d1	složený
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Thaletův	Thaletův	k2eAgMnSc1d1	Thaletův
student	student	k1gMnSc1	student
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
chaotické	chaotický	k2eAgFnSc2d1	chaotická
látky	látka	k1gFnSc2	látka
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
apeiron	apeiron	k1gInSc1	apeiron
(	(	kIx(	(
<g/>
bezmezno	bezmezno	k1gNnSc1	bezmezno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
modernímu	moderní	k2eAgInSc3d1	moderní
pojmu	pojem	k1gInSc3	pojem
kvantové	kvantový	k2eAgFnSc2d1	kvantová
pěny	pěna	k1gFnSc2	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc3	teorie
apeiron	apeiron	k1gInSc1	apeiron
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
modifikoval	modifikovat	k5eAaBmAgInS	modifikovat
Anaxágoras	Anaxágoras	k1gInSc1	Anaxágoras
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
věci	věc	k1gFnPc1	věc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
utkané	utkaný	k2eAgInPc1d1	utkaný
z	z	k7c2	z
rychle	rychle	k6eAd1	rychle
rotujícího	rotující	k2eAgInSc2d1	rotující
apeironu	apeiron	k1gInSc2	apeiron
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
Nús	Nús	k1gFnSc2	Nús
(	(	kIx(	(
<g/>
mysl	mysl	k1gFnSc1	mysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
filozofové	filozof	k1gMnPc1	filozof
-	-	kIx~	-
především	především	k9	především
Leukippos	Leukippos	k1gMnSc1	Leukippos
a	a	k8xC	a
Démokritos	Démokritos	k1gMnSc1	Démokritos
-	-	kIx~	-
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
nedělitelných	dělitelný	k2eNgInPc2d1	nedělitelný
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
pohybujících	pohybující	k2eAgMnPc2d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
vakuu	vakuum	k1gNnSc3	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
tomuto	tento	k3xDgInSc3	tento
názoru	názor	k1gInSc3	názor
oponoval	oponovat	k5eAaImAgMnS	oponovat
(	(	kIx(	(
<g/>
Příroda	příroda	k1gFnSc1	příroda
se	se	k3xPyFc4	se
hrozí	hrozit	k5eAaImIp3nS	hrozit
prázdnoty	prázdnota	k1gFnSc2	prázdnota
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
pohybu	pohyb	k1gInSc3	pohyb
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
<g/>
;	;	kIx,	;
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
by	by	kYmCp3nS	by
prázdný	prázdný	k2eAgInSc1d1	prázdný
prostor	prostor	k1gInSc1	prostor
neměl	mít	k5eNaImAgInS	mít
bránit	bránit	k5eAaImF	bránit
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
možnosti	možnost	k1gFnSc3	možnost
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Hérakleitos	Hérakleitos	k1gMnSc1	Hérakleitos
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
věčné	věčný	k2eAgFnSc2d1	věčná
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
současník	současník	k1gMnSc1	současník
Parmenidés	Parmenidés	k1gInSc4	Parmenidés
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
iluzí	iluze	k1gFnSc7	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
opravdová	opravdový	k2eAgFnSc1d1	opravdová
základní	základní	k2eAgFnSc1d1	základní
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
věčně	věčně	k6eAd1	věčně
neměnná	neměnný	k2eAgFnSc1d1	neměnná
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Parmenidés	Parmenidés	k6eAd1	Parmenidés
označil	označit	k5eAaPmAgMnS	označit
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
za	za	k7c4	za
τ	τ	k?	τ
ἐ	ἐ	k?	ἐ
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parmenidova	Parmenidův	k2eAgFnSc1d1	Parmenidova
teorie	teorie	k1gFnSc1	teorie
nebyla	být	k5eNaImAgFnS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Řeků	Řek	k1gMnPc2	Řek
přijatelná	přijatelný	k2eAgFnSc1d1	přijatelná
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
žák	žák	k1gMnSc1	žák
Zénón	Zénón	k1gMnSc1	Zénón
z	z	k7c2	z
Eleje	Elea	k1gFnSc2	Elea
předložil	předložit	k5eAaPmAgMnS	předložit
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
několik	několik	k4yIc4	několik
slavných	slavný	k2eAgInPc2d1	slavný
paradoxů	paradox	k1gInPc2	paradox
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
paradoxy	paradox	k1gInPc4	paradox
zavedením	zavedení	k1gNnSc7	zavedení
pojmu	pojem	k1gInSc2	pojem
nekonečně	konečně	k6eNd1	konečně
dělitelného	dělitelný	k2eAgNnSc2d1	dělitelné
kontinua	kontinuum	k1gNnSc2	kontinuum
a	a	k8xC	a
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Indický	indický	k2eAgMnSc1d1	indický
filozof	filozof	k1gMnSc1	filozof
Kanáda	Kanáda	k1gFnSc1	Kanáda
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
filosofické	filosofický	k2eAgFnSc2d1	filosofická
školy	škola	k1gFnSc2	škola
Vaišéšika	Vaišéšika	k1gFnSc1	Vaišéšika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
atomismu	atomismus	k1gInSc2	atomismus
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
teplo	teplo	k1gNnSc1	teplo
jsou	být	k5eAaImIp3nP	být
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
téže	tenže	k3xDgFnSc2	tenže
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
si	se	k3xPyFc3	se
buddhistický	buddhistický	k2eAgMnSc1d1	buddhistický
filozof	filozof	k1gMnSc1	filozof
atomista	atomista	k1gMnSc1	atomista
Dignā	Dignā	k1gMnSc1	Dignā
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
atomy	atom	k1gInPc1	atom
bodové	bodový	k2eAgFnSc2d1	bodová
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
Dignā	Dignā	k1gFnPc1	Dignā
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
krátkodobými	krátkodobý	k2eAgInPc7d1	krátkodobý
záblesky	záblesk	k1gInPc7	záblesk
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Popíral	popírat	k5eAaImAgInS	popírat
existenci	existence	k1gFnSc4	existence
podstatných	podstatný	k2eAgFnPc2d1	podstatná
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
a	a	k8xC	a
myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
okamžitými	okamžitý	k2eAgInPc7d1	okamžitý
proudy	proud	k1gInPc7	proud
záblesků	záblesk	k1gInPc2	záblesk
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
<g/>
Teorie	teorie	k1gFnSc1	teorie
omezeného	omezený	k2eAgInSc2d1	omezený
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
temporal	temporat	k5eAaPmAgMnS	temporat
finitism	finitism	k1gMnSc1	finitism
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
doktrínou	doktrína	k1gFnSc7	doktrína
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
sdíleného	sdílený	k2eAgInSc2d1	sdílený
třemi	tři	k4xCgFnPc7	tři
abrahámovskými	abrahámovský	k2eAgNnPc7d1	abrahámovské
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
:	:	kIx,	:
judaismem	judaismus	k1gInSc7	judaismus
<g/>
,	,	kIx,	,
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
a	a	k8xC	a
islámem	islám	k1gInSc7	islám
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
filozof	filozof	k1gMnSc1	filozof
Jan	Jan	k1gMnSc1	Jan
Filoponos	Filoponos	k1gMnSc1	Filoponos
předložil	předložit	k5eAaPmAgMnS	předložit
filozofické	filozofický	k2eAgInPc4d1	filozofický
argumenty	argument	k1gInPc4	argument
proti	proti	k7c3	proti
starověké	starověký	k2eAgFnSc3d1	starověká
řecké	řecký	k2eAgFnSc3d1	řecká
představě	představa	k1gFnSc3	představa
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Filoponovy	Filoponův	k2eAgInPc1d1	Filoponův
argumenty	argument	k1gInPc1	argument
proti	proti	k7c3	proti
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
minulosti	minulost	k1gFnSc3	minulost
používal	používat	k5eAaImAgInS	používat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
muslimský	muslimský	k2eAgMnSc1d1	muslimský
filosof	filosof	k1gMnSc1	filosof
Alkindus	Alkindus	k1gMnSc1	Alkindus
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
filozof	filozof	k1gMnSc1	filozof
Josefem	Josef	k1gMnSc7	Josef
Gaonem	Gaon	k1gMnSc7	Gaon
a	a	k8xC	a
muslimský	muslimský	k2eAgMnSc1d1	muslimský
teolog	teolog	k1gMnSc1	teolog
Al-Ghazzálím	Al-Ghazzálím	k1gMnSc1	Al-Ghazzálím
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
kosmologický	kosmologický	k2eAgInSc1d1	kosmologický
argument	argument	k1gInSc1	argument
Kalam	Kalam	k1gInSc1	Kalam
<g/>
.	.	kIx.	.
</s>
<s>
Použili	použít	k5eAaPmAgMnP	použít
dva	dva	k4xCgInPc4	dva
logické	logický	k2eAgInPc4d1	logický
argumenty	argument	k1gInPc4	argument
proti	proti	k7c3	proti
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
minulosti	minulost	k1gFnSc3	minulost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
argument	argument	k1gInSc1	argument
"	"	kIx"	"
<g/>
proti	proti	k7c3	proti
možnosti	možnost	k1gFnSc3	možnost
existence	existence	k1gFnSc2	existence
časového	časový	k2eAgInSc2d1	časový
nekonečného	konečný	k2eNgInSc2d1	nekonečný
regresu	regres	k1gInSc2	regres
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
následují	následovat	k5eAaImIp3nP	následovat
strukturu	struktura	k1gFnSc4	struktura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
nekonečno	nekonečno	k1gNnSc1	nekonečno
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Nekonečný	konečný	k2eNgInSc1d1	nekonečný
časový	časový	k2eAgInSc1d1	časový
regres	regres	k1gInSc1	regres
událostí	událost	k1gFnPc2	událost
by	by	kYmCp3nS	by
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
aktuální	aktuální	k2eAgNnSc4d1	aktuální
nekonečno	nekonečno	k1gNnSc4	nekonečno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
nekonečný	konečný	k2eNgInSc1d1	nekonečný
časový	časový	k2eAgInSc1d1	časový
regres	regres	k1gInSc1	regres
událostí	událost	k1gFnPc2	událost
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
<g/>
Druhém	druhý	k4xOgInSc6	druhý
argumentu	argument	k1gInSc6	argument
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nemožnosti	nemožnost	k1gFnPc4	nemožnost
utvořit	utvořit	k5eAaPmF	utvořit
aktuální	aktuální	k2eAgNnSc4d1	aktuální
nekonečno	nekonečno	k1gNnSc4	nekonečno
přidáváním	přidávání	k1gNnSc7	přidávání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Časová	časový	k2eAgFnSc1d1	časová
série	série	k1gFnSc1	série
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
množinou	množina	k1gFnSc7	množina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
utvářená	utvářený	k2eAgFnSc1d1	utvářená
přidáváním	přidávání	k1gNnSc7	přidávání
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Množina	množina	k1gFnSc1	množina
utvářená	utvářený	k2eAgFnSc1d1	utvářená
za	za	k7c7	za
sebou	se	k3xPyFc7	se
následující	následující	k2eAgInPc4d1	následující
událostmi	událost	k1gFnPc7	událost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
nekonečnem	nekonečno	k1gNnSc7	nekonečno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
časová	časový	k2eAgFnSc1d1	časová
série	série	k1gFnSc1	série
událostí	událost	k1gFnPc2	událost
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
nekonečnem	nekonečno	k1gNnSc7	nekonečno
<g/>
.	.	kIx.	.
<g/>
Oba	dva	k4xCgInPc1	dva
argumenty	argument	k1gInPc1	argument
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
později	pozdě	k6eAd2	pozdě
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
filosofy	filosof	k1gMnPc7	filosof
a	a	k8xC	a
teology	teolog	k1gMnPc7	teolog
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
argument	argument	k1gInSc1	argument
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
zejména	zejména	k9	zejména
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Immanuelem	Immanuel	k1gMnSc7	Immanuel
Kantem	Kant	k1gMnSc7	Kant
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
diplomové	diplomový	k2eAgFnSc6d1	Diplomová
práci	práce	k1gFnSc6	práce
o	o	k7c6	o
protimluvech	protimluv	k1gInPc6	protimluv
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Astronomické	astronomický	k2eAgInPc1d1	astronomický
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
astronomické	astronomický	k2eAgInPc1d1	astronomický
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
astronomie	astronomie	k1gFnSc2	astronomie
babylonskými	babylonský	k2eAgMnPc7d1	babylonský
astronomy	astronom	k1gMnPc7	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
viděli	vidět	k5eAaImAgMnP	vidět
Zemi	zem	k1gFnSc4	zem
jako	jako	k8xS	jako
plochý	plochý	k2eAgInSc4d1	plochý
disk	disk	k1gInSc4	disk
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
předpoklady	předpoklad	k1gInPc4	předpoklad
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
raných	raný	k2eAgFnPc2d1	raná
řeckých	řecký	k2eAgFnPc2d1	řecká
map	mapa	k1gFnPc2	mapa
světa	svět	k1gInSc2	svět
od	od	k7c2	od
Anaximandra	Anaximandr	k1gMnSc2	Anaximandr
a	a	k8xC	a
Hekataia	Hekataius	k1gMnSc2	Hekataius
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
<g/>
.	.	kIx.	.
<g/>
Pozdější	pozdní	k2eAgMnPc1d2	pozdější
řečtí	řecký	k2eAgMnPc1d1	řecký
filozofové	filozof	k1gMnPc1	filozof
sledovali	sledovat	k5eAaImAgMnP	sledovat
pohyby	pohyb	k1gInPc1	pohyb
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
modely	model	k1gInPc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
hlubších	hluboký	k2eAgInPc6d2	hlubší
empirických	empirický	k2eAgInPc6d1	empirický
důkazech	důkaz	k1gInPc6	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
Eudoxa	Eudox	k1gInSc2	Eudox
z	z	k7c2	z
Knidu	Knid	k1gInSc2	Knid
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
nekonečný	konečný	k2eNgInSc4d1	nekonečný
a	a	k8xC	a
věčný	věčný	k2eAgInSc4d1	věčný
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
kulatá	kulatý	k2eAgFnSc1d1	kulatá
a	a	k8xC	a
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
ostatní	ostatní	k1gNnSc1	ostatní
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
otáčení	otáčení	k1gNnSc4	otáčení
dutých	dutý	k2eAgFnPc2d1	dutá
soustředných	soustředný	k2eAgFnPc2d1	soustředná
sfér	sféra	k1gFnPc2	sféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
nehybná	hybný	k2eNgFnSc1d1	nehybná
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
střed	střed	k1gInSc4	střed
je	být	k5eAaImIp3nS	být
středem	středem	k7c2	středem
všech	všecek	k3xTgFnPc2	všecek
koulí	koule	k1gFnPc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
zdokonalen	zdokonalit	k5eAaPmNgInS	zdokonalit
řeckými	řecký	k2eAgMnPc7d1	řecký
astronomy	astronom	k1gMnPc7	astronom
Kalippem	Kalipp	k1gMnSc7	Kalipp
a	a	k8xC	a
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
astronomickým	astronomický	k2eAgNnSc7d1	astronomické
pozorováním	pozorování	k1gNnSc7	pozorování
jej	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
Zemí	zem	k1gFnSc7	zem
uprostřed	uprostřed	k6eAd1	uprostřed
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
lidskou	lidský	k2eAgFnSc7d1	lidská
zkušeností	zkušenost	k1gFnSc7	zkušenost
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
kruhový	kruhový	k2eAgInSc4d1	kruhový
pohyb	pohyb	k1gInSc4	pohyb
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
věčně	věčně	k6eAd1	věčně
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
všichni	všechen	k3xTgMnPc1	všechen
řečtí	řecký	k2eAgMnPc1d1	řecký
vědci	vědec	k1gMnPc1	vědec
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
geocentrický	geocentrický	k2eAgInSc4d1	geocentrický
model	model	k1gInSc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Pythagorejský	pythagorejský	k2eAgMnSc1d1	pythagorejský
filozof	filozof	k1gMnSc1	filozof
Filolaos	Filolaos	k1gMnSc1	Filolaos
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
ohnivé	ohnivý	k2eAgNnSc1d1	ohnivé
centrum	centrum	k1gNnSc1	centrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yRgInSc2	který
krouží	kroužit	k5eAaImIp3nS	kroužit
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
planety	planeta	k1gFnPc1	planeta
rovnoměrným	rovnoměrný	k2eAgInSc7d1	rovnoměrný
kruhovým	kruhový	k2eAgInSc7d1	kruhový
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
astronom	astronom	k1gMnSc1	astronom
Aristarchos	Aristarchos	k1gMnSc1	Aristarchos
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
známým	známý	k2eAgMnSc7d1	známý
astronomem	astronom	k1gMnSc7	astronom
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
heliocentrický	heliocentrický	k2eAgInSc4d1	heliocentrický
model	model	k1gInSc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gInSc4	jeho
původní	původní	k2eAgInSc4d1	původní
spis	spis	k1gInSc4	spis
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
odkaz	odkaz	k1gInSc4	odkaz
v	v	k7c6	v
Archimédově	Archimédův	k2eAgFnSc6d1	Archimédova
knize	kniha	k1gFnSc6	kniha
O	o	k7c6	o
počítání	počítání	k1gNnSc6	počítání
písku	písek	k1gInSc2	písek
popisuje	popisovat	k5eAaImIp3nS	popisovat
Aristarchovu	aristarchův	k2eAgFnSc4d1	aristarchův
heliocentrickou	heliocentrický	k2eAgFnSc4d1	heliocentrická
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Archimedes	Archimedes	k1gMnSc1	Archimedes
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Aristarchos	Aristarchos	k1gMnSc1	Aristarchos
Samský	Samský	k1gMnSc1	Samský
však	však	k9	však
vydal	vydat	k5eAaPmAgInS	vydat
knihy	kniha	k1gFnSc2	kniha
jakési	jakýsi	k3yIgNnSc1	jakýsi
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hypothesy	hypothesa	k1gFnSc2	hypothesa
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
</s>
</p>
<p>
<s>
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
jest	být	k5eAaImIp3nS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k6eAd1	jak
výše	vysoce	k6eAd2	vysoce
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
stálice	stálice	k1gFnSc2	stálice
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nehybné	hybný	k2eNgFnPc1d1	nehybná
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
pak	pak	k6eAd1	pak
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
kruhu	kruh	k1gInSc2	kruh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
stojí	stát	k5eAaImIp3nS	stát
uprostřed	uprostřed	k7c2	uprostřed
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
koule	koule	k1gFnSc1	koule
stálic	stálice	k1gFnPc2	stálice
rozložená	rozložený	k2eAgFnSc1d1	rozložená
kolem	kolem	k7c2	kolem
téhož	týž	k3xTgInSc2	týž
středu	střed	k1gInSc2	střed
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
jest	být	k5eAaImIp3nS	být
takové	takový	k3xDgFnPc4	takový
velikosti	velikost	k1gFnPc4	velikost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
ku	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
stálic	stálice	k1gFnPc2	stálice
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
jest	být	k5eAaImIp3nS	být
střed	střed	k1gInSc4	střed
koule	koule	k1gFnSc2	koule
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Totoť	Tototit	k5eAaPmRp2nS	Tototit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
patrno	patrn	k2eAgNnSc1d1	patrno
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
nemožno	možno	k6eNd1	možno
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
<g/>
,	,	kIx,	,
ježto	ježto	k8xS	ježto
střed	střed	k1gInSc1	střed
koule	koule	k1gFnSc2	koule
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
velikosti	velikost	k1gFnPc4	velikost
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
domnívati	domnívat	k5eAaImF	domnívat
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Jest	být	k5eAaImIp3nS	být
však	však	k9	však
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aristarchos	Aristarchos	k1gMnSc1	Aristarchos
myslil	myslit	k5eAaImAgMnS	myslit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
jakmile	jakmile	k8xS	jakmile
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
jest	být	k5eAaImIp3nS	být
jakoby	jakoby	k8xS	jakoby
středem	střed	k1gInSc7	střed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
jest	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jest	být	k5eAaImIp3nS	být
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kouli	koule	k1gFnSc3	koule
stálic	stálice	k1gFnPc2	stálice
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
důkazy	důkaz	k1gInPc1	důkaz
fenoménů	fenomén	k1gInPc2	fenomén
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
předpokladu	předpoklad	k1gInSc3	předpoklad
<g/>
,	,	kIx,	,
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
dává	dávat	k5eAaImIp3nS	dávat
Zemi	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
pohybovati	pohybovat	k5eAaImF	pohybovat
<g/>
,	,	kIx,	,
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
stejnou	stejná	k1gFnSc4	stejná
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Aristarchos	Aristarchos	k1gMnSc1	Aristarchos
také	také	k9	také
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
paralaxu	paralaxa	k1gFnSc4	paralaxa
<g/>
,	,	kIx,	,
a	a	k8xC	a
nelze	lze	k6eNd1	lze
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
pozorovat	pozorovat	k5eAaImF	pozorovat
pohyb	pohyb	k1gInSc4	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
pohyb	pohyb	k1gInSc4	pohyb
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
všeobecně	všeobecně	k6eAd1	všeobecně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
paralaxa	paralaxa	k1gFnSc1	paralaxa
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
dalekohledy	dalekohled	k1gInPc7	dalekohled
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
starověcí	starověký	k2eAgMnPc1d1	starověký
astronomové	astronom	k1gMnPc1	astronom
nemohli	moct	k5eNaImAgMnP	moct
tušit	tušit	k5eAaImF	tušit
<g/>
.	.	kIx.	.
</s>
<s>
Geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
model	model	k1gInSc1	model
s	s	k7c7	s
planetární	planetární	k2eAgFnSc7d1	planetární
paralaxou	paralaxa	k1gFnSc7	paralaxa
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
vysvětloval	vysvětlovat	k5eAaImAgInS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nelze	lze	k6eNd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
paralaxy	paralaxa	k1gFnPc4	paralaxa
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
heliocentrického	heliocentrický	k2eAgInSc2d1	heliocentrický
názoru	názor	k1gInSc2	názor
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
následující	následující	k2eAgFnSc1d1	následující
pasáž	pasáž	k1gFnSc1	pasáž
z	z	k7c2	z
Plutarchova	Plutarchův	k2eAgNnSc2d1	Plutarchův
díla	dílo	k1gNnSc2	dílo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
Kleanthés	Kleanthés	k1gInSc1	Kleanthés
z	z	k7c2	z
Assu	Assus	k1gInSc2	Assus
<g/>
,	,	kIx,	,
současník	současník	k1gMnSc1	současník
Aristarcha	aristarch	k1gMnSc2	aristarch
a	a	k8xC	a
představitel	představitel	k1gMnSc1	představitel
stoicismu	stoicismus	k1gInSc2	stoicismus
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
povinností	povinnost	k1gFnPc2	povinnost
Řeků	Řek	k1gMnPc2	Řek
obvinit	obvinit	k5eAaPmF	obvinit
Aristarcha	aristarch	k1gMnSc4	aristarch
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
z	z	k7c2	z
bezbožnosti	bezbožnost	k1gFnSc2	bezbožnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
srdce	srdce	k1gNnSc2	srdce
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
...	...	k?	...
a	a	k8xC	a
že	že	k8xS	že
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebe	nebe	k1gNnSc1	nebe
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
obíhá	obíhat	k5eAaImIp3nS	obíhat
po	po	k7c6	po
šikmé	šikmý	k2eAgFnSc6d1	šikmá
kružnici	kružnice	k1gFnSc6	kružnice
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
první	první	k4xOgFnSc1	první
doložená	doložený	k2eAgFnSc1d1	doložená
žaloba	žaloba	k1gFnSc1	žaloba
na	na	k7c4	na
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
odlišný	odlišný	k2eAgInSc4d1	odlišný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
další	další	k2eAgMnSc1d1	další
známý	známý	k2eAgMnSc1d1	známý
astronom	astronom	k1gMnSc1	astronom
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
Aristarchův	aristarchův	k2eAgInSc1d1	aristarchův
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
model	model	k1gInSc1	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Seleukos	Seleukos	k1gInSc1	Seleukos
z	z	k7c2	z
Babylónu	babylón	k1gInSc2	babylón
<g/>
,	,	kIx,	,
helénský	helénský	k2eAgMnSc1d1	helénský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Aristarchovi	aristarch	k1gMnSc6	aristarch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Plutarcha	Plutarch	k1gMnSc2	Plutarch
byl	být	k5eAaImAgMnS	být
Seleukos	Seleukos	k1gMnSc1	Seleukos
první	první	k4xOgMnSc1	první
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dokázat	dokázat	k5eAaPmF	dokázat
heliocentrický	heliocentrický	k2eAgInSc4d1	heliocentrický
systém	systém	k1gInSc4	systém
racionální	racionální	k2eAgFnSc7d1	racionální
úvahou	úvaha	k1gFnSc7	úvaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
argumenty	argument	k1gInPc1	argument
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
souvisely	souviset	k5eAaImAgFnP	souviset
s	s	k7c7	s
fenoménem	fenomén	k1gInSc7	fenomén
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Strabóna	Strabón	k1gMnSc2	Strabón
totiž	totiž	k9	totiž
Seleukos	Seleukos	k1gMnSc1	Seleukos
přišel	přijít	k5eAaPmAgMnS	přijít
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
přílivy	příliv	k1gInPc1	příliv
a	a	k8xC	a
odlivy	odliv	k1gInPc1	odliv
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
přitažlivostí	přitažlivost	k1gFnSc7	přitažlivost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
výška	výška	k1gFnSc1	výška
přílivu	příliv	k1gInSc2	příliv
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
Měsíce	měsíc	k1gInSc2	měsíc
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
heliocentrickým	heliocentrický	k2eAgInSc7d1	heliocentrický
modelem	model	k1gInSc7	model
vesmíru	vesmír	k1gInSc2	vesmír
ještě	ještě	k6eAd1	ještě
indický	indický	k2eAgMnSc1d1	indický
astronom	astronom	k1gMnSc1	astronom
Árjabhata	Árjabhata	k1gFnSc1	Árjabhata
a	a	k8xC	a
perští	perský	k2eAgMnPc1d1	perský
astronomové	astronom	k1gMnPc1	astronom
Albumasar	Albumasara	k1gFnPc2	Albumasara
a	a	k8xC	a
Al-Sijzi	Al-Sijze	k1gFnSc4	Al-Sijze
<g/>
.	.	kIx.	.
<g/>
Aristotelův	Aristotelův	k2eAgInSc1d1	Aristotelův
a	a	k8xC	a
Ptolemaiův	Ptolemaiův	k2eAgInSc1d1	Ptolemaiův
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
přijímán	přijímat	k5eAaImNgInS	přijímat
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c4	po
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
,	,	kIx,	,
než	než	k8xS	než
Koperník	Koperník	k1gMnSc1	Koperník
oživil	oživit	k5eAaPmAgMnS	oživit
Aristarchovu	aristarchův	k2eAgFnSc4d1	aristarchův
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
astronomická	astronomický	k2eAgNnPc1d1	astronomické
data	datum	k1gNnPc1	datum
dala	dát	k5eAaPmAgNnP	dát
lépe	dobře	k6eAd2	dobře
vyložit	vyložit	k5eAaPmF	vyložit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
otáčela	otáčet	k5eAaImAgFnS	otáčet
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koperník	Koperník	k1gMnSc1	Koperník
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
názor	názor	k1gInSc1	názor
o	o	k7c6	o
rotaci	rotace	k1gFnSc6	rotace
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
starého	starý	k2eAgInSc2d1	starý
původu	původ	k1gInSc2	původ
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
sledovat	sledovat	k5eAaImF	sledovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
k	k	k7c3	k
Filolaovi	Filolaa	k1gMnSc3	Filolaa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
Herakleidovi	Herakleid	k1gMnSc3	Herakleid
z	z	k7c2	z
Pontu	Pont	k1gInSc2	Pont
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
Ekfantovi	Ekfant	k1gMnSc3	Ekfant
ze	z	k7c2	z
Syrakus	Syrakusy	k1gFnPc2	Syrakusy
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Koperníkem	Koperník	k1gMnSc7	Koperník
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
učenec	učenec	k1gMnSc1	učenec
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Kusánský	Kusánský	k2eAgMnSc1d1	Kusánský
napsal	napsat	k5eAaBmAgInS	napsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
De	De	k?	De
docta	doct	k1gMnSc4	doct
ignorantia	ignorantius	k1gMnSc4	ignorantius
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vědění	vědění	k1gNnPc4	vědění
o	o	k7c4	o
nevědění	nevědění	k1gNnSc4	nevědění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1440	[number]	k4	1440
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc4	týž
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
Árjabhata	Árjabhe	k1gNnPc1	Árjabhe
(	(	kIx(	(
<g/>
476	[number]	k4	476
<g/>
-	-	kIx~	-
<g/>
550	[number]	k4	550
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brahmagupta	Brahmagupta	k1gMnSc1	Brahmagupta
(	(	kIx(	(
<g/>
598	[number]	k4	598
<g/>
-	-	kIx~	-
<g/>
668	[number]	k4	668
<g/>
)	)	kIx)	)
a	a	k8xC	a
Albumasar	Albumasar	k1gMnSc1	Albumasar
Al-Sijzi	Al-Sijh	k1gMnPc1	Al-Sijh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
empirický	empirický	k2eAgInSc1d1	empirický
doklad	doklad	k1gInSc1	doklad
rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
komet	kometa	k1gFnPc2	kometa
podal	podat	k5eAaPmAgMnS	podat
ázerbájdžánský	ázerbájdžánský	k2eAgMnSc1d1	ázerbájdžánský
astronom	astronom	k1gMnSc1	astronom
Tusi	Tus	k1gFnSc2	Tus
(	(	kIx(	(
<g/>
1201	[number]	k4	1201
<g/>
-	-	kIx~	-
<g/>
1274	[number]	k4	1274
<g/>
)	)	kIx)	)
a	a	k8xC	a
perský	perský	k2eAgMnSc1d1	perský
astronom	astronom	k1gMnSc1	astronom
Ali	Ali	k1gMnSc3	Ali
Qushji	Qushj	k1gMnSc3	Qushj
(	(	kIx(	(
<g/>
1403	[number]	k4	1403
-1474	-1474	k4	-1474
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tusi	Tusi	k6eAd1	Tusi
dále	daleko	k6eAd2	daleko
hájil	hájit	k5eAaImAgMnS	hájit
Aristotelův	Aristotelův	k2eAgInSc4d1	Aristotelův
geometrický	geometrický	k2eAgInSc4d1	geometrický
model	model	k1gInSc4	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Qushji	Qushj	k1gMnSc3	Qushj
ho	on	k3xPp3gMnSc4	on
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Koperník	Koperník	k1gMnSc1	Koperník
později	pozdě	k6eAd2	pozdě
obhájil	obhájit	k5eAaPmAgMnS	obhájit
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Al-Birjandi	Al-Birjand	k1gMnPc1	Al-Birjand
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1528	[number]	k4	1528
dál	daleko	k6eAd2	daleko
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
teorii	teorie	k1gFnSc4	teorie
"	"	kIx"	"
<g/>
kruhové	kruhový	k2eAgFnPc1d1	kruhová
setrvačnosti	setrvačnost	k1gFnPc1	setrvačnost
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
rotace	rotace	k1gFnSc2	rotace
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dále	daleko	k6eAd2	daleko
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnPc5	Galile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koperníkův	Koperníkův	k2eAgInSc1d1	Koperníkův
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
modelu	model	k1gInSc2	model
vesmíru	vesmír	k1gInSc2	vesmír
hvězdy	hvězda	k1gFnSc2	hvězda
umístil	umístit	k5eAaPmAgMnS	umístit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
do	do	k7c2	do
nekonečného	konečný	k2eNgInSc2d1	nekonečný
prostotu	prostota	k1gFnSc4	prostota
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
Thomas	Thomas	k1gMnSc1	Thomas
Digges	Digges	k1gMnSc1	Digges
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Perfit	Perfit	k2eAgInSc4d1	Perfit
Description	Description	k1gInSc4	Description
of	of	k?	of
the	the	k?	the
Caelestiall	Caelestiall	k1gInSc1	Caelestiall
Orbes	Orbes	k1gMnSc1	Orbes
according	according	k1gInSc1	according
to	ten	k3xDgNnSc1	ten
the	the	k?	the
most	most	k1gInSc1	most
aunciente	auncient	k1gInSc5	auncient
doctrine	doctrin	k1gInSc5	doctrin
of	of	k?	of
the	the	k?	the
Pythagoreanse	Pythagoreanse	k1gFnSc1	Pythagoreanse
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Úplný	úplný	k2eAgInSc1d1	úplný
popis	popis	k1gInSc1	popis
nebeských	nebeský	k2eAgFnPc2d1	nebeská
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
podle	podle	k7c2	podle
prastaré	prastarý	k2eAgFnSc2d1	prastará
nauky	nauka	k1gFnSc2	nauka
Pythagorejců	pythagorejec	k1gMnPc2	pythagorejec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánský	dominikánský	k2eAgMnSc1d1	dominikánský
mnich	mnich	k1gMnSc1	mnich
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
myšlenku	myšlenka	k1gFnSc4	myšlenka
o	o	k7c6	o
nekonečnosti	nekonečnost	k1gFnSc6	nekonečnost
prostoru	prostor	k1gInSc2	prostor
též	též	k9	též
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
Slunečních	sluneční	k2eAgFnPc2d1	sluneční
soustav	soustava	k1gFnPc2	soustava
podobných	podobný	k2eAgFnPc2d1	podobná
naší	náš	k3xOp1gFnSc7	náš
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
přijetí	přijetí	k1gNnSc4	přijetí
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
byl	být	k5eAaImAgInS	být
upálen	upálen	k2eAgInSc1d1	upálen
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1600	[number]	k4	1600
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Campo	Campa	k1gFnSc5	Campa
dei	dei	k?	dei
Fiori	Fiori	k1gNnPc1	Fiori
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jako	jako	k8xS	jako
kacíř	kacíř	k1gMnSc1	kacíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc4	tento
pojetí	pojetí	k1gNnSc4	pojetí
vesmíru	vesmír	k1gInSc2	vesmír
přijal	přijmout	k5eAaPmAgMnS	přijmout
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
Christiaan	Christiaan	k1gMnSc1	Christiaan
Huygens	Huygens	k1gInSc1	Huygens
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
několik	několik	k4yIc1	několik
paradoxů	paradox	k1gInPc2	paradox
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
jsou	být	k5eAaImIp3nP	být
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
stále	stále	k6eAd1	stále
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nemůže	moct	k5eNaImIp3nS	moct
věčně	věčně	k6eAd1	věčně
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jean-Philippe	Jean-Philipp	k1gInSc5	Jean-Philipp
de	de	k?	de
Cheseaux	Cheseaux	k1gInSc1	Cheseaux
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
)	)	kIx)	)
upozornili	upozornit	k5eAaPmAgMnP	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
prostor	prostor	k1gInSc1	prostor
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
noční	noční	k2eAgFnSc1d1	noční
obloha	obloha	k1gFnSc1	obloha
zářit	zářit	k5eAaImF	zářit
tak	tak	k6eAd1	tak
jasně	jasně	k6eAd1	jasně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Slunce	slunce	k1gNnSc1	slunce
ve	v	k7c6	v
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dostal	dostat	k5eAaPmAgMnS	dostat
tento	tento	k3xDgInSc4	tento
paradox	paradox	k1gInSc4	paradox
název	název	k1gInSc1	název
Olbersův	Olbersův	k2eAgInSc1d1	Olbersův
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
Za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
<g/>
,	,	kIx,	,
Newton	Newton	k1gMnSc1	Newton
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nekonečný	konečný	k2eNgInSc1d1	nekonečný
prostor	prostor	k1gInSc1	prostor
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
vyplněný	vyplněný	k2eAgInSc1d1	vyplněný
hmotou	hmota	k1gFnSc7	hmota
by	by	kYmCp3nS	by
způsobil	způsobit	k5eAaPmAgMnS	způsobit
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
nestabilitu	nestabilita	k1gFnSc4	nestabilita
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hmota	hmota	k1gFnSc1	hmota
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nesnáz	nesnáz	k1gFnSc4	nesnáz
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
kritérium	kritérium	k1gNnSc4	kritérium
Jeansovy	Jeansův	k2eAgFnSc2d1	Jeansův
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
řešení	řešení	k1gNnPc2	řešení
posledních	poslední	k2eAgInPc2d1	poslední
dvou	dva	k4xCgInPc2	dva
paradoxů	paradox	k1gInPc2	paradox
byl	být	k5eAaImAgInS	být
Charlierův	Charlierův	k2eAgInSc1d1	Charlierův
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
hmota	hmota	k1gFnSc1	hmota
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
hierarchicky	hierarchicky	k6eAd1	hierarchicky
(	(	kIx(	(
<g/>
systémy	systém	k1gInPc1	systém
obíhajících	obíhající	k2eAgNnPc2d1	obíhající
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
větší	veliký	k2eAgInSc4d2	veliký
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
fraktálně	fraktálně	k6eAd1	fraktálně
uspořádán	uspořádán	k2eAgMnSc1d1	uspořádán
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zanedbatelně	zanedbatelně	k6eAd1	zanedbatelně
malou	malý	k2eAgFnSc4d1	malá
celkovou	celkový	k2eAgFnSc4d1	celková
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
model	model	k1gInSc1	model
vesmíru	vesmír	k1gInSc2	vesmír
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1761	[number]	k4	1761
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
pokrokem	pokrok	k1gInSc7	pokrok
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
např.	např.	kA	např.
Thomas	Thomas	k1gMnSc1	Thomas
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
pochopili	pochopit	k5eAaPmAgMnP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
nejsou	být	k5eNaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
se	se	k3xPyFc4	se
do	do	k7c2	do
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
<g/>
Moderní	moderní	k2eAgFnSc1d1	moderní
éra	éra	k1gFnSc1	éra
kosmologie	kosmologie	k1gFnSc2	kosmologie
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
poprvé	poprvé	k6eAd1	poprvé
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
svoji	svůj	k3xOyFgFnSc4	svůj
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
pro	pro	k7c4	pro
modelování	modelování	k1gNnSc4	modelování
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
dynamiky	dynamika	k1gFnSc2	dynamika
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
důsledky	důsledek	k1gInPc4	důsledek
je	být	k5eAaImIp3nS	být
podrobněji	podrobně	k6eAd2	podrobně
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teoretické	teoretický	k2eAgInPc1d1	teoretický
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
===	===	k?	===
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
převládá	převládat	k5eAaImIp3nS	převládat
v	v	k7c6	v
kosmologickém	kosmologický	k2eAgNnSc6d1	kosmologické
měřítku	měřítko	k1gNnSc6	měřítko
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
síly	síl	k1gInPc1	síl
hrají	hrát	k5eAaImIp3nP	hrát
zanedbatelnou	zanedbatelný	k2eAgFnSc4d1	zanedbatelná
roli	role	k1gFnSc4	role
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
struktur	struktura	k1gFnPc2	struktura
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
větších	veliký	k2eAgFnPc2d2	veliký
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerá	veškerý	k3xTgFnSc1	veškerý
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
a	a	k8xC	a
gravitační	gravitační	k2eAgInPc1d1	gravitační
účinky	účinek	k1gInPc1	účinek
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
kumulují	kumulovat	k5eAaImIp3nP	kumulovat
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
kladné	kladný	k2eAgInPc1d1	kladný
a	a	k8xC	a
záporné	záporný	k2eAgInPc1d1	záporný
náboje	náboj	k1gInPc1	náboj
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
ruší	rušit	k5eAaImIp3nS	rušit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
elektromagnetismus	elektromagnetismus	k1gInSc1	elektromagnetismus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kosmologických	kosmologický	k2eAgInPc6d1	kosmologický
měřítcích	měřítek	k1gInPc6	měřítek
relativně	relativně	k6eAd1	relativně
nevýznamný	významný	k2eNgMnSc1d1	nevýznamný
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
dvě	dva	k4xCgFnPc1	dva
interakce	interakce	k1gFnPc1	interakce
<g/>
,	,	kIx,	,
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
silná	silný	k2eAgFnSc1d1	silná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
klesají	klesat	k5eAaImIp3nP	klesat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
účinky	účinek	k1gInPc1	účinek
se	se	k3xPyFc4	se
omezují	omezovat	k5eAaImIp3nP	omezovat
především	především	k9	především
na	na	k7c4	na
subatomární	subatomární	k2eAgFnPc4d1	subatomární
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
vývoje	vývoj	k1gInSc2	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
však	však	k9	však
potřeba	potřeba	k6eAd1	potřeba
brát	brát	k5eAaImF	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
i	i	k8xC	i
globální	globální	k2eAgFnPc4d1	globální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
křivost	křivost	k1gFnSc1	křivost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
takovýchto	takovýto	k3xDgNnPc6	takovýto
měřítkách	měřítko	k1gNnPc6	měřítko
již	již	k6eAd1	již
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
první	první	k4xOgInPc1	první
seriózní	seriózní	k2eAgInPc1d1	seriózní
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
až	až	k9	až
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zahrnout	zahrnout	k5eAaPmF	zahrnout
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc4	tento
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
popisuje	popisovat	k5eAaImIp3nS	popisovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
zakřivením	zakřivení	k1gNnSc7	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
a	a	k8xC	a
rozložením	rozložení	k1gNnSc7	rozložení
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
něm.	něm.	k?	něm.
Zakřivení	zakřivení	k1gNnSc6	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
určuje	určovat	k5eAaImIp3nS	určovat
pohyb	pohyb	k1gInSc1	pohyb
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
podél	podél	k6eAd1	podél
nejkratších	krátký	k2eAgInPc6d3	nejkratší
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
spojnic	spojnice	k1gFnPc2	spojnice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
geodetik	geodetik	k1gMnSc1	geodetik
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
hmoty	hmota	k1gFnSc2	hmota
naopak	naopak	k6eAd1	naopak
určuje	určovat	k5eAaImIp3nS	určovat
zakřivení	zakřivení	k1gNnSc4	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
pomocí	pomocí	k7c2	pomocí
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
nelineární	lineární	k2eNgFnSc1d1	nelineární
parciální	parciální	k2eAgFnSc1d1	parciální
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
čase	čas	k1gInSc6	čas
i	i	k8xC	i
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Kosmologické	kosmologický	k2eAgInPc1d1	kosmologický
modely	model	k1gInPc1	model
vycházející	vycházející	k2eAgInPc1d1	vycházející
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
kosmologický	kosmologický	k2eAgInSc1d1	kosmologický
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
měřítka	měřítko	k1gNnSc2	měřítko
stovek	stovka	k1gFnPc2	stovka
megaparseků	megaparsek	k1gInPc2	megaparsek
homogenní	homogenní	k2eAgMnSc1d1	homogenní
a	a	k8xC	a
izotropní	izotropní	k2eAgMnSc1d1	izotropní
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitační	gravitační	k2eAgInPc1d1	gravitační
účinky	účinek	k1gInPc1	účinek
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
měřítka	měřítko	k1gNnSc2	měřítko
ekvivalentní	ekvivalentní	k2eAgNnSc1d1	ekvivalentní
působení	působení	k1gNnSc1	působení
jemného	jemný	k2eAgInSc2d1	jemný
prachu	prach	k1gInSc2	prach
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženého	rozložený	k2eAgInSc2d1	rozložený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
všude	všude	k6eAd1	všude
stejnou	stejný	k2eAgFnSc4d1	stejná
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předpoklad	předpoklad	k1gInSc1	předpoklad
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadno	snadno	k6eAd1	snadno
řešit	řešit	k5eAaImF	řešit
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
předpovědět	předpovědět	k5eAaPmF	předpovědět
minulost	minulost	k1gFnSc4	minulost
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc4	budoucnost
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
kosmologických	kosmologický	k2eAgInPc6d1	kosmologický
časových	časový	k2eAgInPc6d1	časový
měřítcích	měřítek	k1gInPc6	měřítek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
pole	pole	k1gNnSc2	pole
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kosmologickou	kosmologický	k2eAgFnSc4d1	kosmologická
konstantu	konstanta	k1gFnSc4	konstanta
(	(	kIx(	(
<g/>
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
interpretována	interpretován	k2eAgFnSc1d1	interpretována
jako	jako	k8xS	jako
hustota	hustota	k1gFnSc1	hustota
energie	energie	k1gFnSc2	energie
prázdného	prázdný	k2eAgInSc2d1	prázdný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
znaménku	znaménko	k1gNnSc6	znaménko
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
buď	buď	k8xC	buď
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
(	(	kIx(	(
<g/>
záporné	záporný	k2eAgFnPc4d1	záporná
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
či	či	k8xC	či
zrychlovat	zrychlovat	k5eAaImF	zrychlovat
(	(	kIx(	(
<g/>
kladné	kladný	k2eAgFnPc4d1	kladná
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
expanzi	expanze	k1gFnSc3	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vědců	vědec	k1gMnPc2	vědec
včetně	včetně	k7c2	včetně
Einsteina	Einstein	k1gMnSc2	Einstein
se	se	k3xPyFc4	se
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
nulovou	nulový	k2eAgFnSc4d1	nulová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Nedávná	dávný	k2eNgFnSc1d1	nedávná
astronomická	astronomický	k2eAgFnSc1d1	astronomická
pozorování	pozorování	k1gNnSc1	pozorování
supernov	supernova	k1gFnPc2	supernova
typu	typ	k1gInSc2	typ
Ia	ia	k0	ia
však	však	k9	však
objevila	objevit	k5eAaPmAgFnS	objevit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
"	"	kIx"	"
<g/>
temné	temný	k2eAgFnSc2d1	temná
energie	energie	k1gFnSc2	energie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
expanzi	expanze	k1gFnSc3	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Předběžné	předběžný	k2eAgFnPc1d1	předběžná
studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
temná	temný	k2eAgFnSc1d1	temná
energie	energie	k1gFnSc1	energie
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kladné	kladný	k2eAgFnSc3d1	kladná
hodnotě	hodnota	k1gFnSc3	hodnota
kosmologické	kosmologický	k2eAgFnSc2d1	kosmologická
konstanty	konstanta	k1gFnSc2	konstanta
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nelze	lze	k6eNd1	lze
ještě	ještě	k6eAd1	ještě
vyloučit	vyloučit	k5eAaPmF	vyloučit
jiné	jiný	k2eAgFnSc2d1	jiná
alternativní	alternativní	k2eAgFnSc2d1	alternativní
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
fyzik	fyzik	k1gMnSc1	fyzik
Zeldovič	Zeldovič	k1gMnSc1	Zeldovič
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Λ	Λ	k?	Λ
je	být	k5eAaImIp3nS	být
míra	míra	k1gFnSc1	míra
energie	energie	k1gFnSc2	energie
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
vakua	vakuum	k1gNnSc2	vakuum
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
virtuálních	virtuální	k2eAgFnPc2d1	virtuální
částic	částice	k1gFnPc2	částice
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
jen	jen	k9	jen
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
objemu	objem	k1gInSc6	objem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
vlastností	vlastnost	k1gFnSc7	vlastnost
kosmologické	kosmologický	k2eAgFnSc2d1	kosmologická
konstanty	konstanta	k1gFnSc2	konstanta
v	v	k7c6	v
Einsteinových	Einsteinových	k2eAgFnPc6d1	Einsteinových
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Casimirův	Casimirův	k2eAgInSc1d1	Casimirův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fridmanův	Fridmanův	k2eAgInSc1d1	Fridmanův
model	model	k1gInSc1	model
===	===	k?	===
</s>
</p>
<p>
<s>
Fridmanovy	Fridmanův	k2eAgFnPc1d1	Fridmanův
rovnice	rovnice	k1gFnPc1	rovnice
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
jsou	být	k5eAaImIp3nP	být
řešením	řešení	k1gNnSc7	řešení
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
homogenního	homogenní	k2eAgMnSc2d1	homogenní
a	a	k8xC	a
izotropního	izotropní	k2eAgInSc2d1	izotropní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgInSc4d1	speciální
tvar	tvar	k1gInSc4	tvar
metrického	metrický	k2eAgInSc2d1	metrický
tenzoru	tenzor	k1gInSc2	tenzor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
θ	θ	k?	θ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sin	sin	kA	sin
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
θ	θ	k?	θ
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ds	ds	k?	ds
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
dt	dt	k?	dt
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
dr	dr	kA	dr
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-kr	r	k?	-kr
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
theta	theta	k1gMnSc1	theta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
theta	theta	k1gFnSc1	theta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
phi	phi	k?	phi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tzv.	tzv.	kA	tzv.
Fridman-Lemaîter-Robertson-Walkerovu	Fridman-Lemaîter-Robertson-Walkerův	k2eAgFnSc4d1	Fridman-Lemaîter-Robertson-Walkerův
metriku	metrika	k1gFnSc4	metrika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
principu	princip	k1gInSc2	princip
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nezávisle	závisle	k6eNd1	závisle
objevena	objevit	k5eAaPmNgFnS	objevit
čtyřmi	čtyři	k4xCgNnPc7	čtyři
vědci	vědec	k1gMnPc7	vědec
<g/>
:	:	kIx,	:
Fridmanem	Fridman	k1gMnSc7	Fridman
<g/>
,	,	kIx,	,
Lemaîtrem	Lemaîtr	k1gMnSc7	Lemaîtr
<g/>
,	,	kIx,	,
Robertsonem	Robertson	k1gMnSc7	Robertson
a	a	k8xC	a
Walkerem	Walker	k1gMnSc7	Walker
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
můžeme	moct	k5eAaImIp1nP	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
metrika	metrika	k1gFnSc1	metrika
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
časoprostor	časoprostor	k1gInSc4	časoprostor
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
v	v	k7c6	v
Eukleidově	Eukleidův	k2eAgInSc6d1	Eukleidův
prostoru	prostor	k1gInSc6	prostor
-	-	kIx~	-
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
časoprostorový	časoprostorový	k2eAgInSc4d1	časoprostorový
interval	interval	k1gInSc4	interval
(	(	kIx(	(
<g/>
analogie	analogie	k1gFnSc1	analogie
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
blízkými	blízký	k2eAgInPc7d1	blízký
body	bod	k1gInPc7	bod
popsanými	popsaný	k2eAgInPc7d1	popsaný
rozdíly	rozdíl	k1gInPc7	rozdíl
souřadnic	souřadnice	k1gFnPc2	souřadnice
t	t	k?	t
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
θ	θ	k?	θ
<g/>
,	,	kIx,	,
φ	φ	k?	φ
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
metrika	metrika	k1gFnSc1	metrika
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
neurčené	určený	k2eNgInPc4d1	neurčený
parametry	parametr	k1gInPc4	parametr
<g/>
:	:	kIx,	:
celkové	celkový	k2eAgNnSc4d1	celkové
měřítko	měřítko	k1gNnSc4	měřítko
délky	délka	k1gFnSc2	délka
R	R	kA	R
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
index	index	k1gInSc1	index
zakřivení	zakřivení	k1gNnSc2	zakřivení
k	k	k7c3	k
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
hodnot	hodnota	k1gFnPc2	hodnota
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
nebo	nebo	k8xC	nebo
-1	-1	k4	-1
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
ploché	plochý	k2eAgFnSc6d1	plochá
eukleidovské	eukleidovský	k2eAgFnSc6d1	eukleidovská
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prostoru	prostor	k1gInSc2	prostor
s	s	k7c7	s
kladným	kladný	k2eAgNnSc7d1	kladné
či	či	k8xC	či
záporným	záporný	k2eAgNnSc7d1	záporné
zakřivením	zakřivení	k1gNnSc7	zakřivení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
R	R	kA	R
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgFnPc4	veškerý
prostorové	prostorový	k2eAgFnPc4d1	prostorová
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
rozšíření	rozšíření	k1gNnSc3	rozšíření
nebo	nebo	k8xC	nebo
smrštění	smrštění	k1gNnSc3	smrštění
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pozorování	pozorování	k1gNnSc1	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
galaxie	galaxie	k1gFnPc1	galaxie
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
prostoru	prostor	k1gInSc2	prostor
také	také	k9	také
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
za	za	k7c4	za
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
paradox	paradox	k1gInSc4	paradox
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvě	dva	k4xCgFnPc1	dva
galaxie	galaxie	k1gFnPc1	galaxie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
40	[number]	k4	40
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vycházely	vycházet	k5eAaImAgFnP	vycházet
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
místa	místo	k1gNnSc2	místo
prostoru	prostor	k1gInSc2	prostor
před	před	k7c7	před
13,8	[number]	k4	13,8
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nepohybovaly	pohybovat	k5eNaImAgInP	pohybovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
rovnice	rovnice	k1gFnSc1	rovnice
popisující	popisující	k2eAgFnSc1d1	popisující
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
R	R	kA	R
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
jako	jako	k8xS	jako
Fridmanovy	Fridmanův	k2eAgFnPc1d1	Fridmanův
rovnice	rovnice	k1gFnPc1	rovnice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
̇	̇	k?	̇
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Λ	Λ	k?	Λ
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
dot	dot	k?	dot
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
kc	kc	k?	kc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
̈	̈	k?	̈
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
G	G	kA	G
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
ρ	ρ	k?	ρ
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Λ	Λ	k?	Λ
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ddot	ddot	k1gMnSc1	ddot
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
G	G	kA	G
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
řešení	řešení	k1gNnSc1	řešení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
několika	několik	k4yIc6	několik
parametrech	parametr	k1gInPc6	parametr
<g/>
:	:	kIx,	:
kosmologické	kosmologický	k2eAgFnSc3d1	kosmologická
konstantě	konstanta	k1gFnSc3	konstanta
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnSc6d1	průměrná
hustotě	hustota	k1gFnSc6	hustota
látky	látka	k1gFnSc2	látka
ρ	ρ	k?	ρ
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc3	tlak
(	(	kIx(	(
<g/>
především	především	k9	především
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc1	záření
p	p	k?	p
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc6d1	gravitační
konstantě	konstanta	k1gFnSc6	konstanta
G	G	kA	G
a	a	k8xC	a
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
parametru	parametr	k1gInSc6	parametr
křivosti	křivost	k1gFnSc2	křivost
k.	k.	k?	k.
Podle	podle	k7c2	podle
hodnot	hodnota	k1gFnPc2	hodnota
těchto	tento	k3xDgInPc2	tento
parametrů	parametr	k1gInPc2	parametr
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rovnic	rovnice	k1gFnPc2	rovnice
několik	několik	k4yIc1	několik
scénářů	scénář	k1gInPc2	scénář
a	a	k8xC	a
obecných	obecný	k2eAgNnPc2d1	obecné
pozorování	pozorování	k1gNnPc2	pozorování
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statický	statický	k2eAgInSc1d1	statický
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
délkové	délkový	k2eAgNnSc4d1	délkové
měřítko	měřítko	k1gNnSc4	měřítko
R	R	kA	R
zůstane	zůstat	k5eAaPmIp3nS	zůstat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vesmír	vesmír	k1gInSc1	vesmír
kladnou	kladný	k2eAgFnSc4d1	kladná
křivost	křivost	k1gFnSc4	křivost
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
vyladěné	vyladěný	k2eAgFnPc4d1	vyladěná
hodnoty	hodnota	k1gFnPc4	hodnota
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
kosmologické	kosmologický	k2eAgFnSc2d1	kosmologická
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
upozornil	upozornit	k5eAaPmAgMnS	upozornit
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rovnováha	rovnováha	k1gFnSc1	rovnováha
je	být	k5eAaImIp3nS	být
však	však	k9	však
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
musely	muset	k5eAaImAgFnP	muset
zvrátit	zvrátit	k5eAaPmF	zvrátit
drobné	drobný	k2eAgFnPc4d1	drobná
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
počáteční	počáteční	k2eAgFnSc2d1	počáteční
izotropie	izotropie	k1gFnSc2	izotropie
a	a	k8xC	a
homogenity	homogenita	k1gFnSc2	homogenita
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
však	však	k9	však
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dobře	dobře	k6eAd1	dobře
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
předpovědí	předpověď	k1gFnSc7	předpověď
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dynamický	dynamický	k2eAgInSc4d1	dynamický
vesmír	vesmír	k1gInSc4	vesmír
obsahující	obsahující	k2eAgInSc4d1	obsahující
baryonovou	baryonový	k2eAgFnSc4d1	baryonová
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
záření	záření	k1gNnSc4	záření
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Fridmanových	Fridmanův	k2eAgFnPc6d1	Fridmanův
rovnicích	rovnice	k1gFnPc6	rovnice
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
fázích	fág	k1gInPc6	fág
dominantní	dominantní	k2eAgInPc4d1	dominantní
různé	různý	k2eAgInPc4d1	různý
členy	člen	k1gInPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
vesmíru	vesmír	k1gInSc6	vesmír
hraje	hrát	k5eAaImIp3nS	hrát
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
roli	role	k1gFnSc4	role
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
relativní	relativní	k2eAgInSc1d1	relativní
podíl	podíl	k1gInSc1	podíl
roste	růst	k5eAaImIp3nS	růst
při	při	k7c6	při
zmenšování	zmenšování	k1gNnSc6	zmenšování
R	R	kA	R
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
hustota	hustota	k1gFnSc1	hustota
běžné	běžný	k2eAgFnSc2d1	běžná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
fázi	fáze	k1gFnSc6	fáze
hraje	hrát	k5eAaImIp3nS	hrát
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
hustota	hustota	k1gFnSc1	hustota
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přebírá	přebírat	k5eAaImIp3nS	přebírat
diktát	diktát	k1gInSc1	diktát
parametr	parametr	k1gInSc1	parametr
křivosti	křivost	k1gFnSc2	křivost
k	k	k7c3	k
a	a	k8xC	a
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
teoriích	teorie	k1gFnPc6	teorie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
inflační	inflační	k2eAgFnSc2d1	inflační
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
dominují	dominovat	k5eAaImIp3nP	dominovat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
vesmíru	vesmír	k1gInSc6	vesmír
jiné	jiný	k2eAgFnSc2d1	jiná
formy	forma	k1gFnSc2	forma
látky	látka	k1gFnSc2	látka
s	s	k7c7	s
exotickou	exotický	k2eAgFnSc7d1	exotická
stavovou	stavový	k2eAgFnSc7d1	stavová
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řešení	řešení	k1gNnSc1	řešení
Fridmanových	Fridmanův	k2eAgFnPc2d1	Fridmanův
rovnic	rovnice	k1gFnPc2	rovnice
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
začal	začít	k5eAaPmAgInS	začít
gravitační	gravitační	k2eAgFnSc7d1	gravitační
singularitou	singularita	k1gFnSc7	singularita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
parametr	parametr	k1gInSc4	parametr
R	R	kA	R
nulový	nulový	k2eAgMnSc1d1	nulový
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
nabývaly	nabývat	k5eAaImAgFnP	nabývat
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zdát	zdát	k5eAaPmF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
slabě	slabě	k6eAd1	slabě
podložený	podložený	k2eAgMnSc1d1	podložený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
nejistých	jistý	k2eNgInPc6d1	nejistý
předpokladech	předpoklad	k1gInPc6	předpoklad
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
homogenity	homogenita	k1gFnSc2	homogenita
a	a	k8xC	a
izotropie	izotropie	k1gFnSc2	izotropie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
doložen	doložen	k2eAgMnSc1d1	doložen
také	také	k6eAd1	také
Hawkingovou	Hawkingový	k2eAgFnSc7d1	Hawkingová
a	a	k8xC	a
Penroseovou	Penroseův	k2eAgFnSc7d1	Penroseova
teorií	teorie	k1gFnSc7	teorie
singularity	singularita	k1gFnSc2	singularita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počáteční	počáteční	k2eAgFnSc1d1	počáteční
singularita	singularita	k1gFnSc1	singularita
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
existovat	existovat	k5eAaImF	existovat
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
všeobecných	všeobecný	k2eAgFnPc2d1	všeobecná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nesmíme	smět	k5eNaImIp1nP	smět
však	však	k9	však
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
pracujeme	pracovat	k5eAaImIp1nP	pracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obecné	obecná	k1gFnSc2	obecná
teorii	teorie	k1gFnSc3	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
závěr	závěr	k1gInSc1	závěr
tak	tak	k6eAd1	tak
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
její	její	k3xOp3gFnSc2	její
platnosti	platnost	k1gFnSc2	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
začal	začít	k5eAaPmAgInS	začít
nepředstavitelně	představitelně	k6eNd1	představitelně
horkým	horký	k2eAgInSc7d1	horký
a	a	k8xC	a
hustým	hustý	k2eAgInSc7d1	hustý
stavem	stav	k1gInSc7	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
singularitě	singularita	k1gFnSc6	singularita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
podstatou	podstata	k1gFnSc7	podstata
modelu	model	k1gInSc2	model
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečné	konečný	k2eNgFnPc4d1	nekonečná
hustoty	hustota	k1gFnPc4	hustota
počáteční	počáteční	k2eAgFnSc2d1	počáteční
singularity	singularita	k1gFnSc2	singularita
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
potřeba	potřeba	k6eAd1	potřeba
použít	použít	k5eAaPmF	použít
přesnější	přesný	k2eAgFnSc4d2	přesnější
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
osud	osud	k1gInSc1	osud
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kriticky	kriticky	k6eAd1	kriticky
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
indexu	index	k1gInSc6	index
zakřivení	zakřivení	k1gNnSc2	zakřivení
k	k	k7c3	k
a	a	k8xC	a
kosmologické	kosmologický	k2eAgFnSc3d1	kosmologická
konstantě	konstanta	k1gFnSc3	konstanta
Λ	Λ	k?	Λ
Vesmír	vesmír	k1gInSc1	vesmír
se	s	k7c7	s
zápornou	záporný	k2eAgFnSc7d1	záporná
kosmologickou	kosmologický	k2eAgFnSc7d1	kosmologická
konstantou	konstanta	k1gFnSc7	konstanta
vždy	vždy	k6eAd1	vždy
skončí	skončit	k5eAaPmIp3nS	skončit
velkým	velký	k2eAgInSc7d1	velký
křachem	křach	k1gInSc7	křach
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
čeká	čekat	k5eAaImIp3nS	čekat
i	i	k9	i
dostatečně	dostatečně	k6eAd1	dostatečně
hustý	hustý	k2eAgInSc4d1	hustý
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
+1	+1	k4	+1
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
průměrné	průměrný	k2eAgNnSc1d1	průměrné
zakřivení	zakřivení	k1gNnSc1	zakřivení
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
kladné	kladný	k2eAgNnSc1d1	kladné
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
vesmír	vesmír	k1gInSc1	vesmír
dostatečně	dostatečně	k6eAd1	dostatečně
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
,	,	kIx,	,
k	k	k7c3	k
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
0	[number]	k4	0
(	(	kIx(	(
<g/>
plochý	plochý	k2eAgInSc1d1	plochý
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
-1	-1	k4	-1
(	(	kIx(	(
<g/>
otevřený	otevřený	k2eAgInSc1d1	otevřený
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
donekonečna	donekonečna	k6eAd1	donekonečna
<g/>
,	,	kIx,	,
zchladne	zchladnout	k5eAaPmIp3nS	zchladnout
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
nehostinným	hostinný	k2eNgMnSc7d1	nehostinný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
skončí	skončit	k5eAaPmIp3nS	skončit
i	i	k9	i
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
dost	dost	k6eAd1	dost
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
měření	měření	k1gNnPc1	měření
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozpínání	rozpínání	k1gNnSc1	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
očekávání	očekávání	k1gNnSc3	očekávání
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
vesmír	vesmír	k1gInSc4	vesmír
s	s	k7c7	s
kladnou	kladný	k2eAgFnSc7d1	kladná
kosmologickou	kosmologický	k2eAgFnSc7d1	kosmologická
konstantou	konstanta	k1gFnSc7	konstanta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
rozpínat	rozpínat	k5eAaImF	rozpínat
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teorie	teorie	k1gFnSc1	teorie
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Převažující	převažující	k2eAgInSc1d1	převažující
model	model	k1gInSc1	model
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
experimentálních	experimentální	k2eAgNnPc2d1	experimentální
měření	měření	k1gNnPc2	měření
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vztah	vztah	k1gInSc4	vztah
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
rudého	rudý	k2eAgInSc2d1	rudý
posuvu	posuv	k1gInSc2	posuv
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
poměru	poměr	k1gInSc3	poměr
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
všudypřítomného	všudypřítomný	k2eAgMnSc2d1	všudypřítomný
<g/>
,	,	kIx,	,
izotropního	izotropní	k2eAgNnSc2d1	izotropní
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
kosmického	kosmický	k2eAgNnSc2d1	kosmické
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
rozpínání	rozpínání	k1gNnSc6	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
prostor	prostor	k1gInSc4	prostor
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
se	se	k3xPyFc4	se
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
fotonu	foton	k1gInSc2	foton
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jak	jak	k6eAd1	jak
klesá	klesat	k5eAaImIp3nS	klesat
jeho	jeho	k3xOp3gFnSc2	jeho
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
delší	dlouhý	k2eAgMnSc1d2	delší
cestu	cesta	k1gFnSc4	cesta
musel	muset	k5eAaImAgInS	muset
foton	foton	k1gInSc1	foton
absolvovat	absolvovat	k5eAaPmF	absolvovat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
expanzi	expanze	k1gFnSc4	expanze
vesmíru	vesmír	k1gInSc2	vesmír
zažil	zažít	k5eAaPmAgInS	zažít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
záření	záření	k1gNnSc1	záření
nejstarších	starý	k2eAgInPc2d3	nejstarší
fotonů	foton	k1gInPc2	foton
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
galaxií	galaxie	k1gFnPc2	galaxie
nejvíce	nejvíce	k6eAd1	nejvíce
posunuto	posunut	k2eAgNnSc1d1	posunuto
do	do	k7c2	do
červené	červený	k2eAgFnSc2d1	červená
oblasti	oblast	k1gFnSc2	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
korelace	korelace	k1gFnSc2	korelace
mezi	mezi	k7c7	mezi
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
a	a	k8xC	a
rudým	rudý	k2eAgInSc7d1	rudý
posuvem	posuv	k1gInSc7	posuv
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
problémem	problém	k1gInSc7	problém
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
experimentální	experimentální	k2eAgNnSc1d1	experimentální
pozorování	pozorování	k1gNnSc1	pozorování
expanze	expanze	k1gFnSc2	expanze
vesmíru	vesmír	k1gInSc2	vesmír
může	moct	k5eAaImIp3nS	moct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
kombinace	kombinace	k1gFnSc1	kombinace
jaderné	jaderný	k2eAgFnSc2d1	jaderná
a	a	k8xC	a
atomové	atomový	k2eAgFnSc2d1	atomová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
probíhala	probíhat	k5eAaImAgFnS	probíhat
expanze	expanze	k1gFnSc1	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
energie	energie	k1gFnSc1	energie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
klesala	klesat	k5eAaImAgFnS	klesat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
hustota	hustota	k1gFnSc1	hustota
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
energie	energie	k1gFnSc1	energie
fotonu	foton	k1gInSc2	foton
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
hustota	hustota	k1gFnSc1	hustota
energie	energie	k1gFnSc2	energie
vakua	vakuum	k1gNnSc2	vakuum
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
dominuje	dominovat	k5eAaImIp3nS	dominovat
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
dominovalo	dominovat	k5eAaImAgNnS	dominovat
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
poeticky	poeticky	k6eAd1	poeticky
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
rozpínal	rozpínat	k5eAaImAgInS	rozpínat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
energie	energie	k1gFnSc2	energie
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
stával	stávat	k5eAaImAgInS	stávat
se	s	k7c7	s
chladnějším	chladný	k2eAgMnSc7d2	chladnější
a	a	k8xC	a
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
hmoty	hmota	k1gFnSc2	hmota
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
spojovat	spojovat	k5eAaImF	spojovat
do	do	k7c2	do
stále	stále	k6eAd1	stále
složitějších	složitý	k2eAgInPc2d2	složitější
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
vesmíru	vesmír	k1gInSc2	vesmír
s	s	k7c7	s
převládající	převládající	k2eAgFnSc7d1	převládající
hmotou	hmota	k1gFnSc7	hmota
mohly	moct	k5eAaImAgFnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
stabilní	stabilní	k2eAgInPc4d1	stabilní
protony	proton	k1gInPc4	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sdružovaly	sdružovat	k5eAaImAgFnP	sdružovat
do	do	k7c2	do
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
byl	být	k5eAaImAgInS	být
vesmír	vesmír	k1gInSc4	vesmír
velmi	velmi	k6eAd1	velmi
horkým	horký	k2eAgMnSc7d1	horký
a	a	k8xC	a
hustým	hustý	k2eAgNnSc7d1	husté
plazmatem	plazma	k1gNnSc7	plazma
ze	z	k7c2	z
záporných	záporný	k2eAgInPc2d1	záporný
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
neutrálních	neutrální	k2eAgNnPc2d1	neutrální
neutrin	neutrino	k1gNnPc2	neutrino
a	a	k8xC	a
kladných	kladný	k2eAgNnPc2d1	kladné
jader	jádro	k1gNnPc2	jádro
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
reakce	reakce	k1gFnPc1	reakce
mezi	mezi	k7c7	mezi
jádry	jádro	k1gNnPc7	jádro
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
hojnosti	hojnost	k1gFnSc3	hojnost
lehčích	lehký	k2eAgNnPc2d2	lehčí
jader	jádro	k1gNnPc2	jádro
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
deuteria	deuterium	k1gNnSc2	deuterium
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
jádra	jádro	k1gNnPc1	jádro
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
stabilních	stabilní	k2eAgInPc2d1	stabilní
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
průhledným	průhledný	k2eAgMnSc7d1	průhledný
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
se	se	k3xPyFc4	se
záření	záření	k1gNnSc1	záření
oddělilo	oddělit	k5eAaPmAgNnS	oddělit
od	od	k7c2	od
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
všudypřítomné	všudypřítomný	k2eAgNnSc1d1	všudypřítomné
<g/>
,	,	kIx,	,
izotropní	izotropní	k2eAgNnSc1d1	izotropní
mikrovlnné	mikrovlnný	k2eAgNnSc1d1	mikrovlnné
záření	záření	k1gNnSc1	záření
kosmického	kosmický	k2eAgNnSc2d1	kosmické
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dodnes	dodnes	k6eAd1	dodnes
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
jiná	jiný	k2eAgNnPc1d1	jiné
pozorování	pozorování	k1gNnPc1	pozorování
nemůže	moct	k5eNaImIp3nS	moct
současná	současný	k2eAgFnSc1d1	současná
fyzika	fyzika	k1gFnSc1	fyzika
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
převládající	převládající	k2eAgFnSc2d1	převládající
teorie	teorie	k1gFnSc2	teorie
hmoty	hmota	k1gFnSc2	hmota
nad	nad	k7c7	nad
antihmotou	antihmota	k1gFnSc7	antihmota
mírně	mírně	k6eAd1	mírně
převládala	převládat	k5eAaImAgFnS	převládat
už	už	k6eAd1	už
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
možná	možná	k9	možná
díky	díky	k7c3	díky
narušení	narušení	k1gNnSc3	narušení
CP	CP	kA	CP
invariance	invariance	k1gFnPc1	invariance
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
anihilace	anihilace	k1gFnSc1	anihilace
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
antihmoty	antihmota	k1gFnSc2	antihmota
většinu	většina	k1gFnSc4	většina
hmoty	hmota	k1gFnSc2	hmota
zničila	zničit	k5eAaPmAgFnS	zničit
a	a	k8xC	a
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
fotony	foton	k1gInPc4	foton
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
zbytek	zbytek	k1gInSc4	zbytek
hmoty	hmota	k1gFnSc2	hmota
existuje	existovat	k5eAaImIp3nS	existovat
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
dnešní	dnešní	k2eAgInSc4d1	dnešní
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
důkazy	důkaz	k1gInPc1	důkaz
rovněž	rovněž	k9	rovněž
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlá	rychlý	k2eAgFnSc1d1	rychlá
kosmická	kosmický	k2eAgFnSc1d1	kosmická
inflace	inflace	k1gFnSc1	inflace
vesmíru	vesmír	k1gInSc2	vesmír
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
35	[number]	k4	35
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedávná	dávný	k2eNgNnPc1d1	nedávné
pozorování	pozorování	k1gNnPc1	pozorování
také	také	k9	také
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
souhrnné	souhrnný	k2eAgFnSc6d1	souhrnná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
a	a	k8xC	a
energii	energie	k1gFnSc6	energie
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
dominují	dominovat	k5eAaImIp3nP	dominovat
temná	temný	k2eAgFnSc1d1	temná
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
temná	temný	k2eAgFnSc1d1	temná
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgFnP	být
vědecky	vědecky	k6eAd1	vědecky
popsány	popsat	k5eAaPmNgFnP	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nP	lišit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
gravitační	gravitační	k2eAgInPc1d1	gravitační
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Temná	temný	k2eAgFnSc1d1	temná
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
gravitací	gravitace	k1gFnSc7	gravitace
a	a	k8xC	a
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
expanzi	expanze	k1gFnSc4	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
temná	temný	k2eAgFnSc1d1	temná
energie	energie	k1gFnSc1	energie
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
expanzi	expanze	k1gFnSc3	expanze
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teorie	teorie	k1gFnPc4	teorie
mnohovesmíru	mnohovesmíra	k1gFnSc4	mnohovesmíra
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
spekulativní	spekulativní	k2eAgFnPc1d1	spekulativní
kosmologické	kosmologický	k2eAgFnPc1d1	kosmologická
teorie	teorie	k1gFnPc1	teorie
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
vesmírů	vesmír	k1gInPc2	vesmír
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
mnohovesmír	mnohovesmíra	k1gFnPc2	mnohovesmíra
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
multiverse	multiverse	k1gFnSc2	multiverse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
psychologem	psycholog	k1gMnSc7	psycholog
a	a	k8xC	a
filozofem	filozof	k1gMnSc7	filozof
Williamem	William	k1gInSc7	William
Jamesem	James	k1gMnSc7	James
<g/>
.	.	kIx.	.
</s>
<s>
Vesmíry	vesmír	k1gInPc1	vesmír
tvořící	tvořící	k2eAgInPc1d1	tvořící
mnohovesmír	mnohovesmíra	k1gFnPc2	mnohovesmíra
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
paralelní	paralelní	k2eAgInPc4d1	paralelní
vesmíry	vesmír	k1gInPc4	vesmír
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
jejich	jejich	k3xOp3gFnSc2	jejich
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
interakce	interakce	k1gFnSc2	interakce
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
modelu	model	k1gInSc3	model
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
mnohovesmíru	mnohovesmíra	k1gFnSc4	mnohovesmíra
a	a	k8xC	a
oddělených	oddělený	k2eAgInPc2d1	oddělený
vesmírů	vesmír	k1gInPc2	vesmír
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
biskup	biskup	k1gMnSc1	biskup
Étienne	Étienn	k1gInSc5	Étienn
Tempier	Tempier	k1gInSc4	Tempier
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1277	[number]	k4	1277
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
mohl	moct	k5eAaImAgMnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
tolik	tolik	k4yIc4	tolik
vesmírů	vesmír	k1gInPc2	vesmír
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
uznal	uznat	k5eAaPmAgInS	uznat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
hodně	hodně	k6eAd1	hodně
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
<g/>
.	.	kIx.	.
<g/>
Kosmolog	kosmolog	k1gMnSc1	kosmolog
Max	Max	k1gMnSc1	Max
Tegmark	Tegmark	k1gInSc4	Tegmark
klasifikoval	klasifikovat	k5eAaImAgMnS	klasifikovat
nejčastěji	často	k6eAd3	často
diskutované	diskutovaný	k2eAgInPc4d1	diskutovaný
modely	model	k1gInPc4	model
mnohovesmíru	mnohovesmíra	k1gFnSc4	mnohovesmíra
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
kategorií	kategorie	k1gFnPc2	kategorie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohovesmír	mnohovesmír	k1gMnSc1	mnohovesmír
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mnoho	mnoho	k4c1	mnoho
světů	svět	k1gInPc2	svět
řádu	řád	k1gInSc2	řád
nižšího	nízký	k2eAgInSc2d2	nižší
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Časoprostor	časoprostor	k1gInSc1	časoprostor
za	za	k7c7	za
kosmologickým	kosmologický	k2eAgInSc7d1	kosmologický
horizontem	horizont	k1gInSc7	horizont
</s>
</p>
<p>
<s>
Inflační	inflační	k2eAgFnSc1d1	inflační
teorie	teorie	k1gFnSc1	teorie
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vesmíru	vesmír	k1gInSc2	vesmír
velmi	velmi	k6eAd1	velmi
rychlou	rychlý	k2eAgFnSc4d1	rychlá
metrickou	metrický	k2eAgFnSc4d1	metrická
expanzi	expanze	k1gFnSc4	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
vesmír	vesmír	k1gInSc1	vesmír
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
nemůžeme	moct	k5eNaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
světlo	světlo	k1gNnSc1	světlo
ještě	ještě	k9	ještě
nestihlo	stihnout	k5eNaPmAgNnS	stihnout
dorazit	dorazit	k5eAaPmF	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
scénářích	scénář	k1gInPc6	scénář
vývoje	vývoj	k1gInSc2	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
světlo	světlo	k1gNnSc4	světlo
nedorazí	dorazit	k5eNaPmIp3nS	dorazit
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nevidíme	vidět	k5eNaImIp1nP	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
pozorovatelný	pozorovatelný	k2eAgInSc4d1	pozorovatelný
vesmír	vesmír	k1gInSc4	vesmír
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
multiverzum	multiverzum	k1gInSc1	multiverzum
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Vesmíry	vesmír	k1gInPc1	vesmír
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
konstantami	konstanta	k1gFnPc7	konstanta
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
věčné	věčný	k2eAgFnSc2d1	věčná
inflace	inflace	k1gFnSc2	inflace
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
falešného	falešný	k2eAgNnSc2d1	falešné
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
exponenciální	exponenciální	k2eAgNnSc4d1	exponenciální
rozpínání	rozpínání	k1gNnSc4	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
počátku	počátek	k1gInSc6	počátek
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
všude	všude	k6eAd1	všude
najednou	najednou	k6eAd1	najednou
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
klesající	klesající	k2eAgFnSc4d1	klesající
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lokálně	lokálně	k6eAd1	lokálně
<g/>
,	,	kIx,	,
formou	forma	k1gFnSc7	forma
kvantového	kvantový	k2eAgNnSc2d1	kvantové
tunelování	tunelování	k1gNnSc2	tunelování
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tak	tak	k9	tak
zárodečná	zárodečný	k2eAgFnSc1d1	zárodečná
bublina	bublina	k1gFnSc1	bublina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
řádově	řádově	k6eAd1	řádově
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
okolní	okolní	k2eAgInSc4d1	okolní
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
bublinách	bublina	k1gFnPc6	bublina
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
různému	různý	k2eAgNnSc3d1	různé
spontánnímu	spontánní	k2eAgNnSc3d1	spontánní
narušení	narušení	k1gNnSc3	narušení
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
obecně	obecně	k6eAd1	obecně
jinou	jiný	k2eAgFnSc4d1	jiná
hodnotu	hodnota	k1gFnSc4	hodnota
různých	různý	k2eAgFnPc2d1	různá
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
vesmíry	vesmír	k1gInPc1	vesmír
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
multiverzem	multiverz	k1gMnSc7	multiverz
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
teorie	teorie	k1gFnSc1	teorie
oscilujícího	oscilující	k2eAgInSc2d1	oscilující
vesmíru	vesmír	k1gInSc2	vesmír
Johna	John	k1gMnSc2	John
A.	A.	kA	A.
Wheelera	Wheeler	k1gMnSc2	Wheeler
nebo	nebo	k8xC	nebo
teorie	teorie	k1gFnSc2	teorie
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
Lee	Lea	k1gFnSc3	Lea
Smolina	smolina	k1gFnSc1	smolina
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Interpretace	interpretace	k1gFnPc1	interpretace
mnoha	mnoho	k4c2	mnoho
světů	svět	k1gInPc2	svět
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
</s>
</p>
<p>
<s>
Everettova	Everettův	k2eAgFnSc1d1	Everettova
teorie	teorie	k1gFnSc1	teorie
mnoha	mnoho	k4c2	mnoho
světů	svět	k1gInPc2	svět
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
standardních	standardní	k2eAgFnPc2d1	standardní
interpretací	interpretace	k1gFnPc2	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
globální	globální	k2eAgFnSc7d1	globální
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
neprochází	procházet	k5eNaImIp3nS	procházet
kolapsem	kolaps	k1gInSc7	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
provádíme	provádět	k5eAaImIp1nP	provádět
měření	měření	k1gNnSc4	měření
na	na	k7c6	na
dílčím	dílčí	k2eAgInSc6d1	dílčí
kvantovém	kvantový	k2eAgInSc6d1	kvantový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
realizují	realizovat	k5eAaBmIp3nP	realizovat
všechny	všechen	k3xTgInPc1	všechen
možné	možný	k2eAgInPc1d1	možný
výsledky	výsledek	k1gInPc1	výsledek
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnostní	pravděpodobnostní	k2eAgInSc1d1	pravděpodobnostní
charakter	charakter	k1gInSc1	charakter
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc1	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
se	se	k3xPyFc4	se
budeme	být	k5eAaImBp1nP	být
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
vesmíru	vesmír	k1gInSc2	vesmír
nacházet	nacházet	k5eAaImF	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
různé	různý	k2eAgInPc1d1	různý
vesmíry	vesmír	k1gInPc1	vesmír
multiverza	multiverza	k1gFnSc1	multiverza
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdáleny	vzdálen	k2eAgFnPc1d1	vzdálena
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
větve	větev	k1gFnPc1	větev
mnohasvětové	mnohasvětový	k2eAgFnSc2d1	mnohasvětový
interpretace	interpretace	k1gFnSc2	interpretace
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdáleny	vzdálen	k2eAgFnPc1d1	vzdálena
v	v	k7c6	v
nekonečněrozměrném	konečněrozměrný	k2eNgInSc6d1	konečněrozměrný
Hilbertově	Hilbertův	k2eAgInSc6d1	Hilbertův
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
globální	globální	k2eAgFnSc1d1	globální
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.4	.4	k4	.4
<g/>
.	.	kIx.	.
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Soubor	soubor	k1gInSc1	soubor
všech	všecek	k3xTgInPc2	všecek
vesmírů	vesmír	k1gInPc2	vesmír
dostatečně	dostatečně	k6eAd1	dostatečně
popsatelných	popsatelný	k2eAgInPc2d1	popsatelný
matematickou	matematický	k2eAgFnSc7d1	matematická
strukturou	struktura	k1gFnSc7	struktura
</s>
</p>
<p>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
Tegmark	Tegmark	k1gInSc1	Tegmark
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
připsat	připsat	k5eAaPmF	připsat
rovnou	rovnou	k6eAd1	rovnou
měrou	míra	k1gFnSc7wR	míra
existenci	existence	k1gFnSc4	existence
všem	všecek	k3xTgInPc3	všecek
myslitelným	myslitelný	k2eAgInPc3d1	myslitelný
vesmírům	vesmír	k1gInPc3	vesmír
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
lze	lze	k6eAd1	lze
dostatečně	dostatečně	k6eAd1	dostatečně
formálně	formálně	k6eAd1	formálně
popsat	popsat	k5eAaPmF	popsat
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
matematickou	matematický	k2eAgFnSc7d1	matematická
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tegmarkovu	Tegmarkův	k2eAgFnSc4d1	Tegmarkův
hypotézu	hypotéza	k1gFnSc4	hypotéza
dále	daleko	k6eAd2	daleko
rozvedl	rozvést	k5eAaPmAgInS	rozvést
Jürgen	Jürgen	k1gInSc1	Jürgen
Schmidhuber	Schmidhuber	k1gInSc4	Schmidhuber
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
kritizovat	kritizovat	k5eAaImF	kritizovat
především	především	k9	především
vágní	vágní	k2eAgFnSc4d1	vágní
definici	definice	k1gFnSc4	definice
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
každá	každý	k3xTgFnSc1	každý
myslitelná	myslitelný	k2eAgFnSc1d1	myslitelná
matematická	matematický	k2eAgFnSc1d1	matematická
struktura	struktura	k1gFnSc1	struktura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
množinu	množina	k1gFnSc4	množina
světů	svět	k1gInPc2	svět
popsatelných	popsatelný	k2eAgFnPc2d1	popsatelná
počítačovými	počítačový	k2eAgInPc7d1	počítačový
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
skončí	skončit	k5eAaPmIp3nP	skončit
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
nebyl	být	k5eNaImAgInS	být
předpověditelný	předpověditelný	k2eAgInSc4d1	předpověditelný
kvůli	kvůli	k7c3	kvůli
Gödelovým	Gödelův	k2eAgFnPc3d1	Gödelova
větám	věta	k1gFnPc3	věta
<g/>
.	.	kIx.	.
<g/>
Teorie	teorie	k1gFnSc1	teorie
mnohovesmíru	mnohovesmíra	k1gFnSc4	mnohovesmíra
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
spekulativní	spekulativní	k2eAgFnPc4d1	spekulativní
až	až	k8xS	až
nevědecké	vědecký	k2eNgFnPc4d1	nevědecká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádný	žádný	k3yNgInSc4	žádný
experimentální	experimentální	k2eAgInSc4d1	experimentální
test	test	k1gInSc4	test
dostupný	dostupný	k2eAgInSc4d1	dostupný
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vesmíru	vesmír	k1gInSc6	vesmír
nemůže	moct	k5eNaImIp3nS	moct
odhalit	odhalit	k5eAaPmF	odhalit
existenci	existence	k1gFnSc4	existence
nebo	nebo	k8xC	nebo
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vesmíru	vesmír	k1gInSc2	vesmír
jiného	jiný	k2eAgInSc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
Occamovy	Occamův	k2eAgFnSc2d1	Occamova
břitvy	břitva	k1gFnSc2	břitva
bychom	by	kYmCp1nP	by
neměli	mít	k5eNaImAgMnP	mít
zavádět	zavádět	k5eAaImF	zavádět
do	do	k7c2	do
popisu	popis	k1gInSc2	popis
entity	entita	k1gFnSc2	entita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemůžeme	moct	k5eNaImIp1nP	moct
empiricky	empiricky	k6eAd1	empiricky
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
podle	podle	k7c2	podle
stejného	stejné	k1gNnSc2	stejné
principu	princip	k1gInSc2	princip
měli	mít	k5eAaImAgMnP	mít
upřednostnit	upřednostnit	k5eAaPmF	upřednostnit
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
matematický	matematický	k2eAgInSc1d1	matematický
popis	popis	k1gInSc1	popis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
více	hodně	k6eAd2	hodně
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
než	než	k8xS	než
zavést	zavést	k5eAaPmF	zavést
dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
axiom	axiom	k1gInSc4	axiom
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
jejich	jejich	k3xOp3gFnSc4	jejich
existenci	existence	k1gFnSc4	existence
popřeme	popřít	k5eAaPmIp1nP	popřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvar	tvar	k1gInSc1	tvar
vesmíru	vesmír	k1gInSc2	vesmír
==	==	k?	==
</s>
</p>
<p>
<s>
Tvarem	tvar	k1gInSc7	tvar
vesmíru	vesmír	k1gInSc2	vesmír
rozumíme	rozumět	k5eAaImIp1nP	rozumět
jeho	jeho	k3xOp3gFnSc4	jeho
geometrii	geometrie	k1gFnSc4	geometrie
a	a	k8xC	a
topologii	topologie	k1gFnSc4	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
vesmíru	vesmír	k1gInSc2	vesmír
přitom	přitom	k6eAd1	přitom
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
především	především	k9	především
jeho	jeho	k3xOp3gFnSc1	jeho
křivost	křivost	k1gFnSc1	křivost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
topologie	topologie	k1gFnSc1	topologie
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
,	,	kIx,	,
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Křivost	křivost	k1gFnSc4	křivost
můžeme	moct	k5eAaImIp1nP	moct
měřit	měřit	k5eAaImF	měřit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
topologii	topologie	k1gFnSc4	topologie
vesmíru	vesmír	k1gInSc2	vesmír
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
empiricky	empiricky	k6eAd1	empiricky
zjistit	zjistit	k5eAaPmF	zjistit
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
velikost	velikost	k1gFnSc1	velikost
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
směru	směr	k1gInSc6	směr
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
velikostí	velikost	k1gFnSc7	velikost
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formálněji	formálně	k6eAd2	formálně
řečeno	říct	k5eAaPmNgNnS	říct
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
topologie	topologie	k1gFnSc2	topologie
vesmíru	vesmír	k1gInSc2	vesmír
zkoumáme	zkoumat	k5eAaImIp1nP	zkoumat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trojrozměrná	trojrozměrný	k2eAgFnSc1d1	trojrozměrná
varieta	varieta	k1gFnSc1	varieta
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
prostorovému	prostorový	k2eAgInSc3d1	prostorový
řezu	řez	k1gInSc3	řez
čtyřrozměrného	čtyřrozměrný	k2eAgInSc2d1	čtyřrozměrný
časoprostoru	časoprostor	k1gInSc2	časoprostor
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
souřadnicích	souřadnice	k1gFnPc6	souřadnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
comoving	comoving	k1gInSc1	comoving
coordinates	coordinates	k1gInSc1	coordinates
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
představuje	představovat	k5eAaImIp3nS	představovat
světelný	světelný	k2eAgInSc4d1	světelný
či	či	k8xC	či
kauzální	kauzální	k2eAgInSc4d1	kauzální
kužel	kužel	k1gInSc4	kužel
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
množinu	množina	k1gFnSc4	množina
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mohlo	moct	k5eAaImAgNnS	moct
světlo	světlo	k1gNnSc1	světlo
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vesmír	vesmír	k1gInSc1	vesmír
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
modelech	model	k1gInPc6	model
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
řádů	řád	k1gInPc2	řád
menší	malý	k2eAgMnSc1d2	menší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
globální	globální	k2eAgFnSc4d1	globální
strukturu	struktura	k1gFnSc4	struktura
vesmíru	vesmír	k1gInSc2	vesmír
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výpočty	výpočet	k1gInPc1	výpočet
z	z	k7c2	z
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
např.	např.	kA	např.
modely	model	k1gInPc1	model
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
FLRW	FLRW	kA	FLRW
metrice	metrika	k1gFnSc6	metrika
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c4	o
topologii	topologie	k1gFnSc4	topologie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
jsou	být	k5eAaImIp3nP	být
lokální	lokální	k2eAgFnPc1d1	lokální
a	a	k8xC	a
nepředepisují	předepisovat	k5eNaImIp3nP	předepisovat
celkový	celkový	k2eAgInSc4d1	celkový
tvar	tvar	k1gInSc4	tvar
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
a	a	k8xC	a
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
populárních	populární	k2eAgInPc6d1	populární
textech	text	k1gInPc6	text
<g/>
)	)	kIx)	)
asi	asi	k9	asi
nejčastěji	často	k6eAd3	často
používaným	používaný	k2eAgInSc7d1	používaný
dodatečným	dodatečný	k2eAgInSc7d1	dodatečný
předpokladem	předpoklad	k1gInSc7	předpoklad
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
jednoduše	jednoduše	k6eAd1	jednoduše
souvislý	souvislý	k2eAgInSc1d1	souvislý
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
s	s	k7c7	s
kladnou	kladný	k2eAgFnSc7d1	kladná
křivostí	křivost	k1gFnSc7	křivost
má	mít	k5eAaImIp3nS	mít
konečný	konečný	k2eAgInSc4d1	konečný
objem	objem	k1gInSc4	objem
a	a	k8xC	a
topologii	topologie	k1gFnSc4	topologie
povrchu	povrch	k1gInSc2	povrch
čtyřrozměrné	čtyřrozměrný	k2eAgFnSc2d1	čtyřrozměrná
koule	koule	k1gFnSc2	koule
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tři-sféry	třiféra	k1gFnSc2	tři-sféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
plochý	plochý	k2eAgInSc1d1	plochý
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
se	s	k7c7	s
zápornou	záporný	k2eAgFnSc7d1	záporná
křivostí	křivost	k1gFnSc7	křivost
jsou	být	k5eAaImIp3nP	být
prostorově	prostorově	k6eAd1	prostorově
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
<g/>
.	.	kIx.	.
</s>
<s>
Obecnější	obecní	k2eAgInPc1d2	obecní
modely	model	k1gInPc1	model
však	však	k9	však
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
například	například	k6eAd1	například
Poincarého	Poincarý	k2eAgInSc2d1	Poincarý
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
sférický	sférický	k2eAgInSc1d1	sférický
dvanáctistěn	dvanáctistěn	k2eAgInSc1d1	dvanáctistěn
<g/>
,	,	kIx,	,
či	či	k8xC	či
model	model	k1gInSc1	model
Picardova	Picardův	k2eAgInSc2d1	Picardův
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
<g/>
Údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
modely	model	k1gInPc1	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
zejména	zejména	k9	zejména
družice	družice	k1gFnSc1	družice
Wilkinson	Wilkinsona	k1gFnPc2	Wilkinsona
Microwave	Microwav	k1gInSc5	Microwav
Anisotropy	Anisotrop	k1gInPc1	Anisotrop
Probe	Prob	k1gInSc5	Prob
(	(	kIx(	(
<g/>
WMAP	WMAP	kA	WMAP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
první	první	k4xOgInPc4	první
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
WMAP	WMAP	kA	WMAP
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc1	činnost
kosmická	kosmický	k2eAgFnSc1d1	kosmická
observatoř	observatoř	k1gFnSc1	observatoř
Planck	Plancka	k1gFnPc2	Plancka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
záření	záření	k1gNnSc3	záření
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
kosmického	kosmický	k2eAgNnSc2d1	kosmické
pozadí	pozadí	k1gNnSc2	pozadí
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
než	než	k8xS	než
měla	mít	k5eAaImAgFnS	mít
WMAP	WMAP	kA	WMAP
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
přinést	přinést	k5eAaPmF	přinést
nová	nový	k2eAgNnPc4d1	nové
data	datum	k1gNnPc4	datum
dat	datum	k1gNnPc2	datum
o	o	k7c6	o
tvaru	tvar	k1gInSc6	tvar
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BARROW	BARROW	kA	BARROW
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
D.	D.	kA	D.
Původ	původ	k1gInSc1	původ
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7115	[number]	k4	7115
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAVIES	DAVIES	kA	DAVIES
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Posledné	Posledný	k2eAgInPc1d1	Posledný
tri	tri	k?	tri
minúty	minút	k1gInPc1	minút
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7115	[number]	k4	7115
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GREENE	GREENE	kA	GREENE
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
<g/>
.	.	kIx.	.
</s>
<s>
Elegantní	elegantní	k2eAgInSc1d1	elegantní
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
882	[number]	k4	882
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GREENE	GREENE	kA	GREENE
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
720	[number]	k4	720
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRYGAR	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
HORSKÝ	Horský	k1gMnSc1	Horský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
MAYER	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GRYGAR	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLECZEK	KLECZEK	kA	KLECZEK
<g/>
,	,	kIx,	,
Josip	Josip	k1gInSc1	Josip
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
věda	věda	k1gFnSc1	věda
-	-	kIx~	-
bioastronomie	bioastronomie	k1gFnSc1	bioastronomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Horáček-Paseka	Horáček-Paseka	k1gMnSc1	Horáček-Paseka
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAWKING	HAWKING	kA	HAWKING
<g/>
,	,	kIx,	,
Stephen	Stephen	k1gInSc4	Stephen
W.	W.	kA	W.
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
169	[number]	k4	169
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HORSKÝ	Horský	k1gMnSc1	Horský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
PLAVEC	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Poznávání	poznávání	k1gNnSc1	poznávání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAKU	KAKU	kA	KAKU
<g/>
,	,	kIx,	,
Michio	Michio	k6eAd1	Michio
<g/>
.	.	kIx.	.
</s>
<s>
Paralelní	paralelní	k2eAgInPc1d1	paralelní
světy	svět	k1gInPc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
847	[number]	k4	847
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVIKOV	NOVIKOV	kA	NOVIKOV
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REES	REES	kA	REES
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Pouhých	pouhý	k2eAgNnPc2d1	pouhé
šest	šest	k4xCc1	šest
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
Skryté	skrytý	k2eAgFnPc4d1	skrytá
síly	síla	k1gFnPc4	síla
utvářející	utvářející	k2eAgFnSc1d1	utvářející
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1152	[number]	k4	1152
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VILENKIN	VILENKIN	kA	VILENKIN
<g/>
,	,	kIx,	,
Alex	Alex	k1gMnSc1	Alex
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
světů	svět	k1gInPc2	svět
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Horáček-Paseka	Horáček-Paseka	k1gMnSc1	Horáček-Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
936	[number]	k4	936
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bod	bod	k1gInSc1	bod
Omega	omega	k1gNnSc2	omega
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
</s>
</p>
<p>
<s>
Kosmický	kosmický	k2eAgInSc1d1	kosmický
prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
Kardašovova	Kardašovův	k2eAgFnSc1d1	Kardašovův
škála	škála	k1gFnSc1	škála
</s>
</p>
<p>
<s>
Kosmologie	kosmologie	k1gFnSc1	kosmologie
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
názor	názor	k1gInSc1	názor
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
průzkumu	průzkum	k1gInSc2	průzkum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vesmír	vesmír	k1gInSc1	vesmír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Vesmír	vesmír	k1gInSc1	vesmír
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vesmír	vesmír	k1gInSc1	vesmír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
online	onlinout	k5eAaPmIp3nS	onlinout
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Atlas	Atlas	k1gInSc1	Atlas
Vesmíru	vesmír	k1gInSc2	vesmír
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Is	Is	k1gMnSc5	Is
there	ther	k1gMnSc5	ther
a	a	k8xC	a
hole	hole	k1gFnSc2	hole
in	in	k?	in
the	the	k?	the
universe	universe	k1gFnSc2	universe
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Universe	Universe	k1gFnSc2	Universe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Frequently	Frequently	k1gFnSc1	Frequently
Asked	Asked	k1gMnSc1	Asked
Questions	Questions	k1gInSc1	Questions
in	in	k?	in
Cosmology	Cosmolog	k1gMnPc4	Cosmolog
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Comparative	Comparativ	k1gInSc5	Comparativ
planetary	planetara	k1gFnPc4	planetara
and	and	k?	and
stellar	stellar	k1gMnSc1	stellar
sizes	sizes	k1gMnSc1	sizes
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Logarithmic	Logarithmic	k1gMnSc1	Logarithmic
Maps	Mapsa	k1gFnPc2	Mapsa
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
So-Called	So-Called	k1gMnSc1	So-Called
Universe	Universe	k1gFnPc1	Universe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Dark	Dark	k1gInSc1	Dark
Side	Side	k1gFnSc1	Side
and	and	k?	and
the	the	k?	the
Bright	Brightum	k1gNnPc2	Brightum
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Exploring	Exploring	k1gInSc1	Exploring
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
</s>
</p>
