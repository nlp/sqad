<s>
Současný	současný	k2eAgInSc1d1	současný
výklad	výklad	k1gInSc1	výklad
astronomických	astronomický	k2eAgNnPc2d1	astronomické
pozorování	pozorování	k1gNnPc2	pozorování
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stáří	stáří	k1gNnSc1	stáří
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
13,799	[number]	k4	13,799
±	±	k?	±
<g/>
0,021	[number]	k4	0,021
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
že	že	k8xS	že
průměr	průměr	k1gInSc4	průměr
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
93	[number]	k4	93
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
čili	čili	k8xC	čili
8,80	[number]	k4	8,80
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
</s>
