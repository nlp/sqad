<s>
Harry	Harr	k1gInPc1	Harr
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
Lamar	Lamara	k1gFnPc2	Lamara
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gNnSc1	Missouri
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
34	[number]	k4	34
<g/>
.	.	kIx.	.
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
a	a	k8xC	a
33	[number]	k4	33
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Franklina	Franklin	k2eAgNnSc2d1	Franklin
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zastával	zastávat	k5eAaImAgMnS	zastávat
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
množství	množství	k1gNnSc3	množství
zásadních	zásadní	k2eAgFnPc2d1	zásadní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nástupu	nástup	k1gInSc6	nástup
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
první	první	k4xOgFnSc1	první
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1947	[number]	k4	1947
začala	začít	k5eAaPmAgFnS	začít
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
Trumanovou	Trumanův	k2eAgFnSc7d1	Trumanova
doktrínou	doktrína	k1gFnSc7	doktrína
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
základní	základní	k2eAgInSc4d1	základní
postoj	postoj	k1gInSc4	postoj
USA	USA	kA	USA
vůči	vůči	k7c3	vůči
rozpínání	rozpínání	k1gNnSc3	rozpínání
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
obnově	obnova	k1gFnSc6	obnova
Evropy	Evropa	k1gFnSc2	Evropa
Marshallovým	Marshallův	k2eAgInSc7d1	Marshallův
plánem	plán	k1gInSc7	plán
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
stahovaly	stahovat	k5eAaImAgFnP	stahovat
americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
prošly	projít	k5eAaPmAgInP	projít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
obdobím	období	k1gNnSc7	období
panického	panický	k2eAgInSc2d1	panický
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
období	období	k1gNnSc2	období
tzv.	tzv.	kA	tzv.
red	red	k?	red
scare	scar	k1gInSc5	scar
(	(	kIx(	(
<g/>
rudá	rudý	k2eAgFnSc1d1	rudá
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
prošel	projít	k5eAaPmAgInS	projít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
F.	F.	kA	F.
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
strázněmi	strázeň	k1gFnPc7	strázeň
běžného	běžný	k2eAgMnSc2d1	běžný
Američana	Američan	k1gMnSc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
kariéru	kariéra	k1gFnSc4	kariéra
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
lidovou	lidový	k2eAgFnSc4d1	lidová
image	image	k1gFnSc4	image
a	a	k8xC	a
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgFnSc4d1	počáteční
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
schopnosti	schopnost	k1gFnSc6	schopnost
(	(	kIx(	(
<g/>
tisk	tisk	k1gInSc1	tisk
ho	on	k3xPp3gMnSc4	on
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
malým	malý	k2eAgMnSc7d1	malý
mužem	muž	k1gMnSc7	muž
z	z	k7c2	z
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
získat	získat	k5eAaPmF	získat
respekt	respekt	k1gInSc4	respekt
a	a	k8xC	a
pověst	pověst	k1gFnSc4	pověst
silného	silný	k2eAgMnSc2d1	silný
a	a	k8xC	a
rozhodného	rozhodný	k2eAgMnSc2d1	rozhodný
státníka	státník	k1gMnSc2	státník
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ať	ať	k9	ať
již	již	k6eAd1	již
na	na	k7c6	na
postu	post	k1gInSc6	post
soudce	soudce	k1gMnSc2	soudce
nebo	nebo	k8xC	nebo
prezidenta	prezident	k1gMnSc2	prezident
typická	typický	k2eAgFnSc1d1	typická
také	také	k9	také
velká	velký	k2eAgFnSc1d1	velká
píle	píle	k1gFnSc1	píle
a	a	k8xC	a
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vůči	vůči	k7c3	vůči
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Známý	známý	k2eAgInSc1d1	známý
nápis	nápis	k1gInSc1	nápis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
stole	stol	k1gInSc6	stol
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pracovně	pracovna	k1gFnSc6	pracovna
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
končí	končit	k5eAaImIp3nS	končit
dolar	dolar	k1gInSc1	dolar
<g/>
"	"	kIx"	"
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
americkému	americký	k2eAgNnSc3d1	americké
rčení	rčení	k1gNnSc3	rčení
"	"	kIx"	"
<g/>
pass	pass	k6eAd1	pass
the	the	k?	the
buck	buck	k1gInSc1	buck
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
předat	předat	k5eAaPmF	předat
dolar	dolar	k1gInSc4	dolar
<g/>
)	)	kIx)	)
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
dalšího	další	k2eAgMnSc4d1	další
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
tedy	tedy	k9	tedy
svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
má	mít	k5eAaImIp3nS	mít
prezident	prezident	k1gMnSc1	prezident
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úřadu	úřad	k1gInSc6	úřad
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
střední	střední	k2eAgNnSc4d1	střední
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
střední	střední	k2eAgFnSc4d1	střední
iniciálu	iniciála	k1gFnSc4	iniciála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
toto	tento	k3xDgNnSc4	tento
byla	být	k5eAaImAgFnS	být
běžná	běžný	k2eAgFnSc1d1	běžná
praxe	praxe	k1gFnSc1	praxe
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
S.	S.	kA	S.
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kompromisem	kompromis	k1gInSc7	kompromis
mezi	mezi	k7c7	mezi
jmény	jméno	k1gNnPc7	jméno
jeho	jeho	k3xOp3gMnPc2	jeho
dědů	děd	k1gMnPc2	děd
<g/>
:	:	kIx,	:
Andersonem	Anderson	k1gMnSc7	Anderson
Shippem	Shipp	k1gMnSc7	Shipp
Trumanem	Truman	k1gMnSc7	Truman
a	a	k8xC	a
Solomonem	Solomon	k1gMnSc7	Solomon
Youngem	Young	k1gInSc7	Young
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
existovala	existovat	k5eAaImAgFnS	existovat
kontroverze	kontroverze	k1gFnSc1	kontroverze
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
užívat	užívat	k5eAaImF	užívat
S	s	k7c7	s
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
či	či	k8xC	či
bez	bez	k7c2	bez
tečky	tečka	k1gFnSc2	tečka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
asi	asi	k9	asi
jako	jako	k9	jako
vtip	vtip	k1gInSc4	vtip
začal	začít	k5eAaPmAgMnS	začít
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
řekl	říct	k5eAaPmAgMnS	říct
jednomu	jeden	k4xCgMnSc3	jeden
reportérovi	reportér	k1gMnSc3	reportér
<g/>
,	,	kIx,	,
že	že	k8xS	že
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
ne	ne	k9	ne
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
podpisu	podpis	k1gInSc6	podpis
používal	používat	k5eAaImAgMnS	používat
variantu	varianta	k1gFnSc4	varianta
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
závaznou	závazný	k2eAgFnSc4d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
v	v	k7c6	v
Lamaru	Lamar	k1gInSc6	Lamar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gNnSc2	Missouri
jako	jako	k9	jako
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dítě	dítě	k1gNnSc4	dítě
Johna	John	k1gMnSc2	John
Andersona	Anderson	k1gMnSc2	Anderson
Trumana	Truman	k1gMnSc2	Truman
a	a	k8xC	a
Marthy	Martha	k1gMnSc2	Martha
Elleny	Ellena	k1gFnSc2	Ellena
Young	Younga	k1gFnPc2	Younga
Trumanové	Trumanová	k1gFnSc2	Trumanová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gInSc3	on
brzy	brzy	k6eAd1	brzy
přibyl	přibýt	k5eAaPmAgMnS	přibýt
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
John	John	k1gMnSc1	John
Vivian	Viviana	k1gFnPc2	Viviana
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
sestra	sestra	k1gFnSc1	sestra
Mary	Mary	k1gFnSc1	Mary
Jane	Jan	k1gMnSc5	Jan
Truman	Truman	k1gMnSc1	Truman
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Independence	Independence	k1gFnSc2	Independence
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
Missouri	Missouri	k1gFnSc6	Missouri
a	a	k8xC	a
zde	zde	k6eAd1	zde
strávil	strávit	k5eAaPmAgInS	strávit
hlavní	hlavní	k2eAgNnPc4d1	hlavní
formativní	formativní	k2eAgNnPc4d1	formativní
léta	léto	k1gNnPc4	léto
svého	svůj	k3xOyFgNnSc2	svůj
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
ukončil	ukončit	k5eAaPmAgInS	ukončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nezískal	získat	k5eNaPmAgMnS	získat
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
)	)	kIx)	)
a	a	k8xC	a
prošel	projít	k5eAaPmAgMnS	projít
několika	několik	k4yIc7	několik
úřednickými	úřednický	k2eAgNnPc7d1	úřednické
zaměstnáními	zaměstnání	k1gNnPc7	zaměstnání
než	než	k8xS	než
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stát	stát	k5eAaPmF	stát
farmářem	farmář	k1gMnSc7	farmář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
tedy	tedy	k8xC	tedy
nepříliš	příliš	k6eNd1	příliš
úspěšně	úspěšně	k6eAd1	úspěšně
farmařil	farmařit	k5eAaImAgMnS	farmařit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
USA	USA	kA	USA
do	do	k7c2	do
války	válka	k1gFnSc2	válka
dobrovolně	dobrovolně	k6eAd1	dobrovolně
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
odvelen	odvelet	k5eAaPmNgMnS	odvelet
jako	jako	k8xS	jako
důstojník	důstojník	k1gMnSc1	důstojník
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
výcviku	výcvik	k1gInSc2	výcvik
ve	v	k7c6	v
Fort	Fort	k?	Fort
Sill	Sill	k1gInSc1	Sill
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
Lawtonu	Lawton	k1gInSc2	Lawton
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnPc6	Oklahomě
vedl	vést	k5eAaImAgMnS	vést
táborovou	táborový	k2eAgFnSc4d1	táborová
kantýnu	kantýna	k1gFnSc4	kantýna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
později	pozdě	k6eAd2	pozdě
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Thomas	Thomas	k1gMnSc1	Thomas
Joseph	Joseph	k1gMnSc1	Joseph
(	(	kIx(	(
<g/>
T.J.	T.J.	k1gMnSc1	T.J.
<g/>
)	)	kIx)	)
Pendergast	Pendergast	k1gMnSc1	Pendergast
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
z	z	k7c2	z
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc4	City
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Independence	Independence	k1gFnSc2	Independence
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Bess	Bess	k1gInSc1	Bess
Wallacovou	Wallacová	k1gFnSc4	Wallacová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgMnS	mít
jednu	jeden	k4xCgFnSc4	jeden
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
Margareth	Margareth	k1gInSc4	Margareth
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
začal	začít	k5eAaPmAgMnS	začít
podnikat	podnikat	k5eAaImF	podnikat
-	-	kIx~	-
otevřel	otevřít	k5eAaPmAgMnS	otevřít
si	se	k3xPyFc3	se
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
oblečením	oblečení	k1gNnSc7	oblečení
Truman	Truman	k1gMnSc1	Truman
&	&	k?	&
Jacobson	Jacobson	k1gMnSc1	Jacobson
(	(	kIx(	(
<g/>
přítel	přítel	k1gMnSc1	přítel
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
ovšem	ovšem	k9	ovšem
zkrachoval	zkrachovat	k5eAaPmAgMnS	zkrachovat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
výkupní	výkupní	k2eAgFnSc2d1	výkupní
ceny	cena	k1gFnSc2	cena
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
důsledku	důsledek	k1gInSc6	důsledek
ztratil	ztratit	k5eAaPmAgMnS	ztratit
většinu	většina	k1gFnSc4	většina
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
také	také	k9	také
pokusil	pokusit	k5eAaPmAgMnS	pokusit
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Missourské	missourský	k2eAgFnSc6d1	missourská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Toma	Tom	k1gMnSc2	Tom
Pendergasta	Pendergast	k1gMnSc2	Pendergast
zvolen	zvolit	k5eAaPmNgMnS	zvolit
soudcem	soudce	k1gMnSc7	soudce
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
okrsku	okrsek	k1gInSc6	okrsek
Jackson	Jackson	k1gMnSc1	Jackson
v	v	k7c6	v
Missouri	Missouri	k1gFnSc6	Missouri
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
administrativní	administrativní	k2eAgFnSc1d1	administrativní
<g/>
,	,	kIx,	,
ne	ne	k9	ne
justiční	justiční	k2eAgInSc1d1	justiční
(	(	kIx(	(
<g/>
tu	tu	k6eAd1	tu
by	by	kYmCp3nS	by
zastávat	zastávat	k5eAaImF	zastávat
nemohl	moct	k5eNaImAgInS	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
vstupní	vstupní	k2eAgInSc1d1	vstupní
poplatek	poplatek	k1gInSc1	poplatek
deset	deset	k4xCc1	deset
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
ke	k	k7c3	k
Ku	k	k7c3	k
Klux	Klux	k1gInSc1	Klux
Klanu	klan	k1gInSc6	klan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krátkodobý	krátkodobý	k2eAgInSc1d1	krátkodobý
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
ryze	ryze	k6eAd1	ryze
formální	formální	k2eAgInSc4d1	formální
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
<g/>
,	,	kIx,	,
co	co	k8xS	co
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
a	a	k8xC	a
co	co	k3yRnSc1	co
jej	on	k3xPp3gMnSc4	on
přimělo	přimět	k5eAaPmAgNnS	přimět
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
Ku	k	k7c3	k
Klux	Klux	k1gInSc4	Klux
Klanu	klan	k1gInSc2	klan
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vstupu	vstup	k1gInSc3	vstup
formálnímu	formální	k2eAgInSc3d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
výsledku	výsledek	k1gInSc6	výsledek
však	však	k9	však
vyšel	vyjít	k5eAaPmAgMnS	vyjít
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
epizody	epizoda	k1gFnSc2	epizoda
jako	jako	k8xC	jako
nepřítel	nepřítel	k1gMnSc1	nepřítel
Ku	k	k7c3	k
Klux	Klux	k1gInSc4	Klux
Klanu	klan	k1gInSc2	klan
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
jej	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
nepřítele	nepřítel	k1gMnSc4	nepřítel
také	také	k6eAd1	také
vnímali	vnímat	k5eAaImAgMnP	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
tato	tento	k3xDgFnSc1	tento
animozita	animozita	k1gFnSc1	animozita
ještě	ještě	k9	ještě
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
angažovanosti	angažovanost	k1gFnSc3	angažovanost
za	za	k7c4	za
rovnoprávné	rovnoprávný	k2eAgNnSc4d1	rovnoprávné
postavení	postavení	k1gNnSc4	postavení
černých	černý	k2eAgMnPc2d1	černý
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
klanem	klan	k1gInSc7	klan
Pendergastů	Pendergasta	k1gMnPc2	Pendergasta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
senátní	senátní	k2eAgNnSc4d1	senátní
křeslo	křeslo	k1gNnSc4	křeslo
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
.	.	kIx.	.
</s>
<s>
Kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
New	New	k1gMnSc4	New
Dealu	Deala	k1gMnSc4	Deala
a	a	k8xC	a
prezidenta	prezident	k1gMnSc4	prezident
Franklina	Franklin	k2eAgInSc2d1	Franklin
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
senátor	senátor	k1gMnSc1	senátor
takřka	takřka	k6eAd1	takřka
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
podporoval	podporovat	k5eAaImAgMnS	podporovat
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
členem	člen	k1gMnSc7	člen
senátního	senátní	k2eAgInSc2d1	senátní
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgInS	být
i	i	k9	i
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
oblékaných	oblékaný	k2eAgMnPc2d1	oblékaný
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgMnSc6d1	populární
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Senate	Senat	k1gInSc5	Senat
Special	Special	k1gInSc1	Special
Committee	Committee	k1gNnSc2	Committee
to	ten	k3xDgNnSc4	ten
Investigate	Investigat	k1gInSc5	Investigat
the	the	k?	the
National	National	k1gFnPc6	National
Defense	defense	k1gFnSc2	defense
Program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
skandál	skandál	k1gInSc4	skandál
odhalením	odhalení	k1gNnSc7	odhalení
plýtvání	plýtvání	k1gNnSc2	plýtvání
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
složkách	složka	k1gFnPc6	složka
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
podvody	podvod	k1gInPc4	podvod
a	a	k8xC	a
špatným	špatný	k2eAgInSc7d1	špatný
managementem	management	k1gInSc7	management
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vedl	vést	k5eAaImAgMnS	vést
tuto	tento	k3xDgFnSc4	tento
komisi	komise	k1gFnSc4	komise
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
problémů	problém	k1gInPc2	problém
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
motorů	motor	k1gInPc2	motor
Wright	Wright	k1gMnSc1	Wright
R-	R-	k1gMnSc1	R-
<g/>
3350	[number]	k4	3350
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
potřeba	potřeba	k6eAd1	potřeba
pro	pro	k7c4	pro
sériovou	sériový	k2eAgFnSc4d1	sériová
výrobu	výroba	k1gFnSc4	výroba
nových	nový	k2eAgInPc2d1	nový
strategických	strategický	k2eAgInPc2d1	strategický
bombardérů	bombardér	k1gInPc2	bombardér
B-	B-	k1gFnSc2	B-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kritika	kritika	k1gFnSc1	kritika
plýtvání	plýtvání	k1gNnSc1	plýtvání
a	a	k8xC	a
obhajoba	obhajoba	k1gFnSc1	obhajoba
návrhů	návrh	k1gInPc2	návrh
na	na	k7c6	na
šetření	šetření	k1gNnSc6	šetření
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělala	udělat	k5eAaPmAgFnS	udělat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
výběru	výběr	k1gInSc3	výběr
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
Rooseveltova	Rooseveltův	k2eAgMnSc2d1	Rooseveltův
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgMnS	být
průběžně	průběžně	k6eAd1	průběžně
informován	informovat	k5eAaBmNgMnS	informovat
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
tří	tři	k4xCgFnPc2	tři
jaderných	jaderný	k2eAgFnPc2d1	jaderná
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
uranové	uranový	k2eAgFnSc2d1	uranová
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
plutoniových	plutoniový	k2eAgInPc2d1	plutoniový
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zpraven	zpravit	k5eAaPmNgInS	zpravit
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
prvního	první	k4xOgInSc2	první
výbuchu	výbuch	k1gInSc2	výbuch
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
plutoniových	plutoniový	k2eAgFnPc2d1	plutoniová
bomb	bomba	k1gFnPc2	bomba
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Následným	následný	k2eAgInSc7d1	následný
vývojem	vývoj	k1gInSc7	vývoj
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
použití	použití	k1gNnSc4	použití
(	(	kIx(	(
<g/>
následně	následně	k6eAd1	následně
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1945	[number]	k4	1945
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
stejné	stejný	k2eAgNnSc1d1	stejné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
musel	muset	k5eAaImAgInS	muset
učinit	učinit	k5eAaImF	učinit
i	i	k9	i
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Povolil	povolit	k5eAaPmAgMnS	povolit
generálu	generál	k1gMnSc3	generál
MacArthurovi	MacArthur	k1gMnSc3	MacArthur
použití	použití	k1gNnSc2	použití
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
za	za	k7c2	za
přesně	přesně	k6eAd1	přesně
definovaných	definovaný	k2eAgFnPc2d1	definovaná
okolností	okolnost	k1gFnPc2	okolnost
(	(	kIx(	(
<g/>
start	start	k1gInSc1	start
nepřátelských	přátelský	k2eNgNnPc2d1	nepřátelské
letadel	letadlo	k1gNnPc2	letadlo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
státník	státník	k1gMnSc1	státník
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
třikrát	třikrát	k6eAd1	třikrát
kladně	kladně	k6eAd1	kladně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
zbraně	zbraň	k1gFnSc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
sférou	sféra	k1gFnSc7	sféra
jeho	on	k3xPp3gInSc2	on
zájmu	zájem	k1gInSc2	zájem
se	se	k3xPyFc4	se
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
musela	muset	k5eAaImAgFnS	muset
stát	stát	k5eAaPmF	stát
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
nebyla	být	k5eNaImAgFnS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
jako	jako	k9	jako
senátor	senátor	k1gMnSc1	senátor
i	i	k8xC	i
jako	jako	k8xC	jako
viceprezident	viceprezident	k1gMnSc1	viceprezident
zabýval	zabývat	k5eAaImAgMnS	zabývat
hlavně	hlavně	k9	hlavně
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
navíc	navíc	k6eAd1	navíc
neměl	mít	k5eNaImAgMnS	mít
potřebu	potřeba	k1gFnSc4	potřeba
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
projednávat	projednávat	k5eAaImF	projednávat
věci	věc	k1gFnPc4	věc
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
musel	muset	k5eAaImAgMnS	muset
projít	projít	k5eAaPmF	projít
množství	množství	k1gNnSc4	množství
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
mnohé	mnohé	k1gNnSc4	mnohé
úplně	úplně	k6eAd1	úplně
nové	nový	k2eAgFnSc2d1	nová
skutečnosti	skutečnost	k1gFnSc2	skutečnost
(	(	kIx(	(
<g/>
jaltská	jaltský	k2eAgFnSc1d1	Jaltská
dohoda	dohoda	k1gFnSc1	dohoda
ohledně	ohledně	k7c2	ohledně
podmínek	podmínka	k1gFnPc2	podmínka
vstupu	vstup	k1gInSc2	vstup
SSSR	SSSR	kA	SSSR
do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vybral	vybrat	k5eAaPmAgMnS	vybrat
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
politiku	politika	k1gFnSc4	politika
ústupků	ústupek	k1gInPc2	ústupek
vůči	vůči	k7c3	vůči
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
např.	např.	kA	např.
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
James	James	k1gMnSc1	James
Byrnes	Byrnes	k1gMnSc1	Byrnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
za	za	k7c2	za
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
cenu	cena	k1gFnSc4	cena
byla	být	k5eAaImAgFnS	být
i	i	k9	i
většina	většina	k1gFnSc1	většina
dobového	dobový	k2eAgInSc2d1	dobový
amerického	americký	k2eAgInSc2d1	americký
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
co	co	k9	co
hůře	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
i	i	k8xC	i
veřejnost	veřejnost	k1gFnSc1	veřejnost
(	(	kIx(	(
<g/>
Gallupův	Gallupův	k2eAgInSc1d1	Gallupův
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
54	[number]	k4	54
%	%	kIx~	%
veřejnosti	veřejnost	k1gFnSc6	veřejnost
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
dobrou	dobrý	k2eAgFnSc4d1	dobrá
spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
období	období	k1gNnSc4	období
jeho	jeho	k3xOp3gInPc2	jeho
začátků	začátek	k1gInPc2	začátek
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
naslouchat	naslouchat	k5eAaImF	naslouchat
odborníkům	odborník	k1gMnPc3	odborník
na	na	k7c4	na
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
politiku	politika	k1gFnSc4	politika
nebo	nebo	k8xC	nebo
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přijetí	přijetí	k1gNnSc4	přijetí
a	a	k8xC	a
obecné	obecný	k2eAgNnSc4d1	obecné
uznání	uznání	k1gNnSc4	uznání
pravdivosti	pravdivost	k1gFnSc2	pravdivost
Kennanova	Kennanův	k2eAgInSc2d1	Kennanův
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
telegramu	telegram	k1gInSc2	telegram
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
telegram	telegram	k1gInSc4	telegram
o	o	k7c6	o
8	[number]	k4	8
tisících	tisící	k4xOgInPc6	tisící
slovech	slovo	k1gNnPc6	slovo
poslaný	poslaný	k2eAgMnSc1d1	poslaný
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
znalostí	znalost	k1gFnPc2	znalost
sovětské	sovětský	k2eAgFnSc2d1	sovětská
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
projev	projev	k1gInSc4	projev
z	z	k7c2	z
února	únor	k1gInSc2	únor
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c4	v
úřadu	úřada	k1gMnSc4	úřada
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
Rooseveltovu	Rooseveltův	k2eAgFnSc4d1	Rooseveltova
zahraničně-politickou	zahraničněolitický	k2eAgFnSc4d1	zahraničně-politická
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednoproudou	jednoproudý	k2eAgFnSc4d1	jednoproudá
silnici	silnice	k1gFnSc4	silnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sovětský	sovětský	k2eAgMnSc1d1	sovětský
partner	partner	k1gMnSc1	partner
na	na	k7c4	na
spojenecké	spojenecký	k2eAgInPc4d1	spojenecký
ústupky	ústupek	k1gInPc4	ústupek
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
stejnou	stejný	k2eAgFnSc7d1	stejná
mincí	mince	k1gFnSc7	mince
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nový	nový	k2eAgMnSc1d1	nový
britský	britský	k2eAgMnSc1d1	britský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Attlee	Attle	k1gFnSc2	Attle
diplomatickým	diplomatický	k2eAgMnSc7d1	diplomatický
nováčkem	nováček	k1gMnSc7	nováček
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Sovětům	Sovět	k1gMnPc3	Sovět
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Trumanův	Trumanův	k2eAgInSc1d1	Trumanův
postoj	postoj	k1gInSc1	postoj
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
změnil	změnit	k5eAaPmAgMnS	změnit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sice	sice	k8xC	sice
nabízel	nabízet	k5eAaImAgInS	nabízet
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
pomoc	pomoc	k1gFnSc4	pomoc
i	i	k8xC	i
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
nevěřil	věřit	k5eNaImAgMnS	věřit
ve	v	k7c4	v
Stalinovo	Stalinův	k2eAgNnSc4d1	Stalinovo
přijetí	přijetí	k1gNnSc4	přijetí
(	(	kIx(	(
<g/>
generál	generál	k1gMnSc1	generál
Marshall	Marshall	k1gMnSc1	Marshall
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Jamese	Jamese	k1gFnPc4	Jamese
Byrnse	Byrnse	k1gFnSc2	Byrnse
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
státního	státní	k2eAgMnSc2d1	státní
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Marshallův	Marshallův	k2eAgInSc1d1	Marshallův
plán	plán	k1gInSc1	plán
měl	mít	k5eAaImAgInS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
a	a	k8xC	a
opravdu	opravdu	k6eAd1	opravdu
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
politicky	politicky	k6eAd1	politicky
silnou	silný	k2eAgFnSc4d1	silná
Evropu	Evropa	k1gFnSc4	Evropa
schopnou	schopný	k2eAgFnSc4d1	schopná
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
šíření	šíření	k1gNnSc3	šíření
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
deklarována	deklarován	k2eAgFnSc1d1	deklarována
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1947	[number]	k4	1947
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
doktrína	doktrína	k1gFnSc1	doktrína
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
USA	USA	kA	USA
musí	muset	k5eAaImIp3nS	muset
podporovat	podporovat	k5eAaImF	podporovat
svobodné	svobodný	k2eAgInPc1d1	svobodný
národy	národ	k1gInPc1	národ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odolávají	odolávat	k5eAaImIp3nP	odolávat
pokusům	pokus	k1gInPc3	pokus
o	o	k7c6	o
podrobení	podrobení	k1gNnSc6	podrobení
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
menšinami	menšina	k1gFnPc7	menšina
nebo	nebo	k8xC	nebo
vnějším	vnější	k2eAgInSc7d1	vnější
nátlakem	nátlak	k1gInSc7	nátlak
a	a	k8xC	a
jestliže	jestliže	k8xS	jestliže
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vůdcovské	vůdcovský	k2eAgFnSc6d1	vůdcovská
roli	role	k1gFnSc6	role
selžeme	selhat	k5eAaPmIp1nP	selhat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
můžeme	moct	k5eAaImIp1nP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
světový	světový	k2eAgInSc4d1	světový
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zatíženy	zatížen	k2eAgMnPc4d1	zatížen
státním	státní	k2eAgInSc7d1	státní
dluhem	dluh	k1gInSc7	dluh
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1050	[number]	k4	1050
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
%	%	kIx~	%
HDP	HDP	kA	HDP
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
snad	snad	k9	snad
jen	jen	k6eAd1	jen
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
po	po	k7c6	po
konci	konec	k1gInSc6	konec
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
necelých	celý	k2eNgNnPc2d1	necelé
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgNnSc2	svůj
prezidentování	prezidentování	k1gNnSc2	prezidentování
se	se	k3xPyFc4	se
Trumanovi	Truman	k1gMnSc3	Truman
podařilo	podařit	k5eAaPmAgNnS	podařit
srazit	srazit	k5eAaPmF	srazit
tuto	tento	k3xDgFnSc4	tento
cifru	cifra	k1gFnSc4	cifra
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
72	[number]	k4	72
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1946	[number]	k4	1946
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
republikánům	republikán	k1gMnPc3	republikán
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
získali	získat	k5eAaPmAgMnP	získat
55	[number]	k4	55
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
několik	několik	k4yIc4	několik
křesel	křeslo	k1gNnPc2	křeslo
i	i	k9	i
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Kongresem	kongres	k1gInSc7	kongres
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zamezit	zamezit	k5eAaPmF	zamezit
snížení	snížení	k1gNnSc4	snížení
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mírnému	mírný	k2eAgNnSc3d1	mírné
snížení	snížení	k1gNnSc3	snížení
i	i	k8xC	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gNnSc4	jeho
veto	veto	k1gNnSc4	veto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
netrvalo	trvat	k5eNaImAgNnS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
začala	začít	k5eAaPmAgFnS	začít
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
naložit	naložit	k5eAaPmF	naložit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
opětnému	opětný	k2eAgNnSc3d1	opětné
zvýšení	zvýšení	k1gNnSc3	zvýšení
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
Moore	Moor	k1gInSc5	Moor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ford	ford	k1gInSc1	ford
Bridge	Bridge	k1gFnSc4	Bridge
ve	v	k7c6	v
Waltonském	Waltonský	k2eAgInSc6d1	Waltonský
okrese	okres	k1gInSc6	okres
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Georgia	Georgium	k1gNnSc2	Georgium
zlynčováni	zlynčovat	k5eAaPmNgMnP	zlynčovat
2	[number]	k4	2
mladí	mladý	k2eAgMnPc1d1	mladý
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
2	[number]	k4	2
černošky	černoška	k1gFnPc4	černoška
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
pozornosti	pozornost	k1gFnSc3	pozornost
v	v	k7c6	v
problematice	problematika	k1gFnSc6	problematika
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
ustanovení	ustanovení	k1gNnSc2	ustanovení
Trumanovy	Trumanův	k2eAgFnSc2d1	Trumanova
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
administrativa	administrativa	k1gFnSc1	administrativa
vydala	vydat	k5eAaPmAgFnS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
secure	secur	k1gMnSc5	secur
these	these	k1gFnPc1	these
rights	rights	k1gInSc1	rights
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
reformu	reforma	k1gFnSc4	reforma
zákonů	zákon	k1gInPc2	zákon
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgFnPc2d1	lidská
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
proti	proti	k7c3	proti
lynčování	lynčování	k1gNnSc3	lynčování
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
federální	federální	k2eAgInSc4d1	federální
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
tlakem	tlak	k1gInSc7	tlak
Kongres	kongres	k1gInSc1	kongres
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
řady	řada	k1gFnSc2	řada
federálních	federální	k2eAgInPc2d1	federální
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
věnovat	věnovat	k5eAaImF	věnovat
např.	např.	kA	např.
odstranění	odstranění	k1gNnSc4	odstranění
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
kladeny	klást	k5eAaImNgFnP	klást
účasti	účast	k1gFnPc1	účast
černochů	černoch	k1gMnPc2	černoch
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
stejným	stejný	k2eAgNnPc3d1	stejné
zaměstnavatelským	zaměstnavatelský	k2eAgNnPc3d1	zaměstnavatelské
právům	právo	k1gNnPc3	právo
nebo	nebo	k8xC	nebo
odstranění	odstranění	k1gNnSc6	odstranění
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
oddělení	oddělení	k1gNnPc2	oddělení
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
četnou	četný	k2eAgFnSc4d1	četná
kritiku	kritika	k1gFnSc4	kritika
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
demokratů	demokrat	k1gMnPc2	demokrat
v	v	k7c6	v
jižanských	jižanský	k2eAgInPc6d1	jižanský
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
uzavírat	uzavírat	k5eAaImF	uzavírat
kompromisy	kompromis	k1gInPc4	kompromis
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1948	[number]	k4	1948
vydal	vydat	k5eAaPmAgMnS	vydat
Executive	Executiv	k1gInSc5	Executiv
Order	Ordra	k1gFnPc2	Ordra
9981	[number]	k4	9981
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
rovnost	rovnost	k1gFnSc4	rovnost
zacházení	zacházení	k1gNnSc2	zacházení
a	a	k8xC	a
šancí	šance	k1gFnPc2	šance
lidem	lid	k1gInSc7	lid
všech	všecek	k3xTgFnPc2	všecek
ras	rasa	k1gFnPc2	rasa
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
složkách	složka	k1gFnPc6	složka
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
provedení	provedení	k1gNnSc6	provedení
založil	založit	k5eAaPmAgInS	založit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
nové	nový	k2eAgFnPc1d1	nová
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
Plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
s	s	k7c7	s
demokraty	demokrat	k1gMnPc7	demokrat
a	a	k8xC	a
tradicí	tradice	k1gFnSc7	tradice
New	New	k1gFnSc2	New
Dealu	Deal	k1gInSc2	Deal
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
Taft-Hartleyho	Taft-Hartley	k1gMnSc2	Taft-Hartley
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Taft-Hartley	Taft-Hartle	k2eAgFnPc1d1	Taft-Hartle
Act	Act	k1gFnPc1	Act
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
vnitropolitického	vnitropolitický	k2eAgInSc2d1	vnitropolitický
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nazýval	nazývat	k5eAaImAgInS	nazývat
Fair	fair	k2eAgInSc4d1	fair
Deal	Deal	k1gInSc4	Deal
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
plnou	plný	k2eAgFnSc4d1	plná
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
minimálních	minimální	k2eAgFnPc2d1	minimální
mezd	mzda	k1gFnPc2	mzda
<g/>
,	,	kIx,	,
daňovou	daňový	k2eAgFnSc7d1	daňová
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
záruky	záruka	k1gFnPc4	záruka
farmářům	farmář	k1gMnPc3	farmář
a	a	k8xC	a
federální	federální	k2eAgInSc1d1	federální
program	program	k1gInSc1	program
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
zchátralých	zchátralý	k2eAgFnPc2d1	zchátralá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
chudiny	chudina	k1gFnSc2	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
aktivita	aktivita	k1gFnSc1	aktivita
během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
byla	být	k5eAaImAgFnS	být
maximální	maximální	k2eAgFnSc1d1	maximální
a	a	k8xC	a
důležité	důležitý	k2eAgNnSc1d1	důležité
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
farmářů	farmář	k1gMnPc2	farmář
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
Středozápadu	středozápad	k1gInSc2	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dokázal	dokázat	k5eAaPmAgInS	dokázat
k	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
překvapení	překvapení	k1gNnSc3	překvapení
porazit	porazit	k5eAaPmF	porazit
hlavního	hlavní	k2eAgMnSc4d1	hlavní
protikandidáta	protikandidát	k1gMnSc4	protikandidát
Thomase	Thomas	k1gMnSc2	Thomas
E.	E.	kA	E.
Deweyho	Dewey	k1gMnSc2	Dewey
<g/>
.	.	kIx.	.
</s>
<s>
Demokraté	demokrat	k1gMnPc1	demokrat
také	také	k9	také
opět	opět	k6eAd1	opět
získali	získat	k5eAaPmAgMnP	získat
převahu	převaha	k1gFnSc4	převaha
i	i	k8xC	i
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
komorách	komora	k1gFnPc6	komora
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
zvolen	zvolit	k5eAaPmNgInS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
ani	ani	k8xC	ani
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tiskem	tisek	k1gMnSc7	tisek
neustále	neustále	k6eAd1	neustále
napadán	napadán	k2eAgMnSc1d1	napadán
kvůli	kvůli	k7c3	kvůli
mrhání	mrhání	k1gNnSc3	mrhání
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
programy	program	k1gInPc4	program
i	i	k8xC	i
pomoc	pomoc	k1gFnSc4	pomoc
cizím	cizí	k2eAgFnPc3d1	cizí
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
představil	představit	k5eAaPmAgMnS	představit
svůj	svůj	k3xOyFgInSc4	svůj
Fair	fair	k2eAgInSc4d1	fair
Deal	Deal	k1gInSc4	Deal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
program	program	k1gInSc1	program
nebyl	být	k5eNaImAgInS	být
dobře	dobře	k6eAd1	dobře
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
prošla	projít	k5eAaPmAgFnS	projít
jenom	jenom	k9	jenom
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
navrhovaných	navrhovaný	k2eAgInPc2d1	navrhovaný
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1950	[number]	k4	1950
republikánský	republikánský	k2eAgMnSc1d1	republikánský
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Wisconsin	Wisconsin	k2eAgMnSc1d1	Wisconsin
Joseph	Joseph	k1gMnSc1	Joseph
McCarthy	McCartha	k1gFnSc2	McCartha
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
komunistických	komunistický	k2eAgInPc6d1	komunistický
agentech	agens	k1gInPc6	agens
ve	v	k7c6	v
státních	státní	k2eAgInPc6d1	státní
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
náhodou	náhodou	k6eAd1	náhodou
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
obvinění	obvinění	k1gNnSc1	obvinění
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
první	první	k4xOgFnSc6	první
zkoušce	zkouška	k1gFnSc6	zkouška
sovětské	sovětský	k2eAgFnSc2d1	sovětská
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
v	v	k7c6	v
září	září	k1gNnSc6	září
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
očekávali	očekávat	k5eAaImAgMnP	očekávat
američtí	americký	k2eAgMnPc1d1	americký
experti	expert	k1gMnPc1	expert
<g/>
.	.	kIx.	.
</s>
<s>
McCarthy	McCarth	k1gInPc4	McCarth
stál	stát	k5eAaImAgInS	stát
i	i	k9	i
za	za	k7c7	za
návrhem	návrh	k1gInSc7	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
registraci	registrace	k1gFnSc3	registrace
všech	všecek	k3xTgMnPc2	všecek
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
bezhlavě	bezhlavě	k6eAd1	bezhlavě
obviňovat	obviňovat	k5eAaImF	obviňovat
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgInPc6d1	vysoký
postech	post	k1gInPc6	post
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yRgFnPc3	který
neměl	mít	k5eNaImAgMnS	mít
vlastně	vlastně	k9	vlastně
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
omezoval	omezovat	k5eAaImAgMnS	omezovat
občanské	občanský	k2eAgFnSc2d1	občanská
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
přijetí	přijetí	k1gNnSc6	přijetí
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgMnS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
atmosféře	atmosféra	k1gFnSc6	atmosféra
paniky	panika	k1gFnSc2	panika
a	a	k8xC	a
strachu	strach	k1gInSc2	strach
postavil	postavit	k5eAaPmAgInS	postavit
za	za	k7c4	za
některé	některý	k3yIgMnPc4	některý
obviněné	obviněný	k1gMnPc4	obviněný
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
McCarthym	McCarthym	k1gInSc4	McCarthym
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
investicí	investice	k1gFnSc7	investice
Kremlu	Kreml	k1gInSc2	Kreml
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zkouškou	zkouška	k1gFnSc7	zkouška
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
pevnosti	pevnost	k1gFnSc2	pevnost
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
první	první	k4xOgFnSc1	první
blokáda	blokáda	k1gFnSc1	blokáda
Berlína	Berlín	k1gInSc2	Berlín
od	od	k7c2	od
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
do	do	k7c2	do
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Truman	Truman	k1gMnSc1	Truman
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
zastával	zastávat	k5eAaImAgMnS	zastávat
pevné	pevný	k2eAgNnSc4d1	pevné
stanovisko	stanovisko	k1gNnSc4	stanovisko
neustoupit	ustoupit	k5eNaPmF	ustoupit
sovětským	sovětský	k2eAgInPc3d1	sovětský
požadavkům	požadavek	k1gInPc3	požadavek
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
francouzsko-britské	francouzskoritský	k2eAgFnPc4d1	francouzsko-britská
snahy	snaha	k1gFnPc4	snaha
i	i	k8xC	i
snahy	snaha	k1gFnPc4	snaha
amerických	americký	k2eAgMnPc2d1	americký
generálů	generál	k1gMnPc2	generál
o	o	k7c4	o
ústupky	ústupek	k1gInPc4	ústupek
vůči	vůči	k7c3	vůči
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nP	kdyby
spojenci	spojenec	k1gMnPc1	spojenec
odešli	odejít	k5eAaPmAgMnP	odejít
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ztratili	ztratit	k5eAaPmAgMnP	ztratit
by	by	kYmCp3nP	by
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
zač	zač	k6eAd1	zač
bojovali	bojovat	k5eAaImAgMnP	bojovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
také	také	k9	také
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
přesunuty	přesunut	k2eAgFnPc4d1	přesunuta
dvě	dva	k4xCgFnPc4	dva
eskadry	eskadra	k1gFnPc4	eskadra
B-	B-	k1gFnSc4	B-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
Hirošimy	Hirošima	k1gFnSc2	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
v	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
povědomí	povědomí	k1gNnSc6	povědomí
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
atomové	atomový	k2eAgInPc1d1	atomový
bombardéry	bombardér	k1gInPc1	bombardér
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bomby	bomba	k1gFnPc1	bomba
ale	ale	k9	ale
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
bombardérů	bombardér	k1gInPc2	bombardér
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
hrozba	hrozba	k1gFnSc1	hrozba
připravenosti	připravenost	k1gFnSc2	připravenost
použít	použít	k5eAaPmF	použít
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1950	[number]	k4	1950
severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
armáda	armáda	k1gFnSc1	armáda
napadla	napadnout	k5eAaPmAgFnS	napadnout
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
reagovala	reagovat	k5eAaBmAgFnS	reagovat
vysláním	vyslání	k1gNnSc7	vyslání
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
s	s	k7c7	s
většinovým	většinový	k2eAgInSc7d1	většinový
podílem	podíl	k1gInSc7	podíl
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedením	vedení	k1gNnSc7	vedení
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
veřejností	veřejnost	k1gFnSc7	veřejnost
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
generál	generál	k1gMnSc1	generál
Douglas	Douglas	k1gMnSc1	Douglas
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
popularita	popularita	k1gFnSc1	popularita
ještě	ještě	k6eAd1	ještě
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
odvážným	odvážný	k2eAgInPc3d1	odvážný
a	a	k8xC	a
vpravdě	vpravdě	k9	vpravdě
geniálním	geniální	k2eAgInSc7d1	geniální
strategickým	strategický	k2eAgInSc7d1	strategický
tahem	tah	k1gInSc7	tah
-	-	kIx~	-
vyloděním	vylodění	k1gNnSc7	vylodění
vojsk	vojsko	k1gNnPc2	vojsko
u	u	k7c2	u
Inčchonu	Inčchon	k1gInSc2	Inčchon
v	v	k7c6	v
září	září	k1gNnSc6	září
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
odřízl	odříznout	k5eAaPmAgInS	odříznout
část	část	k1gFnSc4	část
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
MacArthur	MacArthur	k1gMnSc1	MacArthur
se	se	k3xPyFc4	se
s	s	k7c7	s
Trumanem	Truman	k1gInSc7	Truman
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
ohledně	ohledně	k7c2	ohledně
užití	užití	k1gNnSc2	užití
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc4	spor
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
i	i	k9	i
MacArthurova	MacArthurův	k2eAgFnSc1d1	MacArthurův
snaha	snaha	k1gFnSc1	snaha
po	po	k7c6	po
intervenci	intervence	k1gFnSc6	intervence
čínských	čínský	k2eAgFnPc2d1	čínská
jednotek	jednotka	k1gFnPc2	jednotka
přenést	přenést	k5eAaPmF	přenést
válečné	válečný	k2eAgFnPc4d1	válečná
operace	operace	k1gFnPc4	operace
i	i	k9	i
do	do	k7c2	do
samotné	samotný	k2eAgFnSc2d1	samotná
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
fatálnost	fatálnost	k1gFnSc4	fatálnost
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vojenští	vojenský	k2eAgMnPc1d1	vojenský
velitelé	velitel	k1gMnPc1	velitel
nevystupovali	vystupovat	k5eNaImAgMnP	vystupovat
před	před	k7c7	před
médii	médium	k1gNnPc7	médium
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
příkaz	příkaz	k1gInSc4	příkaz
MacArthur	MacArthur	k1gMnSc1	MacArthur
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1951	[number]	k4	1951
vydal	vydat	k5eAaPmAgMnS	vydat
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
hrozil	hrozit	k5eAaImAgInS	hrozit
Pekingu	Peking	k1gInSc6	Peking
rozšířením	rozšíření	k1gNnSc7	rozšíření
operací	operace	k1gFnSc7	operace
do	do	k7c2	do
čínského	čínský	k2eAgNnSc2d1	čínské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Vyostřená	vyostřený	k2eAgFnSc1d1	vyostřená
roztržka	roztržka	k1gFnSc1	roztržka
skončila	skončit	k5eAaPmAgFnS	skončit
odvoláním	odvolání	k1gNnSc7	odvolání
MacArthura	MacArthur	k1gMnSc2	MacArthur
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
velitele	velitel	k1gMnSc2	velitel
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
nahrazením	nahrazení	k1gNnSc7	nahrazení
generálem	generál	k1gMnSc7	generál
Ridgwayem	Ridgway	k1gMnSc7	Ridgway
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
Trumanově	Trumanův	k2eAgFnSc3d1	Trumanova
popularitě	popularita	k1gFnSc3	popularita
také	také	k9	také
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
reakci	reakce	k1gFnSc3	reakce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
spojenci	spojenec	k1gMnPc1	spojenec
schválili	schválit	k5eAaPmAgMnP	schválit
<g/>
)	)	kIx)	)
a	a	k8xC	a
klesala	klesat	k5eAaImAgFnS	klesat
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
přestala	přestat	k5eAaPmAgFnS	přestat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
podle	podle	k7c2	podle
obecného	obecný	k2eAgNnSc2d1	obecné
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
byl	být	k5eAaImAgMnS	být
stoupencem	stoupenec	k1gMnSc7	stoupenec
sionistického	sionistický	k2eAgNnSc2d1	sionistické
hnutí	hnutí	k1gNnSc2	hnutí
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
izraelského	izraelský	k2eAgInSc2d1	izraelský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
došla	dojít	k5eAaPmAgFnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
OSN	OSN	kA	OSN
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
z	z	k7c2	z
Palestiny	Palestina	k1gFnSc2	Palestina
začaly	začít	k5eAaPmAgFnP	začít
stahovat	stahovat	k5eAaImF	stahovat
britské	britský	k2eAgFnPc1d1	britská
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
arabské	arabský	k2eAgInPc1d1	arabský
státy	stát	k1gInPc1	stát
naopak	naopak	k6eAd1	naopak
začaly	začít	k5eAaPmAgInP	začít
umísťovat	umísťovat	k5eAaImF	umísťovat
své	svůj	k3xOyFgFnPc4	svůj
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
Truman	Truman	k1gMnSc1	Truman
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
úřady	úřad	k1gInPc7	úřad
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Truman	Truman	k1gMnSc1	Truman
uznal	uznat	k5eAaPmAgMnS	uznat
existenci	existence	k1gFnSc4	existence
Izraele	Izrael	k1gInSc2	Izrael
pouhých	pouhý	k2eAgFnPc2d1	pouhá
11	[number]	k4	11
minut	minuta	k1gFnPc2	minuta
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prezidentství	prezidentství	k1gNnSc2	prezidentství
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
Truman	Truman	k1gMnSc1	Truman
v	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
strávil	strávit	k5eAaPmAgMnS	strávit
minimum	minimum	k1gNnSc4	minimum
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bílému	bílý	k2eAgInSc3d1	bílý
domu	dům	k1gInSc3	dům
bezprostředně	bezprostředně	k6eAd1	bezprostředně
hrozí	hrozit	k5eAaImIp3nP	hrozit
zhroucení	zhroucení	k1gNnSc2	zhroucení
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
zdiva	zdivo	k1gNnSc2	zdivo
a	a	k8xC	a
základů	základ	k1gInPc2	základ
(	(	kIx(	(
<g/>
kteréžto	kteréžto	k?	kteréžto
problémy	problém	k1gInPc1	problém
mohly	moct	k5eAaImAgInP	moct
mít	mít	k5eAaImF	mít
základ	základ	k1gInSc4	základ
již	již	k6eAd1	již
z	z	k7c2	z
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
systematicky	systematicky	k6eAd1	systematicky
přestavovat	přestavovat	k5eAaImF	přestavovat
<g/>
,	,	kIx,	,
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
notoricky	notoricky	k6eAd1	notoricky
známá	známý	k2eAgFnSc1d1	známá
část	část	k1gFnSc1	část
z	z	k7c2	z
fotografií	fotografia	k1gFnPc2	fotografia
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Trumanův	Trumanův	k2eAgInSc1d1	Trumanův
balkón	balkón	k1gInSc1	balkón
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
zatím	zatím	k6eAd1	zatím
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Blair	Blair	k1gMnSc1	Blair
House	house	k1gNnSc1	house
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
strávil	strávit	k5eAaPmAgMnS	strávit
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
i	i	k9	i
v	v	k7c4	v
Little	Little	k1gFnPc4	Little
Torch	Torch	k1gMnSc1	Torch
Key	Key	k1gMnSc1	Key
ve	v	k7c6	v
Florida	Florida	k1gFnSc1	Florida
Key	Key	k1gFnPc7	Key
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Kongres	kongres	k1gInSc1	kongres
ratifikoval	ratifikovat	k5eAaBmAgInS	ratifikovat
22	[number]	k4	22
<g/>
.	.	kIx.	.
doplněk	doplněk	k1gInSc1	doplněk
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nedovoloval	dovolovat	k5eNaImAgMnS	dovolovat
prezidentům	prezident	k1gMnPc3	prezident
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novela	novela	k1gFnSc1	novela
se	se	k3xPyFc4	se
nevztahovala	vztahovat	k5eNaImAgFnS	vztahovat
na	na	k7c4	na
Trumana	Truman	k1gMnSc4	Truman
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
schválení	schválení	k1gNnSc2	schválení
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
primárkách	primárky	k1gFnPc6	primárky
v	v	k7c6	v
New	New	k1gFnSc6	New
Hampshiru	Hampshir	k1gInSc2	Hampshir
ztratil	ztratit	k5eAaPmAgInS	ztratit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Estese	Estese	k1gFnSc2	Estese
Kefauvera	Kefauvera	k1gFnSc1	Kefauvera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
Truman	Truman	k1gMnSc1	Truman
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Independence	Independence	k1gFnSc2	Independence
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
psal	psát	k5eAaImAgMnS	psát
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
své	své	k1gNnSc4	své
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednášel	přednášet	k5eAaImAgMnS	přednášet
a	a	k8xC	a
sbíral	sbírat	k5eAaImAgMnS	sbírat
soukromé	soukromý	k2eAgInPc4d1	soukromý
dary	dar	k1gInPc4	dar
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poté	poté	k6eAd1	poté
daroval	darovat	k5eAaPmAgMnS	darovat
vládě	vláda	k1gFnSc6	vláda
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
i	i	k9	i
jeho	jeho	k3xOp3gMnPc4	jeho
následovníky	následovník	k1gMnPc4	následovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
Trumanovou	Trumanův	k2eAgFnSc7d1	Trumanova
zásluhou	zásluha	k1gFnSc7	zásluha
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
nejen	nejen	k6eAd1	nejen
členové	člen	k1gMnPc1	člen
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
federálních	federální	k2eAgInPc2d1	federální
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
členové	člen	k1gMnPc1	člen
ostatních	ostatní	k2eAgInPc2d1	ostatní
federálních	federální	k2eAgInPc2d1	federální
úřadů	úřad	k1gInPc2	úřad
pobírali	pobírat	k5eAaImAgMnP	pobírat
federální	federální	k2eAgInPc4d1	federální
důchody	důchod	k1gInPc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijel	přijet	k5eAaPmAgMnS	přijet
<g/>
,	,	kIx,	,
senzací	senzace	k1gFnSc7	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
získal	získat	k5eAaPmAgInS	získat
čestný	čestný	k2eAgInSc1d1	čestný
titul	titul	k1gInSc1	titul
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Winstonem	Winston	k1gInSc7	Winston
Churchillem	Churchill	k1gInSc7	Churchill
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
podpořil	podpořit	k5eAaPmAgMnS	podpořit
demokratického	demokratický	k2eAgMnSc4d1	demokratický
kandidáta	kandidát	k1gMnSc4	kandidát
Adlaie	Adlaie	k1gFnSc2	Adlaie
Stevensona	Stevensona	k1gFnSc1	Stevensona
během	během	k7c2	během
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
spadl	spadnout	k5eAaPmAgInS	spadnout
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
a	a	k8xC	a
fyzicky	fyzicky	k6eAd1	fyzicky
se	se	k3xPyFc4	se
z	z	k7c2	z
následků	následek	k1gInPc2	následek
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
nevzpamatoval	vzpamatovat	k5eNaPmAgMnS	vzpamatovat
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
pro	pro	k7c4	pro
srdeční	srdeční	k2eAgFnSc4d1	srdeční
slabost	slabost	k1gFnSc4	slabost
<g/>
,	,	kIx,	,
špatnou	špatný	k2eAgFnSc4d1	špatná
funkci	funkce	k1gFnSc4	funkce
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
zažívací	zažívací	k2eAgInPc4d1	zažívací
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
v	v	k7c4	v
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
ráno	ráno	k6eAd1	ráno
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
88	[number]	k4	88
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
u	u	k7c2	u
Trumanovy	Trumanův	k2eAgFnSc2d1	Trumanova
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c4	v
Independence	Independence	k1gFnPc4	Independence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
5	[number]	k4	5
šekelů	šekel	k1gInPc2	šekel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
(	(	kIx(	(
<g/>
CVN-	CVN-	k1gFnSc1	CVN-
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
sloužící	sloužící	k1gFnSc2	sloužící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Trumanovo	Trumanův	k2eAgNnSc1d1	Trumanovo
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Independence	Independence	k1gFnSc1	Independence
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
on-line	onin	k1gInSc5	on-lin
dokumenty	dokument	k1gInPc4	dokument
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
Trumanova	Trumanův	k2eAgInSc2d1	Trumanův
veřejného	veřejný	k2eAgInSc2d1	veřejný
i	i	k8xC	i
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
databázi	databáze	k1gFnSc4	databáze
fotografií	fotografia	k1gFnPc2	fotografia
i	i	k9	i
audio	audio	k2eAgInPc7d1	audio
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
</s>
