<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Internet	Internet	k1gInSc1	Internet
Assigned	Assigned	k1gInSc1	Assigned
Numbers	Numbers	k1gInSc4	Numbers
Authority	Authorita	k1gFnSc2	Authorita
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
jako	jako	k8xS	jako
Autorita	autorita	k1gFnSc1	autorita
pro	pro	k7c4	pro
přidělování	přidělování	k1gNnSc4	přidělování
čísel	číslo	k1gNnPc2	číslo
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
celosvětově	celosvětově	k6eAd1	celosvětově
na	na	k7c6	na
přidělování	přidělování	k1gNnSc6	přidělování
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
kořenových	kořenový	k2eAgFnPc2d1	kořenová
zón	zóna	k1gFnPc2	zóna
DNS	DNS	kA	DNS
<g/>
,	,	kIx,	,
definování	definování	k1gNnSc4	definování
typů	typ	k1gInPc2	typ
medií	medium	k1gNnPc2	medium
pro	pro	k7c4	pro
MIME	mim	k1gMnSc5	mim
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
náležitosti	náležitost	k1gFnPc4	náležitost
internetových	internetový	k2eAgInPc2d1	internetový
protokolů	protokol	k1gInPc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
řídí	řídit	k5eAaImIp3nS	řídit
tuto	tento	k3xDgFnSc4	tento
organizaci	organizace	k1gFnSc4	organizace
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
ICANN	ICANN	kA	ICANN
a	a	k8xC	a
řídil	řídit	k5eAaImAgInS	řídit
ji	on	k3xPp3gFnSc4	on
nejdříve	dříve	k6eAd3	dříve
Jon	Jon	k1gFnSc4	Jon
Postel	postel	k1gFnSc1	postel
z	z	k7c2	z
Information	Information	k1gInSc4	Information
Sciences	Sciences	k1gMnSc1	Sciences
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
ISI	ISI	kA	ISI
–	–	k?	–
výzkumný	výzkumný	k2eAgInSc4d1	výzkumný
ústav	ústav	k1gInSc4	ústav
<g/>
)	)	kIx)	)
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Southern	Southern	k1gNnSc1	Southern
California	Californium	k1gNnSc2	Californium
(	(	kIx(	(
<g/>
USC	USC	kA	USC
–	–	k?	–
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
USC	USC	kA	USC
<g/>
/	/	kIx~	/
<g/>
ISI	ISI	kA	ISI
a	a	k8xC	a
United	United	k1gMnSc1	United
States	Statesa	k1gFnPc2	Statesa
Department	department	k1gInSc4	department
of	of	k?	of
Defense	defense	k1gFnSc2	defense
(	(	kIx(	(
<g/>
Americké	americký	k2eAgNnSc1d1	americké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
správou	správa	k1gFnSc7	správa
organizace	organizace	k1gFnSc2	organizace
pověřen	pověřen	k2eAgInSc4d1	pověřen
United	United	k1gInSc4	United
States	States	k1gInSc1	States
Department	department	k1gInSc1	department
of	of	k?	of
Commerce	Commerka	k1gFnSc6	Commerka
(	(	kIx(	(
<g/>
Americké	americký	k2eAgNnSc1d1	americké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obchodu	obchod	k1gInSc2	obchod
<g/>
)	)	kIx)	)
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
založení	založení	k1gNnSc2	založení
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Závazky	závazek	k1gInPc1	závazek
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
přidělování	přidělování	k1gNnSc4	přidělování
celosvětově	celosvětově	k6eAd1	celosvětově
jedinečného	jedinečný	k2eAgNnSc2d1	jedinečné
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
internetových	internetový	k2eAgInPc2d1	internetový
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňován	k2eAgInPc1d1	zveřejňován
jako	jako	k8xC	jako
RFC	RFC	kA	RFC
dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
této	tento	k3xDgFnSc2	tento
úlohy	úloha	k1gFnSc2	úloha
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Internet	Internet	k1gInSc4	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
IETF	IETF	kA	IETF
<g/>
)	)	kIx)	)
a	a	k8xC	a
Request	Request	k1gFnSc1	Request
for	forum	k1gNnPc2	forum
Comments	Comments	k1gInSc1	Comments
(	(	kIx(	(
<g/>
RFC	RFC	kA	RFC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přidělování	přidělování	k1gNnPc1	přidělování
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
pro	pro	k7c4	pro
přidělování	přidělování	k1gNnSc4	přidělování
a	a	k8xC	a
registraci	registrace	k1gFnSc4	registrace
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
deleguje	delegovat	k5eAaBmIp3nS	delegovat
organizace	organizace	k1gFnSc1	organizace
Regional	Regional	k1gFnSc2	Regional
Internet	Internet	k1gInSc1	Internet
registry	registr	k1gInPc1	registr
(	(	kIx(	(
<g/>
RIR	RIR	kA	RIR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
RIR	RIR	kA	RIR
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Centrálně	centrálně	k6eAd1	centrálně
jsou	být	k5eAaImIp3nP	být
RIR	RIR	kA	RIR
sdruženy	sdružit	k5eAaPmNgInP	sdružit
pod	pod	k7c4	pod
Number	Number	k1gInSc4	Number
Resource	Resourka	k1gFnSc3	Resourka
Organization	Organization	k1gInSc1	Organization
(	(	kIx(	(
<g/>
NRO	NRO	kA	NRO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
organizaci	organizace	k1gFnSc6	organizace
hájí	hájit	k5eAaImIp3nP	hájit
svoje	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
adresy	adresa	k1gFnSc2	adresa
Internet	Internet	k1gInSc1	Internet
Protocol	Protocol	k1gInSc1	Protocol
version	version	k1gInSc1	version
4	[number]	k4	4
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
RIR	RIR	kA	RIR
po	po	k7c6	po
velkých	velký	k2eAgInPc6d1	velký
blocích	blok	k1gInPc6	blok
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
224	[number]	k4	224
adres	adresa	k1gFnPc2	adresa
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
najednou	najednou	k6eAd1	najednou
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
následně	následně	k6eAd1	následně
RIR	RIR	kA	RIR
tyto	tento	k3xDgInPc4	tento
bloky	blok	k1gInPc4	blok
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
a	a	k8xC	a
přidělí	přidělit	k5eAaPmIp3nS	přidělit
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
oblastním	oblastní	k2eAgFnPc3d1	oblastní
Internet	Internet	k1gInSc1	Internet
Service	Service	k1gFnSc1	Service
Provider	Provider	k1gMnSc1	Provider
(	(	kIx(	(
<g/>
ISP	ISP	kA	ISP
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
organizacím	organizace	k1gFnPc3	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
mechanismu	mechanismus	k1gInSc2	mechanismus
přidělování	přidělování	k1gNnSc2	přidělování
adres	adresa	k1gFnPc2	adresa
Internet	Internet	k1gInSc4	Internet
Protocol	Protocol	k1gInSc1	Protocol
version	version	k1gInSc1	version
6	[number]	k4	6
(	(	kIx(	(
<g/>
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nabídka	nabídka	k1gFnSc1	nabídka
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
používat	používat	k5eAaImF	používat
postup	postup	k1gInSc4	postup
jako	jako	k9	jako
u	u	k7c2	u
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvy	název	k1gInPc1	název
domén	doména	k1gFnPc2	doména
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
spravuje	spravovat	k5eAaImIp3nS	spravovat
také	také	k9	také
DNS	DNS	kA	DNS
servery	server	k1gInPc4	server
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
hierarchického	hierarchický	k2eAgInSc2d1	hierarchický
DNS	DNS	kA	DNS
stromu	strom	k1gInSc2	strom
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kořenové	kořenový	k2eAgInPc1d1	kořenový
servery	server	k1gInPc1	server
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úkol	úkol	k1gInSc1	úkol
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zajištění	zajištění	k1gNnSc4	zajištění
komunikace	komunikace	k1gFnSc2	komunikace
se	s	k7c7	s
správci	správce	k1gMnPc7	správce
domén	doména	k1gFnPc2	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
i	i	k8xC	i
kořenové	kořenový	k2eAgFnSc2d1	kořenová
úrovně	úroveň	k1gFnSc2	úroveň
a	a	k8xC	a
vytváření	vytváření	k1gNnSc4	vytváření
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
také	také	k9	také
doménu	doména	k1gFnSc4	doména
.	.	kIx.	.
<g/>
int	int	k?	int
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
.	.	kIx.	.
<g/>
arpa	arpa	k6eAd1	arpa
doménu	doména	k1gFnSc4	doména
pro	pro	k7c4	pro
administrativní	administrativní	k2eAgInPc4d1	administrativní
účely	účel	k1gInPc4	účel
a	a	k8xC	a
pro	pro	k7c4	pro
reverzní	reverzní	k2eAgInPc4d1	reverzní
záznamy	záznam	k1gInPc4	záznam
v	v	k7c6	v
DNS	DNS	kA	DNS
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
kritické	kritický	k2eAgFnPc1d1	kritická
domény	doména	k1gFnPc1	doména
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
root-servers	rootervers	k1gInSc1	root-servers
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
protokolů	protokol	k1gInPc2	protokol
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
upravuje	upravovat	k5eAaImIp3nS	upravovat
také	také	k9	také
parametry	parametr	k1gInPc1	parametr
IETF	IETF	kA	IETF
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Taska	k1gFnPc2	Taska
Force	force	k1gFnPc2	force
<g/>
)	)	kIx)	)
protokolů	protokol	k1gInPc2	protokol
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
URI	URI	kA	URI
(	(	kIx(	(
<g/>
Uniform	Uniform	k1gInSc4	Uniform
Resource	Resourka	k1gFnSc3	Resourka
Identifier	Identifier	k1gInSc1	Identifier
<g/>
)	)	kIx)	)
schémata	schéma	k1gNnPc1	schéma
nebo	nebo	k8xC	nebo
kódování	kódování	k1gNnPc1	kódování
schválená	schválený	k2eAgNnPc1d1	schválené
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
úkoly	úkol	k1gInPc4	úkol
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
Internet	Internet	k1gInSc1	Internet
Architecture	Architectur	k1gMnSc5	Architectur
Board	Board	k1gInSc1	Board
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
RFC	RFC	kA	RFC
<g/>
2860	[number]	k4	2860
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dozor	dozor	k1gInSc4	dozor
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
je	být	k5eAaImIp3nS	být
řízena	řízen	k2eAgFnSc1d1	řízena
ICANN	ICANN	kA	ICANN
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
Corporation	Corporation	k1gInSc1	Corporation
for	forum	k1gNnPc2	forum
Assigned	Assigned	k1gMnSc1	Assigned
Names	Names	k1gMnSc1	Names
and	and	k?	and
Numbers	Numbers	k1gInSc1	Numbers
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obchodu	obchod	k1gInSc2	obchod
USA	USA	kA	USA
(	(	kIx(	(
<g/>
DOC	doc	kA	doc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c4	na
chod	chod	k1gInSc4	chod
organizací	organizace	k1gFnPc2	organizace
dále	daleko	k6eAd2	daleko
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
změny	změna	k1gFnPc1	změna
uskutečněné	uskutečněný	k2eAgFnPc4d1	uskutečněná
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
IANA	Ianus	k1gMnSc2	Ianus
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
jeho	jeho	k3xOp3gFnSc3	jeho
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
DOC	doc	kA	doc
skrze	skrze	k?	skrze
Národní	národní	k2eAgInSc4d1	národní
úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
atmosféru	atmosféra	k1gFnSc4	atmosféra
(	(	kIx(	(
<g/>
NOAA	NOAA	kA	NOAA
<g/>
)	)	kIx)	)
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
prodloužit	prodloužit	k5eAaPmF	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
ICANN	ICANN	kA	ICANN
na	na	k7c4	na
IANA	Ianus	k1gMnSc4	Ianus
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
organizace	organizace	k1gFnSc2	organizace
ICANN	ICANN	kA	ICANN
získá	získat	k5eAaPmIp3nS	získat
z	z	k7c2	z
grantu	grant	k1gInSc2	grant
od	od	k7c2	od
NOAA	NOAA	kA	NOAA
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
DOC	doc	kA	doc
oslovil	oslovit	k5eAaPmAgInS	oslovit
i	i	k9	i
jiné	jiný	k2eAgMnPc4d1	jiný
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
grant	grant	k1gInSc4	grant
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podrobné	podrobný	k2eAgFnPc1d1	podrobná
nabídky	nabídka	k1gFnPc1	nabídka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nP	by
uspokojily	uspokojit	k5eAaPmAgInP	uspokojit
zájmy	zájem	k1gInPc1	zájem
DOC	doc	kA	doc
předložili	předložit	k5eAaPmAgMnP	předložit
do	do	k7c2	do
deseti	deset	k4xCc2	deset
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
DOC	doc	kA	doc
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ICANN	ICANN	kA	ICANN
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
obnovována	obnovován	k2eAgFnSc1d1	obnovována
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
internetových	internetový	k2eAgFnPc2d1	internetová
domén	doména	k1gFnPc2	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
Národní	národní	k2eAgInPc1d1	národní
internetové	internetový	k2eAgInPc1d1	internetový
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
úzce	úzko	k6eAd1	úzko
propojeny	propojit	k5eAaPmNgInP	propojit
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
politicky	politicky	k6eAd1	politicky
motivovaná	motivovaný	k2eAgFnSc1d1	motivovaná
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
mnoho	mnoho	k4c4	mnoho
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
oddělení	oddělení	k1gNnSc4	oddělení
politiky	politika	k1gFnSc2	politika
od	od	k7c2	od
správy	správa	k1gFnSc2	správa
Internetu	Internet	k1gInSc2	Internet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k9	ještě
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
změna	změna	k1gFnSc1	změna
řídících	řídící	k2eAgInPc2d1	řídící
mechanismů	mechanismus	k1gInPc2	mechanismus
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
znamenat	znamenat	k5eAaImF	znamenat
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
IANA	Ianus	k1gMnSc2	Ianus
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
RFC	RFC	kA	RFC
1960	[number]	k4	1960
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
k	k	k7c3	k
vymezení	vymezení	k1gNnSc3	vymezení
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
názvu	název	k1gInSc2	název
došlo	dojít	k5eAaPmAgNnS	dojít
již	již	k9	již
dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jon	Jon	k?	Jon
Postel	postel	k1gInSc1	postel
řídil	řídit	k5eAaImAgMnS	řídit
IANA	Ianus	k1gMnSc4	Ianus
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
vedl	vést	k5eAaImAgMnS	vést
organizaci	organizace	k1gFnSc6	organizace
jeho	jeho	k3xOp3gMnSc1	jeho
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Joyce	Joyce	k1gFnSc2	Joyce
Reynolds	Reynolds	k1gInSc1	Reynolds
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Postel	postel	k1gInSc1	postel
vedl	vést	k5eAaImAgInS	vést
IANA	Ianus	k1gMnSc4	Ianus
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
v	v	k7c4	v
Information	Information	k1gInSc4	Information
Sciences	Sciencesa	k1gFnPc2	Sciencesa
Institute	institut	k1gInSc5	institut
udělal	udělat	k5eAaPmAgInS	udělat
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
mnoho	mnoho	k6eAd1	mnoho
dobrého	dobrý	k2eAgMnSc2d1	dobrý
včetně	včetně	k7c2	včetně
podepsání	podepsání	k1gNnSc2	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
IANA	Ianus	k1gMnSc4	Ianus
byla	být	k5eAaImAgFnS	být
financována	financovat	k5eAaBmNgFnS	financovat
vládou	vláda	k1gFnSc7	vláda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
DARPA	DARPA	kA	DARPA
(	(	kIx(	(
<g/>
Defense	defense	k1gFnSc1	defense
Advanced	Advanced	k1gMnSc1	Advanced
Research	Research	k1gMnSc1	Research
Projects	Projects	k1gInSc4	Projects
Agency	Agenca	k1gFnSc2	Agenca
<g/>
)	)	kIx)	)
a	a	k8xC	a
ISI	ISI	kA	ISI
(	(	kIx(	(
<g/>
Information	Information	k1gInSc1	Information
Sciences	Sciencesa	k1gFnPc2	Sciencesa
Institute	institut	k1gInSc5	institut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
smlouvy	smlouva	k1gFnSc2	smlouva
vypršela	vypršet	k5eAaPmAgFnS	vypršet
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
USC	USC	kA	USC
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ICANN	ICANN	kA	ICANN
a	a	k8xC	a
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
se	se	k3xPyFc4	se
IANA	Ianus	k1gMnSc2	Ianus
stala	stát	k5eAaPmAgFnS	stát
operační	operační	k2eAgFnSc7d1	operační
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2000	[number]	k4	2000
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
DOC	doc	kA	doc
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1999	[number]	k4	1999
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
IETF	IETF	kA	IETF
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ICANN	ICANN	kA	ICANN
o	o	k7c6	o
úkolech	úkol	k1gInPc6	úkol
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
IANA	Ianus	k1gMnSc4	Ianus
vykonávat	vykonávat	k5eAaImF	vykonávat
pro	pro	k7c4	pro
IETF	IETF	kA	IETF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
manažerem	manažer	k1gMnSc7	manažer
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Doug	Doug	k1gMnSc1	Doug
Barton	Barton	k1gInSc1	Barton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
manažerem	manažer	k1gMnSc7	manažer
jmenován	jmenován	k2eAgMnSc1d1	jmenován
David	David	k1gMnSc1	David
Conrad	Conrada	k1gFnPc2	Conrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
website	websit	k1gInSc5	websit
</s>
</p>
<p>
<s>
Numbers	Numbers	k1gInSc4	Numbers
Resource	Resourka	k1gFnSc3	Resourka
Organization	Organization	k1gInSc1	Organization
</s>
</p>
<p>
<s>
COOK	COOK	kA	COOK
report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
Internet	Internet	k1gInSc1	Internet
<g/>
,	,	kIx,	,
June	jun	k1gMnSc5	jun
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
ISI	ISI	kA	ISI
<g/>
/	/	kIx~	/
<g/>
ICANN	ICANN	kA	ICANN
transition	transition	k1gInSc1	transition
agreement	agreement	k1gInSc1	agreement
</s>
</p>
<p>
<s>
IANA	Ianus	k1gMnSc4	Ianus
Functions	Functions	k1gInSc4	Functions
Purchase	Purchasa	k1gFnSc3	Purchasa
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
United	United	k1gInSc1	United
States	States	k1gMnSc1	States
Department	department	k1gInSc1	department
of	of	k?	of
Commerce	Commerka	k1gFnSc3	Commerka
</s>
</p>
<p>
<s>
ICANN	ICANN	kA	ICANN
contract	contract	k1gMnSc1	contract
for	forum	k1gNnPc2	forum
IANA	Ianus	k1gMnSc2	Ianus
<g/>
,	,	kIx,	,
March	March	k1gInSc1	March
2003	[number]	k4	2003
</s>
</p>
