<s>
IANA	Ianus	k1gMnSc4	Ianus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Internet	Internet	k1gInSc1	Internet
Assigned	Assigned	k1gInSc1	Assigned
Numbers	Numbers	k1gInSc4	Numbers
Authority	Authorita	k1gFnSc2	Authorita
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
jako	jako	k8xS	jako
Autorita	autorita	k1gFnSc1	autorita
pro	pro	k7c4	pro
přidělování	přidělování	k1gNnSc4	přidělování
čísel	číslo	k1gNnPc2	číslo
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
celosvětově	celosvětově	k6eAd1	celosvětově
na	na	k7c6	na
přidělování	přidělování	k1gNnSc6	přidělování
IP	IP	kA	IP
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
kořenových	kořenový	k2eAgFnPc2d1	kořenová
zón	zóna	k1gFnPc2	zóna
DNS	DNS	kA	DNS
<g/>
,	,	kIx,	,
definování	definování	k1gNnSc4	definování
typů	typ	k1gInPc2	typ
medií	medium	k1gNnPc2	medium
pro	pro	k7c4	pro
MIME	mim	k1gMnSc5	mim
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
náležitosti	náležitost	k1gFnPc4	náležitost
internetových	internetový	k2eAgInPc2d1	internetový
protokolů	protokol	k1gInPc2	protokol
<g/>
.	.	kIx.	.
</s>
