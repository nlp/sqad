<s>
Technecium	technecium	k1gNnSc1
</s>
<s>
Technecium	technecium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
98	#num#	k4
</s>
<s>
Tc	tc	kA
</s>
<s>
43	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Technecium	technecium	k1gNnSc1
nanesené	nanesený	k2eAgNnSc1d1
na	na	k7c6
zlaté	zlatý	k2eAgFnSc6d1
folii	folie	k1gFnSc6
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Technecium	technecium	k1gNnSc1
<g/>
,	,	kIx,
Tc	tc	k0
<g/>
,	,	kIx,
43	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Technetium	Technetium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Šedý	šedý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-26-8	7440-26-8	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
98	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
136	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
147	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
56	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
−	−	k?
<g/>
I	I	kA
<g/>
,	,	kIx,
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VII	VII	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,9	1,9	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
702	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1470	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2850	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
8,63	8,63	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
11	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
3324K	3324K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
16200	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
50,6	50,6	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
2156,85	2156,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
430	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
4264,85	4264,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
4	#num#	k4
538	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
33,29	33,29	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
585,2	585,2	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
24,27	24,27	k4
Jmol	Jmol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
6,7	6,7	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
0,272	0,272	k4
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
95	#num#	k4
<g/>
Tc	tc	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
61	#num#	k4
dní	den	k1gInPc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
95	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,204	0,204	k4
</s>
<s>
95	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
96	#num#	k4
<g/>
Tc	tc	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
4,3	4,3	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
96	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,778	0,778	k4
</s>
<s>
96	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
97	#num#	k4
<g/>
Tc	tc	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
4,21	4,21	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
97	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
98	#num#	k4
<g/>
Tc	tc	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
4,2	4,2	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
-	-	kIx~
</s>
<s>
98	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,745	0,745	k4
</s>
<s>
98	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
99	#num#	k4
<g/>
Tc	tc	k0
</s>
<s>
stopy	stopa	k1gFnPc1
</s>
<s>
2,111	2,111	k4
<g/>
×	×	k?
<g/>
105	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,294	0,294	k4
</s>
<s>
99	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mn	Mn	k?
<g/>
⋏	⋏	k?
</s>
<s>
Molybden	molybden	k1gInSc1
≺	≺	k?
<g/>
Tc	tc	k0
<g/>
≻	≻	k?
Ruthenium	ruthenium	k1gNnSc4
</s>
<s>
⋎	⋎	k?
<g/>
Re	re	k9
</s>
<s>
Technecium	technecium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Tc	tc	k0
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Technetium	Technetium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejlehčí	lehký	k2eAgInSc1d3
prvek	prvek	k1gInSc1
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Existence	existence	k1gFnSc1
technecia	technecium	k1gNnSc2
byla	být	k5eAaImAgFnS
předpovězena	předpovědit	k5eAaPmNgFnS,k5eAaImNgFnS
již	již	k6eAd1
roku	rok	k1gInSc2
1871	#num#	k4
D.	D.	kA
I.	I.	kA
Mendělejevem	Mendělejev	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
jej	on	k3xPp3gMnSc4
nazval	nazvat	k5eAaBmAgInS,k5eAaPmAgInS
eka-mangan	eka-mangan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečný	skutečný	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
existence	existence	k1gFnSc2
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
však	však	k9
podali	podat	k5eAaPmAgMnP
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1937	#num#	k4
Italové	Ital	k1gMnPc1
Carlo	Carlo	k1gNnSc1
Perrier	Perrier	k1gMnSc1
a	a	k8xC
Emilio	Emilio	k1gMnSc1
G.	G.	kA
Segré	Segrý	k2eAgFnSc6d1
ve	v	k7c6
vzorku	vzorek	k1gInSc6
kovového	kovový	k2eAgInSc2d1
molybdenu	molybden	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
vystaven	vystavit	k5eAaPmNgMnS
bombardování	bombardování	k1gNnSc1
jádry	jádro	k1gNnPc7
deuteria	deuterium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
fyzikálního	fyzikální	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
za	za	k7c2
teplot	teplota	k1gFnPc2
pod	pod	k7c4
7,46	7,46	k4
K	K	kA
jako	jako	k8xC,k8xS
čistý	čistý	k2eAgInSc1d1
prvek	prvek	k1gInSc1
stává	stávat	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
typu	typ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
řadě	řada	k1gFnSc6
mocenství	mocenství	k1gNnPc2
od	od	k7c2
Tc	tc	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
po	po	k7c6
Tc	tc	k0
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
nejstálejší	stálý	k2eAgFnPc1d3
jsou	být	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
Tc	tc	k0
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
například	například	k6eAd1
oxid	oxid	k1gInSc1
technecistý	technecistý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
využití	využití	k1gNnSc1
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
technecium	technecium	k1gNnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
v	v	k7c6
mimořádně	mimořádně	k6eAd1
stopových	stopový	k2eAgNnPc6d1
množstvích	množství	k1gNnPc6
jako	jako	k8xC,k8xS
produkt	produkt	k1gInSc4
radioaktivního	radioaktivní	k2eAgInSc2d1
rozpadu	rozpad	k1gInSc2
uranu	uran	k1gInSc2
235	#num#	k4
<g/>
U.	U.	kA
Přitom	přitom	k6eAd1
z	z	k7c2
1	#num#	k4
g	g	kA
uranu	uran	k1gInSc2
vznikne	vzniknout	k5eAaPmIp3nS
pouze	pouze	k6eAd1
asi	asi	k9
27	#num#	k4
mg	mg	kA
Tc	tc	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
bylo	být	k5eAaImAgNnS
prokázáno	prokázat	k5eAaPmNgNnS
stopové	stopový	k2eAgNnSc1d1
množství	množství	k1gNnSc1
technecia	technecium	k1gNnSc2
v	v	k7c6
emisním	emisní	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
hvězd	hvězda	k1gFnPc2
typu	typ	k1gInSc2
rudých	rudý	k2eAgMnPc2d1
obrů	obr	k1gMnPc2
a	a	k8xC
tento	tento	k3xDgInSc4
důkaz	důkaz	k1gInSc4
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
jeden	jeden	k4xCgInSc1
ze	z	k7c2
zdrojů	zdroj	k1gInPc2
teorie	teorie	k1gFnSc2
o	o	k7c6
přeměně	přeměna	k1gFnSc6
prvků	prvek	k1gInPc2
uvnitř	uvnitř	k7c2
hvězdných	hvězdný	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
36	#num#	k4
radioizotopů	radioizotop	k1gInPc2
technecia	technecium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
nejvýznamnější	významný	k2eAgMnPc4d3
jsou	být	k5eAaImIp3nP
97	#num#	k4
<g/>
Tc	tc	k0
<g/>
,	,	kIx,
98	#num#	k4
<g/>
Tc	tc	k0
a	a	k8xC
99	#num#	k4
<g/>
Tc	tc	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
dva	dva	k4xCgInPc4
jsou	být	k5eAaImIp3nP
beta	beta	k1gNnSc1
zářiče	zářič	k1gInSc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
4,2	4,2	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
a	a	k8xC
2,111	2,111	k4
<g/>
×	×	k?
<g/>
105	#num#	k4
roku	rok	k1gInSc2
<g/>
,	,	kIx,
využívané	využívaný	k2eAgFnSc6d1
v	v	k7c6
biologii	biologie	k1gFnSc6
a	a	k8xC
medicíně	medicína	k1gFnSc6
pro	pro	k7c4
sledování	sledování	k1gNnSc4
metabolismu	metabolismus	k1gInSc2
vybraných	vybraný	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
a	a	k8xC
sledování	sledování	k1gNnSc2
kostní	kostní	k2eAgFnSc2d1
tkáně	tkáň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměle	uměle	k6eAd1
lze	lze	k6eAd1
tyto	tento	k3xDgInPc1
izotopy	izotop	k1gInPc1
poměrně	poměrně	k6eAd1
jednoduše	jednoduše	k6eAd1
připravit	připravit	k5eAaPmF
z	z	k7c2
izotopů	izotop	k1gInPc2
molybdenu	molybden	k1gInSc2
97	#num#	k4
<g/>
Mo	Mo	k1gMnPc2
a	a	k8xC
98	#num#	k4
<g/>
Mo	Mo	k1gFnPc2
jejich	jejich	k3xOp3gNnSc7
bombardováním	bombardování	k1gNnSc7
neutrony	neutron	k1gInPc1
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
izotopů	izotop	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
IzotopPoločas	IzotopPoločas	k1gMnSc1
rozpaduDruh	rozpaduDruh	k1gMnSc1
rozpaduProdukt	rozpaduProdukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
85	#num#	k4
<g/>
Tc	tc	k0
<g/>
0,5	0,5	k4
sp	sp	k?
<g/>
84	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
86	#num#	k4
<g/>
Tc	tc	k0
<g/>
54	#num#	k4
msε	msε	k?
<g/>
86	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
87	#num#	k4
<g/>
Tc	tc	k0
<g/>
2,2	2,2	k4
sε	sε	k?
<g/>
87	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
88	#num#	k4
<g/>
Tc	tc	k0
<g/>
5,8	5,8	k4
sε	sε	k?
<g/>
88	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
89	#num#	k4
<g/>
Tc	tc	k0
<g/>
12,8	12,8	k4
sε	sε	k?
<g/>
89	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
90	#num#	k4
<g/>
Tc	tc	k0
<g/>
8,7	8,7	k4
sε	sε	k?
<g/>
90	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
91	#num#	k4
<g/>
Tc	tc	k0
<g/>
3,14	3,14	k4
minε	minε	k?
<g/>
91	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
92	#num#	k4
<g/>
Tc	tc	k0
<g/>
4,25	4,25	k4
minε	minε	k?
<g/>
92	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
93	#num#	k4
<g/>
Tc	tc	k0
<g/>
2,75	2,75	k4
hε	hε	k?
<g/>
93	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
94	#num#	k4
<g/>
Tc	tc	k0
<g/>
4,883	4,883	k4
hε	hε	k?
<g/>
94	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
95	#num#	k4
<g/>
Tc	tc	k0
<g/>
61	#num#	k4
dε	dε	k?
<g/>
95	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
96	#num#	k4
<g/>
Tc	tc	k0
<g/>
4,28	4,28	k4
dε	dε	k?
<g/>
96	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
97	#num#	k4
<g/>
Tc	tc	k0
<g/>
4,21	4,21	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
rε	rε	k?
<g/>
97	#num#	k4
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
98	#num#	k4
<g/>
Tc	tc	k0
<g/>
4,2	4,2	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
rβ	rβ	k?
<g/>
−	−	k?
<g/>
98	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
99	#num#	k4
<g/>
Tc	tc	k0
<g/>
2,111	2,111	k4
<g/>
×	×	k?
<g/>
105	#num#	k4
rβ	rβ	k?
<g/>
−	−	k?
<g/>
99	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
100	#num#	k4
<g/>
Tc	tc	k0
<g/>
15,46	15,46	k4
sβ	sβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
ε	ε	k?
(	(	kIx(
<g/>
2,6	2,6	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
100	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
Mo	Mo	k1gFnPc2
</s>
<s>
101	#num#	k4
<g/>
Tc	tc	k0
<g/>
14,02	14,02	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
101	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
102	#num#	k4
<g/>
Tc	tc	k0
<g/>
5,28	5,28	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
102	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
103	#num#	k4
<g/>
Tc	tc	k0
<g/>
54,2	54,2	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
103	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
104	#num#	k4
<g/>
Tc	tc	k0
<g/>
18,3	18,3	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
104	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
105	#num#	k4
<g/>
Tc	tc	k0
<g/>
7,6	7,6	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
105	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
106	#num#	k4
<g/>
Tc	tc	k0
<g/>
35,6	35,6	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
106	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
107	#num#	k4
<g/>
Tc	tc	k0
<g/>
21,2	21,2	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
107	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
108	#num#	k4
<g/>
Tc	tc	k0
<g/>
5,17	5,17	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
108	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
109	#num#	k4
<g/>
Tc	tc	k0
<g/>
0,86	0,86	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
109	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
110	#num#	k4
<g/>
Tc	tc	k0
<g/>
0,92	0,92	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
110	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
111	#num#	k4
<g/>
Tc	tc	k0
<g/>
350	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
111	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
112	#num#	k4
<g/>
Tc	tc	k0
<g/>
0,29	0,29	k4
sβ	sβ	k?
<g/>
−	−	k?
<g/>
112	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
113	#num#	k4
<g/>
Tc	tc	k0
<g/>
160	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
113	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
114	#num#	k4
<g/>
Tc	tc	k0
<g/>
100	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
114	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
115	#num#	k4
<g/>
Tc	tc	k0
<g/>
83	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
115	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
116	#num#	k4
<g/>
Tc	tc	k0
<g/>
56	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
116	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
117	#num#	k4
<g/>
Tc	tc	k0
<g/>
85	#num#	k4
msβ	msβ	k?
<g/>
−	−	k?
<g/>
117	#num#	k4
<g/>
Ru	Ru	k1gFnSc1
</s>
<s>
118	#num#	k4
<g/>
Tc	tc	k0
?	?	kIx.
</s>
<s desamb="1">
<g/>
β	β	k?
<g/>
−	−	k?
<g/>
118	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
119	#num#	k4
<g/>
Tc	tc	k0
<g/>
>	>	kIx)
<g/>
392	#num#	k4
nsβ	nsβ	k?
<g/>
−	−	k?
<g/>
119	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
120	#num#	k4
<g/>
Tc	tc	k0
<g/>
>	>	kIx)
<g/>
394	#num#	k4
nsβ	nsβ	k?
<g/>
−	−	k?
<g/>
120	#num#	k4
<g/>
Ru	Ru	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Technecium	technecium	k1gNnSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
chemickou	chemický	k2eAgFnSc7d1
extrakcí	extrakce	k1gFnSc7
z	z	k7c2
radioaktivního	radioaktivní	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
<g/>
,	,	kIx,
vznikajícího	vznikající	k2eAgInSc2d1
při	při	k7c6
práci	práce	k1gFnSc6
výzkumných	výzkumný	k2eAgInPc2d1
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výrobě	výroba	k1gFnSc6
99	#num#	k4
<g/>
mTc	mTc	k?
<g/>
,	,	kIx,
izotopu	izotop	k1gInSc2
technecia	technecium	k1gNnSc2
<g/>
,	,	kIx,
používaného	používaný	k2eAgNnSc2d1
v	v	k7c6
medicíně	medicína	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
radioaktivní	radioaktivní	k2eAgInSc1d1
izotop	izotop	k1gInSc1
molybdenu	molybden	k1gInSc2
99	#num#	k4
<g/>
Mo	Mo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
po	po	k7c6
extrakci	extrakce	k1gFnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
dopravován	dopravovat	k5eAaImNgInS
do	do	k7c2
nemocnic	nemocnice	k1gFnPc2
v	v	k7c6
ocelových	ocelový	k2eAgFnPc6d1
nádobách	nádoba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
jak	jak	k6eAd1
99	#num#	k4
<g/>
Mo	Mo	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
99	#num#	k4
<g/>
mTc	mTc	k?
činí	činit	k5eAaImIp3nS
řádově	řádově	k6eAd1
hodiny	hodina	k1gFnPc4
(	(	kIx(
<g/>
u	u	k7c2
99	#num#	k4
<g/>
mTc	mTc	k?
asi	asi	k9
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc4
proces	proces	k1gInSc4
logisticky	logisticky	k6eAd1
velmi	velmi	k6eAd1
náročný	náročný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
jej	on	k3xPp3gMnSc4
provádí	provádět	k5eAaImIp3nS
jen	jen	k9
několik	několik	k4yIc4
zařízení	zařízení	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
takže	takže	k8xS
výpadek	výpadek	k1gInSc1
některého	některý	k3yIgInSc2
z	z	k7c2
nich	on	k3xPp3gInPc2
ohrožuje	ohrožovat	k5eAaImIp3nS
celý	celý	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
země	zem	k1gFnSc2
proto	proto	k8xC
vyvíjejí	vyvíjet	k5eAaImIp3nP
snahy	snaha	k1gFnPc1
o	o	k7c4
výrobu	výroba	k1gFnSc4
technecia	technecium	k1gNnSc2
jinou	jiný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
kanadští	kanadský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
pokoušejí	pokoušet	k5eAaImIp3nP
získávat	získávat	k5eAaImF
na	na	k7c6
urychlovačích	urychlovač	k1gInPc6
ostřelováním	ostřelování	k1gNnSc7
terče	terč	k1gInSc2
z	z	k7c2
přirozeně	přirozeně	k6eAd1
se	se	k3xPyFc4
vyskytujícího	vyskytující	k2eAgInSc2d1
izotopu	izotop	k1gInSc2
100	#num#	k4
<g/>
Mo	Mo	k1gFnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Schwochau	Schwochaa	k1gFnSc4
<g/>
,	,	kIx,
K.	K.	kA
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technetium	Technetium	k1gNnSc1
<g/>
:	:	kIx,
chemistry	chemistr	k1gMnPc4
and	and	k?
radiopharmaceutical	radiopharmaceuticat	k5eAaPmAgMnS
applications	applications	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley-VCH	Wiley-VCH	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3527294961	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
<g/>
↑	↑	k?
Technecium	technecium	k1gNnSc1
na	na	k7c6
portálu	portál	k1gInSc6
Jefferson	Jefferson	k1gMnSc1
Lab	Lab	k1gMnSc1
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Nukleární	nukleární	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
bez	bez	k7c2
reaktorů	reaktor	k1gInPc2
<g/>
:	:	kIx,
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
zkoušejí	zkoušet	k5eAaImIp3nP
vyrábět	vyrábět	k5eAaImF
technecium	technecium	k1gNnSc4
jinou	jiný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
běžná	běžný	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
technecium	technecium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
technecium	technecium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4184557-2	4184557-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5773	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85133086	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85133086	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
