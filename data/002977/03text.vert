<s>
Křižák	křižák	k1gMnSc1	křižák
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Araneus	Araneus	k1gMnSc1	Araneus
diadematus	diadematus	k1gMnSc1	diadematus
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
křižákovití	křižákovitý	k2eAgMnPc1d1	křižákovitý
je	být	k5eAaImIp3nS	být
nejznámějším	známý	k2eAgInPc3d3	nejznámější
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hojným	hojný	k2eAgMnSc7d1	hojný
pavoukem	pavouk	k1gMnSc7	pavouk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
Evropy	Evropa	k1gFnSc2	Evropa
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
části	část	k1gFnSc6	část
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
zvláště	zvláště	k6eAd1	zvláště
lesy	les	k1gInPc4	les
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
krajinách	krajina	k1gFnPc6	krajina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
odlesněné	odlesněný	k2eAgFnPc1d1	odlesněná
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
křižáků	křižák	k1gInPc2	křižák
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
od	od	k7c2	od
černohnědého	černohnědý	k2eAgInSc2d1	černohnědý
po	po	k7c6	po
tmavohnědé	tmavohnědý	k2eAgFnSc6d1	tmavohnědá
<g/>
,	,	kIx,	,
světlehnědé	světlehnědý	k2eAgFnSc6d1	světlehnědá
<g/>
,	,	kIx,	,
červenohnědé	červenohnědý	k2eAgFnSc6d1	červenohnědá
<g/>
,	,	kIx,	,
oranžovohnědé	oranžovohnědý	k2eAgFnSc6d1	oranžovohnědá
a	a	k8xC	a
zlatohnědé	zlatohnědý	k2eAgFnSc6d1	zlatohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
křížová	křížový	k2eAgFnSc1d1	křížová
kresba	kresba	k1gFnSc1	kresba
na	na	k7c6	na
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
straně	strana	k1gFnSc6	strana
zadečku	zadeček	k1gInSc2	zadeček
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
pět	pět	k4xCc4	pět
bílých	bílý	k2eAgFnPc2d1	bílá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
splývat	splývat	k5eAaImF	splývat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
částečně	částečně	k6eAd1	částečně
redukované	redukovaný	k2eAgInPc1d1	redukovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
zadečku	zadeček	k1gInSc2	zadeček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
hlavohruď	hlavohruď	k1gFnSc1	hlavohruď
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nápadný	nápadný	k2eAgMnSc1d1	nápadný
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
nejširší	široký	k2eAgNnSc1d3	nejširší
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zbarvením	zbarvení	k1gNnSc7	zbarvení
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zadeček	zadeček	k1gInSc1	zadeček
má	mít	k5eAaImIp3nS	mít
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
před	před	k7c7	před
nakladením	nakladení	k1gNnSc7	nakladení
vajec	vejce	k1gNnPc2	vejce
téměř	téměř	k6eAd1	téměř
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
zadeček	zadeček	k1gInSc4	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
samci	samec	k1gMnPc1	samec
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
jen	jen	k9	jen
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
můžou	můžou	k?	můžou
mít	mít	k5eAaImF	mít
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
tkají	tkát	k5eAaImIp3nP	tkát
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
kolové	kolový	k2eAgFnPc4d1	kolová
pavučiny	pavučina	k1gFnPc4	pavučina
velmi	velmi	k6eAd1	velmi
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezi	mezi	k7c7	mezi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
bylinami	bylina	k1gFnPc7	bylina
a	a	k8xC	a
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
zavěšena	zavěšen	k2eAgFnSc1d1	zavěšena
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
velmi	velmi	k6eAd1	velmi
silném	silný	k2eAgNnSc6d1	silné
nosném	nosný	k2eAgNnSc6d1	nosné
vlákně	vlákno	k1gNnSc6	vlákno
a	a	k8xC	a
mívá	mívat	k5eAaImIp3nS	mívat
obvykle	obvykle	k6eAd1	obvykle
kolem	kolem	k7c2	kolem
třiceti	třicet	k4xCc2	třicet
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavění	stavění	k1gNnSc6	stavění
pavučiny	pavučina	k1gFnSc2	pavučina
nejdříve	dříve	k6eAd3	dříve
upřede	upříst	k5eAaPmIp3nS	upříst
dvě	dva	k4xCgNnPc4	dva
vlákna	vlákno	k1gNnPc4	vlákno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Y.	Y.	kA	Y.
Pak	pak	k6eAd1	pak
následují	následovat	k5eAaImIp3nP	následovat
další	další	k2eAgInPc4d1	další
paprsky	paprsek	k1gInPc4	paprsek
a	a	k8xC	a
pak	pak	k6eAd1	pak
udělá	udělat	k5eAaPmIp3nS	udělat
hustou	hustý	k2eAgFnSc4d1	hustá
spirálu	spirála	k1gFnSc4	spirála
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
opatří	opatřit	k5eAaPmIp3nS	opatřit
lepem	lep	k1gInSc7	lep
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
pevně	pevně	k6eAd1	pevně
chytila	chytit	k5eAaPmAgFnS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Opotřebenou	opotřebený	k2eAgFnSc4d1	opotřebená
pavučinu	pavučina	k1gFnSc4	pavučina
sní	snít	k5eAaImIp3nS	snít
a	a	k8xC	a
utká	utkat	k5eAaPmIp3nS	utkat
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Křižák	křižák	k1gMnSc1	křižák
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
dne	den	k1gInSc2	den
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
uprostřed	uprostřed	k7c2	uprostřed
své	svůj	k3xOyFgFnSc2	svůj
sítě	síť	k1gFnSc2	síť
nebo	nebo	k8xC	nebo
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bývá	bývat	k5eAaImIp3nS	bývat
poletující	poletující	k2eAgInSc1d1	poletující
hmyz	hmyz	k1gInSc1	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
,	,	kIx,	,
pavouk	pavouk	k1gMnSc1	pavouk
bleskurychle	bleskurychle	k6eAd1	bleskurychle
vyběhne	vyběhnout	k5eAaPmIp3nS	vyběhnout
a	a	k8xC	a
kořist	kořist	k1gFnSc1	kořist
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
jedem	jed	k1gInSc7	jed
z	z	k7c2	z
klepítek	klepítko	k1gNnPc2	klepítko
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
obalí	obalit	k5eAaPmIp3nP	obalit
pavučinovými	pavučinový	k2eAgNnPc7d1	pavučinové
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc1	kořist
stále	stále	k6eAd1	stále
hýbe	hýbat	k5eAaImIp3nS	hýbat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
kousnuta	kousnut	k2eAgFnSc1d1	kousnuta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
zavěsí	zavěsit	k5eAaPmIp3nS	zavěsit
na	na	k7c4	na
snovací	snovací	k2eAgFnPc4d1	snovací
bradavky	bradavka	k1gFnPc4	bradavka
a	a	k8xC	a
odnese	odnést	k5eAaPmIp3nS	odnést
do	do	k7c2	do
úkrytu	úkryt	k1gInSc2	úkryt
nebo	nebo	k8xC	nebo
do	do	k7c2	do
středu	střed	k1gInSc2	střed
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
sní	snít	k5eAaImIp3nP	snít
<g/>
.	.	kIx.	.
</s>
<s>
Křižák	křižák	k1gMnSc1	křižák
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
dožívá	dožívat	k5eAaImIp3nS	dožívat
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kousnutí	kousnutí	k1gNnSc1	kousnutí
křižáka	křižák	k1gMnSc2	křižák
obecného	obecný	k2eAgMnSc2d1	obecný
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
naprosto	naprosto	k6eAd1	naprosto
neškodné	škodný	k2eNgNnSc1d1	neškodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnPc1	jeho
klepítka	klepítko	k1gNnPc1	klepítko
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprokousnou	prokousnout	k5eNaPmIp3nP	prokousnout
lidskou	lidský	k2eAgFnSc4d1	lidská
kůži	kůže	k1gFnSc4	kůže
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nepřede	příst	k5eNaImIp3nS	příst
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
neloví	lovit	k5eNaImIp3nP	lovit
<g/>
,	,	kIx,	,
veškerý	veškerý	k3xTgInSc1	veškerý
čas	čas	k1gInSc1	čas
tráví	trávit	k5eAaImIp3nS	trávit
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
partnerky	partnerka	k1gFnSc2	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
objevit	objevit	k5eAaPmF	objevit
síť	síť	k1gFnSc4	síť
dospělé	dospělý	k2eAgFnSc2d1	dospělá
samice	samice	k1gFnSc2	samice
<g/>
,	,	kIx,	,
upřede	upříst	k5eAaPmIp3nS	upříst
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
okraji	okraj	k1gInSc3	okraj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
poslouží	posloužit	k5eAaPmIp3nS	posloužit
jako	jako	k9	jako
komunikační	komunikační	k2eAgInSc4d1	komunikační
prostředek	prostředek	k1gInSc4	prostředek
-	-	kIx~	-
brnká	brnkat	k5eAaImIp3nS	brnkat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
na	na	k7c4	na
strunu	struna	k1gFnSc4	struna
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vibrace	vibrace	k1gFnPc4	vibrace
na	na	k7c4	na
partnerčinu	partnerčin	k2eAgFnSc4d1	partnerčina
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
"	"	kIx"	"
<g/>
zručný	zručný	k2eAgMnSc1d1	zručný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
partnerku	partnerka	k1gFnSc4	partnerka
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vláknu	vlákno	k1gNnSc3	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
makadel	makadlo	k1gNnPc2	makadlo
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
ukládá	ukládat	k5eAaImIp3nS	ukládat
sperma	sperma	k1gNnSc1	sperma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pouzdra	pouzdro	k1gNnPc1	pouzdro
strká	strkat	k5eAaImIp3nS	strkat
do	do	k7c2	do
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
otvoru	otvor	k1gInSc2	otvor
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Námluvy	námluva	k1gFnPc1	námluva
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
končí	končit	k5eAaImIp3nS	končit
zabitím	zabití	k1gNnSc7	zabití
a	a	k8xC	a
sežráním	sežrání	k1gNnSc7	sežrání
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nakladla	naklást	k5eAaPmAgFnS	naklást
samice	samice	k1gFnPc4	samice
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Dospívají	dospívat	k5eAaImIp3nP	dospívat
až	až	k9	až
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
hynou	hynout	k5eAaImIp3nP	hynout
po	po	k7c6	po
nakladení	nakladení	k1gNnSc6	nakladení
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
náhodně	náhodně	k6eAd1	náhodně
neobjeví	objevit	k5eNaPmIp3nP	objevit
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
-	-	kIx~	-
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Křižák	křižák	k1gMnSc1	křižák
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Křižák	křižák	k1gMnSc1	křižák
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Araneus	Araneus	k1gMnSc1	Araneus
diadematus	diadematus	k1gMnSc1	diadematus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
