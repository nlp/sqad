<s>
Kousnutí	kousnutí	k1gNnSc1	kousnutí
křižáka	křižák	k1gMnSc2	křižák
obecného	obecný	k2eAgMnSc2d1	obecný
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
naprosto	naprosto	k6eAd1	naprosto
neškodné	škodný	k2eNgNnSc1d1	neškodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnPc1	jeho
klepítka	klepítko	k1gNnPc1	klepítko
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprokousnou	prokousnout	k5eNaPmIp3nP	prokousnout
lidskou	lidský	k2eAgFnSc4d1	lidská
kůži	kůže	k1gFnSc4	kůže
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nepřede	příst	k5eNaImIp3nS	příst
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
neloví	lovit	k5eNaImIp3nP	lovit
<g/>
,	,	kIx,	,
veškerý	veškerý	k3xTgInSc1	veškerý
čas	čas	k1gInSc1	čas
tráví	trávit	k5eAaImIp3nS	trávit
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
partnerky	partnerka	k1gFnSc2	partnerka
<g/>
.	.	kIx.	.
</s>
