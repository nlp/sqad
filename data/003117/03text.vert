<s>
Termínem	termín	k1gInSc7	termín
mrholení	mrholení	k1gNnSc2	mrholení
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
typ	typ	k1gInSc1	typ
deště	dešť	k1gInSc2	dešť
s	s	k7c7	s
kapkami	kapka	k1gFnPc7	kapka
malých	malý	k2eAgMnPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
běžným	běžný	k2eAgInSc7d1	běžný
<g/>
"	"	kIx"	"
deštěm	dešť	k1gInSc7	dešť
a	a	k8xC	a
mrholením	mrholení	k1gNnSc7	mrholení
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
uváděn	uváděn	k2eAgInSc4d1	uváděn
průměr	průměr	k1gInSc4	průměr
kapek	kapka	k1gFnPc2	kapka
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
při	při	k7c6	při
mrholení	mrholení	k1gNnSc1	mrholení
ale	ale	k8xC	ale
kapky	kapka	k1gFnPc1	kapka
často	často	k6eAd1	často
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnPc4	velikost
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
ani	ani	k8xC	ani
nejde	jít	k5eNaImIp3nS	jít
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
deště	dešť	k1gInSc2	dešť
s	s	k7c7	s
padajícími	padající	k2eAgFnPc7d1	padající
velkými	velký	k2eAgFnPc7d1	velká
kapkami	kapka	k1gFnPc7	kapka
-	-	kIx~	-
často	často	k6eAd1	často
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
spíše	spíše	k9	spíše
jako	jako	k9	jako
aerosol	aerosol	k1gInSc1	aerosol
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
než	než	k8xS	než
jako	jako	k9	jako
tělesa	těleso	k1gNnPc4	těleso
padající	padající	k2eAgNnPc4d1	padající
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
vlivem	vlivem	k7c2	vlivem
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mrholení	mrholení	k1gNnSc3	mrholení
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k9	především
u	u	k7c2	u
nízké	nízký	k2eAgFnSc2d1	nízká
oblačnosti	oblačnost	k1gFnSc2	oblačnost
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
v	v	k7c6	v
případě	případ	k1gInSc6	případ
oblaků	oblak	k1gInPc2	oblak
"	"	kIx"	"
<g/>
sedících	sedící	k2eAgInPc2d1	sedící
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
mlhy	mlha	k1gFnSc2	mlha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
podzimní	podzimní	k2eAgInPc4d1	podzimní
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
