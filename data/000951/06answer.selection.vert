<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
původním	původní	k2eAgNnSc7d1	původní
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Joseph	Joseph	k1gMnSc1	Joseph
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1927	[number]	k4	1927
Marktl	Marktl	k1gFnPc2	Marktl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
emeritní	emeritní	k2eAgMnSc1d1	emeritní
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
265	[number]	k4	265
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
suverénem	suverén	k1gMnSc7	suverén
státu	stát	k1gInSc2	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
