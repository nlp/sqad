<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
původním	původní	k2eAgNnSc7d1	původní
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Joseph	Joseph	k1gMnSc1	Joseph
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1927	[number]	k4	1927
Marktl	Marktl	k1gFnPc2	Marktl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
emeritní	emeritní	k2eAgMnSc1d1	emeritní
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
265	[number]	k4	265
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
suverénem	suverén	k1gMnSc7	suverén
státu	stát	k1gInSc2	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
německých	německý	k2eAgFnPc6d1	německá
universitách	universita	k1gFnPc6	universita
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
teologických	teologický	k2eAgMnPc2d1	teologický
konzultantů	konzultant	k1gMnPc2	konzultant
druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
Freisingu	Freising	k1gInSc2	Freising
a	a	k8xC	a
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
děkanem	děkan	k1gMnSc7	děkan
kolegia	kolegium	k1gNnSc2	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
,	,	kIx,	,
prefektem	prefekt	k1gMnSc7	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
inaugurační	inaugurační	k2eAgFnSc4d1	inaugurační
mši	mše	k1gFnSc4	mše
sloužil	sloužit	k5eAaImAgMnS	sloužit
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
německé	německý	k2eAgNnSc1d1	německé
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vatikánské	vatikánský	k2eAgNnSc4d1	Vatikánské
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
prefektem	prefekt	k1gMnSc7	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
přezdívku	přezdívka	k1gFnSc4	přezdívka
Boží	božit	k5eAaImIp3nS	božit
rotvajler	rotvajler	k1gInSc1	rotvajler
<g/>
,	,	kIx,	,
bulvární	bulvární	k2eAgInSc1d1	bulvární
tisk	tisk	k1gInSc1	tisk
jej	on	k3xPp3gInSc4	on
někdy	někdy	k6eAd1	někdy
pejorativně	pejorativně	k6eAd1	pejorativně
tituluje	titulovat	k5eAaImIp3nS	titulovat
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Papa	papa	k1gMnSc1	papa
Ratzi	Rath	k1gMnPc1	Rath
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
teologických	teologický	k2eAgInPc2d1	teologický
názorů	názor	k1gInPc2	názor
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
k	k	k7c3	k
reformnímu	reformní	k2eAgInSc3d1	reformní
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
konzervativce	konzervativec	k1gMnPc4	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
učení	učení	k1gNnSc6	učení
a	a	k8xC	a
pojednáních	pojednání	k1gNnPc6	pojednání
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
obranu	obrana	k1gFnSc4	obrana
tradiční	tradiční	k2eAgFnSc2d1	tradiční
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
doktríny	doktrína	k1gFnSc2	doktrína
a	a	k8xC	a
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
jako	jako	k8xC	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
dekristianizaci	dekristianizace	k1gFnSc4	dekristianizace
a	a	k8xC	a
sekularizaci	sekularizace	k1gFnSc4	sekularizace
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
relativistické	relativistický	k2eAgNnSc1d1	relativistické
odmítání	odmítání	k1gNnSc1	odmítání
objektivní	objektivní	k2eAgFnSc2d1	objektivní
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
zejména	zejména	k9	zejména
odmítání	odmítání	k1gNnSc3	odmítání
objektivních	objektivní	k2eAgFnPc2d1	objektivní
morálních	morální	k2eAgFnPc2d1	morální
pravd	pravda	k1gFnPc2	pravda
za	za	k7c4	za
ústřední	ústřední	k2eAgInSc4d1	ústřední
problém	problém	k1gInSc4	problém
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Alois	Alois	k1gMnSc1	Alois
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c4	na
Bílou	bílý	k2eAgFnSc4d1	bílá
sobotu	sobota	k1gFnSc4	sobota
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1927	[number]	k4	1927
v	v	k7c4	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
domě	dům	k1gInSc6	dům
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c6	na
Schulstraße	Schulstraße	k1gFnSc6	Schulstraße
11	[number]	k4	11
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Marktl	Marktl	k1gMnSc2	Marktl
am	am	k?	am
Inn	Inn	k1gMnSc2	Inn
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
nejmladším	mladý	k2eAgNnSc7d3	nejmladší
dítětem	dítě	k1gNnSc7	dítě
policisty	policista	k1gMnSc2	policista
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Ratzingerové	Ratzingerový	k2eAgFnSc2d1	Ratzingerový
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Peintnerové	Peintnerová	k1gFnSc2	Peintnerová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
pocházela	pocházet	k5eAaImAgFnS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
Bolzana	Bolzan	k1gMnSc2	Bolzan
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papežův	Papežův	k2eAgMnSc1d1	Papežův
bratr	bratr	k1gMnSc1	bratr
Georg	Georg	k1gMnSc1	Georg
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
(	(	kIx(	(
<g/>
*	*	kIx~	*
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
římsko-katolickým	římskoatolický	k2eAgMnSc7d1	římsko-katolický
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
hudebníkem	hudebník	k1gMnSc7	hudebník
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
regenschori	regenschori	k1gMnSc1	regenschori
sboru	sbor	k1gInSc2	sbor
řezenské	řezenský	k2eAgFnSc2d1	řezenská
katedrály	katedrála	k1gFnSc2	katedrála
-	-	kIx~	-
Řezenských	řezenský	k2eAgMnPc2d1	řezenský
vrabců	vrabec	k1gMnPc2	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
sestra	sestra	k1gFnSc1	sestra
Maria	Maria	k1gFnSc1	Maria
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nevdala	vdát	k5eNaPmAgFnS	vdát
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
pečovala	pečovat	k5eAaImAgFnS	pečovat
o	o	k7c4	o
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
kardinálu	kardinál	k1gMnSc3	kardinál
<g/>
,	,	kIx,	,
Ratzingerovi	Ratzinger	k1gMnSc3	Ratzinger
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
prastrýc	prastrýc	k1gMnSc1	prastrýc
Georg	Georg	k1gMnSc1	Georg
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
reformátor	reformátor	k1gMnSc1	reformátor
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Ratzingerovi	Ratzingerův	k2eAgMnPc1d1	Ratzingerův
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
mnichovského	mnichovský	k2eAgMnSc2d1	mnichovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
nesly	nést	k5eAaImAgFnP	nést
květiny	květina	k1gFnPc1	květina
<g/>
.	.	kIx.	.
</s>
<s>
Ratzingera	Ratzingera	k1gFnSc1	Ratzingera
tehdy	tehdy	k6eAd1	tehdy
ohromil	ohromit	k5eAaPmAgInS	ohromit
kardinálův	kardinálův	k2eAgInSc1d1	kardinálův
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
chtěl	chtít	k5eAaImAgInS	chtít
být	být	k5eAaImF	být
také	také	k9	také
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtrnáctých	čtrnáctý	k4xOgFnPc6	čtrnáctý
narozeninách	narozeniny	k1gFnPc6	narozeniny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
členství	členství	k1gNnSc1	členství
povinné	povinný	k2eAgNnSc1d1	povinné
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
německého	německý	k2eAgMnSc4d1	německý
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
byl	být	k5eAaImAgInS	být
však	však	k9	však
neaktivním	aktivní	k2eNgMnSc7d1	neaktivní
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
jejich	jejich	k3xOp3gNnSc4	jejich
setkání	setkání	k1gNnSc4	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Ratzingerův	Ratzingerův	k2eAgMnSc1d1	Ratzingerův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
životopisce	životopisec	k1gMnSc4	životopisec
Johna	John	k1gMnSc2	John
L.	L.	kA	L.
Allena	Allen	k1gMnSc2	Allen
ostrým	ostrý	k2eAgMnSc7d1	ostrý
odpůrcem	odpůrce	k1gMnSc7	odpůrce
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
neslučitelný	slučitelný	k2eNgInSc4d1	neslučitelný
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
vírou	víra	k1gFnSc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Ratzingerových	Ratzingerův	k2eAgMnPc2d1	Ratzingerův
bratranců	bratranec	k1gMnPc2	bratranec
-	-	kIx~	-
čtrnáctiletý	čtrnáctiletý	k2eAgMnSc1d1	čtrnáctiletý
chlapec	chlapec	k1gMnSc1	chlapec
trpící	trpící	k2eAgMnSc1d1	trpící
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
-	-	kIx~	-
zabit	zabít	k5eAaPmNgInS	zabít
nacistickým	nacistický	k2eAgInSc7d1	nacistický
režimem	režim	k1gInSc7	režim
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jeho	jeho	k3xOp3gNnSc2	jeho
eugenického	eugenický	k2eAgNnSc2d1	eugenické
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Ratzingerovi	Ratzingerův	k2eAgMnPc1d1	Ratzingerův
16	[number]	k4	16
let	léto	k1gNnPc2	léto
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
stále	stále	k6eAd1	stále
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
povolán	povolán	k2eAgInSc1d1	povolán
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
protileteckých	protiletecký	k2eAgFnPc2d1	protiletecká
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
výcvik	výcvik	k1gInSc4	výcvik
v	v	k7c6	v
pozemních	pozemní	k2eAgFnPc6d1	pozemní
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
však	však	k9	však
přerušen	přerušen	k2eAgInSc1d1	přerušen
následnou	následný	k2eAgFnSc7d1	následná
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
ochránilo	ochránit	k5eAaPmAgNnS	ochránit
před	před	k7c7	před
obvykle	obvykle	k6eAd1	obvykle
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
povinností	povinnost	k1gFnSc7	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
již	již	k6eAd1	již
přibližovala	přibližovat	k5eAaImAgNnP	přibližovat
spojenecká	spojenecký	k2eAgNnPc1d1	spojenecké
vojska	vojsko	k1gNnPc1	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
stanoviště	stanoviště	k1gNnSc2	stanoviště
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Traunsteinu	Traunstein	k1gInSc6	Traunstein
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc3	jeho
jednotka	jednotka	k1gFnSc1	jednotka
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
převzaly	převzít	k5eAaPmAgFnP	převzít
na	na	k7c4	na
území	území	k1gNnSc4	území
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
udělali	udělat	k5eAaPmAgMnP	udělat
velení	velení	k1gNnSc4	velení
<g/>
.	.	kIx.	.
</s>
<s>
Josepha	Joseph	k1gMnSc4	Joseph
jako	jako	k8xC	jako
vojáka	voják	k1gMnSc4	voják
poslali	poslat	k5eAaPmAgMnP	poslat
do	do	k7c2	do
zajateckého	zajatecký	k2eAgInSc2d1	zajatecký
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1945	[number]	k4	1945
-	-	kIx~	-
propuštěn	propustit	k5eAaPmNgInS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Georgem	Georg	k1gMnSc7	Georg
znovu	znovu	k6eAd1	znovu
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semináře	seminář	k1gInSc2	seminář
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
Traunsteinu	Traunstein	k1gInSc6	Traunstein
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
studovali	studovat	k5eAaImAgMnP	studovat
Herzogliches	Herzogliches	k1gMnSc1	Herzogliches
Georgianum	Georgianum	k1gInSc4	Georgianum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Ludwig-Maximilians-Universität	Ludwig-Maximilians-Universität	k2eAgInSc4d1	Ludwig-Maximilians-Universität
München	München	k1gInSc4	München
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
vysvěceni	vysvěcen	k2eAgMnPc1d1	vysvěcen
kardinálem	kardinál	k1gMnSc7	kardinál
Michaelem	Michael	k1gMnSc7	Michael
von	von	k1gInSc4	von
Faulhaberem	Faulhaber	k1gInSc7	Faulhaber
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1951	[number]	k4	1951
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
složil	složit	k5eAaPmAgMnS	složit
Joseph	Joseph	k1gMnSc1	Joseph
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
disertaci	disertace	k1gFnSc4	disertace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lidstvo	lidstvo	k1gNnSc1	lidstvo
a	a	k8xC	a
dům	dům	k1gInSc1	dům
Boží	boží	k2eAgInSc1d1	boží
v	v	k7c6	v
Augustinově	Augustinův	k2eAgNnSc6d1	Augustinovo
učení	učení	k1gNnSc6	učení
o	o	k7c6	o
Církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
na	na	k7c6	na
Freisinské	Freisinský	k2eAgFnSc6d1	Freisinský
universitě	universita	k1gFnSc6	universita
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
Bonaventurovi	Bonaventur	k1gMnSc6	Bonaventur
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
Ratzigner	Ratzigner	k1gMnSc1	Ratzigner
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
Universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
inaugurační	inaugurační	k2eAgFnSc1d1	inaugurační
přednáška	přednáška	k1gFnSc1	přednáška
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Bůh	bůh	k1gMnSc1	bůh
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
v	v	k7c6	v
Münsteru	Münster	k1gInSc6	Münster
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
účastnil	účastnit	k5eAaImAgInS	účastnit
druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
peritus	peritus	k1gMnSc1	peritus
(	(	kIx(	(
<g/>
teologický	teologický	k2eAgMnSc1d1	teologický
poradce	poradce	k1gMnSc1	poradce
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc2	Josef
kardinála	kardinál	k1gMnSc2	kardinál
Fringse	Frings	k1gMnSc2	Frings
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
konání	konání	k1gNnSc2	konání
koncilu	koncil	k1gInSc2	koncil
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
pohlíženo	pohlížen	k2eAgNnSc1d1	pohlíženo
jako	jako	k9	jako
na	na	k7c4	na
reformátora	reformátor	k1gMnSc4	reformátor
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
radikálními	radikální	k2eAgMnPc7d1	radikální
modernistickými	modernistický	k2eAgMnPc7d1	modernistický
teology	teolog	k1gMnPc7	teolog
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Hans	Hans	k1gMnSc1	Hans
Küng	Küng	k1gMnSc1	Küng
či	či	k8xC	či
Edward	Edward	k1gMnSc1	Edward
Schillebeeckx	Schillebeeckx	k1gInSc1	Schillebeeckx
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
taktéž	taktéž	k?	taktéž
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
Karla	Karel	k1gMnSc4	Karel
Rahnera	Rahner	k1gMnSc4	Rahner
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
známého	známý	k2eAgMnSc4d1	známý
akademického	akademický	k2eAgMnSc4d1	akademický
teologa	teolog	k1gMnSc4	teolog
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Nouvelle	Nouvelle	k1gFnSc2	Nouvelle
Théologie	Théologie	k1gFnSc2	Théologie
a	a	k8xC	a
zastánce	zastánce	k1gMnSc2	zastánce
církevní	církevní	k2eAgFnSc2d1	církevní
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
Josephu	Joseph	k1gInSc2	Joseph
Ratzingerovi	Ratzinger	k1gMnSc3	Ratzinger
přiděleno	přidělen	k2eAgNnSc1d1	přiděleno
křeslo	křeslo	k1gNnSc1	křeslo
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c4	v
Tübingen	Tübingen	k1gInSc4	Tübingen
<g/>
,	,	kIx,	,
coby	coby	k?	coby
profesoru	profesor	k1gMnSc3	profesor
dogmatické	dogmatický	k2eAgFnSc2d1	dogmatická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
kolegou	kolega	k1gMnSc7	kolega
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
Hans	Hans	k1gMnSc1	Hans
Küng	Küng	k1gMnSc1	Küng
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
napsal	napsat	k5eAaBmAgMnS	napsat
svoje	svůj	k3xOyFgNnSc4	svůj
ústřední	ústřední	k2eAgNnSc4d1	ústřední
dílo	dílo	k1gNnSc4	dílo
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
distancoval	distancovat	k5eAaBmAgMnS	distancovat
od	od	k7c2	od
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c4	v
Tübingen	Tübingen	k1gInSc4	Tübingen
a	a	k8xC	a
marxistických	marxistický	k2eAgFnPc2d1	marxistická
tendencí	tendence	k1gFnPc2	tendence
studentských	studentský	k2eAgNnPc2d1	studentské
hnutí	hnutí	k1gNnPc2	hnutí
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
let	léto	k1gNnPc2	léto
1967	[number]	k4	1967
a	a	k8xC	a
1968	[number]	k4	1968
rychle	rychle	k6eAd1	rychle
zradikalizovala	zradikalizovat	k5eAaPmAgFnS	zradikalizovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c6	v
sériích	série	k1gFnPc6	série
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
výtržností	výtržnost	k1gFnPc2	výtržnost
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
vývoj	vývoj	k1gInSc4	vývoj
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
projevy	projev	k1gInPc1	projev
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
upadající	upadající	k2eAgInSc4d1	upadající
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
autoritám	autorita	k1gFnPc3	autorita
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
studenty	student	k1gMnPc7	student
<g/>
)	)	kIx)	)
spojoval	spojovat	k5eAaImAgMnS	spojovat
s	s	k7c7	s
odklonem	odklon	k1gInSc7	odklon
do	do	k7c2	do
tradičního	tradiční	k2eAgNnSc2d1	tradiční
katolického	katolický	k2eAgNnSc2d1	katolické
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgNnSc3	svůj
reformnímu	reformní	k2eAgNnSc3d1	reformní
naklonění	naklonění	k1gNnSc3	naklonění
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
pohled	pohled	k1gInSc4	pohled
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
kontrastu	kontrast	k1gInSc2	kontrast
s	s	k7c7	s
liberálními	liberální	k2eAgFnPc7d1	liberální
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
všeobecně	všeobecně	k6eAd1	všeobecně
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
v	v	k7c6	v
teologických	teologický	k2eAgInPc6d1	teologický
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Ozvaly	ozvat	k5eAaPmAgInP	ozvat
se	se	k3xPyFc4	se
hlasy	hlas	k1gInPc7	hlas
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
Hans	Hans	k1gMnSc1	Hans
Küng	Küng	k1gMnSc1	Küng
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Ratzingerovo	Ratzingerův	k2eAgNnSc4d1	Ratzingerovo
počínání	počínání	k1gNnSc1	počínání
považovaly	považovat	k5eAaImAgInP	považovat
za	za	k7c2	za
obrácení	obrácení	k1gNnSc2	obrácení
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
konzervatismu	konzervatismus	k1gInSc3	konzervatismus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
se	se	k3xPyFc4	se
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
teologických	teologický	k2eAgInPc6d1	teologický
názorech	názor	k1gInPc6	názor
během	během	k7c2	během
let	léto	k1gNnPc2	léto
nevidí	vidět	k5eNaImIp3nS	vidět
žádné	žádný	k3yNgInPc4	žádný
zlomy	zlom	k1gInPc4	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
hájení	hájení	k1gNnSc6	hájení
koncilního	koncilní	k2eAgNnSc2d1	koncilní
učení	učení	k1gNnSc2	učení
proti	proti	k7c3	proti
kritikám	kritika	k1gFnPc3	kritika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dokumentu	dokument	k1gInSc2	dokument
Nostra	Nostr	k1gMnSc2	Nostr
aetate	aetat	k1gInSc5	aetat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
respektu	respekt	k1gInSc6	respekt
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
,	,	kIx,	,
ekumenismu	ekumenismus	k1gInSc6	ekumenismus
a	a	k8xC	a
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
právo	právo	k1gNnSc4	právo
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
jako	jako	k8xC	jako
prefekt	prefekt	k1gMnSc1	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
jasně	jasně	k6eAd1	jasně
objasnil	objasnit	k5eAaPmAgMnS	objasnit
postoj	postoj	k1gInSc4	postoj
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
jiným	jiný	k2eAgNnPc3d1	jiné
náboženstvím	náboženství	k1gNnPc3	náboženství
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
Dominus	Dominus	k1gMnSc1	Dominus
Iesus	Iesus	k1gInSc1	Iesus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
taktéž	taktéž	k?	taktéž
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
zapojení	zapojení	k1gNnSc2	zapojení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
do	do	k7c2	do
ekumenického	ekumenický	k2eAgInSc2d1	ekumenický
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c4	v
Tübingen	Tübingen	k1gInSc4	Tübingen
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
publikoval	publikovat	k5eAaBmAgMnS	publikovat
články	článek	k1gInPc4	článek
v	v	k7c6	v
reformním	reformní	k2eAgInSc6d1	reformní
teologickém	teologický	k2eAgInSc6d1	teologický
časopise	časopis	k1gInSc6	časopis
Concilium	Concilium	k1gNnSc1	Concilium
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vybíral	vybírat	k5eAaImAgMnS	vybírat
méně	málo	k6eAd2	málo
reformní	reformní	k2eAgNnPc1d1	reformní
témata	téma	k1gNnPc1	téma
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
jako	jako	k8xC	jako
Hans	Hans	k1gMnSc1	Hans
Küng	Küng	k1gMnSc1	Küng
či	či	k8xC	či
Edward	Edward	k1gMnSc1	Edward
Schillebeeckx	Schillebeeckx	k1gInSc1	Schillebeeckx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Ursem	Urs	k1gMnSc7	Urs
von	von	k1gInSc4	von
Balthasarem	Balthasar	k1gInSc7	Balthasar
<g/>
,	,	kIx,	,
Henri	Henri	k1gNnSc7	Henri
de	de	k?	de
Lubacem	Lubace	k1gMnSc7	Lubace
<g/>
,	,	kIx,	,
Waltrem	Walter	k1gMnSc7	Walter
Kasprem	Kaspr	k1gMnSc7	Kaspr
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
teologický	teologický	k2eAgInSc4d1	teologický
časopis	časopis	k1gInSc4	časopis
Communio	Communio	k1gMnSc1	Communio
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dodnes	dodnes	k6eAd1	dodnes
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
17	[number]	k4	17
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
čelním	čelní	k2eAgInSc7d1	čelní
časopisem	časopis	k1gInSc7	časopis
moderní	moderní	k2eAgFnSc2d1	moderní
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
papežem	papež	k1gMnSc7	papež
patřil	patřit	k5eAaImAgMnS	patřit
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
k	k	k7c3	k
jedněm	jeden	k4xCgFnPc3	jeden
z	z	k7c2	z
nejplodnějších	plodný	k2eAgMnPc2d3	nejplodnější
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Augsburské	augsburský	k2eAgNnSc1d1	Augsburské
vyznání	vyznání	k1gNnSc1	vyznání
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
uznat	uznat	k5eAaPmF	uznat
jako	jako	k9	jako
katolické	katolický	k2eAgNnSc4d1	katolické
pojetí	pojetí	k1gNnSc4	pojetí
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nestalo	stát	k5eNaPmAgNnS	stát
kvůli	kvůli	k7c3	kvůli
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
<g/>
.	.	kIx.	.
</s>
<s>
Ratzigner	Ratzigner	k1gInSc1	Ratzigner
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
Freisingu	Freising	k1gInSc2	Freising
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
své	svůj	k3xOyFgNnSc4	svůj
biskupské	biskupský	k2eAgNnSc4d1	Biskupské
motto	motto	k1gNnSc4	motto
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
kapitoly	kapitola	k1gFnSc2	kapitola
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Janova	Janův	k2eAgInSc2d1	Janův
listu	list	k1gInSc2	list
-	-	kIx~	-
Cooperatores	Cooperatores	k1gInSc1	Cooperatores
Veritatis	Veritatis	k1gInSc1	Veritatis
(	(	kIx(	(
<g/>
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
Pravdy	pravda	k1gFnSc2	pravda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důvody	důvod	k1gInPc1	důvod
tohoto	tento	k3xDgInSc2	tento
výběru	výběr	k1gInSc2	výběr
více	hodně	k6eAd2	hodně
popisuje	popisovat	k5eAaImIp3nS	popisovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografické	autobiografický	k2eAgFnSc6d1	autobiografická
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konzistoři	konzistoř	k1gFnSc6	konzistoř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
titulu	titul	k1gInSc2	titul
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
Consolatrice	Consolatrika	k1gFnSc3	Consolatrika
al	ala	k1gFnPc2	ala
Tiburtino	Tiburtina	k1gFnSc5	Tiburtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
konkláve	konkláve	k1gNnSc2	konkláve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
14	[number]	k4	14
žijících	žijící	k2eAgMnPc2d1	žijící
kardinálů	kardinál	k1gMnPc2	kardinál
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
Pavlem	Pavel	k1gMnSc7	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pak	pak	k6eAd1	pak
kromě	kromě	k7c2	kromě
něj	on	k3xPp3gInSc2	on
volil	volit	k5eAaImAgInS	volit
ještě	ještě	k6eAd1	ještě
William	William	k1gInSc1	William
Wakefield	Wakefielda	k1gFnPc2	Wakefielda
Baum	Bauma	k1gFnPc2	Bauma
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Ratzingera	Ratzingero	k1gNnPc4	Ratzingero
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1981	[number]	k4	1981
prefektem	prefekt	k1gMnSc7	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Svatého	svatý	k2eAgNnSc2d1	svaté
Oficia	oficium	k1gNnSc2	oficium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
navazující	navazující	k2eAgInPc1d1	navazující
na	na	k7c4	na
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
jmenování	jmenování	k1gNnSc4	jmenování
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
vzdal	vzdát	k5eAaPmAgMnS	vzdát
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
kolegiem	kolegium	k1gNnSc7	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
kardinálem-biskupem	kardinálemiskup	k1gInSc7	kardinálem-biskup
Velletri-Segni	Velletri-Segen	k2eAgMnPc1d1	Velletri-Segen
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
viceděkanem	viceděkan	k1gMnSc7	viceděkan
kolegia	kolegium	k1gNnPc4	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
jeho	jeho	k3xOp3gMnSc7	jeho
děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
taktéž	taktéž	k?	taktéž
kardinálem-biskupem	kardinálemiskup	k1gInSc7	kardinálem-biskup
ostijské	ostijský	k2eAgFnSc2d1	ostijský
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Pavla	Pavel	k1gMnSc2	Pavel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
-	-	kIx~	-
<g/>
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
kardinálem-biskupem	kardinálemiskup	k1gInSc7	kardinálem-biskup
od	od	k7c2	od
Pia	Pius	k1gMnSc2	Pius
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vykonávání	vykonávání	k1gNnSc4	vykonávání
úřadu	úřad	k1gInSc2	úřad
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
zastával	zastávat	k5eAaImAgMnS	zastávat
katolickou	katolický	k2eAgFnSc4d1	katolická
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
tématech	téma	k1gNnPc6	téma
kontroly	kontrola	k1gFnSc2	kontrola
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
homosexuality	homosexualita	k1gFnSc2	homosexualita
a	a	k8xC	a
mezináboženského	mezináboženský	k2eAgInSc2d1	mezináboženský
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Ratzingerova	Ratzingerův	k2eAgNnSc2d1	Ratzingerovo
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
suspendován	suspendován	k2eAgMnSc1d1	suspendován
Leonardo	Leonardo	k1gMnSc1	Leonardo
Boff	Boff	k1gMnSc1	Boff
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k1gNnPc2	další
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
či	či	k8xC	či
napomenuto	napomenut	k2eAgNnSc1d1	napomenuto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
záležitosti	záležitost	k1gFnPc1	záležitost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
týkaly	týkat	k5eAaImAgInP	týkat
např.	např.	kA	např.
odebrání	odebrání	k1gNnSc4	odebrání
práva	právo	k1gNnSc2	právo
vyučovat	vyučovat	k5eAaImF	vyučovat
či	či	k8xC	či
odsouzení	odsouzení	k1gNnSc2	odsouzení
některých	některý	k3yIgNnPc2	některý
děl	dělo	k1gNnPc2	dělo
<g/>
:	:	kIx,	:
např.	např.	kA	např.
bylo	být	k5eAaImAgNnS	být
upozorňováno	upozorňovat	k5eAaImNgNnS	upozorňovat
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
spisy	spis	k1gInPc4	spis
Anthony	Anthona	k1gFnSc2	Anthona
de	de	k?	de
Mella	Mella	k1gMnSc1	Mella
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Ratzingera	Ratzingero	k1gNnSc2	Ratzingero
a	a	k8xC	a
Kongregace	kongregace	k1gFnSc2	kongregace
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pozdější	pozdní	k2eAgFnPc4d2	pozdější
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
prvky	prvek	k1gInPc4	prvek
náboženského	náboženský	k2eAgInSc2d1	náboženský
indiferentismu	indiferentismus	k1gInSc2	indiferentismus
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
např.	např.	kA	např.
Kristus	Kristus	k1gMnSc1	Kristus
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgMnSc1	jeden
mistr	mistr	k1gMnSc1	mistr
mezi	mezi	k7c7	mezi
dalšími	další	k2eAgFnPc7d1	další
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
funkcí	funkce	k1gFnSc7	funkce
Kongregace	kongregace	k1gFnSc2	kongregace
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
dohled	dohled	k1gInSc1	dohled
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
církevní	církevní	k2eAgFnSc4d1	církevní
věrouku	věrouka	k1gFnSc4	věrouka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Kongregace	kongregace	k1gFnSc1	kongregace
má	mít	k5eAaImIp3nS	mít
právní	právní	k2eAgFnSc1d1	právní
moc	moc	k1gFnSc1	moc
nad	nad	k7c7	nad
dalšími	další	k2eAgFnPc7d1	další
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
svěřeno	svěřen	k2eAgNnSc1d1	svěřeno
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
některých	některý	k3yIgNnPc2	některý
závažných	závažný	k2eAgNnPc2d1	závažné
provinění	provinění	k1gNnPc2	provinění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
aktuálním	aktuální	k2eAgFnPc3d1	aktuální
např.	např.	kA	např.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sexuálních	sexuální	k2eAgInPc2d1	sexuální
skandálů	skandál	k1gInPc2	skandál
kněží	kněz	k1gMnPc2	kněz
v	v	k7c4	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
kteréžto	kteréžto	k?	kteréžto
době	době	k6eAd1	době
byl	být	k5eAaImAgInS	být
prefektem	prefekt	k1gMnSc7	prefekt
právě	právě	k6eAd1	právě
Ratzinger	Ratzingra	k1gFnPc2	Ratzingra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Ratzingerovi	Ratzinger	k1gMnSc3	Ratzinger
ozvaly	ozvat	k5eAaPmAgInP	ozvat
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
obviňující	obviňující	k2eAgFnSc4d1	obviňující
Kongregaci	kongregace	k1gFnSc4	kongregace
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
záležitost	záležitost	k1gFnSc4	záležitost
tutlat	tutlat	k5eAaImF	tutlat
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
těmto	tento	k3xDgFnPc3	tento
vyjádřením	vyjádření	k1gNnSc7	vyjádření
jasně	jasně	k6eAd1	jasně
ohradila	ohradit	k5eAaPmAgFnS	ohradit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
novináři	novinář	k1gMnPc1	novinář
dezinterpretovali	dezinterpretovat	k5eAaImAgMnP	dezinterpretovat
církevní	církevní	k2eAgInPc4d1	církevní
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
týkaly	týkat	k5eAaImAgInP	týkat
výhradně	výhradně	k6eAd1	výhradně
práva	právo	k1gNnSc2	právo
církevního	církevní	k2eAgNnSc2d1	církevní
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
světského	světský	k2eAgMnSc2d1	světský
<g/>
,	,	kIx,	,
a	a	k8xC	a
nařizovaly	nařizovat	k5eAaImAgFnP	nařizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
závažné	závažný	k2eAgInPc1d1	závažný
případy	případ	k1gInPc1	případ
řešila	řešit	k5eAaImAgFnS	řešit
přímo	přímo	k6eAd1	přímo
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
místní	místní	k2eAgMnPc1d1	místní
ordináři	ordinář	k1gMnPc1	ordinář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
zneužívání	zneužívání	k1gNnSc2	zneužívání
mladistvých	mladistvý	k1gMnPc2	mladistvý
pak	pak	k6eAd1	pak
prodlužovaly	prodlužovat	k5eAaImAgInP	prodlužovat
promlčecí	promlčecí	k2eAgFnSc4d1	promlčecí
lhůtu	lhůta	k1gFnSc4	lhůta
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
jako	jako	k8xS	jako
prefekt	prefekt	k1gMnSc1	prefekt
řešil	řešit	k5eAaImAgMnS	řešit
i	i	k8xC	i
případy	případ	k1gInPc4	případ
schizmatu	schizma	k1gNnSc2	schizma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1983	[number]	k4	1983
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
latae	lata	k1gInSc2	lata
sententiae	sententiae	k1gFnSc3	sententiae
vietnamského	vietnamský	k2eAgMnSc2d1	vietnamský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Pierre	Pierr	k1gInSc5	Pierr
Martin	Martin	k1gMnSc1	Martin
Ngo	Ngo	k1gFnSc2	Ngo
Dinh	Dinh	k1gMnSc1	Dinh
Thuc	Thuc	k1gFnSc1	Thuc
<g/>
,	,	kIx,	,
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
a	a	k8xC	a
laiků	laik	k1gMnPc2	laik
kolem	kolem	k7c2	kolem
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nepovoleným	povolený	k2eNgNnPc3d1	nepovolené
svěcením	svěcení	k1gNnPc3	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
taktéž	taktéž	k?	taktéž
vedl	vést	k5eAaImAgInS	vést
diskuse	diskuse	k1gFnPc4	diskuse
s	s	k7c7	s
Bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc4	Pius
X.	X.	kA	X.
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Marcelem	Marcel	k1gMnSc7	Marcel
Lefebvrem	Lefebvr	k1gMnSc7	Lefebvr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
později	pozdě	k6eAd2	pozdě
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
papeže	papež	k1gMnSc2	papež
čtyři	čtyři	k4xCgInPc4	čtyři
biskupy	biskup	k1gInPc1	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dle	dle	k7c2	dle
Říma	Řím	k1gInSc2	Řím
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
latae	lata	k1gMnSc2	lata
sententiae	sententia	k1gMnSc2	sententia
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
sám	sám	k3xTgMnSc1	sám
už	už	k9	už
jako	jako	k9	jako
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
tuto	tento	k3xDgFnSc4	tento
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
mužů	muž	k1gMnPc2	muž
římské	římský	k2eAgFnSc2d1	římská
kurie	kurie	k1gFnSc2	kurie
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
taktéž	taktéž	k?	taktéž
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgInPc1d1	blízký
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jako	jako	k8xS	jako
děkan	děkan	k1gMnSc1	děkan
kolegia	kolegium	k1gNnSc2	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
předsedal	předsedat	k5eAaImAgMnS	předsedat
jeho	jeho	k3xOp3gInSc2	jeho
pohřbu	pohřeb	k1gInSc2	pohřeb
a	a	k8xC	a
předcházející	předcházející	k2eAgFnSc4d1	předcházející
mši	mše	k1gFnSc4	mše
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
shromážděné	shromážděný	k2eAgMnPc4d1	shromážděný
kardinály	kardinál	k1gMnPc4	kardinál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
drželi	držet	k5eAaImAgMnP	držet
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
sedisvakance	sedisvakance	k1gFnSc2	sedisvakance
byl	být	k5eAaImAgInS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
osobou	osoba	k1gFnSc7	osoba
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
formálně	formálně	k6eAd1	formálně
v	v	k7c6	v
administrativních	administrativní	k2eAgFnPc6d1	administrativní
záležitostech	záležitost	k1gFnPc6	záležitost
hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
chrání	chránit	k5eAaImIp3nS	chránit
tradiční	tradiční	k2eAgNnPc4d1	tradiční
katolická	katolický	k2eAgNnPc4d1	katolické
stanoviska	stanovisko	k1gNnPc4	stanovisko
ohledně	ohledně	k7c2	ohledně
umělé	umělý	k2eAgFnSc2d1	umělá
kontroly	kontrola	k1gFnSc2	kontrola
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
interrupcí	interrupce	k1gFnPc2	interrupce
či	či	k8xC	či
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
Time	Tim	k1gInSc2	Tim
již	již	k6eAd1	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
citoval	citovat	k5eAaBmAgInS	citovat
nejmenovaný	jmenovaný	k2eNgInSc1d1	nejmenovaný
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prý	prý	k9	prý
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
post	post	k1gInSc4	post
příštího	příští	k2eAgMnSc2d1	příští
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
příliš	příliš	k6eAd1	příliš
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
schopen	schopen	k2eAgInSc1d1	schopen
úřad	úřad	k1gInSc1	úřad
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
dával	dávat	k5eAaImAgMnS	dávat
britský	britský	k2eAgMnSc1d1	britský
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gMnSc1	Times
Ratzignerovi	Ratzigner	k1gMnSc3	Ratzigner
největší	veliký	k2eAgFnSc2d3	veliký
šance	šance	k1gFnSc2	šance
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
rivalové	rival	k1gMnPc1	rival
z	z	k7c2	z
liberálního	liberální	k2eAgNnSc2d1	liberální
křídla	křídlo	k1gNnSc2	křídlo
církve	církev	k1gFnSc2	církev
byli	být	k5eAaImAgMnP	být
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Ratzingera	Ratzingera	k1gFnSc1	Ratzingera
také	také	k9	také
favorizovalo	favorizovat	k5eAaImAgNnS	favorizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
děkan	děkan	k1gMnSc1	děkan
kolégia	kolégium	k1gNnSc2	kolégium
kardinálů	kardinál	k1gMnPc2	kardinál
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
celebrantem	celebrant	k1gMnSc7	celebrant
a	a	k8xC	a
kázal	kázat	k5eAaImAgInS	kázat
jak	jak	k6eAd1	jak
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
konkláve	konkláve	k1gNnSc2	konkláve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
volbou	volba	k1gFnSc7	volba
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
časopis	časopis	k1gInSc4	časopis
Time	Time	k1gInSc1	Time
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
jako	jako	k8xS	jako
prefekt	prefekt	k1gMnSc1	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
se	se	k3xPyFc4	se
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
rád	rád	k2eAgMnSc1d1	rád
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
na	na	k7c6	na
bavorském	bavorský	k2eAgInSc6d1	bavorský
venkově	venkov	k1gInSc6	venkov
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
postupně	postupně	k6eAd1	postupně
stavěn	stavit	k5eAaImNgInS	stavit
do	do	k7c2	do
role	role	k1gFnSc2	role
favorita	favorit	k1gMnSc2	favorit
mnoha	mnoho	k4c7	mnoho
mezinárodními	mezinárodní	k2eAgNnPc7d1	mezinárodní
médii	médium	k1gNnPc7	médium
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc4d1	další
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
zvolení	zvolení	k1gNnSc1	zvolení
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
jisté	jistý	k2eAgNnSc4d1	jisté
a	a	k8xC	a
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
opravdu	opravdu	k6eAd1	opravdu
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
předpovědí	předpověď	k1gFnPc2	předpověď
ohledně	ohledně	k7c2	ohledně
volby	volba	k1gFnSc2	volba
papeže	papež	k1gMnSc2	papež
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zvolení	zvolení	k1gNnSc1	zvolení
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jeho	jeho	k3xOp3gMnSc4	jeho
předchůdce	předchůdce	k1gMnSc4	předchůdce
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc4	Pavel
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
právě	právě	k9	právě
proto	proto	k6eAd1	proto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
zvolení	zvolení	k1gNnSc1	zvolení
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgFnPc4d1	mnohá
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tradičně	tradičně	k6eAd1	tradičně
konkláve	konkláve	k1gNnSc1	konkláve
dávalo	dávat	k5eAaImAgNnS	dávat
před	před	k7c7	před
žhavými	žhavý	k2eAgMnPc7d1	žhavý
kandidáty	kandidát	k1gMnPc7	kandidát
přednost	přednost	k1gFnSc1	přednost
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nástupcem	nástupce	k1gMnSc7	nástupce
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
po	po	k7c6	po
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
skrutiniu	skrutinium	k1gNnSc6	skrutinium
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
papeže	papež	k1gMnSc2	papež
Lva	Lev	k1gMnSc2	Lev
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nejdůležitějšího	důležitý	k2eAgMnSc2d3	nejdůležitější
německého	německý	k2eAgMnSc2d1	německý
papeže	papež	k1gMnSc2	papež
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
známého	známý	k2eAgInSc2d1	známý
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
významné	významný	k2eAgFnPc4d1	významná
institucionální	institucionální	k2eAgFnPc4d1	institucionální
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
balkón	balkón	k1gInSc4	balkón
Baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
volba	volba	k1gFnSc1	volba
oznámena	oznámen	k2eAgFnSc1d1	oznámena
kardinálem-jáhnem	kardinálemáhn	k1gMnSc7	kardinálem-jáhn
Jorgem	Jorg	k1gMnSc7	Jorg
Medinou	Medina	k1gFnSc7	Medina
Estévezem	Estévez	k1gInSc7	Estévez
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
oslovil	oslovit	k5eAaPmAgInS	oslovit
obrovský	obrovský	k2eAgInSc1d1	obrovský
dav	dav	k1gInSc1	dav
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
španělštině	španělština	k1gFnSc6	španělština
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
drazí	drahý	k2eAgMnPc1d1	drahý
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
<g/>
"	"	kIx"	"
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
latinském	latinský	k2eAgNnSc6d1	latinské
oznámení	oznámení	k1gNnSc6	oznámení
Habemus	Habemus	k1gMnSc1	Habemus
Papam	Papam	k1gInSc1	Papam
<g/>
:	:	kIx,	:
Annuntio	Annuntio	k1gNnSc1	Annuntio
vobis	vobis	k1gFnSc2	vobis
gaudium	gaudium	k1gNnSc1	gaudium
magnum	magnum	k1gInSc1	magnum
<g/>
:	:	kIx,	:
Habemus	Habemus	k1gInSc1	Habemus
Papam	Papam	k1gInSc1	Papam
<g/>
!	!	kIx.	!
</s>
<s>
Eminentissimum	Eminentissimum	k1gInSc1	Eminentissimum
ac	ac	k?	ac
reverendissimum	reverendissimum	k?	reverendissimum
Dominum	Dominum	k1gInSc4	Dominum
<g/>
,	,	kIx,	,
Dominum	Dominum	k1gInSc4	Dominum
Iosephum	Iosephum	k1gNnSc4	Iosephum
Sanctæ	Sanctæ	k1gFnSc2	Sanctæ
Romanæ	Romanæ	k1gMnSc7	Romanæ
Ecclesiæ	Ecclesiæ	k1gMnSc7	Ecclesiæ
Cardinalem	Cardinal	k1gMnSc7	Cardinal
Ratzinger	Ratzinger	k1gInSc4	Ratzinger
<g/>
,	,	kIx,	,
Qui	Qui	k1gFnSc4	Qui
sibi	sibi	k6eAd1	sibi
nomen	nomen	k2eAgInSc1d1	nomen
imposuit	imposuit	k2eAgInSc1d1	imposuit
Benedictus	Benedictus	k1gInSc1	Benedictus
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
sloužil	sloužit	k5eAaImAgMnS	sloužit
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
intronizační	intronizační	k2eAgFnSc4d1	intronizační
mši	mše	k1gFnSc4	mše
svatou	svatý	k2eAgFnSc4d1	svatá
na	na	k7c6	na
Svatopetrském	svatopetrský	k2eAgNnSc6d1	Svatopetrské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
papežské	papežský	k2eAgNnSc1d1	papežské
palium	palium	k1gNnSc1	palium
a	a	k8xC	a
Rybářský	rybářský	k2eAgInSc1d1	rybářský
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
převzal	převzít	k5eAaPmAgMnS	převzít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
papežově	papežův	k2eAgInSc6d1	papežův
katedrálním	katedrální	k2eAgInSc6d1	katedrální
chrámu	chrám	k1gInSc6	chrám
-	-	kIx~	-
Lateránské	lateránský	k2eAgFnSc6d1	Lateránská
bazilice	bazilika	k1gFnSc6	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgMnS	být
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
78	[number]	k4	78
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zvolení	zvolený	k2eAgMnPc1d1	zvolený
starší	starší	k1gMnPc1	starší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Klement	Klement	k1gMnSc1	Klement
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1730	[number]	k4	1730
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
kardinál	kardinál	k1gMnSc1	kardinál
nejdéle	dlouho	k6eAd3	dlouho
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
-	-	kIx~	-
<g/>
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
devátým	devátý	k4xOgMnSc7	devátý
německým	německý	k2eAgMnSc7d1	německý
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
posledním	poslední	k2eAgMnSc7d1	poslední
byl	být	k5eAaImAgMnS	být
holandsko-německý	holandskoěmecký	k2eAgMnSc1d1	holandsko-německý
papež	papež	k1gMnSc1	papež
Hadrián	Hadrián	k1gMnSc1	Hadrián
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1522	[number]	k4	1522
<g/>
-	-	kIx~	-
<g/>
1523	[number]	k4	1523
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Utrechtu	Utrecht	k1gInSc2	Utrecht
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
jméno	jméno	k1gNnSc4	jméno
Benedikt	benedikt	k1gInSc1	benedikt
byl	být	k5eAaImAgMnS	být
Ital	Ital	k1gMnSc1	Ital
Benedikt	Benedikt	k1gMnSc1	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
si	se	k3xPyFc3	se
jako	jako	k9	jako
svoje	svůj	k3xOyFgNnSc4	svůj
papežské	papežský	k2eAgNnSc4d1	papežské
jméno	jméno	k1gNnSc4	jméno
vybral	vybrat	k5eAaPmAgInS	vybrat
jméno	jméno	k1gNnSc4	jméno
Benedikt	Benedikt	k1gMnSc1	Benedikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
znamenající	znamenající	k2eAgNnSc1d1	znamenající
"	"	kIx"	"
<g/>
požehnaný	požehnaný	k2eAgMnSc1d1	požehnaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
odkaz	odkaz	k1gInSc4	odkaz
jednak	jednak	k8xC	jednak
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c4	na
svatého	svatý	k2eAgMnSc4d1	svatý
Benedikta	Benedikt	k1gMnSc4	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
zaníceně	zaníceně	k6eAd1	zaníceně
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
pontifikát	pontifikát	k1gInSc4	pontifikát
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejkratších	krátký	k2eAgMnPc2d3	nejkratší
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Benedikt	Benedikt	k1gMnSc1	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
benediktinských	benediktinský	k2eAgInPc2d1	benediktinský
klášterů	klášter	k1gInPc2	klášter
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
klášterů	klášter	k1gInPc2	klášter
středověku	středověk	k1gInSc2	středověk
měla	mít	k5eAaImAgFnS	mít
benediktinskou	benediktinský	k2eAgFnSc4d1	benediktinská
řeholi	řehole	k1gFnSc4	řehole
<g/>
)	)	kIx)	)
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
Řehole	řehole	k1gFnSc2	řehole
svatého	svatý	k2eAgMnSc2d1	svatý
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
prací	práce	k1gFnSc7	práce
vztahující	vztahující	k2eAgNnPc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
řeholnímu	řeholní	k2eAgInSc3d1	řeholní
životu	život	k1gInSc3	život
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
výběr	výběr	k1gInSc4	výběr
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
generální	generální	k2eAgFnSc6d1	generální
audienci	audience	k1gFnSc6	audience
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
na	na	k7c6	na
Svatopetrském	svatopetrský	k2eAgNnSc6d1	Svatopetrské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
prvním	první	k4xOgInSc6	první
setkání	setkání	k1gNnSc6	setkání
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
především	především	k6eAd1	především
zastavil	zastavit	k5eAaPmAgMnS	zastavit
u	u	k7c2	u
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
a	a	k8xC	a
pastýřem	pastýř	k1gMnSc7	pastýř
univerzální	univerzální	k2eAgFnSc2d1	univerzální
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nazývat	nazývat	k5eAaImF	nazývat
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
ideálně	ideálně	k6eAd1	ideálně
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
ctihodného	ctihodný	k2eAgMnSc4d1	ctihodný
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vedl	vést	k5eAaImAgInS	vést
církev	církev	k1gFnSc4	církev
v	v	k7c6	v
trýznivém	trýznivý	k2eAgNnSc6d1	trýznivé
údobí	údobí	k1gNnSc6	údobí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odvážným	odvážný	k2eAgMnSc7d1	odvážný
a	a	k8xC	a
ryzím	ryzí	k2eAgMnSc7d1	ryzí
prorokem	prorok	k1gMnSc7	prorok
míru	mír	k1gInSc2	mír
a	a	k8xC	a
s	s	k7c7	s
neohroženou	ohrožený	k2eNgFnSc7d1	neohrožená
odvahou	odvaha	k1gFnSc7	odvaha
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
válečnému	válečný	k2eAgNnSc3d1	válečné
dramatu	drama	k1gNnSc3	drama
a	a	k8xC	a
pak	pak	k6eAd1	pak
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
jeho	jeho	k3xOp3gInPc4	jeho
zhoubné	zhoubný	k2eAgInPc4d1	zhoubný
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
stopách	stopa	k1gFnPc6	stopa
chci	chtít	k5eAaImIp1nS	chtít
své	svůj	k3xOyFgNnSc4	svůj
ministerium	ministerium	k1gNnSc4	ministerium
dát	dát	k5eAaPmF	dát
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
smíření	smíření	k1gNnSc2	smíření
a	a	k8xC	a
harmonie	harmonie	k1gFnSc2	harmonie
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsem	být	k5eAaImIp1nS	být
hluboce	hluboko	k6eAd1	hluboko
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
dobro	dobro	k1gNnSc1	dobro
míru	mír	k1gInSc2	mír
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
Božím	boží	k2eAgInSc7d1	boží
darem	dar	k1gInSc7	dar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
křehký	křehký	k2eAgInSc1d1	křehký
a	a	k8xC	a
vzácný	vzácný	k2eAgInSc1d1	vzácný
dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgMnSc4	jenž
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
prosit	prosit	k5eAaImF	prosit
<g/>
,	,	kIx,	,
chránit	chránit	k5eAaImF	chránit
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
budovat	budovat	k5eAaImF	budovat
den	den	k1gInSc4	den
co	co	k9	co
den	den	k1gInSc4	den
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Benedikt	Benedikt	k1gMnSc1	Benedikt
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
připomíná	připomínat	k5eAaImIp3nS	připomínat
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
postavu	postava	k1gFnSc4	postava
velkého	velký	k2eAgMnSc2d1	velký
"	"	kIx"	"
<g/>
patriarchy	patriarcha	k1gMnSc2	patriarcha
západního	západní	k2eAgNnSc2d1	západní
mnišství	mnišství	k1gNnSc2	mnišství
<g/>
"	"	kIx"	"
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc4	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
<g/>
,	,	kIx,	,
spolupatrona	spolupatrona	k1gFnSc1	spolupatrona
Evropy	Evropa	k1gFnSc2	Evropa
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
sv.	sv.	kA	sv.
Cyrilem	Cyril	k1gMnSc7	Cyril
a	a	k8xC	a
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
,	,	kIx,	,
apoštoly	apoštol	k1gMnPc7	apoštol
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Postupné	postupný	k2eAgNnSc1d1	postupné
šíření	šíření	k1gNnSc1	šíření
benediktínského	benediktínský	k2eAgInSc2d1	benediktínský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
nesmírný	smírný	k2eNgInSc4d1	nesmírný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
evropském	evropský	k2eAgInSc6d1	evropský
světadílu	světadíl	k1gInSc6	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Benedikt	Benedikt	k1gMnSc1	Benedikt
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
velmi	velmi	k6eAd1	velmi
uctíván	uctívat	k5eAaImNgMnS	uctívat
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gFnSc3	můj
rodné	rodný	k2eAgFnSc3d1	rodná
zemi	zem	k1gFnSc3	zem
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
jednotu	jednota	k1gFnSc4	jednota
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
mocnou	mocný	k2eAgFnSc7d1	mocná
připomínkou	připomínka	k1gFnSc7	připomínka
nezadatelných	zadatelný	k2eNgInPc2d1	nezadatelný
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kořenů	kořen	k1gInPc2	kořen
její	její	k3xOp3gFnSc2	její
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
intronizační	intronizační	k2eAgFnSc2d1	intronizační
mše	mše	k1gFnSc2	mše
svaté	svatá	k1gFnSc2	svatá
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
byl	být	k5eAaImAgInS	být
akt	akt	k1gInSc1	akt
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g />
.	.	kIx.	.
</s>
<s>
podřízenosti	podřízenost	k1gFnPc1	podřízenost
kardinálů	kardinál	k1gMnPc2	kardinál
vůči	vůči	k7c3	vůči
papeži	papež	k1gMnSc3	papež
nahrazen	nahradit	k5eAaPmNgInS	nahradit
pozdravením	pozdravení	k1gNnPc3	pozdravení
dvanácti	dvanáct	k4xCc2	dvanáct
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
byli	být	k5eAaImAgMnP	být
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
zástupci	zástupce	k1gMnPc1	zástupce
kléru	klér	k1gInSc2	klér
a	a	k8xC	a
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
,	,	kIx,	,
manželský	manželský	k2eAgInSc4d1	manželský
pár	pár	k1gInSc4	pár
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
a	a	k8xC	a
novokněží	novokněz	k1gMnPc1	novokněz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kardinálové	kardinál	k1gMnPc1	kardinál
formálně	formálně	k6eAd1	formálně
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
svoji	svůj	k3xOyFgFnSc4	svůj
podřízenost	podřízenost	k1gFnSc4	podřízenost
papeži	papež	k1gMnSc3	papež
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Benedikt	Benedikt	k1gMnSc1	Benedikt
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
otevřený	otevřený	k2eAgMnSc1d1	otevřený
papamobil	papamobit	k5eAaPmAgMnS	papamobit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
a	a	k8xC	a
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
své	svůj	k3xOyFgFnSc2	svůj
pastorální	pastorální	k2eAgFnSc2d1	pastorální
role	role	k1gFnSc2	role
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
křtí	křtít	k5eAaImIp3nS	křtít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Sixtinské	sixtinský	k2eAgFnSc6d1	Sixtinská
kapli	kaple	k1gFnSc6	kaple
několik	několik	k4yIc4	několik
nemluvňat	nemluvně	k1gNnPc2	nemluvně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
Vatikán	Vatikán	k1gInSc1	Vatikán
spustil	spustit	k5eAaPmAgInS	spustit
vlastní	vlastní	k2eAgInSc4d1	vlastní
kanál	kanál	k1gInSc4	kanál
na	na	k7c6	na
serveru	server	k1gInSc6	server
YouTube	YouTub	k1gInSc5	YouTub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
jazycích	jazyk	k1gInPc6	jazyk
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
videa	video	k1gNnSc2	video
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
děním	dění	k1gNnSc7	dění
okolo	okolo	k7c2	okolo
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
nosit	nosit	k5eAaImF	nosit
některé	některý	k3yIgFnPc4	některý
tradiční	tradiční	k2eAgFnPc4d1	tradiční
části	část	k1gFnPc4	část
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c6	o
papežské	papežský	k2eAgFnSc6d1	Papežská
červené	červená	k1gFnSc6	červená
boty	bota	k1gFnPc4	bota
či	či	k8xC	či
camauro	camaura	k1gFnSc5	camaura
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
naposledy	naposledy	k6eAd1	naposledy
nosil	nosit	k5eAaImAgMnS	nosit
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
byl	být	k5eAaImAgInS	být
i	i	k9	i
pohřben	pohřben	k2eAgInSc1d1	pohřben
<g/>
.	.	kIx.	.
</s>
<s>
Obnovil	obnovit	k5eAaPmAgMnS	obnovit
též	též	k9	též
užívání	užívání	k1gNnSc3	užívání
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
druhů	druh	k1gInPc2	druh
papežské	papežský	k2eAgFnSc2d1	Papežská
mozetty	mozetta	k1gFnSc2	mozetta
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
používal	používat	k5eAaImAgMnS	používat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
beatifikační	beatifikační	k2eAgInSc1d1	beatifikační
proces	proces	k1gInSc1	proces
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
audienci	audience	k1gFnSc6	audience
s	s	k7c7	s
Benediktem	benedikt	k1gInSc7	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Camillo	Camilla	k1gMnSc5	Camilla
Ruini	Ruiň	k1gMnSc5	Ruiň
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
vikář	vikář	k1gMnSc1	vikář
římské	římský	k2eAgFnSc2d1	římská
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
postava	postava	k1gFnSc1	postava
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
kanonizace	kanonizace	k1gFnSc2	kanonizace
osob	osoba	k1gFnPc2	osoba
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
"	"	kIx"	"
<g/>
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
proces	proces	k1gInSc1	proces
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
pětiletou	pětiletý	k2eAgFnSc4d1	pětiletá
čekací	čekací	k2eAgFnSc4d1	čekací
lhůtu	lhůta	k1gFnSc4	lhůta
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
beatifikace	beatifikace	k1gFnSc2	beatifikace
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
-	-	kIx~	-
Pia	Pius	k1gMnSc2	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
beatifikaci	beatifikace	k1gFnSc6	beatifikace
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Fatimské	Fatimský	k2eAgFnSc2d1	Fatimská
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
24	[number]	k4	24
<g/>
.	.	kIx.	.
výročím	výročí	k1gNnSc7	výročí
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
záchranu	záchrana	k1gFnSc4	záchrana
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
přisuzoval	přisuzovat	k5eAaImAgMnS	přisuzovat
právě	právě	k6eAd1	právě
Panně	Panna	k1gFnSc3	Panna
Marii	Mario	k1gMnPc7	Mario
Fatimské	Fatimský	k2eAgNnSc1d1	Fatimské
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Ruini	Ruieň	k1gFnSc3	Ruieň
slavnostně	slavnostně	k6eAd1	slavnostně
zahájil	zahájit	k5eAaPmAgInS	zahájit
beatifikační	beatifikační	k2eAgInSc1d1	beatifikační
proces	proces	k1gInSc1	proces
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
diecézi	diecéze	k1gFnSc6	diecéze
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
v	v	k7c6	v
Lateránské	lateránský	k2eAgFnSc6d1	Lateránská
bazilice	bazilika	k1gFnSc6	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
beatifikace	beatifikace	k1gFnSc1	beatifikace
pod	pod	k7c7	pod
novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
byla	být	k5eAaImAgFnS	být
slavena	slaven	k2eAgFnSc1d1	slavena
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
kardinálem	kardinál	k1gMnSc7	kardinál
José	Josá	k1gFnSc2	Josá
Saraiva	Saraiva	k1gFnSc1	Saraiva
Martinsem	Martinso	k1gNnSc7	Martinso
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
papež	papež	k1gMnSc1	papež
beatifikoval	beatifikovat	k5eAaPmAgMnS	beatifikovat
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgMnPc2d1	další
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gMnSc2	jeho
předchůdce	předchůdce	k1gMnSc2	předchůdce
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
přenechal	přenechat	k5eAaPmAgInS	přenechat
beatifikační	beatifikační	k2eAgFnSc4d1	beatifikační
liturgickou	liturgický	k2eAgFnSc4d1	liturgická
službu	služba	k1gFnSc4	služba
kardinálovi	kardinál	k1gMnSc3	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
blahořečení	blahořečení	k1gNnSc4	blahořečení
a	a	k8xC	a
svatořečení	svatořečení	k1gNnSc4	svatořečení
vydala	vydat	k5eAaPmAgFnS	vydat
úřední	úřední	k2eAgFnSc4d1	úřední
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
beatifikace	beatifikace	k1gFnSc1	beatifikace
bude	být	k5eAaImBp3nS	být
celebrovat	celebrovat	k5eAaImF	celebrovat
zástupce	zástupce	k1gMnSc4	zástupce
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
prefekt	prefekt	k1gMnSc1	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
kanonizaci	kanonizace	k1gFnSc4	kanonizace
celebroval	celebrovat	k5eAaImAgMnS	celebrovat
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
na	na	k7c6	na
Svatopetrském	svatopetrský	k2eAgNnSc6d1	Svatopetrské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Kanonizování	kanonizování	k1gNnSc2	kanonizování
byli	být	k5eAaImAgMnP	být
Josef	Josef	k1gMnSc1	Josef
Bilczewski	Bilczewsk	k1gFnSc2	Bilczewsk
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Hurtado	Hurtada	k1gFnSc5	Hurtada
SJ	SJ	kA	SJ
<g/>
,	,	kIx,	,
Zygmunt	Zygmunt	k1gInSc4	Zygmunt
Goradowski	Goradowsk	k1gFnSc2	Goradowsk
<g/>
,	,	kIx,	,
Gaetano	Gaetana	k1gFnSc5	Gaetana
Catanoso	Catanosa	k1gFnSc5	Catanosa
a	a	k8xC	a
Felice	Felice	k1gInPc1	Felice
z	z	k7c2	z
Nicosie	Nicosie	k1gFnSc2	Nicosie
<g/>
.	.	kIx.	.
</s>
<s>
Kanonizace	kanonizace	k1gFnSc1	kanonizace
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
mše	mše	k1gFnSc2	mše
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
biskupského	biskupský	k2eAgInSc2d1	biskupský
synodu	synod	k1gInSc2	synod
a	a	k8xC	a
roku	rok	k1gInSc2	rok
eucharistie	eucharistie	k1gFnSc2	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
papež	papež	k1gMnSc1	papež
kanonizoval	kanonizovat	k5eAaBmAgMnS	kanonizovat
další	další	k2eAgFnPc4d1	další
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
obřad	obřad	k1gInSc1	obřad
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
při	při	k7c6	při
papežově	papežův	k2eAgFnSc6d1	papežova
návštěvě	návštěva	k1gFnSc6	návštěva
-	-	kIx~	-
prvním	první	k4xOgMnSc6	první
rodilým	rodilý	k2eAgMnSc7d1	rodilý
Brazilcem	Brazilec	k1gMnSc7	Brazilec
prohlášeným	prohlášený	k2eAgMnSc7d1	prohlášený
za	za	k7c4	za
svatého	svatý	k2eAgMnSc2d1	svatý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
fra	fra	k?	fra
Galvăo	Galvăo	k1gMnSc1	Galvăo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kanonizaci	kanonizace	k1gFnSc3	kanonizace
první	první	k4xOgFnPc4	první
Indky	Indka	k1gFnPc4	Indka
-	-	kIx~	-
svaté	svatý	k2eAgMnPc4d1	svatý
Alfonsy	Alfons	k1gMnPc4	Alfons
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
svátku	svátek	k1gInSc2	svátek
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
do	do	k7c2	do
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
rok	rok	k1gInSc4	rok
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
v	v	k7c6	v
Bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zahájil	zahájit	k5eAaPmAgMnS	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
Liturgie	liturgie	k1gFnSc1	liturgie
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
jak	jak	k6eAd1	jak
ekumenický	ekumenický	k2eAgMnSc1d1	ekumenický
patriarcha	patriarcha	k1gMnSc1	patriarcha
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
I.	I.	kA	I.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zástupci	zástupce	k1gMnPc1	zástupce
dalších	další	k2eAgFnPc2d1	další
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Motivací	motivace	k1gFnSc7	motivace
pro	pro	k7c4	pro
konání	konání	k1gNnSc4	konání
roku	rok	k1gInSc2	rok
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
byla	být	k5eAaImAgFnS	být
připomínka	připomínka	k1gFnSc1	připomínka
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
(	(	kIx(	(
<g/>
ohledně	ohledně	k7c2	ohledně
přesného	přesný	k2eAgInSc2d1	přesný
roku	rok	k1gInSc2	rok
narození	narození	k1gNnSc2	narození
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
10	[number]	k4	10
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
byly	být	k5eAaImAgInP	být
též	též	k9	též
prováděny	prováděn	k2eAgFnPc1d1	prováděna
archeologické	archeologický	k2eAgFnPc1d1	archeologická
práce	práce	k1gFnPc1	práce
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
autentičnost	autentičnost	k1gFnSc4	autentičnost
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
večer	večer	k6eAd1	večer
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
slavnostně	slavnostně	k6eAd1	slavnostně
zakončil	zakončit	k5eAaPmAgMnS	zakončit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
rok	rok	k1gInSc4	rok
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tak	tak	k6eAd1	tak
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
kněží	kněz	k1gMnPc2	kněz
je	být	k5eAaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročím	výročí	k1gNnSc7	výročí
úmrtí	úmrť	k1gFnPc2	úmrť
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Vianneye	Vianney	k1gMnSc2	Vianney
-	-	kIx~	-
patrona	patrona	k1gFnSc1	patrona
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
kněží	kněz	k1gMnPc2	kněz
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
mezi	mezi	k7c7	mezi
věřícími	věřící	k2eAgMnPc7d1	věřící
a	a	k8xC	a
především	především	k9	především
mezi	mezi	k7c7	mezi
kněžími	kněz	k1gMnPc7	kněz
obnovit	obnovit	k5eAaPmF	obnovit
vědomí	vědomí	k1gNnSc1	vědomí
důležitosti	důležitost	k1gFnSc2	důležitost
kněžské	kněžský	k2eAgFnSc2d1	kněžská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
přehled	přehled	k1gInSc1	přehled
apoštolských	apoštolský	k2eAgFnPc2d1	apoštolská
cest	cesta	k1gFnPc2	cesta
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
plánovaných	plánovaný	k2eAgFnPc2d1	plánovaná
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
apoštolských	apoštolský	k2eAgFnPc2d1	apoštolská
cest	cesta	k1gFnPc2	cesta
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
..	..	k?	..
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
podnikl	podniknout	k5eAaPmAgMnS	podniknout
taktéž	taktéž	k?	taktéž
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgFnPc2d1	různá
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
návštěv	návštěva	k1gFnPc2	návštěva
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
navštívil	navštívit	k5eAaPmAgMnS	navštívit
dvakrát	dvakrát	k6eAd1	dvakrát
svoje	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
taktéž	taktéž	k?	taktéž
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
neobešla	obešnout	k5eNaPmAgFnS	obešnout
bez	bez	k7c2	bez
protestů	protest	k1gInPc2	protest
tamějších	tamější	k2eAgMnPc2d1	tamější
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
reagujících	reagující	k2eAgMnPc2d1	reagující
na	na	k7c4	na
papežovy	papežův	k2eAgInPc4d1	papežův
výroky	výrok	k1gInPc4	výrok
během	během	k7c2	během
přednášky	přednáška	k1gFnSc2	přednáška
v	v	k7c6	v
Řezně	řezně	k6eAd1	řezně
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
velkými	velký	k2eAgInPc7d1	velký
bezpečnostními	bezpečnostní	k2eAgNnPc7d1	bezpečnostní
opatřeními	opatření	k1gNnPc7	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
též	též	k9	též
papežova	papežův	k2eAgFnSc1d1	papežova
návštěva	návštěva	k1gFnSc1	návštěva
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
země	zem	k1gFnPc1	zem
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
zde	zde	k6eAd1	zde
papež	papež	k1gMnSc1	papež
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
zahajoval	zahajovat	k5eAaImAgInS	zahajovat
sjezd	sjezd	k1gInSc1	sjezd
22	[number]	k4	22
biskupských	biskupský	k2eAgFnPc2d1	biskupská
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgMnPc2	všecek
věřících	věřící	k1gMnPc2	věřící
římské	římský	k2eAgFnSc2d1	římská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
též	též	k9	též
svatořečil	svatořečit	k5eAaBmAgMnS	svatořečit
prvního	první	k4xOgMnSc2	první
rodilého	rodilý	k2eAgMnSc2d1	rodilý
Brazilce	Brazilec	k1gMnSc2	Brazilec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
-	-	kIx~	-
františkána	františkán	k1gMnSc4	františkán
Galvăo	Galvăo	k6eAd1	Galvăo
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc1d1	závěrečná
mše	mše	k1gFnPc1	mše
v	v	k7c6	v
Aparecidě	Aparecida	k1gFnSc6	Aparecida
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
přes	přes	k7c4	přes
150	[number]	k4	150
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
papež	papež	k1gMnSc1	papež
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zároveň	zároveň	k6eAd1	zároveň
oslavil	oslavit	k5eAaPmAgInS	oslavit
své	své	k1gNnSc4	své
81	[number]	k4	81
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
prezidentem	prezident	k1gMnSc7	prezident
Bushem	Bush	k1gMnSc7	Bush
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
oběťmi	oběť	k1gFnPc7	oběť
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
afér	aféra	k1gFnPc2	aféra
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
znovu	znovu	k6eAd1	znovu
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
účastnil	účastnit	k5eAaImAgInS	účastnit
Světových	světový	k2eAgInPc2d1	světový
dnů	den	k1gInPc2	den
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
pak	pak	k6eAd1	pak
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Francii	Francie	k1gFnSc4	Francie
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
zjevení	zjevení	k1gNnSc2	zjevení
v	v	k7c6	v
Lurdech	Lurd	k1gInPc6	Lurd
<g/>
.	.	kIx.	.
</s>
<s>
Papežovy	Papežův	k2eAgFnPc1d1	Papežova
mše	mše	k1gFnPc1	mše
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
účastnilo	účastnit	k5eAaImAgNnS	účastnit
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k8xC	jako
papež	papež	k1gMnSc1	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
postupně	postupně	k6eAd1	postupně
Kamerun	Kamerun	k1gInSc4	Kamerun
a	a	k8xC	a
Angolu	Angola	k1gFnSc4	Angola
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
bylo	být	k5eAaImAgNnS	být
téma	téma	k1gNnSc1	téma
míru	mír	k1gInSc2	mír
a	a	k8xC	a
smíření	smíření	k1gNnSc2	smíření
v	v	k7c6	v
afrických	africký	k2eAgFnPc6d1	africká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
občanské	občanský	k2eAgFnPc1d1	občanská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Kamerunu	Kamerun	k1gInSc6	Kamerun
předal	předat	k5eAaPmAgMnS	předat
Instrumentum	instrumentum	k1gNnSc4	instrumentum
laboris	laboris	k1gFnSc2	laboris
k	k	k7c3	k
biskupskému	biskupský	k2eAgInSc3d1	biskupský
synodu	synod	k1gInSc3	synod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zasedal	zasedat	k5eAaImAgInS	zasedat
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
smíření	smíření	k1gNnSc2	smíření
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
sůl	sůl	k1gFnSc4	sůl
země	zem	k1gFnSc2	zem
<g/>
...	...	k?	...
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
taktéž	taktéž	k?	taktéž
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
setkání	setkání	k1gNnSc4	setkání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
ženských	ženský	k2eAgInPc2d1	ženský
organizacích	organizace	k1gFnPc6	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Promluvil	promluvit	k5eAaPmAgInS	promluvit
taktéž	taktéž	k?	taktéž
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
interrupcí	interrupce	k1gFnPc2	interrupce
či	či	k8xC	či
problému	problém	k1gInSc2	problém
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc1d1	závěrečná
mše	mše	k1gFnPc1	mše
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
papež	papež	k1gMnSc1	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Svatou	svatý	k2eAgFnSc4d1	svatá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k9	jako
cestu	cesta	k1gFnSc4	cesta
poutníka	poutník	k1gMnSc2	poutník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
modlit	modlit	k5eAaImF	modlit
na	na	k7c4	na
posvátná	posvátný	k2eAgNnPc4d1	posvátné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
jeho	jeho	k3xOp3gFnPc2	jeho
promluv	promluva	k1gFnPc2	promluva
byl	být	k5eAaImAgInS	být
mír	mír	k1gInSc1	mír
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
sjednocování	sjednocování	k1gNnSc4	sjednocování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
též	též	k9	též
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
či	či	k8xC	či
spory	spor	k1gInPc1	spor
o	o	k7c4	o
posvátná	posvátný	k2eAgNnPc4d1	posvátné
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
i	i	k9	i
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
palestinské	palestinský	k2eAgFnSc2d1	palestinská
samosprávy	samospráva	k1gFnSc2	samospráva
Mahmúdem	Mahmúd	k1gMnSc7	Mahmúd
Abbásem	Abbás	k1gMnSc7	Abbás
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
proslovu	proslov	k1gInSc6	proslov
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
podporuje	podporovat	k5eAaImIp3nS	podporovat
právo	právo	k1gNnSc4	právo
vašeho	váš	k3xOp2gInSc2	váš
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
svrchovanou	svrchovaný	k2eAgFnSc4d1	svrchovaná
palestinskou	palestinský	k2eAgFnSc4d1	palestinská
domovinu	domovina	k1gFnSc4	domovina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vašich	váš	k3xOp2gInPc2	váš
předků	předek	k1gInPc2	předek
<g/>
,	,	kIx,	,
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
a	a	k8xC	a
existující	existující	k2eAgFnSc4d1	existující
v	v	k7c6	v
míru	mír	k1gInSc6	mír
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
s	s	k7c7	s
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznanými	uznaný	k2eAgFnPc7d1	uznaná
hranicemi	hranice	k1gFnPc7	hranice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
papež	papež	k1gMnSc1	papež
také	také	k9	také
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
nejposvátnějšího	posvátný	k2eAgNnSc2d3	nejposvátnější
místa	místo	k1gNnSc2	místo
islámu	islám	k1gInSc2	islám
-	-	kIx~	-
mešity	mešita	k1gFnSc2	mešita
al-Aksá	al-Aksat	k5eAaPmIp3nS	al-Aksat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Návštěva	návštěva	k1gFnSc1	návštěva
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Třídenní	třídenní	k2eAgFnSc4d1	třídenní
návštěvu	návštěva	k1gFnSc4	návštěva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
svou	svůj	k3xOyFgFnSc4	svůj
13	[number]	k4	13
<g/>
.	.	kIx.	.
apoštolskou	apoštolský	k2eAgFnSc4d1	apoštolská
cestu	cesta	k1gFnSc4	cesta
zahájil	zahájit	k5eAaPmAgMnS	zahájit
na	na	k7c6	na
ruzyňském	ruzyňský	k2eAgNnSc6d1	ruzyňské
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
Svatého	svatý	k2eAgMnSc2d1	svatý
otce	otec	k1gMnSc2	otec
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poklonil	poklonit	k5eAaPmAgMnS	poklonit
Pražskému	pražský	k2eAgNnSc3d1	Pražské
Jezulátku	Jezulátko	k1gNnSc3	Jezulátko
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
stovkami	stovka	k1gFnPc7	stovka
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
,	,	kIx,	,
řeholnic	řeholnice	k1gFnPc2	řeholnice
<g/>
,	,	kIx,	,
seminaristů	seminarista	k1gMnPc2	seminarista
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
laických	laický	k2eAgNnPc2d1	laické
hnutí	hnutí	k1gNnPc2	hnutí
ke	k	k7c3	k
společné	společný	k2eAgFnSc3d1	společná
modlitbě	modlitba	k1gFnSc3	modlitba
nešpor	nešpora	k1gFnPc2	nešpora
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
sloužil	sloužit	k5eAaImAgMnS	sloužit
mši	mše	k1gFnSc4	mše
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Apoštolskou	apoštolský	k2eAgFnSc4d1	apoštolská
cestu	cesta	k1gFnSc4	cesta
zakončil	zakončit	k5eAaPmAgMnS	zakončit
pondělní	pondělní	k2eAgFnSc7d1	pondělní
návštěvou	návštěva	k1gFnSc7	návštěva
Staré	Staré	k2eAgFnSc3d1	Staré
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
svátku	svátek	k1gInSc2	svátek
patrona	patrona	k1gFnSc1	patrona
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
i	i	k9	i
setkání	setkání	k1gNnSc1	setkání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
se	se	k3xPyFc4	se
poklonil	poklonit	k5eAaPmAgMnS	poklonit
vystaveným	vystavený	k2eAgInSc7d1	vystavený
ostatkům	ostatek	k1gInPc3	ostatek
světcovým	světcův	k2eAgInPc3d1	světcův
a	a	k8xC	a
poté	poté	k6eAd1	poté
sloužil	sloužit	k5eAaImAgInS	sloužit
mši	mše	k1gFnSc4	mše
na	na	k7c6	na
Proboštské	proboštský	k2eAgFnSc6d1	Proboštská
louce	louka	k1gFnSc6	louka
před	před	k7c7	před
50	[number]	k4	50
000	[number]	k4	000
věřícími	věřící	k1gMnPc7	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
vzpomenul	vzpomenout	k5eAaPmAgMnS	vzpomenout
80	[number]	k4	80
výročí	výročí	k1gNnPc4	výročí
Svatováclavského	svatováclavský	k2eAgNnSc2d1	Svatováclavské
milénia	milénium	k1gNnSc2	milénium
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
právě	právě	k9	právě
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesté	šestý	k4xOgFnSc6	šestý
hodině	hodina	k1gFnSc6	hodina
odpolední	odpolední	k2eAgMnSc1d1	odpolední
papež	papež	k1gMnSc1	papež
odlétl	odlétnout	k5eAaPmAgMnS	odlétnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
ji	on	k3xPp3gFnSc4	on
kanonické	kanonický	k2eAgNnSc1d1	kanonické
právo	právo	k1gNnSc1	právo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
-	-	kIx~	-
posledním	poslední	k2eAgInSc6d1	poslední
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
papeže	papež	k1gMnSc2	papež
dobrovolně	dobrovolně	k6eAd1	dobrovolně
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Celestýn	Celestýn	k1gMnSc1	Celestýn
V.	V.	kA	V.
roku	rok	k1gInSc2	rok
1294	[number]	k4	1294
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
vynucená	vynucený	k2eAgNnPc4d1	vynucené
opouštění	opouštění	k1gNnPc4	opouštění
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
obdobích	období	k1gNnPc6	období
dvojpapežství	dvojpapežství	k1gNnSc2	dvojpapežství
a	a	k8xC	a
trojpapežství	trojpapežství	k1gNnSc2	trojpapežství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jsem	být	k5eAaImIp1nS	být
opakovaně	opakovaně	k6eAd1	opakovaně
zpytoval	zpytovat	k5eAaImAgMnS	zpytovat
své	svůj	k3xOyFgNnSc4	svůj
svědomí	svědomí	k1gNnSc4	svědomí
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
došel	dojít	k5eAaPmAgMnS	dojít
jsem	být	k5eAaImIp1nS	být
k	k	k7c3	k
jistotě	jistota	k1gFnSc3	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
mé	můj	k3xOp1gFnPc4	můj
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
pokročilému	pokročilý	k2eAgInSc3d1	pokročilý
věku	věk	k1gInSc3	věk
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
vyhovující	vyhovující	k2eAgFnPc1d1	vyhovující
pro	pro	k7c4	pro
adekvátní	adekvátní	k2eAgInSc4d1	adekvátní
výkon	výkon	k1gInSc4	výkon
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
zaskočil	zaskočit	k5eAaPmAgInS	zaskočit
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
ve	v	k7c4	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
SEČ	SEČ	kA	SEČ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
úřadu	úřad	k1gInSc2	úřad
papeže	papež	k1gMnSc2	papež
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
oficiální	oficiální	k2eAgInSc4d1	oficiální
titul	titul	k1gInSc4	titul
Jeho	jeho	k3xOp3gFnSc1	jeho
Svatost	svatost	k1gFnSc1	svatost
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
emeritní	emeritní	k2eAgMnSc1d1	emeritní
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Jeho	jeho	k3xOp3gFnSc1	jeho
Svatost	svatost	k1gFnSc1	svatost
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
emeritní	emeritní	k2eAgInSc1d1	emeritní
pontifik	pontifik	k1gInSc1	pontifik
římský	římský	k2eAgInSc1d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zvolení	zvolení	k1gNnSc2	zvolení
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
Františka	František	k1gMnSc4	František
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
rezidenci	rezidence	k1gFnSc6	rezidence
v	v	k7c4	v
Castel	Castel	k1gInSc4	Castel
Gandolfo	Gandolfo	k6eAd1	Gandolfo
<g/>
,	,	kIx,	,
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
klášteře	klášter	k1gInSc6	klášter
Mater	mater	k1gFnSc2	mater
Ecclesiae	Ecclesia	k1gFnSc2	Ecclesia
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zvolením	zvolení	k1gNnSc7	zvolení
papežem	papež	k1gMnSc7	papež
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pracích	práce	k1gFnPc6	práce
věnoval	věnovat	k5eAaPmAgMnS	věnovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
téma	téma	k1gNnSc1	téma
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
svazek	svazek	k1gInSc1	svazek
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
ze	z	k7c2	z
16	[number]	k4	16
<g/>
)	)	kIx)	)
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
úplného	úplný	k2eAgNnSc2d1	úplné
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
Opera	opera	k1gFnSc1	opera
omnia	omnium	k1gNnSc2	omnium
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Herder	Herdra	k1gFnPc2	Herdra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
právě	právě	k9	právě
Ratzingerovy	Ratzingerův	k2eAgFnPc4d1	Ratzingerova
práce	práce	k1gFnPc4	práce
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
budou	být	k5eAaImBp3nP	být
vydávána	vydávat	k5eAaImNgNnP	vydávat
tematicky	tematicky	k6eAd1	tematicky
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
určil	určit	k5eAaPmAgMnS	určit
sám	sám	k3xTgMnSc1	sám
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
svazku	svazek	k1gInSc2	svazek
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
tvoří	tvořit	k5eAaImIp3nS	tvořit
kniha	kniha	k1gFnSc1	kniha
Duch	duch	k1gMnSc1	duch
liturgie	liturgie	k1gFnSc1	liturgie
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Der	drát	k5eAaImRp2nS	drát
Geist	Geist	k1gMnSc1	Geist
der	drát	k5eAaImRp2nS	drát
Liturgie	liturgie	k1gFnSc2	liturgie
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Ratzinger	Ratzingra	k1gFnPc2	Ratzingra
podává	podávat	k5eAaImIp3nS	podávat
výklad	výklad	k1gInSc1	výklad
liturgických	liturgický	k2eAgInPc2d1	liturgický
úkonů	úkon	k1gInPc2	úkon
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
zakořenění	zakořenění	k1gNnSc4	zakořenění
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
nastaly	nastat	k5eAaPmAgInP	nastat
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvíce	hodně	k6eAd3	hodně
komentovanou	komentovaný	k2eAgFnSc7d1	komentovaná
částí	část	k1gFnSc7	část
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kapitola	kapitola	k1gFnSc1	kapitola
o	o	k7c6	o
směru	směr	k1gInSc6	směr
modliteb	modlitba	k1gFnPc2	modlitba
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
kněze	kněz	k1gMnSc2	kněz
vůči	vůči	k7c3	vůči
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
hájí	hájit	k5eAaImIp3nS	hájit
společné	společný	k2eAgNnSc4d1	společné
nasměrování	nasměrování	k1gNnSc4	nasměrování
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
kněze	kněz	k1gMnSc2	kněz
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
prosadily	prosadit	k5eAaPmAgFnP	prosadit
v	v	k7c6	v
pokoncilní	pokoncilní	k2eAgFnSc6d1	pokoncilní
liturgické	liturgický	k2eAgFnSc6d1	liturgická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
změně	změna	k1gFnSc6	změna
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
kněze	kněz	k1gMnSc2	kněz
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
např.	např.	kA	např.
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tím	ten	k3xDgNnSc7	ten
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
klerikalizace	klerikalizace	k1gFnSc1	klerikalizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neexistoval	existovat	k5eNaImAgInS	existovat
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
kněz	kněz	k1gMnSc1	kněz
-	-	kIx~	-
předsedající	předsedající	k1gMnSc1	předsedající
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nyní	nyní	k6eAd1	nyní
bývá	bývat	k5eAaImIp3nS	bývat
raději	rád	k6eAd2	rád
nazýván	nazývat	k5eAaImNgInS	nazývat
-	-	kIx~	-
stává	stávat	k5eAaImIp3nS	stávat
vlastním	vlastní	k2eAgInSc7d1	vlastní
vztažným	vztažný	k2eAgInSc7d1	vztažný
bodem	bod	k1gInSc7	bod
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
něm.	něm.	k?	něm.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
musíme	muset	k5eAaImIp1nP	muset
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc2	jeho
liturgických	liturgický	k2eAgInPc2d1	liturgický
úkonů	úkon	k1gInPc2	úkon
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gInSc3	on
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odpovídat	odpovídat	k5eAaImF	odpovídat
<g/>
...	...	k?	...
Stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
Bůh	bůh	k1gMnSc1	bůh
<g/>
...	...	k?	...
Obrácení	obrácení	k1gNnSc1	obrácení
kněze	kněz	k1gMnSc2	kněz
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
formuje	formovat	k5eAaImIp3nS	formovat
nyní	nyní	k6eAd1	nyní
obec	obec	k1gFnSc1	obec
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
podobnou	podobný	k2eAgFnSc4d1	podobná
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Řešení	řešení	k1gNnSc1	řešení
situace	situace	k1gFnSc2	situace
pak	pak	k6eAd1	pak
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
kříže	kříž	k1gInSc2	kříž
doprostřed	doprostřed	k7c2	doprostřed
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
věřící	věřící	k1gFnSc1	věřící
i	i	k8xC	i
kněz	kněz	k1gMnSc1	kněz
byli	být	k5eAaImAgMnP	být
společně	společně	k6eAd1	společně
obráceni	obrátit	k5eAaPmNgMnP	obrátit
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
ke	k	k7c3	k
kříži	kříž	k1gInSc3	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
papežských	papežský	k2eAgFnPc2d1	Papežská
mší	mše	k1gFnPc2	mše
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
přijímání	přijímání	k1gNnSc2	přijímání
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
kleče	kleče	k6eAd1	kleče
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Duch	duch	k1gMnSc1	duch
liturgie	liturgie	k1gFnSc2	liturgie
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
staví	stavit	k5eAaPmIp3nS	stavit
proti	proti	k7c3	proti
stavění	stavění	k1gNnSc3	stavění
kříže	kříž	k1gInSc2	kříž
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
oltáře	oltář	k1gInSc2	oltář
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
naprosto	naprosto	k6eAd1	naprosto
absurdní	absurdní	k2eAgInSc4d1	absurdní
jev	jev	k1gInSc4	jev
posledních	poslední	k2eAgNnPc2d1	poslední
desetiletí	desetiletí	k1gNnPc2	desetiletí
považuji	považovat	k5eAaImIp1nS	považovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kříž	kříž	k1gInSc1	kříž
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Ruší	rušit	k5eAaImIp3nS	rušit
snad	snad	k9	snad
kříž	kříž	k1gInSc4	kříž
při	při	k7c6	při
eucharistii	eucharistie	k1gFnSc6	eucharistie
<g/>
?	?	kIx.	?
</s>
<s>
Je	být	k5eAaImIp3nS	být
kněz	kněz	k1gMnSc1	kněz
důležitější	důležitý	k2eAgMnSc1d2	důležitější
než	než	k8xS	než
Pán	pán	k1gMnSc1	pán
<g/>
?	?	kIx.	?
</s>
<s>
Tento	tento	k3xDgInSc1	tento
omyl	omyl	k1gInSc1	omyl
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
napraven	napraven	k2eAgMnSc1d1	napraven
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Téma	téma	k1gNnSc1	téma
papežových	papežův	k2eAgInPc2d1	papežův
názorů	názor	k1gInPc2	názor
na	na	k7c6	na
liturgii	liturgie	k1gFnSc6	liturgie
vyvstalo	vyvstat	k5eAaPmAgNnS	vyvstat
výrazněji	výrazně	k6eAd2	výrazně
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
motu	moto	k1gNnSc6	moto
proprio	proprio	k1gNnSc1	proprio
Summorum	Summorum	k1gInSc4	Summorum
pontificum	pontificum	k1gInSc1	pontificum
či	či	k8xC	či
zrušením	zrušení	k1gNnSc7	zrušení
exkomunikací	exkomunikace	k1gFnPc2	exkomunikace
biskupů	biskup	k1gMnPc2	biskup
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc4	Pius
X.	X.	kA	X.
(	(	kIx(	(
<g/>
tradicionalistické	tradicionalistický	k2eAgFnSc2d1	tradicionalistická
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
zpravidla	zpravidla	k6eAd1	zpravidla
slaví	slavit	k5eAaImIp3nP	slavit
liturgii	liturgie	k1gFnSc4	liturgie
podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
způsobu	způsob	k1gInSc2	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rozhorovů	rozhorov	k1gInPc2	rozhorov
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Seewaldem	Seewald	k1gMnSc7	Seewald
Křesťanství	křesťanství	k1gNnSc2	křesťanství
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
Ratzinger	Ratzingra	k1gFnPc2	Ratzingra
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oživení	oživení	k1gNnSc4	oživení
starého	starý	k2eAgInSc2d1	starý
ritu	rit	k1gInSc2	rit
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
žádné	žádný	k3yNgNnSc1	žádný
řešení	řešení	k1gNnSc1	řešení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
starý	starý	k2eAgInSc1d1	starý
ritus	ritus	k1gInSc1	ritus
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
"	"	kIx"	"
<g/>
mnohem	mnohem	k6eAd1	mnohem
velkoryseji	velkoryse	k6eAd2	velkoryse
dopřát	dopřát	k5eAaPmF	dopřát
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
přejí	přát	k5eAaImIp3nP	přát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tradice	tradice	k1gFnSc2	tradice
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
co	co	k9	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
posuzování	posuzování	k1gNnSc2	posuzování
záležitostí	záležitost	k1gFnPc2	záležitost
sexuální	sexuální	k2eAgFnSc2d1	sexuální
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
prefektem	prefekt	k1gMnSc7	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
názory	názor	k1gInPc4	názor
církve	církev	k1gFnSc2	církev
i	i	k9	i
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
vydal	vydat	k5eAaPmAgMnS	vydat
některé	některý	k3yIgInPc4	některý
dokumenty	dokument	k1gInPc4	dokument
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
etických	etický	k2eAgFnPc2d1	etická
otázek	otázka	k1gFnPc2	otázka
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc2	využití
embryí	embryo	k1gNnPc2	embryo
<g/>
,	,	kIx,	,
interrupce	interrupce	k1gFnPc1	interrupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
tématem	téma	k1gNnSc7	téma
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
vydala	vydat	k5eAaPmAgFnS	vydat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
několik	několik	k4yIc1	několik
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
papeže	papež	k1gMnSc2	papež
Benedikt	benedikt	k1gInSc4	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgInS	nechat
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
Kongregaci	kongregace	k1gFnSc6	kongregace
pro	pro	k7c4	pro
katolickou	katolický	k2eAgFnSc4d1	katolická
výchovu	výchova	k1gFnSc4	výchova
vydat	vydat	k5eAaPmF	vydat
dokument	dokument	k1gInSc4	dokument
Instrukce	instrukce	k1gFnSc2	instrukce
o	o	k7c6	o
kněžství	kněžství	k1gNnSc6	kněžství
a	a	k8xC	a
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
schválil	schválit	k5eAaPmAgInS	schválit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
udělovat	udělovat	k5eAaImF	udělovat
kněžská	kněžský	k2eAgNnPc4d1	kněžské
svěcení	svěcení	k1gNnPc4	svěcení
osobám	osoba	k1gFnPc3	osoba
praktikujícím	praktikující	k2eAgFnPc3d1	praktikující
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
osobám	osoba	k1gFnPc3	osoba
se	se	k3xPyFc4	se
silnými	silný	k2eAgInPc7d1	silný
homosexuálními	homosexuální	k2eAgInPc7d1	homosexuální
sklony	sklon	k1gInPc7	sklon
či	či	k8xC	či
osobám	osoba	k1gFnPc3	osoba
podporujícím	podporující	k2eAgFnPc3d1	podporující
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
gay	gay	k1gMnSc1	gay
kulturu	kultura	k1gFnSc4	kultura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
reakce	reakce	k1gFnPc4	reakce
jak	jak	k8xC	jak
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vně	vně	k6eAd1	vně
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
taktéž	taktéž	k?	taktéž
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
ohledně	ohledně	k7c2	ohledně
morální	morální	k2eAgFnSc2d1	morální
nepřípustnosti	nepřípustnost	k1gFnSc2	nepřípustnost
umělých	umělý	k2eAgFnPc2d1	umělá
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
proti	proti	k7c3	proti
provádění	provádění	k1gNnSc3	provádění
interrupcí	interrupce	k1gFnPc2	interrupce
a	a	k8xC	a
eutanazie	eutanazie	k1gFnSc2	eutanazie
či	či	k8xC	či
lékařskému	lékařský	k2eAgNnSc3d1	lékařské
využívání	využívání	k1gNnSc3	využívání
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Afriky	Afrika	k1gFnSc2	Afrika
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
proti	proti	k7c3	proti
používání	používání	k1gNnSc3	používání
kondomů	kondom	k1gInPc2	kondom
a	a	k8xC	a
doporučil	doporučit	k5eAaPmAgMnS	doporučit
spíše	spíše	k9	spíše
sexuální	sexuální	k2eAgFnSc4d1	sexuální
zdrženlivost	zdrženlivost	k1gFnSc4	zdrženlivost
a	a	k8xC	a
manželskou	manželský	k2eAgFnSc4d1	manželská
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
bylo	být	k5eAaImAgNnS	být
některými	některý	k3yIgMnPc7	některý
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
sdělovacími	sdělovací	k2eAgInPc7d1	sdělovací
prostředky	prostředek	k1gInPc7	prostředek
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
veřejně	veřejně	k6eAd1	veřejně
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
kondom	kondom	k1gInSc1	kondom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
rozhovorů	rozhovor	k1gInPc2	rozhovor
vydané	vydaný	k2eAgInPc4d1	vydaný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
však	však	k9	však
připustil	připustit	k5eAaPmAgMnS	připustit
za	za	k7c2	za
zcela	zcela	k6eAd1	zcela
specifických	specifický	k2eAgFnPc2d1	specifická
situací	situace	k1gFnPc2	situace
užití	užití	k1gNnSc2	užití
prezervativu	prezervativ	k1gInSc2	prezervativ
jako	jako	k8xS	jako
nástroje	nástroj	k1gInPc4	nástroj
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
epidemii	epidemie	k1gFnSc3	epidemie
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
banalizovat	banalizovat	k5eAaImF	banalizovat
sexualitu	sexualita	k1gFnSc4	sexualita
"	"	kIx"	"
<g/>
zaměřením	zaměření	k1gNnSc7	zaměření
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
kondom	kondom	k1gInSc4	kondom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
opravdový	opravdový	k2eAgInSc4d1	opravdový
a	a	k8xC	a
náležitý	náležitý	k2eAgInSc4d1	náležitý
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
infekcí	infekce	k1gFnSc7	infekce
HIV	HIV	kA	HIV
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
lítost	lítost	k1gFnSc4	lítost
a	a	k8xC	a
zděšení	zděšení	k1gNnSc4	zděšení
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sexuálními	sexuální	k2eAgInPc7d1	sexuální
pedofilními	pedofilní	k2eAgInPc7d1	pedofilní
skandály	skandál	k1gInPc7	skandál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
provalily	provalit	k5eAaPmAgFnP	provalit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
do	do	k7c2	do
kterých	který	k3yRgMnPc2	který
byli	být	k5eAaImAgMnP	být
zapleteni	zapleten	k2eAgMnPc1d1	zapleten
někteří	některý	k3yIgMnPc1	některý
církevní	církevní	k2eAgMnPc1d1	církevní
katoličtí	katolický	k2eAgMnPc1d1	katolický
představitelé	představitel	k1gMnPc1	představitel
a	a	k8xC	a
vychovatelé	vychovatel	k1gMnPc1	vychovatel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
papež	papež	k1gMnSc1	papež
je	být	k5eAaImIp3nS	být
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
autoritou	autorita	k1gFnSc7	autorita
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
co	co	k9	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
otázek	otázka	k1gFnPc2	otázka
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
mravů	mrav	k1gInPc2	mrav
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
jeho	jeho	k3xOp3gFnSc4	jeho
věroučnou	věroučný	k2eAgFnSc4d1	věroučná
pozici	pozice	k1gFnSc4	pozice
mu	on	k3xPp3gMnSc3	on
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
dlouholeté	dlouholetý	k2eAgNnSc1d1	dlouholeté
působení	působení	k1gNnSc1	působení
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
prefekta	prefekt	k1gMnSc2	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
osobními	osobní	k2eAgInPc7d1	osobní
teologickými	teologický	k2eAgInPc7d1	teologický
názory	názor	k1gInPc7	názor
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
prezentovanými	prezentovaný	k2eAgFnPc7d1	prezentovaná
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
odborných	odborný	k2eAgNnPc6d1	odborné
dílech	dílo	k1gNnPc6	dílo
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gNnPc7	jeho
vyjádřeními	vyjádření	k1gNnPc7	vyjádření
zaštítěnými	zaštítěný	k2eAgFnPc7d1	zaštítěná
autoritou	autorita	k1gFnSc7	autorita
Petrova	Petrův	k2eAgMnSc4d1	Petrův
nástupce	nástupce	k1gMnSc4	nástupce
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
církevních	církevní	k2eAgInPc6d1	církevní
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
encyklikách	encyklika	k1gFnPc6	encyklika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
rozdělování	rozdělování	k1gNnSc4	rozdělování
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
prefektem	prefekt	k1gMnSc7	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
těchto	tento	k3xDgInPc2	tento
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
zatím	zatím	k6eAd1	zatím
jediným	jediný	k2eAgNnSc7d1	jediné
osobním	osobní	k2eAgNnSc7d1	osobní
teologickým	teologický	k2eAgNnSc7d1	teologické
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
kniha	kniha	k1gFnSc1	kniha
Ježíš	Ježíš	k1gMnSc1	Ježíš
Nazaretský	nazaretský	k2eAgMnSc1d1	nazaretský
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
měl	mít	k5eAaImAgInS	mít
rozpracované	rozpracovaný	k2eAgFnSc3d1	rozpracovaná
už	už	k9	už
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
zvolením	zvolení	k1gNnSc7	zvolení
na	na	k7c4	na
Petrův	Petrův	k2eAgInSc4d1	Petrův
stolec	stolec	k1gInSc4	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
komentátorů	komentátor	k1gMnPc2	komentátor
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
papežovy	papežův	k2eAgFnSc2d1	papežova
zprávy	zpráva	k1gFnSc2	zpráva
lidem	člověk	k1gMnPc3	člověk
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
odstavci	odstavec	k1gInSc6	odstavec
homilie	homilie	k1gFnSc2	homilie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přednesl	přednést	k5eAaPmAgMnS	přednést
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
inaugurační	inaugurační	k2eAgFnSc6d1	inaugurační
mši	mše	k1gFnSc6	mše
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
i	i	k9	i
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připomnění	připomnění	k1gNnSc6	připomnění
známých	známý	k2eAgNnPc2d1	známé
slov	slovo	k1gNnPc2	slovo
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nemějte	mít	k5eNaImRp2nP	mít
strach	strach	k1gInSc4	strach
<g/>
!	!	kIx.	!
</s>
<s>
Otevřete	otevřít	k5eAaPmIp2nP	otevřít
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokořán	dokořán	k6eAd1	dokořán
rozevřete	rozevřít	k5eAaPmIp2nP	rozevřít
brány	brána	k1gFnPc1	brána
Kristu	Krista	k1gFnSc4	Krista
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Což	což	k9	což
snad	snad	k9	snad
všichni	všechen	k3xTgMnPc1	všechen
nemáme	mít	k5eNaImIp1nP	mít
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
strach	strach	k1gInSc1	strach
-	-	kIx~	-
necháme	nechat	k5eAaPmIp1nP	nechat
<g/>
-li	i	k?	-li
Krista	Krista	k1gFnSc1	Krista
zcela	zcela	k6eAd1	zcela
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
nitra	nitro	k1gNnSc2	nitro
<g/>
,	,	kIx,	,
otevřeme	otevřít	k5eAaPmIp1nP	otevřít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naprosto	naprosto	k6eAd1	naprosto
-	-	kIx~	-
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
odnést	odnést	k5eAaPmF	odnést
něco	něco	k3yInSc4	něco
z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
života	život	k1gInSc2	život
<g/>
?	?	kIx.	?
</s>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
snad	snad	k9	snad
strach	strach	k1gInSc4	strach
zříci	zříct	k5eAaPmF	zříct
se	se	k3xPyFc4	se
něčeho	něco	k3yInSc2	něco
velikého	veliký	k2eAgNnSc2d1	veliké
<g/>
,	,	kIx,	,
jedinečného	jedinečný	k2eAgMnSc2d1	jedinečný
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
život	život	k1gInSc1	život
tak	tak	k6eAd1	tak
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
papež	papež	k1gMnSc1	papež
chtěl	chtít	k5eAaImAgMnS	chtít
říci	říct	k5eAaPmF	říct
<g/>
:	:	kIx,	:
Ne	ne	k9	ne
<g/>
!	!	kIx.	!
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
nechá	nechat	k5eAaPmIp3nS	nechat
vstoupit	vstoupit	k5eAaPmF	vstoupit
Krista	Krista	k1gFnSc1	Krista
<g/>
,	,	kIx,	,
neztrácí	ztrácet	k5eNaImIp3nS	ztrácet
nic	nic	k3yNnSc1	nic
-	-	kIx~	-
nic	nic	k3yNnSc1	nic
-	-	kIx~	-
absolutně	absolutně	k6eAd1	absolutně
nic	nic	k3yNnSc1	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
činí	činit	k5eAaImIp3nS	činit
život	život	k1gInSc1	život
svobodným	svobodný	k2eAgNnSc7d1	svobodné
<g/>
,	,	kIx,	,
krásným	krásný	k2eAgNnSc7d1	krásné
a	a	k8xC	a
velkým	velký	k2eAgNnSc7d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
<g/>
!	!	kIx.	!
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
přátelství	přátelství	k1gNnSc6	přátelství
se	se	k3xPyFc4	se
otevírají	otevírat	k5eAaImIp3nP	otevírat
dokořán	dokořán	k6eAd1	dokořán
brány	brána	k1gFnPc1	brána
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
přátelství	přátelství	k1gNnSc6	přátelství
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
otevřou	otevřít	k5eAaPmIp3nP	otevřít
velké	velký	k2eAgFnPc4d1	velká
možnosti	možnost	k1gFnPc4	možnost
lidské	lidský	k2eAgFnSc3d1	lidská
bytosti	bytost	k1gFnSc3	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
přátelství	přátelství	k1gNnSc6	přátelství
zakoušíme	zakoušet	k5eAaImIp1nP	zakoušet
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
krásné	krásný	k2eAgNnSc1d1	krásné
a	a	k8xC	a
co	co	k3yRnSc1	co
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bych	by	kYmCp1nS	by
dnes	dnes	k6eAd1	dnes
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkušenosti	zkušenost	k1gFnSc2	zkušenost
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
osobního	osobní	k2eAgInSc2d1	osobní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
říci	říct	k5eAaPmF	říct
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
drazí	drahý	k2eAgMnPc1d1	drahý
mladí	mladý	k1gMnPc1	mladý
<g/>
:	:	kIx,	:
Nemějte	mít	k5eNaImRp2nP	mít
strach	strach	k1gInSc4	strach
z	z	k7c2	z
Krista	Kristus	k1gMnSc2	Kristus
<g/>
!	!	kIx.	!
</s>
<s>
On	on	k3xPp3gMnSc1	on
nic	nic	k6eAd1	nic
neodnímá	odnímat	k5eNaImIp3nS	odnímat
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
Jemu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
stonásobně	stonásobně	k6eAd1	stonásobně
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
otevřete	otevřít	k5eAaPmIp2nP	otevřít
<g/>
,	,	kIx,	,
dokořán	dokořán	k6eAd1	dokořán
rozevřete	rozevřít	k5eAaPmIp2nP	rozevřít
brány	brána	k1gFnPc4	brána
Kristu	Krista	k1gFnSc4	Krista
-	-	kIx~	-
a	a	k8xC	a
najdete	najít	k5eAaPmIp2nP	najít
pravý	pravý	k2eAgInSc4d1	pravý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgNnSc7d1	časté
tématem	téma	k1gNnSc7	téma
Benediktových	Benediktův	k2eAgFnPc2d1	Benediktova
promluv	promluva	k1gFnPc2	promluva
a	a	k8xC	a
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
i	i	k9	i
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Ježíš	ježit	k5eAaImIp2nS	ježit
Nazaretský	nazaretský	k2eAgInSc1d1	nazaretský
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
hlavní	hlavní	k2eAgInSc1d1	hlavní
smysl	smysl	k1gInSc1	smysl
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
povzbudit	povzbudit	k5eAaPmF	povzbudit
v	v	k7c6	v
čtenáři	čtenář	k1gMnSc6	čtenář
růst	růst	k1gInSc4	růst
živého	živé	k1gNnSc2	živé
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
přátelství	přátelství	k1gNnSc2	přátelství
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
projevuje	projevovat	k5eAaImIp3nS	projevovat
solidaritou	solidarita	k1gFnSc7	solidarita
a	a	k8xC	a
skutky	skutek	k1gInPc1	skutek
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
bližním	bližní	k1gMnPc3	bližní
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
encyklice	encyklika	k1gFnSc6	encyklika
Deus	Deusa	k1gFnPc2	Deusa
caritas	caritas	k1gMnSc1	caritas
est	est	k?	est
<g/>
.	.	kIx.	.
</s>
<s>
Relativismus	relativismus	k1gInSc1	relativismus
papež	papež	k1gMnSc1	papež
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
musí	muset	k5eAaImIp3nS	muset
dnešní	dnešní	k2eAgFnSc1d1	dnešní
víra	víra	k1gFnSc1	víra
čelit	čelit	k5eAaImF	čelit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
relativismem	relativismus	k1gInSc7	relativismus
varoval	varovat	k5eAaImAgMnS	varovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
svých	svůj	k3xOyFgFnPc6	svůj
promluvách	promluva	k1gFnPc6	promluva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
homilii	homilie	k1gFnSc6	homilie
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
konkláve	konkláve	k1gNnSc2	konkláve
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zavádí	zavádět	k5eAaImIp3nS	zavádět
"	"	kIx"	"
<g/>
diktatura	diktatura	k1gFnSc1	diktatura
relativismu	relativismus	k1gInSc2	relativismus
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kolik	kolika	k1gFnPc2	kolika
větrů	vítr	k1gInPc2	vítr
různých	různý	k2eAgFnPc2d1	různá
doktrín	doktrína	k1gFnPc2	doktrína
jsme	být	k5eAaImIp1nP	být
poznali	poznat	k5eAaPmAgMnP	poznat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
ideologických	ideologický	k2eAgInPc2d1	ideologický
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
<g />
.	.	kIx.	.
</s>
<s>
módních	módní	k2eAgFnPc2d1	módní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
...	...	k?	...
Malá	malý	k2eAgFnSc1d1	malá
loďka	loďka	k1gFnSc1	loďka
myšlení	myšlení	k1gNnSc6	myšlení
mnoha	mnoho	k4c2	mnoho
křesťanů	křesťan	k1gMnPc2	křesťan
byla	být	k5eAaImAgFnS	být
nezřídka	nezřídka	k6eAd1	nezřídka
zmítána	zmítán	k2eAgFnSc1d1	zmítána
těmito	tento	k3xDgFnPc7	tento
vlnami	vlna	k1gFnPc7	vlna
-	-	kIx~	-
vrhána	vrhat	k5eAaImNgFnS	vrhat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
krajnosti	krajnost	k1gFnSc2	krajnost
do	do	k7c2	do
druhé	druhý	k4xOgFnPc4	druhý
<g/>
:	:	kIx,	:
od	od	k7c2	od
marxismu	marxismus	k1gInSc2	marxismus
k	k	k7c3	k
liberalismu	liberalismus	k1gInSc3	liberalismus
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
libertinismu	libertinismus	k1gInSc3	libertinismus
[	[	kIx(	[
<g/>
naprostá	naprostý	k2eAgFnSc1d1	naprostá
mravní	mravní	k2eAgFnSc1d1	mravní
nevázanost	nevázanost	k1gFnSc1	nevázanost
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
od	od	k7c2	od
kolektivismu	kolektivismus	k1gInSc2	kolektivismus
k	k	k7c3	k
radikálnímu	radikální	k2eAgInSc3d1	radikální
individualismu	individualismus	k1gInSc3	individualismus
<g/>
;	;	kIx,	;
od	od	k7c2	od
ateizmu	ateizmus	k1gInSc2	ateizmus
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
vágnímu	vágní	k2eAgInSc3d1	vágní
náboženskému	náboženský	k2eAgInSc3d1	náboženský
mysticismu	mysticismus	k1gInSc3	mysticismus
<g/>
;	;	kIx,	;
od	od	k7c2	od
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
k	k	k7c3	k
synkretismu	synkretismus	k1gInSc3	synkretismus
[	[	kIx(	[
<g/>
vybírat	vybírat	k5eAaImF	vybírat
si	se	k3xPyFc3	se
co	co	k9	co
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc1d1	nová
sekty	sekta	k1gFnPc1	sekta
a	a	k8xC	a
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
říká	říkat	k5eAaImIp3nS	říkat
sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
o	o	k7c6	o
"	"	kIx"	"
<g/>
lidské	lidský	k2eAgFnSc6d1	lidská
chytrosti	chytrost	k1gFnSc6	chytrost
a	a	k8xC	a
prohnanosti	prohnanost	k1gFnSc6	prohnanost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
podvádí	podvádět	k5eAaImIp3nS	podvádět
a	a	k8xC	a
klame	klamat	k5eAaImIp3nS	klamat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mít	mít	k5eAaImF	mít
jasnou	jasný	k2eAgFnSc4d1	jasná
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Creda	Credo	k1gNnSc2	Credo
[	[	kIx(	[
<g/>
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
]	]	kIx)	]
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
nálepkou	nálepka	k1gFnSc7	nálepka
"	"	kIx"	"
<g/>
fundamentalismu	fundamentalismus	k1gInSc2	fundamentalismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
relativismus	relativismus	k1gInSc1	relativismus
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dát	dát	k5eAaPmF	dát
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zmítat	zmítat	k5eAaImF	zmítat
a	a	k8xC	a
strhovat	strhovat	k5eAaImF	strhovat
větrem	vítr	k1gInSc7	vítr
kdejakého	kdejaký	k3yIgNnSc2	kdejaký
učení	učení	k1gNnSc2	učení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
postoj	postoj	k1gInSc4	postoj
na	na	k7c6	na
výši	výše	k1gFnSc6	výše
dnešních	dnešní	k2eAgInPc2d1	dnešní
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
diktatura	diktatura	k1gFnSc1	diktatura
relativismu	relativismus	k1gInSc2	relativismus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neuznává	uznávat	k5eNaImIp3nS	uznávat
nic	nic	k6eAd1	nic
za	za	k7c4	za
konečné	konečný	k2eAgNnSc4d1	konečné
(	(	kIx(	(
<g/>
definitivní	definitivní	k2eAgNnSc4d1	definitivní
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yRgFnSc1	který
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc1d1	jediné
měřítko	měřítko	k1gNnSc1	měřítko
pouze	pouze	k6eAd1	pouze
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc4d1	vlastní
chutě	chuť	k1gFnPc4	chuť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
promluvě	promluva	k1gFnSc6	promluva
při	při	k7c6	při
uvítání	uvítání	k1gNnSc6	uvítání
účastníků	účastník	k1gMnPc2	účastník
Světových	světový	k2eAgInPc2d1	světový
dnů	den	k1gInPc2	den
mládeže	mládež	k1gFnSc2	mládež
2008	[number]	k4	2008
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
v	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Relativismus	relativismus	k1gInSc4	relativismus
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
všemu	všecek	k3xTgMnSc3	všecek
přikládá	přikládat	k5eAaImIp3nS	přikládat
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
"	"	kIx"	"
<g/>
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
"	"	kIx"	"
důležitější	důležitý	k2eAgFnSc1d2	důležitější
nad	nad	k7c4	nad
všechno	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
odtržené	odtržený	k2eAgNnSc1d1	odtržené
od	od	k7c2	od
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
či	či	k8xC	či
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
nikoli	nikoli	k9	nikoli
k	k	k7c3	k
ryzí	ryzí	k2eAgFnSc3d1	ryzí
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
k	k	k7c3	k
morálnímu	morální	k2eAgMnSc3d1	morální
a	a	k8xC	a
intelektuálnímu	intelektuální	k2eAgInSc3d1	intelektuální
chaosu	chaos	k1gInSc3	chaos
<g/>
,	,	kIx,	,
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
morální	morální	k2eAgFnSc2d1	morální
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
sebeúcty	sebeúcta	k1gFnSc2	sebeúcta
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
zoufalství	zoufalství	k1gNnSc3	zoufalství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Postavení	postavení	k1gNnSc1	postavení
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc6	tento
"	"	kIx"	"
<g/>
diktatuře	diktatura	k1gFnSc3	diktatura
relativismu	relativismus	k1gInSc2	relativismus
<g/>
"	"	kIx"	"
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
výzvu	výzva	k1gFnSc4	výzva
<g/>
,	,	kIx,	,
před	před	k7c4	před
kterou	který	k3yQgFnSc4	který
stojí	stát	k5eAaImIp3nS	stát
dnešní	dnešní	k2eAgFnSc1d1	dnešní
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
Kantově	Kantův	k2eAgNnSc6d1	Kantovo
samo-omezení	samomezení	k1gNnSc6	samo-omezení
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dle	dle	k7c2	dle
něj	on	k3xPp3gNnSc2	on
i	i	k9	i
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dnešní	dnešní	k2eAgFnSc7d1	dnešní
aklamací	aklamace	k1gFnSc7	aklamace
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
úspěch	úspěch	k1gInSc1	úspěch
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
rozumu	rozum	k1gInSc2	rozum
k	k	k7c3	k
poznávání	poznávání	k1gNnSc3	poznávání
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
samo-amputace	samomputace	k1gFnSc1	samo-amputace
rozumu	rozum	k1gInSc2	rozum
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
patologickým	patologický	k2eAgInPc3d1	patologický
důsledkům	důsledek	k1gInPc3	důsledek
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
terorismus	terorismus	k1gInSc1	terorismus
a	a	k8xC	a
k	k	k7c3	k
patologiím	patologie	k1gFnPc3	patologie
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ekologických	ekologický	k2eAgFnPc2d1	ekologická
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
diecézním	diecézní	k2eAgNnSc6d1	diecézní
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
Benedikt	benedikt	k1gInSc4	benedikt
při	při	k7c6	při
homilii	homilie	k1gFnSc6	homilie
týkající	týkající	k2eAgFnSc6d1	týkající
se	se	k3xPyFc4	se
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zaobíral	zaobírat	k5eAaImAgMnS	zaobírat
i	i	k9	i
tématem	téma	k1gNnSc7	téma
sexuální	sexuální	k2eAgFnSc2d1	sexuální
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dnes	dnes	k6eAd1	dnes
zvláště	zvláště	k6eAd1	zvláště
zákeřnou	zákeřný	k2eAgFnSc7d1	zákeřná
překážkou	překážka	k1gFnSc7	překážka
výchovného	výchovný	k2eAgNnSc2d1	výchovné
díla	dílo	k1gNnSc2	dílo
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
je	být	k5eAaImIp3nS	být
masivní	masivní	k2eAgFnSc1d1	masivní
<g />
.	.	kIx.	.
</s>
<s>
přítomnost	přítomnost	k1gFnSc1	přítomnost
onoho	onen	k3xDgInSc2	onen
relativismu	relativismus	k1gInSc2	relativismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
neuznává	uznávat	k5eNaImIp3nS	uznávat
za	za	k7c4	za
definitivní	definitivní	k2eAgFnSc4d1	definitivní
<g/>
,	,	kIx,	,
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
jako	jako	k9	jako
poslední	poslední	k2eAgNnSc1d1	poslední
měřítko	měřítko	k1gNnSc1	měřítko
všeho	všecek	k3xTgMnSc4	všecek
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
rozmary	rozmara	k1gMnPc7	rozmara
a	a	k8xC	a
choutkami	choutka	k1gFnPc7	choutka
a	a	k8xC	a
pod	pod	k7c7	pod
zdáním	zdání	k1gNnSc7	zdání
svobody	svoboda	k1gFnSc2	svoboda
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
vězením	vězení	k1gNnSc7	vězení
<g/>
...	...	k?	...
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musíme	muset	k5eAaImIp1nP	muset
nejen	nejen	k6eAd1	nejen
snažit	snažit	k5eAaImF	snažit
překonat	překonat	k5eAaPmF	překonat
relativismus	relativismus	k1gInSc4	relativismus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsme	být	k5eAaImIp1nP	být
povoláni	povolán	k2eAgMnPc1d1	povolán
čelit	čelit	k5eAaImF	čelit
jeho	jeho	k3xOp3gFnSc3	jeho
nadvládě	nadvláda	k1gFnSc3	nadvláda
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Deus	Deus	k1gInSc4	Deus
caritas	caritasa	k1gFnPc2	caritasa
est	est	k?	est
<g/>
,	,	kIx,	,
Spe	Spe	k1gFnSc3	Spe
salvi	salev	k1gFnSc3	salev
a	a	k8xC	a
Caritas	Caritas	k1gInSc1	Caritas
in	in	k?	in
veritate	veritat	k1gMnSc5	veritat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
papež	papež	k1gMnSc1	papež
vydal	vydat	k5eAaPmAgMnS	vydat
celkově	celkově	k6eAd1	celkově
tři	tři	k4xCgFnPc4	tři
encykliky	encyklika	k1gFnPc4	encyklika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
s	s	k7c7	s
názvem	název	k1gInSc7	název
Deus	Deusa	k1gFnPc2	Deusa
caritas	caritasa	k1gFnPc2	caritasa
est	est	k?	est
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
)	)	kIx)	)
vydanou	vydaný	k2eAgFnSc4d1	vydaná
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
Spe	Spe	k1gFnSc4	Spe
salvi	salvit	k5eAaPmRp2nS	salvit
(	(	kIx(	(
<g/>
Spaseni	spasen	k2eAgMnPc1d1	spasen
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
)	)	kIx)	)
vydanou	vydaný	k2eAgFnSc4d1	vydaná
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
Caritas	Caritas	k1gInSc4	Caritas
in	in	k?	in
veritate	veritat	k1gInSc5	veritat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k9	již
názvy	název	k1gInPc1	název
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
encyklika	encyklika	k1gFnSc1	encyklika
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
tématu	téma	k1gNnSc2	téma
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
vztahu	vztah	k1gInSc2	vztah
agapé	agapý	k2eAgFnSc3d1	agapý
a	a	k8xC	a
erós	erós	k1gInSc1	erós
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
téma	téma	k1gNnSc4	téma
aplikace	aplikace	k1gFnSc2	aplikace
těchto	tento	k3xDgInPc2	tento
principů	princip	k1gInPc2	princip
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
např.	např.	kA	např.
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
charity	charita	k1gFnSc2	charita
apod.	apod.	kA	apod.
Druhá	druhý	k4xOgFnSc1	druhý
encyklika	encyklika	k1gFnSc1	encyklika
Spe	Spe	k1gFnSc6	Spe
salvi	salev	k1gFnSc6	salev
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
ctností	ctnost	k1gFnSc7	ctnost
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
sociální	sociální	k2eAgInSc4d1	sociální
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
vykoupení	vykoupení	k1gNnSc3	vykoupení
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
encyklika	encyklika	k1gFnSc1	encyklika
je	být	k5eAaImIp3nS	být
encyklikou	encyklika	k1gFnSc7	encyklika
sociální	sociální	k2eAgFnSc7d1	sociální
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Populorum	Populorum	k1gInSc4	Populorum
progressio	progressio	k1gMnSc1	progressio
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
základy	základ	k1gInPc7	základ
a	a	k8xC	a
správným	správný	k2eAgNnSc7d1	správné
směřováním	směřování	k1gNnSc7	směřování
lidského	lidský	k2eAgInSc2d1	lidský
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
otázkami	otázka	k1gFnPc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Sacramentum	Sacramentum	k1gNnSc1	Sacramentum
caritatis	caritatis	k1gFnSc2	caritatis
(	(	kIx(	(
<g/>
Svátost	svátost	k1gFnSc1	svátost
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
exhortace	exhortace	k1gFnSc1	exhortace
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
biskupského	biskupský	k2eAgInSc2d1	biskupský
synodu	synod	k1gInSc2	synod
o	o	k7c6	o
eucharistii	eucharistie	k1gFnSc6	eucharistie
ve	v	k7c6	v
dnech	den	k1gInPc6	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Exhortace	exhortace	k1gFnSc1	exhortace
jím	jíst	k5eAaImIp1nS	jíst
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
a	a	k8xC	a
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
encykliku	encyklika	k1gFnSc4	encyklika
Deus	Deus	k1gInSc1	Deus
caritas	caritas	k1gMnSc1	caritas
est	est	k?	est
<g/>
.	.	kIx.	.
</s>
<s>
Účel	účel	k1gInSc1	účel
exhortace	exhortace	k1gFnSc2	exhortace
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
spočívá	spočívat	k5eAaImIp3nS	spočívat
"	"	kIx"	"
<g/>
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
prohloubení	prohloubení	k1gNnSc6	prohloubení
vztahu	vztah	k1gInSc2	vztah
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
lidu	lid	k1gInSc2	lid
k	k	k7c3	k
eucharistickému	eucharistický	k2eAgNnSc3d1	eucharistické
Tajemství	tajemství	k1gNnSc3	tajemství
<g/>
,	,	kIx,	,
liturgického	liturgický	k2eAgInSc2d1	liturgický
úkonu	úkon	k1gInSc2	úkon
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
duchovního	duchovní	k2eAgInSc2d1	duchovní
kultu	kult	k1gInSc2	kult
<g/>
,	,	kIx,	,
odvozeného	odvozený	k2eAgInSc2d1	odvozený
z	z	k7c2	z
Eucharistie	eucharistie	k1gFnSc2	eucharistie
jako	jako	k8xC	jako
Svátosti	svátost	k1gFnSc2	svátost
Lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
exhortace	exhortace	k1gFnSc2	exhortace
je	být	k5eAaImIp3nS	být
inspirován	inspirován	k2eAgInSc1d1	inspirován
slovy	slovo	k1gNnPc7	slovo
Tomáše	Tomáš	k1gMnSc4	Tomáš
Akvinského	Akvinský	k2eAgMnSc4d1	Akvinský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	on	k3xPp3gNnSc4	on
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
eucharistie	eucharistie	k1gFnSc2	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
čítá	čítat	k5eAaImIp3nS	čítat
cca	cca	kA	cca
140	[number]	k4	140
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jazyce	jazyk	k1gInSc6	jazyk
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Summorum	Summorum	k1gNnSc1	Summorum
pontificum	pontificum	k1gInSc1	pontificum
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
vydal	vydat	k5eAaPmAgMnS	vydat
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
motu	moto	k1gNnSc6	moto
proprio	proprio	k6eAd1	proprio
Summorum	Summorum	k1gInSc1	Summorum
pontificum	pontificum	k1gInSc1	pontificum
-	-	kIx~	-
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
slavení	slavení	k1gNnSc4	slavení
liturgie	liturgie	k1gFnSc2	liturgie
dle	dle	k7c2	dle
misálu	misál	k1gInSc2	misál
bl.	bl.	k?	bl.
Jana	Jana	k1gFnSc1	Jana
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovanému	označovaný	k2eAgInSc3d1	označovaný
jako	jako	k8xC	jako
tridentský	tridentský	k2eAgInSc1d1	tridentský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Řádným	řádný	k2eAgInSc7d1	řádný
způsobem	způsob	k1gInSc7	způsob
slavení	slavení	k1gNnSc2	slavení
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
misál	misál	k1gInSc4	misál
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
přihlížející	přihlížející	k2eAgInSc4d1	přihlížející
k	k	k7c3	k
liturgickým	liturgický	k2eAgFnPc3d1	liturgická
změnám	změna	k1gFnPc3	změna
stanoveným	stanovený	k2eAgFnPc3d1	stanovená
druhým	druhý	k4xOgInSc7	druhý
vatikánským	vatikánský	k2eAgInSc7d1	vatikánský
koncilem	koncil	k1gInSc7	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
užívat	užívat	k5eAaImF	užívat
starší	starý	k2eAgInSc4d2	starší
misál	misál	k1gInSc4	misál
dal	dát	k5eAaPmAgMnS	dát
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
mít	mít	k5eAaImF	mít
dovolení	dovolení	k1gNnSc4	dovolení
místního	místní	k2eAgMnSc2d1	místní
ordináře	ordinář	k1gMnSc2	ordinář
-	-	kIx~	-
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Summorum	Summorum	k1gNnSc1	Summorum
pontificum	pontificum	k1gNnSc4	pontificum
dává	dávat	k5eAaImIp3nS	dávat
kněžím	kněz	k1gMnPc3	kněz
při	při	k7c6	při
soukromém	soukromý	k2eAgNnSc6d1	soukromé
slavení	slavení	k1gNnSc6	slavení
eucharistie	eucharistie	k1gFnSc2	eucharistie
v	v	k7c4	v
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
den	den	k1gInSc4	den
kromě	kromě	k7c2	kromě
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
tridua	tridu	k1gInSc2	tridu
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
způsob	způsob	k1gInSc1	způsob
slavení	slavení	k1gNnSc3	slavení
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
ráda	rád	k2eAgFnSc1d1	ráda
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
slavila	slavit	k5eAaImAgFnS	slavit
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc1	ten
farář	farář	k1gMnSc1	farář
umožnit	umožnit	k5eAaPmF	umožnit
a	a	k8xC	a
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
již	již	k9	již
žádná	žádný	k3yNgNnPc4	žádný
další	další	k2eAgNnPc4d1	další
povolení	povolení	k1gNnPc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
starší	starý	k2eAgInSc4d2	starší
rituál	rituál	k1gInSc4	rituál
při	při	k7c6	při
udílení	udílení	k1gNnSc6	udílení
svátostí	svátost	k1gFnPc2	svátost
křtu	křest	k1gInSc2	křest
<g/>
,	,	kIx,	,
biřmování	biřmování	k1gNnSc2	biřmování
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc2	manželství
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
přejí	přát	k5eAaImIp3nP	přát
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
dotyční	dotyčný	k2eAgMnPc1d1	dotyčný
a	a	k8xC	a
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
pro	pro	k7c4	pro
dobro	dobro	k1gNnSc4	dobro
duší	duše	k1gFnPc2	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
-li	i	k?	-li
však	však	k9	však
například	například	k6eAd1	například
řeholní	řeholní	k2eAgNnSc1d1	řeholní
společenství	společenství	k1gNnSc1	společenství
staršího	starý	k2eAgInSc2d2	starší
způsobu	způsob	k1gInSc2	způsob
slavení	slavený	k2eAgMnPc1d1	slavený
užívat	užívat	k5eAaImF	užívat
"	"	kIx"	"
<g/>
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
nebo	nebo	k8xC	nebo
trvale	trvale	k6eAd1	trvale
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
vyšší	vysoký	k2eAgNnSc4d2	vyšší
Představení	představení	k1gNnSc4	představení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
přijali	přijmout	k5eAaPmAgMnP	přijmout
zejména	zejména	k9	zejména
zástupci	zástupce	k1gMnPc1	zástupce
tradicionalistických	tradicionalistický	k2eAgInPc2d1	tradicionalistický
proudů	proud	k1gInPc2	proud
uvnitř	uvnitř	k7c2	uvnitř
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
,	,	kIx,	,
např.	např.	kA	např.
představený	představený	k1gMnSc1	představený
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc2	Pius
X.	X.	kA	X.
Bernard	Bernard	k1gMnSc1	Bernard
Fellay	Fellaa	k1gFnSc2	Fellaa
označil	označit	k5eAaPmAgMnS	označit
dokument	dokument	k1gInSc4	dokument
za	za	k7c4	za
"	"	kIx"	"
<g/>
dar	dar	k1gInSc4	dar
Milosti	milost	k1gFnSc2	milost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
nejenom	nejenom	k6eAd1	nejenom
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
skok	skok	k1gInSc4	skok
správným	správný	k2eAgInSc7d1	správný
směrem	směr	k1gInSc7	směr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
dokumentu	dokument	k1gInSc2	dokument
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
ozývaly	ozývat	k5eAaImAgFnP	ozývat
i	i	k9	i
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
obavu	obava	k1gFnSc4	obava
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc1	akt
nedotkne	dotknout	k5eNaPmIp3nS	dotknout
autority	autorita	k1gFnPc4	autorita
druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
či	či	k8xC	či
zda	zda	k8xS	zda
nezpůsobí	způsobit	k5eNaPmIp3nS	způsobit
zmatky	zmatek	k1gInPc4	zmatek
ve	v	k7c6	v
farnostech	farnost	k1gFnPc6	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
námitkám	námitka	k1gFnPc3	námitka
se	se	k3xPyFc4	se
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
dopise	dopis	k1gInSc6	dopis
biskupům	biskup	k1gMnPc3	biskup
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
i	i	k9	i
motivace	motivace	k1gFnPc4	motivace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ho	on	k3xPp3gMnSc4	on
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
svého	svůj	k3xOyFgInSc2	svůj
činu	čin	k1gInSc2	čin
shrnul	shrnout	k5eAaPmAgMnS	shrnout
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
interního	interní	k2eAgNnSc2d1	interní
smíření	smíření	k1gNnSc2	smíření
v	v	k7c6	v
lůně	lůno	k1gNnSc6	lůno
Církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
zraňovala	zraňovat	k5eAaImAgFnS	zraňovat
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
Tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kritických	kritický	k2eAgInPc6d1	kritický
momentech	moment	k1gInPc6	moment
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
rozdělení	rozdělení	k1gNnPc1	rozdělení
rodila	rodit	k5eAaImAgNnP	rodit
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
představitelů	představitel	k1gMnPc2	představitel
Církve	církev	k1gFnSc2	církev
učiněno	učinit	k5eAaPmNgNnS	učinit
dostatek	dostatek	k1gInSc4	dostatek
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
či	či	k8xC	či
znovu	znovu	k6eAd1	znovu
dosažení	dosažení	k1gNnSc1	dosažení
smíření	smíření	k1gNnSc2	smíření
a	a	k8xC	a
jednoty	jednota	k1gFnSc2	jednota
<g/>
;	;	kIx,	;
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
opomenutí	opomenutí	k1gNnSc4	opomenutí
Církve	církev	k1gFnSc2	církev
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
díl	díl	k1gInSc4	díl
viny	vina	k1gFnSc2	vina
na	na	k7c6	na
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
rozdělení	rozdělení	k1gNnPc1	rozdělení
mohla	moct	k5eAaImAgNnP	moct
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
nás	my	k3xPp1nPc4	my
dnes	dnes	k6eAd1	dnes
staví	stavit	k5eAaImIp3nS	stavit
před	před	k7c4	před
povinnost	povinnost	k1gFnSc4	povinnost
učinit	učinit	k5eAaImF	učinit
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
lze	lze	k6eAd1	lze
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
skutečně	skutečně	k6eAd1	skutečně
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
jednotě	jednota	k1gFnSc6	jednota
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
jednotě	jednota	k1gFnSc6	jednota
zůstat	zůstat	k5eAaPmF	zůstat
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
opět	opět	k6eAd1	opět
nalézt	nalézt	k5eAaPmF	nalézt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
listě	list	k1gInSc6	list
pak	pak	k6eAd1	pak
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
taktéž	taktéž	k?	taktéž
sám	sám	k3xTgMnSc1	sám
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c4	o
vlastní	vlastní	k2eAgFnPc4d1	vlastní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
liturgické	liturgický	k2eAgFnSc6d1	liturgická
změně	změna	k1gFnSc6	změna
"	"	kIx"	"
<g/>
svévolnými	svévolný	k2eAgFnPc7d1	svévolná
deformacemi	deformace	k1gFnPc7	deformace
Liturgie	liturgie	k1gFnSc2	liturgie
zraňovány	zraňovat	k5eAaImNgFnP	zraňovat
ty	ten	k3xDgFnPc1	ten
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
naprosto	naprosto	k6eAd1	naprosto
zakořeněny	zakořenit	k5eAaPmNgInP	zakořenit
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
Církve	církev	k1gFnSc2	církev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
židovské	židovský	k2eAgFnPc1d1	židovská
skupiny	skupina	k1gFnPc1	skupina
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
papeže	papež	k1gMnSc2	papež
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
velkopáteční	velkopáteční	k2eAgFnSc2d1	velkopáteční
liturgie	liturgie	k1gFnSc2	liturgie
dle	dle	k7c2	dle
misálu	misál	k1gInSc2	misál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
výrazy	výraz	k1gInPc1	výraz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyjádření	vyjádření	k1gNnSc1	vyjádření
o	o	k7c6	o
"	"	kIx"	"
<g/>
slepotě	slepota	k1gFnSc6	slepota
<g/>
"	"	kIx"	"
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
někteří	některý	k3yIgMnPc1	některý
židé	žid	k1gMnPc1	žid
nebyli	být	k5eNaImAgMnP	být
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
jejich	jejich	k3xOp3gInPc3	jejich
protestům	protest	k1gInPc3	protest
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
a	a	k8xC	a
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vydala	vydat	k5eAaPmAgFnS	vydat
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
dokument	dokument	k1gInSc1	dokument
potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
Benediktem	benedikt	k1gInSc7	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
obsahující	obsahující	k2eAgFnPc1d1	obsahující
"	"	kIx"	"
<g/>
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
o	o	k7c6	o
některých	některý	k3yIgInPc6	některý
aspektech	aspekt	k1gInPc6	aspekt
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
dokument	dokument	k1gInSc4	dokument
Dominus	Dominus	k1gMnSc1	Dominus
Iesus	Iesus	k1gMnSc1	Iesus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
připravoval	připravovat	k5eAaImAgMnS	připravovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
prefekta	prefekt	k1gMnSc2	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
o	o	k7c6	o
katolickém	katolický	k2eAgNnSc6d1	katolické
chápání	chápání	k1gNnSc6	chápání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgNnPc1	jaký
společenství	společenství	k1gNnPc1	společenství
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
pohledu	pohled	k1gInSc2	pohled
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
mluví	mluvit	k5eAaImIp3nS	mluvit
dokument	dokument	k1gInSc4	dokument
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jelikož	jelikož	k8xS	jelikož
tyto	tento	k3xDgFnPc1	tento
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
odloučené	odloučený	k2eAgFnPc1d1	odloučená
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pravé	pravý	k2eAgFnPc4d1	pravá
svátosti	svátost	k1gFnPc4	svátost
a	a	k8xC	a
především	především	k9	především
vlivem	vliv	k1gInSc7	vliv
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
kněžství	kněžství	k1gNnSc4	kněžství
a	a	k8xC	a
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
dosud	dosud	k6eAd1	dosud
těsně	těsně	k6eAd1	těsně
pojí	pojit	k5eAaImIp3nP	pojit
<g/>
,	,	kIx,	,
zasluhují	zasluhovat	k5eAaImIp3nP	zasluhovat
titul	titul	k1gInSc4	titul
místní	místní	k2eAgInSc4d1	místní
či	či	k8xC	či
partikulární	partikulární	k2eAgInSc4d1	partikulární
<g />
.	.	kIx.	.
</s>
<s>
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgInP	nazývat
sesterskými	sesterský	k2eAgFnPc7d1	sesterská
církvemi	církev	k1gFnPc7	církev
katolických	katolický	k2eAgFnPc2d1	katolická
partikulárních	partikulární	k2eAgFnPc2d1	partikulární
Církví	církev	k1gFnPc2	církev
<g/>
...	...	k?	...
Poněvadž	poněvadž	k8xS	poněvadž
však	však	k9	však
společenství	společenství	k1gNnSc1	společenství
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
Hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
římský	římský	k2eAgMnSc1d1	římský
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
Petrův	Petrův	k2eAgMnSc1d1	Petrův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
nějakým	nějaký	k3yIgInSc7	nějaký
vnějším	vnější	k2eAgInSc7d1	vnější
doplňkem	doplněk	k1gInSc7	doplněk
partikulární	partikulární	k2eAgFnSc2d1	partikulární
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
konstitutivních	konstitutivní	k2eAgInPc2d1	konstitutivní
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
status	status	k1gInSc1	status
těchto	tento	k3xDgFnPc2	tento
partikulárních	partikulární	k2eAgFnPc2d1	partikulární
církví	církev	k1gFnPc2	církev
<g/>
...	...	k?	...
<g/>
přesto	přesto	k8xC	přesto
určitým	určitý	k2eAgInSc7d1	určitý
nedostatkem	nedostatek	k1gInSc7	nedostatek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
reformovaných	reformovaný	k2eAgFnPc6d1	reformovaná
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
tato	tento	k3xDgNnPc1	tento
společenství	společenství	k1gNnPc1	společenství
nemají	mít	k5eNaImIp3nP	mít
apoštolskou	apoštolský	k2eAgFnSc4d1	apoštolská
posloupnost	posloupnost	k1gFnSc4	posloupnost
ve	v	k7c6	v
svátosti	svátost	k1gFnSc6	svátost
svěcení	svěcení	k1gNnSc2	svěcení
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
postrádají	postrádat	k5eAaImIp3nP	postrádat
bytostně	bytostně	k6eAd1	bytostně
konstitutivní	konstitutivní	k2eAgInSc4d1	konstitutivní
prvek	prvek	k1gInSc4	prvek
bytí	bytí	k1gNnSc2	bytí
Církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněná	zmíněný	k2eAgNnPc1d1	zmíněné
církevní	církevní	k2eAgNnPc1d1	církevní
společenství	společenství	k1gNnPc1	společenství
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
absence	absence	k1gFnSc2	absence
služebného	služebný	k2eAgNnSc2d1	služebné
kněžství	kněžství	k1gNnSc2	kněžství
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
pravou	pravý	k2eAgFnSc4d1	pravá
a	a	k8xC	a
celistvou	celistvý	k2eAgFnSc4d1	celistvá
podstatu	podstata	k1gFnSc4	podstata
eucharistického	eucharistický	k2eAgNnSc2d1	eucharistické
Tajemství	tajemství	k1gNnSc2	tajemství
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
podle	podle	k7c2	podle
katolické	katolický	k2eAgFnSc2d1	katolická
nauky	nauka	k1gFnSc2	nauka
být	být	k5eAaImF	být
nazvány	nazván	k2eAgInPc4d1	nazván
"	"	kIx"	"
<g/>
církvemi	církev	k1gFnPc7	církev
<g/>
"	"	kIx"	"
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dokument	dokument	k1gInSc1	dokument
taktéž	taktéž	k?	taktéž
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
"	"	kIx"	"
<g/>
plnou	plný	k2eAgFnSc4d1	plná
totožnost	totožnost	k1gFnSc4	totožnost
Církve	církev	k1gFnSc2	církev
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
a	a	k8xC	a
katolické	katolický	k2eAgFnSc2d1	katolická
Církve	církev	k1gFnSc2	církev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc4	dokument
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
z	z	k7c2	z
protestantských	protestantský	k2eAgInPc2d1	protestantský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
protestantští	protestantský	k2eAgMnPc1d1	protestantský
biskupové	biskup	k1gMnPc1	biskup
mluvili	mluvit	k5eAaImAgMnP	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
políčku	políčko	k1gNnSc6	políčko
pro	pro	k7c4	pro
ekumenismus	ekumenismus	k1gInSc4	ekumenismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zástupci	zástupce	k1gMnPc1	zástupce
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
jej	on	k3xPp3gMnSc4	on
chválili	chválit	k5eAaImAgMnP	chválit
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
upřímnost	upřímnost	k1gFnSc4	upřímnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
námitky	námitka	k1gFnPc4	námitka
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
následně	následně	k6eAd1	následně
kardinál	kardinál	k1gMnSc1	kardinál
Kasper	Kasper	k1gMnSc1	Kasper
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokument	dokument	k1gInSc1	dokument
neříká	říkat	k5eNaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
nebyly	být	k5eNaImAgFnP	být
církvemi	církev	k1gFnPc7	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
církvemi	církev	k1gFnPc7	církev
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
chápe	chápat	k5eAaImIp3nS	chápat
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
takovými	takový	k3xDgFnPc7	takový
církvemi	církev	k1gFnPc7	církev
sami	sám	k3xTgMnPc1	sám
být	být	k5eAaImF	být
ani	ani	k8xC	ani
nechtějí	chtít	k5eNaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
Svatý	svatý	k2eAgInSc4d1	svatý
stolec	stolec	k1gInSc4	stolec
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svoji	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
křesťany	křesťan	k1gMnPc7	křesťan
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
světovými	světový	k2eAgInPc7d1	světový
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
důležitosti	důležitost	k1gFnSc6	důležitost
a	a	k8xC	a
užitečnosti	užitečnost	k1gFnSc6	užitečnost
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
mluvil	mluvit	k5eAaImAgMnS	mluvit
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
během	během	k7c2	během
pontifikátu	pontifikát	k1gInSc2	pontifikát
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
i	i	k9	i
kontroverze	kontroverze	k1gFnSc2	kontroverze
jak	jak	k8xS	jak
mezi	mezi	k7c7	mezi
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
či	či	k8xC	či
židy	žid	k1gMnPc7	žid
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
navázal	navázat	k5eAaPmAgMnS	navázat
bližší	blízký	k2eAgInPc4d2	bližší
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Kněžského	kněžský	k2eAgNnSc2d1	kněžské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc4	Pius
X.	X.	kA	X.
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
čtyři	čtyři	k4xCgMnPc1	čtyři
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
vysvěcení	vysvěcený	k2eAgMnPc1d1	vysvěcený
bez	bez	k7c2	bez
povolení	povolení	k1gNnPc2	povolení
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
upadli	upadnout	k5eAaPmAgMnP	upadnout
dle	dle	k7c2	dle
Svatého	svatý	k1gMnSc2	svatý
stolce	stolec	k1gInPc4	stolec
tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
do	do	k7c2	do
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Představený	představený	k1gMnSc1	představený
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
mons	monsa	k1gFnPc2	monsa
<g/>
.	.	kIx.	.
</s>
<s>
Fellay	Fellaa	k1gFnPc1	Fellaa
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
kvitoval	kvitovat	k5eAaBmAgMnS	kvitovat
již	již	k6eAd1	již
zvolení	zvolení	k1gNnSc1	zvolení
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
papež	papež	k1gMnSc1	papež
tuto	tento	k3xDgFnSc4	tento
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gInSc4	jejich
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
společenství	společenství	k1gNnSc3	společenství
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
našel	najít	k5eAaPmAgInS	najít
i	i	k9	i
své	svůj	k3xOyFgFnSc2	svůj
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hanse	Hans	k1gMnSc2	Hans
Künga	Küng	k1gMnSc2	Küng
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
Ratzingerova	Ratzingerův	k2eAgMnSc4d1	Ratzingerův
kolegu	kolega	k1gMnSc4	kolega
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
odebráno	odebrán	k2eAgNnSc1d1	odebráno
právo	právo	k1gNnSc4	právo
vyučovat	vyučovat	k5eAaImF	vyučovat
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
již	již	k6eAd1	již
vydání	vydání	k1gNnSc4	vydání
dokumentu	dokument	k1gInSc2	dokument
Summorum	Summorum	k1gInSc4	Summorum
pontificum	pontificum	k1gInSc1	pontificum
a	a	k8xC	a
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
chová	chovat	k5eAaImIp3nS	chovat
k	k	k7c3	k
Marcelu	Marcel	k1gMnSc3	Marcel
Lefebvrovi	Lefebvr	k1gMnSc3	Lefebvr
sympatie	sympatie	k1gFnSc2	sympatie
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
mnoho	mnoho	k6eAd1	mnoho
katolíků	katolík	k1gMnPc2	katolík
"	"	kIx"	"
<g/>
nečeká	čekat	k5eNaImIp3nS	čekat
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
už	už	k6eAd1	už
nic	nic	k3yNnSc1	nic
dobrého	dobrý	k2eAgNnSc2d1	dobré
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
aktem	akt	k1gInSc7	akt
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
bylo	být	k5eAaImAgNnS	být
motu	moto	k1gNnSc3	moto
proprio	proprio	k6eAd1	proprio
Ecclesiae	Ecclesiae	k1gFnSc1	Ecclesiae
unitatem	unitat	k1gInSc7	unitat
vydané	vydaný	k2eAgNnSc4d1	vydané
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
převádí	převádět	k5eAaImIp3nS	převádět
Papežskou	papežský	k2eAgFnSc4d1	Papežská
komisi	komise	k1gFnSc4	komise
Ecclesia	Ecclesium	k1gNnSc2	Ecclesium
Dei	Dei	k1gFnSc2	Dei
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
a	a	k8xC	a
tradicionalisty	tradicionalista	k1gMnPc4	tradicionalista
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
Kongregaci	kongregace	k1gFnSc4	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tak	tak	k9	tak
změnilo	změnit	k5eAaPmAgNnS	změnit
i	i	k9	i
její	její	k3xOp3gNnSc1	její
personální	personální	k2eAgNnSc1d1	personální
vedení	vedení	k1gNnSc1	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
tradicionalistické	tradicionalistický	k2eAgFnPc1d1	tradicionalistická
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
Komunita	komunita	k1gFnSc1	komunita
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
Institut	institut	k1gInSc4	institut
Krista	Kristus	k1gMnSc2	Kristus
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
plně	plně	k6eAd1	plně
začleněny	začlenit	k5eAaPmNgFnP	začlenit
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
těmto	tento	k3xDgFnPc3	tento
skupinám	skupina	k1gFnPc3	skupina
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vstříc	vstříc	k6eAd1	vstříc
již	již	k6eAd1	již
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
Summorum	Summorum	k1gNnSc1	Summorum
pontificum	pontificum	k1gInSc4	pontificum
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
jako	jako	k8xC	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
motivací	motivace	k1gFnPc2	motivace
zmínil	zmínit	k5eAaPmAgMnS	zmínit
právě	právě	k9	právě
předcházení	předcházení	k1gNnSc4	předcházení
štěpení	štěpení	k1gNnSc2	štěpení
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
následně	následně	k6eAd1	následně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
kardinál	kardinál	k1gMnSc1	kardinál
Hoyos	Hoyos	k1gInSc4	Hoyos
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
motu	moto	k1gNnSc3	moto
proprio	proprio	k6eAd1	proprio
nemálo	málo	k6eNd1	málo
tradicionalistů	tradicionalista	k1gMnPc2	tradicionalista
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
společenství	společenství	k1gNnSc3	společenství
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
vícekrát	vícekrát	k6eAd1	vícekrát
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
promluvách	promluva	k1gFnPc6	promluva
výsadní	výsadní	k2eAgNnSc1d1	výsadní
postavení	postavení	k1gNnSc1	postavení
Petrova	Petrův	k2eAgMnSc2d1	Petrův
nástupce	nástupce	k1gMnSc2	nástupce
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
chápe	chápat	k5eAaImIp3nS	chápat
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
neobešlo	obešnout	k5eNaImAgNnS	obešnout
bez	bez	k7c2	bez
reakcí	reakce	k1gFnPc2	reakce
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
jiných	jiný	k2eAgFnPc2d1	jiná
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
též	též	k9	též
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
anglikánskou	anglikánský	k2eAgFnSc7d1	anglikánská
církvi	církev	k1gFnSc3	církev
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
anglikáni	anglikán	k1gMnPc1	anglikán
odhlasovali	odhlasovat	k5eAaPmAgMnP	odhlasovat
biskupské	biskupský	k2eAgNnSc4d1	Biskupské
svěcení	svěcení	k1gNnSc4	svěcení
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počin	počin	k1gInSc1	počin
taktéž	taktéž	k?	taktéž
dle	dle	k7c2	dle
kardinála	kardinál	k1gMnSc2	kardinál
Kaspera	Kasper	k1gMnSc2	Kasper
zkomplikoval	zkomplikovat	k5eAaPmAgInS	zkomplikovat
jinak	jinak	k6eAd1	jinak
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgInSc1d1	rozvíjející
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
dialog	dialog	k1gInSc1	dialog
mezi	mezi	k7c7	mezi
katolickou	katolický	k2eAgFnSc7d1	katolická
a	a	k8xC	a
anglikánskou	anglikánský	k2eAgFnSc7d1	anglikánská
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
řešila	řešit	k5eAaImAgFnS	řešit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
vlastním	vlastní	k2eAgNnSc7d1	vlastní
rozštěpením	rozštěpení	k1gNnSc7	rozštěpení
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
jejích	její	k3xOp3gInPc2	její
členů	člen	k1gInPc2	člen
dokonce	dokonce	k9	dokonce
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
ochotu	ochota	k1gFnSc4	ochota
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
též	též	k9	též
účast	účast	k1gFnSc4	účast
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
moskevského	moskevský	k2eAgMnSc2d1	moskevský
patriarchy	patriarcha	k1gMnSc2	patriarcha
Alexije	Alexije	k1gFnSc2	Alexije
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
papežský	papežský	k2eAgInSc1d1	papežský
legát	legát	k1gInSc1	legát
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
historická	historický	k2eAgFnSc1d1	historická
účast	účast	k1gFnSc1	účast
konstantinopolského	konstantinopolský	k2eAgMnSc2d1	konstantinopolský
patriarchy	patriarcha	k1gMnSc2	patriarcha
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
na	na	k7c6	na
biskupském	biskupský	k2eAgInSc6d1	biskupský
synodu	synod	k1gInSc6	synod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
promluvil	promluvit	k5eAaPmAgMnS	promluvit
zástupce	zástupce	k1gMnSc1	zástupce
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
papež	papež	k1gMnSc1	papež
sám	sám	k3xTgMnSc1	sám
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
dokumentu	dokument	k1gInSc2	dokument
katolicko-pravoslavné	katolickoravoslavný	k2eAgFnSc2d1	katolicko-pravoslavná
komise	komise	k1gFnSc2	komise
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Církevní	církevní	k2eAgNnSc1d1	církevní
společenství	společenství	k1gNnSc1	společenství
<g/>
,	,	kIx,	,
konciliarita	konciliarita	k1gFnSc1	konciliarita
a	a	k8xC	a
autorita	autorita	k1gFnSc1	autorita
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
otevírá	otevírat	k5eAaImIp3nS	otevírat
positivní	positivní	k2eAgFnPc4d1	positivní
perspektivy	perspektiva	k1gFnPc4	perspektiva
v	v	k7c6	v
reflexi	reflexe	k1gFnSc6	reflexe
nad	nad	k7c7	nad
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
primátem	primát	k1gInSc7	primát
a	a	k8xC	a
synodalitou	synodalita	k1gFnSc7	synodalita
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svoji	svůj	k3xOyFgFnSc4	svůj
promluvu	promluva	k1gFnSc4	promluva
k	k	k7c3	k
Papežské	papežský	k2eAgFnSc3d1	Papežská
radě	rada	k1gFnSc3	rada
pro	pro	k7c4	pro
jednotu	jednota	k1gFnSc4	jednota
křesťanů	křesťan	k1gMnPc2	křesťan
zakončil	zakončit	k5eAaPmAgMnS	zakončit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Drazí	drahý	k2eAgMnPc1d1	drahý
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
ekumenická	ekumenický	k2eAgFnSc1d1	ekumenická
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nás	my	k3xPp1nPc4	my
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
úsilí	úsilí	k1gNnSc3	úsilí
o	o	k7c4	o
upřímnou	upřímný	k2eAgFnSc4d1	upřímná
konfrontaci	konfrontace	k1gFnSc4	konfrontace
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
nové	nový	k2eAgFnSc2d1	nová
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
profilaci	profilace	k1gFnSc3	profilace
nových	nový	k2eAgFnPc2d1	nová
tendencí	tendence	k1gFnPc2	tendence
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
napětím	napětí	k1gNnPc3	napětí
mezi	mezi	k7c7	mezi
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
komunitami	komunita	k1gFnPc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
teologický	teologický	k2eAgInSc4d1	teologický
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
prostředí	prostředí	k1gNnSc2	prostředí
života	život	k1gInSc2	život
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
církvích	církev	k1gFnPc6	církev
a	a	k8xC	a
církevních	církevní	k2eAgFnPc6d1	církevní
komunitách	komunita	k1gFnPc6	komunita
<g/>
....	....	k?	....
ekumenismus	ekumenismus	k1gInSc4	ekumenismus
nás	my	k3xPp1nPc4	my
vybízí	vybízet	k5eAaImIp3nS	vybízet
k	k	k7c3	k
bratrské	bratrský	k2eAgFnSc3d1	bratrská
a	a	k8xC	a
velkorysé	velkorysý	k2eAgFnSc3d1	velkorysá
výměně	výměna	k1gFnSc3	výměna
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
plné	plný	k2eAgNnSc1d1	plné
společenství	společenství	k1gNnSc1	společenství
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svátostech	svátost	k1gFnPc6	svátost
a	a	k8xC	a
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
úkolem	úkol	k1gInSc7	úkol
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
celého	celý	k2eAgNnSc2d1	celé
ekumenického	ekumenický	k2eAgNnSc2d1	ekumenické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pulsujícím	pulsující	k2eAgNnSc7d1	pulsující
srdcem	srdce	k1gNnSc7	srdce
tohoto	tento	k3xDgInSc2	tento
náročného	náročný	k2eAgInSc2d1	náročný
úkolu	úkol	k1gInSc2	úkol
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
jasně	jasně	k6eAd1	jasně
formuloval	formulovat	k5eAaImAgMnS	formulovat
II	II	kA	II
<g/>
.	.	kIx.	.
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ekumenismus	ekumenismus	k1gInSc1	ekumenismus
duchovní	duchovní	k2eAgInSc1d1	duchovní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někteří	některý	k3yIgMnPc1	některý
židovští	židovský	k2eAgMnPc1d1	židovský
představitelé	představitel	k1gMnPc1	představitel
či	či	k8xC	či
organizace	organizace	k1gFnSc2	organizace
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
spokojenost	spokojenost	k1gFnSc4	spokojenost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolit	k5eAaPmNgMnS	zvolit
právě	právě	k6eAd1	právě
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sám	sám	k3xTgMnSc1	sám
prožil	prožít	k5eAaPmAgMnS	prožít
období	období	k1gNnSc4	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
se	se	k3xPyFc4	se
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
střízlivěji	střízlivě	k6eAd2	střízlivě
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
doufají	doufat	k5eAaImIp3nP	doufat
v	v	k7c4	v
navázání	navázání	k1gNnSc4	navázání
na	na	k7c4	na
pontifikáty	pontifikát	k1gInPc4	pontifikát
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	k
židům	žid	k1gMnPc3	žid
a	a	k8xC	a
k	k	k7c3	k
státu	stát	k1gInSc3	stát
Izrael	Izrael	k1gMnSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
papež	papež	k1gMnSc1	papež
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
navštívil	navštívit	k5eAaPmAgMnS	navštívit
koncentrační	koncentrační	k2eAgMnSc1d1	koncentrační
tábor	tábor	k1gMnSc1	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
u	u	k7c2	u
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
pronesl	pronést	k5eAaPmAgMnS	pronést
slova	slovo	k1gNnPc4	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tohle	tenhle	k3xDgNnSc4	tenhle
se	se	k3xPyFc4	se
nedostává	dostávat	k5eNaImIp3nS	dostávat
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
zbude	zbýt	k5eAaPmIp3nS	zbýt
jen	jen	k9	jen
ustrnulé	ustrnulý	k2eAgNnSc4d1	ustrnulé
mlčení	mlčení	k1gNnSc4	mlčení
-	-	kIx~	-
mlčení	mlčení	k1gNnSc2	mlčení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
křikem	křik	k1gInSc7	křik
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsi	být	k5eAaImIp2nS	být
mlčel	mlčet	k5eAaImAgMnS	mlčet
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k6eAd1	jak
jsi	být	k5eAaImIp2nS	být
mohl	moct	k5eAaImAgMnS	moct
toto	tento	k3xDgNnSc4	tento
všechno	všechen	k3xTgNnSc4	všechen
připustit	připustit	k5eAaPmF	připustit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
A	a	k9	a
za	za	k7c2	za
tohoto	tento	k3xDgNnSc2	tento
mlčení	mlčení	k1gNnSc2	mlčení
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nitru	nitro	k1gNnSc6	nitro
hluboce	hluboko	k6eAd1	hluboko
skláníme	sklánět	k5eAaImIp1nP	sklánět
před	před	k7c7	před
nesčetným	sčetný	k2eNgInSc7d1	nesčetný
zástupem	zástup	k1gInSc7	zástup
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
trpěli	trpět	k5eAaImAgMnP	trpět
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
mlčení	mlčení	k1gNnSc4	mlčení
se	se	k3xPyFc4	se
však	však	k9	však
později	pozdě	k6eAd2	pozdě
stává	stávat	k5eAaImIp3nS	stávat
nahlas	nahlas	k6eAd1	nahlas
vyslovenou	vyslovený	k2eAgFnSc7d1	vyslovená
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c6	o
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
,	,	kIx,	,
o	o	k7c6	o
smíření	smíření	k1gNnSc6	smíření
<g/>
,	,	kIx,	,
prosbou	prosba	k1gFnSc7	prosba
k	k	k7c3	k
živému	živý	k2eAgMnSc3d1	živý
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc4	nic
podobného	podobný	k2eAgNnSc2d1	podobné
nepřipustil	připustit	k5eNaPmAgMnS	připustit
<g/>
.	.	kIx.	.
</s>
<s>
Připomněl	připomnět	k5eAaPmAgMnS	připomnět
též	též	k9	též
slova	slovo	k1gNnSc2	slovo
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
nemohl	moct	k5eNaImAgMnS	moct
jsem	být	k5eAaImIp1nS	být
sem	sem	k6eAd1	sem
jako	jako	k9	jako
papež	papež	k1gMnSc1	papež
nepřijít	přijít	k5eNaPmF	přijít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
židy	žid	k1gMnPc7	žid
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostaly	dostat	k5eAaPmAgInP	dostat
na	na	k7c4	na
přetřes	přetřes	k1gInSc4	přetřes
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Summorum	Summorum	k1gNnSc1	Summorum
pontificum	pontificum	k1gInSc4	pontificum
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
protestovali	protestovat	k5eAaBmAgMnP	protestovat
proti	proti	k7c3	proti
obsahu	obsah	k1gInSc3	obsah
velkopáteční	velkopáteční	k2eAgFnSc2d1	velkopáteční
liturgie	liturgie	k1gFnSc2	liturgie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
kritické	kritický	k2eAgFnSc2d1	kritická
pasáže	pasáž	k1gFnSc2	pasáž
posléze	posléze	k6eAd1	posléze
nechal	nechat	k5eAaPmAgMnS	nechat
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
kontroverzním	kontroverzní	k2eAgNnSc7d1	kontroverzní
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
případu	případ	k1gInSc2	případ
případného	případný	k2eAgNnSc2d1	případné
blahořečení	blahořečení	k1gNnSc2	blahořečení
Pia	Pius	k1gMnSc2	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
papeže	papež	k1gMnSc2	papež
v	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
obviňován	obviňován	k2eAgInSc4d1	obviňován
z	z	k7c2	z
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
nacistům	nacista	k1gMnPc3	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
naopak	naopak	k6eAd1	naopak
několikrát	několikrát	k6eAd1	několikrát
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
tomuto	tento	k3xDgMnSc3	tento
papeži	papež	k1gMnSc3	papež
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
obdiv	obdiv	k1gInSc4	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgFnPc1d1	židovská
organizace	organizace	k1gFnPc1	organizace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
naopak	naopak	k6eAd1	naopak
odsoudily	odsoudit	k5eAaPmAgFnP	odsoudit
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
blahořečení	blahořečení	k1gNnSc4	blahořečení
tohoto	tento	k3xDgMnSc2	tento
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Gary	Gara	k1gFnPc1	Gara
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
Pave	Pave	k1gFnSc4	Pave
the	the	k?	the
Way	Way	k1gFnSc1	Way
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
neteologické	teologický	k2eNgFnPc4d1	teologický
překážky	překážka	k1gFnPc4	překážka
mezi	mezi	k7c7	mezi
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
taktéž	taktéž	k?	taktéž
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
případ	případ	k1gInSc1	případ
Pia	Pius	k1gMnSc2	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
rozhlas	rozhlas	k1gInSc4	rozhlas
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
onen	onen	k3xDgInSc1	onen
nepříznivý	příznivý	k2eNgInSc1d1	nepříznivý
dojem	dojem	k1gInSc1	dojem
o	o	k7c4	o
papeži	papež	k1gMnSc3	papež
Piu	Pius	k1gMnSc3	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
falešný	falešný	k2eAgInSc1d1	falešný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
pobouření	pobouření	k1gNnSc4	pobouření
uvnitř	uvnitř	k7c2	uvnitř
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
zrušení	zrušení	k1gNnSc1	zrušení
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
čtyř	čtyři	k4xCgMnPc2	čtyři
biskupů	biskup	k1gMnPc2	biskup
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc4	Pius
X.	X.	kA	X.
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Richarda	Richarda	k1gFnSc1	Richarda
Williamsona	Williamson	k1gMnSc2	Williamson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
veřejně	veřejně	k6eAd1	veřejně
pronesl	pronést	k5eAaPmAgMnS	pronést
výroky	výrok	k1gInPc4	výrok
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgFnPc6	který
popírá	popírat	k5eAaImIp3nS	popírat
obecně	obecně	k6eAd1	obecně
přijímaný	přijímaný	k2eAgInSc1d1	přijímaný
rozsah	rozsah	k1gInSc1	rozsah
holocaustu	holocaust	k1gInSc2	holocaust
či	či	k8xC	či
existenci	existence	k1gFnSc4	existence
plynových	plynový	k2eAgFnPc2d1	plynová
komor	komora	k1gFnPc2	komora
(	(	kIx(	(
<g/>
Williamsonovy	Williamsonův	k2eAgInPc1d1	Williamsonův
výroky	výrok	k1gInPc1	výrok
ovšem	ovšem	k9	ovšem
nijak	nijak	k6eAd1	nijak
nesouvisely	souviset	k5eNaImAgInP	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
exkomunikací	exkomunikace	k1gFnSc7	exkomunikace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
Vatikánem	Vatikán	k1gInSc7	Vatikán
nepovolené	povolený	k2eNgNnSc1d1	nepovolené
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
Bratrstvo	bratrstvo	k1gNnSc1	bratrstvo
následně	následně	k6eAd1	následně
tyto	tento	k3xDgInPc1	tento
výroky	výrok	k1gInPc1	výrok
odsoudily	odsoudit	k5eAaPmAgInP	odsoudit
a	a	k8xC	a
vyzvaly	vyzvat	k5eAaPmAgInP	vyzvat
Williamsona	Williamsona	k1gFnSc1	Williamsona
k	k	k7c3	k
přehodnocení	přehodnocení	k1gNnSc3	přehodnocení
svých	svůj	k3xOyFgInPc2	svůj
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
sekretariát	sekretariát	k1gInSc1	sekretariát
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
se	se	k3xPyFc4	se
v	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
prohlášení	prohlášení	k1gNnSc6	prohlášení
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
okolnosti	okolnost	k1gFnPc1	okolnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zrušení	zrušení	k1gNnSc6	zrušení
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
nebyly	být	k5eNaImAgFnP	být
Svatému	svatý	k2eAgMnSc3d1	svatý
otci	otec	k1gMnSc3	otec
známy	znám	k2eAgFnPc4d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Papeže	Papež	k1gMnSc4	Papež
naopak	naopak	k6eAd1	naopak
hájil	hájit	k5eAaImAgMnS	hájit
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
americký	americký	k2eAgMnSc1d1	americký
rabín	rabín	k1gMnSc1	rabín
Yehuda	Yehuda	k1gMnSc1	Yehuda
Levin	Levin	k1gMnSc1	Levin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
židovských	židovský	k2eAgFnPc2d1	židovská
organizací	organizace	k1gFnPc2	organizace
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
papež	papež	k1gMnSc1	papež
znovu	znovu	k6eAd1	znovu
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
své	svůj	k3xOyFgNnSc4	svůj
stanovisko	stanovisko	k1gNnSc4	stanovisko
k	k	k7c3	k
záležitosti	záležitost	k1gFnSc3	záležitost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenávist	nenávist	k1gFnSc1	nenávist
rozpoutaná	rozpoutaný	k2eAgFnSc1d1	rozpoutaná
proti	proti	k7c3	proti
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
ženám	žena	k1gFnPc3	žena
i	i	k8xC	i
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
šoa	šoa	k?	šoa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zločinem	zločin	k1gInSc7	zločin
proti	proti	k7c3	proti
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
proti	proti	k7c3	proti
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nP	držet
tradice	tradice	k1gFnPc1	tradice
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k2eAgNnSc2d1	svaté
<g/>
...	...	k?	...
<g/>
Každá	každý	k3xTgFnSc1	každý
negace	negace	k1gFnSc1	negace
nebo	nebo	k8xC	nebo
minimalizace	minimalizace	k1gFnSc1	minimalizace
tohoto	tento	k3xDgInSc2	tento
zločinu	zločin	k1gInSc2	zločin
je	být	k5eAaImIp3nS	být
netolerovatelná	tolerovatelný	k2eNgFnSc1d1	netolerovatelná
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nepřijatelná	přijatelný	k2eNgNnPc1d1	nepřijatelné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svatou	svatý	k2eAgFnSc4d1	svatá
zemi	zem	k1gFnSc4	zem
papež	papež	k1gMnSc1	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Papežovy	Papežův	k2eAgInPc1d1	Papežův
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
islámem	islám	k1gInSc7	islám
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
diskutovány	diskutovat	k5eAaImNgInP	diskutovat
především	především	k6eAd1	především
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
zástupci	zástupce	k1gMnPc1	zástupce
islámu	islám	k1gInSc2	islám
byli	být	k5eAaImAgMnP	být
pobouřeni	pobouřit	k5eAaPmNgMnP	pobouřit
obsahem	obsah	k1gInSc7	obsah
přednášky	přednáška	k1gFnSc2	přednáška
s	s	k7c7	s
názvem	název	k1gInSc7	název
Víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
rozum	rozum	k1gInSc1	rozum
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
papež	papež	k1gMnSc1	papež
přednesl	přednést	k5eAaPmAgMnS	přednést
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2006	[number]	k4	2006
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pobouření	pobouření	k1gNnSc2	pobouření
byla	být	k5eAaImAgFnS	být
papežova	papežův	k2eAgFnSc1d1	papežova
citace	citace	k1gFnSc1	citace
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ukaž	ukázat	k5eAaPmRp2nS	ukázat
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nového	nový	k2eAgNnSc2d1	nové
přinesl	přinést	k5eAaPmAgMnS	přinést
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
,	,	kIx,	,
a	a	k8xC	a
nalezneš	naleznout	k5eAaPmIp2nS	naleznout
tam	tam	k6eAd1	tam
pouze	pouze	k6eAd1	pouze
věci	věc	k1gFnPc4	věc
špatné	špatný	k2eAgFnPc4d1	špatná
a	a	k8xC	a
nelidské	lidský	k2eNgFnPc4d1	nelidská
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
příkaz	příkaz	k1gInSc4	příkaz
šířit	šířit	k5eAaImF	šířit
víru	víra	k1gFnSc4	víra
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
citaci	citace	k1gFnSc4	citace
textu	text	k1gInSc2	text
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
Manuela	Manuel	k1gMnSc2	Manuel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Palaiologa	Palaiolog	k1gMnSc4	Palaiolog
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
citaci	citace	k1gFnSc3	citace
v	v	k7c6	v
papežově	papežův	k2eAgFnSc6d1	papežova
přednášce	přednáška	k1gFnSc6	přednáška
předcházela	předcházet	k5eAaImAgFnS	předcházet
jeho	jeho	k3xOp3gNnPc4	jeho
vlastní	vlastní	k2eAgNnPc4d1	vlastní
slova	slovo	k1gNnPc4	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zastavoval	zastavovat	k5eAaImAgInS	zastavovat
u	u	k7c2	u
podrobností	podrobnost	k1gFnPc2	podrobnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
u	u	k7c2	u
rozdílu	rozdíl	k1gInSc2	rozdíl
v	v	k7c4	v
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
Knihu	kniha	k1gFnSc4	kniha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
"	"	kIx"	"
<g/>
nevěřícími	věřící	k2eNgFnPc7d1	nevěřící
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obrací	obracet	k5eAaImIp3nS	obracet
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
s	s	k7c7	s
překvapivou	překvapivý	k2eAgFnSc7d1	překvapivá
příkrostí	příkrost	k1gFnSc7	příkrost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
nepřijatelná	přijatelný	k2eNgFnSc1d1	nepřijatelná
<g/>
,	,	kIx,	,
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
společníka	společník	k1gMnSc4	společník
s	s	k7c7	s
ústřední	ústřední	k2eAgFnSc7d1	ústřední
otázkou	otázka	k1gFnSc7	otázka
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
...	...	k?	...
<g/>
"	"	kIx"	"
Na	na	k7c4	na
přednášku	přednáška	k1gFnSc4	přednáška
vzápětí	vzápětí	k6eAd1	vzápětí
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
mnoho	mnoho	k4c1	mnoho
vládních	vládní	k2eAgMnPc2d1	vládní
činitelů	činitel	k1gMnPc2	činitel
muslimských	muslimský	k2eAgFnPc2d1	muslimská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
výrok	výrok	k1gInSc4	výrok
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
a	a	k8xC	a
dokazující	dokazující	k2eAgNnSc4d1	dokazující
nepochopení	nepochopení	k1gNnSc4	nepochopení
skutečného	skutečný	k2eAgInSc2d1	skutečný
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
představitelé	představitel	k1gMnPc1	představitel
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
či	či	k8xC	či
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
papeže	papež	k1gMnPc4	papež
zastali	zastat	k5eAaPmAgMnP	zastat
a	a	k8xC	a
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
nepochopení	nepochopení	k1gNnSc3	nepochopení
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písemném	písemný	k2eAgNnSc6d1	písemné
vydání	vydání	k1gNnSc6	vydání
přednášky	přednáška	k1gFnSc2	přednáška
pak	pak	k6eAd1	pak
papež	papež	k1gMnSc1	papež
k	k	k7c3	k
zmiňovanému	zmiňovaný	k2eAgNnSc3d1	zmiňované
místu	místo	k1gNnSc3	místo
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
ještě	ještě	k9	ještě
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
citace	citace	k1gFnSc1	citace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
muslimském	muslimský	k2eAgInSc6d1	muslimský
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
vyložena	vyložen	k2eAgFnSc1d1	vyložena
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
mého	můj	k3xOp1gInSc2	můj
osobního	osobní	k2eAgInSc2d1	osobní
postoje	postoj	k1gInSc2	postoj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
rozhořčení	rozhořčení	k1gNnSc1	rozhořčení
<g/>
.	.	kIx.	.
</s>
<s>
Doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenář	čtenář	k1gMnSc1	čtenář
mého	můj	k1gMnSc2	můj
textu	text	k1gInSc2	text
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
bezprostředně	bezprostředně	k6eAd1	bezprostředně
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
mé	můj	k3xOp1gNnSc4	můj
osobní	osobní	k2eAgNnSc4d1	osobní
hodnocení	hodnocení	k1gNnSc4	hodnocení
Koránu	korán	k1gInSc2	korán
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
chovám	chovat	k5eAaImIp1nS	chovat
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
příslušející	příslušející	k2eAgFnSc3d1	příslušející
posvátné	posvátný	k2eAgFnSc3d1	posvátná
knize	kniha	k1gFnSc3	kniha
velkého	velký	k2eAgNnSc2d1	velké
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Citací	citace	k1gFnSc7	citace
textu	text	k1gInSc2	text
císaře	císař	k1gMnSc2	císař
Manuela	Manuel	k1gMnSc2	Manuel
II	II	kA	II
<g/>
.	.	kIx.	.
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
pouze	pouze	k6eAd1	pouze
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
podstatný	podstatný	k2eAgInSc4d1	podstatný
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
vírou	víra	k1gFnSc7	víra
a	a	k8xC	a
rozumem	rozum	k1gInSc7	rozum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
se	se	k3xPyFc4	se
s	s	k7c7	s
Manuelem	Manuel	k1gMnSc7	Manuel
II	II	kA	II
<g/>
.	.	kIx.	.
shoduji	shodovat	k5eAaImIp1nS	shodovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
bych	by	kYmCp1nS	by
bral	brát	k5eAaImAgMnS	brát
jeho	jeho	k3xOp3gFnSc4	jeho
polemiku	polemika	k1gFnSc4	polemika
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
význačných	význačný	k2eAgFnPc2d1	význačná
muslimských	muslimský	k2eAgFnPc2d1	muslimská
osobností	osobnost	k1gFnPc2	osobnost
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
katolicko-muslimský	katolickouslimský	k2eAgInSc1d1	katolicko-muslimský
seminář	seminář	k1gInSc1	seminář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
diskutovaly	diskutovat	k5eAaImAgFnP	diskutovat
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
bližnímu	bližní	k1gMnSc3	bližní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konalo	konat	k5eAaImAgNnS	konat
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
muslimsko-katolické	muslimskoatolický	k2eAgNnSc4d1	muslimsko-katolický
sympozium	sympozium	k1gNnSc4	sympozium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mateřské	mateřský	k2eAgFnSc2d1	mateřská
němčiny	němčina	k1gFnSc2	němčina
hovoří	hovořit	k5eAaImIp3nS	hovořit
plynně	plynně	k6eAd1	plynně
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
znalosti	znalost	k1gFnPc4	znalost
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
i	i	k9	i
četbu	četba	k1gFnSc4	četba
starověké	starověký	k2eAgFnSc2d1	starověká
řečtiny	řečtina	k1gFnSc2	řečtina
či	či	k8xC	či
biblické	biblický	k2eAgFnSc2d1	biblická
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
označuje	označovat	k5eAaImIp3nS	označovat
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
členem	člen	k1gMnSc7	člen
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
různých	různý	k2eAgFnPc2d1	různá
akademií	akademie	k1gFnPc2	akademie
jako	jako	k8xC	jako
např.	např.	kA	např.
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Academie	academia	k1gFnSc2	academia
des	des	k1gNnSc2	des
science	science	k1gFnSc2	science
morales	morales	k1gMnSc1	morales
et	et	k?	et
politiques	politiques	k1gMnSc1	politiques
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
sedm	sedm	k4xCc4	sedm
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
taktéž	taktéž	k?	taktéž
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
patronem	patron	k1gMnSc7	patron
tzv.	tzv.	kA	tzv.
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
Foundation	Foundation	k1gInSc1	Foundation
-	-	kIx~	-
charitativní	charitativní	k2eAgFnSc1d1	charitativní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získává	získávat	k5eAaImIp3nS	získávat
peníze	peníz	k1gInPc4	peníz
prodejem	prodej	k1gInSc7	prodej
papežových	papežův	k2eAgFnPc2d1	papežova
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
pojednání	pojednání	k1gNnSc1	pojednání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
podporovala	podporovat	k5eAaImAgFnS	podporovat
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
stipendia	stipendium	k1gNnSc2	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Mozarta	Mozart	k1gMnSc4	Mozart
a	a	k8xC	a
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
skladatelů	skladatel	k1gMnPc2	skladatel
je	být	k5eAaImIp3nS	být
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
velké	velký	k2eAgNnSc1d1	velké
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
papežské	papežský	k2eAgFnSc6d1	Papežská
residenci	residence	k1gFnSc6	residence
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
dalším	další	k2eAgInSc7d1	další
velkým	velký	k2eAgInSc7d1	velký
zájmem	zájem	k1gInSc7	zájem
jsou	být	k5eAaImIp3nP	být
kočky	kočka	k1gFnPc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
papežem	papež	k1gMnSc7	papež
římský	římský	k2eAgInSc1d1	římský
tisk	tisk	k1gInSc1	tisk
mylně	mylně	k6eAd1	mylně
informoval	informovat	k5eAaBmAgInS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
sebou	se	k3xPyFc7	se
do	do	k7c2	do
apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
paláce	palác	k1gInSc2	palác
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
měl	mít	k5eAaImAgMnS	mít
jako	jako	k9	jako
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
prefekta	prefekt	k1gMnSc2	prefekt
Kongregace	kongregace	k1gFnSc2	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
podával	podávat	k5eAaImAgMnS	podávat
kardinál	kardinál	k1gMnSc1	kardinál
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
třikrát	třikrát	k6eAd1	třikrát
žádost	žádost	k1gFnSc4	žádost
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
problémy	problém	k1gInPc1	problém
související	související	k2eAgInPc1d1	související
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
chtěl	chtít	k5eAaImAgInS	chtít
mít	mít	k5eAaImF	mít
rovněž	rovněž	k9	rovněž
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poslušnosti	poslušnost	k1gFnSc3	poslušnost
vůči	vůči	k7c3	vůči
papeži	papež	k1gMnSc3	papež
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
jeho	jeho	k3xOp3gNnSc4	jeho
setrvání	setrvání	k1gNnSc4	setrvání
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1991	[number]	k4	1991
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
prodělal	prodělat	k5eAaPmAgMnS	prodělat
náhlou	náhlý	k2eAgFnSc4d1	náhlá
cévní	cévní	k2eAgFnSc4d1	cévní
mozkovou	mozkový	k2eAgFnSc4d1	mozková
příhodu	příhoda	k1gFnSc4	příhoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dočasně	dočasně	k6eAd1	dočasně
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
jeho	on	k3xPp3gInSc4	on
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1992	[number]	k4	1992
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Alp	Alpy	k1gFnPc2	Alpy
upadl	upadnout	k5eAaPmAgMnS	upadnout
a	a	k8xC	a
udeřil	udeřit	k5eAaPmAgMnS	udeřit
se	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
o	o	k7c4	o
radiátor	radiátor	k1gInSc4	radiátor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
okolnosti	okolnost	k1gFnPc1	okolnost
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgInPc1d1	znám
při	při	k7c6	při
konkláve	konkláve	k1gNnSc6	konkláve
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
Vatikán	Vatikán	k1gInSc1	Vatikán
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
později	pozdě	k6eAd2	pozdě
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
slabší	slabý	k2eAgFnSc4d2	slabší
mozkovou	mozkový	k2eAgFnSc4d1	mozková
příhodu	příhoda	k1gFnSc4	příhoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neoznámil	oznámit	k5eNaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tak	tak	k9	tak
přesně	přesně	k6eAd1	přesně
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nejspíše	nejspíše	k9	nejspíše
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2003	[number]	k4	2003
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
kardinál	kardinál	k1gMnSc1	kardinál
Philippe	Philipp	k1gInSc5	Philipp
Barbarin	Barbarin	k1gInSc1	Barbarin
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
první	první	k4xOgFnSc2	první
příhody	příhoda	k1gFnSc2	příhoda
trpí	trpět	k5eAaImIp3nP	trpět
Ratzinger	Ratzinger	k1gInSc4	Ratzinger
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
srdcem	srdce	k1gNnSc7	srdce
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgFnPc3	který
musí	muset	k5eAaImIp3nS	muset
užívat	užívat	k5eAaImF	užívat
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
byl	být	k5eAaImAgInS	být
Ratzinger	Ratzinger	k1gInSc1	Ratzinger
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
lepším	dobrý	k2eAgInSc6d2	lepší
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
79	[number]	k4	79
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
vypluly	vyplout	k5eAaPmAgFnP	vyplout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
z	z	k7c2	z
neověřených	ověřený	k2eNgInPc2d1	neověřený
zdrojů	zdroj	k1gInPc2	zdroj
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
operaci	operace	k1gFnSc4	operace
jako	jako	k8xS	jako
přípravu	příprava	k1gFnSc4	příprava
pro	pro	k7c4	pro
případnou	případný	k2eAgFnSc4d1	případná
aplikaci	aplikace	k1gFnSc4	aplikace
bypassu	bypass	k1gInSc2	bypass
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
bronchitida	bronchitida	k1gFnSc1	bronchitida
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
papež	papež	k1gMnSc1	papež
prodělal	prodělat	k5eAaPmAgMnS	prodělat
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
nápor	nápor	k1gInSc4	nápor
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
se	se	k3xPyFc4	se
také	také	k9	také
papež	papež	k1gMnSc1	papež
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
opustil	opustit	k5eAaPmAgInS	opustit
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
napsal	napsat	k5eAaBmAgMnS	napsat
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
odborných	odborný	k2eAgMnPc2d1	odborný
teologických	teologický	k2eAgMnPc2d1	teologický
<g/>
,	,	kIx,	,
děl	dělit	k5eAaImRp2nS	dělit
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
taktéž	taktéž	k?	taktéž
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
psaných	psaný	k2eAgNnPc2d1	psané
děl	dělo	k1gNnPc2	dělo
vycházejí	vycházet	k5eAaImIp3nP	vycházet
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
i	i	k8xC	i
sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
výběry	výběr	k1gInPc4	výběr
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
homilií	homilie	k1gFnPc2	homilie
<g/>
,	,	kIx,	,
přednášek	přednáška	k1gFnPc2	přednáška
či	či	k8xC	či
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
středečních	středeční	k2eAgFnPc2d1	středeční
katechezí	katecheze	k1gFnPc2	katecheze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přednáší	přednášet	k5eAaImIp3nP	přednášet
během	během	k7c2	během
generálních	generální	k2eAgFnPc2d1	generální
audiencí	audience	k1gFnPc2	audience
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Nazaretský	nazaretský	k2eAgMnSc1d1	nazaretský
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Eschatologie	eschatologie	k1gFnSc1	eschatologie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Duch	duch	k1gMnSc1	duch
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7364	[number]	k4	7364
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
Benedikta	Benedikt	k1gMnSc2	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
:	:	kIx,	:
její	její	k3xOp3gInPc1	její
základy	základ	k1gInPc1	základ
dnes	dnes	k6eAd1	dnes
a	a	k8xC	a
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
801	[number]	k4	801
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85947	[number]	k4	85947
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Hledět	hledět	k5eAaImF	hledět
na	na	k7c4	na
probodeného	probodený	k2eAgMnSc4d1	probodený
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnPc1	hodnota
a	a	k8xC	a
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
jako	jako	k8xS	jako
společenství	společenství	k1gNnSc1	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ZVON	zvon	k1gInSc1	zvon
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7113	[number]	k4	7113
<g/>
-	-	kIx~	-
<g/>
102	[number]	k4	102
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnSc1	naděje
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Scriptum	Scriptum	k1gNnSc1	Scriptum
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
;	;	kIx,	;
SCHÖNBORN	SCHÖNBORN	kA	SCHÖNBORN
<g/>
,	,	kIx,	,
Christoph	Christoph	k1gInSc1	Christoph
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
KATECHISMU	katechismus	k1gInSc2	katechismus
KATOLICKÉ	katolický	k2eAgFnSc2d1	katolická
CÍRKVE	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nové	Nové	k2eAgNnSc1d1	Nové
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901542	[number]	k4	901542
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SEEWALD	SEEWALD	kA	SEEWALD
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
kardinál	kardinál	k1gMnSc1	kardinál
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
:	:	kIx,	:
Křesťanství	křesťanství	k1gNnSc1	křesťanství
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7367	[number]	k4	7367
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
SEEWALD	SEEWALD	kA	SEEWALD
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Ratzinger	Ratzinger	k1gMnSc1	Ratzinger
<g/>
:	:	kIx,	:
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7364	[number]	k4	7364
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
MESSORI	MESSORI	kA	MESSORI
<g/>
,	,	kIx,	,
Vittorio	Vittorio	k6eAd1	Vittorio
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víře	víra	k1gFnSc6	víra
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Caritas	Caritas	k1gInSc1	Caritas
in	in	k?	in
veritate	veritat	k1gInSc5	veritat
(	(	kIx(	(
<g/>
Láska	láska	k1gFnSc1	láska
v	v	k7c6	v
pravdě	pravda	k1gFnSc6	pravda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vydána	vydán	k2eAgFnSc1d1	vydána
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
Spe	Spe	k1gFnPc2	Spe
salvi	salvit	k5eAaPmRp2nS	salvit
(	(	kIx(	(
<g/>
Spaseni	spasen	k2eAgMnPc1d1	spasen
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vydána	vydán	k2eAgFnSc1d1	vydána
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
Deus	Deus	k1gInSc1	Deus
caritas	caritas	k1gInSc4	caritas
est	est	k?	est
(	(	kIx(	(
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vydána	vydán	k2eAgFnSc1d1	vydána
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
papežských	papežský	k2eAgInPc2d1	papežský
dokumentů	dokument	k1gInPc2	dokument
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
a	a	k8xC	a
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
o	o	k7c4	o
eucharistii	eucharistie	k1gFnSc4	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
327	[number]	k4	327
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Pavlovi	Pavel	k1gMnSc6	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paulínky	Paulínek	k1gInPc1	Paulínek
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86949	[number]	k4	86949
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Otcové	otec	k1gMnPc1	otec
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
263	[number]	k4	263
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
počátcích	počátek	k1gInPc6	počátek
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paulínky	Paulínek	k1gInPc1	Paulínek
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
naději	naděje	k1gFnSc6	naděje
a	a	k8xC	a
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paulínky	Paulínek	k1gInPc1	Paulínek
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
promluvy	promluva	k1gFnPc1	promluva
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Přiď	Přidit	k5eAaPmRp2nS	Přidit
Duchu	duch	k1gMnSc5	duch
svatý	svatý	k1gMnSc5	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
:	:	kIx,	:
Cos	cos	kA	cos
Jesu	Jesus	k1gInSc2	Jesus
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
RATZINGER	RATZINGER	kA	RATZINGER
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
Koloseu	Koloseum	k1gNnSc6	Koloseum
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
WATTS	WATTS	kA	WATTS
<g/>
,	,	kIx,	,
Greg	Greg	k1gInSc1	Greg
<g/>
.	.	kIx.	.
</s>
<s>
Dělník	dělník	k1gMnSc1	dělník
na	na	k7c6	na
vinici	vinice	k1gFnSc6	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7195	[number]	k4	7195
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
67	[number]	k4	67
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
ANONYM	anonym	k1gInSc1	anonym
<g/>
.	.	kIx.	.
</s>
<s>
Anti	Anti	k1gNnSc1	Anti
Ratzinger	Ratzingra	k1gFnPc2	Ratzingra
<g/>
.	.	kIx.	.
</s>
<s>
Protipapežský	protipapežský	k2eAgInSc1d1	protipapežský
pamflet	pamflet	k1gInSc1	pamflet
<g/>
.	.	kIx.	.
</s>
<s>
Všeň	Všeň	k1gFnSc1	Všeň
:	:	kIx,	:
Grimmus	Grimmus	k1gMnSc1	Grimmus
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902831	[number]	k4	902831
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
