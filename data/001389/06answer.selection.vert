<s>
Platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Pt	Pt	k1gFnSc1	Pt
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Platinum	Platinum	k1gInSc1	Platinum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
mimořádně	mimořádně	k6eAd1	mimořádně
odolný	odolný	k2eAgInSc1d1	odolný
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
