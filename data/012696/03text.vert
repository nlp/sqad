<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Food	Food	k1gInSc1	Food
and	and	k?	and
Agriculture	Agricultur	k1gMnSc5	Agricultur
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
agentura	agentura	k1gFnSc1	agentura
OSN	OSN	kA	OSN
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
zajištění	zajištění	k1gNnSc1	zajištění
dostatku	dostatek	k1gInSc2	dostatek
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
těmto	tento	k3xDgFnPc3	tento
zemím	zem	k1gFnPc3	zem
zejména	zejména	k9	zejména
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
<g/>
,	,	kIx,	,
technologické	technologický	k2eAgFnSc3d1	technologická
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc3d1	finanční
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnSc3d1	vzdělávací
stránce	stránka	k1gFnSc3	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jejich	jejich	k3xOp3gFnPc4	jejich
soběstačnosti	soběstačnost	k1gFnPc4	soběstačnost
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
tak	tak	k9	tak
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
závislého	závislý	k2eAgNnSc2d1	závislé
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc1	její
snahy	snaha	k1gFnPc1	snaha
směřovány	směřován	k2eAgFnPc1d1	směřována
jednak	jednak	k8xC	jednak
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
produktivity	produktivita	k1gFnSc2	produktivita
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
produkce	produkce	k1gFnSc1	produkce
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
nezávadným	závadný	k2eNgInPc3d1	nezávadný
zdrojům	zdroj	k1gInPc3	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
trvalou	trvalý	k2eAgFnSc7d1	trvalá
udržitelností	udržitelnost	k1gFnSc7	udržitelnost
hospodaření	hospodaření	k1gNnSc2	hospodaření
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zavádět	zavádět	k5eAaImF	zavádět
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
erozi	eroze	k1gFnSc4	eroze
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
další	další	k2eAgInPc4d1	další
zábory	zábor	k1gInPc4	zábor
zejména	zejména	k9	zejména
lesní	lesní	k2eAgFnSc2d1	lesní
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
běžně	běžně	k6eAd1	běžně
dochází	docházet	k5eAaImIp3nS	docházet
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
používání	používání	k1gNnSc2	používání
způsobů	způsob	k1gInPc2	způsob
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyčerpávají	vyčerpávat	k5eAaImIp3nP	vyčerpávat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
ve	v	k7c6	v
FAO	FAO	kA	FAO
==	==	k?	==
</s>
</p>
<p>
<s>
Bývalé	bývalý	k2eAgNnSc1d1	bývalé
Československo	Československo	k1gNnSc1	Československo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
též	též	k9	též
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
umístění	umístění	k1gNnSc2	umístění
centrály	centrála	k1gFnSc2	centrála
FAO	FAO	kA	FAO
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stává	stávat	k5eAaImIp3nS	stávat
členem	člen	k1gMnSc7	člen
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
založení	založení	k1gNnSc6	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
3	[number]	k4	3
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
však	však	k9	však
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
díky	díky	k7c3	díky
neúměrným	úměrný	k2eNgInPc3d1	neúměrný
členským	členský	k2eAgInPc3d1	členský
poplatkům	poplatek	k1gInPc3	poplatek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
až	až	k6eAd1	až
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
Československo	Československo	k1gNnSc1	Československo
opětně	opětně	k6eAd1	opětně
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
do	do	k7c2	do
organizace	organizace	k1gFnSc2	organizace
podruhé	podruhé	k6eAd1	podruhé
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
následně	následně	k6eAd1	následně
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nepřetržitým	přetržitý	k2eNgMnSc7d1	nepřetržitý
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zemi	zem	k1gFnSc3	zem
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
několik	několik	k4yIc1	několik
konkurzů	konkurz	k1gInPc2	konkurz
jak	jak	k8xS	jak
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
lesnictví	lesnictví	k1gNnSc2	lesnictví
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
lesnictví	lesnictví	k1gNnSc2	lesnictví
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
projekt	projekt	k1gInSc1	projekt
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
lesnického	lesnický	k2eAgInSc2d1	lesnický
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Chittagongu	Chittagong	k1gInSc6	Chittagong
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Pákistánu	Pákistán	k1gInSc6	Pákistán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
kvůli	kvůli	k7c3	kvůli
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
následné	následný	k2eAgFnSc3d1	následná
válce	válka	k1gFnSc3	válka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
znepřátelenými	znepřátelený	k2eAgFnPc7d1	znepřátelená
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
FAO	FAO	kA	FAO
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
FAO	FAO	kA	FAO
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
jednak	jednak	k8xC	jednak
domem	dům	k1gInSc7	dům
OSN	OSN	kA	OSN
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
knihovnou	knihovna	k1gFnSc7	knihovna
Ústavu	ústav	k1gInSc2	ústav
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
instituce	instituce	k1gFnPc1	instituce
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
tištěné	tištěný	k2eAgInPc4d1	tištěný
materiály	materiál	k1gInPc4	materiál
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
monografií	monografie	k1gFnPc2	monografie
<g/>
,	,	kIx,	,
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
výročních	výroční	k2eAgFnPc2d1	výroční
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
UZEI	UZEI	kA	UZEI
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
depozitní	depozitní	k2eAgFnSc7d1	depozitní
knihovnou	knihovna	k1gFnSc7	knihovna
FAO	FAO	kA	FAO
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adresa	adresa	k1gFnSc1	adresa
==	==	k?	==
</s>
</p>
<p>
<s>
Vialle	Vialle	k6eAd1	Vialle
delle	delle	k6eAd1	delle
Terme	term	k1gInSc5	term
di	di	k?	di
Caracalla	Caracalla	k1gMnSc1	Caracalla
<g/>
,	,	kIx,	,
00100	[number]	k4	00100
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Charitativní	charitativní	k2eAgFnSc2d1	charitativní
organizace	organizace	k1gFnSc2	organizace
brojí	brojit	k5eAaImIp3nS	brojit
proti	proti	k7c3	proti
DPH	DPH	kA	DPH
z	z	k7c2	z
darovaných	darovaný	k2eAgFnPc2d1	darovaná
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
dárcovství	dárcovství	k1gNnSc2	dárcovství
se	se	k3xPyFc4	se
plýtvá	plýtvat	k5eAaImIp3nS	plýtvat
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
