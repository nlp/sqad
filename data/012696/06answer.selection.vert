<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Food	Food	k1gInSc1	Food
and	and	k?	and
Agriculture	Agricultur	k1gMnSc5	Agricultur
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
agentura	agentura	k1gFnSc1	agentura
OSN	OSN	kA	OSN
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
