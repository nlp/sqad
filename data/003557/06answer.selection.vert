<s>
Běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c4	na
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgInPc6d1	kompatibilní
systémech	systém	k1gInPc6	systém
rodiny	rodina	k1gFnSc2	rodina
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
(	(	kIx(	(
<g/>
IA-	IA-	k1gFnSc1	IA-
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DEC	DEC	kA	DEC
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
,	,	kIx,	,
SUN	sun	k1gInSc1	sun
UltraSPARC	UltraSPARC	k1gFnSc1	UltraSPARC
<g/>
,	,	kIx,	,
Itanium	Itanium	k1gNnSc1	Itanium
(	(	kIx(	(
<g/>
IA-	IA-	k1gFnSc1	IA-
<g/>
64	[number]	k4	64
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
PowerPC	PowerPC	k1gFnSc1	PowerPC
<g/>
,	,	kIx,	,
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
MIPS	MIPS	kA	MIPS
<g/>
,	,	kIx,	,
NEC	NEC	kA	NEC
PC-98	PC-98	k1gFnSc1	PC-98
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Xbox	Xbox	k1gInSc1	Xbox
<g/>
.	.	kIx.	.
</s>
