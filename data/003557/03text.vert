<s>
FreeBSD	FreeBSD	k?	FreeBSD
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
unixový	unixový	k2eAgInSc1d1	unixový
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
BSD	BSD	kA	BSD
verze	verze	k1gFnSc1	verze
Unixu	Unix	k1gInSc2	Unix
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c4	na
IBM	IBM	kA	IBM
PC	PC	kA	PC
kompatibilních	kompatibilní	k2eAgInPc6d1	kompatibilní
systémech	systém	k1gInPc6	systém
rodiny	rodina	k1gFnSc2	rodina
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
(	(	kIx(	(
<g/>
IA-	IA-	k1gFnSc1	IA-
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DEC	DEC	kA	DEC
Alpha	Alpha	k1gFnSc1	Alpha
<g/>
,	,	kIx,	,
SUN	sun	k1gInSc1	sun
UltraSPARC	UltraSPARC	k1gFnSc1	UltraSPARC
<g/>
,	,	kIx,	,
Itanium	Itanium	k1gNnSc1	Itanium
(	(	kIx(	(
<g/>
IA-	IA-	k1gFnSc1	IA-
<g/>
64	[number]	k4	64
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
PowerPC	PowerPC	k1gFnSc1	PowerPC
<g/>
,	,	kIx,	,
ARM	ARM	kA	ARM
<g/>
,	,	kIx,	,
MIPS	MIPS	kA	MIPS
<g/>
,	,	kIx,	,
NEC	NEC	kA	NEC
PC-98	PC-98	k1gFnSc1	PC-98
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Xbox	Xbox	k1gInSc1	Xbox
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
dalších	další	k2eAgFnPc2d1	další
architektur	architektura	k1gFnPc2	architektura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjen	k2eAgInSc1d1	vyvíjen
jako	jako	k8xC	jako
kompletní	kompletní	k2eAgInSc1d1	kompletní
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
ovladače	ovladač	k1gInPc1	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
uživatelské	uživatelský	k2eAgFnPc1d1	Uživatelská
utility	utilita	k1gFnPc1	utilita
jako	jako	k9	jako
například	například	k6eAd1	například
shell	shella	k1gFnPc2	shella
jsou	být	k5eAaImIp3nP	být
vyvíjeny	vyvíjet	k5eAaImNgInP	vyvíjet
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
stromu	strom	k1gInSc6	strom
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
verzí	verze	k1gFnPc2	verze
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
(	(	kIx(	(
<g/>
CVS	CVS	kA	CVS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
Linuxu	linux	k1gInSc2	linux
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
každý	každý	k3xTgInSc1	každý
program	program	k1gInSc1	program
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
jinou	jiný	k2eAgFnSc7d1	jiná
skupinou	skupina	k1gFnSc7	skupina
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
vydány	vydat	k5eAaPmNgInP	vydat
jako	jako	k8xS	jako
kompletní	kompletní	k2eAgInSc1d1	kompletní
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
jinými	jiný	k2eAgMnPc7d1	jiný
vývojáři	vývojář	k1gMnPc7	vývojář
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
Linuxových	linuxový	k2eAgFnPc2d1	linuxová
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
a	a	k8xC	a
robustní	robustní	k2eAgInSc4d1	robustní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
a	a	k8xC	a
z	z	k7c2	z
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
hlásí	hlásit	k5eAaImIp3nP	hlásit
uptime	uptimat	k5eAaPmIp3nS	uptimat
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
běží	běžet	k5eAaImIp3nS	běžet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
svobodných	svobodný	k2eAgInPc2d1	svobodný
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
50	[number]	k4	50
web	web	k1gInSc4	web
serverů	server	k1gInPc2	server
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
uptimem	uptimo	k1gNnSc7	uptimo
(	(	kIx(	(
<g/>
uptime	uptimat	k5eAaPmIp3nS	uptimat
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
Linuxu	linux	k1gInSc2	linux
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vypočítán	vypočítán	k2eAgInSc1d1	vypočítán
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnPc1	společnost
NetCraft	NetCraft	k1gInSc4	NetCraft
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
převzetím	převzetí	k1gNnSc7	převzetí
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
386BSD	[number]	k4	386BSD
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nebyl	být	k5eNaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
–	–	k?	–
díky	díky	k7c3	díky
obavám	obava	k1gFnPc3	obava
o	o	k7c4	o
legalitu	legalita	k1gFnSc4	legalita
všech	všecek	k3xTgInPc2	všecek
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
386BSD	[number]	k4	386BSD
a	a	k8xC	a
následující	následující	k2eAgFnSc4d1	následující
soudní	soudní	k2eAgFnSc4d1	soudní
při	pře	k1gFnSc4	pře
mezi	mezi	k7c7	mezi
Novellem	Novell	k1gMnSc7	Novell
(	(	kIx(	(
<g/>
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
vlastníkem	vlastník	k1gMnSc7	vlastník
Copyrightu	copyright	k1gInSc2	copyright
UNIX	UNIX	kA	UNIX
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kalifornskou	kalifornský	k2eAgFnSc7d1	kalifornská
Univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
ve	v	k7c6	v
FreeBSD	FreeBSD	k1gFnSc6	FreeBSD
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přepsání	přepsání	k1gNnSc3	přepsání
většiny	většina	k1gFnSc2	většina
systému	systém	k1gInSc2	systém
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
2.0	[number]	k4	2.0
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
verze	verze	k1gFnSc2	verze
4.4	[number]	k4	4.4
<g/>
BSD-Lite	BSD-Lit	k1gInSc5	BSD-Lit
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
manuál	manuál	k1gInSc1	manuál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
historických	historický	k2eAgFnPc2d1	historická
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
zrození	zrození	k1gNnSc6	zrození
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
zlepšením	zlepšení	k1gNnSc7	zlepšení
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
2.0	[number]	k4	2.0
bylo	být	k5eAaImAgNnS	být
obnovení	obnovení	k1gNnSc1	obnovení
originálního	originální	k2eAgNnSc2d1	originální
Mach	macha	k1gFnPc2	macha
systému	systém	k1gInSc2	systém
virtuální	virtuální	k2eAgFnSc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
optimalizován	optimalizovat	k5eAaBmNgInS	optimalizovat
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
pod	pod	k7c7	pod
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zatížením	zatížení	k1gNnSc7	zatížení
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc1	vytvoření
systému	systém	k1gInSc2	systém
FreeBSD	FreeBSD	k1gMnPc2	FreeBSD
portů	port	k1gInPc2	port
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
učinil	učinit	k5eAaPmAgInS	učinit
stahování	stahování	k1gNnSc4	stahování
<g/>
,	,	kIx,	,
sestavení	sestavení	k1gNnSc4	sestavení
a	a	k8xC	a
instalaci	instalace	k1gFnSc4	instalace
softwaru	software	k1gInSc2	software
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
3.0	[number]	k4	3.0
přineslo	přinést	k5eAaPmAgNnS	přinést
mnoho	mnoho	k4c4	mnoho
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
ELF	elf	k1gMnSc1	elf
formát	formát	k1gInSc4	formát
binárek	binárka	k1gFnPc2	binárka
<g/>
,	,	kIx,	,
započetí	započetí	k1gNnSc2	započetí
podpory	podpora	k1gFnSc2	podpora
SMP	SMP	kA	SMP
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
64	[number]	k4	64
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
Alpha	Alph	k1gMnSc4	Alph
platformy	platforma	k1gFnSc2	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
byla	být	k5eAaImAgFnS	být
větev	větev	k1gFnSc1	větev
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
několikrát	několikrát	k6eAd1	několikrát
kritizována	kritizován	k2eAgFnSc1d1	kritizována
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c1	mnoho
změn	změna	k1gFnPc2	změna
nebylo	být	k5eNaImAgNnS	být
evidentně	evidentně	k6eAd1	evidentně
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
a	a	k8xC	a
postihly	postihnout	k5eAaPmAgFnP	postihnout
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
potřebný	potřebný	k2eAgInSc1d1	potřebný
krok	krok	k1gInSc1	krok
pro	pro	k7c4	pro
vyvinutí	vyvinutí	k1gNnSc4	vyvinutí
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaImF	stát
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
větví	větev	k1gFnSc7	větev
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
X.	X.	kA	X.
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
použilo	použít	k5eAaPmAgNnS	použít
jako	jako	k9	jako
své	svůj	k3xOyFgNnSc4	svůj
logo	logo	k1gNnSc4	logo
FreeBSD	FreeBSD	k1gMnSc2	FreeBSD
démona	démon	k1gMnSc2	démon
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
logo	logo	k1gNnSc4	logo
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
soutěž	soutěž	k1gFnSc1	soutěž
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
návrh	návrh	k1gInSc4	návrh
Antona	Anton	k1gMnSc2	Anton
K.	K.	kA	K.
Gurala	Gural	k1gMnSc2	Gural
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xC	jako
nové	nový	k2eAgFnSc2d1	nová
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
logo	logo	k1gNnSc4	logo
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
démon	démon	k1gMnSc1	démon
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
maskotem	maskot	k1gInSc7	maskot
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
a	a	k8xC	a
finální	finální	k2eAgNnSc1d1	finální
vydání	vydání	k1gNnSc1	vydání
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
5-STABLE	[number]	k4	5-STABLE
je	být	k5eAaImIp3nS	být
5.5	[number]	k4	5.5
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
spravují	spravovat	k5eAaImIp3nP	spravovat
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc4	dva
simultánně	simultánně	k6eAd1	simultánně
vyvíjené	vyvíjený	k2eAgFnPc1d1	vyvíjená
větve	větev	k1gFnPc1	větev
<g/>
.	.	kIx.	.
-STABLE	-STABLE	k?	-STABLE
větev	větev	k1gFnSc1	větev
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
je	být	k5eAaImIp3nS	být
vytvářena	vytvářet	k5eAaImNgFnS	vytvářet
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
hlavní	hlavní	k2eAgNnSc4d1	hlavní
číslo	číslo	k1gNnSc4	číslo
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
vydání	vydání	k1gNnSc2	vydání
vychází	vycházet	k5eAaImIp3nS	vycházet
každých	každý	k3xTgNnPc2	každý
4	[number]	k4	4
–	–	k?	–
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
4-STABLE	[number]	k4	4-STABLE
vydání	vydání	k1gNnSc4	vydání
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
je	být	k5eAaImIp3nS	být
4.11	[number]	k4	4.11
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc4d1	poslední
z	z	k7c2	z
vydání	vydání	k1gNnSc2	vydání
4	[number]	k4	4
<g/>
-STABLE	-STABLE	k?	-STABLE
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
5-STABLE	[number]	k4	5-STABLE
vydání	vydání	k1gNnSc6	vydání
bylo	být	k5eAaImAgNnS	být
5.3	[number]	k4	5.3
(	(	kIx(	(
<g/>
5.0	[number]	k4	5.0
až	až	k9	až
5.2	[number]	k4	5.2
<g/>
.1	.1	k4	.1
byly	být	k5eAaImAgFnP	být
vyjmuty	vyjmout	k5eAaPmNgInP	vyjmout
z	z	k7c2	z
-CURRENT	-CURRENT	k?	-CURRENT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
6-STABLE	[number]	k4	6-STABLE
vydání	vydání	k1gNnSc6	vydání
bylo	být	k5eAaImAgNnS	být
6.0	[number]	k4	6.0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývojové	vývojový	k2eAgFnSc6d1	vývojová
větvi	větev	k1gFnSc6	větev
-CURRENT	-CURRENT	k?	-CURRENT
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
7.0	[number]	k4	7.0
<g/>
-CURRENT	-CURRENT	k?	-CURRENT
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
agresivní	agresivní	k2eAgFnPc4d1	agresivní
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kernelu	kernel	k1gInSc2	kernel
a	a	k8xC	a
userspace	userspace	k1gFnSc2	userspace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vlastnost	vlastnost	k1gFnSc4	vlastnost
dostatečně	dostatečně	k6eAd1	dostatečně
STABLE	STABLE	kA	STABLE
a	a	k8xC	a
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
eventuálně	eventuálně	k6eAd1	eventuálně
zpětně	zpětně	k6eAd1	zpětně
portována	portován	k2eAgFnSc1d1	portován
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
MFC	MFC	kA	MFC
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Merge	Merge	k1gNnSc1	Merge
from	froma	k1gFnPc2	froma
CURRENT	CURRENT	kA	CURRENT
<g/>
"	"	kIx"	"
ve	v	k7c6	v
slangu	slang	k1gInSc6	slang
vývojářů	vývojář	k1gMnPc2	vývojář
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
<g/>
)	)	kIx)	)
do	do	k7c2	do
-STABLE	-STABLE	k?	-STABLE
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Vývojový	vývojový	k2eAgInSc1d1	vývojový
model	model	k1gInSc1	model
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
je	být	k5eAaImIp3nS	být
podrobně	podrobně	k6eAd1	podrobně
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Niklase	Niklas	k1gInSc6	Niklas
Saerse	Saerse	k1gFnSc2	Saerse
A	a	k9	a
project	project	k1gInSc1	project
model	modla	k1gFnPc2	modla
for	forum	k1gNnPc2	forum
the	the	k?	the
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
Project	Project	k1gMnSc1	Project
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
rozdílem	rozdíl	k1gInSc7	rozdíl
ve	v	k7c6	v
FreeBSD	FreeBSD	k1gFnSc6	FreeBSD
5	[number]	k4	5
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
nízkoúrovňovém	nízkoúrovňový	k2eAgInSc6d1	nízkoúrovňový
mechanismu	mechanismus	k1gInSc6	mechanismus
kernelových	kernelův	k2eAgInPc2d1	kernelův
zámků	zámek	k1gInPc2	zámek
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
lepší	dobrý	k2eAgFnSc2d2	lepší
podpory	podpora	k1gFnSc2	podpora
symetrického	symetrický	k2eAgInSc2d1	symetrický
multiprocessingu	multiprocessing	k1gInSc2	multiprocessing
(	(	kIx(	(
<g/>
SMP	SMP	kA	SMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvolňující	uvolňující	k2eAgInSc4d1	uvolňující
kernel	kernel	k1gInSc4	kernel
z	z	k7c2	z
MP	MP	kA	MP
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Giant	Giant	k1gMnSc1	Giant
Lock	Lock	k1gMnSc1	Lock
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spouštět	spouštět	k5eAaImF	spouštět
současně	současně	k6eAd1	současně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
proces	proces	k1gInSc1	proces
v	v	k7c4	v
kernel	kernel	k1gInSc4	kernel
módu	mód	k1gInSc2	mód
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
hlavní	hlavní	k2eAgFnPc1d1	hlavní
změny	změna	k1gFnPc1	změna
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
m	m	kA	m
<g/>
:	:	kIx,	:
<g/>
n	n	k0	n
řešení	řešení	k1gNnSc4	řešení
vláken	vlákna	k1gFnPc2	vlákna
zvané	zvaný	k2eAgFnSc2d1	zvaná
KSE	KSE	kA	KSE
(	(	kIx(	(
<g/>
Kernel	kernel	k1gInSc1	kernel
Scheduled	Scheduled	k1gMnSc1	Scheduled
Entities	Entities	k1gMnSc1	Entities
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
defaultní	defaultní	k2eAgFnSc7d1	defaultní
knihovnou	knihovna	k1gFnSc7	knihovna
vláken	vlákna	k1gFnPc2	vlákna
(	(	kIx(	(
<g/>
pthreads	pthreads	k1gInSc1	pthreads
<g/>
)	)	kIx)	)
počínaje	počínaje	k7c7	počínaje
verzí	verze	k1gFnSc7	verze
5.3	[number]	k4	5.3
(	(	kIx(	(
<g/>
první	první	k4xOgMnPc1	první
5-STABLE	[number]	k4	5-STABLE
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terminologie	terminologie	k1gFnSc1	terminologie
m	m	kA	m
<g/>
:	:	kIx,	:
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
kde	kde	k9	kde
m	m	kA	m
a	a	k8xC	a
n	n	k0	n
jsou	být	k5eAaImIp3nP	být
malá	malý	k2eAgNnPc4d1	malé
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
m	m	kA	m
vláknům	vlákno	k1gNnPc3	vlákno
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
prostoru	prostor	k1gInSc6	prostor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
n	n	k0	n
vláken	vlákno	k1gNnPc2	vlákno
v	v	k7c6	v
kernelovém	kernelový	k2eAgInSc6d1	kernelový
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Watson	Watson	k1gMnSc1	Watson
odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
projekt	projekt	k1gInSc4	projekt
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
přidání	přidání	k1gNnSc2	přidání
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
funkcionality	funkcionalita	k1gFnSc2	funkcionalita
do	do	k7c2	do
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřitelný	rozšiřitelný	k2eAgInSc1d1	rozšiřitelný
framework	framework	k1gInSc1	framework
mandatorní	mandatorní	k2eAgFnSc2d1	mandatorní
kontroly	kontrola	k1gFnSc2	kontrola
přístupu	přístup	k1gInSc2	přístup
(	(	kIx(	(
<g/>
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
MAC	Mac	kA	Mac
framework	framework	k1gInSc1	framework
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Access	Access	k1gInSc1	Access
Control	Controla	k1gFnPc2	Controla
Listy	lista	k1gFnSc2	lista
(	(	kIx(	(
<g/>
ACL	ACL	kA	ACL
<g/>
)	)	kIx)	)
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
UFS2	UFS2	k1gFnSc2	UFS2
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
funkcionalita	funkcionalita	k1gFnSc1	funkcionalita
TrustedBSD	TrustedBSD	k1gFnSc4	TrustedBSD
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
integrována	integrovat	k5eAaBmNgFnS	integrovat
i	i	k9	i
do	do	k7c2	do
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
OpenBSD	OpenBSD	k1gFnSc2	OpenBSD
a	a	k8xC	a
NetBSD	NetBSD	k1gFnSc2	NetBSD
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
také	také	k9	také
významně	významně	k6eAd1	významně
změnilo	změnit	k5eAaPmAgNnS	změnit
vrstvu	vrstva	k1gFnSc4	vrstva
blokového	blokový	k2eAgInSc2d1	blokový
vstupu	vstup	k1gInSc2	vstup
<g/>
/	/	kIx~	/
<g/>
výstupu	výstup	k1gInSc2	výstup
představením	představení	k1gNnSc7	představení
GEOM	GEOM	kA	GEOM
modulárního	modulární	k2eAgInSc2d1	modulární
transformačního	transformační	k2eAgInSc2d1	transformační
frameworku	frameworek	k1gInSc2	frameworek
diskových	diskový	k2eAgMnPc2d1	diskový
I	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
O	o	k7c6	o
požadavků	požadavek	k1gInPc2	požadavek
přispěného	přispěný	k2eAgInSc2d1	přispěný
Poulem-Henningem	Poulem-Henning	k1gInSc7	Poulem-Henning
Kampem	kamp	k1gInSc7	kamp
<g/>
.	.	kIx.	.
</s>
<s>
GEOM	GEOM	kA	GEOM
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
vytváření	vytváření	k1gNnSc4	vytváření
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
funkcionality	funkcionalita	k1gFnSc2	funkcionalita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
zrcadlení	zrcadlení	k1gNnSc1	zrcadlení
(	(	kIx(	(
<g/>
gmirror	gmirror	k1gInSc1	gmirror
<g/>
)	)	kIx)	)
a	a	k8xC	a
šifrování	šifrování	k1gNnSc1	šifrování
(	(	kIx(	(
<g/>
gbde	gbde	k1gInSc1	gbde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
vydání	vydání	k1gNnSc1	vydání
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
5.4	[number]	k4	5.4
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
pověst	pověst	k1gFnSc4	pověst
větve	větev	k1gFnSc2	větev
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
jako	jako	k8xC	jako
vysoce	vysoce	k6eAd1	vysoce
stabilního	stabilní	k2eAgInSc2d1	stabilní
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
sloužícího	sloužící	k2eAgNnSc2d1	sloužící
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
6.1	[number]	k4	6.1
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
a	a	k8xC	a
7.0	[number]	k4	7.0
<g/>
-RELEASE	-RELEASE	k?	-RELEASE
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
verze	verze	k1gFnPc1	verze
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
na	na	k7c6	na
SMP	SMP	kA	SMP
a	a	k8xC	a
optimalizacích	optimalizace	k1gFnPc6	optimalizace
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
802.11	[number]	k4	802.11
funkcionality	funkcionalita	k1gFnSc2	funkcionalita
a	a	k8xC	a
auditování	auditování	k1gNnSc2	auditování
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
událostí	událost	k1gFnPc2	událost
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgInSc1d1	primární
klady	klad	k1gInPc7	klad
tohoto	tento	k3xDgNnSc2	tento
vydání	vydání	k1gNnSc2	vydání
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
odstranění	odstranění	k1gNnSc4	odstranění
"	"	kIx"	"
<g/>
Giant	Giant	k1gMnSc1	Giant
lock	lock	k1gMnSc1	lock
<g/>
"	"	kIx"	"
z	z	k7c2	z
VFS	VFS	kA	VFS
<g/>
,	,	kIx,	,
přidání	přidání	k1gNnSc4	přidání
knihovny	knihovna	k1gFnSc2	knihovna
libthr	libthr	k1gMnSc1	libthr
implementující	implementující	k2eAgMnSc1d1	implementující
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
threading	threading	k1gInSc4	threading
a	a	k8xC	a
přidání	přidání	k1gNnSc4	přidání
BSM	BSM	kA	BSM
zvaného	zvaný	k2eAgMnSc4d1	zvaný
OpenBSM	OpenBSM	k1gMnSc4	OpenBSM
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
TrustedBSD	TrustedBSD	k1gMnSc3	TrustedBSD
projektem	projekt	k1gInSc7	projekt
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
implementaci	implementace	k1gFnSc6	implementace
BSM	BSM	kA	BSM
z	z	k7c2	z
Open	Openo	k1gNnPc2	Openo
Source	Source	k1gMnSc2	Source
Darwina	Darwin	k1gMnSc2	Darwin
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
pod	pod	k7c7	pod
BSD	BSD	kA	BSD
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeho	jeho	k3xOp3gInPc4	jeho
sesterské	sesterský	k2eAgInPc4d1	sesterský
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
je	být	k5eAaImIp3nS	být
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
šířen	šířit	k5eAaImNgMnS	šířit
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
různých	různý	k2eAgFnPc2d1	různá
licencí	licence	k1gFnPc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
kód	kód	k1gInSc1	kód
kernelu	kernel	k1gInSc2	kernel
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
nově	nově	k6eAd1	nově
vytvářeného	vytvářený	k2eAgInSc2d1	vytvářený
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
vydávána	vydávat	k5eAaImNgFnS	vydávat
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
dvouklauzulové	dvouklauzulový	k2eAgFnSc2d1	dvouklauzulový
BSD	BSD	kA	BSD
licence	licence	k1gFnSc2	licence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
každému	každý	k3xTgMnSc3	každý
použít	použít	k5eAaPmF	použít
a	a	k8xC	a
redistribuovat	redistribuovat	k5eAaBmF	redistribuovat
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
části	část	k1gFnPc4	část
pod	pod	k7c7	pod
GPL	GPL	kA	GPL
<g/>
,	,	kIx,	,
LGPL	LGPL	kA	LGPL
<g/>
,	,	kIx,	,
ISC	ISC	kA	ISC
<g/>
,	,	kIx,	,
pivní	pivní	k2eAgInSc1d1	pivní
licencí	licence	k1gFnSc7	licence
<g/>
,	,	kIx,	,
tříklauzulovou	tříklauzulův	k2eAgFnSc7d1	tříklauzulův
BSD	BSD	kA	BSD
licencí	licence	k1gFnSc7	licence
a	a	k8xC	a
čtyřklauzulovou	čtyřklauzulův	k2eAgFnSc7d1	čtyřklauzulův
BSD	BSD	kA	BSD
licencí	licence	k1gFnSc7	licence
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
binární	binární	k2eAgInPc1d1	binární
BLOBy	BLOBy	k1gInPc1	BLOBy
pro	pro	k7c4	pro
specifickou	specifický	k2eAgFnSc4d1	specifická
funkcionalitu	funkcionalita	k1gFnSc4	funkcionalita
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Atheros	Atherosa	k1gFnPc2	Atherosa
HAL	hala	k1gFnPc2	hala
pro	pro	k7c4	pro
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
funkcionalitu	funkcionalita	k1gFnSc4	funkcionalita
a	a	k8xC	a
výhradně	výhradně	k6eAd1	výhradně
binární	binární	k2eAgInSc1d1	binární
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
AAC	AAC	kA	AAC
RAIDy	raid	k1gInPc4	raid
Adaptecu	Adapteca	k1gFnSc4	Adapteca
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
binární	binární	k2eAgFnSc4d1	binární
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
dalšími	další	k2eAgInPc7d1	další
Unixovými	unixový	k2eAgInPc7d1	unixový
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
včetně	včetně	k7c2	včetně
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
zdůvodnuje	zdůvodnovat	k5eAaPmIp3nS	zdůvodnovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
schopné	schopný	k2eAgNnSc1d1	schopné
spouštět	spouštět	k5eAaImF	spouštět
aplikace	aplikace	k1gFnPc4	aplikace
vyvíjené	vyvíjený	k2eAgFnPc4d1	vyvíjená
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
distribuovány	distribuovat	k5eAaBmNgInP	distribuovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
binární	binární	k2eAgFnSc6d1	binární
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
proto	proto	k8xC	proto
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
portovány	portovat	k5eAaPmNgInP	portovat
do	do	k7c2	do
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
bez	bez	k7c2	bez
vůle	vůle	k1gFnSc2	vůle
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zdrojovým	zdrojový	k2eAgInSc7d1	zdrojový
kódem	kód	k1gInSc7	kód
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
především	především	k6eAd1	především
komerčních	komerční	k2eAgFnPc2d1	komerční
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stručnosti	stručnost	k1gFnSc6	stručnost
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
spouštět	spouštět	k5eAaImF	spouštět
většinu	většina	k1gFnSc4	většina
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
distribuovány	distribuován	k2eAgMnPc4d1	distribuován
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
Linuxové	linuxový	k2eAgFnPc1d1	linuxová
binárky	binárka	k1gFnPc1	binárka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
množstvím	množství	k1gNnSc7	množství
nativních	nativní	k2eAgFnPc2d1	nativní
aplikací	aplikace	k1gFnPc2	aplikace
dostupných	dostupný	k2eAgFnPc2d1	dostupná
pro	pro	k7c4	pro
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
pomocí	pomocí	k7c2	pomocí
kolekce	kolekce	k1gFnSc2	kolekce
portů	port	k1gInPc2	port
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
používané	používaný	k2eAgFnPc1d1	používaná
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
Linuxem	linux	k1gInSc7	linux
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Linuxovou	linuxový	k2eAgFnSc4d1	linuxová
verzi	verze	k1gFnSc4	verze
Adobe	Adobe	kA	Adobe
Acrobatu	Acrobata	k1gFnSc4	Acrobata
<g/>
,	,	kIx,	,
RealPlayeru	RealPlayera	k1gFnSc4	RealPlayera
<g/>
,	,	kIx,	,
VMwaru	VMwara	k1gFnSc4	VMwara
<g/>
,	,	kIx,	,
Oraclu	Oracla	k1gFnSc4	Oracla
<g/>
,	,	kIx,	,
Mathematicy	Mathematicy	k1gInPc4	Mathematicy
<g/>
,	,	kIx,	,
Matlabu	Matlaba	k1gFnSc4	Matlaba
<g/>
,	,	kIx,	,
WordPerfectu	WordPerfecta	k1gFnSc4	WordPerfecta
<g/>
,	,	kIx,	,
Skypu	Skypa	k1gFnSc4	Skypa
<g/>
,	,	kIx,	,
Dooma	Dooma	k1gFnSc1	Dooma
3	[number]	k4	3
<g/>
,	,	kIx,	,
Quaka	Quaka	k1gFnSc1	Quaka
4	[number]	k4	4
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
Unreal	Unreal	k1gMnSc1	Unreal
Tournament	Tournament	k1gMnSc1	Tournament
<g/>
,	,	kIx,	,
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Firefox	Firefox	k1gInSc1	Firefox
atd.	atd.	kA	atd.
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
ztráta	ztráta	k1gFnSc1	ztráta
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
stála	stát	k5eAaImAgFnS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
při	při	k7c6	při
spuštění	spuštění	k1gNnSc6	spuštění
Linuxových	linuxový	k2eAgFnPc2d1	linuxová
binárek	binárka	k1gFnPc2	binárka
oproti	oproti	k7c3	oproti
nativním	nativní	k2eAgInPc3d1	nativní
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
binárkám	binárka	k1gFnPc3	binárka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
pracují	pracovat	k5eAaImIp3nP	pracovat
bezproblémově	bezproblémově	k6eAd1	bezproblémově
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
není	být	k5eNaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
některé	některý	k3yIgFnPc1	některý
Linuxové	linuxový	k2eAgFnPc1d1	linuxová
binárky	binárka	k1gFnPc1	binárka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
či	či	k8xC	či
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
funkčností	funkčnost	k1gFnSc7	funkčnost
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
podporována	podporován	k2eAgNnPc1d1	podporováno
pouze	pouze	k6eAd1	pouze
systémová	systémový	k2eAgNnPc1d1	systémové
volání	volání	k1gNnPc1	volání
Linuxového	linuxový	k2eAgInSc2d1	linuxový
kernelu	kernel	k1gInSc2	kernel
verze	verze	k1gFnSc2	verze
2.6	[number]	k4	2.6
<g/>
.16	.16	k4	.16
a	a	k8xC	a
jen	jen	k9	jen
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Google	Google	k1gFnSc1	Google
Summer	Summer	k1gInSc1	Summer
of	of	k?	of
Code	Code	k1gInSc1	Code
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
akceptován	akceptovat	k5eAaBmNgInS	akceptovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
mající	mající	k2eAgInSc1d1	mající
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
aktualizovat	aktualizovat	k5eAaBmF	aktualizovat
vrstvu	vrstva	k1gFnSc4	vrstva
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
a	a	k8xC	a
chybějící	chybějící	k2eAgNnPc1d1	chybějící
systémová	systémový	k2eAgNnPc1d1	systémové
volání	volání	k1gNnPc1	volání
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
7.0	[number]	k4	7.0
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
podporuje	podporovat	k5eAaImIp3nS	podporovat
Linuxovou	linuxový	k2eAgFnSc4d1	linuxová
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
2.6	[number]	k4	2.6
<g/>
.16	.16	k4	.16
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
novejší	nový	k2eAgInSc4d2	novější
Linuxove	Linuxov	k1gInSc5	Linuxov
distribuce	distribuce	k1gFnSc2	distribuce
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
Linux	linux	k1gInSc4	linux
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
aplikací	aplikace	k1gFnPc2	aplikace
funguje	fungovat	k5eAaImIp3nS	fungovat
vrstva	vrstva	k1gFnSc1	vrstva
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
Linuxem	linux	k1gInSc7	linux
korektně	korektně	k6eAd1	korektně
<g/>
;	;	kIx,	;
aplikace	aplikace	k1gFnSc1	aplikace
jako	jako	k9	jako
nmrpipe	nmrpipat	k5eAaPmIp3nS	nmrpipat
<g/>
,	,	kIx,	,
ccp	ccp	k?	ccp
<g/>
,	,	kIx,	,
Mathematica	Mathematica	k1gMnSc1	Mathematica
a	a	k8xC	a
Matlab	Matlab	k1gMnSc1	Matlab
pracují	pracovat	k5eAaImIp3nP	pracovat
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
projektů	projekt	k1gInPc2	projekt
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c4	na
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
<g/>
,	,	kIx,	,
vestavěnými	vestavěný	k2eAgNnPc7d1	vestavěné
zařízeními	zařízení	k1gNnPc7	zařízení
jako	jako	k8xC	jako
například	například	k6eAd1	například
routery	router	k1gInPc4	router
společnosti	společnost	k1gFnSc2	společnost
Juniper	Junipra	k1gFnPc2	Junipra
Networks	Networks	k1gInSc1	Networks
a	a	k8xC	a
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
použitým	použitý	k2eAgInSc7d1	použitý
ve	v	k7c6	v
firewallech	firewall	k1gInPc6	firewall
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
počínaje	počínaje	k7c7	počínaje
a	a	k8xC	a
částmi	část	k1gFnPc7	část
jiných	jiný	k2eAgInPc2d1	jiný
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
včetně	včetně	k7c2	včetně
Linuxu	linux	k1gInSc2	linux
a	a	k8xC	a
RTOS	RTOS	kA	RTOS
VxWorks	VxWorks	k1gInSc1	VxWorks
konče	konče	k7c7	konče
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc1	jádro
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
silně	silně	k6eAd1	silně
těží	těžet	k5eAaImIp3nS	těžet
z	z	k7c2	z
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jeho	on	k3xPp3gInSc2	on
virtuálního	virtuální	k2eAgInSc2d1	virtuální
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
síťového	síťový	k2eAgInSc2d1	síťový
zásobníku	zásobník	k1gInSc2	zásobník
a	a	k8xC	a
komponent	komponenta	k1gFnPc2	komponenta
jeho	on	k3xPp3gInSc2	on
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
integraci	integrace	k1gFnSc6	integrace
nového	nový	k2eAgInSc2d1	nový
kódu	kód	k1gInSc2	kód
z	z	k7c2	z
a	a	k8xC	a
přispívání	přispívání	k1gNnSc6	přispívání
nových	nový	k2eAgFnPc2d1	nová
změn	změna	k1gFnPc2	změna
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Open	Open	k1gMnSc1	Open
Source	Source	k1gMnSc1	Source
Open	Open	k1gMnSc1	Open
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
odvozený	odvozený	k2eAgMnSc1d1	odvozený
z	z	k7c2	z
kódové	kódový	k2eAgFnSc2d1	kódová
základny	základna	k1gFnSc2	základna
Applu	Appl	k1gInSc2	Appl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
oddělený	oddělený	k2eAgInSc4d1	oddělený
projekt	projekt	k1gInSc4	projekt
rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k6eAd1	mnoho
kódu	kód	k1gInSc3	kód
z	z	k7c2	z
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
forknutých	forknutý	k2eAgInPc2d1	forknutý
z	z	k7c2	z
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
FreeBSD	FreeBSD	k1gMnPc4	FreeBSD
včetně	včetně	k7c2	včetně
PC-BSD	PC-BSD	k1gFnPc2	PC-BSD
a	a	k8xC	a
DesktopBSD	DesktopBSD	k1gFnPc2	DesktopBSD
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
změny	změna	k1gFnPc4	změna
zacílené	zacílený	k2eAgFnPc4d1	zacílená
na	na	k7c4	na
domácí	domácí	k2eAgMnPc4d1	domácí
uživatele	uživatel	k1gMnPc4	uživatel
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc4d1	pracovní
stanice	stanice	k1gFnPc4	stanice
<g/>
;	;	kIx,	;
distribuce	distribuce	k1gFnSc1	distribuce
FreeSBIE	FreeSBIE	k1gFnSc1	FreeSBIE
a	a	k8xC	a
Live	Live	k1gFnSc1	Live
CD	CD	kA	CD
Frenzy	Frenza	k1gFnSc2	Frenza
<g/>
;	;	kIx,	;
zapouzdřené	zapouzdřený	k2eAgFnSc2d1	zapouzdřená
firewally	firewalla	k1gFnSc2	firewalla
m	m	kA	m
<g/>
0	[number]	k4	0
<g/>
n	n	k0	n
<g/>
0	[number]	k4	0
<g/>
wall	walla	k1gFnPc2	walla
a	a	k8xC	a
pfSense	pfSense	k1gFnSc2	pfSense
<g/>
;	;	kIx,	;
a	a	k8xC	a
DragonFly	DragonFla	k1gFnSc2	DragonFla
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
fork	fork	k1gMnSc1	fork
z	z	k7c2	z
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
4.8	[number]	k4	4.8
zaměřující	zaměřující	k2eAgFnSc4d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
strategii	strategie	k1gFnSc4	strategie
synchronizace	synchronizace	k1gFnSc2	synchronizace
více	hodně	k6eAd2	hodně
procesorů	procesor	k1gInPc2	procesor
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
FreeBSD	FreeBSD	k1gFnSc6	FreeBSD
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
některých	některý	k3yIgFnPc2	některý
mikrokernelových	mikrokernelův	k2eAgFnPc2d1	mikrokernelův
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
započatý	započatý	k2eAgInSc4d1	započatý
Robertem	Robert	k1gMnSc7	Robert
Watsonem	Watson	k1gMnSc7	Watson
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sadu	sada	k1gFnSc4	sada
rozšíření	rozšíření	k1gNnSc2	rozšíření
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
implementovat	implementovat	k5eAaImF	implementovat
koncepty	koncept	k1gInPc7	koncept
ze	z	k7c2	z
společných	společný	k2eAgNnPc2d1	společné
kritérií	kritérion	k1gNnPc2	kritérion
pro	pro	k7c4	pro
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
oranžové	oranžový	k2eAgFnSc2d1	oranžová
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
projektu	projekt	k1gInSc2	projekt
stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
rozšíření	rozšíření	k1gNnPc2	rozšíření
bylo	být	k5eAaImAgNnS	být
integrováno	integrovat	k5eAaBmNgNnS	integrovat
do	do	k7c2	do
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
aktuální	aktuální	k2eAgFnPc4d1	aktuální
vývojové	vývojový	k2eAgFnPc4d1	vývojová
větve	větev	k1gFnPc4	větev
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
x.	x.	k?	x.
Hlavními	hlavní	k2eAgInPc7d1	hlavní
cíli	cíl	k1gInPc7	cíl
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
Access	Access	k1gInSc4	Access
Control	Controla	k1gFnPc2	Controla
Listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
auditování	auditování	k1gNnSc6	auditování
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
...	...	k?	...
a	a	k8xC	a
mandatorní	mandatorní	k2eAgFnSc3d1	mandatorní
kontrole	kontrola	k1gFnSc3	kontrola
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
i	i	k9	i
port	port	k1gInSc1	port
Flask	Flask	k1gInSc1	Flask
<g/>
/	/	kIx~	/
<g/>
TE	TE	kA	TE
implementace	implementace	k1gFnSc2	implementace
NSA	NSA	kA	NSA
v	v	k7c6	v
SELinuxu	SELinux	k1gInSc6	SELinux
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vývoj	vývoj	k1gInSc1	vývoj
OpenBSM	OpenBSM	k1gFnSc2	OpenBSM
–	–	k?	–
Open	Open	k1gMnSc1	Open
Source	Source	k1gMnSc2	Source
implementace	implementace	k1gFnSc2	implementace
API	API	kA	API
Basic	Basic	kA	Basic
Security	Securita	k1gFnPc4	Securita
Modulů	modul	k1gInPc2	modul
(	(	kIx(	(
<g/>
BSM	BSM	kA	BSM
<g/>
)	)	kIx)	)
a	a	k8xC	a
souborového	souborový	k2eAgInSc2d1	souborový
formátu	formát	k1gInSc2	formát
pro	pro	k7c4	pro
audit	audit	k1gInSc4	audit
logy	log	k1gInPc7	log
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
systém	systém	k1gInSc4	systém
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
auditu	audit	k1gInSc2	audit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bude	být	k5eAaImBp3nS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
jako	jako	k8xC	jako
část	část	k1gFnSc1	část
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
6.2	[number]	k4	6.2
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
komponent	komponenta	k1gFnPc2	komponenta
TrustedBSD	TrustedBSD	k1gMnSc2	TrustedBSD
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
eventuálně	eventuálně	k6eAd1	eventuálně
přidána	přidat	k5eAaPmNgFnS	přidat
do	do	k7c2	do
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gInSc7	jejich
jediným	jediný	k2eAgInSc7d1	jediný
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
<g/>
,	,	kIx,	,
najde	najít	k5eAaPmIp3nS	najít
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
OpenBSD	OpenBSD	k1gMnSc2	OpenBSD
a	a	k8xC	a
Darwina	Darwin	k1gMnSc2	Darwin
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
Computer	computer	k1gInSc1	computer
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Baldwin	Baldwin	k1gMnSc1	Baldwin
Alan	Alan	k1gMnSc1	Alan
L.	L.	kA	L.
Cox	Cox	k1gMnSc1	Cox
Bruce	Bruce	k1gMnSc1	Bruce
Evans	Evansa	k1gFnPc2	Evansa
David	David	k1gMnSc1	David
Greenman	Greenman	k1gMnSc1	Greenman
Jordan	Jordan	k1gMnSc1	Jordan
Hubbard	Hubbard	k1gMnSc1	Hubbard
(	(	kIx(	(
<g/>
Spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
<g/>
)	)	kIx)	)
Poul-Henning	Poul-Henning	k1gInSc1	Poul-Henning
Kamp	Kampa	k1gFnPc2	Kampa
Sam	Sam	k1gMnSc1	Sam
Leffler	Leffler	k1gMnSc1	Leffler
Scott	Scott	k1gMnSc1	Scott
Long	Long	k1gMnSc1	Long
Warner	Warner	k1gMnSc1	Warner
Losh	Losh	k1gMnSc1	Losh
Marshall	Marshall	k1gMnSc1	Marshall
Kirk	Kirk	k1gMnSc1	Kirk
McKusick	McKusick	k1gMnSc1	McKusick
Marcel	Marcel	k1gMnSc1	Marcel
Moolenaar	Moolenaar	k1gMnSc1	Moolenaar
George	Georg	k1gMnSc2	Georg
V.	V.	kA	V.
Neville-Neil	Neville-Neil	k1gMnSc1	Neville-Neil
David	David	k1gMnSc1	David
E.	E.	kA	E.
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k2eAgMnSc1d1	Brien
Bill	Bill	k1gMnSc1	Bill
Paul	Paul	k1gMnSc1	Paul
Robert	Robert	k1gMnSc1	Robert
Watson	Watson	k1gMnSc1	Watson
Peter	Peter	k1gMnSc1	Peter
Wemm	Wemm	k1gMnSc1	Wemm
Matt	Matt	k2eAgInSc4d1	Matt
Dillon	Dillon	k1gInSc4	Dillon
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nyní	nyní	k6eAd1	nyní
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
DragonFly	DragonFla	k1gFnPc4	DragonFla
BSD	BSD	kA	BSD
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Dyson	Dyson	k1gMnSc1	Dyson
Rodney	Rodnea	k1gFnSc2	Rodnea
Grimes	Grimes	k1gMnSc1	Grimes
(	(	kIx(	(
<g/>
Spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Smith	Smith	k1gMnSc1	Smith
Nate	Nate	k1gFnPc2	Nate
Williams	Williams	k1gInSc1	Williams
(	(	kIx(	(
<g/>
Spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
)	)	kIx)	)
Free	Fre	k1gInSc2	Fre
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
FSF	FSF	kA	FSF
<g/>
)	)	kIx)	)
–	–	k?	–
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
nadace	nadace	k1gFnSc1	nadace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
Projekt	projekt	k1gInSc4	projekt
<g />
.	.	kIx.	.
</s>
<s>
GNU	gnu	k1gNnSc1	gnu
Projekt	projekt	k1gInSc1	projekt
GNU	gnu	k1gMnSc1	gnu
–	–	k?	–
projekt	projekt	k1gInSc1	projekt
původně	původně	k6eAd1	původně
Richarda	Richard	k1gMnSc2	Richard
Stallmana	Stallman	k1gMnSc2	Stallman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vyvinout	vyvinout	k5eAaPmF	vyvinout
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
a	a	k8xC	a
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
–	–	k?	–
GNU	gnu	k1gNnSc1	gnu
GNU	gnu	k1gNnPc2	gnu
GPL	GPL	kA	GPL
–	–	k?	–
licence	licence	k1gFnSc2	licence
napsané	napsaný	k2eAgFnSc2d1	napsaná
Richardem	Richard	k1gMnSc7	Richard
Stallmanem	Stallman	k1gMnSc7	Stallman
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
k	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
cílů	cíl	k1gInPc2	cíl
Projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gMnSc1	gnu
GNU	gnu	k1gMnSc1	gnu
Hurd	hurda	k1gFnPc2	hurda
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
mikrojádře	mikrojádra	k1gFnSc6	mikrojádra
GNU	gnu	k1gNnSc2	gnu
Mach	macha	k1gFnPc2	macha
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
Projektem	projekt	k1gInSc7	projekt
GNU	gnu	k1gMnSc1	gnu
GNU	gnu	k1gMnSc1	gnu
Mach	Mach	k1gMnSc1	Mach
–	–	k?	–
jádro	jádro	k1gNnSc4	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
GNU	gnu	k1gNnSc2	gnu
Hurd	hurda	k1gFnPc2	hurda
<g/>
;	;	kIx,	;
mikrojádro	mikrojádro	k6eAd1	mikrojádro
GNU	gnu	k1gNnSc1	gnu
Hurd	hurda	k1gFnPc2	hurda
NG	NG	kA	NG
–	–	k?	–
svobodný	svobodný	k2eAgInSc4d1	svobodný
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
mikrojádře	mikrojádra	k1gFnSc6	mikrojádra
L4	L4	k1gFnSc1	L4
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
Projektem	projekt	k1gInSc7	projekt
GNU	gnu	k1gMnSc1	gnu
L4	L4	k1gMnSc1	L4
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
GNU	gnu	k1gNnSc2	gnu
Hurd	hurda	k1gFnPc2	hurda
NG	NG	kA	NG
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
mikrojádro	mikrojádro	k6eAd1	mikrojádro
navržené	navržený	k2eAgNnSc1d1	navržené
a	a	k8xC	a
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
vědcem	vědec	k1gMnSc7	vědec
Jochenem	Jochen	k1gMnSc7	Jochen
Liedtkem	Liedtek	k1gMnSc7	Liedtek
Linux	Linux	kA	Linux
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
svobodného	svobodný	k2eAgInSc2d1	svobodný
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
vyvíjené	vyvíjený	k2eAgInPc1d1	vyvíjený
Linux	linux	k1gInSc4	linux
Foundation	Foundation	k1gInSc1	Foundation
<g/>
;	;	kIx,	;
modulární	modulární	k2eAgNnSc1d1	modulární
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
Linux-libre	Linuxibr	k1gInSc5	Linux-libr
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
svobodného	svobodný	k2eAgInSc2d1	svobodný
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
vyvíjené	vyvíjený	k2eAgFnSc2d1	vyvíjená
dcerou	dcera	k1gFnSc7	dcera
FSF	FSF	kA	FSF
(	(	kIx(	(
<g/>
FSFLA	FSFLA	kA	FSFLA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fork	fork	k1gInSc1	fork
Linux	linux	k1gInSc1	linux
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
modulární	modulární	k2eAgNnSc1d1	modulární
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
Berkeley	Berkelea	k1gFnSc2	Berkelea
Software	software	k1gInSc1	software
Distribution	Distribution	k1gInSc1	Distribution
–	–	k?	–
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
při	pře	k1gFnSc3	pře
University	universita	k1gFnSc2	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
licenci	licence	k1gFnSc4	licence
BSD	BSD	kA	BSD
a	a	k8xC	a
používala	používat	k5eAaImAgFnS	používat
pro	pro	k7c4	pro
práce	práce	k1gFnPc4	práce
nad	nad	k7c7	nad
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
BSD	BSD	kA	BSD
Unix	Unix	k1gInSc1	Unix
<g/>
.	.	kIx.	.
</s>
<s>
BSD	BSD	kA	BSD
licence	licence	k1gFnPc1	licence
–	–	k?	–
licence	licence	k1gFnSc1	licence
organizace	organizace	k1gFnSc2	organizace
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používala	používat	k5eAaImAgFnS	používat
pro	pro	k7c4	pro
BSD	BSD	kA	BSD
Unix	Unix	k1gInSc4	Unix
a	a	k8xC	a
odvozená	odvozený	k2eAgNnPc1d1	odvozené
díla	dílo	k1gNnPc1	dílo
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
BSD	BSD	kA	BSD
Unixu	Unix	k1gInSc2	Unix
<g/>
;	;	kIx,	;
modulární	modulární	k2eAgNnSc1d1	modulární
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
DragonFly	DragonFla	k1gFnSc2	DragonFla
BSD	BSD	kA	BSD
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
fork	fork	k1gInSc1	fork
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
4.8	[number]	k4	4.8
s	s	k7c7	s
hybridním	hybridní	k2eAgNnSc7d1	hybridní
jádrem	jádro	k1gNnSc7	jádro
NetBSD	NetBSD	k1gFnSc2	NetBSD
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
<g />
.	.	kIx.	.
</s>
<s>
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
BSD	BSD	kA	BSD
Unixu	Unix	k1gInSc2	Unix
(	(	kIx(	(
<g/>
před	před	k7c7	před
FreeBSD	FreeBSD	k1gFnSc7	FreeBSD
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
modulární	modulární	k2eAgNnSc1d1	modulární
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
OpenBSD	OpenBSD	k1gFnSc2	OpenBSD
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
fork	fork	k1gInSc1	fork
NetBSD	NetBSD	k1gFnPc2	NetBSD
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
;	;	kIx,	;
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
MINIX	MINIX	kA	MINIX
3	[number]	k4	3
–	–	k?	–
svobodný	svobodný	k2eAgInSc1d1	svobodný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
;	;	kIx,	;
mikrojádro	mikrojádro	k6eAd1	mikrojádro
navržené	navržený	k2eAgFnPc4d1	navržená
a	a	k8xC	a
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
vědcem	vědec	k1gMnSc7	vědec
Andrew	Andrew	k1gFnSc2	Andrew
S.	S.	kA	S.
Tanenbaumem	Tanenbaum	k1gInSc7	Tanenbaum
Česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Mock	Mock	k1gMnSc1	Mock
<g/>
.	.	kIx.	.
</s>
<s>
Neocortex	Neocortex	k1gInSc1	Neocortex
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86330	[number]	k4	86330
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Překlad	překlad	k1gInSc1	překlad
knižní	knižní	k2eAgFnSc2d1	knižní
podoby	podoba	k1gFnSc2	podoba
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
manuálu	manuál	k1gInSc2	manuál
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
Podrobný	podrobný	k2eAgMnSc1d1	podrobný
průvodce	průvodce	k1gMnSc1	průvodce
síťovým	síťový	k2eAgInSc7d1	síťový
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Lucas	Lucas	k1gMnSc1	Lucas
<g/>
.	.	kIx.	.
</s>
<s>
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7226	[number]	k4	7226
<g/>
-	-	kIx~	-
<g/>
795	[number]	k4	795
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Překlad	překlad	k1gInSc1	překlad
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Absolute	Absolut	k1gInSc5	Absolut
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Ultimate	Ultimat	k1gInSc5	Ultimat
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc4	ten
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Absolute	Absolut	k1gInSc5	Absolut
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Ultimate	Ultimat	k1gInSc5	Ultimat
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Lucas	Lucas	k1gMnSc1	Lucas
<g/>
.	.	kIx.	.
</s>
<s>
No	no	k9	no
Starch	Starch	k1gInSc1	Starch
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
July	Jula	k1gFnSc2	Jula
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
886411	[number]	k4	886411
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
BSD	BSD	kA	BSD
Hacks	Hacks	k1gInSc1	Hacks
<g/>
,	,	kIx,	,
100	[number]	k4	100
Industrial-Strength	Industrial-Strengtha	k1gFnPc2	Industrial-Strengtha
tips	tips	k6eAd1	tips
for	forum	k1gNnPc2	forum
BSD	BSD	kA	BSD
users	users	k1gInSc1	users
and	and	k?	and
administrators	administrators	k1gInSc1	administrators
<g/>
.	.	kIx.	.
</s>
<s>
Dru	Dru	k?	Dru
Lavigne	Lavign	k1gMnSc5	Lavign
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Reilly	Reill	k1gMnPc4	Reill
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
596	[number]	k4	596
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
679	[number]	k4	679
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Building	Building	k1gInSc1	Building
an	an	k?	an
Internet	Internet	k1gInSc1	Internet
Server	server	k1gInSc1	server
with	with	k1gInSc1	with
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
6	[number]	k4	6
<g/>
:	:	kIx,	:
Installing	Installing	k1gInSc1	Installing
open	opena	k1gFnPc2	opena
source	sourec	k1gInSc2	sourec
server	server	k1gInSc4	server
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Bryan	Bryan	k1gMnSc1	Bryan
Hong	Hong	k1gMnSc1	Hong
<g/>
.	.	kIx.	.
</s>
<s>
Lulu	lula	k1gFnSc4	lula
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4116	[number]	k4	4116
<g/>
-	-	kIx~	-
<g/>
9574	[number]	k4	9574
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
6	[number]	k4	6
Unleashed	Unleashed	k1gInSc1	Unleashed
<g/>
.	.	kIx.	.
</s>
<s>
Brian	Brian	k1gMnSc1	Brian
Tiemann	Tiemann	k1gMnSc1	Tiemann
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Urban	Urban	k1gMnSc1	Urban
<g/>
.	.	kIx.	.
</s>
<s>
Sams	Sams	k1gInSc1	Sams
<g/>
,	,	kIx,	,
Paperback	paperback	k1gInSc1	paperback
<g/>
,	,	kIx,	,
Bk	Bk	k1gFnSc1	Bk
<g/>
&	&	k?	&
<g/>
DVD	DVD	kA	DVD
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
Published	Published	k1gInSc1	Published
June	jun	k1gMnSc5	jun
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
912	[number]	k4	912
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
672	[number]	k4	672
<g/>
-	-	kIx~	-
<g/>
32875	[number]	k4	32875
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Mastering	Mastering	k1gInSc1	Mastering
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
and	and	k?	and
OpenBSD	OpenBSD	k1gFnSc2	OpenBSD
Security	Securita	k1gFnSc2	Securita
<g/>
.	.	kIx.	.
</s>
<s>
Yanek	Yanek	k1gMnSc1	Yanek
Korff	Korff	k1gMnSc1	Korff
<g/>
,	,	kIx,	,
Paco	Paco	k1gMnSc1	Paco
Hope	Hop	k1gFnSc2	Hop
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
Potter	Potter	k1gMnSc1	Potter
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Reilly	Reill	k1gInPc4	Reill
<g/>
,	,	kIx,	,
March	March	k1gInSc4	March
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
596	[number]	k4	596
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
626	[number]	k4	626
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Complete	Comple	k1gNnSc2	Comple
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
th	th	k?	th
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
Documentation	Documentation	k1gInSc1	Documentation
from	from	k1gMnSc1	from
the	the	k?	the
Source	Source	k1gMnSc1	Source
<g/>
.	.	kIx.	.
</s>
<s>
Greg	Greg	k1gInSc1	Greg
Lehey	Lehea	k1gFnSc2	Lehea
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Reilly	Reill	k1gInPc4	Reill
<g/>
,	,	kIx,	,
April	April	k1gInSc4	April
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
596	[number]	k4	596
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
516	[number]	k4	516
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Design	design	k1gInSc1	design
and	and	k?	and
Implementation	Implementation	k1gInSc1	Implementation
of	of	k?	of
the	the	k?	the
FreeBSD	FreeBSD	k1gFnSc7	FreeBSD
Operating	Operating	k1gInSc4	Operating
System	Syst	k1gInSc7	Syst
<g/>
.	.	kIx.	.
</s>
<s>
Marshall	Marshall	k1gMnSc1	Marshall
Kirk	Kirk	k1gMnSc1	Kirk
McKusick	McKusick	k1gMnSc1	McKusick
and	and	k?	and
George	Georg	k1gFnSc2	Georg
V.	V.	kA	V.
Neville-Neil	Neville-Neil	k1gMnSc1	Neville-Neil
<g/>
,	,	kIx,	,
Addison	Addison	k1gMnSc1	Addison
Wesley	Weslea	k1gFnSc2	Weslea
Professional	Professional	k1gMnSc1	Professional
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
70245	[number]	k4	70245
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
FreeBSD	FreeBSD	k1gMnSc5	FreeBSD
Corporate	Corporat	k1gMnSc5	Corporat
Networkers	Networkers	k1gInSc1	Networkers
Guide	Guid	k1gInSc5	Guid
<g/>
.	.	kIx.	.
</s>
<s>
Ted	Ted	k1gMnSc1	Ted
Mittelstaedt	Mittelstaedt	k1gMnSc1	Mittelstaedt
<g/>
.	.	kIx.	.
</s>
<s>
Addison-Wesley	Addison-Weslea	k1gFnPc1	Addison-Weslea
<g/>
,	,	kIx,	,
December	December	k1gInSc1	December
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Paperback	paperback	k1gInSc1	paperback
<g/>
,	,	kIx,	,
book	book	k1gInSc1	book
&	&	k?	&
CD	CD	kA	CD
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
401	[number]	k4	401
pages	pagesa	k1gFnPc2	pagesa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
70481	[number]	k4	70481
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
Handbook	handbook	k1gInSc1	handbook
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
:	:	kIx,	:
User	usrat	k5eAaPmRp2nS	usrat
Guide	Guid	k1gMnSc5	Guid
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
Documentation	Documentation	k1gInSc1	Documentation
Project	Project	k1gInSc1	Project
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
Mall	Mall	k1gMnSc1	Mall
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
November	November	k1gMnSc1	November
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
57176	[number]	k4	57176
<g/>
-	-	kIx~	-
<g/>
327	[number]	k4	327
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
Handbook	handbook	k1gInSc1	handbook
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
:	:	kIx,	:
Admin	Admin	k2eAgMnSc5d1	Admin
Guide	Guid	k1gMnSc5	Guid
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
Documentation	Documentation	k1gInSc1	Documentation
Project	Project	k1gInSc1	Project
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
Mall	Mall	k1gMnSc1	Mall
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
September	September	k1gMnSc1	September
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
57176	[number]	k4	57176
<g/>
-	-	kIx~	-
<g/>
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
ABCLinuxu	ABCLinux	k1gInSc3	ABCLinux
<g/>
:	:	kIx,	:
Fórum	fórum	k1gNnSc1	fórum
*	*	kIx~	*
<g/>
BSD	BSD	kA	BSD
a	a	k8xC	a
*	*	kIx~	*
<g/>
nixy	nixa	k1gFnSc2	nixa
–	–	k?	–
Diskusní	diskusní	k2eAgNnSc1d1	diskusní
fórum	fórum	k1gNnSc1	fórum
na	na	k7c6	na
ABCLinuxu	ABCLinux	k1gInSc6	ABCLinux
věnované	věnovaný	k2eAgNnSc1d1	věnované
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
operačním	operační	k2eAgInPc3d1	operační
systémům	systém	k1gInPc3	systém
rodiny	rodina	k1gFnSc2	rodina
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kapotou	kapota	k1gFnSc7	kapota
Sony	Sony	kA	Sony
PlayStation	PlayStation	k1gInSc4	PlayStation
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
upravené	upravený	k2eAgFnPc4d1	upravená
FreeBSD	FreeBSD	k1gFnPc4	FreeBSD
9.0	[number]	k4	9.0
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
10.0	[number]	k4	10.0
s	s	k7c7	s
ovladači	ovladač	k1gInPc7	ovladač
AMD	AMD	kA	AMD
a	a	k8xC	a
podporou	podpora	k1gFnSc7	podpora
Raspberry	Raspberra	k1gFnSc2	Raspberra
Pi	pi	k0	pi
Slovensky	Slovensko	k1gNnPc7	Slovensko
<g/>
:	:	kIx,	:
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
–	–	k?	–
Články	článek	k1gInPc1	článek
o	o	k7c4	o
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
publikované	publikovaný	k2eAgFnSc6d1	publikovaná
i	i	k8xC	i
v	v	k7c6	v
PC	PC	kA	PC
Revue	revue	k1gFnSc1	revue
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
–	–	k?	–
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
projektu	projekt	k1gInSc2	projekt
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
FreeBSD	FreeBSD	k1gMnSc2	FreeBSD
spoluzakladatele	spoluzakladatel	k1gMnSc2	spoluzakladatel
FreeBSD	FreeBSD	k1gMnSc2	FreeBSD
Jordana	Jordan	k1gMnSc2	Jordan
Hubbarda	Hubbard	k1gMnSc2	Hubbard
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
logo	logo	k1gNnSc1	logo
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
porty	port	k1gInPc1	port
–	–	k?	–
Archiv	archiv	k1gInSc1	archiv
portů	port	k1gInPc2	port
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
–	–	k?	–
Planeta	planeta	k1gFnSc1	planeta
vývojářů	vývojář	k1gMnPc2	vývojář
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
serverů	server	k1gInPc2	server
Planet	planeta	k1gFnPc2	planeta
Něco	něco	k6eAd1	něco
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
diář	diář	k1gInSc1	diář
–	–	k?	–
Praktické	praktický	k2eAgFnPc1d1	praktická
ukázky	ukázka	k1gFnPc1	ukázka
řešení	řešení	k1gNnSc2	řešení
různých	různý	k2eAgFnPc2d1	různá
úloh	úloha	k1gFnPc2	úloha
ve	v	k7c6	v
FreeBSD	FreeBSD	k1gFnSc6	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
wiki	wiki	k6eAd1	wiki
–	–	k?	–
FreeBSD-specifická	FreeBSDpecifický	k2eAgFnSc1d1	FreeBSD-specifický
wiki	wiki	k6eAd1	wiki
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
administrátory	administrátor	k1gMnPc4	administrátor
<g/>
.	.	kIx.	.
</s>
<s>
TrustedBSD	TrustedBSD	k?	TrustedBSD
–	–	k?	–
Webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
TrustedBSD	TrustedBSD	k1gFnSc2	TrustedBSD
<g/>
.	.	kIx.	.
</s>
<s>
BSD	BSD	kA	BSD
fóra	fórum	k1gNnSc2	fórum
–	–	k?	–
Stránka	stránka	k1gFnSc1	stránka
s	s	k7c7	s
diskusními	diskusní	k2eAgInPc7d1	diskusní
fóry	fór	k1gInPc7	fór
k	k	k7c3	k
operačním	operační	k2eAgInPc3d1	operační
systémům	systém	k1gInPc3	systém
rodiny	rodina	k1gFnSc2	rodina
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
IBM	IBM	kA	IBM
developerWorks	developerWorks	k6eAd1	developerWorks
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Rychlá	rychlý	k2eAgFnSc1d1	rychlá
přehlídka	přehlídka	k1gFnSc1	přehlídka
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
OpenBSM	OpenBSM	k1gFnSc2	OpenBSM
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
manuál	manuál	k1gInSc1	manuál
<g/>
:	:	kIx,	:
Audit	audit	k1gInSc1	audit
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
událostí	událost	k1gFnPc2	událost
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
manuál	manuál	k1gInSc1	manuál
<g/>
:	:	kIx,	:
Access	Access	k1gInSc1	Access
Control	Controla	k1gFnPc2	Controla
Listy	lista	k1gFnSc2	lista
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
manuál	manuál	k1gInSc1	manuál
<g/>
:	:	kIx,	:
Mandatorní	mandatorní	k2eAgFnSc1d1	mandatorní
kontrola	kontrola	k1gFnSc1	kontrola
přístupu	přístup	k1gInSc2	přístup
(	(	kIx(	(
<g/>
MAC	Mac	kA	Mac
<g/>
)	)	kIx)	)
FreeBSD	FreeBSD	k1gMnSc1	FreeBSD
manuál	manuál	k1gInSc4	manuál
vývojáře	vývojář	k1gMnSc4	vývojář
<g/>
:	:	kIx,	:
MAC	Mac	kA	Mac
Framework	Framework	k1gInSc1	Framework
TrustedBSD	TrustedBSD	k1gMnSc2	TrustedBSD
FreeBSD	FreeBSD	k1gMnSc2	FreeBSD
na	na	k7c4	na
NoBlueScreens	NoBlueScreens	k1gInSc4	NoBlueScreens
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
Excelentní	excelentní	k2eAgMnSc1d1	excelentní
průvodce	průvodce	k1gMnSc1	průvodce
začátečníka	začátečník	k1gMnSc2	začátečník
s	s	k7c7	s
tutoriály	tutoriál	k1gInPc7	tutoriál
psanými	psaný	k2eAgInPc7d1	psaný
prostým	prostý	k2eAgInSc7d1	prostý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
lokalizaci	lokalizace	k1gFnSc6	lokalizace
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
<g/>
;	;	kIx,	;
také	také	k9	také
MP3	MP3	k1gFnSc1	MP3
a	a	k8xC	a
CD	CD	kA	CD
přehrávač	přehrávač	k1gInSc1	přehrávač
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
disketě	disketa	k1gFnSc6	disketa
<g/>
.	.	kIx.	.
</s>
<s>
FreeBSD	FreeBSD	k?	FreeBSD
8	[number]	k4	8
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
8	[number]	k4	8
review	review	k?	review
</s>
