<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
známý	známý	k2eAgInSc4d1	známý
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
byla	být	k5eAaImAgFnS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořená	tvořený	k2eAgFnSc1d1	tvořená
konvekcí	konvekce	k1gFnSc7	konvekce
probíhající	probíhající	k2eAgFnSc6d1	probíhající
uvnitř	uvnitř	k7c2	uvnitř
tekutého	tekutý	k2eAgNnSc2d1	tekuté
železného	železný	k2eAgNnSc2d1	železné
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
