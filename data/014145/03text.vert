<s>
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
</s>
<s>
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1958	#num#	k4
(	(	kIx(
<g/>
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
New	New	k1gMnPc2
York	York	k1gInSc1
Povolání	povolání	k1gNnSc4
</s>
<s>
scenárista	scenárista	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
producent	producent	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Boston	Boston	k1gInSc1
University	universita	k1gFnSc2
College	Colleg	k1gFnSc2
of	of	k?
Fine	Fin	k1gMnSc5
ArtsTisch	ArtsTisch	k1gMnSc1
School	Schoola	k1gFnPc2
of	of	k?
the	the	k?
ArtsHall	ArtsHall	k1gInSc1
High	High	k1gInSc1
School	Schoola	k1gFnPc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Writers	Writers	k6eAd1
Guild	Guild	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
AwardCena	AwardCen	k1gMnSc2
Akademie	akademie	k1gFnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
scénář	scénář	k1gInSc4
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Charles	Charles	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Kaufman	Kaufman	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1958	#num#	k4
New	New	k1gFnPc2
York	York	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
scenárista	scenárista	k1gMnSc1
oceněný	oceněný	k2eAgMnSc1d1
Oscarem	Oscar	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
byl	být	k5eAaImAgMnS
časopisem	časopis	k1gInSc7
Premiere	Premiere	k1gInSc1
označený	označený	k2eAgInSc1d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
100	#num#	k4
nejvlivnějších	vlivný	k2eAgMnPc2d3
lidí	člověk	k1gMnPc2
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Kaufman	Kaufman	k1gMnSc1
začínal	začínat	k5eAaImAgMnS
v	v	k7c6
televizi	televize	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
dva	dva	k4xCgInPc4
díly	díl	k1gInPc4
seriálu	seriál	k1gInSc2
Chris	Chris	k1gFnSc2
Elliott	Elliotta	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Get	Get	k1gFnSc7
a	a	k8xC
Life	Life	k1gFnSc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
dílů	díl	k1gInPc2
jiných	jiný	k2eAgInPc2d1
seriálů	seriál	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Ned	Ned	k1gFnSc2
a	a	k8xC
Stacey	Stacea	k1gFnSc2
a	a	k8xC
The	The	k1gFnSc2
Dana	Dana	k1gFnSc1
Carvey	Carvea	k1gFnSc2
Show	show	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
jako	jako	k9
scenárista	scenárista	k1gMnSc1
filmu	film	k1gInSc2
V	v	k7c6
kůži	kůže	k1gFnSc6
Johna	John	k1gMnSc2
Malkoviche	Malkovich	k1gMnSc2
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
získal	získat	k5eAaPmAgMnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
Oscara	Oscar	k1gMnSc4
a	a	k8xC
cenu	cena	k1gFnSc4
BAFTA	BAFTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
také	také	k9
scénář	scénář	k1gInSc4
k	k	k7c3
filmu	film	k1gInSc3
Slez	Slez	k1gFnSc1
ze	z	k7c2
stromu	strom	k1gInSc2
<g/>
,	,	kIx,
režírovaný	režírovaný	k2eAgInSc1d1
Michelem	Michel	k1gInSc7
Gondry	Gondr	k1gInPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
spolupracoval	spolupracovat	k5eAaImAgMnS
se	s	k7c7
Spikem	Spiek	k1gMnSc7
Jonzem	Jonz	k1gMnSc7
jako	jako	k8xS,k8xC
scenárista	scenárista	k1gMnSc1
k	k	k7c3
filmu	film	k1gInSc3
Adaptace	adaptace	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
získal	získat	k5eAaPmAgMnS
další	další	k2eAgFnSc4d1
nominaci	nominace	k1gFnSc4
na	na	k7c4
Oscara	Oscar	k1gMnSc4
a	a	k8xC
druhou	druhý	k4xOgFnSc4
cenu	cena	k1gFnSc4
BAFTA	BAFTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adaptace	adaptace	k1gFnSc1
vypráví	vyprávět	k5eAaImIp3nS
příběh	příběh	k1gInSc4
„	„	k?
<g/>
Charlieho	Charlie	k1gMnSc2
Kaufmana	Kaufman	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
postavy	postava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
beletrizovanou	beletrizovaný	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
scenáristy	scenárista	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ale	ale	k8xC
ve	v	k7c6
skutečném	skutečný	k2eAgInSc6d1
životě	život	k1gInSc6
nemá	mít	k5eNaImIp3nS
identické	identický	k2eAgNnSc4d1
dvojče	dvojče	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
sotva	sotva	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zažít	zažít	k5eAaPmF
drama	drama	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
sledujeme	sledovat	k5eAaImIp1nP
ve	v	k7c6
filmu	film	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
také	také	k9
Milujte	milovat	k5eAaImRp2nP
svého	své	k1gNnSc2
zabijáka	zabiják	k1gMnSc4
<g/>
,	,	kIx,
biografii	biografie	k1gFnSc4
Chucka	Chucko	k1gNnSc2
Barrisa	Barris	k1gMnSc2
<g/>
,	,	kIx,
moderátora	moderátor	k1gMnSc2
zábavného	zábavný	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
vrahem	vrah	k1gMnSc7
na	na	k7c4
objednávku	objednávka	k1gFnSc4
pro	pro	k7c4
CIA	CIA	kA
<g/>
;	;	kIx,
režisérský	režisérský	k2eAgInSc1d1
debut	debut	k1gInSc1
George	Georg	k1gMnSc2
Clooneyho	Clooney	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaufman	Kaufman	k1gMnSc1
velmi	velmi	k6eAd1
kritizoval	kritizovat	k5eAaImAgMnS
Georga	Georg	k1gMnSc4
Clooneyho	Clooney	k1gMnSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
zasahoval	zasahovat	k5eAaImAgMnS
do	do	k7c2
scénáře	scénář	k1gInSc2
Milujte	milovat	k5eAaImRp2nP
svého	svůj	k3xOyFgMnSc2
zabijáka	zabiják	k1gMnSc2
bez	bez	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
s	s	k7c7
ním	on	k3xPp3gMnSc7
cokoliv	cokoliv	k3yInSc1
konzultoval	konzultovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
interview	interview	k1gNnSc6
s	s	k7c7
Williamem	William	k1gInSc7
Arnoldem	Arnold	k1gMnSc7
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Obvykle	obvykle	k6eAd1
scenárista	scenárista	k1gMnSc1
dodá	dodat	k5eAaPmIp3nS
scénář	scénář	k1gInSc4
a	a	k8xC
vypaří	vypařit	k5eAaPmIp3nP
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chci	chtít	k5eAaImIp1nS
se	se	k3xPyFc4
na	na	k7c6
všem	všecek	k3xTgNnSc6
podílet	podílet	k5eAaImF
od	od	k7c2
začátku	začátek	k1gInSc2
do	do	k7c2
konce	konec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tito	tento	k3xDgMnPc1
režiséři	režisér	k1gMnPc1
[	[	kIx(
<g/>
Gondry	Gondr	k1gInPc1
<g/>
,	,	kIx,
Jonze	Jonze	k1gFnPc1
<g/>
]	]	kIx)
to	ten	k3xDgNnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
respektují	respektovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
nejnovější	nový	k2eAgInSc1d3
scenáristický	scenáristický	k2eAgInSc1d1
počin	počin	k1gInSc1
je	být	k5eAaImIp3nS
Věčný	věčný	k2eAgInSc4d1
svit	svit	k1gInSc4
neposkvrněné	poskvrněný	k2eNgFnSc2d1
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
film	film	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
režisérem	režisér	k1gMnSc7
Michelem	Michel	k1gMnSc7
Gondrym	Gondrym	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
získal	získat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
Cenu	cena	k1gFnSc4
akademie	akademie	k1gFnSc2
za	za	k7c4
originální	originální	k2eAgInSc4d1
scénář	scénář	k1gInSc4
a	a	k8xC
třetí	třetí	k4xOgNnSc4
ocenění	ocenění	k1gNnSc4
BAFTA	BAFTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Edelstein	Edelstein	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
film	film	k1gInSc4
v	v	k7c6
časopise	časopis	k1gInSc6
Slate	Slat	k1gInSc5
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Strašná	strašný	k2eAgFnSc1d1
pravda	pravda	k1gFnSc1
obrácená	obrácený	k2eAgFnSc1d1
naruby	naruby	k6eAd1
Philipom	Philipom	k1gInSc1
K.	K.	kA
Dickem	Dicko	k1gNnSc7
<g/>
,	,	kIx,
s	s	k7c7
odkazy	odkaz	k1gInPc7
na	na	k7c4
Samuela	Samuel	k1gMnSc4
Becketta	Beckett	k1gMnSc4
<g/>
,	,	kIx,
Chrisa	Chris	k1gMnSc4
Markera	Marker	k1gMnSc4
<g/>
,	,	kIx,
Johna	John	k1gMnSc4
Guarea	Guareus	k1gMnSc4
—	—	k?
největších	veliký	k2eAgMnPc2d3
dramatiků	dramatik	k1gMnPc2
současného	současný	k2eAgNnSc2d1
rozbitého	rozbitý	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
zápletka	zápletka	k1gFnSc1
je	být	k5eAaImIp3nS
čistě	čistě	k6eAd1
kaufmanovská	kaufmanovský	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1
modelem	model	k1gInSc7
v	v	k7c6
Kaufmanově	Kaufmanův	k2eAgFnSc6d1
práci	práce	k1gFnSc6
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
často	často	k6eAd1
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
introvertní	introvertní	k2eAgNnSc4d1
<g/>
,	,	kIx,
svým	svůj	k3xOyFgInSc7
způsobem	způsob	k1gInSc7
stydlivé	stydlivý	k2eAgMnPc4d1
mužské	mužský	k2eAgMnPc4d1
protagonisty	protagonista	k1gMnPc4
a	a	k8xC
dominantní	dominantní	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vidíme	vidět	k5eAaImIp1nP
ve	v	k7c6
filmech	film	k1gInPc6
Věčný	věčný	k2eAgInSc4d1
svit	svit	k1gInSc4
neposkvrněné	poskvrněný	k2eNgFnSc2d1
mysli	mysl	k1gFnSc2
(	(	kIx(
<g/>
Joel	Joel	k1gInSc4
<g/>
/	/	kIx~
<g/>
Clementine	Clementin	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Adaptace	adaptace	k1gFnSc1
(	(	kIx(
<g/>
Charlie	Charlie	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c4
V	v	k7c4
kůži	kůže	k1gFnSc4
Johna	John	k1gMnSc2
Malkoviche	Malkovich	k1gMnSc2
(	(	kIx(
<g/>
Craig	Craig	k1gInSc4
<g/>
/	/	kIx~
<g/>
Maxine	Maxin	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Pasadene	Pasaden	k1gInSc5
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
Kalifornie	Kalifornie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
Naposledy	naposledy	k6eAd1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
režíroval	režírovat	k5eAaImAgMnS
hru	hra	k1gFnSc4
Naděje	naděje	k1gFnSc2
opouští	opouštět	k5eAaImIp3nS
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
výhradně	výhradně	k6eAd1
zvukové	zvukový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
Theater	Theater	k1gMnSc1
of	of	k?
the	the	k?
New	New	k1gMnSc1
Ear	Ear	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
hře	hra	k1gFnSc6
hrají	hrát	k5eAaImIp3nP
takové	takový	k3xDgFnPc1
hvězdy	hvězda	k1gFnPc1
jako	jako	k8xS,k8xC
Meryl	Meryl	k1gInSc1
Streep	Streep	k1gInSc1
<g/>
,	,	kIx,
Hope	Hope	k1gFnSc1
Davis	Davis	k1gFnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Dinklage	Dinklag	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
hry	hra	k1gFnSc2
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
věc	věc	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
(	(	kIx(
<g/>
postava	postava	k1gFnSc1
<g/>
)	)	kIx)
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
předtím	předtím	k6eAd1
než	než	k8xS
spáchal	spáchat	k5eAaPmAgMnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
na	na	k7c4
postavu	postava	k1gFnSc4
Hopa	Hopus	k1gMnSc2
Davisa	Davis	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
„	„	k?
<g/>
opouští	opouštět	k5eAaImIp3nS
divadlo	divadlo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c4
oblíbené	oblíbený	k2eAgMnPc4d1
spisovatele	spisovatel	k1gMnPc4
Charlieho	Charlie	k1gMnSc4
Kaufmana	Kaufman	k1gMnSc4
patří	patřit	k5eAaImIp3nS
Franz	Franz	k1gMnSc1
Kafka	Kafka	k1gMnSc1
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
Beckett	Beckett	k1gMnSc1
<g/>
,	,	kIx,
Stanisław	Stanisław	k1gMnSc1
Lem	lem	k1gInSc1
<g/>
,	,	kIx,
Philip	Philip	k1gMnSc1
K.	K.	kA
Dick	Dick	k1gMnSc1
<g/>
,	,	kIx,
Flannery	Flanner	k1gMnPc4
O	O	kA
<g/>
'	'	kIx"
<g/>
Connor	Connor	k1gInSc1
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
Dixon	Dixon	k1gInSc1
<g/>
,	,	kIx,
Shirley	Shirley	k1gInPc1
Jackson	Jackson	k1gInSc1
a	a	k8xC
Patricia	Patricius	k1gMnSc4
Highsmith	Highsmith	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kaufmanově	Kaufmanův	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
reference	reference	k1gFnPc4
na	na	k7c4
další	další	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
Italo	Italo	k1gNnSc1
Svevo	Svevo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
postav	postava	k1gFnPc2
je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Mary	Mary	k1gFnSc1
Svevo	Svevo	k1gNnSc1
ve	v	k7c4
Věčný	věčný	k2eAgInSc4d1
svit	svit	k1gInSc4
neposkvrněné	poskvrněný	k2eNgFnSc2d1
mysli	mysl	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Svevův	Svevův	k2eAgInSc1d1
román	román	k1gInSc1
La	la	k1gNnSc2
Coscienza	Coscienza	k1gFnSc1
di	di	k?
Zeno	Zeno	k1gNnSc1
(	(	kIx(
<g/>
Zenova	Zenův	k2eAgFnSc1d1
zpověď	zpověď	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Zenovo	Zenův	k2eAgNnSc1d1
svědomí	svědomí	k1gNnSc1
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
s	s	k7c7
Kaufmanovou	Kaufmanův	k2eAgFnSc7d1
tvorbou	tvorba	k1gFnSc7
důležitou	důležitý	k2eAgFnSc4d1
spojitost	spojitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Citáty	citát	k1gInPc1
</s>
<s>
"	"	kIx"
<g/>
Vědomí	vědomí	k1gNnSc1
je	být	k5eAaImIp3nS
strašné	strašný	k2eAgNnSc1d1
prokletí	prokletí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslím	myslet	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cítím	cítit	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trpím	trpět	k5eAaImIp1nS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
-	-	kIx~
Craig	Craig	k1gMnSc1
Schwartz	Schwartz	k1gMnSc1
(	(	kIx(
<g/>
John	John	k1gMnSc1
Cusack	Cusack	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
V	V	kA
kůži	kůže	k1gFnSc6
Johna	John	k1gMnSc4
Malkoviche	Malkovich	k1gMnSc4
</s>
<s>
„	„	k?
<g/>
Když	když	k8xS
jsem	být	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
mladší	mladý	k2eAgMnSc1d2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
rád	rád	k6eAd1
Woodyho	Woody	k1gMnSc2
Allena	Allen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
starší	starý	k2eAgFnSc1d2
tvorba	tvorba	k1gFnSc1
je	být	k5eAaImIp3nS
úplně	úplně	k6eAd1
chaotická	chaotický	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
mi	já	k3xPp1nSc3
jako	jako	k8xS,k8xC
dítěti	dítě	k1gNnSc6
líbilo	líbit	k5eAaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
osobnost	osobnost	k1gFnSc1
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
jsem	být	k5eAaImIp1nS
mohl	moct	k5eAaImAgMnS
vzhlížet	vzhlížet	k5eAaImF
<g/>
,	,	kIx,
chápete	chápat	k5eAaImIp2nP
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgMnSc1
žid	žid	k1gMnSc1
tam	tam	k6eAd1
nahoře	nahoře	k6eAd1
na	na	k7c6
plátně	plátno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jsem	být	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
mladší	mladý	k2eAgMnSc1d2
<g/>
,	,	kIx,
chtěl	chtít	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
psát	psát	k5eAaImF
komedie	komedie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
styl	styl	k1gInSc1
se	se	k3xPyFc4
mi	já	k3xPp1nSc3
líbil	líbit	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
tehdy	tehdy	k6eAd1
jsem	být	k5eAaImIp1nS
měl	mít	k5eAaImAgMnS
o	o	k7c6
věcích	věk	k1gInPc6
jinou	jiný	k2eAgFnSc4d1
představu	představa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
„	„	k?
<g/>
Nemám	mít	k5eNaImIp1nS
nic	nic	k3yNnSc1
proti	proti	k7c3
příběhům	příběh	k1gInPc3
<g/>
,	,	kIx,
ale	ale	k8xC
potřebuji	potřebovat	k5eAaImIp1nS
cítit	cítit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
něco	něco	k3yInSc4
děje	dít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Četl	číst	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
něco	něco	k3yInSc4
od	od	k7c2
Emily	Emil	k1gMnPc4
Dickinsonové	Dickinsonový	k2eAgNnSc1d1
a	a	k8xC
parafrázuji	parafrázovat	k5eAaBmIp1nS
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
víte	vědět	k5eAaImIp2nP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poezie	poezie	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
vám	vy	k3xPp2nPc3
z	z	k7c2
toho	ten	k3xDgNnSc2
běhá	běhat	k5eAaImIp3nS
mráz	mráz	k1gInSc1
po	po	k7c4
zádech	zádech	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
-	-	kIx~
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Michaelem	Michael	k1gMnSc7
Koreskym	Koreskym	k1gInSc4
a	a	k8xC
Matthewem	Matthewem	k1gInSc4
Plouffe	Plouff	k1gInSc5
<g/>
,	,	kIx,
Reverse	reverse	k1gFnSc1
Shot	shot	k1gInSc1
Online	Onlin	k1gMnSc5
<g/>
,	,	kIx,
jaro	jaro	k6eAd1
2005	#num#	k4
</s>
<s>
Vybraná	vybraný	k2eAgFnSc1d1
filmografie	filmografie	k1gFnSc1
</s>
<s>
Scénáře	scénář	k1gInPc1
</s>
<s>
V	v	k7c6
kůži	kůže	k1gFnSc6
Johna	John	k1gMnSc2
Malkoviche	Malkovich	k1gMnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slez	Slez	k1gFnSc1
ze	z	k7c2
stromu	strom	k1gInSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Adaptace	adaptace	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milujte	milovat	k5eAaImRp2nP
svého	svůj	k3xOyFgMnSc2
zabijáka	zabiják	k1gMnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Věčný	věčný	k2eAgInSc1d1
svit	svit	k1gInSc1
neposkvrněné	poskvrněný	k2eNgFnSc2d1
mysli	mysl	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Synecdoche	Synecdoche	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Režie	režie	k1gFnSc1
</s>
<s>
Synecdoche	Synecdoche	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Being	Being	k1gMnSc1
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
-	-	kIx~
Detailní	detailní	k2eAgFnSc1d1
fanouškovská	fanouškovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Being	Being	k1gMnSc1
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
-	-	kIx~
Rozhovor	rozhovor	k1gInSc1
Michaela	Michael	k1gMnSc2
Sragowa	Sragowus	k1gMnSc2
v	v	k7c4
Salon	salon	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1999	#num#	k4
</s>
<s>
Arnold	Arnold	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
and	and	k?
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
Charlie	Charlie	k1gMnSc1
<g/>
;	;	kIx,
interview	interview	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
A	a	k8xC
moment	moment	k1gInSc1
with	with	k1gMnSc1
…	…	k?
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
screenwriter	screenwriter	k1gMnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Seattle	Seattle	k1gFnSc1
Post-Intelligencer	Post-Intelligencer	k1gInSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
19	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
</s>
<s>
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Man	Man	k1gMnSc1
behind	behind	k1gMnSc1
„	„	k?
<g/>
Malkovich	Malkovich	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
by	by	kYmCp3nP
Anthony	Anthona	k1gFnPc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
indieWire	indieWir	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
October	October	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
Au	au	k0
Naturel	naturel	k1gInSc4
<g/>
,	,	kIx,
On	on	k3xPp3gMnSc1
Human	Human	k1gMnSc1
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
by	by	kYmCp3nS
Rod	rod	k1gInSc4
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
Reel	Reel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
April	April	k1gInSc1
11	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Wanted	Wanted	k1gMnSc1
<g/>
:	:	kIx,
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
Outlaw	Outlaw	k1gMnSc5
Scribe	Scrib	k1gMnSc5
<g/>
,	,	kIx,
by	by	kYmCp3nS
David	David	k1gMnSc1
Fear	Fear	k1gMnSc1
<g/>
,	,	kIx,
Moviemaker	Moviemaker	k1gMnSc1
</s>
<s>
Interview	interview	k1gInSc1
with	with	k1gInSc1
Michel	Michel	k1gInSc1
Gondry	Gondr	k1gInPc1
and	and	k?
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
by	by	kYmCp3nS
Ray	Ray	k1gMnSc5
Pride	Prid	k1gMnSc5
<g/>
,	,	kIx,
Movie	Movie	k1gFnSc2
City	City	k1gFnSc2
News	News	k1gInSc1
<g/>
,	,	kIx,
March	March	k1gInSc1
17	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Interview	interview	k1gNnSc1
with	with	k1gMnSc1
Michael	Michael	k1gMnSc1
Gondry	Gondr	k1gInPc7
and	and	k?
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
by	by	kYmCp3nS
Brendan	Brendan	k1gInSc1
MacDevette	MacDevett	k1gInSc5
<g/>
,	,	kIx,
Independent	independent	k1gMnSc1
Film	film	k1gInSc4
Quarterly	Quarterla	k1gFnSc2
</s>
<s>
Interview	interview	k1gNnSc1
-	-	kIx~
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
<g/>
,	,	kIx,
by	by	kYmCp3nS
Tim	Tim	k?
Stewart	Stewart	k1gMnSc1
<g/>
,	,	kIx,
X-Press	X-Press	k1gInSc1
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
April	April	k1gInSc1
25	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Why	Why	k?
Charlie	Charlie	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
doesn	doesn	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
watch	watch	k1gMnSc1
movies	movies	k1gMnSc1
anymore	anymor	k1gInSc5
<g/>
,	,	kIx,
by	by	kYmCp3nS
Michael	Michael	k1gMnSc1
Koresky	Koreska	k1gFnSc2
and	and	k?
Matthew	Matthew	k1gMnSc5
Plouffe	Plouff	k1gMnSc5
<g/>
,	,	kIx,
Reverse	reverse	k1gFnSc1
Shot	shot	k1gInSc1
Online	Onlin	k1gInSc5
<g/>
,	,	kIx,
Spring	Spring	k1gInSc1
2005	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2006327266	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
131817876	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2146	#num#	k4
0805	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2001002173	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
103198746	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2001002173	#num#	k4
</s>
