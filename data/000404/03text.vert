<s>
Dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
N	N	kA	N
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Nitrogenium	nitrogenium	k1gNnSc4	nitrogenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc4d1	plynný
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
základními	základní	k2eAgInPc7d1	základní
stavebními	stavební	k2eAgInPc7d1	stavební
kameny	kámen	k1gInPc7	kámen
živé	živý	k2eAgFnSc2d1	živá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
složka	složka	k1gFnSc1	složka
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
hoření	hoření	k1gNnSc4	hoření
ani	ani	k8xC	ani
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
plyn	plyn	k1gInSc4	plyn
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Němec	Němec	k1gMnSc1	Němec
Carl	Carl	k1gMnSc1	Carl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Scheele	Scheel	k1gInSc2	Scheel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
a	a	k8xC	a
Francouz	Francouz	k1gMnSc1	Francouz
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisier	k1gMnSc1	Lavoisier
ho	on	k3xPp3gMnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k8xC	jako
azote	azote	k5eAaPmIp2nP	azote
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
nebo	nebo	k8xC	nebo
ruštině	ruština	k1gFnSc6	ruština
(	(	kIx(	(
<g/>
А	А	k?	А
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
'	'	kIx"	'
<g/>
dusivý	dusivý	k2eAgInSc1d1	dusivý
plyn	plyn	k1gInSc1	plyn
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Chaptal	Chaptal	k1gMnSc1	Chaptal
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
název	název	k1gInSc4	název
nitrogéne	nitrogén	k1gInSc5	nitrogén
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
'	'	kIx"	'
<g/>
ledkotvorný	ledkotvorný	k2eAgMnSc1d1	ledkotvorný
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgMnS	udržet
v	v	k7c6	v
latinském	latinský	k2eAgNnSc6d1	latinské
označení	označení	k1gNnSc6	označení
nitrogenium	nitrogenium	k1gNnSc1	nitrogenium
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
dusík	dusík	k1gInSc1	dusík
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
překladem	překlad	k1gInSc7	překlad
jeho	jeho	k3xOp3gInSc2	jeho
německého	německý	k2eAgInSc2d1	německý
názvu	název	k1gInSc2	název
Stickstoff	Stickstoff	k1gMnSc1	Stickstoff
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
Preslových	Preslový	k2eAgMnPc2d1	Preslový
<g/>
;	;	kIx,	;
podobné	podobný	k2eAgInPc1d1	podobný
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
např.	např.	kA	např.
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
(	(	kIx(	(
<g/>
dusík	dusík	k1gInSc1	dusík
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
slovinštině	slovinština	k1gFnSc3	slovinština
a	a	k8xC	a
chorvatštině	chorvatština	k1gFnSc3	chorvatština
(	(	kIx(	(
<g/>
dušik	dušik	k1gInSc1	dušik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc4	plyn
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
ani	ani	k8xC	ani
jinak	jinak	k6eAd1	jinak
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
tvořen	tvořit	k5eAaImNgInS	tvořit
dvouatomovými	dvouatomový	k2eAgFnPc7d1	dvouatomová
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgNnSc1d1	spojené
velmi	velmi	k6eAd1	velmi
pevnou	pevný	k2eAgFnSc7d1	pevná
trojnou	trojný	k2eAgFnSc7d1	trojná
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trojná	trojný	k2eAgFnSc1d1	trojná
vazba	vazba	k1gFnSc1	vazba
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	jeho	k3xOp3gFnSc4	jeho
nízkou	nízký	k2eAgFnSc4d1	nízká
reaktivitu	reaktivita	k1gFnSc4	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
inertní	inertní	k2eAgInSc1d1	inertní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
chemickými	chemický	k2eAgFnPc7d1	chemická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
teploty	teplota	k1gFnSc2	teplota
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
lithiem	lithium	k1gNnSc7	lithium
a	a	k8xC	a
plutoniem	plutonium	k1gNnSc7	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
se	se	k3xPyFc4	se
však	však	k9	však
dusík	dusík	k1gInSc1	dusík
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
prvků	prvek	k1gInPc2	prvek
-	-	kIx~	-
např.	např.	kA	např.
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
okolo	okolo	k7c2	okolo
teploty	teplota	k1gFnSc2	teplota
2	[number]	k4	2
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
atomární	atomární	k2eAgInSc4d1	atomární
dusík	dusík	k1gInSc4	dusík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gMnSc4	on
uchovávat	uchovávat	k5eAaImF	uchovávat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vysoká	vysoký	k2eAgFnSc1d1	vysoká
reaktivita	reaktivita	k1gFnSc1	reaktivita
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
valenční	valenční	k2eAgFnSc6d1	valenční
vrstvě	vrstva	k1gFnSc6	vrstva
3	[number]	k4	3
nepárové	párový	k2eNgInPc4d1	nepárový
elektrony	elektron	k1gInPc4	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Stability	stabilita	k1gFnPc1	stabilita
docílí	docílit	k5eAaPmIp3nP	docílit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
buď	buď	k8xC	buď
přijme	přijmout	k5eAaPmIp3nS	přijmout
tři	tři	k4xCgInPc4	tři
elektrony	elektron	k1gInPc4	elektron
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
stabilní	stabilní	k2eAgInSc4d1	stabilní
oktet	oktet	k1gInSc4	oktet
ve	v	k7c6	v
valenční	valenční	k2eAgFnSc6d1	valenční
sféře	sféra	k1gFnSc6	sféra
N	N	kA	N
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odevzdá	odevzdat	k5eAaPmIp3nS	odevzdat
až	až	k9	až
5	[number]	k4	5
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
tím	ten	k3xDgNnSc7	ten
kladnou	kladný	k2eAgFnSc7d1	kladná
valenci	valence	k1gFnSc4	valence
<g/>
,	,	kIx,	,
např.	např.	kA	např.
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
nebo	nebo	k8xC	nebo
N	N	kA	N
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
fluoru	fluor	k1gInSc2	fluor
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
hodnotu	hodnota	k1gFnSc4	hodnota
elektronegativity	elektronegativita	k1gFnSc2	elektronegativita
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
převládá	převládat	k5eAaImIp3nS	převládat
schopnost	schopnost	k1gFnSc4	schopnost
vytvářet	vytvářet	k5eAaImF	vytvářet
anion	anion	k1gInSc4	anion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nitridový	nitridový	k2eAgInSc1d1	nitridový
N	N	kA	N
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
fluorem	fluor	k1gInSc7	fluor
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
kladné	kladný	k2eAgFnSc6d1	kladná
valenci	valence	k1gFnSc6	valence
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
dusičnanech	dusičnan	k1gInPc6	dusičnan
má	mít	k5eAaImIp3nS	mít
dusík	dusík	k1gInSc4	dusík
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
N	N	kA	N
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
přesunů	přesun	k1gInPc2	přesun
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
a	a	k8xC	a
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
určité	určitý	k2eAgFnSc2d1	určitá
mezní	mezní	k2eAgFnSc2d1	mezní
elektronové	elektronový	k2eAgFnSc2d1	elektronová
konfigurace	konfigurace	k1gFnSc2	konfigurace
valenční	valenční	k2eAgFnSc2d1	valenční
sféry	sféra	k1gFnSc2	sféra
atomů	atom	k1gInPc2	atom
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
sloučeném	sloučený	k2eAgInSc6d1	sloučený
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
tedy	tedy	k9	tedy
dusík	dusík	k1gInSc1	dusík
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
max	max	kA	max
<g/>
.	.	kIx.	.
záporného	záporný	k2eAgInSc2d1	záporný
oxidačního	oxidační	k2eAgInSc2d1	oxidační
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
přijmout	přijmout	k5eAaPmF	přijmout
tři	tři	k4xCgInPc4	tři
elektrony	elektron	k1gInPc4	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
max	max	kA	max
<g/>
.	.	kIx.	.
kladného	kladný	k2eAgInSc2d1	kladný
oxidačního	oxidační	k2eAgInSc2d1	oxidační
stavu	stav	k1gInSc2	stav
V	V	kA	V
musí	muset	k5eAaImIp3nS	muset
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
pět	pět	k4xCc4	pět
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
elektronegativitě	elektronegativita	k1gFnSc3	elektronegativita
dusík	dusík	k1gInSc4	dusík
nemůže	moct	k5eNaImIp3nS	moct
spontánně	spontánně	k6eAd1	spontánně
přesunout	přesunout	k5eAaPmF	přesunout
svojí	svojit	k5eAaImIp3nS	svojit
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
hustotu	hustota	k1gFnSc4	hustota
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vazeb	vazba	k1gFnPc2	vazba
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
elektronegativnějšími	elektronegativný	k2eAgInPc7d2	elektronegativný
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
elektronegativita	elektronegativita	k1gFnSc1	elektronegativita
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
na	na	k7c4	na
"	"	kIx"	"
<g/>
přetáhnutí	přetáhnutí	k1gNnSc4	přetáhnutí
<g/>
"	"	kIx"	"
vazebných	vazebný	k2eAgInPc2d1	vazebný
elektronů	elektron	k1gInPc2	elektron
do	do	k7c2	do
valenční	valenční	k2eAgFnSc2d1	valenční
sféry	sféra	k1gFnSc2	sféra
při	při	k7c6	při
vazbě	vazba	k1gFnSc6	vazba
s	s	k7c7	s
elektropozitivními	elektropozitivní	k2eAgInPc7d1	elektropozitivní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
dusíku	dusík	k1gInSc2	dusík
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
svých	svůj	k3xOyFgFnPc2	svůj
sloučenin	sloučenina	k1gFnPc2	sloučenina
zapojeny	zapojit	k5eAaPmNgFnP	zapojit
v	v	k7c6	v
kovalentních	kovalentní	k2eAgFnPc6d1	kovalentní
vazbách	vazba	k1gFnPc6	vazba
a	a	k8xC	a
jedno-atomové	jednotomový	k2eAgInPc4d1	jedno-atomový
ionty	ion	k1gInPc4	ion
téměř	téměř	k6eAd1	téměř
netvoří	tvořit	k5eNaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
u	u	k7c2	u
dusíku	dusík	k1gInSc2	dusík
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vazby	vazba	k1gFnPc1	vazba
homonukleární	homonukleární	k2eAgFnPc1d1	homonukleární
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
pestrosti	pestrost	k1gFnSc3	pestrost
vazebných	vazebný	k2eAgFnPc2d1	vazebná
situací	situace	k1gFnPc2	situace
se	se	k3xPyFc4	se
dusík	dusík	k1gInSc1	dusík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stavech	stav	k1gInPc6	stav
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
III	III	kA	III
až	až	k8xS	až
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koloběh	koloběh	k1gInSc1	koloběh
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elementární	elementární	k2eAgFnSc6d1	elementární
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
setkáváme	setkávat	k5eAaImIp1nP	setkávat
prakticky	prakticky	k6eAd1	prakticky
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
totiž	totiž	k9	totiž
78	[number]	k4	78
%	%	kIx~	%
(	(	kIx(	(
<g/>
objemových	objemový	k2eAgInPc2d1	objemový
<g/>
)	)	kIx)	)
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
amoniak	amoniak	k1gInSc1	amoniak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
tlením	tlení	k1gNnSc7	tlení
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
při	při	k7c6	při
elektrickém	elektrický	k2eAgInSc6d1	elektrický
výboji	výboj	k1gInSc6	výboj
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
blesku	blesk	k1gInSc3	blesk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
blesku	blesk	k1gInSc6	blesk
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
dusnatého	dusnatý	k2eAgInSc2d1	dusnatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ihned	ihned	k6eAd1	ihned
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
dusičitého	dusičitý	k2eAgInSc2d1	dusičitý
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
a	a	k8xC	a
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
kyselých	kyselý	k2eAgInPc6d1	kyselý
deštích	dešť	k1gInPc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
anorganických	anorganický	k2eAgFnPc2d1	anorganická
solí	sůl	k1gFnPc2	sůl
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
dávno	dávno	k6eAd1	dávno
spláchnuty	spláchnut	k2eAgMnPc4d1	spláchnut
do	do	k7c2	do
oceánů	oceán	k1gInPc2	oceán
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zapojily	zapojit	k5eAaPmAgInP	zapojit
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
biologických	biologický	k2eAgInPc2d1	biologický
cyklů	cyklus	k1gInPc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
chilský	chilský	k2eAgInSc4d1	chilský
ledek	ledek	k1gInSc4	ledek
neboli	neboli	k8xC	neboli
dusičnan	dusičnan	k1gInSc4	dusičnan
sodný	sodný	k2eAgInSc4d1	sodný
NaNO	NaNO	k1gFnSc7	NaNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
rozkladem	rozklad	k1gInSc7	rozklad
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
a	a	k8xC	a
živočišných	živočišný	k2eAgFnPc2d1	živočišná
látek	látka	k1gFnPc2	látka
zejména	zejména	k9	zejména
na	na	k7c6	na
chilském	chilský	k2eAgNnSc6d1	Chilské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
organického	organický	k2eAgInSc2d1	organický
dusíku	dusík	k1gInSc2	dusík
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
objemné	objemný	k2eAgFnPc1d1	objemná
vrstvy	vrstva	k1gFnPc1	vrstva
ptačího	ptačí	k2eAgInSc2d1	ptačí
trusu	trus	k1gInSc2	trus
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
guano	guano	k1gNnSc1	guano
a	a	k8xC	a
využívané	využívaný	k2eAgFnPc1d1	využívaná
především	především	k9	především
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
biogenní	biogenní	k2eAgInSc1d1	biogenní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
významných	významný	k2eAgFnPc6d1	významná
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
a	a	k8xC	a
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnSc2	rostlina
ho	on	k3xPp3gMnSc4	on
přijímají	přijímat	k5eAaImIp3nP	přijímat
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
růstu	růst	k1gInSc3	růst
a	a	k8xC	a
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Živočichové	živočich	k1gMnPc1	živočich
ho	on	k3xPp3gMnSc4	on
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
močoviny	močovina	k1gFnSc2	močovina
<g/>
,	,	kIx,	,
amoniaku	amoniak	k1gInSc2	amoniak
nebo	nebo	k8xC	nebo
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýhodnější	výhodný	k2eAgFnSc1d3	nejvýhodnější
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
dusíku	dusík	k1gInSc2	dusík
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zahříváním	zahřívání	k1gNnSc7	zahřívání
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
roztoku	roztok	k1gInSc2	roztok
dusitanu	dusitan	k1gInSc2	dusitan
amonného	amonný	k2eAgInSc2d1	amonný
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnPc1	směs
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
a	a	k8xC	a
dusitanu	dusitan	k1gInSc2	dusitan
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
odstranily	odstranit	k5eAaPmAgFnP	odstranit
stopy	stopa	k1gFnPc1	stopa
přimíšených	přimíšený	k2eAgMnPc2d1	přimíšený
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
promývá	promývat	k5eAaImIp3nS	promývat
se	se	k3xPyFc4	se
dusík	dusík	k1gInSc1	dusík
směsí	směs	k1gFnPc2	směs
dichromanu	dichroman	k1gMnSc3	dichroman
draselného	draselný	k2eAgMnSc2d1	draselný
a	a	k8xC	a
kyseliny	kyselina	k1gFnPc4	kyselina
sírové	sírový	k2eAgFnPc4d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
NH4NO2	NH4NO2	k4	NH4NO2
→	→	k?	→
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dusík	dusík	k1gInSc1	dusík
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
připravuje	připravovat	k5eAaImIp3nS	připravovat
vedením	vedení	k1gNnSc7	vedení
vzduchu	vzduch	k1gInSc2	vzduch
přes	přes	k7c4	přes
rozžhavenou	rozžhavený	k2eAgFnSc4d1	rozžhavená
měď	měď	k1gFnSc4	měď
<g/>
.	.	kIx.	.
</s>
<s>
Měď	měď	k1gFnSc1	měď
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
černý	černý	k2eAgInSc1d1	černý
oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
dusík	dusík	k1gInSc1	dusík
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzduch	vzduch	k1gInSc1	vzduch
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
%	%	kIx~	%
argonu	argon	k1gInSc2	argon
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
vzácných	vzácný	k2eAgInPc2d1	vzácný
a	a	k8xC	a
netečných	netečný	k2eAgInPc2d1	netečný
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
dusíku	dusík	k1gInSc3	dusík
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
atmosferický	atmosferický	k2eAgInSc1d1	atmosferický
dusík	dusík	k1gInSc1	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možná	možný	k2eAgFnSc1d1	možná
příprava	příprava	k1gFnSc1	příprava
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
získáme	získat	k5eAaPmIp1nP	získat
obzvláště	obzvláště	k6eAd1	obzvláště
čistý	čistý	k2eAgInSc4d1	čistý
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tepelný	tepelný	k2eAgInSc4d1	tepelný
rozklad	rozklad	k1gInSc4	rozklad
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
postupu	postup	k1gInSc6	postup
vedeme	vést	k5eAaImIp1nP	vést
amoniak	amoniak	k1gInSc4	amoniak
přes	přes	k7c4	přes
práškový	práškový	k2eAgInSc4d1	práškový
nikl	nikl	k1gInSc4	nikl
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
Vodík	vodík	k1gInSc4	vodík
poté	poté	k6eAd1	poté
od	od	k7c2	od
dusíku	dusík	k1gInSc2	dusík
oddělíme	oddělit	k5eAaPmIp1nP	oddělit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odlišných	odlišný	k2eAgFnPc2d1	odlišná
teplot	teplota	k1gFnPc2	teplota
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
technicky	technicky	k6eAd1	technicky
připravoval	připravovat	k5eAaImAgMnS	připravovat
vedením	vedení	k1gNnSc7	vedení
vzduchu	vzduch	k1gInSc2	vzduch
přes	přes	k7c4	přes
rozžhavené	rozžhavený	k2eAgNnSc4d1	rozžhavené
uhlí	uhlí	k1gNnSc4	uhlí
nebo	nebo	k8xC	nebo
koks	koks	k1gInSc4	koks
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
spálí	spálit	k5eAaPmIp3nS	spálit
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
od	od	k7c2	od
dusíku	dusík	k1gInSc2	dusík
odstraní	odstranit	k5eAaPmIp3nP	odstranit
promýváním	promývání	k1gNnPc3	promývání
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získaný	získaný	k2eAgInSc1d1	získaný
dusík	dusík	k1gInSc1	dusík
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
%	%	kIx~	%
vzácných	vzácný	k2eAgInPc2d1	vzácný
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
netečných	tečný	k2eNgInPc2d1	netečný
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
atmosferický	atmosferický	k2eAgInSc1d1	atmosferický
dusík	dusík	k1gInSc1	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
prakticky	prakticky	k6eAd1	prakticky
výlučně	výlučně	k6eAd1	výlučně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nízkoteplotní	nízkoteplotní	k2eAgFnSc7d1	nízkoteplotní
rektifikací	rektifikace	k1gFnSc7	rektifikace
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
přitom	přitom	k6eAd1	přitom
spíše	spíše	k9	spíše
přebytky	přebytek	k1gInPc1	přebytek
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
více	hodně	k6eAd2	hodně
žádaného	žádaný	k2eAgInSc2d1	žádaný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postupném	postupný	k2eAgNnSc6d1	postupné
ochlazování	ochlazování	k1gNnSc6	ochlazování
nejprve	nejprve	k6eAd1	nejprve
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
kapalného	kapalný	k2eAgNnSc2d1	kapalné
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zkapalnění	zkapalnění	k1gNnSc3	zkapalnění
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
argonem	argon	k1gInSc7	argon
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
plynné	plynný	k2eAgFnPc4d1	plynná
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
odděleno	oddělen	k2eAgNnSc1d1	odděleno
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
jiných	jiný	k2eAgInPc2d1	jiný
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapalná	kapalný	k2eAgFnSc1d1	kapalná
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dělena	dělit	k5eAaImNgFnS	dělit
v	v	k7c6	v
rektifikační	rektifikační	k2eAgFnSc6d1	rektifikační
koloně	kolona	k1gFnSc6	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
o	o	k7c6	o
frakční	frakční	k2eAgFnSc6d1	frakční
destilaci	destilace	k1gFnSc6	destilace
vyučovaná	vyučovaný	k2eAgFnSc1d1	vyučovaná
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
realitě	realita	k1gFnSc3	realita
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
velkokapacitní	velkokapacitní	k2eAgFnSc1d1	velkokapacitní
výroba	výroba	k1gFnSc1	výroba
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
realizována	realizován	k2eAgFnSc1d1	realizována
např.	např.	kA	např.
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Litvínov-Záluží	Litvínov-Záluží	k1gFnSc2	Litvínov-Záluží
(	(	kIx(	(
<g/>
areál	areál	k1gInSc1	areál
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Unipetrol	Unipetrol	k1gInSc1	Unipetrol
RPA	RPA	kA	RPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
přímého	přímý	k2eAgNnSc2d1	přímé
expedování	expedování	k1gNnSc2	expedování
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
důležitý	důležitý	k2eAgInSc1d1	důležitý
prvek	prvek	k1gInSc1	prvek
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
umístěných	umístěný	k2eAgFnPc2d1	umístěná
chemických	chemický	k2eAgFnPc2d1	chemická
výrob	výroba	k1gFnPc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
a	a	k8xC	a
následně	následně	k6eAd1	následně
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
látka	látka	k1gFnSc1	látka
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dalších	další	k2eAgFnPc2d1	další
amonných	amonný	k2eAgFnPc2d1	amonná
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
sloučenin	sloučenina	k1gFnPc2	sloučenina
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
jako	jako	k8xS	jako
významné	významný	k2eAgNnSc1d1	významné
oxidovadlo	oxidovadlo	k1gNnSc1	oxidovadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
anorganickým	anorganický	k2eAgFnPc3d1	anorganická
a	a	k8xC	a
organickým	organický	k2eAgFnPc3d1	organická
syntézám	syntéza	k1gFnPc3	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Plynný	plynný	k2eAgInSc1d1	plynný
dusík	dusík	k1gInSc1	dusík
nalézá	nalézat	k5eAaImIp3nS	nalézat
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
inertní	inertní	k2eAgFnSc1d1	inertní
atmosféra	atmosféra	k1gFnSc1	atmosféra
např.	např.	kA	např.
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
obalů	obal	k1gInPc2	obal
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zmačkání	zmačkání	k1gNnSc3	zmačkání
a	a	k8xC	a
zvlhčení	zvlhčení	k1gNnSc3	zvlhčení
-	-	kIx~	-
například	například	k6eAd1	například
sáčky	sáček	k1gInPc4	sáček
s	s	k7c7	s
brambůrky	brambůrek	k1gInPc7	brambůrek
<g/>
.	.	kIx.	.
</s>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
dusík	dusík	k1gInSc1	dusík
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kryogenních	kryogenní	k2eAgInPc2d1	kryogenní
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
udržet	udržet	k5eAaPmF	udržet
prostředí	prostředí	k1gNnSc4	prostředí
na	na	k7c6	na
značně	značně	k6eAd1	značně
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
uchovávání	uchovávání	k1gNnSc1	uchovávání
tkání	tkáň	k1gFnPc2	tkáň
nebo	nebo	k8xC	nebo
spermií	spermie	k1gFnPc2	spermie
a	a	k8xC	a
vajíček	vajíčko	k1gNnPc2	vajíčko
v	v	k7c6	v
lázni	lázeň	k1gFnSc6	lázeň
z	z	k7c2	z
kapalného	kapalný	k2eAgInSc2d1	kapalný
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Kapalným	kapalný	k2eAgInSc7d1	kapalný
dusíkem	dusík	k1gInSc7	dusík
jsou	být	k5eAaImIp3nP	být
chlazeny	chlazen	k2eAgInPc1d1	chlazen
polovodičové	polovodičový	k2eAgInPc1d1	polovodičový
detektory	detektor	k1gInPc1	detektor
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
spektrometrických	spektrometrický	k2eAgFnPc6d1	spektrometrická
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
nekrotizaci	nekrotizace	k1gFnSc3	nekrotizace
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Dusíkatá	dusíkatý	k2eAgNnPc1d1	dusíkaté
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rostlinám	rostlina	k1gFnPc3	rostlina
dodávají	dodávat	k5eAaImIp3nP	dodávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rostly	růst	k5eAaImAgFnP	růst
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
dusík	dusík	k1gInSc1	dusík
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
a	a	k8xC	a
plně	plně	k6eAd1	plně
ho	on	k3xPp3gMnSc4	on
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
hnojeny	hnojit	k5eAaImNgFnP	hnojit
nadbytkem	nadbytkem	k6eAd1	nadbytkem
hnojiv	hnojivo	k1gNnPc2	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
poznat	poznat	k5eAaPmF	poznat
podle	podle	k7c2	podle
dužnatých	dužnatý	k2eAgFnPc2d1	dužnatá
tkání	tkáň	k1gFnPc2	tkáň
křehkých	křehký	k2eAgInPc2d1	křehký
dužnatých	dužnatý	k2eAgInPc2d1	dužnatý
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
,	,	kIx,	,
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
poškodí	poškodit	k5eAaPmIp3nS	poškodit
a	a	k8xC	a
také	také	k9	také
více	hodně	k6eAd2	hodně
trpí	trpět	k5eAaImIp3nS	trpět
chorobami	choroba	k1gFnPc7	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc7	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
NH3	NH3	k1gFnSc2	NH3
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvyužívanějších	využívaný	k2eAgNnPc2d3	nejvyužívanější
hnojiv	hnojivo	k1gNnPc2	hnojivo
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Plynný	plynný	k2eAgInSc1d1	plynný
amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
stává	stávat	k5eAaImIp3nS	stávat
náhradou	náhrada	k1gFnSc7	náhrada
freonů	freon	k1gInPc2	freon
v	v	k7c6	v
chladírenství	chladírenství	k1gNnSc6	chladírenství
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
přímou	přímý	k2eAgFnSc7d1	přímá
syntézou	syntéza	k1gFnSc7	syntéza
z	z	k7c2	z
plynů	plyn	k1gInPc2	plyn
tzv.	tzv.	kA	tzv.
Haberovým	Haberův	k2eAgInSc7d1	Haberův
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
NH4NO3	NH4NO3	k1gFnSc4	NH4NO3
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnSc1d1	další
často	často	k6eAd1	často
používané	používaný	k2eAgNnSc1d1	používané
hnojivo	hnojivo	k1gNnSc1	hnojivo
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
síran	síran	k1gInSc1	síran
amonný	amonný	k2eAgInSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
a	a	k8xC	a
dusíkaté	dusíkatý	k2eAgNnSc4d1	dusíkaté
vápno	vápno	k1gNnSc4	vápno
neboli	neboli	k8xC	neboli
kyanamid	kyanamid	k1gInSc4	kyanamid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
CaCN	CaCN	k1gFnSc7	CaCN
<g/>
2	[number]	k4	2
využívá	využívat	k5eAaPmIp3nS	využívat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
amonný	amonný	k2eAgInSc1d1	amonný
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
,	,	kIx,	,
bengálských	bengálský	k2eAgInPc2d1	bengálský
ohňů	oheň	k1gInPc2	oheň
a	a	k8xC	a
samozápalných	samozápalný	k2eAgFnPc2d1	samozápalná
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Močovina	močovina	k1gFnSc1	močovina
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CO	co	k6eAd1	co
neboli	neboli	k8xC	neboli
diamid	diamid	k1gInSc1	diamid
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
se	se	k3xPyFc4	se
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
využívá	využívat	k5eAaPmIp3nS	využívat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
levná	levný	k2eAgFnSc1d1	levná
<g/>
.	.	kIx.	.
</s>
<s>
Močovina	močovina	k1gFnSc1	močovina
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
kopolymerů	kopolymer	k1gInPc2	kopolymer
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
močovinoformaldehydové	močovinoformaldehydový	k2eAgFnPc1d1	močovinoformaldehydový
pryskyřice	pryskyřice	k1gFnPc1	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
dusičnany	dusičnan	k1gInPc1	dusičnan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
významné	významný	k2eAgInPc4d1	významný
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tak	tak	k6eAd1	tak
komplexní	komplexní	k2eAgNnPc4d1	komplexní
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dusičnan	dusičnan	k1gInSc4	dusičnan
sodný	sodný	k2eAgInSc4d1	sodný
NaNO	NaNO	k1gFnSc7	NaNO
<g/>
3	[number]	k4	3
a	a	k8xC	a
dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
dusíku	dusík	k1gInSc2	dusík
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
třeba	třeba	k6eAd1	třeba
zefektivnit	zefektivnit	k5eAaPmF	zefektivnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
například	například	k6eAd1	například
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
zbytečnému	zbytečný	k2eAgNnSc3d1	zbytečné
znečišťování	znečišťování	k1gNnSc3	znečišťování
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
vlastností	vlastnost	k1gFnPc2	vlastnost
sloučenin	sloučenina	k1gFnPc2	sloučenina
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
dávnověku	dávnověk	k1gInSc2	dávnověk
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
explozivních	explozivní	k2eAgFnPc2d1	explozivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
výroba	výroba	k1gFnSc1	výroba
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
složku	složka	k1gFnSc4	složka
tvoří	tvořit	k5eAaImIp3nS	tvořit
dusičnan	dusičnan	k1gInSc1	dusičnan
sodný	sodný	k2eAgInSc1d1	sodný
nebo	nebo	k8xC	nebo
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
spíše	spíše	k9	spíše
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nitroglycerin	nitroglycerin	k1gInSc4	nitroglycerin
nebo	nebo	k8xC	nebo
trinitrotoluen	trinitrotoluen	k1gInSc4	trinitrotoluen
(	(	kIx(	(
<g/>
TNT	TNT	kA	TNT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
paliva	palivo	k1gNnPc4	palivo
raketových	raketový	k2eAgInPc2d1	raketový
motorů	motor	k1gInPc2	motor
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používala	používat	k5eAaImAgFnS	používat
jak	jak	k6eAd1	jak
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
jako	jako	k8xS	jako
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hydrazin	hydrazin	k1gInSc1	hydrazin
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
spalovaného	spalovaný	k2eAgInSc2d1	spalovaný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
dusíku	dusík	k1gInSc2	dusík
jako	jako	k8xC	jako
například	například	k6eAd1	například
aminy	amin	k1gInPc1	amin
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
organické	organický	k2eAgInPc1d1	organický
deriváty	derivát	k1gInPc1	derivát
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
indikátorů	indikátor	k1gInPc2	indikátor
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
obsahu	obsah	k1gInSc2	obsah
plynného	plynný	k2eAgInSc2d1	plynný
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
dušení	dušení	k1gNnSc3	dušení
<g/>
,	,	kIx,	,
ztrátě	ztráta	k1gFnSc3	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
až	až	k6eAd1	až
relativně	relativně	k6eAd1	relativně
netraumatické	traumatický	k2eNgFnSc2d1	traumatický
smrti	smrt	k1gFnSc2	smrt
vlivem	vlivem	k7c2	vlivem
nedostatku	nedostatek	k1gInSc2	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
anorganické	anorganický	k2eAgFnPc4d1	anorganická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
dusíku	dusík	k1gInSc2	dusík
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
amoniak	amoniak	k1gInSc1	amoniak
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
oxidy	oxid	k1gInPc1	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
dusíkaté	dusíkatý	k2eAgFnSc2d1	dusíkatá
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
peroxokyseliny	peroxokyselina	k1gFnSc2	peroxokyselina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
deriváty	derivát	k1gInPc7	derivát
amoniaku	amoniak	k1gInSc2	amoniak
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
amidy	amid	k1gInPc1	amid
<g/>
,	,	kIx,	,
imidy	imid	k1gInPc1	imid
a	a	k8xC	a
nitridy	nitrid	k1gInPc1	nitrid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
nahrazováním	nahrazování	k1gNnSc7	nahrazování
atomů	atom	k1gInPc2	atom
vodíků	vodík	k1gInPc2	vodík
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
molekule	molekula	k1gFnSc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hydrazin	hydrazin	k1gInSc1	hydrazin
<g/>
,	,	kIx,	,
hydroxylamin	hydroxylamin	k1gInSc1	hydroxylamin
<g/>
,	,	kIx,	,
halogenidy	halogenid	k1gInPc1	halogenid
a	a	k8xC	a
oxidohalogenidy	oxidohalogenid	k1gInPc1	oxidohalogenid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnSc2	sloučenina
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
NH3	NH3	k1gFnSc2	NH3
je	být	k5eAaImIp3nS	být
plyn	plyn	k1gInSc1	plyn
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
štiplavý	štiplavý	k2eAgInSc4d1	štiplavý
zápach	zápach	k1gInSc4	zápach
<g/>
,	,	kIx,	,
leptá	leptat	k5eAaImIp3nS	leptat
sliznice	sliznice	k1gFnSc1	sliznice
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc1	hnojivo
a	a	k8xC	a
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
dalších	další	k2eAgFnPc2d1	další
anorganických	anorganický	k2eAgFnPc2d1	anorganická
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
amoniaku	amoniak	k1gInSc2	amoniak
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
tvoří	tvořit	k5eAaImIp3nP	tvořit
z	z	k7c2	z
části	část	k1gFnSc2	část
molekul	molekula	k1gFnPc2	molekula
amoniaku	amoniak	k1gInSc2	amoniak
tzv.	tzv.	kA	tzv.
hydroxid	hydroxid	k1gInSc4	hydroxid
amonný	amonný	k2eAgInSc1d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
Derivát	derivát	k1gInSc1	derivát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
jednoho	jeden	k4xCgInSc2	jeden
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
amid	amid	k1gInSc1	amid
nebo	nebo	k8xC	nebo
amin	amin	k1gInSc1	amin
<g/>
.	.	kIx.	.
</s>
<s>
Amidy	amid	k1gInPc1	amid
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
nahrazen	nahradit	k5eAaPmNgInS	nahradit
kovem	kov	k1gInSc7	kov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
amid	amid	k1gInSc4	amid
sodný	sodný	k2eAgInSc4d1	sodný
NaNH	NaNH	k1gFnSc7	NaNH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
představit	představit	k5eAaPmF	představit
odvozené	odvozený	k2eAgFnPc1d1	odvozená
náhradou	náhrada	k1gFnSc7	náhrada
skupiny	skupina	k1gFnSc2	skupina
OH	OH	kA	OH
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c4	za
skupinu	skupina	k1gFnSc4	skupina
-NH2	-NH2	k4	-NH2
(	(	kIx(	(
<g/>
např.	např.	kA	např.
diamid	diamid	k1gInSc4	diamid
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
neboli	neboli	k8xC	neboli
močovina	močovina	k1gFnSc1	močovina
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
aminy	amin	k1gInPc1	amin
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
ostatní	ostatní	k2eAgFnPc1d1	ostatní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chloramin	chloramin	k2eAgInSc4d1	chloramin
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnPc1	Cl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
anorganické	anorganický	k2eAgFnPc4d1	anorganická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
tyto	tento	k3xDgInPc1	tento
názvy	název	k1gInPc1	název
označují	označovat	k5eAaImIp3nP	označovat
jiné	jiný	k2eAgFnPc1d1	jiná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Derivát	derivát	k1gInSc1	derivát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
dvou	dva	k4xCgInPc2	dva
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
imid	imid	k1gInSc1	imid
nebo	nebo	k8xC	nebo
imin	imin	k1gInSc1	imin
<g/>
.	.	kIx.	.
</s>
<s>
Anion	anion	k1gInSc1	anion
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
>	>	kIx)	>
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
imidem	imid	k1gInSc7	imid
a	a	k8xC	a
iminem	imin	k1gInSc7	imin
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
těchto	tento	k3xDgInPc2	tento
názvů	název	k1gInPc2	název
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
u	u	k7c2	u
amidu	amid	k1gInSc2	amid
a	a	k8xC	a
aminu	amin	k1gInSc2	amin
<g/>
.	.	kIx.	.
</s>
<s>
Derivát	derivát	k1gInSc1	derivát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
odtržením	odtržení	k1gNnSc7	odtržení
všech	všecek	k3xTgInPc2	všecek
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
z	z	k7c2	z
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nitrid	nitrid	k1gInSc1	nitrid
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
)	)	kIx)	)
nitril	nitril	k1gInSc4	nitril
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nitril	nitril	k1gInSc1	nitril
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kyselina	kyselina	k1gFnSc1	kyselina
nitrilosulfonová	nitrilosulfonová	k1gFnSc1	nitrilosulfonová
N	N	kA	N
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nitridy	nitrid	k1gInPc1	nitrid
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
dvouprvkové	dvouprvkový	k2eAgFnPc1d1	dvouprvková
sloučeniny	sloučenina	k1gFnPc1	sloučenina
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgFnPc4d1	pevná
látky	látka	k1gFnPc4	látka
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Hydrazin	hydrazin	k1gInSc1	hydrazin
N2H4	N2H4	k1gFnSc2	N2H4
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
silně	silně	k6eAd1	silně
dýmající	dýmající	k2eAgFnSc1d1	dýmající
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zásaditý	zásaditý	k2eAgInSc4d1	zásaditý
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
soli	sůl	k1gFnPc4	sůl
hydrazínia	hydrazínium	k1gNnSc2	hydrazínium
<g/>
.	.	kIx.	.
</s>
<s>
Hydrazin	hydrazin	k1gInSc1	hydrazin
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
je	on	k3xPp3gNnSc4	on
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
a	a	k8xC	a
raketové	raketový	k2eAgNnSc4d1	raketové
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxylamin	Hydroxylamin	k2eAgMnSc1d1	Hydroxylamin
NH2OH	NH2OH	k1gMnSc1	NH2OH
nelze	lze	k6eNd1	lze
snadno	snadno	k6eAd1	snadno
připravit	připravit	k5eAaPmF	připravit
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
látka	látka	k1gFnSc1	látka
snadno	snadno	k6eAd1	snadno
detonuje	detonovat	k5eAaBmIp3nS	detonovat
<g/>
.	.	kIx.	.
</s>
<s>
Stabilnější	stabilní	k2eAgFnPc1d2	stabilnější
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
hydroxylaminia	hydroxylaminium	k1gNnSc2	hydroxylaminium
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
hydroxylaminu	hydroxylamin	k1gInSc2	hydroxylamin
reaguje	reagovat	k5eAaBmIp3nS	reagovat
silně	silně	k6eAd1	silně
zásaditě	zásaditě	k6eAd1	zásaditě
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
halogenidy	halogenid	k1gMnPc7	halogenid
dusíku	dusík	k1gInSc2	dusík
řadíme	řadit	k5eAaImIp1nP	řadit
fluorodusík	fluorodusík	k1gInSc4	fluorodusík
NF	NF	kA	NF
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
chlorodusík	chlorodusík	k1gInSc4	chlorodusík
NCl	NCl	k1gFnSc2	NCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
jododusík	jododusík	k1gInSc4	jododusík
NI	on	k3xPp3gFnSc4	on
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Bromodusík	Bromodusík	k1gInSc1	Bromodusík
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
čistý	čistý	k2eAgInSc4d1	čistý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
je	být	k5eAaImIp3nS	být
amoniakát	amoniakát	k5eAaPmF	amoniakát
NBr	NBr	k1gFnSc7	NBr
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
6	[number]	k4	6
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Fluorodusík	Fluorodusík	k1gInSc1	Fluorodusík
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
chlorodusík	chlorodusík	k1gInSc4	chlorodusík
těkavá	těkavý	k2eAgFnSc1d1	těkavá
tmavě	tmavě	k6eAd1	tmavě
žlutá	žlutý	k2eAgFnSc1d1	žlutá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
a	a	k8xC	a
jododusík	jododusík	k1gInSc1	jododusík
hnědočervená	hnědočervený	k2eAgFnSc1d1	hnědočervená
pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
explozivní	explozivní	k2eAgFnSc1d1	explozivní
<g/>
,	,	kIx,	,
jododusík	jododusík	k1gInSc1	jododusík
již	již	k6eAd1	již
při	při	k7c6	při
dotyku	dotyk	k1gInSc6	dotyk
<g/>
.	.	kIx.	.
</s>
<s>
Oxidohalogenidy	Oxidohalogenida	k1gFnPc1	Oxidohalogenida
dusíku	dusík	k1gInSc2	dusík
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kromě	kromě	k7c2	kromě
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
halogenu	halogen	k1gInSc2	halogen
ještě	ještě	k9	ještě
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stechiometrické	stechiometrický	k2eAgNnSc4d1	stechiometrické
složení	složení	k1gNnSc4	složení
NOF	NOF	kA	NOF
<g/>
,	,	kIx,	,
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
NOCl	NOCl	k1gInSc1	NOCl
<g/>
,	,	kIx,	,
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnPc2	Cl
a	a	k8xC	a
NOBr	NOBra	k1gFnPc2	NOBra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
pokojové	pokojový	k2eAgFnSc2d1	pokojová
teploty	teplota	k1gFnSc2	teplota
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
NOCl	NOCl	k1gInSc4	NOCl
a	a	k8xC	a
NOCl	NOCl	k1gInSc4	NOCl
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
sloučeniny	sloučenina	k1gFnPc4	sloučenina
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
patří	patřit	k5eAaImIp3nS	patřit
několik	několik	k4yIc1	několik
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
mají	mít	k5eAaImIp3nP	mít
složení	složený	k2eAgMnPc1d1	složený
S4N4	S4N4	k1gMnPc1	S4N4
tetranitrid	tetranitrida	k1gFnPc2	tetranitrida
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
S4N2	S4N2	k1gFnSc2	S4N2
dinitrid	dinitrida	k1gFnPc2	dinitrida
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgFnPc4d1	pevná
krystalické	krystalický	k2eAgFnPc4d1	krystalická
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
detonují	detonovat	k5eAaBmIp3nP	detonovat
a	a	k8xC	a
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
síru	síra	k1gFnSc4	síra
<g/>
.	.	kIx.	.
</s>
<s>
Oxidy	oxid	k1gInPc1	oxid
dusíku	dusík	k1gInSc2	dusík
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
valence	valence	k1gFnSc2	valence
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
až	až	k8xS	až
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Oxidy	oxid	k1gInPc4	oxid
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
mocenstvím	mocenství	k1gNnSc7	mocenství
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
až	až	k8xS	až
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
složkami	složka	k1gFnPc7	složka
tzv.	tzv.	kA	tzv.
suchého	suchý	k2eAgInSc2d1	suchý
neboli	neboli	k8xC	neboli
losangelského	losangelský	k2eAgInSc2d1	losangelský
smogu	smog	k1gInSc2	smog
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusný	dusný	k2eAgInSc1d1	dusný
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
rajský	rajský	k2eAgInSc1d1	rajský
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
slabého	slabý	k2eAgInSc2d1	slabý
zápachu	zápach	k1gInSc2	zápach
a	a	k8xC	a
nasládlé	nasládlý	k2eAgFnSc2d1	nasládlá
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
narkotikum	narkotikum	k1gNnSc1	narkotikum
při	při	k7c6	při
chirurgických	chirurgický	k2eAgFnPc6d1	chirurgická
operacích	operace	k1gFnPc6	operace
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
hnací	hnací	k2eAgInSc1d1	hnací
plyn	plyn	k1gInSc1	plyn
ve	v	k7c6	v
sprejích	sprej	k1gInPc6	sprej
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusnatý	dusnatý	k2eAgInSc1d1	dusnatý
NO	no	k9	no
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
dusičitý	dusičitý	k2eAgInSc4d1	dusičitý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
inertní	inertní	k2eAgInPc4d1	inertní
oxidy	oxid	k1gInPc4	oxid
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důležitý	důležitý	k2eAgInSc1d1	důležitý
meziprodukt	meziprodukt	k1gInSc1	meziprodukt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusitý	dusitý	k2eAgInSc1d1	dusitý
N2O3	N2O3	k1gFnSc4	N2O3
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
modrá	modrý	k2eAgFnSc1d1	modrá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
rychle	rychle	k6eAd1	rychle
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
dusnatý	dusnatý	k2eAgInSc4d1	dusnatý
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
dusičitý	dusičitý	k2eAgInSc4d1	dusičitý
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgNnSc1d1	stabilní
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
oxidů	oxid	k1gInPc2	oxid
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusičitý	dusičitý	k2eAgMnSc1d1	dusičitý
NO2	NO2	k1gMnSc1	NO2
je	být	k5eAaImIp3nS	být
hnědočervený	hnědočervený	k2eAgMnSc1d1	hnědočervený
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
plyn	plyn	k1gInSc4	plyn
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
za	za	k7c2	za
pokojové	pokojový	k2eAgFnSc2d1	pokojová
teploty	teplota	k1gFnSc2	teplota
dimeruje	dimerovat	k5eAaBmIp3nS	dimerovat
na	na	k7c4	na
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusičitý	dusičitý	k2eAgInSc1d1	dusičitý
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgInSc7d1	poslední
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
dusité	dusitý	k2eAgFnSc2d1	dusitá
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusičný	dusičný	k2eAgInSc1d1	dusičný
N2O5	N2O5	k1gFnSc4	N2O5
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rychle	rychle	k6eAd1	rychle
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
dusičný	dusičný	k2eAgInSc1d1	dusičný
není	být	k5eNaImIp3nS	být
stabilní	stabilní	k2eAgInSc1d1	stabilní
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
bez	bez	k7c2	bez
vnější	vnější	k2eAgFnSc2d1	vnější
příčiny	příčina	k1gFnSc2	příčina
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
ozonem	ozon	k1gInSc7	ozon
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
sloučeninu	sloučenina	k1gFnSc4	sloučenina
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
složení	složení	k1gNnSc4	složení
NO3	NO3	k1gFnSc2	NO3
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
peroxid	peroxid	k1gInSc1	peroxid
nitrosylu	nitrosyl	k1gInSc2	nitrosyl
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgFnSc7d3	nejstálejší
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
kyselinou	kyselina	k1gFnSc7	kyselina
dusíku	dusík	k1gInSc2	dusík
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
známé	známý	k2eAgFnPc1d1	známá
kyseliny	kyselina	k1gFnPc1	kyselina
dusíku	dusík	k1gInSc2	dusík
jsou	být	k5eAaImIp3nP	být
kyselina	kyselina	k1gFnSc1	kyselina
dusitá	dusitý	k2eAgFnSc1d1	dusitá
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
azidovodíková	azidovodíková	k1gFnSc1	azidovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
téměř	téměř	k6eAd1	téměř
neznámé	známý	k2eNgFnSc2d1	neznámá
kyseliny	kyselina	k1gFnSc2	kyselina
dusíku	dusík	k1gInSc2	dusík
jsou	být	k5eAaImIp3nP	být
kyselina	kyselina	k1gFnSc1	kyselina
dusná	dusný	k2eAgFnSc1d1	dusná
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
dusnatá	dusnatý	k2eAgFnSc1d1	dusnatý
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
azidovodíková	azidovodíková	k1gFnSc1	azidovodíková
HN3	HN3	k1gFnSc2	HN3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgNnPc1d1	bezbarvé
<g/>
,	,	kIx,	,
ostře	ostro	k6eAd1	ostro
páchnoucí	páchnoucí	k2eAgFnSc1d1	páchnoucí
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
jedovatými	jedovatý	k2eAgFnPc7d1	jedovatá
parami	para	k1gFnPc7	para
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
explodují	explodovat	k5eAaBmIp3nP	explodovat
velmi	velmi	k6eAd1	velmi
prudce	prudko	k6eAd1	prudko
pokud	pokud	k8xS	pokud
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
horkým	horký	k2eAgInSc7d1	horký
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
azidovodíková	azidovodíková	k1gFnSc1	azidovodíková
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
azidy	azid	k1gInPc1	azid
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stálejší	stálý	k2eAgFnSc1d2	stálejší
než	než	k8xS	než
volná	volný	k2eAgFnSc1d1	volná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusná	dusný	k2eAgFnSc1d1	dusná
H2N2O2	H2N2O2	k1gFnSc1	H2N2O2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
stavu	stav	k1gInSc6	stav
krajně	krajně	k6eAd1	krajně
explozivní	explozivní	k2eAgFnSc1d1	explozivní
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
lihu	líh	k1gInSc6	líh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc1	její
disociace	disociace	k1gFnSc1	disociace
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
znatelně	znatelně	k6eAd1	znatelně
stabilnější	stabilní	k2eAgFnPc1d2	stabilnější
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
dusnany	dusnana	k1gFnSc2	dusnana
neboli	neboli	k8xC	neboli
hyponitrily	hyponitrila	k1gFnSc2	hyponitrila
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusnatá	dusnatý	k2eAgFnSc1d1	dusnatý
H4N2O4	H4N2O4	k1gFnSc1	H4N2O4
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svých	svůj	k3xOyFgFnPc2	svůj
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
doposud	doposud	k6eAd1	doposud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusitá	dusitý	k2eAgFnSc1d1	dusitá
HNO2	HNO2	k1gFnSc7	HNO2
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
stálá	stálý	k2eAgFnSc1d1	stálá
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
zředěných	zředěný	k2eAgInPc6d1	zředěný
roztocích	roztok	k1gInPc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
koncentraci	koncentrace	k1gFnSc6	koncentrace
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
dusičnou	dusičný	k2eAgFnSc4d1	dusičná
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
dusnatý	dusnatý	k2eAgInSc4d1	dusnatý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
dusité	dusitý	k2eAgFnPc1d1	dusitá
<g/>
,	,	kIx,	,
dusitany	dusitan	k1gInPc1	dusitan
neboli	neboli	k8xC	neboli
nitrity	nitrit	k1gInPc1	nitrit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kyseliny	kyselina	k1gFnSc2	kyselina
znatelně	znatelně	k6eAd1	znatelně
stabilnější	stabilní	k2eAgFnPc1d2	stabilnější
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
organických	organický	k2eAgFnPc6d1	organická
syntézách	syntéza	k1gFnPc6	syntéza
např.	např.	kA	např.
diazotaci	diazotace	k1gFnSc4	diazotace
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
HNO3	HNO3	k1gFnSc1	HNO3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
koncentraci	koncentrace	k1gFnSc6	koncentrace
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
dusičitý	dusičitý	k2eAgInSc4d1	dusičitý
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
je	být	k5eAaImIp3nS	být
silné	silný	k2eAgNnSc4d1	silné
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
průmyslově	průmyslově	k6eAd1	průmyslově
nejvyráběnějších	vyráběný	k2eAgFnPc2d3	vyráběný
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
množství	množství	k1gNnSc1	množství
použití	použití	k1gNnSc2	použití
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gFnPc4	její
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
dusičnany	dusičnan	k1gInPc4	dusičnan
neboli	neboli	k8xC	neboli
nitráty	nitrát	k1gInPc4	nitrát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
dusíku	dusík	k1gInSc2	dusík
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
nitrosloučeniny	nitrosloučenina	k1gFnPc1	nitrosloučenina
<g/>
,	,	kIx,	,
nitrososloučeniny	nitrososloučenina	k1gFnPc1	nitrososloučenina
<g/>
,	,	kIx,	,
aminy	amin	k1gInPc1	amin
<g/>
,	,	kIx,	,
amoniové	amoniový	k2eAgFnPc1d1	amoniová
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
kyanatany	kyanatan	k1gInPc1	kyanatan
neboli	neboli	k8xC	neboli
kyanáty	kyanát	k1gInPc1	kyanát
<g/>
,	,	kIx,	,
isokyanatany	isokyanatan	k1gInPc1	isokyanatan
neboli	neboli	k8xC	neboli
isokyanáty	isokyanát	k1gInPc1	isokyanát
<g/>
,	,	kIx,	,
thiokyanatany	thiokyanatan	k1gInPc1	thiokyanatan
neboli	neboli	k8xC	neboli
thiokyanáty	thiokyanát	k1gInPc1	thiokyanát
<g/>
,	,	kIx,	,
isothiokyanatany	isothiokyanatan	k1gInPc1	isothiokyanatan
neboli	neboli	k8xC	neboli
isothiokyanáty	isothiokyanát	k1gInPc1	isothiokyanát
<g/>
,	,	kIx,	,
azosloučeniny	azosloučenina	k1gFnPc1	azosloučenina
<g/>
,	,	kIx,	,
diazoniové	diazoniový	k2eAgFnPc1d1	diazoniová
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
deriváty	derivát	k1gInPc1	derivát
hydrazinu	hydrazin	k1gInSc2	hydrazin
<g/>
,	,	kIx,	,
deriváty	derivát	k1gInPc7	derivát
hydroxylaminu	hydroxylamin	k1gInSc2	hydroxylamin
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
amidy	amid	k1gInPc1	amid
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
hydrazidy	hydrazida	k1gFnSc2	hydrazida
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
laktamy	laktam	k1gInPc1	laktam
<g/>
,	,	kIx,	,
imidy	imid	k1gInPc1	imid
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
nitrily	nitril	k1gInPc4	nitril
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
skupinou	skupina	k1gFnSc7	skupina
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
dusíku	dusík	k1gInSc2	dusík
jsou	být	k5eAaImIp3nP	být
heterocyklické	heterocyklický	k2eAgFnPc1d1	heterocyklická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
a	a	k8xC	a
nitráty	nitrát	k1gInPc1	nitrát
<g/>
.	.	kIx.	.
</s>
<s>
Nitrosloučeniny	nitrosloučenina	k1gFnPc1	nitrosloučenina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
skupinu	skupina	k1gFnSc4	skupina
-NO	-NO	k?	-NO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
reakčních	reakční	k2eAgFnPc2d1	reakční
podmínek	podmínka	k1gFnPc2	podmínka
mírné	mírný	k2eAgFnSc2d1	mírná
oxidační	oxidační	k2eAgFnSc2d1	oxidační
vlastnosti	vlastnost	k1gFnSc2	vlastnost
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgInPc7d1	významný
meziprodukty	meziprodukt	k1gInPc7	meziprodukt
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
aromatických	aromatický	k2eAgInPc2d1	aromatický
aminů	amin	k1gInPc2	amin
a	a	k8xC	a
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
nitrací	nitrace	k1gFnSc7	nitrace
aromátů	aromát	k1gInPc2	aromát
nitrační	nitrační	k2eAgFnSc7d1	nitrační
směsí	směs	k1gFnSc7	směs
nebo	nebo	k8xC	nebo
nitrací	nitrace	k1gFnSc7	nitrace
alifátů	alifát	k1gMnPc2	alifát
plynnou	plynný	k2eAgFnSc7d1	plynná
HNO3	HNO3	k1gFnSc7	HNO3
nebo	nebo	k8xC	nebo
NOx	noxa	k1gFnPc2	noxa
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
nitrotolueny	nitrotoluen	k1gInPc4	nitrotoluen
<g/>
,	,	kIx,	,
trinitrotoluen	trinitrotoluen	k1gInSc4	trinitrotoluen
nebo	nebo	k8xC	nebo
nitromethan	nitromethan	k1gInSc4	nitromethan
<g/>
.	.	kIx.	.
</s>
<s>
Nitrososloučeniny	nitrososloučenina	k1gFnPc1	nitrososloučenina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
skupinu	skupina	k1gFnSc4	skupina
-NO	-NO	k?	-NO
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
deriváty	derivát	k1gInPc1	derivát
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
náhradou	náhrada	k1gFnSc7	náhrada
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
na	na	k7c6	na
terciárním	terciární	k2eAgInSc6d1	terciární
atomu	atom	k1gInSc6	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
nitrosobenzen	nitrosobenzit	k5eAaPmNgInS	nitrosobenzit
<g/>
.	.	kIx.	.
</s>
<s>
Aminy	amin	k1gInPc1	amin
jsou	být	k5eAaImIp3nP	být
deriváty	derivát	k1gInPc1	derivát
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
náhradou	náhrada	k1gFnSc7	náhrada
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
molekule	molekula	k1gFnSc6	molekula
<g/>
,	,	kIx,	,
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
primární	primární	k2eAgNnSc4d1	primární
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
-NH	-NH	k?	-NH
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgMnSc1d1	sekundární
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
=	=	kIx~	=
<g/>
NH	NH	kA	NH
a	a	k8xC	a
terciární	terciární	k2eAgMnPc1d1	terciární
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
=	=	kIx~	=
<g/>
N-	N-	k1gFnPc2	N-
<g/>
.	.	kIx.	.
</s>
<s>
Aminoskupina	Aminoskupina	k1gFnSc1	Aminoskupina
je	být	k5eAaImIp3nS	být
přítomna	přítomno	k1gNnPc4	přítomno
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
aminokyselinách	aminokyselina	k1gFnPc6	aminokyselina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
aromatickým	aromatický	k2eAgInSc7d1	aromatický
aminem	amin	k1gInSc7	amin
je	být	k5eAaImIp3nS	být
anilín	anilín	k1gInSc1	anilín
<g/>
.	.	kIx.	.
</s>
<s>
Atom	atom	k1gInSc1	atom
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
aminech	amin	k1gInPc6	amin
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
amoniaku	amoniak	k1gInSc6	amoniak
volný	volný	k2eAgInSc1d1	volný
elektronový	elektronový	k2eAgInSc1d1	elektronový
pár	pár	k1gInSc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Aminy	amin	k1gInPc1	amin
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
zásadami	zásada	k1gFnPc7	zásada
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
poutat	poutat	k5eAaImF	poutat
H	H	kA	H
<g/>
+	+	kIx~	+
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
amoniové	amoniový	k2eAgFnPc4d1	amoniová
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Kyanatany	Kyanatan	k1gInPc1	Kyanatan
vznikají	vznikat	k5eAaImIp3nP	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
uhlovodíku	uhlovodík	k1gInSc6	uhlovodík
skupinou	skupina	k1gFnSc7	skupina
-O-CΞ	-O-CΞ	k?	-O-CΞ
Isokyanatany	Isokyanatan	k1gInPc1	Isokyanatan
vznikají	vznikat	k5eAaImIp3nP	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
uhlovodíku	uhlovodík	k1gInSc6	uhlovodík
skupinou	skupina	k1gFnSc7	skupina
-N	-N	k?	-N
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
O	o	k7c4	o
Thiokyanatany	Thiokyanatan	k1gInPc4	Thiokyanatan
vznikají	vznikat	k5eAaImIp3nP	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
uhlovodíku	uhlovodík	k1gInSc6	uhlovodík
skupinou	skupina	k1gFnSc7	skupina
-S-CΞ	-S-CΞ	k?	-S-CΞ
Isothiokyanatany	Isothiokyanatan	k1gInPc1	Isothiokyanatan
vznikají	vznikat	k5eAaImIp3nP	vznikat
nahrazením	nahrazení	k1gNnSc7	nahrazení
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
uhlovodíku	uhlovodík	k1gInSc6	uhlovodík
skupinou	skupina	k1gFnSc7	skupina
-N	-N	k?	-N
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
=	=	kIx~	=
<g/>
S	s	k7c7	s
Azosloučeniny	azosloučenina	k1gFnSc2	azosloučenina
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-N	-N	k?	-N
<g/>
=	=	kIx~	=
<g/>
N-	N-	k1gMnSc1	N-
<g/>
,	,	kIx,	,
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
skupinu	skupina	k1gFnSc4	skupina
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vázat	vázat	k5eAaImF	vázat
dva	dva	k4xCgInPc4	dva
stejné	stejný	k2eAgInPc4d1	stejný
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc4d1	různý
uhlovodíkové	uhlovodíkový	k2eAgInPc4d1	uhlovodíkový
zbytky	zbytek	k1gInPc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
dimethylamino	dimethylamin	k2eAgNnSc1d1	dimethylamin
<g/>
)	)	kIx)	)
<g/>
fenylazo	fenylaza	k1gFnSc5	fenylaza
<g/>
]	]	kIx)	]
<g/>
benzen-	benzen-	k?	benzen-
<g/>
1	[number]	k4	1
<g/>
-sulfonová	ulfonová	k1gFnSc1	-sulfonová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sodná	sodný	k2eAgFnSc1d1	sodná
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
triviálně	triviálně	k6eAd1	triviálně
methyloranž	methyloranž	k6eAd1	methyloranž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
indikátor	indikátor	k1gInSc4	indikátor
<g/>
.	.	kIx.	.
</s>
<s>
Diazoniové	Diazoniový	k2eAgFnPc1d1	Diazoniový
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
obsahující	obsahující	k2eAgFnSc4d1	obsahující
funkční	funkční	k2eAgFnSc4d1	funkční
skupinu	skupina	k1gFnSc4	skupina
-N	-N	k?	-N
<g/>
+	+	kIx~	+
<g/>
Ξ	Ξ	k?	Ξ
<g/>
.	.	kIx.	.
</s>
<s>
Deriváty	derivát	k1gInPc1	derivát
hydroxylaminu	hydroxylamin	k1gInSc2	hydroxylamin
NH2OH	NH2OH	k1gFnSc2	NH2OH
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skupinu	skupina	k1gFnSc4	skupina
-NHOH	-NHOH	k?	-NHOH
nebo	nebo	k8xC	nebo
>	>	kIx)	>
<g/>
NOH	noh	k1gMnSc1	noh
Deriváty	derivát	k1gInPc1	derivát
hydrazinu	hydrazin	k1gInSc2	hydrazin
NH2NH2	NH2NH2	k1gFnSc2	NH2NH2
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
atomy	atom	k1gInPc1	atom
v	v	k7c6	v
hydrazinu	hydrazin	k1gInSc6	hydrazin
substituované	substituovaný	k2eAgInPc4d1	substituovaný
uhlovodíkovými	uhlovodíkový	k2eAgInPc7d1	uhlovodíkový
zbytky	zbytek	k1gInPc7	zbytek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
a	a	k8xC	a
kyslíkaté	kyslíkatý	k2eAgInPc1d1	kyslíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
aminoskupinu	aminoskupina	k1gFnSc4	aminoskupina
-NH2	-NH2	k4	-NH2
a	a	k8xC	a
karboxylovou	karboxylový	k2eAgFnSc4d1	karboxylová
skupinu	skupina	k1gFnSc4	skupina
-COOH	-COOH	k?	-COOH
<g/>
.	.	kIx.	.
</s>
<s>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnPc4d1	základní
stavební	stavební	k2eAgFnPc4d1	stavební
jednotky	jednotka	k1gFnPc4	jednotka
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
v	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
proteinogenních	proteinogenní	k2eAgFnPc2d1	proteinogenní
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
takových	takový	k3xDgInPc6	takový
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
bílkovinách	bílkovina	k1gFnPc6	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
mají	mít	k5eAaImIp3nP	mít
triviální	triviální	k2eAgInPc4d1	triviální
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
glycin	glycin	k1gInSc4	glycin
jsou	být	k5eAaImIp3nP	být
opticky	opticky	k6eAd1	opticky
aktivní	aktivní	k2eAgInPc1d1	aktivní
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
L-aminokyseliny	Lminokyselin	k2eAgFnPc4d1	L-aminokyselin
<g/>
.	.	kIx.	.
</s>
<s>
R-aminokyseliny	Rminokyselina	k1gFnPc1	R-aminokyselina
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
savčích	savčí	k2eAgInPc6d1	savčí
mozcích	mozek	k1gInPc6	mozek
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
zde	zde	k6eAd1	zde
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
při	při	k7c6	při
mozkových	mozkový	k2eAgInPc6d1	mozkový
pochodech	pochod	k1gInPc6	pochod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
uchovávání	uchovávání	k1gNnSc4	uchovávání
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Amidy	amid	k1gInPc1	amid
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
amidovou	amidový	k2eAgFnSc4d1	amidový
skupinu	skupina	k1gFnSc4	skupina
-CO-NH	-CO-NH	k?	-CO-NH
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Amid	amid	k1gInSc1	amid
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
karboxylové	karboxylový	k2eAgFnSc2d1	karboxylová
kyseliny	kyselina	k1gFnSc2	kyselina
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
COOH	COOH	kA	COOH
<g/>
+	+	kIx~	+
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
→	→	k?	→
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
CO-NH	CO-NH	k1gFnSc1	CO-NH
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
Imidy	imid	k1gInPc4	imid
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
imidovou	imidový	k2eAgFnSc4d1	imidový
skupinu	skupina	k1gFnSc4	skupina
-CO-NH-CO-	-CO-NH-CO-	k?	-CO-NH-CO-
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
dvou	dva	k4xCgFnPc6	dva
karboxylových	karboxylový	k2eAgFnPc2d1	karboxylová
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
COOH	COOH	kA	COOH
<g/>
+	+	kIx~	+
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
COOH	COOH	kA	COOH
→	→	k?	→
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
CO-NH-COR	CO-NH-COR	k1gFnSc1	CO-NH-COR
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Nitrily	nitril	k1gInPc4	nitril
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nitrilovou	nitrilový	k2eAgFnSc4d1	nitrilový
skupinu	skupina	k1gFnSc4	skupina
-CΞ	-CΞ	k?	-CΞ
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pojmenováváme	pojmenovávat	k5eAaImIp1nP	pojmenovávat
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jako	jako	k8xS	jako
nitrily	nitril	k1gInPc1	nitril
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uhlík	uhlík	k1gInSc1	uhlík
vázaný	vázaný	k2eAgInSc1d1	vázaný
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
do	do	k7c2	do
názvu	název	k1gInSc2	název
uhlovodíkového	uhlovodíkový	k2eAgInSc2d1	uhlovodíkový
zbytku	zbytek	k1gInSc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
pojmenovávat	pojmenovávat	k5eAaImF	pojmenovávat
tyto	tento	k3xDgFnPc4	tento
sloučeniny	sloučenina	k1gFnPc4	sloučenina
jako	jako	k8xS	jako
kyanidy	kyanid	k1gInPc4	kyanid
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uhlík	uhlík	k1gInSc1	uhlík
vázaný	vázaný	k2eAgInSc1d1	vázaný
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
do	do	k7c2	do
názvu	název	k1gInSc2	název
uhlovodíkového	uhlovodíkový	k2eAgInSc2d1	uhlovodíkový
zbytku	zbytek	k1gInSc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Hydrazidy	Hydrazid	k1gInPc1	Hydrazid
jsou	být	k5eAaImIp3nP	být
dusíkaté	dusíkatý	k2eAgInPc1d1	dusíkatý
deriváty	derivát	k1gInPc1	derivát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hydrazidovou	hydrazidový	k2eAgFnSc4d1	hydrazidový
skupinu	skupina	k1gFnSc4	skupina
R-CO-NH-NH	R-CO-NH-NH	k1gFnSc2	R-CO-NH-NH
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hydrazinu	hydrazin	k1gInSc6	hydrazin
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
až	až	k6eAd1	až
všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
COOH	COOH	kA	COOH
<g/>
+	+	kIx~	+
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
NH	NH	kA	NH
<g/>
2	[number]	k4	2
→	→	k?	→
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
-NH-NH	-NH-NH	k?	-NH-NH
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Laktamy	laktam	k1gInPc4	laktam
neboli	neboli	k8xC	neboli
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
amidy	amid	k1gInPc4	amid
vznikají	vznikat	k5eAaImIp3nP	vznikat
zacyklením	zacyklení	k1gNnSc7	zacyklení
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
uhlíkatého	uhlíkatý	k2eAgInSc2d1	uhlíkatý
<g/>
,	,	kIx,	,
amidů	amid	k1gInPc2	amid
karboxylových	karboxylový	k2eAgMnPc2d1	karboxylový
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
uhlíkatého	uhlíkatý	k2eAgInSc2d1	uhlíkatý
řetězce	řetězec	k1gInSc2	řetězec
je	být	k5eAaImIp3nS	být
aminoskupina	aminoskupin	k2eAgFnSc1d1	aminoskupin
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
karboxylová	karboxylový	k2eAgFnSc1d1	karboxylová
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Laktam	laktam	k1gInSc1	laktam
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
reakcí	reakce	k1gFnSc7	reakce
karboxylové	karboxylový	k2eAgFnSc2d1	karboxylová
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
aminoskupiny	aminoskupina	k1gFnSc2	aminoskupina
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
molekule	molekula	k1gFnSc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Nitráty	nitrát	k1gInPc1	nitrát
jsou	být	k5eAaImIp3nP	být
estery	ester	k1gInPc1	ester
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgInPc1d1	dusičný
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nitroskupinu	nitroskupina	k1gFnSc4	nitroskupina
-O-NO	-O-NO	k?	-O-NO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nitráty	nitrát	k1gInPc1	nitrát
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
připravují	připravovat	k5eAaImIp3nP	připravovat
"	"	kIx"	"
<g/>
nitrací	nitrace	k1gFnSc7	nitrace
<g/>
"	"	kIx"	"
hydroxysloučenin	hydroxysloučenin	k2eAgInSc4d1	hydroxysloučenin
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zpravidla	zpravidla	k6eAd1	zpravidla
nitrační	nitrační	k2eAgFnPc4d1	nitrační
směsi	směs	k1gFnPc4	směs
<g/>
.	.	kIx.	.
</s>
<s>
Ukázkovými	ukázkový	k2eAgMnPc7d1	ukázkový
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
kapalný	kapalný	k2eAgInSc1d1	kapalný
glyceroltrinitrát	glyceroltrinitrát	k1gInSc1	glyceroltrinitrát
"	"	kIx"	"
<g/>
nitroglycerin	nitroglycerin	k1gInSc1	nitroglycerin
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
krystalický	krystalický	k2eAgInSc1d1	krystalický
pentaerythritoltetranitrát	pentaerythritoltetranitrát	k1gInSc1	pentaerythritoltetranitrát
"	"	kIx"	"
<g/>
pentrit	pentrit	k1gInSc1	pentrit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
brizantní	brizantní	k2eAgMnPc1d1	brizantní
výbušiny	výbušina	k1gFnSc2	výbušina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
heterocyklické	heterocyklický	k2eAgFnPc4d1	heterocyklická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
obsahující	obsahující	k2eAgFnPc4d1	obsahující
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
atom	atom	k1gInSc1	atom
dusíku	dusík	k1gInSc2	dusík
patří	patřit	k5eAaImIp3nS	patřit
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85427	[number]	k4	85427
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fikr	Fikr	k1gMnSc1	Fikr
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kahovec	Kahovec	k1gMnSc1	Kahovec
<g/>
;	;	kIx,	;
Názvosloví	názvosloví	k1gNnSc1	názvosloví
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Karlson	Karlson	k1gMnSc1	Karlson
<g/>
;	;	kIx,	;
Základy	základ	k1gInPc1	základ
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
;	;	kIx,	;
vydání	vydání	k1gNnSc1	vydání
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dusík	dusík	k1gInSc1	dusík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dusík	dusík	k1gInSc1	dusík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chemistry	Chemistr	k1gMnPc4	Chemistr
in	in	k?	in
its	its	k?	its
element	element	k1gInSc1	element
podcast	podcast	k1gFnSc1	podcast
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
from	from	k6eAd1	from
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chemistry	Chemistr	k1gMnPc7	Chemistr
World	Worlda	k1gFnPc2	Worlda
<g/>
:	:	kIx,	:
Nitrogen	Nitrogen	k1gInSc1	Nitrogen
Schenectady	Schenectada	k1gFnSc2	Schenectada
County	Counta	k1gFnSc2	Counta
Community	Communita	k1gFnSc2	Communita
College	Colleg	k1gFnSc2	Colleg
-	-	kIx~	-
Nitrogen	Nitrogen	k1gInSc1	Nitrogen
Nitrogen	Nitrogen	k1gInSc1	Nitrogen
N2	N2	k1gFnPc2	N2
Properties	Properties	k1gInSc1	Properties
<g/>
,	,	kIx,	,
Uses	Uses	k1gInSc1	Uses
<g/>
,	,	kIx,	,
Applications	Applications	k1gInSc1	Applications
Handling	Handling	k1gInSc1	Handling
procedures	proceduresa	k1gFnPc2	proceduresa
for	forum	k1gNnPc2	forum
liquid	liquid	k1gInSc1	liquid
nitrogen	nitrogen	k1gInSc4	nitrogen
</s>
