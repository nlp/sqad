<s>
Každá	každý	k3xTgFnSc1	každý
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
firmě	firma	k1gFnSc6	firma
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
společnosti	společnost	k1gFnPc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
akc	akc	k?	akc
<g/>
.	.	kIx.	.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
