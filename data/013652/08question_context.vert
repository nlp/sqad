<s>
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
korporace	korporace	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
základní	základní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
se	se	k3xPyFc4
neskládá	skládat	k5eNaImIp3nS
z	z	k7c2
(	(	kIx(
<g/>
nehmotných	hmotný	k2eNgFnPc2d1
<g/>
,	,	kIx,
abstraktních	abstraktní	k2eAgFnPc2d1
<g/>
)	)	kIx)
podílů	podíl	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
akcií	akcie	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
cenné	cenný	k2eAgInPc1d1
papíry	papír	k1gInPc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
zaknihované	zaknihovaný	k2eAgInPc1d1
cenné	cenný	k2eAgInPc1d1
papíry	papír	k1gInPc1
<g/>
)	)	kIx)
tento	tento	k3xDgInSc1
podíl	podíl	k1gInSc1
představující	představující	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>