<s>
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
opera	opera	k1gFnSc1	opera
českého	český	k2eAgMnSc2d1	český
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Bernarda	Bernard	k1gMnSc2	Bernard
Guldenera	Guldener	k1gMnSc2	Guldener
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
<g/>
:	:	kIx,	:
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
Prozatímním	prozatímní	k2eAgNnSc7d1	prozatímní
divadlem	divadlo	k1gNnSc7	divadlo
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
jako	jako	k9	jako
nehratelná	hratelný	k2eNgFnSc1d1	nehratelná
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
Dvořák	Dvořák	k1gMnSc1	Dvořák
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
na	na	k7c4	na
totéž	týž	k3xTgNnSc4	týž
libreto	libreto	k1gNnSc4	libreto
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
Dvořák	Dvořák	k1gMnSc1	Dvořák
napsal	napsat	k5eAaBmAgInS	napsat
již	již	k9	již
řadu	řada	k1gFnSc4	řada
komorních	komorní	k2eAgFnPc2d1	komorní
i	i	k8xC	i
orchestrálních	orchestrální	k2eAgFnPc2d1	orchestrální
skladeb	skladba	k1gFnPc2	skladba
včetně	včetně	k7c2	včetně
symfonií	symfonie	k1gFnPc2	symfonie
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
však	však	k9	však
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nebyla	být	k5eNaImAgFnS	být
veřejně	veřejně	k6eAd1	veřejně
provedena	provést	k5eAaPmNgFnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
výslovně	výslovně	k6eAd1	výslovně
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
Prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
mu	on	k3xPp3gMnSc3	on
dodal	dodat	k5eAaPmAgMnS	dodat
nepříliš	příliš	k6eNd1	příliš
známý	známý	k2eAgMnSc1d1	známý
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Bernard	Bernard	k1gMnSc1	Bernard
Guldener	Guldener	k1gMnSc1	Guldener
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgMnSc1d1	píšící
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Bernard	Bernard	k1gMnSc1	Bernard
J.	J.	kA	J.
Lobeský	Lobeský	k2eAgInSc1d1	Lobeský
<g/>
.	.	kIx.	.
</s>
<s>
Vycházelo	vycházet	k5eAaImAgNnS	vycházet
ze	z	k7c2	z
známé	známý	k2eAgFnSc2d1	známá
hry	hra	k1gFnSc2	hra
pro	pro	k7c4	pro
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
Posvícení	posvícení	k1gNnSc2	posvícení
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
znal	znát	k5eAaImAgInS	znát
Guldener	Guldener	k1gInSc1	Guldener
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
zpracování	zpracování	k1gNnSc6	zpracování
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
ze	z	k7c2	z
souboru	soubor	k1gInSc2	soubor
děl	dělo	k1gNnPc2	dělo
Matěje	Matěj	k1gMnSc2	Matěj
Kopeckého	Kopecký	k1gMnSc2	Kopecký
vydaného	vydaný	k2eAgInSc2d1	vydaný
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
;	;	kIx,	;
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejspíš	nejspíš	k9	nejspíš
Prokop	Prokop	k1gMnSc1	Prokop
Konopásek	Konopásek	k1gMnSc1	Konopásek
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
látku	látka	k1gFnSc4	látka
o	o	k7c6	o
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
poznává	poznávat	k5eAaImIp3nS	poznávat
život	život	k1gInSc1	život
svých	svůj	k3xOyFgMnPc2	svůj
poddaných	poddaný	k1gMnPc2	poddaný
–	–	k?	–
to	ten	k3xDgNnSc4	ten
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyobrazit	vyobrazit	k5eAaPmF	vyobrazit
různá	různý	k2eAgNnPc4d1	různé
sociální	sociální	k2eAgNnPc4d1	sociální
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
Dvořák	Dvořák	k1gMnSc1	Dvořák
při	při	k7c6	při
zhudebnění	zhudebnění	k1gNnSc6	zhudebnění
plně	plně	k6eAd1	plně
využil	využít	k5eAaPmAgInS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
doby	doba	k1gFnSc2	doba
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
bližší	blízký	k2eAgFnSc2d2	bližší
(	(	kIx(	(
<g/>
a	a	k8xC	a
hudebně	hudebně	k6eAd1	hudebně
lépe	dobře	k6eAd2	dobře
charakterizovatelné	charakterizovatelný	k2eAgFnPc1d1	charakterizovatelná
<g/>
)	)	kIx)	)
doby	doba	k1gFnPc1	doba
panování	panování	k1gNnSc2	panování
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
-	-	kIx~	-
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Guldenerovo	Guldenerův	k2eAgNnSc1d1	Guldenerův
libreto	libreto	k1gNnSc1	libreto
sice	sice	k8xC	sice
stojí	stát	k5eAaImIp3nS	stát
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
stavbou	stavba	k1gFnSc7	stavba
i	i	k8xC	i
konkrétním	konkrétní	k2eAgNnSc7d1	konkrétní
jazykovým	jazykový	k2eAgNnSc7d1	jazykové
vyjádřením	vyjádření	k1gNnSc7	vyjádření
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ostatně	ostatně	k6eAd1	ostatně
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
i	i	k9	i
soudobá	soudobý	k2eAgFnSc1d1	soudobá
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
skladateli	skladatel	k1gMnSc3	skladatel
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
alespoň	alespoň	k9	alespoň
živé	živý	k2eAgFnSc2d1	živá
postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
barvité	barvitý	k2eAgFnSc2d1	barvitá
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
libreto	libreto	k1gNnSc4	libreto
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
názory	názor	k1gInPc7	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
opíraly	opírat	k5eAaImAgFnP	opírat
o	o	k7c4	o
vzor	vzor	k1gInSc4	vzor
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Wagnerova	Wagnerův	k2eAgNnPc1d1	Wagnerovo
díla	dílo	k1gNnPc1	dílo
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgFnS	být
hrána	hrát	k5eAaImNgFnS	hrát
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
–	–	k?	–
šéf	šéf	k1gMnSc1	šéf
opery	opera	k1gFnSc2	opera
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
sice	sice	k8xC	sice
Wagnera	Wagner	k1gMnSc2	Wagner
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
,	,	kIx,	,
nepovažoval	považovat	k5eNaImAgMnS	považovat
však	však	k9	však
podmínky	podmínka	k1gFnPc4	podmínka
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
za	za	k7c4	za
vhodné	vhodný	k2eAgInPc4d1	vhodný
pro	pro	k7c4	pro
provozování	provozování	k1gNnSc4	provozování
jeho	jeho	k3xOp3gFnPc2	jeho
oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
se	se	k3xPyFc4	se
zato	zato	k6eAd1	zato
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Kompozici	kompozice	k1gFnSc4	kompozice
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
nejvíce	nejvíce	k6eAd1	nejvíce
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
Wagnerovi	Wagnerův	k2eAgMnPc1d1	Wagnerův
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnPc1	pěvec
norimberští	norimberský	k2eAgMnPc1d1	norimberský
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
poprvé	poprvé	k6eAd1	poprvé
uvedeni	uvést	k5eAaPmNgMnP	uvést
právě	právě	k9	právě
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
začal	začít	k5eAaPmAgMnS	začít
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
a	a	k8xC	a
partituru	partitura	k1gFnSc4	partitura
dokončil	dokončit	k5eAaPmAgMnS	dokončit
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
nese	nést	k5eAaImIp3nS	nést
v	v	k7c6	v
Burghauserově	Burghauserův	k2eAgInSc6d1	Burghauserův
katalogu	katalog	k1gInSc6	katalog
označení	označení	k1gNnSc2	označení
B	B	kA	B
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
ji	on	k3xPp3gFnSc4	on
ihned	ihned	k6eAd1	ihned
zadal	zadat	k5eAaPmAgMnS	zadat
Prozatímnímu	prozatímní	k2eAgNnSc3d1	prozatímní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Šéf	šéf	k1gMnSc1	šéf
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vážná	vážný	k2eAgFnSc1d1	vážná
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
geniálních	geniální	k2eAgMnPc2d1	geniální
nápadů	nápad	k1gInPc2	nápad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1872	[number]	k4	1872
provedl	provést	k5eAaPmAgInS	provést
koncertně	koncertně	k6eAd1	koncertně
alespoň	alespoň	k9	alespoň
předehru	předehra	k1gFnSc4	předehra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
příznivým	příznivý	k2eAgInSc7d1	příznivý
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Nastudování	nastudování	k1gNnSc1	nastudování
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
však	však	k9	však
oddalovalo	oddalovat	k5eAaImAgNnS	oddalovat
<g/>
,	,	kIx,	,
k	k	k7c3	k
Dvořákovu	Dvořákův	k2eAgNnSc3d1	Dvořákovo
rozčarování	rozčarování	k1gNnSc3	rozčarování
<g/>
,	,	kIx,	,
a	a	k8xC	a
Smetanovi	Smetanův	k2eAgMnPc1d1	Smetanův
bylo	být	k5eAaImAgNnS	být
neuvedení	neuvedení	k1gNnSc3	neuvedení
opery	opera	k1gFnSc2	opera
jeho	jeho	k3xOp3gFnSc2	jeho
kritiky	kritika	k1gFnSc2	kritika
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
Dvořákova	Dvořákův	k2eAgInSc2d1	Dvořákův
hymnu	hymnus	k1gInSc2	hymnus
Dědicové	dědic	k1gMnPc1	dědic
Bílé	bílý	k2eAgFnSc2d1	bílá
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
začal	začít	k5eAaPmAgMnS	začít
zkoušet	zkoušet	k5eAaImF	zkoušet
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc1	zkouška
však	však	k9	však
nepokročily	pokročit	k5eNaPmAgFnP	pokročit
za	za	k7c4	za
první	první	k4xOgNnSc4	první
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
i	i	k8xC	i
zpěváci	zpěvák	k1gMnPc1	zpěvák
považovali	považovat	k5eAaImAgMnP	považovat
své	svůj	k3xOyFgFnPc4	svůj
party	parta	k1gFnPc4	parta
za	za	k7c4	za
nehratelné	hratelný	k2eNgNnSc4d1	nehratelné
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Juda	judo	k1gNnSc2	judo
Novotný	Novotný	k1gMnSc1	Novotný
popisuje	popisovat	k5eAaImIp3nS	popisovat
zkoušky	zkouška	k1gFnPc4	zkouška
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
zadána	zadán	k2eAgFnSc1d1	zadána
<g/>
,	,	kIx,	,
rozepsána	rozepsán	k2eAgFnSc1d1	rozepsána
<g/>
,	,	kIx,	,
počalo	počnout	k5eAaPmAgNnS	počnout
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
se	s	k7c7	s
studováním	studování	k1gNnSc7	studování
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
však	však	k9	však
nechtělo	chtít	k5eNaImAgNnS	chtít
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
jaksi	jaksi	k6eAd1	jaksi
kupředu	kupředu	k6eAd1	kupředu
<g/>
:	:	kIx,	:
každý	každý	k3xTgMnSc1	každý
sólista	sólista	k1gMnSc1	sólista
naříkal	naříkat	k5eAaBmAgMnS	naříkat
si	se	k3xPyFc3	se
na	na	k7c4	na
nesnadnost	nesnadnost	k1gFnSc4	nesnadnost
a	a	k8xC	a
nevděčnost	nevděčnost	k1gFnSc4	nevděčnost
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
sboru	sbor	k1gInSc2	sbor
nechtěly	chtít	k5eNaImAgFnP	chtít
melodie	melodie	k1gFnPc1	melodie
Dvořákovy	Dvořákův	k2eAgFnPc1d1	Dvořákova
nijak	nijak	k6eAd1	nijak
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
Dvořákově	Dvořákův	k2eAgFnSc6d1	Dvořákova
opeře	opera	k1gFnSc6	opera
určeno	určit	k5eAaPmNgNnS	určit
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zdržovalo	zdržovat	k5eAaImAgNnS	zdržovat
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
nechutí	nechuť	k1gFnSc7	nechuť
šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nitru	nitro	k1gNnSc6	nitro
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
marná	marný	k2eAgFnSc1d1	marná
je	být	k5eAaImIp3nS	být
všechna	všechen	k3xTgFnSc1	všechen
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
hudba	hudba	k1gFnSc1	hudba
že	že	k8xS	že
obliby	obliba	k1gFnSc2	obliba
nikdy	nikdy	k6eAd1	nikdy
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
"	"	kIx"	"
<g/>
neproveditelnost	neproveditelnost	k1gFnSc4	neproveditelnost
<g/>
"	"	kIx"	"
Krále	Král	k1gMnPc4	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnPc4	uhlíř
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
trpělo	trpět	k5eAaImAgNnS	trpět
nedostatečnými	nedostatečná	k1gFnPc7	nedostatečná
prostory	prostora	k1gFnPc4	prostora
i	i	k8xC	i
akustikou	akustika	k1gFnSc7	akustika
a	a	k8xC	a
nedostatečným	nedostatečná	k1gFnPc3	nedostatečná
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
"	"	kIx"	"
<g/>
wagnerovském	wagnerovský	k2eAgInSc6d1	wagnerovský
<g/>
"	"	kIx"	"
stylu	styl	k1gInSc6	styl
neškoleným	školený	k2eNgMnSc7d1	neškolený
a	a	k8xC	a
nezkušeným	zkušený	k2eNgInSc7d1	nezkušený
hudebním	hudební	k2eAgInSc7d1	hudební
i	i	k8xC	i
pěveckým	pěvecký	k2eAgInSc7d1	pěvecký
personálem	personál	k1gInSc7	personál
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
verze	verze	k1gFnPc1	verze
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
sice	sice	k8xC	sice
některé	některý	k3yIgInPc4	některý
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
orchestrálním	orchestrální	k2eAgInSc6d1	orchestrální
zvuku	zvuk	k1gInSc6	zvuk
i	i	k8xC	i
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
zpěvních	zpěvní	k2eAgInPc2d1	zpěvní
partů	part	k1gInPc2	part
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pramení	pramenit	k5eAaImIp3nP	pramenit
z	z	k7c2	z
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
nezkušenosti	nezkušenost	k1gFnSc2	nezkušenost
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
neslyšel	slyšet	k5eNaImAgInS	slyšet
žádnou	žádný	k3yNgFnSc4	žádný
svou	svůj	k3xOyFgFnSc4	svůj
větší	veliký	k2eAgFnSc4d2	veliký
skladbu	skladba	k1gFnSc4	skladba
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
moderní	moderní	k2eAgFnSc2d1	moderní
opery	opera	k1gFnSc2	opera
není	být	k5eNaImIp3nS	být
nepřekonatelně	překonatelně	k6eNd1	překonatelně
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Závažným	závažný	k2eAgInSc7d1	závažný
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
náročným	náročný	k2eAgNnSc7d1	náročné
polyfonním	polyfonní	k2eAgNnSc7d1	polyfonní
zhudebněním	zhudebnění	k1gNnSc7	zhudebnění
a	a	k8xC	a
plytkostí	plytkost	k1gFnSc7	plytkost
a	a	k8xC	a
naivností	naivnost	k1gFnSc7	naivnost
libreta	libreto	k1gNnSc2	libreto
pocházejícího	pocházející	k2eAgNnSc2d1	pocházející
z	z	k7c2	z
pimprlové	pimprlový	k2eAgFnSc2d1	pimprlová
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
vida	vida	k?	vida
marnost	marnost	k1gFnSc1	marnost
divadelních	divadelní	k2eAgFnPc2d1	divadelní
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
partituru	partitura	k1gFnSc4	partitura
z	z	k7c2	z
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
později	pozdě	k6eAd2	pozdě
šířil	šířit	k5eAaImAgMnS	šířit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zničil	zničit	k5eAaPmAgMnS	zničit
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
skutečně	skutečně	k6eAd1	skutečně
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
ranými	raný	k2eAgInPc7d1	raný
díly	díl	k1gInPc7	díl
učinil	učinit	k5eAaImAgInS	učinit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Dvořákově	Dvořákův	k2eAgFnSc6d1	Dvořákova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
rukopisná	rukopisný	k2eAgFnSc1d1	rukopisná
partitura	partitura	k1gFnSc1	partitura
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
nalezena	naleznout	k5eAaPmNgFnS	naleznout
v	v	k7c6	v
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
člena	člen	k1gMnSc2	člen
Nového	Nového	k2eAgNnSc2d1	Nového
německého	německý	k2eAgNnSc2d1	německé
divadla	divadlo	k1gNnSc2	divadlo
Josefa	Josef	k1gMnSc2	Josef
Aupěky	Aupěka	k1gMnSc2	Aupěka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k6eAd1	rovněž
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
provozovací	provozovací	k2eAgInPc4d1	provozovací
materiály	materiál	k1gInPc4	materiál
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
uvedení	uvedení	k1gNnSc3	uvedení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
i	i	k9	i
zbývající	zbývající	k2eAgNnSc4d1	zbývající
dějství	dějství	k1gNnSc4	dějství
rekonstruováno	rekonstruován	k2eAgNnSc4d1	rekonstruováno
a	a	k8xC	a
již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
poprvé	poprvé	k6eAd1	poprvé
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
představení	představení	k1gNnPc2	představení
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Otakara	Otakar	k1gMnSc2	Otakar
Ostrčila	Ostrčil	k1gMnSc2	Ostrčil
a	a	k8xC	a
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Pujmana	Pujman	k1gMnSc2	Pujman
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
jedinými	jediný	k2eAgMnPc7d1	jediný
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnPc4	verze
zazněla	zaznět	k5eAaImAgFnS	zaznět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stažení	stažení	k1gNnSc6	stažení
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
Dvořák	Dvořák	k1gMnSc1	Dvořák
ihned	ihned	k6eAd1	ihned
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
neobvyklému	obvyklý	k2eNgMnSc3d1	neobvyklý
<g/>
,	,	kIx,	,
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
hudby	hudba	k1gFnSc2	hudba
ojedinělému	ojedinělý	k2eAgInSc3d1	ojedinělý
kroku	krok	k1gInSc3	krok
<g/>
:	:	kIx,	:
zhudebnil	zhudebnit	k5eAaPmAgMnS	zhudebnit
celé	celý	k2eAgNnSc4d1	celé
libreto	libreto	k1gNnSc4	libreto
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
jediného	jediný	k2eAgInSc2d1	jediný
taktu	takt	k1gInSc2	takt
z	z	k7c2	z
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Poučen	poučit	k5eAaPmNgInS	poučit
zkušeností	zkušenost	k1gFnSc7	zkušenost
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
zhudebněním	zhudebnění	k1gNnSc7	zhudebnění
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
ke	k	k7c3	k
kompozici	kompozice	k1gFnSc4	kompozice
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Wagnerovy	Wagnerův	k2eAgInPc1d1	Wagnerův
vlivy	vliv	k1gInPc1	vliv
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Mistrů	mistr	k1gMnPc2	mistr
pěvců	pěvec	k1gMnPc2	pěvec
norimberských	norimberský	k2eAgMnPc2d1	norimberský
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
patrné	patrný	k2eAgInPc1d1	patrný
<g/>
,	,	kIx,	,
stylově	stylově	k6eAd1	stylově
se	se	k3xPyFc4	se
však	však	k9	však
Dvořák	Dvořák	k1gMnSc1	Dvořák
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
snadnějšímu	snadný	k2eAgInSc3d2	snadnější
slohu	sloh	k1gInSc2	sloh
prací	práce	k1gFnPc2	práce
C.	C.	kA	C.
M.	M.	kA	M.
von	von	k1gInSc1	von
Webera	Weber	k1gMnSc2	Weber
a	a	k8xC	a
A.	A.	kA	A.
Lortzinga	Lortzinga	k1gFnSc1	Lortzinga
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
francouzských	francouzský	k2eAgMnPc2d1	francouzský
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc4d2	veliký
úlohu	úloha	k1gFnSc4	úloha
má	mít	k5eAaImIp3nS	mít
zapojení	zapojení	k1gNnSc1	zapojení
prvků	prvek	k1gInPc2	prvek
české	český	k2eAgFnSc2d1	Česká
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
hojných	hojný	k2eAgFnPc6d1	hojná
tanečních	taneční	k2eAgFnPc6d1	taneční
scénách	scéna	k1gFnPc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Orchestrace	orchestrace	k1gFnSc1	orchestrace
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
střízlivější	střízlivý	k2eAgFnSc1d2	střízlivější
a	a	k8xC	a
souladnější	souladný	k2eAgFnSc1d2	souladný
s	s	k7c7	s
námětem	námět	k1gInSc7	námět
než	než	k8xS	než
u	u	k7c2	u
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
také	také	k9	také
zpěvní	zpěvní	k2eAgFnPc1d1	zpěvní
party	parta	k1gFnPc1	parta
jsou	být	k5eAaImIp3nP	být
melodičtější	melodický	k2eAgFnPc1d2	melodičtější
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
hudebně	hudebně	k6eAd1	hudebně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
dramatických	dramatický	k2eAgFnPc2d1	dramatická
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
upomínají	upomínat	k5eAaImIp3nP	upomínat
na	na	k7c4	na
Alfreda	Alfred	k1gMnSc4	Alfred
a	a	k8xC	a
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
pak	pak	k6eAd1	pak
na	na	k7c4	na
pozdější	pozdní	k2eAgFnSc4d2	pozdější
Vandu	Vanda	k1gFnSc4	Vanda
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
lidové	lidový	k2eAgInPc4d1	lidový
a	a	k8xC	a
komické	komický	k2eAgInPc4d1	komický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
vydařenější	vydařený	k2eAgInPc1d2	vydařenější
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgInPc2	jenž
vede	vést	k5eAaImIp3nS	vést
cesta	cesta	k1gFnSc1	cesta
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Tvrdým	tvrdý	k2eAgFnPc3d1	tvrdá
palicím	palice	k1gFnPc3	palice
a	a	k8xC	a
Šelmě	šelma	k1gFnSc3	šelma
sedlákovi	sedlákův	k2eAgMnPc1d1	sedlákův
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
první	první	k4xOgFnSc7	první
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
komických	komický	k2eAgFnPc2d1	komická
oper	opera	k1gFnPc2	opera
začal	začít	k5eAaPmAgMnS	začít
Dvořák	Dvořák	k1gMnSc1	Dvořák
skládat	skládat	k5eAaImF	skládat
ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dokončoval	dokončovat	k5eAaImAgMnS	dokončovat
druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
Dvořákův	Dvořákův	k2eAgInSc1d1	Dvořákův
odklon	odklon	k1gInSc1	odklon
od	od	k7c2	od
přísného	přísný	k2eAgNnSc2d1	přísné
sledování	sledování	k1gNnSc2	sledování
Wagnerova	Wagnerův	k2eAgInSc2d1	Wagnerův
vzoru	vzor	k1gInSc2	vzor
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
klasičtějšímu	klasický	k2eAgInSc3d2	klasičtější
výrazu	výraz	k1gInSc3	výraz
obdobnému	obdobný	k2eAgInSc3d1	obdobný
stylu	styl	k1gInSc3	styl
Johannese	Johannese	k1gFnSc2	Johannese
Brahmse	Brahms	k1gMnPc4	Brahms
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
k	k	k7c3	k
vlastnímu	vlastní	k2eAgNnSc3d1	vlastní
osobitému	osobitý	k2eAgNnSc3d1	osobité
a	a	k8xC	a
samostatnému	samostatný	k2eAgInSc3d1	samostatný
stylu	styl	k1gInSc3	styl
"	"	kIx"	"
<g/>
dvořákovskému	dvořákovský	k2eAgNnSc3d1	dvořákovské
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
–	–	k?	–
i	i	k8xC	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
souběžně	souběžně	k6eAd1	souběžně
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
a	a	k8xC	a
písňových	písňový	k2eAgFnPc2d1	písňová
skladeb	skladba	k1gFnPc2	skladba
–	–	k?	–
datovat	datovat	k5eAaImF	datovat
právě	právě	k9	právě
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1872-74	[number]	k4	1872-74
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
verze	verze	k1gFnPc4	verze
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
napsal	napsat	k5eAaBmAgMnS	napsat
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
I.	I.	kA	I.
jednání	jednání	k1gNnSc1	jednání
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc4	jednání
2	[number]	k4	2
<g/>
.	.	kIx.	.
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc4	jednání
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
předehru	předehra	k1gFnSc4	předehra
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
opery	opera	k1gFnSc2	opera
nese	nést	k5eAaImIp3nS	nést
opusové	opusový	k2eAgNnSc1d1	opusové
číslo	číslo	k1gNnSc1	číslo
14	[number]	k4	14
<g/>
,	,	kIx,	,
v	v	k7c6	v
Burghauserově	Burghauserův	k2eAgInSc6d1	Burghauserův
katalogu	katalog	k1gInSc6	katalog
je	být	k5eAaImIp3nS	být
zanesena	zanesen	k2eAgFnSc1d1	zanesena
jako	jako	k8xC	jako
B	B	kA	B
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
Prozatímním	prozatímní	k2eAgNnSc7d1	prozatímní
divadlem	divadlo	k1gNnSc7	divadlo
přijata	přijmout	k5eAaPmNgFnS	přijmout
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
musel	muset	k5eAaImAgMnS	muset
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
ohluchnutí	ohluchnutí	k1gNnSc3	ohluchnutí
vedení	vedení	k1gNnSc1	vedení
opery	opera	k1gFnSc2	opera
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
opustit	opustit	k5eAaPmF	opustit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
rivala	rival	k1gMnSc2	rival
J.	J.	kA	J.
N.	N.	kA	N.
Maýra	Maýra	k1gMnSc1	Maýra
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovi	Smetana	k1gMnSc3	Smetana
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
jeho	jeho	k3xOp3gMnPc4	jeho
protivníky	protivník	k1gMnPc4	protivník
vyčítáno	vyčítán	k2eAgNnSc1d1	vyčítáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc4	funkce
šéfa	šéf	k1gMnSc2	šéf
opery	opera	k1gFnSc2	opera
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tvorbu	tvorba	k1gFnSc4	tvorba
proti	proti	k7c3	proti
tvorbě	tvorba	k1gFnSc3	tvorba
jiných	jiný	k2eAgMnPc2d1	jiný
mladých	mladý	k1gMnPc2	mladý
domácích	domácí	k2eAgMnPc2d1	domácí
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
právě	právě	k9	právě
Dvořáka	Dvořák	k1gMnSc4	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vedení	vedení	k1gNnSc1	vedení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Smetanovi	Smetana	k1gMnSc3	Smetana
vymezovalo	vymezovat	k5eAaImAgNnS	vymezovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mělo	mít	k5eAaImAgNnS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
rychlém	rychlý	k2eAgNnSc6d1	rychlé
uvedení	uvedení	k1gNnSc6	uvedení
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
nové	nový	k2eAgFnSc2d1	nová
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
oceňovali	oceňovat	k5eAaImAgMnP	oceňovat
i	i	k9	i
samotní	samotný	k2eAgMnPc1d1	samotný
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
zpěváci	zpěvák	k1gMnPc1	zpěvák
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
zkoušky	zkouška	k1gFnPc1	zkouška
tentokrát	tentokrát	k6eAd1	tentokrát
probíhaly	probíhat	k5eAaImAgFnP	probíhat
bez	bez	k7c2	bez
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
skutečně	skutečně	k6eAd1	skutečně
následovala	následovat	k5eAaImAgFnS	následovat
již	již	k6eAd1	již
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Dvořák	Dvořák	k1gMnSc1	Dvořák
dokončil	dokončit	k5eAaPmAgMnS	dokončit
partituru	partitura	k1gFnSc4	partitura
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
první	první	k4xOgNnSc1	první
nastudování	nastudování	k1gNnSc1	nastudování
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
kritickým	kritický	k2eAgInSc7d1	kritický
i	i	k8xC	i
diváckým	divácký	k2eAgInSc7d1	divácký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
chabé	chabý	k2eAgNnSc1d1	chabé
libreto	libreto	k1gNnSc1	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Dočkalo	dočkat	k5eAaPmAgNnS	dočkat
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k6eAd1	jen
čtyř	čtyři	k4xCgNnPc2	čtyři
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
verzi	verze	k1gFnSc3	verze
několikrát	několikrát	k6eAd1	několikrát
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
opera	opera	k1gFnSc1	opera
opět	opět	k6eAd1	opět
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
na	na	k7c4	na
pouhá	pouhý	k2eAgNnPc4d1	pouhé
dvě	dva	k4xCgNnPc4	dva
představení	představení	k1gNnPc4	představení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
výjimečně	výjimečně	k6eAd1	výjimečně
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
skladatel	skladatel	k1gMnSc1	skladatel
pro	pro	k7c4	pro
zpěváka	zpěvák	k1gMnSc4	zpěvák
Josefa	Josef	k1gMnSc4	Josef
Lva	Lev	k1gMnSc4	Lev
novou	nový	k2eAgFnSc4d1	nová
baladu	balada	k1gFnSc4	balada
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
dějství	dějství	k1gNnSc2	dějství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zpívána	zpíván	k2eAgFnSc1d1	zpívána
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
uvedení	uvedení	k1gNnSc4	uvedení
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
podrobil	podrobit	k5eAaPmAgMnS	podrobit
Dvořák	Dvořák	k1gMnSc1	Dvořák
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
1887	[number]	k4	1887
operu	opera	k1gFnSc4	opera
hlubší	hluboký	k2eAgFnSc4d2	hlubší
revizi	revize	k1gFnSc4	revize
<g/>
.	.	kIx.	.
</s>
<s>
Úpravy	úprava	k1gFnPc1	úprava
libreta	libreto	k1gNnSc2	libreto
provedl	provést	k5eAaPmAgMnS	provést
Václav	Václav	k1gMnSc1	Václav
Juda	judo	k1gNnSc2	judo
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
;	;	kIx,	;
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
a	a	k8xC	a
druhém	druhý	k4xOgInSc6	druhý
dějství	dějství	k1gNnSc6	dějství
se	se	k3xPyFc4	se
omezil	omezit	k5eAaPmAgMnS	omezit
na	na	k7c4	na
drobné	drobný	k2eAgInPc4d1	drobný
škrty	škrt	k1gInPc4	škrt
a	a	k8xC	a
na	na	k7c4	na
četné	četný	k2eAgInPc4d1	četný
stylistické	stylistický	k2eAgInPc4d1	stylistický
zásahy	zásah	k1gInPc4	zásah
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
však	však	k9	však
nedotýkaly	dotýkat	k5eNaImAgFnP	dotýkat
hudebního	hudební	k2eAgNnSc2d1	hudební
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
příliš	příliš	k6eAd1	příliš
"	"	kIx"	"
<g/>
lidové	lidový	k2eAgNnSc1d1	lidové
<g/>
"	"	kIx"	"
výrazivo	výrazivo	k1gNnSc1	výrazivo
a	a	k8xC	a
kresba	kresba	k1gFnSc1	kresba
charakterů	charakter	k1gInPc2	charakter
byly	být	k5eAaImAgFnP	být
uhlazeny	uhlazen	k2eAgFnPc1d1	uhlazena
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
zploštěny	zploštěn	k2eAgFnPc1d1	zploštěna
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
změnu	změna	k1gFnSc4	změna
prodělalo	prodělat	k5eAaPmAgNnS	prodělat
třetí	třetí	k4xOgNnSc1	třetí
dějství	dějství	k1gNnSc1	dějství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Novotný	Novotný	k1gMnSc1	Novotný
zcela	zcela	k6eAd1	zcela
přepsal	přepsat	k5eAaPmAgMnS	přepsat
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
zpřehlednil	zpřehlednit	k5eAaPmAgMnS	zpřehlednit
a	a	k8xC	a
zjednodušil	zjednodušit	k5eAaPmAgMnS	zjednodušit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mj.	mj.	kA	mj.
vyškrtnul	vyškrtnout	k5eAaPmAgInS	vyškrtnout
epizodní	epizodní	k2eAgFnPc4d1	epizodní
postavy	postava	k1gFnPc4	postava
Evy	Eva	k1gFnSc2	Eva
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
Sekáčka	Sekáček	k1gMnSc2	Sekáček
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
toto	tento	k3xDgNnSc4	tento
dějství	dějství	k1gNnSc4	dějství
rovněž	rovněž	k9	rovněž
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
znění	znění	k1gNnSc2	znění
převal	převal	k1gInSc4	převal
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
původní	původní	k2eAgInSc1d1	původní
balet	balet	k1gInSc1	balet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
autorsky	autorsky	k6eAd1	autorsky
definitivní	definitivní	k2eAgFnSc6d1	definitivní
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
katalogové	katalogový	k2eAgNnSc1d1	Katalogové
označení	označení	k1gNnSc1	označení
této	tento	k3xDgFnSc2	tento
revize	revize	k1gFnSc2	revize
je	být	k5eAaImIp3nS	být
B	B	kA	B
<g/>
151	[number]	k4	151
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
představena	představit	k5eAaPmNgFnS	představit
poprvé	poprvé	k6eAd1	poprvé
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
představení	představení	k1gNnSc2	představení
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
nastudování	nastudování	k1gNnSc6	nastudování
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
posledními	poslední	k2eAgMnPc7d1	poslední
za	za	k7c2	za
Dvořákova	Dvořákův	k2eAgInSc2d1	Dvořákův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
byl	být	k5eAaImAgMnS	být
i	i	k9	i
později	pozdě	k6eAd2	pozdě
uváděn	uvádět	k5eAaImNgInS	uvádět
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
jej	on	k3xPp3gMnSc4	on
nastudovalo	nastudovat	k5eAaBmAgNnS	nastudovat
opět	opět	k6eAd1	opět
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
v	v	k7c6	v
zásadní	zásadní	k2eAgFnSc6d1	zásadní
úpravě	úprava	k1gFnSc6	úprava
Karla	Karel	k1gMnSc2	Karel
Kovařovice	Kovařovice	k1gFnSc2	Kovařovice
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
opery	opera	k1gFnSc2	opera
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
také	také	k9	také
vydána	vydán	k2eAgFnSc1d1	vydána
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
opět	opět	k6eAd1	opět
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
poprvé	poprvé	k6eAd1	poprvé
nastudována	nastudován	k2eAgFnSc1d1	nastudována
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
uvedlo	uvést	k5eAaPmAgNnS	uvést
Krále	Král	k1gMnSc2	Král
a	a	k8xC	a
uhlíře	uhlíř	k1gMnSc2	uhlíř
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
počátkem	počátkem	k7c2	počátkem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
křivoklátských	křivoklátský	k2eAgInPc6d1	křivoklátský
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
pasáže	pasáž	k1gFnPc4	pasáž
hudby	hudba	k1gFnSc2	hudba
i	i	k8xC	i
libreta	libreto	k1gNnSc2	libreto
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
2	[number]	k4	2
<g/>
.	.	kIx.	.
verze	verze	k1gFnSc1	verze
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
revize	revize	k1gFnSc2	revize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
křivoklátských	křivoklátský	k2eAgInPc6d1	křivoklátský
lesích	les	k1gInPc6	les
<g/>
)	)	kIx)	)
Dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
rozveselených	rozveselený	k2eAgMnPc2d1	rozveselený
lovců	lovec	k1gMnPc2	lovec
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
honu	hon	k1gInSc2	hon
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
první	první	k4xOgFnSc2	první
skupiny	skupina	k1gFnSc2	skupina
lovců	lovec	k1gMnPc2	lovec
Oddechu	oddech	k1gInSc2	oddech
přejte	přát	k5eAaImRp2nP	přát
veselé	veselý	k2eAgFnPc4d1	veselá
píli	píle	k1gFnSc4	píle
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
lovců	lovec	k1gMnPc2	lovec
Když	když	k8xS	když
se	se	k3xPyFc4	se
honba	honba	k1gFnSc1	honba
dokonává	dokonávat	k5eAaImIp3nS	dokonávat
a	a	k8xC	a
společný	společný	k2eAgInSc1d1	společný
sbor	sbor	k1gInSc1	sbor
Spěšme	spěšit	k5eAaImRp1nP	spěšit
ku	k	k7c3	k
králi	král	k1gMnSc3	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
purkrabí	purkrabí	k1gMnSc1	purkrabí
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
árie	árie	k1gFnSc1	árie
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
Evě	Eva	k1gFnSc3	Eva
Marně	marně	k6eAd1	marně
prchám	prchat	k5eAaImIp1nS	prchat
v	v	k7c6	v
lesů	les	k1gInPc2	les
hloubi	hloub	k1gFnSc2	hloub
<g/>
)	)	kIx)	)
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
po	po	k7c6	po
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
lovu	lov	k1gInSc2	lov
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Jindřichův	Jindřichův	k2eAgInSc4d1	Jindřichův
pokyn	pokyn	k1gInSc4	pokyn
se	se	k3xPyFc4	se
lovci	lovec	k1gMnPc1	lovec
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
hledat	hledat	k5eAaImF	hledat
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Proměna	proměna	k1gFnSc1	proměna
–	–	k?	–
Lesní	lesní	k2eAgInSc4d1	lesní
palouk	palouk	k1gInSc4	palouk
před	před	k7c7	před
uhlířovou	uhlířův	k2eAgFnSc7d1	uhlířova
chatou	chata	k1gFnSc7	chata
<g/>
)	)	kIx)	)
Před	před	k7c7	před
chýší	chýš	k1gFnSc7	chýš
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žije	žít	k5eAaImIp3nS	žít
uhlíř	uhlíř	k1gMnSc1	uhlíř
Matěje	Matěj	k1gMnSc2	Matěj
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
Liduškou	Liduška	k1gFnSc7	Liduška
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
uhlíř	uhlíř	k1gMnSc1	uhlíř
Jeník	Jeník	k1gMnSc1	Jeník
domáhá	domáhat	k5eAaImIp3nS	domáhat
na	na	k7c6	na
Lidušce	Liduška	k1gFnSc6	Liduška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
směl	smět	k5eAaImAgMnS	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Liduška	Liduška	k1gFnSc1	Liduška
sice	sice	k8xC	sice
Jeníkovu	Jeníkův	k2eAgFnSc4d1	Jeníkova
lásku	láska	k1gFnSc4	láska
opětuje	opětovat	k5eAaImIp3nS	opětovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Jeníka	Jeník	k1gMnSc4	Jeník
nezahlídl	zahlídnout	k5eNaPmAgMnS	zahlídnout
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
Smím	smět	k5eAaImIp1nS	smět
<g/>
-li	i	k?	-li
vstoupit	vstoupit	k5eAaPmF	vstoupit
na	na	k7c4	na
chvilinku	chvilinka	k1gFnSc4	chvilinka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
obavy	obava	k1gFnPc1	obava
nejsou	být	k5eNaImIp3nP	být
plané	planý	k2eAgMnPc4d1	planý
<g/>
,	,	kIx,	,
zamilovaný	zamilovaný	k2eAgInSc4d1	zamilovaný
párek	párek	k1gInSc4	párek
brzy	brzy	k6eAd1	brzy
překvapí	překvapit	k5eAaPmIp3nP	překvapit
Liduščini	Liduščin	k2eAgMnPc1d1	Liduščin
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
předstírání	předstírání	k1gNnSc6	předstírání
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jeník	Jeník	k1gMnSc1	Jeník
a	a	k8xC	a
Liduškou	Liduška	k1gFnSc7	Liduška
přiznávají	přiznávat	k5eAaImIp3nP	přiznávat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
lásce	láska	k1gFnSc3	láska
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ostatně	ostatně	k6eAd1	ostatně
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
(	(	kIx(	(
<g/>
a	a	k8xC	a
zejména	zejména	k9	zejména
Anna	Anna	k1gFnSc1	Anna
<g/>
)	)	kIx)	)
považují	považovat	k5eAaImIp3nP	považovat
Jeníka	Jeník	k1gMnSc4	Jeník
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
chudou	chudý	k2eAgFnSc4d1	chudá
partii	partie	k1gFnSc4	partie
a	a	k8xC	a
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
mladým	mladý	k2eAgMnPc3d1	mladý
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
vídal	vídat	k5eAaImAgMnS	vídat
(	(	kIx(	(
<g/>
kvartet	kvartet	k1gInSc4	kvartet
Aj	aj	kA	aj
<g/>
,	,	kIx,	,
aj	aj	kA	aj
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
to	ten	k3xDgNnSc1	ten
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
přivádějí	přivádět	k5eAaImIp3nP	přivádět
uhlíři	uhlíř	k1gMnPc1	uhlíř
k	k	k7c3	k
Matějovi	Matěj	k1gMnSc3	Matěj
neznámého	známý	k2eNgMnSc2d1	neznámý
vznešeného	vznešený	k2eAgMnSc2d1	vznešený
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zabloudil	zabloudit	k5eAaPmAgMnS	zabloudit
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
návštěva	návštěva	k1gFnSc1	návštěva
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
mezi	mezi	k7c7	mezi
uhlíři	uhlíř	k1gMnPc7	uhlíř
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
zvědavými	zvědavý	k2eAgFnPc7d1	zvědavá
ženami	žena	k1gFnPc7	žena
pozdvižení	pozdvižení	k1gNnPc2	pozdvižení
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
hostu	host	k1gMnSc3	host
pohoštění	pohoštění	k1gNnSc4	pohoštění
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
a	a	k8xC	a
potěšení	potěšení	k1gNnSc3	potěšení
všech	všecek	k3xTgFnPc2	všecek
na	na	k7c6	na
prostém	prostý	k2eAgInSc6d1	prostý
pokrmu	pokrm	k1gInSc6	pokrm
rád	rád	k6eAd1	rád
pochutná	pochutnat	k5eAaPmIp3nS	pochutnat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
uhlíře	uhlíř	k1gMnSc2	uhlíř
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
dcera	dcera	k1gFnSc1	dcera
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
uhlíře	uhlíř	k1gMnSc4	uhlíř
Matěje	Matěj	k1gMnSc4	Matěj
potěší	potěšit	k5eAaPmIp3nS	potěšit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
jmenovci	jmenovec	k1gMnPc1	jmenovec
(	(	kIx(	(
<g/>
Matyáš	Matyáš	k1gMnSc1	Matyáš
anebo	anebo	k8xC	anebo
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jeden	jeden	k4xCgInSc4	jeden
svatej	svatat	k5eAaImRp2nS	svatat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Host	host	k1gMnSc1	host
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabloudil	zabloudit	k5eAaPmAgMnS	zabloudit
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
krále	král	k1gMnSc4	král
(	(	kIx(	(
<g/>
romance	romance	k1gFnSc1	romance
Vesele	vesele	k6eAd1	vesele
s	s	k7c7	s
králem	král	k1gMnSc7	král
hnal	hnát	k5eAaImAgMnS	hnát
jsem	být	k5eAaImIp1nS	být
se	s	k7c7	s
cvalem	cval	k1gInSc7	cval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
–	–	k?	–
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc4	ten
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
oním	onen	k3xDgInSc7	onen
zbloudilcem	zbloudilec	k1gMnSc7	zbloudilec
–	–	k?	–
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
zpovzdálí	zpovzdálí	k6eAd1	zpovzdálí
zvuk	zvuk	k1gInSc4	zvuk
dud	dudy	k1gFnPc2	dudy
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
vyzvání	vyzvání	k1gNnSc4	vyzvání
uhlíři	uhlíř	k1gMnPc1	uhlíř
přivádějí	přivádět	k5eAaImIp3nP	přivádět
dudáka	dudák	k1gMnSc4	dudák
Víta	Vít	k1gMnSc4	Vít
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
veselý	veselý	k2eAgInSc1d1	veselý
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
zapojí	zapojit	k5eAaPmIp3nS	zapojit
a	a	k8xC	a
Matěj	Matěj	k1gMnSc1	Matěj
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
zanotují	zanotovat	k5eAaPmIp3nP	zanotovat
i	i	k9	i
lidovou	lidový	k2eAgFnSc4d1	lidová
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
Nuž	nuž	k9	nuž
tedy	tedy	k9	tedy
dnes	dnes	k6eAd1	dnes
přejte	přát	k5eAaImRp2nP	přát
nám	my	k3xPp1nPc3	my
ples	ples	k1gInSc1	ples
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
...	...	k?	...
Zapuďme	zapudit	k5eAaPmRp1nP	zapudit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
srdce	srdce	k1gNnSc4	srdce
souží	soužit	k5eAaImIp3nP	soužit
<g/>
...	...	k?	...
Dudáčku	dudáček	k1gInSc6	dudáček
náš	náš	k3xOp1gInSc1	náš
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
to	ten	k3xDgNnSc4	ten
hráš	hráš	k1gInSc1	hráš
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připozdívá	připozdívat	k5eAaImIp3nS	připozdívat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
a	a	k8xC	a
modlitbou	modlitba	k1gFnSc7	modlitba
připravuje	připravovat	k5eAaImIp3nS	připravovat
k	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
(	(	kIx(	(
<g/>
Hospodine	Hospodin	k1gMnSc5	Hospodin
na	na	k7c4	na
výsosti	výsost	k1gFnPc4	výsost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Před	před	k7c7	před
uhlířovou	uhlířův	k2eAgFnSc7d1	uhlířova
chatou	chata	k1gFnSc7	chata
<g/>
)	)	kIx)	)
Teprve	teprve	k6eAd1	teprve
začíná	začínat	k5eAaImIp3nS	začínat
svítat	svítat	k5eAaImF	svítat
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
vzhůru	vzhůru	k6eAd1	vzhůru
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc1	árie
Nechce	chtít	k5eNaImIp3nS	chtít
spánek	spánek	k1gInSc4	spánek
v	v	k7c4	v
oči	oko	k1gNnPc4	oko
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
Lidušku	Liduška	k1gFnSc4	Liduška
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vykrádá	vykrádat	k5eAaImIp3nS	vykrádat
na	na	k7c4	na
tajnou	tajný	k2eAgFnSc4d1	tajná
schůzku	schůzka	k1gFnSc4	schůzka
s	s	k7c7	s
Jeníkem	Jeník	k1gMnSc7	Jeník
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
milence	milenec	k1gMnSc4	milenec
čekám	čekat	k5eAaImIp1nS	čekat
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
dnes	dnes	k6eAd1	dnes
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
ji	on	k3xPp3gFnSc4	on
svou	svůj	k3xOyFgFnSc7	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
překvapí	překvapit	k5eAaPmIp3nS	překvapit
<g/>
;	;	kIx,	;
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
naléhání	naléhání	k1gNnSc6	naléhání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
i	i	k8xC	i
soužením	soužení	k1gNnSc7	soužení
<g/>
.	.	kIx.	.
</s>
<s>
Neznámý	známý	k2eNgMnSc1d1	neznámý
host	host	k1gMnSc1	host
jí	on	k3xPp3gFnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vystrojení	vystrojení	k1gNnSc2	vystrojení
svatby	svatba	k1gFnSc2	svatba
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
<g/>
-li	i	k?	-li
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
hubičku	hubička	k1gFnSc4	hubička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Liduška	Liduška	k1gFnSc1	Liduška
ráda	rád	k2eAgFnSc1d1	ráda
přistoupí	přistoupit	k5eAaPmIp3nS	přistoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
zmocní	zmocnit	k5eAaPmIp3nS	zmocnit
záchvat	záchvat	k1gInSc1	záchvat
žárlivosti	žárlivost	k1gFnSc2	žárlivost
a	a	k8xC	a
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
krále	král	k1gMnSc4	král
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
(	(	kIx(	(
<g/>
Braň	bránit	k5eAaImRp2nS	bránit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
bídný	bídný	k2eAgInSc4d1	bídný
svůdníku	svůdník	k1gMnSc6	svůdník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
potyčku	potyčka	k1gFnSc4	potyčka
musí	muset	k5eAaImIp3nS	muset
ukončit	ukončit	k5eAaPmF	ukončit
až	až	k9	až
přispěchavší	přispěchavší	k2eAgMnPc1d1	přispěchavší
uhlíři	uhlíř	k1gMnPc1	uhlíř
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
král	král	k1gMnSc1	král
a	a	k8xC	a
Lidka	Lidka	k1gFnSc1	Lidka
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
<g/>
,	,	kIx,	,
Jeníka	Jeník	k1gMnSc2	Jeník
to	ten	k3xDgNnSc1	ten
nijak	nijak	k6eAd1	nijak
neuspokojí	uspokojit	k5eNaPmIp3nS	uspokojit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
chvíli	chvíle	k1gFnSc6	chvíle
přicházejí	přicházet	k5eAaImIp3nP	přicházet
lovci	lovec	k1gMnPc1	lovec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
(	(	kIx(	(
<g/>
Zde	zde	k6eAd1	zde
náš	náš	k3xOp1gMnSc1	náš
<g/>
,	,	kIx,	,
hle	hle	k0	hle
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
jim	on	k3xPp3gFnPc3	on
zapoví	zapovědět	k5eAaPmIp3nS	zapovědět
prozradit	prozradit	k5eAaPmF	prozradit
jeho	jeho	k3xOp3gFnSc4	jeho
totožnost	totožnost	k1gFnSc4	totožnost
a	a	k8xC	a
Matějovi	Matěj	k1gMnSc3	Matěj
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
děkuje	děkovat	k5eAaImIp3nS	děkovat
za	za	k7c4	za
pohostinnost	pohostinnost	k1gFnSc4	pohostinnost
<g/>
;	;	kIx,	;
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
je	on	k3xPp3gMnPc4	on
zve	zvát	k5eAaImIp3nS	zvát
o	o	k7c6	o
posvícení	posvícení	k1gNnSc6	posvícení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
Zdrávi	zdráv	k2eAgMnPc1d1	zdráv
buďte	budit	k5eAaImRp2nP	budit
<g/>
,	,	kIx,	,
na	na	k7c4	na
shledání	shledání	k1gNnSc4	shledání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lovecká	lovecký	k2eAgFnSc1d1	lovecká
družina	družina	k1gFnSc1	družina
se	se	k3xPyFc4	se
vesele	vesele	k6eAd1	vesele
chystá	chystat	k5eAaImIp3nS	chystat
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
<g/>
,	,	kIx,	,
Jeník	Jeník	k1gMnSc1	Jeník
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
rozhořčen	rozhořčit	k5eAaPmNgMnS	rozhořčit
<g/>
.	.	kIx.	.
</s>
<s>
Neuchlácholí	uchlácholit	k5eNaPmIp3nP	uchlácholit
jej	on	k3xPp3gMnSc4	on
ani	ani	k8xC	ani
smírná	smírný	k2eAgNnPc1d1	smírné
slova	slovo	k1gNnPc1	slovo
Matěje	Matěj	k1gMnSc2	Matěj
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	se	k3xPyFc4	se
odejít	odejít	k5eAaPmF	odejít
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Nezůstanu	zůstat	k5eNaPmIp1nS	zůstat
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
<g/>
,	,	kIx,	,
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
se	se	k3xPyFc4	se
dám	dát	k5eAaPmIp1nS	dát
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
naléhání	naléhání	k1gNnSc4	naléhání
uhlířovy	uhlířův	k2eAgFnSc2d1	uhlířova
rodiny	rodina	k1gFnSc2	rodina
nemůže	moct	k5eNaImIp3nS	moct
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sál	sál	k1gInSc1	sál
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
)	)	kIx)	)
Dvorská	dvorský	k2eAgFnSc1d1	dvorská
společnost	společnost	k1gFnSc1	společnost
tancuje	tancovat	k5eAaImIp3nS	tancovat
gavotu	gavota	k1gFnSc4	gavota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
vojně	vojna	k1gFnSc6	vojna
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
plesání	plesání	k1gNnSc1	plesání
neúčastní	účastnit	k5eNaImIp3nS	účastnit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
jej	on	k3xPp3gMnSc4	on
trápí	trápit	k5eAaImIp3nP	trápit
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Lidušku	Liduška	k1gFnSc4	Liduška
(	(	kIx(	(
<g/>
árie	árie	k1gFnPc4	árie
Jděte	jít	k5eAaImRp2nP	jít
<g/>
,	,	kIx,	,
nezávidím	závidět	k5eNaImIp1nS	závidět
blaha	blaho	k1gNnSc2	blaho
vám	vy	k3xPp2nPc3	vy
<g/>
...	...	k?	...
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
toužím	toužit	k5eAaImIp1nS	toužit
k	k	k7c3	k
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
Lidko	Lidka	k1gFnSc5	Lidka
<g/>
,	,	kIx,	,
duše	duše	k1gFnSc1	duše
má	mít	k5eAaImIp3nS	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Purkrabí	purkrabí	k1gMnSc1	purkrabí
Jindřich	Jindřich	k1gMnSc1	Jindřich
jej	on	k3xPp3gNnSc4	on
utěšuje	utěšovat	k5eAaImIp3nS	utěšovat
<g/>
,	,	kIx,	,
připomíná	připomínat	k5eAaImIp3nS	připomínat
mu	on	k3xPp3gMnSc3	on
královu	králův	k2eAgFnSc4d1	králova
přízeň	přízeň	k1gFnSc4	přízeň
a	a	k8xC	a
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
posvícení	posvícení	k1gNnSc1	posvícení
a	a	k8xC	a
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
uhlíř	uhlíř	k1gMnSc1	uhlíř
Matěj	Matěj	k1gMnSc1	Matěj
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
přichází	přicházet	k5eAaImIp3nS	přicházet
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Buď	buď	k8xC	buď
zdráv	zdráv	k2eAgMnSc1d1	zdráv
<g/>
,	,	kIx,	,
velemocný	velemocný	k2eAgInSc1d1	velemocný
králi	král	k1gMnPc7	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připravuje	připravovat	k5eAaImIp3nS	připravovat
přítomné	přítomný	k1gMnPc4	přítomný
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
žert	žert	k1gInSc4	žert
<g/>
:	:	kIx,	:
nechal	nechat	k5eAaPmAgMnS	nechat
uhlíře	uhlíř	k1gMnSc4	uhlíř
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
obvinit	obvinit	k5eAaPmF	obvinit
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
a	a	k8xC	a
Jeník	Jeník	k1gMnSc1	Jeník
je	on	k3xPp3gNnPc4	on
má	mít	k5eAaImIp3nS	mít
soudit	soudit	k5eAaImF	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
schová	schovat	k5eAaPmIp3nS	schovat
a	a	k8xC	a
stráže	stráž	k1gFnPc1	stráž
přivádějí	přivádět	k5eAaImIp3nP	přivádět
Matěje	Matěj	k1gMnPc4	Matěj
a	a	k8xC	a
Annu	Anna	k1gFnSc4	Anna
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Soud	soud	k1gInSc1	soud
<g/>
"	"	kIx"	"
jim	on	k3xPp3gInPc3	on
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
vražedné	vražedný	k2eAgNnSc1d1	vražedné
spiknutí	spiknutí	k1gNnSc1	spiknutí
proti	proti	k7c3	proti
cizinci	cizinec	k1gMnSc3	cizinec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ostatně	ostatně	k6eAd1	ostatně
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pobytu	pobyt	k1gInSc6	pobyt
u	u	k7c2	u
uhlíře	uhlíř	k1gMnSc2	uhlíř
stěží	stěží	k6eAd1	stěží
unikl	uniknout	k5eAaPmAgMnS	uniknout
ozbrojenému	ozbrojený	k2eAgNnSc3d1	ozbrojené
napadení	napadení	k1gNnSc3	napadení
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
soudců	soudce	k1gMnPc2	soudce
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
upřímně	upřímně	k6eAd1	upřímně
a	a	k8xC	a
prostořece	prostořeko	k6eAd1	prostořeko
<g/>
.	.	kIx.	.
</s>
<s>
Liduška	Liduška	k1gFnSc1	Liduška
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
soud	soud	k1gInSc1	soud
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
viníka	viník	k1gMnSc4	viník
Jeníka	Jeník	k1gMnSc4	Jeník
a	a	k8xC	a
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
(	(	kIx(	(
<g/>
árie	árie	k1gFnPc4	árie
Slyšte	slyšet	k5eAaImRp2nP	slyšet
<g/>
,	,	kIx,	,
rozvažte	rozvázat	k5eAaPmRp2nP	rozvázat
svůj	svůj	k3xOyFgInSc4	svůj
soud	soud	k1gInSc4	soud
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
Jeník	Jeník	k1gMnSc1	Jeník
dává	dávat	k5eAaImIp3nS	dávat
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Milenci	milenec	k1gMnPc1	milenec
si	se	k3xPyFc3	se
padají	padat	k5eAaImIp3nP	padat
do	do	k7c2	do
náruče	náruč	k1gFnSc2	náruč
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
přichází	přicházet	k5eAaImIp3nS	přicházet
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
a	a	k8xC	a
zdraví	zdravit	k5eAaImIp3nS	zdravit
své	svůj	k3xOyFgMnPc4	svůj
hosty	host	k1gMnPc4	host
(	(	kIx(	(
<g/>
Vítám	vítat	k5eAaImIp1nS	vítat
vás	vy	k3xPp2nPc4	vy
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
hosté	host	k1gMnPc1	host
milí	milý	k2eAgMnPc1d1	milý
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
,	,	kIx,	,
Liduška	Liduška	k1gFnSc1	Liduška
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
nádheru	nádhera	k1gFnSc4	nádhera
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
je	on	k3xPp3gInPc4	on
zve	zvát	k5eAaImIp3nS	zvát
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
pak	pak	k6eAd1	pak
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc4	sbor
Hodujme	hodovat	k5eAaImRp1nP	hodovat
<g/>
,	,	kIx,	,
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
píseň	píseň	k1gFnSc4	píseň
krále	král	k1gMnSc2	král
V	v	k7c6	v
radosti	radost	k1gFnSc6	radost
<g/>
,	,	kIx,	,
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
žertu	žert	k1gInSc3	žert
se	se	k3xPyFc4	se
mějte	mít	k5eAaImRp2nP	mít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
hostiteli	hostitel	k1gMnSc6	hostitel
domáhá	domáhat	k5eAaImIp3nS	domáhat
náhrady	náhrada	k1gFnPc4	náhrada
za	za	k7c4	za
onen	onen	k3xDgInSc4	onen
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
polibek	polibek	k1gInSc4	polibek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
Matyáš	Matyáš	k1gMnSc1	Matyáš
vtiskl	vtisknout	k5eAaPmAgMnS	vtisknout
Lidušce	Liduška	k1gFnSc3	Liduška
<g/>
,	,	kIx,	,
a	a	k8xC	a
král	král	k1gMnSc1	král
odevzdává	odevzdávat	k5eAaImIp3nS	odevzdávat
Lidušku	Liduška	k1gFnSc4	Liduška
Jeníkovi	Jeník	k1gMnSc3	Jeník
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
uhlířova	uhlířův	k2eAgFnSc1d1	uhlířova
rodina	rodina	k1gFnSc1	rodina
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnSc1	jejich
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
host	host	k1gMnSc1	host
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Liduška	Liduška	k1gFnSc1	Liduška
a	a	k8xC	a
Jeník	Jeník	k1gMnSc1	Jeník
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
oddat	oddat	k5eAaPmF	oddat
svému	svůj	k3xOyFgNnSc3	svůj
štěstí	štěstí	k1gNnSc3	štěstí
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
zpívají	zpívat	k5eAaImIp3nP	zpívat
chválu	chvála	k1gFnSc4	chvála
dobrotivému	dobrotivý	k2eAgMnSc3d1	dobrotivý
králi	král	k1gMnSc3	král
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Věčná	věčný	k2eAgFnSc1d1	věčná
tobě	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
králi	král	k1gMnPc7	král
<g/>
,	,	kIx,	,
sláva	sláva	k1gFnSc1	sláva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sál	sál	k1gInSc1	sál
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
)	)	kIx)	)
Dvorská	dvorský	k2eAgFnSc1d1	dvorská
společnost	společnost	k1gFnSc1	společnost
tancuje	tancovat	k5eAaImIp3nS	tancovat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Eva	Eva	k1gFnSc1	Eva
je	být	k5eAaImIp3nS	být
zasmušilá	zasmušilý	k2eAgFnSc1d1	zasmušilá
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc1	árie
Ó	Ó	kA	Ó
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mdle	mdle	k6eAd1	mdle
a	a	k8xC	a
zdlouha	zdlouha	k6eAd1	zdlouha
plyne	plynout	k5eAaImIp3nS	plynout
mi	já	k3xPp1nSc3	já
dnes	dnes	k6eAd1	dnes
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
však	však	k9	však
purkrabí	purkrabí	k1gMnSc1	purkrabí
Jindřich	Jindřich	k1gMnSc1	Jindřich
a	a	k8xC	a
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
se	se	k3xPyFc4	se
vášnivě	vášnivě	k6eAd1	vášnivě
vítá	vítat	k5eAaImIp3nS	vítat
(	(	kIx(	(
<g/>
duet	duet	k1gInSc4	duet
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
kéž	kéž	k9	kéž
písně	píseň	k1gFnPc4	píseň
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
král	král	k1gMnSc1	král
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Pochválen	pochválit	k5eAaPmNgInS	pochválit
buď	buď	k8xC	buď
na	na	k7c6	na
výsosti	výsost	k1gFnSc6	výsost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
vybízí	vybízet	k5eAaImIp3nS	vybízet
shromážděné	shromážděný	k2eAgInPc4d1	shromážděný
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
slibuje	slibovat	k5eAaImIp3nS	slibovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
šprým	šprým	k1gInSc4	šprým
<g/>
:	:	kIx,	:
dnes	dnes	k6eAd1	dnes
přišel	přijít	k5eAaPmAgMnS	přijít
uhlíř	uhlíř	k1gMnSc1	uhlíř
Matěj	Matěj	k1gMnSc1	Matěj
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
Liduškou	Liduška	k1gFnSc7	Liduška
na	na	k7c6	na
posvícení	posvícení	k1gNnSc6	posvícení
a	a	k8xC	a
král	král	k1gMnSc1	král
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
již	již	k6eAd1	již
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
předvést	předvést	k5eAaPmF	předvést
je	on	k3xPp3gInPc4	on
se	s	k7c7	s
zavázanýma	zavázaný	k2eAgNnPc7d1	zavázané
očima	oko	k1gNnPc7	oko
před	před	k7c4	před
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
je	on	k3xPp3gNnSc4	on
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
sebe	se	k3xPyFc2	se
přechovávali	přechovávat	k5eAaImAgMnP	přechovávat
vojenského	vojenský	k2eAgMnSc4d1	vojenský
zběha	zběh	k1gMnSc4	zběh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
žertem	žertem	k6eAd1	žertem
dostatečně	dostatečně	k6eAd1	dostatečně
pobaví	pobavit	k5eAaPmIp3nS	pobavit
<g/>
,	,	kIx,	,
sundává	sundávat	k5eAaImIp3nS	sundávat
král	král	k1gMnSc1	král
Matějovi	Matějův	k2eAgMnPc1d1	Matějův
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
pásku	pásek	k1gInSc2	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Uhlířova	Uhlířův	k2eAgFnSc1d1	Uhlířova
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
okolní	okolní	k2eAgFnSc3d1	okolní
nádheře	nádhera	k1gFnSc3	nádhera
nestačí	stačit	k5eNaBmIp3nS	stačit
divit	divit	k5eAaImF	divit
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
je	on	k3xPp3gInPc4	on
zve	zvát	k5eAaImIp3nS	zvát
k	k	k7c3	k
pohoštění	pohoštění	k1gNnSc3	pohoštění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
chce	chtít	k5eAaImIp3nS	chtít
Matěj	Matěj	k1gMnSc1	Matěj
přijmout	přijmout	k5eAaPmF	přijmout
jen	jen	k9	jen
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
"	"	kIx"	"
<g/>
hospodyně	hospodyně	k1gFnSc1	hospodyně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
královna	královna	k1gFnSc1	královna
vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Ha	ha	kA	ha
<g/>
,	,	kIx,	,
nalejte	nalít	k5eAaPmRp2nP	nalít
do	do	k7c2	do
pohárů	pohár	k1gInPc2	pohár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
Lidušky	Liduška	k1gFnSc2	Liduška
vyptává	vyptávat	k5eAaImIp3nS	vyptávat
na	na	k7c4	na
Jeníka	Jeník	k1gMnSc4	Jeník
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
vsi	ves	k1gFnSc6	ves
se	se	k3xPyFc4	se
neukázal	ukázat	k5eNaPmAgMnS	ukázat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
žádají	žádat	k5eAaImIp3nP	žádat
Jindřich	Jindřich	k1gMnSc1	Jindřich
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
Matyáši	Matyáš	k1gMnSc3	Matyáš
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
sňatku	sňatek	k1gInSc2	sňatek
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chce	chtít	k5eAaImIp3nS	chtít
mít	mít	k5eAaImF	mít
Evu	Eva	k1gFnSc4	Eva
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
coby	coby	k?	coby
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přivádí	přivádět	k5eAaImIp3nS	přivádět
důstojník	důstojník	k1gMnSc1	důstojník
Sekáček	sekáček	k1gInSc1	sekáček
před	před	k7c4	před
krále	král	k1gMnSc4	král
spoutaného	spoutaný	k2eAgMnSc2d1	spoutaný
Jeníka	Jeník	k1gMnSc2	Jeník
a	a	k8xC	a
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Pozor	pozor	k1gInSc1	pozor
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
milí	milý	k2eAgMnPc1d1	milý
braši	brach	k1gMnPc1	brach
<g/>
...	...	k?	...
Zprvu	zprvu	k6eAd1	zprvu
řádný	řádný	k2eAgInSc1d1	řádný
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
dvorný	dvorný	k2eAgInSc1d1	dvorný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeník	Jeník	k1gMnSc1	Jeník
byl	být	k5eAaImAgMnS	být
zprvu	zprvu	k6eAd1	zprvu
vzorný	vzorný	k2eAgMnSc1d1	vzorný
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
si	se	k3xPyFc3	se
povýšení	povýšení	k1gNnSc4	povýšení
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
však	však	k9	však
vzdychá	vzdychat	k5eAaImIp3nS	vzdychat
po	po	k7c6	po
domově	domov	k1gInSc6	domov
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
z	z	k7c2	z
vojska	vojsko	k1gNnSc2	vojsko
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
jej	on	k3xPp3gNnSc4	on
hodlá	hodlat	k5eAaImIp3nS	hodlat
potrestat	potrestat	k5eAaPmF	potrestat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
ožení	oženit	k5eAaPmIp3nS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeník	Jeník	k1gMnSc1	Jeník
se	se	k3xPyFc4	se
vzpouzí	vzpouzet	k5eAaImIp3nS	vzpouzet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nechce	chtít	k5eNaImIp3nS	chtít
zradit	zradit	k5eAaPmF	zradit
Lidušku	Liduška	k1gFnSc4	Liduška
–	–	k?	–
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
zamýšlenou	zamýšlený	k2eAgFnSc7d1	zamýšlená
nevěstou	nevěsta	k1gFnSc7	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
milenci	milenec	k1gMnPc1	milenec
i	i	k9	i
Matěj	Matěj	k1gMnSc1	Matěj
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
jsou	být	k5eAaImIp3nP	být
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
honosném	honosný	k2eAgNnSc6d1	honosné
prostředí	prostředí	k1gNnSc6	prostředí
nesvůj	nesvůj	k6eAd1	nesvůj
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
pobízí	pobízet	k5eAaImIp3nP	pobízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byl	být	k5eAaImAgInS	být
hostem	host	k1gMnSc7	host
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
hostem	host	k1gMnSc7	host
u	u	k7c2	u
Matěje	Matěj	k1gMnSc2	Matěj
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
tedy	tedy	k9	tedy
hodují	hodovat	k5eAaImIp3nP	hodovat
a	a	k8xC	a
tančí	tančit	k5eAaImIp3nP	tančit
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
číši	číš	k1gFnSc6	číš
pění	pění	k1gNnPc2	pění
<g/>
...	...	k?	...
Strun	struna	k1gFnPc2	struna
libý	libý	k2eAgInSc4d1	libý
zvuk	zvuk	k1gInSc4	zvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Matěj	Matěj	k1gMnSc1	Matěj
chce	chtít	k5eAaImIp3nS	chtít
ještě	ještě	k6eAd1	ještě
něco	něco	k3yInSc4	něco
(	(	kIx(	(
<g/>
árie	árie	k1gFnPc1	árie
Nuž	nuž	k9	nuž
poslyš	poslyšet	k5eAaPmRp2nS	poslyšet
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
to	ten	k3xDgNnSc1	ten
zvíš	zvědět	k5eAaPmIp2nS	zvědět
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
když	když	k8xS	když
on	on	k3xPp3gMnSc1	on
svému	svůj	k3xOyFgMnSc3	svůj
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
hostu	host	k1gMnSc3	host
k	k	k7c3	k
vůli	vůle	k1gFnSc3	vůle
požehná	požehnat	k5eAaPmIp3nS	požehnat
sňatku	sňatek	k1gInSc2	sňatek
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
Jeníka	Jeník	k1gMnSc2	Jeník
<g/>
,	,	kIx,	,
nechť	nechť	k9	nechť
tedy	tedy	k9	tedy
Matyáš	Matyáš	k1gMnSc1	Matyáš
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
lásce	láska	k1gFnSc3	láska
Jindřicha	Jindřich	k1gMnSc2	Jindřich
a	a	k8xC	a
Evy	Eva	k1gFnSc2	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
je	být	k5eAaImIp3nS	být
rozezlen	rozezlen	k2eAgMnSc1d1	rozezlen
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
jej	on	k3xPp3gNnSc4	on
chlácholí	chlácholit	k5eAaImIp3nP	chlácholit
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jej	on	k3xPp3gMnSc4	on
upomínají	upomínat	k5eAaImIp3nP	upomínat
o	o	k7c4	o
dané	daný	k2eAgNnSc4d1	dané
slovo	slovo	k1gNnSc4	slovo
(	(	kIx(	(
<g/>
ansámbl	ansámbl	k1gInSc1	ansámbl
Aj	aj	kA	aj
<g/>
,	,	kIx,	,
přílišná	přílišný	k2eAgFnSc1d1	přílišná
má	mít	k5eAaImIp3nS	mít
důvěra	důvěra	k1gFnSc1	důvěra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nepomáhá	pomáhat	k5eNaImIp3nS	pomáhat
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
hodlá	hodlat	k5eAaImIp3nS	hodlat
ve	v	k7c6	v
zlém	zlé	k1gNnSc6	zlé
odejít	odejít	k5eAaPmF	odejít
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
bezcitného	bezcitný	k2eAgInSc2d1	bezcitný
přepychu	přepych	k1gInSc2	přepych
(	(	kIx(	(
<g/>
árie	árie	k1gFnSc1	árie
Les	les	k1gInSc4	les
je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
jaré	jarý	k2eAgFnSc2d1	Jará
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
nakonec	nakonec	k6eAd1	nakonec
povolí	povolit	k5eAaPmIp3nS	povolit
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
uhlířova	uhlířův	k2eAgFnSc1d1	uhlířova
rodina	rodina	k1gFnSc1	rodina
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
krále	král	k1gMnSc4	král
urazili	urazit	k5eAaPmAgMnP	urazit
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
děkuje	děkovat	k5eAaImIp3nS	děkovat
za	za	k7c4	za
ponaučení	ponaučení	k1gNnSc4	ponaučení
(	(	kIx(	(
<g/>
V	v	k7c6	v
tobě	ty	k3xPp2nSc6	ty
k	k	k7c3	k
prsům	prs	k1gInPc3	prs
lid	lid	k1gInSc1	lid
svůj	svůj	k3xOyFgInSc4	svůj
tlačím	tlačit	k5eAaImIp1nS	tlačit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
chválí	chválit	k5eAaImIp3nP	chválit
královu	králův	k2eAgFnSc4d1	králova
moudrost	moudrost	k1gFnSc4	moudrost
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
Co	co	k8xS	co
svět	svět	k1gInSc1	svět
blaha	blaho	k1gNnSc2	blaho
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
lůnu	lůno	k1gNnSc3	lůno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
existující	existující	k2eAgFnPc1d1	existující
nahrávky	nahrávka	k1gFnPc1	nahrávka
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
opery	opera	k1gFnSc2	opera
s	s	k7c7	s
úpravami	úprava	k1gFnPc7	úprava
Karla	Karel	k1gMnSc2	Karel
Kovařovice	Kovařovice	k1gFnSc2	Kovařovice
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
nevydána	vydán	k2eNgFnSc1d1	nevydána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
)	)	kIx)	)
Bořek	Bořek	k1gMnSc1	Bořek
Rujan	Rujan	k1gMnSc1	Rujan
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Kalaš	Kalaš	k1gMnSc1	Kalaš
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
)	)	kIx)	)
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hanzalíková	Hanzalíková	k1gFnSc1	Hanzalíková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
)	)	kIx)	)
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
)	)	kIx)	)
Štefa	Štef	k1gMnSc2	Štef
Petrová	Petrová	k1gFnSc1	Petrová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Votava	Votava	k1gMnSc1	Votava
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Bohuš	Bohuš	k1gMnSc1	Bohuš
Holubář	holubář	k1gMnSc1	holubář
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Soumar	soumar	k1gMnSc1	soumar
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
řídí	řídit	k5eAaImIp3nS	řídit
František	František	k1gMnSc1	František
Dyk	Dyk	k?	Dyk
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
nevydána	vydán	k2eNgFnSc1d1	nevydána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
Jindrák	Jindrák	k1gMnSc1	Jindrák
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
)	)	kIx)	)
Eduard	Eduard	k1gMnSc1	Eduard
Haken	Haken	k1gMnSc1	Haken
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
)	)	kIx)	)
Ivana	Ivana	k1gFnSc1	Ivana
Mixová	Mixová	k1gFnSc1	Mixová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
)	)	kIx)	)
Milan	Milan	k1gMnSc1	Milan
Karpíšek	Karpíšek	k1gMnSc1	Karpíšek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
)	)	kIx)	)
Libuše	Libuše	k1gFnSc1	Libuše
Domanínská	Domanínský	k2eAgFnSc1d1	Domanínská
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
)	)	kIx)	)
Oldřich	Oldřich	k1gMnSc1	Oldřich
Spisar	Spisar	k1gMnSc1	Spisar
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
královský	královský	k2eAgMnSc1d1	královský
lovčí	lovčí	k1gMnSc1	lovčí
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Joran	Joran	k1gMnSc1	Joran
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
Vonásek	Vonásek	k1gMnSc1	Vonásek
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
řídí	řídit	k5eAaImIp3nS	řídit
František	František	k1gMnSc1	František
Dyk	Dyk	k?	Dyk
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
SU	SU	k?	SU
3078-2	[number]	k4	3078-2
611	[number]	k4	611
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
)	)	kIx)	)
René	René	k1gMnSc1	René
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
)	)	kIx)	)
Dalibor	Dalibor	k1gMnSc1	Dalibor
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
)	)	kIx)	)
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Drobková	Drobkový	k2eAgFnSc1d1	Drobková
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
)	)	kIx)	)
Viktor	Viktor	k1gMnSc1	Viktor
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
)	)	kIx)	)
Jitka	Jitka	k1gFnSc1	Jitka
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
)	)	kIx)	)
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kopp	Kopp	k1gMnSc1	Kopp
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kavalír	kavalír	k1gMnSc1	kavalír
<g/>
)	)	kIx)	)
Štěpán	Štěpán	k1gMnSc1	Štěpán
Buršík	Buršík	k1gMnSc1	Buršík
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kavalír	kavalír	k1gMnSc1	kavalír
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Prodělal	prodělat	k5eAaPmAgMnS	prodělat
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
řídí	řídit	k5eAaImIp3nS	řídit
Josef	Josef	k1gMnSc1	Josef
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výrazně	výrazně	k6eAd1	výrazně
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
nahrávka	nahrávka	k1gFnSc1	nahrávka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
televizní	televizní	k2eAgFnSc2d1	televizní
inscenace	inscenace	k1gFnSc2	inscenace
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
Orfeo	Orfeo	k6eAd1	Orfeo
C	C	kA	C
678	[number]	k4	678
062	[number]	k4	062
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
)	)	kIx)	)
Dalibor	Dalibor	k1gMnSc1	Dalibor
Jenis	Jenis	k1gFnSc2	Jenis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Liduška	Liduška	k1gFnSc1	Liduška
<g/>
)	)	kIx)	)
Lívia	Lívia	k1gFnSc1	Lívia
Ághová	Ághová	k1gFnSc1	Ághová
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
)	)	kIx)	)
Michelle	Michelle	k1gInSc1	Michelle
Breedt	Breedt	k2eAgInSc1d1	Breedt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Lehotsky	Lehotsky	k1gMnSc1	Lehotsky
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
)	)	kIx)	)
Markus	Markus	k1gMnSc1	Markus
Schäfer	Schäfer	k1gMnSc1	Schäfer
<g/>
.	.	kIx.	.
</s>
<s>
WDR	WDR	kA	WDR
Sinfonieorchester	Sinfonieorchester	k1gMnSc1	Sinfonieorchester
Köln	Köln	k1gMnSc1	Köln
<g/>
,	,	kIx,	,
Pražský	pražský	k2eAgInSc1d1	pražský
komorní	komorní	k2eAgInSc1d1	komorní
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
WDR	WDR	kA	WDR
Rundfunkchor	Rundfunkchor	k1gMnSc1	Rundfunkchor
Köln	Köln	k1gMnSc1	Köln
řídí	řídit	k5eAaImIp3nS	řídit
Gerd	Gerd	k1gMnSc1	Gerd
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
nahrávku	nahrávka	k1gFnSc4	nahrávka
písně	píseň	k1gFnSc2	píseň
Lidušky	Liduška	k1gFnSc2	Liduška
ze	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
dějství	dějství	k1gNnSc6	dějství
Hle	hle	k0	hle
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
milence	milenec	k1gMnSc4	milenec
čekám	čekat	k5eAaImIp1nS	čekat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Marie	Maria	k1gFnSc2	Maria
Tauberové	Tauberová	k1gFnSc2	Tauberová
vydal	vydat	k5eAaPmAgInS	vydat
Radioservis	Radioservis	k1gInSc1	Radioservis
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
na	na	k7c6	na
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
romantická	romantický	k2eAgFnSc1d1	romantická
opera	opera	k1gFnSc1	opera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
CR	cr	k0	cr
<g/>
0	[number]	k4	0
<g/>
782	[number]	k4	782
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
televizní	televizní	k2eAgFnSc4d1	televizní
inscenaci	inscenace	k1gFnSc4	inscenace
opery	opera	k1gFnSc2	opera
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
partituře	partitura	k1gFnSc3	partitura
značně	značně	k6eAd1	značně
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
obsazení	obsazení	k1gNnSc6	obsazení
jako	jako	k8xS	jako
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
nahrávka	nahrávka	k1gFnSc1	nahrávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
inscenace	inscenace	k1gFnSc2	inscenace
byl	být	k5eAaImAgMnS	být
Milan	Milan	k1gMnSc1	Milan
Macků	Macků	k1gMnSc1	Macků
<g/>
.	.	kIx.	.
</s>
<s>
Inscenace	inscenace	k1gFnSc1	inscenace
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vysílána	vysílán	k2eAgFnSc1d1	vysílána
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
