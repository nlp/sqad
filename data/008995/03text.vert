<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
charakteristika	charakteristika	k1gFnSc1	charakteristika
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
poloha	poloha	k1gFnSc1	poloha
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
vektorová	vektorový	k2eAgFnSc1d1	vektorová
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
udává	udávat	k5eAaImIp3nS	udávat
jak	jak	k6eAd1	jak
velikost	velikost	k1gFnSc1	velikost
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
její	její	k3xOp3gInSc4	její
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
dva	dva	k4xCgMnPc1	dva
běžci	běžec	k1gMnPc1	běžec
závodí	závodit	k5eAaImIp3nP	závodit
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
stejné	stejný	k2eAgFnSc6d1	stejná
trajektorii	trajektorie	k1gFnSc6	trajektorie
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
závodu	závod	k1gInSc2	závod
mají	mít	k5eAaImIp3nP	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
stejnou	stejný	k2eAgFnSc4d1	stejná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
závodníků	závodník	k1gMnPc2	závodník
doběhne	doběhnout	k5eAaPmIp3nS	doběhnout
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
pohyby	pohyb	k1gInPc1	pohyb
obou	dva	k4xCgMnPc2	dva
závodníků	závodník	k1gMnPc2	závodník
stejné	stejná	k1gFnSc2	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
urazí	urazit	k5eAaPmIp3nP	urazit
tedy	tedy	k9	tedy
danou	daný	k2eAgFnSc4d1	daná
dráhu	dráha	k1gFnSc4	dráha
v	v	k7c6	v
rozdílném	rozdílný	k2eAgInSc6d1	rozdílný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Veličina	veličina	k1gFnSc1	veličina
charakterizující	charakterizující	k2eAgFnSc7d1	charakterizující
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
pohybech	pohyb	k1gInPc6	pohyb
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
rychlost	rychlost	k1gFnSc4	rychlost
časovou	časový	k2eAgFnSc4d1	časová
změnu	změna	k1gFnSc4	změna
polohy	poloha	k1gFnSc2	poloha
při	při	k7c6	při
mechanickém	mechanický	k2eAgInSc6d1	mechanický
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Obecněji	obecně	k6eAd2	obecně
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
časové	časový	k2eAgFnSc2d1	časová
změny	změna	k1gFnSc2	změna
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rychlost	rychlost	k1gFnSc4	rychlost
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
společenských	společenský	k2eAgFnPc2d1	společenská
změn	změna	k1gFnPc2	změna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časová	časový	k2eAgFnSc1d1	časová
změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
,	,	kIx,	,
záporné	záporný	k2eAgNnSc1d1	záporné
zrychlení	zrychlení	k1gNnSc1	zrychlení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zpomalení	zpomalení	k1gNnSc1	zpomalení
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc4	dva
veličiny	veličina	k1gFnPc4	veličina
vyjadřuji	vyjadřovat	k5eAaImIp1nS	vyjadřovat
změnu	změna	k1gFnSc4	změna
resp.	resp.	kA	resp.
přírůstek	přírůstek	k1gInSc1	přírůstek
či	či	k8xC	či
úbytek	úbytek	k1gInSc1	úbytek
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
nekonečně	konečně	k6eNd1	konečně
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
derivaci	derivace	k1gFnSc4	derivace
dráhy	dráha	k1gFnSc2	dráha
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
popř.	popř.	kA	popř.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
velocity	velocit	k1gInPc1	velocit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnSc2	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
metr	metr	k1gInSc1	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
,	,	kIx,	,
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
používané	používaný	k2eAgFnPc1d1	používaná
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
větru	vítr	k1gInSc2	vítr
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kilometr	kilometr	k1gInSc4	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
1	[number]	k4	1
m	m	kA	m
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
=	=	kIx~	=
3,6	[number]	k4	3,6
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
(	(	kIx(	(
<g/>
některých	některý	k3yIgMnPc2	některý
<g/>
)	)	kIx)	)
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
namísto	namísto	k7c2	namísto
něho	on	k3xPp3gMnSc2	on
běžná	běžný	k2eAgFnSc1d1	běžná
míle	míle	k1gFnSc1	míle
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
</s>
</p>
<p>
<s>
V	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
praxi	praxe	k1gFnSc6	praxe
a	a	k8xC	a
v	v	k7c6	v
letectví	letectví	k1gNnSc6	letectví
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jednotka	jednotka	k1gFnSc1	jednotka
uzel	uzel	k1gInSc1	uzel
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
knot	knot	k1gInSc1	knot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
kn	kn	k?	kn
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kt	kt	k?	kt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgFnSc1d1	námořní
míle	míle	k1gFnSc1	míle
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokým	vysoký	k2eAgFnPc3d1	vysoká
rychlostem	rychlost	k1gFnPc3	rychlost
astronomických	astronomický	k2eAgInPc2d1	astronomický
objektů	objekt	k1gInPc2	objekt
se	se	k3xPyFc4	se
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
tisícinásobek	tisícinásobek	k1gInSc1	tisícinásobek
hlavní	hlavní	k2eAgFnSc2d1	hlavní
jednotky	jednotka	k1gFnSc2	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
kilometr	kilometr	k1gInSc4	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
rychlosti	rychlost	k1gFnSc2	rychlost
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
liší	lišit	k5eAaImIp3nS	lišit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
celková	celkový	k2eAgFnSc1d1	celková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
uražená	uražený	k2eAgFnSc1d1	uražená
za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
80	[number]	k4	80
kilometrů	kilometr	k1gInPc2	kilometr
ujetá	ujetý	k2eAgFnSc1d1	ujetá
za	za	k7c4	za
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
80	[number]	k4	80
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
320	[number]	k4	320
kilometrů	kilometr	k1gInPc2	kilometr
ujeto	ujet	k2eAgNnSc1d1	ujeto
za	za	k7c4	za
4	[number]	k4	4
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
opět	opět	k6eAd1	opět
80	[number]	k4	80
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
v	v	k7c6	v
kilometrech	kilometr	k1gInPc6	kilometr
(	(	kIx(	(
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
vydělena	vydělen	k2eAgNnPc1d1	vyděleno
časem	časem	k6eAd1	časem
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
kilometry	kilometr	k1gInPc4	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
(	(	kIx(	(
<g/>
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
nepopisuje	popisovat	k5eNaImIp3nS	popisovat
změny	změna	k1gFnPc4	změna
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
nastat	nastat	k5eAaPmF	nastat
v	v	k7c6	v
kratších	krátký	k2eAgInPc6d2	kratší
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dělená	dělený	k2eAgFnSc1d1	dělená
celkovým	celkový	k2eAgInSc7d1	celkový
časem	čas	k1gInSc7	čas
cesty	cesta	k1gFnSc2	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
t	t	k?	t
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
exaktněji	exaktně	k6eAd2	exaktně
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-t_	_	k?	-t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
rychlost	rychlost	k1gFnSc1	rychlost
==	==	k?	==
</s>
</p>
<p>
<s>
Okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
časovém	časový	k2eAgInSc6d1	časový
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
časový	časový	k2eAgInSc4d1	časový
okamžik	okamžik	k1gInSc4	okamžik
nekonečně	konečně	k6eNd1	konečně
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
vypočte	vypočíst	k5eAaPmIp3nS	vypočíst
se	se	k3xPyFc4	se
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
rychlost	rychlost	k1gFnSc1	rychlost
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
derivace	derivace	k1gFnSc1	derivace
dráhy	dráha	k1gFnSc2	dráha
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
limitním	limitní	k2eAgInSc7d1	limitní
přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
průměrné	průměrný	k2eAgFnSc2d1	průměrná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
lim	limo	k1gNnPc2	limo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
right	right	k1gInSc1	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
-t_	_	k?	-t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Rychlost	rychlost	k1gFnSc1	rychlost
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
po	po	k7c6	po
kružnici	kružnice	k1gFnSc6	kružnice
se	se	k3xPyFc4	se
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
rychlosti	rychlost	k1gFnSc2	rychlost
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
veličiny	veličina	k1gFnPc1	veličina
–	–	k?	–
obvodová	obvodový	k2eAgFnSc1d1	obvodová
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
úhlová	úhlový	k2eAgFnSc1d1	úhlová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
rozměrem	rozměr	k1gInSc7	rozměr
i	i	k8xC	i
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c4	mezi
obvodovou	obvodový	k2eAgFnSc4d1	obvodová
a	a	k8xC	a
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
rychlosti	rychlost	k1gFnPc4	rychlost
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
obvodovou	obvodový	k2eAgFnSc7d1	obvodová
a	a	k8xC	a
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
rychlostí	rychlost	k1gFnSc7	rychlost
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc1	vztah
</s>
</p>
<p>
<s>
v	v	k7c6	v
=	=	kIx~	=
ω	ω	k?	ω
·	·	k?	·
r	r	kA	r
<g/>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
ω	ω	k?	ω
je	být	k5eAaImIp3nS	být
úhlová	úhlový	k2eAgFnSc1d1	úhlová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
r	r	kA	r
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vektorovém	vektorový	k2eAgNnSc6d1	vektorové
vyjádření	vyjádření	k1gNnSc6	vyjádření
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
vektorového	vektorový	k2eAgNnSc2d1	vektorové
vyjádření	vyjádření	k1gNnSc2	vyjádření
úhlové	úhlový	k2eAgFnSc2d1	úhlová
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Relativistická	relativistický	k2eAgFnSc1d1	relativistická
rychlost	rychlost	k1gFnSc1	rychlost
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
určování	určování	k1gNnSc6	určování
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
relativistické	relativistický	k2eAgFnSc6d1	relativistická
mechanice	mechanika	k1gFnSc6	mechanika
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
(	(	kIx(	(
<g/>
nerelativistické	relativistický	k2eNgFnSc2d1	nerelativistická
<g/>
)	)	kIx)	)
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
rychlost	rychlost	k1gFnSc1	rychlost
ve	v	k7c6	v
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
složkami	složka	k1gFnPc7	složka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
budou	být	k5eAaImBp3nP	být
složky	složka	k1gFnPc1	složka
rychlosti	rychlost	k1gFnSc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
</s>
</p>
<p>
<s>
tohoto	tento	k3xDgInSc2	tento
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
vůči	vůči	k7c3	vůči
soustavě	soustava	k1gFnSc3	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
mít	mít	k5eAaImF	mít
následující	následující	k2eAgFnPc4d1	následující
složky	složka	k1gFnPc4	složka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
vyjádření	vyjádření	k1gNnSc1	vyjádření
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
však	však	k9	však
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
souřadnice	souřadnice	k1gFnPc4	souřadnice
(	(	kIx(	(
<g/>
prostorové	prostorový	k2eAgFnPc4d1	prostorová
i	i	k8xC	i
časové	časový	k2eAgFnPc4d1	časová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
transformují	transformovat	k5eAaBmIp3nP	transformovat
odlišně	odlišně	k6eAd1	odlišně
než	než	k8xS	než
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
soustava	soustava	k1gFnSc1	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
soustavě	soustava	k1gFnSc3	soustava
S	s	k7c7	s
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
Přičemž	přičemž	k6eAd1	přičemž
pohyb	pohyb	k1gInSc1	pohyb
probíhá	probíhat	k5eAaImIp3nS	probíhat
podél	podél	k7c2	podél
os	osa	k1gFnPc2	osa
x	x	k?	x
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
'	'	kIx"	'
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vzájemně	vzájemně	k6eAd1	vzájemně
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složky	složka	k1gFnPc1	složka
rychlosti	rychlost	k1gFnSc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
speciální	speciální	k2eAgFnSc2d1	speciální
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
diferencováním	diferencování	k1gNnSc7	diferencování
dostaneme	dostat	k5eAaPmIp1nP	dostat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x-w	x	k?	x-w
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}}	}}}}}}}	k?	}}}}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
y	y	k?	y
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t-	t-	k?	t-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}}	}}}}}}}	k?	}}}}}}}
</s>
</p>
<p>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
dostaneme	dostat	k5eAaPmIp1nP	dostat
transformační	transformační	k2eAgInPc4d1	transformační
vztahy	vztah	k1gInPc4	vztah
pro	pro	k7c4	pro
složky	složka	k1gFnPc4	složka
relativistické	relativistický	k2eAgFnSc2d1	relativistická
rychlosti	rychlost	k1gFnSc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
-w	-w	k?	-w
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
vztahy	vztah	k1gInPc1	vztah
představují	představovat	k5eAaImIp3nP	představovat
relativistickou	relativistický	k2eAgFnSc4d1	relativistická
transformaci	transformace	k1gFnSc4	transformace
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
malá	malý	k2eAgNnPc4d1	malé
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
tyto	tento	k3xDgInPc4	tento
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c4	v
vztahy	vztah	k1gInPc4	vztah
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
(	(	kIx(	(
<g/>
nerelativistickou	relativistický	k2eNgFnSc4d1	nerelativistická
<g/>
)	)	kIx)	)
transformaci	transformace	k1gFnSc4	transformace
rychlosti	rychlost	k1gFnSc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
-w	-w	k?	-w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Vyjádření	vyjádření	k1gNnSc4	vyjádření
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
složek	složka	k1gFnPc2	složka
rychlosti	rychlost	k1gFnSc2	rychlost
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
získáme	získat	k5eAaPmIp1nP	získat
záměnou	záměna	k1gFnSc7	záměna
čárkovaných	čárkovaný	k2eAgFnPc2d1	čárkovaná
a	a	k8xC	a
nečárkovaných	čárkovaný	k2eNgFnPc2d1	čárkovaný
veličin	veličina	k1gFnPc2	veličina
a	a	k8xC	a
záměnou	záměna	k1gFnSc7	záměna
znaménka	znaménko	k1gNnSc2	znaménko
u	u	k7c2	u
rychlosti	rychlost	k1gFnSc2	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wv_	wv_	k?	wv_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
uvedených	uvedený	k2eAgInPc2d1	uvedený
transformačních	transformační	k2eAgInPc2d1	transformační
vztahů	vztah	k1gInPc2	vztah
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
bude	být	k5eAaImBp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
druhému	druhý	k4xOgInSc3	druhý
postulátu	postulát	k1gInSc3	postulát
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
totiž	totiž	k9	totiž
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
světelný	světelný	k2eAgInSc4d1	světelný
paprsek	paprsek	k1gInSc4	paprsek
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
osy	osa	k1gFnSc2	osa
x	x	k?	x
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
stejného	stejný	k2eAgInSc2d1	stejný
paprsku	paprsek	k1gInSc2	paprsek
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
c-w	c	k?	c-w
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
wc	wc	k?	wc
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
těchto	tento	k3xDgInPc2	tento
transformačních	transformační	k2eAgInPc2d1	transformační
vztahů	vztah	k1gInPc2	vztah
je	být	k5eAaImIp3nS	být
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
rychlostí	rychlost	k1gFnSc7	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
'	'	kIx"	'
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
prime	prim	k1gInSc5	prim
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
9	[number]	k4	9
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
osy	osa	k1gFnSc2	osa
x	x	k?	x
a	a	k8xC	a
samotná	samotný	k2eAgFnSc1d1	samotná
soustava	soustava	k1gFnSc1	soustava
S	s	k7c7	s
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
soustavě	soustava	k1gFnSc3	soustava
S	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
8	[number]	k4	8
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
hmotného	hmotný	k2eAgInSc2d1	hmotný
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
S	s	k7c7	s
rovna	roven	k2eAgFnSc1d1	rovna
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
7	[number]	k4	7
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Relativistická	relativistický	k2eAgFnSc1d1	relativistická
mechanika	mechanika	k1gFnSc1	mechanika
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
hodnotě	hodnota	k1gFnSc3	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0,988	[number]	k4	0,988
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
9	[number]	k4	9
<g/>
c	c	k0	c
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
8	[number]	k4	8
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
8	[number]	k4	8
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
9	[number]	k4	9
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
988	[number]	k4	988
<g/>
\	\	kIx~	\
<g/>
,4	,4	k4	,4
<g/>
c	c	k0	c
<g/>
<	<	kIx(	<
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c7	za
podsvětelnou	podsvětelna	k1gFnSc7	podsvětelna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
<	<	kIx(	<
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
světelnou	světelný	k2eAgFnSc4d1	světelná
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nadsvětelnou	nadsvětelna	k1gFnSc7	nadsvětelna
při	při	k7c6	při
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
>	>	kIx)	>
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
</s>
</p>
<p>
<s>
Nadsvětelná	Nadsvětelný	k2eAgFnSc1d1	Nadsvětelná
rychlost	rychlost	k1gFnSc1	rychlost
</s>
</p>
<p>
<s>
Rapidita	Rapidita	k1gFnSc1	Rapidita
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
rychlostí	rychlost	k1gFnPc2	rychlost
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
</s>
</p>
<p>
<s>
Skládání	skládání	k1gNnSc1	skládání
rychlostí	rychlost	k1gFnPc2	rychlost
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rychlost	rychlost	k1gFnSc1	rychlost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rychlost	rychlost	k1gFnSc1	rychlost
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
