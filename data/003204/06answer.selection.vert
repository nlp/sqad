<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
<g/>
,	,	kIx,	,
rychlých	rychlý	k2eAgInPc6d1	rychlý
či	či	k8xC	či
komplikovaných	komplikovaný	k2eAgInPc6d1	komplikovaný
rytmicky	rytmicky	k6eAd1	rytmicky
sekaných	sekaný	k2eAgInPc6d1	sekaný
kytarových	kytarový	k2eAgInPc6d1	kytarový
riffech	riff	k1gInPc6	riff
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
<g/>
.	.	kIx.	.
</s>
