<p>
<s>
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
(	(	kIx(	(
<g/>
původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ج	ج	k?	ج
ط	ط	k?	ط
–	–	k?	–
Džabal	Džabal	k1gInSc1	Džabal
at-Tárik	at-Tárika	k1gFnPc2	at-Tárika
<g/>
,	,	kIx,	,
Tárikova	Tárikův	k2eAgFnSc1d1	Tárikova
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
The	The	k1gMnSc1	The
Rock	rock	k1gInSc1	rock
–	–	k?	–
"	"	kIx"	"
<g/>
skála	skála	k1gFnSc1	skála
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
korunní	korunní	k2eAgFnSc2d1	korunní
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
)	)	kIx)	)
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strategická	strategický	k2eAgFnSc1d1	strategická
poloha	poloha	k1gFnSc1	poloha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
Gibraltarskou	gibraltarský	k2eAgFnSc4d1	Gibraltarská
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Atlantikem	Atlantik	k1gInSc7	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
postoupit	postoupit	k5eAaPmF	postoupit
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
Británii	Británie	k1gFnSc4	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
Utrechtskou	Utrechtský	k2eAgFnSc7d1	Utrechtská
smlouvou	smlouva	k1gFnSc7	smlouva
ukončující	ukončující	k2eAgFnSc2d1	ukončující
války	válka	k1gFnSc2	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
gibraltarská	gibraltarský	k2eAgFnSc1d1	Gibraltarská
posádka	posádka	k1gFnSc1	posádka
stala	stát	k5eAaPmAgFnS	stát
formálně	formálně	k6eAd1	formálně
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
a	a	k8xC	a
2002	[number]	k4	2002
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
referenda	referendum	k1gNnSc2	referendum
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
ke	k	k7c3	k
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
6,5	[number]	k4	6,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
Španělskem	Španělsko	k1gNnSc7	Španělsko
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
1,2	[number]	k4	1,2
km	km	kA	km
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
12	[number]	k4	12
km	km	kA	km
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Vápencová	vápencový	k2eAgFnSc1d1	vápencová
skála	skála	k1gFnSc1	skála
Rock	rock	k1gInSc1	rock
of	of	k?	of
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
a	a	k8xC	a
která	který	k3yRgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Gibraltarskou	gibraltarský	k2eAgFnSc4d1	Gibraltarská
zátoku	zátoka	k1gFnSc4	zátoka
od	od	k7c2	od
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
426	[number]	k4	426
m.	m.	k?	m.
Na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
není	být	k5eNaImIp3nS	být
zdroj	zdroj	k1gInSc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
sběru	sběr	k1gInSc2	sběr
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
získání	získání	k1gNnSc3	získání
využívá	využívat	k5eAaPmIp3nS	využívat
reverzní	reverzní	k2eAgFnSc1d1	reverzní
osmóza	osmóza	k1gFnSc1	osmóza
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
činí	činit	k5eAaImIp3nS	činit
přes	přes	k7c4	přes
4200	[number]	k4	4200
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
pevninská	pevninský	k2eAgFnSc1d1	pevninská
silnice	silnice	k1gFnSc1	silnice
na	na	k7c4	na
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
kříží	křížit	k5eAaImIp3nS	křížit
dráhu	dráha	k1gFnSc4	dráha
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
unikátní	unikátní	k2eAgFnSc1d1	unikátní
křižovatka	křižovatka	k1gFnSc1	křižovatka
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
běžnými	běžný	k2eAgInPc7d1	běžný
semafory	semafor	k1gInPc7	semafor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
evropská	evropský	k2eAgFnSc1d1	Evropská
kolonie	kolonie	k1gFnSc1	kolonie
opic	opice	k1gFnPc2	opice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
opice	opice	k1gFnPc1	opice
<g/>
,	,	kIx,	,
náležící	náležící	k2eAgInSc1d1	náležící
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
magot	magot	k1gMnSc1	magot
bezocasý	bezocasý	k2eAgMnSc1d1	bezocasý
(	(	kIx(	(
<g/>
Macaca	Macaca	k1gMnSc1	Macaca
sylvanus	sylvanus	k1gMnSc1	sylvanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
velmi	velmi	k6eAd1	velmi
hýčkají	hýčkat	k5eAaImIp3nP	hýčkat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
legendě	legenda	k1gFnSc3	legenda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
bude	být	k5eAaImBp3nS	být
britský	britský	k2eAgInSc1d1	britský
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
zde	zde	k6eAd1	zde
opice	opice	k1gFnPc1	opice
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
až	až	k6eAd1	až
přehnané	přehnaný	k2eAgFnSc2d1	přehnaná
péče	péče	k1gFnSc2	péče
jsou	být	k5eAaImIp3nP	být
opice	opice	k1gFnPc1	opice
vůči	vůči	k7c3	vůči
lidem	člověk	k1gMnPc3	člověk
značně	značně	k6eAd1	značně
drzé	drzý	k2eAgInPc4d1	drzý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1	Gibraltarská
skála	skála	k1gFnSc1	skála
byla	být	k5eAaImAgFnS	být
obývána	obývat	k5eAaImNgFnS	obývat
už	už	k6eAd1	už
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
lomu	lom	k1gInSc6	lom
Forbes	forbes	k1gInSc1	forbes
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
objevena	objevit	k5eAaPmNgFnS	objevit
první	první	k4xOgFnSc1	první
lebka	lebka	k1gFnSc1	lebka
neandrtálce	neandrtálec	k1gMnSc4	neandrtálec
ještě	ještě	k9	ještě
před	před	k7c7	před
slavným	slavný	k2eAgInSc7d1	slavný
objevem	objev	k1gInSc7	objev
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Neanderthalu	Neanderthal	k1gInSc6	Neanderthal
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
z	z	k7c2	z
fénických	fénický	k2eAgInPc2d1	fénický
<g/>
,	,	kIx,	,
kartaginských	kartaginský	k2eAgInPc2d1	kartaginský
i	i	k8xC	i
římských	římský	k2eAgInPc2d1	římský
pramenů	pramen	k1gInPc2	pramen
(	(	kIx(	(
<g/>
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
Calpe	Calp	k1gInSc5	Calp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
odsud	odsud	k6eAd1	odsud
odpluli	odplout	k5eAaPmAgMnP	odplout
barbarští	barbarský	k2eAgMnPc1d1	barbarský
Vandalové	Vandal	k1gMnPc1	Vandal
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
711	[number]	k4	711
zde	zde	k6eAd1	zde
přistálo	přistát	k5eAaImAgNnS	přistát
vojsko	vojsko	k1gNnSc1	vojsko
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
marockých	marocký	k2eAgMnPc2d1	marocký
Berberů	Berber	k1gMnPc2	Berber
vedené	vedený	k2eAgInPc1d1	vedený
Tárikem	Tárik	k1gInSc7	Tárik
ibn	ibn	k?	ibn
Zijádem	Zijád	k1gMnSc7	Zijád
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dobylo	dobýt	k5eAaPmAgNnS	dobýt
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
vizigótskou	vizigótský	k2eAgFnSc4d1	Vizigótská
říši	říše	k1gFnSc4	říše
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Skála	skála	k1gFnSc1	skála
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
po	po	k7c6	po
dobyvateli	dobyvatel	k1gMnSc3	dobyvatel
na	na	k7c4	na
Džabal	Džabal	k1gInSc4	Džabal
at-Tárik	at-Tárika	k1gFnPc2	at-Tárika
(	(	kIx(	(
<g/>
Tárikova	Tárikův	k2eAgFnSc1d1	Tárikova
hora	hora	k1gFnSc1	hora
či	či	k8xC	či
skála	skála	k1gFnSc1	skála
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
.	.	kIx.	.
</s>
<s>
Maurové	Maurové	k?	Maurové
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
postavili	postavit	k5eAaPmAgMnP	postavit
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
části	část	k1gFnPc1	část
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
reconquisty	reconquista	k1gMnSc2	reconquista
dobyta	dobýt	k5eAaPmNgFnS	dobýt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1462	[number]	k4	1462
kastilským	kastilský	k2eAgNnSc7d1	Kastilské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
dobyt	dobýt	k5eAaPmNgInS	dobýt
holandsko-britským	holandskoritský	k2eAgInSc7d1	holandsko-britský
expedičním	expediční	k2eAgInSc7d1	expediční
sborem	sbor	k1gInSc7	sbor
vedeným	vedený	k2eAgInSc7d1	vedený
admirálem	admirál	k1gMnSc7	admirál
Georgem	Georg	k1gMnSc7	Georg
Rookem	Rooek	k1gMnSc7	Rooek
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
formalizován	formalizovat	k5eAaBmNgInS	formalizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
Utrechtskou	Utrechtský	k2eAgFnSc7d1	Utrechtská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
několikrát	několikrát	k6eAd1	několikrát
neúspěšně	úspěšně	k6eNd1	úspěšně
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejtvrdší	tvrdý	k2eAgInSc4d3	nejtvrdší
nápor	nápor	k1gInSc4	nápor
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
Velké	velká	k1gFnSc2	velká
obležení	obležení	k1gNnSc2	obležení
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1779	[number]	k4	1779
až	až	k9	až
1783	[number]	k4	1783
během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
veškeré	veškerý	k3xTgNnSc4	veškerý
civilní	civilní	k2eAgNnSc4d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
a	a	k8xC	a
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
námořní	námořní	k2eAgFnSc4d1	námořní
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
Britové	Brit	k1gMnPc1	Brit
ovládali	ovládat	k5eAaImAgMnP	ovládat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zásobovat	zásobovat	k5eAaImF	zásobovat
Maltu	Malta	k1gFnSc4	Malta
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
cílem	cíl	k1gInSc7	cíl
náletu	nálet	k1gInSc2	nálet
83	[number]	k4	83
francouzských	francouzský	k2eAgNnPc2d1	francouzské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
odveta	odveta	k1gFnSc1	odveta
za	za	k7c2	za
britské	britský	k2eAgFnSc2d1	britská
akce	akce	k1gFnSc2	akce
v	v	k7c6	v
Mers-el-Kébiru	Mersl-Kébir	k1gInSc6	Mers-el-Kébir
a	a	k8xC	a
Dakaru	Dakar	k1gInSc6	Dakar
<g/>
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
za	za	k7c2	za
války	válka	k1gFnSc2	válka
pevninské	pevninský	k2eAgInPc4d1	pevninský
přístupy	přístup	k1gInPc4	přístup
k	k	k7c3	k
pevnosti	pevnost	k1gFnSc3	pevnost
<g/>
,	,	kIx,	,
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
ale	ale	k9	ale
umožnit	umožnit	k5eAaPmF	umožnit
průchod	průchod	k1gInSc4	průchod
německým	německý	k2eAgFnPc3d1	německá
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
chtěly	chtít	k5eAaImAgFnP	chtít
pevnost	pevnost	k1gFnSc4	pevnost
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
napjaté	napjatý	k2eAgInPc1d1	napjatý
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Frankovým	Frankův	k2eAgNnSc7d1	Frankovo
Španělskem	Španělsko	k1gNnSc7	Španělsko
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
ohromná	ohromný	k2eAgFnSc1d1	ohromná
plechová	plechový	k2eAgFnSc1d1	plechová
"	"	kIx"	"
<g/>
střecha	střecha	k1gFnSc1	střecha
<g/>
"	"	kIx"	"
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
zachytávání	zachytávání	k1gNnSc3	zachytávání
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
během	během	k7c2	během
španělské	španělský	k2eAgFnSc2d1	španělská
blokády	blokáda	k1gFnSc2	blokáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1	Gibraltarská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
turistice	turistika	k1gFnSc6	turistika
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
finančních	finanční	k2eAgFnPc6d1	finanční
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Gibraltaru	Gibraltar	k1gInSc6	Gibraltar
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
více	hodně	k6eAd2	hodně
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
přes	přes	k7c4	přes
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
rovněž	rovněž	k9	rovněž
vydává	vydávat	k5eAaImIp3nS	vydávat
svoje	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
mince	mince	k1gFnPc4	mince
a	a	k8xC	a
bankovky	bankovka	k1gFnPc4	bankovka
–	–	k?	–
gibraltarskou	gibraltarský	k2eAgFnSc4d1	Gibraltarská
libru	libra	k1gFnSc4	libra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
běžně	běžně	k6eAd1	běžně
platit	platit	k5eAaImF	platit
i	i	k9	i
britskými	britský	k2eAgFnPc7d1	britská
librami	libra	k1gFnPc7	libra
a	a	k8xC	a
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
eurem	euro	k1gNnSc7	euro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrací	vracet	k5eAaImIp3nS	vracet
jejich	jejich	k3xOp3gFnSc4	jejich
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
-	-	kIx~	-
vojenská	vojenský	k2eAgFnSc1d1	vojenská
minulost	minulost	k1gFnSc1	minulost
</s>
</p>
