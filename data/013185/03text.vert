<p>
<s>
Park	park	k1gInSc1	park
Range	Rang	k1gInSc2	Rang
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc4	pohoří
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Colorada	Colorado	k1gNnSc2	Colorado
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
Wyomingu	Wyoming	k1gInSc2	Wyoming
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
amerických	americký	k2eAgFnPc2d1	americká
Skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
hor.	hor.	k?	hor.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
okolo	okolo	k7c2	okolo
170	[number]	k4	170
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
poměrně	poměrně	k6eAd1	poměrně
strmě	strmě	k6eAd1	strmě
z	z	k7c2	z
Koloradské	Koloradský	k2eAgFnSc2d1	Koloradský
plošiny	plošina	k1gFnSc2	plošina
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
Roanské	Roanský	k2eAgFnPc1d1	Roanský
plošiny	plošina	k1gFnPc1	plošina
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
významnou	významný	k2eAgFnSc4d1	významná
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
North	North	k1gMnSc1	North
Platte	Platt	k1gInSc5	Platt
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Medicine	Medicin	k1gInSc5	Medicin
Bow	Bow	k1gMnSc4	Bow
Mountains	Mountains	k1gInSc1	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Park	park	k1gInSc1	park
Range	Rang	k1gInSc2	Rang
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gMnSc1	Mount
Zirkel	Zirkel	k1gMnSc1	Zirkel
(	(	kIx(	(
<g/>
3	[number]	k4	3
712	[number]	k4	712
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Park	park	k1gInSc1	park
Range	Range	k1gNnPc2	Range
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
