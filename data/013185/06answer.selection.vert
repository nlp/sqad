<s>
Pohoří	pohoří	k1gNnSc1	pohoří
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
poměrně	poměrně	k6eAd1	poměrně
strmě	strmě	k6eAd1	strmě
z	z	k7c2	z
Koloradské	Koloradský	k2eAgFnSc2d1	Koloradský
plošiny	plošina	k1gFnSc2	plošina
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
Roanské	Roanský	k2eAgFnPc1d1	Roanský
plošiny	plošina	k1gFnPc1	plošina
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
významnou	významný	k2eAgFnSc4d1	významná
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
