<s>
Klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
samonosná	samonosný	k2eAgFnSc1d1	samonosná
<g/>
,	,	kIx,	,
vzhůru	vzhůru	k6eAd1	vzhůru
vypuklá	vypuklý	k2eAgFnSc1d1	vypuklá
stavební	stavební	k2eAgFnSc1d1	stavební
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c4	na
zastropení	zastropení	k1gNnSc4	zastropení
a	a	k8xC	a
zakrytí	zakrytí	k1gNnSc4	zakrytí
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
oblouk	oblouk	k1gInSc1	oblouk
využívá	využívat	k5eAaPmIp3nS	využívat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
váha	váha	k1gFnSc1	váha
klenby	klenba	k1gFnSc2	klenba
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
její	její	k3xOp3gFnPc4	její
podpory	podpora	k1gFnPc4	podpora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svislém	svislý	k2eAgInSc6d1	svislý
i	i	k8xC	i
vodorovném	vodorovný	k2eAgInSc6d1	vodorovný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
především	především	k6eAd1	především
ploché	plochý	k2eAgInPc1d1	plochý
(	(	kIx(	(
<g/>
neklenuté	klenutý	k2eNgInPc1d1	neklenutý
<g/>
)	)	kIx)	)
trámové	trámový	k2eAgInPc1d1	trámový
stropy	strop	k1gInPc1	strop
<g/>
,	,	kIx,	,
klenba	klenba	k1gFnSc1	klenba
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
tvořila	tvořit	k5eAaImAgFnS	tvořit
podstatně	podstatně	k6eAd1	podstatně
náročnější	náročný	k2eAgFnSc4d2	náročnější
a	a	k8xC	a
nákladnější	nákladný	k2eAgFnSc4d2	nákladnější
alternativu	alternativa	k1gFnSc4	alternativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
zastropit	zastropit	k5eAaPmF	zastropit
větší	veliký	k2eAgFnPc4d2	veliký
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
nehořlavého	hořlavý	k2eNgInSc2d1	nehořlavý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
použití	použití	k1gNnSc2	použití
kleneb	klenba	k1gFnPc2	klenba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
modernější	moderní	k2eAgFnPc1d2	modernější
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
ocelové	ocelový	k2eAgInPc1d1	ocelový
nosníky	nosník	k1gInPc1	nosník
<g/>
,	,	kIx,	,
příhradová	příhradový	k2eAgFnSc1d1	příhradová
konstrukce	konstrukce	k1gFnSc1	konstrukce
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
železobeton	železobeton	k1gInSc4	železobeton
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnSc3	část
pata	pata	k1gFnSc1	pata
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
bod	bod	k1gInSc1	bod
nebo	nebo	k8xC	nebo
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
svislá	svislý	k2eAgFnSc1d1	svislá
konstrukce	konstrukce	k1gFnSc1	konstrukce
podpěry	podpěra	k1gFnSc2	podpěra
(	(	kIx(	(
<g/>
stěna	stěna	k1gFnSc1	stěna
nebo	nebo	k8xC	nebo
sloup	sloup	k1gInSc1	sloup
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
odchylovat	odchylovat	k5eAaImF	odchylovat
od	od	k7c2	od
svislice	svislice	k1gFnSc2	svislice
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
křivky	křivka	k1gFnSc2	křivka
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
protilehlé	protilehlý	k2eAgFnSc3d1	protilehlá
straně	strana	k1gFnSc3	strana
zaklenutého	zaklenutý	k2eAgInSc2d1	zaklenutý
prostoru	prostor	k1gInSc2	prostor
vrchol	vrchol	k1gInSc1	vrchol
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
vzestupný	vzestupný	k2eAgInSc1d1	vzestupný
směr	směr	k1gInSc1	směr
klenební	klenební	k2eAgFnSc2d1	klenební
křivky	křivka	k1gFnSc2	křivka
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
směr	směr	k1gInSc1	směr
sestupný	sestupný	k2eAgInSc1d1	sestupný
–	–	k?	–
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
klenby	klenba	k1gFnSc2	klenba
vrcholnice	vrcholnice	k1gFnSc2	vrcholnice
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
spojující	spojující	k2eAgMnSc1d1	spojující
vrcholové	vrcholový	k2eAgInPc4d1	vrcholový
body	bod	k1gInPc4	bod
myšlených	myšlený	k2eAgInPc2d1	myšlený
oblouků	oblouk	k1gInPc2	oblouk
tvořících	tvořící	k2eAgInPc2d1	tvořící
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
líc	líc	k1gInSc4	líc
klenby	klenba	k1gFnSc2	klenba
křivka	křivka	k1gFnSc1	křivka
klenby	klenba	k1gFnSc2	klenba
v	v	k7c6	v
řezu	řez	k1gInSc6	řez
–	–	k?	–
kruhový	kruhový	k2eAgInSc1d1	kruhový
oblouk	oblouk	k1gInSc1	oblouk
(	(	kIx(	(
<g/>
polokruh	polokruh	k1gInSc1	polokruh
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgFnSc1d1	kruhová
výseč	výseč	k1gFnSc1	výseč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stlačený	stlačený	k2eAgInSc1d1	stlačený
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
lomený	lomený	k2eAgInSc1d1	lomený
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
parabola	parabola	k1gFnSc1	parabola
<g/>
,	,	kIx,	,
řetězovka	řetězovka	k1gFnSc1	řetězovka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
oslí	oslí	k2eAgInSc4d1	oslí
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
líc	líc	k1gFnSc1	líc
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
její	její	k3xOp3gFnSc4	její
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
<g/>
,	,	kIx,	,
pohledová	pohledový	k2eAgFnSc1d1	pohledová
strana	strana	k1gFnSc1	strana
daná	daný	k2eAgFnSc1d1	daná
tvarem	tvar	k1gInSc7	tvar
bednění	bednění	k1gNnSc2	bednění
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
<g/>
)	)	kIx)	)
být	být	k5eAaImF	být
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
odstranění	odstranění	k1gNnSc6	odstranění
povrchově	povrchově	k6eAd1	povrchově
upravená	upravený	k2eAgFnSc1d1	upravená
rub	roubit	k5eAaImRp2nS	roubit
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
horní	horní	k2eAgFnSc1d1	horní
strana	strana	k1gFnSc1	strana
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
podlahou	podlaha	k1gFnSc7	podlaha
dalšího	další	k2eAgNnSc2d1	další
patra	patro	k1gNnSc2	patro
nebo	nebo	k8xC	nebo
střechou	střecha	k1gFnSc7	střecha
<g />
.	.	kIx.	.
</s>
<s>
čelo	čelo	k1gNnSc1	čelo
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
část	část	k1gFnSc1	část
stěny	stěna	k1gFnSc2	stěna
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
obloukem	oblouk	k1gInSc7	oblouk
klenby	klenba	k1gFnPc1	klenba
Rozměry	rozměra	k1gFnSc2	rozměra
rozpětí	rozpětí	k1gNnSc2	rozpětí
nebo	nebo	k8xC	nebo
rozpon	rozpona	k1gFnPc2	rozpona
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
svislými	svislý	k2eAgFnPc7d1	svislá
stavebními	stavební	k2eAgFnPc7d1	stavební
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
(	(	kIx(	(
<g/>
sloupy	sloup	k1gInPc7	sloup
<g/>
/	/	kIx~	/
<g/>
stěny	stěna	k1gFnPc4	stěna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
patami	pata	k1gFnPc7	pata
klenby	klenba	k1gFnPc4	klenba
výška	výška	k1gFnSc1	výška
(	(	kIx(	(
<g/>
světlost	světlost	k1gFnSc1	světlost
<g/>
)	)	kIx)	)
klenby	klenba	k1gFnPc1	klenba
(	(	kIx(	(
<g/>
vzepětí	vzepětí	k1gNnSc1	vzepětí
klenby	klenba	k1gFnSc2	klenba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
–	–	k?	–
výška	výška	k1gFnSc1	výška
mezi	mezi	k7c7	mezi
patami	pata	k1gFnPc7	pata
klenby	klenba	k1gFnSc2	klenba
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
výsledné	výsledný	k2eAgFnSc6d1	výsledná
výšce	výška	k1gFnSc6	výška
zaklenuté	zaklenutý	k2eAgFnSc2d1	zaklenutá
místnosti	místnost	k1gFnSc2	místnost
délka	délka	k1gFnSc1	délka
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
způsob	způsob	k1gInSc1	způsob
zastropení	zastropení	k1gNnSc2	zastropení
představuje	představovat	k5eAaImIp3nS	představovat
trám	trám	k1gInSc1	trám
nebo	nebo	k8xC	nebo
kamenný	kamenný	k2eAgInSc1d1	kamenný
nosník	nosník	k1gInSc1	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
lze	lze	k6eAd1	lze
zvětšit	zvětšit	k5eAaPmF	zvětšit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
kameny	kámen	k1gInPc1	kámen
opřou	opřít	k5eAaPmIp3nP	opřít
šikmo	šikmo	k6eAd1	šikmo
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Přechodem	přechod	k1gInSc7	přechod
ke	k	k7c3	k
klenbě	klenba	k1gFnSc3	klenba
je	být	k5eAaImIp3nS	být
ústupková	ústupkový	k2eAgFnSc1d1	ústupkový
klenba	klenba	k1gFnSc1	klenba
z	z	k7c2	z
vodorovných	vodorovný	k2eAgFnPc2d1	vodorovná
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
stupňovitě	stupňovitě	k6eAd1	stupňovitě
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrcholnici	vrcholnice	k1gFnSc6	vrcholnice
setkají	setkat	k5eAaPmIp3nP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgFnPc1d1	skutečná
klenby	klenba	k1gFnPc1	klenba
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c4	na
lešení	lešení	k1gNnSc4	lešení
nebo	nebo	k8xC	nebo
bednění	bednění	k1gNnSc4	bednění
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
klenby	klenba	k1gFnPc1	klenba
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgFnP	doložit
již	již	k6eAd1	již
ze	z	k7c2	z
sumerské	sumerský	k2eAgFnSc2d1	sumerská
architektury	architektura	k1gFnSc2	architektura
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Ramesse	Ramesse	k1gFnSc2	Ramesse
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Medínit	Medínit	k1gFnSc6	Medínit
Habu	Habus	k1gInSc2	Habus
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Klenby	klenba	k1gFnPc1	klenba
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
jak	jak	k6eAd1	jak
v	v	k7c6	v
monumentálních	monumentální	k2eAgFnPc6d1	monumentální
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
v	v	k7c6	v
inženýrských	inženýrský	k2eAgFnPc6d1	inženýrská
stavbách	stavba	k1gFnPc6	stavba
(	(	kIx(	(
<g/>
stoky	stok	k1gInPc1	stok
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
tunely	tunel	k1gInPc1	tunel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
dokonalosti	dokonalost	k1gFnSc3	dokonalost
dovedla	dovést	k5eAaPmAgFnS	dovést
klenby	klenba	k1gFnPc1	klenba
římská	římský	k2eAgFnSc1d1	římská
a	a	k8xC	a
byzantská	byzantský	k2eAgFnSc1d1	byzantská
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
klenby	klenba	k1gFnSc2	klenba
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgInPc1d1	veliký
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc1d1	vysoký
prostory	prostor	k1gInPc1	prostor
i	i	k8xC	i
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
veliká	veliký	k2eAgNnPc4d1	veliké
okna	okno	k1gNnPc4	okno
v	v	k7c6	v
gotice	gotika	k1gFnSc6	gotika
si	se	k3xPyFc3	se
vynutily	vynutit	k5eAaPmAgFnP	vynutit
stále	stále	k6eAd1	stále
důmyslnější	důmyslný	k2eAgFnPc1d2	důmyslnější
konstrukce	konstrukce	k1gFnPc1	konstrukce
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
forem	forma	k1gFnPc2	forma
i	i	k8xC	i
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
renesance	renesance	k1gFnSc2	renesance
se	se	k3xPyFc4	se
klenby	klenba	k1gFnPc1	klenba
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
užívaly	užívat	k5eAaImAgFnP	užívat
i	i	k9	i
u	u	k7c2	u
světských	světský	k2eAgFnPc2d1	světská
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
sály	sál	k1gInPc1	sál
v	v	k7c6	v
palácích	palác	k1gInPc6	palác
<g/>
,	,	kIx,	,
na	na	k7c6	na
městských	městský	k2eAgFnPc6d1	městská
radnicích	radnice	k1gFnPc6	radnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pomalu	pomalu	k6eAd1	pomalu
převládaly	převládat	k5eAaImAgFnP	převládat
klenby	klenba	k1gFnPc4	klenba
cihlové	cihlový	k2eAgFnPc4d1	cihlová
nad	nad	k7c7	nad
kamennými	kamenný	k2eAgInPc7d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Vícepodlažní	vícepodlažní	k2eAgFnPc1d1	vícepodlažní
budovy	budova	k1gFnPc1	budova
přitom	přitom	k6eAd1	přitom
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
snížení	snížení	k1gNnSc4	snížení
klenby	klenba	k1gFnSc2	klenba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
celého	celý	k2eAgNnSc2d1	celé
podlaží	podlaží	k1gNnSc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ocelové	ocelový	k2eAgInPc1d1	ocelový
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
betonové	betonový	k2eAgInPc4d1	betonový
nosníky	nosník	k1gInPc4	nosník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
klenby	klenba	k1gFnPc1	klenba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
už	už	k6eAd1	už
jen	jen	k9	jen
u	u	k7c2	u
reprezentativních	reprezentativní	k2eAgFnPc2d1	reprezentativní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zejména	zejména	k9	zejména
betonové	betonový	k2eAgFnPc1d1	betonová
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
i	i	k9	i
složitější	složitý	k2eAgInPc1d2	složitější
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
paraboloid	paraboloid	k1gInSc1	paraboloid
<g/>
,	,	kIx,	,
hyperboloid	hyperboloid	k1gInSc1	hyperboloid
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
dokladem	doklad	k1gInSc7	doklad
vyspělého	vyspělý	k2eAgNnSc2d1	vyspělé
stavebního	stavební	k2eAgNnSc2d1	stavební
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
sestrojení	sestrojení	k1gNnSc1	sestrojení
klenby	klenba	k1gFnSc2	klenba
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
schopnosti	schopnost	k1gFnPc4	schopnost
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
opracování	opracování	k1gNnSc2	opracování
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dokladem	doklad	k1gInSc7	doklad
vysoké	vysoký	k2eAgFnSc2d1	vysoká
úrovně	úroveň	k1gFnSc2	úroveň
organizace	organizace	k1gFnSc2	organizace
pracovních	pracovní	k2eAgInPc2d1	pracovní
postupů	postup	k1gInPc2	postup
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
dokladem	doklad	k1gInSc7	doklad
rozvinutého	rozvinutý	k2eAgNnSc2d1	rozvinuté
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgInSc1d1	ideální
tvar	tvar	k1gInSc1	tvar
klenby	klenba	k1gFnSc2	klenba
by	by	kYmCp3nS	by
tvořila	tvořit	k5eAaImAgFnS	tvořit
řetězovka	řetězovka	k1gFnSc1	řetězovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
obtížně	obtížně	k6eAd1	obtížně
konstruuje	konstruovat	k5eAaImIp3nS	konstruovat
a	a	k8xC	a
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
jinými	jiný	k2eAgFnPc7d1	jiná
křivkami	křivka	k1gFnPc7	křivka
(	(	kIx(	(
<g/>
oblouk	oblouk	k1gInSc1	oblouk
kružnice	kružnice	k1gFnSc2	kružnice
nebo	nebo	k8xC	nebo
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
,	,	kIx,	,
lomený	lomený	k2eAgInSc1d1	lomený
oblouk	oblouk	k1gInSc1	oblouk
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Klenba	klenba	k1gFnSc1	klenba
účinně	účinně	k6eAd1	účinně
převádí	převádět	k5eAaImIp3nS	převádět
síly	síla	k1gFnPc4	síla
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vodorovného	vodorovný	k2eAgInSc2d1	vodorovný
nosníku	nosník	k1gInSc2	nosník
by	by	kYmCp3nP	by
působily	působit	k5eAaImAgFnP	působit
svisle	svisle	k1gFnSc1	svisle
<g/>
)	)	kIx)	)
do	do	k7c2	do
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
působí	působit	k5eAaImIp3nS	působit
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
křivky	křivka	k1gFnSc2	křivka
oblouku	oblouk	k1gInSc2	oblouk
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
vnáší	vnášet	k5eAaImIp3nS	vnášet
do	do	k7c2	do
svislých	svislý	k2eAgFnPc2d1	svislá
nosných	nosný	k2eAgFnPc2d1	nosná
konstrukcí	konstrukce	k1gFnPc2	konstrukce
složku	složka	k1gFnSc4	složka
horizontálních	horizontální	k2eAgFnPc2d1	horizontální
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
stavitelé	stavitel	k1gMnPc1	stavitel
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
hmotností	hmotnost	k1gFnPc2	hmotnost
svislých	svislý	k2eAgFnPc2d1	svislá
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
účinky	účinek	k1gInPc1	účinek
bočních	boční	k2eAgFnPc2d1	boční
sil	síla	k1gFnPc2	síla
vydrží	vydržet	k5eAaPmIp3nP	vydržet
<g/>
,	,	kIx,	,
vyrovnáním	vyrovnání	k1gNnSc7	vyrovnání
silových	silový	k2eAgMnPc2d1	silový
účinků	účinek	k1gInPc2	účinek
navazujících	navazující	k2eAgFnPc2d1	navazující
kleneb	klenba	k1gFnPc2	klenba
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
pomocnými	pomocný	k2eAgFnPc7d1	pomocná
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
zvenčí	zvenčí	k6eAd1	zvenčí
i	i	k9	i
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
řešení	řešení	k1gNnSc1	řešení
těchto	tento	k3xDgMnPc2	tento
problémů	problém	k1gInPc2	problém
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
o	o	k7c6	o
úrovni	úroveň	k1gFnSc6	úroveň
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
kleneb	klenba	k1gFnPc2	klenba
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
–	–	k?	–
sklenutá	sklenutý	k2eAgFnSc1d1	sklenutá
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1	rovnoběžná
zdmi	zeď	k1gFnPc7	zeď
Křížová	Křížová	k1gFnSc1	Křížová
klenba	klenba	k1gFnSc1	klenba
–	–	k?	–
sklenutá	sklenutý	k2eAgFnSc1d1	sklenutá
do	do	k7c2	do
rohů	roh	k1gInPc2	roh
čtyřúhelníku	čtyřúhelník	k1gInSc2	čtyřúhelník
Kupole	kupole	k1gFnSc1	kupole
–	–	k?	–
sklenutá	sklenutý	k2eAgFnSc1d1	sklenutá
symetricky	symetricky	k6eAd1	symetricky
podle	podle	k7c2	podle
svislé	svislý	k2eAgFnSc2d1	svislá
středové	středový	k2eAgFnSc2d1	středová
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tvar	tvar	k1gInSc4	tvar
půlkoule	půlkoule	k1gFnSc2	půlkoule
nad	nad	k7c7	nad
kruhovým	kruhový	k2eAgInSc7d1	kruhový
půdorysem	půdorys	k1gInSc7	půdorys
Valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
konstruuje	konstruovat	k5eAaImIp3nS	konstruovat
podle	podle	k7c2	podle
kruhového	kruhový	k2eAgInSc2d1	kruhový
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
část	část	k1gFnSc4	část
válcové	válcový	k2eAgFnSc2d1	válcová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
protilehlých	protilehlý	k2eAgFnPc6d1	protilehlá
stěnách	stěna	k1gFnPc6	stěna
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
vodorovných	vodorovný	k2eAgInPc6d1	vodorovný
nosnících	nosník	k1gInPc6	nosník
<g/>
)	)	kIx)	)
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
nad	nad	k7c7	nad
čtyřúhelnými	čtyřúhelný	k2eAgInPc7d1	čtyřúhelný
prostory	prostor	k1gInPc7	prostor
(	(	kIx(	(
<g/>
klenebními	klenební	k2eAgNnPc7d1	klenební
poli	pole	k1gNnPc7	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stoupající	stoupající	k2eAgFnSc1d1	stoupající
/	/	kIx~	/
klesající	klesající	k2eAgFnSc1d1	klesající
valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vrcholnice	vrcholnice	k1gFnSc1	vrcholnice
má	mít	k5eAaImIp3nS	mít
stoupající	stoupající	k2eAgInSc4d1	stoupající
nebo	nebo	k8xC	nebo
klesající	klesající	k2eAgInSc4d1	klesající
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
chodbách	chodba	k1gFnPc6	chodba
se	s	k7c7	s
šikmým	šikmý	k2eAgInSc7d1	šikmý
sklonem	sklon	k1gInSc7	sklon
podlahy	podlaha	k1gFnSc2	podlaha
nebo	nebo	k8xC	nebo
nad	nad	k7c7	nad
schodišti	schodiště	k1gNnPc7	schodiště
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
u	u	k7c2	u
klenby	klenba	k1gFnSc2	klenba
křížové	křížový	k2eAgFnSc2d1	křížová
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přímky	přímka	k1gFnPc1	přímka
znázorňující	znázorňující	k2eAgFnSc1d1	znázorňující
patu	pata	k1gFnSc4	pata
klenby	klenba	k1gFnSc2	klenba
nejsou	být	k5eNaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
a	a	k8xC	a
klenba	klenba	k1gFnSc1	klenba
kryje	krýt	k5eAaImIp3nS	krýt
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
lichoběžníkovým	lichoběžníkový	k2eAgInSc7d1	lichoběžníkový
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
líc	líc	k1gInSc1	líc
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
výseč	výseč	k1gFnSc4	výseč
komolého	komolý	k2eAgInSc2d1	komolý
kužele	kužel	k1gInSc2	kužel
a	a	k8xC	a
vrcholnice	vrcholnice	k1gFnSc1	vrcholnice
bude	být	k5eAaImBp3nS	být
stoupající	stoupající	k2eAgFnSc1d1	stoupající
nebo	nebo	k8xC	nebo
klesající	klesající	k2eAgFnSc1d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosahovat	dosahovat	k5eAaImF	dosahovat
iluze	iluze	k1gFnPc4	iluze
prodloužení	prodloužení	k1gNnSc2	prodloužení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zkrácení	zkrácení	k1gNnSc4	zkrácení
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgInPc2	takový
postupů	postup	k1gInPc2	postup
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
užívali	užívat	k5eAaImAgMnP	užívat
barokní	barokní	k2eAgMnPc1d1	barokní
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Lunetová	lunetový	k2eAgFnSc1d1	lunetová
valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hlavní	hlavní	k2eAgFnSc7d1	hlavní
podélnou	podélný	k2eAgFnSc7d1	podélná
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
z	z	k7c2	z
boku	bok	k1gInSc2	bok
pronikají	pronikat	k5eAaImIp3nP	pronikat
valené	valený	k2eAgFnPc1d1	valená
klenby	klenba	k1gFnPc1	klenba
o	o	k7c6	o
menším	malý	k2eAgInSc6d2	menší
poloměru	poloměr	k1gInSc6	poloměr
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klenbě	klenba	k1gFnSc6	klenba
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
vykrojeny	vykrojen	k2eAgInPc1d1	vykrojen
trojúhelné	trojúhelný	k2eAgInPc1d1	trojúhelný
zářezy	zářez	k1gInPc1	zářez
(	(	kIx(	(
<g/>
lunety	luneta	k1gFnPc1	luneta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nad	nad	k7c7	nad
okny	okno	k1gNnPc7	okno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
prostor	prostor	k1gInSc1	prostor
osvětlují	osvětlovat	k5eAaImIp3nP	osvětlovat
a	a	k8xC	a
klenbu	klenba	k1gFnSc4	klenba
rytmicky	rytmicky	k6eAd1	rytmicky
člení	členit	k5eAaImIp3nS	členit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
stavitelství	stavitelství	k1gNnSc2	stavitelství
v	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
mohutně	mohutně	k6eAd1	mohutně
pak	pak	k6eAd1	pak
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
lunetová	lunetový	k2eAgFnSc1d1	lunetová
zdrojem	zdroj	k1gInSc7	zdroj
mnoha	mnoho	k4c2	mnoho
inovativních	inovativní	k2eAgInPc2d1	inovativní
postupů	postup	k1gInPc2	postup
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
reprezentačních	reprezentační	k2eAgFnPc2d1	reprezentační
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
pronikem	pronik	k1gInSc7	pronik
dvou	dva	k4xCgFnPc2	dva
navzájem	navzájem	k6eAd1	navzájem
kolmých	kolmý	k2eAgFnPc2d1	kolmá
valených	valený	k2eAgFnPc2d1	valená
kleneb	klenba	k1gFnPc2	klenba
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
podle	podle	k7c2	podle
oblouků	oblouk	k1gInPc2	oblouk
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
poloměrem	poloměr	k1gInSc7	poloměr
<g/>
,	,	kIx,	,
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
prostor	prostor	k1gInSc4	prostor
se	s	k7c7	s
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dvou	dva	k4xCgFnPc2	dva
stěn	stěna	k1gFnPc2	stěna
(	(	kIx(	(
<g/>
přímek	přímka	k1gFnPc2	přímka
<g/>
)	)	kIx)	)
u	u	k7c2	u
valené	valený	k2eAgFnSc2d1	valená
klenby	klenba	k1gFnSc2	klenba
tvoří	tvořit	k5eAaImIp3nS	tvořit
podporu	podpora	k1gFnSc4	podpora
křížové	křížový	k2eAgFnSc2d1	křížová
klenby	klenba	k1gFnSc2	klenba
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc1	čtyři
rohové	rohový	k2eAgInPc1d1	rohový
body	bod	k1gInPc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
uspořádání	uspořádání	k1gNnSc3	uspořádání
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přiřazovat	přiřazovat	k5eAaImF	přiřazovat
k	k	k7c3	k
základnímu	základní	k2eAgNnSc3d1	základní
klenebnímu	klenební	k2eAgNnSc3d1	klenební
poli	pole	k1gNnSc3	pole
další	další	k2eAgFnSc2d1	další
pole	pole	k1gFnSc2	pole
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
čtyřech	čtyři	k4xCgInPc6	čtyři
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
vrcholnice	vrcholnice	k1gFnSc2	vrcholnice
křížících	křížící	k2eAgFnPc2d1	křížící
se	se	k3xPyFc4	se
valených	valený	k2eAgFnPc2d1	valená
kleneb	klenba	k1gFnPc2	klenba
klenby	klenba	k1gFnSc2	klenba
křížové	křížový	k2eAgFnSc2d1	křížová
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
křížovou	křížový	k2eAgFnSc4d1	křížová
klenbu	klenba	k1gFnSc4	klenba
přímou	přímý	k2eAgFnSc4d1	přímá
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vrcholnice	vrcholnice	k1gFnSc1	vrcholnice
křížících	křížící	k2eAgFnPc2d1	křížící
se	se	k3xPyFc4	se
kleneb	klenba	k1gFnPc2	klenba
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
křížení	křížení	k1gNnSc3	křížení
stoupají	stoupat	k5eAaImIp3nP	stoupat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
křížovou	křížový	k2eAgFnSc4d1	křížová
klenbu	klenba	k1gFnSc4	klenba
stoupající	stoupající	k2eAgFnSc4d1	stoupající
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
klenba	klenba	k1gFnSc1	klenba
vznikala	vznikat	k5eAaImAgFnS	vznikat
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
klenby	klenba	k1gFnSc2	klenba
žebrové	žebrový	k2eAgFnSc2d1	žebrová
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
žebra	žebro	k1gNnSc2	žebro
tvar	tvar	k1gInSc4	tvar
kruhového	kruhový	k2eAgInSc2d1	kruhový
oblouku	oblouk	k1gInSc2	oblouk
<g/>
;	;	kIx,	;
křížová	křížový	k2eAgFnSc1d1	křížová
klenba	klenba	k1gFnSc1	klenba
přímá	přímý	k2eAgFnSc1d1	přímá
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
buď	buď	k8xC	buď
zploštělý	zploštělý	k2eAgInSc1d1	zploštělý
tvar	tvar	k1gInSc1	tvar
příčného	příčný	k2eAgNnSc2d1	příčné
žebra	žebro	k1gNnSc2	žebro
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
převýšený	převýšený	k2eAgInSc1d1	převýšený
tvar	tvar	k1gInSc1	tvar
základních	základní	k2eAgInPc2d1	základní
oblouků	oblouk	k1gInPc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
oblouky	oblouk	k1gInPc1	oblouk
lomené	lomený	k2eAgInPc1d1	lomený
Jestliže	jestliže	k8xS	jestliže
vrcholnice	vrcholnice	k1gFnSc1	vrcholnice
kleneb	klenba	k1gFnPc2	klenba
valených	valený	k2eAgFnPc2d1	valená
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
křížení	křížení	k1gNnSc3	křížení
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klenbu	klenba	k1gFnSc4	klenba
křížovou	křížový	k2eAgFnSc4d1	křížová
klesající	klesající	k2eAgFnSc4d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dosti	dosti	k6eAd1	dosti
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
typ	typ	k1gInSc4	typ
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
svébytnost	svébytnost	k1gFnSc4	svébytnost
zaklenutého	zaklenutý	k2eAgInSc2d1	zaklenutý
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Křížové	k2eAgNnSc1d1	Křížové
zaklenutí	zaklenutí	k1gNnSc1	zaklenutí
utváří	utvářet	k5eAaImIp3nS	utvářet
samostatně	samostatně	k6eAd1	samostatně
účinný	účinný	k2eAgInSc4d1	účinný
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spjatých	spjatý	k2eAgFnPc2d1	spjatá
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Rytmicky	rytmicky	k6eAd1	rytmicky
opakovaná	opakovaný	k2eAgNnPc1d1	opakované
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
architektuře	architektura	k1gFnSc6	architektura
stala	stát	k5eAaPmAgFnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Klenba	klenba	k1gFnSc1	klenba
žebrová	žebrový	k2eAgFnSc1d1	žebrová
využívá	využívat	k5eAaImIp3nS	využívat
nosných	nosný	k2eAgNnPc2d1	nosné
žeber	žebro	k1gNnPc2	žebro
k	k	k7c3	k
usměrnění	usměrnění	k1gNnSc3	usměrnění
a	a	k8xC	a
přenosu	přenos	k1gInSc3	přenos
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
klenby	klenba	k1gFnSc2	klenba
do	do	k7c2	do
podpor	podpora	k1gFnPc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
půdorysem	půdorys	k1gInSc7	půdorys
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
postaví	postavit	k5eAaPmIp3nP	postavit
diagonální	diagonální	k2eAgInPc1d1	diagonální
oblouky	oblouk	k1gInPc1	oblouk
čili	čili	k8xC	čili
žebra	žebro	k1gNnPc1	žebro
a	a	k8xC	a
prostory	prostora	k1gFnPc1	prostora
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
(	(	kIx(	(
<g/>
kápě	kápě	k1gFnSc2	kápě
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
lehčím	lehký	k2eAgNnSc7d2	lehčí
zdivem	zdivo	k1gNnSc7	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
řešení	řešení	k1gNnSc1	řešení
klenby	klenba	k1gFnSc2	klenba
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
často	často	k6eAd1	často
bohatě	bohatě	k6eAd1	bohatě
profilovaných	profilovaný	k2eAgNnPc2d1	profilované
kamenných	kamenný	k2eAgNnPc2d1	kamenné
žeber	žebro	k1gNnPc2	žebro
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
kameníci	kameník	k1gMnPc1	kameník
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
podle	podle	k7c2	podle
šablon	šablona	k1gFnPc2	šablona
a	a	k8xC	a
výkresů	výkres	k1gInPc2	výkres
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
pracovat	pracovat	k5eAaImF	pracovat
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
práce	práce	k1gFnSc1	práce
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
dokonale	dokonale	k6eAd1	dokonale
organizována	organizovat	k5eAaBmNgFnS	organizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dojem	dojem	k1gInSc4	dojem
plynulého	plynulý	k2eAgInSc2d1	plynulý
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Rozkreslováním	rozkreslování	k1gNnSc7	rozkreslování
tvarů	tvar	k1gInPc2	tvar
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kamenů	kámen	k1gInPc2	kámen
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tzv.	tzv.	kA	tzv.
kamenořez	kamenořez	k1gInSc1	kamenořez
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
žebrové	žebrový	k2eAgFnSc2d1	žebrová
klenby	klenba	k1gFnSc2	klenba
románská	románský	k2eAgFnSc1d1	románská
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
Toskánsku	Toskánsko	k1gNnSc6	Toskánsko
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
stavitelské	stavitelský	k2eAgNnSc1d1	stavitelské
prostředí	prostředí	k1gNnSc1	prostředí
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
rozmachu	rozmach	k1gInSc6	rozmach
a	a	k8xC	a
tak	tak	k6eAd1	tak
brzo	brzo	k6eAd1	brzo
obohatilo	obohatit	k5eAaPmAgNnS	obohatit
klenbu	klenba	k1gFnSc4	klenba
žebrovou	žebrový	k2eAgFnSc4d1	žebrová
o	o	k7c4	o
lomený	lomený	k2eAgInSc4d1	lomený
oblouk	oblouk	k1gInSc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
značně	značně	k6eAd1	značně
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
i	i	k9	i
zlevňují	zlevňovat	k5eAaImIp3nP	zlevňovat
stavby	stavba	k1gFnPc1	stavba
snížením	snížení	k1gNnSc7	snížení
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
spotřebu	spotřeba	k1gFnSc4	spotřeba
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Zlehčením	zlehčení	k1gNnSc7	zlehčení
kleneb	klenba	k1gFnPc2	klenba
a	a	k8xC	a
optimalizací	optimalizace	k1gFnPc2	optimalizace
převodu	převod	k1gInSc2	převod
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
podpor	podpora	k1gFnPc2	podpora
se	se	k3xPyFc4	se
snižují	snižovat	k5eAaImIp3nP	snižovat
i	i	k9	i
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
svislé	svislý	k2eAgFnPc4d1	svislá
nosné	nosný	k2eAgFnPc4d1	nosná
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stavbu	stavba	k1gFnSc4	stavba
lze	lze	k6eAd1	lze
odhmotňovat	odhmotňovat	k5eAaImF	odhmotňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klenební	klenební	k2eAgNnPc1d1	klenební
žebra	žebro	k1gNnPc1	žebro
zviditelňují	zviditelňovat	k5eAaImIp3nP	zviditelňovat
hlavní	hlavní	k2eAgFnPc1d1	hlavní
linie	linie	k1gFnPc1	linie
sil	síla	k1gFnPc2	síla
působících	působící	k2eAgFnPc2d1	působící
ve	v	k7c6	v
klenbě	klenba	k1gFnSc6	klenba
<g/>
,	,	kIx,	,
dostávají	dostávat	k5eAaImIp3nP	dostávat
stavitelé	stavitel	k1gMnPc1	stavitel
jakýsi	jakýsi	k3yIgInSc4	jakýsi
návod	návod	k1gInSc4	návod
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
formování	formování	k1gNnSc4	formování
a	a	k8xC	a
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
kleneb	klenba	k1gFnPc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
žebrové	žebrový	k2eAgFnSc2d1	žebrová
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zaklenutí	zaklenutí	k1gNnSc1	zaklenutí
prostoru	prostora	k1gFnSc4	prostora
svébytným	svébytný	k2eAgInSc7d1	svébytný
uměleckým	umělecký	k2eAgInSc7d1	umělecký
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gotické	gotický	k2eAgFnSc6d1	gotická
architektuře	architektura	k1gFnSc6	architektura
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
žebrová	žebrový	k2eAgFnSc1d1	žebrová
klenba	klenba	k1gFnSc1	klenba
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
natolik	natolik	k6eAd1	natolik
hodnotný	hodnotný	k2eAgInSc4d1	hodnotný
počin	počin	k1gInSc4	počin
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
často	často	k6eAd1	často
obejde	obejít	k5eAaPmIp3nS	obejít
bez	bez	k7c2	bez
výzdoby	výzdoba	k1gFnSc2	výzdoba
nástěnnými	nástěnný	k2eAgFnPc7d1	nástěnná
malbami	malba	k1gFnPc7	malba
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
bohatství	bohatství	k1gNnSc1	bohatství
typů	typ	k1gInPc2	typ
gotických	gotický	k2eAgFnPc2d1	gotická
kleneb	klenba	k1gFnPc2	klenba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
konstrukční	konstrukční	k2eAgFnSc2d1	konstrukční
funkce	funkce	k1gFnSc2	funkce
žeber	žebro	k1gNnPc2	žebro
čím	co	k3yRnSc7	co
dále	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
funkci	funkce	k1gFnSc4	funkce
dekorativní	dekorativní	k2eAgFnSc4d1	dekorativní
<g/>
.	.	kIx.	.
</s>
<s>
Jmenujme	jmenovat	k5eAaBmRp1nP	jmenovat
alespoň	alespoň	k9	alespoň
typy	typ	k1gInPc4	typ
-	-	kIx~	-
klenby	klenba	k1gFnPc4	klenba
síťové	síťový	k2eAgFnSc2d1	síťová
<g/>
,	,	kIx,	,
visuté	visutý	k2eAgFnSc2d1	visutá
<g/>
,	,	kIx,	,
vějířové	vějířový	k2eAgFnSc2d1	vějířová
a	a	k8xC	a
kroužené	kroužený	k2eAgFnSc2d1	kroužená
<g/>
.	.	kIx.	.
</s>
<s>
Komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
útvary	útvar	k1gInPc1	útvar
žebrových	žebrový	k2eAgFnPc2d1	žebrová
kleneb	klenba	k1gFnPc2	klenba
nakonec	nakonec	k6eAd1	nakonec
zanikají	zanikat	k5eAaImIp3nP	zanikat
ve	v	k7c6	v
sklípkových	sklípkový	k2eAgFnPc6d1	sklípková
či	či	k8xC	či
diamantových	diamantový	k2eAgFnPc6d1	Diamantová
klenbách	klenba	k1gFnPc6	klenba
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
je	být	k5eAaImIp3nS	být
klenba	klenba	k1gFnSc1	klenba
s	s	k7c7	s
kruhovým	kruhový	k2eAgInSc7d1	kruhový
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
kulový	kulový	k2eAgInSc4d1	kulový
vrchlík	vrchlík	k1gInSc4	vrchlík
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
klene	klenout	k5eAaImIp3nS	klenout
nad	nad	k7c7	nad
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
se	se	k3xPyFc4	se
rohy	roh	k1gInPc7	roh
tzv.	tzv.	kA	tzv.
pendentivy	pendentiv	k1gInPc1	pendentiv
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
kulový	kulový	k2eAgInSc4d1	kulový
vrchlík	vrchlík	k1gInSc4	vrchlík
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
"	"	kIx"	"
<g/>
seřízne	seříznout	k5eAaPmIp3nS	seříznout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tzv.	tzv.	kA	tzv.
česká	český	k2eAgFnSc1d1	Česká
klenba	klenba	k1gFnSc1	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Kupole	kupole	k1gFnSc1	kupole
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
už	už	k6eAd1	už
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc3d1	velká
dokonalosti	dokonalost	k1gFnSc3	dokonalost
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
byzantská	byzantský	k2eAgFnSc1d1	byzantská
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Cihelná	cihelný	k2eAgFnSc1d1	cihelná
centrální	centrální	k2eAgFnSc1d1	centrální
kupole	kupole	k1gFnSc1	kupole
chrámu	chrám	k1gInSc2	chrám
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
má	mít	k5eAaImIp3nS	mít
rozpětí	rozpětí	k1gNnSc1	rozpětí
32	[number]	k4	32
m.	m.	k?	m.
Starší	starý	k2eAgFnSc1d2	starší
kupole	kupole	k1gFnSc1	kupole
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stavěly	stavět	k5eAaImAgInP	stavět
na	na	k7c4	na
válcovitý	válcovitý	k2eAgInSc4d1	válcovitý
podstavec	podstavec	k1gInSc4	podstavec
(	(	kIx(	(
<g/>
tambur	tambur	k1gInSc1	tambur
<g/>
)	)	kIx)	)
s	s	k7c7	s
okny	okno	k1gNnPc7	okno
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc2d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc2d1	barokní
kupole	kupole	k1gFnSc2	kupole
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
lucernu	lucerna	k1gFnSc4	lucerna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prostor	prostor	k1gInSc4	prostor
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
<g/>
.	.	kIx.	.
</s>
<s>
Plochá	plochý	k2eAgFnSc1d1	plochá
kupole	kupole	k1gFnSc1	kupole
haly	hala	k1gFnSc2	hala
Z	z	k7c2	z
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
přes	přes	k7c4	přes
80	[number]	k4	80
m.	m.	k?	m.
Velmi	velmi	k6eAd1	velmi
plochá	plochý	k2eAgFnSc1d1	plochá
kupole	kupole	k1gFnSc1	kupole
<g/>
,	,	kIx,	,
častá	častý	k2eAgFnSc1d1	častá
v	v	k7c6	v
barokním	barokní	k2eAgNnSc6d1	barokní
stavitelství	stavitelství	k1gNnSc6	stavitelství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
česká	český	k2eAgFnSc1d1	Česká
placka	placka	k1gFnSc1	placka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
klenby	klenba	k1gFnPc4	klenba
větších	veliký	k2eAgInPc2d2	veliký
rozponů	rozpon	k1gInPc2	rozpon
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používal	používat	k5eAaImAgInS	používat
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Kámen	kámen	k1gInSc1	kámen
jako	jako	k8xS	jako
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
prvek	prvek	k1gInSc1	prvek
kleneb	klenba	k1gFnPc2	klenba
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
starší	starý	k2eAgFnPc4d2	starší
klenby	klenba	k1gFnPc4	klenba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
cihel	cihla	k1gFnPc2	cihla
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Kámen	kámen	k1gInSc1	kámen
s	s	k7c7	s
cihlou	cihla	k1gFnSc7	cihla
lze	lze	k6eAd1	lze
však	však	k9	však
i	i	k9	i
kombinovat	kombinovat	k5eAaImF	kombinovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
žebra	žebro	k1gNnPc1	žebro
klenby	klenba	k1gFnSc2	klenba
jsou	být	k5eAaImIp3nP	být
kamenná	kamenný	k2eAgFnSc1d1	kamenná
a	a	k8xC	a
výplň	výplň	k1gFnSc1	výplň
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
klenutá	klenutý	k2eAgFnSc1d1	klenutá
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Užívala	užívat	k5eAaImAgFnS	užívat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyl	být	k5eNaImAgInS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
lomový	lomový	k2eAgInSc1d1	lomový
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konstrukce	konstrukce	k1gFnPc4	konstrukce
cihelných	cihelný	k2eAgFnPc2d1	cihelná
kleneb	klenba	k1gFnPc2	klenba
byly	být	k5eAaImAgFnP	být
využívány	využíván	k2eAgFnPc1d1	využívána
tvarovky	tvarovka	k1gFnPc1	tvarovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zlepšovaly	zlepšovat	k5eAaImAgFnP	zlepšovat
a	a	k8xC	a
zjednodušovaly	zjednodušovat	k5eAaImAgFnP	zjednodušovat
zdění	zdění	k1gNnSc4	zdění
obloukových	obloukový	k2eAgInPc2d1	obloukový
tvarů	tvar	k1gInPc2	tvar
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velké	velký	k2eAgFnPc1d1	velká
obdélníkové	obdélníkový	k2eAgFnPc1d1	obdélníková
strany	strana	k1gFnPc1	strana
cihly	cihla	k1gFnSc2	cihla
sbíhaly	sbíhat	k5eAaImAgFnP	sbíhat
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
odpovídajícím	odpovídající	k2eAgInSc6d1	odpovídající
úseku	úsek	k1gInSc6	úsek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
klenbě	klenba	k1gFnSc6	klenba
jednotlivá	jednotlivý	k2eAgFnSc1d1	jednotlivá
cihla	cihla	k1gFnSc1	cihla
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
památkově	památkově	k6eAd1	památkově
chráněná	chráněný	k2eAgFnSc1d1	chráněná
klenba	klenba	k1gFnSc1	klenba
začne	začít	k5eAaPmIp3nS	začít
sedat	sedat	k5eAaImF	sedat
<g/>
,	,	kIx,	,
opravuje	opravovat	k5eAaImIp3nS	opravovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
cihlových	cihlový	k2eAgFnPc2d1	cihlová
spár	spára	k1gFnPc2	spára
zespodu	zespodu	k6eAd1	zespodu
zatloukají	zatloukat	k5eAaImIp3nP	zatloukat
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
klíny	klín	k1gInPc4	klín
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
klenba	klenba	k1gFnSc1	klenba
"	"	kIx"	"
<g/>
utáhne	utáhnout	k5eAaPmIp3nS	utáhnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
vyschlé	vyschlý	k2eAgNnSc1d1	vyschlé
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
<g/>
,	,	kIx,	,
ideální	ideální	k2eAgNnSc1d1	ideální
je	být	k5eAaImIp3nS	být
jasanové	jasanový	k2eAgNnSc1d1	jasanové
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
téměř	téměř	k6eAd1	téměř
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
s	s	k7c7	s
vápnem	vápno	k1gNnSc7	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
dubové	dubový	k2eAgNnSc1d1	dubové
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
také	také	k9	také
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
-	-	kIx~	-
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vlhkosti	vlhkost	k1gFnSc2	vlhkost
v	v	k7c6	v
klenutí	klenutí	k1gNnSc6	klenutí
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
tříslo	tříslo	k1gNnSc1	tříslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pak	pak	k6eAd1	pak
s	s	k7c7	s
vápnem	vápno	k1gNnSc7	vápno
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
na	na	k7c6	na
omítce	omítka	k1gFnSc6	omítka
těžko	těžko	k6eAd1	těžko
odstranitelné	odstranitelný	k2eAgFnPc1d1	odstranitelná
hnědé	hnědý	k2eAgFnPc1d1	hnědá
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Staří	starý	k2eAgMnPc1d1	starý
praktici	praktik	k1gMnPc1	praktik
k	k	k7c3	k
vyzdívce	vyzdívka	k1gFnSc3	vyzdívka
kleneb	klenba	k1gFnPc2	klenba
poznamenávají	poznamenávat	k5eAaImIp3nP	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
cementová	cementový	k2eAgFnSc1d1	cementová
malta	malta	k1gFnSc1	malta
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
než	než	k8xS	než
cihly	cihla	k1gFnPc1	cihla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lehké	lehký	k2eAgNnSc1d1	lehké
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
malta	malta	k1gFnSc1	malta
však	však	k9	však
tvrdla	tvrdnout	k5eAaImAgFnS	tvrdnout
i	i	k9	i
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
bednění	bednění	k1gNnSc1	bednění
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
měkká	měkký	k2eAgFnSc1d1	měkká
vápenná	vápenný	k2eAgFnSc1d1	vápenná
malta	malta	k1gFnSc1	malta
se	se	k3xPyFc4	se
stlačila	stlačit	k5eAaPmAgFnS	stlačit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
zřícení	zřícení	k1gNnSc3	zřícení
chrámových	chrámový	k2eAgFnPc2d1	chrámová
kleneb	klenba	k1gFnPc2	klenba
a	a	k8xC	a
kopulí	kopule	k1gFnSc7	kopule
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
ocelových	ocelový	k2eAgInPc2d1	ocelový
nosníků	nosník	k1gInPc2	nosník
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
tzv.	tzv.	kA	tzv.
Hönelovy	Hönelův	k2eAgFnSc2d1	Hönelův
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
ploché	plochý	k2eAgFnPc1d1	plochá
valené	valený	k2eAgFnPc1d1	valená
klenby	klenba	k1gFnPc1	klenba
<g/>
,	,	kIx,	,
vyzdívané	vyzdívaný	k2eAgFnPc1d1	vyzdívaná
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
paralelní	paralelní	k2eAgInPc4d1	paralelní
ocelové	ocelový	k2eAgInPc4d1	ocelový
nosníky	nosník	k1gInPc4	nosník
(	(	kIx(	(
<g/>
traverzy	traverza	k1gFnPc4	traverza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
m.	m.	k?	m.
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
stavbách	stavba	k1gFnPc6	stavba
aj.	aj.	kA	aj.
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaBmIp3nS	stavit
se	se	k3xPyFc4	se
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
desek	deska	k1gFnPc2	deska
nebo	nebo	k8xC	nebo
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
špalíků	špalík	k1gInPc2	špalík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
klenbami	klenba	k1gFnPc7	klenba
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Špalíky	špalík	k1gInPc1	špalík
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
klenby	klenba	k1gFnPc4	klenba
užívaly	užívat	k5eAaImAgFnP	užívat
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
zespod	zespod	k6eAd1	zespod
pobitá	pobitý	k2eAgNnPc4d1	pobité
rákosem	rákos	k1gInSc7	rákos
a	a	k8xC	a
omítnutá	omítnutý	k2eAgNnPc4d1	omítnuté
<g/>
,	,	kIx,	,
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
stavbu	stavba	k1gFnSc4	stavba
náročného	náročný	k2eAgNnSc2d1	náročné
lešení	lešení	k1gNnSc2	lešení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
barokní	barokní	k2eAgFnSc7d1	barokní
užívala	užívat	k5eAaImAgFnS	užívat
jako	jako	k9	jako
levnější	levný	k2eAgFnSc1d2	levnější
náhrada	náhrada	k1gFnSc1	náhrada
náročných	náročný	k2eAgFnPc2d1	náročná
kleneb	klenba	k1gFnPc2	klenba
cihlových	cihlový	k2eAgFnPc2d1	cihlová
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
chudších	chudý	k2eAgInPc6d2	chudší
venkovských	venkovský	k2eAgInPc6d1	venkovský
kostelech	kostel	k1gInPc6	kostel
i	i	k9	i
u	u	k7c2	u
reprezentativnějších	reprezentativní	k2eAgNnPc2d2	reprezentativnější
venkovských	venkovský	k2eAgNnPc2d1	venkovské
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
betonu	beton	k1gInSc2	beton
u	u	k7c2	u
litých	litý	k2eAgFnPc2d1	litá
kleneb	klenba	k1gFnPc2	klenba
je	být	k5eAaImIp3nS	být
emplekton	emplekton	k1gInSc1	emplekton
<g/>
,	,	kIx,	,
cementová	cementový	k2eAgFnSc1d1	cementová
malta	malta	k1gFnSc1	malta
vylehčená	vylehčený	k2eAgFnSc1d1	vylehčená
zalitými	zalitý	k2eAgFnPc7d1	zalitá
keramickými	keramický	k2eAgFnPc7d1	keramická
nádobami	nádoba	k1gFnPc7	nádoba
nebo	nebo	k8xC	nebo
tufem	tuf	k1gInSc7	tuf
<g/>
.	.	kIx.	.
</s>
<s>
Litá	litý	k2eAgFnSc1d1	litá
kopule	kopule	k1gFnSc1	kopule
římského	římský	k2eAgInSc2d1	římský
Pantheonu	Pantheon	k1gInSc2	Pantheon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
125	[number]	k4	125
o	o	k7c6	o
rozpětí	rozpětí	k1gNnSc6	rozpětí
32	[number]	k4	32
m	m	kA	m
je	být	k5eAaImIp3nS	být
zevnitř	zevnitř	k6eAd1	zevnitř
vylehčena	vylehčen	k2eAgFnSc1d1	vylehčena
kasetovými	kasetový	k2eAgFnPc7d1	kasetový
prohlubněmi	prohlubeň	k1gFnPc7	prohlubeň
a	a	k8xC	a
shora	shora	k6eAd1	shora
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
velkým	velký	k2eAgInSc7d1	velký
kruhovým	kruhový	k2eAgInSc7d1	kruhový
otvorem	otvor	k1gInSc7	otvor
(	(	kIx(	(
<g/>
oculus	oculus	k1gInSc1	oculus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stavět	stavět	k5eAaImF	stavět
skořepinové	skořepinový	k2eAgFnPc1d1	skořepinová
klenby	klenba	k1gFnPc1	klenba
z	z	k7c2	z
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
náročné	náročný	k2eAgNnSc4d1	náročné
bednění	bednění	k1gNnSc4	bednění
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
veliká	veliký	k2eAgNnPc1d1	veliké
rozpětí	rozpětí	k1gNnPc1	rozpětí
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
libovolné	libovolný	k2eAgInPc4d1	libovolný
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
je	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
konstrukce	konstrukce	k1gFnSc1	konstrukce
Pavilonu	pavilon	k1gInSc2	pavilon
A	a	k9	a
brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Valená	valený	k2eAgFnSc1d1	valená
klenba	klenba	k1gFnSc1	klenba
–	–	k?	–
klenba	klenba	k1gFnSc1	klenba
na	na	k7c6	na
obdélném	obdélný	k2eAgInSc6d1	obdélný
půdoryse	půdorys	k1gInSc6	půdorys
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
zaklenutí	zaklenutí	k1gNnSc1	zaklenutí
chodby	chodba	k1gFnSc2	chodba
<g/>
,	,	kIx,	,
oporou	opora	k1gFnSc7	opora
jsou	být	k5eAaImIp3nP	být
podélné	podélný	k2eAgFnPc1d1	podélná
zdi	zeď	k1gFnPc1	zeď
Křížová	Křížová	k1gFnSc1	Křížová
klenba	klenba	k1gFnSc1	klenba
–	–	k?	–
pronik	pronik	k1gInSc1	pronik
dvou	dva	k4xCgFnPc2	dva
kleneb	klenba	k1gFnPc2	klenba
valených	valený	k2eAgFnPc2d1	valená
nad	nad	k7c7	nad
klenebními	klenební	k2eAgInPc7d1	klenební
pasy	pas	k1gInPc7	pas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
tíhu	tíha	k1gFnSc4	tíha
klenby	klenba	k1gFnSc2	klenba
do	do	k7c2	do
rohových	rohový	k2eAgFnPc2d1	rohová
podpor	podpora	k1gFnPc2	podpora
(	(	kIx(	(
<g/>
sloupů	sloup	k1gInPc2	sloup
nebo	nebo	k8xC	nebo
pilířů	pilíř	k1gInPc2	pilíř
<g/>
)	)	kIx)	)
Klášterní	klášterní	k2eAgFnSc1d1	klášterní
klenba	klenba	k1gFnSc1	klenba
–	–	k?	–
průnik	průnik	k1gInSc1	průnik
plochých	plochý	k2eAgFnPc2d1	plochá
valených	valený	k2eAgFnPc2d1	valená
kleneb	klenba	k1gFnPc2	klenba
<g/>
,	,	kIx,	,
oporou	opora	k1gFnSc7	opora
jsou	být	k5eAaImIp3nP	být
stěny	stěna	k1gFnPc4	stěna
nebo	nebo	k8xC	nebo
nosníky	nosník	k1gInPc4	nosník
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
půdorysu	půdorys	k1gInSc2	půdorys
Česká	český	k2eAgFnSc1d1	Česká
placka	placka	k1gFnSc1	placka
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
plochého	plochý	k2eAgInSc2d1	plochý
kulového	kulový	k2eAgInSc2d1	kulový
vrchlíku	vrchlík	k1gInSc2	vrchlík
nad	nad	k7c7	nad
poměrně	poměrně	k6eAd1	poměrně
libovolným	libovolný	k2eAgInSc7d1	libovolný
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
;	;	kIx,	;
běžná	běžný	k2eAgFnSc1d1	běžná
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
a	a	k8xC	a
rokokové	rokokový	k2eAgFnSc6d1	rokoková
době	doba	k1gFnSc6	doba
i	i	k9	i
u	u	k7c2	u
venkovských	venkovský	k2eAgNnPc2d1	venkovské
stavení	stavení	k1gNnPc2	stavení
</s>
