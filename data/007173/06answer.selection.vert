<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
–	–	k?	–
výška	výška	k1gFnSc1	výška
mezi	mezi	k7c7	mezi
patami	pata	k1gFnPc7	pata
klenby	klenba	k1gFnSc2	klenba
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
výsledné	výsledný	k2eAgFnSc6d1	výsledná
výšce	výška	k1gFnSc6	výška
zaklenuté	zaklenutý	k2eAgFnSc2d1	zaklenutá
místnosti	místnost	k1gFnSc2	místnost
délka	délka	k1gFnSc1	délka
klenby	klenba	k1gFnSc2	klenba
–	–	k?	–
měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
její	její	k3xOp3gFnSc2	její
osy	osa	k1gFnSc2	osa
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
způsob	způsob	k1gInSc1	způsob
zastropení	zastropení	k1gNnSc2	zastropení
představuje	představovat	k5eAaImIp3nS	představovat
trám	trám	k1gInSc1	trám
nebo	nebo	k8xC	nebo
kamenný	kamenný	k2eAgInSc1d1	kamenný
nosník	nosník	k1gInSc1	nosník
<g/>
.	.	kIx.	.
</s>
