<s>
Tufahija	Tufahija	k6eAd1
</s>
<s>
Tufahija	Tufahij	k2eAgFnSc1d1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Místo	místo	k7c2
původu	původ	k1gInSc2
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tufahija	Tufahija	k1gFnSc1
se	s	k7c7
šlehačkou	šlehačka	k1gFnSc7
podávaná	podávaný	k2eAgFnSc1d1
v	v	k7c6
cukrárně	cukrárna	k1gFnSc6
v	v	k7c6
Mostaru	Mostar	k1gInSc6
</s>
<s>
Tufahija	Tufahija	k6eAd1
<g/>
,	,	kIx,
česky	česky	k6eAd1
i	i	k9
tufahije	tufahít	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bosenský	bosenský	k2eAgInSc1d1
dezert	dezert	k1gInSc1
vyrobený	vyrobený	k2eAgInSc1d1
z	z	k7c2
jablka	jablko	k1gNnSc2
plněného	plněný	k2eAgInSc2d1
ořechovou	ořechový	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
vařeného	vařený	k2eAgNnSc2d1
ve	v	k7c6
vodě	voda	k1gFnSc6
s	s	k7c7
cukrem	cukr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tufahija	Tufahij	k1gInSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
populární	populární	k2eAgMnSc1d1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
a	a	k8xC
Srbsku	Srbsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tufahija	Tufahija	k1gFnSc1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
Balkán	Balkán	k1gInSc4
během	během	k7c2
osmanské	osmanský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
,	,	kIx,
zákusek	zákusek	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Persie	Persie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Slovo	slovo	k1gNnSc1
tufahija	tufahija	k6eAd1
má	mít	k5eAaImIp3nS
původ	původ	k1gInSc4
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc1
tuffà	tuffà	k1gFnPc2
(	(	kIx(
<g/>
ت	ت	k?
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
jablko	jablko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Jablka	jablko	k1gNnPc1
se	se	k3xPyFc4
oloupou	oloupat	k5eAaPmIp3nP
<g/>
,	,	kIx,
jadřince	jadřinka	k1gFnSc3
odstraní	odstranit	k5eAaPmIp3nS
a	a	k8xC
vaří	vařit	k5eAaImIp3nS
ve	v	k7c6
sladké	sladký	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
s	s	k7c7
citronem	citron	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našlehá	našlehat	k5eAaPmIp3nS
se	se	k3xPyFc4
máslo	máslo	k1gNnSc1
<g/>
,	,	kIx,
přidají	přidat	k5eAaPmIp3nP
se	se	k3xPyFc4
mleté	mletý	k2eAgFnPc1d1
mandle	mandle	k1gFnPc1
nebo	nebo	k8xC
vlašské	vlašský	k2eAgInPc1d1
ořechy	ořech	k1gInPc1
<g/>
,	,	kIx,
trochu	trochu	k6eAd1
mleté	mletý	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
a	a	k8xC
míchaný	míchaný	k2eAgInSc4d1
vaječný	vaječný	k2eAgInSc4d1
bílek	bílek	k1gInSc4
a	a	k8xC
trochu	trocha	k1gFnSc4
šlehačky	šlehačka	k1gFnSc2
(	(	kIx(
<g/>
sladké	sladký	k2eAgFnSc2d1
smetany	smetana	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
ze	z	k7c2
všeho	všecek	k3xTgNnSc2
se	se	k3xPyFc4
udělá	udělat	k5eAaPmIp3nS
jednolitá	jednolitý	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jablka	jablko	k1gNnPc1
se	se	k3xPyFc4
naplní	naplnit	k5eAaPmIp3nP
náplní	náplň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přelije	přelít	k5eAaPmIp3nS
se	se	k3xPyFc4
cukrovým	cukrový	k2eAgInSc7d1
sirupem	sirup	k1gInSc7
(	(	kIx(
<g/>
sorbetem	sorbet	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
ozdobí	ozdobit	k5eAaPmIp3nP
smetanovou	smetanový	k2eAgFnSc7d1
pěnou	pěna	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
položí	položit	k5eAaPmIp3nS
višeň	višeň	k1gFnSc1
nebo	nebo	k8xC
třešeň	třešeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tufahija	Tufahij	k1gInSc2
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
chladná	chladný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Servírování	servírování	k1gNnSc1
</s>
<s>
Tufahija	Tufahija	k6eAd1
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
např.	např.	kA
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
sklenici	sklenice	k1gFnSc6
přelitá	přelitý	k2eAgFnSc1d1
sirupem	sirup	k1gInSc7
a	a	k8xC
nahoře	nahoře	k6eAd1
ozdobená	ozdobený	k2eAgFnSc1d1
šlehačkou	šlehačka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Tufahije	Tufahije	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Tufahija	Tufahija	k1gFnSc1
na	na	k7c6
bosenské	bosenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tufahija	Tufahij	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Balkán	Balkán	k1gInSc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnSc1
</s>
