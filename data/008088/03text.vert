<s>
Milion	milion	k4xCgInSc1	milion
<g/>
,	,	kIx,	,
též	též	k9	též
milión	milión	k4xCgInSc4	milión
(	(	kIx(	(
<g/>
1	[number]	k4	1
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
následující	následující	k2eAgFnSc4d1	následující
999	[number]	k4	999
999	[number]	k4	999
a	a	k8xC	a
předcházející	předcházející	k2eAgFnSc1d1	předcházející
1	[number]	k4	1
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
tisíci	tisíc	k4xCgInPc7	tisíc
tisíců	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
zápisu	zápis	k1gInSc6	zápis
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
106	[number]	k4	106
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metrickém	metrický	k2eAgInSc6d1	metrický
systému	systém	k1gInSc6	systém
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
násobky	násobek	k1gInPc4	násobek
milionu	milion	k4xCgInSc2	milion
řecká	řecký	k2eAgFnSc1d1	řecká
předpona	předpona	k1gFnSc1	předpona
"	"	kIx"	"
<g/>
mega-	mega-	k?	mega-
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
μ	μ	k?	μ
<g/>
,	,	kIx,	,
mégas	mégas	k1gInSc1	mégas
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
miliontiny	miliontina	k1gFnPc4	miliontina
předpona	předpona	k1gFnSc1	předpona
"	"	kIx"	"
<g/>
mikro-	mikro-	k?	mikro-
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
μ	μ	k?	μ
<g/>
,	,	kIx,	,
mikrós	mikrós	k6eAd1	mikrós
–	–	k?	–
malý	malý	k1gMnSc1	malý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
milion	milion	k4xCgInSc1	milion
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
mille	mille	k1gFnSc2	mille
(	(	kIx(	(
<g/>
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
)	)	kIx)	)
a	a	k8xC	a
přípona	přípona	k1gFnSc1	přípona
-one	ne	k1gFnSc1	-one
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
milion	milion	k4xCgInSc1	milion
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
výrazům	výraz	k1gInPc3	výraz
procento	procento	k1gNnSc4	procento
a	a	k8xC	a
promile	promile	k1gNnSc4	promile
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
million	million	k1gInSc1	million
(	(	kIx(	(
<g/>
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
miliontiny	miliontina	k1gFnSc2	miliontina
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
milion	milion	k4xCgInSc1	milion
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnPc4	označení
zvlášť	zvlášť	k6eAd1	zvlášť
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
nebo	nebo	k8xC	nebo
částky	částka	k1gFnSc2	částka
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
milionář	milionář	k1gMnSc1	milionář
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
bohatého	bohatý	k2eAgMnSc4d1	bohatý
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
přesnou	přesný	k2eAgFnSc4d1	přesná
výši	výše	k1gFnSc4	výše
jmění	jmění	k1gNnSc2	jmění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
za	za	k7c4	za
milion	milion	k4xCgInSc4	milion
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Částka	částka	k1gFnSc1	částka
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
řeči	řeč	k1gFnSc6	řeč
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
meloun	meloun	k1gInSc1	meloun
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jedno	jeden	k4xCgNnSc1	jeden
mega	mega	k1gNnSc1	mega
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Milion	milion	k4xCgInSc1	milion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
milion	milion	k4xCgInSc4	milion
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
