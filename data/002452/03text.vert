<s>
Zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Le	Le	k1gFnSc1	Le
Mur	mur	k1gInSc1	mur
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
souboru	soubor	k1gInSc6	soubor
pěti	pět	k4xCc2	pět
novel	novela	k1gFnPc2	novela
Jeana	Jean	k1gMnSc2	Jean
Paula	Paul	k1gMnSc2	Paul
Sartrea	Sartreus	k1gMnSc2	Sartreus
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
koncipovány	koncipovat	k5eAaBmNgInP	koncipovat
jako	jako	k9	jako
existencialistické	existencialistický	k2eAgFnPc1d1	existencialistická
fikce	fikce	k1gFnPc1	fikce
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
dílo	dílo	k1gNnSc4	dílo
dedikoval	dedikovat	k5eAaBmAgMnS	dedikovat
své	svůj	k3xOyFgFnSc3	svůj
dlouholeté	dlouholetý	k2eAgFnSc3d1	dlouholetá
přítelkyni	přítelkyně	k1gFnSc3	přítelkyně
Olze	Olga	k1gFnSc3	Olga
Kosakiewiczové	Kosakiewiczová	k1gFnSc3	Kosakiewiczová
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc3d1	bývalá
studentce	studentka	k1gFnSc3	studentka
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirové	Beauvoirový	k2eAgFnSc6d1	Beauvoirový
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
pěti	pět	k4xCc2	pět
příběhů	příběh	k1gInPc2	příběh
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
novel	novela	k1gFnPc2	novela
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1939	[number]	k4	1939
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
souborném	souborný	k2eAgNnSc6d1	souborné
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
Zeď	zeď	k1gFnSc1	zeď
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
povídky	povídka	k1gFnPc1	povídka
uvolňovány	uvolňovat	k5eAaImNgFnP	uvolňovat
pouze	pouze	k6eAd1	pouze
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
povídka	povídka	k1gFnSc1	povídka
knihy	kniha	k1gFnSc2	kniha
napsané	napsaný	k2eAgFnSc2d1	napsaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
období	období	k1gNnSc2	období
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
zatčených	zatčený	k2eAgFnPc2d1	zatčená
levicových	levicový	k2eAgFnPc2d1	levicová
republikánech	republikán	k1gMnPc6	republikán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
falangisty	falangista	k1gMnSc2	falangista
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
zastřelením	zastřelení	k1gNnPc3	zastřelení
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudek	rozsudek	k1gInSc1	rozsudek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vykonán	vykonán	k2eAgMnSc1d1	vykonán
následující	následující	k2eAgNnSc4d1	následující
ráno	ráno	k1gNnSc4	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mladého	mladý	k2eAgMnSc4d1	mladý
Juana	Juan	k1gMnSc4	Juan
Mirbala	Mirbal	k1gMnSc4	Mirbal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
žádných	žádný	k3yNgFnPc2	žádný
akcí	akce	k1gFnPc2	akce
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
anarchistiského	anarchistiský	k1gMnSc2	anarchistiský
bratra	bratr	k1gMnSc2	bratr
Josého	Josý	k1gMnSc2	Josý
<g/>
.	.	kIx.	.
</s>
<s>
Nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
odsouzencem	odsouzenec	k1gMnSc7	odsouzenec
je	být	k5eAaImIp3nS	být
Tom	Tom	k1gMnSc1	Tom
Steinbock	Steinbock	k1gMnSc1	Steinbock
sloužící	sloužící	k2eAgMnSc1d1	sloužící
v	v	k7c6	v
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
brigádě	brigáda	k1gFnSc6	brigáda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
noc	noc	k1gFnSc4	noc
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
popisuje	popisovat	k5eAaImIp3nS	popisovat
své	svůj	k3xOyFgInPc4	svůj
činy	čin	k1gInPc4	čin
a	a	k8xC	a
vraždy	vražda	k1gFnPc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
děj	děj	k1gInSc4	děj
je	být	k5eAaImIp3nS	být
Pablo	Pablo	k1gNnSc1	Pablo
Ibbieta	Ibbieto	k1gNnSc2	Ibbieto
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
v	v	k7c6	v
suterénní	suterénní	k2eAgFnSc6d1	suterénní
místnosti	místnost	k1gFnSc6	místnost
přestavěné	přestavěný	k2eAgFnSc2d1	přestavěná
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekají	čekat	k5eAaImIp3nP	čekat
tři	tři	k4xCgMnPc1	tři
republikáni	republikán	k1gMnPc1	republikán
na	na	k7c4	na
popravu	poprava	k1gFnSc4	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
nastíněny	nastíněn	k2eAgInPc4d1	nastíněn
psychické	psychický	k2eAgInPc4d1	psychický
a	a	k8xC	a
fyzické	fyzický	k2eAgInPc4d1	fyzický
stavy	stav	k1gInPc4	stav
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
organizmu	organizmus	k1gInSc2	organizmus
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
s	s	k7c7	s
nevyhnutelností	nevyhnutelnost	k1gFnSc7	nevyhnutelnost
blízké	blízký	k2eAgFnSc2d1	blízká
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
je	být	k5eAaImIp3nS	být
přizván	přizvat	k5eAaPmNgMnS	přizvat
belgický	belgický	k2eAgMnSc1d1	belgický
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odsouzené	odsouzená	k1gFnSc3	odsouzená
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
jeho	jeho	k3xOp3gInSc4	jeho
skutečný	skutečný	k2eAgInSc4d1	skutečný
důvod	důvod	k1gInSc4	důvod
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ránem	ráno	k1gNnSc7	ráno
jsou	být	k5eAaImIp3nP	být
Juan	Juan	k1gMnSc1	Juan
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
odvedeni	odveden	k2eAgMnPc1d1	odveden
na	na	k7c4	na
popraviště	popraviště	k1gNnPc4	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Pablo	Pablo	k1gNnSc1	Pablo
překvapivě	překvapivě	k6eAd1	překvapivě
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
místnosti	místnost	k1gFnSc6	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
úkrytu	úkryt	k1gInSc6	úkryt
vůdce	vůdce	k1gMnSc2	vůdce
skupiny	skupina	k1gFnSc2	skupina
Ramóna	Ramóna	k1gFnSc1	Ramóna
Grise	Grise	k1gFnSc1	Grise
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůdce	vůdce	k1gMnSc1	vůdce
je	být	k5eAaImIp3nS	být
ukrytý	ukrytý	k2eAgInSc4d1	ukrytý
u	u	k7c2	u
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
udělat	udělat	k5eAaPmF	udělat
si	se	k3xPyFc3	se
z	z	k7c2	z
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
srandu	sranda	k1gFnSc4	sranda
a	a	k8xC	a
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
si	se	k3xPyFc3	se
falešné	falešný	k2eAgNnSc4d1	falešné
místo	místo	k1gNnSc4	místo
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Baví	bavit	k5eAaImIp3nS	bavit
se	se	k3xPyFc4	se
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
představou	představa	k1gFnSc7	představa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vojáci	voják	k1gMnPc1	voják
marně	marně	k6eAd1	marně
prohledávají	prohledávat	k5eAaImIp3nP	prohledávat
hroby	hrob	k1gInPc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
krátkou	krátký	k2eAgFnSc4d1	krátká
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
očekává	očekávat	k5eAaImIp3nS	očekávat
svoji	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
odveden	odvést	k5eAaPmNgInS	odvést
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
se	se	k3xPyFc4	se
neděje	dít	k5eNaImIp3nS	dít
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nového	nový	k2eAgMnSc2d1	nový
vězně	vězeň	k1gMnSc2	vězeň
pekaře	pekař	k1gMnSc4	pekař
Garcii	Garcie	k1gFnSc6	Garcie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
večer	večer	k1gInSc1	večer
se	se	k3xPyFc4	se
dovídá	dovídat	k5eAaImIp3nS	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ramón	Ramón	k1gMnSc1	Ramón
Gris	Gris	k1gInSc4	Gris
před	před	k7c7	před
několika	několik	k4yIc7	několik
dny	den	k1gInPc7	den
opustil	opustit	k5eAaPmAgInS	opustit
úkryt	úkryt	k1gInSc1	úkryt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
s	s	k7c7	s
bratrancem	bratranec	k1gMnSc7	bratranec
pohádali	pohádat	k5eAaPmAgMnP	pohádat
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
schovat	schovat	k5eAaPmF	schovat
u	u	k7c2	u
Ibbiety	Ibbieta	k1gFnSc2	Ibbieta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
zavřený	zavřený	k2eAgMnSc1d1	zavřený
<g/>
,	,	kIx,	,
vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
dopoledne	dopoledne	k6eAd1	dopoledne
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
Darbédatově	Darbédatův	k2eAgFnSc6d1	Darbédatův
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
nemocná	nemocný	k2eAgFnSc1d1	nemocná
matka	matka	k1gFnSc1	matka
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
péči	péče	k1gFnSc4	péče
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
rozhovory	rozhovor	k1gInPc4	rozhovor
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
Charlesem	Charles	k1gMnSc7	Charles
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
stresují	stresovat	k5eAaImIp3nP	stresovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Eva	Eva	k1gFnSc1	Eva
žije	žít	k5eAaImIp3nS	žít
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
s	s	k7c7	s
duševně	duševně	k6eAd1	duševně
nemocným	nemocný	k2eAgInSc7d1	nemocný
Pierrem	Pierr	k1gInSc7	Pierr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jako	jako	k8xS	jako
ona	onen	k3xDgFnSc1	onen
neopouští	opouštět	k5eNaImIp3nS	opouštět
byt	byt	k1gInSc4	byt
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
stará	starat	k5eAaImIp3nS	starat
a	a	k8xC	a
sdílí	sdílet	k5eAaImIp3nS	sdílet
jeho	on	k3xPp3gInSc4	on
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Pierrův	Pierrův	k2eAgInSc1d1	Pierrův
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nP	trpět
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
,	,	kIx,	,
ženu	hnát	k5eAaImIp1nS	hnát
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
Agáto	Agáta	k1gFnSc5	Agáta
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Frachot	Frachot	k1gMnSc1	Frachot
mu	on	k3xPp3gNnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
poměrně	poměrně	k6eAd1	poměrně
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
zcela	zcela	k6eAd1	zcela
opustí	opustit	k5eAaPmIp3nP	opustit
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Charles	Charles	k1gMnSc1	Charles
Darbédat	Darbédat	k1gMnSc1	Darbédat
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
Eva	Eva	k1gFnSc1	Eva
stále	stále	k6eAd1	stále
udržuje	udržovat	k5eAaImIp3nS	udržovat
intimní	intimní	k2eAgInSc4d1	intimní
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Pierre	Pierr	k1gInSc5	Pierr
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
začala	začít	k5eAaPmAgFnS	začít
nový	nový	k2eAgInSc4d1	nový
šťastnější	šťastný	k2eAgInSc4d2	šťastnější
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
neuspěje	uspět	k5eNaPmIp3nS	uspět
<g/>
,	,	kIx,	,
pouto	pouto	k1gNnSc1	pouto
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
mužem	muž	k1gMnSc7	muž
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k1gNnSc6	další
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
blouznivých	blouznivý	k2eAgInPc2d1	blouznivý
stavů	stav	k1gInPc2	stav
Eva	Eva	k1gFnSc1	Eva
přitiskne	přitisknout	k5eAaPmIp3nS	přitisknout
rty	ret	k1gInPc4	ret
k	k	k7c3	k
manželově	manželův	k2eAgFnSc3d1	manželova
ruce	ruka	k1gFnSc3	ruka
a	a	k8xC	a
tiše	tiš	k1gFnSc2	tiš
pronese	pronést	k5eAaPmIp3nS	pronést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
být	být	k5eAaImF	být
internován	internovat	k5eAaBmNgMnS	internovat
na	na	k7c6	na
klinice	klinika	k1gFnSc6	klinika
<g/>
.	.	kIx.	.
</s>
<s>
Hérostratos	Hérostratos	k1gMnSc1	Hérostratos
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jediným	jediný	k2eAgInSc7d1	jediný
činem	čin	k1gInSc7	čin
<g/>
,	,	kIx,	,
zapálením	zapálení	k1gNnSc7	zapálení
Artemidina	Artemidin	k2eAgInSc2d1	Artemidin
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Efesu	Efes	k1gInSc6	Efes
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Paul	Paul	k1gMnSc1	Paul
Hilbert	Hilbert	k1gMnSc1	Hilbert
je	on	k3xPp3gNnSc4	on
ničím	ničit	k5eAaImIp1nS	ničit
nevýznamný	významný	k2eNgMnSc1d1	nevýznamný
pařížský	pařížský	k2eAgMnSc1d1	pařížský
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
samotářským	samotářský	k2eAgInSc7d1	samotářský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pohrdá	pohrdat	k5eAaImIp3nS	pohrdat
a	a	k8xC	a
nenávidí	návidět	k5eNaImIp3nS	návidět
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
veřejný	veřejný	k2eAgInSc4d1	veřejný
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sleduje	sledovat	k5eAaImIp3nS	sledovat
nahé	nahý	k2eAgFnPc4d1	nahá
prostitutky	prostitutka	k1gFnPc4	prostitutka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tělesný	tělesný	k2eAgInSc1d1	tělesný
kontakt	kontakt	k1gInSc1	kontakt
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
odporný	odporný	k2eAgInSc1d1	odporný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
fascinován	fascinovat	k5eAaBmNgInS	fascinovat
velkým	velký	k2eAgInSc7d1	velký
činem	čin	k1gInSc7	čin
<g/>
,	,	kIx,	,
sní	sníst	k5eAaPmIp3nS	sníst
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyjde	vyjít	k5eAaPmIp3nS	vyjít
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
postřílí	postřílet	k5eAaPmIp3nS	postřílet
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výpovědi	výpověď	k1gFnSc6	výpověď
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
přebývá	přebývat	k5eAaImIp3nS	přebývat
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
a	a	k8xC	a
po	po	k7c6	po
útratě	útrata	k1gFnSc6	útrata
posledních	poslední	k2eAgInPc2d1	poslední
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
k	k	k7c3	k
činu	čin	k1gInSc3	čin
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
odešle	odeslat	k5eAaPmIp3nS	odeslat
102	[number]	k4	102
kopií	kopie	k1gFnPc2	kopie
dopisu	dopis	k1gInSc2	dopis
slavným	slavný	k2eAgMnPc3d1	slavný
francouzským	francouzský	k2eAgMnPc3d1	francouzský
spisovatelům	spisovatel	k1gMnPc3	spisovatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
svou	svůj	k3xOyFgFnSc4	svůj
motivaci	motivace	k1gFnSc4	motivace
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
psychickém	psychický	k2eAgInSc6d1	psychický
stavu	stav	k1gInSc6	stav
vychází	vycházet	k5eAaImIp3nS	vycházet
do	do	k7c2	do
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
ptá	ptat	k5eAaImIp3nS	ptat
hledající	hledající	k2eAgMnSc1d1	hledající
chodec	chodec	k1gMnSc1	chodec
na	na	k7c6	na
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Hilbert	Hilbert	k1gInSc1	Hilbert
mu	on	k3xPp3gMnSc3	on
vpálí	vpálit	k5eAaPmIp3nS	vpálit
tři	tři	k4xCgFnPc4	tři
kulky	kulka	k1gFnPc4	kulka
do	do	k7c2	do
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
utíkat	utíkat	k5eAaImF	utíkat
před	před	k7c7	před
dopadením	dopadení	k1gNnSc7	dopadení
<g/>
,	,	kIx,	,
vystřelí	vystřelit	k5eAaPmIp3nP	vystřelit
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
kulky	kulka	k1gFnPc1	kulka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgMnS	uniknout
davu	dav	k1gInSc3	dav
a	a	k8xC	a
zaběhne	zaběhnout	k5eAaPmIp3nS	zaběhnout
do	do	k7c2	do
kavárny	kavárna	k1gFnSc2	kavárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uzamkne	uzamknout	k5eAaPmIp3nS	uzamknout
na	na	k7c6	na
toaletě	toaleta	k1gFnSc6	toaleta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
koncích	konec	k1gInPc6	konec
<g/>
,	,	kIx,	,
zbývá	zbývat	k5eAaImIp3nS	zbývat
poslední	poslední	k2eAgFnSc1d1	poslední
kulka	kulka	k1gFnSc1	kulka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Slyší	slyšet	k5eAaImIp3nP	slyšet
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
kdosi	kdosi	k3yInSc1	kdosi
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odemkl	odemknout	k5eAaPmAgMnS	odemknout
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Revolver	revolver	k1gInSc1	revolver
si	se	k3xPyFc3	se
strčí	strčit	k5eAaPmIp3nS	strčit
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
zmáčknou	zmáčknout	k5eAaPmIp3nP	zmáčknout
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
Vyndá	vyndat	k5eAaPmIp3nS	vyndat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
hodí	hodit	k5eAaPmIp3nS	hodit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
otevře	otevřít	k5eAaPmIp3nS	otevřít
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Lucienne	Luciennout	k5eAaImIp3nS	Luciennout
Crispinová	Crispinová	k1gFnSc1	Crispinová
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Lulu	lula	k1gFnSc4	lula
a	a	k8xC	a
Henri	Henre	k1gFnSc4	Henre
jsou	být	k5eAaImIp3nP	být
manželé	manžel	k1gMnPc1	manžel
<g/>
,	,	kIx,	,
intimně	intimně	k6eAd1	intimně
spolu	spolu	k6eAd1	spolu
nežijí	žít	k5eNaImIp3nP	žít
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
formální	formální	k2eAgInSc4d1	formální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
trápeními	trápení	k1gNnPc7	trápení
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
své	svůj	k3xOyFgFnSc6	svůj
přítelkyni	přítelkyně	k1gFnSc6	přítelkyně
Rirretě	Rirreta	k1gFnSc6	Rirreta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
naléhá	naléhat	k5eAaImIp3nS	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
poměr	poměr	k1gInSc1	poměr
neudržovala	udržovat	k5eNaImAgFnS	udržovat
a	a	k8xC	a
od	od	k7c2	od
manžela	manžel	k1gMnSc2	manžel
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Lule	lula	k1gFnSc3	lula
se	se	k3xPyFc4	se
dvoří	dvořit	k5eAaImIp3nS	dvořit
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
muž	muž	k1gMnSc1	muž
Pierre	Pierr	k1gInSc5	Pierr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k6eAd1	také
líbí	líbit	k5eAaImIp3nS	líbit
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
odjedou	odjet	k5eAaPmIp3nP	odjet
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
vily	vila	k1gFnSc2	vila
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
v	v	k7c6	v
Nizze	Nizza	k1gFnSc6	Nizza
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
dá	dát	k5eAaPmIp3nS	dát
Henri	Henr	k1gMnSc3	Henr
facku	facka	k1gFnSc4	facka
jejímu	její	k3xOp3gMnSc3	její
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
<g/>
,	,	kIx,	,
pohár	pohár	k1gInSc1	pohár
trpělivosti	trpělivost	k1gFnSc2	trpělivost
přeteče	přetéct	k5eAaPmIp3nS	přetéct
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
Lulu	lula	k1gFnSc4	lula
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
manželovi	manžel	k1gMnSc3	manžel
na	na	k7c6	na
stole	stol	k1gInSc6	stol
vzkaz	vzkaz	k1gInSc4	vzkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
uvařenou	uvařený	k2eAgFnSc4d1	uvařená
čočku	čočka	k1gFnSc4	čočka
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
to	ten	k3xDgNnSc1	ten
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
Rirretě	Rirreta	k1gFnSc3	Rirreta
<g/>
,	,	kIx,	,
prozatím	prozatím	k6eAd1	prozatím
bude	být	k5eAaImBp3nS	být
bydlet	bydlet	k5eAaImF	bydlet
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
pařížském	pařížský	k2eAgInSc6d1	pařížský
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Pierrem	Pierro	k1gNnSc7	Pierro
je	být	k5eAaImIp3nS	být
smluvená	smluvený	k2eAgFnSc1d1	smluvená
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
odjedou	odjet	k5eAaPmIp3nP	odjet
do	do	k7c2	do
Nizze	Nizza	k1gFnSc3	Nizza
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Rirretou	Rirreta	k1gFnSc7	Rirreta
jdou	jít	k5eAaImIp3nP	jít
z	z	k7c2	z
odpolední	odpolední	k2eAgFnSc2d1	odpolední
kávy	káva	k1gFnSc2	káva
nakupovat	nakupovat	k5eAaBmF	nakupovat
a	a	k8xC	a
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
potkávají	potkávat	k5eAaImIp3nP	potkávat
Henriho	Henri	k1gMnSc4	Henri
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	on	k3xPp3gNnSc4	on
dohání	dohánět	k5eAaImIp3nS	dohánět
a	a	k8xC	a
zoufale	zoufale	k6eAd1	zoufale
naléhá	naléhat	k5eAaImIp3nS	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
manželka	manželka	k1gFnSc1	manželka
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ujíždí	ujíždět	k5eAaImIp3nS	ujíždět
taxíkem	taxík	k1gInSc7	taxík
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
na	na	k7c6	na
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
milování	milování	k1gNnSc6	milování
s	s	k7c7	s
Pierrem	Pierr	k1gInSc7	Pierr
osiří	osiřet	k5eAaPmIp3nS	osiřet
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
výčitky	výčitka	k1gFnPc4	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouto	pouto	k1gNnSc4	pouto
k	k	k7c3	k
manželovi	manžel	k1gMnSc3	manžel
je	být	k5eAaImIp3nS	být
pevnější	pevný	k2eAgFnSc1d2	pevnější
než	než	k8xS	než
očekávala	očekávat	k5eAaImAgFnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozhodnuta	rozhodnout	k5eAaPmNgFnS	rozhodnout
jít	jít	k5eAaImF	jít
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
a	a	k8xC	a
rozejít	rozejít	k5eAaPmF	rozejít
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Henriho	Henrize	k6eAd1	Henrize
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
depresi	deprese	k1gFnSc6	deprese
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnPc4	jeho
prosby	prosba	k1gFnPc4	prosba
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k6eAd1	dopoledne
však	však	k9	však
Pierre	Pierr	k1gInSc5	Pierr
na	na	k7c6	na
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
její	její	k3xOp3gInSc4	její
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
hotelu	hotel	k1gInSc6	hotel
navštívili	navštívit	k5eAaPmAgMnP	navštívit
sousedé	soused	k1gMnPc1	soused
Texierovi	Texierův	k2eAgMnPc1d1	Texierův
a	a	k8xC	a
přemlouvali	přemlouvat	k5eAaImAgMnP	přemlouvat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
manžel	manžel	k1gMnSc1	manžel
v	v	k7c6	v
koncích	konec	k1gInPc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Pierra	Pierra	k6eAd1	Pierra
prý	prý	k9	prý
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gInSc4	on
nadále	nadále	k6eAd1	nadále
vídat	vídat	k5eAaImF	vídat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
odjet	odjet	k5eAaPmF	odjet
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
zodpovědně	zodpovědně	k6eAd1	zodpovědně
postarat	postarat	k5eAaPmF	postarat
o	o	k7c4	o
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k6eAd1	jen
je	být	k5eAaImIp3nS	být
Pierrovi	Pierr	k1gMnSc3	Pierr
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
hotelu	hotel	k1gInSc6	hotel
nalezli	naleznout	k5eAaPmAgMnP	naleznout
sousedé	soused	k1gMnPc1	soused
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
...	...	k?	...
<g/>
?	?	kIx.	?
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
z	z	k7c2	z
perspektivy	perspektiva	k1gFnSc2	perspektiva
Luciena	Lucien	k1gMnSc2	Lucien
Fleuriera	Fleurier	k1gMnSc2	Fleurier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
malého	malý	k2eAgMnSc2d1	malý
průmyslníka	průmyslník	k1gMnSc2	průmyslník
zaměstnávajícího	zaměstnávající	k2eAgMnSc2d1	zaměstnávající
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
předurčen	předurčit	k5eAaPmNgMnS	předurčit
převzít	převzít	k5eAaPmF	převzít
otcovu	otcův	k2eAgFnSc4d1	otcova
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
být	být	k5eAaImF	být
vůdčím	vůdčí	k2eAgMnSc7d1	vůdčí
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
Lucien	Lucien	k1gInSc1	Lucien
prochází	procházet	k5eAaImIp3nS	procházet
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
krůčků	krůček	k1gInPc2	krůček
postupným	postupný	k2eAgInSc7d1	postupný
procesem	proces	k1gInSc7	proces
sebeuvědomování	sebeuvědomování	k1gNnSc2	sebeuvědomování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
vnímat	vnímat	k5eAaImF	vnímat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
mužským	mužský	k2eAgNnSc7d1	mužské
a	a	k8xC	a
ženským	ženský	k2eAgNnSc7d1	ženské
pohlavím	pohlaví	k1gNnSc7	pohlaví
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
období	období	k1gNnSc4	období
vzdoru	vzdor	k1gInSc2	vzdor
<g/>
,	,	kIx,	,
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
milovat	milovat	k5eAaImF	milovat
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
předstírá	předstírat	k5eAaImIp3nS	předstírat
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
konvence	konvence	k1gFnSc1	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
komplexy	komplex	k1gInPc4	komplex
<g/>
,	,	kIx,	,
v	v	k7c6	v
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
neukotven	ukotven	k2eNgInSc1d1	ukotven
a	a	k8xC	a
stále	stále	k6eAd1	stále
hledá	hledat	k5eAaImIp3nS	hledat
jeho	on	k3xPp3gInSc4	on
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
ani	ani	k9	ani
on	on	k3xPp3gMnSc1	on
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
přelud	přelud	k1gInSc4	přelud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
prázdnin	prázdniny	k1gFnPc2	prázdniny
si	se	k3xPyFc3	se
naplánuje	naplánovat	k5eAaBmIp3nS	naplánovat
vykonat	vykonat	k5eAaPmF	vykonat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
malým	malý	k2eAgInSc7d1	malý
revolverem	revolver	k1gInSc7	revolver
uloženým	uložený	k2eAgInSc7d1	uložený
v	v	k7c6	v
maminčině	maminčin	k2eAgInSc6d1	maminčin
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
neodhodlá	odhodlat	k5eNaPmIp3nS	odhodlat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
stejně	stejně	k6eAd1	stejně
tápajícího	tápající	k2eAgMnSc2d1	tápající
spolužáka	spolužák	k1gMnSc2	spolužák
Berliaka	Berliak	k1gMnSc2	Berliak
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
surrealistou	surrealista	k1gMnSc7	surrealista
Bergérem	Bergér	k1gMnSc7	Bergér
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zcela	zcela	k6eAd1	zcela
propadne	propadnout	k5eAaPmIp3nS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Čte	číst	k5eAaImIp3nS	číst
Rimbauda	Rimbauda	k1gFnSc1	Rimbauda
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
Freudovu	Freudův	k2eAgFnSc4d1	Freudova
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nutkání	nutkání	k1gNnSc4	nutkání
vypravit	vypravit	k5eAaPmF	vypravit
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
vlastní	vlastní	k2eAgInSc4d1	vlastní
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
komplex	komplex	k1gInSc4	komplex
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
heterosexuální	heterosexuální	k2eAgFnSc1d1	heterosexuální
orientace	orientace	k1gFnSc1	orientace
poddává	poddávat	k5eAaImIp3nS	poddávat
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgNnSc3	svůj
"	"	kIx"	"
<g/>
učiteli	učitel	k1gMnPc7	učitel
<g/>
"	"	kIx"	"
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
během	během	k7c2	během
výletu	výlet	k1gInSc2	výlet
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nP	trpět
výčitkami	výčitka	k1gFnPc7	výčitka
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
sám	sám	k3xTgMnSc1	sám
sebou	se	k3xPyFc7	se
zhnusen	zhnusen	k2eAgMnSc1d1	zhnusen
a	a	k8xC	a
obává	obávat	k5eAaImIp3nS	obávat
se	se	k3xPyFc4	se
vyzrazení	vyzrazení	k1gNnPc2	vyzrazení
intimního	intimní	k2eAgInSc2d1	intimní
poměru	poměr	k1gInSc2	poměr
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
jistě	jistě	k9	jistě
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
jeho	jeho	k3xOp3gFnSc4	jeho
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Přerušuje	přerušovat	k5eAaImIp3nS	přerušovat
další	další	k2eAgInPc4d1	další
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k8xC	i
Berliakem	Berliak	k1gMnSc7	Berliak
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
přátelství	přátelství	k1gNnSc4	přátelství
se	s	k7c7	s
spolužákem	spolužák	k1gMnSc7	spolužák
Guigardem	Guigard	k1gMnSc7	Guigard
<g/>
,	,	kIx,	,
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Poznává	poznávat	k5eAaImIp3nS	poznávat
první	první	k4xOgFnSc4	první
lásku	láska	k1gFnSc4	láska
Maud	Mauda	k1gFnPc2	Mauda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemíní	mínit	k5eNaImIp3nS	mínit
se	se	k3xPyFc4	se
vázat	vázat	k5eAaImF	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Kamarád	kamarád	k1gMnSc1	kamarád
ho	on	k3xPp3gNnSc4	on
přivádí	přivádět	k5eAaImIp3nS	přivádět
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
radikálních	radikální	k2eAgMnPc2d1	radikální
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdě	tvrdě	k6eAd1	tvrdě
propagují	propagovat	k5eAaImIp3nP	propagovat
francouzský	francouzský	k2eAgInSc4d1	francouzský
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
<g/>
,	,	kIx,	,
Lucien	Lucien	k2eAgMnSc1d1	Lucien
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
silným	silný	k2eAgMnSc7d1	silný
antisemitou	antisemita	k1gMnSc7	antisemita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
hrdý	hrdý	k2eAgMnSc1d1	hrdý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
zkopou	zkopat	k5eAaPmIp3nP	zkopat
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
chodce	chodec	k1gMnSc4	chodec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
četl	číst	k5eAaImAgMnS	číst
komunistický	komunistický	k2eAgInSc4d1	komunistický
deník	deník	k1gInSc4	deník
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Humanité	Humanitý	k2eAgFnPc1d1	Humanitý
<g/>
.	.	kIx.	.
</s>
<s>
Našel	najít	k5eAaPmAgMnS	najít
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
dozrál	dozrát	k5eAaPmAgMnS	dozrát
čas	čas	k1gInSc4	čas
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
období	období	k1gNnSc6	období
hledání	hledání	k1gNnSc2	hledání
a	a	k8xC	a
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
má	mít	k5eAaImIp3nS	mít
jasný	jasný	k2eAgInSc4d1	jasný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
odpovědně	odpovědně	k6eAd1	odpovědně
<g/>
,	,	kIx,	,
dostuduje	dostudovat	k5eAaPmIp3nS	dostudovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
nahradí	nahradit	k5eAaPmIp3nS	nahradit
otce	otec	k1gMnSc4	otec
ve	v	k7c6	v
vůdcovské	vůdcovský	k2eAgFnSc6d1	vůdcovská
pozici	pozice	k1gFnSc6	pozice
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ožení	oženit	k5eAaPmIp3nS	oženit
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
kupu	kupa	k1gFnSc4	kupa
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mladíka	mladík	k1gMnSc2	mladík
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
existenciální	existenciální	k2eAgFnSc4d1	existenciální
změnu	změna	k1gFnSc4	změna
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
světu	svět	k1gInSc3	svět
zvěstovat	zvěstovat	k5eAaImF	zvěstovat
ještě	ještě	k6eAd1	ještě
novým	nový	k2eAgInSc7d1	nový
pěstěným	pěstěný	k2eAgInSc7d1	pěstěný
knírem	knír	k1gInSc7	knír
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Musilová	Musilová	k1gFnSc1	Musilová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Musilová	Musilová	k1gFnSc1	Musilová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
;	;	kIx,	;
Nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMA	KMA	kA	KMA
1	[number]	k4	1
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Musilová	Musilová	k1gFnSc1	Musilová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čermák	Čermák	k1gMnSc1	Čermák
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Steinová	Steinová	k1gFnSc1	Steinová
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
;	;	kIx,	;
Nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMA	KMA	kA	KMA
1	[number]	k4	1
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Musilová	Musilová	k1gFnSc1	Musilová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čermák	Čermák	k1gMnSc1	Čermák
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Steinová	Steinová	k1gFnSc1	Steinová
<g/>
.	.	kIx.	.
</s>
