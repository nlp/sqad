<s>
Želivka	Želivka	k1gFnSc1	Želivka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
a	a	k8xC	a
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Benešov	Benešov	k1gInSc1	Benešov
a	a	k8xC	a
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
levostranný	levostranný	k2eAgInSc1d1	levostranný
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Sázavy	Sázava	k1gFnSc2	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
Želivka	Želivka	k1gFnSc1	Želivka
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Sázava	Sázava	k1gFnSc1	Sázava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Soutická	Soutický	k2eAgFnSc1d1	Soutický
nebo	nebo	k8xC	nebo
Zahrádecká	Zahrádecký	k2eAgFnSc1d1	Zahrádecká
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInSc1	její
dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
až	až	k9	až
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
Siloe	Silo	k1gInSc2	Silo
tak	tak	k6eAd1	tak
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
Želivský	želivský	k2eAgInSc4d1	želivský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svou	svůj	k3xOyFgFnSc7	svůj
čistotou	čistota	k1gFnSc7	čistota
a	a	k8xC	a
kvalitou	kvalita	k1gFnSc7	kvalita
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
103,9	[number]	k4	103,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
délky	délka	k1gFnSc2	délka
připadá	připadat	k5eAaPmIp3nS	připadat
40	[number]	k4	40
km	km	kA	km
na	na	k7c4	na
říčku	říčka	k1gFnSc4	říčka
Hejlovku	Hejlovka	k1gFnSc4	Hejlovka
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Želivky	Želivka	k1gFnSc2	Želivka
měří	měřit	k5eAaImIp3nS	měřit
1188,4	[number]	k4	1188,4
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
jako	jako	k9	jako
Hejlovka	Hejlovka	k1gFnSc1	Hejlovka
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
osady	osada	k1gFnSc2	osada
Vlásenice-Drbohlavy	Vlásenice-Drbohlava	k1gFnSc2	Vlásenice-Drbohlava
<g/>
,	,	kIx,	,
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
se	se	k3xPyFc4	se
pramen	pramen	k1gInSc1	pramen
řeky	řeka	k1gFnSc2	řeka
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
Bukovým	bukový	k2eAgInSc7d1	bukový
kopcem	kopec	k1gInSc7	kopec
(	(	kIx(	(
<g/>
702	[number]	k4	702
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
677,3	[number]	k4	677,3
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
katastrů	katastr	k1gInPc2	katastr
obcí	obec	k1gFnPc2	obec
Mezná	mezný	k2eAgFnSc1d1	Mezná
a	a	k8xC	a
Častrov	Častrov	k1gInSc1	Častrov
<g/>
.	.	kIx.	.
</s>
<s>
Říčka	říčka	k1gFnSc1	říčka
teče	téct	k5eAaImIp3nS	téct
nejprve	nejprve	k6eAd1	nejprve
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
údolím	údolí	k1gNnSc7	údolí
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
nedaleko	nedaleko	k7c2	nedaleko
Vlásenice	Vlásenice	k1gFnSc2	Vlásenice
přijímá	přijímat	k5eAaImIp3nS	přijímat
zleva	zleva	k6eAd1	zleva
Cerekvický	Cerekvický	k2eAgInSc1d1	Cerekvický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přitéká	přitékat	k5eAaImIp3nS	přitékat
od	od	k7c2	od
Nové	Nové	k2eAgFnSc2d1	Nové
Cerekve	Cerekev	k1gFnSc2	Cerekev
<g/>
.	.	kIx.	.
</s>
<s>
Říčka	říčka	k1gFnSc1	říčka
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
lesnaté	lesnatý	k2eAgFnSc2d1	lesnatá
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
nedaleko	nedaleko	k7c2	nedaleko
Krasíkovic	Krasíkovice	k1gFnPc2	Krasíkovice
posiluje	posilovat	k5eAaImIp3nS	posilovat
její	její	k3xOp3gInSc1	její
tok	tok	k1gInSc1	tok
zprava	zprava	k6eAd1	zprava
přitékající	přitékající	k2eAgInSc1d1	přitékající
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hráze	hráz	k1gFnSc2	hráz
Sedlické	sedlický	k2eAgFnSc2d1	Sedlická
nádrže	nádrž	k1gFnSc2	nádrž
(	(	kIx(	(
<g/>
ř.	ř.	k?	ř.
km	km	kA	km
63	[number]	k4	63
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
již	již	k6eAd1	již
nazývána	nazývat	k5eAaImNgFnS	nazývat
Želivkou	Želivka	k1gFnSc7	Želivka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
řeka	řeka	k1gFnSc1	řeka
udržuje	udržovat	k5eAaImIp3nS	udržovat
převážně	převážně	k6eAd1	převážně
severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Želiva	Želiv	k1gInSc2	Želiv
přijímá	přijímat	k5eAaImIp3nS	přijímat
zleva	zleva	k6eAd1	zleva
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
přítok	přítok	k1gInSc4	přítok
řeku	řeka	k1gFnSc4	řeka
Trnavu	Trnava	k1gFnSc4	Trnava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
dalších	další	k2eAgFnPc2d1	další
10	[number]	k4	10
km	km	kA	km
toku	tok	k1gInSc2	tok
vzdouvá	vzdouvat	k5eAaImIp3nS	vzdouvat
její	její	k3xOp3gFnPc4	její
vody	voda	k1gFnPc4	voda
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
39,1	[number]	k4	39,1
km	km	kA	km
významná	významný	k2eAgFnSc1d1	významná
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
nádrž	nádrž	k1gFnSc1	nádrž
Švihov	Švihov	k1gInSc1	Švihov
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Želivka	Želivka	k1gFnSc1	Želivka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hráz	hráz	k1gFnSc1	hráz
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
4,29	[number]	k4	4,29
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
je	být	k5eAaImIp3nS	být
dálkovým	dálkový	k2eAgInSc7d1	dálkový
přivaděčem	přivaděč	k1gInSc7	přivaděč
zásobena	zásoben	k2eAgFnSc1d1	zásobena
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
čtyřech	čtyři	k4xCgInPc6	čtyři
kilometrech	kilometr	k1gInPc6	kilometr
toku	tok	k1gInSc2	tok
ústí	ústit	k5eAaImIp3nS	ústit
Želivka	Želivka	k1gFnSc1	Želivka
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Sázavy	Sázava	k1gFnSc2	Sázava
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
98,8	[number]	k4	98,8
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
u	u	k7c2	u
Soutic	Soutice	k1gFnPc2	Soutice
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
318,1	[number]	k4	318,1
m.	m.	k?	m.
Největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Želivky	Želivka	k1gFnPc4	Želivka
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
délky	délka	k1gFnSc2	délka
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
plochy	plocha	k1gFnPc1	plocha
povodí	povodí	k1gNnSc2	povodí
a	a	k8xC	a
vodnosti	vodnost	k1gFnSc2	vodnost
týče	týkat	k5eAaImIp3nS	týkat
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
Želivky	Želivka	k1gFnSc2	Želivka
činí	činit	k5eAaImIp3nS	činit
1,24	[number]	k4	1,24
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
povodí	povodí	k1gNnSc6	povodí
nachází	nacházet	k5eAaImIp3nS	nacházet
1288	[number]	k4	1288
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
360	[number]	k4	360
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Potoků	potok	k1gInPc2	potok
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
pět	pět	k4xCc1	pět
vodotečí	vodoteč	k1gFnPc2	vodoteč
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
mezi	mezi	k7c7	mezi
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
délce	délka	k1gFnSc6	délka
40	[number]	k4	40
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
nalézá	nalézat	k5eAaImIp3nS	nalézat
jeden	jeden	k4xCgInSc4	jeden
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Cerekvický	Cerekvický	k2eAgInSc1d1	Cerekvický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
89,4	[number]	k4	89,4
Bělá	bělat	k5eAaImIp3nS	bělat
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
72,9	[number]	k4	72,9
Jankovský	Jankovský	k2eAgInSc4d1	Jankovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
64,4	[number]	k4	64,4
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
52,0	[number]	k4	52,0
Martinický	Martinický	k2eAgInSc4d1	Martinický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
36,7	[number]	k4	36,7
Blažejovický	Blažejovický	k2eAgInSc4d1	Blažejovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
29,1	[number]	k4	29,1
Sedlický	sedlický	k2eAgInSc4d1	sedlický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
7,3	[number]	k4	7,3
Želivka	Želivka	k1gFnSc1	Želivka
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
jako	jako	k9	jako
Sázava	Sázava	k1gFnSc1	Sázava
mezi	mezi	k7c7	mezi
toky	tok	k1gInPc7	tok
vrchovinno-nížinné	vrchovinnoížinný	k2eAgFnSc2d1	vrchovinno-nížinný
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
a	a	k8xC	a
jarním	jarní	k2eAgNnSc6d1	jarní
období	období	k1gNnSc6	období
odteče	odtéct	k5eAaPmIp3nS	odtéct
nad	nad	k7c7	nad
60	[number]	k4	60
%	%	kIx~	%
celoročního	celoroční	k2eAgInSc2d1	celoroční
odtoku	odtok	k1gInSc2	odtok
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Želivky	Želivka	k1gFnSc2	Želivka
v	v	k7c6	v
Souticích	Soutik	k1gMnPc6	Soutik
nedaleko	nedaleko	k7c2	nedaleko
ústí	ústí	k1gNnPc2	ústí
činí	činit	k5eAaImIp3nS	činit
3,91	[number]	k4	3,91
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrné	průměrný	k2eAgInPc4d1	průměrný
měsíční	měsíční	k2eAgInPc4d1	měsíční
průtoky	průtok	k1gInPc4	průtok
Želivky	Želivka	k1gFnSc2	Želivka
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nesměřice	Nesměřice	k1gFnSc2	Nesměřice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
M-denní	Menní	k2eAgInPc4d1	M-denní
průtoky	průtok	k1gInPc4	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
<g/>
:	:	kIx,	:
N-leté	Netý	k2eAgInPc1d1	N-letý
průtoky	průtok	k1gInPc1	průtok
v	v	k7c6	v
Čakovicích	Čakovice	k1gFnPc6	Čakovice
<g/>
:	:	kIx,	:
N-leté	Netý	k2eAgInPc4d1	N-letý
průtoky	průtok	k1gInPc4	průtok
v	v	k7c6	v
Želivě	Želiv	k1gInSc6	Želiv
<g/>
:	:	kIx,	:
N-leté	Netý	k2eAgInPc1d1	N-letý
průtoky	průtok	k1gInPc1	průtok
v	v	k7c6	v
Poříčí	Poříčí	k1gNnSc6	Poříčí
<g/>
:	:	kIx,	:
N-leté	Netý	k2eAgInPc1d1	N-letý
průtoky	průtok	k1gInPc1	průtok
v	v	k7c6	v
Souticích	Soutice	k1gFnPc6	Soutice
<g/>
:	:	kIx,	:
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
Želivky	Želivka	k1gFnSc2	Želivka
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
přehradními	přehradní	k2eAgFnPc7d1	přehradní
nádržemi	nádrž	k1gFnPc7	nádrž
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
povodí	povodí	k1gNnSc6	povodí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
hospodařením	hospodaření	k1gNnSc7	hospodaření
na	na	k7c6	na
vodním	vodní	k2eAgNnSc6d1	vodní
díle	dílo	k1gNnSc6	dílo
Želivka-Švihov	Želivka-Švihovo	k1gNnPc2	Želivka-Švihovo
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
průtok	průtok	k1gInSc1	průtok
(	(	kIx(	(
<g/>
Qa	Qa	k1gFnSc1	Qa
<g/>
)	)	kIx)	)
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
VD	VD	kA	VD
Švihov	Švihov	k1gInSc1	Švihov
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
6,98	[number]	k4	6,98
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
odváděno	odváděn	k2eAgNnSc4d1	odváděno
do	do	k7c2	do
vodovodní	vodovodní	k2eAgFnSc2d1	vodovodní
sítě	síť	k1gFnSc2	síť
průměrně	průměrně	k6eAd1	průměrně
2,5	[number]	k4	2,5
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chybí	chybět	k5eAaImIp3nS	chybět
Sázavě	Sázava	k1gFnSc3	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úbytek	úbytek	k1gInSc1	úbytek
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Porovnání	porovnání	k1gNnSc1	porovnání
průměrných	průměrný	k2eAgInPc2d1	průměrný
ročních	roční	k2eAgInPc2d1	roční
průtoků	průtok	k1gInPc2	průtok
(	(	kIx(	(
<g/>
QRO	QRO	kA	QRO
<g/>
/	/	kIx~	/
<g/>
QRN	QRN	kA	QRN
<g/>
)	)	kIx)	)
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
průměrnému	průměrný	k2eAgInSc3d1	průměrný
průtoku	průtok	k1gInSc3	průtok
Želivky	Želivka	k1gFnSc2	Želivka
v	v	k7c6	v
profilech	profil	k1gInPc6	profil
Soutice	Soutice	k1gFnSc1	Soutice
a	a	k8xC	a
Nesměřice	Nesměřice	k1gFnSc1	Nesměřice
(	(	kIx(	(
<g/>
Qa	Qa	k1gFnSc1	Qa
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
odběry	odběr	k1gInPc7	odběr
na	na	k7c4	na
VD	VD	kA	VD
Želivka-Švihov	Želivka-Švihov	k1gInSc4	Želivka-Švihov
<g/>
)	)	kIx)	)
Qa	Qa	k1gFnSc4	Qa
-	-	kIx~	-
průměrný	průměrný	k2eAgInSc1d1	průměrný
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
průtok	průtok	k1gInSc1	průtok
<g />
.	.	kIx.	.
</s>
<s>
QRO	QRO	kA	QRO
-	-	kIx~	-
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
(	(	kIx(	(
<g/>
měřený	měřený	k2eAgInSc1d1	měřený
<g/>
)	)	kIx)	)
průtok	průtok	k1gInSc1	průtok
QRN	QRN	kA	QRN
-	-	kIx~	-
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
neovlivněný	ovlivněný	k2eNgInSc1d1	neovlivněný
(	(	kIx(	(
<g/>
rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
<g/>
)	)	kIx)	)
průtok	průtok	k1gInSc1	průtok
ZPR	ZPR	kA	ZPR
-	-	kIx~	-
změna	změna	k1gFnSc1	změna
průtoku	průtok	k1gInSc2	průtok
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
QRO	QRO	kA	QRO
a	a	k8xC	a
QRN	QRN	kA	QRN
<g/>
)	)	kIx)	)
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Němčice	Němčice	k1gFnSc2	Němčice
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Sedlice	Sedlice	k1gFnPc4	Sedlice
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Trnávka	Trnávka	k1gFnSc1	Trnávka
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vřesník	Vřesník	k1gInSc1	Vřesník
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Želivka-Švihov	Želivka-Švihov	k1gInSc1	Želivka-Švihov
<g />
.	.	kIx.	.
</s>
<s>
11253-1	[number]	k4	11253-1
-	-	kIx~	-
v	v	k7c6	v
Ústrašíně	Ústrašína	k1gFnSc6	Ústrašína
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
34-028	[number]	k4	34-028
-	-	kIx~	-
mezi	mezi	k7c7	mezi
Ondřejovem	Ondřejovo	k1gNnSc7	Ondřejovo
a	a	k8xC	a
Ústrašínem	Ústrašín	k1gInSc7	Ústrašín
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
03414-1	[number]	k4	03414-1
-	-	kIx~	-
ve	v	k7c6	v
Vlásenici	Vlásenice	k1gFnSc6	Vlásenice
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
MK	MK	kA	MK
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
pod	pod	k7c7	pod
Dubovicemi	Dubovice	k1gFnPc7	Dubovice
34-060	[number]	k4	34-060
a	a	k8xC	a
34-061	[number]	k4	34-061
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
inundační	inundační	k2eAgInSc1d1	inundační
<g/>
)	)	kIx)	)
-	-	kIx~	-
lipické	lipický	k2eAgNnSc1d1	lipický
údolí	údolí	k1gNnSc1	údolí
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
112-049	[number]	k4	112-049
-	-	kIx~	-
pod	pod	k7c7	pod
Bácovicemi	Bácovice	k1gFnPc7	Bácovice
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
12925-2	[number]	k4	12925-2
-	-	kIx~	-
mezi	mezi	k7c7	mezi
Krasíkovicemi	Krasíkovice	k1gFnPc7	Krasíkovice
a	a	k8xC	a
Pobistrýcemi	Pobistrýce	k1gFnPc7	Pobistrýce
-	-	kIx~	-
rok	rok	k1gInSc1	rok
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
MK	MK	kA	MK
u	u	k7c2	u
Prokopova	Prokopův	k2eAgInSc2d1	Prokopův
mlýna	mlýn	k1gInSc2	mlýn
-	-	kIx~	-
mezi	mezi	k7c7	mezi
Svépravicemi	Svépravice	k1gFnPc7	Svépravice
a	a	k8xC	a
Kojčicemi	Kojčice	k1gFnPc7	Kojčice
</s>
