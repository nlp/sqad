<s>
Muska	Muska	k1gFnSc1
(	(	kIx(
<g/>
amulet	amulet	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Muska	Muska	k1gFnSc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
amulet	amulet	k1gInSc1
obsahující	obsahující	k2eAgInSc1d1
kopii	kopie	k1gFnSc4
veršů	verš	k1gInPc2
z	z	k7c2
Koránu	korán	k1gInSc2
nebo	nebo	k8xC
jiných	jiný	k2eAgFnPc2d1
modliteb	modlitba	k1gFnPc2
zabalených	zabalený	k2eAgFnPc2d1
do	do	k7c2
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
chránit	chránit	k5eAaImF
majitele	majitel	k1gMnPc4
před	před	k7c7
zlými	zlý	k2eAgInPc7d1
vlivy	vliv	k1gInPc7
nebo	nebo	k8xC
mu	on	k3xPp3gMnSc3
dopřát	dopřát	k5eAaPmF
štěstí	štěstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopie	kopie	k1gFnSc1
modliteb	modlitba	k1gFnPc2
je	být	k5eAaImIp3nS
vložena	vložit	k5eAaPmNgFnS
do	do	k7c2
ozdobné	ozdobný	k2eAgFnSc2d1
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1
dózy	dóza	k1gFnSc2
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
stříbrné	stříbrný	k2eAgFnSc2d1
nebo	nebo	k8xC
zlaté	zlatý	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
nosí	nosit	k5eAaImIp3nS
buď	buď	k8xC
jako	jako	k9
medailónek	medailónek	k1gInSc4
na	na	k7c6
krku	krk	k1gInSc6
nebo	nebo	k8xC
zavěšený	zavěšený	k2eAgMnSc1d1
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
slova	slovo	k1gNnSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
tureckého	turecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
“	“	k?
<g/>
nüsha	nüsha	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
opis	opis	k1gInSc1
<g/>
“	“	k?
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
modlitby	modlitba	k1gFnSc2
vložené	vložený	k2eAgFnSc2d1
do	do	k7c2
schránky	schránka	k1gFnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
a	a	k8xC
většinou	většina	k1gFnSc7
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
předcházení	předcházení	k1gNnSc1
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
vyléčení	vyléčení	k1gNnSc1
nebo	nebo	k8xC
vymanutí	vymanutí	k1gNnSc1
se	se	k3xPyFc4
ze	z	k7c2
špatných	špatný	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
a	a	k8xC
situací	situace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Turecku	Turecko	k1gNnSc6
je	být	k5eAaImIp3nS
nejrozšířenější	rozšířený	k2eAgInSc1d3
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1
tvar	tvar	k1gInSc1
musky	muska	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
islámských	islámský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
jej	on	k3xPp3gMnSc4
můžete	moct	k5eAaImIp2nP
najít	najít	k5eAaPmF
i	i	k9
ve	v	k7c6
tvaru	tvar	k1gInSc6
čtverce	čtverec	k1gInSc2
nebo	nebo	k8xC
srdce	srdce	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nazar	Nazar	k1gMnSc1
</s>
<s>
Uhranutí	uhranutí	k1gNnSc1
(	(	kIx(
<g/>
zlý	zlý	k2eAgInSc1d1
pohled	pohled	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.islamseli.com	www.islamseli.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
bilimlersitesi	bilimlersitese	k1gFnSc4
<g/>
.	.	kIx.
<g/>
tr	tr	k?
<g/>
.	.	kIx.
<g/>
gg	gg	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
