<s>
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
sasko-gothajsko-altenburský	sasko-gothajsko-altenburský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
vévoda	vévoda	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Christian	Christian	k1gMnSc1
Vogel	Vogel	k1gMnSc1
von	von	k1gInSc4
Vogelstein	Vogelstein	k2eAgInSc4d1
<g/>
,	,	kIx,
1815	#num#	k4
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1822	#num#	k4
–	–	k?
1825	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1774	#num#	k4
</s>
<s>
Gotha	Gotha	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1825	#num#	k4
</s>
<s>
Gotha	Gotha	k1gFnSc1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Wettinové	Wettin	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1774	#num#	k4
<g/>
,	,	kIx,
Gotha	Gotha	k1gFnSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1825	#num#	k4
<g/>
,	,	kIx,
Gotha	Gotha	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
sasko-gothajsko-altenburským	sasko-gothajsko-altenburský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
třetí	třetí	k4xOgMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
druhý	druhý	k4xOgMnSc1
přeživší	přeživší	k2eAgMnSc1d1
syn	syn	k1gMnSc1
vévody	vévoda	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburského	Sasko-Gothajsko-Altenburský	k2eAgNnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Šarloty	Šarlota	k1gFnSc2
Sasko-Meiningenské	Sasko-Meiningenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
staršího	starý	k2eAgMnSc2d2
bratra	bratr	k1gMnSc2
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
neměl	mít	k5eNaImAgMnS
syna	syn	k1gMnSc4
(	(	kIx(
<g/>
1822	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zdědil	zdědit	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
sasko-gothajsko-altenburské	sasko-gothajsko-altenburský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vojenském	vojenský	k2eAgInSc6d1
výcviku	výcvik	k1gInSc6
v	v	k7c6
mládí	mládí	k1gNnSc6
bojoval	bojovat	k5eAaImAgInS
v	v	k7c6
napoleonských	napoleonský	k2eAgNnPc6d1
taženích	tažení	k1gNnPc6
a	a	k8xC
byl	být	k5eAaImAgMnS
těžce	těžce	k6eAd1
raněn	ranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
těchto	tento	k3xDgNnPc2
zranění	zranění	k1gNnPc2
byl	být	k5eAaImAgInS
po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
neustále	neustále	k6eAd1
nemocný	mocný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
nemocem	nemoc	k1gFnPc3
dlouho	dlouho	k6eAd1
cestoval	cestovat	k5eAaImAgMnS
a	a	k8xC
hledal	hledat	k5eAaImAgInS
lék	lék	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
cest	cesta	k1gFnPc2
opouštěl	opouštět	k5eAaImAgMnS
vévodství	vévodství	k1gNnSc4
a	a	k8xC
vládu	vláda	k1gFnSc4
ponechával	ponechávat	k5eAaImAgMnS
na	na	k7c6
tajném	tajný	k1gMnSc6
radovi	rada	k1gMnSc6
Bernhardovi	Bernhard	k1gMnSc6
von	von	k1gInSc1
Lindenau	Lindenaus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1825	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
50	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
vlády	vláda	k1gFnSc2
svobodný	svobodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
smrtí	smrt	k1gFnPc2
vymřela	vymřít	k5eAaPmAgFnS
sasko-gothajsko-altenburská	sasko-gothajsko-altenburský	k2eAgFnSc1d1
linie	linie	k1gFnSc1
a	a	k8xC
vévodství	vévodství	k1gNnSc1
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
mezi	mezi	k7c4
Wettinské	Wettinský	k2eAgMnPc4d1
příbuzné	příbuzný	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgInSc1d1
obdržel	obdržet	k5eAaPmAgMnS
Gothu	Gotha	k1gFnSc4
a	a	k8xC
změnil	změnit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
titul	titul	k1gInSc4
na	na	k7c4
sasko-kobursko-gothajského	sasko-kobursko-gothajský	k2eAgMnSc4d1
vévodu	vévoda	k1gMnSc4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
obě	dva	k4xCgNnPc4
vévodství	vévodství	k1gNnPc4
zůstala	zůstat	k5eAaPmAgFnS
technicky	technicky	k6eAd1
oddělená	oddělený	k2eAgFnSc1d1
v	v	k7c6
personální	personální	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Altenburgu	Altenburg	k1gInSc2
následně	následně	k6eAd1
vládl	vládnout	k5eAaImAgMnS
sasko-hildbughausenský	sasko-hildbughausenský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Anhaltsko-Zerbstský	Anhaltsko-Zerbstský	k2eAgMnSc1d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Augusta	August	k1gMnSc2
Anhaltsko-Zerbstská	Anhaltsko-Zerbstský	k2eAgFnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc5d1
</s>
<s>
Marie	Marie	k1gFnSc1
Hedvika	Hedvika	k1gFnSc1
Hesensko-Darmstadtská	Hesensko-Darmstadtský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Dorotea	Dorote	k1gInSc2
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc4d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
Sasko-Gothajsko-Altenburská	Sasko-Gothajsko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
Sasko-Weissenfelská	Sasko-Weissenfelský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Sasko-Gothajský	sasko-gothajský	k2eAgMnSc5d1
</s>
<s>
Bernard	Bernard	k1gMnSc1
I.	I.	kA
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgInSc4d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Sasko-Altenburská	Sasko-Altenburský	k2eAgFnSc1d1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgMnSc1d1
</s>
<s>
Anton	Anton	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
Brunšvicko-Wolfenbüttelský	Brunšvicko-Wolfenbüttelský	k2eAgMnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Eleonora	Eleonora	k1gFnSc1
Brunšvicko-Wolfenbüttelská	Brunšvicko-Wolfenbüttelský	k2eAgFnSc1d1
</s>
<s>
Juliána	Juliána	k1gFnSc1
Šlesvicko-Holštínsko-Sonderbursko-Norburský	Šlesvicko-Holštínsko-Sonderbursko-Norburský	k2eAgInSc1d1
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Sasko-Meiningenská	Sasko-Meiningenský	k2eAgFnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
Hesensko-Philippsthalská	Hesensko-Philippsthalský	k2eAgNnPc5d1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
Hesensko-Philippsthalský	Hesensko-Philippsthalský	k2eAgInSc4d1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Amálie	Amálie	k1gFnSc1
ze	z	k7c2
Solms-Laubachu	Solms-Laubach	k1gInSc2
</s>
<s>
Šarlota	Šarlota	k1gFnSc1
Amálie	Amálie	k1gFnSc2
Hesensko-Philippsthalská	Hesensko-Philippsthalský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Vilém	Vilém	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Eisenašský	Sasko-Eisenašský	k2eAgInSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Sasko-Eisenašská	Sasko-Eisenašský	k2eAgFnSc1d1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Juliána	Juliána	k1gFnSc1
Bádensko-Durlašská	Bádensko-Durlašský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Frederick	Fredericka	k1gFnPc2
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
Duke	Duke	k1gFnPc2
of	of	k?
Saxe-Gotha-Altenburg	Saxe-Gotha-Altenburg	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Sasko-gothajsko-altenburský	Sasko-gothajsko-altenburský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
August	August	k1gMnSc1
</s>
<s>
1822	#num#	k4
<g/>
–	–	k?
<g/>
1825	#num#	k4
Fridrich	Fridrich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Gothajsko-Altenburský	Sasko-Gothajsko-Altenburský	k2eAgInSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
<g/>
Fridrich	Fridrich	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
136026257	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2018016686	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
80440214	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2018016686	#num#	k4
</s>
