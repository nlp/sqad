<s>
Uvolněte	uvolnit	k5eAaPmRp2nP	uvolnit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
pořadu	pořad	k1gInSc2	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
moderoval	moderovat	k5eAaBmAgMnS	moderovat
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Vysílal	vysílat	k5eAaImAgInS	vysílat
se	se	k3xPyFc4	se
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
každý	každý	k3xTgInSc4	každý
páteční	páteční	k2eAgInSc4d1	páteční
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
Ponec	Ponec	k1gMnSc1	Ponec
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
zhlédávali	zhlédávat	k5eAaPmAgMnP	zhlédávat
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k6eAd1	půl
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
talk	talk	k1gInSc4	talk
show	show	k1gFnSc2	show
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dva	dva	k4xCgMnPc1	dva
hosté	host	k1gMnPc1	host
byli	být	k5eAaImAgMnP	být
zpravidla	zpravidla	k6eAd1	zpravidla
veřejně	veřejně	k6eAd1	veřejně
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
host	host	k1gMnSc1	host
byl	být	k5eAaImAgMnS	být
většinou	většina	k1gFnSc7	většina
zajímavý	zajímavý	k2eAgMnSc1d1	zajímavý
či	či	k8xC	či
něčím	něco	k3yInSc7	něco
výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
člověk	člověk	k1gMnSc1	člověk
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
již	již	k9	již
jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
100	[number]	k4	100
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
tohoto	tento	k3xDgInSc2	tento
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
prázdninách	prázdniny	k1gFnPc6	prázdniny
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vysílají	vysílat	k5eAaImIp3nP	vysílat
reprízy	repríza	k1gFnPc4	repríza
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Nesestříhané	sestříhaný	k2eNgFnPc1d1	nesestříhaná
verze	verze	k1gFnPc1	verze
pořadů	pořad	k1gInPc2	pořad
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
prodávají	prodávat	k5eAaImIp3nP	prodávat
i	i	k9	i
na	na	k7c6	na
DVD	DVD	kA	DVD
nosičích	nosič	k1gInPc6	nosič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
pořad	pořad	k1gInSc1	pořad
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Kateřina	Kateřina	k1gFnSc1	Kateřina
Jacques	Jacques	k1gMnSc1	Jacques
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
biomasa	biomasa	k1gFnSc1	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
pak	pak	k6eAd1	pak
ve	v	k7c6	v
volební	volební	k2eAgFnSc6d1	volební
kampani	kampaň	k1gFnSc6	kampaň
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
neznalost	neznalost	k1gFnSc4	neznalost
poukazovala	poukazovat	k5eAaImAgFnS	poukazovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
volební	volební	k2eAgInPc4d1	volební
plakáty	plakát	k1gInPc4	plakát
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
slogan	slogan	k1gInSc4	slogan
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
biomasa	biomasa	k1gFnSc1	biomasa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
odvysíláno	odvysílán	k2eAgNnSc1d1	odvysíláno
dvousté	dvoustý	k2eAgNnSc1d1	dvoustý
pokračování	pokračování	k1gNnSc1	pokračování
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
konec	konec	k1gInSc4	konec
svého	svůj	k3xOyFgInSc2	svůj
pořadu	pořad	k1gInSc2	pořad
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hostem	host	k1gMnSc7	host
byl	být	k5eAaImAgMnS	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
nový	nový	k2eAgInSc4d1	nový
pořad	pořad	k1gInSc4	pořad
Show	show	k1gFnSc2	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
na	na	k7c6	na
Primě	prima	k1gFnSc6	prima
<g/>
.	.	kIx.	.
</s>
