<p>
<s>
Kuna	Kuna	k1gMnSc1	Kuna
skalní	skalní	k2eAgMnSc1d1	skalní
(	(	kIx(	(
<g/>
Martes	Martes	k1gMnSc1	Martes
foina	foina	k6eAd1	foina
Erxleben	Erxleben	k2eAgMnSc1d1	Erxleben
<g/>
,	,	kIx,	,
1777	[number]	k4	1777
<g/>
)	)	kIx)	)
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
kuna	kuna	k1gFnSc1	kuna
domácí	domácí	k2eAgFnSc1d1	domácí
je	být	k5eAaImIp3nS	být
šelma	šelma	k1gFnSc1	šelma
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgMnPc2d1	lasicovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
štíhlé	štíhlý	k2eAgNnSc1d1	štíhlé
tělo	tělo	k1gNnSc1	tělo
s	s	k7c7	s
hedvábnou	hedvábný	k2eAgFnSc7d1	hedvábná
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
náprsenkou	náprsenka	k1gFnSc7	náprsenka
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
30-50	[number]	k4	30-50
cm	cm	kA	cm
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ocas	ocas	k1gInSc1	ocas
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
a	a	k8xC	a
váhy	váha	k1gFnSc2	váha
až	až	k9	až
2,3	[number]	k4	2,3
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	s	k7c7	s
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Polštářky	polštářek	k1gInPc1	polštářek
na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
nemá	mít	k5eNaImIp3nS	mít
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
srstí	srst	k1gFnSc7	srst
<g/>
.	.	kIx.	.
</s>
<s>
Výborně	výborně	k6eAd1	výborně
šplhá	šplhat	k5eAaImIp3nS	šplhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Oblíbenými	oblíbený	k2eAgNnPc7d1	oblíbené
stanovišti	stanoviště	k1gNnPc7	stanoviště
jsou	být	k5eAaImIp3nP	být
zarostlá	zarostlý	k2eAgNnPc4d1	zarostlé
skalnatá	skalnatý	k2eAgNnPc4d1	skalnaté
prostranství	prostranství	k1gNnPc4	prostranství
<g/>
,	,	kIx,	,
ruiny	ruina	k1gFnSc2	ruina
či	či	k8xC	či
lidmi	člověk	k1gMnPc7	člověk
řídce	řídce	k6eAd1	řídce
navštěvované	navštěvovaný	k2eAgFnSc2d1	navštěvovaná
stavby	stavba	k1gFnSc2	stavba
(	(	kIx(	(
<g/>
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnSc2	zahrada
či	či	k8xC	či
stavby	stavba	k1gFnSc2	stavba
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
chat	chata	k1gFnPc2	chata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
žije	žít	k5eAaImIp3nS	žít
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lidských	lidský	k2eAgFnPc2d1	lidská
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
včetně	včetně	k7c2	včetně
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
obývá	obývat	k5eAaImIp3nS	obývat
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
šachty	šachta	k1gFnPc4	šachta
a	a	k8xC	a
podobné	podobný	k2eAgInPc4d1	podobný
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
potkany	potkan	k1gMnPc7	potkan
<g/>
,	,	kIx,	,
zdivočelými	zdivočelý	k2eAgMnPc7d1	zdivočelý
holuby	holub	k1gMnPc7	holub
i	i	k9	i
odpadky	odpadek	k1gInPc4	odpadek
z	z	k7c2	z
popelnic	popelnice	k1gFnPc2	popelnice
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
také	také	k9	také
návštěvami	návštěva	k1gFnPc7	návštěva
chat	chata	k1gFnPc2	chata
a	a	k8xC	a
chalup	chalupa	k1gFnPc2	chalupa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozkousává	rozkousávat	k5eAaImIp3nS	rozkousávat
izolace	izolace	k1gFnSc1	izolace
<g/>
,	,	kIx,	,
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
trus	trus	k1gInSc4	trus
i	i	k8xC	i
zbytky	zbytek	k1gInPc4	zbytek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
lidem	člověk	k1gMnPc3	člověk
mohou	moct	k5eAaImIp3nP	moct
vadit	vadit	k5eAaImF	vadit
také	také	k9	také
hlučné	hlučný	k2eAgFnPc4d1	hlučná
aktivity	aktivita	k1gFnPc4	aktivita
kun	kuna	k1gFnPc2	kuna
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
návštěvami	návštěva	k1gFnPc7	návštěva
zaparkovaných	zaparkovaný	k2eAgInPc2d1	zaparkovaný
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
technické	technický	k2eAgInPc4d1	technický
poruchy	poruch	k1gInPc4	poruch
přehryzáním	přehryzání	k1gNnSc7	přehryzání
kabelů	kabel	k1gInPc2	kabel
a	a	k8xC	a
hadiček	hadička	k1gFnPc2	hadička
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
lese	les	k1gInSc6	les
a	a	k8xC	a
šplhá	šplhat	k5eAaImIp3nS	šplhat
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vzácné	vzácný	k2eAgInPc4d1	vzácný
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
její	její	k3xOp3gNnSc1	její
příbuzné	příbuzný	k2eAgNnSc1d1	příbuzné
kuně	kuně	k1gNnSc1	kuně
lesní	lesní	k2eAgNnSc1d1	lesní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Kuna	Kuna	k1gMnSc1	Kuna
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
jídelníčku	jídelníček	k1gInSc2	jídelníček
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
složku	složka	k1gFnSc4	složka
její	její	k3xOp3gFnSc2	její
potravy	potrava	k1gFnSc2	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
ptáci	pták	k1gMnPc1	pták
(	(	kIx(	(
<g/>
především	především	k9	především
pěvci	pěvec	k1gMnPc1	pěvec
a	a	k8xC	a
hrabaví	hrabavý	k2eAgMnPc1d1	hrabavý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
dospělí	dospělí	k1gMnPc1	dospělí
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vejce	vejce	k1gNnPc1	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Kuna	kuna	k1gFnSc1	kuna
systematicky	systematicky	k6eAd1	systematicky
plení	plenit	k5eAaImIp3nS	plenit
ptačí	ptačí	k2eAgNnPc4d1	ptačí
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
počíhá	počíhat	k5eAaPmIp3nS	počíhat
i	i	k9	i
na	na	k7c4	na
dospělé	dospělý	k2eAgMnPc4d1	dospělý
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
obávané	obávaný	k2eAgMnPc4d1	obávaný
návštěvníky	návštěvník	k1gMnPc4	návštěvník
kurníků	kurník	k1gInPc2	kurník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
zakousne	zakousnout	k5eAaPmIp3nS	zakousnout
všechny	všechen	k3xTgFnPc4	všechen
slepice	slepice	k1gFnPc4	slepice
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
sežrala	sežrat	k5eAaPmAgFnS	sežrat
či	či	k8xC	či
vysála	vysát	k5eAaPmAgFnS	vysát
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tvor	tvor	k1gMnSc1	tvor
aktivní	aktivní	k2eAgMnSc1d1	aktivní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
samotářský	samotářský	k2eAgMnSc1d1	samotářský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kuna	kuna	k1gFnSc1	kuna
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
asi	asi	k9	asi
devět	devět	k4xCc1	devět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
dvě	dva	k4xCgFnPc4	dva
až	až	k8xS	až
pět	pět	k4xCc4	pět
slepých	slepý	k2eAgNnPc2d1	slepé
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
otevírají	otevírat	k5eAaImIp3nP	otevírat
oči	oko	k1gNnPc1	oko
asi	asi	k9	asi
kolem	kolem	k7c2	kolem
pátého	pátý	k4xOgInSc2	pátý
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
však	však	k9	však
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
lovena	lovit	k5eAaImNgFnS	lovit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
hájená	hájený	k2eAgFnSc1d1	hájená
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
chovu	chov	k1gInSc2	chov
tetřeva	tetřev	k1gMnSc2	tetřev
<g/>
,	,	kIx,	,
tetřívka	tetřívek	k1gMnSc2	tetřívek
<g/>
,	,	kIx,	,
jeřábka	jeřábek	k1gMnSc2	jeřábek
a	a	k8xC	a
koroptve	koroptev	k1gFnPc1	koroptev
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nP	smět
lovit	lovit	k5eAaImF	lovit
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnPc4d1	skalní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
www.priroda.cz	www.priroda.cz	k1gInSc4	www.priroda.cz
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
biolibu	bioliba	k1gFnSc4	bioliba
</s>
</p>
<p>
<s>
Informace	informace	k1gFnSc1	informace
a	a	k8xC	a
fotky	fotka	k1gFnSc2	fotka
kuny	kuna	k1gFnPc4	kuna
skalní	skalní	k2eAgFnPc4d1	skalní
na	na	k7c6	na
webu	web	k1gInSc6	web
Kunalesni	Kunalesni	k1gFnSc2	Kunalesni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
