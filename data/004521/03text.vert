<s>
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
(	(	kIx(	(
<g/>
zkracována	zkracován	k2eAgFnSc1d1	zkracována
na	na	k7c4	na
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
webový	webový	k2eAgInSc4d1	webový
projekt	projekt	k1gInSc4	projekt
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xC	jako
databáze	databáze	k1gFnSc2	databáze
filmů	film	k1gInPc2	film
a	a	k8xC	a
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
síť	síť	k1gFnSc4	síť
pro	pro	k7c4	pro
filmové	filmový	k2eAgMnPc4d1	filmový
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
příspěvky	příspěvek	k1gInPc1	příspěvek
–	–	k?	–
především	především	k6eAd1	především
hodnocení	hodnocení	k1gNnSc1	hodnocení
a	a	k8xC	a
recenze	recenze	k1gFnPc1	recenze
filmů	film	k1gInPc2	film
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgInPc1d1	veřejný
a	a	k8xC	a
z	z	k7c2	z
obsáhlé	obsáhlý	k2eAgFnSc2d1	obsáhlá
databáze	databáze	k1gFnSc2	databáze
filmů	film	k1gInPc2	film
tvoří	tvořit	k5eAaImIp3nP	tvořit
mapu	mapa	k1gFnSc4	mapa
jejich	jejich	k3xOp3gFnSc2	jejich
kvality	kvalita	k1gFnSc2	kvalita
napříč	napříč	k7c7	napříč
žánry	žánr	k1gInPc7	žánr
a	a	k8xC	a
historickými	historický	k2eAgFnPc7d1	historická
etapami	etapa	k1gFnPc7	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Databázi	databáze	k1gFnSc4	databáze
provozuje	provozovat	k5eAaImIp3nS	provozovat
firma	firma	k1gFnSc1	firma
POMO	POMO	kA	POMO
Media	medium	k1gNnSc2	medium
Group	Group	k1gMnSc1	Group
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Martin	Martin	k1gInSc1	Martin
Pomothy	Pomotha	k1gFnSc2	Pomotha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
IMDb	IMDba	k1gFnPc2	IMDba
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
kromě	kromě	k7c2	kromě
lepšího	dobrý	k2eAgNnSc2d2	lepší
pokrytí	pokrytí	k1gNnSc2	pokrytí
lokální	lokální	k2eAgFnSc2d1	lokální
scény	scéna	k1gFnSc2	scéna
liší	lišit	k5eAaImIp3nS	lišit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatelům	uživatel	k1gMnPc3	uživatel
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vést	vést	k5eAaImF	vést
si	se	k3xPyFc3	se
přehled	přehled	k1gInSc4	přehled
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
filmotéky	filmotéka	k1gFnSc2	filmotéka
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
program	program	k1gInSc4	program
všech	všecek	k3xTgNnPc2	všecek
českých	český	k2eAgNnPc2d1	české
a	a	k8xC	a
slovenských	slovenský	k2eAgNnPc2d1	slovenské
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc1	přehled
filmů	film	k1gInPc2	film
vycházejících	vycházející	k2eAgInPc2d1	vycházející
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
Blu-ray	Bluaa	k1gFnSc2	Blu-raa
a	a	k8xC	a
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
taky	taky	k9	taky
program	program	k1gInSc1	program
desítek	desítka	k1gFnPc2	desítka
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
IMDb	IMDb	k1gMnSc1	IMDb
celkově	celkově	k6eAd1	celkově
naopak	naopak	k6eAd1	naopak
uvádí	uvádět	k5eAaImIp3nS	uvádět
více	hodně	k6eAd2	hodně
detailů	detail	k1gInPc2	detail
o	o	k7c6	o
audiovizuálním	audiovizuální	k2eAgNnSc6d1	audiovizuální
díle	dílo	k1gNnSc6	dílo
než	než	k8xS	než
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kompletní	kompletní	k2eAgFnSc4d1	kompletní
titulkovou	titulkový	k2eAgFnSc4d1	titulková
listinu	listina	k1gFnSc4	listina
tvůrců	tvůrce	k1gMnPc2	tvůrce
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
změnil	změnit	k5eAaPmAgInS	změnit
server	server	k1gInSc1	server
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
<g/>
×	×	k?	×
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
mediálním	mediální	k2eAgMnSc7d1	mediální
partnerem	partner	k1gMnSc7	partner
různých	různý	k2eAgFnPc2d1	různá
filmových	filmový	k2eAgFnPc2d1	filmová
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
kiny	kino	k1gNnPc7	kino
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
a	a	k8xC	a
VOD	voda	k1gFnPc2	voda
distributory	distributor	k1gMnPc4	distributor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zájmové	zájmový	k2eAgInPc4d1	zájmový
weby	web	k1gInPc4	web
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
All	All	k1gFnSc2	All
Star	Star	kA	Star
obsadila	obsadit	k5eAaPmAgFnS	obsadit
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
za	za	k7c4	za
giganty	gigant	k1gMnPc4	gigant
Google	Google	k1gInSc1	Google
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
iDnes	iDnes	k1gInSc1	iDnes
<g/>
.	.	kIx.	.
</s>
<s>
Profily	profil	k1gInPc1	profil
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
tvůrců	tvůrce	k1gMnPc2	tvůrce
a	a	k8xC	a
herců	herec	k1gMnPc2	herec
Televizní	televizní	k2eAgInSc4d1	televizní
program	program	k1gInSc4	program
Přehled	přehled	k1gInSc1	přehled
Kino	kino	k1gNnSc1	kino
premiér	premiéra	k1gFnPc2	premiéra
Přehled	přehled	k1gInSc4	přehled
DVD	DVD	kA	DVD
premiér	premiéra	k1gFnPc2	premiéra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
oddělené	oddělený	k2eAgFnSc2d1	oddělená
sekce	sekce	k1gFnSc2	sekce
pro	pro	k7c4	pro
levná	levný	k2eAgFnSc1d1	levná
příbalová	příbalová	k1gFnSc1	příbalová
DVD	DVD	kA	DVD
Filmové	filmový	k2eAgFnSc2d1	filmová
diskuze	diskuze	k1gFnSc2	diskuze
Statistiky	statistika	k1gFnSc2	statistika
a	a	k8xC	a
žebříčky	žebříček	k1gInPc4	žebříček
filmů	film	k1gInPc2	film
Uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
hodnotit	hodnotit	k5eAaImF	hodnotit
a	a	k8xC	a
komentovat	komentovat	k5eAaBmF	komentovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
mohou	moct	k5eAaImIp3nP	moct
navrhovat	navrhovat	k5eAaImF	navrhovat
různé	různý	k2eAgFnPc4d1	různá
zajímavosti	zajímavost	k1gFnPc4	zajímavost
či	či	k8xC	či
úpravy	úprava	k1gFnPc4	úprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nP	muset
před	před	k7c7	před
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
schválit	schválit	k5eAaPmF	schválit
administrátoři	administrátor	k1gMnPc1	administrátor
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mohou	moct	k5eAaImIp3nP	moct
uživatelé	uživatel	k1gMnPc1	uživatel
vést	vést	k5eAaImF	vést
svoji	svůj	k3xOyFgFnSc4	svůj
videotéku	videotéka	k1gFnSc4	videotéka
–	–	k?	–
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
BR	br	k0	br
psát	psát	k5eAaImF	psát
si	se	k3xPyFc3	se
deníček	deníček	k1gInSc4	deníček
nastavit	nastavit	k5eAaPmF	nastavit
si	se	k3xPyFc3	se
budíček	budíček	k1gInSc4	budíček
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Chci	chtít	k5eAaImIp1nS	chtít
vidět	vidět	k5eAaImF	vidět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
nabízet	nabízet	k5eAaImF	nabízet
či	či	k8xC	či
shánět	shánět	k5eAaImF	shánět
filmy	film	k1gInPc4	film
v	v	k7c6	v
bazaru	bazar	k1gInSc6	bazar
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
uživatelský	uživatelský	k2eAgInSc4d1	uživatelský
profil	profil	k1gInSc4	profil
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
nebe	nebe	k1gNnSc2	nebe
Filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
Filmový	filmový	k2eAgInSc1d1	filmový
přehled	přehled	k1gInSc4	přehled
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
