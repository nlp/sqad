<s>
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
(	(	kIx(	(
<g/>
zkracována	zkracován	k2eAgFnSc1d1	zkracována
na	na	k7c4	na
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
webový	webový	k2eAgInSc4d1	webový
projekt	projekt	k1gInSc4	projekt
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xC	jako
databáze	databáze	k1gFnSc2	databáze
filmů	film	k1gInPc2	film
a	a	k8xC	a
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
síť	síť	k1gFnSc4	síť
pro	pro	k7c4	pro
filmové	filmový	k2eAgMnPc4d1	filmový
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
příspěvky	příspěvek	k1gInPc1	příspěvek
–	–	k?	–
především	především	k6eAd1	především
hodnocení	hodnocení	k1gNnSc1	hodnocení
a	a	k8xC	a
recenze	recenze	k1gFnPc1	recenze
filmů	film	k1gInPc2	film
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgInPc1d1	veřejný
a	a	k8xC	a
z	z	k7c2	z
obsáhlé	obsáhlý	k2eAgFnSc2d1	obsáhlá
databáze	databáze	k1gFnSc2	databáze
filmů	film	k1gInPc2	film
tvoří	tvořit	k5eAaImIp3nP	tvořit
mapu	mapa	k1gFnSc4	mapa
jejich	jejich	k3xOp3gFnSc2	jejich
kvality	kvalita	k1gFnSc2	kvalita
napříč	napříč	k7c7	napříč
žánry	žánr	k1gInPc7	žánr
a	a	k8xC	a
historickými	historický	k2eAgFnPc7d1	historická
etapami	etapa	k1gFnPc7	etapa
<g/>
.	.	kIx.	.
</s>
