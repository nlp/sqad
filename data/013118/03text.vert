<p>
<s>
Radost	radost	k1gFnSc1	radost
je	být	k5eAaImIp3nS	být
příjemná	příjemný	k2eAgFnSc1d1	příjemná
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc1d1	vznikající
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
či	či	k8xC	či
zisk	zisk	k1gInSc4	zisk
u	u	k7c2	u
jednodušších	jednoduchý	k2eAgInPc2d2	jednodušší
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnSc7	emoce
schopných	schopný	k2eAgMnPc2d1	schopný
tvorů	tvor	k1gMnPc2	tvor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
prožitek	prožitek	k1gInSc4	prožitek
čisté	čistý	k2eAgFnSc2d1	čistá
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slabší	slabý	k2eAgFnSc7d2	slabší
formou	forma	k1gFnSc7	forma
radosti	radost	k1gFnSc2	radost
je	být	k5eAaImIp3nS	být
spokojenost	spokojenost	k1gFnSc1	spokojenost
<g/>
,	,	kIx,	,
silnější	silný	k2eAgFnSc1d2	silnější
je	být	k5eAaImIp3nS	být
extáze	extáze	k1gFnSc1	extáze
<g/>
.	.	kIx.	.
</s>
<s>
Trvalejší	trvalý	k2eAgFnSc1d2	trvalejší
a	a	k8xC	a
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
forma	forma	k1gFnSc1	forma
radosti	radost	k1gFnSc2	radost
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Radost	radost	k1gFnSc1	radost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
úsměvem	úsměv	k1gInSc7	úsměv
nebo	nebo	k8xC	nebo
smíchem	smích	k1gInSc7	smích
<g/>
.	.	kIx.	.
</s>
<s>
Tělesnými	tělesný	k2eAgInPc7d1	tělesný
projevy	projev	k1gInPc7	projev
radosti	radost	k1gFnSc2	radost
je	být	k5eAaImIp3nS	být
zvýšení	zvýšení	k1gNnSc1	zvýšení
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
a	a	k8xC	a
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Religionista	Religionista	k1gMnSc1	Religionista
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
radost	radost	k1gFnSc1	radost
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
proměněné	proměněný	k2eAgFnSc2d1	proměněná
bolesti	bolest	k1gFnSc2	bolest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opakem	opak	k1gInSc7	opak
radosti	radost	k1gFnSc2	radost
je	být	k5eAaImIp3nS	být
smutek	smutek	k1gInSc1	smutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
</s>
</p>
<p>
<s>
Smích	smích	k1gInSc1	smích
</s>
</p>
<p>
<s>
Úsměv	úsměv	k1gInSc1	úsměv
</s>
</p>
<p>
<s>
Emoce	emoce	k1gFnSc1	emoce
</s>
</p>
<p>
<s>
Naděje	naděje	k1gFnSc1	naděje
</s>
</p>
<p>
<s>
Sangvinik	sangvinik	k1gMnSc1	sangvinik
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
radost	radost	k6eAd1	radost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
radost	radost	k1gFnSc1	radost
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
radost	radost	k1gFnSc1	radost
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
NAKONEČNÝ	NAKONEČNÝ	kA	NAKONEČNÝ
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
emoce	emoce	k1gFnPc1	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
763	[number]	k4	763
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
s.	s.	k?	s.
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc1d1	fantastický
a	a	k8xC	a
magické	magický	k2eAgInPc1d1	magický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7136	[number]	k4	7136
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
</p>
