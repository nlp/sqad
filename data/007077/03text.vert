<s>
Reliéf	reliéf	k1gInSc1	reliéf
v	v	k7c6	v
geografii	geografie	k1gFnSc6	geografie
<g/>
,	,	kIx,	,
též	též	k9	též
georeliéf	georeliéf	k1gInSc1	georeliéf
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc4d1	odborný
pojem	pojem	k1gInSc4	pojem
geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
geografických	geografický	k2eAgFnPc2d1	geografická
disciplín	disciplína	k1gFnPc2	disciplína
ve	v	k7c6	v
věcném	věcný	k2eAgInSc6d1	věcný
významu	význam	k1gInSc6	význam
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
"	"	kIx"	"
<g/>
tvářnost	tvářnost	k1gFnSc1	tvářnost
neboli	neboli	k8xC	neboli
vzhled	vzhled	k1gInSc1	vzhled
povrchu	povrch	k1gInSc2	povrch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
reliéf	reliéf	k1gInSc1	reliéf
<g/>
)	)	kIx)	)
Země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
užití	užití	k1gNnSc2	užití
složeného	složený	k2eAgNnSc2d1	složené
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
georeliéf	georeliéf	k1gInSc1	georeliéf
<g/>
)	)	kIx)	)
s	s	k7c7	s
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
významem	význam	k1gInSc7	význam
spojeným	spojený	k2eAgInSc7d1	spojený
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
částí	část	k1gFnSc7	část
"	"	kIx"	"
<g/>
geo-	geo-	k?	geo-
<g/>
"	"	kIx"	"
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
litosféry	litosféra	k1gFnSc2	litosféra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
georeliéf	georeliéf	k1gInSc1	georeliéf
svrchní	svrchní	k2eAgFnSc1d1	svrchní
plocha	plocha	k1gFnSc1	plocha
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
oddělující	oddělující	k2eAgFnSc4d1	oddělující
pevnou	pevný	k2eAgFnSc4d1	pevná
a	a	k8xC	a
tekutou	tekutý	k2eAgFnSc4d1	tekutá
část	část	k1gFnSc4	část
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
částmi	část	k1gFnPc7	část
plochou	plocha	k1gFnSc7	plocha
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
<g/>
.	.	kIx.	.
</s>
<s>
Pevnou	pevný	k2eAgFnSc7d1	pevná
částí	část	k1gFnSc7	část
Země	zem	k1gFnSc2	zem
tvořící	tvořící	k2eAgInSc4d1	tvořící
její	její	k3xOp3gInSc4	její
reliéf	reliéf	k1gInSc4	reliéf
jsou	být	k5eAaImIp3nP	být
pevniny	pevnina	k1gFnPc1	pevnina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
souše	souš	k1gFnSc2	souš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pedosféry	pedosféra	k1gFnSc2	pedosféra
a	a	k8xC	a
dna	dno	k1gNnSc2	dno
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
také	také	k9	také
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
tekutou	tekutý	k2eAgFnSc7d1	tekutá
částí	část	k1gFnSc7	část
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
povrchová	povrchový	k2eAgFnSc1d1	povrchová
voda	voda	k1gFnSc1	voda
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
a	a	k8xC	a
také	také	k9	také
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Georeliéf	Georeliéf	k1gInSc1	Georeliéf
není	být	k5eNaImIp3nS	být
neměnný	měnný	k2eNgInSc1d1	neměnný
<g/>
,	,	kIx,	,
podléhá	podléhat	k5eAaImIp3nS	podléhat
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
procesů	proces	k1gInPc2	proces
působících	působící	k2eAgInPc2d1	působící
na	na	k7c4	na
svrchní	svrchní	k2eAgFnSc4d1	svrchní
plochu	plocha	k1gFnSc4	plocha
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
přesunu	přesun	k1gInSc2	přesun
hornin	hornina	k1gFnPc2	hornina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přírodních	přírodní	k2eAgInPc2d1	přírodní
nebo	nebo	k8xC	nebo
antropogenních	antropogenní	k2eAgInPc2d1	antropogenní
vlivů	vliv	k1gInPc2	vliv
i	i	k8xC	i
dalších	další	k2eAgFnPc2d1	další
změn	změna	k1gFnPc2	změna
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
činiteli	činitel	k1gInPc7	činitel
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
s	s	k7c7	s
sebou	se	k3xPyFc7	se
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
z	z	k7c2	z
vnitra	vnitro	k1gNnSc2	vnitro
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
magma	magma	k1gNnSc1	magma
<g/>
)	)	kIx)	)
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
pochodech	pochod	k1gInPc6	pochod
reliéf	reliéf	k1gInSc1	reliéf
zpravidla	zpravidla	k6eAd1	zpravidla
rozčleňující	rozčleňující	k2eAgMnSc1d1	rozčleňující
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
z	z	k7c2	z
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc1	proudění
vzduchu	vzduch	k1gInSc2	vzduch
–	–	k?	–
vítr	vítr	k1gInSc1	vítr
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
působení	působení	k1gNnSc6	působení
energie	energie	k1gFnSc2	energie
reliéf	reliéf	k1gInSc4	reliéf
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zarovnávající	zarovnávající	k2eAgFnSc1d1	zarovnávající
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
různých	různý	k2eAgFnPc2d1	různá
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
uskupených	uskupený	k2eAgMnPc2d1	uskupený
do	do	k7c2	do
tvarů	tvar	k1gInPc2	tvar
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnPc1d1	lišící
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
rozměry	rozměr	k1gInPc7	rozměr
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
georeliéf	georeliéf	k1gInSc4	georeliéf
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
komplexu	komplex	k1gInSc6	komplex
značně	značně	k6eAd1	značně
složitý	složitý	k2eAgInSc1d1	složitý
a	a	k8xC	a
v	v	k7c6	v
čase	čas	k1gInSc6	čas
proměnný	proměnný	k2eAgInSc1d1	proměnný
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
horninotvorného	horninotvorný	k2eAgInSc2d1	horninotvorný
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
procesy	proces	k1gInPc7	proces
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
geologickou	geologický	k2eAgFnSc7d1	geologická
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
omezeného	omezený	k2eAgNnSc2d1	omezené
území	území	k1gNnSc2	území
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
lze	lze	k6eAd1	lze
georeliéf	georeliéf	k1gInSc4	georeliéf
typologicky	typologicky	k6eAd1	typologicky
rozlišit	rozlišit	k5eAaPmF	rozlišit
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
lokalizace	lokalizace	k1gFnSc2	lokalizace
zeměpisnými	zeměpisný	k2eAgFnPc7d1	zeměpisná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
)	)	kIx)	)
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
–	–	k?	–
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
m	m	kA	m
<g/>
,	,	kIx,	,
výšková	výškový	k2eAgFnSc1d1	výšková
členitost	členitost	k1gFnSc1	členitost
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysočiny	vysočina	k1gFnSc2	vysočina
(	(	kIx(	(
<g/>
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
nad	nad	k7c7	nad
300	[number]	k4	300
m	m	kA	m
<g/>
,	,	kIx,	,
výšková	výškový	k2eAgFnSc1d1	výšková
členitost	členitost	k1gFnSc1	členitost
nad	nad	k7c7	nad
30	[number]	k4	30
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
blízkostí	blízkost	k1gFnSc7	blízkost
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
akumulační	akumulační	k2eAgFnSc2d1	akumulační
roviny	rovina	k1gFnSc2	rovina
s	s	k7c7	s
nezpevněným	zpevněný	k2eNgNnSc7d1	nezpevněné
podložím	podloží	k1gNnSc7	podloží
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
přesunem	přesun	k1gInSc7	přesun
hmotných	hmotný	k2eAgFnPc2d1	hmotná
částic	částice	k1gFnPc2	částice
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
odnosem	odnos	k1gInSc7	odnos
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
jezerní	jezerní	k2eAgFnSc1d1	jezerní
<g/>
,	,	kIx,	,
říční	říční	k2eAgFnSc1d1	říční
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naváté	navátý	k2eAgFnPc4d1	navátá
větrem	vítr	k1gInSc7	vítr
(	(	kIx(	(
<g/>
eolické	eolický	k2eAgNnSc1d1	eolický
<g/>
,	,	kIx,	,
např.	např.	kA	např.
písečná	písečný	k2eAgFnSc1d1	písečná
duna	duna	k1gFnSc1	duna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysočiny	vysočina	k1gFnPc1	vysočina
jsou	být	k5eAaImIp3nP	být
obecným	obecný	k2eAgNnSc7d1	obecné
označením	označení	k1gNnSc7	označení
členitého	členitý	k2eAgInSc2d1	členitý
georeliéfu	georeliéf	k1gInSc2	georeliéf
<g/>
,	,	kIx,	,
morfometricky	morfometricky	k6eAd1	morfometricky
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
relativní	relativní	k2eAgFnSc2d1	relativní
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
sníženiny	sníženina	k1gFnPc1	sníženina
<g/>
,	,	kIx,	,
pahorkatiny	pahorkatina	k1gFnPc1	pahorkatina
<g/>
,	,	kIx,	,
vrchoviny	vrchovina	k1gFnPc1	vrchovina
<g/>
,	,	kIx,	,
hornatiny	hornatina	k1gFnPc1	hornatina
a	a	k8xC	a
velehornatiny	velehornatina	k1gFnPc1	velehornatina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
(	(	kIx(	(
<g/>
typologicky	typologicky	k6eAd1	typologicky
<g/>
)	)	kIx)	)
forma	forma	k1gFnSc1	forma
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
nepřesahující	přesahující	k2eNgFnSc2d1	nepřesahující
30	[number]	k4	30
m	m	kA	m
svoji	svůj	k3xOyFgFnSc4	svůj
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výškové	výškový	k2eAgFnSc2d1	výšková
členitosti	členitost	k1gFnSc2	členitost
se	se	k3xPyFc4	se
pahorkatiny	pahorkatina	k1gFnPc1	pahorkatina
<g/>
,	,	kIx,	,
vrchoviny	vrchovina	k1gFnPc1	vrchovina
a	a	k8xC	a
hornatiny	hornatina	k1gFnPc1	hornatina
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nP	členit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
podtypy	podtyp	k1gInPc1	podtyp
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgFnPc1d1	plochá
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnPc1d2	nižší
hodnoty	hodnota	k1gFnPc1	hodnota
relativní	relativní	k2eAgFnSc2d1	relativní
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
a	a	k8xC	a
členité	členitý	k2eAgNnSc1d1	členité
(	(	kIx(	(
<g/>
s	s	k7c7	s
vyššími	vysoký	k2eAgFnPc7d2	vyšší
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tektonických	tektonický	k2eAgFnPc2d1	tektonická
struktur	struktura	k1gFnPc2	struktura
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
georeliéf	georeliéf	k1gInSc1	georeliéf
rozčleněn	rozčlenit	k5eAaPmNgInS	rozčlenit
plošně	plošně	k6eAd1	plošně
do	do	k7c2	do
územních	územní	k2eAgFnPc2d1	územní
geomorfologických	geomorfologický	k2eAgFnPc2d1	geomorfologická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
řádově	řádově	k6eAd1	řádově
odlišných	odlišný	k2eAgInPc2d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Řádově	řádově	k6eAd1	řádově
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
tzv.	tzv.	kA	tzv.
Kaledonská	Kaledonský	k2eAgFnSc1d1	Kaledonská
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
orientačně	orientačně	k6eAd1	orientačně
část	část	k1gFnSc4	část
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Britanie	Britanie	k1gFnSc2	Britanie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Baltský	baltský	k2eAgInSc1d1	baltský
štít	štít	k1gInSc1	štít
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Východoevropská	východoevropský	k2eAgFnSc1d1	východoevropská
platforma	platforma	k1gFnSc1	platforma
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Pobaltské	pobaltský	k2eAgNnSc1d1	pobaltské
<g />
.	.	kIx.	.
</s>
<s>
republiky	republika	k1gFnPc1	republika
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Alpsko-himalájská	alpskoimalájský	k2eAgFnSc1d1	alpsko-himalájský
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
státy	stát	k1gInPc1	stát
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
také	také	k9	také
část	část	k1gFnSc1	část
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
tzv.	tzv.	kA	tzv.
Hercynská	hercynský	k2eAgFnSc1d1	hercynská
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
větší	veliký	k2eAgFnPc1d2	veliký
části	část	k1gFnPc1	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
také	také	k9	také
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
členění	členění	k1gNnSc2	členění
georeliéfu	georeliéf	k1gInSc2	georeliéf
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
základních	základní	k2eAgFnPc2d1	základní
tektonických	tektonický	k2eAgFnPc2d1	tektonická
struktur	struktura	k1gFnPc2	struktura
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
spolupráci	spolupráce	k1gFnSc4	spolupráce
geografů	geograf	k1gMnPc2	geograf
<g/>
,	,	kIx,	,
geologů	geolog	k1gMnPc2	geolog
a	a	k8xC	a
geomorfologů	geomorfolog	k1gMnPc2	geomorfolog
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
soustavy	soustava	k1gFnPc1	soustava
specifické	specifický	k2eAgFnPc1d1	specifická
a	a	k8xC	a
vztažené	vztažený	k2eAgInPc1d1	vztažený
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
území	území	k1gNnSc3	území
s	s	k7c7	s
názvoslovím	názvosloví	k1gNnSc7	názvosloví
tvarů	tvar	k1gInPc2	tvar
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
vymezením	vymezení	k1gNnSc7	vymezení
zeměpisnými	zeměpisný	k2eAgFnPc7d1	zeměpisná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
v	v	k7c6	v
regionálním	regionální	k2eAgNnSc6d1	regionální
členění	členění	k1gNnSc6	členění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
fyzické	fyzický	k2eAgFnSc2d1	fyzická
geografie	geografie	k1gFnSc2	geografie
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
vývojem	vývoj	k1gInSc7	vývoj
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
reliéfem	reliéf	k1gInSc7	reliéf
<g/>
)	)	kIx)	)
zabývá	zabývat	k5eAaImIp3nS	zabývat
oborová	oborový	k2eAgFnSc1d1	oborová
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
jsou	být	k5eAaImIp3nP	být
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
georeliéfem	georeliéf	k1gInSc7	georeliéf
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
procesy	proces	k1gInPc7	proces
(	(	kIx(	(
<g/>
jevy	jev	k1gInPc7	jev
<g/>
)	)	kIx)	)
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
utvářením	utváření	k1gNnSc7	utváření
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
označuje	označovat	k5eAaImIp3nS	označovat
reliéf	reliéf	k1gInSc4	reliéf
jako	jako	k8xS	jako
uskupení	uskupení	k1gNnSc4	uskupení
rozličných	rozličný	k2eAgInPc2d1	rozličný
tvarů	tvar	k1gInPc2	tvar
povrchu	povrch	k1gInSc2	povrch
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
vymezenému	vymezený	k2eAgNnSc3d1	vymezené
území	území	k1gNnSc3	území
určité	určitý	k2eAgFnSc2d1	určitá
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
georeliéfem	georeliéf	k1gInSc7	georeliéf
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
rozumí	rozumět	k5eAaImIp3nS	rozumět
tvar	tvar	k1gInSc4	tvar
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
půdního	půdní	k2eAgInSc2d1	půdní
obalu	obal	k1gInSc2	obal
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
pedosféry	pedosféra	k1gFnSc2	pedosféra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
,	,	kIx,	,
sklonem	sklon	k1gInSc7	sklon
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
útvarů	útvar	k1gInPc2	útvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orientací	orientace	k1gFnSc7	orientace
vůči	vůči	k7c3	vůči
světovým	světový	k2eAgFnPc3d1	světová
stranám	strana	k1gFnPc3	strana
a	a	k8xC	a
expozicí	expozice	k1gFnSc7	expozice
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc3	proudění
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
větru	vítr	k1gInSc2	vítr
<g/>
)	)	kIx)	)
a	a	k8xC	a
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
též	též	k9	též
dešti	dešť	k1gInSc3	dešť
<g/>
)	)	kIx)	)
důležitým	důležitý	k2eAgInSc7d1	důležitý
půdotvorným	půdotvorný	k2eAgInSc7d1	půdotvorný
činitelem	činitel	k1gInSc7	činitel
<g/>
,	,	kIx,	,
ovlivňujícím	ovlivňující	k2eAgMnSc7d1	ovlivňující
např.	např.	kA	např.
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
je	být	k5eAaImIp3nS	být
georeliéf	georeliéf	k1gMnSc1	georeliéf
významný	významný	k2eAgMnSc1d1	významný
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
projektování	projektování	k1gNnSc2	projektování
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
vytyčování	vytyčování	k1gNnPc2	vytyčování
tras	trasa	k1gFnPc2	trasa
dopravních	dopravní	k2eAgFnPc2d1	dopravní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Georeliéf	Georeliéf	k1gMnSc1	Georeliéf
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
složené	složený	k2eAgFnSc2d1	složená
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyku	jazyk	k1gInSc6	jazyk
s	s	k7c7	s
cizím	cizí	k2eAgInSc7d1	cizí
původem	původ	k1gInSc7	původ
a	a	k8xC	a
pravopisem	pravopis	k1gInSc7	pravopis
s	s	k7c7	s
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
e	e	k0	e
<g/>
"	"	kIx"	"
počeštěné	počeštěný	k2eAgNnSc4d1	počeštěné
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
geo-	geo-	k?	geo-
<g/>
"	"	kIx"	"
tvoří	tvořit	k5eAaImIp3nS	tvořit
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
odborných	odborný	k2eAgFnPc2d1	odborná
pojmenování	pojmenování	k1gNnSc4	pojmenování
(	(	kIx(	(
<g/>
-dezie	ezie	k1gFnSc1	-dezie
<g/>
,	,	kIx,	,
-fyzika	yzika	k1gFnSc1	-fyzika
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
-grafie	rafie	k1gFnSc5	-grafie
<g/>
,	,	kIx,	,
-logie	ogie	k1gFnSc5	-logie
<g/>
,	,	kIx,	,
-morfologie	orfologie	k1gFnSc5	-morfologie
<g/>
,	,	kIx,	,
-politika	olitik	k1gMnSc2	-politik
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
gaeo	gaeo	k6eAd1	gaeo
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
původně	původně	k6eAd1	původně
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
gaia	gaia	k6eAd1	gaia
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
reliéf	reliéf	k1gInSc1	reliéf
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
slovo	slovo	k1gNnSc1	slovo
vícevýznamové	vícevýznamový	k2eAgNnSc1d1	vícevýznamové
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italského	italský	k2eAgMnSc2d1	italský
"	"	kIx"	"
<g/>
relief	relief	k1gInSc1	relief
<g/>
"	"	kIx"	"
označující	označující	k2eAgInSc1d1	označující
plastický	plastický	k2eAgInSc1d1	plastický
(	(	kIx(	(
<g/>
vypouklý	vypouklý	k2eAgInSc1d1	vypouklý
<g/>
)	)	kIx)	)
obraz	obraz	k1gInSc1	obraz
vystupující	vystupující	k2eAgInSc1d1	vystupující
z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
oblasti	oblast	k1gFnSc6	oblast
zeměpisu	zeměpis	k1gInSc2	zeměpis
značí	značit	k5eAaImIp3nS	značit
tvářnost	tvářnost	k1gFnSc4	tvářnost
neboli	neboli	k8xC	neboli
vzhled	vzhled	k1gInSc4	vzhled
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
užití	užití	k1gNnSc2	užití
složeného	složený	k2eAgNnSc2d1	složené
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
georeliéf	georeliéf	k1gInSc1	georeliéf
<g/>
"	"	kIx"	"
s	s	k7c7	s
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Georeliéf	Georeliéf	k1gMnSc1	Georeliéf
je	být	k5eAaImIp3nS	být
nehmotný	hmotný	k2eNgMnSc1d1	nehmotný
<g/>
,	,	kIx,	,
hmotným	hmotný	k2eAgMnSc7d1	hmotný
nositelem	nositel	k1gMnSc7	nositel
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc1	vzhled
svrchního	svrchní	k2eAgInSc2d1	svrchní
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
hornin	hornina	k1gFnPc2	hornina
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
uložením	uložení	k1gNnSc7	uložení
<g/>
,	,	kIx,	,
stářím	stáří	k1gNnSc7	stáří
apod.	apod.	kA	apod.
Výsledný	výsledný	k2eAgInSc1d1	výsledný
tvar	tvar	k1gInSc4	tvar
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
působení	působení	k1gNnSc2	působení
endogenních	endogenní	k2eAgFnPc2d1	endogenní
(	(	kIx(	(
<g/>
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
<g/>
)	)	kIx)	)
a	a	k8xC	a
exogenních	exogenní	k2eAgInPc2d1	exogenní
(	(	kIx(	(
<g/>
vnějších	vnější	k2eAgInPc2d1	vnější
<g/>
)	)	kIx)	)
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
endogenní	endogenní	k2eAgInPc1d1	endogenní
pochody	pochod	k1gInPc1	pochod
vedou	vést	k5eAaImIp3nP	vést
zejména	zejména	k9	zejména
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
nerovností	nerovnost	k1gFnPc2	nerovnost
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
exogenní	exogenní	k2eAgInPc1d1	exogenní
pochody	pochod	k1gInPc1	pochod
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
zarovnávání	zarovnávání	k1gNnSc3	zarovnávání
povrchu	povrch	k1gInSc3	povrch
a	a	k8xC	a
zmenšování	zmenšování	k1gNnSc3	zmenšování
výškových	výškový	k2eAgInPc2d1	výškový
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
,	,	kIx,	,
popis	popis	k1gInSc1	popis
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
komplexnosti	komplexnost	k1gFnSc6	komplexnost
nesnadný	snadný	k2eNgInSc1d1	nesnadný
<g/>
.	.	kIx.	.
</s>
<s>
Členěn	členěn	k2eAgMnSc1d1	členěn
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
geometricky	geometricky	k6eAd1	geometricky
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
plochy	plocha	k1gFnPc4	plocha
navzájem	navzájem	k6eAd1	navzájem
oddělené	oddělený	k2eAgInPc1d1	oddělený
lomy	lom	k1gInPc1	lom
spádu	spád	k1gInSc2	spád
(	(	kIx(	(
<g/>
hranami	hrana	k1gFnPc7	hrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc4d1	označovaná
jako	jako	k8xS	jako
hřbetnice	hřbetnice	k1gFnPc4	hřbetnice
<g/>
,	,	kIx,	,
údolnice	údolnice	k1gFnPc4	údolnice
<g/>
,	,	kIx,	,
úpatnice	úpatnice	k1gFnPc4	úpatnice
či	či	k8xC	či
úpatí	úpatí	k1gNnPc4	úpatí
<g/>
,	,	kIx,	,
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
ostrý	ostrý	k2eAgInSc1d1	ostrý
lom	lom	k1gInSc1	lom
spádu	spád	k1gInSc2	spád
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
tvoří	tvořit	k5eAaImIp3nP	tvořit
úzké	úzký	k2eAgFnPc1d1	úzká
přechodové	přechodový	k2eAgFnPc1d1	přechodová
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Plochy	plocha	k1gFnPc1	plocha
reliéfu	reliéf	k1gInSc2	reliéf
vznikají	vznikat	k5eAaImIp3nP	vznikat
působením	působení	k1gNnSc7	působení
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různý	různý	k2eAgInSc4d1	různý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc4	sklon
<g/>
,	,	kIx,	,
orientaci	orientace	k1gFnSc4	orientace
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
expozici	expozice	k1gFnSc4	expozice
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
proudění	proudění	k1gNnSc3	proudění
vzduchu	vzduch	k1gInSc3	vzduch
nebo	nebo	k8xC	nebo
vodě	voda	k1gFnSc3	voda
i	i	k8xC	i
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
deště	dešť	k1gInSc2	dešť
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
sklonu	sklon	k1gInSc6	sklon
a	a	k8xC	a
orientaci	orientace	k1gFnSc6	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
pochodu	pochod	k1gInSc2	pochod
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
vznikají	vznikat	k5eAaImIp3nP	vznikat
geneticky	geneticky	k6eAd1	geneticky
stejnorodé	stejnorodý	k2eAgFnPc1d1	stejnorodá
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
plochy	plocha	k1gFnPc1	plocha
georeliéfu	georeliéf	k1gInSc2	georeliéf
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
složitější	složitý	k2eAgInPc1d2	složitější
povrchové	povrchový	k2eAgInPc1d1	povrchový
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
formy	forma	k1gFnPc1	forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plochy	plocha	k1gFnPc1	plocha
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
podle	podle	k7c2	podle
spádnic	spádnice	k1gFnPc2	spádnice
na	na	k7c4	na
přímkové	přímkový	k2eAgInPc4d1	přímkový
neboli	neboli	k8xC	neboli
přímé	přímý	k2eAgInPc4d1	přímý
<g/>
,	,	kIx,	,
konkávní	konkávní	k2eAgInPc4d1	konkávní
neboli	neboli	k8xC	neboli
vkleslé	vkleslý	k2eAgInPc4d1	vkleslý
(	(	kIx(	(
<g/>
vhloubené	vhloubený	k2eAgInPc4d1	vhloubený
tvary	tvar	k1gInPc4	tvar
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
např.	např.	kA	např.
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
brázda	brázda	k1gFnSc1	brázda
<g/>
,	,	kIx,	,
kotlina	kotlina	k1gFnSc1	kotlina
<g/>
,	,	kIx,	,
prolom	prolom	k1gInSc1	prolom
<g/>
,	,	kIx,	,
úval	úval	k1gInSc1	úval
<g/>
)	)	kIx)	)
a	a	k8xC	a
konvexní	konvexní	k2eAgInPc4d1	konvexní
neboli	neboli	k8xC	neboli
vypuklé	vypuklý	k2eAgInPc4d1	vypuklý
(	(	kIx(	(
<g/>
vystouplé	vystouplý	k2eAgInPc4d1	vystouplý
tvary	tvar	k1gInPc4	tvar
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
kupa	kupa	k1gFnSc1	kupa
<g/>
,	,	kIx,	,
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklonem	sklon	k1gInSc7	sklon
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
rovinné	rovinný	k2eAgInPc4d1	rovinný
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
sklonité	sklonitý	k2eAgNnSc1d1	sklonitý
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příkře	příkro	k6eAd1	příkro
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
příkře	příkro	k6eAd1	příkro
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srázy	sráz	k1gInPc1	sráz
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
sruby	srub	k1gInPc1	srub
či	či	k8xC	či
stěny	stěna	k1gFnPc1	stěna
(	(	kIx(	(
<g/>
sklon	sklon	k1gInSc1	sklon
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morfografie	Morfografie	k1gFnSc1	Morfografie
popisuje	popisovat	k5eAaImIp3nS	popisovat
povrchové	povrchový	k2eAgInPc4d1	povrchový
tvary	tvar	k1gInPc4	tvar
na	na	k7c6	na
omezeném	omezený	k2eAgNnSc6d1	omezené
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
útvary	útvar	k1gInPc4	útvar
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
historií	historie	k1gFnSc7	historie
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
pochody	pochod	k1gInPc7	pochod
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
geologickém	geologický	k2eAgNnSc6d1	geologické
podloží	podloží	k1gNnSc6	podloží
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vymezeného	vymezený	k2eAgNnSc2d1	vymezené
území	území	k1gNnSc2	území
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
georeliéfu	georeliéf	k1gInSc2	georeliéf
<g/>
.	.	kIx.	.
</s>
<s>
Morfometrie	Morfometrie	k1gFnSc1	Morfometrie
(	(	kIx(	(
<g/>
měření	měření	k1gNnSc1	měření
povrchových	povrchový	k2eAgInPc2d1	povrchový
tvarů	tvar	k1gInPc2	tvar
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
výškovou	výškový	k2eAgFnSc4d1	výšková
členitost	členitost	k1gFnSc4	členitost
tvarů	tvar	k1gInPc2	tvar
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ve	v	k7c6	v
čtverci	čtverec	k1gInSc6	čtverec
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
4	[number]	k4	4
km	km	kA	km
jako	jako	k8xS	jako
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nejnižším	nízký	k2eAgInSc7d3	nejnižší
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
(	(	kIx(	(
<g/>
typologicky	typologicky	k6eAd1	typologicky
roviny	rovina	k1gFnPc1	rovina
<g/>
,	,	kIx,	,
sníženiny	sníženina	k1gFnPc1	sníženina
<g/>
,	,	kIx,	,
pahorkatiny	pahorkatina	k1gFnPc1	pahorkatina
<g/>
,	,	kIx,	,
vrchoviny	vrchovina	k1gFnPc1	vrchovina
<g/>
,	,	kIx,	,
hornatiny	hornatina	k1gFnPc1	hornatina
<g/>
,	,	kIx,	,
velehornatiny	velehornatina	k1gFnPc1	velehornatina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
ploch	plocha	k1gFnPc2	plocha
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
uložení	uložení	k1gNnSc6	uložení
a	a	k8xC	a
stáří	stáří	k1gNnSc6	stáří
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
a	a	k8xC	a
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
reliéf	reliéf	k1gInSc1	reliéf
pevninský	pevninský	k2eAgInSc1d1	pevninský
reliéf	reliéf	k1gInSc1	reliéf
oceánský	oceánský	k2eAgInSc1d1	oceánský
(	(	kIx(	(
<g/>
dno	dno	k1gNnSc1	dno
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
přímkové	přímkový	k2eAgInPc1d1	přímkový
(	(	kIx(	(
<g/>
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
)	)	kIx)	)
plošina	plošina	k1gFnSc1	plošina
-	-	kIx~	-
plochý	plochý	k2eAgInSc1d1	plochý
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
<g />
.	.	kIx.	.
</s>
<s>
povrch	povrch	k1gInSc4	povrch
s	s	k7c7	s
převládající	převládající	k2eAgFnSc7d1	převládající
malou	malý	k2eAgFnSc7d1	malá
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
do	do	k7c2	do
30	[number]	k4	30
m.	m.	k?	m.
planina	planina	k1gFnSc1	planina
-	-	kIx~	-
plochý	plochý	k2eAgInSc1d1	plochý
povrch	povrch	k1gInSc1	povrch
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vrchovin	vrchovina	k1gFnPc2	vrchovina
nebo	nebo	k8xC	nebo
hornatin	hornatina	k1gFnPc2	hornatina
s	s	k7c7	s
převládající	převládající	k2eAgFnSc7d1	převládající
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
do	do	k7c2	do
30	[number]	k4	30
m	m	kA	m
konkávní	konkávní	k2eAgFnSc1d1	konkávní
brázda	brázda	k1gFnSc1	brázda
brána	brána	k1gFnSc1	brána
kotlina	kotlina	k1gFnSc1	kotlina
úval	úval	k1gInSc4	úval
konvexní	konvexní	k2eAgInSc1d1	konvexní
pahorek	pahorek	k1gInSc1	pahorek
kupa	kupa	k1gFnSc1	kupa
vrch	vrch	k1gInSc1	vrch
hora	hora	k1gFnSc1	hora
velehora	velehora	k1gFnSc1	velehora
štít	štít	k1gInSc1	štít
hřeben	hřeben	k1gInSc1	hřeben
hřbet	hřbet	k1gInSc4	hřbet
rovinné	rovinný	k2eAgNnSc1d1	rovinné
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
mírně	mírně	k6eAd1	mírně
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
značně	značně	k6eAd1	značně
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
příkře	příkro	k6eAd1	příkro
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
příkře	příkro	k6eAd1	příkro
skloněné	skloněný	k2eAgNnSc1d1	skloněné
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
srázy	sráz	k1gInPc1	sráz
(	(	kIx(	(
<g/>
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
stěny	stěna	k1gFnPc4	stěna
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
55	[number]	k4	55
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
SZ	SZ	kA	SZ
až	až	k8xS	až
SV	sv	kA	sv
jako	jako	k8xC	jako
orientované	orientovaný	k2eAgInPc4d1	orientovaný
k	k	k7c3	k
severu	sever	k1gInSc3	sever
SV	sv	kA	sv
až	až	k8xS	až
JV	JV	kA	JV
jako	jako	k8xS	jako
orientované	orientovaný	k2eAgInPc4d1	orientovaný
k	k	k7c3	k
východu	východ	k1gInSc3	východ
JV	JV	kA	JV
až	až	k8xS	až
JZ	JZ	kA	JZ
jako	jako	k8xC	jako
orientované	orientovaný	k2eAgInPc4d1	orientovaný
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
JZ	JZ	kA	JZ
až	až	k8xS	až
SZ	SZ	kA	SZ
jako	jako	k8xS	jako
orientované	orientovaný	k2eAgInPc4d1	orientovaný
k	k	k7c3	k
západu	západ	k1gInSc3	západ
Orientace	orientace	k1gFnSc2	orientace
rovinných	rovinný	k2eAgFnPc2d1	rovinná
ploch	plocha	k1gFnPc2	plocha
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
do	do	k7c2	do
2	[number]	k4	2
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
nezjišťuje	zjišťovat	k5eNaImIp3nS	zjišťovat
a	a	k8xC	a
nekategorizuje	kategorizovat	k5eNaBmIp3nS	kategorizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
georeliéfu	georeliéf	k1gInSc2	georeliéf
krajiny	krajina	k1gFnSc2	krajina
zabýval	zabývat	k5eAaImAgInS	zabývat
Geografický	geografický	k2eAgInSc1d1	geografický
ústav	ústav	k1gInSc1	ústav
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
zrušen	zrušit	k5eAaPmNgInS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
–	–	k?	–
1977	[number]	k4	1977
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
geomorfologické	geomorfologický	k2eAgNnSc1d1	Geomorfologické
členění	členění	k1gNnSc1	členění
státního	státní	k2eAgNnSc2d1	státní
území	území	k1gNnSc2	území
tehdy	tehdy	k6eAd1	tehdy
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
okrsků	okrsek	k1gInPc2	okrsek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnSc2	jejich
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnPc1	veřejnost
prezentovány	prezentovat	k5eAaBmNgFnP	prezentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
vydané	vydaný	k2eAgNnSc1d1	vydané
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Academie	academia	k1gFnSc2	academia
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
lexikon	lexikon	k1gInSc1	lexikon
ČSR	ČSR	kA	ČSR
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
nížiny	nížina	k1gFnSc2	nížina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
použití	použití	k1gNnSc4	použití
digitálních	digitální	k2eAgFnPc2d1	digitální
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
vrstevnicové	vrstevnicový	k2eAgFnSc2d1	vrstevnicová
mapy	mapa	k1gFnSc2	mapa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
digitální	digitální	k2eAgFnSc1d1	digitální
mapa	mapa	k1gFnSc1	mapa
prezentující	prezentující	k2eAgFnSc1d1	prezentující
územně	územně	k6eAd1	územně
vymezené	vymezený	k2eAgFnPc4d1	vymezená
geomorfologické	geomorfologický	k2eAgFnPc4d1	geomorfologická
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
systémově	systémově	k6eAd1	systémově
provedeném	provedený	k2eAgNnSc6d1	provedené
členění	členění	k1gNnSc6	členění
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
řádově	řádově	k6eAd1	řádově
na	na	k7c4	na
4	[number]	k4	4
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
10	[number]	k4	10
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
28	[number]	k4	28
podsoustav	podsoustat	k5eAaPmDgInS	podsoustat
<g/>
,	,	kIx,	,
94	[number]	k4	94
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
243	[number]	k4	243
pocelků	pocelka	k1gMnPc2	pocelka
a	a	k8xC	a
933	[number]	k4	933
okrsků	okrsek	k1gInPc2	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
geomorfologických	geomorfologický	k2eAgFnPc2d1	geomorfologická
jednotek	jednotka	k1gFnPc2	jednotka
publikován	publikován	k2eAgMnSc1d1	publikován
Agenturou	agentura	k1gFnSc7	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
"	"	kIx"	"
<g/>
Zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
lexikonu	lexikon	k1gInSc2	lexikon
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
nížiny	nížina	k1gFnSc2	nížina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
georeliéfu	georeliéf	k1gInSc6	georeliéf
Česka	Česko	k1gNnSc2	Česko
jsou	být	k5eAaImIp3nP	být
typologicky	typologicky	k6eAd1	typologicky
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
nížiny	nížina	k1gFnPc1	nížina
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
300	[number]	k4	300
m	m	kA	m
a	a	k8xC	a
vysočiny	vysočina	k1gFnSc2	vysočina
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
nad	nad	k7c4	nad
300	[number]	k4	300
m	m	kA	m
<g/>
,	,	kIx,	,
omezené	omezený	k2eAgFnSc2d1	omezená
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
okolní	okolní	k2eAgFnSc7d1	okolní
vyvýšeninou	vyvýšenina	k1gFnSc7	vyvýšenina
jsou	být	k5eAaImIp3nP	být
sníženiny	sníženina	k1gFnPc1	sníženina
(	(	kIx(	(
<g/>
typem	typ	k1gInSc7	typ
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
brázdy	brázda	k1gFnSc2	brázda
<g/>
,	,	kIx,	,
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
,	,	kIx,	,
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
prolomy	prolom	k1gInPc1	prolom
<g/>
,	,	kIx,	,
úvaly	úval	k1gInPc1	úval
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
nebo	nebo	k8xC	nebo
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
erozně	erozně	k6eAd1	erozně
denudačními	denudační	k2eAgInPc7d1	denudační
pochody	pochod	k1gInPc7	pochod
s	s	k7c7	s
odnosem	odnos	k1gInSc7	odnos
méně	málo	k6eAd2	málo
odolných	odolný	k2eAgFnPc2d1	odolná
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionálním	regionální	k2eAgNnSc6d1	regionální
členění	členění	k1gNnSc6	členění
např.	např.	kA	např.
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
pánve	pánev	k1gFnSc2	pánev
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc4	vznik
tektonickým	tektonický	k2eAgInSc7d1	tektonický
pohybem	pohyb	k1gInSc7	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Podhradská	podhradský	k2eAgFnSc1d1	Podhradská
kotlina	kotlina	k1gFnSc1	kotlina
v	v	k7c6	v
Železných	železný	k2eAgFnPc6d1	železná
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc4	vznik
v	v	k7c6	v
krystalických	krystalický	k2eAgFnPc6d1	krystalická
břidlicích	břidlice	k1gFnPc6	břidlice
odnosem	odnos	k1gInSc7	odnos
méně	málo	k6eAd2	málo
odolných	odolný	k2eAgFnPc2d1	odolná
hornin	hornina	k1gFnPc2	hornina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typa	k1gFnPc1	typa
georeliéfu	georeliéf	k1gInSc2	georeliéf
dle	dle	k7c2	dle
výškové	výškový	k2eAgFnSc2d1	výšková
členitosti	členitost	k1gFnSc2	členitost
<g/>
:	:	kIx,	:
roviny	rovina	k1gFnSc2	rovina
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
nivních	nivní	k2eAgInPc6d1	nivní
sedimentech	sediment	k1gInPc6	sediment
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionálním	regionální	k2eAgNnSc6d1	regionální
členění	členění	k1gNnSc6	členění
např.	např.	kA	např.
Poděbradská	poděbradský	k2eAgFnSc1d1	Poděbradská
nebo	nebo	k8xC	nebo
Smiřická	smiřický	k2eAgFnSc1d1	Smiřická
rovina	rovina	k1gFnSc1	rovina
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
(	(	kIx(	(
<g/>
ploché	plochý	k2eAgFnSc2d1	plochá
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
m	m	kA	m
a	a	k8xC	a
členité	členitý	k2eAgFnSc2d1	členitá
75	[number]	k4	75
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Skutečská	skutečský	k2eAgFnSc1d1	Skutečská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
nebo	nebo	k8xC	nebo
členitá	členitý	k2eAgFnSc1d1	členitá
Stružinecká	Stružinecký	k2eAgFnSc1d1	Stružinecký
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
ploché	plochý	k2eAgFnSc2d1	plochá
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
členité	členitý	k2eAgFnSc2d1	členitá
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
ploché	plochý	k2eAgFnSc2d1	plochá
vrchoviny	vrchovina	k1gFnSc2	vrchovina
jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgFnPc1d1	železná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
členité	členitý	k2eAgFnPc1d1	členitá
jejich	jejich	k3xOp3gInSc4	jejich
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
okrsek	okrsek	k1gInSc4	okrsek
Kameničská	Kameničský	k2eAgFnSc1d1	Kameničský
vrchovina	vrchovina	k1gFnSc1	vrchovina
hornatiny	hornatina	k1gFnSc2	hornatina
(	(	kIx(	(
<g/>
ploché	plochý	k2eAgFnSc2d1	plochá
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
m	m	kA	m
a	a	k8xC	a
členité	členitý	k2eAgFnSc2d1	členitá
450	[number]	k4	450
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plochou	plochý	k2eAgFnSc7d1	plochá
hornatinou	hornatina	k1gFnSc7	hornatina
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
členitou	členitý	k2eAgFnSc7d1	členitá
hornatinou	hornatina	k1gFnSc7	hornatina
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
pohoří	pohoří	k1gNnSc1	pohoří
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
velehornatiny	velehornatina	k1gFnSc2	velehornatina
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nad	nad	k7c7	nad
600	[number]	k4	600
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
územním	územní	k2eAgInSc6d1	územní
celku	celek	k1gInSc6	celek
geomorfologické	geomorfologický	k2eAgFnSc2d1	geomorfologická
jednotky	jednotka	k1gFnSc2	jednotka
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
Georeliéf	Georeliéf	k1gInSc4	Georeliéf
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
regionálním	regionální	k2eAgInSc6d1	regionální
(	(	kIx(	(
<g/>
územním	územní	k2eAgInSc6d1	územní
<g/>
)	)	kIx)	)
členění	členění	k1gNnSc3	členění
tvoří	tvořit	k5eAaImIp3nP	tvořit
řádově	řádově	k6eAd1	řádově
odlišné	odlišný	k2eAgFnPc1d1	odlišná
geomorfologické	geomorfologický	k2eAgFnPc1d1	geomorfologická
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
členění	členění	k1gNnSc4	členění
4	[number]	k4	4
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
zastoupeného	zastoupený	k2eAgInSc2d1	zastoupený
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
geomorfologickými	geomorfologický	k2eAgFnPc7d1	geomorfologická
jednotkami	jednotka	k1gFnPc7	jednotka
obecně	obecně	k6eAd1	obecně
označovanými	označovaný	k2eAgFnPc7d1	označovaná
provincie	provincie	k1gFnPc4	provincie
s	s	k7c7	s
názvy	název	k1gInPc4	název
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Západní	západní	k2eAgInPc1d1	západní
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
Západopanonská	Západopanonský	k2eAgFnSc1d1	Západopanonská
pánev	pánev	k1gFnSc1	pánev
a	a	k8xC	a
rozlohou	rozloha	k1gFnSc7	rozloha
malým	malý	k2eAgInPc3d1	malý
územím	území	k1gNnSc7	území
Středopolská	Středopolský	k2eAgFnSc1d1	Středopolský
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
používaná	používaný	k2eAgFnSc1d1	používaná
řádově	řádově	k6eAd1	řádově
vyšší	vysoký	k2eAgFnSc1d2	vyšší
geomorfologická	geomorfologický	k2eAgFnSc1d1	geomorfologická
jednotka	jednotka	k1gFnSc1	jednotka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Středoevropská	středoevropský	k2eAgFnSc1d1	středoevropská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Řádově	řádově	k6eAd1	řádově
nižší	nízký	k2eAgFnPc1d2	nižší
geomorfologické	geomorfologický	k2eAgFnPc1d1	geomorfologická
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
kodifikovány	kodifikovat	k5eAaBmNgFnP	kodifikovat
do	do	k7c2	do
sestavy	sestava	k1gFnSc2	sestava
s	s	k7c7	s
vlastními	vlastní	k2eAgNnPc7d1	vlastní
jmény	jméno	k1gNnPc7	jméno
<g/>
,	,	kIx,	,
indexy	index	k1gInPc7	index
a	a	k8xC	a
plošnou	plošný	k2eAgFnSc7d1	plošná
výměrou	výměra	k1gFnSc7	výměra
s	s	k7c7	s
obecnými	obecný	k2eAgInPc7d1	obecný
názvy	název	k1gInPc7	název
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
územně	územně	k6eAd1	územně
vymezené	vymezený	k2eAgFnPc1d1	vymezená
především	především	k9	především
podle	podle	k7c2	podle
geologické	geologický	k2eAgFnSc2d1	geologická
struktury	struktura	k1gFnSc2	struktura
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podsoustava	podsoustava	k1gFnSc1	podsoustava
(	(	kIx(	(
<g/>
shodná	shodný	k2eAgFnSc1d1	shodná
morfostrukturou	morfostruktura	k1gFnSc7	morfostruktura
a	a	k8xC	a
podobnou	podobný	k2eAgFnSc7d1	podobná
výškovou	výškový	k2eAgFnSc7d1	výšková
členitostí	členitost	k1gFnSc7	členitost
v	v	k7c6	v
6	[number]	k4	6
<g/>
<g />
.	.	kIx.	.
</s>
<s>
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celek	celek	k1gInSc1	celek
(	(	kIx(	(
<g/>
shodný	shodný	k2eAgInSc1d1	shodný
geomorfologickou	geomorfologický	k2eAgFnSc7d1	geomorfologická
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
georeliéfu	georeliéf	k1gInSc2	georeliéf
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podcelek	podcelek	k1gInSc1	podcelek
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
stejnorodý	stejnorodý	k2eAgInSc1d1	stejnorodý
povrchovými	povrchový	k2eAgInPc7d1	povrchový
tvary	tvar	k1gInPc7	tvar
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
vývojem	vývoj	k1gInSc7	vývoj
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
a	a	k8xC	a
okrsek	okrsek	k1gInSc1	okrsek
(	(	kIx(	(
<g/>
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
výškovou	výškový	k2eAgFnSc7d1	výšková
polohou	poloha	k1gFnSc7	poloha
se	s	k7c7	s
shodným	shodný	k2eAgInSc7d1	shodný
původem	původ	k1gInSc7	původ
georeliéfu	georeliéf	k1gInSc2	georeliéf
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
systémově	systémově	k6eAd1	systémově
provedeném	provedený	k2eAgNnSc6d1	provedené
členění	členění	k1gNnSc6	členění
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Česka	Česko	k1gNnSc2	Česko
řádově	řádově	k6eAd1	řádově
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
geomorfologická	geomorfologický	k2eAgFnSc1d1	geomorfologická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
typologicky	typologicky	k6eAd1	typologicky
se	s	k7c7	s
stejnorodým	stejnorodý	k2eAgInSc7d1	stejnorodý
reliéfem	reliéf	k1gInSc7	reliéf
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
