<s>
Reliéf	reliéf	k1gInSc1	reliéf
v	v	k7c6	v
geografii	geografie	k1gFnSc6	geografie
<g/>
,	,	kIx,	,
též	též	k9	též
georeliéf	georeliéf	k1gInSc1	georeliéf
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc4d1	odborný
pojem	pojem	k1gInSc4	pojem
geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
geografických	geografický	k2eAgFnPc2d1	geografická
disciplín	disciplína	k1gFnPc2	disciplína
ve	v	k7c6	v
věcném	věcný	k2eAgInSc6d1	věcný
významu	význam	k1gInSc6	význam
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
"	"	kIx"	"
<g/>
tvářnost	tvářnost	k1gFnSc1	tvářnost
neboli	neboli	k8xC	neboli
vzhled	vzhled	k1gInSc1	vzhled
povrchu	povrch	k1gInSc2	povrch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
reliéf	reliéf	k1gInSc1	reliéf
<g/>
)	)	kIx)	)
Země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
užití	užití	k1gNnSc2	užití
složeného	složený	k2eAgNnSc2d1	složené
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
georeliéf	georeliéf	k1gInSc1	georeliéf
<g/>
)	)	kIx)	)
s	s	k7c7	s
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
významem	význam	k1gInSc7	význam
spojeným	spojený	k2eAgInSc7d1	spojený
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
částí	část	k1gFnSc7	část
"	"	kIx"	"
<g/>
geo-	geo-	k?	geo-
<g/>
"	"	kIx"	"
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
