<s>
Rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
jednoděložných	jednoděložný	k2eAgFnPc2d1	jednoděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgMnPc2d1	lipnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
botanice	botanika	k1gFnSc6	botanika
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
přes	přes	k7c4	přes
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgFnPc2d1	kulturní
forem	forma	k1gFnPc2	forma
–	–	k?	–
obilnin	obilnina	k1gFnPc2	obilnina
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
rýže	rýže	k1gFnSc1	rýže
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
dvacet	dvacet	k4xCc4	dvacet
procent	procento	k1gNnPc2	procento
kalorické	kalorický	k2eAgFnSc2d1	kalorická
spotřeby	spotřeba	k1gFnSc2	spotřeba
lidské	lidský	k2eAgFnSc2d1	lidská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
L.	L.	kA	L.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
lipnicovitých	lipnicovitý	k2eAgNnPc2d1	lipnicovité
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gNnPc2	Poaceae
<g/>
)	)	kIx)	)
rostlin	rostlina	k1gFnPc2	rostlina
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
rýžovitých	rýžovitý	k2eAgFnPc2d1	rýžovitý
(	(	kIx(	(
<g/>
Oryzeae	Oryzeae	k1gFnPc2	Oryzeae
<g/>
)	)	kIx)	)
zahrnujících	zahrnující	k2eAgFnPc2d1	zahrnující
asi	asi	k9	asi
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
tisíce	tisíc	k4xCgInSc2	tisíc
taxonů	taxon	k1gInPc2	taxon
nižších	nízký	k2eAgInPc2d2	nižší
než	než	k8xS	než
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
rýže	rýže	k1gFnSc2	rýže
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
<g/>
)	)	kIx)	)
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
Carl	Carlum	k1gNnPc2	Carlum
Liné	Liná	k1gFnSc2	Liná
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
popsal	popsat	k5eAaPmAgMnS	popsat
druh	druh	k1gInSc4	druh
rýže	rýže	k1gFnSc2	rýže
seté	setý	k2eAgNnSc1d1	seté
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
sativa	sativo	k1gNnSc2	sativo
<g/>
)	)	kIx)	)
pěstované	pěstovaný	k2eAgInPc4d1	pěstovaný
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
herbářích	herbář	k1gInPc6	herbář
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
ale	ale	k9	ale
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejné	stejný	k2eAgInPc4d1	stejný
druhy	druh	k1gInPc4	druh
nebo	nebo	k8xC	nebo
o	o	k7c4	o
taxony	taxon	k1gInPc4	taxon
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
asi	asi	k9	asi
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
druhů	druh	k1gInPc2	druh
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
rýže	rýže	k1gFnSc1	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
sativa	sativo	k1gNnSc2	sativo
L.	L.	kA	L.
<g/>
)	)	kIx)	)
a	a	k8xC	a
rýže	rýže	k1gFnSc1	rýže
africká	africký	k2eAgFnSc1d1	africká
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
glaberrima	glaberrimum	k1gNnSc2	glaberrimum
Staud	Stauda	k1gFnPc2	Stauda
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
rýže	rýže	k1gFnSc1	rýže
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
rýže	rýže	k1gFnSc2	rýže
jsou	být	k5eAaImIp3nP	být
divoké	divoký	k2eAgMnPc4d1	divoký
nebo	nebo	k8xC	nebo
chápané	chápaný	k2eAgMnPc4d1	chápaný
jako	jako	k8xC	jako
plevelné	plevelný	k2eAgMnPc4d1	plevelný
v	v	k7c6	v
rýžovištích	rýžoviště	k1gNnPc6	rýžoviště
uvedených	uvedený	k2eAgInPc2d1	uvedený
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
chromozomů	chromozom	k1gInPc2	chromozom
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rýže	rýže	k1gFnSc1	rýže
diploidní	diploidní	k2eAgFnSc1d1	diploidní
(	(	kIx(	(
<g/>
24	[number]	k4	24
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
sady	sada	k1gFnSc2	sada
AA	AA	kA	AA
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
,	,	kIx,	,
CC	CC	kA	CC
<g/>
,	,	kIx,	,
EE	EE	kA	EE
<g/>
,	,	kIx,	,
FF	ff	kA	ff
a	a	k8xC	a
GG	GG	kA	GG
<g/>
)	)	kIx)	)
a	a	k8xC	a
tetraplodní	tetraplodní	k2eAgFnSc1d1	tetraplodní
(	(	kIx(	(
<g/>
48	[number]	k4	48
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
sady	sada	k1gFnSc2	sada
BBCC	BBCC	kA	BBCC
<g/>
,	,	kIx,	,
CCDD	CCDD	kA	CCDD
<g/>
,	,	kIx,	,
HHJJ	HHJJ	kA	HHJJ
a	a	k8xC	a
HHKK	HHKK	kA	HHKK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diplodními	Diplodní	k2eAgInPc7d1	Diplodní
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
divoké	divoký	k2eAgFnSc2d1	divoká
rýže	rýže	k1gFnSc2	rýže
Oryza	Oryz	k1gMnSc2	Oryz
alta	altus	k1gMnSc2	altus
<g/>
,	,	kIx,	,
O.	O.	kA	O.
australiensis	australiensis	k1gInSc1	australiensis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
barthii	barthie	k1gFnSc4	barthie
<g/>
,	,	kIx,	,
O.	O.	kA	O.
brachyantha	brachyantha	k1gFnSc1	brachyantha
<g/>
,	,	kIx,	,
O.	O.	kA	O.
coarctata	coarctata	k1gFnSc1	coarctata
<g/>
,	,	kIx,	,
O.	O.	kA	O.
eichingeri	eichingeri	k1gNnSc1	eichingeri
<g/>
,	,	kIx,	,
O.	O.	kA	O.
grandiglumis	grandiglumis	k1gInSc1	grandiglumis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
granulata	granule	k1gNnPc4	granule
<g/>
,	,	kIx,	,
O.	O.	kA	O.
latifolia	latifolia	k1gFnSc1	latifolia
<g/>
,	,	kIx,	,
O.	O.	kA	O.
longiglumis	longiglumis	k1gInSc1	longiglumis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
malampuzhaensis	malampuzhaensis	k1gInSc1	malampuzhaensis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
meyeriana	meyeriana	k1gFnSc1	meyeriana
<g/>
,	,	kIx,	,
O.	O.	kA	O.
minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
O.	O.	kA	O.
officinalis	officinalis	k1gInSc1	officinalis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
perennis	perennis	k1gInSc1	perennis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
punctata	punctata	k1gFnSc1	punctata
<g/>
,	,	kIx,	,
O.	O.	kA	O.
rhizomatis	rhizomatis	k1gInSc1	rhizomatis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
ridleyi	ridley	k1gFnSc2	ridley
a	a	k8xC	a
O.	O.	kA	O.
schlecteri	schlecteri	k6eAd1	schlecteri
<g/>
.	.	kIx.	.
</s>
<s>
Tetraplodních	Tetraplodní	k2eAgInPc2d1	Tetraplodní
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
druhů	druh	k1gInPc2	druh
včetně	včetně	k7c2	včetně
2	[number]	k4	2
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
<g/>
:	:	kIx,	:
O.	O.	kA	O.
rufipogon	rufipogon	k1gNnSc1	rufipogon
<g/>
,	,	kIx,	,
O.	O.	kA	O.
nivara	nivara	k1gFnSc1	nivara
(	(	kIx(	(
<g/>
synonymě	synonymě	k6eAd1	synonymě
Oryza	Oryz	k1gMnSc2	Oryz
sativa	sativ	k1gMnSc2	sativ
f.	f.	k?	f.
spontanea	spontaneus	k1gMnSc2	spontaneus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O.	O.	kA	O.
glumipatula	glumipatula	k1gFnSc1	glumipatula
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Oryza	Oryza	k1gFnSc1	Oryza
glumaepatula	glumaepatula	k1gFnSc1	glumaepatula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O.	O.	kA	O.
meridionalis	meridionalis	k1gInSc1	meridionalis
<g/>
,	,	kIx,	,
O.	O.	kA	O.
breviligualata	breviliguale	k1gNnPc4	breviliguale
<g/>
,	,	kIx,	,
O.	O.	kA	O.
logistaminata	logistaminata	k1gFnSc1	logistaminata
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Oryza	Oryza	k1gFnSc1	Oryza
glumaepatula	glumaepatula	k1gFnSc1	glumaepatula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
a	a	k8xC	a
O.	O.	kA	O.
glaberrima	glaberrima	k1gFnSc1	glaberrima
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
Perlové	perlový	k2eAgFnSc2d1	Perlová
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
domestikována	domestikovat	k5eAaBmNgFnS	domestikovat
před	před	k7c7	před
8	[number]	k4	8
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
500	[number]	k4	500
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
všude	všude	k6eAd1	všude
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
divoké	divoký	k2eAgFnSc2d1	divoká
Oryza	Oryz	k1gMnSc4	Oryz
rufipogon	rufipogon	k1gInSc4	rufipogon
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
pěstovanou	pěstovaný	k2eAgFnSc7d1	pěstovaná
rýží	rýže	k1gFnSc7	rýže
je	být	k5eAaImIp3nS	být
Oryza	Oryza	k1gFnSc1	Oryza
glaberrima	glaberrima	k1gFnSc1	glaberrima
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Nigeru	Niger	k1gInSc2	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
Oryza	Oryza	k1gFnSc1	Oryza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
či	či	k8xC	či
víceletá	víceletý	k2eAgFnSc1d1	víceletá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpěstovanější	pěstovaný	k2eAgInSc1d3	nejpěstovanější
druh	druh	k1gInSc1	druh
rýže	rýže	k1gFnSc2	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
bylina	bylina	k1gFnSc1	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mohutný	mohutný	k2eAgInSc1d1	mohutný
svazčitý	svazčitý	k2eAgInSc1d1	svazčitý
kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
odnoží	odnož	k1gFnSc7	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
klíčí	klíčit	k5eAaImIp3nS	klíčit
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
jedním	jeden	k4xCgInSc7	jeden
primárním	primární	k2eAgInSc7d1	primární
kořenem	kořen	k1gInSc7	kořen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
větví	větvit	k5eAaImIp3nS	větvit
ve	v	k7c4	v
dva	dva	k4xCgInPc4	dva
adventivní	adventivní	k2eAgInPc4d1	adventivní
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
větví	větvit	k5eAaImIp3nS	větvit
v	v	k7c4	v
další	další	k2eAgInPc4d1	další
postranní	postranní	k2eAgInPc4d1	postranní
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Mohutnost	mohutnost	k1gFnSc1	mohutnost
kořenového	kořenový	k2eAgInSc2d1	kořenový
systému	systém	k1gInSc2	systém
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
odrůdě	odrůda	k1gFnSc6	odrůda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
používané	používaný	k2eAgFnSc3d1	používaná
agrotechnice	agrotechnika	k1gFnSc3	agrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
aerenchymem	aerenchymum	k1gNnSc7	aerenchymum
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nahnědlé	nahnědlý	k2eAgInPc1d1	nahnědlý
až	až	k9	až
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
růstu	růst	k1gInSc2	růst
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
kvetení	kvetení	k1gNnSc2	kvetení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kvetení	kvetení	k1gNnSc2	kvetení
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nerozvíjí	rozvíjet	k5eNaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
má	mít	k5eAaImIp3nS	mít
rostlina	rostlina	k1gFnSc1	rostlina
odnoží	odnoží	k1gNnSc2	odnoží
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
mohutnější	mohutný	k2eAgInPc4d2	mohutnější
má	mít	k5eAaImIp3nS	mít
kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tyto	tento	k3xDgInPc4	tento
odnože	odnož	k1gInPc4	odnož
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
<g/>
.	.	kIx.	.
</s>
<s>
Nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
rostliny	rostlina	k1gFnSc2	rostlina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
svazkem	svazek	k1gInSc7	svazek
vzpřímených	vzpřímený	k2eAgNnPc2d1	vzpřímené
stébel	stéblo	k1gNnPc2	stéblo
dosahujících	dosahující	k2eAgFnPc2d1	dosahující
délky	délka	k1gFnSc2	délka
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
až	až	k9	až
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
m.	m.	k?	m.
Stéblo	stéblo	k1gNnSc1	stéblo
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
odnožovacího	odnožovací	k2eAgNnSc2d1	odnožovací
kolénka	kolénko	k1gNnSc2	kolénko
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
v	v	k7c6	v
kolínkách	kolínko	k1gNnPc6	kolínko
větví	větev	k1gFnSc7	větev
na	na	k7c4	na
odnože	odnož	k1gInPc4	odnož
druhého	druhý	k4xOgNnSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
rostlina	rostlina	k1gFnSc1	rostlina
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
odnoží	odnož	k1gFnPc2	odnož
z	z	k7c2	z
čehož	což	k3yRnSc2	což
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
neplodné	plodný	k2eNgInPc1d1	neplodný
a	a	k8xC	a
odumírají	odumírat	k5eAaImIp3nP	odumírat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Stéblo	stéblo	k1gNnSc1	stéblo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
kolénka	kolénko	k1gNnPc4	kolénko
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
listy	list	k1gInPc1	list
a	a	k8xC	a
na	na	k7c4	na
internody	internoda	k1gFnPc4	internoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
vrcholové	vrcholový	k2eAgFnSc3d1	vrcholová
části	část	k1gFnSc3	část
rostliny	rostlina	k1gFnSc2	rostlina
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Stéblo	stéblo	k1gNnSc1	stéblo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
bohatě	bohatě	k6eAd1	bohatě
olistěno	olistit	k5eAaPmNgNnS	olistit
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
průřezu	průřez	k1gInSc6	průřez
oválné	oválný	k2eAgFnSc2d1	oválná
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hladké	hladký	k2eAgNnSc1d1	hladké
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
vegetačním	vegetační	k2eAgNnSc7d1	vegetační
stářím	stáří	k1gNnSc7	stáří
mění	měnit	k5eAaImIp3nS	měnit
do	do	k7c2	do
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
kultivary	kultivar	k1gInPc1	kultivar
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
do	do	k7c2	do
růžova	růžovo	k1gNnSc2	růžovo
či	či	k8xC	či
tmavě	tmavě	k6eAd1	tmavě
červena	červen	k2eAgFnSc1d1	červen
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
rýže	rýže	k1gFnSc2	rýže
je	být	k5eAaImIp3nS	být
vzpřímená	vzpřímený	k2eAgFnSc1d1	vzpřímená
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
poléhavé	poléhavý	k2eAgInPc1d1	poléhavý
taxony	taxon	k1gInPc1	taxon
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
trsech	trs	k1gInPc6	trs
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
střídavě	střídavě	k6eAd1	střídavě
z	z	k7c2	z
kolének	kolénko	k1gNnPc2	kolénko
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
jazýčkem	jazýček	k1gInSc7	jazýček
<g/>
,	,	kIx,	,
oušky	ouško	k1gNnPc7	ouško
a	a	k8xC	a
čepelí	čepel	k1gFnSc7	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Pochva	pochva	k1gFnSc1	pochva
obaluje	obalovat	k5eAaImIp3nS	obalovat
stéblo	stéblo	k1gNnSc4	stéblo
a	a	k8xC	a
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
listovou	listový	k2eAgFnSc4d1	listová
čepel	čepel	k1gFnSc4	čepel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
předělu	předěl	k1gInSc2	předěl
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
tzv.	tzv.	kA	tzv.
ouška	ouško	k1gNnPc4	ouško
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
chlupatá	chlupatý	k2eAgNnPc1d1	chlupaté
<g/>
.	.	kIx.	.
</s>
<s>
Ouška	ouško	k1gNnPc1	ouško
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
vývojových	vývojový	k2eAgFnPc6d1	vývojová
fázích	fáze	k1gFnPc6	fáze
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
.	.	kIx.	.
</s>
<s>
Jazýček	jazýček	k1gInSc1	jazýček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
předělu	předěl	k1gInSc6	předěl
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
je	on	k3xPp3gNnSc4	on
blanitý	blanitý	k2eAgMnSc1d1	blanitý
<g/>
,	,	kIx,	,
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
mm	mm	kA	mm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
rozeklaný	rozeklaný	k2eAgMnSc1d1	rozeklaný
na	na	k7c4	na
2	[number]	k4	2
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
cm	cm	kA	cm
úzká	úzký	k2eAgFnSc1d1	úzká
a	a	k8xC	a
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
0,3	[number]	k4	0,3
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podélnou	podélný	k2eAgFnSc4d1	podélná
žilnatinu	žilnatina	k1gFnSc4	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
je	být	k5eAaImIp3nS	být
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
,	,	kIx,	,
rub	rub	k1gInSc1	rub
a	a	k8xC	a
líc	líc	k1gInSc1	líc
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
obalena	obalen	k2eAgFnSc1d1	obalena
chloupky	chloupek	k1gInPc7	chloupek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
pigmentovaná	pigmentovaný	k2eAgFnSc1d1	pigmentovaná
<g/>
.	.	kIx.	.
</s>
<s>
Praporcový	praporcový	k2eAgInSc1d1	praporcový
list	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInSc1d2	širší
než	než	k8xS	než
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
delších	dlouhý	k2eAgInPc2d2	delší
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ohlá	ohlat	k5eAaPmIp3nS	ohlat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kvítky	kvítek	k1gInPc1	kvítek
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
v	v	k7c6	v
klasech	klas	k1gInPc6	klas
a	a	k8xC	a
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
se	se	k3xPyFc4	se
v	v	k7c4	v
laty	lata	k1gFnPc4	lata
různého	různý	k2eAgNnSc2d1	různé
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Latu	lata	k1gFnSc4	lata
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
jednokvětých	jednokvětý	k2eAgInPc2d1	jednokvětý
klásků	klásek	k1gInPc2	klásek
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
krátkém	krátký	k2eAgNnSc6d1	krátké
vřetenu	vřeteno	k1gNnSc6	vřeteno
obalen	obalit	k5eAaPmNgInS	obalit
pětižilnou	pětižilný	k2eAgFnSc7d1	pětižilný
<g/>
,	,	kIx,	,
kožovitou	kožovitý	k2eAgFnSc7d1	kožovitá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
osinatou	osinatý	k2eAgFnSc7d1	osinatá
a	a	k8xC	a
chlupatou	chlupatý	k2eAgFnSc7d1	chlupatá
pluchou	plucha	k1gFnSc7	plucha
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gNnSc2	on
šest	šest	k4xCc1	šest
tyčinek	tyčinka	k1gFnPc2	tyčinka
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
kruzích	kruh	k1gInPc6	kruh
a	a	k8xC	a
jednopouzdrý	jednopouzdrý	k2eAgInSc4d1	jednopouzdrý
semeník	semeník	k1gInSc4	semeník
obalený	obalený	k2eAgInSc4d1	obalený
dvěma	dva	k4xCgFnPc7	dva
plenkami	plenka	k1gFnPc7	plenka
s	s	k7c7	s
pérovou	pérový	k2eAgFnSc7d1	pérová
bliznou	blizna	k1gFnSc7	blizna
rozdělenou	rozdělený	k2eAgFnSc7d1	rozdělená
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
jednokvěté	jednokvětý	k2eAgInPc4d1	jednokvětý
klásky	klásek	k1gInPc4	klásek
o	o	k7c6	o
6	[number]	k4	6
tyčinkách	tyčinka	k1gFnPc6	tyčinka
v	v	k7c6	v
latách	lata	k1gFnPc6	lata
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
obilka	obilka	k1gFnSc1	obilka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
smáčklá	smáčklý	k2eAgFnSc1d1	smáčklá
s	s	k7c7	s
různě	různě	k6eAd1	různě
barevným	barevný	k2eAgNnSc7d1	barevné
oplodím	oplodí	k1gNnSc7	oplodí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obalena	obalen	k2eAgFnSc1d1	obalena
pluchou	plucha	k1gFnSc7	plucha
a	a	k8xC	a
pluškou	pluška	k1gFnSc7	pluška
<g/>
.	.	kIx.	.
</s>
<s>
Obilka	obilka	k1gFnSc1	obilka
bývá	bývat	k5eAaImIp3nS	bývat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
1,9	[number]	k4	1,9
<g/>
–	–	k?	–
<g/>
3,7	[number]	k4	3,7
mm	mm	kA	mm
široká	široký	k2eAgFnSc1d1	široká
s	s	k7c7	s
embryem	embryo	k1gNnSc7	embryo
umístěným	umístěný	k2eAgNnSc7d1	umístěné
v	v	k7c4	v
bazální	bazální	k2eAgFnSc4d1	bazální
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rýže	rýže	k1gFnSc2	rýže
seté	setý	k2eAgFnSc2d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obilka	obilka	k1gFnSc1	obilka
obalena	obalen	k2eAgFnSc1d1	obalena
pluchou	plucha	k1gFnSc7	plucha
a	a	k8xC	a
pluškou	pluška	k1gFnSc7	pluška
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
škrobu	škrob	k1gInSc2	škrob
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bílkoviny	bílkovina	k1gFnPc1	bílkovina
a	a	k8xC	a
vitamín	vitamín	k1gInSc1	vitamín
B.	B.	kA	B.
Obilka	obilka	k1gFnSc1	obilka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
%	%	kIx~	%
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
2,4	[number]	k4	2,4
%	%	kIx~	%
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
68	[number]	k4	68
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
%	%	kIx~	%
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
vlákniny	vláknina	k1gFnPc4	vláknina
a	a	k8xC	a
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vitamíny	vitamín	k1gInPc1	vitamín
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B2	B2	k1gFnSc1	B2
a	a	k8xC	a
B	B	kA	B
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
výnos	výnos	k1gInSc1	výnos
rýže	rýže	k1gFnSc2	rýže
seté	setý	k2eAgFnSc2d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
při	při	k7c6	při
dvou	dva	k4xCgInPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgFnPc6	tři
sklizních	sklizeň	k1gFnPc6	sklizeň
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
obilovinou	obilovina	k1gFnSc7	obilovina
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zkvašuje	zkvašovat	k5eAaImIp3nS	zkvašovat
a	a	k8xC	a
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
jako	jako	k8xC	jako
arak	arak	k1gInSc1	arak
<g/>
,	,	kIx,	,
rýžové	rýžový	k2eAgNnSc1d1	rýžové
pivo	pivo	k1gNnSc1	pivo
či	či	k8xC	či
víno	víno	k1gNnSc1	víno
<g/>
;	;	kIx,	;
z	z	k7c2	z
rýžové	rýžový	k2eAgFnSc2d1	rýžová
slámy	sláma	k1gFnSc2	sláma
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
cigaretový	cigaretový	k2eAgInSc4d1	cigaretový
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
agrochemických	agrochemický	k2eAgFnPc2d1	agrochemická
firem	firma	k1gFnPc2	firma
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
produkcí	produkce	k1gFnSc7	produkce
rýže	rýže	k1gFnSc2	rýže
její	její	k3xOp3gFnSc7	její
genetickou	genetický	k2eAgFnSc7d1	genetická
modifikací	modifikace	k1gFnSc7	modifikace
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
patentováním	patentování	k1gNnSc7	patentování
takto	takto	k6eAd1	takto
získaného	získaný	k2eAgInSc2d1	získaný
GMO	GMO	kA	GMO
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Bayer	Bayer	k1gMnSc1	Bayer
provedla	provést	k5eAaPmAgFnS	provést
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
odbytu	odbyt	k1gInSc2	odbyt
svého	svůj	k3xOyFgInSc2	svůj
herbicidu	herbicid	k1gInSc2	herbicid
genetickou	genetický	k2eAgFnSc4d1	genetická
modifikaci	modifikace	k1gFnSc4	modifikace
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
odolnost	odolnost	k1gFnSc4	odolnost
GM	GM	kA	GM
rýže	rýže	k1gFnSc1	rýže
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgInSc3	tento
herbicidu	herbicid	k1gInSc3	herbicid
(	(	kIx(	(
<g/>
GM	GM	kA	GM
rýže	rýže	k1gFnSc2	rýže
LL	LL	kA	LL
<g/>
62	[number]	k4	62
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
evropskách	evropska	k1gFnPc6	evropska
laboratořích	laboratoř	k1gFnPc6	laboratoř
mnohé	mnohý	k2eAgFnPc4d1	mnohá
genetické	genetický	k2eAgFnPc4d1	genetická
studie	studie	k1gFnPc4	studie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zvýšení	zvýšení	k1gNnSc2	zvýšení
hektarové	hektarový	k2eAgFnSc2d1	hektarová
produkce	produkce	k1gFnSc2	produkce
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vedla	vést	k5eAaImAgFnS	vést
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
organizace	organizace	k1gFnSc1	organizace
HNGAC	HNGAC	kA	HNGAC
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výzkumy	výzkum	k1gInPc1	výzkum
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
zastaveny	zastavit	k5eAaPmNgInP	zastavit
kvůli	kvůli	k7c3	kvůli
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
financování	financování	k1gNnSc3	financování
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nelegální	legální	k2eNgFnSc1d1	nelegální
kontaminace	kontaminace	k1gFnSc1	kontaminace
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
rýže	rýže	k1gFnSc2	rýže
neschválenými	schválený	k2eNgInPc7d1	neschválený
a	a	k8xC	a
neprověřeným	prověřený	k2eNgInSc7d1	neprověřený
odrůdami	odrůda	k1gFnPc7	odrůda
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgFnPc2d1	modifikovaná
rýží	rýže	k1gFnPc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
odhalily	odhalit	k5eAaPmAgInP	odhalit
odpovědné	odpovědný	k2eAgInPc1d1	odpovědný
orgány	orgán	k1gInPc1	orgán
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
přírodní	přírodní	k2eAgFnSc6d1	přírodní
/	/	kIx~	/
natural	natural	k?	natural
–	–	k?	–
obilky	obilka	k1gFnPc1	obilka
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgFnPc1d1	hnědá
barvy	barva	k1gFnPc1	barva
bílá	bílý	k2eAgFnSc1d1	bílá
–	–	k?	–
s	s	k7c7	s
obilkami	obilka	k1gFnPc7	obilka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dlouhozrnné	dlouhozrnný	k2eAgFnPc1d1	dlouhozrnná
nebo	nebo	k8xC	nebo
krátkozrnné	krátkozrnný	k2eAgFnPc1d1	krátkozrnná
arborio	arborio	k6eAd1	arborio
–	–	k?	–
kulatozrnná	kulatozrnný	k2eAgFnSc1d1	kulatozrnná
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
vhodná	vhodný	k2eAgFnSc1d1	vhodná
do	do	k7c2	do
italského	italský	k2eAgNnSc2d1	italské
rizota	rizoto	k1gNnSc2	rizoto
basmati	basmat	k5eAaImF	basmat
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
kuchyni	kuchyně	k1gFnSc6	kuchyně
jasmínová	jasmínový	k2eAgFnSc1d1	jasmínová
–	–	k?	–
voňavá	voňavý	k2eAgFnSc1d1	voňavá
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
kuchyni	kuchyně	k1gFnSc6	kuchyně
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
bílá	bílý	k2eAgFnSc1d1	bílá
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
do	do	k7c2	do
nákypů	nákyp	k1gInPc2	nákyp
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
kuchyně	kuchyně	k1gFnSc2	kuchyně
dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
Ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
planá	planý	k2eAgFnSc1d1	planá
rýže	rýže	k1gFnSc1	rýže
<g/>
"	"	kIx"	"
–	–	k?	–
pro	pro	k7c4	pro
semena	semeno	k1gNnPc4	semeno
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
vodní	vodní	k2eAgFnSc2d1	vodní
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
pravá	pravý	k2eAgFnSc1d1	pravá
rýže	rýže	k1gFnSc1	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
využití	využití	k1gNnSc1	využití
rýže	rýže	k1gFnSc2	rýže
je	být	k5eAaImIp3nS	být
coby	coby	k?	coby
potraviny	potravina	k1gFnPc4	potravina
–	–	k?	–
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
pokrmů	pokrm	k1gInPc2	pokrm
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
,	,	kIx,	,
masem	maso	k1gNnSc7	maso
<g/>
,	,	kIx,	,
omáčkami	omáčka	k1gFnPc7	omáčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
receptech	recept	k1gInPc6	recept
(	(	kIx(	(
<g/>
rýžový	rýžový	k2eAgInSc1d1	rýžový
nákyp	nákyp	k1gInSc1	nákyp
<g/>
,	,	kIx,	,
rýžový	rýžový	k2eAgInSc1d1	rýžový
puding	puding	k1gInSc4	puding
<g/>
,	,	kIx,	,
rýžové	rýžový	k2eAgFnPc4d1	rýžová
nudle	nudle	k1gFnPc4	nudle
<g/>
,	,	kIx,	,
suši	suš	k1gFnSc6	suš
<g/>
)	)	kIx)	)
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Obilky	obilka	k1gFnPc1	obilka
rýže	rýže	k1gFnSc2	rýže
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
potravin	potravina	k1gFnPc2	potravina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
druh	druh	k1gInSc1	druh
rýže	rýže	k1gFnSc2	rýže
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
O.	O.	kA	O.
sativa	sativa	k1gFnSc1	sativa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
pak	pak	k6eAd1	pak
druh	druh	k1gInSc4	druh
rýže	rýže	k1gFnSc2	rýže
africká	africký	k2eAgFnSc1d1	africká
(	(	kIx(	(
<g/>
O.	O.	kA	O.
glaberrima	glaberrima	k1gFnSc1	glaberrima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
lepek	lepek	k1gInSc4	lepek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Obilky	obilka	k1gFnPc1	obilka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
buď	buď	k8xC	buď
neloupané	loupaný	k2eNgInPc1d1	neloupaný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
loupané	loupaný	k2eAgInPc1d1	loupaný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mletím	mletí	k1gNnSc7	mletí
připravuje	připravovat	k5eAaImIp3nS	připravovat
rýžová	rýžový	k2eAgFnSc1d1	rýžová
mouka	mouka	k1gFnSc1	mouka
<g/>
.	.	kIx.	.
</s>
<s>
Rýžové	rýžový	k2eAgFnPc1d1	rýžová
otruby	otruba	k1gFnPc1	otruba
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
rizika	riziko	k1gNnSc2	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
rýže	rýže	k1gFnSc2	rýže
stabilně	stabilně	k6eAd1	stabilně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
z	z	k7c2	z
asi	asi	k9	asi
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
neloupané	loupaný	k2eNgFnSc2d1	neloupaná
rýže	rýže	k1gFnSc2	rýže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
na	na	k7c4	na
650	[number]	k4	650
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
producenty	producent	k1gMnPc4	producent
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
28,5	[number]	k4	28,5
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
21,7	[number]	k4	21,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
8,8	[number]	k4	8,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
(	(	kIx(	(
<g/>
6,7	[number]	k4	6,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gMnSc1	Myanmar
(	(	kIx(	(
<g/>
5,0	[number]	k4	5,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thajsko	Thajsko	k1gNnSc1	Thajsko
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
asi	asi	k9	asi
6	[number]	k4	6
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
rýže	rýže	k1gFnSc2	rýže
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
obchodováno	obchodován	k2eAgNnSc1d1	obchodováno
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
vývozci	vývozce	k1gMnPc7	vývozce
jsou	být	k5eAaImIp3nP	být
Thajsko	Thajsko	k1gNnSc1	Thajsko
(	(	kIx(	(
<g/>
26	[number]	k4	26
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
vývozu	vývoz	k1gInSc2	vývoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
11	[number]	k4	11
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
dovozcem	dovozce	k1gMnSc7	dovozce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
dovozu	dovoz	k1gInSc2	dovoz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
rýže	rýže	k1gFnSc2	rýže
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
:	:	kIx,	:
Rýže	rýže	k1gFnSc1	rýže
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
ze	z	k7c2	z
světových	světový	k2eAgNnPc2d1	světové
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
buddhismu	buddhismus	k1gInSc6	buddhismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
buddhističtí	buddhistický	k2eAgMnPc1d1	buddhistický
mniši	mnich	k1gMnPc1	mnich
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tzv.	tzv.	kA	tzv.
mandaly	mandal	k1gMnPc4	mandal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
zrna	zrno	k1gNnSc2	zrno
bílé	bílý	k2eAgFnSc2d1	bílá
rýže	rýže	k1gFnSc2	rýže
házejí	házet	k5eAaImIp3nP	házet
na	na	k7c4	na
novomanžele	novomanžel	k1gMnPc4	novomanžel
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rýže	rýže	k1gFnSc2	rýže
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rýže	rýže	k1gFnSc2	rýže
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
rýže	rýže	k1gFnSc2	rýže
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rice	Rice	k1gFnSc1	Rice
Research	Researcha	k1gFnPc2	Researcha
Institute	institut	k1gInSc5	institut
Recepty	recept	k1gInPc7	recept
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
</s>
