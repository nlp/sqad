<s>
Eva	Eva	k1gFnSc1	Eva
Herzigová	Herzigový	k2eAgFnSc1d1	Herzigová
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1973	[number]	k4	1973
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
supermodelka	supermodelka	k1gFnSc1	supermodelka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
modelky	modelka	k1gFnSc2	modelka
začala	začít	k5eAaPmAgNnP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
konkurzu	konkurz	k1gInSc6	konkurz
agentury	agentura	k1gFnSc2	agentura
Metropolitan	metropolitan	k1gInSc1	metropolitan
Models	Models	k1gInSc1	Models
a	a	k8xC	a
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
díky	díky	k7c3	díky
reklamě	reklama	k1gFnSc3	reklama
na	na	k7c4	na
podprsenky	podprsenka	k1gFnPc4	podprsenka
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Miss	miss	k1gFnSc1	miss
Wonderbra	Wonderbra	k1gFnSc1	Wonderbra
a	a	k8xC	a
novináři	novinář	k1gMnPc1	novinář
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monro	k1gFnSc2	Monro
Východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
z	z	k7c2	z
blondýnky	blondýnka	k1gFnSc2	blondýnka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tmavovláska	tmavovláska	k1gFnSc1	tmavovláska
kvůli	kvůli	k7c3	kvůli
kampani	kampaň	k1gFnSc3	kampaň
pro	pro	k7c4	pro
Calvina	Calvin	k2eAgMnSc4d1	Calvin
Kleina	Klein	k1gMnSc4	Klein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
anorexii	anorexie	k1gFnSc6	anorexie
a	a	k8xC	a
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kokainu	kokain	k1gInSc6	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
získala	získat	k5eAaPmAgFnS	získat
zakázky	zakázka	k1gFnPc4	zakázka
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
přehlídky	přehlídka	k1gFnSc2	přehlídka
modelů	model	k1gInPc2	model
Versace	Versace	k1gFnSc2	Versace
i	i	k8xC	i
Huga	Hugo	k1gMnSc2	Hugo
Bosse	boss	k1gMnSc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
a	a	k8xC	a
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
Gérarda	Gérard	k1gMnSc2	Gérard
Depardieuho	Depardieu	k1gMnSc2	Depardieu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
americké	americký	k2eAgFnPc4d1	americká
obdoby	obdoba	k1gFnPc4	obdoba
soutěže	soutěž	k1gFnSc2	soutěž
Chcete	chtít	k5eAaImIp2nP	chtít
být	být	k5eAaImF	být
milionářem	milionář	k1gMnSc7	milionář
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Nechyběla	chybět	k5eNaImAgFnS	chybět
na	na	k7c6	na
titulních	titulní	k2eAgFnPc6d1	titulní
stránkách	stránka	k1gFnPc6	stránka
takových	takový	k3xDgInPc2	takový
časopisů	časopis	k1gInPc2	časopis
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Harpers	Harpers	k1gInSc1	Harpers
bazar	bazar	k1gInSc1	bazar
<g/>
,	,	kIx,	,
Elle	Elle	k1gInSc1	Elle
<g/>
,	,	kIx,	,
Woman	Woman	k1gInSc1	Woman
<g/>
,	,	kIx,	,
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
i	i	k8xC	i
italské	italský	k2eAgFnSc2d1	italská
Vogue	Vogu	k1gFnSc2	Vogu
<g/>
,	,	kIx,	,
Amica	Amicus	k1gMnSc2	Amicus
nebo	nebo	k8xC	nebo
amerického	americký	k2eAgMnSc2d1	americký
Playboye	playboy	k1gMnSc2	playboy
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fotografie	fotografia	k1gFnPc4	fotografia
časopisu	časopis	k1gInSc2	časopis
italského	italský	k2eAgNnSc2d1	italské
vydání	vydání	k1gNnSc2	vydání
Vogue	Vogu	k1gFnSc2	Vogu
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
záběry	záběr	k1gInPc1	záběr
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
Litvínově	Litvínov	k1gInSc6	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
malá	malý	k2eAgFnSc1d1	malá
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
dělala	dělat	k5eAaImAgFnS	dělat
gymnastiku	gymnastika	k1gFnSc4	gymnastika
<g/>
,	,	kIx,	,
košíkovou	košíková	k1gFnSc4	košíková
nebo	nebo	k8xC	nebo
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
fanoušci	fanoušek	k1gMnPc1	fanoušek
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
občas	občas	k6eAd1	občas
vídat	vídat	k5eAaImF	vídat
i	i	k9	i
díky	díky	k7c3	díky
charitativním	charitativní	k2eAgFnPc3d1	charitativní
akcím	akce	k1gFnPc3	akce
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
připravuje	připravovat	k5eAaImIp3nS	připravovat
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kolegyní	kolegyně	k1gFnSc7	kolegyně
Terezou	Tereza	k1gFnSc7	Tereza
Maxovou	Maxová	k1gFnSc7	Maxová
<g/>
.	.	kIx.	.
</s>
<s>
Partnerem	partner	k1gMnSc7	partner
Evy	Eva	k1gFnSc2	Eva
Herzigové	Herzigový	k2eAgNnSc1d1	Herzigové
je	být	k5eAaImIp3nS	být
turínský	turínský	k2eAgMnSc1d1	turínský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Gregorio	Gregorio	k1gMnSc1	Gregorio
Marsiaj	Marsiaj	k1gMnSc1	Marsiaj
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříží	Paříž	k1gFnSc7	Paříž
syn	syn	k1gMnSc1	syn
George	Georg	k1gInPc4	Georg
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Philipe	Philip	k1gInSc5	Philip
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
třetí	třetí	k4xOgMnSc1	třetí
syn	syn	k1gMnSc1	syn
Edward	Edward	k1gMnSc1	Edward
<g/>
.	.	kIx.	.
rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
DNES	dnes	k6eAd1	dnes
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Informace	informace	k1gFnSc1	informace
na	na	k7c4	na
Idnes	Idnes	k1gInSc4	Idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Galerie	galerie	k1gFnSc1	galerie
Eva	Eva	k1gFnSc1	Eva
Herzigová	Herzigový	k2eAgFnSc1d1	Herzigová
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eva	Eva	k1gFnSc1	Eva
Herzigová	Herzigový	k2eAgFnSc1d1	Herzigová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
