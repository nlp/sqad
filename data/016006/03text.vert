<s>
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
</s>
<s>
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
byla	být	k5eAaImAgFnS
setkání	setkání	k1gNnSc4
českých	český	k2eAgFnPc2d1
ženských	ženský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
jednou	jednou	k6eAd1
za	za	k7c4
rok	rok	k1gInSc4
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
až	až	k9
2012	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozia	sympozion	k1gNnPc1
spočívala	spočívat	k5eAaImAgNnP
ve	v	k7c6
společenském	společenský	k2eAgInSc6d1
večeru	večer	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
promlouvaly	promlouvat	k5eAaImAgFnP
významné	významný	k2eAgFnPc4d1
české	český	k2eAgFnPc4d1
ženské	ženský	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
na	na	k7c4
téma	téma	k1gNnSc4
„	„	k?
<g/>
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
mého	můj	k3xOp1gInSc2
osobního	osobní	k2eAgInSc2d1
názoru	názor	k1gInSc2
dobré	dobrá	k1gFnSc2
pro	pro	k7c4
naši	náš	k3xOp1gFnSc4
zemi	zem	k1gFnSc4
a	a	k8xC
kam	kam	k6eAd1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
směřovat	směřovat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Záměrem	záměr	k1gInSc7
sympozií	sympozion	k1gNnPc2
bylo	být	k5eAaImAgNnS
důstojnou	důstojný	k2eAgFnSc7d1
formou	forma	k1gFnSc7
zviditelňovat	zviditelňovat	k5eAaImF
myšlenky	myšlenka	k1gFnPc4
a	a	k8xC
vize	vize	k1gFnPc4
českých	český	k2eAgFnPc2d1
ženských	ženský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sympoziích	sympozion	k1gNnPc6
vystoupilo	vystoupit	k5eAaPmAgNnS
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
tezemi	teze	k1gFnPc7
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
nejvýznamnějších	významný	k2eAgFnPc2d3
žen	žena	k1gFnPc2
českého	český	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
<g/>
,	,	kIx,
kulturního	kulturní	k2eAgInSc2d1
a	a	k8xC
vědeckého	vědecký	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozia	sympozion	k1gNnPc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
podpořena	podpořit	k5eAaPmNgFnS
dopisem	dopis	k1gInSc7
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zdravicí	zdravice	k1gFnSc7
Madeleine	Madeleine	k1gFnSc2
Albrightové	Albrightová	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
na	na	k7c6
sympoziu	sympozion	k1gNnSc6
přednášela	přednášet	k5eAaImAgFnS
budoucí	budoucí	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
slovenské	slovenský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
prezidentská	prezidentský	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
Iveta	Iveta	k1gFnSc1
Radičová	Radičová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
řečnic	řečnice	k1gFnPc2
Sympozií	sympozion	k1gNnPc2
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
centru	centr	k1gInSc6
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Haisová	Haisová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Hybášková	Hybášková	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
Illnerová	Illnerová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Marvanová	Marvanová	k1gFnSc1
<g/>
,	,	kIx,
Meda	Med	k2eAgFnSc1d1
Mládková	Mládková	k1gFnSc1
<g/>
,	,	kIx,
Jiřina	Jiřina	k1gFnSc1
Šiklová	Šiklová	k1gFnSc1
(	(	kIx(
<g/>
její	její	k3xOp3gInSc1
příspěvek	příspěvek	k1gInSc1
byl	být	k5eAaImAgInS
přečten	přečíst	k5eAaPmNgInS
jednou	jednou	k6eAd1
z	z	k7c2
organizátorek	organizátorka	k1gFnPc2
Sympozia	sympozion	k1gNnSc2
Lenkou	Lenka	k1gFnSc7
Bennerovou	Bennerová	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Valterová	Valterová	k1gFnSc1
<g/>
,	,	kIx,
Renáta	Renáta	k1gFnSc1
Vesecká	Vesecká	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
centru	centr	k1gInSc6
<g/>
:	:	kIx,
Jana	Jana	k1gFnSc1
Šilerová	Šilerová	k1gFnSc1
<g/>
,	,	kIx,
Blanka	Blanka	k1gFnSc1
Říhová	Říhová	k1gFnSc1
<g/>
,	,	kIx,
Táňa	Táňa	k1gFnSc1
Fischerová	Fischerová	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Gajdůšková	Gajdůšková	k1gFnSc1
<g/>
,	,	kIx,
Eliška	Eliška	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
<g/>
,	,	kIx,
Rut	rout	k5eAaImNgInS
Kolínská	kolínský	k2eAgFnSc1d1
<g/>
;	;	kIx,
zdravici	zdravice	k1gFnSc4
pronesli	pronést	k5eAaPmAgMnP
Václav	Václav	k1gMnSc1
Pačes	pačes	k1gInSc1
a	a	k8xC
Blanka	Blanka	k1gFnSc1
Knotková-Čapková	Knotková-Čapková	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Kaunickém	Kaunický	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Panské	panský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
:	:	kIx,
Jana	Jana	k1gFnSc1
Šmídová	Šmídová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Benešová	Benešová	k1gFnSc1
<g/>
,	,	kIx,
Iveta	Iveta	k1gFnSc1
Radičová	Radičová	k1gFnSc1
<g/>
,	,	kIx,
Soňa	Soňa	k1gFnSc1
Paukrtová	Paukrtová	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Kaunickém	Kaunický	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Panské	panský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
:	:	kIx,
Milena	Milena	k1gFnSc1
Černá	Černá	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Horváthová	Horváthová	k1gFnSc1
<g/>
,	,	kIx,
Rostya	Rostya	k1gMnSc1
Gordon-Smith	Gordon-Smith	k1gMnSc1
<g/>
,	,	kIx,
Vladimíra	Vladimíra	k1gFnSc1
Dvořáková	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
Seitlová	Seitlová	k1gFnSc1
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
Ritschelová	Ritschelová	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Kaunickém	Kaunický	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Panské	panský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
:	:	kIx,
Iva	Iva	k1gFnSc1
Holmerová	Holmerová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Zamrazilová	Zamrazilová	k1gFnSc1
<g/>
,	,	kIx,
Alexandra	Alexandra	k1gFnSc1
Brabcová	Brabcová	k1gFnSc1
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
Vicenová	Vicenová	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
Centru	centrum	k1gNnSc6
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Dox	Dox	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
partnerská	partnerský	k2eAgFnSc1d1
akce	akce	k1gFnSc1
konference	konference	k1gFnSc2
Forum	forum	k1gNnSc1
2000	#num#	k4
<g/>
:	:	kIx,
Marie	Marie	k1gFnSc1
Čermáková	Čermáková	k1gFnSc1
<g/>
,	,	kIx,
Klára	Klára	k1gFnSc1
Samková	Samková	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Hogenová	Hogenová	k1gFnSc1
<g/>
,	,	kIx,
Táňa	Táňa	k1gFnSc1
Fischerová	Fischerová	k1gFnSc1
<g/>
,	,	kIx,
Rút	Rút	k1gFnSc1
Kolínská	kolínská	k1gFnSc1
<g/>
,	,	kIx,
Adriana	Adriana	k1gFnSc1
Krnáčová	Krnáčová	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
v	v	k7c6
Národním	národní	k2eAgInSc6d1
domě	dům	k1gInSc6
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
:	:	kIx,
Daniela	Daniela	k1gFnSc1
Pěničková	Pěničková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
Krejčí	Krejčí	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Vondráková	Vondráková	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Gajdůšková	Gajdůšková	k1gFnSc1
<g/>
,	,	kIx,
Milena	Milena	k1gFnSc1
Bartlová	Bartlová	k1gFnSc1
</s>
<s>
Organizační	organizační	k2eAgNnSc1d1
zázemí	zázemí	k1gNnSc1
</s>
<s>
Sympozia	sympozion	k1gNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
až	až	k9
2012	#num#	k4
organizována	organizovat	k5eAaBmNgFnS
dobrovolnicemi	dobrovolnice	k1gFnPc7
sdruženými	sdružený	k2eAgFnPc7d1
v	v	k7c4
uskupení	uskupení	k1gNnSc4
s	s	k7c7
názvem	název	k1gInSc7
nadstranická	nadstranický	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
zrodu	zrod	k1gInSc2
tohoto	tento	k3xDgNnSc2
uskupení	uskupení	k1gNnSc2
stála	stát	k5eAaImAgFnS
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
Forum	forum	k1gNnSc1
50	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
také	také	k9
spolupořádala	spolupořádat	k5eAaImAgFnS
všechna	všechen	k3xTgNnPc4
Sympozia	sympozion	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Literární	literární	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
a	a	k8xC
videozáznamy	videozáznam	k1gInPc4
projevů	projev	k1gInPc2
</s>
<s>
Projevy	projev	k1gInPc4
z	z	k7c2
prvních	první	k4xOgInPc2
čtyř	čtyři	k4xCgInPc2
ročníků	ročník	k1gInPc2
Sympozií	sympozion	k1gNnPc2
byly	být	k5eAaImAgInP
zpracovány	zpracován	k2eAgInPc1d1
do	do	k7c2
dvou	dva	k4xCgInPc2
tištěných	tištěný	k2eAgInPc2d1
sborníků	sborník	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Videozáznamy	videozáznam	k1gInPc1
Sympozií	sympozion	k1gNnPc2
z	z	k7c2
let	léto	k1gNnPc2
2009	#num#	k4
až	až	k8xS
2012	#num#	k4
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
PETÁKOVÁ	PETÁKOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
<g/>
,	,	kIx,
editor	editor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
55	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
PETÁKOVÁ	PETÁKOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
<g/>
,	,	kIx,
editor	editor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
uspořádaný	uspořádaný	k2eAgInSc1d1
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
5	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
<g/>
:	:	kIx,
české	český	k2eAgFnSc2d1
ženské	ženský	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
o	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
země	zem	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
101	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7075	#num#	k4
<g/>
-	-	kIx~
<g/>
772	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrn	souhrn	k1gInSc4
údajů	údaj	k1gInPc2
o	o	k7c6
organizačním	organizační	k2eAgNnSc6d1
zázemí	zázemí	k1gNnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
89	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rubrika	rubrika	k1gFnSc1
Ekonomika	ekonomika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2.11	2.11	k4
<g/>
.2009	.2009	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
minutová	minutový	k2eAgFnSc1d1
reportáž	reportáž	k1gFnSc1
o	o	k7c6
Sympoziu	sympozion	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VÁLKOVÁ	Válková	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stovka	stovka	k1gFnSc1
žen	žena	k1gFnPc2
hledá	hledat	k5eAaImIp3nS
prezidentku	prezidentka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystřídá	vystřídat	k5eAaPmIp3nS
na	na	k7c6
Hradě	hrad	k1gInSc6
už	už	k6eAd1
Klause	Klaus	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
,	,	kIx,
21.8	21.8	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BAČOVÁ	BAČOVÁ	kA
<g/>
,	,	kIx,
Veronika	Veronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategie	strategie	k1gFnSc1
podpory	podpora	k1gFnSc2
politické	politický	k2eAgFnSc2d1
participace	participace	k1gFnSc2
žen	žena	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případová	případový	k2eAgFnSc1d1
studie	studie	k1gFnSc1
organizace	organizace	k1gFnSc2
Fórum	fórum	k1gNnSc1
50	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
123	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
52	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Videozáznamy	videozáznam	k1gInPc1
ze	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
.	.	kIx.
ceska-prezidentka	ceska-prezidentka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Videozáznamy	videozáznam	k1gInPc1
z	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
.	.	kIx.
ceska-prezidentka	ceska-prezidentka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Videozáznamy	videozáznam	k1gInPc1
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
(	(	kIx(
<g/>
25.10	25.10	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
)	)	kIx)
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
.	.	kIx.
ceska-prezidentka	ceska-prezidentka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Videozáznamy	videozáznam	k1gInPc1
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
ČP	ČP	kA
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
.	.	kIx.
ceska-prezidentka	ceska-prezidentka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
Česká	český	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
<g/>
.	.	kIx.
ceska-prezidentka	ceska-prezidentka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PETÁKOVÁ	PETÁKOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
<g/>
,	,	kIx,
editor	editor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
55	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
PETÁKOVÁ	PETÁKOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
<g/>
;	;	kIx,
KOTIŠOVÁ	KOTIŠOVÁ	kA
<g/>
,	,	kIx,
Miluš	Miluš	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gender	Gendra	k1gFnPc2
<g/>
,	,	kIx,
rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociologický	sociologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
45	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PETÁKOVÁ	PETÁKOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeňka	k1gFnSc1
<g/>
,	,	kIx,
editor	editor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
uspořádaný	uspořádaný	k2eAgInSc1d1
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
5	#num#	k4
<g/>
.	.	kIx.
sympozia	sympozion	k1gNnSc2
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
<g/>
:	:	kIx,
české	český	k2eAgFnSc2d1
ženské	ženský	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
o	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
země	zem	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
101	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7075	#num#	k4
<g/>
-	-	kIx~
<g/>
772	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
bulletin	bulletin	k1gInSc1
(	(	kIx(
<g/>
časopis	časopis	k1gInSc1
vydávaný	vydávaný	k2eAgInSc1d1
Akademií	akademie	k1gFnSc7
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
.	.	kIx.
</s>
