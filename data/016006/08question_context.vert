<s>
Sympozium	sympozium	k1gNnSc1
České	český	k2eAgFnSc2d1
prezidentky	prezidentka	k1gFnSc2
byla	být	k5eAaImAgFnS
setkání	setkání	k1gNnSc4
českých	český	k2eAgFnPc2d1
ženských	ženský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
jednou	jednou	k6eAd1
za	za	k7c4
rok	rok	k1gInSc4
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
až	až	k9
2012	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>