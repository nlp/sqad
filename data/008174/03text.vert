<p>
<s>
Francis	Francis	k1gInSc1	Francis
Jean	Jean	k1gMnSc1	Jean
Marcel	Marcel	k1gMnSc1	Marcel
Poulenc	Poulenc	k1gFnSc1	Poulenc
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
Paříž	Paříž	k1gFnSc1	Paříž
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1963	[number]	k4	1963
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
šestka	šestka	k1gFnSc1	šestka
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
six	six	k?	six
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Poulenc	Poulenc	k6eAd1	Poulenc
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
farmaceutických	farmaceutický	k2eAgMnPc2d1	farmaceutický
magnátů	magnát	k1gMnPc2	magnát
(	(	kIx(	(
<g/>
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
firma	firma	k1gFnSc1	firma
založená	založený	k2eAgFnSc1d1	založená
jeho	jeho	k3xOp3gFnSc1	jeho
předky	předek	k1gMnPc4	předek
existuje	existovat	k5eAaImIp3nS	existovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
talentovaná	talentovaný	k2eAgFnSc1d1	talentovaná
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
,	,	kIx,	,
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
synovi	syn	k1gMnSc3	syn
základní	základní	k2eAgFnSc2d1	základní
hudební	hudební	k2eAgFnSc2d1	hudební
vzdělání	vzdělání	k1gNnSc3	vzdělání
a	a	k8xC	a
díky	díky	k7c3	díky
bohatému	bohatý	k2eAgNnSc3d1	bohaté
rodinnému	rodinný	k2eAgNnSc3d1	rodinné
zázemí	zázemí	k1gNnSc3	zázemí
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
studovat	studovat	k5eAaImF	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
u	u	k7c2	u
španělského	španělský	k2eAgInSc2d1	španělský
virtuóze	virtuóza	k1gFnSc6	virtuóza
Ricarda	Ricard	k1gMnSc4	Ricard
Viñ	Viñ	k1gMnSc4	Viñ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
samouk	samouk	k1gMnSc1	samouk
<g/>
)	)	kIx)	)
složil	složit	k5eAaPmAgMnS	složit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
Rapsodie	rapsodie	k1gFnSc2	rapsodie
Négre	Négr	k1gInSc5	Négr
pro	pro	k7c4	pro
baryton	baryton	k1gInSc4	baryton
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
senzaci	senzace	k1gFnSc4	senzace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
Poulenc	Poulenc	k1gInSc1	Poulenc
povolán	povolat	k5eAaPmNgInS	povolat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Poulenc	Poulenc	k1gInSc1	Poulenc
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
ve	v	k7c6	v
skladatelské	skladatelský	k2eAgFnSc6d1	skladatelská
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
docházel	docházet	k5eAaImAgMnS	docházet
k	k	k7c3	k
učiteli	učitel	k1gMnSc3	učitel
Charlesovi	Charles	k1gMnSc3	Charles
Koechlinovi	Koechlin	k1gMnSc3	Koechlin
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
pro	pro	k7c4	pro
Poulencův	Poulencův	k2eAgInSc4d1	Poulencův
život	život	k1gInSc4	život
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
zahynul	zahynout	k5eAaPmAgMnS	zahynout
jeho	jeho	k3xOp3gMnSc1	jeho
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
Pierre-Octave	Pierre-Octav	k1gInSc5	Pierre-Octav
Ferroud	Ferrouda	k1gFnPc2	Ferrouda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tragická	tragický	k2eAgFnSc1d1	tragická
událost	událost	k1gFnSc1	událost
podnítila	podnítit	k5eAaPmAgFnS	podnítit
Poulenca	Poulencus	k1gMnSc4	Poulencus
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
objevují	objevovat	k5eAaImIp3nP	objevovat
vážnější	vážní	k2eAgFnPc1d2	vážnější
duchovní	duchovní	k2eAgFnPc1d1	duchovní
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
složil	složit	k5eAaPmAgMnS	složit
Poulanc	Poulanc	k1gFnSc4	Poulanc
své	svůj	k3xOyFgFnSc3	svůj
nejrozsáhlejší	rozsáhlý	k2eAgNnSc1d3	nejrozsáhlejší
dílo	dílo	k1gNnSc1	dílo
-	-	kIx~	-
operu	oprat	k5eAaPmIp1nS	oprat
Dialogy	dialog	k1gInPc4	dialog
karmelitek	karmelitka	k1gFnPc2	karmelitka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
poslední	poslední	k2eAgNnPc1d1	poslední
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
hobojová	hobojový	k2eAgNnPc1d1	hobojový
a	a	k8xC	a
klarinetová	klarinetový	k2eAgFnSc1d1	klarinetová
sonáta	sonáta	k1gFnSc1	sonáta
<g/>
.	.	kIx.	.
</s>
<s>
Poulenc	Poulenc	k6eAd1	Poulenc
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
pochován	pochován	k2eAgMnSc1d1	pochován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Poulenc	Poulenc	k1gFnSc1	Poulenc
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
svébytného	svébytný	k2eAgInSc2d1	svébytný
vtipu	vtip	k1gInSc2	vtip
a	a	k8xC	a
duchaplnosti	duchaplnost	k1gFnSc2	duchaplnost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
pestrý	pestrý	k2eAgInSc1d1	pestrý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
partnerem	partner	k1gMnSc7	partner
byl	být	k5eAaImAgMnS	být
malíř	malíř	k1gMnSc1	malíř
Richard	Richard	k1gMnSc1	Richard
Chanlaire	Chanlair	k1gInSc5	Chanlair
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
muži	muž	k1gMnPc7	muž
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
barytonistou	barytonista	k1gMnSc7	barytonista
Pierrem	Pierr	k1gMnSc7	Pierr
Bernacem	Bernace	k1gMnSc7	Bernace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
napsal	napsat	k5eAaPmAgMnS	napsat
několik	několik	k4yIc1	několik
děl	dělo	k1gNnPc2	dělo
<g/>
)	)	kIx)	)
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zahalené	zahalený	k2eAgFnPc1d1	zahalená
rouškou	rouška	k1gFnSc7	rouška
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
však	však	k9	však
vztahy	vztah	k1gInPc4	vztah
i	i	k8xC	i
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
se	se	k3xPyFc4	se
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Raymonde	Raymond	k1gInSc5	Raymond
Linossierovou	Linossierový	k2eAgFnSc4d1	Linossierový
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
však	však	k9	však
zabránila	zabránit	k5eAaPmAgFnS	zabránit
její	její	k3xOp3gFnSc4	její
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Poulencův	Poulencův	k2eAgInSc4d1	Poulencův
život	život	k1gInSc4	život
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
několik	několik	k4yIc1	několik
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
blízké	blízký	k2eAgMnPc4d1	blízký
přátele	přítel	k1gMnPc4	přítel
<g/>
;	;	kIx,	;
nejdřív	dříve	k6eAd3	dříve
o	o	k7c4	o
spisovatele	spisovatel	k1gMnSc4	spisovatel
Raymonda	Raymond	k1gMnSc4	Raymond
Radigueta	Radiguet	k1gMnSc4	Radiguet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tyfus	tyfus	k1gInSc4	tyfus
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
skladatele	skladatel	k1gMnSc2	skladatel
Pierra-Octava	Pierra-Octava	k1gFnSc1	Pierra-Octava
Ferrouda	Ferrouda	k1gFnSc1	Ferrouda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Claude	Claud	k1gInSc5	Claud
Rostand	Rostand	k1gInSc4	Rostand
o	o	k7c6	o
Poulancovi	Poulanec	k1gMnSc6	Poulanec
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
napůl	napůl	k6eAd1	napůl
darebák	darebák	k1gMnSc1	darebák
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
mnich	mnich	k1gMnSc1	mnich
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
le	le	k?	le
moine	moinout	k5eAaImIp3nS	moinout
et	et	k?	et
le	le	k?	le
voyou	voya	k1gFnSc7	voya
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výrok	výrok	k1gInSc1	výrok
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vžil	vžít	k5eAaPmAgInS	vžít
a	a	k8xC	a
nechybí	chybit	k5eNaPmIp3nS	chybit
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
Poulencově	Poulencův	k2eAgFnSc6d1	Poulencova
biografii	biografie	k1gFnSc6	biografie
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
totiž	totiž	k9	totiž
na	na	k7c6	na
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
polarizaci	polarizace	k1gFnSc6	polarizace
skladatelovy	skladatelův	k2eAgFnSc2d1	skladatelova
osobnosti	osobnost	k1gFnSc2	osobnost
i	i	k8xC	i
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
tvorby	tvorba	k1gFnSc2	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Ony	onen	k3xDgFnPc1	onen
dvě	dva	k4xCgFnPc1	dva
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
kontrastní	kontrastní	k2eAgFnPc1d1	kontrastní
stránky	stránka	k1gFnPc1	stránka
skladatelovy	skladatelův	k2eAgFnPc1d1	skladatelova
osobnosti	osobnost	k1gFnPc1	osobnost
se	se	k3xPyFc4	se
odrazily	odrazit	k5eAaPmAgFnP	odrazit
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
-	-	kIx~	-
zanechal	zanechat	k5eAaPmAgMnS	zanechat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
mnoho	mnoho	k6eAd1	mnoho
odlehčených	odlehčený	k2eAgFnPc2d1	odlehčená
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
sonát	sonáta	k1gFnPc2	sonáta
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hluboká	hluboký	k2eAgFnSc1d1	hluboká
a	a	k8xC	a
závažná	závažný	k2eAgNnPc1d1	závažné
vokální	vokální	k2eAgNnPc1d1	vokální
hudební	hudební	k2eAgNnPc1d1	hudební
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Stylově	stylově	k6eAd1	stylově
má	mít	k5eAaImIp3nS	mít
nejblíž	blízce	k6eAd3	blízce
k	k	k7c3	k
neoklasicismu	neoklasicismus	k1gInSc3	neoklasicismus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spíš	spíš	k9	spíš
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kolegů	kolega	k1gMnPc2	kolega
z	z	k7c2	z
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
šestky	šestka	k1gFnSc2	šestka
se	se	k3xPyFc4	se
důsledněji	důsledně	k6eAd2	důsledně
držel	držet	k5eAaImAgMnS	držet
diatoniky	diatonika	k1gFnSc2	diatonika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
nezvykle	zvykle	k6eNd1	zvykle
bohatá	bohatý	k2eAgFnSc1d1	bohatá
melodická	melodický	k2eAgFnSc1d1	melodická
invence	invence	k1gFnSc1	invence
<g/>
.	.	kIx.	.
</s>
<s>
Poulenc	Poulenc	k1gInSc1	Poulenc
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
neměl	mít	k5eNaImAgMnS	mít
klasické	klasický	k2eAgNnSc4d1	klasické
skladatelské	skladatelský	k2eAgNnSc4d1	skladatelské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hudby	hudba	k1gFnSc2	hudba
nové	nový	k2eAgFnSc2d1	nová
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
nadhled	nadhled	k1gInSc4	nadhled
<g/>
,	,	kIx,	,
vtip	vtip	k1gInSc4	vtip
a	a	k8xC	a
eleganci	elegance	k1gFnSc4	elegance
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
ustálené	ustálený	k2eAgInPc4d1	ustálený
harmonické	harmonický	k2eAgInPc4d1	harmonický
postupy	postup	k1gInPc4	postup
a	a	k8xC	a
používal	používat	k5eAaImAgMnS	používat
své	svůj	k3xOyFgInPc4	svůj
typické	typický	k2eAgInPc4d1	typický
harmonické	harmonický	k2eAgInPc4d1	harmonický
přechody	přechod	k1gInPc4	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
skládal	skládat	k5eAaImAgInS	skládat
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
sám	sám	k3xTgInSc4	sám
jako	jako	k9	jako
interpret	interpret	k1gMnSc1	interpret
vynikal	vynikat	k5eAaImAgMnS	vynikat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vokální	vokální	k2eAgNnPc1d1	vokální
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
více	hodně	k6eAd2	hodně
dechové	dechový	k2eAgInPc1d1	dechový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
než	než	k8xS	než
smyčcové	smyčcový	k2eAgFnPc1d1	smyčcová
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
kritiků	kritik	k1gMnPc2	kritik
na	na	k7c4	na
Poulencovu	Poulencův	k2eAgFnSc4d1	Poulencova
tvorbu	tvorba	k1gFnSc4	tvorba
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ho	on	k3xPp3gNnSc4	on
odmítají	odmítat	k5eAaImIp3nP	odmítat
brát	brát	k5eAaImF	brát
vážně	vážně	k6eAd1	vážně
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
kýčem	kýč	k1gInSc7	kýč
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
ho	on	k3xPp3gMnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nejpřínosnějšího	přínosný	k2eAgMnSc4d3	nejpřínosnější
člena	člen	k1gMnSc4	člen
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
šestky	šestka	k1gFnSc2	šestka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Komorní	komorní	k2eAgFnSc1d1	komorní
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
2	[number]	k4	2
klarinety	klarinet	k1gInPc4	klarinet
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
12	[number]	k4	12
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
suita	suita	k1gFnSc1	suita
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
fagot	fagot	k1gInSc4	fagot
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
32	[number]	k4	32
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
trubku	trubka	k1gFnSc4	trubka
a	a	k8xC	a
trombón	trombón	k1gInSc4	trombón
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
33	[number]	k4	33
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trio	trio	k1gNnSc1	trio
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
<g/>
,	,	kIx,	,
fagot	fagot	k1gInSc4	fagot
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
43	[number]	k4	43
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Suite	Suite	k5eAaPmIp2nP	Suite
française	française	k1gFnPc4	française
pro	pro	k7c4	pro
2	[number]	k4	2
hoboje	hoboj	k1gInPc4	hoboj
<g/>
,	,	kIx,	,
2	[number]	k4	2
fagoty	fagot	k1gInPc1	fagot
<g/>
,	,	kIx,	,
2	[number]	k4	2
trumpety	trumpeta	k1gFnSc2	trumpeta
<g/>
,	,	kIx,	,
3	[number]	k4	3
trombóny	trombón	k1gInPc1	trombón
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnPc1d1	bicí
a	a	k8xC	a
cemballo	cemballo	k1gNnSc1	cemballo
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
80	[number]	k4	80
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sextet	sextet	k1gInSc1	sextet
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
dechový	dechový	k2eAgInSc1d1	dechový
kvintet	kvintet	k1gInSc1	kvintet
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
100	[number]	k4	100
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
119	[number]	k4	119
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
143	[number]	k4	143
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trois	Trois	k1gFnSc1	Trois
mouvements	mouvements	k6eAd1	mouvements
perpétuels	perpétuels	k6eAd1	perpétuels
pro	pro	k7c4	pro
9	[number]	k4	9
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
14	[number]	k4	14
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
164	[number]	k4	164
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elegie	elegie	k1gFnSc1	elegie
pro	pro	k7c4	pro
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
168	[number]	k4	168
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sarabanda	sarabanda	k1gFnSc1	sarabanda
pro	pro	k7c4	pro
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
179	[number]	k4	179
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
184	[number]	k4	184
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
hoboj	hoboj	k1gFnSc4	hoboj
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
185	[number]	k4	185
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
čtyřručně	čtyřručně	k6eAd1	čtyřručně
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Koncerty	koncert	k1gInPc1	koncert
===	===	k?	===
</s>
</p>
<p>
<s>
Concert	Concert	k1gInSc1	Concert
champê	champê	k?	champê
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Concerto	Concerta	k1gFnSc5	Concerta
Choréographique	Choréographique	k1gFnSc5	Choréographique
pour	pour	k1gMnSc1	pour
piano	piano	k1gNnSc1	piano
et	et	k?	et
dix-huit	dixuita	k1gFnPc2	dix-huita
instruments	instruments	k6eAd1	instruments
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Aubade	Aubad	k1gInSc5	Aubad
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
d-moll	dolla	k1gFnPc2	d-molla
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
<g/>
,	,	kIx,	,
smyčce	smyčec	k1gInPc4	smyčec
a	a	k8xC	a
tympány	tympán	k1gInPc4	tympán
g-moll	golla	k1gFnPc2	g-molla
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Písně	píseň	k1gFnPc1	píseň
===	===	k?	===
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Le	Le	k1gMnSc1	Le
Portrait	Portrait	k1gMnSc1	Portrait
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
La	la	k1gNnSc2	la
Grenouillè	Grenouillè	k1gFnSc2	Grenouillè
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Bleuet	Bleueta	k1gFnPc2	Bleueta
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
Banalités	Banalitésa	k1gFnPc2	Banalitésa
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
:	:	kIx,	:
Chanson	Chanson	k1gInSc1	Chanson
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orkenise	Orkenise	k1gFnSc2	Orkenise
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
:	:	kIx,	:
Hôtel	Hôtel	k1gMnSc1	Hôtel
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
:	:	kIx,	:
Fagnes	Fagnes	k1gMnSc1	Fagnes
de	de	k?	de
Wallonie	Wallonie	k1gFnSc2	Wallonie
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
:	:	kIx,	:
Voyage	Voyag	k1gMnSc2	Voyag
à	à	k?	à
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
:	:	kIx,	:
Sanglots	Sanglots	k1gInSc1	Sanglots
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
Paul	Paul	k1gMnSc1	Paul
et	et	k?	et
Virginie	Virginie	k1gFnSc1	Virginie
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vokálně-instrumentální	Vokálněnstrumentální	k2eAgMnPc4d1	Vokálně-instrumentální
===	===	k?	===
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
Prsy	prs	k1gInPc1	prs
Tiresiovy	Tiresiův	k2eAgInPc1d1	Tiresiův
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
mamelles	mamelles	k1gMnSc1	mamelles
de	de	k?	de
Tirésias	Tirésias	k1gMnSc1	Tirésias
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
Dialogy	dialog	k1gInPc1	dialog
karmelitek	karmelitka	k1gFnPc2	karmelitka
(	(	kIx(	(
<g/>
Dialogues	Dialogues	k1gInSc1	Dialogues
des	des	k1gNnSc1	des
Carmélites	Carmélites	k1gInSc1	Carmélites
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Monoopera	Monooper	k1gMnSc4	Monooper
Lidský	lidský	k2eAgInSc1d1	lidský
hlas	hlas	k1gInSc1	hlas
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Voix	Voix	k1gInSc1	Voix
Humaine	Humain	k1gInSc5	Humain
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kantáta	kantáta	k1gFnSc1	kantáta
Lidská	lidský	k2eAgFnSc1d1	lidská
tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
Figure	Figur	k1gMnSc5	Figur
humaine	humain	k1gMnSc5	humain
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
a	a	k8xC	a
cappella	cappella	k1gFnSc1	cappella
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
===	===	k?	===
Sborová	sborový	k2eAgFnSc1d1	sborová
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Mše	mše	k1gFnSc1	mše
G-dur	Gur	k1gMnSc1	G-dur
(	(	kIx(	(
<g/>
SATB	SATB	kA	SATB
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Exultate	Exultat	k1gMnSc5	Exultat
Deo	Deo	k1gMnSc5	Deo
(	(	kIx(	(
<g/>
SATB	SATB	kA	SATB
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Salve	Salve	k1gNnSc1	Salve
regina	regina	k1gMnSc1	regina
(	(	kIx(	(
<g/>
SATB	SATB	kA	SATB
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Un	Un	k?	Un
soir	soir	k1gInSc1	soir
de	de	k?	de
neige	neige	k1gInSc1	neige
(	(	kIx(	(
<g/>
6	[number]	k4	6
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Quatre	Quatr	k1gMnSc5	Quatr
petites	petites	k1gMnSc1	petites
priè	priè	k?	priè
de	de	k?	de
Saint	Saint	k1gInSc1	Saint
François	François	k1gInSc1	François
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Assise	Assise	k1gFnSc1	Assise
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stabat	Stabat	k5eAaImF	Stabat
Mater	mater	k1gFnSc4	mater
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gloria	Gloria	k1gFnSc1	Gloria
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sept	septum	k1gNnPc2	septum
répons	répons	k6eAd1	répons
des	des	k1gNnSc2	des
ténè	ténè	k?	ténè
pro	pro	k7c4	pro
dětský	dětský	k2eAgInSc4d1	dětský
soprán	soprán	k1gInSc4	soprán
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc1d1	mužský
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zdroje	zdroj	k1gInPc1	zdroj
===	===	k?	===
</s>
</p>
<p>
<s>
Cizojazyčné	cizojazyčný	k2eAgFnPc1d1	cizojazyčná
verze	verze	k1gFnPc1	verze
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Francis	Francis	k1gFnSc2	Francis
Poulenc	Poulenc	k1gInSc1	Poulenc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
Francise	Francise	k1gFnSc2	Francise
Poulenca	Poulenc	k1gInSc2	Poulenc
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
skladateli	skladatel	k1gMnPc7	skladatel
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
