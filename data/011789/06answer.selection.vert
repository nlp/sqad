<s>
Řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
veškeré	veškerý	k3xTgFnPc4	veškerý
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
či	či	k8xC	či
vnímání	vnímání	k1gNnSc1	vnímání
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
