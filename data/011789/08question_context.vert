<s>
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řídící	řídící	k2eAgMnSc1d1	řídící
a	a	k8xC	a
integrační	integrační	k2eAgInSc1d1	integrační
orgán	orgán	k1gInSc1	orgán
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
veškeré	veškerý	k3xTgFnPc4	veškerý
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
či	či	k8xC	či
vnímání	vnímání	k1gNnSc1	vnímání
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
