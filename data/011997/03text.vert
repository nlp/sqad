<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Deštném	deštný	k2eAgNnSc6d1	Deštné
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
projektu	projekt	k1gInSc2	projekt
významného	významný	k2eAgMnSc2d1	významný
českého	český	k2eAgMnSc2d1	český
architekta	architekt	k1gMnSc2	architekt
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
Jana	Jan	k1gMnSc2	Jan
Blažeje	Blažej	k1gMnSc2	Blažej
Santiniho	Santini	k1gMnSc2	Santini
–	–	k?	–
Aichela	Aichel	k1gMnSc2	Aichel
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
hraběte	hrabě	k1gMnSc2	hrabě
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Libštejnského	Libštejnského	k2eAgMnSc1d1	Libštejnského
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1723	[number]	k4	1723
–	–	k?	–
1725	[number]	k4	1725
jako	jako	k8xC	jako
novostavba	novostavba	k1gFnSc1	novostavba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
starého	starý	k2eAgInSc2d1	starý
gotického	gotický	k2eAgInSc2d1	gotický
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
už	už	k6eAd1	už
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
farní	farní	k2eAgInSc1d1	farní
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
však	však	k9	však
autorství	autorství	k1gNnSc2	autorství
Santiniho	Santini	k1gMnSc2	Santini
nelze	lze	k6eNd1	lze
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novostavbou	novostavba	k1gFnSc7	novostavba
kostela	kostel	k1gInSc2	kostel
přímo	přímo	k6eAd1	přímo
archivně	archivně	k6eAd1	archivně
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
velmi	velmi	k6eAd1	velmi
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Santini	Santin	k2eAgMnPc1d1	Santin
nestál	stát	k5eNaImAgInS	stát
u	u	k7c2	u
samotného	samotný	k2eAgInSc2d1	samotný
zrodu	zrod	k1gInSc2	zrod
tohoto	tento	k3xDgInSc2	tento
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jeho	jeho	k3xOp3gNnSc2	jeho
autorství	autorství	k1gNnSc2	autorství
totiž	totiž	k9	totiž
hovoří	hovořit	k5eAaImIp3nS	hovořit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
skutečností	skutečnost	k1gFnPc2	skutečnost
vyplývajících	vyplývající	k2eAgFnPc2d1	vyplývající
nejen	nejen	k6eAd1	nejen
ze	z	k7c2	z
Santiniho	Santini	k1gMnSc2	Santini
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktéž	taktéž	k?	taktéž
ze	z	k7c2	z
slohového	slohový	k2eAgInSc2d1	slohový
rozboru	rozbor	k1gInSc2	rozbor
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Deštné	deštný	k2eAgNnSc1d1	Deštné
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
situován	situovat	k5eAaBmNgInS	situovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
údolí	údolí	k1gNnPc2	údolí
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Deštenské	Deštenský	k2eAgFnSc2d1	Deštenská
hornatiny	hornatina	k1gFnSc2	hornatina
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1350	[number]	k4	1350
a	a	k8xC	a
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1362	[number]	k4	1362
spadala	spadat	k5eAaPmAgFnS	spadat
jako	jako	k9	jako
farní	farní	k2eAgFnSc1d1	farní
ves	ves	k1gFnSc1	ves
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
cisterciáků	cisterciák	k1gMnPc2	cisterciák
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
Svaté	svatý	k2eAgFnSc2d1	svatá
Pole	pole	k1gFnSc2	pole
u	u	k7c2	u
Třebechovic	Třebechovice	k1gFnPc2	Třebechovice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
barokního	barokní	k2eAgInSc2d1	barokní
kostela	kostel	k1gInSc2	kostel
nicméně	nicméně	k8xC	nicméně
sehrál	sehrát	k5eAaPmAgInS	sehrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
až	až	k8xS	až
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1640	[number]	k4	1640
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
rychnovské	rychnovský	k2eAgNnSc4d1	rychnovské
panství	panství	k1gNnSc4	panství
za	za	k7c4	za
190	[number]	k4	190
000	[number]	k4	000
zlatých	zlatý	k2eAgNnPc2d1	Zlaté
rýnských	rýnské	k1gNnPc2	rýnské
Albrecht	Albrecht	k1gMnSc1	Albrecht
Libštejnský	Libštejnský	k1gMnSc1	Libštejnský
z	z	k7c2	z
Kolovrat	kolovrat	k1gInSc4	kolovrat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zahájil	zahájit	k5eAaPmAgMnS	zahájit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
období	období	k1gNnSc4	období
významných	významný	k2eAgInPc2d1	významný
stavebních	stavební	k2eAgInPc2d1	stavební
počinů	počin	k1gInPc2	počin
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
samotné	samotný	k2eAgInPc1d1	samotný
Deštné	deštný	k2eAgInPc1d1	deštný
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
černíkovickým	černíkovický	k2eAgNnSc7d1	černíkovický
panstvím	panství	k1gNnSc7	panství
dostává	dostávat	k5eAaImIp3nS	dostávat
po	po	k7c6	po
několika	několik	k4yIc6	několik
změnách	změna	k1gFnPc6	změna
vlastníků	vlastník	k1gMnPc2	vlastník
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kolowrat	Kolowrat	k1gInSc4	Kolowrat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
za	za	k7c4	za
hraběte	hrabě	k1gMnSc4	hrabě
Františka	František	k1gMnSc4	František
Karla	Karel	k1gMnSc4	Karel
I.	I.	kA	I.
Libštejnského	Libštejnský	k1gMnSc4	Libštejnský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
držitelů	držitel	k1gMnPc2	držitel
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
následně	následně	k6eAd1	následně
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Deštném	deštný	k2eAgNnSc6d1	Deštné
projevila	projevit	k5eAaPmAgFnS	projevit
například	například	k6eAd1	například
nejen	nejen	k6eAd1	nejen
postupným	postupný	k2eAgInSc7d1	postupný
přesunem	přesun	k1gInSc7	přesun
tamní	tamní	k2eAgFnSc2d1	tamní
sklárny	sklárna	k1gFnSc2	sklárna
založené	založený	k2eAgFnSc2d1	založená
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1595	[number]	k4	1595
až	až	k9	až
na	na	k7c4	na
úbočí	úbočí	k1gNnPc4	úbočí
Malé	Malá	k1gFnSc2	Malá
Deštné	deštný	k2eAgFnPc4d1	Deštná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
její	její	k3xOp3gFnSc2	její
sklářské	sklářský	k2eAgFnSc2d1	sklářská
produkce	produkce	k1gFnSc2	produkce
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
orientující	orientující	k2eAgFnSc1d1	orientující
na	na	k7c4	na
barokní	barokní	k2eAgNnSc4d1	barokní
luxusní	luxusní	k2eAgNnSc4d1	luxusní
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kolowratský	kolowratský	k2eAgInSc1d1	kolowratský
křišťál	křišťál	k1gInSc1	křišťál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
působícího	působící	k2eAgMnSc2d1	působící
významného	významný	k2eAgMnSc2d1	významný
malíře	malíř	k1gMnSc2	malíř
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
Ignáce	Ignác	k1gMnSc2	Ignác
Preisslera	Preissler	k1gMnSc2	Preissler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Kolowrat	Kolowrat	k1gInSc1	Kolowrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
šlechticů	šlechtic	k1gMnPc2	šlechtic
z	z	k7c2	z
Kolowrat	Kolowrat	k1gInSc1	Kolowrat
však	však	k9	však
samozřejmě	samozřejmě	k6eAd1	samozřejmě
našel	najít	k5eAaPmAgMnS	najít
odezvu	odezva	k1gFnSc4	odezva
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
oblastech	oblast	k1gFnPc6	oblast
–	–	k?	–
zejména	zejména	k9	zejména
však	však	k9	však
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dostáváme	dostávat	k5eAaImIp1nP	dostávat
už	už	k6eAd1	už
k	k	k7c3	k
osobě	osoba	k1gFnSc3	osoba
Santiniho	Santini	k1gMnSc2	Santini
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
majitele	majitel	k1gMnSc4	majitel
rychnovsko	rychnovsko	k6eAd1	rychnovsko
–	–	k?	–
černíkovického	černíkovický	k2eAgNnSc2d1	černíkovický
panství	panství	k1gNnSc2	panství
hraběte	hrabě	k1gMnSc2	hrabě
Norberta	Norbert	k1gMnSc2	Norbert
Leopolda	Leopold	k1gMnSc2	Leopold
Libštejnského	Libštejnský	k1gMnSc2	Libštejnský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gInSc4	Kolowrat
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejpozději	pozdě	k6eAd3	pozdě
roku	rok	k1gInSc2	rok
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
rychnovském	rychnovský	k2eAgNnSc6d1	rychnovské
panství	panství	k1gNnSc6	panství
pak	pak	k6eAd1	pak
nepochybně	pochybně	k6eNd1	pochybně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Rychnově	Rychnov	k1gInSc6	Rychnov
byly	být	k5eAaImAgFnP	být
stavby	stavba	k1gFnPc1	stavba
podle	podle	k7c2	podle
Santiniho	Santini	k1gMnSc2	Santini
projektů	projekt	k1gInPc2	projekt
dokončovány	dokončován	k2eAgFnPc1d1	dokončována
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zakázkami	zakázka	k1gFnPc7	zakázka
pro	pro	k7c4	pro
rychnovské	rychnovský	k2eAgNnSc4d1	rychnovské
panství	panství	k1gNnSc4	panství
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Santini	Santin	k2eAgMnPc1d1	Santin
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1720	[number]	k4	1720
i	i	k9	i
projekt	projekt	k1gInSc1	projekt
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
v	v	k7c6	v
Deštném	deštný	k2eAgNnSc6d1	Deštné
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
společně	společně	k6eAd1	společně
se	s	k7c7	s
zajímavě	zajímavě	k6eAd1	zajímavě
komponovaným	komponovaný	k2eAgInSc7d1	komponovaný
oltářem	oltář	k1gInSc7	oltář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
životopisných	životopisný	k2eAgNnPc2d1	životopisné
dat	datum	k1gNnPc2	datum
Santiniho	Santini	k1gMnSc2	Santini
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
z	z	k7c2	z
období	období	k1gNnSc2	období
realizace	realizace	k1gFnSc2	realizace
projektu	projekt	k1gInSc2	projekt
daného	daný	k2eAgInSc2d1	daný
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dílo	dílo	k1gNnSc4	dílo
zařaditelné	zařaditelný	k2eAgNnSc4d1	zařaditelné
do	do	k7c2	do
pozdní	pozdní	k2eAgFnSc2d1	pozdní
mistrovy	mistrův	k2eAgFnSc2d1	Mistrova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kompozice	kompozice	k1gFnSc2	kompozice
kostela	kostel	k1gInSc2	kostel
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
chladným	chladný	k2eAgNnSc7d1	chladné
a	a	k8xC	a
racionálně	racionálně	k6eAd1	racionálně
hravým	hravý	k2eAgInSc7d1	hravý
geometrismem	geometrismus	k1gInSc7	geometrismus
Santiniho	Santini	k1gMnSc2	Santini
uvažování	uvažování	k1gNnSc4	uvažování
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
jednolodím	jednolodí	k1gNnSc7	jednolodí
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
západní	západní	k2eAgNnSc4d1	západní
průčelí	průčelí	k1gNnSc4	průčelí
se	se	k3xPyFc4	se
však	však	k9	však
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
dvěma	dva	k4xCgInPc3	dva
nakoso	nakoso	k6eAd1	nakoso
vytočenými	vytočený	k2eAgFnPc7d1	vytočená
hranolovými	hranolový	k2eAgFnPc7d1	hranolová
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tříosý	tříosý	k2eAgInSc1d1	tříosý
<g/>
,	,	kIx,	,
konvexně	konvexně	k6eAd1	konvexně
vypnutý	vypnutý	k2eAgInSc1d1	vypnutý
portikus	portikus	k1gInSc1	portikus
členěný	členěný	k2eAgInSc1d1	členěný
slepými	slepý	k2eAgFnPc7d1	slepá
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Deštném	deštný	k2eAgNnSc6d1	Deštné
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xS	jako
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
primárně	primárně	k6eAd1	primárně
určen	určit	k5eAaPmNgInS	určit
centrálním	centrální	k2eAgInSc7d1	centrální
obrazcem	obrazec	k1gInSc7	obrazec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
užitým	užitý	k2eAgInPc3d1	užitý
ve	v	k7c6	v
zmnožení	zmnožení	k1gNnSc6	zmnožení
a	a	k8xC	a
diagonálním	diagonální	k2eAgNnSc6d1	diagonální
pootočení	pootočení	k1gNnSc6	pootočení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interiér	interiér	k1gInSc1	interiér
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
plochostropý	plochostropý	k2eAgInSc1d1	plochostropý
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
kruchta	kruchta	k1gFnSc1	kruchta
na	na	k7c6	na
pilířích	pilíř	k1gInPc6	pilíř
podklenuta	podklenut	k2eAgFnSc1d1	podklenut
křížově	křížově	k6eAd1	křížově
a	a	k8xC	a
valeně	valeně	k6eAd1	valeně
s	s	k7c7	s
výsečemi	výseč	k1gFnPc7	výseč
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
příčně	příčně	k6eAd1	příčně
situovaném	situovaný	k2eAgInSc6d1	situovaný
šestiúhelném	šestiúhelný	k2eAgInSc6d1	šestiúhelný
půdorysu	půdorys	k1gInSc6	půdorys
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
zakončen	zakončit	k5eAaPmNgInS	zakončit
segmentově	segmentově	k6eAd1	segmentově
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zevně	zevně	k6eAd1	zevně
naopak	naopak	k6eAd1	naopak
pravoúhle	pravoúhle	k6eAd1	pravoúhle
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
vybavení	vybavení	k1gNnSc2	vybavení
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
sochařská	sochařský	k2eAgFnSc1d1	sochařská
výzdoba	výzdoba	k1gFnSc1	výzdoba
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
J.	J.	kA	J.
Hartmanna	Hartmann	k1gMnSc4	Hartmann
a	a	k8xC	a
datuje	datovat	k5eAaImIp3nS	datovat
se	se	k3xPyFc4	se
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dva	dva	k4xCgInPc4	dva
boční	boční	k2eAgInPc4d1	boční
tabulové	tabulový	k2eAgInPc4d1	tabulový
oltáře	oltář	k1gInPc4	oltář
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
Utrpení	utrpení	k1gNnPc1	utrpení
Páně	páně	k2eAgNnPc1d1	páně
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
spatřit	spatřit	k5eAaPmF	spatřit
kazatelnu	kazatelna	k1gFnSc4	kazatelna
s	s	k7c7	s
figurálními	figurální	k2eAgInPc7d1	figurální
reliéfy	reliéf	k1gInPc7	reliéf
na	na	k7c6	na
řečništi	řečniště	k1gNnSc6	řečniště
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInPc1d1	barokní
kamennou	kamenný	k2eAgFnSc4d1	kamenná
křtitelnici	křtitelnice	k1gFnSc4	křtitelnice
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
Křížové	Křížové	k2eAgFnSc2d1	Křížové
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
již	již	k9	již
ve	v	k7c6	v
venkovních	venkovní	k2eAgFnPc6d1	venkovní
prostorách	prostora	k1gFnPc6	prostora
nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
i	i	k8xC	i
sousoší	sousoší	k1gNnSc4	sousoší
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
se	s	k7c7	s
sv.	sv.	kA	sv.
Josefem	Josef	k1gMnSc7	Josef
<g/>
,	,	kIx,	,
Jáchymem	Jáchym	k1gMnSc7	Jáchym
a	a	k8xC	a
Annou	Anna	k1gFnSc7	Anna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
svědčících	svědčící	k2eAgMnPc2d1	svědčící
pro	pro	k7c4	pro
Santiniho	Santini	k1gMnSc2	Santini
autorství	autorství	k1gNnSc2	autorství
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
patrné	patrný	k2eAgNnSc1d1	patrné
výrazně	výrazně	k6eAd1	výrazně
redukované	redukovaný	k2eAgNnSc1d1	redukované
<g/>
,	,	kIx,	,
nedekorativní	dekorativní	k2eNgNnSc1d1	dekorativní
a	a	k8xC	a
lineární	lineární	k2eAgNnSc1d1	lineární
tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
stavby	stavba	k1gFnSc2	stavba
s	s	k7c7	s
hladkými	hladký	k2eAgFnPc7d1	hladká
a	a	k8xC	a
nečleněnými	členěný	k2eNgFnPc7d1	nečleněná
fasádami	fasáda	k1gFnPc7	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
budí	budit	k5eAaImIp3nS	budit
taktéž	taktéž	k?	taktéž
relativně	relativně	k6eAd1	relativně
nezvyklý	zvyklý	k2eNgInSc4d1	nezvyklý
půdorys	půdorys	k1gInSc4	půdorys
jak	jak	k8xS	jak
kněžiště	kněžiště	k1gNnSc4	kněžiště
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ale	ale	k9	ale
i	i	k9	i
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
formován	formovat	k5eAaImNgInS	formovat
postupem	postup	k1gInSc7	postup
kvadriangulace	kvadriangulace	k1gFnSc2	kvadriangulace
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
v	v	k7c6	v
Chrašicích	Chrašice	k1gFnPc6	Chrašice
<g/>
,	,	kIx,	,
o	o	k7c4	o
jehož	jehož	k3xOyRp3gInSc4	jehož
vznik	vznik	k1gInSc4	vznik
se	se	k3xPyFc4	se
Santini	Santin	k2eAgMnPc1d1	Santin
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
opět	opět	k6eAd1	opět
skrze	skrze	k?	skrze
vytvoření	vytvoření	k1gNnSc4	vytvoření
návrhu	návrh	k1gInSc2	návrh
v	v	k7c6	v
bezprostředně	bezprostředně	k6eAd1	bezprostředně
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BARTH	BARTH	kA	BARTH
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gInSc1	Fritz
<g/>
:	:	kIx,	:
Santini	Santin	k2eAgMnPc1d1	Santin
1677	[number]	k4	1677
<g/>
-	-	kIx~	-
<g/>
1723	[number]	k4	1723
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Baumeister	Baumeister	k1gMnSc1	Baumeister
des	des	k1gNnSc2	des
Barock	Barock	k1gMnSc1	Barock
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
.	.	kIx.	.
</s>
<s>
Ostfildern	Ostfildern	k1gNnSc1	Ostfildern
:	:	kIx,	:
Hatje	Hatje	k1gFnSc1	Hatje
Cantz	Cantz	k1gMnSc1	Cantz
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
s.	s.	k?	s.
87	[number]	k4	87
<g/>
,	,	kIx,	,
246	[number]	k4	246
<g/>
,	,	kIx,	,
419	[number]	k4	419
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7757	[number]	k4	7757
<g/>
-	-	kIx~	-
<g/>
1468	[number]	k4	1468
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HORYNA	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Mojmír	Mojmír	k1gMnSc1	Mojmír
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
s.	s.	k?	s.
174	[number]	k4	174
<g/>
,	,	kIx,	,
386	[number]	k4	386
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
664	[number]	k4	664
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POCHE	POCHE	kA	POCHE
<g/>
,	,	kIx,	,
Emanuel	Emanuela	k1gFnPc2	Emanuela
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
A-	A-	k1gFnSc1	A-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
s.	s.	k?	s.
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
258	[number]	k4	258
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
na	na	k7c4	na
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
