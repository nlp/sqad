<s desamb="1">
Samce	samec	k1gInSc2
od	od	k7c2
samice	samice	k1gFnSc2
lze	lze	k6eAd1
rozpoznat	rozpoznat	k5eAaPmF
pomocí	pomocí	k7c2
pářicích	pářicí	k2eAgInPc2d1
mozolů	mozol	k1gInPc2
na	na	k7c6
palci	palec	k1gInSc6
přední	přední	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
samce	samec	k1gInSc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc7d2
velikostí	velikost	k1gFnSc7
a	a	k8xC
hlavně	hlavně	k9
velice	velice	k6eAd1
příjemného	příjemný	k2eAgInSc2d1
hlasového	hlasový	k2eAgInSc2d1
projevu	projev	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
připomíná	připomínat	k5eAaImIp3nS
hlas	hlas	k1gInSc4
ptáka	pták	k1gMnSc2
a	a	k8xC
zní	znět	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
crrrríííííí	crrrríííííí	k0
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>