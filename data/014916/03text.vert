<s>
Ropucha	ropucha	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Ropucha	ropucha	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
Ropucha	ropucha	k1gFnSc1
zelená	zelený	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
obojživelníci	obojživelník	k1gMnPc1
(	(	kIx(
<g/>
Amphibia	Amphibia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
žáby	žába	k1gFnPc1
(	(	kIx(
<g/>
Anura	Anura	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
ropuchovití	ropuchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Bufonidae	Bufonidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
ropucha	ropucha	k1gFnSc1
(	(	kIx(
<g/>
Bufo	bufa	k1gFnSc5
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Bufotes	Bufotes	k1gMnSc1
viridisLaurenti	viridisLaurent	k1gMnPc1
<g/>
,	,	kIx,
1768	#num#	k4
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Bufo	bufa	k1gFnSc5
viridis	viridis	k1gFnSc5
</s>
<s>
Bufo	bufa	k1gFnSc5
cursor	cursor	k1gMnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ropucha	ropucha	k1gFnSc1
zelená	zelená	k1gFnSc1
(	(	kIx(
<g/>
Bufotes	Bufotes	k1gInSc1
viridis	viridis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
žáby	žába	k1gFnSc2
z	z	k7c2
čeledě	čeleď	k1gFnSc2
ropuchovití	ropuchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Bufonidae	Bufonidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ropucha	ropucha	k1gFnSc1
zelená	zelenat	k5eAaImIp3nS
velikostí	velikost	k1gFnSc7
málokdy	málokdy	k6eAd1
přesáhne	přesáhnout	k5eAaPmIp3nS
8	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oční	oční	k2eAgFnSc1d1
duhovka	duhovka	k1gFnSc1
je	být	k5eAaImIp3nS
žlutá	žlutý	k2eAgFnSc1d1
až	až	k6eAd1
zelenavá	zelenavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvení	zbarvení	k1gNnSc1
hřbetu	hřbet	k1gInSc2
je	být	k5eAaImIp3nS
bělavé	bělavý	k2eAgNnSc1d1
s	s	k7c7
zelenými	zelený	k2eAgFnPc7d1
až	až	k8xS
olivovými	olivový	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břicho	břicho	k1gNnSc1
je	být	k5eAaImIp3nS
bílé	bílý	k2eAgNnSc1d1
až	až	k6eAd1
nažloutlé	nažloutlý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samce	samec	k1gInSc2
od	od	k7c2
samice	samice	k1gFnSc2
lze	lze	k6eAd1
rozpoznat	rozpoznat	k5eAaPmF
pomocí	pomocí	k7c2
pářicích	pářicí	k2eAgInPc2d1
mozolů	mozol	k1gInPc2
na	na	k7c6
palci	palec	k1gInSc6
přední	přední	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
samce	samec	k1gInSc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc7d2
velikostí	velikost	k1gFnSc7
a	a	k8xC
hlavně	hlavně	k9
velice	velice	k6eAd1
příjemného	příjemný	k2eAgInSc2d1
hlasového	hlasový	k2eAgInSc2d1
projevu	projev	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
připomíná	připomínat	k5eAaImIp3nS
hlas	hlas	k1gInSc4
ptáka	pták	k1gMnSc2
a	a	k8xC
zní	znět	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
crrrríííííí	crrrríííííí	k0
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
V	v	k7c6
ČR	ČR	kA
i	i	k8xC
SR	SR	kA
je	být	k5eAaImIp3nS
v	v	k7c6
téměř	téměř	k6eAd1
souvislých	souvislý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
je	být	k5eAaImIp3nS
rozšířena	rozšířit	k5eAaPmNgFnS
po	po	k7c6
střední	střední	k2eAgFnSc6d1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
Arábie	Arábie	k1gFnSc2
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k6eAd1
to	ten	k3xDgNnSc1
žije	žít	k5eAaImIp3nS
na	na	k7c6
ostrovech	ostrov	k1gInPc6
západního	západní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
a	a	k8xC
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
nás	my	k3xPp1nPc2
v	v	k7c6
nížinách	nížina	k1gFnPc6
na	na	k7c6
vesnicích	vesnice	k1gFnPc6
místy	místy	k6eAd1
i	i	k9
hojná	hojný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ekologie	ekologie	k1gFnSc1
a	a	k8xC
biotop	biotop	k1gInSc1
</s>
<s>
Vyhýbá	vyhýbat	k5eAaImIp3nS
se	se	k3xPyFc4
chladnějším	chladný	k2eAgFnPc3d2
a	a	k8xC
horským	horský	k2eAgFnPc3d1
oblastem	oblast	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
v	v	k7c6
polohách	poloha	k1gFnPc6
do	do	k7c2
450	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Žije	žít	k5eAaImIp3nS
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
i	i	k8xC
zahradách	zahrada	k1gFnPc6
<g/>
,	,	kIx,
parcích	park	k1gInPc6
<g/>
,	,	kIx,
polích	pole	k1gNnPc6
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
jezírek	jezírko	k1gNnPc2
a	a	k8xC
rybníků	rybník	k1gInPc2
a	a	k8xC
také	také	k9
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
lidského	lidský	k2eAgNnSc2d1
obydlí	obydlí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
všech	všecek	k3xTgMnPc2
našich	náš	k3xOp1gMnPc2
obojživelníků	obojživelník	k1gMnPc2
nejlépe	dobře	k6eAd3
snáší	snášet	k5eAaImIp3nS
teplo	teplo	k1gNnSc1
a	a	k8xC
sucho	sucho	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
vyhledává	vyhledávat	k5eAaImIp3nS
menší	malý	k2eAgFnSc2d2
prohřáté	prohřátý	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
<g/>
,	,	kIx,
nevyhýbá	vyhýbat	k5eNaImIp3nS
se	se	k3xPyFc4
i	i	k9
větším	veliký	k2eAgFnPc3d2
kalužím	kaluž	k1gFnPc3
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
bez	bez	k7c2
vodní	vodní	k2eAgFnSc2d1
vegetace	vegetace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provazce	provazec	k1gInSc2
ropuchy	ropucha	k1gFnSc2
zelené	zelená	k1gFnSc2
jsou	být	k5eAaImIp3nP
kratší	krátký	k2eAgInPc1d2
než	než	k8xS
u	u	k7c2
ropuchy	ropucha	k1gFnSc2
obecné	obecná	k1gFnSc2
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
do	do	k7c2
6000	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pulci	pulec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mohou	moct	k5eAaImIp3nP
dosahovat	dosahovat	k5eAaImF
až	až	k9
50	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
líhnou	líhnout	k5eAaImIp3nP
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
aktivita	aktivita	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
převážné	převážný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
noční	noční	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
rozmnožování	rozmnožování	k1gNnSc2
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
zastihnout	zastihnout	k5eAaPmF
i	i	k9
za	za	k7c2
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Dungel	Dungel	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Řehák	Řehák	k1gMnSc1
–	–	k?
Atlas	Atlas	k1gInSc1
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
obojživelníků	obojživelník	k1gMnPc2
a	a	k8xC
plazů	plaz	k1gMnPc2
České	český	k2eAgFnSc2d1
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Čambal	Čambal	k1gInSc1
Štefan	Štefan	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojživelníky	obojživelník	k1gMnPc7
a	a	k8xC
plazy	plaz	k1gMnPc7
mesta	mesta	k1gFnSc1
Levice	levice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tekovské	Tekovské	k2eAgInSc1d1
múzeum	múzeum	k1gInSc1
<g/>
,	,	kIx,
Levice	levice	k1gFnSc1
<g/>
.	.	kIx.
16	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0-00-219318-3	0-00-219318-3	k4
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88831	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čambal	Čambat	k5eAaImAgMnS,k5eAaPmAgMnS
Štefan	Štefan	k1gMnSc1
<g/>
,	,	kIx,
Hoffmann	Hoffmann	k1gMnSc1
Peter	Peter	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Obojživelníky	obojživelník	k1gMnPc4
a	a	k8xC
plazy	plaz	k1gMnPc4
našej	našej	k1gInSc4
prírody	prírod	k1gInPc7
<g/>
,	,	kIx,
Tekovské	Tekovský	k2eAgNnSc1d1
múzeum	múzeum	k1gNnSc1
<g/>
,	,	kIx,
Levice	levice	k1gFnSc1
<g/>
,	,	kIx,
64	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Obojživelníci	obojživelník	k1gMnPc1
</s>
