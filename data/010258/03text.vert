<p>
<s>
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
nesmrtelnosti	nesmrtelnost	k1gFnPc1	nesmrtelnost
a	a	k8xC	a
práva	právo	k1gNnPc1	právo
člověka	člověk	k1gMnSc2	člověk
prodlužovat	prodlužovat	k5eAaImF	prodlužovat
si	se	k3xPyFc3	se
uměle	uměle	k6eAd1	uměle
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
dějství	dějství	k1gNnSc6	dějství
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
doktor	doktor	k1gMnSc1	doktor
Kolenatý	kolenatý	k2eAgInSc4d1	kolenatý
svůj	svůj	k3xOyFgInSc4	svůj
probíhající	probíhající	k2eAgInSc4d1	probíhající
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
již	již	k6eAd1	již
po	po	k7c4	po
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
divákovi	divákův	k2eAgMnPc1d1	divákův
(	(	kIx(	(
<g/>
čtenáři	čtenář	k1gMnPc1	čtenář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
zpěvačce	zpěvačka	k1gFnSc6	zpěvačka
Emílii	Emílie	k1gFnSc6	Emílie
Marty	Marta	k1gFnSc2	Marta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zná	znát	k5eAaImIp3nS	znát
některé	některý	k3yIgFnPc4	některý
podrobnosti	podrobnost	k1gFnPc4	podrobnost
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
ví	vědět	k5eAaImIp3nS	vědět
o	o	k7c6	o
důležitých	důležitý	k2eAgInPc6d1	důležitý
důkazech	důkaz	k1gInPc6	důkaz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
ozřejmit	ozřejmit	k5eAaPmF	ozřejmit
celý	celý	k2eAgInSc4d1	celý
případ	případ	k1gInSc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
může	moct	k5eAaImIp3nS	moct
Emílii	Emílie	k1gFnSc4	Emílie
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
šílence	šílenec	k1gMnPc4	šílenec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
doktor	doktor	k1gMnSc1	doktor
Kolenatý	kolenatý	k2eAgMnSc1d1	kolenatý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc3	on
může	moct	k5eAaImIp3nS	moct
důvěřovat	důvěřovat	k5eAaImF	důvěřovat
jako	jako	k9	jako
Albert	Albert	k1gMnSc1	Albert
Gregor	Gregor	k1gMnSc1	Gregor
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
případu	případ	k1gInSc2	případ
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
týkají	týkat	k5eAaImIp3nP	týkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
žena	žena	k1gFnSc1	žena
působí	působit	k5eAaImIp3nS	působit
ještě	ještě	k9	ještě
záhadněji	záhadně	k6eAd2	záhadně
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
popisované	popisovaný	k2eAgInPc1d1	popisovaný
dokumenty	dokument	k1gInPc1	dokument
skutečně	skutečně	k6eAd1	skutečně
najdou	najít	k5eAaPmIp3nP	najít
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Emílie	Emílie	k1gFnSc1	Emílie
se	se	k3xPyFc4	se
nesnaží	snažit	k5eNaImIp3nS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
vyřešit	vyřešit	k5eAaPmF	vyřešit
soudní	soudní	k2eAgInSc4d1	soudní
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
zpočátku	zpočátku	k6eAd1	zpočátku
oprávněně	oprávněně	k6eAd1	oprávněně
myslet	myslet	k5eAaImF	myslet
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
dopisy	dopis	k1gInPc4	dopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
uchovány	uchovat	k5eAaPmNgInP	uchovat
společně	společně	k6eAd1	společně
se	s	k7c7	s
závětí	závěť	k1gFnSc7	závěť
v	v	k7c6	v
Prusově	Prusův	k2eAgInSc6d1	Prusův
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
jí	on	k3xPp3gFnSc3	on
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ostatními	ostatní	k2eAgFnPc7d1	ostatní
(	(	kIx(	(
<g/>
především	především	k9	především
Gregorem	Gregor	k1gMnSc7	Gregor
<g/>
)	)	kIx)	)
považována	považován	k2eAgNnPc1d1	považováno
téměř	téměř	k6eAd1	téměř
za	za	k7c4	za
spasitelku	spasitelka	k1gFnSc4	spasitelka
<g/>
,	,	kIx,	,
a	a	k8xC	a
blahosklonně	blahosklonně	k6eAd1	blahosklonně
přijímá	přijímat	k5eAaImIp3nS	přijímat
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC	a
z	z	k7c2	z
úspěchů	úspěch	k1gInPc2	úspěch
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
Gregor	Gregor	k1gMnSc1	Gregor
versus	versus	k7c1	versus
Prus	Prus	k1gMnSc1	Prus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Albert	Albert	k1gMnSc1	Albert
Gregor	Gregor	k1gMnSc1	Gregor
proti	proti	k7c3	proti
Josefu	Josef	k1gMnSc3	Josef
Prusovi	Prus	k1gMnSc3	Prus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
vyjasnění	vyjasnění	k1gNnSc3	vyjasnění
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zamotává	zamotávat	k5eAaImIp3nS	zamotávat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
prokázat	prokázat	k5eAaPmF	prokázat
totožnost	totožnost	k1gFnSc4	totožnost
hlavních	hlavní	k2eAgMnPc2d1	hlavní
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
především	především	k9	především
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Makropula	Makropula	k1gFnSc1	Makropula
s	s	k7c7	s
Ellian	Elliana	k1gFnPc2	Elliana
Mac	Mac	kA	Mac
Gregor	Gregor	k1gMnSc1	Gregor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
dějství	dějství	k1gNnSc1	dějství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Emíliina	Emíliin	k2eAgInSc2d1	Emíliin
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
kromě	kromě	k7c2	kromě
přijímání	přijímání	k1gNnSc2	přijímání
gratulací	gratulace	k1gFnPc2	gratulace
k	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
výkonu	výkon	k1gInSc3	výkon
od	od	k7c2	od
okouzlených	okouzlený	k2eAgMnPc2d1	okouzlený
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
vyjednává	vyjednávat	k5eAaImIp3nS	vyjednávat
s	s	k7c7	s
Prusem	Prus	k1gMnSc7	Prus
a	a	k8xC	a
Gregorem	Gregor	k1gMnSc7	Gregor
o	o	k7c6	o
zalepeném	zalepený	k2eAgInSc6d1	zalepený
dopise	dopis	k1gInSc6	dopis
určeném	určený	k2eAgInSc6d1	určený
"	"	kIx"	"
<g/>
synu	syn	k1gMnSc3	syn
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
nalezenými	nalezený	k2eAgInPc7d1	nalezený
dokumenty	dokument	k1gInPc7	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
ho	on	k3xPp3gInSc4	on
nechce	chtít	k5eNaImIp3nS	chtít
vydat	vydat	k5eAaPmF	vydat
Josef	Josef	k1gMnSc1	Josef
Prus	Prus	k1gMnSc1	Prus
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
vymámit	vymámit	k5eAaPmF	vymámit
na	na	k7c6	na
jeho	jeho	k3xOp3gMnSc6	jeho
synu	syn	k1gMnSc6	syn
Jankovi	Janek	k1gMnSc6	Janek
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
rovněž	rovněž	k6eAd1	rovněž
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
zpěvaččino	zpěvaččin	k2eAgNnSc4d1	zpěvaččino
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
Janka	Janka	k1gFnSc1	Janka
Prusových	Prusův	k2eAgFnPc2d1	Prusova
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkáváme	setkávat	k5eAaImIp1nP	setkávat
opět	opět	k6eAd1	opět
s	s	k7c7	s
Kristinou	Kristin	k2eAgFnSc7d1	Kristina
(	(	kIx(	(
<g/>
milou	milá	k1gFnSc7	milá
Janka	Janek	k1gMnSc2	Janek
Pruse	Prus	k1gMnSc5	Prus
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
solicitátora	solicitátor	k1gMnSc2	solicitátor
Vítka	Vítek	k1gMnSc2	Vítek
z	z	k7c2	z
advokátní	advokátní	k2eAgFnSc2d1	advokátní
kanceláře	kancelář	k1gFnSc2	kancelář
doktora	doktor	k1gMnSc2	doktor
Kolenatého	kolenatý	k2eAgMnSc2d1	kolenatý
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nS	toužit
stát	stát	k5eAaImF	stát
výtečnou	výtečný	k2eAgFnSc7d1	výtečná
a	a	k8xC	a
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
a	a	k8xC	a
Emílii	Emílie	k1gFnSc3	Emílie
bezmezně	bezmezně	k6eAd1	bezmezně
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
po	po	k7c6	po
profesionální	profesionální	k2eAgFnSc6d1	profesionální
stránce	stránka	k1gFnSc6	stránka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Gregorem	Gregor	k1gMnSc7	Gregor
<g/>
,	,	kIx,	,
s	s	k7c7	s
Vítkem	Vítek	k1gMnSc7	Vítek
a	a	k8xC	a
nově	nově	k6eAd1	nově
s	s	k7c7	s
Hauk-Šendorfem	Hauk-Šendorf	k1gInSc7	Hauk-Šendorf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Emílii	Emílie	k1gFnSc4	Emílie
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
španělskou	španělský	k2eAgFnSc4d1	španělská
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
a	a	k8xC	a
Eugenii	Eugenie	k1gFnSc4	Eugenie
Montez	Monteza	k1gFnPc2	Monteza
<g/>
.	.	kIx.	.
</s>
<s>
Celkovou	celkový	k2eAgFnSc4d1	celková
atmosféru	atmosféra	k1gFnSc4	atmosféra
divadla	divadlo	k1gNnSc2	divadlo
po	po	k7c6	po
představení	představení	k1gNnSc6	představení
dokreslují	dokreslovat	k5eAaImIp3nP	dokreslovat
postavy	postava	k1gFnPc4	postava
poklízečky	poklízečka	k1gFnSc2	poklízečka
a	a	k8xC	a
strojníka	strojník	k1gMnSc2	strojník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
dějství	dějství	k1gNnSc1	dějství
začíná	začínat	k5eAaImIp3nS	začínat
ránem	ráno	k1gNnSc7	ráno
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Prus	Prus	k1gMnSc1	Prus
předává	předávat	k5eAaImIp3nS	předávat
Emílii	Emílie	k1gFnSc4	Emílie
obálku	obálka	k1gFnSc4	obálka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jí	jíst	k5eAaImIp3nS	jíst
slíbil	slíbit	k5eAaPmAgMnS	slíbit
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
stráví	strávit	k5eAaPmIp3nS	strávit
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
tak	tak	k6eAd1	tak
předešel	předejít	k5eAaPmAgMnS	předejít
i	i	k9	i
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
Janek	Janka	k1gFnPc2	Janka
obálku	obálka	k1gFnSc4	obálka
pro	pro	k7c4	pro
Emílii	Emílie	k1gFnSc4	Emílie
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mladík	mladík	k1gMnSc1	mladík
ze	z	k7c2	z
žárlivosti	žárlivost	k1gFnSc2	žárlivost
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
podpis	podpis	k1gInSc1	podpis
Ellian	Elliana	k1gFnPc2	Elliana
Mac	Mac	kA	Mac
Gregor	Gregor	k1gMnSc1	Gregor
na	na	k7c6	na
vlastnoručním	vlastnoruční	k2eAgNnSc6d1	vlastnoruční
prohlášení	prohlášení	k1gNnSc6	prohlášení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
matkou	matka	k1gFnSc7	matka
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Gregora	Gregor	k1gMnSc2	Gregor
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
podobá	podobat	k5eAaImIp3nS	podobat
podpisu	podpis	k1gInSc3	podpis
Emílie	Emílie	k1gFnSc2	Emílie
Marty	Marta	k1gFnSc2	Marta
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obdarovala	obdarovat	k5eAaPmAgFnS	obdarovat
svou	svůj	k3xOyFgFnSc4	svůj
obdivovatelku	obdivovatelka	k1gFnSc4	obdivovatelka
Kristinu	Kristina	k1gFnSc4	Kristina
<g/>
,	,	kIx,	,
přicházejí	přicházet	k5eAaImIp3nP	přicházet
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
od	od	k7c2	od
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
opilá	opilý	k2eAgFnSc1d1	opilá
Emílie	Emílie	k1gFnSc1	Emílie
v	v	k7c6	v
absurdně	absurdně	k6eAd1	absurdně
zinscenovaném	zinscenovaný	k2eAgInSc6d1	zinscenovaný
procesu	proces	k1gInSc6	proces
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
rudolfínského	rudolfínský	k2eAgMnSc2d1	rudolfínský
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
císaře	císař	k1gMnSc4	císař
objevil	objevit	k5eAaPmAgInS	objevit
elixír	elixír	k1gInSc1	elixír
mládí	mládí	k1gNnSc2	mládí
na	na	k7c4	na
300	[number]	k4	300
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
ho	on	k3xPp3gNnSc4	on
právě	právě	k6eAd1	právě
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Elina	Elina	k1gFnSc1	Elina
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
Emíliino	Emíliin	k2eAgNnSc4d1	Emíliin
pravé	pravý	k2eAgNnSc4d1	pravé
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
337	[number]	k4	337
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
ji	on	k3xPp3gFnSc4	on
život	život	k1gInSc1	život
nudí	nudit	k5eAaImIp3nS	nudit
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
chce	chtít	k5eAaImIp3nS	chtít
získat	získat	k5eAaPmF	získat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
návod	návod	k1gInSc4	návod
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
elixíru	elixír	k1gInSc2	elixír
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
procesu	proces	k1gInSc2	proces
si	se	k3xPyFc3	se
však	však	k9	však
sama	sám	k3xTgFnSc1	sám
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelidsky	lidsky	k6eNd1	lidsky
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgMnSc1d2	horší
než	než	k8xS	než
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vzdávají	vzdávat	k5eAaImIp3nP	vzdávat
plánů	plán	k1gInPc2	plán
s	s	k7c7	s
receptem	recept	k1gInSc7	recept
na	na	k7c4	na
věčný	věčný	k2eAgInSc4d1	věčný
život	život	k1gInSc4	život
a	a	k8xC	a
nechávají	nechávat	k5eAaImIp3nP	nechávat
Kristinu	Kristina	k1gFnSc4	Kristina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pergamen	pergamen	k1gInSc4	pergamen
spálila	spálit	k5eAaPmAgFnS	spálit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
působící	působící	k2eAgFnSc1d1	působící
jako	jako	k8xS	jako
femme	femmat	k5eAaPmIp3nS	femmat
fatale	fatale	k6eAd1	fatale
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
věk	věk	k1gInSc4	věk
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
přítomní	přítomný	k1gMnPc1	přítomný
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
autor	autor	k1gMnSc1	autor
nazývá	nazývat	k5eAaImIp3nS	nazývat
Emílie	Emílie	k1gFnPc4	Emílie
Marty	Marta	k1gFnSc2	Marta
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
oslovovat	oslovovat	k5eAaImF	oslovovat
Ellian	Ellian	k1gMnSc1	Ellian
Mac	Mac	kA	Mac
Gregor	Gregor	k1gMnSc1	Gregor
<g/>
,	,	kIx,	,
Ekaterina	Ekaterina	k1gMnSc1	Ekaterina
Myškin	Myškin	k1gMnSc1	Myškin
<g/>
,	,	kIx,	,
Elsa	Elsa	k1gFnSc1	Elsa
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
Eugenia	Eugenium	k1gNnSc2	Eugenium
Montez	Monteza	k1gFnPc2	Monteza
nebo	nebo	k8xC	nebo
jejím	její	k3xOp3gNnSc7	její
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Elina	Elina	k1gFnSc1	Elina
Makropulos	Makropulos	k1gInSc1	Makropulos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc4d1	další
postavu	postava	k1gFnSc4	postava
představuje	představovat	k5eAaImIp3nS	představovat
strohý	strohý	k2eAgMnSc1d1	strohý
a	a	k8xC	a
přísný	přísný	k2eAgMnSc1d1	přísný
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Prus	Prus	k1gMnSc1	Prus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
Max	Max	k1gMnSc1	Max
Hauk-Šendorf	Hauk-Šendorf	k1gMnSc1	Hauk-Šendorf
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
pominuvším	pominuvší	k2eAgInSc7d1	pominuvší
jižanským	jižanský	k2eAgInSc7d1	jižanský
temperamentem	temperament	k1gInSc7	temperament
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
Čapek	Čapek	k1gMnSc1	Čapek
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
i	i	k9	i
použitím	použití	k1gNnSc7	použití
mnohým	mnohé	k1gNnSc7	mnohé
španělských	španělský	k2eAgInPc2d1	španělský
výrazů	výraz	k1gInPc2	výraz
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Emílií	Emílie	k1gFnSc7	Emílie
jako	jako	k8xC	jako
Eugenií	Eugenie	k1gFnSc7	Eugenie
(	(	kIx(	(
<g/>
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
do	do	k7c2	do
španělštiny	španělština	k1gFnSc2	španělština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
nerozvážného	rozvážný	k2eNgNnSc2d1	nerozvážné
mládí	mládí	k1gNnSc2	mládí
je	být	k5eAaImIp3nS	být
Janek	Janek	k1gMnSc1	Janek
Prus	Prus	k1gMnSc1	Prus
a	a	k8xC	a
oproti	oproti	k7c3	oproti
němu	on	k3xPp3gNnSc3	on
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
rozvážnou	rozvážný	k2eAgFnSc7d1	rozvážná
teprve	teprve	k6eAd1	teprve
Jankovou	Jankův	k2eAgFnSc7d1	Jankova
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Kolenatý	kolenatý	k2eAgMnSc1d1	kolenatý
svou	svůj	k3xOyFgFnSc7	svůj
pragmatičností	pragmatičnost	k1gFnSc7	pragmatičnost
ctí	ctít	k5eAaImIp3nP	ctít
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
jen	jen	k9	jen
tak	tak	k6eAd1	tak
něčemu	něco	k3yInSc3	něco
nedůvěřuje	důvěřovat	k5eNaImIp3nS	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Vítek	Vítek	k1gMnSc1	Vítek
stále	stále	k6eAd1	stále
nevyrostl	vyrůst	k5eNaPmAgMnS	vyrůst
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
je	být	k5eAaImIp3nS	být
má	můj	k3xOp1gFnSc1	můj
možnost	možnost	k1gFnSc1	možnost
projevit	projevit	k5eAaPmF	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
z	z	k7c2	z
charakterových	charakterový	k2eAgInPc2d1	charakterový
rysů	rys	k1gInPc2	rys
postav	postava	k1gFnPc2	postava
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
všichni	všechen	k3xTgMnPc1	všechen
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
o	o	k7c6	o
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
využití	využití	k1gNnSc6	využití
či	či	k8xC	či
zpeněžení	zpeněžení	k1gNnSc6	zpeněžení
elixíru	elixír	k1gInSc2	elixír
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
dějství	dějství	k1gNnPc2	dějství
a	a	k8xC	a
Proměny	proměna	k1gFnSc2	proměna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
objasnění	objasnění	k1gNnSc4	objasnění
celé	celý	k2eAgFnSc2d1	celá
zápletky	zápletka	k1gFnSc2	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
scénách	scéna	k1gFnPc6	scéna
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Emílie	Emílie	k1gFnSc2	Emílie
Marty	Marta	k1gFnSc2	Marta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
přímé	přímý	k2eAgFnSc2d1	přímá
řeči	řeč	k1gFnSc2	řeč
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vystihnout	vystihnout	k5eAaPmF	vystihnout
charakter	charakter	k1gInSc4	charakter
nebo	nebo	k8xC	nebo
momentální	momentální	k2eAgFnSc4d1	momentální
náladu	nálada	k1gFnSc4	nálada
dotyčné	dotyčný	k2eAgFnSc2d1	dotyčná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Patrné	patrný	k2eAgNnSc1d1	patrné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
volí	volit	k5eAaImIp3nP	volit
například	například	k6eAd1	například
Emílie	Emílie	k1gFnPc1	Emílie
<g/>
,	,	kIx,	,
střídání	střídání	k1gNnSc1	střídání
tykání	tykání	k1gNnSc1	tykání
a	a	k8xC	a
vykání	vykání	k1gNnSc1	vykání
(	(	kIx(	(
<g/>
Gregorovi	Gregor	k1gMnSc3	Gregor
familiérně	familiérně	k6eAd1	familiérně
tyká	tykat	k5eAaImIp3nS	tykat
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
jí	jíst	k5eAaImIp3nS	jíst
ne	ne	k9	ne
<g/>
,	,	kIx,	,
Maxovi	Max	k1gMnSc3	Max
ale	ale	k9	ale
vyká	vykat	k5eAaImIp3nS	vykat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zná	znát	k5eAaImIp3nS	znát
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tempo	tempo	k1gNnSc1	tempo
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
španělštiny	španělština	k1gFnSc2	španělština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
na	na	k7c6	na
scénách	scéna	k1gFnPc6	scéna
českých	český	k2eAgNnPc2d1	české
divadel	divadlo	k1gNnPc2	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
31	[number]	k4	31
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Drábek	Drábek	k1gMnSc1	Drábek
<g/>
;	;	kIx,	;
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Daniela	Daniela	k1gFnSc1	Daniela
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Meduna	Meduna	k1gFnSc1	Meduna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Batěk	Batěk	k1gMnSc1	Batěk
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Štěpánková	Štěpánková	k1gFnSc1	Štěpánková
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Plachý	Plachý	k1gMnSc1	Plachý
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Tomsa	Tomsa	k1gFnSc1	Tomsa
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kepka	Kepka	k1gMnSc1	Kepka
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Bernášková	Bernášková	k1gFnSc1	Bernášková
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Polišenská	Polišenský	k2eAgFnSc1d1	Polišenská
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Davídek	Davídek	k1gMnSc1	Davídek
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
18	[number]	k4	18
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
;	;	kIx,	;
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Soňa	Soňa	k1gFnSc1	Soňa
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bidlas	Bidlas	k1gMnSc1	Bidlas
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
Beretová	Beretový	k2eAgFnSc1d1	Beretová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pelzer	Pelzer	k1gMnSc1	Pelzer
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Postránecký	Postránecký	k2eAgMnSc1d1	Postránecký
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Rajmont	Rajmont	k1gMnSc1	Rajmont
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Javorský	Javorský	k1gMnSc1	Javorský
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
</s>
</p>
<p>
<s>
HaDivadlo	HaDivadlo	k1gNnSc1	HaDivadlo
<g/>
;	;	kIx,	;
premiéra	premiéra	k1gFnSc1	premiéra
7	[number]	k4	7
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
;	;	kIx,	;
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Ondřej	Ondřej	k1gMnSc1	Ondřej
Elbel	Elbel	k1gMnSc1	Elbel
<g/>
;	;	kIx,	;
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
:	:	kIx,	:
Kamila	Kamila	k1gFnSc1	Kamila
Valůšková	Valůšková	k1gFnSc1	Valůšková
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Siničák	Siničák	k1gMnSc1	Siničák
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Maršálek	Maršálek	k1gMnSc1	Maršálek
<g/>
,	,	kIx,	,
Erika	Erika	k1gFnSc1	Erika
Stárková	Stárková	k1gFnSc1	Stárková
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Elbel	Elbel	k1gMnSc1	Elbel
<g/>
,	,	kIx,	,
Marián	Marián	k1gMnSc1	Marián
Chalány	Chalán	k1gInPc4	Chalán
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Drozda	Drozd	k1gMnSc2	Drozd
<g/>
,	,	kIx,	,
Sára	Sára	k1gFnSc1	Sára
Venclovská	Venclovský	k2eAgFnSc1d1	Venclovská
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kumhala	Kumhal	k1gMnSc2	Kumhal
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BRABEC	Brabec	k1gMnSc1	Brabec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Díl	díl	k1gInSc1	díl
<g/>
]	]	kIx)	]
4	[number]	k4	4
<g/>
,	,	kIx,	,
Činoherní	činoherní	k2eAgNnSc1d1	činoherní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
706	[number]	k4	706
s.	s.	k?	s.
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
71	[number]	k4	71
<g/>
,	,	kIx,	,
74	[number]	k4	74
<g/>
,	,	kIx,	,
77	[number]	k4	77
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
171	[number]	k4	171
<g/>
,	,	kIx,	,
173	[number]	k4	173
<g/>
,	,	kIx,	,
518	[number]	k4	518
<g/>
,	,	kIx,	,
605	[number]	k4	605
<g/>
,	,	kIx,	,
629	[number]	k4	629
<g/>
,	,	kIx,	,
646	[number]	k4	646
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
</s>
</p>
<p>
<s>
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
nemoc	nemoc	k1gFnSc1	nemoc
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
zpracování	zpracování	k1gNnSc4	zpracování
===	===	k?	===
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
-	-	kIx~	-
Československý	československý	k2eAgInSc1d1	československý
rozhlas	rozhlas	k1gInSc1	rozhlas
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Osoby	osoba	k1gFnPc1	osoba
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
<g/>
:	:	kIx,	:
Emilia	Emilia	k1gFnSc1	Emilia
Marty	Marta	k1gFnSc2	Marta
(	(	kIx(	(
<g/>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Prus	Prus	k1gMnSc1	Prus
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janek	Janek	k1gMnSc1	Janek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Preiss	Preissa	k1gFnPc2	Preissa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Gregor	Gregor	k1gMnSc1	Gregor
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hauk-Sendorf	Hauk-Sendorf	k1gMnSc1	Hauk-Sendorf
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kolenatý	kolenatý	k2eAgMnSc1d1	kolenatý
<g/>
,	,	kIx,	,
advokát	advokát	k1gMnSc1	advokát
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Solicitátor	solicitátor	k1gMnSc1	solicitátor
Vítek	Vítek	k1gMnSc1	Vítek
(	(	kIx(	(
<g/>
Martin	Martin	k1gMnSc1	Martin
Růžek	Růžek	k1gMnSc1	Růžek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kristina	Kristina	k1gFnSc1	Kristina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Vránová	Vránová	k1gFnSc1	Vránová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komorná	komorná	k1gFnSc1	komorná
(	(	kIx(	(
<g/>
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Fialková	fialkový	k2eAgFnSc1d1	Fialková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Janovský	Janovský	k1gMnSc1	Janovský
<g/>
)	)	kIx)	)
a	a	k8xC	a
poklízečka	poklízečka	k1gFnSc1	poklízečka
(	(	kIx(	(
<g/>
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Drmlová	Drmlová	k1gFnSc1	Drmlová
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
úprava	úprava	k1gFnSc1	úprava
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Procházka	Procházka	k1gMnSc1	Procházka
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
improvizace	improvizace	k1gFnSc1	improvizace
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Musil	Musil	k1gMnSc1	Musil
</s>
</p>
<p>
<s>
Dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Hlavnička	hlavnička	k1gFnSc1	hlavnička
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Horčička	Horčička	k1gMnSc1	Horčička
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
audia	audia	k1gFnSc1	audia
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
</s>
</p>
<p>
<s>
https://vltava.rozhlas.cz/karel-capek-vec-makropulos-5047722%20?eco=mail&	[url]	k?	https://vltava.rozhlas.cz/karel-capek-vec-makropulos-5047722%20?eco=mail&
</s>
</p>
