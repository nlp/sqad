<p>
<s>
Letausas	Letausas	k1gInSc1	Letausas
je	být	k5eAaImIp3nS	být
říčka	říčka	k1gFnSc1	říčka
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
v	v	k7c6	v
Žemaitsku	Žemaitsek	k1gInSc6	Žemaitsek
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
Telšiajském	Telšiajský	k2eAgInSc6d1	Telšiajský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rietavas	Rietavas	k1gInSc1	Rietavas
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
západoseverozápadním	západoseverozápadní	k2eAgInSc6d1	západoseverozápadní
okraji	okraj	k1gInSc6	okraj
okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
Rietavas	Rietavas	k1gInSc1	Rietavas
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jaujupis	Jaujupis	k1gFnSc2	Jaujupis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
nese	nést	k5eAaImIp3nS	nést
jen	jen	k9	jen
až	až	k9	až
do	do	k7c2	do
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
Urnupis	Urnupis	k1gFnSc2	Urnupis
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
a	a	k8xC	a
jih	jih	k1gInSc4	jih
po	po	k7c6	po
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
Rietavas	Rietavasa	k1gFnPc2	Rietavasa
<g/>
.	.	kIx.	.
</s>
<s>
Podtéká	podtékat	k5eAaImIp3nS	podtékat
pod	pod	k7c7	pod
Žemaitskou	Žemaitský	k2eAgFnSc7d1	Žemaitský
magistrálou	magistrála	k1gFnSc7	magistrála
-	-	kIx~	-
silnicí	silnice	k1gFnSc7	silnice
č.	č.	k?	č.
197	[number]	k4	197
Kryžkalnis	Kryžkalnis	k1gInSc1	Kryžkalnis
-	-	kIx~	-
Klaipė	Klaipė	k1gFnSc1	Klaipė
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
západu	západ	k1gInSc3	západ
<g/>
,	,	kIx,	,
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
Urnupis	Urnupis	k1gFnSc2	Urnupis
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Jaujupė	Jaujupė	k1gFnSc2	Jaujupė
mění	měnit	k5eAaImIp3nS	měnit
název	název	k1gInSc1	název
na	na	k7c4	na
Letausas	Letausas	k1gInSc4	Letausas
<g/>
,	,	kIx,	,
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
Klietė	Klietė	k1gFnSc2	Klietė
se	se	k3xPyFc4	se
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
stáčí	stáčet	k5eAaImIp3nS	stáčet
do	do	k7c2	do
protisměru	protisměr	k1gInSc2	protisměr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Žadvainiai	Žadvainia	k1gFnSc2	Žadvainia
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
dálnice	dálnice	k1gFnSc2	dálnice
A1	A1	k1gFnSc2	A1
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Jū	Jū	k1gFnSc2	Jū
145,1	[number]	k4	145,1
km	km	kA	km
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
Němenu	Němen	k1gInSc2	Němen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
její	její	k3xOp3gInSc1	její
pravý	pravý	k2eAgInSc1d1	pravý
přítok	přítok	k1gInSc1	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
koryta	koryto	k1gNnSc2	koryto
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
–	–	k?	–
10	[number]	k4	10
m	m	kA	m
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
1,6	[number]	k4	1,6
–	–	k?	–
1,8	[number]	k4	1,8
m.	m.	k?	m.
Rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
0,1	[number]	k4	0,1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
je	být	k5eAaImIp3nS	být
1,12	[number]	k4	1,12
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Odtokový	odtokový	k2eAgInSc1d1	odtokový
modul	modul	k1gInSc1	modul
Letausu	Letaus	k1gInSc2	Letaus
(	(	kIx(	(
<g/>
13,4	[number]	k4	13,4
l	l	kA	l
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
z	z	k7c2	z
1	[number]	k4	1
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Levé	levá	k1gFnPc1	levá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pravé	pravá	k1gFnPc1	pravá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnSc6d1	jazyková
souvislosti	souvislost	k1gFnSc6	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
probádán	probádán	k2eAgMnSc1d1	probádán
<g/>
,	,	kIx,	,
doc.	doc.	kA	doc.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vytenis	Vytenis	k1gFnSc1	Vytenis
Almonaitis	Almonaitis	k1gFnSc1	Almonaitis
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
lieti	lieti	k1gNnSc2	lieti
<g/>
"	"	kIx"	"
-	-	kIx~	-
lít	lít	k5eAaImF	lít
<g/>
,	,	kIx,	,
odlévat	odlévat	k5eAaImF	odlévat
-	-	kIx~	-
pozměněného	pozměněný	k2eAgNnSc2d1	pozměněné
vlivem	vlivem	k7c2	vlivem
nářečí	nářečí	k1gNnSc2	nářečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Letausas	Letausasa	k1gFnPc2	Letausasa
na	na	k7c6	na
litevské	litevský	k2eAgFnSc6d1	Litevská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letausas	Letausasa	k1gFnPc2	Letausasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
