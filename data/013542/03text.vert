<s>
Micubiši	Micubiš	k1gMnPc1
A5M	A5M	k1gFnSc2
</s>
<s>
Micubiši	Micubiš	k1gMnSc3
A5M	A5M	k1gMnSc3
Micubiši	Micubiše	k1gFnSc4
A5M4	A5M4	k1gFnSc3
s	s	k7c7
přídavnou	přídavný	k2eAgFnSc7d1
palivovou	palivový	k2eAgFnSc7d1
nádržíUrčení	nádržíUrčení	k1gNnSc2
</s>
<s>
palubní	palubní	k2eAgMnSc1d1
stíhací	stíhací	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Džiró	Džiró	k?
Horikoši	Horikoš	k1gMnPc1
První	první	k4xOgFnSc1
let	léto	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
Zařazeno	zařadit	k5eAaPmNgNnS
</s>
<s>
1936	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
asi	asi	k9
1100	#num#	k4
ks	ks	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
A5M	A5M	k1gFnSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
三	三	k?
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
jednoplošný	jednoplošný	k2eAgInSc1d1
palubní	palubní	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenecké	spojenecký	k2eAgNnSc1d1
kódové	kódový	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
A5M	A5M	k1gFnSc4
bylo	být	k5eAaImAgNnS
Claude	Claude	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
japonském	japonský	k2eAgNnSc6d1
námořnictvu	námořnictvo	k1gNnSc6
byl	být	k5eAaImAgInS
typ	typ	k1gInSc1
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
palubní	palubní	k2eAgInSc1d1
stíhací	stíhací	k2eAgInSc1d1
letoun	letoun	k1gInSc1
typ	typ	k1gInSc1
96	#num#	k4
(	(	kIx(
<g/>
九	九	k?
<g/>
,	,	kIx,
96	#num#	k4
šiki	šik	k1gFnSc2
kandžó	kandžó	k?
sentóki	sentók	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A5M	A5M	k1gFnSc1
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgInSc7d1
stíhacím	stíhací	k2eAgInSc7d1
typem	typ	k1gInSc7
japonského	japonský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
během	během	k7c2
prvních	první	k4xOgNnPc2
let	léto	k1gNnPc2
druhé	druhý	k4xOgFnSc2
čínsko-japonské	čínsko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
poměrně	poměrně	k6eAd1
rozšířeným	rozšířený	k2eAgInSc7d1
typem	typ	k1gInSc7
na	na	k7c6
samém	samý	k3xTgInSc6
začátku	začátek	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
a	a	k8xC
dočkal	dočkat	k5eAaPmAgMnS
se	se	k3xPyFc4
omezeného	omezený	k2eAgInSc2d1
nasazení	nasazený	k2eAgMnPc1d1
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
dalších	další	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
stroj	stroj	k1gInSc1
byl	být	k5eAaImAgInS
rychle	rychle	k6eAd1
vyřazován	vyřazovat	k5eAaImNgInS
ze	z	k7c2
služby	služba	k1gFnSc2
a	a	k8xC
do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1942	#num#	k4
byl	být	k5eAaImAgInS
prakticky	prakticky	k6eAd1
u	u	k7c2
všech	všecek	k3xTgFnPc2
bojových	bojový	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
námořnictva	námořnictvo	k1gNnSc2
nahrazen	nahradit	k5eAaPmNgInS
stíhačkou	stíhačka	k1gFnSc7
Micubiši	Micubiše	k1gFnSc4
A6M	A6M	k1gFnSc2
(	(	kIx(
<g/>
Zero	Zero	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
zbývající	zbývající	k2eAgFnPc1d1
letadla	letadlo	k1gNnPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
cvičných	cvičný	k2eAgFnPc2d1
<g/>
)	)	kIx)
masivně	masivně	k6eAd1
nasazena	nasazen	k2eAgFnSc1d1
při	při	k7c6
útocích	útok	k1gInPc6
kamikaze	kamikaze	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Letoun	letoun	k1gMnSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgMnS
výbornou	výborný	k2eAgFnSc7d1
obratností	obratnost	k1gFnSc7
a	a	k8xC
dobrou	dobrý	k2eAgFnSc7d1
odolností	odolnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
slabinou	slabina	k1gFnSc7
byla	být	k5eAaImAgFnS
výzbroj	výzbroj	k1gFnSc4
a	a	k8xC
postupem	postup	k1gInSc7
doby	doba	k1gFnSc2
i	i	k9
rychlost	rychlost	k1gFnSc4
a	a	k8xC
dolet	dolet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
vyrobeno	vyrobit	k5eAaPmNgNnS
asi	asi	k9
1	#num#	k4
100	#num#	k4
letounů	letoun	k1gInPc2
všech	všecek	k3xTgFnPc2
verzí	verze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
letounu	letoun	k1gInSc6
létal	létat	k5eAaImAgMnS
i	i	k9
nejúspěšnější	úspěšný	k2eAgMnSc1d3
japonský	japonský	k2eAgMnSc1d1
námořní	námořní	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Saburo	Sabura	k1gFnSc5
Sakai	Saka	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
zahájil	zahájit	k5eAaPmAgInS
japonský	japonský	k2eAgInSc1d1
Námořní	námořní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vývojový	vývojový	k2eAgInSc4d1
program	program	k1gInSc4
9	#num#	k4
ši	ši	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomuto	tento	k3xDgInSc3
rozsáhlému	rozsáhlý	k2eAgInSc3d1
programu	program	k1gInSc3
brzo	brzo	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
slavných	slavný	k2eAgInPc2d1
bojových	bojový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nich	on	k3xPp3gInPc2
bylo	být	k5eAaImAgNnS
palubní	palubní	k2eAgNnSc4d1
stíhací	stíhací	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
Typ	typ	k1gInSc1
96	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Micubiši	Micubiš	k1gMnPc1
A	a	k9
<g/>
5	#num#	k4
<g/>
M.	M.	kA
Prototyp	prototyp	k1gInSc4
letounu	letoun	k1gInSc2
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgInS
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
vůbec	vůbec	k9
první	první	k4xOgNnSc4
japonské	japonský	k2eAgNnSc4d1
jednoplošné	jednoplošný	k2eAgNnSc4d1
stíhací	stíhací	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
nástup	nástup	k1gInSc1
znamenal	znamenat	k5eAaImAgInS
definitivní	definitivní	k2eAgInSc1d1
konec	konec	k1gInSc1
japonské	japonský	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
na	na	k7c6
cizích	cizí	k2eAgFnPc6d1
konstrukcích	konstrukce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
jeho	jeho	k3xOp3gInSc7
vývojem	vývoj	k1gInSc7
stál	stát	k5eAaImAgMnS
inženýr	inženýr	k1gMnSc1
jménem	jméno	k1gNnSc7
Džiró	Džiró	k1gFnSc2
Horikoši	Horikoš	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
významně	významně	k6eAd1
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
vývoji	vývoj	k1gInSc6
vynikajícího	vynikající	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Micubiši	Micubiše	k1gFnSc4
A	a	k8xC
<g/>
7	#num#	k4
<g/>
M.	M.	kA
</s>
<s>
Varianty	varianta	k1gFnPc1
letounu	letoun	k1gInSc2
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
A5M	A5M	k1gFnSc2
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
Ka-	Ka-	k1gFnSc4
<g/>
14	#num#	k4
:	:	kIx,
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
(	(	kIx(
<g/>
firemní	firemní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M1	A5M1	k1gFnPc4
:	:	kIx,
První	první	k4xOgFnSc1
sériová	sériový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
s	s	k7c7
uzavřenou	uzavřený	k2eAgFnSc7d1
kabinou	kabina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A	a	k9
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
a	a	k8xC
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
sériová	sériový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
se	s	k7c7
silnějším	silný	k2eAgInSc7d2
motorem	motor	k1gInSc7
a	a	k8xC
otevřenou	otevřený	k2eAgFnSc7d1
kabinou	kabina	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
vynutil	vynutit	k5eAaPmAgMnS
tlak	tlak	k1gInSc4
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A	a	k9
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
b	b	k?
:	:	kIx,
Zabudován	zabudovat	k5eAaPmNgInS
ještě	ještě	k9
silnější	silný	k2eAgInSc1d2
motor	motor	k1gInSc1
s	s	k7c7
třílistou	třílistý	k2eAgFnSc7d1
vrtulí	vrtule	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M3	A5M3	k1gFnSc1
:	:	kIx,
Jediný	jediný	k2eAgInSc1d1
pokusný	pokusný	k2eAgInSc1d1
prototyp	prototyp	k1gInSc1
s	s	k7c7
20	#num#	k4
<g/>
mm	mm	kA
kanónem	kanón	k1gInSc7
pálícím	pálící	k2eAgInSc7d1
skrz	skrz	k7c4
vrtuli	vrtule	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
nebyl	být	k5eNaImAgInS
přijat	přijmout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M4	A5M4	k1gFnSc1
:	:	kIx,
Řada	řada	k1gFnSc1
menších	malý	k2eAgFnPc2d2
úprav	úprava	k1gFnPc2
podle	podle	k7c2
bojových	bojový	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
vytrvalost	vytrvalost	k1gFnSc4
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M4-K	A5M4-K	k1gFnSc1
:	:	kIx,
Dvoumístný	dvoumístný	k2eAgInSc1d1
cvičný	cvičný	k2eAgInSc1d1
stroj	stroj	k1gInSc1
vyráběný	vyráběný	k2eAgInSc1d1
do	do	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
postaveno	postavit	k5eAaPmNgNnS
103	#num#	k4
letounů	letoun	k1gInPc2
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
A	a	k9
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
</s>
<s>
A5M4	A5M4	k4
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
7,6	7,6	k4
m	m	kA
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
11,0	11,0	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
3,30	3,30	k4
m	m	kA
</s>
<s>
Plocha	plocha	k1gFnSc1
křídel	křídlo	k1gNnPc2
<g/>
:	:	kIx,
17,8	17,8	k4
m²	m²	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
prázdný	prázdný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1	#num#	k4
216	#num#	k4
kg	kg	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
(	(	kIx(
<g/>
naložen	naložen	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
708	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
×	×	k?
devítiválcový	devítiválcový	k2eAgInSc1d1
hvězdicový	hvězdicový	k2eAgInSc1d1
motor	motor	k1gInSc1
Nakadžima	Nakadžimum	k1gNnSc2
Kotobuki	Kotobuk	k1gFnSc2
41	#num#	k4
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
::	::	k?
585	#num#	k4
kW	kW	kA
</s>
<s>
Plocha	plocha	k1gFnSc1
čelního	čelní	k2eAgInSc2d1
odporu	odpor	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Štíhlost	štíhlost	k1gFnSc1
křídla	křídlo	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Zásoba	zásoba	k1gFnSc1
paliva	palivo	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
440	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Akční	akční	k2eAgInSc1d1
rádius	rádius	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
200	#num#	k4
km	km	kA
s	s	k7c7
přídavnými	přídavný	k2eAgFnPc7d1
nádržemi	nádrž	k1gFnPc7
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
9	#num#	k4
800	#num#	k4
m	m	kA
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
850	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Zátěž	zátěž	k1gFnSc1
křídel	křídlo	k1gNnPc2
<g/>
:	:	kIx,
93,7	93,7	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
2	#num#	k4
</s>
<s>
Tah	tah	k1gInSc1
<g/>
/	/	kIx~
<g/>
Hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
316	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
2	#num#	k4
x	x	k?
7,7	7,7	k4
mm	mm	kA
kulomet	kulomet	k1gInSc1
Typ	typ	k1gInSc1
89	#num#	k4
</s>
<s>
Dva	dva	k4xCgInPc4
závěsy	závěs	k1gInPc4
na	na	k7c6
konci	konec	k1gInSc6
křídla	křídlo	k1gNnSc2
pro	pro	k7c4
dvě	dva	k4xCgFnPc4
malé	malý	k2eAgFnPc4d1
pumy	puma	k1gFnPc4
(	(	kIx(
<g/>
30	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FRANCILLON	FRANCILLON	kA
<g/>
,	,	kIx,
René	René	k1gMnSc1
J.	J.	kA
Japanese	Japanese	k1gFnPc1
Aircraft	Aircraft	k1gMnSc1
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
313	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Mitsubishi	mitsubishi	k1gNnSc3
A	a	k9
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
s.	s.	k?
342	#num#	k4
až	až	k9
349	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GUNSTON	GUNSTON	kA
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojová	bojový	k2eAgFnSc1d1
letadla	letadlo	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svojtka	Svojtka	k1gFnSc1
<g/>
&	&	k?
<g/>
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7237	#num#	k4
<g/>
-	-	kIx~
<g/>
203	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Micubiši	Micubiš	k1gInSc3
A5M	A5M	k1gMnSc1
„	„	k?
<g/>
Claude	Claud	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
282	#num#	k4
a	a	k8xC
283	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
VÁLKA	Válka	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stíhací	stíhací	k2eAgInSc1d1
letadla	letadlo	k1gNnPc4
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
<g/>
/	/	kIx~
<g/>
USA-Japonsko	USA-Japonsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
88	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7198	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Micubiši	Micubiše	k1gFnSc4
A	a	k8xC
<g/>
5	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Micubiši	Micubiš	k1gMnPc1
A5M	A5M	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M	A5M	k1gFnSc1
-	-	kIx~
přehled	přehled	k1gInSc1
verzí	verze	k1gFnPc2
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
A5M	A5M	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Micubiši	Micubisat	k5eAaPmIp1nSwK
A5M	A5M	k1gFnSc1
„	„	k?
<g/>
CLAUDE	CLAUDE	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
A5M	A5M	k4
Type	typ	k1gInSc5
97	#num#	k4
-	-	kIx~
fotografie	fotografia	k1gFnSc2
</s>
<s>
Arawasi	Arawas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IJAAF	IJAAF	kA
&	&	k?
IJNAF	IJNAF	kA
drop	drop	k1gMnSc1
tanks	tanks	k6eAd1
pt	pt	k?
<g/>
.	.	kIx.
2	#num#	k4
-	-	kIx~
Mitsubishi	mitsubishi	k1gNnSc4
A5M	A5M	k1gFnSc2
"	"	kIx"
<g/>
Claude	Claud	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arawasi-wildeagles	arawasi-wildeagles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2020-07-26	2020-07-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Letouny	letoun	k1gInPc7
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
A4N	A4N	k4
•	•	k?
A5M	A5M	k1gMnSc1
•	•	k?
A6M	A6M	k1gMnSc1
•	•	k?
A6M2-N	A6M2-N	k1gMnSc1
•	•	k?
A7M	A7M	k1gMnSc1
•	•	k?
J1N	J1N	k1gMnSc1
•	•	k?
J2M	J2M	k1gMnSc1
•	•	k?
J5N	J5N	k1gMnSc1
•	•	k?
N1K	N1K	k1gMnSc1
Bombardovací	bombardovací	k2eAgInPc4d1
a	a	k8xC
torpédové	torpédový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
B5M	B5M	k4
•	•	k?
B5N	B5N	k1gMnSc1
•	•	k?
B6N	B6N	k1gMnSc1
•	•	k?
B7A	B7A	k1gMnSc1
•	•	k?
D1A	D1A	k1gMnSc1
•	•	k?
D3A	D3A	k1gMnSc1
•	•	k?
D4Y	D4Y	k1gMnSc1
•	•	k?
G3M	G3M	k1gMnSc1
•	•	k?
G4M	G4M	k1gMnSc1
•	•	k?
G5N	G5N	k1gMnSc1
•	•	k?
G8N	G8N	k1gMnSc1
•	•	k?
M6A	M6A	k1gMnSc1
•	•	k?
P1Y	P1Y	k1gMnSc1
Průzkumné	průzkumný	k2eAgInPc4d1
a	a	k8xC
spojovací	spojovací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
C3N	C3N	k4
•	•	k?
C5M	C5M	k1gMnSc1
•	•	k?
C6N	C6N	k1gMnSc1
•	•	k?
E7K	E7K	k1gMnSc1
•	•	k?
E8N	E8N	k1gMnSc1
•	•	k?
E11A	E11A	k1gMnSc1
•	•	k?
E13A	E13A	k1gMnSc1
•	•	k?
E14Y	E14Y	k1gMnSc1
•	•	k?
E15K	E15K	k1gMnSc1
•	•	k?
E16A	E16A	k1gMnSc1
•	•	k?
F1M	F1M	k1gMnSc1
•	•	k?
H5Y	H5Y	k1gMnSc1
•	•	k?
H6K	H6K	k1gMnSc1
•	•	k?
H8K	H8K	k1gMnSc1
•	•	k?
H9A	H9A	k1gMnSc1
•	•	k?
Q1W	Q1W	k1gMnSc1
Zbývající	zbývající	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Baika	Baika	k1gFnSc1
•	•	k?
MXY	MXY	kA
5	#num#	k4
•	•	k?
MXY7	MXY7	k1gMnSc1
Transportní	transportní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
L2D	L2D	k4
•	•	k?
L3Y	L3Y	k1gMnSc1
•	•	k?
L4M	L4M	k1gMnSc1
Experimentální	experimentální	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
</s>
<s>
J7W	J7W	k4
•	•	k?
J8M	J8M	k1gMnSc1
•	•	k?
J9N	J9N	k1gMnSc1
•	•	k?
R2Y	R2Y	k1gMnSc1
•	•	k?
S1A	S1A	k1gFnSc2
Značení	značení	k1gNnSc2
japonských	japonský	k2eAgNnPc2d1
vojenských	vojenský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
