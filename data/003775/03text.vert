<s>
PowerPC	PowerPC	k?	PowerPC
je	být	k5eAaImIp3nS	být
architektura	architektura	k1gFnSc1	architektura
mikroprocesorů	mikroprocesor	k1gInPc2	mikroprocesor
typu	typ	k1gInSc2	typ
RISC	RISC	kA	RISC
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
aliancí	aliance	k1gFnSc7	aliance
Apple-IBM-Motorola	Apple-IBM-Motorola	k1gFnSc1	Apple-IBM-Motorola
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c4	v
embedded	embedded	k1gInSc4	embedded
zařízeních	zařízení	k1gNnPc6	zařízení
i	i	k8xC	i
mezi	mezi	k7c7	mezi
vysoce	vysoce	k6eAd1	vysoce
výkonnými	výkonný	k2eAgInPc7d1	výkonný
procesory	procesor	k1gInPc7	procesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
procesory	procesor	k1gInPc4	procesor
PowerPC	PowerPC	k1gFnSc2	PowerPC
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
PReP	PReP	k1gFnSc2	PReP
a	a	k8xC	a
Common	Common	k1gNnSc1	Common
Hardware	hardware	k1gInSc4	hardware
Reference	reference	k1gFnSc2	reference
Platform	Platform	k1gInSc1	Platform
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
ale	ale	k8xC	ale
architektura	architektura	k1gFnSc1	architektura
našla	najít	k5eAaPmAgFnS	najít
v	v	k7c6	v
osobních	osobní	k2eAgInPc6d1	osobní
počítačích	počítač	k1gInPc6	počítač
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994-2006	[number]	k4	1994-2006
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
platformu	platforma	k1gFnSc4	platforma
Intel	Intel	kA	Intel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
jsou	být	k5eAaImIp3nP	být
procesory	procesor	k1gInPc1	procesor
PowerPC	PowerPC	k1gMnPc2	PowerPC
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
otevřené	otevřený	k2eAgFnSc6d1	otevřená
počítačové	počítačový	k2eAgFnSc6d1	počítačová
platformě	platforma	k1gFnSc6	platforma
Pegasos	Pegasosa	k1gFnPc2	Pegasosa
a	a	k8xC	a
AmigaOne	AmigaOn	k1gInSc5	AmigaOn
X	X	kA	X
<g/>
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
PowerPC	PowerPC	k1gFnSc6	PowerPC
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
starším	starý	k2eAgInSc6d2	starší
procesoru	procesor	k1gInSc6	procesor
IBM	IBM	kA	IBM
POWER	POWER	kA	POWER
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
čipy	čip	k1gInPc1	čip
řady	řada	k1gFnSc2	řada
POWER	POWER	kA	POWER
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
instrukční	instrukční	k2eAgFnSc4d1	instrukční
sadu	sada	k1gFnSc4	sada
PowerPC	PowerPC	k1gFnSc2	PowerPC
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
PowerPC	PowerPC	k1gFnSc2	PowerPC
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Czech	Czech	k1gMnSc1	Czech
PowerPC	PowerPC	k1gMnSc1	PowerPC
User	usrat	k5eAaPmRp2nS	usrat
Group	Group	k1gInSc1	Group
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
skupina	skupina	k1gFnSc1	skupina
podporující	podporující	k2eAgFnPc4d1	podporující
otevřené	otevřený	k2eAgFnPc4d1	otevřená
PowerPC	PowerPC	k1gFnPc4	PowerPC
platformy	platforma	k1gFnSc2	platforma
Pegasos	Pegasosa	k1gFnPc2	Pegasosa
<g/>
,	,	kIx,	,
EFIKA	EFIKA	kA	EFIKA
a	a	k8xC	a
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
MorphOS	MorphOS	k1gFnPc2	MorphOS
Procesory	procesor	k1gInPc1	procesor
PowerPC	PowerPC	k1gFnSc2	PowerPC
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
procesorech	procesor	k1gInPc6	procesor
PowerPC	PowerPC	k1gFnSc2	PowerPC
(	(	kIx(	(
<g/>
Czech	Czech	k1gMnSc1	Czech
PowerPC	PowerPC	k1gMnSc1	PowerPC
User	usrat	k5eAaPmRp2nS	usrat
Group	Group	k1gMnSc1	Group
<g/>
)	)	kIx)	)
PowerPC	PowerPC	k1gMnSc1	PowerPC
overview	overview	k?	overview
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Roots	Roots	k1gInSc1	Roots
of	of	k?	of
the	the	k?	the
Power	Powero	k1gNnPc2	Powero
Mac	Mac	kA	Mac
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Recenze	recenze	k1gFnSc1	recenze
systému	systém	k1gInSc2	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Warp	Warp	k1gMnSc1	Warp
<g/>
,	,	kIx,	,
PowerPC	PowerPC	k1gMnSc1	PowerPC
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
A	a	k8xC	a
developer	developer	k1gMnSc1	developer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
guide	guide	k6eAd1	guide
to	ten	k3xDgNnSc1	ten
the	the	k?	the
POWER	POWER	kA	POWER
architecture	architectur	k1gMnSc5	architectur
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
POWER	POWER	kA	POWER
to	ten	k3xDgNnSc1	ten
the	the	k?	the
people	people	k6eAd1	people
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Freescale	Freescala	k1gFnSc3	Freescala
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Power	Power	k1gMnSc1	Power
Architecture	Architectur	k1gMnSc5	Architectur
Processors	Processors	k1gInSc4	Processors
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
