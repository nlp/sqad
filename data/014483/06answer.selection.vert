<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
v	v	k7c6
pořadu	pořad	k1gInSc6
je	být	k5eAaImIp3nS
záhadný	záhadný	k2eAgInSc1d1
testovací	testovací	k2eAgInSc1d1
jezdec	jezdec	k1gInSc1
v	v	k7c6
bílém	bílé	k1gNnSc6
overalu	overal	k1gInSc2
<g/>
,	,	kIx,
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
The	The	k1gMnSc1
Stig	Stig	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
jednom	jeden	k4xCgInSc6
díle	díl	k1gInSc6
ho	on	k3xPp3gNnSc4
hrál	hrát	k5eAaImAgMnS
bývalý	bývalý	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
F1	F1	k1gFnSc2
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Celosvětová	celosvětový	k2eAgFnSc1d1
sledovanost	sledovanost	k1gFnSc1
pořadu	pořad	k1gInSc2
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
350	#num#	k4
milionů	milion	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>