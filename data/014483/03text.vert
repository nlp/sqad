<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
Logo	logo	k1gNnSc1
pořaduZákladní	pořaduZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Geara	k1gFnPc2
Žánr	žánr	k1gInSc1
</s>
<s>
motoristický	motoristický	k2eAgMnSc1d1
Režie	režie	k1gFnSc2
</s>
<s>
Brian	Brian	k1gMnSc1
KleinPhil	KleinPhil	k1gMnSc1
ChurchwardNigel	ChurchwardNigel	k1gMnSc1
SimpkissJames	SimpkissJames	k1gMnSc1
Bryce	Bryce	k1gMnSc1
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Jeremy	Jerem	k1gInPc1
Clarkson	Clarksona	k1gFnPc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Richard	Richard	k1gMnSc1
Hammond	Hammond	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
James	James	k1gMnSc1
May	May	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Stig	Stiga	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
Jason	Jason	k1gMnSc1
Dawe	Daw	k1gMnSc2
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Chris	Chris	k1gFnSc1
Evans	Evansa	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Matt	Matt	k2eAgInSc1d1
LeBlanc	LeBlanc	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Chris	Chris	k1gFnSc2
Harris	Harris	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
Rory	Rora	k1gMnSc2
Reid	Reid	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Eddie	Eddie	k1gFnSc1
Jordan	Jordan	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Sabine	Sabin	k1gInSc5
Schmitz	Schmitz	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Andrew	Andrew	k1gMnSc1
Flintoff	Flintoff	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
Paddy	Padd	k1gInPc4
McGuinness	McGuinness	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
24	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
191	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
11	#num#	k4
speciálů	speciál	k1gInPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
60	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Producent	producent	k1gMnSc1
</s>
<s>
Andy	Anda	k1gFnSc2
Wilman	Wilman	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Hudba	hudba	k1gFnSc1
</s>
<s>
The	The	k?
Allman	Allman	k1gMnSc1
Brothers	Brothersa	k1gFnPc2
Band	banda	k1gFnPc2
-	-	kIx~
„	„	k?
<g/>
Jessica	Jessic	k2eAgFnSc1d1
<g/>
“	“	k?
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc1
</s>
<s>
BBC	BBC	kA
Distributor	distributor	k1gMnSc1
</s>
<s>
BBC	BBC	kA
WorldwideNetflix	WorldwideNetflix	k1gInSc4
Premiérové	premiérový	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
Stanice	stanice	k1gFnSc2
</s>
<s>
BBC	BBC	kA
Formát	formát	k1gInSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
stereo	stereo	k2eAgNnSc1d1
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
–	–	k?
dosud	dosud	k6eAd1
Posloupnost	posloupnost	k1gFnSc4
Související	související	k2eAgFnSc4d1
</s>
<s>
Fifth	Fifth	k1gMnSc1
Gear	Gear	k1gMnSc1
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
je	být	k5eAaImIp3nS
motoristický	motoristický	k2eAgInSc1d1
pořad	pořad	k1gInSc1
převážně	převážně	k6eAd1
o	o	k7c6
automobilech	automobil	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
vysílat	vysílat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
jako	jako	k8xC,k8xS
běžný	běžný	k2eAgInSc1d1
pořad	pořad	k1gInSc1
pro	pro	k7c4
motoristy	motorista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časem	časem	k6eAd1
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
od	od	k7c2
znovuzahájení	znovuzahájení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
formát	formát	k1gInSc1
pořadu	pořad	k1gInSc2
vyvinul	vyvinout	k5eAaPmAgInS
spíše	spíše	k9
na	na	k7c4
zábavný	zábavný	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
uváděn	uvádět	k5eAaImNgInS
Richardem	Richard	k1gMnSc7
Hammondem	Hammond	k1gMnSc7
<g/>
,	,	kIx,
Jamesem	James	k1gMnSc7
Mayem	May	k1gMnSc7
a	a	k8xC
Jeremym	Jeremym	k1gInSc4
Clarksonem	Clarkson	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
v	v	k7c6
pořadu	pořad	k1gInSc6
je	být	k5eAaImIp3nS
záhadný	záhadný	k2eAgInSc1d1
testovací	testovací	k2eAgInSc1d1
jezdec	jezdec	k1gInSc1
v	v	k7c6
bílém	bílé	k1gNnSc6
overalu	overal	k1gInSc2
<g/>
,	,	kIx,
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
The	The	k1gMnSc1
Stig	Stig	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
v	v	k7c6
jednom	jeden	k4xCgInSc6
díle	díl	k1gInSc6
ho	on	k3xPp3gNnSc4
hrál	hrát	k5eAaImAgMnS
bývalý	bývalý	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
F1	F1	k1gFnSc2
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Celosvětová	celosvětový	k2eAgFnSc1d1
sledovanost	sledovanost	k1gFnSc1
pořadu	pořad	k1gInSc2
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
350	#num#	k4
milionů	milion	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Premiéry	premiéra	k1gFnPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
epizod	epizoda	k1gFnPc2
jsou	být	k5eAaImIp3nP
vysílány	vysílat	k5eAaImNgInP
na	na	k7c6
britských	britský	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
BBC	BBC	kA
–	–	k?
BBC	BBC	kA
Two	Two	k1gFnSc2
a	a	k8xC
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
také	také	k9
na	na	k7c4
BBC	BBC	kA
HD	HD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořad	pořad	k1gInSc1
je	být	k5eAaImIp3nS
vysílán	vysílat	k5eAaImNgInS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
Česka	Česko	k1gNnSc2
(	(	kIx(
<g/>
Prima	prima	k2eAgInSc1d1
Cool	Cool	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pořad	pořad	k1gInSc1
byl	být	k5eAaImAgInS
vyzdvihován	vyzdvihovat	k5eAaImNgInS
za	za	k7c4
styl	styl	k1gInSc4
a	a	k8xC
přednes	přednes	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
za	za	k7c4
značné	značný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
kritiky	kritika	k1gFnSc2
v	v	k7c6
obsahu	obsah	k1gInSc6
a	a	k8xC
komentářích	komentář	k1gInPc6
moderátorů	moderátor	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1
kategorií	kategorie	k1gFnSc7
epizod	epizoda	k1gFnPc2
pořadu	pořad	k1gInSc2
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
speciály	speciál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
během	během	k7c2
natáčení	natáčení	k1gNnSc2
Polárního	polární	k2eAgInSc2d1
speciálu	speciál	k1gInSc2
expedice	expedice	k1gFnSc2
Top	topit	k5eAaImRp2nS
Gearu	Gear	k1gMnSc6
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Jeremym	Jeremym	k1gInSc1
Clarksonem	Clarkson	k1gInSc7
a	a	k8xC
Jamesem	James	k1gMnSc7
Mayem	May	k1gMnSc7
dostala	dostat	k5eAaPmAgFnS
jako	jako	k9
první	první	k4xOgFnSc1
automobilem	automobil	k1gInSc7
na	na	k7c4
severní	severní	k2eAgInSc4d1
magnetický	magnetický	k2eAgInSc4d1
pól	pól	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Africkém	africký	k2eAgInSc6d1
speciálu	speciál	k1gInSc6
James	James	k1gMnSc1
May	May	k1gMnSc1
„	„	k?
<g/>
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
“	“	k?
pramen	pramen	k1gInSc4
Nilu	Nil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
z	z	k7c2
důvodu	důvod	k1gInSc2
problémů	problém	k1gInPc2
s	s	k7c7
Jeremym	Jeremym	k1gInSc1
Clarksonem	Clarkson	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
posledním	poslední	k2eAgInSc6d1
incidentu	incident	k1gInSc6
napadl	napadnout	k5eAaPmAgInS
producenta	producent	k1gMnSc4
Oisina	Oisin	k1gMnSc4
Timona	Timon	k1gMnSc4
<g/>
,	,	kIx,
pozastaveno	pozastaven	k2eAgNnSc4d1
další	další	k2eAgNnSc4d1
natáčení	natáčení	k1gNnSc4
pořadu	pořad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zdaleka	zdaleka	k6eAd1
nebyl	být	k5eNaImAgInS
Clarksonův	Clarksonův	k2eAgInSc4d1
první	první	k4xOgInSc4
přešlap	přešlap	k1gInSc4
a	a	k8xC
proto	proto	k8xC
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
ředitel	ředitel	k1gMnSc1
BBC	BBC	kA
Tony	Tony	k1gMnSc1
Hall	Hall	k1gMnSc1
oficiálně	oficiálně	k6eAd1
ohlásil	ohlásit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
propuštění	propuštění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
ho	on	k3xPp3gMnSc4
měl	mít	k5eAaImAgMnS
nahradit	nahradit	k5eAaPmF
anglický	anglický	k2eAgMnSc1d1
moderátor	moderátor	k1gMnSc1
Chris	Chris	k1gFnSc2
Evans	Evans	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
nicméně	nicméně	k8xC
zbývající	zbývající	k2eAgMnPc1d1
dva	dva	k4xCgMnPc1
moderátoři	moderátor	k1gMnPc1
Richard	Richard	k1gMnSc1
Hammond	Hammond	k1gMnSc1
a	a	k8xC
James	James	k1gMnSc1
May	May	k1gMnSc1
prohlásili	prohlásit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
bez	bez	k7c2
Clarksona	Clarkson	k1gMnSc2
natáčet	natáčet	k5eAaImF
nebudou	být	k5eNaImBp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
pořad	pořad	k1gInSc1
se	s	k7c7
slavným	slavný	k2eAgNnSc7d1
triem	trio	k1gNnSc7
ukončen	ukončen	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
moderátoři	moderátor	k1gMnPc1
neobnovili	obnovit	k5eNaPmAgMnP
svoje	svůj	k3xOyFgFnPc4
smlouvy	smlouva	k1gFnPc4
s	s	k7c7
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
obnoven	obnovit	k5eAaPmNgInS
a	a	k8xC
moderátoři	moderátor	k1gMnPc1
nahrazeni	nahradit	k5eAaPmNgMnP
Chrisem	Chris	k1gInSc7
Evansem	Evans	k1gInSc7
a	a	k8xC
Mattem	Matt	k1gMnSc7
LeBlancem	LeBlanec	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moderátoři	moderátor	k1gMnPc1
</s>
<s>
Série	série	k1gFnSc1
(	(	kIx(
<g/>
Roky	rok	k1gInPc1
<g/>
)	)	kIx)
<g/>
Moderátoři	moderátor	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jeremy	Jerem	k1gInPc1
ClarksonRichard	ClarksonRicharda	k1gFnPc2
HammondJason	HammondJasona	k1gFnPc2
Dawe	Daw	k1gFnSc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jeremy	Jerema	k1gFnSc2
ClarksonRichard	ClarksonRicharda	k1gFnPc2
HammondJames	HammondJames	k1gMnSc1
May	May	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Matt	Matt	k2eAgInSc1d1
LeBlancChris	LeBlancChris	k1gInSc1
Evans	Evansa	k1gFnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Matt	Matt	k2eAgInSc1d1
LeBlancChris	LeBlancChris	k1gInSc1
HarrisRory	HarrisRora	k1gFnSc2
Reid	Reida	k1gFnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Chris	Chris	k1gFnSc1
HarrisPaddy	HarrisPadda	k1gFnSc2
McGuinessFreddie	McGuinessFreddie	k1gFnSc1
Flintoff	Flintoff	k1gMnSc1
</s>
<s>
Další	další	k2eAgMnPc1d1
moderátoři	moderátor	k1gMnPc1
<g/>
:	:	kIx,
Stig	Stig	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sabine	Sabin	k1gInSc5
Schmitzová	Schmitzová	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eddie	Eddie	k1gFnSc1
Jordan	Jordan	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chris	Chris	k1gInSc1
Harris	Harris	k1gFnSc2
a	a	k8xC
Rory	Rora	k1gFnSc2
Reid	Reida	k1gFnPc2
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
jako	jako	k8xS,k8xC
pomocní	pomocní	k2eAgMnPc1d1
moderátoři	moderátor	k1gMnPc1
<g/>
,	,	kIx,
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc1
hlavní	hlavní	k2eAgMnPc1d1
moderátoři	moderátor	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnPc1d1
verze	verze	k1gFnPc1
</s>
<s>
Země	země	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
Název	název	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
zemi	zem	k1gFnSc6
původu	původ	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
Australia	Australius	k1gMnSc2
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
<g/>
:	:	kIx,
Р	Р	k?
в	в	k?
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Top	topit	k5eAaImRp2nS
gear	gear	k1gMnSc1
USA	USA	kA
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
탑	탑	k?
기	기	k?
코	코	k?
</s>
<s>
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
</s>
<s>
最	最	k?
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
France	Franc	k1gMnSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
Italia	Italius	k1gMnSc2
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Moderátoři	moderátor	k1gMnPc1
pořadu	pořad	k1gInSc2
Richard	Richard	k1gMnSc1
Hammond	Hammond	k1gMnSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
May	May	k1gMnSc1
a	a	k8xC
Jeremy	Jerema	k1gFnPc1
Clarkson	Clarksona	k1gFnPc2
</s>
<s>
Cesta	cesta	k1gFnSc1
na	na	k7c4
severní	severní	k2eAgInSc4d1
pól	pól	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
</s>
<s>
Stig	Stig	k1gMnSc1
–	–	k?
závodní	závodní	k2eAgMnSc1d1
řidič	řidič	k1gMnSc1
pořadu	pořad	k1gInSc2
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Moderátor	moderátor	k1gMnSc1
z	z	k7c2
pořadu	pořad	k1gInSc2
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc1
si	se	k3xPyFc3
splnil	splnit	k5eAaPmAgInS
sen	sen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavil	postavit	k5eAaPmAgMnS
si	se	k3xPyFc3
dům	dům	k1gInSc4
z	z	k7c2
kostek	kostka	k1gFnPc2
lega	lego	k1gNnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-09-04	2009-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pořad	pořad	k1gInSc1
<g/>
.	.	kIx.
cool	cool	k1gInSc1
<g/>
.	.	kIx.
<g/>
iprima	iprima	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hybrid	hybrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hybrid	hybrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Toyota	toyota	k1gFnSc1
Hilux	Hilux	k1gInSc1
<g/>
:	:	kIx,
Dostaveníčko	dostaveníčko	k1gNnSc1
u	u	k7c2
islandského	islandský	k2eAgInSc2d1
vulkánu	vulkán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auto	auto	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Moderátor	moderátor	k1gMnSc1
Clarkson	Clarkson	k1gMnSc1
si	se	k3xPyFc3
díky	díky	k7c3
novému	nový	k2eAgInSc3d1
pořadu	pořad	k1gInSc3
vydělá	vydělat	k5eAaPmIp3nS
stamilióny	stamilióny	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Matt	Matt	k2eAgInSc4d1
LeBlanc	LeBlanc	k1gInSc4
ze	z	k7c2
seriálu	seriál	k1gInSc2
Přátelé	přítel	k1gMnPc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
moderátorem	moderátor	k1gMnSc7
Top	topit	k5eAaImRp2nS
Gearu	Gear	k1gInSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-02-04	2016-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
pořadu	pořad	k1gInSc2
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
</s>
<s>
Fifth	Fifth	k1gMnSc1
Gear	Gear	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
na	na	k7c6
Twitteru	Twitter	k1gInSc6
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
na	na	k7c6
SerialZone	SerialZon	k1gInSc5
</s>
<s>
České	český	k2eAgFnPc1d1
fanouškovské	fanouškovský	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
České	český	k2eAgFnPc1d1
fanouškovské	fanouškovský	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
unn	unn	k?
<g/>
2013741883	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2009031634	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
184456118	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2009031634	#num#	k4
</s>
