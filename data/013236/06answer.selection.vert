<s>
Přídavek	přídavek	k1gInSc1	přídavek
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
manganu	mangan	k1gInSc2	mangan
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
může	moct	k5eAaImIp3nS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
jasnost	jasnost	k1gFnSc4	jasnost
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
zelenavý	zelenavý	k2eAgInSc1d1	zelenavý
nádech	nádech	k1gInSc1	nádech
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
stopy	stopa	k1gFnPc4	stopa
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
