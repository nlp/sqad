<s>
Afrodíté	Afrodíta	k1gMnPc1	Afrodíta
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Ἀ	Ἀ	k?	Ἀ
<g/>
,	,	kIx,	,
v	v	k7c6	v
čestině	čestina	k1gFnSc6	čestina
též	též	k9	též
Afrodita	Afrodita	k1gFnSc1	Afrodita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
,	,	kIx,	,
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
;	;	kIx,	;
vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgNnSc2	tento
nejrozšířenějšího	rozšířený	k2eAgNnSc2d3	nejrozšířenější
pojetí	pojetí	k1gNnSc2	pojetí
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
i	i	k9	i
zcela	zcela	k6eAd1	zcela
odlišných	odlišný	k2eAgInPc2d1	odlišný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k9	i
nevyzpytatelných	vyzpytatelný	k2eNgInPc2d1	nevyzpytatelný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
bohyní	bohyně	k1gFnPc2	bohyně
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
dvanáct	dvanáct	k4xCc4	dvanáct
Olympanů	Olympan	k1gMnPc2	Olympan
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
příběhů	příběh	k1gInPc2	příběh
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
řeckém	řecký	k2eAgInSc6d1	řecký
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
antickém	antický	k2eAgInSc6d1	antický
světě	svět	k1gInSc6	svět
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
,	,	kIx,	,
v	v	k7c6	v
helénistickém	helénistický	k2eAgInSc6d1	helénistický
kontextu	kontext	k1gInSc6	kontext
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
Isidou	Isida	k1gFnSc7	Isida
<g/>
.	.	kIx.	.
</s>
<s>
Kult	kult	k1gInSc1	kult
Afrodíty	Afrodíta	k1gFnSc2	Afrodíta
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
syrskofénickou	syrskofénický	k2eAgFnSc7d1	syrskofénický
bohyní	bohyně	k1gFnSc7	bohyně
Astartou	Astarta	k1gFnSc7	Astarta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
snad	snad	k9	snad
byla	být	k5eAaImAgFnS	být
jejím	její	k3xOp3gInSc7	její
předobrazem	předobraz	k1gInSc7	předobraz
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
v	v	k7c6	v
homérských	homérský	k2eAgInPc6d1	homérský
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
Afrodítin	Afrodítin	k2eAgInSc4d1	Afrodítin
charakter	charakter	k1gInSc4	charakter
zcela	zcela	k6eAd1	zcela
řecký	řecký	k2eAgMnSc1d1	řecký
a	a	k8xC	a
přes	přes	k7c4	přes
nedoloženost	nedoloženost	k1gFnSc4	nedoloženost
jejího	její	k3xOp3gNnSc2	její
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
lineárním	lineární	k2eAgNnSc6d1	lineární
písmu	písmo	k1gNnSc6	písmo
B	B	kA	B
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
již	již	k6eAd1	již
v	v	k7c6	v
mykénském	mykénský	k2eAgNnSc6d1	mykénské
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bohyně	bohyně	k1gFnSc1	bohyně
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
kráse	krása	k1gFnSc3	krása
a	a	k8xC	a
kouzlům	kouzlo	k1gNnPc3	kouzlo
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
bohyň	bohyně	k1gFnPc2	bohyně
<g/>
,	,	kIx,	,
neodolali	odolat	k5eNaPmAgMnP	odolat
jí	jíst	k5eAaImIp3nS	jíst
lidé	člověk	k1gMnPc1	člověk
ani	ani	k8xC	ani
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
boha	bůh	k1gMnSc2	bůh
Héfaista	Héfaist	k1gMnSc2	Héfaist
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
otcem	otec	k1gMnSc7	otec
jejích	její	k3xOp3gMnPc2	její
synů	syn	k1gMnPc2	syn
Foba	Fobus	k1gMnSc2	Fobus
<g/>
,	,	kIx,	,
Deima	Deim	k1gMnSc2	Deim
a	a	k8xC	a
Eróta	Erós	k1gMnSc2	Erós
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Harmonie	harmonie	k1gFnSc1	harmonie
byl	být	k5eAaImAgMnS	být
bůh	bůh	k1gMnSc1	bůh
války	válka	k1gFnSc2	válka
Árés	Árés	k1gInSc1	Árés
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
pocestných	pocestná	k1gFnPc2	pocestná
Hermem	Hermes	k1gMnSc7	Hermes
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Hermafrodíta	Hermafrodít	k1gMnSc4	Hermafrodít
a	a	k8xC	a
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
moře	moře	k1gNnSc2	moře
Poseidónem	Poseidón	k1gMnSc7	Poseidón
další	další	k2eAgMnPc4d1	další
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Rhoda	Rhod	k1gMnSc4	Rhod
a	a	k8xC	a
Hérofila	Hérofil	k1gMnSc4	Hérofil
<g/>
.	.	kIx.	.
</s>
<s>
Bohu	bůh	k1gMnSc3	bůh
vína	víno	k1gNnSc2	víno
Dionýsovi	Dionýsos	k1gMnSc3	Dionýsos
zplodila	zplodit	k5eAaPmAgFnS	zplodit
syna	syn	k1gMnSc4	syn
Priapa	Priapos	k1gMnSc4	Priapos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
snad	snad	k9	snad
nejčastěji	často	k6eAd3	často
zobrazovanou	zobrazovaný	k2eAgFnSc7d1	zobrazovaná
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c6	na
mozaikách	mozaika	k1gFnPc6	mozaika
a	a	k8xC	a
vázových	vázový	k2eAgFnPc6d1	vázový
malbách	malba	k1gFnPc6	malba
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
sochách	socha	k1gFnPc6	socha
a	a	k8xC	a
na	na	k7c6	na
reliéfech	reliéf	k1gInPc6	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
reliéfní	reliéfní	k2eAgNnSc1d1	reliéfní
zobrazení	zobrazení	k1gNnSc1	zobrazení
Afrodity	Afrodita	k1gFnSc2	Afrodita
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Trůnu	trůn	k1gInSc2	trůn
Ludovisi	Ludovise	k1gFnSc4	Ludovise
z	z	k7c2	z
roku	rok	k1gInSc2	rok
460	[number]	k4	460
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
pěny	pěna	k1gFnSc2	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
sochařskými	sochařský	k2eAgFnPc7d1	sochařská
znázorněními	znázornění	k1gNnPc7	znázornění
jsou	být	k5eAaImIp3nP	být
Praxitelova	Praxitelův	k2eAgFnSc1d1	Praxitelova
Afrodita	Afrodita	k1gFnSc1	Afrodita
Knidská	Knidský	k2eAgFnSc1d1	Knidská
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
Afrodita	Afrodita	k1gFnSc1	Afrodita
Mélská	Mélská	k1gFnSc1	Mélská
(	(	kIx(	(
<g/>
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k8xS	jako
Venuše	Venuše	k1gFnSc1	Venuše
Mélská	Mélská	k1gFnSc1	Mélská
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
130	[number]	k4	130
–	–	k?	–
100	[number]	k4	100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
galerii	galerie	k1gFnSc6	galerie
Louvre	Louvre	k1gInSc4	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Řekové	Řek	k1gMnPc1	Řek
jí	on	k3xPp3gFnSc3	on
zasvětili	zasvětit	k5eAaPmAgMnP	zasvětit
několik	několik	k4yIc1	několik
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
:	:	kIx,	:
delfína	delfín	k1gMnSc4	delfín
<g/>
,	,	kIx,	,
holubici	holubice	k1gFnSc4	holubice
<g/>
,	,	kIx,	,
vrabce	vrabec	k1gMnSc4	vrabec
a	a	k8xC	a
vlaštovku	vlaštovka	k1gFnSc4	vlaštovka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
:	:	kIx,	:
mák	mák	k1gInSc1	mák
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
granátové	granátový	k2eAgNnSc4d1	granátové
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
myrtu	myrta	k1gFnSc4	myrta
a	a	k8xC	a
růži	růže	k1gFnSc4	růže
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
studií	studio	k1gNnPc2	studio
byla	být	k5eAaImAgFnS	být
Afrodita	Afrodita	k1gFnSc1	Afrodita
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bohyně	bohyně	k1gFnSc2	bohyně
maloasijského	maloasijský	k2eAgInSc2d1	maloasijský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
syrsko-foinické	syrskooinický	k2eAgFnSc2d1	syrsko-foinický
bohyně	bohyně	k1gFnSc2	bohyně
Astarty	Astarta	k1gFnSc2	Astarta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
asyrsko-babylónské	asyrskoabylónský	k2eAgFnSc6d1	asyrsko-babylónský
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
Ištar	Ištara	k1gFnPc2	Ištara
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
jí	on	k3xPp3gFnSc3	on
převzali	převzít	k5eAaPmAgMnP	převzít
už	už	k6eAd1	už
v	v	k7c6	v
období	období	k1gNnSc6	období
krétsko-mykénské	krétskoykénský	k2eAgFnSc2d1	krétsko-mykénský
civilizace	civilizace	k1gFnSc2	civilizace
přes	přes	k7c4	přes
ostrov	ostrov	k1gInSc4	ostrov
Kypr	Kypr	k1gInSc1	Kypr
a	a	k8xC	a
Kythéru	Kythér	k1gInSc6	Kythér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
byla	být	k5eAaImAgNnP	být
nejvíce	hodně	k6eAd3	hodně
uctívána	uctívat	k5eAaImNgNnP	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Bohyni	bohyně	k1gFnSc3	bohyně
Afroditě	Afrodita	k1gFnSc3	Afrodita
zasvětili	zasvětit	k5eAaPmAgMnP	zasvětit
myrtu	myrta	k1gFnSc4	myrta
<g/>
,	,	kIx,	,
růži	růže	k1gFnSc4	růže
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
holubici	holubice	k1gFnSc4	holubice
<g/>
,	,	kIx,	,
delfína	delfín	k1gMnSc4	delfín
<g/>
,	,	kIx,	,
vlaštovku	vlaštovka	k1gFnSc4	vlaštovka
a	a	k8xC	a
lípu	lípa	k1gFnSc4	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
mnoho	mnoho	k4c1	mnoho
chrámů	chrám	k1gInPc2	chrám
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
kult	kult	k1gInSc1	kult
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
ztotožněna	ztotožnit	k5eAaPmNgFnS	ztotožnit
se	s	k7c7	s
staroitalskou	staroitalský	k2eAgFnSc7d1	staroitalská
bohyní	bohyně	k1gFnSc7	bohyně
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
Venuší	Venuše	k1gFnPc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
slavné	slavný	k2eAgInPc4d1	slavný
římské	římský	k2eAgInPc4d1	římský
chrámy	chrám	k1gInPc4	chrám
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
chrám	chrám	k1gInSc1	chrám
Venuše	Venuše	k1gFnSc2	Venuše
Roditelky	roditelka	k1gFnSc2	roditelka
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Romy	Rom	k1gMnPc4	Rom
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Afroditin	Afroditin	k2eAgInSc1d1	Afroditin
kult	kult	k1gInSc1	kult
byl	být	k5eAaImAgInS	být
vytlačen	vytlačit	k5eAaPmNgInS	vytlačit
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Řečtí	řecký	k2eAgMnPc1d1	řecký
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
v	v	k7c6	v
Afroditině	Afroditin	k2eAgInSc6d1	Afroditin
původu	původ	k1gInSc6	původ
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Homéra	Homér	k1gMnSc2	Homér
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
deště	dešť	k1gInSc2	dešť
Dióny	Dióna	k1gFnSc2	Dióna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
básníka	básník	k1gMnSc2	básník
Hésioda	Hésiod	k1gMnSc2	Hésiod
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
pěny	pěna	k1gFnSc2	pěna
oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
genitáliemi	genitálie	k1gFnPc7	genitálie
boha	bůh	k1gMnSc2	bůh
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
Urana	Uran	k1gMnSc2	Uran
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
uřízl	uříznout	k5eAaPmAgInS	uříznout
a	a	k8xC	a
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
odhodil	odhodit	k5eAaPmAgMnS	odhodit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Kronos	Kronos	k1gMnSc1	Kronos
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
vyvrhnuta	vyvrhnout	k5eAaPmNgFnS	vyvrhnout
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kypr	Kypr	k1gInSc1	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
aphros	aphrosa	k1gFnPc2	aphrosa
(	(	kIx(	(
<g/>
mořská	mořský	k2eAgFnSc1d1	mořská
pěna	pěna	k1gFnSc1	pěna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
Afroditě	Afrodita	k1gFnSc3	Afrodita
říká	říkat	k5eAaImIp3nS	říkat
Aphrogenia	Aphrogenium	k1gNnPc4	Aphrogenium
(	(	kIx(	(
<g/>
Z	z	k7c2	z
pěny	pěna	k1gFnSc2	pěna
narozená	narozený	k2eAgFnSc1d1	narozená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
nesla	nést	k5eAaImAgFnS	nést
přídomek	přídomek	k1gInSc4	přídomek
Anadyomené	Anadyomený	k2eAgFnSc2d1	Anadyomený
–	–	k?	–
"	"	kIx"	"
<g/>
vynořující	vynořující	k2eAgInSc1d1	vynořující
se	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
Diem	Diem	k1gInSc4	Diem
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
kulhavého	kulhavý	k2eAgMnSc2d1	kulhavý
boha	bůh	k1gMnSc2	bůh
ohně	oheň	k1gInSc2	oheň
Héfaista	Héfaist	k1gMnSc4	Héfaist
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Afrodity	Afrodita	k1gFnSc2	Afrodita
zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
družka	družka	k1gFnSc1	družka
mu	on	k3xPp3gMnSc3	on
lásku	láska	k1gFnSc4	láska
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Nemohla	moct	k5eNaImAgFnS	moct
se	se	k3xPyFc4	se
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
manželovým	manželův	k2eAgInSc7d1	manželův
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
trávila	trávit	k5eAaImAgFnS	trávit
noci	noc	k1gFnSc6	noc
milostnými	milostný	k2eAgFnPc7d1	milostná
radovánkami	radovánka	k1gFnPc7	radovánka
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
války	válka	k1gFnSc2	válka
Áreem	Áres	k1gMnSc7	Áres
<g/>
,	,	kIx,	,
mužem	muž	k1gMnSc7	muž
s	s	k7c7	s
pohlednou	pohledný	k2eAgFnSc7d1	pohledná
tváří	tvář	k1gFnSc7	tvář
a	a	k8xC	a
silným	silný	k2eAgNnSc7d1	silné
<g/>
,	,	kIx,	,
statným	statný	k2eAgNnSc7d1	statné
a	a	k8xC	a
vypracovaným	vypracovaný	k2eAgNnSc7d1	vypracované
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Pokaždé	pokaždé	k6eAd1	pokaždé
když	když	k8xS	když
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
opustil	opustit	k5eAaPmAgMnS	opustit
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
odešla	odejít	k5eAaPmAgFnS	odejít
bohyně	bohyně	k1gFnSc1	bohyně
za	za	k7c7	za
milencem	milenec	k1gMnSc7	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
by	by	kYmCp3nP	by
je	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
nepodezíral	podezírat	k5eNaImAgMnS	podezírat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
Hélios	Hélios	k1gMnSc1	Hélios
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
nespatřil	spatřit	k5eNaPmAgInS	spatřit
v	v	k7c6	v
Áreově	Áreův	k2eAgInSc6d1	Áreův
paláci	palác	k1gInSc6	palác
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
činu	čin	k1gInSc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
informoval	informovat	k5eAaBmAgMnS	informovat
Héfaista	Héfaista	k1gMnSc1	Héfaista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
i	i	k9	i
přes	přes	k7c4	přes
ohromný	ohromný	k2eAgInSc4d1	ohromný
vztek	vztek	k1gInSc4	vztek
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
milovanou	milovaný	k2eAgFnSc4d1	milovaná
ženu	žena	k1gFnSc4	žena
dokázal	dokázat	k5eAaPmAgMnS	dokázat
uchovat	uchovat	k5eAaPmF	uchovat
chladnou	chladný	k2eAgFnSc4d1	chladná
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
milence	milenec	k1gMnSc4	milenec
přistihnout	přistihnout	k5eAaPmF	přistihnout
a	a	k8xC	a
obvinit	obvinit	k5eAaPmF	obvinit
je	on	k3xPp3gInPc4	on
před	před	k7c7	před
všemi	všecek	k3xTgMnPc7	všecek
bohy	bůh	k1gMnPc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
ukoval	ukovat	k5eAaPmAgMnS	ukovat
nezničitelnou	zničitelný	k2eNgFnSc4d1	nezničitelná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jemnou	jemný	k2eAgFnSc4d1	jemná
a	a	k8xC	a
hebkou	hebký	k2eAgFnSc4d1	hebká
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
potají	potají	k6eAd1	potají
připevnil	připevnit	k5eAaPmAgInS	připevnit
na	na	k7c4	na
lože	lože	k1gNnSc4	lože
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
Afrodita	Afrodita	k1gFnSc1	Afrodita
a	a	k8xC	a
Áres	Áres	k1gMnSc1	Áres
milovali	milovat	k5eAaImAgMnP	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgMnS	oznámit
pak	pak	k6eAd1	pak
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Lémnos	Lémnosa	k1gFnPc2	Lémnosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rád	rád	k6eAd1	rád
trávil	trávit	k5eAaImAgMnS	trávit
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
ihned	ihned	k6eAd1	ihned
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
boha	bůh	k1gMnSc2	bůh
války	válka	k1gFnSc2	válka
pozvala	pozvat	k5eAaPmAgFnS	pozvat
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
to	ten	k3xDgNnSc4	ten
moc	moc	k6eAd1	moc
dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
a	a	k8xC	a
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
ráno	ráno	k1gNnSc4	ráno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
po	po	k7c6	po
noci	noc	k1gFnSc6	noc
plné	plný	k2eAgFnSc2d1	plná
rozkoše	rozkoš	k1gFnSc2	rozkoš
probudili	probudit	k5eAaPmAgMnP	probudit
a	a	k8xC	a
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
uvěznění	uvěznění	k1gNnSc4	uvěznění
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
kovář	kovář	k1gMnSc1	kovář
povolal	povolat	k5eAaPmAgMnS	povolat
všechny	všechen	k3xTgMnPc4	všechen
bohy	bůh	k1gMnPc4	bůh
ke	k	k7c3	k
spoutaným	spoutaný	k2eAgMnPc3d1	spoutaný
milencům	milenec	k1gMnPc3	milenec
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
nevěrná	věrný	k2eNgFnSc1d1	nevěrná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
otci	otec	k1gMnSc6	otec
Diovi	Diův	k2eAgMnPc1d1	Diův
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
adoptivním	adoptivní	k2eAgMnPc3d1	adoptivní
<g/>
)	)	kIx)	)
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
tučné	tučný	k2eAgNnSc4d1	tučné
výkupné	výkupné	k1gNnSc4	výkupné
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
že	že	k8xS	že
bohyni	bohyně	k1gFnSc4	bohyně
nepropustí	propustit	k5eNaPmIp3nS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Áreeovi	Áreeus	k1gMnSc3	Áreeus
dal	dát	k5eAaPmAgMnS	dát
stejnou	stejný	k2eAgFnSc4d1	stejná
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
se	se	k3xPyFc4	se
Héfaistovi	Héfaista	k1gMnSc3	Héfaista
jen	jen	k9	jen
vysmál	vysmát	k5eAaPmAgMnS	vysmát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Poseidon	Poseidon	k1gMnSc1	Poseidon
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
nahou	nahý	k2eAgFnSc4d1	nahá
bohyni	bohyně	k1gFnSc4	bohyně
lásky	láska	k1gFnSc2	láska
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
totiž	totiž	k9	totiž
bezhlavě	bezhlavě	k6eAd1	bezhlavě
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ještě	ještě	k9	ještě
doufal	doufat	k5eAaImAgMnS	doufat
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Áres	Áres	k1gMnSc1	Áres
výkupné	výkupné	k1gNnSc4	výkupné
nezaplatí	zaplatit	k5eNaPmIp3nS	zaplatit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Afroditu	Afrodita	k1gFnSc4	Afrodita
mohl	moct	k5eAaImAgMnS	moct
sám	sám	k3xTgInSc4	sám
vzít	vzít	k5eAaPmF	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
manžel	manžel	k1gMnSc1	manžel
tak	tak	k9	tak
hněval	hněvat	k5eAaImAgMnS	hněvat
<g/>
.	.	kIx.	.
</s>
<s>
Hermés	Hermés	k1gInSc1	Hermés
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
pocestných	pocestný	k1gMnPc2	pocestný
a	a	k8xC	a
kupců	kupec	k1gMnPc2	kupec
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
milenci	milenec	k1gMnPc7	milenec
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
nevadilo	vadit	k5eNaImAgNnS	vadit
být	být	k5eAaImF	být
pod	pod	k7c7	pod
třemi	tři	k4xCgInPc7	tři
takovými	takový	k3xDgFnPc7	takový
sítěmi	síť	k1gFnPc7	síť
<g/>
,	,	kIx,	,
jen	jen	k9	jen
kdyby	kdyby	kYmCp3nS	kdyby
měl	mít	k5eAaImAgMnS	mít
Afroditu	Afrodita	k1gFnSc4	Afrodita
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
tak	tak	k6eAd1	tak
situaci	situace	k1gFnSc4	situace
zesměšnil	zesměšnit	k5eAaPmAgMnS	zesměšnit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
nucen	nucen	k2eAgInSc1d1	nucen
milence	milenka	k1gFnSc3	milenka
propustit	propustit	k5eAaPmF	propustit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
získal	získat	k5eAaPmAgMnS	získat
jediný	jediný	k2eAgInSc4d1	jediný
peníz	peníz	k1gInSc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
s	s	k7c7	s
Afroditou	Afrodita	k1gFnSc7	Afrodita
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
nerozvedl	rozvést	k5eNaPmAgMnS	rozvést
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gInPc4	její
zálety	zálet	k1gInPc4	zálet
stále	stále	k6eAd1	stále
miloval	milovat	k5eAaImAgInS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
potěšena	potěšit	k5eAaPmNgFnS	potěšit
vyznáním	vyznání	k1gNnSc7	vyznání
lásky	láska	k1gFnSc2	láska
jak	jak	k8xC	jak
od	od	k7c2	od
Poseidona	Poseidon	k1gMnSc2	Poseidon
<g/>
,	,	kIx,	,
tak	tak	k9	tak
od	od	k7c2	od
Herma	Hermes	k1gMnSc2	Hermes
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
vdaná	vdaný	k2eAgFnSc1d1	vdaná
<g/>
,	,	kIx,	,
jako	jako	k9	jako
milence	milenec	k1gMnPc4	milenec
měla	mít	k5eAaImAgFnS	mít
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
Aárea	Aáre	k2eAgFnSc1d1	Aáre
<g/>
,	,	kIx,	,
strávila	strávit	k5eAaPmAgFnS	strávit
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
několik	několik	k4yIc4	několik
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Bohu	bůh	k1gMnSc3	bůh
moře	moře	k1gNnSc2	moře
pak	pak	k6eAd1	pak
porodila	porodit	k5eAaPmAgFnS	porodit
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Rhoda	Rhod	k1gMnSc4	Rhod
a	a	k8xC	a
Hérofila	Hérofil	k1gMnSc4	Hérofil
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Hermem	Hermes	k1gMnSc7	Hermes
zplodila	zplodit	k5eAaPmAgFnS	zplodit
Hermafrodíta	Hermafrodíta	k1gFnSc1	Hermafrodíta
<g/>
,	,	kIx,	,
krásného	krásný	k2eAgMnSc2d1	krásný
potomka	potomek	k1gMnSc2	potomek
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mužského	mužský	k2eAgNnSc2d1	mužské
<g/>
.	.	kIx.	.
</s>
<s>
Psyché	psyché	k1gFnSc1	psyché
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
krásná	krásný	k2eAgFnSc1d1	krásná
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
natolik	natolik	k6eAd1	natolik
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
samotnou	samotný	k2eAgFnSc4d1	samotná
Afroditu	Afrodita	k1gFnSc4	Afrodita
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgInS	ujmout
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
Afrodita	Afrodita	k1gFnSc1	Afrodita
neoplývá	oplývat	k5eNaImIp3nS	oplývat
takovou	takový	k3xDgFnSc7	takový
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
začali	začít	k5eAaPmAgMnP	začít
uctívat	uctívat	k5eAaImF	uctívat
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
bohyni	bohyně	k1gFnSc4	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k4c1	málo
věcí	věc	k1gFnPc2	věc
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
Afroditu	Afrodita	k1gFnSc4	Afrodita
takto	takto	k6eAd1	takto
rozzuřit	rozzuřit	k5eAaPmF	rozzuřit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
povolala	povolat	k5eAaPmAgFnS	povolat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Érota	Érot	k1gMnSc4	Érot
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Psyché	psyché	k1gFnSc4	psyché
pomstil	pomstit	k5eAaImAgMnS	pomstit
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
ji	on	k3xPp3gFnSc4	on
svým	svůj	k3xOyFgInSc7	svůj
šípem	šíp	k1gInSc7	šíp
lásky	láska	k1gFnSc2	láska
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
toho	ten	k3xDgMnSc2	ten
nejhoršího	zlý	k2eAgMnSc2d3	nejhorší
muže	muž	k1gMnSc2	muž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
bůžek	bůžek	k1gMnSc1	bůžek
lásky	láska	k1gFnSc2	láska
už	už	k6eAd1	už
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
pohledu	pohled	k1gInSc6	pohled
na	na	k7c6	na
Psyché	psyché	k1gFnSc6	psyché
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
šíp	šíp	k1gInSc4	šíp
vystřelit	vystřelit	k5eAaPmF	vystřelit
z	z	k7c2	z
luku	luk	k1gInSc2	luk
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
tak	tak	k8xC	tak
matku	matka	k1gFnSc4	matka
neposlechl	poslechnout	k5eNaPmAgMnS	poslechnout
a	a	k8xC	a
luk	luk	k1gInSc4	luk
i	i	k8xC	i
šíp	šíp	k1gInSc4	šíp
schoval	schovat	k5eAaPmAgMnS	schovat
<g/>
.	.	kIx.	.
</s>
<s>
Éros	Éros	k1gInSc1	Éros
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Psyché	psyché	k1gFnSc2	psyché
tak	tak	k6eAd1	tak
zamilován	zamilován	k2eAgMnSc1d1	zamilován
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pojmout	pojmout	k5eAaPmF	pojmout
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
svázána	svázat	k5eAaPmNgFnS	svázat
jednou	jednou	k6eAd1	jednou
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nesměla	smět	k5eNaImAgFnS	smět
snažit	snažit	k5eAaImF	snažit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
na	na	k7c4	na
naléhání	naléhání	k1gNnPc4	naléhání
sester	sestra	k1gFnPc2	sestra
Psyché	psyché	k1gFnSc1	psyché
toto	tento	k3xDgNnSc4	tento
nařízení	nařízení	k1gNnSc4	nařízení
porušila	porušit	k5eAaPmAgFnS	porušit
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
když	když	k8xS	když
Éros	Éros	k1gInSc1	Éros
spal	spát	k5eAaImAgInS	spát
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
prohlédla	prohlédnout	k5eAaPmAgFnS	prohlédnout
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
lampy	lampa	k1gFnSc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
krása	krása	k1gFnSc1	krása
jí	on	k3xPp3gFnSc3	on
vzala	vzít	k5eAaPmAgFnS	vzít
dech	dech	k1gInSc4	dech
a	a	k8xC	a
upustila	upustit	k5eAaPmAgFnS	upustit
lampu	lampa	k1gFnSc4	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Éros	Éros	k6eAd1	Éros
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
lampy	lampa	k1gFnSc2	lampa
popálen	popálen	k2eAgMnSc1d1	popálen
<g/>
.	.	kIx.	.
</s>
<s>
Beze	beze	k7c2	beze
slova	slovo	k1gNnSc2	slovo
roztáhl	roztáhnout	k5eAaPmAgMnS	roztáhnout
svá	svůj	k3xOyFgNnPc4	svůj
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
odletěl	odletět	k5eAaPmAgMnS	odletět
<g/>
.	.	kIx.	.
</s>
<s>
Psyché	psyché	k1gFnSc4	psyché
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
hledala	hledat	k5eAaImAgFnS	hledat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
zoufalosti	zoufalost	k1gFnSc6	zoufalost
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
jít	jít	k5eAaImF	jít
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
Afroditou	Afrodita	k1gFnSc7	Afrodita
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
sice	sice	k8xC	sice
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Éros	Éros	k1gInSc1	Éros
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehodlala	hodlat	k5eNaImAgFnS	hodlat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
pustit	pustit	k5eAaPmF	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
to	ten	k3xDgNnSc1	ten
udělat	udělat	k5eAaPmF	udělat
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
Psyché	psyché	k1gFnSc1	psyché
splnila	splnit	k5eAaPmAgFnS	splnit
tři	tři	k4xCgInPc4	tři
nemyslitelné	myslitelný	k2eNgInPc4d1	nemyslitelný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Milující	milující	k2eAgFnSc1d1	milující
dívka	dívka	k1gFnSc1	dívka
ani	ani	k8xC	ani
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
nezaváhala	zaváhat	k5eNaPmAgFnS	zaváhat
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
úkoly	úkol	k1gInPc7	úkol
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Psyché	psyché	k1gFnSc7	psyché
se	se	k3xPyFc4	se
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
nemožných	možný	k2eNgInPc2d1	nemožný
úkolů	úkol	k1gInPc2	úkol
vždy	vždy	k6eAd1	vždy
někdo	někdo	k3yInSc1	někdo
slitoval	slitovat	k5eAaPmAgMnS	slitovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
posazena	posadit	k5eAaPmNgFnS	posadit
před	před	k7c4	před
hromadu	hromada	k1gFnSc4	hromada
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
luštěnin	luštěnina	k1gFnPc2	luštěnina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
roztřídila	roztřídit	k5eAaPmAgFnS	roztřídit
každé	každý	k3xTgNnSc1	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
pomoci	pomoc	k1gFnSc3	pomoc
od	od	k7c2	od
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
misky	miska	k1gFnSc2	miska
nabrat	nabrat	k5eAaPmF	nabrat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spadal	spadat	k5eAaPmAgInS	spadat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
pomoc	pomoc	k1gFnSc4	pomoc
jí	on	k3xPp3gFnSc2	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
úkol	úkol	k1gInSc1	úkol
byl	být	k5eAaImAgInS	být
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
<g/>
.	.	kIx.	.
</s>
<s>
Psyché	psyché	k1gFnSc1	psyché
stála	stát	k5eAaImAgFnS	stát
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
vládkyní	vládkyně	k1gFnSc7	vládkyně
Persefonou	Persefona	k1gFnSc7	Persefona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
přinesla	přinést	k5eAaPmAgFnS	přinést
pro	pro	k7c4	pro
Érota	Érot	k1gMnSc4	Érot
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
mast	mast	k1gFnSc4	mast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zhojit	zhojit	k5eAaPmF	zhojit
jeho	jeho	k3xOp3gFnSc4	jeho
popáleninu	popálenina	k1gFnSc4	popálenina
od	od	k7c2	od
lampy	lampa	k1gFnSc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
?	?	kIx.	?
</s>
<s>
Jedině	jedině	k6eAd1	jedině
snad	snad	k9	snad
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
Psyché	psyché	k1gFnSc1	psyché
vzala	vzít	k5eAaPmAgFnS	vzít
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
když	když	k8xS	když
vylezla	vylézt	k5eAaPmAgFnS	vylézt
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vrhla	vrhnout	k5eAaPmAgFnS	vrhnout
<g/>
,	,	kIx,	,
poradila	poradit	k5eAaPmAgFnS	poradit
jí	jíst	k5eAaImIp3nS	jíst
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
k	k	k7c3	k
bráně	brána	k1gFnSc3	brána
podsvětí	podsvětí	k1gNnSc2	podsvětí
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
zlatými	zlatý	k2eAgInPc7d1	zlatý
penízky	penízek	k1gInPc7	penízek
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
koláči	koláč	k1gInPc7	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc4	peníz
darovala	darovat	k5eAaPmAgFnS	darovat
převozníku	převozník	k1gMnSc3	převozník
Cháronovi	Cháron	k1gMnSc3	Cháron
za	za	k7c4	za
převoz	převoz	k1gInSc4	převoz
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
tříhlavému	tříhlavý	k2eAgInSc3d1	tříhlavý
psu	pes	k1gMnSc6	pes
Kerberovi	Kerber	k1gMnSc3	Kerber
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
nechal	nechat	k5eAaPmAgInS	nechat
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Psyché	psyché	k1gFnSc1	psyché
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
od	od	k7c2	od
Persefony	Persefona	k1gFnSc2	Persefona
vrátila	vrátit	k5eAaPmAgFnS	vrátit
s	s	k7c7	s
krabičkou	krabička	k1gFnSc7	krabička
s	s	k7c7	s
léčivou	léčivý	k2eAgFnSc7d1	léčivá
mastí	mast	k1gFnSc7	mast
<g/>
.	.	kIx.	.
</s>
<s>
Splnila	splnit	k5eAaPmAgFnS	splnit
tak	tak	k9	tak
i	i	k9	i
poslední	poslední	k2eAgInSc1d1	poslední
úkol	úkol	k1gInSc1	úkol
a	a	k8xC	a
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
spokojena	spokojen	k2eAgFnSc1d1	spokojena
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
svolila	svolit	k5eAaPmAgFnS	svolit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Zeus	Zeus	k1gInSc4	Zeus
Psyché	psyché	k1gFnSc2	psyché
daroval	darovat	k5eAaPmAgMnS	darovat
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
a	a	k8xC	a
s	s	k7c7	s
Érotem	Érot	k1gInSc7	Érot
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
provdal	provdat	k5eAaPmAgInS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
pomstychtivá	pomstychtivý	k2eAgFnSc1d1	pomstychtivá
vůči	vůči	k7c3	vůči
každé	každý	k3xTgFnSc3	každý
ženě	žena	k1gFnSc3	žena
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
o	o	k7c4	o
sobě	se	k3xPyFc3	se
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
krásnější	krásný	k2eAgFnSc1d2	krásnější
než	než	k8xS	než
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
kyperský	kyperský	k2eAgMnSc1d1	kyperský
král	král	k1gMnSc1	král
Kinyrás	Kinyrása	k1gFnPc2	Kinyrása
opovážil	opovážit	k5eAaPmAgMnS	opovážit
říct	říct	k5eAaPmF	říct
to	ten	k3xDgNnSc4	ten
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
dceři	dcera	k1gFnSc6	dcera
<g/>
,	,	kIx,	,
Afrodita	Afrodita	k1gFnSc1	Afrodita
potrestala	potrestat	k5eAaPmAgFnS	potrestat
jak	jak	k6eAd1	jak
jeho	jeho	k3xOp3gNnSc2	jeho
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
dceru	dcera	k1gFnSc4	dcera
Myrrhu	Myrrh	k1gInSc2	Myrrh
<g/>
.	.	kIx.	.
</s>
<s>
Donutila	donutit	k5eAaPmAgFnS	donutit
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
a	a	k8xC	a
pomilovala	pomilovat	k5eAaPmAgFnS	pomilovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
opilý	opilý	k2eAgMnSc1d1	opilý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Kinyrás	Kinyrás	k1gInSc1	Kinyrás
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
Myrrhu	Myrrha	k1gMnSc4	Myrrha
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Afrodita	Afrodita	k1gFnSc1	Afrodita
jí	jíst	k5eAaImIp3nS	jíst
před	před	k7c7	před
takovým	takový	k3xDgInSc7	takový
osudem	osud	k1gInSc7	osud
zachránila	zachránit	k5eAaPmAgFnS	zachránit
–	–	k?	–
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ji	on	k3xPp3gFnSc4	on
ve	v	k7c4	v
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Kinyrás	Kinyrás	k1gInSc1	Kinyrás
ťal	tít	k5eAaPmAgInS	tít
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
a	a	k8xC	a
rozsekl	rozseknout	k5eAaPmAgMnS	rozseknout
ho	on	k3xPp3gNnSc4	on
vedví	vedví	k6eAd1	vedví
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
pak	pak	k6eAd1	pak
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Myrrha	Myrrh	k1gMnSc4	Myrrh
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
zplodila	zplodit	k5eAaPmAgFnS	zplodit
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
jejich	jejich	k3xOp3gMnSc4	jejich
syna	syn	k1gMnSc4	syn
před	před	k7c7	před
králem	král	k1gMnSc7	král
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc1	jméno
Adonis	Adonis	k1gFnSc2	Adonis
<g/>
.	.	kIx.	.
</s>
<s>
Bála	bát	k5eAaImAgFnS	bát
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
vychovat	vychovat	k5eAaPmF	vychovat
sama	sám	k3xTgMnSc4	sám
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
nevzbudil	vzbudit	k5eNaPmAgMnS	vzbudit
rozruch	rozruch	k1gInSc4	rozruch
pro	pro	k7c4	pro
její	její	k3xOp3gInPc4	její
zálety	zálet	k1gInPc4	zálet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
chlapce	chlapec	k1gMnPc4	chlapec
svěřila	svěřit	k5eAaPmAgFnS	svěřit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Persefony	Persefona	k1gFnSc2	Persefona
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Háda	Hádes	k1gMnSc2	Hádes
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Adonis	Adonis	k1gInSc1	Adonis
dospěl	dochvít	k5eAaPmAgInS	dochvít
v	v	k7c4	v
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
obě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
zamilovány	zamilován	k2eAgFnPc1d1	zamilována
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
nemohly	moct	k5eNaImAgFnP	moct
se	se	k3xPyFc4	se
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
užívat	užívat	k5eAaImF	užívat
jeho	jeho	k3xOp3gFnPc4	jeho
lásky	láska	k1gFnPc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
případ	případ	k1gInSc1	případ
přednesly	přednést	k5eAaPmAgInP	přednést
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
nakázal	nakázat	k5eAaPmAgMnS	nakázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Adonis	Adonis	k1gInSc1	Adonis
trávil	trávit	k5eAaImAgInS	trávit
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
z	z	k7c2	z
bohyň	bohyně	k1gFnPc2	bohyně
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zbylou	zbylý	k2eAgFnSc4d1	zbylá
třetinu	třetina	k1gFnSc4	třetina
měl	mít	k5eAaImAgMnS	mít
mladík	mladík	k1gMnSc1	mladík
pro	pro	k7c4	pro
odpočinek	odpočinek	k1gInSc4	odpočinek
od	od	k7c2	od
milování	milování	k1gNnSc2	milování
s	s	k7c7	s
náruživými	náruživý	k2eAgFnPc7d1	náruživá
milenkami	milenka	k1gFnPc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Afroditě	Afrodita	k1gFnSc3	Afrodita
podařilo	podařit	k5eAaPmAgNnS	podařit
Adonise	Adonise	k1gFnPc4	Adonise
zlomit	zlomit	k5eAaPmF	zlomit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zbylou	zbylý	k2eAgFnSc4d1	zbylá
třetinu	třetina	k1gFnSc4	třetina
věnoval	věnovat	k5eAaImAgMnS	věnovat
jen	jen	k9	jen
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Persefona	Persefona	k1gFnSc1	Persefona
o	o	k7c6	o
mladíkovi	mladík	k1gMnSc6	mladík
řekla	říct	k5eAaPmAgFnS	říct
Áreovi	Áres	k1gMnSc3	Áres
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
rozzuřen	rozzuřen	k2eAgMnSc1d1	rozzuřen
<g/>
,	,	kIx,	,
že	že	k8xS	že
Afrodita	Afrodita	k1gFnSc1	Afrodita
dává	dávat	k5eAaImIp3nS	dávat
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
přednost	přednost	k1gFnSc4	přednost
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Áres	Áres	k1gMnSc1	Áres
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
Adonise	Adonise	k1gFnSc2	Adonise
zabil	zabít	k5eAaPmAgInS	zabít
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
duši	duše	k1gFnSc4	duše
nechal	nechat	k5eAaPmAgMnS	nechat
uvrhnout	uvrhnout	k5eAaPmF	uvrhnout
do	do	k7c2	do
Tartaru	Tartar	k1gInSc2	Tartar
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
zamilované	zamilovaný	k2eAgFnPc1d1	zamilovaná
ženy	žena	k1gFnPc1	žena
si	se	k3xPyFc3	se
nedaly	dát	k5eNaPmAgFnP	dát
pokoj	pokoj	k1gInSc1	pokoj
a	a	k8xC	a
Afrodita	Afrodita	k1gFnSc1	Afrodita
na	na	k7c6	na
Diovi	Diův	k2eAgMnPc1d1	Diův
vymohla	vymoct	k5eAaPmAgFnS	vymoct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
Adonis	Adonis	k1gInSc1	Adonis
propast	propast	k1gFnSc4	propast
opouštět	opouštět	k5eAaImF	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc4d1	zimní
pochmurné	pochmurný	k2eAgInPc4d1	pochmurný
měsíce	měsíc	k1gInPc4	měsíc
měl	mít	k5eAaImAgInS	mít
pak	pak	k6eAd1	pak
trávit	trávit	k5eAaImF	trávit
s	s	k7c7	s
Persefonou	Persefona	k1gFnSc7	Persefona
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgInPc4d1	krásný
letní	letní	k2eAgInPc4d1	letní
dny	den	k1gInPc4	den
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
užívat	užívat	k5eAaImF	užívat
s	s	k7c7	s
Afroditou	Afrodita	k1gFnSc7	Afrodita
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc7	takový
mocí	moc	k1gFnSc7	moc
Afrodita	Afrodita	k1gFnSc1	Afrodita
vládla	vládnout	k5eAaImAgFnS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
svatbu	svatba	k1gFnSc4	svatba
mořské	mořský	k2eAgFnSc2d1	mořská
bohyně	bohyně	k1gFnSc2	bohyně
Thetis	Thetis	k1gFnSc2	Thetis
a	a	k8xC	a
krále	král	k1gMnSc2	král
Pélea	Péleum	k1gNnSc2	Péleum
byli	být	k5eAaImAgMnP	být
pozváni	pozván	k2eAgMnPc1d1	pozván
všichni	všechen	k3xTgMnPc1	všechen
bohové	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnPc1	bohyně
<g/>
,	,	kIx,	,
bůžci	bůžek	k1gMnPc1	bůžek
i	i	k8xC	i
polobozi	polobůh	k1gMnPc1	polobůh
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
bohyně	bohyně	k1gFnSc1	bohyně
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
nemilovaná	milovaný	k2eNgFnSc1d1	nemilovaná
bohyně	bohyně	k1gFnSc1	bohyně
sváru	svár	k1gInSc2	svár
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Hluboce	hluboko	k6eAd1	hluboko
dotčená	dotčený	k2eAgFnSc1d1	dotčená
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
veselku	veselka	k1gFnSc4	veselka
nebyla	být	k5eNaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
celému	celý	k2eAgInSc3d1	celý
božstvu	božstvo	k1gNnSc6	božstvo
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svatby	svatba	k1gFnSc2	svatba
hodila	hodit	k5eAaImAgFnS	hodit
před	před	k7c4	před
bohyni	bohyně	k1gFnSc4	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
Athénu	Athéna	k1gFnSc4	Athéna
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc4	bohyně
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
žárlivosti	žárlivost	k1gFnSc2	žárlivost
Héru	Héra	k1gFnSc4	Héra
a	a	k8xC	a
Afroditu	Afrodita	k1gFnSc4	Afrodita
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
jablko	jablko	k1gNnSc1	jablko
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Té	ten	k3xDgFnSc2	ten
nejkrásnější	krásný	k2eAgFnSc2d3	nejkrásnější
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
bohyň	bohyně	k1gFnPc2	bohyně
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
ihned	ihned	k6eAd1	ihned
začala	začít	k5eAaPmAgFnS	začít
dělat	dělat	k5eAaImF	dělat
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
věřila	věřit	k5eAaImAgFnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
nejkrásnější	krásný	k2eAgFnSc7d3	nejkrásnější
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hádka	hádka	k1gFnSc1	hádka
o	o	k7c4	o
jablko	jablko	k1gNnSc4	jablko
sváru	svár	k1gInSc2	svár
neustávala	ustávat	k5eNaImAgFnS	ustávat
<g/>
.	.	kIx.	.
</s>
<s>
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Athéna	Athéna	k1gFnSc1	Athéna
a	a	k8xC	a
Afrodita	Afrodita	k1gFnSc1	Afrodita
se	se	k3xPyFc4	se
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
shodnout	shodnout	k5eAaBmF	shodnout
a	a	k8xC	a
bezradní	bezradný	k2eAgMnPc1d1	bezradný
byli	být	k5eAaImAgMnP	být
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Hermés	Hermés	k1gInSc1	Hermés
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
krásu	krása	k1gFnSc4	krása
mohl	moct	k5eAaImAgInS	moct
posoudit	posoudit	k5eAaPmF	posoudit
jedině	jedině	k6eAd1	jedině
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
nespatřil	spatřit	k5eNaPmAgMnS	spatřit
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kde	kde	k6eAd1	kde
takového	takový	k3xDgMnSc4	takový
najít	najít	k5eAaPmF	najít
<g/>
?	?	kIx.	?
</s>
<s>
Hermés	Hermés	k6eAd1	Hermés
o	o	k7c6	o
jednom	jeden	k4xCgMnSc6	jeden
takovém	takový	k3xDgNnSc6	takový
věděl	vědět	k5eAaImAgMnS	vědět
–	–	k?	–
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
trójský	trójský	k2eAgMnSc1d1	trójský
princ	princ	k1gMnSc1	princ
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
s	s	k7c7	s
pastýři	pastýř	k1gMnPc7	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Hermés	Hermés	k6eAd1	Hermés
vzal	vzít	k5eAaPmAgMnS	vzít
jablko	jablko	k1gNnSc4	jablko
sváru	svár	k1gInSc2	svár
a	a	k8xC	a
odvedl	odvést	k5eAaPmAgMnS	odvést
trojici	trojice	k1gFnSc4	trojice
bohyň	bohyně	k1gFnPc2	bohyně
za	za	k7c7	za
Paridem	Paris	k1gMnSc7	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
ho	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
bohyň	bohyně	k1gFnPc2	bohyně
jablko	jablko	k1gNnSc4	jablko
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Paris	Paris	k1gMnSc1	Paris
byl	být	k5eAaImAgMnS	být
krásou	krása	k1gFnSc7	krása
doslova	doslova	k6eAd1	doslova
oslepen	oslepen	k2eAgMnSc1d1	oslepen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnPc1	bohyně
využily	využít	k5eAaPmAgFnP	využít
jeho	jeho	k3xOp3gFnPc1	jeho
váhavosti	váhavost	k1gFnPc1	váhavost
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
ho	on	k3xPp3gMnSc4	on
svádět	svádět	k5eAaImF	svádět
svými	svůj	k3xOyFgInPc7	svůj
sliby	slib	k1gInPc7	slib
<g/>
.	.	kIx.	.
</s>
<s>
Héra	Héra	k1gFnSc1	Héra
Paridovi	Paris	k1gMnSc3	Paris
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
Athéna	Athéna	k1gFnSc1	Athéna
princi	princa	k1gFnSc2	princa
slibovala	slibovat	k5eAaImAgFnS	slibovat
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
Afrodita	Afrodita	k1gFnSc1	Afrodita
mu	on	k3xPp3gMnSc3	on
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Lásku	láska	k1gFnSc4	láska
ženy	žena	k1gFnSc2	žena
tak	tak	k6eAd1	tak
krásné	krásný	k2eAgNnSc1d1	krásné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
samotné	samotný	k2eAgFnPc4d1	samotná
bohyně	bohyně	k1gFnPc4	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
přijal	přijmout	k5eAaPmAgMnS	přijmout
Afroditinu	Afroditin	k2eAgFnSc4d1	Afroditina
nabídku	nabídka	k1gFnSc4	nabídka
a	a	k8xC	a
jablko	jablko	k1gNnSc4	jablko
sváru	svár	k1gInSc2	svár
daroval	darovat	k5eAaPmAgMnS	darovat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
splnila	splnit	k5eAaPmAgFnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Zavedla	zavést	k5eAaPmAgFnS	zavést
Parise	Paris	k1gMnSc4	Paris
do	do	k7c2	do
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
unesl	unést	k5eAaPmAgMnS	unést
Helenu	Helena	k1gFnSc4	Helena
<g/>
,	,	kIx,	,
manželku	manželka	k1gFnSc4	manželka
krále	král	k1gMnSc2	král
Meneláa	Meneláus	k1gMnSc2	Meneláus
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Lédy	Léda	k1gFnSc2	Léda
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
do	do	k7c2	do
Tróji	Trója	k1gFnSc6	Trója
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
prince	princ	k1gMnSc2	princ
a	a	k8xC	a
Helenu	Helena	k1gFnSc4	Helena
hodlal	hodlat	k5eAaImAgMnS	hodlat
pojmout	pojmout	k5eAaPmF	pojmout
na	na	k7c4	na
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
zklamané	zklamaný	k2eAgFnPc1d1	zklamaná
bohyně	bohyně	k1gFnPc1	bohyně
Héra	Héra	k1gFnSc1	Héra
a	a	k8xC	a
Athéna	Athéna	k1gFnSc1	Athéna
nalezly	nalézt	k5eAaBmAgFnP	nalézt
společnou	společný	k2eAgFnSc4d1	společná
řeč	řeč	k1gFnSc4	řeč
v	v	k7c6	v
pomstě	pomsta	k1gFnSc6	pomsta
a	a	k8xC	a
chystaly	chystat	k5eAaImAgFnP	chystat
pro	pro	k7c4	pro
milence	milenec	k1gMnSc4	milenec
hrozivé	hrozivý	k2eAgNnSc1d1	hrozivé
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Meneláos	Meneláos	k1gInSc1	Meneláos
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
Hérou	Héra	k1gFnSc7	Héra
a	a	k8xC	a
Athénou	Athéna	k1gFnSc7	Athéna
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
mykénského	mykénský	k2eAgMnSc2d1	mykénský
krále	král	k1gMnSc2	král
Agamemnóna	Agamemnón	k1gMnSc2	Agamemnón
<g/>
.	.	kIx.	.
</s>
<s>
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
vycítil	vycítit	k5eAaPmAgMnS	vycítit
příležitost	příležitost	k1gFnSc4	příležitost
získat	získat	k5eAaPmF	získat
velké	velký	k2eAgNnSc4d1	velké
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Trója	Trója	k1gFnSc1	Trója
ukrývala	ukrývat	k5eAaImAgFnS	ukrývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ihned	ihned	k6eAd1	ihned
započal	započnout	k5eAaPmAgMnS	započnout
válečné	válečný	k2eAgFnSc2d1	válečná
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
ithacký	ithacký	k2eAgMnSc1d1	ithacký
král	král	k1gMnSc1	král
Odysseus	Odysseus	k1gMnSc1	Odysseus
<g/>
,	,	kIx,	,
argoský	argoský	k1gMnSc1	argoský
vládce	vládce	k1gMnSc1	vládce
Diomédes	Diomédes	k1gMnSc1	Diomédes
<g/>
,	,	kIx,	,
Aiás	Aiás	k1gInSc1	Aiás
ze	z	k7c2	z
Salamíny	Salamína	k1gFnSc2	Salamína
<g/>
,	,	kIx,	,
Patrokles	Patrokles	k1gMnSc1	Patrokles
<g/>
,	,	kIx,	,
Achilleus	Achilleus	k1gMnSc1	Achilleus
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
k	k	k7c3	k
Tróji	Trója	k1gFnSc3	Trója
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
ji	on	k3xPp3gFnSc4	on
dobývat	dobývat	k5eAaImF	dobývat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tvrdému	tvrdý	k2eAgInSc3d1	tvrdý
odporu	odpor	k1gInSc3	odpor
se	se	k3xPyFc4	se
ale	ale	k9	ale
obléhání	obléhání	k1gNnSc1	obléhání
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
jen	jen	k9	jen
díky	díky	k7c3	díky
obráncům	obránce	k1gMnPc3	obránce
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
díky	díky	k7c3	díky
rozepři	rozepře	k1gFnSc3	rozepře
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
vleklého	vleklý	k2eAgInSc2d1	vleklý
sporu	spor	k1gInSc2	spor
nadešel	nadejít	k5eAaPmAgInS	nadejít
až	až	k9	až
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Zeus	Zeus	k1gInSc1	Zeus
svolil	svolit	k5eAaPmAgInS	svolit
bohům	bůh	k1gMnPc3	bůh
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
bohové	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
Paridovu	Paridův	k2eAgFnSc4d1	Paridova
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
šípem	šíp	k1gInSc7	šíp
po	po	k7c6	po
Achilleovi	Achilleus	k1gMnSc6	Achilleus
a	a	k8xC	a
přivodil	přivodit	k5eAaPmAgMnS	přivodit
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
stáli	stát	k5eAaImAgMnP	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Trójanů	Trójan	k1gMnPc2	Trójan
<g/>
.	.	kIx.	.
</s>
<s>
Afrodita	Afrodita	k1gFnSc1	Afrodita
byla	být	k5eAaImAgFnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
vnuknutí	vnuknutí	k1gNnSc4	vnuknutí
Odyssea	Odyssea	k1gFnSc1	Odyssea
od	od	k7c2	od
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
poradila	poradit	k5eAaPmAgFnS	poradit
lest	lest	k1gFnSc4	lest
s	s	k7c7	s
trójským	trójský	k2eAgMnSc7d1	trójský
koněm	kůň	k1gMnSc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Poseidon	Poseidon	k1gMnSc1	Poseidon
připravil	připravit	k5eAaPmAgMnS	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
věštce	věštec	k1gMnSc2	věštec
Láokoóna	Láokoón	k1gMnSc2	Láokoón
<g/>
,	,	kIx,	,
nestála	stát	k5eNaImAgFnS	stát
už	už	k9	už
Řekům	Řek	k1gMnPc3	Řek
při	při	k7c6	při
dobytí	dobytí	k1gNnSc6	dobytí
města	město	k1gNnSc2	město
nic	nic	k6eAd1	nic
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Trója	Trója	k1gFnSc1	Trója
padla	padnout	k5eAaPmAgFnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
ji	on	k3xPp3gFnSc4	on
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
a	a	k8xC	a
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
hrstka	hrstka	k1gFnSc1	hrstka
přeživších	přeživší	k2eAgInPc2d1	přeživší
z	z	k7c2	z
Tróje	Trója	k1gFnSc2	Trója
unikla	uniknout	k5eAaPmAgFnS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
čela	čelo	k1gNnSc2	čelo
stál	stát	k5eAaImAgInS	stát
Aeneas	Aeneas	k1gMnSc1	Aeneas
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
nejmocnější	mocný	k2eAgMnSc1d3	nejmocnější
obránce	obránce	k1gMnSc1	obránce
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
