<s>
Afrodíté	Afrodíta	k1gMnPc1	Afrodíta
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Ἀ	Ἀ	k?	Ἀ
<g/>
,	,	kIx,	,
v	v	k7c6	v
čestině	čestina	k1gFnSc6	čestina
též	též	k9	též
Afrodita	Afrodita	k1gFnSc1	Afrodita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověká	starověký	k2eAgFnSc1d1	starověká
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
,	,	kIx,	,
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
;	;	kIx,	;
vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgNnSc2	tento
nejrozšířenějšího	rozšířený	k2eAgNnSc2d3	nejrozšířenější
pojetí	pojetí	k1gNnSc2	pojetí
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
i	i	k9	i
zcela	zcela	k6eAd1	zcela
odlišných	odlišný	k2eAgInPc2d1	odlišný
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k9	i
nevyzpytatelných	vyzpytatelný	k2eNgInPc2d1	nevyzpytatelný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
bohyní	bohyně	k1gFnPc2	bohyně
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
dvanáct	dvanáct	k4xCc4	dvanáct
Olympanů	Olympan	k1gMnPc2	Olympan
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
příběhů	příběh	k1gInPc2	příběh
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
řeckém	řecký	k2eAgInSc6d1	řecký
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
antickém	antický	k2eAgInSc6d1	antický
světě	svět	k1gInSc6	svět
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
pohanských	pohanský	k2eAgInPc2d1	pohanský
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
,	,	kIx,	,
v	v	k7c6	v
helénistickém	helénistický	k2eAgInSc6d1	helénistický
kontextu	kontext	k1gInSc6	kontext
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
Isidou	Isida	k1gFnSc7	Isida
<g/>
.	.	kIx.	.
</s>

