<s>
V	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
,	,	kIx,	,
v	v	k7c6	v
helénistickém	helénistický	k2eAgInSc6d1	helénistický
kontextu	kontext	k1gInSc6	kontext
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
Isidou	Isida	k1gFnSc7	Isida
<g/>
.	.	kIx.	.
</s>
