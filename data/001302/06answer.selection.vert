<s>
Bismut	bismut	k1gInSc1	bismut
(	(	kIx(	(
<g/>
vizmut	vizmut	k1gInSc1	vizmut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Bi	Bi	k1gFnSc1	Bi
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Bismuthum	Bismuthum	k1gInSc1	Bismuthum
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
těžké	těžký	k2eAgInPc4d1	těžký
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
