<s>
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
3	[number]	k4	3
jmenné	jmenný	k2eAgInPc1d1	jmenný
rody	rod	k1gInPc1	rod
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc1d1	mužský
(	(	kIx(	(
<g/>
maskulinum	maskulinum	k1gNnSc1	maskulinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
(	(	kIx(	(
<g/>
femininum	femininum	k1gNnSc1	femininum
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
(	(	kIx(	(
<g/>
neutrum	neutrum	k1gNnSc4	neutrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
