<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
neboli	neboli	k8xC	neboli
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
İ	İ	k?	İ
<g/>
;	;	kIx,	;
z	z	k7c2	z
řec.	řec.	k?	řec.
ε	ε	k?	ε
τ	τ	k?	τ
π	π	k?	π
eis	eis	k1gNnSc2	eis
tén	tén	k?	tén
polin	polina	k1gFnPc2	polina
(	(	kIx(	(
<g/>
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
is	is	k?	is
tin	tin	k?	tin
polin	polina	k1gFnPc2	polina
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
do	do	k7c2	do
Města	město	k1gNnSc2	město
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
turecké	turecký	k2eAgNnSc1d1	turecké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
důležitá	důležitý	k2eAgFnSc1d1	důležitá
obchodní	obchodní	k2eAgFnSc1d1	obchodní
křižovatka	křižovatka	k1gFnSc1	křižovatka
a	a	k8xC	a
brána	brána	k1gFnSc1	brána
Orientu	Orient	k1gInSc2	Orient
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
je	být	k5eAaImIp3nS	být
proslulý	proslulý	k2eAgInSc1d1	proslulý
především	především	k9	především
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
<g/>
:	:	kIx,	:
Byzantion	Byzantion	k1gNnSc1	Byzantion
<g/>
,	,	kIx,	,
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
nebo	nebo	k8xC	nebo
Cařihrad	Cařihrad	k1gInSc1	Cařihrad
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
kontinenty	kontinent	k1gInPc4	kontinent
spojuje	spojovat	k5eAaImIp3nS	spojovat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
úžina	úžina	k1gFnSc1	úžina
Bospor	Bospor	k1gInSc1	Bospor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
většina	většina	k1gFnSc1	většina
významných	významný	k2eAgFnPc2d1	významná
památek	památka	k1gFnPc2	památka
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
také	také	k9	také
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Bospor	Bospor	k1gInSc1	Bospor
je	být	k5eAaImIp3nS	být
strategická	strategický	k2eAgFnSc1d1	strategická
mořská	mořský	k2eAgFnSc1d1	mořská
úžina	úžina	k1gFnSc1	úžina
<g/>
,	,	kIx,	,
dopravně	dopravně	k6eAd1	dopravně
velmi	velmi	k6eAd1	velmi
využívaná	využívaný	k2eAgFnSc1d1	využívaná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
s	s	k7c7	s
úzkým	úzký	k2eAgNnSc7d1	úzké
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
tohoto	tento	k3xDgInSc2	tento
průlivu	průliv	k1gInSc2	průliv
činí	činit	k5eAaImIp3nS	činit
118	[number]	k4	118
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
je	být	k5eAaImIp3nS	být
31,5	[number]	k4	31,5
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mořská	mořský	k2eAgFnSc1d1	mořská
cesta	cesta	k1gFnSc1	cesta
spojuje	spojovat	k5eAaImIp3nS	spojovat
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
s	s	k7c7	s
Marmarským	Marmarský	k2eAgInSc7d1	Marmarský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
přepluli	přeplout	k5eAaPmAgMnP	přeplout
Bospor	Bospor	k1gInSc4	Bospor
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
Argonauti	argonaut	k1gMnPc1	argonaut
hledající	hledající	k2eAgFnSc2d1	hledající
zlaté	zlatá	k1gFnSc2	zlatá
rouno	rouno	k1gNnSc1	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Istanbulu	Istanbul	k1gInSc2	Istanbul
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Severoanatolský	Severoanatolský	k2eAgInSc1d1	Severoanatolský
zlom	zlom	k1gInSc1	zlom
<g/>
,	,	kIx,	,
litosférické	litosférický	k2eAgNnSc1d1	litosférický
rozhraní	rozhraní	k1gNnSc1	rozhraní
s	s	k7c7	s
častým	častý	k2eAgInSc7d1	častý
výskytem	výskyt	k1gInSc7	výskyt
různě	různě	k6eAd1	různě
silných	silný	k2eAgNnPc2d1	silné
a	a	k8xC	a
ničivých	ničivý	k2eAgNnPc2d1	ničivé
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
časů	čas	k1gInPc2	čas
antiky	antika	k1gFnSc2	antika
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xS	jako
malá	malý	k2eAgFnSc1d1	malá
osada	osada	k1gFnSc1	osada
Byzantion	Byzantion	k1gNnSc1	Byzantion
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Konstantinem	Konstantin	k1gMnSc7	Konstantin
I.	I.	kA	I.
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
přetvořil	přetvořit	k5eAaPmAgMnS	přetvořit
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
celé	celý	k2eAgFnSc2d1	celá
východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Nova	novum	k1gNnSc2	novum
Roma	Rom	k1gMnSc2	Rom
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
ale	ale	k9	ale
Konstantin	Konstantin	k1gMnSc1	Konstantin
brzy	brzy	k6eAd1	brzy
rozmyslel	rozmyslet	k5eAaPmAgMnS	rozmyslet
a	a	k8xC	a
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
ho	on	k3xPp3gNnSc4	on
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
neboli	neboli	k8xC	neboli
Konstantinopolis	Konstantinopolis	k1gInSc1	Konstantinopolis
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
centrem	centrum	k1gNnSc7	centrum
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spoustu	spoustu	k6eAd1	spoustu
významných	významný	k2eAgFnPc2d1	významná
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc1d3	veliký
stavba	stavba	k1gFnSc1	stavba
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
byla	být	k5eAaImAgFnS	být
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
vypleněna	vypleněn	k2eAgFnSc1d1	vypleněna
vojsky	vojsky	k6eAd1	vojsky
4	[number]	k4	4
<g/>
.	.	kIx.	.
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
časů	čas	k1gInPc2	čas
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
jako	jako	k8xS	jako
Konstantinopolis	Konstantinopolis	k1gFnSc4	Konstantinopolis
hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
všeho	všecek	k3xTgInSc2	všecek
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
i	i	k9	i
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Osmany	Osman	k1gMnPc4	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
sem	sem	k6eAd1	sem
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
velmi	velmi	k6eAd1	velmi
cizí	cizí	k2eAgInSc1d1	cizí
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obyčejnými	obyčejný	k2eAgMnPc7d1	obyčejný
Turky	Turek	k1gMnPc7	Turek
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
-	-	kIx~	-
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
však	však	k9	však
město	město	k1gNnSc1	město
oficiálně	oficiálně	k6eAd1	oficiálně
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Kostantiniyye	Kostantiniyye	k1gNnSc1	Kostantiniyye
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
státy	stát	k1gInPc1	stát
stále	stále	k6eAd1	stále
město	město	k1gNnSc4	město
nazývali	nazývat	k5eAaImAgMnP	nazývat
jako	jako	k8xS	jako
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
oficiálně	oficiálně	k6eAd1	oficiálně
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Istanbul	Istanbul	k1gInSc4	Istanbul
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopisy	dopis	k1gInPc1	dopis
adresované	adresovaný	k2eAgInPc1d1	adresovaný
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
budou	být	k5eAaImBp3nP	být
skartovány	skartovat	k5eAaBmNgFnP	skartovat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
mešity	mešita	k1gFnPc1	mešita
(	(	kIx(	(
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
Tulipánová	tulipánový	k2eAgFnSc1d1	Tulipánová
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
Sulejmanova	Sulejmanův	k2eAgFnSc1d1	Sulejmanova
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
na	na	k7c4	na
mešitu	mešita	k1gFnSc4	mešita
byla	být	k5eAaImAgFnS	být
přebudována	přebudován	k2eAgFnSc1d1	přebudována
i	i	k8xC	i
katedrála	katedrála	k1gFnSc1	katedrála
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
velmi	velmi	k6eAd1	velmi
bohatě	bohatě	k6eAd1	bohatě
zdobených	zdobený	k2eAgInPc2d1	zdobený
chrámů	chrám	k1gInPc2	chrám
z	z	k7c2	z
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
každý	každý	k3xTgInSc4	každý
panovník	panovník	k1gMnSc1	panovník
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c6	na
svojí	svůj	k3xOyFgFnSc6	svůj
počest	počest	k1gFnSc6	počest
vlastní	vlastní	k2eAgFnSc4d1	vlastní
mešitu	mešita	k1gFnSc4	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Usazovali	usazovat	k5eAaImAgMnP	usazovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
porobených	porobený	k2eAgFnPc2d1	porobená
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
či	či	k8xC	či
Arménie	Arménie	k1gFnSc2	Arménie
<g/>
;	;	kIx,	;
Istanbul	Istanbul	k1gInSc1	Istanbul
tak	tak	k6eAd1	tak
získával	získávat	k5eAaImAgInS	získávat
kosmopolitní	kosmopolitní	k2eAgInSc4d1	kosmopolitní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
moderním	moderní	k2eAgNnSc7d1	moderní
milionovým	milionový	k2eAgNnSc7d1	milionové
městem	město	k1gNnSc7	město
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
i	i	k8xC	i
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
bylo	být	k5eAaImAgNnS	být
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
postupně	postupně	k6eAd1	postupně
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
slábla	slábnout	k5eAaImAgFnS	slábnout
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
Istanbul	Istanbul	k1gInSc1	Istanbul
obsazen	obsadit	k5eAaPmNgInS	obsadit
cizími	cizí	k2eAgNnPc7d1	cizí
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Výslovně	výslovně	k6eAd1	výslovně
mělo	mít	k5eAaImAgNnS	mít
o	o	k7c4	o
získání	získání	k1gNnPc4	získání
metropole	metropol	k1gFnSc2	metropol
někdejší	někdejší	k2eAgFnPc4d1	někdejší
Byzance	Byzanc	k1gFnPc4	Byzanc
zájem	zájem	k1gInSc4	zájem
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
to	ten	k3xDgNnSc1	ten
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
ideologický	ideologický	k2eAgInSc4d1	ideologický
i	i	k8xC	i
mocenský	mocenský	k2eAgInSc4d1	mocenský
cíl	cíl	k1gInSc4	cíl
<g/>
;	;	kIx,	;
ziskem	zisk	k1gInSc7	zisk
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
naplnily	naplnit	k5eAaPmAgInP	naplnit
cíle	cíl	k1gInPc4	cíl
řady	řada	k1gFnSc2	řada
panslavistů	panslavista	k1gMnPc2	panslavista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
strategického	strategický	k2eAgNnSc2d1	strategické
hlediska	hledisko	k1gNnSc2	hledisko
by	by	kYmCp3nS	by
Rusko	Rusko	k1gNnSc1	Rusko
mohlo	moct	k5eAaImAgNnS	moct
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
jedinou	jediný	k2eAgFnSc4d1	jediná
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
Černým	černý	k2eAgMnSc7d1	černý
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
mocnosti	mocnost	k1gFnPc1	mocnost
během	během	k7c2	během
dělení	dělení	k1gNnSc2	dělení
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
Istanbul	Istanbul	k1gInSc1	Istanbul
Rusku	Ruska	k1gFnSc4	Ruska
sice	sice	k8xC	sice
několikrát	několikrát	k6eAd1	několikrát
přislíbily	přislíbit	k5eAaPmAgFnP	přislíbit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
zábor	zábor	k1gInSc1	zábor
nerealizoval	realizovat	k5eNaBmAgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíž	blízce	k6eAd3	blízce
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ale	ale	k9	ale
mělo	mít	k5eAaImAgNnS	mít
ruské	ruský	k2eAgNnSc1d1	ruské
imperiální	imperiální	k2eAgNnSc1d1	imperiální
vojsko	vojsko	k1gNnSc1	vojsko
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
slabá	slabý	k2eAgNnPc1d1	slabé
<g/>
,	,	kIx,	,
přidala	přidat	k5eAaPmAgNnP	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
však	však	k9	však
země	země	k1gFnSc1	země
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
vítěznými	vítězný	k2eAgFnPc7d1	vítězná
mocnostmi	mocnost	k1gFnPc7	mocnost
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
rozvrátila	rozvrátit	k5eAaPmAgFnS	rozvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kemala	Kemal	k1gMnSc2	Kemal
Atatürka	Atatürek	k1gMnSc2	Atatürek
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
z	z	k7c2	z
taktických	taktický	k2eAgInPc2d1	taktický
důvodů	důvod	k1gInPc2	důvod
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přesunutí	přesunutí	k1gNnSc3	přesunutí
sídla	sídlo	k1gNnSc2	sídlo
státu	stát	k1gInSc2	stát
do	do	k7c2	do
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
další	další	k2eAgFnSc2d1	další
války	válka	k1gFnSc2	válka
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
napadnutelná	napadnutelný	k2eAgFnSc1d1	napadnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pouhým	pouhý	k2eAgNnSc7d1	pouhé
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc3	množství
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tu	tu	k6eAd1	tu
tvořili	tvořit	k5eAaImAgMnP	tvořit
komunitu	komunita	k1gFnSc4	komunita
žijící	žijící	k2eAgFnSc4d1	žijící
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
pádu	pád	k1gInSc2	pád
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
však	však	k9	však
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
odejít	odejít	k5eAaPmF	odejít
<g/>
;	;	kIx,	;
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
terčem	terč	k1gInSc7	terč
násilných	násilný	k2eAgInPc2d1	násilný
a	a	k8xC	a
nenávistných	návistný	k2eNgInPc2d1	nenávistný
útoků	útok	k1gInPc2	útok
tureckých	turecký	k2eAgMnPc2d1	turecký
ultranacionalistů	ultranacionalista	k1gMnPc2	ultranacionalista
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
Adnan	Adnan	k1gMnSc1	Adnan
Menderes	Menderes	k1gMnSc1	Menderes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
útoky	útok	k1gInPc7	útok
stál	stát	k5eAaImAgInS	stát
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zahájil	zahájit	k5eAaPmAgMnS	zahájit
velkou	velký	k2eAgFnSc4d1	velká
modernizaci	modernizace	k1gFnSc4	modernizace
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
dotkla	dotknout	k5eAaPmAgFnS	dotknout
i	i	k8xC	i
jejího	její	k3xOp3gNnSc2	její
největšího	veliký	k2eAgNnSc2d3	veliký
města	město	k1gNnSc2	město
a	a	k8xC	a
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
také	také	k9	také
odvést	odvést	k5eAaPmF	odvést
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
zvěrstev	zvěrstvo	k1gNnPc2	zvěrstvo
běsnící	běsnící	k2eAgFnSc2d1	běsnící
turecké	turecký	k2eAgFnSc2d1	turecká
lůzy	lůza	k1gFnSc2	lůza
<g/>
.	.	kIx.	.
</s>
<s>
Postavily	postavit	k5eAaPmAgFnP	postavit
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
silnice	silnice	k1gFnPc1	silnice
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
také	také	k6eAd1	také
rostla	růst	k5eAaImAgFnS	růst
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přimělo	přimět	k5eAaPmAgNnS	přimět
i	i	k9	i
další	další	k2eAgMnPc4d1	další
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
milionového	milionový	k2eAgNnSc2d1	milionové
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
během	během	k7c2	během
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
pětimilionové	pětimilionový	k2eAgInPc1d1	pětimilionový
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
již	již	k6eAd1	již
předstihlo	předstihnout	k5eAaPmAgNnS	předstihnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
velikosti	velikost	k1gFnSc6	velikost
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
malých	malý	k2eAgFnPc2d1	malá
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
čtvrtě	čtvrt	k1gFnPc4	čtvrt
s	s	k7c7	s
městskými	městský	k2eAgInPc7d1	městský
domy	dům	k1gInPc7	dům
a	a	k8xC	a
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
však	však	k9	však
byla	být	k5eAaImAgFnS	být
úroveň	úroveň	k1gFnSc1	úroveň
zástavby	zástavba	k1gFnSc2	zástavba
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Bospor	Bospor	k1gInSc1	Bospor
byl	být	k5eAaImAgInS	být
přemostěn	přemostit	k5eAaPmNgInS	přemostit
dvěma	dva	k4xCgInPc7	dva
zavěšenými	zavěšený	k2eAgInPc7d1	zavěšený
mosty	most	k1gInPc7	most
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dálnice	dálnice	k1gFnSc1	dálnice
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
,	,	kIx,	,
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velice	velice	k6eAd1	velice
významná	významný	k2eAgFnSc1d1	významná
kulturní	kulturní	k2eAgFnSc1d1	kulturní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
křižovatka	křižovatka	k1gFnSc1	křižovatka
vždy	vždy	k6eAd1	vždy
přitahoval	přitahovat	k5eAaImAgMnS	přitahovat
lidi	člověk	k1gMnPc4	člověk
všech	všecek	k3xTgFnPc2	všecek
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
náboženských	náboženský	k2eAgNnPc2d1	náboženské
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Istanbul	Istanbul	k1gInSc1	Istanbul
kosmopolitní	kosmopolitní	k2eAgInSc1d1	kosmopolitní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
v	v	k7c6	v
přátelství	přátelství	k1gNnSc6	přátelství
a	a	k8xC	a
míru	míra	k1gFnSc4	míra
žijí	žít	k5eAaImIp3nP	žít
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
a	a	k8xC	a
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Istanbul	Istanbul	k1gInSc1	Istanbul
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
Turecka	Turecko	k1gNnSc2	Turecko
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgMnSc7	druhý
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Izmit	Izmita	k1gFnPc2	Izmita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hranice	hranice	k1gFnPc4	hranice
shodné	shodný	k2eAgFnPc4d1	shodná
s	s	k7c7	s
hranicemi	hranice	k1gFnPc7	hranice
své	svůj	k3xOyFgFnSc2	svůj
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
39	[number]	k4	39
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
a	a	k8xC	a
asijské	asijský	k2eAgFnSc3d1	asijská
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
epocha	epocha	k1gFnSc1	epocha
zanechala	zanechat	k5eAaPmAgFnS	zanechat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nesmazatelné	smazatelný	k2eNgFnSc2d1	nesmazatelná
památky	památka	k1gFnSc2	památka
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čemberlitaš	Čemberlitaš	k1gMnSc1	Čemberlitaš
-	-	kIx~	-
nejstarší	starý	k2eAgNnSc4d3	nejstarší
stavební	stavební	k2eAgNnSc4d1	stavební
dílo	dílo	k1gNnSc4	dílo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Postaven	postaven	k2eAgInSc1d1	postaven
roku	rok	k1gInSc2	rok
330	[number]	k4	330
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
povýšení	povýšení	k1gNnSc1	povýšení
Byzantionu	Byzantion	k1gInSc2	Byzantion
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
císařem	císař	k1gMnSc7	císař
Konstantinem	Konstantin	k1gMnSc7	Konstantin
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Sulejmanova	Sulejmanův	k2eAgFnSc1d1	Sulejmanova
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
istanbulských	istanbulský	k2eAgInPc2d1	istanbulský
kopců	kopec	k1gInPc2	kopec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Sülejmana	Sülejman	k1gMnSc2	Sülejman
Nádherného	nádherný	k2eAgMnSc2d1	nádherný
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říš	k1gFnSc2	říš
světovou	světový	k2eAgFnSc7d1	světová
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
minarety	minaret	k1gInPc1	minaret
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
10	[number]	k4	10
balkóny	balkón	k1gInPc7	balkón
mají	mít	k5eAaImIp3nP	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sulejman	Sulejman	k1gMnSc1	Sulejman
byl	být	k5eAaImAgMnS	být
desátým	desátý	k4xOgMnSc7	desátý
osmanským	osmanský	k2eAgMnSc7d1	osmanský
sultánem	sultán	k1gMnSc7	sultán
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
padišáhem	padišáh	k1gInSc7	padišáh
od	od	k7c2	od
dobytí	dobytí	k1gNnSc2	dobytí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mešita	mešita	k1gFnSc1	mešita
je	být	k5eAaImIp3nS	být
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dilo	dilo	k1gNnSc4	dilo
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
klasiky	klasika	k1gFnSc2	klasika
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
interiér	interiér	k1gInSc1	interiér
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
celkem	celkem	k6eAd1	celkem
138	[number]	k4	138
zdobených	zdobený	k2eAgNnPc2d1	zdobené
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Mešita	mešita	k1gFnSc1	mešita
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
za	za	k7c4	za
pouhých	pouhý	k2eAgNnPc2d1	pouhé
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1557	[number]	k4	1557
shlíží	shlížet	k5eAaImIp3nS	shlížet
její	její	k3xOp3gFnSc1	její
silueta	silueta	k1gFnSc1	silueta
na	na	k7c4	na
město	město	k1gNnSc4	město
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
koberců	koberec	k1gInPc2	koberec
<g/>
,	,	kIx,	,
arkád	arkáda	k1gFnPc2	arkáda
z	z	k7c2	z
lomených	lomený	k2eAgInPc2d1	lomený
oblouků	oblouk	k1gInPc2	oblouk
a	a	k8xC	a
žulových	žulový	k2eAgInPc2d1	žulový
sloupů	sloup	k1gInPc2	sloup
zaujmou	zaujmout	k5eAaPmIp3nP	zaujmout
i	i	k9	i
lustry	lustr	k1gInPc1	lustr
s	s	k7c7	s
bronzovými	bronzový	k2eAgInPc7d1	bronzový
svícny	svícen	k1gInPc7	svícen
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
mešita	mešita	k1gFnSc1	mešita
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
komplexu	komplex	k1gInSc2	komplex
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
medresa	medresa	k1gFnSc1	medresa
<g/>
,	,	kIx,	,
karavanseraj	karavanseraj	k1gFnSc1	karavanseraj
<g/>
,	,	kIx,	,
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
imaret	imaret	k1gInSc1	imaret
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
paláce	palác	k1gInPc1	palác
však	však	k9	však
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
sultána	sultán	k1gMnSc4	sultán
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
palác	palác	k1gInSc4	palác
Topkapi	Topkap	k1gFnSc2	Topkap
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
Topkapi	Topkap	k1gFnSc2	Topkap
byl	být	k5eAaImAgInS	být
sultánovou	sultánův	k2eAgFnSc7d1	sultánova
rezidencí	rezidence	k1gFnSc7	rezidence
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
centrem	centr	k1gMnSc7	centr
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
dobách	doba	k1gFnPc6	doba
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
až	až	k9	až
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
serail	serail	k1gInSc1	serail
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
městem	město	k1gNnSc7	město
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Klenotnice	klenotnice	k1gFnSc2	klenotnice
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Topkapi	Topkap	k1gFnSc2	Topkap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
oddělen	oddělit	k5eAaPmNgInS	oddělit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
střežících	střežící	k2eAgInPc2d1	střežící
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
Dolmabahce	Dolmabahec	k1gInSc2	Dolmabahec
je	být	k5eAaImIp3nS	být
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
empírový	empírový	k2eAgInSc1d1	empírový
palác	palác	k1gInSc1	palác
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Bosporu	Bospor	k1gInSc2	Bospor
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
sultán	sultán	k1gMnSc1	sultán
Abdulmecid	Abdulmecida	k1gFnPc2	Abdulmecida
I.	I.	kA	I.
i	i	k8xC	i
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
se	se	k3xPyFc4	se
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
až	až	k9	až
přezdobená	přezdobený	k2eAgFnSc1d1	přezdobená
stavba	stavba	k1gFnSc1	stavba
osmanského	osmanský	k2eAgInSc2d1	osmanský
slohu	sloh	k1gInSc2	sloh
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
prý	prý	k9	prý
tak	tak	k6eAd1	tak
nákladná	nákladný	k2eAgNnPc1d1	nákladné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
velké	velký	k2eAgFnSc3d1	velká
míře	míra	k1gFnSc3	míra
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
státnímu	státní	k2eAgInSc3d1	státní
bankrotu	bankrot	k1gInSc3	bankrot
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Přepychové	přepychový	k2eAgNnSc1d1	přepychové
řešení	řešení	k1gNnSc1	řešení
interiéru	interiér	k1gInSc2	interiér
věrně	věrně	k6eAd1	věrně
ctí	ctít	k5eAaImIp3nS	ctít
tradice	tradice	k1gFnSc1	tradice
pohádkových	pohádkový	k2eAgInPc2d1	pohádkový
sultánských	sultánský	k2eAgInPc2d1	sultánský
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc4	jeho
styl	styl	k1gInSc4	styl
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
orientální	orientální	k2eAgNnSc1d1	orientální
<g/>
.	.	kIx.	.
</s>
<s>
Obdivuhodné	obdivuhodný	k2eAgNnSc1d1	obdivuhodné
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
těžkých	těžký	k2eAgInPc2d1	těžký
brokátů	brokát	k1gInPc2	brokát
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
prostorami	prostora	k1gFnPc7	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
sídlo	sídlo	k1gNnSc1	sídlo
tureckých	turecký	k2eAgMnPc2d1	turecký
sultánů	sultán	k1gMnPc2	sultán
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
asi	asi	k9	asi
700	[number]	k4	700
tisíc	tisíc	k4xCgInSc4	tisíc
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
komplex	komplex	k1gInSc4	komplex
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
pavilonů	pavilon	k1gInPc2	pavilon
<g/>
,	,	kIx,	,
mešit	mešita	k1gFnPc2	mešita
<g/>
,	,	kIx,	,
fontán	fontána	k1gFnPc2	fontána
a	a	k8xC	a
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
nádvoří	nádvoří	k1gNnPc2	nádvoří
a	a	k8xC	a
komplexu	komplex	k1gInSc2	komplex
harému	harém	k1gInSc2	harém
<g/>
.	.	kIx.	.
</s>
<s>
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
neboli	neboli	k8xC	neboli
Chrám	chrám	k1gInSc1	chrám
Svaté	svatá	k1gFnSc2	svatá
(	(	kIx(	(
<g/>
Boží	boží	k2eAgFnSc2d1	boží
<g/>
)	)	kIx)	)
Moudrosti	moudrost	k1gFnSc2	moudrost
je	být	k5eAaImIp3nS	být
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Zlatý	zlatý	k2eAgInSc4d1	zlatý
roh	roh	k1gInSc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
letech	léto	k1gNnPc6	léto
532	[number]	k4	532
<g/>
-	-	kIx~	-
<g/>
537	[number]	k4	537
císař	císař	k1gMnSc1	císař
Justinián	Justinián	k1gMnSc1	Justinián
I.	I.	kA	I.
<g/>
.	.	kIx.	.
27.12	[number]	k4	27.12
<g/>
.537	.537	k4	.537
císař	císař	k1gMnSc1	císař
slavnostně	slavnostně	k6eAd1	slavnostně
otevřel	otevřít	k5eAaPmAgMnS	otevřít
chrám	chrám	k1gInSc4	chrám
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
zvolal	zvolat	k5eAaPmAgMnS	zvolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
jsem	být	k5eAaImIp1nS	být
i	i	k9	i
nad	nad	k7c7	nad
tebou	ty	k3xPp2nSc7	ty
<g/>
,	,	kIx,	,
Šalamoune	Šalamoun	k1gMnSc5	Šalamoun
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Katedrála	katedrála	k1gFnSc1	katedrála
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejhonosnější	honosný	k2eAgFnSc4d3	nejhonosnější
stavbu	stavba	k1gFnSc4	stavba
byzantské	byzantský	k2eAgFnSc2d1	byzantská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Stavělo	stavět	k5eAaImAgNnS	stavět
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
náboženských	náboženský	k2eAgFnPc2d1	náboženská
budov	budova	k1gFnPc2	budova
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
patriarchální	patriarchální	k2eAgFnSc7d1	patriarchální
bazilikou	bazilika	k1gFnSc7	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
Osmany	Osman	k1gMnPc7	Osman
(	(	kIx(	(
<g/>
1453	[number]	k4	1453
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
sultánem	sultán	k1gMnSc7	sultán
Mehmedem	Mehmed	k1gMnSc7	Mehmed
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dal	dát	k5eAaPmAgInS	dát
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
interiéru	interiér	k1gInSc2	interiér
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
mešitu	mešita	k1gFnSc4	mešita
a	a	k8xC	a
přistavěl	přistavět	k5eAaPmAgMnS	přistavět
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
čtyři	čtyři	k4xCgInPc4	čtyři
minarety	minaret	k1gInPc1	minaret
<g/>
,	,	kIx,	,
opěrné	opěrný	k2eAgFnPc1d1	opěrná
zdi	zeď	k1gFnPc1	zeď
<g/>
,	,	kIx,	,
studny	studna	k1gFnPc1	studna
<g/>
,	,	kIx,	,
kuchyni	kuchyně	k1gFnSc4	kuchyně
a	a	k8xC	a
náhrobní	náhrobní	k2eAgFnPc4d1	náhrobní
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
pět	pět	k4xCc1	pět
tureckých	turecký	k2eAgMnPc2d1	turecký
sultánů	sultán	k1gMnPc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Mustafa	Mustaf	k1gMnSc2	Mustaf
Kemal	Kemal	k1gMnSc1	Kemal
Atatürk	Atatürk	k1gInSc1	Atatürk
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
bude	být	k5eAaImBp3nS	být
sekularizována	sekularizovat	k5eAaBmNgFnS	sekularizovat
a	a	k8xC	a
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obložení	obložení	k1gNnSc3	obložení
stěn	stěna	k1gFnPc2	stěna
modrými	modrý	k2eAgFnPc7d1	modrá
dlaždicemi	dlaždice	k1gFnPc7	dlaždice
je	být	k5eAaImIp3nS	být
Mešita	mešita	k1gFnSc1	mešita
Sultan	Sultan	k1gInSc4	Sultan
Ahmed	Ahmed	k1gInSc1	Ahmed
též	též	k9	též
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
Modrá	modrý	k2eAgFnSc1d1	modrá
mešita	mešita	k1gFnSc1	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejznámějším	známý	k2eAgInSc7d3	nejznámější
symbolem	symbol	k1gInSc7	symbol
Istanbulu	Istanbul	k1gInSc2	Istanbul
hned	hned	k6eAd1	hned
po	po	k7c6	po
Hagii	Hagie	k1gFnSc6	Hagie
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
mešitu	mešita	k1gFnSc4	mešita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
minaretů	minaret	k1gInPc2	minaret
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mešit	mešita	k1gFnPc2	mešita
platí	platit	k5eAaImIp3nS	platit
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
:	:	kIx,	:
běžná	běžný	k2eAgFnSc1d1	běžná
mešita	mešita	k1gFnSc1	mešita
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
minaret	minaret	k1gInSc4	minaret
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgMnSc1d2	lepší
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dva	dva	k4xCgInPc4	dva
minarety	minaret	k1gInPc4	minaret
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šest	šest	k4xCc1	šest
minaretů	minaret	k1gInPc2	minaret
smí	smět	k5eAaImIp3nS	smět
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
mešita	mešita	k1gFnSc1	mešita
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
proto	proto	k8xC	proto
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sultán	sultán	k1gMnSc1	sultán
musel	muset	k5eAaImAgMnS	muset
nakonec	nakonec	k6eAd1	nakonec
nechat	nechat	k5eAaPmF	nechat
postavit	postavit	k5eAaPmF	postavit
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
sedmý	sedmý	k4xOgInSc4	sedmý
minaret	minaret	k1gInSc4	minaret
<g/>
.	.	kIx.	.
</s>
<s>
Tulipánová	tulipánový	k2eAgFnSc1d1	Tulipánová
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
laleli	lalet	k5eAaBmAgMnP	lalet
Džamii	Džamie	k1gFnSc4	Džamie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mešita	mešita	k1gFnSc1	mešita
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
za	za	k7c7	za
Mustafy	Mustaf	k1gMnPc7	Mustaf
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Mešita	mešita	k1gFnSc1	mešita
je	být	k5eAaImIp3nS	být
nejhezčím	hezký	k2eAgInSc7d3	nejhezčí
příkladem	příklad	k1gInSc7	příklad
osmanského	osmanský	k2eAgNnSc2d1	osmanské
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Beyazitova	Beyazitův	k2eAgFnSc1d1	Beyazitův
věž	věž	k1gFnSc1	věž
-	-	kIx~	-
dobře	dobře	k6eAd1	dobře
viditelná	viditelný	k2eAgFnSc1d1	viditelná
dominanta	dominanta	k1gFnSc1	dominanta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgNnSc1d1	Archeologické
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
Arkeoloji	Arkeoloj	k1gInSc6	Arkeoloj
Müzesi	Müzese	k1gFnSc3	Müzese
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
sbírkou	sbírka	k1gFnSc7	sbírka
antického	antický	k2eAgNnSc2d1	antické
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
exponát	exponát	k1gInSc1	exponát
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
a	a	k8xC	a
sarkofág	sarkofág	k1gInSc1	sarkofág
z	z	k7c2	z
roku	rok	k1gInSc2	rok
310	[number]	k4	310
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
bazar	bazar	k1gInSc4	bazar
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
Kapali	kapat	k5eAaImAgMnP	kapat
Carşi	Carş	k1gMnPc1	Carş
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc4d1	obrovský
trh	trh	k1gInSc4	trh
pod	pod	k7c7	pod
jednou	jeden	k4xCgFnSc7	jeden
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgInSc4d1	klasický
osmanský	osmanský	k2eAgInSc4d1	osmanský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
4000	[number]	k4	4000
malých	malý	k2eAgInPc2d1	malý
krámků	krámek	k1gInPc2	krámek
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
tísní	tísnit	k5eAaImIp3nS	tísnit
jeden	jeden	k4xCgMnSc1	jeden
vedle	vedle	k7c2	vedle
druhého	druhý	k4xOgMnSc2	druhý
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
polovina	polovina	k1gFnSc1	polovina
patří	patřit	k5eAaImIp3nS	patřit
zlatníkům	zlatník	k1gMnPc3	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
procházejí	procházet	k5eAaImIp3nP	procházet
nekonečnými	konečný	k2eNgFnPc7d1	nekonečná
chodbami	chodba	k1gFnPc7	chodba
a	a	k8xC	a
míjí	míjet	k5eAaImIp3nS	míjet
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
známé	známý	k2eAgInPc4d1	známý
bazary	bazar	k1gInPc4	bazar
patří	patřit	k5eAaImIp3nS	patřit
Egyptský	egyptský	k2eAgInSc1d1	egyptský
bazar	bazar	k1gInSc1	bazar
a	a	k8xC	a
Knižní	knižní	k2eAgInSc4d1	knižní
bazar	bazar	k1gInSc4	bazar
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1	egyptský
bazar	bazar	k1gInSc1	bazar
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Bosporu	Bospor	k1gInSc2	Bospor
u	u	k7c2	u
Galatského	Galatský	k2eAgInSc2d1	Galatský
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
Velký	velký	k2eAgInSc4d1	velký
bazar	bazar	k1gInSc4	bazar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
většinou	většina	k1gFnSc7	většina
koření	koření	k1gNnSc2	koření
a	a	k8xC	a
sušené	sušený	k2eAgNnSc4d1	sušené
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Prodejci	prodejce	k1gMnPc1	prodejce
mají	mít	k5eAaImIp3nP	mít
zboží	zboží	k1gNnSc4	zboží
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
pytlích	pytel	k1gInPc6	pytel
a	a	k8xC	a
odvažují	odvažovat	k5eAaImIp3nP	odvažovat
požadované	požadovaný	k2eAgNnSc4d1	požadované
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
Jerebatanské	Jerebatanský	k2eAgFnPc4d1	Jerebatanský
cisterny	cisterna	k1gFnPc4	cisterna
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc4d1	postavená
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Dokážou	dokázat	k5eAaPmIp3nP	dokázat
pojmout	pojmout	k5eAaPmF	pojmout
80	[number]	k4	80
tisíc	tisíc	k4xCgInPc2	tisíc
metrů	metr	k1gInPc2	metr
krychlových	krychlový	k2eAgFnPc2d1	krychlová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Calouste	Caloousit	k5eAaPmRp2nP	Caloousit
Gulbenkian	Gulbenkian	k1gInSc4	Gulbenkian
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
-	-	kIx~	-
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
umění	umění	k1gNnSc2	umění
Istanbul	Istanbul	k1gInSc1	Istanbul
jako	jako	k8xC	jako
čtrnáctimilionové	čtrnáctimilionový	k2eAgNnSc1d1	čtrnáctimilionový
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
systém	systém	k1gInSc4	systém
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
linka	linka	k1gFnSc1	linka
podzemního	podzemní	k2eAgNnSc2d1	podzemní
i	i	k8xC	i
pozemního	pozemní	k2eAgNnSc2d1	pozemní
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Istanbul	Istanbul	k1gInSc1	Istanbul
je	on	k3xPp3gMnPc4	on
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgInSc7	svůj
velmi	velmi	k6eAd1	velmi
krátkým	krátký	k2eAgInSc7d1	krátký
metrem	metr	k1gInSc7	metr
Tünel	Tünela	k1gFnPc2	Tünela
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
jako	jako	k8xC	jako
druhé	druhý	k4xOgNnSc4	druhý
po	po	k7c6	po
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lukavec	Lukavec	k1gMnSc1	Lukavec
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zde	zde	k6eAd1	zde
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
zvuky	zvuk	k1gInPc1	zvuk
i	i	k8xC	i
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
sloučily	sloučit	k5eAaPmAgFnP	sloučit
v	v	k7c4	v
báseň	báseň	k1gFnSc4	báseň
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Istanbul	Istanbul	k1gInSc1	Istanbul
očima	oko	k1gNnPc7	oko
evropských	evropský	k2eAgMnPc2d1	evropský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
,	,	kIx,	,
s.	s.	k?	s.
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
<s>
Orhan	Orhan	k1gMnSc1	Orhan
Pamuk	Pamuk	k1gMnSc1	Pamuk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
:	:	kIx,	:
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
Litesi	Litese	k1gFnSc4	Litese
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
univerzita	univerzita	k1gFnSc1	univerzita
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Istanbul	Istanbul	k1gInSc4	Istanbul
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Istanbul	Istanbul	k1gInSc4	Istanbul
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Istanbul	Istanbul	k1gInSc1	Istanbul
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.istanbul.com	www.istanbul.com	k1gInSc4	www.istanbul.com
Mapa	mapa	k1gFnSc1	mapa
Istanbul	Istanbul	k1gInSc1	Istanbul
J.	J.	kA	J.
Lukavec	Lukavec	k1gInSc1	Lukavec
<g/>
:	:	kIx,	:
Istanbul	Istanbul	k1gInSc1	Istanbul
mezi	mezi	k7c7	mezi
Pierrem	Pierr	k1gMnSc7	Pierr
Lotim	Loti	k1gNnSc7	Loti
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Nerudou	Neruda	k1gMnSc7	Neruda
</s>
