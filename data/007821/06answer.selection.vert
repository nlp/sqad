<s>
Centrum	centrum	k1gNnSc1	centrum
vnímaní	vnímaný	k2eAgMnPc1d1	vnímaný
chuti	chuť	k1gFnSc2	chuť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
temenním	temenní	k2eAgInSc6d1	temenní
laloku	lalok	k1gInSc6	lalok
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kombinací	kombinace	k1gFnSc7	kombinace
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
tvoří	tvořit	k5eAaImIp3nS	tvořit
výsledná	výsledný	k2eAgFnSc1d1	výsledná
chuť	chuť	k1gFnSc1	chuť
<g/>
.	.	kIx.	.
</s>
