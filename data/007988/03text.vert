<s>
Mallorca	Mallorca	k1gFnSc1	Mallorca
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
Mallorca	Mallorc	k2eAgFnSc1d1	Mallorca
[	[	kIx(	[
<g/>
majorka	majorka	k1gFnSc1	majorka
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
náležící	náležící	k2eAgInSc1d1	náležící
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
Baleárských	baleárský	k2eAgInPc2d1	baleárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Valencie	Valencie	k1gFnSc2	Valencie
<g/>
,	,	kIx,	,
Barcelony	Barcelona	k1gFnSc2	Barcelona
a	a	k8xC	a
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
ostrova	ostrov	k1gInSc2	ostrov
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
648	[number]	k4	648
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
110	[number]	k4	110
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
mezi	mezi	k7c7	mezi
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgMnSc7d3	veliký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gFnSc1	Mallorca
<g/>
.	.	kIx.	.
</s>
<s>
Mallorca	Mallorca	k1gFnSc1	Mallorca
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
příjmů	příjem	k1gInPc2	příjem
tamního	tamní	k2eAgNnSc2d1	tamní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Mallorcu	Mallorca	k1gFnSc4	Mallorca
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
kromě	kromě	k7c2	kromě
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
i	i	k8xC	i
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
a	a	k8xC	a
pěstováním	pěstování	k1gNnSc7	pěstování
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
necelý	celý	k2eNgMnSc1d1	necelý
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
mají	mít	k5eAaImIp3nP	mít
svoje	svůj	k3xOyFgNnSc4	svůj
specifické	specifický	k2eAgNnSc4d1	specifické
nářečí	nářečí	k1gNnSc4	nářečí
tzv.	tzv.	kA	tzv.
Mallorqui	Mallorqui	k1gNnSc4	Mallorqui
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
katalánštiny	katalánština	k1gFnSc2	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
Mallorcu	Mallorcu	k6eAd1	Mallorcu
obývali	obývat	k5eAaImAgMnP	obývat
a	a	k8xC	a
dobývali	dobývat	k5eAaImAgMnP	dobývat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
Maurové	Maurové	k?	Maurové
i	i	k8xC	i
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
mnoho	mnoho	k4c1	mnoho
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
působení	působení	k1gNnSc4	působení
různých	různý	k2eAgFnPc2d1	různá
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Kolonizací	kolonizace	k1gFnSc7	kolonizace
i	i	k9	i
kartograficky	kartograficky	k6eAd1	kartograficky
zruční	zručný	k2eAgMnPc1d1	zručný
Římané	Říman	k1gMnPc1	Říman
dali	dát	k5eAaPmAgMnP	dát
Mallorce	Mallorka	k1gFnSc6	Mallorka
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
verzi	verze	k1gFnSc6	verze
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Ta	ten	k3xDgFnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
ostrova	ostrov	k1gInSc2	ostrov
před	před	k7c7	před
Barbarskými	barbarský	k2eAgMnPc7d1	barbarský
piráty	pirát	k1gMnPc7	pirát
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
podél	podél	k6eAd1	podél
pobřeží	pobřeží	k1gNnSc4	pobřeží
postaveno	postaven	k2eAgNnSc4d1	postaveno
na	na	k7c4	na
84	[number]	k4	84
obranných	obranný	k2eAgFnPc2d1	obranná
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
Palmě	palma	k1gFnSc3	palma
dominuje	dominovat	k5eAaImIp3nS	dominovat
katedrála	katedrála	k1gFnSc1	katedrála
La	la	k1gNnSc2	la
Seu	Seu	k1gFnSc1	Seu
(	(	kIx(	(
<g/>
katedrála	katedrála	k1gFnSc1	katedrála
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
dostavěna	dostavět	k5eAaPmNgFnS	dostavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1587	[number]	k4	1587
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
je	být	k5eAaImIp3nS	být
i	i	k9	i
gotický	gotický	k2eAgInSc4d1	gotický
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
Jakub	Jakub	k1gMnSc1	Jakub
II	II	kA	II
<g/>
..	..	k?	..
Ten	ten	k3xDgMnSc1	ten
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k8xS	jako
vězení	vězení	k1gNnSc1	vězení
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byzantskou	byzantský	k2eAgFnSc4d1	byzantská
periodu	perioda	k1gFnSc4	perioda
připomínají	připomínat	k5eAaImIp3nP	připomínat
základy	základ	k1gInPc1	základ
baptisteria	baptisterium	k1gNnSc2	baptisterium
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
kříže	kříž	k1gInSc2	kříž
s	s	k7c7	s
trojlistou	trojlistý	k2eAgFnSc7d1	trojlistá
nádrží	nádrž	k1gFnSc7	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Arabech	Arab	k1gMnPc6	Arab
zde	zde	k6eAd1	zde
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
arabské	arabský	k2eAgFnPc1d1	arabská
lázně	lázeň	k1gFnPc1	lázeň
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
zástavba	zástavba	k1gFnSc1	zástavba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
panování	panování	k1gNnSc2	panování
aragonských	aragonský	k2eAgMnPc2d1	aragonský
králů	král	k1gMnPc2	král
a	a	k8xC	a
španělských	španělský	k2eAgMnPc2d1	španělský
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1399	[number]	k4	1399
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
kartuziánský	kartuziánský	k2eAgInSc1d1	kartuziánský
klášter	klášter	k1gInSc1	klášter
Valldemosa	Valldemosa	k1gFnSc1	Valldemosa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
stavby	stavba	k1gFnSc2	stavba
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lékárnou	lékárna	k1gFnSc7	lékárna
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Pobýval	pobývat	k5eAaImAgMnS	pobývat
tam	tam	k6eAd1	tam
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1838	[number]	k4	1838
-	-	kIx~	-
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
polský	polský	k2eAgMnSc1d1	polský
skladatel	skladatel	k1gMnSc1	skladatel
Frederik	Frederik	k1gMnSc1	Frederik
Chopin	Chopin	k1gMnSc1	Chopin
se	s	k7c7	s
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
George	George	k1gFnSc7	George
Sandovou	Sandová	k1gFnSc7	Sandová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technických	technický	k2eAgFnPc2d1	technická
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
zachovala	zachovat	k5eAaPmAgFnS	zachovat
dráha	dráha	k1gFnSc1	dráha
starého	starý	k2eAgInSc2d1	starý
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Palmy	palma	k1gFnSc2	palma
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Sóller	Sóller	k1gInSc1	Sóller
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
Mallorcu	Mallorca	k1gMnSc4	Mallorca
objevil	objevit	k5eAaPmAgMnS	objevit
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
toskánský	toskánský	k2eAgMnSc1d1	toskánský
Ludvík	Ludvík	k1gMnSc1	Ludvík
Salvátor	Salvátor	k1gMnSc1	Salvátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
Palmě	palma	k1gFnSc6	palma
zbudoval	zbudovat	k5eAaPmAgMnS	zbudovat
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Palmy	palma	k1gFnSc2	palma
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Mallorce	Mallorka	k1gFnSc6	Mallorka
několik	několik	k4yIc4	několik
oblíbených	oblíbený	k2eAgNnPc2d1	oblíbené
městeček	městečko	k1gNnPc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Deia	Deia	k1gFnSc1	Deia
-	-	kIx~	-
městečko	městečko	k1gNnSc1	městečko
se	s	k7c7	s
strmými	strmý	k2eAgFnPc7d1	strmá
uličkami	ulička	k1gFnPc7	ulička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
především	především	k9	především
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
Andratx	Andratx	k1gInSc4	Andratx
plný	plný	k2eAgInSc4d1	plný
starých	starý	k2eAgInPc2d1	starý
kostelů	kostel	k1gInPc2	kostel
nebo	nebo	k8xC	nebo
Alcudia	Alcudium	k1gNnSc2	Alcudium
s	s	k7c7	s
římskými	římský	k2eAgFnPc7d1	římská
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Tramuntana	Tramuntan	k1gMnSc2	Tramuntan
nabízí	nabízet	k5eAaImIp3nS	nabízet
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
tu	tu	k6eAd1	tu
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ideálním	ideální	k2eAgNnSc7d1	ideální
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
horolezce	horolezec	k1gMnPc4	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Mallorca	Mallorca	k1gFnSc1	Mallorca
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
nabízí	nabízet	k5eAaImIp3nS	nabízet
spoustu	spousta	k1gFnSc4	spousta
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
mallorských	mallorský	k2eAgMnPc2d1	mallorský
králů	král	k1gMnPc2	král
Mallorské	Mallorský	k2eAgFnSc2d1	Mallorská
království	království	k1gNnSc2	království
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mallorca	Mallorc	k1gInSc2	Mallorc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
