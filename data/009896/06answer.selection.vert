<s>
Malý	Malý	k1gMnSc1	Malý
Mendel	Mendel	k1gMnSc1	Mendel
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtěn	k2eAgInSc4d1	pokřtěn
podle	podle	k7c2	podle
zápisků	zápisek	k1gInPc2	zápisek
z	z	k7c2	z
matriky	matrika	k1gFnSc2	matrika
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
moravské	moravský	k2eAgFnSc6d1	Moravská
vesnici	vesnice	k1gFnSc6	vesnice
Dolním	dolní	k2eAgNnSc6d1	dolní
Vražném	vražný	k2eAgNnSc6d1	Vražné
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Gross-Petersdorf	Gross-Petersdorf	k1gInSc4	Gross-Petersdorf
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
Johann	Johann	k1gMnSc1	Johann
<g/>
.	.	kIx.	.
</s>
