<p>
<s>
Gregor	Gregor	k1gMnSc1	Gregor
Johann	Johann	k1gMnSc1	Johann
Mendel	Mendel	k1gMnSc1	Mendel
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
Řehoř	Řehoř	k1gMnSc1	Řehoř
Jan	Jan	k1gMnSc1	Jan
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1822	[number]	k4	1822
Hynčice	Hynčice	k1gFnSc2	Hynčice
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1884	[number]	k4	1884
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
objevitel	objevitel	k1gMnSc1	objevitel
základních	základní	k2eAgInPc2d1	základní
zákonů	zákon	k1gInPc2	zákon
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
mnich	mnich	k1gMnSc1	mnich
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
opat	opat	k1gMnSc1	opat
augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Mendel	Mendel	k1gMnSc1	Mendel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1822	[number]	k4	1822
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgMnPc2d1	mluvící
drobných	drobný	k2eAgMnPc2d1	drobný
zemědělců	zemědělec	k1gMnPc2	zemědělec
v	v	k7c6	v
Hynčicích	Hynčice	k1gFnPc6	Hynčice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Heinzendorf	Heinzendorf	k1gInSc1	Heinzendorf
bei	bei	k?	bei
Odrau	Odraus	k1gInSc2	Odraus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
58	[number]	k4	58
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
čp.	čp.	k?	čp.
69	[number]	k4	69
<g/>
)	)	kIx)	)
Antonu	Anton	k1gMnSc3	Anton
Mendelovi	Mendel	k1gMnSc3	Mendel
a	a	k8xC	a
matce	matka	k1gFnSc3	matka
Rosině	Rosin	k2eAgFnSc3d1	Rosina
rozené	rozený	k2eAgFnSc2d1	rozená
Schwirtlich	Schwirtlich	k1gInSc4	Schwirtlich
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
Hynčice	Hynčice	k1gFnSc1	Hynčice
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Vražné	vražný	k2eAgFnSc2d1	vražný
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nový	nový	k2eAgInSc1d1	nový
Jičín	Jičín	k1gInSc1	Jičín
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
J.	J.	kA	J.
G.	G.	kA	G.
Mendela	Mendel	k1gMnSc2	Mendel
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
rolnický	rolnický	k2eAgInSc1d1	rolnický
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
zapsán	zapsat	k5eAaPmNgInS	zapsat
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
s	s	k7c7	s
významnou	významný	k2eAgFnSc7d1	významná
pomocí	pomoc	k1gFnSc7	pomoc
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
zcela	zcela	k6eAd1	zcela
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
může	moct	k5eAaImIp3nS	moct
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nabídnout	nabídnout	k5eAaPmF	nabídnout
kromě	kromě	k7c2	kromě
muzejní	muzejní	k2eAgFnSc2d1	muzejní
expozice	expozice	k1gFnSc2	expozice
také	také	k9	také
konferenční	konferenční	k2eAgFnSc2d1	konferenční
a	a	k8xC	a
společenské	společenský	k2eAgFnSc2d1	společenská
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc1	Malý
Mendel	Mendel	k1gMnSc1	Mendel
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtěn	k2eAgInSc4d1	pokřtěn
podle	podle	k7c2	podle
zápisků	zápisek	k1gInPc2	zápisek
z	z	k7c2	z
matriky	matrika	k1gFnSc2	matrika
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
moravské	moravský	k2eAgFnSc6d1	Moravská
vesnici	vesnice	k1gFnSc6	vesnice
Dolním	dolní	k2eAgNnSc6d1	dolní
Vražném	vražný	k2eAgNnSc6d1	Vražné
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Gross-Petersdorf	Gross-Petersdorf	k1gInSc4	Gross-Petersdorf
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
Johann	Johann	k1gMnSc1	Johann
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
dvojjazyčném	dvojjazyčný	k2eAgNnSc6d1	dvojjazyčné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
plynně	plynně	k6eAd1	plynně
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
Moravanem	Moravan	k1gMnSc7	Moravan
německé	německý	k2eAgFnSc2d1	německá
řeči	řeč	k1gFnSc2	řeč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Hynčicích	Hynčice	k1gFnPc6	Hynčice
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
mladý	mladý	k2eAgMnSc1d1	mladý
Johann	Johann	k1gMnSc1	Johann
piaristickou	piaristický	k2eAgFnSc4d1	piaristická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Lipníku	Lipník	k1gInSc6	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
ukončil	ukončit	k5eAaPmAgInS	ukončit
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1840	[number]	k4	1840
až	až	k9	až
1843	[number]	k4	1843
studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
přírodní	přírodní	k2eAgFnSc4d1	přírodní
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
polní	polní	k2eAgNnSc4d1	polní
hospodářství	hospodářství	k1gNnSc4	hospodářství
Johann	Johann	k1gMnSc1	Johann
Karl	Karl	k1gMnSc1	Karl
Nestler	Nestler	k1gMnSc1	Nestler
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
výzkumník	výzkumník	k1gMnSc1	výzkumník
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
šlechtění	šlechtění	k1gNnSc2	šlechtění
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výzkum	výzkum	k1gInSc1	výzkum
šlechtění	šlechtění	k1gNnSc2	šlechtění
ovcí	ovce	k1gFnPc2	ovce
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
pozdější	pozdní	k2eAgFnSc4d2	pozdější
Mendelovu	Mendelův	k2eAgFnSc4d1	Mendelova
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Mendela	Mendel	k1gMnSc4	Mendel
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Friedrich	Friedrich	k1gMnSc1	Friedrich
Franz	Franz	k1gMnSc1	Franz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
Mendel	Mendel	k1gMnSc1	Mendel
živil	živit	k5eAaImAgMnS	živit
převážně	převážně	k6eAd1	převážně
kondicemi	kondice	k1gFnPc7	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
a	a	k8xC	a
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
matky	matka	k1gFnSc2	matka
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
augustinianského	augustinianský	k2eAgInSc2d1	augustinianský
kláštera	klášter	k1gInSc2	klášter
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
řeholní	řeholní	k2eAgNnSc4d1	řeholní
jméno	jméno	k1gNnSc4	jméno
Gregor	Gregor	k1gMnSc1	Gregor
(	(	kIx(	(
<g/>
řeholník	řeholník	k1gMnSc1	řeholník
dle	dle	k7c2	dle
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
používá	používat	k5eAaImIp3nS	používat
právě	právě	k9	právě
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
správné	správný	k2eAgNnSc1d1	správné
pořadí	pořadí	k1gNnSc1	pořadí
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
je	být	k5eAaImIp3nS	být
Gregor	Gregor	k1gMnSc1	Gregor
Johann	Johann	k1gMnSc1	Johann
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
28	[number]	k4	28
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jako	jako	k9	jako
suplent	suplent	k1gMnSc1	suplent
(	(	kIx(	(
<g/>
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
<g/>
)	)	kIx)	)
gymnázia	gymnázium	k1gNnSc2	gymnázium
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
přechodně	přechodně	k6eAd1	přechodně
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
42	[number]	k4	42
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Horní	horní	k2eAgNnSc1d1	horní
České	český	k2eAgNnSc1d1	české
<g/>
,	,	kIx,	,
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
učitelským	učitelský	k2eAgFnPc3d1	učitelská
zkouškám	zkouška	k1gFnPc3	zkouška
z	z	k7c2	z
přírodopisu	přírodopis	k1gInSc2	přírodopis
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zkoušek	zkouška	k1gFnPc2	zkouška
celkově	celkově	k6eAd1	celkově
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
paradoxně	paradoxně	k6eAd1	paradoxně
díky	díky	k7c3	díky
neúspěchu	neúspěch	k1gInSc3	neúspěch
v	v	k7c6	v
přírodopisu	přírodopis	k1gInSc6	přírodopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
studoval	studovat	k5eAaImAgMnS	studovat
Mendel	Mendel	k1gMnSc1	Mendel
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
botaniku	botanika	k1gFnSc4	botanika
<g/>
,	,	kIx,	,
zoologii	zoologie	k1gFnSc4	zoologie
a	a	k8xC	a
paleontologii	paleontologie	k1gFnSc4	paleontologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
byl	být	k5eAaImAgMnS	být
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
užití	užití	k1gNnSc4	užití
kombinatoriky	kombinatorika	k1gFnSc2	kombinatorika
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
v	v	k7c6	v
aplikovaných	aplikovaný	k2eAgFnPc6d1	aplikovaná
vědách	věda	k1gFnPc6	věda
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
náhlé	náhlý	k2eAgFnSc2d1	náhlá
těžké	těžký	k2eAgFnSc2d1	těžká
nemoci	nemoc	k1gFnSc2	nemoc
podařilo	podařit	k5eAaPmAgNnS	podařit
složit	složit	k5eAaPmF	složit
profesorské	profesorský	k2eAgFnPc4d1	profesorská
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
učil	učit	k5eAaImAgMnS	učit
jako	jako	k9	jako
suplent	suplent	k1gMnSc1	suplent
přírodopisu	přírodopis	k1gInSc2	přírodopis
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
německé	německý	k2eAgFnSc3d1	německá
reálce	reálka	k1gFnSc3	reálka
v	v	k7c6	v
Jánské	Jánské	k2eAgFnSc6d1	Jánské
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
meteorologii	meteorologie	k1gFnSc4	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
důkladnému	důkladný	k2eAgNnSc3d1	důkladné
studiu	studio	k1gNnSc3	studio
těchto	tento	k3xDgFnPc2	tento
věd	věda	k1gFnPc2	věda
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
důležitost	důležitost	k1gFnSc4	důležitost
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
statistiky	statistika	k1gFnSc2	statistika
pro	pro	k7c4	pro
vysvětlování	vysvětlování	k1gNnSc4	vysvětlování
přírodních	přírodní	k2eAgInPc2d1	přírodní
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
později	pozdě	k6eAd2	pozdě
využil	využít	k5eAaPmAgMnS	využít
během	během	k7c2	během
svých	svůj	k3xOyFgInPc2	svůj
pokusů	pokus	k1gInPc2	pokus
s	s	k7c7	s
hrachem	hrách	k1gInSc7	hrách
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
opata	opat	k1gMnSc2	opat
Cyrila	Cyril	k1gMnSc2	Cyril
Nappa	Napp	k1gMnSc2	Napp
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
opatem	opat	k1gMnSc7	opat
Augustiniánského	augustiniánský	k2eAgInSc2d1	augustiniánský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
představoval	představovat	k5eAaImAgMnS	představovat
významnou	významný	k2eAgFnSc4d1	významná
osobnost	osobnost	k1gFnSc4	osobnost
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
císařství	císařství	k1gNnSc6	císařství
rakousko-uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
vysoké	vysoký	k2eAgFnSc3d1	vysoká
funkci	funkce	k1gFnSc3	funkce
postupně	postupně	k6eAd1	postupně
přibíral	přibírat	k5eAaImAgInS	přibírat
další	další	k2eAgInSc1d1	další
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
svou	svůj	k3xOyFgFnSc4	svůj
pokusnou	pokusný	k2eAgFnSc4d1	pokusná
činnost	činnost	k1gFnSc4	činnost
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zanedbávat	zanedbávat	k5eAaImF	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
spotřeboval	spotřebovat	k5eAaPmAgMnS	spotřebovat
Mendel	Mendel	k1gMnSc1	Mendel
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
vládou	vláda	k1gFnSc7	vláda
kvůli	kvůli	k7c3	kvůli
neoprávněně	oprávněně	k6eNd1	oprávněně
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
klášterního	klášterní	k2eAgInSc2d1	klášterní
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
Mendel	Mendel	k1gMnSc1	Mendel
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1884	[number]	k4	1884
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
Ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
do	do	k7c2	do
hrobky	hrobka	k1gFnSc2	hrobka
augustiniánů	augustinián	k1gMnPc2	augustinián
<g/>
.	.	kIx.	.
</s>
<s>
Rekviem	rekviem	k1gNnSc1	rekviem
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
klášterem	klášter	k1gInSc7	klášter
na	na	k7c6	na
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
podporovaný	podporovaný	k2eAgMnSc1d1	podporovaný
lašský	lašský	k2eAgMnSc1d1	lašský
rodák	rodák	k1gMnSc1	rodák
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zkoumání	zkoumání	k1gNnPc4	zkoumání
dědičnosti	dědičnost	k1gFnSc2	dědičnost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1863	[number]	k4	1863
věnoval	věnovat	k5eAaPmAgMnS	věnovat
křížení	křížení	k1gNnSc3	křížení
hrachu	hrách	k1gInSc2	hrách
a	a	k8xC	a
sledování	sledování	k1gNnSc2	sledování
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgInPc2	svůj
pokusů	pokus	k1gInPc2	pokus
formuloval	formulovat	k5eAaImAgMnS	formulovat
tři	tři	k4xCgNnPc4	tři
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
později	pozdě	k6eAd2	pozdě
vešla	vejít	k5eAaPmAgNnP	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xS	jako
Mendelovy	Mendelův	k2eAgInPc4d1	Mendelův
zákony	zákon	k1gInPc4	zákon
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgNnP	být
jeho	jeho	k3xOp3gNnPc1	jeho
experimentální	experimentální	k2eAgNnPc1d1	experimentální
data	datum	k1gNnPc1	datum
mnohokrát	mnohokrát	k6eAd1	mnohokrát
prověřována	prověřován	k2eAgNnPc1d1	prověřováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
kritikům	kritik	k1gMnPc3	kritik
zdála	zdát	k5eAaImAgFnS	zdát
až	až	k6eAd1	až
příliš	příliš	k6eAd1	příliš
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
než	než	k8xS	než
falšování	falšování	k1gNnSc1	falšování
dat	datum	k1gNnPc2	datum
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
Mendelovi	Mendelův	k2eAgMnPc1d1	Mendelův
vytýkat	vytýkat	k5eAaImF	vytýkat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
mnoha	mnoho	k4c2	mnoho
tisícovek	tisícovka	k1gFnPc2	tisícovka
pokusů	pokus	k1gInPc2	pokus
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
pouze	pouze	k6eAd1	pouze
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejlépe	dobře	k6eAd3	dobře
dokládaly	dokládat	k5eAaImAgFnP	dokládat
jeho	jeho	k3xOp3gFnPc1	jeho
teorie	teorie	k1gFnPc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
závěru	závěr	k1gInSc3	závěr
také	také	k9	také
napovídá	napovídat	k5eAaBmIp3nS	napovídat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Mendel	Mendel	k1gMnSc1	Mendel
sledoval	sledovat	k5eAaImAgMnS	sledovat
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
popsal	popsat	k5eAaPmAgMnS	popsat
pouze	pouze	k6eAd1	pouze
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
jediným	jediný	k2eAgInSc7d1	jediný
genem	gen	k1gInSc7	gen
a	a	k8xC	a
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
dědičnost	dědičnost	k1gFnSc1	dědičnost
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
dnes	dnes	k6eAd1	dnes
nepřípustný	přípustný	k2eNgInSc4d1	nepřípustný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pokusy	pokus	k1gInPc4	pokus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nevyšly	vyjít	k5eNaPmAgInP	vyjít
úplně	úplně	k6eAd1	úplně
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
nastavoval	nastavovat	k5eAaImAgInS	nastavovat
dalšími	další	k2eAgInPc7d1	další
minipokusy	minipokus	k1gInPc7	minipokus
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedostal	dostat	k5eNaPmAgInS	dostat
přesně	přesně	k6eAd1	přesně
ten	ten	k3xDgInSc1	ten
poměr	poměr	k1gInSc1	poměr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chtěl	chtít	k5eAaImAgInS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
statistika	statistika	k1gFnSc1	statistika
de	de	k?	de
facto	facto	k1gNnSc1	facto
neexistovala	existovat	k5eNaImAgFnS	existovat
a	a	k8xC	a
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
aplikovala	aplikovat	k5eAaBmAgFnS	aplikovat
matematické	matematický	k2eAgFnPc4d1	matematická
metody	metoda	k1gFnPc4	metoda
na	na	k7c4	na
biologický	biologický	k2eAgInSc4d1	biologický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
normální	normální	k2eAgInSc4d1	normální
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
tím	ten	k3xDgMnSc7	ten
Mendel	Mendel	k1gMnSc1	Mendel
ani	ani	k8xC	ani
nijak	nijak	k6eAd1	nijak
zvláště	zvláště	k6eAd1	zvláště
netajil	tajit	k5eNaImAgMnS	tajit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
Brněnského	brněnský	k2eAgInSc2d1	brněnský
přírodovědeckého	přírodovědecký	k2eAgInSc2d1	přírodovědecký
spolku	spolek	k1gInSc2	spolek
a	a	k8xC	a
následně	následně	k6eAd1	následně
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
rostlinnými	rostlinný	k2eAgInPc7d1	rostlinný
hybridy	hybrid	k1gInPc7	hybrid
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Versuche	Versuche	k1gNnSc1	Versuche
über	übera	k1gFnPc2	übera
Pflanzen-Hybriden	Pflanzen-Hybridna	k1gFnPc2	Pflanzen-Hybridna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
jediné	jediné	k1gNnSc4	jediné
pocty	pocta	k1gFnSc2	pocta
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
přírodovědných	přírodovědný	k2eAgInPc6d1	přírodovědný
kruzích	kruh	k1gInPc6	kruh
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Přírodovědeckého	přírodovědecký	k2eAgInSc2d1	přírodovědecký
spolku	spolek	k1gInSc2	spolek
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyložil	vyložit	k5eAaPmAgMnS	vyložit
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
tohoto	tento	k3xDgInSc2	tento
spolku	spolek	k1gInSc2	spolek
výsledky	výsledek	k1gInPc1	výsledek
své	svůj	k3xOyFgFnPc4	svůj
druhé	druhý	k4xOgFnPc4	druhý
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
křížení	křížení	k1gNnSc2	křížení
rostlin	rostlina	k1gFnPc2	rostlina
o	o	k7c6	o
jestřábnících	jestřábník	k1gInPc6	jestřábník
<g/>
.	.	kIx.	.
</s>
<s>
Jestřábníky	jestřábník	k1gInPc1	jestřábník
byly	být	k5eAaImAgInP	být
ovšem	ovšem	k9	ovšem
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
volbou	volba	k1gFnSc7	volba
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
atypickému	atypický	k2eAgNnSc3d1	atypické
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Mendel	Mendel	k1gMnSc1	Mendel
tak	tak	k9	tak
nabyl	nabýt	k5eAaPmAgMnS	nabýt
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
objasněné	objasněný	k2eAgFnPc4d1	objasněná
zákonitosti	zákonitost	k1gFnPc4	zákonitost
vlastně	vlastně	k9	vlastně
neplatí	platit	k5eNaImIp3nP	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mendelův	Mendelův	k2eAgInSc1d1	Mendelův
přínos	přínos	k1gInSc1	přínos
pro	pro	k7c4	pro
biologii	biologie	k1gFnSc4	biologie
byl	být	k5eAaImAgInS	být
rozpoznán	rozpoznat	k5eAaPmNgInS	rozpoznat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Hugo	Hugo	k1gMnSc1	Hugo
de	de	k?	de
Vriesem	Vries	k1gMnSc7	Vries
<g/>
,	,	kIx,	,
Carlem	Carl	k1gMnSc7	Carl
Corrensem	Correns	k1gMnSc7	Correns
a	a	k8xC	a
Erichem	Erich	k1gMnSc7	Erich
von	von	k1gInSc1	von
Tschermakem	Tschermak	k1gInSc7	Tschermak
a	a	k8xC	a
především	především	k9	především
Williamem	William	k1gInSc7	William
Batesonem	Bateson	k1gMnSc7	Bateson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
přeložit	přeložit	k5eAaPmF	přeložit
Mendelovu	Mendelův	k2eAgFnSc4d1	Mendelova
práci	práce	k1gFnSc4	práce
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
oboru	obor	k1gInSc2	obor
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
definoval	definovat	k5eAaBmAgInS	definovat
principy	princip	k1gInPc4	princip
nyní	nyní	k6eAd1	nyní
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
Mendelovy	Mendelův	k2eAgInPc4d1	Mendelův
zákony	zákon	k1gInPc4	zákon
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
biostatistické	biostatistický	k2eAgFnSc2d1	biostatistický
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mendel	Mendel	k1gMnSc1	Mendel
jako	jako	k8xC	jako
meteorolog	meteorolog	k1gMnSc1	meteorolog
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
prováděl	provádět	k5eAaImAgMnS	provádět
Mendel	Mendel	k1gMnSc1	Mendel
každodenní	každodenní	k2eAgNnPc4d1	každodenní
meteorologická	meteorologický	k2eAgNnPc4d1	meteorologické
pozorování	pozorování	k1gNnPc4	pozorování
pro	pro	k7c4	pro
Meteorologický	meteorologický	k2eAgInSc4d1	meteorologický
ústav	ústav	k1gInSc4	ústav
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
třinácti	třináct	k4xCc2	třináct
Mendelových	Mendelová	k1gFnPc2	Mendelová
publikací	publikace	k1gFnPc2	publikace
se	se	k3xPyFc4	se
devět	devět	k4xCc4	devět
týká	týkat	k5eAaImIp3nS	týkat
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
jméno	jméno	k1gNnSc1	jméno
čestně	čestně	k6eAd1	čestně
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
vědecká	vědecký	k2eAgFnSc1d1	vědecká
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památka	památka	k1gFnSc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
život	život	k1gInSc1	život
Gregora	Gregor	k1gMnSc2	Gregor
Mendela	Mendel	k1gMnSc2	Mendel
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
autentických	autentický	k2eAgFnPc6d1	autentická
prostorách	prostora	k1gFnPc6	prostora
starobrněnského	starobrněnského	k2eAgNnSc1d1	starobrněnského
opatství	opatství	k1gNnSc1	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
propagaci	propagace	k1gFnSc4	propagace
jeho	jeho	k3xOp3gInSc2	jeho
vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
odkazu	odkaz	k1gInSc2	odkaz
věnuje	věnovat	k5eAaImIp3nS	věnovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
centra	centrum	k1gNnSc2	centrum
Mendelianum	Mendelianum	k1gInSc1	Mendelianum
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
genetiky	genetika	k1gFnSc2	genetika
nese	nést	k5eAaImIp3nS	nést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
názvu	název	k1gInSc6	název
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Gregora	Gregor	k1gMnSc2	Gregor
Mendela	Mendel	k1gMnSc2	Mendel
v	v	k7c6	v
Hynčicích	Hynčice	k1gFnPc6	Hynčice
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
a	a	k8xC	a
vedle	vedle	k7c2	vedle
expozice	expozice	k1gFnSc2	expozice
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Mendelově	Mendelův	k2eAgFnSc3d1	Mendelova
životu	život	k1gInSc2	život
a	a	k8xC	a
dílu	díl	k1gInSc2	díl
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
exponáty	exponát	k1gInPc1	exponát
zobrazující	zobrazující	k2eAgInSc1d1	zobrazující
venkovský	venkovský	k2eAgInSc4d1	venkovský
život	život	k1gInSc4	život
regionu	region	k1gInSc2	region
Moravské	moravský	k2eAgNnSc4d1	Moravské
Kravařsko	Kravařsko	k1gNnSc4	Kravařsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
J.	J.	kA	J.
G.	G.	kA	G.
Mendelovi	Mendel	k1gMnSc6	Mendel
se	se	k3xPyFc4	se
také	také	k6eAd1	také
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
pár	pár	k4xCyI	pár
vlaků	vlak	k1gInPc2	vlak
kategorie	kategorie	k1gFnSc1	kategorie
expres	expres	k1gInSc1	expres
společnosti	společnost	k1gFnSc2	společnost
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
jezdící	jezdící	k2eAgFnSc2d1	jezdící
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
–	–	k?	–
<g/>
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Versuche	Versuche	k1gFnSc1	Versuche
über	übera	k1gFnPc2	übera
Pflanzen-Hybriden	Pflanzen-Hybridna	k1gFnPc2	Pflanzen-Hybridna
<g/>
.	.	kIx.	.
</s>
<s>
Verh	Verh	k1gMnSc1	Verh
<g/>
.	.	kIx.	.
</s>
<s>
Naturforsch	Naturforsch	k1gMnSc1	Naturforsch
<g/>
.	.	kIx.	.
</s>
<s>
Ver	Ver	k?	Ver
<g/>
.	.	kIx.	.
</s>
<s>
Brünn	Brünn	k1gInSc1	Brünn
4	[number]	k4	4
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
J.	J.	kA	J.
R.	R.	kA	R.
Hortic	Hortice	k1gFnPc2	Hortice
<g/>
.	.	kIx.	.
</s>
<s>
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OREL	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
<g/>
.	.	kIx.	.
</s>
<s>
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
a	a	k8xC	a
počátky	počátek	k1gInPc4	počátek
genetiky	genetika	k1gFnSc2	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1082	[number]	k4	1082
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mendelovy	Mendelův	k2eAgInPc1d1	Mendelův
zákony	zákon	k1gInPc1	zákon
dědičnosti	dědičnost	k1gFnSc2	dědičnost
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Unger	Unger	k1gMnSc1	Unger
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Gregor	Gregor	k1gMnSc1	Gregor
Johann	Johann	k1gMnSc1	Johann
Mendel	Mendel	k1gMnSc1	Mendel
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
</s>
</p>
<p>
<s>
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
v	v	k7c4	v
Encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
</s>
</p>
<p>
<s>
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
MU	MU	kA	MU
</s>
</p>
<p>
<s>
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Mendelianum	Mendelianum	k1gNnSc1	Mendelianum
při	při	k7c6	při
MZM	MZM	kA	MZM
</s>
</p>
<p>
<s>
mendel-rodnydum	mendelodnydum	k1gInSc1	mendel-rodnydum
<g/>
.	.	kIx.	.
<g/>
vrazne	vraznout	k5eAaPmIp3nS	vraznout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Gregor	Gregor	k1gMnSc1	Gregor
Johann	Johann	k1gMnSc1	Johann
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
:	:	kIx,	:
brněnský	brněnský	k2eAgMnSc1d1	brněnský
Darwin	Darwin	k1gMnSc1	Darwin
</s>
</p>
