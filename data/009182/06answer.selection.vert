<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc4	otec
<g/>
"	"	kIx"	"
klasické	klasický	k2eAgFnSc2d1	klasická
symfonie	symfonie	k1gFnSc2	symfonie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
;	;	kIx,	;
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měla	mít	k5eAaImAgFnS	mít
symfonie	symfonie	k1gFnSc1	symfonie
obvykle	obvykle	k6eAd1	obvykle
sonátovou	sonátový	k2eAgFnSc4d1	sonátová
cyklickou	cyklický	k2eAgFnSc4d1	cyklická
formu	forma	k1gFnSc4	forma
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
větách	věta	k1gFnPc6	věta
<g/>
.	.	kIx.	.
</s>
