<s>
Dean	Dean	k1gMnSc1	Dean
Ray	Ray	k1gMnSc1	Ray
Koontz	Koontz	k1gMnSc1	Koontz
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
především	především	k9	především
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
thrillerů	thriller	k1gInPc2	thriller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
prvky	prvek	k1gInPc4	prvek
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
hororu	horor	k1gInSc2	horor
<g/>
.	.	kIx.	.
</s>
<s>
Dean	Dean	k1gMnSc1	Dean
Koontz	Koontz	k1gMnSc1	Koontz
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
Raye	Ray	k1gFnSc2	Ray
a	a	k8xC	a
Florence	Florenc	k1gFnSc2	Florenc
Koontzových	Koontzová	k1gFnPc2	Koontzová
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
v	v	k7c6	v
chudých	chudý	k2eAgInPc6d1	chudý
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
alkoholik	alkoholik	k1gMnSc1	alkoholik
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
týral	týrat	k5eAaImAgMnS	týrat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc1	osm
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
kapesné	kapesné	k1gNnSc4	kapesné
<g/>
,	,	kIx,	,
prodával	prodávat	k5eAaImAgMnS	prodávat
je	být	k5eAaImIp3nS	být
příbuzným	příbuzný	k1gMnPc3	příbuzný
a	a	k8xC	a
sousedům	soused	k1gMnPc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
líný	líný	k2eAgMnSc1d1	líný
a	a	k8xC	a
dělal	dělat	k5eAaImAgInS	dělat
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
musel	muset	k5eAaImAgMnS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
třídního	třídní	k2eAgMnSc4d1	třídní
klauna	klaun	k1gMnSc4	klaun
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
prodal	prodat	k5eAaPmAgMnS	prodat
poprvé	poprvé	k6eAd1	poprvé
povídku	povídka	k1gFnSc4	povídka
profesionálnímu	profesionální	k2eAgInSc3d1	profesionální
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
se	se	k3xPyFc4	se
Koťátko	koťátko	k1gNnSc1	koťátko
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svých	svůj	k3xOyFgNnPc2	svůj
vysokoškolských	vysokoškolský	k2eAgNnPc2d1	vysokoškolské
studií	studio	k1gNnPc2	studio
na	na	k7c4	na
Shippensburg	Shippensburg	k1gInSc4	Shippensburg
University	universita	k1gFnSc2	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
nynější	nynější	k2eAgFnSc4d1	nynější
ženu	žena	k1gFnSc4	žena
Gerdu	Gerd	k1gInSc2	Gerd
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zakončení	zakončení	k1gNnSc6	zakončení
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
začal	začít	k5eAaPmAgMnS	začít
prodávat	prodávat	k5eAaImF	prodávat
povídky	povídka	k1gFnPc4	povídka
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
zisky	zisk	k1gInPc4	zisk
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepřineslo	přinést	k5eNaPmAgNnS	přinést
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
ho	on	k3xPp3gInSc4	on
však	však	k9	však
bavilo	bavit	k5eAaImAgNnS	bavit
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
učil	učít	k5eAaPmAgInS	učít
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Mechanicsburgu	Mechanicsburg	k1gInSc6	Mechanicsburg
v	v	k7c6	v
Pennsylvanii	Pennsylvanie	k1gFnSc6	Pennsylvanie
<g/>
.	.	kIx.	.
</s>
<s>
Koontz	Koontz	k1gMnSc1	Koontz
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
román	román	k1gInSc1	román
Night	Night	k2eAgInSc4d1	Night
Whispers	Whispers	k1gInSc4	Whispers
(	(	kIx(	(
<g/>
Šepot	šepot	k1gInSc4	šepot
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
)	)	kIx)	)
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
bestsellerů	bestseller	k1gInPc2	bestseller
novin	novina	k1gFnPc2	novina
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Koontz	Koontz	k1gMnSc1	Koontz
je	být	k5eAaImIp3nS	být
workoholik	workoholik	k1gMnSc1	workoholik
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
až	až	k9	až
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
knihovna	knihovna	k1gFnSc1	knihovna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
30	[number]	k4	30
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
čerpá	čerpat	k5eAaImIp3nS	čerpat
inspiraci	inspirace	k1gFnSc4	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Dvacet	dvacet	k4xCc4	dvacet
jeho	jeho	k3xOp3gInPc2	jeho
románů	román	k1gInPc2	román
bylo	být	k5eAaImAgNnS	být
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Velocity	Velocit	k1gInPc4	Velocit
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Neobyčejné	obyčejný	k2eNgNnSc1d1	neobyčejné
dědictví	dědictví	k1gNnSc1	dědictví
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Expectancy	Expectanca	k1gMnSc2	Expectanca
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Podivný	podivný	k2eAgMnSc1d1	podivný
Thomas	Thomas	k1gMnSc1	Thomas
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Odd	odd	kA	odd
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
The	The	k1gMnSc2	The
Face	Fac	k1gMnSc2	Fac
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Koutkem	koutek	k1gInSc7	koutek
oka	oko	k1gNnSc2	oko
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
From	From	k1gMnSc1	From
the	the	k?	the
Corner	Corner	k1gMnSc1	Corner
of	of	k?	of
His	his	k1gNnSc2	his
Eye	Eye	k1gFnPc2	Eye
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Falešná	falešný	k2eAgFnSc1d1	falešná
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
False	Fals	k1gMnSc2	Fals
Memory	Memora	k1gFnSc2	Memora
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
rozměr	rozměr	k1gInSc1	rozměr
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Seize	Seize	k1gFnSc2	Seize
the	the	k?	the
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Ničeho	nic	k3yNnSc2	nic
se	se	k3xPyFc4	se
neboj	bát	k5eNaImRp2nS	bát
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Fear	Feara	k1gFnPc2	Feara
Nothing	Nothing	k1gInSc1	Nothing
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Přežít	přežít	k5eAaPmF	přežít
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Sole	sol	k1gInSc5	sol
Survivor	Survivor	k1gMnSc1	Survivor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Platzová	Platzová	k1gFnSc1	Platzová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
:	:	kIx,	:
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-242-0473-8	[number]	k4	80-242-0473-8
Podivné	podivný	k2eAgFnSc2d1	podivná
<g />
.	.	kIx.	.
</s>
<s>
cesty	cesta	k1gFnPc1	cesta
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Strange	Strange	k1gFnPc2	Strange
Highways	Highwaysa	k1gFnPc2	Highwaysa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Půlnoc	půlnoc	k1gFnSc1	půlnoc
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
The	The	k1gMnSc2	The
Face	Fac	k1gMnSc2	Fac
of	of	k?	of
Fear	Fear	k1gMnSc1	Fear
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Skrýš	skrýš	k1gFnSc1	skrýš
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Hideaway	Hideawaa	k1gMnSc2	Hideawaa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Chladný	chladný	k2eAgInSc1d1	chladný
Plamen	plamen	k1gInSc1	plamen
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
originálu	originál	k1gInSc2	originál
Cold	Colda	k1gFnPc2	Colda
Fire	Fire	k1gFnPc2	Fire
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
