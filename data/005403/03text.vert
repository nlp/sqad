<s>
Orestés	Orestés	k1gInSc1	Orestés
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ὀ	Ὀ	k?	Ὀ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Orestes	Orestes	k1gMnSc1	Orestes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
syn	syn	k1gMnSc1	syn
mykénského	mykénský	k2eAgMnSc2d1	mykénský
krále	král	k1gMnSc2	král
Agamemnona	Agamemnon	k1gMnSc2	Agamemnon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Klytaimnéstry	Klytaimnéstra	k1gFnSc2	Klytaimnéstra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nástupcem	nástupce	k1gMnSc7	nástupce
na	na	k7c6	na
mykénském	mykénský	k2eAgInSc6d1	mykénský
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vítězném	vítězný	k2eAgInSc6d1	vítězný
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
zahynul	zahynout	k5eAaPmAgInS	zahynout
rukou	ruka	k1gFnSc7	ruka
vrahů	vrah	k1gMnPc2	vrah
najatých	najatý	k2eAgMnPc2d1	najatý
jeho	jeho	k3xOp3gNnPc2	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
Aigisthem	Aigisth	k1gInSc7	Aigisth
a	a	k8xC	a
zrádnou	zrádný	k2eAgFnSc7d1	zrádná
manželkou	manželka	k1gFnSc7	manželka
Klytaimnéstrou	Klytaimnéstra	k1gFnSc7	Klytaimnéstra
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
posléze	posléze	k6eAd1	posléze
manželi	manžel	k1gMnPc7	manžel
a	a	k8xC	a
Aigisthos	Aigisthos	k1gMnSc1	Aigisthos
usedl	usednout	k5eAaPmAgMnS	usednout
na	na	k7c4	na
uprázdněný	uprázdněný	k2eAgInSc4d1	uprázdněný
mykénský	mykénský	k2eAgInSc4d1	mykénský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Usiloval	usilovat	k5eAaImAgMnS	usilovat
také	také	k9	také
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
zákonného	zákonný	k2eAgMnSc2d1	zákonný
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgMnSc2d1	mladý
Oresta	Orestes	k1gMnSc2	Orestes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
překazila	překazit	k5eAaPmAgFnS	překazit
Orestova	Orestův	k2eAgFnSc1d1	Orestova
sestra	sestra	k1gFnSc1	sestra
Élektra	Élektra	k1gFnSc1	Élektra
<g/>
.	.	kIx.	.
</s>
<s>
Odvedla	odvést	k5eAaPmAgFnS	odvést
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
k	k	k7c3	k
fóckému	fócký	k2eAgMnSc3d1	fócký
králi	král	k1gMnSc3	král
Strofiovi	Strofius	k1gMnSc3	Strofius
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Agamemnónova	Agamemnónův	k2eAgFnSc1d1	Agamemnónova
sestra	sestra	k1gFnSc1	sestra
Astyochea	Astyoche	k2eAgFnSc1d1	Astyoche
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Anaxibie	Anaxibie	k1gFnSc1	Anaxibie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
a	a	k8xC	a
vychoval	vychovat	k5eAaPmAgMnS	vychovat
ho	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
vlastního	vlastní	k2eAgNnSc2d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Strofiův	Strofiův	k2eAgMnSc1d1	Strofiův
syn	syn	k1gMnSc1	syn
Pyladés	Pyladésa	k1gFnPc2	Pyladésa
a	a	k8xC	a
Orestés	Orestésa	k1gFnPc2	Orestésa
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
doživotními	doživotní	k2eAgMnPc7d1	doživotní
věrnými	věrný	k2eAgMnPc7d1	věrný
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Élektra	Élektra	k1gFnSc1	Élektra
věnovala	věnovat	k5eAaPmAgFnS	věnovat
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
přípravě	příprava	k1gFnSc3	příprava
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
a	a	k8xC	a
pomstě	pomsta	k1gFnSc3	pomsta
za	za	k7c4	za
zákeřnou	zákeřný	k2eAgFnSc4d1	zákeřná
smrt	smrt	k1gFnSc4	smrt
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k6eAd1	Orestés
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
,	,	kIx,	,
vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Mykén	Mykény	k1gFnPc2	Mykény
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgInS	dostat
se	se	k3xPyFc4	se
nepoznán	poznán	k2eNgInSc1d1	nepoznán
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
ohlásit	ohlásit	k5eAaPmF	ohlásit
královně	královna	k1gFnSc3	královna
<g/>
,	,	kIx,	,
že	že	k8xS	že
přináší	přinášet	k5eAaImIp3nS	přinášet
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyslechnut	vyslechnout	k5eAaPmNgInS	vyslechnout
s	s	k7c7	s
uspokojením	uspokojení	k1gNnSc7	uspokojení
a	a	k8xC	a
poté	poté	k6eAd1	poté
nechala	nechat	k5eAaPmAgFnS	nechat
zavolat	zavolat	k5eAaPmF	zavolat
Aigistha	Aigistha	k1gFnSc1	Aigistha
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k6eAd1	Orestés
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nečekal	čekat	k5eNaImAgMnS	čekat
<g/>
,	,	kIx,	,
probodl	probodnout	k5eAaPmAgMnS	probodnout
ho	on	k3xPp3gInSc4	on
hned	hned	k6eAd1	hned
ve	v	k7c6	v
dveřích	dveře	k1gFnPc6	dveře
a	a	k8xC	a
poté	poté	k6eAd1	poté
stejně	stejně	k6eAd1	stejně
naložil	naložit	k5eAaPmAgMnS	naložit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Pocítil	pocítit	k5eAaPmAgMnS	pocítit
uspokojení	uspokojení	k1gNnSc4	uspokojení
nad	nad	k7c7	nad
potrestáním	potrestání	k1gNnSc7	potrestání
vrahů	vrah	k1gMnPc2	vrah
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
čin	čin	k1gInSc4	čin
pohlížel	pohlížet	k5eAaImAgMnS	pohlížet
i	i	k9	i
mykénský	mykénský	k2eAgInSc4d1	mykénský
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
tak	tak	k9	tak
bohyně	bohyně	k1gFnSc1	bohyně
pomsty	pomsta	k1gFnSc2	pomsta
Erínye	Eríny	k1gFnSc2	Eríny
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
ho	on	k3xPp3gInSc4	on
neúprosně	úprosně	k6eNd1	úprosně
pronásledovaly	pronásledovat	k5eAaImAgInP	pronásledovat
svýma	svůj	k3xOyFgNnPc7	svůj
planoucíma	planoucí	k2eAgNnPc7d1	planoucí
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
syčením	syčení	k1gNnSc7	syčení
a	a	k8xC	a
doháněly	dohánět	k5eAaImAgFnP	dohánět
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
šílenství	šílenství	k1gNnSc3	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Slitoval	slitovat	k5eAaPmAgMnS	slitovat
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ústy	ústa	k1gNnPc7	ústa
věštkyně	věštkyně	k1gFnSc2	věštkyně
Pýthie	Pýthia	k1gFnSc2	Pýthia
poradil	poradit	k5eAaPmAgMnS	poradit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
odebere	odebrat	k5eAaPmIp3nS	odebrat
do	do	k7c2	do
daleké	daleký	k2eAgInPc4d1	daleký
Tauridy	Taurid	k1gInPc7	Taurid
<g/>
,	,	kIx,	,
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
očistit	očistit	k5eAaPmF	očistit
před	před	k7c7	před
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
sochou	socha	k1gFnSc7	socha
bohyně	bohyně	k1gFnSc2	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
.	.	kIx.	.
</s>
<s>
Čekala	čekat	k5eAaImAgFnS	čekat
ho	on	k3xPp3gNnSc4	on
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končila	končit	k5eAaImAgFnS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příchozí	příchozí	k1gMnPc1	příchozí
byli	být	k5eAaImAgMnP	být
zajímáni	zajímat	k5eAaImNgMnP	zajímat
a	a	k8xC	a
odevzdáváni	odevzdávat	k5eAaImNgMnP	odevzdávat
Artemidiným	Artemidin	k2eAgInPc3d1	Artemidin
kněžkám	kněžka	k1gFnPc3	kněžka
k	k	k7c3	k
oběti	oběť	k1gFnSc3	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k6eAd1	Orestés
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
vypravil	vypravit	k5eAaPmAgMnS	vypravit
<g/>
,	,	kIx,	,
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
ho	on	k3xPp3gMnSc4	on
přítel	přítel	k1gMnSc1	přítel
Pyladés	Pyladés	k1gInSc1	Pyladés
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
překonali	překonat	k5eAaPmAgMnP	překonat
šťastně	šťastně	k6eAd1	šťastně
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
Tauridě	Taurida	k1gFnSc6	Taurida
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Krym	Krym	k1gInSc1	Krym
<g/>
)	)	kIx)	)
však	však	k9	však
byli	být	k5eAaImAgMnP	být
zajati	zajat	k2eAgMnPc1d1	zajat
vojáky	voják	k1gMnPc7	voják
krále	král	k1gMnSc2	král
Thoanta	Thoant	k1gMnSc2	Thoant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
kněžce	kněžka	k1gFnSc3	kněžka
Ífigenei	Ífigenee	k1gFnSc4	Ífigenee
<g/>
.	.	kIx.	.
</s>
<s>
Kněžce	kněžka	k1gFnSc3	kněžka
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
líto	líto	k6eAd1	líto
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zachrání	zachránit	k5eAaPmIp3nP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
bez	bez	k7c2	bez
zaváhání	zaváhání	k1gNnSc2	zaváhání
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Pyladés	Pyladés	k1gInSc1	Pyladés
<g/>
.	.	kIx.	.
</s>
<s>
Kněžka	kněžka	k1gFnSc1	kněžka
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
povídala	povídat	k5eAaImAgFnS	povídat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jejími	její	k3xOp3gMnPc7	její
krajany	krajan	k1gMnPc7	krajan
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
vyprávělo	vyprávět	k5eAaImAgNnS	vyprávět
o	o	k7c6	o
králi	král	k1gMnSc6	král
Agamemnónovi	Agamemnón	k1gMnSc6	Agamemnón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
obětoval	obětovat	k5eAaBmAgMnS	obětovat
bohyni	bohyně	k1gFnSc4	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
zdar	zdar	k1gInSc4	zdar
výpravy	výprava	k1gFnSc2	výprava
řeckých	řecký	k2eAgNnPc2d1	řecké
vojsk	vojsko	k1gNnPc2	vojsko
proti	proti	k7c3	proti
Tróji	Trója	k1gFnSc3	Trója
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Orestés	Orestés	k1gInSc4	Orestés
a	a	k8xC	a
Ífigeneia	Ífigeneius	k1gMnSc4	Ífigeneius
jsou	být	k5eAaImIp3nP	být
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
Ífigeneia	Ífigeneia	k1gFnSc1	Ífigeneia
nakonec	nakonec	k6eAd1	nakonec
lstí	lest	k1gFnPc2	lest
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
získali	získat	k5eAaPmAgMnP	získat
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
z	z	k7c2	z
Tauridy	Taurid	k1gInPc4	Taurid
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Mykén	Mykény	k1gFnPc2	Mykény
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
nevítal	vítat	k5eNaImAgMnS	vítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Orestově	Orestův	k2eAgFnSc6d1	Orestova
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
Alétés	Alétés	k1gInSc1	Alétés
<g/>
,	,	kIx,	,
Aighistův	Aighistův	k2eAgMnSc1d1	Aighistův
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
se	se	k3xPyFc4	se
k	k	k7c3	k
pomstě	pomsta	k1gFnSc3	pomsta
Orestovi	Orestes	k1gMnSc3	Orestes
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgInS	zbavit
právoplatného	právoplatný	k2eAgMnSc4d1	právoplatný
nástupce	nástupce	k1gMnSc4	nástupce
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
odstranit	odstranit	k5eAaPmF	odstranit
Pylada	Pylades	k1gMnSc4	Pylades
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
nápadníkem	nápadník	k1gMnSc7	nápadník
Élektry	Élektrum	k1gNnPc7	Élektrum
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Alétovy	Alétův	k2eAgInPc1d1	Alétův
zločinné	zločinný	k2eAgInPc1d1	zločinný
plány	plán	k1gInPc1	plán
ale	ale	k9	ale
nevyšly	vyjít	k5eNaPmAgInP	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k6eAd1	Orestés
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
když	když	k8xS	když
hájil	hájit	k5eAaImAgMnS	hájit
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
i	i	k8xC	i
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jím	jíst	k5eAaImIp1nS	jíst
na	na	k7c4	na
mykénský	mykénský	k2eAgInSc4d1	mykénský
trůn	trůn	k1gInSc4	trůn
zasedl	zasednout	k5eAaPmAgMnS	zasednout
opět	opět	k6eAd1	opět
potomek	potomek	k1gMnSc1	potomek
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Átreovců	Átreovec	k1gInPc2	Átreovec
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
připojil	připojit	k5eAaPmAgInS	připojit
ještě	ještě	k6eAd1	ještě
Argos	Argos	k1gInSc4	Argos
a	a	k8xC	a
Spartu	Sparta	k1gFnSc4	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
ruku	ruka	k1gFnSc4	ruka
krásné	krásný	k2eAgFnSc2d1	krásná
Hermioné	Hermioná	k1gFnSc2	Hermioná
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
krále	král	k1gMnSc4	král
Meneláa	Meneláus	k1gMnSc4	Meneláus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgMnS	ucházet
také	také	k9	také
Achilleův	Achilleův	k2eAgMnSc1d1	Achilleův
syn	syn	k1gMnSc1	syn
Neoptolemos	Neoptolemos	k1gMnSc1	Neoptolemos
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Meneláos	Meneláos	k1gInSc1	Meneláos
ji	on	k3xPp3gFnSc4	on
skutečně	skutečně	k6eAd1	skutečně
za	za	k7c4	za
Neoptolema	Neoptolema	k1gNnSc4	Neoptolema
provdal	provdat	k5eAaPmAgInS	provdat
a	a	k8xC	a
Orestés	Orestés	k1gInSc1	Orestés
ho	on	k3xPp3gNnSc4	on
potom	potom	k6eAd1	potom
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Orestés	Orestés	k1gInSc1	Orestés
měl	mít	k5eAaImAgInS	mít
prý	prý	k9	prý
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Penthila	Penthil	k1gMnSc4	Penthil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Aigisthovy	Aigisthův	k2eAgFnSc2d1	Aigisthův
dcery	dcera	k1gFnSc2	dcera
Érigoné	Érigoné	k2eAgNnPc1d1	Érigoné
a	a	k8xC	a
Tísamena	Tísamen	k2eAgNnPc1d1	Tísamen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Hermioné	Hermioné	k2eAgFnSc1d1	Hermioné
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zajedno	zajedno	k6eAd1	zajedno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Orestovo	Orestův	k2eAgNnSc1d1	Orestův
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
Alétem	Alét	k1gInSc7	Alét
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
dlouholeté	dlouholetý	k2eAgNnSc1d1	dlouholeté
krvavé	krvavý	k2eAgInPc1d1	krvavý
boje	boj	k1gInPc1	boj
o	o	k7c4	o
mykénský	mykénský	k2eAgInSc4d1	mykénský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
začátku	začátek	k1gInSc6	začátek
byli	být	k5eAaImAgMnP	být
Pelopovi	Pelopův	k2eAgMnPc1d1	Pelopův
synové	syn	k1gMnPc1	syn
Átreus	Átreus	k1gMnSc1	Átreus
a	a	k8xC	a
Thyestés	Thyestésa	k1gFnPc2	Thyestésa
<g/>
,	,	kIx,	,
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
v	v	k7c6	v
generaci	generace	k1gFnSc6	generace
Agamemnónově	Agamemnónův	k2eAgFnSc6d1	Agamemnónova
<g/>
,	,	kIx,	,
Aigisthově	Aigisthův	k2eAgFnSc3d1	Aigisthův
a	a	k8xC	a
uzavřely	uzavřít	k5eAaPmAgFnP	uzavřít
se	se	k3xPyFc4	se
návratem	návrat	k1gInSc7	návrat
Oresta	Orestes	k1gMnSc4	Orestes
na	na	k7c4	na
dědičné	dědičný	k2eAgNnSc4d1	dědičné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Orestés	Orestés	k1gInSc1	Orestés
prý	prý	k9	prý
zemřel	zemřít	k5eAaPmAgInS	zemřít
jako	jako	k8xS	jako
devadesátiletý	devadesátiletý	k2eAgMnSc1d1	devadesátiletý
kmet	kmet	k1gMnSc1	kmet
po	po	k7c6	po
šťastné	šťastný	k2eAgFnSc6d1	šťastná
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
Houtzager	Houtzagra	k1gFnPc2	Houtzagra
<g/>
,	,	kIx,	,
Guus	Guusa	k1gFnPc2	Guusa
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
Gerhard	Gerhard	k1gInSc4	Gerhard
Löwe	Löw	k1gFnSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Orestés	Orestésa	k1gFnPc2	Orestésa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
