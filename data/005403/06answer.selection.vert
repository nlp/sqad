<s>
Orestés	Orestés	k1gInSc1	Orestés
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ὀ	Ὀ	k?	Ὀ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Orestes	Orestes	k1gMnSc1	Orestes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
syn	syn	k1gMnSc1	syn
mykénského	mykénský	k2eAgMnSc2d1	mykénský
krále	král	k1gMnSc2	král
Agamemnona	Agamemnon	k1gMnSc2	Agamemnon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Klytaimnéstry	Klytaimnéstra	k1gFnSc2	Klytaimnéstra
<g/>
.	.	kIx.	.
</s>
