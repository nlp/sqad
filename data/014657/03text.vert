<s>
Bílek	Bílek	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Bílek	Bílek	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slepičí	slepičí	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
na	na	k7c6
talíři	talíř	k1gInSc6
</s>
<s>
Vaječný	vaječný	k2eAgInSc1d1
bílek	bílek	k1gInSc1
(	(	kIx(
<g/>
albumen	albumen	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
stavebních	stavební	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
ochranné	ochranný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
zabraňuje	zabraňovat	k5eAaImIp3nS
poškození	poškození	k1gNnSc4
vajíčka	vajíčko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
vyživuje	vyživovat	k5eAaImIp3nS
vajíčko	vajíčko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Složení	složení	k1gNnSc1
</s>
<s>
Představuje	představovat	k5eAaImIp3nS
asi	asi	k9
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
z	z	k7c2
hmotnosti	hmotnost	k1gFnSc2
vejce	vejce	k1gNnSc2
<g/>
;	;	kIx,
ve	v	k7c6
slepičím	slepičí	k2eAgNnSc6d1
vejci	vejce	k1gNnSc6
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
58	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
g	g	kA
činí	činit	k5eAaImIp3nS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
objem	objem	k1gInSc4
kolem	kolem	k7c2
30	#num#	k4
ml.	ml.	kA
Obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
11	#num#	k4
%	%	kIx~
sušiny	sušina	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
tvoří	tvořit	k5eAaImIp3nS
z	z	k7c2
92	#num#	k4
%	%	kIx~
bílkoviny	bílkovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
mezi	mezi	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
bílkovinami	bílkovina	k1gFnPc7
je	být	k5eAaImIp3nS
asi	asi	k9
8	#num#	k4
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
bílek	bílek	k1gInSc1
lze	lze	k6eAd1
pokládat	pokládat	k5eAaImF
za	za	k7c4
vodný	vodný	k2eAgInSc4d1
roztok	roztok	k1gInSc4
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bílku	bílek	k1gInSc6
lze	lze	k6eAd1
detekovat	detekovat	k5eAaImF
kolem	kolem	k7c2
40	#num#	k4
bílkovin	bílkovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neproteinová	proteinový	k2eNgFnSc1d1
sušina	sušina	k1gFnSc1
(	(	kIx(
<g/>
kolem	kolem	k7c2
400	#num#	k4
µ	µ	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
zhruba	zhruba	k6eAd1
z	z	k7c2
poloviny	polovina	k1gFnSc2
sacharid	sacharid	k1gInSc1
a	a	k8xC
minerálními	minerální	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílek	Bílek	k1gMnSc1
čerstvého	čerstvý	k2eAgNnSc2d1
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
má	mít	k5eAaImIp3nS
pH	ph	kA
7,6	7,6	k4
a	a	k8xC
po	po	k7c6
snesení	snesení	k1gNnSc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
na	na	k7c4
pH	ph	kA
9,4	9,4	k4
<g/>
–	–	k?
<g/>
9,6	9,6	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koagulace	koagulace	k1gFnSc1
bílku	bílek	k1gInSc2
nastává	nastávat	k5eAaImIp3nS
při	při	k7c6
60	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Bod	bod	k1gInSc1
mrazu	mráz	k1gInSc2
volného	volný	k2eAgInSc2d1
bílku	bílek	k1gInSc2
je	být	k5eAaImIp3nS
–	–	k?
<g/>
0,45	0,45	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgNnSc1d1
strukturální	strukturální	k2eAgNnSc1d1
složení	složení	k1gNnSc1
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
bílkové	bílkový	k2eAgInPc1d1
vazy	vaz	k1gInPc1
<g/>
,	,	kIx,
chalázy	chaláza	k1gFnPc1
<g/>
,	,	kIx,
chalázová	chalázový	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
řídký	řídký	k2eAgInSc4d1
a	a	k8xC
hustý	hustý	k2eAgInSc4d1
bílek	bílek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koloidní	koloidní	k2eAgInSc1d1
roztok	roztok	k1gInSc1
bílkovin	bílkovina	k1gFnPc2
bílku	bílek	k1gInSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
není	být	k5eNaImIp3nS
homogenní	homogenní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukládá	ukládat	k5eAaImIp3nS
se	se	k3xPyFc4
kolem	kolem	k7c2
žloutkové	žloutkový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
ve	v	k7c6
4	#num#	k4
vrstvách	vrstva	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1
hustý	hustý	k2eAgInSc1d1
(	(	kIx(
<g/>
tuhý	tuhý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chalázový	chalázový	k2eAgInSc1d1
bílek	bílek	k1gInSc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
84	#num#	k4
%	%	kIx~
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představuje	představovat	k5eAaImIp3nS
asi	asi	k9
3	#num#	k4
%	%	kIx~
z	z	k7c2
celkového	celkový	k2eAgNnSc2d1
množství	množství	k1gNnSc2
bílku	bílek	k1gInSc2
ve	v	k7c6
vejci	vejce	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
utvářen	utvářet	k5eAaImNgInS,k5eAaPmNgInS
vakovitě	vakovitě	k6eAd1
<g/>
,	,	kIx,
obaluje	obalovat	k5eAaImIp3nS
žloutek	žloutek	k1gInSc1
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
chalázy	chaláza	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chalázy	Chaláza	k1gFnPc1
(	(	kIx(
<g/>
poutka	poutko	k1gNnPc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
spirálovité	spirálovitý	k2eAgInPc4d1
bílkové	bílkový	k2eAgInPc4d1
provazce	provazec	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vznikají	vznikat	k5eAaImIp3nP
v	v	k7c6
chalázové	chalázový	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
z	z	k7c2
vnitřního	vnitřní	k2eAgInSc2d1
hustého	hustý	k2eAgInSc2d1
bílku	bílek	k1gInSc2
a	a	k8xC
přisedají	přisedat	k5eAaImIp3nP
dvěma	dva	k4xCgInPc7
vazy	vaz	k1gInPc7
ve	v	k7c6
vnější	vnější	k2eAgFnSc6d1
vrstvě	vrstva	k1gFnSc6
hustého	hustý	k2eAgInSc2d1
bílku	bílek	k1gInSc2
na	na	k7c6
ostrém	ostrý	k2eAgInSc6d1
a	a	k8xC
tupém	tupý	k2eAgInSc6d1
konci	konec	k1gInSc6
ke	k	k7c3
skořápce	skořápka	k1gFnSc3
vejce	vejce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napomáhají	napomáhat	k5eAaImIp3nP,k5eAaBmIp3nP
udržovat	udržovat	k5eAaImF
žloutek	žloutek	k1gInSc4
ve	v	k7c6
středu	střed	k1gInSc6
vejce	vejce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1
řídký	řídký	k2eAgInSc1d1
bílek	bílek	k1gInSc1
představuje	představovat	k5eAaImIp3nS
16	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
%	%	kIx~
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
bílku	bílek	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
5	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
ml	ml	kA
</s>
<s>
Vnější	vnější	k2eAgInSc1d1
hustý	hustý	k2eAgInSc1d1
(	(	kIx(
<g/>
tuhý	tuhý	k2eAgInSc1d1
<g/>
)	)	kIx)
bílek	bílek	k1gInSc1
představuje	představovat	k5eAaImIp3nS
57	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
%	%	kIx~
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
bílku	bílek	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
ml.	ml.	kA
</s>
<s>
Vnější	vnější	k2eAgInSc1d1
řídký	řídký	k2eAgInSc1d1
bílek	bílek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
vody	voda	k1gFnSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
19	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
%	%	kIx~
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
bílku	bílek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1
vaječného	vaječný	k2eAgInSc2d1
bílku	bílek	k1gInSc2
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1
bílku	bílek	k1gInSc2
jsou	být	k5eAaImIp3nP
plnohodnotné	plnohodnotný	k2eAgInPc1d1
pro	pro	k7c4
obsah	obsah	k1gInSc4
esenciálních	esenciální	k2eAgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
stravitelnost	stravitelnost	k1gFnSc4
u	u	k7c2
člověka	člověk	k1gMnSc2
je	být	k5eAaImIp3nS
96	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladba	skladba	k1gFnSc1
a	a	k8xC
obsah	obsah	k1gInSc1
nepostradatelných	postradatelný	k2eNgFnPc2d1
aminokyselin	aminokyselina	k1gFnPc2
ve	v	k7c6
vaječném	vaječný	k2eAgInSc6d1
bílku	bílek	k1gInSc6
je	být	k5eAaImIp3nS
bohatší	bohatý	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
mase	maso	k1gNnSc6
a	a	k8xC
mléce	mléko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgFnPc4d3
bílkoviny	bílkovina	k1gFnPc4
vaječného	vaječný	k2eAgInSc2d1
bílku	bílek	k1gInSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Ovoalbumin	Ovoalbumin	k1gInSc4
je	být	k5eAaImIp3nS
zastoupen	zastoupit	k5eAaPmNgMnS
v	v	k7c6
sušině	sušina	k1gFnSc6
bílku	bílek	k1gInSc2
v	v	k7c6
největším	veliký	k2eAgNnSc6d3
množství	množství	k1gNnSc6
(	(	kIx(
<g/>
54	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
nejhodnotnější	hodnotný	k2eAgFnSc4d3
bílkovinu	bílkovina	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
esenciální	esenciální	k2eAgFnPc4d1
aminokyseliny	aminokyselina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
mol	mol	k1gInSc1
<g/>
.	.	kIx.
<g/>
hm	hm	k?
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
kolem	kolem	k7c2
46.000	46.000	k4
<g/>
,	,	kIx,
izoelektrický	izoelektrický	k2eAgInSc1d1
bod	bod	k1gInSc1
leží	ležet	k5eAaImIp3nS
mezi	mezi	k7c7
pH	ph	kA
4,5	4,5	k4
<g/>
–	–	k?
<g/>
4,8	4,8	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
kolem	kolem	k7c2
3	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovlivňuje	ovlivňovat	k5eAaImIp3nS
koagulaci	koagulace	k1gFnSc4
bílku	bílek	k1gInSc2
při	při	k7c6
vaření	vaření	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denaturuje	denaturovat	k5eAaBmIp3nS
při	při	k7c6
teplotě	teplota	k1gFnSc6
64	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Ovotransferin	Ovotransferin	k1gInSc1
(	(	kIx(
<g/>
konalbumin	konalbumin	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
glykoprotein	glykoprotein	k1gMnSc1
o	o	k7c4
mol	mol	k1gInSc4
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
76.600	76.600	k4
<g/>
–	–	k?
<g/>
86.000	86.000	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
izoelektrický	izoelektrický	k2eAgInSc1d1
bod	bod	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
hodnot	hodnota	k1gFnPc2
pH	ph	kA
5,8	5,8	k4
<g/>
–	–	k?
<g/>
6,6	6,6	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
12	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
denaturuje	denaturovat	k5eAaBmIp3nS
při	při	k7c6
teplotě	teplota	k1gFnSc6
57	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Obsahuje	obsahovat	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
sacharidů	sacharid	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
2,2	2,2	k4
%	%	kIx~
<g/>
)	)	kIx)
–	–	k?
hexózu	hexóza	k1gFnSc4
(	(	kIx(
<g/>
0,8	0,8	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
hexózoamin	hexózoamin	k1gInSc1
(	(	kIx(
<g/>
1,4	1,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Váže	vázat	k5eAaImIp3nS
kovové	kovový	k2eAgInPc4d1
ionty	ion	k1gInPc4
(	(	kIx(
<g/>
Fe	Fe	k1gMnSc1
<g/>
,	,	kIx,
Cu	Cu	k1gMnSc1
<g/>
,	,	kIx,
Mn	Mn	k1gMnSc1
<g/>
,	,	kIx,
Zn	zn	kA
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
baktericidní	baktericidní	k2eAgInSc4d1
účinek	účinek	k1gInSc4
(	(	kIx(
<g/>
konkuruje	konkurovat	k5eAaImIp3nS
bakteriálním	bakteriální	k2eAgInPc3d1
enzymům	enzym	k1gInPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
plasmatického	plasmatický	k2eAgInSc2d1
transferinu	transferin	k1gInSc2
neobsahuje	obsahovat	k5eNaImIp3nS
kyselinu	kyselina	k1gFnSc4
sialovou	sialový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Ovomukoid	Ovomukoid	k1gInSc1
je	být	k5eAaImIp3nS
termostabilní	termostabilní	k2eAgInSc1d1
glykoprotein	glykoprotein	k1gInSc1
o	o	k7c4
mol	mol	k1gInSc4
<g/>
.	.	kIx.
hm	hm	k?
28.000	28.000	k4
<g/>
,	,	kIx,
pH	ph	kA
3,9	3,9	k4
<g/>
–	–	k?
<g/>
4,3	4,3	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
11	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
22	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
%	%	kIx~
glukózoaminu	glukózoamin	k1gInSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
%	%	kIx~
manózy	manóza	k1gFnSc2
<g/>
,	,	kIx,
1,5	1,5	k4
%	%	kIx~
galaktózy	galaktóza	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
0,4	0,4	k4
<g/>
–	–	k?
<g/>
4,0	4,0	k4
%	%	kIx~
kyseliny	kyselina	k1gFnSc2
sialové	sialový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inhibuje	inhibovat	k5eAaBmIp3nS
proteázy	proteáza	k1gFnPc4
(	(	kIx(
<g/>
ovomukoid	ovomukoid	k1gInSc1
kura	kur	k1gMnSc2
<g/>
,	,	kIx,
husy	husa	k1gFnSc2
a	a	k8xC
křepelky	křepelka	k1gFnSc2
inhibuje	inhibovat	k5eAaBmIp3nS
trypsin	trypsin	k1gInSc1
<g/>
;	;	kIx,
ovomukoid	ovomukoid	k1gInSc1
krůt	krůta	k1gFnPc2
<g/>
,	,	kIx,
bažanta	bažant	k1gMnSc2
<g/>
,	,	kIx,
perličky	perlička	k1gFnSc2
a	a	k8xC
kachny	kachna	k1gFnSc2
inhibuje	inhibovat	k5eAaBmIp3nS
trypsin	trypsin	k1gInSc1
i	i	k8xC
chymotrypsin	chymotrypsin	k1gInSc1
<g/>
;	;	kIx,
ovomukoid	ovomukoid	k1gInSc1
kura	kur	k1gMnSc2
inaktivuje	inaktivovat	k5eAaBmIp3nS
trypsin	trypsin	k1gInSc4
ovčí	ovčí	k2eAgInSc4d1
<g/>
,	,	kIx,
bovinní	bovinní	k2eAgInSc4d1
a	a	k8xC
prasečí	prasečí	k2eAgInSc4d1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
ale	ale	k9
humánní	humánní	k2eAgMnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ovoglobuliny	Ovoglobulin	k2eAgInPc1d1
(	(	kIx(
<g/>
G	G	kA
<g/>
2	#num#	k4
a	a	k8xC
G	G	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
představují	představovat	k5eAaImIp3nP
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
dohromady	dohromady	k6eAd1
8	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mol	mol	k1gInSc1
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
kolem	kolem	k7c2
36	#num#	k4
<g/>
–	–	k?
<g/>
45.000	45.000	k4
<g/>
,	,	kIx,
pH	ph	kA
5,5	5,5	k4
(	(	kIx(
<g/>
G	G	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
5,8	5,8	k4
(	(	kIx(
<g/>
G	G	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
na	na	k7c4
pěnění	pěnění	k1gNnSc4
bílku	bílek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denaturují	denaturovat	k5eAaBmIp3nP
při	při	k7c6
teplotě	teplota	k1gFnSc6
80	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Ovoglobulin	Ovoglobulin	k2eAgMnSc1d1
G1	G1	k1gMnSc1
–	–	k?
viz	vidět	k5eAaImRp2nS
lysozym	lysozym	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Lysozym	Lysozym	k1gInSc1
je	být	k5eAaImIp3nS
termostabilní	termostabilní	k2eAgInSc1d1
protein	protein	k1gInSc1
s	s	k7c7
enzymatickými	enzymatický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
a	a	k8xC
antibakteriálním	antibakteriální	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
o	o	k7c4
mol	mol	k1gInSc4
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
14,300	14,300	k4
<g/>
–	–	k?
<g/>
17,000	17,000	k4
<g/>
,	,	kIx,
pH	ph	kA
10,5	10,5	k4
<g/>
–	–	k?
<g/>
11,0	11,0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
3,4	3,4	k4
<g/>
–	–	k?
<g/>
3,5	3,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antibakteriální	antibakteriální	k2eAgInSc4d1
účinek	účinek	k1gInSc4
lysozymu	lysozym	k1gInSc2
rozpoznal	rozpoznat	k5eAaPmAgMnS
Alexander	Alexandra	k1gFnPc2
Fleming	Fleming	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lysozym	Lysozym	k1gInSc1
rozrušuje	rozrušovat	k5eAaImIp3nS
základní	základní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
buněčné	buněčný	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
bakterií	bakterie	k1gFnPc2
–	–	k?
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS
beta-	beta-	k?
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
-D-glykozidickou	-D-glykozidický	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
mezi	mezi	k7c7
N-acetylmuramovou	N-acetylmuramový	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
a	a	k8xC
N-acetylglukózoaminem	N-acetylglukózoamin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vlastně	vlastně	k9
beta-	beta-	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
-D-glukózaminidázou	-D-glukózaminidáza	k1gFnSc7
(	(	kIx(
<g/>
muraminidázou	muraminidáza	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lysozym	Lysozym	k1gInSc1
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
totožný	totožný	k2eAgInSc1d1
s	s	k7c7
ovoglobulinem	ovoglobulin	k1gInSc7
G	G	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
některým	některý	k3yIgInPc3
mikroorganismům	mikroorganismus	k1gInPc3
se	se	k3xPyFc4
lysozym	lysozym	k1gInSc1
chová	chovat	k5eAaImIp3nS
baktericidně	baktericidně	k6eAd1
<g/>
,	,	kIx,
k	k	k7c3
jiným	jiný	k1gMnPc3
bakteriostaticky	bakteriostaticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
G	G	kA
<g/>
+	+	kIx~
bakterie	bakterie	k1gFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c4
lytické	lytický	k2eAgNnSc4d1
působení	působení	k1gNnSc4
lysozymu	lysozym	k1gInSc2
citlivější	citlivý	k2eAgFnSc1d2
než	než	k8xS
plísně	plíseň	k1gFnPc1
a	a	k8xC
kvasinky	kvasinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
nejen	nejen	k6eAd1
ve	v	k7c6
vaječném	vaječný	k2eAgInSc6d1
bílku	bílek	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
některých	některý	k3yIgInPc6
živočišných	živočišný	k2eAgInPc6d1
sekretech	sekret	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
slzách	slza	k1gFnPc6
<g/>
,	,	kIx,
slinách	slina	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slezině	slezina	k1gFnSc6
<g/>
,	,	kIx,
ledvinách	ledvina	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
krevním	krevní	k2eAgNnSc6d1
séru	sérum	k1gNnSc6
<g/>
,	,	kIx,
mateřském	mateřský	k2eAgNnSc6d1
mléce	mléko	k1gNnSc6
<g/>
,	,	kIx,
jikrách	jikra	k1gFnPc6
některých	některý	k3yIgFnPc2
ryb	ryba	k1gFnPc2
a	a	k8xC
u	u	k7c2
bezobratlých	bezobratlý	k2eAgFnPc2d1
a	a	k8xC
některých	některý	k3yIgFnPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpodrobněji	podrobně	k6eAd3
je	být	k5eAaImIp3nS
prostudován	prostudován	k2eAgInSc1d1
lysozym	lysozym	k1gInSc1
z	z	k7c2
bílku	bílek	k1gInSc2
slepičích	slepičí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
sekvence	sekvence	k1gFnSc1
aminokyselin	aminokyselina	k1gFnPc2
i	i	k9
terciární	terciární	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
molekuly	molekula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lysozymy	Lysozym	k1gInPc4
jiného	jiný	k2eAgInSc2d1
původu	původ	k1gInSc2
mají	mít	k5eAaImIp3nP
při	při	k7c6
kvalitativně	kvalitativně	k6eAd1
stejné	stejný	k2eAgFnSc6d1
biologické	biologický	k2eAgFnSc6d1
aktivitě	aktivita	k1gFnSc6
jinou	jiný	k2eAgFnSc4d1
primární	primární	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tepelná	tepelný	k2eAgFnSc1d1
inaktivace	inaktivace	k1gFnSc1
lysozymu	lysozym	k1gInSc2
závisí	záviset	k5eAaImIp3nS
nejen	nejen	k6eAd1
na	na	k7c6
teplotě	teplota	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
pH	ph	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
kyselém	kyselé	k1gNnSc6
pH	ph	kA
je	být	k5eAaImIp3nS
stálý	stálý	k2eAgInSc1d1
<g/>
,	,	kIx,
při	při	k7c6
alkalickém	alkalický	k2eAgMnSc6d1
pH	ph	kA
nestabilní	stabilní	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahříváním	zahřívání	k1gNnSc7
při	při	k7c6
teplotě	teplota	k1gFnSc6
62,5	62,5	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
pH	ph	kA
>	>	kIx)
<g/>
7,0	7,0	k4
se	se	k3xPyFc4
lysozym	lysozym	k1gInSc1
inaktivuje	inaktivovat	k5eAaBmIp3nS
za	za	k7c4
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
inaktivace	inaktivace	k1gFnPc4
probíhá	probíhat	k5eAaImIp3nS
i	i	k9
při	při	k7c6
smíchání	smíchání	k1gNnSc6
se	s	k7c7
žloutkem	žloutek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
vaječná	vaječný	k2eAgFnSc1d1
směs	směs	k1gFnSc1
podléhá	podléhat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
rychlejšímu	rychlý	k2eAgInSc3d2
mikrobiálnímu	mikrobiální	k2eAgInSc3d1
rozkladu	rozklad	k1gInSc3
než	než	k8xS
samotný	samotný	k2eAgInSc1d1
bílek	bílek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dlouhodobějším	dlouhodobý	k2eAgNnSc6d2
skladování	skladování	k1gNnSc6
vajec	vejce	k1gNnPc2
ztrácí	ztrácet	k5eAaImIp3nS
lysozym	lysozym	k1gInSc1
svoje	svůj	k3xOyFgFnPc4
antibakteriální	antibakteriální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinek	účinek	k1gInSc4
lysozymů	lysozym	k1gMnPc2
je	být	k5eAaImIp3nS
inhibován	inhibovat	k5eAaBmNgInS
heparinem	heparin	k1gInSc7
<g/>
,	,	kIx,
pouzdernými	pouzderný	k2eAgInPc7d1
polysacharidy	polysacharid	k1gInPc7
<g/>
,	,	kIx,
glutamylpolypeptidem	glutamylpolypeptid	k1gMnSc7
a	a	k8xC
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgNnSc3
protilátkami	protilátka	k1gFnPc7
je	být	k5eAaImIp3nS
účinek	účinek	k1gInSc1
lysozymu	lysozym	k1gInSc2
stupňován	stupňován	k2eAgMnSc1d1
(	(	kIx(
<g/>
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baktericidní	baktericidní	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
bílku	bílek	k1gInSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
u	u	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
i	i	k8xC
jednotlivců	jednotlivec	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
jejich	jejich	k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
lysozymu	lysozym	k1gInSc2
ve	v	k7c6
vejci	vejce	k1gNnSc6
je	být	k5eAaImIp3nS
určován	určovat	k5eAaImNgInS
geneticky	geneticky	k6eAd1
(	(	kIx(
<g/>
Wilcox	Wilcox	k1gInSc1
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ovomucin	Ovomucin	k1gInSc1
je	být	k5eAaImIp3nS
nerozpustný	rozpustný	k2eNgInSc4d1
kyselý	kyselý	k2eAgInSc4d1
glykoprotein	glykoprotein	k1gInSc4
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
19	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
1,5	1,5	k4
<g/>
–	–	k?
<g/>
2,9	2,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
v	v	k7c6
tuhém	tuhý	k2eAgInSc6d1
a	a	k8xC
chalázovém	chalázový	k2eAgInSc6d1
bílku	bílek	k1gInSc6
a	a	k8xC
membráně	membrána	k1gFnSc6
žloutku	žloutek	k1gInSc2
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
komplex	komplex	k1gInSc1
s	s	k7c7
lysozymem	lysozym	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
umožňuje	umožňovat	k5eAaImIp3nS
stabilitu	stabilita	k1gFnSc4
tuhého	tuhý	k2eAgInSc2d1
bílku	bílek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inhibuje	inhibovat	k5eAaBmIp3nS
virovou	virový	k2eAgFnSc4d1
hemaglutinaci	hemaglutinace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denaturuje	denaturovat	k5eAaBmIp3nS
při	při	k7c6
75	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Flavoprotein	Flavoprotein	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
s	s	k7c7
riboflavinem	riboflavin	k1gInSc7
(	(	kIx(
<g/>
vit	vit	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
poměrně	poměrně	k6eAd1
stabilní	stabilní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
biologická	biologický	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
pravděpodobně	pravděpodobně	k6eAd1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
transportu	transport	k1gInSc6
riboflavinu	riboflavin	k1gInSc2
do	do	k7c2
vyvíjejícího	vyvíjející	k2eAgMnSc2d1
se	se	k3xPyFc4
embrya	embryo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškerý	veškerý	k3xTgInSc4
riboflavin	riboflavin	k1gInSc4
v	v	k7c6
bílku	bílek	k1gInSc6
je	být	k5eAaImIp3nS
vázán	vázat	k5eAaImNgInS
flavoproteinem	flavoprotein	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,8	0,8	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
asi	asi	k9
14	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc4
mol	mol	k1gMnSc1
<g/>
.	.	kIx.
<g/>
hm	hm	k?
<g/>
.	.	kIx.
je	být	k5eAaImIp3nS
32	#num#	k4
<g/>
–	–	k?
<g/>
36,000	36,000	k4
<g/>
,	,	kIx,
pH	ph	kA
3,9	3,9	k4
<g/>
–	–	k?
<g/>
4,1	4,1	k4
<g/>
.	.	kIx.
</s>
<s>
Ovomakroglobulin	Ovomakroglobulin	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
výrazné	výrazný	k2eAgFnPc4d1
imunogenní	imunogenní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,5	0,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
9	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
<g/>
,	,	kIx,
molekulová	molekulový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
760	#num#	k4
<g/>
–	–	k?
<g/>
900	#num#	k4
kDa	kDa	k?
a	a	k8xC
pH	ph	kA
4,5	4,5	k4
<g/>
–	–	k?
<g/>
4,7	4,7	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
přítomen	přítomen	k2eAgMnSc1d1
v	v	k7c6
krůtích	krůtí	k2eAgNnPc6d1
vejcích	vejce	k1gNnPc6
a	a	k8xC
nepravidelně	pravidelně	k6eNd1
v	v	k7c6
křepelčích	křepelčí	k2eAgNnPc6d1
vejcích	vejce	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Ovoglykoprotein	Ovoglykoprotein	k1gInSc1
je	být	k5eAaImIp3nS
kyselý	kyselý	k2eAgInSc4d1
glykoprotein	glykoprotein	k1gInSc4
o	o	k7c4
mol	mol	k1gInSc4
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
24	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
pH	ph	kA
3,9	3,9	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
16	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
a	a	k8xC
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,5	0,5	k4
<g/>
–	–	k?
<g/>
1,0	1,0	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Ovoinhibitor	Ovoinhibitor	k1gInSc1
je	být	k5eAaImIp3nS
glykoprotein	glykoprotein	k1gInSc4
o	o	k7c4
mol	mol	k1gInSc4
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
44	#num#	k4
<g/>
–	–	k?
<g/>
49,000	49,000	k4
<g/>
,	,	kIx,
pH	ph	kA
5,1	5,1	k4
<g/>
–	–	k?
<g/>
5,2	5,2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,1	0,1	k4
<g/>
–	–	k?
<g/>
1,5	1,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
6	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
(	(	kIx(
<g/>
3,5	3,5	k4
%	%	kIx~
hexózy	hexóza	k1gFnSc2
a	a	k8xC
2,7	2,7	k4
%	%	kIx~
hexózoaminu	hexózoamin	k1gInSc2
s	s	k7c7
malým	malý	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
kyseliny	kyselina	k1gFnSc2
sialové	sialový	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inhibuje	inhibovat	k5eAaBmIp3nS
proteázy	proteáza	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
trypsinu	trypsin	k1gInSc2
a	a	k8xC
chymotrypsinu	chymotrypsina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Avidin	Avidin	k2eAgMnSc1d1
má	mít	k5eAaImIp3nS
antibakteriální	antibakteriální	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Váže	vázat	k5eAaImIp3nS
biotin	biotin	k1gInSc4
ve	v	k7c4
stabilní	stabilní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
zabraňuje	zabraňovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc3
biologickému	biologický	k2eAgNnSc3d1
využití	využití	k1gNnSc3
jako	jako	k8xS,k8xC
vitamínu	vitamín	k1gInSc3
či	či	k8xC
koenzymu	koenzym	k1gInSc3
bakteriemi	bakterie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,05	0,05	k4
%	%	kIx~
<g/>
;	;	kIx,
pH	ph	kA
je	být	k5eAaImIp3nS
9,5	9,5	k4
<g/>
–	–	k?
<g/>
10,0	10,0	k4
a	a	k8xC
mol	mol	k1gMnSc1
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
68,300	68,300	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
8	#num#	k4
%	%	kIx~
sacharidů	sacharid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Inhibitor	inhibitor	k1gInSc1
papainu	papain	k1gInSc2
má	mít	k5eAaImIp3nS
mol	mol	k1gInSc1
<g/>
.	.	kIx.
hm	hm	k?
<g/>
.	.	kIx.
12.700	12.700	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
sušiny	sušina	k1gFnSc2
bílku	bílek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
0,1	0,1	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inhibuje	inhibovat	k5eAaBmIp3nS
proteázy	proteáza	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
papainu	papain	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
popsaných	popsaný	k2eAgInPc2d1
proteinů	protein	k1gInPc2
s	s	k7c7
enzymatickými	enzymatický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
obsahuje	obsahovat	k5eAaImIp3nS
vaječný	vaječný	k2eAgInSc4d1
bílek	bílek	k1gInSc4
také	také	k9
glykozidázy	glykozidáza	k1gFnSc2
<g/>
,	,	kIx,
katalázy	kataláza	k1gFnSc2
<g/>
,	,	kIx,
peptidázy	peptidáza	k1gFnSc2
a	a	k8xC
esterázy	esteráza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
sacharidů	sacharid	k1gInPc2
je	být	k5eAaImIp3nS
přítomna	přítomen	k2eAgFnSc1d1
pouze	pouze	k6eAd1
volná	volný	k2eAgFnSc1d1
glukóza	glukóza	k1gFnSc1
(	(	kIx(
<g/>
0,4	0,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgMnPc1d1
<g/>
,	,	kIx,
především	především	k6eAd1
manóza	manóza	k1gFnSc1
a	a	k8xC
galaktóza	galaktóza	k1gFnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vázány	vázat	k5eAaImNgFnP
jako	jako	k9
glykoproteiny	glykoproteina	k1gFnPc1
(	(	kIx(
<g/>
0,5	0,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lipidů	lipid	k1gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
vaječný	vaječný	k2eAgInSc1d1
bílek	bílek	k1gInSc1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
(	(	kIx(
<g/>
0,03	0,03	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Sníh	sníh	k1gInSc1
z	z	k7c2
bílků	bílek	k1gInPc2
</s>
<s>
Kulinářství	Kulinářství	k1gNnSc1
</s>
<s>
potírání	potírání	k1gNnSc1
pečiva	pečivo	k1gNnSc2
–	–	k?
např.	např.	kA
perníku	perník	k1gInSc2
pro	pro	k7c4
lesklý	lesklý	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
</s>
<s>
výroba	výroba	k1gFnSc1
sněhu	sníh	k1gInSc2
–	–	k?
šleháním	šlehání	k1gNnSc7
vzniká	vznikat	k5eAaImIp3nS
pěna	pěna	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
např.	např.	kA
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
cukrářských	cukrářský	k2eAgFnPc2d1
pusinek	pusinka	k1gFnPc2
</s>
<s>
čištění	čištění	k1gNnSc1
roztoku	roztok	k1gInSc2
–	–	k?
např.	např.	kA
vývaru	vývar	k1gInSc2
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
výroba	výroba	k1gFnSc1
albuminového	albuminový	k2eAgInSc2d1
papíru	papír	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
fotografie	fotografia	k1gFnSc2
</s>
<s>
použití	použití	k1gNnSc1
jako	jako	k8xC,k8xS
lepidlo	lepidlo	k1gNnSc1
–	–	k?
např.	např.	kA
při	při	k7c6
zlacení	zlacení	k1gNnSc6
v	v	k7c6
knihařství	knihařství	k1gNnSc6
a	a	k8xC
u	u	k7c2
iluminací	iluminace	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ROČEK	Roček	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
obratlovců	obratlovec	k1gMnPc2
:	:	kIx,
evoluce	evoluce	k1gFnSc1
<g/>
,	,	kIx,
fylogeneze	fylogeneze	k1gFnSc1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
858	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠATAVA	ŠATAVA	kA
<g/>
,	,	kIx,
M.	M.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chov	chov	k1gInSc1
drůbeže	drůbež	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
</s>
<s>
STURKIE	STURKIE	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
D.	D.	kA
Avian	Avian	k1gMnSc1
Physiology	Physiolog	k1gMnPc7
<g/>
.	.	kIx.
5	#num#	k4
<g/>
th	th	k?
Ed	Ed	k1gMnSc1
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Elsevier	Elsevier	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
704	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
747605	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Žloutek	žloutek	k1gInSc1
</s>
<s>
Sníh	sníh	k1gInSc1
(	(	kIx(
<g/>
gastronomie	gastronomie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bílek	bílek	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
bílek	bílek	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
