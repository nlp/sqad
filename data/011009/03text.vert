<p>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
byl	být	k5eAaImAgInS	být
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
(	(	kIx(	(
<g/>
průkopnický	průkopnický	k2eAgInSc4d1	průkopnický
<g/>
,	,	kIx,	,
objevný	objevný	k2eAgInSc4d1	objevný
<g/>
)	)	kIx)	)
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
směr	směr	k1gInSc4	směr
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
odmítání	odmítání	k1gNnSc1	odmítání
všech	všecek	k3xTgFnPc2	všecek
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
futurismus	futurismus	k1gInSc1	futurismus
stal	stát	k5eAaPmAgInS	stát
hnutím	hnutí	k1gNnSc7	hnutí
odmítajícím	odmítající	k2eAgNnSc7d1	odmítající
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
ukázat	ukázat	k5eAaPmF	ukázat
moderní	moderní	k2eAgFnSc4d1	moderní
uspěchanou	uspěchaný	k2eAgFnSc4d1	uspěchaná
a	a	k8xC	a
rušnou	rušný	k2eAgFnSc4d1	rušná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nekritizovali	kritizovat	k5eNaImAgMnP	kritizovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
líbila	líbit	k5eAaImAgFnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
byla	být	k5eAaImAgFnS	být
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
jejich	jejich	k3xOp3gFnSc1	jejich
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
jednak	jednak	k8xC	jednak
zkratkovitostí	zkratkovitost	k1gFnSc7	zkratkovitost
a	a	k8xC	a
jednak	jednak	k8xC	jednak
proměnlivostí	proměnlivost	k1gFnSc7	proměnlivost
básnického	básnický	k2eAgInSc2d1	básnický
rytmu	rytmus	k1gInSc2	rytmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
nacionální	nacionální	k2eAgInSc1d1	nacionální
<g/>
,	,	kIx,	,
schvaloval	schvalovat	k5eAaImAgInS	schvalovat
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
možný	možný	k2eAgInSc4d1	možný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
očistit	očistit	k5eAaPmF	očistit
svět	svět	k1gInSc4	svět
od	od	k7c2	od
jeho	jeho	k3xOp3gInPc2	jeho
nešvarů	nešvar	k1gInPc2	nešvar
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
všeho	všecek	k3xTgNnSc2	všecek
starého	starý	k2eAgNnSc2d1	staré
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgMnSc1d1	populární
především	především	k9	především
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
konstruktivismus	konstruktivismus	k1gInSc4	konstruktivismus
a	a	k8xC	a
imažinismus	imažinismus	k1gInSc4	imažinismus
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
patrný	patrný	k2eAgInSc1d1	patrný
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
i	i	k9	i
na	na	k7c4	na
surrealismus	surrealismus	k1gInSc4	surrealismus
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
uměleckých	umělecký	k2eAgInPc2d1	umělecký
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
lze	lze	k6eAd1	lze
některé	některý	k3yIgInPc4	některý
trendy	trend	k1gInPc4	trend
vedoucí	vedoucí	k2eAgInPc4d1	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
futurismu	futurismus	k1gInSc2	futurismus
sledovat	sledovat	k5eAaImF	sledovat
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
futurismu	futurismus	k1gInSc2	futurismus
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgInSc1d1	považován
rok	rok	k1gInSc1	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vydal	vydat	k5eAaPmAgMnS	vydat
italský	italský	k2eAgMnSc1d1	italský
básník	básník	k1gMnSc1	básník
Filippo	Filippa	k1gFnSc5	Filippa
Tommaso	Tommasa	k1gFnSc5	Tommasa
Marinetti	Marinetť	k1gFnSc3	Marinetť
futuristický	futuristický	k2eAgInSc4d1	futuristický
manifest	manifest	k1gInSc4	manifest
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
otištěn	otisknout	k5eAaPmNgInS	otisknout
francouzsky	francouzsky	k6eAd1	francouzsky
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
listu	list	k1gInSc6	list
Le	Le	k1gFnSc2	Le
Figaro	Figara	k1gFnSc5	Figara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
manifestu	manifest	k1gInSc6	manifest
Marinetti	Marinetť	k1gFnSc2	Marinetť
shrnul	shrnout	k5eAaPmAgInS	shrnout
program	program	k1gInSc4	program
futurismu	futurismus	k1gInSc2	futurismus
do	do	k7c2	do
několika	několik	k4yIc2	několik
základních	základní	k2eAgInPc2d1	základní
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
krása	krása	k1gFnSc1	krása
neklidu	neklid	k1gInSc2	neklid
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
boje	boj	k1gInSc2	boj
poezie	poezie	k1gFnSc2	poezie
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
revolty	revolta	k1gFnSc2	revolta
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
zde	zde	k6eAd1	zde
prezentovali	prezentovat	k5eAaBmAgMnP	prezentovat
triumf	triumf	k1gInSc4	triumf
člověka	člověk	k1gMnSc2	člověk
nad	nad	k7c7	nad
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
vydali	vydat	k5eAaPmAgMnP	vydat
podobný	podobný	k2eAgInSc4d1	podobný
manifest	manifest	k1gInSc4	manifest
i	i	k9	i
malíři	malíř	k1gMnPc1	malíř
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
architekti	architekt	k1gMnPc1	architekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
futurismus	futurismus	k1gInSc1	futurismus
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velice	velice	k6eAd1	velice
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
odnože	odnož	k1gFnPc4	odnož
<g/>
:	:	kIx,	:
kubofuturismus	kubofuturismus	k1gInSc1	kubofuturismus
a	a	k8xC	a
egofuturismus	egofuturismus	k1gInSc1	egofuturismus
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
egofuturismus	egofuturismus	k1gInSc1	egofuturismus
neměl	mít	k5eNaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
kubofuturismus	kubofuturismus	k1gInSc1	kubofuturismus
značně	značně	k6eAd1	značně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
ruské	ruský	k2eAgNnSc4d1	ruské
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ideologicky	ideologicky	k6eAd1	ideologicky
byl	být	k5eAaImAgInS	být
futurismus	futurismus	k1gInSc1	futurismus
nejednotný	jednotný	k2eNgInSc1d1	nejednotný
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
připojovaly	připojovat	k5eAaImAgFnP	připojovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc1	všechen
politické	politický	k2eAgFnPc1d1	politická
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
levicové	levicový	k2eAgFnSc2d1	levicová
–	–	k?	–
anarchisté	anarchista	k1gMnPc1	anarchista
<g/>
,	,	kIx,	,
komunisté	komunista	k1gMnPc1	komunista
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc1d1	aktivní
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
časopisu	časopis	k1gInSc2	časopis
Roma	Rom	k1gMnSc2	Rom
Futurista	futurista	k1gMnSc1	futurista
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgMnPc1d1	vystupující
proti	proti	k7c3	proti
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3	Rakousku-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
Marinetti	Marinetti	k1gNnSc2	Marinetti
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
B.	B.	kA	B.
Mussolinim	Mussolinima	k1gFnPc2	Mussolinima
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
futuristů	futurista	k1gMnPc2	futurista
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
fašismu	fašismus	k1gInSc3	fašismus
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
fašismem	fašismus	k1gInSc7	fašismus
byla	být	k5eAaImAgFnS	být
dovršena	dovršit	k5eAaPmNgFnS	dovršit
po	po	k7c6	po
Italském	italský	k2eAgNnSc6d1	italské
obsazení	obsazení	k1gNnSc6	obsazení
Habeše	Habeš	k1gFnSc2	Habeš
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
futuristé	futurista	k1gMnPc1	futurista
nejen	nejen	k6eAd1	nejen
schvalovali	schvalovat	k5eAaImAgMnP	schvalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
básnicky	básnicky	k6eAd1	básnicky
oslavili	oslavit	k5eAaPmAgMnP	oslavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
futuristé	futurista	k1gMnPc1	futurista
teorii	teorie	k1gFnSc4	teorie
osvobozených	osvobozený	k2eAgNnPc2d1	osvobozené
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
hlásky	hláska	k1gFnSc2	hláska
<g/>
)	)	kIx)	)
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
nebo	nebo	k8xC	nebo
verši	verš	k1gInSc6	verš
byla	být	k5eAaImAgFnS	být
vytržena	vytržen	k2eAgFnSc1d1	vytržena
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
logické	logický	k2eAgFnPc1d1	logická
vazby	vazba	k1gFnPc1	vazba
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
syntaxe	syntax	k1gFnSc2	syntax
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
prosadil	prosadit	k5eAaPmAgInS	prosadit
i	i	k9	i
experimentální	experimentální	k2eAgInSc1d1	experimentální
futurismus	futurismus	k1gInSc1	futurismus
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
kubismem	kubismus	k1gInSc7	kubismus
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kubofuturismus	kubofuturismus	k1gInSc1	kubofuturismus
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
extrémním	extrémní	k2eAgInSc7d1	extrémní
pokusem	pokus	k1gInSc7	pokus
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Poesia	Poesia	k1gFnSc1	Poesia
Pentagrammata	Pentagramma	k1gNnPc1	Pentagramma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Francesca	Francesca	k1gMnSc1	Francesca
Canguilla	Canguillo	k1gNnSc2	Canguillo
–	–	k?	–
zasazoval	zasazovat	k5eAaImAgInS	zasazovat
osvobozená	osvobozený	k2eAgNnPc4d1	osvobozené
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
slabiky	slabika	k1gFnPc4	slabika
do	do	k7c2	do
notové	notový	k2eAgFnSc2d1	notová
osnovy	osnova	k1gFnSc2	osnova
a	a	k8xC	a
místo	místo	k7c2	místo
interpunkce	interpunkce	k1gFnSc2	interpunkce
používal	používat	k5eAaImAgInS	používat
hudební	hudební	k2eAgNnPc4d1	hudební
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
futurismu	futurismus	k1gInSc6	futurismus
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
(	(	kIx(	(
<g/>
především	především	k9	především
dynamika	dynamika	k1gFnSc1	dynamika
a	a	k8xC	a
důraz	důraz	k1gInSc1	důraz
na	na	k7c6	na
mládí	mládí	k1gNnSc6	mládí
<g/>
)	)	kIx)	)
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
dodnes	dodnes	k6eAd1	dodnes
ve	v	k7c6	v
sci-fi	scii	k1gFnSc6	sci-fi
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
cyberpunk	cyberpunk	k1gInSc1	cyberpunk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Světoví	světový	k2eAgMnPc1d1	světový
spisovatelé	spisovatel	k1gMnPc1	spisovatel
===	===	k?	===
</s>
</p>
<p>
<s>
Filippo	Filippa	k1gFnSc5	Filippa
Tommaso	Tommasa	k1gFnSc5	Tommasa
Marinetti	Marinetti	k1gNnPc7	Marinetti
–	–	k?	–
Ital	Ital	k1gMnSc1	Ital
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Majakovskij	Majakovskij	k1gMnSc1	Majakovskij
–	–	k?	–
Rus	Rus	k1gMnSc1	Rus
</s>
</p>
<p>
<s>
Velemir	Velemir	k1gInSc1	Velemir
Chlebnikov	Chlebnikov	k1gInSc1	Chlebnikov
–	–	k?	–
Rus	Rus	k1gFnSc1	Rus
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Viktor	Viktor	k1gMnSc1	Viktor
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Chlebnikov	Chlebnikov	k1gInSc1	Chlebnikov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fernando	Fernando	k6eAd1	Fernando
Pessoa	Pessoa	k1gMnSc1	Pessoa
–	–	k?	–
Portugalec	Portugalec	k1gMnSc1	Portugalec
</s>
</p>
<p>
<s>
===	===	k?	===
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
===	===	k?	===
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kostka	Kostka	k1gMnSc1	Kostka
Neumann	Neumann	k1gMnSc1	Neumann
</s>
</p>
<p>
<s>
==	==	k?	==
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
kubismu	kubismus	k1gInSc3	kubismus
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
dostat	dostat	k5eAaPmF	dostat
zobrazovaný	zobrazovaný	k2eAgInSc4d1	zobrazovaný
předmět	předmět	k1gInSc4	předmět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
pohyb	pohyb	k1gInSc4	pohyb
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
k	k	k7c3	k
abstrakci	abstrakce	k1gFnSc3	abstrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čeští	český	k2eAgMnPc1d1	český
představitelé	představitel	k1gMnPc1	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
–	–	k?	–
spojuje	spojovat	k5eAaImIp3nS	spojovat
futurismus	futurismus	k1gInSc1	futurismus
s	s	k7c7	s
kubismem	kubismus	k1gInSc7	kubismus
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Gutfreund	Gutfreund	k1gMnSc1	Gutfreund
–	–	k?	–
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
kubismus	kubismus	k1gInSc1	kubismus
</s>
</p>
<p>
<s>
Růžena	Růžena	k1gFnSc1	Růžena
Zátková	zátkový	k2eAgFnSc1d1	Zátková
–	–	k?	–
předzvěst	předzvěst	k1gFnSc1	předzvěst
informelu	informel	k1gInSc2	informel
a	a	k8xC	a
abstrakce	abstrakce	k1gFnSc1	abstrakce
</s>
</p>
<p>
<s>
===	===	k?	===
Světoví	světový	k2eAgMnPc1d1	světový
představitelé	představitel	k1gMnPc1	představitel
===	===	k?	===
</s>
</p>
<p>
<s>
Umberto	Umberta	k1gFnSc5	Umberta
Boccioni	Boccioň	k1gFnSc5	Boccioň
–	–	k?	–
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
manifestu	manifest	k1gInSc2	manifest
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Giacomo	Giacomo	k6eAd1	Giacomo
Balla	Balla	k1gFnSc1	Balla
</s>
</p>
<p>
<s>
Uberto	Uberta	k1gFnSc5	Uberta
Bonetti	Bonetť	k1gFnSc5	Bonetť
</s>
</p>
<p>
<s>
Gino	Gino	k6eAd1	Gino
Severini	Severin	k1gMnPc1	Severin
</s>
</p>
<p>
<s>
Carlo	Carlo	k1gNnSc1	Carlo
Carrà	Carrà	k1gFnSc2	Carrà
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Grosz	Grosz	k1gMnSc1	Grosz
</s>
</p>
<p>
<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
Malevič	Malevič	k1gMnSc1	Malevič
</s>
</p>
<p>
<s>
Luigi	Luig	k1gFnSc5	Luig
Russolo	Russola	k1gFnSc5	Russola
</s>
</p>
<p>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Duchamp	Duchamp	k1gMnSc1	Duchamp
–	–	k?	–
prvky	prvek	k1gInPc7	prvek
futurismu	futurismus	k1gInSc2	futurismus
<g/>
,	,	kIx,	,
dadaismu	dadaismus	k1gInSc2	dadaismus
<g/>
,	,	kIx,	,
kubismu	kubismus	k1gInSc2	kubismus
i	i	k8xC	i
surrealismu	surrealismus	k1gInSc2	surrealismus
</s>
</p>
<p>
<s>
Filippo	Filippa	k1gFnSc5	Filippa
Tommaso	Tommasa	k1gFnSc5	Tommasa
Marinetti	Marinetť	k1gFnPc1	Marinetť
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
se	se	k3xPyFc4	se
klonili	klonit	k5eAaImAgMnP	klonit
k	k	k7c3	k
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
využívat	využívat	k5eAaImF	využívat
nové	nový	k2eAgInPc4d1	nový
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
beton	beton	k1gInSc1	beton
<g/>
)	)	kIx)	)
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
kombinace	kombinace	k1gFnPc4	kombinace
sklo-kov	skloov	k1gInSc4	sklo-kov
<g/>
,	,	kIx,	,
beton-sklo	betonknout	k5eAaPmAgNnS	beton-sknout
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Elia	Elia	k1gMnSc1	Elia
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
prosazení	prosazení	k1gNnSc3	prosazení
futurismu	futurismus	k1gInSc2	futurismus
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
hudebního	hudební	k2eAgInSc2d1	hudební
futurismu	futurismus	k1gInSc2	futurismus
nebyly	být	k5eNaImAgInP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
cílů	cíl	k1gInPc2	cíl
celého	celý	k2eAgNnSc2d1	celé
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
futuristická	futuristický	k2eAgFnSc1d1	futuristická
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
směry	směr	k1gInPc1	směr
typu	typ	k1gInSc2	typ
techno	techno	k6eAd1	techno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
italských	italský	k2eAgMnPc2d1	italský
futuristů	futurista	k1gMnPc2	futurista
kolem	kolem	k7c2	kolem
F.	F.	kA	F.
T.	T.	kA	T.
Marinettiho	Marinetti	k1gMnSc2	Marinetti
reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
Manifest	manifest	k1gInSc4	manifest
futurismu	futurismus	k1gInSc2	futurismus
<g/>
,	,	kIx,	,
uveřejněný	uveřejněný	k2eAgInSc4d1	uveřejněný
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Figaru	Figar	k1gInSc6	Figar
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
manifest	manifest	k1gInSc1	manifest
F.	F.	kA	F.
B.	B.	kA	B.
Pratelly	Pratella	k1gMnSc2	Pratella
publikovaný	publikovaný	k2eAgInSc4d1	publikovaný
r.	r.	kA	r.
1913	[number]	k4	1913
v	v	k7c6	v
klavírním	klavírní	k2eAgInSc6d1	klavírní
výtahu	výtah	k1gInSc6	výtah
skladby	skladba	k1gFnSc2	skladba
Musica	Musica	k1gMnSc1	Musica
futuristica	futuristica	k1gMnSc1	futuristica
per	pero	k1gNnPc2	pero
orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
podnětem	podnět	k1gInSc7	podnět
k	k	k7c3	k
zásadnější	zásadní	k2eAgFnSc3d2	zásadnější
formulaci	formulace	k1gFnSc3	formulace
futuristické	futuristický	k2eAgFnSc2d1	futuristická
hudební	hudební	k2eAgFnSc2d1	hudební
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
byl	být	k5eAaImAgMnS	být
Russolův	Russolův	k2eAgInSc4d1	Russolův
otevřený	otevřený	k2eAgInSc4d1	otevřený
dopis	dopis	k1gInSc4	dopis
Umění	umění	k1gNnSc1	umění
hluku	hluk	k1gInSc2	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Luigi	Luig	k1gFnSc5	Luig
Russolo	Russola	k1gFnSc5	Russola
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
svou	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
ihned	ihned	k6eAd1	ihned
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Uggem	Ugg	k1gInSc7	Ugg
Piatim	Piatim	k1gMnSc1	Piatim
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
19	[number]	k4	19
nových	nový	k2eAgInPc2d1	nový
hlukových	hlukový	k2eAgInPc2d1	hlukový
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
nazvali	nazvat	k5eAaBmAgMnP	nazvat
je	on	k3xPp3gFnPc4	on
intonarumori	intonarumor	k1gFnPc4	intonarumor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
nástroje	nástroj	k1gInPc4	nástroj
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Russolo	Russola	k1gFnSc5	Russola
první	první	k4xOgFnSc2	první
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1913	[number]	k4	1913
v	v	k7c4	v
Teatro	Teatro	k1gNnSc4	Teatro
Storchi	Storch	k1gFnSc2	Storch
v	v	k7c6	v
Modeně	Modena	k1gFnSc6	Modena
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1914	[number]	k4	1914
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
Marinetti	Marinett	k1gMnPc1	Marinett
a	a	k8xC	a
Russolo	Russola	k1gFnSc5	Russola
koncert	koncert	k1gInSc4	koncert
futuristické	futuristický	k2eAgFnSc2d1	futuristická
hudby	hudba	k1gFnSc2	hudba
v	v	k7c4	v
Teatro	Teatro	k1gNnSc4	Teatro
dal	dát	k5eAaPmAgMnS	dát
Verme	Verm	k1gInSc5	Verm
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
využito	využít	k5eAaPmNgNnS	využít
všech	všecek	k3xTgInPc2	všecek
19	[number]	k4	19
intonarumori	intonarumori	k1gNnPc2	intonarumori
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
měl	mít	k5eAaImAgInS	mít
4	[number]	k4	4
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Procitnutí	procitnutí	k1gNnSc1	procitnutí
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Setkání	setkání	k1gNnSc1	setkání
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
Jídlo	jídlo	k1gNnSc1	jídlo
na	na	k7c6	na
hotelové	hotelový	k2eAgFnSc6d1	hotelová
terase	terasa	k1gFnSc6	terasa
<g/>
,	,	kIx,	,
Šarvátka	šarvátka	k1gFnSc1	šarvátka
v	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1921	[number]	k4	1921
v	v	k7c6	v
Théâtre	Théâtr	k1gInSc5	Théâtr
des	des	k1gNnSc3	des
Champs-Élysées	Champs-Élysées	k1gInSc4	Champs-Élysées
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
důležité	důležitý	k2eAgFnPc1d1	důležitá
3	[number]	k4	3
koncerty	koncert	k1gInPc7	koncert
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Šestky	šestka	k1gFnSc2	šestka
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckou	umělecký	k2eAgFnSc4d1	umělecká
úroveň	úroveň	k1gFnSc4	úroveň
Russolových	Russolový	k2eAgFnPc2d1	Russolový
skladeb	skladba	k1gFnPc2	skladba
nelze	lze	k6eNd1	lze
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
posoudit	posoudit	k5eAaPmF	posoudit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnSc1	jejich
autor	autor	k1gMnSc1	autor
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
malování	malování	k1gNnSc3	malování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
futuristického	futuristický	k2eAgInSc2d1	futuristický
orchestru	orchestr	k1gInSc2	orchestr
se	se	k3xPyFc4	se
již	již	k9	již
nikdo	nikdo	k3yNnSc1	nikdo
důsledně	důsledně	k6eAd1	důsledně
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Russolovo	Russolův	k2eAgNnSc1d1	Russolův
Umění	umění	k1gNnSc1	umění
hluku	hluk	k1gInSc2	hluk
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
bezprostřední	bezprostřední	k2eAgNnSc1d1	bezprostřední
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
musique	musiqu	k1gFnSc2	musiqu
concrè	concrè	k?	concrè
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferruccio	Ferruccio	k6eAd1	Ferruccio
Busoni	Buson	k1gMnPc1	Buson
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
futurismus	futurismus	k1gInSc1	futurismus
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
pohled	pohled	k1gInSc4	pohled
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
krásní	krásný	k2eAgMnPc1d1	krásný
perspektivní	perspektivní	k2eAgMnPc1d1	perspektivní
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
komerčnost	komerčnost	k1gFnSc4	komerčnost
a	a	k8xC	a
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
témat	téma	k1gNnPc2	téma
z	z	k7c2	z
nových	nový	k2eAgNnPc2d1	nové
prostředí	prostředí	k1gNnPc2	prostředí
(	(	kIx(	(
<g/>
továrny	továrna	k1gFnSc2	továrna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
reprezentován	reprezentován	k2eAgInSc4d1	reprezentován
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
projevilo	projevit	k5eAaPmAgNnS	projevit
především	především	k9	především
v	v	k7c4	v
manga	mango	k1gNnPc4	mango
<g/>
/	/	kIx~	/
<g/>
anime	anim	k1gInSc5	anim
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ukazována	ukazován	k2eAgFnSc1d1	ukazována
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
i	i	k9	i
oslavována	oslavován	k2eAgFnSc1d1	oslavována
dnešní	dnešní	k2eAgFnSc1d1	dnešní
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc1d1	budoucí
technika	technika	k1gFnSc1	technika
(	(	kIx(	(
<g/>
Šinja	Šinja	k1gFnSc1	Šinja
Cukamoto	Cukamota	k1gFnSc5	Cukamota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
</s>
</p>
<p>
<s>
Kubofuturismus	Kubofuturismus	k1gInSc1	Kubofuturismus
</s>
</p>
<p>
<s>
Egofuturismus	Egofuturismus	k1gInSc1	Egofuturismus
</s>
</p>
<p>
<s>
Art	Art	k?	Art
deco	deco	k6eAd1	deco
</s>
</p>
<p>
<s>
Estorick	Estorick	k6eAd1	Estorick
Collection	Collection	k1gInSc1	Collection
of	of	k?	of
Modern	Modern	k1gInSc1	Modern
Italian	Italian	k1gMnSc1	Italian
Art	Art	k1gMnSc1	Art
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
R.	R.	kA	R.
W.	W.	kA	W.
Nevinson	Nevinson	k1gMnSc1	Nevinson
</s>
</p>
