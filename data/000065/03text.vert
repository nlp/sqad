<s>
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
(	(	kIx(	(
<g/>
Some	Some	k1gFnSc1	Some
Like	Lik	k1gMnSc2	Lik
It	It	k1gMnSc2	It
Hot	hot	k0	hot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
napsal	napsat	k5eAaBmAgMnS	napsat
Billy	Bill	k1gMnPc4	Bill
Wilder	Wilder	k1gMnSc1	Wilder
společně	společně	k6eAd1	společně
s	s	k7c7	s
I.A.L.	I.A.L.	k1gMnSc7	I.A.L.
Diamondem	Diamond	k1gMnSc7	Diamond
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
německá	německý	k2eAgFnSc1d1	německá
fraška	fraška	k1gFnSc1	fraška
Fanfaren	Fanfarna	k1gFnPc2	Fanfarna
der	drát	k5eAaImRp2nS	drát
Liebe	Lieb	k1gMnSc5	Lieb
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Kurt	Kurt	k1gMnSc1	Kurt
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
proslul	proslout	k5eAaPmAgMnS	proslout
zejména	zejména	k9	zejména
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
větou	věta	k1gFnSc7	věta
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
jazzoví	jazzový	k2eAgMnPc1d1	jazzový
muzikanti	muzikant	k1gMnPc1	muzikant
se	se	k3xPyFc4	se
na	na	k7c4	na
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
stanou	stanout	k5eAaPmIp3nP	stanout
nechtěnými	chtěný	k2eNgMnPc7d1	nechtěný
svědky	svědek	k1gMnPc7	svědek
masakru	masakr	k1gInSc2	masakr
tamní	tamní	k2eAgFnSc2d1	tamní
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Utíkají	utíkat	k5eAaImIp3nP	utíkat
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
pronásledovateli	pronásledovatel	k1gMnPc7	pronásledovatel
a	a	k8xC	a
převlečou	převléct	k5eAaPmIp3nP	převléct
se	se	k3xPyFc4	se
za	za	k7c4	za
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
Josefína	Josefína	k1gFnSc1	Josefína
a	a	k8xC	a
Dafné	Dafná	k1gFnPc1	Dafná
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
v	v	k7c6	v
převleku	převlek	k1gInSc6	převlek
za	za	k7c2	za
ženy	žena	k1gFnSc2	žena
společně	společně	k6eAd1	společně
s	s	k7c7	s
výhradně	výhradně	k6eAd1	výhradně
dívčí	dívčí	k2eAgFnSc7d1	dívčí
jazzovou	jazzový	k2eAgFnSc7d1	jazzová
kapelou	kapela	k1gFnSc7	kapela
na	na	k7c4	na
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Joe	Joe	k?	Joe
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Sugar	Sugara	k1gFnPc2	Sugara
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
Dafné	Dafná	k1gFnSc2	Dafná
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
postarší	postarší	k2eAgMnSc1d1	postarší
milionář	milionář	k1gMnSc1	milionář
Osgood	Osgooda	k1gFnPc2	Osgooda
Fielding	Fielding	k1gInSc1	Fielding
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
zkomplikuje	zkomplikovat	k5eAaPmIp3nS	zkomplikovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
dorazí	dorazit	k5eAaPmIp3nP	dorazit
do	do	k7c2	do
floridského	floridský	k2eAgInSc2d1	floridský
hotelu	hotel	k1gInSc2	hotel
také	také	k9	také
gangsteři	gangster	k1gMnPc1	gangster
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
muzikanty	muzikant	k1gMnPc7	muzikant
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
Sugar	Sugara	k1gFnPc2	Sugara
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
jako	jako	k8xS	jako
sexuální	sexuální	k2eAgInSc4d1	sexuální
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Předvedla	předvést	k5eAaPmAgFnS	předvést
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
polohu	poloha	k1gFnSc4	poloha
zranitelné	zranitelný	k2eAgFnSc2d1	zranitelná
osamělé	osamělý	k2eAgFnSc2d1	osamělá
sólové	sólový	k2eAgFnSc2d1	sólová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
mužské	mužský	k2eAgFnPc1d1	mužská
postavy	postava	k1gFnPc1	postava
hrají	hrát	k5eAaImIp3nP	hrát
Jack	Jack	k1gInSc4	Jack
Lemmon	Lemmon	k1gInSc1	Lemmon
(	(	kIx(	(
<g/>
Jerry	Jerra	k1gFnPc1	Jerra
-	-	kIx~	-
Dafné	Dafné	k2eAgFnSc1d1	Dafné
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
Curtis	Curtis	k1gFnSc2	Curtis
(	(	kIx(	(
<g/>
Joe	Joe	k1gFnSc1	Joe
-	-	kIx~	-
Josefína	Josefína	k1gFnSc1	Josefína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
<g/>
,	,	kIx,	,
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
adaptovaný	adaptovaný	k2eAgInSc1d1	adaptovaný
scénář	scénář	k1gInSc1	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Oscara	Oscar	k1gMnSc4	Oscar
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
obdržel	obdržet	k5eAaPmAgInS	obdržet
také	také	k9	také
ocenění	ocenění	k1gNnSc4	ocenění
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
nominován	nominován	k2eAgMnSc1d1	nominován
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
a	a	k8xC	a
komediální	komediální	k2eAgMnSc1d1	komediální
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
komediální	komediální	k2eAgFnSc1d1	komediální
a	a	k8xC	a
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
cenu	cena	k1gFnSc4	cena
Britského	britský	k2eAgNnSc2d1	Britské
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
"	"	kIx"	"
<g/>
nebritského	britský	k2eNgMnSc2d1	nebritský
<g/>
"	"	kIx"	"
herce	herec	k1gMnSc2	herec
<g/>
.	.	kIx.	.
</s>
