<s>
Adolf	Adolf	k1gMnSc1
Pidljašeckyj	Pidljašeckyj	k1gMnSc1
<g/>
,	,	kIx,
cyrilicí	cyrilice	k1gFnSc7
А	А	k?
П	П	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
uváděn	uváděn	k2eAgInSc1d1
též	též	k9
jako	jako	k8xS,k8xC
Adolf	Adolf	k1gMnSc1
Podlaszecki	Podlaszeck	k1gFnSc2
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
???	???	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
soudce	soudce	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
rusínské	rusínský	k2eAgFnSc2d1
(	(	kIx(
<g/>
ukrajinské	ukrajinský	k2eAgFnSc2d1
<g/>
)	)	kIx)
národnosti	národnost	k1gFnSc2
z	z	k7c2
Haliče	Halič	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>