<s>
Adolf	Adolf	k1gMnSc1
Pidljašeckyj	Pidljašeckyj	k1gMnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Pidljašeckyj	Pidljašeckyj	k1gMnSc1
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1891	#num#	k4
–	–	k?
1897	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Rusínský	rusínský	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1839	#num#	k4
Wolica	Wolica	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
???	???	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Pidljašeckyj	Pidljašeckyj	k1gMnSc1
<g/>
,	,	kIx,
cyrilicí	cyrilice	k1gFnSc7
А	А	k?
П	П	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
uváděn	uváděn	k2eAgInSc1d1
též	též	k9
jako	jako	k8xS,k8xC
Adolf	Adolf	k1gMnSc1
Podlaszecki	Podlaszeck	k1gFnSc2
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
???	???	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
soudce	soudce	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
rusínské	rusínský	k2eAgFnSc2d1
(	(	kIx(
<g/>
ukrajinské	ukrajinský	k2eAgFnSc2d1
<g/>
)	)	kIx)
národnosti	národnost	k1gFnSc2
z	z	k7c2
Haliče	Halič	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Profesí	profes	k1gFnSc7
byl	být	k5eAaImAgInS
advokátem	advokát	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
i	i	k9
poslancem	poslanec	k1gMnSc7
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
celostátního	celostátní	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kam	kam	k6eAd1
usedl	usednout	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1891	#num#	k4
za	za	k7c4
kurii	kurie	k1gFnSc4
venkovských	venkovský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
v	v	k7c6
Haliči	Halič	k1gFnSc6
<g/>
,	,	kIx,
obvod	obvod	k1gInSc1
Kolomyja	Kolomyja	k1gFnSc1
<g/>
,	,	kIx,
Sniatyn	Sniatyn	k1gInSc1
atd.	atd.	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volebním	volební	k2eAgNnSc6d1
období	období	k1gNnSc6
1891	#num#	k4
<g/>
–	–	k?
<g/>
1897	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
Adolf	Adolf	k1gMnSc1
Podlaszecki	Podlaszeck	k1gFnSc2
<g/>
,	,	kIx,
rada	rada	k1gFnSc1
c.	c.	k?
k.	k.	k?
zemského	zemský	k2eAgInSc2d1
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
bytem	byt	k1gInSc7
Lvov	Lvov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1891	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
rusínský	rusínský	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zasedal	zasedat	k5eAaImAgInS
v	v	k7c6
Rusínském	rusínský	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
О	О	k?
Б	Б	k?
С	С	k?
з	з	k?
м	м	k?
ж	ж	k?
<g/>
↑	↑	k?
MASARYK	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
projevy	projev	k1gInPc1
1891	#num#	k4
<g/>
-	-	kIx~
<g/>
1893	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Masarykův	Masarykův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
450	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788086495101	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
441	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Databáze	databáze	k1gFnSc2
stenografických	stenografický	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
rejstříků	rejstřík	k1gInPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
z	z	k7c2
příslušných	příslušný	k2eAgNnPc2d1
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
http://alex.onb.ac.at/spa.htm.	http://alex.onb.ac.at/spa.htm.	k?
<g/>
↑	↑	k?
http://alex.onb.ac.at/cgi-content/alex?aid=spa&	http://alex.onb.ac.at/cgi-content/alex?aid=spa&	k?
<g/>
↑	↑	k?
Národní	národní	k2eAgFnSc2d1
listy	lista	k1gFnSc2
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1891	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Südsteirische	Südsteirische	k1gNnSc2
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1893	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Ukrajina	Ukrajina	k1gFnSc1
</s>
