<s>
Řezník	řezník	k1gMnSc1	řezník
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
porážkou	porážka	k1gFnSc7	porážka
<g/>
,	,	kIx,	,
bouráním	bourání	k1gNnSc7	bourání
a	a	k8xC	a
porcováním	porcování	k1gNnSc7	porcování
jatečných	jatečný	k2eAgNnPc2d1	jatečné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
jejich	jejich	k3xOp3gNnSc2	jejich
masa	maso	k1gNnSc2	maso
<g/>
;	;	kIx,	;
prodejna	prodejna	k1gFnSc1	prodejna
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
řezník	řezník	k1gMnSc1	řezník
prodává	prodávat	k5eAaImIp3nS	prodávat
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
zboží	zboží	k1gNnSc4	zboží
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
uzeniny	uzenina	k1gFnSc2	uzenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
řeznictví	řeznictví	k1gNnSc1	řeznictví
<g/>
.	.	kIx.	.
</s>
