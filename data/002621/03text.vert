<s>
Řezník	řezník	k1gMnSc1	řezník
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
porážkou	porážka	k1gFnSc7	porážka
<g/>
,	,	kIx,	,
bouráním	bourání	k1gNnSc7	bourání
a	a	k8xC	a
porcováním	porcování	k1gNnSc7	porcování
jatečných	jatečný	k2eAgNnPc2d1	jatečné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
úpravou	úprava	k1gFnSc7	úprava
jejich	jejich	k3xOp3gNnSc2	jejich
masa	maso	k1gNnSc2	maso
<g/>
;	;	kIx,	;
prodejna	prodejna	k1gFnSc1	prodejna
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
řezník	řezník	k1gMnSc1	řezník
prodává	prodávat	k5eAaImIp3nS	prodávat
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
zboží	zboží	k1gNnSc4	zboží
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
uzeniny	uzenina	k1gFnSc2	uzenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
řeznictví	řeznictví	k1gNnSc1	řeznictví
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
profesí	profes	k1gFnSc7	profes
je	být	k5eAaImIp3nS	být
uzenář	uzenář	k1gMnSc1	uzenář
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
povolání	povolání	k1gNnSc6	povolání
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
uzenin	uzenina	k1gFnPc2	uzenina
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Galů	Gal	k1gMnPc2	Gal
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
řezník	řezník	k1gMnSc1	řezník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
řezník	řezník	k1gMnSc1	řezník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Řezník	řezník	k1gMnSc1	řezník
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Národní	národní	k2eAgFnSc1d1	národní
soustava	soustava	k1gFnSc1	soustava
povolání	povolání	k1gNnSc2	povolání
<g/>
:	:	kIx,	:
Řezník	řezník	k1gMnSc1	řezník
a	a	k8xC	a
uzenář	uzenář	k1gMnSc1	uzenář
na	na	k7c6	na
nsp	nsp	k?	nsp
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
