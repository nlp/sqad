<s>
Trabant	trabant	k1gInSc1	trabant
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
značek	značka	k1gFnPc2	značka
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
automobilkou	automobilka	k1gFnSc7	automobilka
Sachsenring	Sachsenring	k1gInSc1	Sachsenring
v	v	k7c6	v
saském	saský	k2eAgNnSc6d1	Saské
městě	město	k1gNnSc6	město
Zwickau	Zwickaus	k1gInSc2	Zwickaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
období	období	k1gNnSc6	období
patřila	patřit	k5eAaImAgFnS	patřit
továrna	továrna	k1gFnSc1	továrna
automobilce	automobilka	k1gFnSc3	automobilka
Horch	Horch	k1gInSc1	Horch
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
výrobou	výroba	k1gFnSc7	výroba
luxusních	luxusní	k2eAgFnPc2d1	luxusní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
601	[number]	k4	601
s	s	k7c7	s
dvoutaktním	dvoutaktní	k2eAgInSc7d1	dvoutaktní
motorem	motor	k1gInSc7	motor
a	a	k8xC	a
karosérií	karosérie	k1gFnSc7	karosérie
z	z	k7c2	z
duroplastu	duroplast	k1gInSc2	duroplast
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
Východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
Horch	Horcha	k1gFnPc2	Horcha
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
kontrolované	kontrolovaný	k2eAgInPc4d1	kontrolovaný
Sověty	Sověty	k1gInPc4	Sověty
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
rovněž	rovněž	k9	rovněž
vozy	vůz	k1gInPc1	vůz
IFA	IFA	kA	IFA
F8	F8	k1gMnSc7	F8
a	a	k8xC	a
IFA	IFA	kA	IFA
F9	F9	k1gFnSc1	F9
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
předválečného	předválečný	k2eAgInSc2d1	předválečný
vozu	vůz	k1gInSc2	vůz
DKW	DKW	kA	DKW
F	F	kA	F
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
automobilky	automobilka	k1gFnSc2	automobilka
AWE	AWE	kA	AWE
v	v	k7c6	v
Eisenachu	Eisenach	k1gInSc6	Eisenach
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
zrodil	zrodit	k5eAaPmAgInS	zrodit
automobil	automobil	k1gInSc1	automobil
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
podle	podle	k7c2	podle
blízkého	blízký	k2eAgInSc2d1	blízký
hradu	hrad	k1gInSc2	hrad
Wartburg	Wartburg	k1gInSc1	Wartburg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
ve	v	k7c6	v
Zwickau	Zwickaus	k1gInSc6	Zwickaus
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
luxusní	luxusní	k2eAgInPc4d1	luxusní
automobily	automobil	k1gInPc4	automobil
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
P240	P240	k1gFnSc2	P240
Sachsenring	Sachsenring	k1gInSc1	Sachsenring
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
prominentní	prominentní	k2eAgMnPc4d1	prominentní
představitele	představitel	k1gMnPc4	představitel
NDR	NDR	kA	NDR
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
i	i	k9	i
otevřené	otevřený	k2eAgInPc1d1	otevřený
modely	model	k1gInPc1	model
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
přehlídky	přehlídka	k1gFnPc4	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
Výraznějšího	výrazný	k2eAgNnSc2d2	výraznější
rozšíření	rozšíření	k1gNnSc2	rozšíření
tyto	tento	k3xDgInPc1	tento
vozy	vůz	k1gInPc4	vůz
ovšem	ovšem	k9	ovšem
nezaznamenaly	zaznamenat	k5eNaPmAgFnP	zaznamenat
a	a	k8xC	a
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
uplatňované	uplatňovaný	k2eAgFnSc2d1	uplatňovaná
politiky	politika	k1gFnSc2	politika
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
přednost	přednost	k1gFnSc1	přednost
vývoji	vývoj	k1gInSc6	vývoj
skutečně	skutečně	k6eAd1	skutečně
lidového	lidový	k2eAgNnSc2d1	lidové
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
Trabantu	trabant	k1gInSc2	trabant
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
P70	P70	k1gFnSc2	P70
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
základech	základ	k1gInPc6	základ
automobilu	automobil	k1gInSc2	automobil
DKW	DKW	kA	DKW
F	F	kA	F
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
karoserie	karoserie	k1gFnSc1	karoserie
z	z	k7c2	z
Duroplastu	Duroplast	k1gInSc2	Duroplast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
stala	stát	k5eAaPmAgFnS	stát
typickou	typický	k2eAgFnSc7d1	typická
známkou	známka	k1gFnSc7	známka
Trabantů	trabant	k1gInPc2	trabant
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Velké	velký	k2eAgFnSc2d1	velká
říjnové	říjnový	k2eAgFnSc2d1	říjnová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
sjelo	sjet	k5eAaPmAgNnS	sjet
z	z	k7c2	z
pásu	pás	k1gInSc2	pás
prvních	první	k4xOgInPc2	první
50	[number]	k4	50
kusů	kus	k1gInPc2	kus
prototypu	prototyp	k1gInSc2	prototyp
Trabant	trabant	k1gInSc1	trabant
500	[number]	k4	500
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
výroba	výroba	k1gFnSc1	výroba
modelu	model	k1gInSc2	model
P	P	kA	P
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
představena	představen	k2eAgFnSc1d1	představena
verze	verze	k1gFnSc1	verze
P	P	kA	P
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
odlišovala	odlišovat	k5eAaImAgFnS	odlišovat
jen	jen	k6eAd1	jen
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
karoserie	karoserie	k1gFnSc1	karoserie
Trabantu	trabant	k1gInSc2	trabant
významně	významně	k6eAd1	významně
přepracována	přepracovat	k5eAaPmNgFnS	přepracovat
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Trabant	trabant	k1gInSc1	trabant
601	[number]	k4	601
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
601	[number]	k4	601
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
změnami	změna	k1gFnPc7	změna
byl	být	k5eAaImAgInS	být
Trabant	trabant	k1gInSc1	trabant
601	[number]	k4	601
vyráběn	vyráběn	k2eAgMnSc1d1	vyráběn
až	až	k8xS	až
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Trabant	trabant	k1gInSc4	trabant
601	[number]	k4	601
velmi	velmi	k6eAd1	velmi
progresivním	progresivní	k2eAgInSc7d1	progresivní
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
v	v	k7c6	v
masové	masový	k2eAgFnSc6d1	masová
míře	míra	k1gFnSc6	míra
vyráběn	vyráběn	k2eAgInSc4d1	vyráběn
z	z	k7c2	z
plastů	plast	k1gInPc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Poháněl	pohánět	k5eAaImAgMnS	pohánět
ho	on	k3xPp3gNnSc4	on
dvoutaktní	dvoutaktní	k2eAgMnSc1d1	dvoutaktní
<g/>
,	,	kIx,	,
vzduchem	vzduch	k1gInSc7	vzduch
chlazený	chlazený	k2eAgInSc4d1	chlazený
dvouválec	dvouválec	k1gInSc4	dvouválec
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
26	[number]	k4	26
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Vybaven	vybavit	k5eAaPmNgInS	vybavit
byl	být	k5eAaImAgInS	být
čtyřstupňovou	čtyřstupňový	k2eAgFnSc7d1	čtyřstupňová
převodovkou	převodovka	k1gFnSc7	převodovka
s	s	k7c7	s
volnoběžkou	volnoběžka	k1gFnSc7	volnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
byla	být	k5eAaImAgFnS	být
montována	montován	k2eAgFnSc1d1	montována
poloautomatická	poloautomatický	k2eAgFnSc1d1	poloautomatická
spojka	spojka	k1gFnSc1	spojka
Hycomat	Hycomat	k1gInSc1	Hycomat
<g/>
.	.	kIx.	.
</s>
<s>
Trabant	trabant	k1gMnSc1	trabant
601	[number]	k4	601
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stupních	stupeň	k1gInPc6	stupeň
výbavy	výbava	k1gFnSc2	výbava
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Standard	standard	k1gInSc1	standard
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
de	de	k?	de
Luxe	Lux	k1gMnSc4	Lux
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
karosářských	karosářský	k2eAgFnPc6d1	karosářská
variantách	varianta	k1gFnPc6	varianta
–	–	k?	–
Limousine	Limousin	k1gMnSc5	Limousin
a	a	k8xC	a
Universal	Universal	k1gMnSc5	Universal
(	(	kIx(	(
<g/>
Combi	Comb	k1gMnSc6	Comb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
i	i	k8xC	i
verze	verze	k1gFnSc1	verze
Tramp	tramp	k1gInSc1	tramp
s	s	k7c7	s
plátěnou	plátěný	k2eAgFnSc7d1	plátěná
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
plátěnými	plátěný	k2eAgFnPc7d1	plátěná
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
východoněmeckou	východoněmecký	k2eAgFnSc7d1	východoněmecká
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
v	v	k7c6	v
civilním	civilní	k2eAgNnSc6d1	civilní
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
období	období	k1gNnSc2	období
výroby	výroba	k1gFnSc2	výroba
docházelo	docházet	k5eAaImAgNnS	docházet
jen	jen	k9	jen
k	k	k7c3	k
minimálním	minimální	k2eAgFnPc3d1	minimální
inovacím	inovace	k1gFnPc3	inovace
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
velmi	velmi	k6eAd1	velmi
zdařilých	zdařilý	k2eAgInPc2d1	zdařilý
prototypů	prototyp	k1gInPc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výraznější	výrazný	k2eAgFnSc7d2	výraznější
inovací	inovace	k1gFnSc7	inovace
bylo	být	k5eAaImAgNnS	být
zahájení	zahájení	k1gNnSc1	zahájení
montáže	montáž	k1gFnSc2	montáž
již	již	k6eAd1	již
vodou	voda	k1gFnSc7	voda
chlazených	chlazený	k2eAgInPc2d1	chlazený
čtyřtaktních	čtyřtaktní	k2eAgInPc2d1	čtyřtaktní
motorů	motor	k1gInPc2	motor
Volkswagen	volkswagen	k1gInSc1	volkswagen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
při	při	k7c6	při
které	který	k3yRgFnSc6	který
i	i	k9	i
karoserie	karoserie	k1gFnSc1	karoserie
prodělala	prodělat	k5eAaPmAgFnS	prodělat
drobné	drobný	k2eAgNnSc4d1	drobné
omlazení	omlazení	k1gNnSc4	omlazení
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
typ	typ	k1gInSc1	typ
motoru	motor	k1gInSc2	motor
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
montovat	montovat	k5eAaImF	montovat
i	i	k9	i
do	do	k7c2	do
vozů	vůz	k1gInPc2	vůz
Volkswagen	volkswagen	k1gInSc1	volkswagen
Polo	polo	k6eAd1	polo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
výrazné	výrazný	k2eAgFnPc1d1	výrazná
změny	změna	k1gFnPc1	změna
byly	být	k5eAaImAgFnP	být
řadicí	řadicí	k2eAgFnPc1d1	řadicí
páka	páka	k1gFnSc1	páka
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
,	,	kIx,	,
vpředu	vpředu	k6eAd1	vpředu
kotoučové	kotoučový	k2eAgFnSc2d1	kotoučová
brzdy	brzda	k1gFnSc2	brzda
a	a	k8xC	a
přemístění	přemístění	k1gNnSc2	přemístění
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Trabant	trabant	k1gInSc1	trabant
601	[number]	k4	601
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dočkal	dočkat	k5eAaPmAgMnS	dočkat
označení	označení	k1gNnSc4	označení
Trabant	trabant	k1gInSc1	trabant
1.1	[number]	k4	1.1
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
automobilky	automobilka	k1gFnSc2	automobilka
a	a	k8xC	a
nezájem	nezájem	k1gInSc1	nezájem
zákazníků	zákazník	k1gMnPc2	zákazník
o	o	k7c4	o
tyto	tento	k3xDgInPc4	tento
vozy	vůz	k1gInPc4	vůz
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
sjel	sjet	k5eAaPmAgMnS	sjet
z	z	k7c2	z
výrobních	výrobní	k2eAgInPc2d1	výrobní
pásů	pás	k1gInPc2	pás
ve	v	k7c6	v
Zwickau	Zwickaus	k1gInSc6	Zwickaus
poslední	poslední	k2eAgInSc4d1	poslední
Trabant	trabant	k1gInSc4	trabant
1.1	[number]	k4	1.1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
panovala	panovat	k5eAaImAgFnS	panovat
fáma	fáma	k1gFnSc1	fáma
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
<g/>
Ale	ale	k9	ale
žádná	žádný	k3yNgFnSc1	žádný
další	další	k2eAgFnSc1d1	další
výroba	výroba	k1gFnSc1	výroba
legendárního	legendární	k2eAgMnSc2d1	legendární
Trabanta	trabant	k1gMnSc2	trabant
nepokračovala	pokračovat	k5eNaImAgNnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Industrieverband	Industrieverband	k1gInSc4	Industrieverband
Fahrzeugbau	Fahrzeugbaus	k1gInSc2	Fahrzeugbaus
Multicar	Multicar	k1gInSc1	Multicar
Robur	Robur	k1gMnSc1	Robur
Barkas	barkasa	k1gFnPc2	barkasa
Transtrabant	Transtrabant	k1gInSc4	Transtrabant
Seznam	seznam	k1gInSc4	seznam
značek	značka	k1gFnPc2	značka
automobilů	automobil	k1gInPc2	automobil
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trabant	trabant	k1gInSc1	trabant
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Trabant	trabant	k1gInSc1	trabant
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
trabantech	trabant	k1gInPc6	trabant
http://www.trabant.cz	[url]	k1gFnPc2	http://www.trabant.cz
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
trabantech	trabant	k1gInPc6	trabant
(	(	kIx(	(
<g/>
prospekty	prospekt	k1gInPc1	prospekt
<g/>
,	,	kIx,	,
historie	historie	k1gFnPc1	historie
<g/>
,	,	kIx,	,
diskuse	diskuse	k1gFnPc1	diskuse
<g/>
)	)	kIx)	)
http://www.vtkbohumin.net	[url]	k1gInSc1	http://www.vtkbohumin.net
–	–	k?	–
stránky	stránka	k1gFnPc4	stránka
Bohumínského	bohumínský	k2eAgInSc2d1	bohumínský
Trabant	trabant	k1gMnSc1	trabant
klubu	klub	k1gInSc2	klub
http://www.trabantmuzeum.cz/	[url]	k?	http://www.trabantmuzeum.cz/
-	-	kIx~	-
MUZEUM	muzeum	k1gNnSc1	muzeum
TRABANTŮ	trabant	k1gInPc2	trabant
</s>
