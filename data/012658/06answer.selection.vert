<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgInSc4d1	tradiční
indický	indický	k2eAgInSc4d1	indický
filosofický	filosofický	k2eAgInSc4d1	filosofický
a	a	k8xC	a
náboženský	náboženský	k2eAgInSc4d1	náboženský
koncept	koncept	k1gInSc4	koncept
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
historických	historický	k2eAgFnPc6d1	historická
duchovních	duchovní	k2eAgFnPc6d1	duchovní
praktikách	praktika	k1gFnPc6	praktika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
