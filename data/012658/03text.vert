<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
křesťanství	křesťanství	k1gNnSc6	křesťanství
a	a	k8xC	a
islámu	islám	k1gInSc6	islám
třetí	třetí	k4xOgNnPc1	třetí
nejrozšířenější	rozšířený	k2eAgNnPc1d3	nejrozšířenější
náboženství	náboženství	k1gNnPc1	náboženství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
jednu	jeden	k4xCgFnSc4	jeden
miliardu	miliarda	k4xCgFnSc4	miliarda
následovníků	následovník	k1gMnPc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgInSc4d1	tradiční
indický	indický	k2eAgInSc4d1	indický
filosofický	filosofický	k2eAgInSc4d1	filosofický
a	a	k8xC	a
náboženský	náboženský	k2eAgInSc4d1	náboženský
koncept	koncept	k1gInSc4	koncept
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
historických	historický	k2eAgFnPc6d1	historická
duchovních	duchovní	k2eAgFnPc6d1	duchovní
praktikách	praktika	k1gFnPc6	praktika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
perského	perský	k2eAgInSc2d1	perský
slovního	slovní	k2eAgInSc2d1	slovní
kořene	kořen	k1gInSc2	kořen
hindu	hind	k1gMnSc3	hind
(	(	kIx(	(
<g/>
ح	ح	k?	ح
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
sindhu	sindh	k1gInSc2	sindh
(	(	kIx(	(
<g/>
स	स	k?	स
<g/>
ि	ि	k?	ि
<g/>
न	न	k?	न
<g/>
्	्	k?	्
<g/>
ध	ध	k?	ध
<g/>
ु	ु	k?	ु
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pojmenování	pojmenování	k1gNnSc1	pojmenování
řeky	řeka	k1gFnSc2	řeka
Indus	Indus	k1gInSc1	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Muslimští	muslimský	k2eAgMnPc1d1	muslimský
Peršané	Peršan	k1gMnPc1	Peršan
slovem	slovem	k6eAd1	slovem
hindu	hind	k1gMnSc3	hind
označovali	označovat	k5eAaImAgMnP	označovat
nemuslimské	muslimský	k2eNgNnSc4d1	nemuslimské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
Indus	Indus	k1gInSc4	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
principu	princip	k1gInSc6	princip
podobné	podobný	k2eAgNnSc1d1	podobné
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
pohan	pohana	k1gFnPc2	pohana
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
nekřesťanské	křesťanský	k2eNgNnSc4d1	nekřesťanské
<g/>
.	.	kIx.	.
</s>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
náboženství	náboženství	k1gNnSc4	náboženství
v	v	k7c6	v
evropském	evropský	k2eAgMnSc6d1	evropský
<g/>
,	,	kIx,	,
teistickém	teistický	k2eAgInSc6d1	teistický
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dharma	dharma	k1gFnSc1	dharma
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
filozofii	filozofie	k1gFnSc6	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
překládal	překládat	k5eAaImAgMnS	překládat
přední	přední	k2eAgMnSc1d1	přední
český	český	k2eAgMnSc1d1	český
indolog	indolog	k1gMnSc1	indolog
Vladimír	Vladimír	k1gMnSc1	Vladimír
Miltner	Miltner	k1gMnSc1	Miltner
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xC	jako
držmo	držmo	k6eAd1	držmo
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
jej	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
jest	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
držeti	držet	k5eAaImF	držet
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
je	on	k3xPp3gNnSc4	on
jako	jako	k8xC	jako
všeindické	všeindický	k2eAgNnSc4d1	všeindický
pojetí	pojetí	k1gNnSc4	pojetí
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
zákonů	zákon	k1gInPc2	zákon
přírody	příroda	k1gFnSc2	příroda
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hinduismu	hinduismus	k1gInSc6	hinduismus
<g/>
"	"	kIx"	"
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
koncepty	koncept	k1gInPc4	koncept
dharmy	dharma	k1gFnSc2	dharma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vaidika	vaidika	k1gFnSc1	vaidika
dharma	dharma	k1gFnSc1	dharma
(	(	kIx(	(
<g/>
व	व	k?	व
<g/>
ै	ै	k?	ै
<g/>
द	द	k?	द
<g/>
ि	ि	k?	ि
<g/>
क	क	k?	क
ध	ध	k?	ध
<g/>
्	्	k?	्
<g/>
म	म	k?	म
=	=	kIx~	=
védské	védský	k2eAgFnPc1d1	védská
držmo	držmo	k6eAd1	držmo
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
<s>
sanátana	sanátana	k1gFnSc1	sanátana
dharma	dharma	k1gFnSc1	dharma
(	(	kIx(	(
<g/>
स	स	k?	स
<g/>
ा	ा	k?	ा
<g/>
त	त	k?	त
ध	ध	k?	ध
<g/>
्	्	k?	्
<g/>
म	म	k?	म
=	=	kIx~	=
věčné	věčný	k2eAgNnSc1d1	věčné
držmo	držmo	k6eAd1	držmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
různým	různý	k2eAgMnPc3d1	různý
druhům	druh	k1gMnPc3	druh
dharmy	dharma	k1gFnSc2	dharma
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
neortodoxní	ortodoxní	k2eNgInPc4d1	neortodoxní
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nevycházejí	vycházet	k5eNaImIp3nP	vycházet
z	z	k7c2	z
učení	učení	k1gNnSc2	učení
Véd	véda	k1gFnPc2	véda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
</s>
</p>
<p>
<s>
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
džinismus	džinismus	k1gInSc1	džinismus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
sikhismus	sikhismus	k1gInSc1	sikhismus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
tantra	tantr	k1gMnSc4	tantr
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
<g/>
Původní	původní	k2eAgFnSc4d1	původní
<g/>
,	,	kIx,	,
před-buddhistickou	předuddhistický	k2eAgFnSc4d1	před-buddhistický
védskou	védský	k2eAgFnSc4d1	védská
dharmu	dharma	k1gFnSc4	dharma
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
dnes	dnes	k6eAd1	dnes
především	především	k9	především
společnost	společnost	k1gFnSc1	společnost
Árjasamádž	Árjasamádž	k1gFnSc1	Árjasamádž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nábožensko-sociální	náboženskoociální	k2eAgMnPc4d1	nábožensko-sociální
(	(	kIx(	(
<g/>
či	či	k8xC	či
filozoficko-sociální	filozofickoociální	k2eAgInSc1d1	filozoficko-sociální
<g/>
)	)	kIx)	)
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
právní	právní	k2eAgInPc4d1	právní
a	a	k8xC	a
společenské	společenský	k2eAgFnPc4d1	společenská
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hinduismu	hinduismus	k1gInSc3	hinduismus
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
cca	cca	kA	cca
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgInSc7	ten
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgNnPc3d3	veliký
společenstvím	společenství	k1gNnPc3	společenství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
ústava	ústava	k1gFnSc1	ústava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
do	do	k7c2	do
hinduismu	hinduismus	k1gInSc2	hinduismus
i	i	k9	i
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
sikhismus	sikhismus	k1gInSc1	sikhismus
a	a	k8xC	a
džinismus	džinismus	k1gInSc1	džinismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
slovem	slovo	k1gNnSc7	slovo
hinduista	hinduista	k1gMnSc1	hinduista
rozumí	rozumět	k5eAaImIp3nS	rozumět
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vyznavači	vyznavač	k1gMnPc7	vyznavač
"	"	kIx"	"
<g/>
hinduistického	hinduistický	k2eAgNnSc2d1	hinduistické
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
spíše	spíše	k9	spíše
politický	politický	k2eAgInSc1d1	politický
kontext	kontext	k1gInSc1	kontext
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
příslušníky	příslušník	k1gMnPc4	příslušník
politické	politický	k2eAgFnSc2d1	politická
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
až	až	k9	až
extremisty	extremista	k1gMnPc4	extremista
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
BJP	BJP	kA	BJP
–	–	k?	–
Bharatíja	Bharatíj	k2eAgFnSc1d1	Bharatíj
Džanata	Džanat	k2eAgFnSc1d1	Džanat
Párty	párty	k1gFnSc1	párty
=	=	kIx~	=
Indická	indický	k2eAgFnSc1d1	indická
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
prosazující	prosazující	k2eAgInPc4d1	prosazující
teokratické	teokratický	k2eAgInPc4d1	teokratický
principy	princip	k1gInPc4	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
převládá	převládat	k5eAaImIp3nS	převládat
mimo	mimo	k7c4	mimo
Indii	Indie	k1gFnSc4	Indie
také	také	k9	také
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
a	a	k8xC	a
na	na	k7c6	na
Mauriciu	Mauricium	k1gNnSc6	Mauricium
<g/>
,	,	kIx,	,
významné	významný	k2eAgFnPc1d1	významná
menšiny	menšina	k1gFnPc1	menšina
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
(	(	kIx(	(
<g/>
9,35	[number]	k4	9,35
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
(	(	kIx(	(
<g/>
9,3	[number]	k4	9,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Réunionu	Réunion	k1gInSc6	Réunion
<g/>
,	,	kIx,	,
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
<g/>
,	,	kIx,	,
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
na	na	k7c6	na
Fidži	Fidž	k1gFnSc6	Fidž
(	(	kIx(	(
<g/>
47	[number]	k4	47
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trinidadu	Trinidad	k1gInSc2	Trinidad
a	a	k8xC	a
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
indičtí	indický	k2eAgMnPc1d1	indický
hinduisté	hinduista	k1gMnPc1	hinduista
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Bali	Bal	k1gFnSc2	Bal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
terčem	terč	k1gInSc7	terč
stále	stále	k6eAd1	stále
častějších	častý	k2eAgInPc2d2	častější
útoků	útok	k1gInPc2	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
islamistů	islamista	k1gMnPc2	islamista
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nedávnými	dávný	k2eNgMnPc7d1	nedávný
imigranty	imigrant	k1gMnPc7	imigrant
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInSc4	tisíc
hinduistů	hinduista	k1gMnPc2	hinduista
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
bylo	být	k5eAaImAgNnS	být
donuceno	donucen	k2eAgNnSc1d1	donuceno
opustit	opustit	k5eAaPmF	opustit
Myanmar	Myanmar	k1gInSc4	Myanmar
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
hinduistická	hinduistický	k2eAgFnSc1d1	hinduistická
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Početná	početný	k2eAgFnSc1d1	početná
menšina	menšina	k1gFnSc1	menšina
hinduistů	hinduista	k1gMnPc2	hinduista
nepálského	nepálský	k2eAgInSc2d1	nepálský
původu	původ	k1gInSc2	původ
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
čelila	čelit	k5eAaImAgFnS	čelit
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
diskriminaci	diskriminace	k1gFnSc4	diskriminace
a	a	k8xC	a
vyhánění	vyhánění	k1gNnSc4	vyhánění
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
většinových	většinový	k2eAgMnPc2d1	většinový
buddhistů	buddhista	k1gMnPc2	buddhista
<g/>
.	.	kIx.	.
<g/>
Mocné	mocný	k2eAgNnSc1d1	mocné
hinduistické	hinduistický	k2eAgNnSc1d1	hinduistické
království	království	k1gNnSc1	království
Čampa	Čampa	k1gFnSc1	Čampa
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
středního	střední	k2eAgInSc2d1	střední
Vietnamu	Vietnam	k1gInSc2	Vietnam
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
dobyto	dobyt	k2eAgNnSc4d1	dobyto
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
a	a	k8xC	a
hinduističtí	hinduistický	k2eAgMnPc1d1	hinduistický
Čamové	Čamus	k1gMnPc1	Čamus
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
zredukováni	zredukovat	k5eAaPmNgMnP	zredukovat
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
nepatrné	patrný	k2eNgFnSc2d1	patrný
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
říše	říše	k1gFnSc1	říše
Madžapahit	Madžapahita	k1gFnPc2	Madžapahita
ovládající	ovládající	k2eAgFnSc4d1	ovládající
většinu	většina	k1gFnSc4	většina
indonéského	indonéský	k2eAgNnSc2d1	indonéské
souostroví	souostroví	k1gNnSc2	souostroví
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
hinduisticko-buddhistické	hinduistickouddhistický	k2eAgNnSc1d1	hinduisticko-buddhistický
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
islamizováno	islamizován	k2eAgNnSc1d1	islamizováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
žije	žít	k5eAaImIp3nS	žít
cca	cca	kA	cca
milionová	milionový	k2eAgFnSc1d1	milionová
hinduistická	hinduistický	k2eAgFnSc1d1	hinduistická
komunita	komunita	k1gFnSc1	komunita
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Surinamu	Surinam	k1gInSc2	Surinam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
cca	cca	kA	cca
180	[number]	k4	180
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
hinduistickou	hinduistický	k2eAgFnSc7d1	hinduistická
<g />
.	.	kIx.	.
</s>
<s>
komunitu	komunita	k1gFnSc4	komunita
tamilských	tamilský	k2eAgInPc2d1	tamilský
šivaistů	šivaist	k1gInPc2	šivaist
–	–	k?	–
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Tamilského	tamilský	k2eAgInSc2d1	tamilský
Ílamu	Ílam	k1gInSc2	Ílam
před	před	k7c7	před
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
(	(	kIx(	(
<g/>
33	[number]	k4	33
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
)	)	kIx)	)
najdeme	najít	k5eAaPmIp1nP	najít
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
cca	cca	kA	cca
40	[number]	k4	40
tisíci	tisíc	k4xCgInPc7	tisíc
hinduisty	hinduista	k1gMnPc7	hinduista
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
šivaistických	šivaistický	k2eAgInPc2d1	šivaistický
mandirů	mandir	k1gInPc2	mandir
(	(	kIx(	(
<g/>
hinduistických	hinduistický	k2eAgInPc2d1	hinduistický
chrámů	chrám	k1gInPc2	chrám
<g/>
)	)	kIx)	)
v	v	k7c6	v
jihoindickém	jihoindický	k2eAgInSc6d1	jihoindický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
hinduismu	hinduismus	k1gInSc3	hinduismus
637	[number]	k4	637
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
675	[number]	k4	675
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
Hare	Har	k1gInSc2	Har
Kršna	Kršna	k1gMnSc1	Kršna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmu	pojmout	k5eAaPmIp1nS	pojmout
"	"	kIx"	"
<g/>
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
pohledu	pohled	k1gInSc6	pohled
nepoužívá	používat	k5eNaImIp3nS	používat
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
</s>
<s>
Dušan	Dušan	k1gMnSc1	Dušan
Zbavitel	zbavitel	k1gMnSc1	zbavitel
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
védské	védský	k2eAgInPc4d1	védský
(	(	kIx(	(
<g/>
předbuddhistické	předbuddhistický	k2eAgInPc4d1	předbuddhistický
<g/>
)	)	kIx)	)
a	a	k8xC	a
hinduistické	hinduistický	k2eAgNnSc4d1	hinduistické
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
rozlišení	rozlišení	k1gNnPc4	rozlišení
jiná	jiný	k2eAgNnPc4d1	jiné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
není	být	k5eNaImIp3nS	být
podstatný	podstatný	k2eAgInSc1d1	podstatný
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
postupný	postupný	k2eAgInSc4d1	postupný
vývoj	vývoj	k1gInSc4	vývoj
jednoho	jeden	k4xCgNnSc2	jeden
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
veliké	veliký	k2eAgInPc1d1	veliký
skoky	skok	k1gInPc1	skok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
západních	západní	k2eAgInPc2d1	západní
názorů	názor	k1gInPc2	názor
(	(	kIx(	(
<g/>
nepodporovaných	podporovaný	k2eNgFnPc2d1	nepodporovaná
tradicí	tradice	k1gFnPc2	tradice
ale	ale	k8xC	ale
ani	ani	k9	ani
posledními	poslední	k2eAgInPc7d1	poslední
výzkumy	výzkum	k1gInPc7	výzkum
<g/>
)	)	kIx)	)
nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
hinduismus	hinduismus	k1gInSc4	hinduismus
náhlým	náhlý	k2eAgInSc7d1	náhlý
zlomem	zlom	k1gInSc7	zlom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
védského	védský	k2eAgNnSc2d1	védské
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
védské	védský	k2eAgFnSc2d1	védská
dharmy	dharma	k1gFnSc2	dharma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zaměřovalo	zaměřovat	k5eAaImAgNnS	zaměřovat
na	na	k7c4	na
uctívání	uctívání	k1gNnSc4	uctívání
přírodních	přírodní	k2eAgFnPc2d1	přírodní
<g />
.	.	kIx.	.
</s>
<s>
sil	síla	k1gFnPc2	síla
jako	jako	k8xS	jako
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Okolo	okolo	k7c2	okolo
2	[number]	k4	2
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
měli	mít	k5eAaImAgMnP	mít
dobýt	dobýt	k5eAaPmF	dobýt
Árjové	Árjové	k2eAgFnSc4d1	Árjové
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
zničit	zničit	k5eAaPmF	zničit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
civilizaci	civilizace	k1gFnSc4	civilizace
(	(	kIx(	(
<g/>
našly	najít	k5eAaPmAgInP	najít
se	se	k3xPyFc4	se
zbytky	zbytek	k1gInPc7	zbytek
měst	město	k1gNnPc2	město
Mohendžo-daro	Mohendžoara	k1gFnSc5	Mohendžo-dara
<g/>
,	,	kIx,	,
Harrapa	Harrap	k1gMnSc2	Harrap
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
postavených	postavený	k2eAgMnPc2d1	postavený
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
s	s	k7c7	s
pokročilou	pokročilý	k2eAgFnSc7d1	pokročilá
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
myšlenka	myšlenka	k1gFnSc1	myšlenka
o	o	k7c6	o
vpádu	vpád	k1gInSc6	vpád
Árjů	Árj	k1gInPc2	Árj
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
čím	co	k3yRnSc7	co
dále	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
badateli	badatel	k1gMnPc7	badatel
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
.	.	kIx.	.
</s>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
pak	pak	k6eAd1	pak
údajně	údajně	k6eAd1	údajně
přebíral	přebírat	k5eAaImAgMnS	přebírat
vlivy	vliv	k1gInPc7	vliv
i	i	k8xC	i
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
původní	původní	k2eAgFnSc2d1	původní
civilizace	civilizace	k1gFnSc2	civilizace
(	(	kIx(	(
<g/>
myšlenka	myšlenka	k1gFnSc1	myšlenka
osvícení	osvícení	k1gNnSc2	osvícení
(	(	kIx(	(
<g/>
mókša	mókša	k6eAd1	mókša
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
1	[number]	k4	1
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
společenské	společenský	k2eAgFnSc2d1	společenská
vrstvy	vrstva	k1gFnSc2	vrstva
bráhmanů	bráhman	k1gMnPc2	bráhman
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
následek	následek	k1gInSc4	následek
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznik	vznik	k1gInSc4	vznik
mnoha	mnoho	k4c3	mnoho
"	"	kIx"	"
<g/>
heretických	heretický	k2eAgNnPc2d1	heretické
<g/>
"	"	kIx"	"
hnutí	hnutí	k1gNnPc2	hnutí
–	–	k?	–
různých	různý	k2eAgFnPc2d1	různá
duchovních	duchovní	k2eAgFnPc2d1	duchovní
škol	škola	k1gFnPc2	škola
hledající	hledající	k2eAgFnSc2d1	hledající
mókšu	móksat	k5eAaPmIp1nS	móksat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
skrze	skrze	k?	skrze
bráhmanské	bráhmanský	k2eAgInPc4d1	bráhmanský
obřady	obřad	k1gInPc4	obřad
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
takto	takto	k6eAd1	takto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
džinismus	džinismus	k1gInSc1	džinismus
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hinduismus	hinduismus	k1gInSc1	hinduismus
zakládá	zakládat	k5eAaImIp3nS	zakládat
předně	předně	k6eAd1	předně
na	na	k7c6	na
rituální	rituální	k2eAgFnSc6d1	rituální
praxi	praxe	k1gFnSc6	praxe
prováděné	prováděný	k2eAgMnPc4d1	prováděný
bráhmany	bráhman	k1gMnPc4	bráhman
(	(	kIx(	(
<g/>
cesta	cesta	k1gFnSc1	cesta
obřadů	obřad	k1gInPc2	obřad
–	–	k?	–
karmamárga	karmamárg	k1gMnSc2	karmamárg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
božství	božství	k1gNnSc3	božství
zprostředkovaný	zprostředkovaný	k2eAgInSc4d1	zprostředkovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
šramanského	šramanský	k2eAgNnSc2d1	šramanský
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
západních	západní	k2eAgInPc2d1	západní
názorů	názor	k1gInPc2	názor
okolo	okolo	k7c2	okolo
6	[number]	k4	6
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
Upanišady	Upanišada	k1gFnPc1	Upanišada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysvobození	vysvobození	k1gNnSc4	vysvobození
(	(	kIx(	(
<g/>
mókši	móksat	k5eAaPmIp1nSwK	móksat
<g/>
)	)	kIx)	)
také	také	k9	také
díky	díky	k7c3	díky
poznání	poznání	k1gNnSc3	poznání
či	či	k8xC	či
moudrosti	moudrost	k1gFnSc3	moudrost
(	(	kIx(	(
<g/>
cesta	cesta	k1gFnSc1	cesta
moudrosti	moudrost	k1gFnSc2	moudrost
–	–	k?	–
džňánamárga	džňánamárga	k1gFnSc1	džňánamárga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
vysvobození	vysvobození	k1gNnSc3	vysvobození
otevřela	otevřít	k5eAaPmAgFnS	otevřít
i	i	k9	i
pro	pro	k7c4	pro
kšatrije	kšatrij	k1gMnPc4	kšatrij
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ostatní	ostatní	k2eAgFnPc1d1	ostatní
varny	varna	k1gFnPc1	varna
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
přiklánět	přiklánět	k5eAaImF	přiklánět
k	k	k7c3	k
buddhismu	buddhismus	k1gInSc3	buddhismus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ten	ten	k3xDgInSc1	ten
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysvobození	vysvobození	k1gNnSc4	vysvobození
i	i	k9	i
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Ašóky	Ašóka	k1gMnSc2	Ašóka
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velikému	veliký	k2eAgNnSc3d1	veliké
rozšíření	rozšíření	k1gNnSc3	rozšíření
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
ale	ale	k8xC	ale
hinduismus	hinduismus	k1gInSc1	hinduismus
nabírá	nabírat	k5eAaImIp3nS	nabírat
novou	nový	k2eAgFnSc4d1	nová
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
Bhagavadgítě	Bhagavadgíta	k1gFnSc3	Bhagavadgíta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vysvobození	vysvobození	k1gNnSc3	vysvobození
pomocí	pomocí	k7c2	pomocí
uctívání	uctívání	k1gNnSc2	uctívání
božstva	božstvo	k1gNnSc2	božstvo
(	(	kIx(	(
<g/>
cesta	cesta	k1gFnSc1	cesta
oddanosti	oddanost	k1gFnSc2	oddanost
–	–	k?	–
bhaktimárga	bhaktimárga	k1gFnSc1	bhaktimárga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
klasický	klasický	k2eAgInSc4d1	klasický
hinduismus	hinduismus	k1gInSc4	hinduismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
náboženské	náboženský	k2eAgFnPc1d1	náboženská
představy	představa	k1gFnPc1	představa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Svět	svět	k1gInSc1	svět
a	a	k8xC	a
čas	čas	k1gInSc1	čas
===	===	k?	===
</s>
</p>
<p>
<s>
Nejautoritativnější	autoritativní	k2eAgFnSc1d3	nejautoritativnější
teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
světa	svět	k1gInSc2	svět
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Manuova	Manuův	k2eAgInSc2d1	Manuův
zákoníku	zákoník	k1gInSc2	zákoník
(	(	kIx(	(
<g/>
Manu	mana	k1gFnSc4	mana
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
stvořitelem	stvořitel	k1gMnSc7	stvořitel
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
bůh	bůh	k1gMnSc1	bůh
(	(	kIx(	(
<g/>
déva	déva	k1gMnSc1	déva
<g/>
)	)	kIx)	)
Brahma	Brahma	k1gFnSc1	Brahma
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
netrvá	trvat	k5eNaImIp3nS	trvat
věčně	věčně	k6eAd1	věčně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
periodicky	periodicky	k6eAd1	periodicky
ničen	ničen	k2eAgInSc1d1	ničen
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
tvořen	tvořit	k5eAaImNgInS	tvořit
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hinduismu	hinduismus	k1gInSc6	hinduismus
cyklický	cyklický	k2eAgMnSc1d1	cyklický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
prochází	procházet	k5eAaImIp3nS	procházet
čtyřmi	čtyři	k4xCgFnPc7	čtyři
jugami	juga	k1gFnPc7	juga
(	(	kIx(	(
<g/>
věky	věk	k1gInPc1	věk
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krtajuga	Krtajuga	k1gFnSc1	Krtajuga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
1	[number]	k4	1
728	[number]	k4	728
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tretájuga	Tretájuga	k1gFnSc1	Tretájuga
trvá	trvat	k5eAaImIp3nS	trvat
1	[number]	k4	1
296	[number]	k4	296
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
</s>
</p>
<p>
<s>
Dváparajuga	Dváparajuga	k1gFnSc1	Dváparajuga
trvající	trvající	k2eAgFnSc1d1	trvající
842	[number]	k4	842
000	[number]	k4	000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
</s>
</p>
<p>
<s>
Kalijuga	Kalijuga	k1gFnSc1	Kalijuga
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
432	[number]	k4	432
000	[number]	k4	000
letMezi	letMeze	k1gFnSc3	letMeze
každými	každý	k3xTgFnPc7	každý
dvěma	dva	k4xCgInPc7	dva
věky	věk	k1gInPc7	věk
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
400	[number]	k4	400
let	léto	k1gNnPc2	léto
soumraku	soumrak	k1gInSc2	soumrak
předchozího	předchozí	k2eAgInSc2d1	předchozí
a	a	k8xC	a
poté	poté	k6eAd1	poté
400	[number]	k4	400
let	léto	k1gNnPc2	léto
svítání	svítání	k1gNnSc1	svítání
následujícího	následující	k2eAgInSc2d1	následující
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Krta-jugam	Krtaugam	k1gInSc1	Krta-jugam
(	(	kIx(	(
<g/>
též	též	k9	též
juga	juga	k6eAd1	juga
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
počáteční	počáteční	k2eAgInSc1d1	počáteční
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
další	další	k2eAgInSc1d1	další
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
horší	zlý	k2eAgNnSc1d2	horší
<g/>
:	:	kIx,	:
upadají	upadat	k5eAaPmIp3nP	upadat
mravy	mrav	k1gInPc4	mrav
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
i	i	k8xC	i
inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
žijeme	žít	k5eAaImIp1nP	žít
v	v	k7c6	v
Kali-juze	Kaliuha	k1gFnSc6	Kali-juha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
těchto	tento	k3xDgInPc2	tento
čtyř	čtyři	k4xCgInPc2	čtyři
věků	věk	k1gInPc2	věk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mahájuga	mahájuga	k1gFnSc1	mahájuga
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
juga	juga	k1gFnSc1	juga
<g/>
)	)	kIx)	)
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
4	[number]	k4	4
320	[number]	k4	320
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
pralaja	pralaja	k1gMnSc1	pralaja
–	–	k?	–
rozplynutí	rozplynutí	k1gNnSc4	rozplynutí
<g/>
.	.	kIx.	.
1000	[number]	k4	1000
mahájug	mahájug	k1gInSc1	mahájug
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
kalpu	kalpa	k1gFnSc4	kalpa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
každé	každý	k3xTgFnSc2	každý
kalpy	kalpa	k1gFnSc2	kalpa
následuje	následovat	k5eAaImIp3nS	následovat
mahápralaja	mahápralaja	k1gFnSc1	mahápralaja
–	–	k?	–
velké	velký	k2eAgNnSc4d1	velké
rozplynutí	rozplynutí	k1gNnSc4	rozplynutí
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
kalpy	kalpa	k1gFnPc1	kalpa
také	také	k9	také
představují	představovat	k5eAaImIp3nP	představovat
jeden	jeden	k4xCgInSc4	jeden
Brahmův	Brahmův	k2eAgInSc4d1	Brahmův
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
kalpa	kalpa	k1gFnSc1	kalpa
pro	pro	k7c4	pro
den	den	k1gInSc4	den
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
pro	pro	k7c4	pro
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brahma	Brahma	k1gFnSc1	Brahma
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
100	[number]	k4	100
"	"	kIx"	"
<g/>
svých	svůj	k3xOyFgNnPc2	svůj
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
50	[number]	k4	50
kalp	kalp	k1gMnSc1	kalp
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
velkých	velký	k2eAgNnPc2d1	velké
rozplynutí	rozplynutí	k1gNnPc2	rozplynutí
<g/>
,	,	kIx,	,
asi	asi	k9	asi
200	[number]	k4	200
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kosmický	kosmický	k2eAgInSc1d1	kosmický
cyklus	cyklus	k1gInSc1	cyklus
pak	pak	k6eAd1	pak
začne	začít	k5eAaPmIp3nS	začít
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
podobné	podobný	k2eAgInPc4d1	podobný
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stvoření	stvoření	k1gNnSc1	stvoření
===	===	k?	===
</s>
</p>
<p>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
stvoření	stvoření	k1gNnSc3	stvoření
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Védách	véda	k1gFnPc6	véda
<g/>
.	.	kIx.	.
</s>
<s>
Nejzákladnějším	základní	k2eAgInSc7d3	nejzákladnější
hymnem	hymnus	k1gInSc7	hymnus
celého	celý	k2eAgInSc2d1	celý
Rgvédu	Rgvéd	k1gInSc2	Rgvéd
je	být	k5eAaImIp3nS	být
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
RV	RV	kA	RV
10.129	[number]	k4	10.129
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
se	se	k3xPyFc4	se
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
strofách	strofa	k1gFnPc6	strofa
snaží	snažit	k5eAaImIp3nP	snažit
zbavit	zbavit	k5eAaPmF	zbavit
různých	různý	k2eAgFnPc2d1	různá
rozšířených	rozšířený	k2eAgFnPc2d1	rozšířená
představ	představa	k1gFnPc2	představa
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
světa	svět	k1gInSc2	svět
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nebylo	být	k5eNaImAgNnS	být
jsoucna	jsoucno	k1gNnSc2	jsoucno
ani	ani	k8xC	ani
nejsoucna	nejsoucno	k1gNnSc2	nejsoucno
tehdy	tehdy	k6eAd1	tehdy
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byla	být	k5eAaImAgFnS	být
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
představa	představa	k1gFnSc1	představa
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
)	)	kIx)	)
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
chaos	chaos	k1gInSc1	chaos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Motivem	motiv	k1gInSc7	motiv
stvoření	stvoření	k1gNnSc2	stvoření
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
káma	káma	k1gFnSc1	káma
<g/>
,	,	kIx,	,
smyslná	smyslný	k2eAgFnSc1d1	smyslná
touha	touha	k1gFnSc1	touha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
proces	proces	k1gInSc1	proces
stvoření	stvoření	k1gNnSc2	stvoření
ale	ale	k8xC	ale
nemůžeme	moct	k5eNaImIp1nP	moct
pochopit	pochopit	k5eAaPmF	pochopit
–	–	k?	–
"	"	kIx"	"
<g/>
Nemůžeme	moct	k5eNaImIp1nP	moct
jej	on	k3xPp3gMnSc4	on
pochopit	pochopit	k5eAaPmF	pochopit
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
působením	působení	k1gNnSc7	působení
<g/>
,	,	kIx,	,
podnětem	podnět	k1gInSc7	podnět
vyšší	vysoký	k2eAgFnSc2d2	vyšší
moci	moc	k1gFnSc2	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
básník	básník	k1gMnSc1	básník
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
pravdě	pravda	k1gFnSc6	pravda
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
světě	svět	k1gInSc6	svět
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
<g/>
odkud	odkud	k6eAd1	odkud
vše	všechen	k3xTgNnSc1	všechen
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
<g/>
.	.	kIx.	.
<g/>
Bozi	bůh	k1gMnPc1	bůh
se	se	k3xPyFc4	se
zrodili	zrodit	k5eAaPmAgMnP	zrodit
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
tedy	tedy	k9	tedy
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yQnSc2	co
svět	svět	k1gInSc1	svět
povstal	povstat	k5eAaPmAgInS	povstat
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
toto	tento	k3xDgNnSc4	tento
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svou	svůj	k3xOyFgFnSc7	svůj
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
sám	sám	k3xTgMnSc1	sám
vše	všechen	k3xTgNnSc4	všechen
vykonal	vykonat	k5eAaPmAgMnS	vykonat
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
bdící	bdící	k2eAgMnPc1d1	bdící
nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
nebi	nebe	k1gNnSc6	nebe
<g/>
,	,	kIx,	,
<g/>
jistě	jistě	k9	jistě
to	ten	k3xDgNnSc1	ten
ví	vědět	k5eAaImIp3nS	vědět
-	-	kIx~	-
nebo	nebo	k8xC	nebo
neví	vědět	k5eNaImIp3nS	vědět
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
má	mít	k5eAaImIp3nS	mít
většinou	většina	k1gFnSc7	většina
více	hodně	k6eAd2	hodně
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
variant	variant	k1gInSc1	variant
stvoření	stvoření	k1gNnSc2	stvoření
existuje	existovat	k5eAaImIp3nS	existovat
veliké	veliký	k2eAgNnSc4d1	veliké
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výkladu	výklad	k1gInSc6	výklad
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
pět	pět	k4xCc4	pět
živlů	živel	k1gInPc2	živel
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
guny	guna	k1gFnPc4	guna
(	(	kIx(	(
<g/>
kvality	kvalita	k1gFnPc4	kvalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
živlů	živel	k1gInPc2	živel
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
tento	tento	k3xDgInSc4	tento
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
a	a	k8xC	a
pátý	pátý	k4xOgInSc1	pátý
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
akáša	akáša	k1gFnSc1	akáša
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
pět	pět	k4xCc4	pět
živlů	živel	k1gInPc2	živel
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
už	už	k6eAd1	už
u	u	k7c2	u
Jádžňavalkji	Jádžňavalkj	k1gMnSc6	Jádžňavalkj
(	(	kIx(	(
<g/>
640	[number]	k4	640
<g/>
–	–	k?	–
<g/>
610	[number]	k4	610
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
často	často	k6eAd1	často
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
Upanišadách	Upanišada	k1gFnPc6	Upanišada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tři	tři	k4xCgFnPc1	tři
guny	guna	k1gFnPc1	guna
jsou	být	k5eAaImIp3nP	být
jakési	jakýsi	k3yIgNnSc1	jakýsi
kvality	kvalita	k1gFnPc1	kvalita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
všechno	všechen	k3xTgNnSc4	všechen
existující	existující	k2eAgNnSc4d1	existující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
sattva	sattva	k6eAd1	sattva
–	–	k?	–
čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
radžas	radžas	k1gInSc1	radžas
–	–	k?	–
neklid	neklid	k1gInSc1	neklid
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
tamas	tamas	k1gInSc1	tamas
–	–	k?	–
temnota	temnota	k1gFnSc1	temnota
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc1	tento
kvality	kvalita	k1gFnPc1	kvalita
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
definovány	definovat	k5eAaBmNgFnP	definovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
–	–	k?	–
např.	např.	kA	např.
o	o	k7c6	o
mase	masa	k1gFnSc6	masa
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
kvality	kvalita	k1gFnPc4	kvalita
radžas	radžasa	k1gFnPc2	radžasa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
člověka	člověk	k1gMnSc4	člověk
zneklidňuje	zneklidňovat	k5eAaImIp3nS	zneklidňovat
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
aktivním	aktivní	k2eAgMnSc7d1	aktivní
a	a	k8xC	a
obrací	obracet	k5eAaImIp3nS	obracet
ho	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
světským	světský	k2eAgFnPc3d1	světská
věcem	věc	k1gFnPc3	věc
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňovat	k5eAaImNgFnP	zmiňovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
celý	celý	k2eAgInSc4d1	celý
čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
zpěv	zpěv	k1gInSc4	zpěv
Bhagavadgíty	Bhagavadgíta	k1gFnSc2	Bhagavadgíta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
starou	starý	k2eAgFnSc7d1	stará
a	a	k8xC	a
důležitou	důležitý	k2eAgFnSc7d1	důležitá
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c4	o
reinkarnaci	reinkarnace	k1gFnSc4	reinkarnace
(	(	kIx(	(
<g/>
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
šíři	šíř	k1gFnSc6	šíř
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
už	už	k6eAd1	už
v	v	k7c6	v
Upanišadách	Upanišada	k1gFnPc6	Upanišada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
duše	duše	k1gFnSc1	duše
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
znovu	znovu	k6eAd1	znovu
narodí	narodit	k5eAaPmIp3nP	narodit
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
a	a	k8xC	a
v	v	k7c6	v
jakých	jaký	k3yQgFnPc6	jaký
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
tzv.	tzv.	kA	tzv.
karmanový	karmanový	k2eAgInSc1d1	karmanový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
od	od	k7c2	od
karman	karman	k1gInSc4	karman
=	=	kIx~	=
čin	čin	k1gInSc4	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
svými	svůj	k3xOyFgInPc7	svůj
činy	čin	k1gInPc7	čin
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
buď	buď	k8xC	buď
dobrou	dobrý	k2eAgFnSc4d1	dobrá
nebo	nebo	k8xC	nebo
špatnou	špatný	k2eAgFnSc4d1	špatná
karmu	karma	k1gFnSc4	karma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
hromadí	hromadit	k5eAaImIp3nS	hromadit
a	a	k8xC	a
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
a	a	k8xC	a
příštích	příští	k2eAgNnPc6d1	příští
zrozeních	zrození	k1gNnPc6	zrození
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c4	na
bozích	bůh	k1gMnPc6	bůh
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prostě	prostě	k9	prostě
příčina	příčina	k1gFnSc1	příčina
a	a	k8xC	a
následek	následek	k1gInSc1	následek
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
dopadu	dopad	k1gInSc6	dopad
své	svůj	k3xOyFgFnSc2	svůj
karmy	karma	k1gFnSc2	karma
nemůže	moct	k5eNaImIp3nS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Karmu	karma	k1gFnSc4	karma
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
i	i	k9	i
mluva	mluva	k1gFnSc1	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgFnSc1d1	negativní
karma	karma	k1gFnSc1	karma
není	být	k5eNaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
"	"	kIx"	"
<g/>
neposlušností	neposlušnost	k1gFnSc7	neposlušnost
vůči	vůči	k7c3	vůči
Bohu	bůh	k1gMnSc3	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porušením	porušení	k1gNnSc7	porušení
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
zákonů	zákon	k1gInPc2	zákon
dharmy	dharma	k1gFnSc2	dharma
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
nevědomost	nevědomost	k1gFnSc1	nevědomost
(	(	kIx(	(
<g/>
avidja	avidja	k1gFnSc1	avidja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
a	a	k8xC	a
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
poznáním	poznání	k1gNnSc7	poznání
–	–	k?	–
"	"	kIx"	"
<g/>
Satjaméva	Satjamév	k1gMnSc2	Satjamév
džajaté	džajatý	k2eAgFnPc1d1	džajatý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mundakópanišada	Mundakópanišada	k1gFnSc1	Mundakópanišada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pravda	pravda	k9	pravda
vždy	vždy	k6eAd1	vždy
vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
koloběhu	koloběh	k1gInSc2	koloběh
zrození	zrození	k1gNnSc2	zrození
(	(	kIx(	(
<g/>
samsára	samsár	k1gMnSc2	samsár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velice	velice	k6eAd1	velice
strastný	strastný	k2eAgInSc4d1	strastný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
jedině	jedině	k6eAd1	jedině
vysvobozením	vysvobození	k1gNnSc7	vysvobození
(	(	kIx(	(
<g/>
mókša	mókša	k1gMnSc1	mókša
<g/>
)	)	kIx)	)
–	–	k?	–
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doména	doména	k1gFnSc1	doména
mnoha	mnoho	k4c2	mnoho
spirituálních	spirituální	k2eAgInPc2d1	spirituální
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
předěl	předěl	k1gInSc4	předěl
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
hinduismus	hinduismus	k1gInSc4	hinduismus
není	být	k5eNaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
ku	k	k7c3	k
příkladu	příklad	k1gInSc3	příklad
v	v	k7c6	v
západních	západní	k2eAgNnPc6d1	západní
náboženstvích	náboženství	k1gNnPc6	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosažení	dosažení	k1gNnSc1	dosažení
mókši	móksat	k5eAaPmIp1nSwK	móksat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
muže	muž	k1gMnSc2	muž
(	(	kIx(	(
<g/>
předně	předně	k6eAd1	předně
bráhmana	bráhman	k1gMnSc2	bráhman
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Manuova	Manuův	k2eAgInSc2d1	Manuův
zákoníku	zákoník	k1gInSc2	zákoník
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
období	období	k1gNnPc1	období
(	(	kIx(	(
<g/>
ášramy	ášram	k1gInPc1	ášram
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
brahmačárin	brahmačárin	k1gInSc1	brahmačárin
–	–	k?	–
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
védy	véd	k1gInPc1	véd
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
písma	písmo	k1gNnPc1	písmo
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
gurua	guru	k1gMnSc2	guru
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
grhastha	grhastha	k1gMnSc1	grhastha
–	–	k?	–
hospodář	hospodář	k1gMnSc1	hospodář
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
rodinnému	rodinný	k2eAgInSc3d1	rodinný
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
vánaprastha	vánaprastha	k1gMnSc1	vánaprastha
–	–	k?	–
poustevník	poustevník	k1gMnSc1	poustevník
<g/>
,	,	kIx,	,
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hledal	hledat	k5eAaImAgInS	hledat
spirituální	spirituální	k2eAgNnSc4d1	spirituální
poznání	poznání	k1gNnSc4	poznání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
sannjásin	sannjásin	k1gMnSc1	sannjásin
–	–	k?	–
svatý	svatý	k1gMnSc1	svatý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
vysvobození	vysvobození	k1gNnSc4	vysvobození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uspořádání	uspořádání	k1gNnSc4	uspořádání
společnosti	společnost	k1gFnSc2	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
zaměňované	zaměňovaný	k2eAgInPc1d1	zaměňovaný
pojmy	pojem	k1gInPc1	pojem
jsou	být	k5eAaImIp3nP	být
varna	varna	k1gFnSc1	varna
a	a	k8xC	a
kasty	kasta	k1gFnPc1	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
varna	varna	k1gFnSc1	varna
a	a	k8xC	a
kasta	kasta	k1gFnSc1	kasta
definována	definovat	k5eAaBmNgFnS	definovat
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
činnostmi	činnost	k1gFnPc7	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
příslušnost	příslušnost	k1gFnSc1	příslušnost
definována	definovat	k5eAaBmNgFnS	definovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
narozením	narození	k1gNnSc7	narození
(	(	kIx(	(
<g/>
děděna	děděn	k2eAgFnSc1d1	děděna
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
neměnila	měnit	k5eNaImAgFnS	měnit
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k6eAd1	jen
klesnout	klesnout	k5eAaPmF	klesnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
varny	varna	k1gFnPc4	varna
(	(	kIx(	(
<g/>
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
kasty	kasta	k1gFnSc2	kasta
<g/>
)	)	kIx)	)
+	+	kIx~	+
nedotýkatelní	dotýkatelný	k2eNgMnPc1d1	dotýkatelný
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nedotknutelní	nedotknutelní	k?	nedotknutelní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mimo	mimo	k7c4	mimo
varny	varna	k1gFnPc4	varna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kasta	kasta	k1gFnSc1	kasta
(	(	kIx(	(
<g/>
džáti	džáti	k1gNnSc1	džáti
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
zrození	zrození	k1gNnSc1	zrození
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
významem	význam	k1gInSc7	význam
rod	rod	k1gInSc1	rod
nebo	nebo	k8xC	nebo
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podrobnější	podrobný	k2eAgNnSc4d2	podrobnější
rozdělení	rozdělení	k1gNnSc4	rozdělení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
varen	varna	k1gFnPc2	varna
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
podle	podle	k7c2	podle
povolání	povolání	k1gNnSc2	povolání
na	na	k7c4	na
kastu	kasta	k1gFnSc4	kasta
hrnčířů	hrnčíř	k1gMnPc2	hrnčíř
<g/>
,	,	kIx,	,
kastu	kasta	k1gFnSc4	kasta
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Varny	Varna	k1gFnSc2	Varna
===	===	k?	===
</s>
</p>
<p>
<s>
Varna	Varna	k1gFnSc1	Varna
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
<g/>
)	)	kIx)	)
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
barva	barva	k1gFnSc1	barva
–	–	k?	–
vyšší	vysoký	k2eAgFnPc4d2	vyšší
varny	varna	k1gFnPc4	varna
mají	mít	k5eAaImIp3nP	mít
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
mívaly	mívat	k5eAaImAgFnP	mívat
<g/>
,	,	kIx,	,
přišly	přijít	k5eAaPmAgFnP	přijít
totiž	totiž	k9	totiž
na	na	k7c4	na
jih	jih	k1gInSc4	jih
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
světlejší	světlý	k2eAgFnSc4d2	světlejší
barvu	barva	k1gFnSc4	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
fakticky	fakticky	k6eAd1	fakticky
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starověkých	starověký	k2eAgInPc2d1	starověký
Indů	Indus	k1gInPc2	Indus
čtyři	čtyři	k4xCgFnPc1	čtyři
varny	varna	k1gFnPc1	varna
měly	mít	k5eAaImAgFnP	mít
rovnocenné	rovnocenný	k2eAgNnSc4d1	rovnocenné
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
podle	podle	k7c2	podle
povinností	povinnost	k1gFnPc2	povinnost
vůči	vůči	k7c3	vůči
společnosti	společnost	k1gFnSc3	společnost
a	a	k8xC	a
společně	společně	k6eAd1	společně
pracovaly	pracovat	k5eAaImAgFnP	pracovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
všech	všecek	k3xTgFnPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
jde	jít	k5eAaImIp3nS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
zlatých	zlatý	k2eAgInPc6d1	zlatý
časech	čas	k1gInPc6	čas
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
rovnosti	rovnost	k1gFnSc2	rovnost
do	do	k7c2	do
mytologických	mytologický	k2eAgFnPc2d1	mytologická
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
varny	varna	k1gFnPc1	varna
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
bráhmani	bráhman	k1gMnPc1	bráhman
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jediní	jediný	k2eAgMnPc1d1	jediný
mohou	moct	k5eAaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
tradiční	tradiční	k2eAgInPc4d1	tradiční
védské	védský	k2eAgInPc4d1	védský
rituály	rituál	k1gInPc4	rituál
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
Zklidnění	zklidnění	k1gNnSc1	zklidnění
<g/>
,	,	kIx,	,
sebevláda	sebevláda	k1gFnSc1	sebevláda
a	a	k8xC	a
pokání	pokání	k1gNnSc1	pokání
<g/>
,	,	kIx,	,
čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
snášenlivost	snášenlivost	k1gFnSc1	snášenlivost
a	a	k8xC	a
přímost	přímost	k1gFnSc1	přímost
<g/>
,	,	kIx,	,
poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
rozpoznávání	rozpoznávání	k1gNnSc1	rozpoznávání
a	a	k8xC	a
náboženské	náboženský	k2eAgNnSc1d1	náboženské
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
tvoří	tvořit	k5eAaImIp3nS	tvořit
činy	čin	k1gInPc4	čin
bráhmanů	bráhman	k1gMnPc2	bráhman
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nS	rodit
se	se	k3xPyFc4	se
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
podstaty	podstata	k1gFnSc2	podstata
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kšatrijové	kšatrij	k1gMnPc1	kšatrij
<g/>
,	,	kIx,	,
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
šlechta	šlechta	k1gFnSc1	šlechta
–	–	k?	–
"	"	kIx"	"
<g/>
Chrabrost	chrabrost	k1gFnSc1	chrabrost
a	a	k8xC	a
životnost	životnost	k1gFnSc1	životnost
<g/>
,	,	kIx,	,
vytrvalost	vytrvalost	k1gFnSc1	vytrvalost
a	a	k8xC	a
obratnost	obratnost	k1gFnSc1	obratnost
<g/>
,	,	kIx,	,
neochvějnost	neochvějnost	k1gFnSc1	neochvějnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
štědrost	štědrost	k1gFnSc1	štědrost
a	a	k8xC	a
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
činům	čin	k1gInPc3	čin
kšatrijů	kšatrij	k1gMnPc2	kšatrij
zrozeným	zrozený	k2eAgFnPc3d1	zrozená
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
podstaty	podstata	k1gFnSc2	podstata
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
vaišjové	vaišj	k1gMnPc1	vaišj
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
zemědělci	zemědělec	k1gMnPc1	zemědělec
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
stav	stav	k1gInSc4	stav
–	–	k?	–
"	"	kIx"	"
<g/>
Obdělávání	obdělávání	k1gNnSc1	obdělávání
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
činům	čin	k1gInPc3	čin
vaišjů	vaišj	k1gMnPc2	vaišj
zrozeným	zrozený	k2eAgFnPc3d1	zrozená
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
podstaty	podstata	k1gFnSc2	podstata
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
šúdrové	šúdr	k1gMnPc1	šúdr
<g/>
,	,	kIx,	,
služebníci	služebník	k1gMnPc1	služebník
ostatních	ostatní	k2eAgFnPc2d1	ostatní
varen	varna	k1gFnPc2	varna
–	–	k?	–
"	"	kIx"	"
<g/>
A	a	k9	a
činností	činnost	k1gFnSc7	činnost
šúdry	šúdra	k1gMnSc2	šúdra
<g/>
,	,	kIx,	,
zrozenou	zrozený	k2eAgFnSc7d1	zrozená
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
podstaty	podstata	k1gFnSc2	podstata
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
varna	varna	k1gFnSc1	varna
má	mít	k5eAaImIp3nS	mít
podrobně	podrobně	k6eAd1	podrobně
stanovené	stanovený	k2eAgFnPc4d1	stanovená
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
Manuův	Manuův	k2eAgInSc4d1	Manuův
zákoník	zákoník	k1gInSc4	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
povinnosti	povinnost	k1gFnPc1	povinnost
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgFnP	týkat
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
neúčastnily	účastnit	k5eNaImAgFnP	účastnit
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
povinností	povinnost	k1gFnPc2	povinnost
byla	být	k5eAaImAgFnS	být
oddanost	oddanost	k1gFnSc4	oddanost
manželovi	manžel	k1gMnSc3	manžel
a	a	k8xC	a
mateřství	mateřství	k1gNnPc2	mateřství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nedotýkatelní	Nedotýkatelný	k2eAgMnPc1d1	Nedotýkatelný
===	===	k?	===
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
nedotýkatelní	dotýkatelný	k2eNgMnPc1d1	dotýkatelný
(	(	kIx(	(
<g/>
také	také	k9	také
"	"	kIx"	"
<g/>
nedotknutelní	nedotknutelní	k?	nedotknutelní
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Mahátma	Mahátma	k1gFnSc1	Mahátma
Gándhí	Gándhí	k1gFnSc1	Gándhí
razil	razit	k5eAaImAgInS	razit
termín	termín	k1gInSc1	termín
haridžan	haridžany	k1gInPc2	haridžany
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
boží	boží	k2eAgInSc1d1	boží
lid	lid	k1gInSc1	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dalit	dalit	k1gInSc1	dalit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
utlačovaný	utlačovaný	k1gMnSc1	utlačovaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
dalita	dalitum	k1gNnSc2	dalitum
–	–	k?	–
द	द	k?	द
<g/>
ि	ि	k?	ि
<g/>
त	त	k?	त
<g/>
)	)	kIx)	)
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
varně	varna	k1gFnSc3	varna
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
naspodu	naspodu	k7c2	naspodu
tradiční	tradiční	k2eAgFnSc2d1	tradiční
sociální	sociální	k2eAgFnSc2d1	sociální
hierarchie	hierarchie	k1gFnSc2	hierarchie
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
pána	pán	k1gMnSc2	pán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
jen	jen	k9	jen
rituálně	rituálně	k6eAd1	rituálně
nečisté	čistý	k2eNgFnSc2d1	nečistá
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnPc4	on
mnohá	mnohý	k2eAgNnPc4d1	mnohé
přísná	přísný	k2eAgNnPc4d1	přísné
omezení	omezení	k1gNnSc4	omezení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
varnou	varna	k1gFnSc7	varna
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
znečistil	znečistit	k5eAaPmAgInS	znečistit
už	už	k9	už
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
padl	padnout	k5eAaPmAgInS	padnout
stín	stín	k1gInSc1	stín
nedotýkatelného	dotýkatelný	k2eNgInSc2d1	dotýkatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
náleží	náležet	k5eAaImIp3nS	náležet
přes	přes	k7c4	přes
200	[number]	k4	200
miliónů	milión	k4xCgInPc2	milión
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gótra	Gótro	k1gNnSc2	Gótro
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
atributem	atribut	k1gInSc7	atribut
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hinduisty	hinduista	k1gMnSc2	hinduista
společensky	společensky	k6eAd1	společensky
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
gótra	gótra	k1gFnSc1	gótra
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
ohrada	ohrada	k1gFnSc1	ohrada
pro	pro	k7c4	pro
krávy	kráva	k1gFnPc4	kráva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
významově	významově	k6eAd1	významově
podobná	podobný	k2eAgFnSc1d1	podobná
slovu	slovo	k1gNnSc3	slovo
"	"	kIx"	"
<g/>
rod	rod	k1gInSc4	rod
<g/>
"	"	kIx"	"
či	či	k8xC	či
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
především	především	k9	především
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
–	–	k?	–
ženich	ženich	k1gMnSc1	ženich
a	a	k8xC	a
nevěsta	nevěsta	k1gFnSc1	nevěsta
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
kastu	kasta	k1gFnSc4	kasta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
gótru	gótra	k1gFnSc4	gótra
(	(	kIx(	(
<g/>
těch	ten	k3xDgInPc2	ten
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
je	být	k5eAaImIp3nS	být
bráhmanských	bráhmanský	k2eAgFnPc2d1	bráhmanská
góter	gótra	k1gFnPc2	gótra
okolo	okolo	k7c2	okolo
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
svatby	svatba	k1gFnSc2	svatba
platí	platit	k5eAaImIp3nS	platit
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
nařízení	nařízení	k1gNnPc2	nařízení
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jemnější	jemný	k2eAgNnSc4d2	jemnější
rozlišení	rozlišení	k1gNnSc4	rozlišení
než	než	k8xS	než
gótry	gótra	k1gFnPc4	gótra
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Proti	proti	k7c3	proti
kastovnímu	kastovní	k2eAgInSc3d1	kastovní
systému	systém	k1gInSc3	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k6eAd1	mnoho
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
duchovních	duchovní	k2eAgInPc2d1	duchovní
směrů	směr	k1gInPc2	směr
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
úpadkovému	úpadkový	k2eAgInSc3d1	úpadkový
kastovnímu	kastovní	k2eAgInSc3d1	kastovní
systému	systém	k1gInSc3	systém
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
explicitně	explicitně	k6eAd1	explicitně
a	a	k8xC	a
jasně	jasně	k9	jasně
např.	např.	kA	např.
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
neměly	mít	k5eNaImAgFnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
reformovat	reformovat	k5eAaBmF	reformovat
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
odtrhly	odtrhnout	k5eAaPmAgFnP	odtrhnout
od	od	k7c2	od
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
začaly	začít	k5eAaPmAgFnP	začít
usilovat	usilovat	k5eAaImF	usilovat
až	až	k9	až
později	pozdě	k6eAd2	pozdě
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
založil	založit	k5eAaPmAgInS	založit
Rámmóhan	Rámmóhan	k1gInSc1	Rámmóhan
Ráj	ráj	k1gInSc1	ráj
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
společnost	společnost	k1gFnSc1	společnost
Brahmo	Brahma	k1gFnSc5	Brahma
samádž	samádž	k1gFnSc1	samádž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
zrušit	zrušit	k5eAaPmF	zrušit
diskriminaci	diskriminace	k1gFnSc4	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
varen	varna	k1gFnPc2	varna
i	i	k8xC	i
kast	kasta	k1gFnPc2	kasta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
populárnější	populární	k2eAgNnPc1d2	populárnější
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Árja	Árja	k1gMnSc1	Árja
samádž	samádž	k1gFnSc4	samádž
(	(	kIx(	(
<g/>
Společenství	společenství	k1gNnSc2	společenství
Árjů	Árj	k1gInPc2	Árj
<g/>
,	,	kIx,	,
zal	zal	k?	zal
<g/>
.	.	kIx.	.
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
očistit	očistit	k5eAaPmF	očistit
hinduismus	hinduismus	k1gInSc4	hinduismus
od	od	k7c2	od
domnělých	domnělý	k2eAgInPc2d1	domnělý
pozdějších	pozdní	k2eAgInPc2d2	pozdější
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
hlásalo	hlásat	k5eAaImAgNnS	hlásat
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
védám	véda	k1gFnPc3	véda
a	a	k8xC	a
reformu	reforma	k1gFnSc4	reforma
sociálního	sociální	k2eAgInSc2d1	sociální
systému	systém	k1gInSc2	systém
hinduismu	hinduismus	k1gInSc2	hinduismus
(	(	kIx(	(
<g/>
žádná	žádný	k3yNgNnPc4	žádný
dědičná	dědičný	k2eAgNnPc4d1	dědičné
privilegia	privilegium	k1gNnPc4	privilegium
vyšších	vysoký	k2eAgFnPc2d2	vyšší
kast	kasta	k1gFnPc2	kasta
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
Indické	indický	k2eAgFnSc2d1	indická
republiky	republika	k1gFnSc2	republika
všechny	všechen	k3xTgFnPc4	všechen
varny	varna	k1gFnPc4	varna
i	i	k8xC	i
další	další	k2eAgNnPc4d1	další
společenská	společenský	k2eAgNnPc4d1	společenské
rozdělení	rozdělení	k1gNnPc4	rozdělení
rovnoprávné	rovnoprávný	k2eAgFnSc2d1	rovnoprávná
a	a	k8xC	a
diskriminace	diskriminace	k1gFnSc2	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
varen	varna	k1gFnPc2	varna
je	být	k5eAaImIp3nS	být
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
už	už	k6eAd1	už
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
varnu	varna	k1gFnSc4	varna
či	či	k8xC	či
kastu	kasta	k1gFnSc4	kasta
nehledí	hledět	k5eNaImIp3nS	hledět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc1	systém
často	často	k6eAd1	často
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Společná	společný	k2eAgFnSc1d1	společná
gótra	gótra	k1gFnSc1	gótra
jako	jako	k8xC	jako
překážka	překážka	k1gFnSc1	překážka
sňatku	sňatek	k1gInSc2	sňatek
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosud	dosud	k6eAd1	dosud
platí	platit	k5eAaImIp3nS	platit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bůh	bůh	k1gMnSc1	bůh
a	a	k8xC	a
bohové	bůh	k1gMnPc1	bůh
==	==	k?	==
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
hinduismu	hinduismus	k1gInSc2	hinduismus
za	za	k7c4	za
polyteismus	polyteismus	k1gInSc4	polyteismus
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
další	další	k2eAgFnSc7d1	další
zavádějící	zavádějící	k2eAgFnSc7d1	zavádějící
doktrínou	doktrína	k1gFnSc7	doktrína
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
misionáři	misionář	k1gMnPc7	misionář
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
systém	systém	k1gInSc1	systém
kast	kasta	k1gFnPc2	kasta
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
A	a	k9	a
také	také	k9	také
samotné	samotný	k2eAgNnSc1d1	samotné
slovo	slovo	k1gNnSc1	slovo
kasta	kasta	k1gFnSc1	kasta
je	být	k5eAaImIp3nS	být
portugalského	portugalský	k2eAgInSc2d1	portugalský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
Ondračka	Ondračka	k1gFnSc1	Ondračka
parafrázuje	parafrázovat	k5eAaBmIp3nS	parafrázovat
Mahábháratu	Mahábhárata	k1gFnSc4	Mahábhárata
(	(	kIx(	(
<g/>
1.56	[number]	k4	1.56
<g/>
.33	.33	k4	.33
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
co	co	k3yRnSc4	co
religionista	religionista	k1gMnSc1	religionista
narazí	narazit	k5eAaPmIp3nS	narazit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
co	co	k9	co
však	však	k9	však
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
někde	někde	k6eAd1	někde
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
subkontinentu	subkontinent	k1gInSc6	subkontinent
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velikou	veliký	k2eAgFnSc7d1	veliká
tolerantností	tolerantnost	k1gFnSc7	tolerantnost
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
zde	zde	k6eAd1	zde
existují	existovat	k5eAaImIp3nP	existovat
filosofické	filosofický	k2eAgFnPc1d1	filosofická
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
snaží	snažit	k5eAaImIp3nS	snažit
božství	božství	k1gNnSc1	božství
"	"	kIx"	"
<g/>
promyslet	promyslet	k5eAaPmF	promyslet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mystici	mystik	k1gMnPc1	mystik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nechtějí	chtít	k5eNaImIp3nP	chtít
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chtějí	chtít	k5eAaImIp3nP	chtít
ho	on	k3xPp3gMnSc4	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
prostí	prostý	k2eAgMnPc1d1	prostý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
to	ten	k3xDgNnSc4	ten
řeší	řešit	k5eAaImIp3nP	řešit
spíše	spíše	k9	spíše
vlastním	vlastní	k2eAgInSc7d1	vlastní
názorem	názor	k1gInSc7	názor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
uctívá	uctívat	k5eAaImIp3nS	uctívat
stejného	stejný	k2eAgMnSc4d1	stejný
Boha	bůh	k1gMnSc4	bůh
<g/>
/	/	kIx~	/
<g/>
bohy	bůh	k1gMnPc4	bůh
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gMnPc4	jejich
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k9	už
v	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
nacházíme	nacházet	k5eAaImIp1nP	nacházet
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
bozích	bůh	k1gMnPc6	bůh
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobené	způsobený	k2eAgFnPc4d1	způsobená
jejich	jejich	k3xOp3gInSc7	jejich
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
vnější	vnější	k2eAgFnSc7d1	vnější
různorodostí	různorodost	k1gFnSc7	různorodost
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
jednota	jednota	k1gFnSc1	jednota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rgvédu	Rgvédo	k1gNnSc6	Rgvédo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velice	velice	k6eAd1	velice
známý	známý	k2eAgInSc1d1	známý
verš	verš	k1gInSc1	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Různě	různě	k6eAd1	různě
nazývají	nazývat	k5eAaImIp3nP	nazývat
věštci	věštec	k1gMnPc7	věštec
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jest	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
RV	RV	kA	RV
1.164.46	[number]	k4	1.164.46
<g/>
;	;	kIx,	;
př	př	kA	př
<g/>
.	.	kIx.	.
O.	O.	kA	O.
Friš	Friš	k1gInSc1	Friš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
názor	názor	k1gInSc4	názor
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
Upanišadách	Upanišada	k1gFnPc6	Upanišada
i	i	k8xC	i
Bhagavadgítě	Bhagavadgít	k1gInSc6	Bhagavadgít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Védská	védský	k2eAgNnPc1d1	védské
božstva	božstvo	k1gNnPc1	božstvo
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Védské	védský	k2eAgNnSc1d1	védské
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
ve	v	k7c6	v
Védách	véda	k1gFnPc6	véda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
védských	védský	k2eAgInPc2d1	védský
dévů	dév	k1gInPc2	dév
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
přírodními	přírodní	k2eAgFnPc7d1	přírodní
silami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Váju	Vájus	k1gInSc2	Vájus
–	–	k?	–
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
Agni	Agn	k1gFnPc1	Agn
–	–	k?	–
oheň	oheň	k1gInSc1	oheň
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgNnSc1d1	přední
místo	místo	k1gNnSc1	místo
pantheonu	pantheon	k1gInSc2	pantheon
náleží	náležet	k5eAaImIp3nS	náležet
Indrovi	Indra	k1gMnSc3	Indra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
atmosférickým	atmosférický	k2eAgMnSc7d1	atmosférický
bohem	bůh	k1gMnSc7	bůh
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
vládne	vládnout	k5eAaImIp3nS	vládnout
hlavně	hlavně	k9	hlavně
počasí	počasí	k1gNnSc1	počasí
<g/>
)	)	kIx)	)
a	a	k8xC	a
bohem	bůh	k1gMnSc7	bůh
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
dévou	déva	k1gMnSc7	déva
je	být	k5eAaImIp3nS	být
Varuna	Varuna	k1gFnSc1	Varuna
<g/>
,	,	kIx,	,
strážce	strážce	k1gMnSc1	strážce
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
řádu	řád	k1gInSc2	řád
(	(	kIx(	(
<g/>
rta	rta	k?	rta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aditi	Aditi	k1gNnSc1	Aditi
je	být	k5eAaImIp3nS	být
androgynní	androgynní	k2eAgFnSc7d1	androgynní
bohyní	bohyně	k1gFnSc7	bohyně
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
živé	živý	k2eAgNnSc1d1	živé
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
matkou	matka	k1gFnSc7	matka
dévů	dév	k1gMnPc2	dév
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
veškerá	veškerý	k3xTgNnPc4	veškerý
božstva	božstvo	k1gNnPc4	božstvo
védského	védský	k2eAgInSc2d1	védský
pantheonu	pantheon	k1gInSc2	pantheon
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgMnPc2d1	hlavní
bohů	bůh	k1gMnPc2	bůh
jako	jako	k8xC	jako
Višnu	Višna	k1gMnSc4	Višna
<g/>
,	,	kIx,	,
Šiva	Šivus	k1gMnSc4	Šivus
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Diti	Dit	k1gMnSc3	Dit
a	a	k8xC	a
Danu	Dan	k1gMnSc3	Dan
jsou	být	k5eAaImIp3nP	být
matky	matka	k1gFnPc1	matka
asurů	asur	k1gMnPc2	asur
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
mocní	mocnit	k5eAaImIp3nP	mocnit
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
dévové	dévové	k2eAgMnSc1d1	dévové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
ovládnou	ovládnout	k5eAaPmIp3nP	ovládnout
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dévové	dévus	k1gMnPc1	dévus
je	on	k3xPp3gMnPc4	on
přemohou	přemoct	k5eAaPmIp3nP	přemoct
nebo	nebo	k8xC	nebo
přelstí	přelstít	k5eAaPmIp3nP	přelstít
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Višnua	Višnuum	k1gNnSc2	Višnuum
či	či	k8xC	či
Šivy	Šiva	k1gFnSc2	Šiva
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
dévů	dév	k1gInPc2	dév
i	i	k8xC	i
asurů	asur	k1gInPc2	asur
je	být	k5eAaImIp3nS	být
mudrc	mudrc	k1gMnSc1	mudrc
Kašjapa	Kašjap	k1gMnSc2	Kašjap
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
dévy	dév	k1gMnPc7	dév
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
posvátná	posvátný	k2eAgFnSc1d1	posvátná
řeka	řeka	k1gFnSc1	řeka
Ganga	Ganga	k1gFnSc1	Ganga
a	a	k8xC	a
Sarasvatí	Sarasvatý	k2eAgMnPc1d1	Sarasvatý
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
řeka	řeka	k1gFnSc1	řeka
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
se	se	k3xPyFc4	se
debaty	debata	k1gFnSc2	debata
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
vůbec	vůbec	k9	vůbec
existovala	existovat	k5eAaImAgNnP	existovat
<g/>
,	,	kIx,	,
nedávno	nedávno	k6eAd1	nedávno
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
objeveno	objeven	k2eAgNnSc1d1	objeveno
vyschlé	vyschlý	k2eAgNnSc1d1	vyschlé
řečiště	řečiště	k1gNnSc1	řečiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc7	on
mohlo	moct	k5eAaImAgNnS	moct
patřit	patřit	k5eAaImF	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ganga	Ganga	k1gFnSc1	Ganga
je	být	k5eAaImIp3nS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
i	i	k9	i
v	v	k7c6	v
personifikované	personifikovaný	k2eAgFnSc6d1	personifikovaná
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
veliký	veliký	k2eAgInSc1d1	veliký
počet	počet	k1gInSc1	počet
uctívačů	uctívač	k1gMnPc2	uctívač
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
védských	védský	k2eAgInPc2d1	védský
dévů	dév	k1gInPc2	dév
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
védské	védský	k2eAgInPc1d1	védský
hymny	hymnus	k1gInPc1	hymnus
označují	označovat	k5eAaImIp3nP	označovat
různé	různý	k2eAgInPc1d1	různý
dévy	dévy	k1gInPc1	dévy
za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
představiteli	představitel	k1gMnPc7	představitel
svrchovaného	svrchovaný	k2eAgNnSc2d1	svrchované
brahma	brahmum	k1gNnSc2	brahmum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasický	klasický	k2eAgInSc1d1	klasický
hinduismus	hinduismus	k1gInSc1	hinduismus
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
bohy	bůh	k1gMnPc7	bůh
(	(	kIx(	(
<g/>
dá	dát	k5eAaPmIp3nS	dát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
říci	říct	k5eAaPmF	říct
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trojice	trojice	k1gFnSc1	trojice
Brahma	Brahmum	k1gNnSc2	Brahmum
<g/>
,	,	kIx,	,
Višnu	Višn	k1gInSc2	Višn
a	a	k8xC	a
Šiva	Šiv	k1gInSc2	Šiv
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xC	jako
Trimúrti	Trimúrt	k1gMnPc1	Trimúrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brahmani	Brahman	k1gMnPc1	Brahman
zase	zase	k9	zase
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
a	a	k8xC	a
jediného	jediný	k2eAgMnSc4d1	jediný
boha	bůh	k1gMnSc4	bůh
Šivu	Šiva	k1gMnSc4	Šiva
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
většinou	většinou	k6eAd1	většinou
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
a	a	k8xC	a
chodí	chodit	k5eAaImIp3nP	chodit
do	do	k7c2	do
stejných	stejný	k2eAgInPc2d1	stejný
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tito	tento	k3xDgMnPc1	tento
bohové	bůh	k1gMnPc1	bůh
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
ženské	ženský	k2eAgInPc4d1	ženský
protějšky	protějšek	k1gInPc4	protějšek
(	(	kIx(	(
<g/>
šakti	šakť	k1gFnPc4	šakť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sarasvatí	Sarasvatit	k5eAaPmIp3nS	Sarasvatit
je	on	k3xPp3gFnPc4	on
Brahmova	Brahmův	k2eAgFnSc1d1	Brahmova
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
i	i	k8xC	i
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
jako	jako	k8xC	jako
bohyně	bohyně	k1gFnSc1	bohyně
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Lakšmí	Lakšmí	k1gNnSc1	Lakšmí
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
manželkou	manželka	k1gFnSc7	manželka
Višnua	Višnuum	k1gNnSc2	Višnuum
<g/>
.	.	kIx.	.
</s>
<s>
Manželkou	manželka	k1gFnSc7	manželka
Šivy	Šiva	k1gFnSc2	Šiva
je	být	k5eAaImIp3nS	být
Párvatí	Párvatý	k2eAgMnPc1d1	Párvatý
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
rozličných	rozličný	k2eAgFnPc2d1	rozličná
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
forem	forma	k1gFnPc2	forma
je	být	k5eAaImIp3nS	být
bohyně	bohyně	k1gFnSc1	bohyně
Kálí	kálet	k5eAaImIp3nP	kálet
(	(	kIx(	(
<g/>
na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
chápaná	chápaný	k2eAgFnSc1d1	chápaná
jako	jako	k8xS	jako
jakési	jakýsi	k3yIgNnSc1	jakýsi
"	"	kIx"	"
<g/>
ďábelské	ďábelský	k2eAgNnSc4d1	ďábelské
božstvo	božstvo	k1gNnSc4	božstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
pravdě	pravda	k1gFnSc3	pravda
–	–	k?	–
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
symbolem	symbol	k1gInSc7	symbol
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
a	a	k8xC	a
nového	nový	k2eAgNnSc2d1	nové
zrození	zrození	k1gNnSc2	zrození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Světci	světec	k1gMnPc7	světec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
milují	milovat	k5eAaImIp3nP	milovat
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
bohy	bůh	k1gMnPc7	bůh
hinduismu	hinduismus	k1gInSc2	hinduismus
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
různé	různý	k2eAgMnPc4d1	různý
světce	světec	k1gMnPc4	světec
či	či	k8xC	či
mudrce	mudrc	k1gMnPc4	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hinduismu	hinduismus	k1gInSc2	hinduismus
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
možné	možný	k2eAgNnSc1d1	možné
narodit	narodit	k5eAaPmF	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
déva	déva	k6eAd1	déva
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
interpretace	interpretace	k1gFnSc1	interpretace
stavu	stav	k1gInSc2	stav
světce	světec	k1gMnSc2	světec
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
duchovním	duchovní	k2eAgInSc6d1	duchovní
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Světci	světec	k1gMnPc1	světec
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c2	za
inkarnace	inkarnace	k1gFnSc2	inkarnace
brahma	brahmum	k1gNnSc2	brahmum
<g/>
,	,	kIx,	,
určitého	určitý	k2eAgInSc2d1	určitý
dévy	dév	k2eAgInPc4d1	dév
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vysvobození	vysvobozený	k2eAgMnPc1d1	vysvobozený
(	(	kIx(	(
<g/>
mókša	mókša	k1gMnSc1	mókša
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
gurua	guru	k1gMnSc2	guru
(	(	kIx(	(
<g/>
duchovního	duchovní	k1gMnSc2	duchovní
učitele	učitel	k1gMnSc2	učitel
<g/>
)	)	kIx)	)
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c2	za
vyššího	vysoký	k2eAgInSc2d2	vyšší
a	a	k8xC	a
důležitějšího	důležitý	k2eAgInSc2d2	Důležitější
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
běžní	běžný	k2eAgMnPc1d1	běžný
dévové	dévus	k1gMnPc1	dévus
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
verš	verš	k1gInSc1	verš
z	z	k7c2	z
Guru	guru	k1gMnSc2	guru
gíty	gíta	k1gMnSc2	gíta
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Uttara	Uttar	k1gMnSc2	Uttar
Khandy	Khanda	k1gFnSc2	Khanda
Skanda	Skand	k1gMnSc2	Skand
Purány	Purána	k1gFnSc2	Purána
<g/>
)	)	kIx)	)
–	–	k?	–
gurur	gurur	k1gMnSc1	gurur
Brahma	Brahma	k1gNnSc1	Brahma
<g/>
,	,	kIx,	,
gurur	gurur	k1gMnSc1	gurur
Višnu	Višn	k1gInSc2	Višn
<g/>
,	,	kIx,	,
gururdévo	gururdévo	k6eAd1	gururdévo
Mahéšvara	Mahéšvara	k1gFnSc1	Mahéšvara
gurur	gurura	k1gFnPc2	gurura
eva	eva	k?	eva
parabrahma	parabrahma	k1gNnSc4	parabrahma
tasmai	tasma	k1gFnSc2	tasma
šrí	šrí	k?	šrí
gurave	guravat	k5eAaPmIp3nS	guravat
namah	namah	k1gInSc1	namah
(	(	kIx(	(
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
Guru	guru	k1gMnSc2	guru
je	být	k5eAaImIp3nS	být
Brahma	Brahma	k1gFnSc1	Brahma
<g/>
,	,	kIx,	,
Guru	guru	k1gMnSc1	guru
je	být	k5eAaImIp3nS	být
Višnu	Višna	k1gFnSc4	Višna
<g/>
,	,	kIx,	,
Guru	guru	k1gMnSc1	guru
je	být	k5eAaImIp3nS	být
Mahéšvara	Mahéšvar	k1gMnSc4	Mahéšvar
(	(	kIx(	(
<g/>
Šiva	Šiva	k1gMnSc1	Šiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Guru	guru	k1gMnSc1	guru
je	být	k5eAaImIp3nS	být
Parabrahma	Parabrahma	k1gNnSc4	Parabrahma
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
guruovi	guru	k1gMnSc6	guru
se	se	k3xPyFc4	se
klaním	klanit	k5eAaImIp1nS	klanit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
puránských	puránský	k2eAgInPc6d1	puránský
příbězích	příběh	k1gInPc6	příběh
je	být	k5eAaImIp3nS	být
postavení	postavení	k1gNnSc1	postavení
dévů	dév	k1gInPc2	dév
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Indry	Indra	k1gMnPc4	Indra
<g/>
,	,	kIx,	,
domněle	domněle	k6eAd1	domněle
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
nějakým	nějaký	k3yIgMnSc7	nějaký
světcem	světec	k1gMnSc7	světec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přísnou	přísný	k2eAgFnSc4d1	přísná
askezí	askeze	k1gFnSc7	askeze
<g/>
,	,	kIx,	,
oddaností	oddanost	k1gFnSc7	oddanost
či	či	k8xC	či
různými	různý	k2eAgFnPc7d1	různá
duchovními	duchovní	k2eAgFnPc7d1	duchovní
technikami	technika	k1gFnPc7	technika
získal	získat	k5eAaPmAgMnS	získat
příliš	příliš	k6eAd1	příliš
velikou	veliký	k2eAgFnSc4d1	veliká
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gInSc4	on
třeba	třeba	k6eAd1	třeba
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Indra	Indra	k1gMnSc1	Indra
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
často	často	k6eAd1	často
posílá	posílat	k5eAaImIp3nS	posílat
apsary	apsara	k1gFnSc2	apsara
<g/>
,	,	kIx,	,
nebeské	nebeský	k2eAgFnSc2d1	nebeská
kurtizány	kurtizána	k1gFnSc2	kurtizána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
daného	daný	k2eAgNnSc2d1	dané
světce	světec	k1gMnPc4	světec
rozptýlily	rozptýlit	k5eAaPmAgInP	rozptýlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindy	jindy	k6eAd1	jindy
dévům	dév	k1gInPc3	dév
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
od	od	k7c2	od
asurů	asur	k1gInPc2	asur
a	a	k8xC	a
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
dévům	dév	k1gInPc3	dév
často	často	k6eAd1	často
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
Višnu	Višna	k1gMnSc4	Višna
nebo	nebo	k8xC	nebo
Šiva	Šivus	k1gMnSc4	Šivus
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Višnu	Višn	k1gInSc3	Višn
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgMnS	zrodit
jako	jako	k9	jako
Narasinha	Narasinh	k1gMnSc4	Narasinh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabil	zabít	k5eAaPmAgMnS	zabít
asuru	asura	k1gMnSc4	asura
Hiranjakašipua	Hiranjakašipuus	k1gMnSc4	Hiranjakašipuus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
oddaností	oddanost	k1gFnSc7	oddanost
Brahmovi	Brahmův	k2eAgMnPc1d1	Brahmův
získal	získat	k5eAaPmAgMnS	získat
velikou	veliký	k2eAgFnSc4d1	veliká
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nesmrtelným	nesmrtelný	k1gMnSc7	nesmrtelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
indické	indický	k2eAgMnPc4d1	indický
světce	světec	k1gMnPc4	světec
patří	patřit	k5eAaImIp3nS	patřit
Vjása	Vjása	k1gFnSc1	Vjása
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
napsal	napsat	k5eAaPmAgMnS	napsat
Mahábháratu	Mahábhárata	k1gFnSc4	Mahábhárata
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
konečnou	konečný	k2eAgFnSc4d1	konečná
redakci	redakce	k1gFnSc4	redakce
Véd	véda	k1gFnPc2	véda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šankaráčárja	Šankaráčárja	k1gMnSc1	Šankaráčárja
(	(	kIx(	(
<g/>
významný	významný	k2eAgMnSc1d1	významný
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
788	[number]	k4	788
<g/>
-	-	kIx~	-
<g/>
820	[number]	k4	820
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kannappa	Kannappa	k1gFnSc1	Kannappa
(	(	kIx(	(
<g/>
významný	významný	k2eAgMnSc1d1	významný
uctívač	uctívač	k1gMnSc1	uctívač
Šivy	Šiva	k1gFnSc2	Šiva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Míra	Míra	k1gFnSc1	Míra
Báí	Báí	k1gFnSc1	Báí
(	(	kIx(	(
<g/>
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
uctívačka	uctívačka	k1gFnSc1	uctívačka
Kršny	Kršna	k1gMnSc2	Kršna
<g/>
,	,	kIx,	,
1498	[number]	k4	1498
<g/>
-	-	kIx~	-
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
legendárních	legendární	k2eAgInPc2d1	legendární
i	i	k8xC	i
historicky	historicky	k6eAd1	historicky
doložených	doložený	k2eAgMnPc2d1	doložený
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
málo	málo	k6eAd1	málo
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
světce	světec	k1gMnPc4	světec
považováni	považován	k2eAgMnPc1d1	považován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hinduismus	hinduismus	k1gInSc4	hinduismus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Bhagavadgíta	Bhagavadgíta	k1gMnSc1	Bhagavadgíta
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
J.	J.	kA	J.
Filipský	Filipský	k2eAgInSc1d1	Filipský
&	&	k?	&
J.	J.	kA	J.
Vacek	Vacek	k1gMnSc1	Vacek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Upanišady	Upanišada	k1gFnPc1	Upanišada
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
D.	D.	kA	D.
Zbavitel	zbavitel	k1gMnSc1	zbavitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86685	[number]	k4	86685
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Védské	védský	k2eAgFnPc1d1	védská
hymny	hymna	k1gFnPc1	hymna
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
O.	O.	kA	O.
Friš	Friš	k1gInSc4	Friš
<g/>
,	,	kIx,	,
studii	studie	k1gFnSc4	studie
napsal	napsat	k5eAaPmAgMnS	napsat
P.	P.	kA	P.
Vavroušek	Vavroušek	k1gMnSc1	Vavroušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
a	a	k8xC	a
revidované	revidovaný	k2eAgNnSc1d1	revidované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85905	[number]	k4	85905
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Preinhaelterová	Preinhaelterová	k1gFnSc1	Preinhaelterová
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Hinduista	hinduista	k1gMnSc1	hinduista
od	od	k7c2	od
zrození	zrození	k1gNnSc2	zrození
do	do	k7c2	do
zrození	zrození	k1gNnSc2	zrození
<g/>
.	.	kIx.	.
</s>
<s>
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
211	[number]	k4	211
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
Zbavitel	zbavitel	k1gMnSc1	zbavitel
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Hinduismus	hinduismus	k1gInSc4	hinduismus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901225	[number]	k4	901225
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbavitel	zbavitel	k1gMnSc1	zbavitel
<g/>
,	,	kIx,	,
D.	D.	kA	D.
(	(	kIx(	(
<g/>
hl.	hl.	k?	hl.
red	red	k?	red
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bozi	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
bráhmani	bráhman	k1gMnPc1	bráhman
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
ydání	ydání	k1gNnSc6	ydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbavitel	zbavitel	k1gMnSc1	zbavitel
<g/>
,	,	kIx,	,
D.	D.	kA	D.
&	&	k?	&
Vacek	Vacek	k1gMnSc1	Vacek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
dějinami	dějiny	k1gFnPc7	dějiny
staroindické	staroindický	k2eAgFnSc2d1	staroindická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Arca	Arcus	k1gMnSc4	Arcus
JiMfa	JiMf	k1gMnSc4	JiMf
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85766	[number]	k4	85766
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Óm	óm	k1gInSc1	óm
</s>
</p>
<p>
<s>
Indra	Indra	k1gMnSc1	Indra
</s>
</p>
<p>
<s>
Mahádéva	Mahádéva	k6eAd1	Mahádéva
</s>
</p>
<p>
<s>
Šiva	Šiva	k6eAd1	Šiva
</s>
</p>
<p>
<s>
Višnu	Višnout	k5eAaPmIp1nS	Višnout
</s>
</p>
<p>
<s>
Živá	živý	k2eAgFnSc1d1	živá
Etika	etika	k1gFnSc1	etika
</s>
</p>
<p>
<s>
Súrja	Súrja	k6eAd1	Súrja
</s>
</p>
<p>
<s>
Káma	Káma	k6eAd1	Káma
</s>
</p>
<p>
<s>
Ganéša	Ganéša	k6eAd1	Ganéša
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgNnPc1d1	související
náboženství	náboženství	k1gNnPc1	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
</s>
</p>
<p>
<s>
Džinismus	Džinismus	k1gInSc1	Džinismus
</s>
</p>
<p>
<s>
Jóga	jóga	k1gFnSc1	jóga
</s>
</p>
<p>
<s>
Sikhismus	Sikhismus	k1gInSc1	Sikhismus
</s>
</p>
<p>
<s>
Tantrismus	Tantrismus	k1gInSc1	Tantrismus
</s>
</p>
<p>
<s>
Teosofie	teosofie	k1gFnSc1	teosofie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hinduizmus	hinduizmus	k1gInSc1	hinduizmus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hinduismus	hinduismus	k1gInSc1	hinduismus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hinduismus	hinduismus	k1gInSc1	hinduismus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
hinduistická	hinduistický	k2eAgFnSc1d1	hinduistická
náboženská	náboženský	k2eAgFnSc1d1	náboženská
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hinduism	Hinduis	k1gNnSc7	Hinduis
Today	Todaa	k1gFnSc2	Todaa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Oxford	Oxford	k1gInSc1	Oxford
Centre	centr	k1gInSc5	centr
for	forum	k1gNnPc2	forum
Hindu	hind	k1gMnSc3	hind
Studies	Studies	k1gMnSc1	Studies
</s>
</p>
<p>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
(	(	kIx(	(
<g/>
Cyklus	cyklus	k1gInSc1	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Náboženství	náboženství	k1gNnSc1	náboženství
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
–	–	k?	–
video	video	k1gNnSc1	video
on-line	onin	k1gInSc5	on-lin
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
</s>
</p>
