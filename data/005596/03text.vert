<s>
Eufonie	eufonie	k1gFnSc1	eufonie
neboli	neboli	k8xC	neboli
libozvuk	libozvuk	k1gInSc1	libozvuk
či	či	k8xC	či
libozvučnost	libozvučnost	k1gFnSc1	libozvučnost
je	být	k5eAaImIp3nS	být
umělecky	umělecky	k6eAd1	umělecky
působivé	působivý	k2eAgNnSc1d1	působivé
uspořádání	uspořádání	k1gNnSc1	uspořádání
hlásek	hláska	k1gFnPc2	hláska
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
ve	v	k7c6	v
verši	verš	k1gInSc6	verš
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
opakování	opakování	k1gNnSc4	opakování
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
samohlásky	samohláska	k1gFnPc1	samohláska
a	a	k8xC	a
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
kakofonie	kakofonie	k1gFnSc1	kakofonie
neboli	neboli	k8xC	neboli
nelibozvučnost	nelibozvučnost	k1gFnSc1	nelibozvučnost
<g/>
.	.	kIx.	.
</s>
