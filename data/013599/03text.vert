<s>
Jozef	Jozef	k1gInSc4
Ulehla	ulehnout	k5eAaPmAgFnS
</s>
<s>
Jozef	Jozef	k1gMnSc1
UlehlaOsobní	UlehlaOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1922	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1975	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
obránce	obránce	k1gMnSc1
<g/>
,	,	kIx,
útočník	útočník	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mládežnické	mládežnický	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Seredský	Seredský	k2eAgInSc1d1
ŠK	ŠK	kA
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1942	#num#	k4
<g/>
–	–	k?
<g/>
19431945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
TŠS	TŠS	kA
Trnava	Trnava	k1gFnSc1
OAP	OAP	kA
Bratislava	Bratislava	k1gFnSc1
TŠS	TŠS	kA
Trnava	Trnava	k1gFnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jozef	Jozef	k1gMnSc1
Ulehla	ulehnout	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1922	#num#	k4
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nastupoval	nastupovat	k5eAaImAgMnS
i	i	k9
jako	jako	k9
obránce	obránce	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hráčská	hráčský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
dorosteneckém	dorostenecký	k2eAgInSc6d1
věku	věk	k1gInSc6
nastupoval	nastupovat	k5eAaImAgMnS
za	za	k7c4
Seredský	Seredský	k2eAgInSc4d1
ŠK	ŠK	kA
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Trnavy	Trnava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
války	válka	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
s	s	k7c7
OAP	OAP	kA
Bratislava	Bratislava	k1gFnSc1
mistrem	mistr	k1gMnSc7
Slovenska	Slovensko	k1gNnSc2
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Trnavský	trnavský	k2eAgInSc4d1
športový	športový	k2eAgInSc4d1
spolok	spolok	k1gInSc4
<g/>
,	,	kIx,
vstřelil	vstřelit	k5eAaPmAgMnS
pět	pět	k4xCc4
prvoligových	prvoligový	k2eAgFnPc2d1
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Prvoligová	prvoligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Minuty	minuta	k1gFnPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
4	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
TŠS	TŠS	kA
Trnava	Trnava	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
1	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
TŠS	TŠS	kA
Trnava	Trnava	k1gFnSc1
</s>
<s>
CELKEM	celek	k1gInSc7
I.	I.	kA
ČS	čs	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
LIGA	liga	k1gFnSc1
</s>
<s>
–	–	k?
</s>
<s>
5	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Jenšík	Jenšík	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
−	−	k?
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
20061	#num#	k4
2	#num#	k4
3	#num#	k4
Priezvisko	Priezvisko	k1gNnSc4
Ulehla	ulehnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
Meno	Meno	k6eAd1
Jozef	Jozef	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
cintoriny	cintorin	k1gInPc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NOSKOVIČ	NOSKOVIČ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Futbal	Futbal	k1gInSc1
v	v	k7c6
Seredi	Sered	k1gMnPc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sereď	Sereď	k1gFnPc2
<g/>
,	,	kIx,
2009-05-25	2009-05-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
História	Histórium	k1gNnSc2
trnavského	trnavský	k2eAgInSc2d1
futbalu	futbal	k1gInSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
stary	star	k1gInPc1
<g/>
.	.	kIx.
<g/>
spartak	spartak	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2007-09-30	2007-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Luboš	Luboš	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
v	v	k7c6
číslech	číslo	k1gNnPc6
a	a	k8xC
faktech	fakt	k1gInPc6
−	−	k?
Olympia	Olympia	k1gFnSc1
1991	#num#	k4
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
−	−	k?
Libri	Libri	k1gNnSc1
1997	#num#	k4
</s>
<s>
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Jenšík	Jenšík	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
−	−	k?
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
2006	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
História	Histórium	k1gNnPc1
trnavského	trnavský	k2eAgInSc2d1
futbalu	futbal	k1gInSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stary	star	k1gInPc1
<g/>
.	.	kIx.
<g/>
spartak	spartak	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
