<s>
Jaké	jaký	k3yRgNnSc1	jaký
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
letu	let	k1gInSc2	let
či	či	k8xC	či
plavby	plavba	k1gFnSc2	plavba
schopné	schopný	k2eAgFnSc2d1	schopná
ovládat	ovládat	k5eAaImF	ovládat
letící	letící	k2eAgInSc4d1	letící
stroje	stroj	k1gInPc4	stroj
nebo	nebo	k8xC	nebo
lodě	loď	k1gFnPc4	loď
bez	bez	k7c2	bez
asistence	asistence	k1gFnSc2	asistence
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
<g/>
?	?	kIx.	?
</s>
