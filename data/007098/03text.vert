<s>
Autopilot	autopilot	k1gMnSc1	autopilot
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
letu	let	k1gInSc2	let
či	či	k8xC	či
plavby	plavba	k1gFnSc2	plavba
schopné	schopný	k2eAgFnSc2d1	schopná
ovládat	ovládat	k5eAaImF	ovládat
létající	létající	k2eAgInSc4d1	létající
stroje	stroj	k1gInPc4	stroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc1	vrtulník
<g/>
,	,	kIx,	,
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lodě	loď	k1gFnPc1	loď
bez	bez	k7c2	bez
asistence	asistence	k1gFnSc2	asistence
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
mechanické	mechanický	k2eAgNnSc4d1	mechanické
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc4d1	elektrické
<g/>
,	,	kIx,	,
elektronické	elektronický	k2eAgNnSc4d1	elektronické
<g/>
,	,	kIx,	,
hydraulické	hydraulický	k2eAgNnSc4d1	hydraulické
nebo	nebo	k8xC	nebo
kombinace	kombinace	k1gFnSc2	kombinace
uvedených	uvedený	k2eAgNnPc2d1	uvedené
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
letecký	letecký	k2eAgMnSc1d1	letecký
autopilot	autopilot	k1gMnSc1	autopilot
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
Američan	Američan	k1gMnSc1	Američan
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Sperry	Sperra	k1gMnSc2	Sperra
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
samotné	samotný	k2eAgNnSc1d1	samotné
představení	představení	k1gNnSc1	představení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
tankeru	tanker	k1gInSc6	tanker
J.A	J.A	k1gMnSc1	J.A
Moffet	Moffet	k1gMnSc1	Moffet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
a	a	k8xC	a
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
používají	používat	k5eAaImIp3nP	používat
počítačem	počítač	k1gInSc7	počítač
řízené	řízený	k2eAgInPc4d1	řízený
systémy	systém	k1gInPc4	systém
schopné	schopný	k2eAgNnSc1d1	schopné
zajistit	zajistit	k5eAaPmF	zajistit
automatický	automatický	k2eAgInSc4d1	automatický
let	let	k1gInSc4	let
letadla	letadlo	k1gNnSc2	letadlo
nebo	nebo	k8xC	nebo
plavbu	plavba	k1gFnSc4	plavba
lodě	loď	k1gFnPc1	loď
po	po	k7c6	po
zvolené	zvolený	k2eAgFnSc6d1	zvolená
trajektorii	trajektorie	k1gFnSc6	trajektorie
a	a	k8xC	a
trase	trasa	k1gFnSc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
aktivně	aktivně	k6eAd1	aktivně
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
řízení	řízení	k1gNnSc2	řízení
letadel	letadlo	k1gNnPc2	letadlo
nebo	nebo	k8xC	nebo
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
polohy	poloha	k1gFnSc2	poloha
stroje	stroj	k1gInSc2	stroj
využívají	využívat	k5eAaImIp3nP	využívat
pozemních	pozemní	k2eAgFnPc2d1	pozemní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
radiomajáky	radiomaják	k1gInPc1	radiomaják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palubních	palubní	k2eAgFnPc2d1	palubní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
inerciální	inerciální	k2eAgFnSc1d1	inerciální
navigace	navigace	k1gFnSc1	navigace
<g/>
,	,	kIx,	,
kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
výškoměr	výškoměr	k1gInSc1	výškoměr
<g/>
)	)	kIx)	)
i	i	k8xC	i
satelitních	satelitní	k2eAgInPc2d1	satelitní
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
GPS	GPS	kA	GPS
<g/>
,	,	kIx,	,
GLONASS	GLONASS	kA	GLONASS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tempomat	Tempomat	k5eAaPmF	Tempomat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Autopilot	autopilot	k1gMnSc1	autopilot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
autopilot	autopilot	k1gMnSc1	autopilot
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
