<s>
Autopilot	autopilot	k1gMnSc1	autopilot
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
letu	let	k1gInSc2	let
či	či	k8xC	či
plavby	plavba	k1gFnSc2	plavba
schopné	schopný	k2eAgFnSc2d1	schopná
ovládat	ovládat	k5eAaImF	ovládat
létající	létající	k2eAgInSc4d1	létající
stroje	stroj	k1gInPc4	stroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc1	vrtulník
<g/>
,	,	kIx,	,
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lodě	loď	k1gFnPc1	loď
bez	bez	k7c2	bez
asistence	asistence	k1gFnSc2	asistence
lidské	lidský	k2eAgFnSc2d1	lidská
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
