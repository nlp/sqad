<s>
Recenze	recenze	k1gFnSc1	recenze
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
re-censeó	reenseó	k?	re-censeó
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
znovu	znovu	k6eAd1	znovu
<g/>
)	)	kIx)	)
posoudit	posoudit	k5eAaPmF	posoudit
<g/>
,	,	kIx,	,
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
písemný	písemný	k2eAgInSc1d1	písemný
kritický	kritický	k2eAgInSc1d1	kritický
posudek	posudek	k1gInSc1	posudek
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
nebo	nebo	k8xC	nebo
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
recenze	recenze	k1gFnSc2	recenze
je	být	k5eAaImIp3nS	být
recenzent	recenzent	k1gMnSc1	recenzent
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
publikaci	publikace	k1gFnSc3	publikace
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
naopak	naopak	k6eAd1	naopak
jako	jako	k9	jako
podklad	podklad	k1gInSc1	podklad
recenzního	recenzní	k2eAgNnSc2d1	recenzní
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
o	o	k7c6	o
publikaci	publikace	k1gFnSc6	publikace
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
je	být	k5eAaImIp3nS	být
publicistický	publicistický	k2eAgInSc4d1	publicistický
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
širší	široký	k2eAgFnSc4d2	širší
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
literárním	literární	k2eAgNnSc7d1	literární
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
divadelním	divadelní	k2eAgNnSc7d1	divadelní
představením	představení	k1gNnSc7	představení
<g/>
,	,	kIx,	,
koncertem	koncert	k1gInSc7	koncert
<g/>
,	,	kIx,	,
výstavou	výstava	k1gFnSc7	výstava
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c4	na
významné	významný	k2eAgFnPc4d1	významná
vědecké	vědecký	k2eAgFnPc4d1	vědecká
publikace	publikace	k1gFnPc4	publikace
nebo	nebo	k8xC	nebo
objevy	objev	k1gInPc1	objev
se	se	k3xPyFc4	se
publikují	publikovat	k5eAaBmIp3nP	publikovat
recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	být	k5eAaImIp3nS	být
věcně	věcně	k6eAd1	věcně
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
a	a	k8xC	a
popularizují	popularizovat	k5eAaImIp3nP	popularizovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
bývají	bývat	k5eAaImIp3nP	bývat
recenzovány	recenzován	k2eAgInPc1d1	recenzován
také	také	k9	také
nově	nově	k6eAd1	nově
publikované	publikovaný	k2eAgInPc4d1	publikovaný
psychologické	psychologický	k2eAgInPc4d1	psychologický
testy	test	k1gInPc4	test
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
například	například	k6eAd1	například
Burosův	Burosův	k2eAgInSc1d1	Burosův
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
psychologická	psychologický	k2eAgFnSc1d1	psychologická
společnost	společnost	k1gFnSc1	společnost
nebo	nebo	k8xC	nebo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
časopis	časopis	k1gInSc1	časopis
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
psychologické	psychologický	k2eAgFnSc2d1	psychologická
společnosti	společnost	k1gFnSc2	společnost
Testfórum	Testfórum	k1gNnSc1	Testfórum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náročnějších	náročný	k2eAgInPc6d2	náročnější
odborných	odborný	k2eAgInPc6d1	odborný
a	a	k8xC	a
vědeckých	vědecký	k2eAgInPc6d1	vědecký
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
uveřejňovat	uveřejňovat	k5eAaImF	uveřejňovat
jen	jen	k9	jen
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
každý	každý	k3xTgInSc1	každý
došlý	došlý	k2eAgInSc1d1	došlý
příspěvek	příspěvek	k1gInSc1	příspěvek
recenzním	recenzní	k2eAgNnSc7d1	recenzní
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
současném	současný	k2eAgNnSc6d1	současné
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
šíři	šíř	k1gFnSc6	šíř
vědeckých	vědecký	k2eAgInPc2d1	vědecký
i	i	k8xC	i
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
totiž	totiž	k9	totiž
sami	sám	k3xTgMnPc1	sám
redaktoři	redaktor	k1gMnPc1	redaktor
často	často	k6eAd1	často
nemohou	moct	k5eNaImIp3nP	moct
posoudit	posoudit	k5eAaPmF	posoudit
kvalitu	kvalita	k1gFnSc4	kvalita
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
originalitu	originalita	k1gFnSc4	originalita
a	a	k8xC	a
případný	případný	k2eAgInSc4d1	případný
přínos	přínos	k1gInSc4	přínos
<g/>
.	.	kIx.	.
</s>
<s>
Postupují	postupovat	k5eAaImIp3nP	postupovat
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
zásady	zásada	k1gFnSc2	zásada
peer	peer	k1gMnSc1	peer
review	review	k?	review
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
příspěvek	příspěvek	k1gInSc4	příspěvek
bez	bez	k7c2	bez
uvedení	uvedení	k1gNnSc2	uvedení
autora	autor	k1gMnSc4	autor
posoudit	posoudit	k5eAaPmF	posoudit
nezávislým	závislý	k2eNgMnPc3d1	nezávislý
odborníkům	odborník	k1gMnPc3	odborník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
sami	sám	k3xTgMnPc1	sám
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
recenzenta	recenzent	k1gMnSc2	recenzent
je	být	k5eAaImIp3nS	být
posoudit	posoudit	k5eAaPmF	posoudit
odbornou	odborný	k2eAgFnSc4d1	odborná
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
původnost	původnost	k1gFnSc4	původnost
i	i	k8xC	i
zpracování	zpracování	k1gNnSc4	zpracování
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
,	,	kIx,	,
připomenout	připomenout	k5eAaPmF	připomenout
jeho	jeho	k3xOp3gInPc4	jeho
nedostatky	nedostatek	k1gInPc4	nedostatek
a	a	k8xC	a
příspěvek	příspěvek	k1gInSc4	příspěvek
doporučit	doporučit	k5eAaPmF	doporučit
nebo	nebo	k8xC	nebo
nedoporučit	doporučit	k5eNaPmF	doporučit
k	k	k7c3	k
publikaci	publikace	k1gFnSc3	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Recenzent	recenzent	k1gMnSc1	recenzent
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
blízkým	blízký	k2eAgMnSc7d1	blízký
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
tomtéž	týž	k3xTgNnSc6	týž
pracovišti	pracoviště	k1gNnSc6	pracoviště
atd.	atd.	kA	atd.
Připomínky	připomínka	k1gFnSc2	připomínka
recenzenta	recenzent	k1gMnSc2	recenzent
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
anonymně	anonymně	k6eAd1	anonymně
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
autorovi	autorův	k2eAgMnPc1d1	autorův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
článek	článek	k1gInSc1	článek
případně	případně	k6eAd1	případně
opravil	opravit	k5eAaPmAgInS	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
peer	peer	k1gMnSc1	peer
review	review	k?	review
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
publikace	publikace	k1gFnSc1	publikace
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
recenzent	recenzent	k1gMnSc1	recenzent
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
článku	článek	k1gInSc6	článek
sám	sám	k3xTgMnSc1	sám
zneužít	zneužít	k5eAaPmF	zneužít
a	a	k8xC	a
publikovat	publikovat	k5eAaBmF	publikovat
pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
jazykových	jazykový	k2eAgFnPc6d1	jazyková
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
oboru	obor	k1gInSc6	obor
znají	znát	k5eAaImIp3nP	znát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
anonymita	anonymita	k1gFnSc1	anonymita
příspěvku	příspěvek	k1gInSc2	příspěvek
i	i	k9	i
recenze	recenze	k1gFnSc1	recenze
často	často	k6eAd1	často
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
i	i	k9	i
posudky	posudek	k1gInPc1	posudek
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
postup	postup	k1gInSc4	postup
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
grantové	grantový	k2eAgFnPc1d1	Grantová
agentury	agentura	k1gFnPc1	agentura
při	při	k7c6	při
hodnocení	hodnocení	k1gNnSc6	hodnocení
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
