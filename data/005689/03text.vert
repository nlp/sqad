<s>
Hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
cizojazyčně	cizojazyčně	k6eAd1	cizojazyčně
a	a	k8xC	a
odborně	odborně	k6eAd1	odborně
též	též	k9	též
kastel	kastel	k1gInSc1	kastel
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
castellum	castellum	k1gInSc1	castellum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opevněné	opevněný	k2eAgNnSc1d1	opevněné
feudální	feudální	k2eAgNnSc1d1	feudální
sídlo	sídlo	k1gNnSc1	sídlo
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
většinou	většina	k1gFnSc7	většina
v	v	k7c4	v
rozmezí	rozmezí	k1gNnSc4	rozmezí
jedenáctého	jedenáctý	k4xOgMnSc2	jedenáctý
až	až	k8xS	až
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
"	"	kIx"	"
<g/>
raně	raně	k6eAd1	raně
středověký	středověký	k2eAgInSc1d1	středověký
hrad	hrad	k1gInSc1	hrad
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
raně	raně	k6eAd1	raně
středověká	středověký	k2eAgNnPc4d1	středověké
hradiště	hradiště	k1gNnPc4	hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
hradem	hrad	k1gInSc7	hrad
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hradů	hrad	k1gInPc2	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
hradů	hrad	k1gInPc2	hrad
rozdělené	rozdělená	k1gFnSc2	rozdělená
podle	podle	k7c2	podle
rozmístění	rozmístění	k1gNnSc2	rozmístění
a	a	k8xC	a
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
vztahu	vztah	k1gInSc2	vztah
budov	budova	k1gFnPc2	budova
<g/>
:	:	kIx,	:
Hrad	hrad	k1gInSc1	hrad
přechodného	přechodný	k2eAgInSc2d1	přechodný
typu	typ	k1gInSc2	typ
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
hradů	hrad	k1gInPc2	hrad
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
mezi	mezi	k7c7	mezi
raně	raně	k6eAd1	raně
středověkými	středověký	k2eAgNnPc7d1	středověké
hradišti	hradiště	k1gNnPc7	hradiště
a	a	k8xC	a
vrcholně	vrcholně	k6eAd1	vrcholně
středověkými	středověký	k2eAgInPc7d1	středověký
hrady	hrad	k1gInPc7	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
představovaly	představovat	k5eAaImAgInP	představovat
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
plnohodnotné	plnohodnotný	k2eAgInPc4d1	plnohodnotný
královské	královský	k2eAgInPc4d1	královský
hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
mísení	mísení	k1gNnSc4	mísení
prvků	prvek	k1gInPc2	prvek
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
hradišť	hradiště	k1gNnPc2	hradiště
i	i	k8xC	i
hradů	hrad	k1gInPc2	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
valové	valový	k2eAgNnSc4d1	valové
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
val	val	k1gInSc1	val
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hradišť	hradiště	k1gNnPc2	hradiště
tvořen	tvořit	k5eAaImNgInS	tvořit
pouze	pouze	k6eAd1	pouze
náspem	násep	k1gInSc7	násep
a	a	k8xC	a
čelní	čelní	k2eAgFnSc7d1	čelní
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
Tomášem	Tomáš	k1gMnSc7	Tomáš
Durdíkem	Durdík	k1gInSc7	Durdík
prozkoumané	prozkoumaný	k2eAgFnPc1d1	prozkoumaná
lokality	lokalita	k1gFnPc1	lokalita
Angerbach	Angerbacha	k1gFnPc2	Angerbacha
u	u	k7c2	u
Kožlan	Kožlana	k1gFnPc2	Kožlana
a	a	k8xC	a
Hlavačov	Hlavačovo	k1gNnPc2	Hlavačovo
u	u	k7c2	u
Rakovníka	Rakovník	k1gInSc2	Rakovník
v	v	k7c6	v
Přemyslovském	přemyslovský	k2eAgInSc6d1	přemyslovský
loveckém	lovecký	k2eAgInSc6d1	lovecký
hvozdu	hvozd	k1gInSc6	hvozd
<g/>
,	,	kIx,	,
Tachov	Tachov	k1gInSc1	Tachov
<g/>
,	,	kIx,	,
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Některými	některý	k3yIgMnPc7	některý
historiky	historik	k1gMnPc7	historik
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
hradu	hrad	k1gInSc2	hrad
přechodného	přechodný	k2eAgInSc2d1	přechodný
typu	typ	k1gInSc2	typ
zpochybňována	zpochybňován	k2eAgFnSc1d1	zpochybňována
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
obvodovou	obvodový	k2eAgFnSc7d1	obvodová
zástavbou	zástavba	k1gFnSc7	zástavba
–	–	k?	–
rozměrný	rozměrný	k2eAgInSc1d1	rozměrný
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vícedílný	vícedílný	k2eAgInSc1d1	vícedílný
typ	typ	k1gInSc1	typ
královského	královský	k2eAgInSc2d1	královský
hradu	hrad	k1gInSc2	hrad
rozvíjený	rozvíjený	k2eAgInSc4d1	rozvíjený
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgFnPc1d1	hradní
budovy	budova	k1gFnPc1	budova
tvořily	tvořit	k5eAaImAgFnP	tvořit
některými	některý	k3yIgFnPc7	některý
svými	svůj	k3xOyFgMnPc7	svůj
stranami	strana	k1gFnPc7	strana
součást	součást	k1gFnSc4	součást
opevnění	opevnění	k1gNnPc4	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
obvykle	obvykle	k6eAd1	obvykle
mívaly	mívat	k5eAaImAgInP	mívat
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
věží	věžit	k5eAaImIp3nS	věžit
bez	bez	k7c2	bez
schopnosti	schopnost	k1gFnSc2	schopnost
flankování	flankování	k1gNnSc2	flankování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
hradu	hrad	k1gInSc2	hrad
stává	stávat	k5eAaImIp3nS	stávat
také	také	k9	také
donjon	donjon	k1gInSc1	donjon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Zvíkov	Zvíkov	k1gInSc1	Zvíkov
<g/>
,	,	kIx,	,
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
<g/>
,	,	kIx,	,
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Osek	Osek	k1gInSc1	Osek
<g/>
,	,	kIx,	,
Bezděz	Bezděz	k1gInSc1	Bezděz
<g/>
,	,	kIx,	,
Příběnice	Příběnice	k1gFnSc1	Příběnice
(	(	kIx(	(
<g/>
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
<g/>
)	)	kIx)	)
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
bergfritového	bergfritový	k2eAgInSc2d1	bergfritový
typu	typ	k1gInSc2	typ
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
stavby	stavba	k1gFnSc2	stavba
zejména	zejména	k9	zejména
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
jako	jako	k8xS	jako
například	například	k6eAd1	například
hrad	hrad	k1gInSc1	hrad
Zbiroh	Zbiroh	k1gInSc1	Zbiroh
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
stávala	stávat	k5eAaImAgFnS	stávat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
hradu	hrad	k1gInSc2	hrad
útočištná	útočištný	k2eAgFnSc1d1	útočištná
věž	věž	k1gFnSc1	věž
–	–	k?	–
bergfrit	bergfrita	k1gFnPc2	bergfrita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
poslední	poslední	k2eAgNnSc4d1	poslední
útočiště	útočiště	k1gNnSc4	útočiště
obránců	obránce	k1gMnPc2	obránce
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
převážně	převážně	k6eAd1	převážně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
můstku	můstek	k1gInSc6	můstek
nebo	nebo	k8xC	nebo
žebříku	žebřík	k1gInSc6	žebřík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
obránci	obránce	k1gMnPc1	obránce
mohli	moct	k5eAaImAgMnP	moct
snadno	snadno	k6eAd1	snadno
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
základní	základní	k2eAgInSc1d1	základní
typ	typ	k1gInSc1	typ
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
hradu	hrad	k1gInSc2	hrad
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
Hasištejn	Hasištejn	k1gInSc1	Hasištejn
<g/>
,	,	kIx,	,
Krašov	Krašov	k1gInSc1	Krašov
(	(	kIx(	(
<g/>
bergfrit	bergfrit	k1gInSc1	bergfrit
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
věžicí	věžicí	k?	věžicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kokořín	Kokořín	k1gInSc1	Kokořín
<g/>
,	,	kIx,	,
Šelmberk	Šelmberk	k1gInSc1	Šelmberk
<g/>
,	,	kIx,	,
Skalka	skalka	k1gFnSc1	skalka
Hrad	hrad	k1gInSc1	hrad
donjonového	donjonový	k2eAgInSc2d1	donjonový
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Rabí	rabí	k1gMnSc1	rabí
<g/>
,	,	kIx,	,
Čejchanov	Čejchanov	k1gInSc1	Čejchanov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
čtverhrannou	čtverhranný	k2eAgFnSc7d1	čtverhranná
obytnou	obytný	k2eAgFnSc7d1	obytná
věží	věž	k1gFnSc7	věž
obehnanou	obehnaný	k2eAgFnSc7d1	obehnaná
hradbou	hradba	k1gFnSc7	hradba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
celý	celý	k2eAgInSc1d1	celý
hrad	hrad	k1gInSc1	hrad
skládat	skládat	k5eAaImF	skládat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
věže	věž	k1gFnSc2	věž
bez	bez	k7c2	bez
opevnění	opevnění	k1gNnSc2	opevnění
(	(	kIx(	(
<g/>
Kunžvart	Kunžvart	k1gInSc1	Kunžvart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvinutější	rozvinutý	k2eAgFnSc6d2	rozvinutější
podobě	podoba	k1gFnSc6	podoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
věž	věž	k1gFnSc1	věž
doplněna	doplněn	k2eAgFnSc1d1	doplněna
dalšími	další	k2eAgFnPc7d1	další
stavbami	stavba	k1gFnPc7	stavba
(	(	kIx(	(
<g/>
Kozí	kozí	k2eAgInSc1d1	kozí
hrádek	hrádek	k1gInSc1	hrádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
kastelového	kastelový	k2eAgInSc2d1	kastelový
typu	typ	k1gInSc2	typ
–	–	k?	–
hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
půdorysem	půdorys	k1gInSc7	půdorys
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
nebo	nebo	k8xC	nebo
obdélníkový	obdélníkový	k2eAgInSc1d1	obdélníkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
věží	věž	k1gFnPc2	věž
a	a	k8xC	a
obvodovou	obvodový	k2eAgFnSc7d1	obvodová
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
importovaný	importovaný	k2eAgInSc4d1	importovaný
francouzský	francouzský	k2eAgInSc4d1	francouzský
kastel	kastel	k1gInSc4	kastel
(	(	kIx(	(
<g/>
Týřov	Týřov	k1gInSc4	Týřov
<g/>
,	,	kIx,	,
Konopiště	Konopiště	k1gNnSc4	Konopiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
domácí	domácí	k2eAgInSc4d1	domácí
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
kastel	kastel	k1gInSc4	kastel
(	(	kIx(	(
<g/>
Písek	Písek	k1gInSc4	Písek
<g/>
,	,	kIx,	,
Kadaň	Kadaň	k1gFnSc4	Kadaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
plášťovou	plášťový	k2eAgFnSc7d1	plášťová
zdí	zeď	k1gFnSc7	zeď
–	–	k?	–
typ	typ	k1gInSc4	typ
bezvěžového	bezvěžový	k2eAgInSc2d1	bezvěžový
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
obranným	obranný	k2eAgInSc7d1	obranný
prvkem	prvek	k1gInSc7	prvek
silná	silný	k2eAgFnSc1d1	silná
plášťová	plášťový	k2eAgFnSc1d1	plášťová
zeď	zeď	k1gFnSc1	zeď
s	s	k7c7	s
průběžným	průběžný	k2eAgInSc7d1	průběžný
ochozem	ochoz	k1gInSc7	ochoz
<g/>
,	,	kIx,	,
za	za	k7c7	za
níž	jenž	k3xRgFnSc7	jenž
jsou	být	k5eAaImIp3nP	být
skryty	skryt	k2eAgFnPc1d1	skryta
budovy	budova	k1gFnPc1	budova
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
hrady	hrad	k1gInPc1	hrad
Lanšperk	Lanšperk	k1gInSc1	Lanšperk
a	a	k8xC	a
Košumberk	Košumberk	k1gInSc1	Košumberk
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
palácem	palác	k1gInSc7	palác
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc7d1	hlavní
obrannou	obranný	k2eAgFnSc7d1	obranná
i	i	k8xC	i
obytnou	obytný	k2eAgFnSc7d1	obytná
stavbou	stavba	k1gFnSc7	stavba
–	–	k?	–
úsporný	úsporný	k2eAgInSc1d1	úsporný
typ	typ	k1gInSc1	typ
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
palác	palác	k1gInSc4	palác
s	s	k7c7	s
přihrazeným	přihrazený	k2eAgNnSc7d1	přihrazený
nádvořím	nádvoří	k1gNnSc7	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
zástupci	zástupce	k1gMnPc7	zástupce
jsou	být	k5eAaImIp3nP	být
hrady	hrad	k1gInPc1	hrad
Řebřík	řebřík	k1gInSc1	řebřík
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Žeberk	Žeberk	k1gInSc1	Žeberk
<g/>
,	,	kIx,	,
Tetín	Tetín	k1gInSc1	Tetín
nebo	nebo	k8xC	nebo
Vlčtejn	Vlčtejn	k1gInSc1	Vlčtejn
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
blokového	blokový	k2eAgInSc2d1	blokový
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
bloková	blokový	k2eAgFnSc1d1	bloková
dispozice	dispozice	k1gFnSc1	dispozice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc1d1	tvořený
donjonem	donjon	k1gInSc7	donjon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přímo	přímo	k6eAd1	přímo
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
palácová	palácový	k2eAgNnPc4d1	palácové
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
obvykle	obvykle	k6eAd1	obvykle
malé	malý	k2eAgNnSc4d1	malé
nádvoří	nádvoří	k1gNnSc4	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
ukázky	ukázka	k1gFnPc4	ukázka
tohoto	tento	k3xDgInSc2	tento
hradního	hradní	k2eAgInSc2d1	hradní
typu	typ	k1gInSc2	typ
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nS	patřit
Děvín	Děvín	k1gInSc1	Děvín
nebo	nebo	k8xC	nebo
Zlenice	Zlenice	k1gFnSc1	Zlenice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rozsáhlejších	rozsáhlý	k2eAgFnPc2d2	rozsáhlejší
staveb	stavba	k1gFnPc2	stavba
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
obytná	obytný	k2eAgFnSc1d1	obytná
věž	věž	k1gFnSc1	věž
zdvojena	zdvojen	k2eAgFnSc1d1	zdvojena
(	(	kIx(	(
<g/>
Libštejn	Libštejno	k1gNnPc2	Libštejno
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nebo	nebo	k8xC	nebo
zástavba	zástavba	k1gFnSc1	zástavba
hradního	hradní	k2eAgNnSc2d1	hradní
jádra	jádro	k1gNnSc2	jádro
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
rozvolněna	rozvolněn	k2eAgFnSc1d1	rozvolněn
(	(	kIx(	(
<g/>
Okoř	Okoř	k1gFnSc1	Okoř
<g/>
,	,	kIx,	,
Kost	kost	k1gFnSc1	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
možné	možný	k2eAgNnSc1d1	možné
členění	členění	k1gNnSc1	členění
hradů	hrad	k1gInPc2	hrad
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
stavebníka	stavebník	k1gMnSc2	stavebník
na	na	k7c4	na
hrady	hrad	k1gInPc4	hrad
královské	královský	k2eAgInPc4d1	královský
a	a	k8xC	a
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
typy	typ	k1gInPc7	typ
hradů	hrad	k1gInPc2	hrad
jsou	být	k5eAaImIp3nP	být
hrady	hrad	k1gInPc1	hrad
skalní	skalní	k2eAgInPc1d1	skalní
nebo	nebo	k8xC	nebo
vodní	vodní	k2eAgInPc1d1	vodní
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
hradby	hradba	k1gFnPc1	hradba
byly	být	k5eAaImAgFnP	být
obklopené	obklopený	k2eAgInPc4d1	obklopený
příkopy	příkop	k1gInPc4	příkop
naplněnými	naplněný	k2eAgFnPc7d1	naplněná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bývaly	bývat	k5eAaImAgFnP	bývat
stavěny	stavit	k5eAaImNgFnP	stavit
v	v	k7c6	v
rovinatém	rovinatý	k2eAgInSc6d1	rovinatý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
Švihov	Švihov	k1gInSc4	Švihov
<g/>
,	,	kIx,	,
Blatná	blatný	k2eAgFnSc1d1	Blatná
a	a	k8xC	a
Okoř	Okoř	k1gFnSc1	Okoř
nebo	nebo	k8xC	nebo
Loevestein	Loevestein	k1gInSc1	Loevestein
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prvotní	prvotní	k2eAgFnSc4d1	prvotní
fázi	fáze	k1gFnSc4	fáze
hradů	hrad	k1gInPc2	hrad
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
raně	raně	k6eAd1	raně
středověké	středověký	k2eAgInPc4d1	středověký
hrady	hrad	k1gInPc4	hrad
označované	označovaný	k2eAgInPc4d1	označovaný
nepřesně	přesně	k6eNd1	přesně
jako	jako	k9	jako
hradiště	hradiště	k1gNnSc2	hradiště
(	(	kIx(	(
<g/>
hradiska	hradisko	k1gNnSc2	hradisko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
opevněné	opevněný	k2eAgFnSc2d1	opevněná
pomocí	pomoc	k1gFnSc7	pomoc
zemního	zemní	k2eAgInSc2d1	zemní
valu	val	k1gInSc2	val
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
konstrukci	konstrukce	k1gFnSc4	konstrukce
od	od	k7c2	od
prostého	prostý	k2eAgNnSc2d1	prosté
sypaného	sypané	k1gNnSc2	sypané
přes	přes	k7c4	přes
roštovou	roštový	k2eAgFnSc4d1	roštová
až	až	k6eAd1	až
po	po	k7c4	po
komorovou	komorový	k2eAgFnSc4d1	komorová
(	(	kIx(	(
<g/>
srubovou	srubový	k2eAgFnSc4d1	srubová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byl	být	k5eAaImAgInS	být
osazen	osadit	k5eAaPmNgInS	osadit
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
palisádou	palisáda	k1gFnSc7	palisáda
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zpevněna	zpevnit	k5eAaPmNgFnS	zpevnit
kamennou	kamenný	k2eAgFnSc7d1	kamenná
plentou	plenta	k1gFnSc7	plenta
(	(	kIx(	(
<g/>
na	na	k7c4	na
sucho	sucho	k1gNnSc4	sucho
postavenou	postavený	k2eAgFnSc7d1	postavená
zdí	zeď	k1gFnSc7	zeď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vojensky	vojensky	k6eAd1	vojensky
exponovaných	exponovaný	k2eAgNnPc6d1	exponované
místech	místo	k1gNnPc6	místo
byl	být	k5eAaImAgMnS	být
většinou	většinou	k6eAd1	většinou
před	před	k7c4	před
val	val	k1gInSc4	val
předsazen	předsazen	k2eAgInSc4d1	předsazen
příkop	příkop	k1gInSc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
hradiště	hradiště	k1gNnSc2	hradiště
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
prostor	prostor	k1gInSc1	prostor
hradiště	hradiště	k1gNnSc2	hradiště
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
členěn	členit	k5eAaImNgInS	členit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
nejchráněnějším	chráněný	k2eAgNnSc6d3	chráněný
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
akropole	akropole	k1gFnSc1	akropole
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
majitele	majitel	k1gMnSc2	majitel
nebo	nebo	k8xC	nebo
správce	správce	k1gMnSc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
osídlení	osídlení	k1gNnSc2	osídlení
se	se	k3xPyFc4	se
hradiště	hradiště	k1gNnPc1	hradiště
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
útočištná	útočištný	k2eAgNnPc4d1	útočištný
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
místo	místo	k7c2	místo
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nouze	nouze	k1gFnSc2	nouze
bez	bez	k7c2	bez
trvalého	trvalý	k2eAgNnSc2d1	trvalé
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
osídlená	osídlený	k2eAgFnSc1d1	osídlená
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k6eAd1	až
rozměrů	rozměr	k1gInPc2	rozměr
prvotních	prvotní	k2eAgNnPc2d1	prvotní
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
se	se	k3xPyFc4	se
hradiště	hradiště	k1gNnSc1	hradiště
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
výšinná	výšinný	k2eAgNnPc4d1	výšinné
<g/>
,	,	kIx,	,
nížinná	nížinný	k2eAgNnPc1d1	nížinné
<g/>
,	,	kIx,	,
blatná	blatný	k2eAgNnPc1d1	Blatné
nebo	nebo	k8xC	nebo
ostrožná	ostrožný	k2eAgNnPc1d1	ostrožný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInPc4	první
kamenné	kamenný	k2eAgInPc4d1	kamenný
hrady	hrad	k1gInPc4	hrad
začal	začít	k5eAaPmAgMnS	začít
stavět	stavět	k5eAaImF	stavět
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patrný	patrný	k2eAgInSc1d1	patrný
vývoj	vývoj	k1gInSc1	vývoj
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zámku	zámek	k1gInSc3	zámek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
palác	palác	k1gInSc1	palác
není	být	k5eNaImIp3nS	být
stavěn	stavit	k5eAaImNgInS	stavit
na	na	k7c6	na
nejchráněnějším	chráněný	k2eAgNnSc6d3	chráněný
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
přední	přední	k2eAgFnSc2d1	přední
linie	linie	k1gFnSc2	linie
(	(	kIx(	(
<g/>
Točník	Točník	k1gInSc1	Točník
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
hrad	hrad	k1gInSc1	hrad
u	u	k7c2	u
Kunratic	Kunratice	k1gFnPc2	Kunratice
<g/>
,	,	kIx,	,
Krakovec	krakovec	k1gInSc1	krakovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
husitskými	husitský	k2eAgFnPc7d1	husitská
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
vojenské	vojenský	k2eAgFnPc4d1	vojenská
události	událost	k1gFnPc4	událost
a	a	k8xC	a
masivní	masivní	k2eAgNnPc4d1	masivní
nasazení	nasazení	k1gNnPc4	nasazení
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
tendencím	tendence	k1gFnPc3	tendence
–	–	k?	–
uchýlení	uchýlení	k1gNnSc1	uchýlení
se	se	k3xPyFc4	se
na	na	k7c4	na
nedostupné	dostupný	k2eNgFnPc4d1	nedostupná
výšiny	výšina	k1gFnPc4	výšina
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
prvních	první	k4xOgFnPc2	první
dělostřeleckých	dělostřelecký	k2eAgFnPc2d1	dělostřelecká
věží	věž	k1gFnPc2	věž
a	a	k8xC	a
bašt	bašta	k1gFnPc2	bašta
až	až	k6eAd1	až
po	po	k7c6	po
znovuzavedení	znovuzavedení	k1gNnSc6	znovuzavedení
zemních	zemní	k2eAgInPc2d1	zemní
valů	val	k1gInPc2	val
jako	jako	k8xS	jako
prvních	první	k4xOgInPc2	první
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Českých	český	k2eAgInPc2d1	český
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
736	[number]	k4	736
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
PLAČEK	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
moravských	moravský	k2eAgInPc2d1	moravský
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
hrádků	hrádek	k1gInPc2	hrádek
a	a	k8xC	a
tvrzí	tvrz	k1gFnPc2	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
768	[number]	k4	768
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
PLAČEK	Plaček	k1gMnSc1	Plaček
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
;	;	kIx,	;
BÓNA	BÓNA	kA	BÓNA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
slovenských	slovenský	k2eAgInPc2d1	slovenský
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
392	[number]	k4	392
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
333	[number]	k4	333
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
tvrzí	tvrz	k1gFnPc2	tvrz
a	a	k8xC	a
zřícenin	zřícenina	k1gFnPc2	zřícenina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Obléhání	obléhání	k1gNnSc2	obléhání
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hrad	hrad	k1gInSc1	hrad
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hrad	hrad	k1gInSc1	hrad
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Podrobný	podrobný	k2eAgInSc4d1	podrobný
seznam	seznam	k1gInSc4	seznam
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
tvrzí	tvrz	k1gFnPc2	tvrz
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Hrady	hrad	k1gInPc4	hrad
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Hrady	hrad	k1gInPc4	hrad
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
</s>
