<s>
Hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
cizojazyčně	cizojazyčně	k6eAd1	cizojazyčně
a	a	k8xC	a
odborně	odborně	k6eAd1	odborně
též	též	k9	též
kastel	kastel	k1gInSc1	kastel
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
castellum	castellum	k1gInSc1	castellum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opevněné	opevněný	k2eAgNnSc1d1	opevněné
feudální	feudální	k2eAgNnSc1d1	feudální
sídlo	sídlo	k1gNnSc1	sídlo
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
většinou	většina	k1gFnSc7	většina
v	v	k7c4	v
rozmezí	rozmezí	k1gNnSc4	rozmezí
jedenáctého	jedenáctý	k4xOgMnSc2	jedenáctý
až	až	k8xS	až
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
