<s>
Šilka	Šilka	k6eAd1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
ruské	ruský	k2eAgFnSc6d1
řece	řeka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
sovětském	sovětský	k2eAgInSc6d1
protiletadlovém	protiletadlový	k2eAgInSc6d1
systému	systém	k1gInSc6
„	„	k?
<g/>
Šilka	Šilek	k1gInSc2
<g/>
“	“	k?
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
ZSU-	ZSU-	k1gFnSc2
<g/>
23	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Šilka	Šilka	k6eAd1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
560	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
206	#num#	k4
000	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
521	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Zdrojnice	zdrojnice	k1gFnSc2
</s>
<s>
Onon	Onon	k1gNnSc1
<g/>
,	,	kIx,
Ingoda	Ingoda	k1gFnSc1
51	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
115	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Amur	Amur	k1gInSc1
53	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
121	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Zabajkalský	zabajkalský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Ochotské	ochotský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Amur	Amur	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šilka	Šilka	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Ш	Ш	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Zabajkalském	zabajkalský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
560	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
řeky	řeka	k1gFnSc2
je	být	k5eAaImIp3nS
210	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS
soutokem	soutok	k1gInSc7
řek	řek	k1gMnSc1
Onon	Onon	k1gMnSc1
a	a	k8xC
Ingoda	Ingoda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teče	Téct	k5eAaImIp3nS
v	v	k7c6
hluboké	hluboký	k2eAgFnSc6d1
dolině	dolina	k1gFnSc6
mezi	mezi	k7c7
Šilkinským	Šilkinský	k2eAgInSc7d1
a	a	k8xC
Amazarským	Amazarský	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
Borščovočným	Borščovočný	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tok	tok	k1gInSc1
je	být	k5eAaImIp3nS
rychlý	rychlý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místy	místo	k1gNnPc7
se	se	k3xPyFc4
řeka	řeka	k1gFnSc1
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
jednotlivá	jednotlivý	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
říční	říční	k2eAgInPc4d1
prahy	práh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
levou	levý	k2eAgFnSc7d1
zdrojnicí	zdrojnice	k1gFnSc7
Amuru	Amur	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Arguní	Argueň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdrojem	zdroj	k1gInSc7
vody	voda	k1gFnSc2
jsou	být	k5eAaImIp3nP
především	především	k9
dešťové	dešťový	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
149	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
činí	činit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
521	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Největšího	veliký	k2eAgInSc2d3
průtoku	průtok	k1gInSc2
11	#num#	k4
400	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
dosáhla	dosáhnout	k5eAaPmAgFnS
řeka	řeka	k1gFnSc1
v	v	k7c6
červnu	červen	k1gInSc6
1958	#num#	k4
a	a	k8xC
nejmenšího	malý	k2eAgInSc2d3
0,81	0,81	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
v	v	k7c6
březnu	březen	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
letech	let	k1gInPc6
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
promrzá	promrzat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
dna	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamrzá	zamrzat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
října	říjen	k1gInSc2
až	až	k9
na	na	k7c6
začátku	začátek	k1gInSc6
listopadu	listopad	k1gInSc2
a	a	k8xC
rozmrzá	rozmrzat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
dubna	duben	k1gInSc2
až	až	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největších	veliký	k2eAgInPc2d3
vodních	vodní	k2eAgInPc2d1
stavů	stav	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
v	v	k7c6
červenci	červenec	k1gInSc6
a	a	k8xC
srpnu	srpen	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
často	často	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
povodním	povodeň	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Na	na	k7c6
řece	řeka	k1gFnSc6
je	být	k5eAaImIp3nS
možná	možná	k9
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
město	město	k1gNnSc4
Sretěnsk	Sretěnsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Ш	Ш	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šilka	Šilek	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Příroda	příroda	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
