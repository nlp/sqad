<s>
Helena	Helena	k1gFnSc1	Helena
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
současně	současně	k6eAd1	současně
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
kraje	kraj	k1gInSc2	kraj
Lewis	Lewis	k1gFnSc2	Lewis
and	and	k?	and
Clark	Clark	k1gInSc1	Clark
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
mělo	mít	k5eAaImAgNnS	mít
samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
28	[number]	k4	28
190	[number]	k4	190
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
metropolitní	metropolitní	k2eAgFnSc7d1	metropolitní
aglomerací	aglomerace	k1gFnSc7	aglomerace
kraje	kraj	k1gInSc2	kraj
Lewis	Lewis	k1gInSc1	Lewis
and	and	k?	and
Clark	Clark	k1gInSc1	Clark
a	a	k8xC	a
Jefferson	Jefferson	k1gInSc1	Jefferson
pak	pak	k6eAd1	pak
67	[number]	k4	67
636	[number]	k4	636
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
potoku	potok	k1gInSc6	potok
Last	Lasta	k1gFnPc2	Lasta
Chance	Chanec	k1gInSc2	Chanec
Creek	Creky	k1gFnPc2	Creky
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
Státní	státní	k2eAgInSc1d1	státní
Kapitol	Kapitol	k1gInSc1	Kapitol
The	The	k1gFnSc2	The
University	universita	k1gFnSc2	universita
of	of	k?	of
Montana	Montana	k1gFnSc1	Montana
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
pro	pro	k7c4	pro
cca	cca	kA	cca
1000	[number]	k4	1000
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
Caroll	Carollum	k1gNnPc2	Carollum
College	College	k1gFnPc2	College
-	-	kIx~	-
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
univerzita	univerzita	k1gFnSc1	univerzita
liberálních	liberální	k2eAgNnPc2d1	liberální
umění	umění	k1gNnPc2	umění
pro	pro	k7c4	pro
1500	[number]	k4	1500
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
komunální	komunální	k2eAgInPc4d1	komunální
centrum	centrum	k1gNnSc4	centrum
lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
areál	areál	k1gInSc1	areál
Great	Great	k2eAgInSc1d1	Great
Divide	Divid	k1gInSc5	Divid
Ski	ski	k1gFnPc1	ski
Area	area	k1gFnSc1	area
Regionální	regionální	k2eAgNnSc1d1	regionální
letiště	letiště	k1gNnSc1	letiště
Helena	Helena	k1gFnSc1	Helena
Helena	Helena	k1gFnSc1	Helena
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
Montany	Montana	k1gFnSc2	Montana
na	na	k7c6	na
46,59	[number]	k4	46,59
severní	severní	k2eAgFnSc6d1	severní
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
;	;	kIx,	;
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
,	,	kIx,	,
ovlivněné	ovlivněný	k2eAgInPc1d1	ovlivněný
Skalistými	skalistý	k2eAgFnPc7d1	skalistý
horami	hora	k1gFnPc7	hora
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0	[number]	k4	0
<g/>
°	°	k?	°
až	až	k9	až
-10	-10	k4	-10
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
+11	+11	k4	+11
<g/>
°	°	k?	°
až	až	k9	až
28	[number]	k4	28
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
28	[number]	k4	28
190	[number]	k4	190
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
93,3	[number]	k4	93,3
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g />
.	.	kIx.	.
</s>
<s>
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
2,8	[number]	k4	2,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
22,4	[number]	k4	22,4
%	%	kIx~	%
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
11,1	[number]	k4	11,1
%	%	kIx~	%
18-24	[number]	k4	18-24
26,6	[number]	k4	26,6
%	%	kIx~	%
25-44	[number]	k4	25-44
26	[number]	k4	26
%	%	kIx~	%
45-64	[number]	k4	45-64
a	a	k8xC	a
13,9	[number]	k4	13,9
%	%	kIx~	%
65	[number]	k4	65
<g/>
+	+	kIx~	+
let	léto	k1gNnPc2	léto
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
:	:	kIx,	:
34	[number]	k4	34
416	[number]	k4	416
dolarů	dolar	k1gInPc2	dolar
Roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
<g />
.	.	kIx.	.
</s>
<s>
průměrné	průměrný	k2eAgFnPc1d1	průměrná
rodiny	rodina	k1gFnPc1	rodina
<g/>
:	:	kIx,	:
50	[number]	k4	50
018	[number]	k4	018
dolarů	dolar	k1gInPc2	dolar
Roční	roční	k2eAgInSc4d1	roční
příjem	příjem	k1gInSc4	příjem
per	pero	k1gNnPc2	pero
capita	capit	k1gMnSc2	capit
<g/>
:	:	kIx,	:
20	[number]	k4	20
020	[number]	k4	020
dolarů	dolar	k1gInPc2	dolar
14,5	[number]	k4	14,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
Gary	Gara	k1gFnSc2	Gara
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
-	-	kIx~	-
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Dirk	Dirk	k1gMnSc1	Dirk
Benedict	Benedict	k1gMnSc1	Benedict
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Helena	Helena	k1gFnSc1	Helena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Helena	Helena	k1gFnSc1	Helena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
