<s>
Pivovar	pivovar	k1gInSc1	pivovar
Starobrno	Starobrno	k1gNnSc1	Starobrno
je	být	k5eAaImIp3nS	být
brněnský	brněnský	k2eAgInSc1d1	brněnský
pivovar	pivovar	k1gInSc1	pivovar
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
pivovarské	pivovarský	k2eAgFnSc2d1	Pivovarská
společnosti	společnost	k1gFnSc2	společnost
Heineken	Heinekna	k1gFnPc2	Heinekna
<g/>
.	.	kIx.	.
</s>
<s>
Výstav	výstav	k1gInSc1	výstav
Starobrna	Starobrno	k1gNnSc2	Starobrno
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
se	s	k7c7	s
znojemskou	znojemský	k2eAgFnSc7d1	Znojemská
filiálkou	filiálka	k1gFnSc7	filiálka
Hostan	Hostan	k1gInSc1	Hostan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
914	[number]	k4	914
000	[number]	k4	000
hl	hl	k?	hl
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starobrněnském	starobrněnský	k2eAgInSc6d1	starobrněnský
pivovaru	pivovar	k1gInSc6	pivovar
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nS	vařit
pivo	pivo	k1gNnSc1	pivo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
udělil	udělit	k5eAaPmAgInS	udělit
Brnu	Brno	k1gNnSc6	Brno
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
při	při	k7c6	při
klášteru	klášter	k1gInSc3	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
měnil	měnit	k5eAaImAgInS	měnit
majitele	majitel	k1gMnPc4	majitel
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Starobrněnský	Starobrněnský	k2eAgInSc1d1	Starobrněnský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
stáčírna	stáčírna	k1gFnSc1	stáčírna
láhví	láhev	k1gFnPc2	láhev
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
provozy	provoz	k1gInPc4	provoz
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
Žlutého	žlutý	k2eAgInSc2d1	žlutý
kopce	kopec	k1gInSc2	kopec
nad	nad	k7c7	nad
pivovarem	pivovar	k1gInSc7	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
druhů	druh	k1gInPc2	druh
piv	pivo	k1gNnPc2	pivo
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgNnPc1d1	obchodní
jména	jméno	k1gNnPc1	jméno
Osma	osma	k1gFnSc1	osma
<g/>
,	,	kIx,	,
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnPc1d1	Černé
<g/>
,	,	kIx,	,
Řezák	řezák	k1gInSc1	řezák
<g/>
,	,	kIx,	,
Medium	medium	k1gNnSc1	medium
<g/>
,	,	kIx,	,
Ležák	ležák	k1gInSc1	ležák
a	a	k8xC	a
Fríí	Fríí	k1gFnSc1	Fríí
<g/>
)	)	kIx)	)
také	také	k9	také
speciální	speciální	k2eAgNnPc1d1	speciální
piva	pivo	k1gNnPc1	pivo
Baron	baron	k1gMnSc1	baron
Trenck	Trenck	k1gMnSc1	Trenck
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
drak	drak	k1gInSc1	drak
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
drak	drak	k1gMnSc1	drak
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
má	mít	k5eAaImIp3nS	mít
Starobrno	Starobrno	k1gNnSc1	Starobrno
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
portfoliu	portfolio	k1gNnSc6	portfolio
tato	tento	k3xDgNnPc1	tento
piva	pivo	k1gNnSc2	pivo
<g/>
:	:	kIx,	:
Starobrno	Starobrno	k1gNnSc1	Starobrno
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
22,5	[number]	k4	22,5
EBU	EBU	kA	EBU
<g/>
;	;	kIx,	;
barva	barva	k1gFnSc1	barva
12	[number]	k4	12
EBC	EBC	kA	EBC
<g/>
/	/	kIx~	/
-	-	kIx~	-
nepasterizované	pasterizovaný	k2eNgNnSc4d1	nepasterizované
řízné	řízný	k2eAgNnSc4d1	řízné
pivo	pivo	k1gNnSc4	pivo
pro	pro	k7c4	pro
všední	všední	k2eAgInPc4d1	všední
i	i	k8xC	i
sváteční	sváteční	k2eAgInPc4d1	sváteční
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
k	k	k7c3	k
nevšední	všední	k2eNgFnSc3d1	nevšední
zábavě	zábava	k1gFnSc3	zábava
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
doma	doma	k6eAd1	doma
i	i	k9	i
ve	v	k7c6	v
Vaší	váš	k3xOp2gFnSc6	váš
hospůdce	hospůdka	k1gFnSc6	hospůdka
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
svojí	svojit	k5eAaImIp3nP	svojit
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
hořkou	hořký	k2eAgFnSc7d1	hořká
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
sladovou	sladový	k2eAgFnSc7d1	sladová
vůní	vůně	k1gFnSc7	vůně
/	/	kIx~	/
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
dostalo	dostat	k5eAaPmAgNnS	dostat
čtyři	čtyři	k4xCgFnPc4	čtyři
medajle	medajle	k1gFnPc4	medajle
<g/>
/	/	kIx~	/
Starobrno	Starobrno	k1gNnSc1	Starobrno
Reserva	reserva	k1gFnSc1	reserva
2016	[number]	k4	2016
/	/	kIx~	/
<g/>
6,5	[number]	k4	6,5
<g/>
%	%	kIx~	%
<g/>
/	/	kIx~	/
-	-	kIx~	-
speciální	speciální	k2eAgNnSc1d1	speciální
pivo	pivo	k1gNnSc1	pivo
šestnáctka	šestnáctka	k1gFnSc1	šestnáctka
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
výběr	výběr	k1gInSc1	výběr
ze	z	k7c2	z
sladů	slad	k1gInPc2	slad
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
6,5	[number]	k4	6,5
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
letos	letos	k6eAd1	letos
krásnou	krásný	k2eAgFnSc4d1	krásná
jantarovou	jantarový	k2eAgFnSc4d1	jantarová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
vařeno	vařit	k5eAaImNgNnS	vařit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
a	a	k8xC	a
jak	jak	k6eAd1	jak
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
z	z	k7c2	z
jakostních	jakostní	k2eAgFnPc2d1	jakostní
moravských	moravský	k2eAgFnPc2d1	Moravská
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Slad	slad	k1gInSc1	slad
je	být	k5eAaImIp3nS	být
tentokrát	tentokrát	k6eAd1	tentokrát
z	z	k7c2	z
Litovle	Litovel	k1gFnSc2	Litovel
<g/>
,	,	kIx,	,
Prostějova	Prostějov	k1gInSc2	Prostějov
a	a	k8xC	a
Hodonic	Hodonice	k1gFnPc2	Hodonice
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Tršic	Tršice	k1gFnPc2	Tršice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
pivo	pivo	k1gNnSc1	pivo
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Láhev	láhev	k1gFnSc1	láhev
má	mít	k5eAaImIp3nS	mít
0,75	[number]	k4	0,75
<g/>
l.	l.	k?	l.
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
pivo	pivo	k1gNnSc1	pivo
prodejné	prodejný	k2eAgNnSc1d1	prodejné
v	v	k7c6	v
sudu	sud	k1gInSc6	sud
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
30	[number]	k4	30
<g/>
l.	l.	k?	l.
Starobrno	Starobrno	k1gNnSc1	Starobrno
Medium	medium	k1gNnSc1	medium
/	/	kIx~	/
<g/>
4,7	[number]	k4	4,7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
26	[number]	k4	26
EBU	EBU	kA	EBU
<g/>
;	;	kIx,	;
barva	barva	k1gFnSc1	barva
13	[number]	k4	13
EBC	EBC	kA	EBC
<g/>
/	/	kIx~	/
chlouba	chlouba	k1gFnSc1	chlouba
pivovaru	pivovar	k1gInSc6	pivovar
který	který	k3yIgInSc4	který
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
dobré	dobrý	k2eAgNnSc4d1	dobré
pivo	pivo	k1gNnSc4	pivo
šest	šest	k4xCc4	šest
medajlí	medajle	k1gFnPc2	medajle
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
je	být	k5eAaImIp3nS	být
nepasterizovaný	pasterizovaný	k2eNgInSc1d1	nepasterizovaný
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
bohatou	bohatý	k2eAgFnSc7d1	bohatá
pěnou	pěna	k1gFnSc7	pěna
<g/>
,	,	kIx,	,
neobyčejně	obyčejně	k6eNd1	obyčejně
lahodnou	lahodný	k2eAgFnSc4d1	lahodná
<g/>
,	,	kIx,	,
plnou	plný	k2eAgFnSc4d1	plná
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
chmelovou	chmelový	k2eAgFnSc7d1	chmelová
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
dokonalým	dokonalý	k2eAgInSc7d1	dokonalý
řízem	říz	k1gInSc7	říz
<g/>
.	.	kIx.	.
</s>
<s>
Starobrno	Starobrno	k1gNnSc1	Starobrno
Drak	drak	k1gMnSc1	drak
/	/	kIx~	/
<g/>
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
29,5	[number]	k4	29,5
EBU	EBU	kA	EBU
<g/>
;	;	kIx,	;
barva	barva	k1gFnSc1	barva
14	[number]	k4	14
EBC	EBC	kA	EBC
<g/>
/	/	kIx~	/
originální	originální	k2eAgInSc4d1	originální
nepasterizovaný	pasterizovaný	k2eNgInSc4d1	nepasterizovaný
extra	extra	k2eAgInSc4d1	extra
chmelený	chmelený	k2eAgInSc4d1	chmelený
světlý	světlý	k2eAgInSc4d1	světlý
ležák	ležák	k1gInSc4	ležák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vařený	vařený	k2eAgMnSc1d1	vařený
tradičním	tradiční	k2eAgInSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
rmuty	rmut	k1gInPc4	rmut
a	a	k8xC	a
po	po	k7c6	po
dozrávání	dozrávání	k1gNnSc6	dozrávání
ještě	ještě	k9	ještě
prochází	procházet	k5eAaImIp3nS	procházet
speciálním	speciální	k2eAgNnSc7d1	speciální
studeným	studený	k2eAgNnSc7d1	studené
dochmelením	dochmelení	k1gNnSc7	dochmelení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
vyváženou	vyvážený	k2eAgFnSc7d1	vyvážená
chmelovou	chmelový	k2eAgFnSc7d1	chmelová
vůní	vůně	k1gFnSc7	vůně
a	a	k8xC	a
chutí	chuť	k1gFnSc7	chuť
s	s	k7c7	s
příjemným	příjemný	k2eAgNnSc7d1	příjemné
hořkosladkým	hořkosladký	k2eAgNnSc7d1	hořkosladké
dozníváním	doznívání	k1gNnSc7	doznívání
<g/>
,	,	kIx,	,
nabádajícím	nabádající	k2eAgInSc6d1	nabádající
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
napiti	napít	k5eAaBmNgMnP	napít
<g/>
.	.	kIx.	.
</s>
<s>
Pěna	pěna	k1gFnSc1	pěna
je	být	k5eAaImIp3nS	být
sametová	sametový	k2eAgFnSc1d1	sametová
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
dostalo	dostat	k5eAaPmAgNnS	dostat
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Pivexu	Pivex	k1gInSc6	Pivex
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Starobrno	Starobrno	k1gNnSc1	Starobrno
Nefiltrovaný	filtrovaný	k2eNgMnSc1d1	nefiltrovaný
Ležák	ležák	k1gMnSc1	ležák
/	/	kIx~	/
<g/>
5,0	[number]	k4	5,0
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
31	[number]	k4	31
EBU	EBU	kA	EBU
<g/>
;	;	kIx,	;
barva	barva	k1gFnSc1	barva
15,5	[number]	k4	15,5
EBC	EBC	kA	EBC
<g/>
/	/	kIx~	/
tento	tento	k3xDgInSc1	tento
ležák	ležák	k1gInSc1	ležák
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nejopečovávanější	opečovávaný	k2eAgNnSc4d3	opečovávaný
pivo	pivo	k1gNnSc4	pivo
ze	z	k7c2	z
starobrněnského	starobrněnský	k2eAgInSc2d1	starobrněnský
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nepasterizovaný	pasterizovaný	k2eNgInSc4d1	nepasterizovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
i	i	k9	i
nefiltrovaný	filtrovaný	k2eNgInSc4d1	nefiltrovaný
ležák	ležák	k1gInSc4	ležák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
typické	typický	k2eAgNnSc4d1	typické
zlatavé	zlatavý	k2eAgNnSc4d1	zlatavé
zakalení	zakalení	k1gNnSc4	zakalení
způsobené	způsobený	k2eAgInPc1d1	způsobený
pivovarskými	pivovarský	k2eAgFnPc7d1	Pivovarská
kvasnicemi	kvasnice	k1gFnPc7	kvasnice
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tak	tak	k6eAd1	tak
významné	významný	k2eAgNnSc1d1	významné
množství	množství	k1gNnSc1	množství
vitaminu	vitamin	k1gInSc2	vitamin
B.	B.	kA	B.
Právě	právě	k6eAd1	právě
díky	díky	k7c3	díky
kvasnicím	kvasnice	k1gFnPc3	kvasnice
má	mít	k5eAaImIp3nS	mít
nefiltrovaný	filtrovaný	k2eNgInSc4d1	nefiltrovaný
ležák	ležák	k1gInSc4	ležák
tak	tak	k8xC	tak
typickou	typický	k2eAgFnSc4d1	typická
plnou	plný	k2eAgFnSc4d1	plná
chuť	chuť	k1gFnSc4	chuť
s	s	k7c7	s
jemným	jemný	k2eAgInSc7d1	jemný
nádechem	nádech	k1gInSc7	nádech
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
chléb	chléb	k1gInSc4	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
ležák	ležák	k1gInSc4	ležák
nechávají	nechávat	k5eAaImIp3nP	nechávat
dozrát	dozrát	k5eAaPmF	dozrát
a	a	k8xC	a
zakulatit	zakulatit	k5eAaPmF	zakulatit
v	v	k7c6	v
ležáckých	ležácký	k2eAgInPc6d1	ležácký
sklepích	sklep	k1gInPc6	sklep
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
Žlutým	žlutý	k2eAgInSc7d1	žlutý
kopcem	kopec	k1gInSc7	kopec
a	a	k8xC	a
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
vás	vy	k3xPp2nPc4	vy
ručně	ručně	k6eAd1	ručně
stáčí	stáčet	k5eAaImIp3nS	stáčet
do	do	k7c2	do
sudů	sud	k1gInPc2	sud
<g/>
.	.	kIx.	.
</s>
<s>
Věnují	věnovat	k5eAaPmIp3nP	věnovat
mu	on	k3xPp3gInSc3	on
tak	tak	k6eAd1	tak
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
domu	dům	k1gInSc2	dům
maximální	maximální	k2eAgFnSc3d1	maximální
péči	péče	k1gFnSc3	péče
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
provozovnách	provozovna	k1gFnPc6	provozovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vám	vy	k3xPp2nPc3	vy
jej	on	k3xPp3gInSc4	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
/	/	kIx~	/
<g/>
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dodržovány	dodržovat	k5eAaImNgFnP	dodržovat
speciální	speciální	k2eAgFnPc1d1	speciální
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
jeho	jeho	k3xOp3gFnSc2	jeho
čerstvosti	čerstvost	k1gFnSc2	čerstvost
<g/>
.	.	kIx.	.
</s>
<s>
Ponořte	ponořit	k5eAaPmRp2nP	ponořit
se	se	k3xPyFc4	se
i	i	k9	i
vy	vy	k3xPp2nPc1	vy
do	do	k7c2	do
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
vůně	vůně	k1gFnSc2	vůně
chmele	chmel	k1gInSc2	chmel
a	a	k8xC	a
rozeznejte	rozeznat	k5eAaPmRp2nP	rozeznat
každý	každý	k3xTgInSc4	každý
tón	tón	k1gInSc4	tón
poctivé	poctivý	k2eAgFnSc2d1	poctivá
pivovarské	pivovarský	k2eAgFnSc2d1	Pivovarská
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
ingrediencí	ingredience	k1gFnPc2	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
Starobrno	Starobrno	k1gNnSc1	Starobrno
licenčně	licenčně	k6eAd1	licenčně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
slovenského	slovenský	k2eAgMnSc4d1	slovenský
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
bažanta	bažant	k1gMnSc4	bažant
<g/>
,	,	kIx,	,
dováží	dovážet	k5eAaImIp3nS	dovážet
piva	pivo	k1gNnSc2	pivo
značky	značka	k1gFnSc2	značka
Heineken	Heineken	k1gInSc1	Heineken
<g/>
,	,	kIx,	,
Amstel	Amstel	k1gInSc1	Amstel
<g/>
,	,	kIx,	,
Murphy	Murph	k1gInPc1	Murph
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Edelweiss	Edelweissa	k1gFnPc2	Edelweissa
<g/>
.	.	kIx.	.
</s>
<s>
Specialitou	specialita	k1gFnSc7	specialita
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
firmy	firma	k1gFnSc2	firma
Starobrno	Starobrno	k1gNnSc4	Starobrno
je	být	k5eAaImIp3nS	být
Zelené	Zelené	k2eAgNnSc1d1	Zelené
pivo	pivo	k1gNnSc1	pivo
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
nabízeno	nabízet	k5eAaImNgNnS	nabízet
na	na	k7c4	na
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Pivovar	pivovar	k1gInSc1	pivovar
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
surovina	surovina	k1gFnSc1	surovina
dodávající	dodávající	k2eAgFnSc1d1	dodávající
pivu	pivo	k1gNnSc3	pivo
svěže	svěže	k6eAd1	svěže
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
bylinného	bylinný	k2eAgInSc2d1	bylinný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tajemstvím	tajemství	k1gNnSc7	tajemství
starobrněnského	starobrněnský	k1gMnSc2	starobrněnský
sládka	sládek	k1gMnSc2	sládek
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
ale	ale	k9	ale
nařídila	nařídit	k5eAaPmAgFnS	nařídit
pivovaru	pivovar	k1gInSc3	pivovar
změnit	změnit	k5eAaPmF	změnit
prezentaci	prezentace	k1gFnSc4	prezentace
zeleného	zelený	k2eAgInSc2d1	zelený
speciálu	speciál	k1gInSc2	speciál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
barvě	barva	k1gFnSc6	barva
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k8xC	i
likér	likér	k1gInSc4	likér
obsahující	obsahující	k2eAgNnSc4d1	obsahující
umělé	umělý	k2eAgNnSc4d1	umělé
potravinářské	potravinářský	k2eAgNnSc4d1	potravinářské
barvivo	barvivo	k1gNnSc4	barvivo
E	E	kA	E
<g/>
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
Portfolio	portfolio	k1gNnSc1	portfolio
pivních	pivní	k2eAgFnPc2d1	pivní
značek	značka	k1gFnPc2	značka
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
ještě	ještě	k6eAd1	ještě
vlastní	vlastní	k2eAgFnSc2d1	vlastní
sudové	sudový	k2eAgFnSc2d1	sudová
limonády	limonáda	k1gFnSc2	limonáda
ZULU	Zulu	k1gMnSc1	Zulu
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
ZULU	Zulu	k1gMnSc1	Zulu
citron	citron	k1gInSc1	citron
a	a	k8xC	a
pivní	pivní	k2eAgFnSc1d1	pivní
pálenka	pálenka	k1gFnSc1	pálenka
BierBrand	BierBranda	k1gFnPc2	BierBranda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Starobrno	Starobrno	k1gNnSc1	Starobrno
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Stránky	stránka	k1gFnSc2	stránka
pivovaru	pivovar	k1gInSc2	pivovar
Starobrno	Starobrno	k1gNnSc1	Starobrno
</s>
