<s>
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
je	být	k5eAaImIp3nS
Padmaja	Padmaja	k1gMnSc1
Naidu	Naid	k1gInSc2
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gMnSc1
Park	park	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Dárdžiling	Dárdžiling	k1gInSc1
v	v	k7c6
indickém	indický	k2eAgInSc6d1
státu	stát	k1gInSc6
Západní	západní	k2eAgNnSc1d1
Bengálsko	Bengálsko	k1gNnSc1
<g/>
.	.	kIx.
</s>