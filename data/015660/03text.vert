<s>
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
</s>
<s>
Zoo	zoo	k1gFnSc1
Dárdžiling	Dárdžiling	k1gInSc1
(	(	kIx(
<g/>
Padmaja	Padmaja	k1gMnSc1
Naidu	Naid	k1gInSc2
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gMnSc1
Park	park	k1gInSc1
<g/>
)	)	kIx)
Vchod	vchod	k1gInSc1
do	do	k7c2
zoologické	zoologický	k2eAgFnSc2d1
zahradyStát	zahradyStát	k1gInSc4
</s>
<s>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc2
Město	město	k1gNnSc1
</s>
<s>
Dárdžiling	Dárdžiling	k1gInSc4
Datum	datum	k1gInSc1
založení	založení	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
Zaměření	zaměření	k1gNnSc1
chovu	chov	k1gInSc2
</s>
<s>
ohrožené	ohrožený	k2eAgInPc4d1
druhy	druh	k1gInPc4
Počet	počet	k1gInSc1
zvířat	zvíře	k1gNnPc2
</s>
<s>
156	#num#	k4
Vyhledávané	vyhledávaný	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
panda	panda	k1gFnSc1
červená	červená	k1gFnSc1
<g/>
,	,	kIx,
tibetský	tibetský	k2eAgMnSc1d1
vlk	vlk	k1gMnSc1
<g/>
,	,	kIx,
sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
27	#num#	k4
ha	ha	kA
Členství	členství	k1gNnSc1
</s>
<s>
WAZA	WAZA	kA
CZA	CZA	kA
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
88	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Zoo	zoo	k1gFnSc1
Dárdžiling	Dárdžiling	k1gInSc1
(	(	kIx(
<g/>
Padmaja	Padmaja	k1gMnSc1
Naidu	Naid	k1gInSc2
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gMnSc1
Park	park	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
je	být	k5eAaImIp3nS
Padmaja	Padmaja	k1gMnSc1
Naidu	Naid	k1gInSc2
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gMnSc1
Park	park	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Dárdžiling	Dárdžiling	k1gInSc1
v	v	k7c6
indickém	indický	k2eAgInSc6d1
státu	stát	k1gInSc6
Západní	západní	k2eAgNnSc1d1
Bengálsko	Bengálsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
27	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
2134	#num#	k4
m.	m	kA
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejvýše	vysoce	k6eAd3,k6eAd1
položená	položený	k2eAgFnSc1d1
zoo	zoo	k1gFnSc1
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specializuje	specializovat	k5eAaBmIp3nS
se	se	k3xPyFc4
na	na	k7c4
chov	chov	k1gInSc4
horských	horský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
a	a	k8xC
úspěšně	úspěšně	k6eAd1
chová	chovat	k5eAaImIp3nS
v	v	k7c6
zajetí	zajetí	k1gNnSc6
sněžné	sněžný	k2eAgFnPc4d1
leopardy	leopard	k1gMnPc7
<g/>
,	,	kIx,
kriticky	kriticky	k6eAd1
ohrožené	ohrožený	k2eAgMnPc4d1
tibetské	tibetský	k2eAgMnPc4d1
vlky	vlk	k1gMnPc4
a	a	k8xC
pandy	panda	k1gFnPc4
červené	červený	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
návštěvnost	návštěvnost	k1gFnSc4
asi	asi	k9
300	#num#	k4
000	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
bývalé	bývalý	k2eAgFnSc6d1
guvernérce	guvernérka	k1gFnSc6
státu	stát	k1gInSc2
Západní	západní	k2eAgNnSc4d1
Bengálsko	Bengálsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předmětná	předmětný	k2eAgFnSc1d1
zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
program	program	k1gInSc4
pandy	panda	k1gFnSc2
červené	červený	k2eAgFnSc2d1
v	v	k7c6
Indii	Indie	k1gFnSc6
jako	jako	k9
ústřední	ústřední	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
také	také	k9
členem	člen	k1gMnSc7
Světové	světový	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
a	a	k8xC
akvárií	akvárium	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Brána	brán	k2eAgFnSc1d1
zoo	zoo	k1gFnSc1
</s>
<s>
Zoo	zoo	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1958	#num#	k4
společně	společně	k6eAd1
vládou	vláda	k1gFnSc7
Indie	Indie	k1gFnSc2
a	a	k8xC
vládou	vláda	k1gFnSc7
Západního	západní	k2eAgNnSc2d1
Bengálska	Bengálsko	k1gNnSc2
(	(	kIx(
<g/>
formou	forma	k1gFnSc7
joint	jointa	k1gFnPc2
venture	ventur	k1gMnSc5
<g/>
)	)	kIx)
s	s	k7c7
cílem	cíl	k1gInSc7
studovat	studovat	k5eAaImF
a	a	k8xC
chránit	chránit	k5eAaImF
himálajskou	himálajský	k2eAgFnSc4d1
faunu	fauna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
prvním	první	k4xOgMnSc6
ředitelem	ředitel	k1gMnSc7
a	a	k8xC
zakladatelem	zakladatel	k1gMnSc7
byl	být	k5eAaImAgMnS
Dilip	Dilip	k1gMnSc1
Kumar	Kumar	k1gMnSc1
Dey	Dey	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
ve	v	k7c6
správě	správa	k1gFnSc6
lesů	les	k1gInPc2
a	a	k8xC
měl	mít	k5eAaImAgMnS
jasný	jasný	k2eAgInSc4d1
záměr	záměr	k1gInSc4
vytvořit	vytvořit	k5eAaPmF
vysokohorskou	vysokohorský	k2eAgFnSc4d1
zoologickou	zoologický	k2eAgFnSc4d1
zahradu	zahrada	k1gFnSc4
<g/>
,	,	kIx,
specializovanou	specializovaný	k2eAgFnSc4d1
především	především	k6eAd1
na	na	k7c4
himálajskou	himálajský	k2eAgFnSc4d1
faunu	fauna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Nikita	Nikita	k1gMnSc1
Chruščov	Chruščov	k1gInSc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
zoologické	zoologický	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
cenný	cenný	k2eAgInSc1d1
pár	pár	k1gInSc1
ussurijských	ussurijský	k2eAgMnPc2d1
tygrů	tygr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoo	zoo	k1gNnSc1
nyní	nyní	k6eAd1
chová	chovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
např.	např.	kA
sněžné	sněžný	k2eAgFnPc4d1
leopardy	leopard	k1gMnPc7
<g/>
,	,	kIx,
pandy	panda	k1gFnPc4
červené	červená	k1gFnSc2
<g/>
,	,	kIx,
goraly	goral	k1gMnPc4
tmavé	tmavý	k2eAgMnPc4d1
<g/>
,	,	kIx,
sibiřské	sibiřský	k2eAgMnPc4d1
tygry	tygr	k1gMnPc4
a	a	k8xC
mnoho	mnoho	k4c1
ohrožených	ohrožený	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
zvířata	zvíře	k1gNnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
časem	časem	k6eAd1
ohrožena	ohrozit	k5eAaPmNgFnS
stoupající	stoupající	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
v	v	k7c6
této	tento	k3xDgFnSc6
horské	horský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
lednu	leden	k1gInSc6
1972	#num#	k4
stala	stát	k5eAaPmAgFnS
registrovanou	registrovaný	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
a	a	k8xC
náklady	náklad	k1gInPc1
byly	být	k5eAaImAgInP
rozděleny	rozdělit	k5eAaPmNgInP
mezi	mezi	k7c4
vládu	vláda	k1gFnSc4
Indie	Indie	k1gFnSc2
a	a	k8xC
vládu	vláda	k1gFnSc4
Západního	západní	k2eAgNnSc2d1
Bengálska	Bengálsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1993	#num#	k4
byla	být	k5eAaImAgFnS
zoo	zoo	k1gFnSc1
převedena	převést	k5eAaPmNgFnS
pod	pod	k7c4
západobengálské	západobengálský	k2eAgNnSc4d1
ministerstvo	ministerstvo	k1gNnSc4
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zahradu	zahrada	k1gFnSc4
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
navštívila	navštívit	k5eAaPmAgFnS
ministerská	ministerský	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
Indira	Indira	k1gFnSc1
Gándhíová	Gándhíová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Záchrana	záchrana	k1gFnSc1
zvířat	zvíře	k1gNnPc2
</s>
<s>
Červená	červený	k2eAgFnSc1d1
panda	panda	k1gFnSc1
v	v	k7c6
zoologické	zoologický	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
</s>
<s>
Zoo	zoo	k1gFnSc1
má	mít	k5eAaImIp3nS
rozmnožovací	rozmnožovací	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
pro	pro	k7c4
sněžné	sněžný	k2eAgMnPc4d1
leopardy	leopard	k1gMnPc4
a	a	k8xC
pandy	panda	k1gFnPc4
červené	červený	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chov	chov	k1gInSc4
sněžných	sněžný	k2eAgMnPc2d1
leopardů	leopard	k1gMnPc2
v	v	k7c6
zajetí	zajetí	k1gNnSc6
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leopardi	leopard	k1gMnPc1
byli	být	k5eAaImAgMnP
dodáni	dodat	k5eAaPmNgMnP
do	do	k7c2
zoo	zoo	k1gFnSc2
v	v	k7c6
Curychu	Curych	k1gInSc6
<g/>
,	,	kIx,
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
do	do	k7c2
Léhu	Léhus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
chovu	chov	k1gInSc2
pandy	panda	k1gFnSc2
červené	červená	k1gFnSc2
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvířata	zvíře	k1gNnPc1
byla	být	k5eAaImAgNnP
dodána	dodat	k5eAaPmNgNnP
do	do	k7c2
zoo	zoo	k1gFnSc2
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
,	,	kIx,
Rotterdamu	Rotterdam	k1gInSc6
a	a	k8xC
do	do	k7c2
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
v	v	k7c6
zoologické	zoologický	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
chovají	chovat	k5eAaImIp3nP
např.	např.	kA
tato	tento	k3xDgNnPc1
zvířata	zvíře	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
tahr	tahr	k1gInSc1
himálajský	himálajský	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
nahur	nahur	k1gMnSc1
modrý	modrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
bažant	bažant	k1gMnSc1
lesklý	lesklý	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
bažant	bažant	k1gMnSc1
paví	paví	k2eAgMnSc1d1
šedý	šedý	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
trnočolek	trnočolek	k1gInSc1
bradavčitý	bradavčitý	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
bažant	bažant	k1gMnSc1
krvavý	krvavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
</s>
<s>
satyr	satyr	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zoo	zoo	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
světě	svět	k1gInSc6
známá	známý	k2eAgFnSc1d1
svými	svůj	k3xOyFgInPc7
programy	program	k1gInPc7
na	na	k7c4
zachování	zachování	k1gNnPc4
ohrožených	ohrožený	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
pandy	panda	k1gFnSc2
červené	červené	k1gNnSc1
<g/>
,	,	kIx,
trnočolka	trnočolka	k1gFnSc1
bradavčitého	bradavčitý	k2eAgMnSc2d1
<g/>
,	,	kIx,
tibetského	tibetský	k2eAgMnSc2d1
vlka	vlk	k1gMnSc2
a	a	k8xC
sněžného	sněžný	k2eAgMnSc2d1
leoparda	leopard	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Plán	plán	k1gInSc1
zoo	zoo	k1gFnSc2
v	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
</s>
<s>
O	o	k7c6
sněžném	sněžný	k2eAgMnSc6d1
leopardovi	leopard	k1gMnSc6
</s>
<s>
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
</s>
<s>
Panda	panda	k1gFnSc1
červená	červený	k2eAgFnSc1d1
</s>
<s>
Panda	panda	k1gFnSc1
červená	červený	k2eAgFnSc1d1
</s>
<s>
O	o	k7c6
tibetském	tibetský	k2eAgMnSc6d1
vlkovi	vlk	k1gMnSc6
</s>
<s>
Tibetský	tibetský	k2eAgMnSc1d1
vlk	vlk	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Padmaja	Padmajus	k1gMnSc4
Naidu	Naida	k1gMnSc4
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gFnSc2
Park	park	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ESSAY	ESSAY	kA
<g/>
:	:	kIx,
Promoting	Promoting	k1gInSc1
life	lif	k1gMnSc2
–	–	k?
Padmaja	Padmajus	k1gMnSc4
Naidu	Naida	k1gMnSc4
Himalayan	Himalayan	k1gMnSc1
Zoological	Zoological	k1gFnSc2
Park	park	k1gInSc1
<g/>
↑	↑	k?
Central	Central	k1gFnSc2
Zoo	zoo	k1gFnSc2
Authority	Authorita	k1gFnSc2
(	(	kIx(
<g/>
CZA	CZA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
cza	cza	k?
<g/>
.	.	kIx.
<g/>
nic	nic	k6eAd1
<g/>
.	.	kIx.
<g/>
in	in	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
Zoos	Zoos	k1gInSc1
and	and	k?
Aquariums	Aquariums	k1gInSc1
(	(	kIx(
<g/>
WAZA	WAZA	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Depleting	Depleting	k1gInSc1
Forests	Forests	k1gInSc1
Threaten	Threaten	k2eAgInSc1d1
Animals	Animals	k1gInSc1
in	in	k?
Darjeeling	Darjeeling	k1gInSc1
Zoo	zoo	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
zoo	zoo	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014092152	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
309848439	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014092152	#num#	k4
</s>
