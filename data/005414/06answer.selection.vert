<s>
Římské	římský	k2eAgFnPc4d1	římská
smlouvy	smlouva	k1gFnPc4	smlouva
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
podepsány	podepsat	k5eAaPmNgFnP	podepsat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
<g/>
:	:	kIx,	:
smlouvu	smlouva	k1gFnSc4	smlouva
zakládající	zakládající	k2eAgFnSc4d1	zakládající
Evropské	evropský	k2eAgNnSc4d1	Evropské
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
společenství	společenství	k1gNnSc4	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
zakládající	zakládající	k2eAgFnSc4d1	zakládající
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Euratom	Euratom	k1gInSc4	Euratom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
