<s>
Římské	římský	k2eAgFnPc4d1	římská
smlouvy	smlouva	k1gFnPc4	smlouva
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
podepsány	podepsat	k5eAaPmNgFnP	podepsat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
<g/>
:	:	kIx,	:
smlouvu	smlouva	k1gFnSc4	smlouva
zakládající	zakládající	k2eAgFnSc4d1	zakládající
Evropské	evropský	k2eAgNnSc4d1	Evropské
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
společenství	společenství	k1gNnSc4	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
zakládající	zakládající	k2eAgFnSc4d1	zakládající
Evropské	evropský	k2eAgNnSc1d1	Evropské
společenství	společenství	k1gNnSc1	společenství
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Euratom	Euratom	k1gInSc4	Euratom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgFnPc1d1	římská
smlouvy	smlouva	k1gFnPc1	smlouva
byly	být	k5eAaImAgFnP	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
pozdější	pozdní	k2eAgFnSc2d2	pozdější
evropské	evropský	k2eAgFnSc2d1	Evropská
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Signatářské	signatářský	k2eAgInPc1d1	signatářský
státy	stát	k1gInPc1	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byly	být	k5eAaImAgInP	být
<g/>
:	:	kIx,	:
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
Evropském	evropský	k2eAgNnSc6d1	Evropské
hospodářském	hospodářský	k2eAgNnSc6d1	hospodářské
společenství	společenství	k1gNnSc6	společenství
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
