<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
je	být	k5eAaImIp3nS	být
102	[number]	k4	102
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
postavená	postavený	k2eAgFnSc1d1	postavená
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
Páté	pátá	k1gFnSc2	pátá
Avenue	avenue	k1gFnSc2	avenue
a	a	k8xC	a
West	West	k1gInSc1	West
34	[number]	k4	34
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
