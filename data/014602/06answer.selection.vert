<s>
Halové	halový	k2eAgInPc1d1
jevy	jev	k1gInPc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
halo	halo	k1gNnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
optické	optický	k2eAgInPc4d1
úkazy	úkaz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vznikají	vznikat	k5eAaImIp3nP
odrazem	odraz	k1gInSc7
či	či	k8xC
průchodem	průchod	k1gInSc7
slunečních	sluneční	k2eAgInPc2d1
respektive	respektive	k9
měsíčních	měsíční	k2eAgInPc2d1
paprsků	paprsek	k1gInPc2
drobnými	drobný	k2eAgInPc7d1
ledovými	ledový	k2eAgInPc7d1
krystaly	krystal	k1gInPc7
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>