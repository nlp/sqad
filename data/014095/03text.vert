<s>
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
</s>
<s>
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
Bankovky	bankovka	k1gFnPc4
japonského	japonský	k2eAgInSc2d1
jenuZemě	jenuZemě	k6eAd1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
JPY	JPY	kA
Symbol	symbol	k1gInSc1
</s>
<s>
¥	¥	k?
(	(	kIx(
<g/>
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
円	円	k?
<g/>
)	)	kIx)
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
sen	sen	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
<g/>
rin	rin	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1000	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
jenů	jen	k1gInPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
1000	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
<g/>
,	,	kIx,
10000	#num#	k4
jenů	jen	k1gInPc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
kurzu	kurz	k1gInSc2
JPY	JPY	kA
vůči	vůči	k7c3
CZK	CZK	kA
od	od	k7c2
července	červenec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jen	jen	k9
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
široce	široko	k6eAd1
používána	používán	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
bankovní	bankovní	k2eAgFnSc1d1
rezerva	rezerva	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
dolarem	dolar	k1gInSc7
a	a	k8xC
eurem	euro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
japonštině	japonština	k1gFnSc6
je	být	k5eAaImIp3nS
název	název	k1gInSc4
měny	měna	k1gFnSc2
vyslovován	vyslovován	k2eAgInSc4d1
„	„	k?
<g/>
en	en	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
dalších	další	k2eAgInPc2d1
„	„	k?
<g/>
západních	západní	k2eAgInPc2d1
<g/>
“	“	k?
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
používán	používán	k2eAgInSc4d1
název	název	k1gInSc4
„	„	k?
<g/>
jen	jen	k9
<g/>
“	“	k?
(	(	kIx(
<g/>
díky	díky	k7c3
starému	starý	k2eAgMnSc3d1
a	a	k8xC
nepřesnému	přesný	k2eNgInSc3d1
přepisu	přepis	k1gInSc3
japonštiny	japonština	k1gFnSc2
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
jenu	jen	k1gInSc2
podle	podle	k7c2
ISO	ISO	kA
4217	#num#	k4
je	být	k5eAaImIp3nS
JPY	JPY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
latince	latinka	k1gFnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
symbol	symbol	k1gInSc1
¥	¥	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
japonštině	japonština	k1gFnSc6
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
pomocí	pomoc	k1gFnSc7
kandži	kandzat	k5eAaPmIp1nS
円	円	k?
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
kulatý	kulatý	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starší	k1gMnSc1
japonské	japonský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
měly	mít	k5eAaImAgFnP
totiž	totiž	k9
tvar	tvar	k1gInSc4
oválu	ovál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
V	v	k7c6
oběhu	oběh	k1gInSc6
jsou	být	k5eAaImIp3nP
mince	mince	k1gFnSc2
hodnot	hodnota	k1gFnPc2
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
a	a	k8xC
500	#num#	k4
jenů	jen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
hodnot	hodnota	k1gFnPc2
5	#num#	k4
a	a	k8xC
50	#num#	k4
jenů	jen	k1gInPc2
mají	mít	k5eAaImIp3nP
uprostřed	uprostřed	k6eAd1
kulatý	kulatý	k2eAgInSc4d1
otvor	otvor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
500	#num#	k4
jenů	jen	k1gInPc2
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
mincí	mince	k1gFnPc2
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
hodnotou	hodnota	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
oběhu	oběh	k1gInSc6
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
lednu	leden	k1gInSc6
2006	#num#	k4
bylo	být	k5eAaImAgNnS
500	#num#	k4
¥	¥	k?
asi	asi	k9
100	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkyvy	výkyv	k1gInPc1
kurzu	kurz	k1gInSc2
zejména	zejména	k9
k	k	k7c3
CZK	CZK	kA
jsou	být	k5eAaImIp3nP
v	v	k7c6
době	doba	k1gFnSc6
krize	krize	k1gFnSc2
velké	velký	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
2008	#num#	k4
do	do	k7c2
února	únor	k1gInSc2
2009	#num#	k4
posílil	posílit	k5eAaPmAgInS
jen	jen	k9
o	o	k7c4
100	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
1	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
000	#num#	k4
a	a	k8xC
10	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovka	bankovka	k1gFnSc1
1000	#num#	k4
jenů	jen	k1gInPc2
má	mít	k5eAaImIp3nS
v	v	k7c6
oběhu	oběh	k1gInSc6
starší	starý	k2eAgMnPc1d2
a	a	k8xC
novou	nový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
lišící	lišící	k2eAgFnSc4d1
se	se	k3xPyFc4
vyobrazenou	vyobrazený	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	Stará	k1gFnSc1
není	být	k5eNaImIp3nS
akceptována	akceptován	k2eAgFnSc1d1
některými	některý	k3yIgInPc7
automaty	automat	k1gInPc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
hodnot	hodnota	k1gFnPc2
1000	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
a	a	k8xC
10	#num#	k4
000	#num#	k4
jenů	jen	k1gInPc2
je	být	k5eAaImIp3nS
připravována	připravovat	k5eAaImNgFnS
na	na	k7c4
rok	rok	k1gInSc4
2024	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
bankovka	bankovka	k1gFnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
2000	#num#	k4
jenů	jen	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
lícové	lícový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
zobrazenu	zobrazen	k2eAgFnSc4d1
bránu	brána	k1gFnSc4
Šureimon	Šureimona	k1gFnPc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Okinawa	Okinawa	k1gFnSc1
a	a	k8xC
na	na	k7c6
lícové	lícový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
motiv	motiv	k1gInSc4
z	z	k7c2
příběhu	příběh	k1gInSc2
o	o	k7c4
Gendžim	Gendžim	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Bankovka	bankovka	k1gFnSc1
2000	#num#	k4
není	být	k5eNaImIp3nS
mezi	mezi	k7c7
Japonci	Japonec	k1gMnPc7
příliš	příliš	k6eAd1
populární	populární	k2eAgMnPc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
JPY	JPY	kA
(	(	kIx(
<g/>
japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
<g/>
)	)	kIx)
=	=	kIx~
100	#num#	k4
senů	sen	k1gInPc2
(	(	kIx(
<g/>
銭	銭	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mince	mince	k1gFnSc1
sen	sen	k1gInSc1
již	již	k6eAd1
desítky	desítka	k1gFnPc1
let	léto	k1gNnPc2
neexistují	existovat	k5eNaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
slovo	slovo	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
JPY	JPY	kA
=	=	kIx~
1000	#num#	k4
rinů	rin	k1gInPc2
(	(	kIx(
<g/>
厘	厘	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
1	#num#	k4
sen	sen	k1gInSc1
=	=	kIx~
10	#num#	k4
rinů	rin	k1gInPc2
–	–	k?
používáno	používán	k2eAgNnSc1d1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
pol.	pol.	k?
20	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jen	jen	k9
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
oběhu	oběh	k1gInSc2
během	během	k7c2
reforem	reforma	k1gFnPc2
Meidži	Meidž	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
podle	podle	k7c2
vzoru	vzor	k1gInSc2
evropských	evropský	k2eAgFnPc2d1
měn	měna	k1gFnPc2
a	a	k8xC
nahradil	nahradit	k5eAaPmAgInS
tak	tak	k6eAd1
složitý	složitý	k2eAgInSc1d1
monetární	monetární	k2eAgInSc1d1
systém	systém	k1gInSc1
období	období	k1gNnSc2
Edo	Eda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
Zákon	zákon	k1gInSc1
o	o	k7c6
nové	nový	k2eAgFnSc6d1
měně	měna	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
definoval	definovat	k5eAaBmAgInS
jen	jen	k9
jako	jako	k9
0,8667	0,8667	k4
trojské	trojský	k2eAgFnSc2d1
unce	unce	k1gFnSc2
(	(	kIx(
<g/>
26,956	26,956	k4
g	g	kA
<g/>
)	)	kIx)
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
6,50	6,50	k4
dnešních	dnešní	k2eAgMnPc2d1
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
také	také	k6eAd1
zavedl	zavést	k5eAaPmAgInS
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
zlatý	zlatý	k2eAgInSc4d1
standard	standard	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jen	jen	k6eAd1
ztratil	ztratit	k5eAaPmAgInS
většinu	většina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
hodnoty	hodnota	k1gFnSc2
během	běh	k1gInSc7
a	a	k8xC
po	po	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
období	období	k1gNnSc6
nestability	nestabilita	k1gFnSc2
byl	být	k5eAaImAgInS
jen	jen	k9
od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
pevně	pevně	k6eAd1
svázán	svázat	k5eAaPmNgInS
s	s	k7c7
americkým	americký	k2eAgInSc7d1
dolarem	dolar	k1gInSc7
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
USD	USD	kA
=	=	kIx~
360	#num#	k4
¥	¥	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
trvalo	trvat	k5eAaImAgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zhroutil	zhroutit	k5eAaPmAgInS
brettonwoodský	brettonwoodský	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
jenu	jen	k1gInSc2
určována	určovat	k5eAaImNgFnS
trhem	trh	k1gInSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
jen	jen	k6eAd1
posiloval	posilovat	k5eAaImAgInS
až	až	k9
na	na	k7c4
100	#num#	k4
jenů	jen	k1gInPc2
za	za	k7c4
dolar	dolar	k1gInSc4
a	a	k8xC
kolem	kolem	k7c2
této	tento	k3xDgFnSc2
úrovně	úroveň	k1gFnSc2
se	se	k3xPyFc4
s	s	k7c7
významnějšími	významný	k2eAgInPc7d2
výkyvy	výkyv	k1gInPc7
drží	držet	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
japonského	japonský	k2eAgInSc2d1
jenu	jen	k1gInSc2
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Platné	platný	k2eAgFnPc4d1
mince	mince	k1gFnPc4
(	(	kIx(
<g/>
korunové	korunový	k2eAgFnPc4d1
<g/>
)	)	kIx)
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
Obrázek	obrázek	k1gInSc1
</s>
<s>
PrůměrTloušťkaHmotnostSlitinaHrana	PrůměrTloušťkaHmotnostSlitinaHrana	k1gFnSc1
</s>
<s>
¥	¥	k?
<g/>
1	#num#	k4
</s>
<s>
20	#num#	k4
mm	mm	kA
</s>
<s>
1,5	1,5	k4
mm	mm	kA
</s>
<s>
1	#num#	k4
g	g	kA
</s>
<s>
100	#num#	k4
%	%	kIx~
hliníku	hliník	k1gInSc2
</s>
<s>
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
¥	¥	k?
<g/>
5	#num#	k4
</s>
<s>
22	#num#	k4
mm	mm	kA
</s>
<s>
3,75	3,75	k4
g	g	kA
</s>
<s>
60	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
%	%	kIx~
mědi	měď	k1gFnSc2
</s>
<s>
30	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
%	%	kIx~
zinku	zinek	k1gInSc2
</s>
<s>
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
¥	¥	k?
<g/>
10	#num#	k4
</s>
<s>
23	#num#	k4
mm	mm	kA
</s>
<s>
4,5	4,5	k4
g	g	kA
</s>
<s>
95	#num#	k4
%	%	kIx~
mědi	měď	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
%	%	kIx~
zinku	zinek	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
%	%	kIx~
cínu	cín	k1gInSc2
</s>
<s>
rýhovaná	rýhovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
¥	¥	k?
<g/>
50	#num#	k4
</s>
<s>
21	#num#	k4
mm	mm	kA
</s>
<s>
1,7	1,7	k4
mm	mm	kA
</s>
<s>
4	#num#	k4
g	g	kA
</s>
<s>
mědinikl	mědinikl	k1gInSc1
</s>
<s>
rýhovaná	rýhovaný	k2eAgFnSc1d1
</s>
<s>
¥	¥	k?
<g/>
100	#num#	k4
</s>
<s>
22,6	22,6	k4
mm	mm	kA
</s>
<s>
4,8	4,8	k4
g	g	kA
</s>
<s>
mědinikl	mědinikl	k1gInSc1
</s>
<s>
rýhovaná	rýhovaný	k2eAgFnSc1d1
</s>
<s>
¥	¥	k?
<g/>
500	#num#	k4
</s>
<s>
26,6	26,6	k4
mm	mm	kA
</s>
<s>
2	#num#	k4
mm	mm	kA
</s>
<s>
7	#num#	k4
g	g	kA
</s>
<s>
alpaka	alpaka	k1gFnSc1
</s>
<s>
šikmá	šikmý	k2eAgFnSc1d1
<g/>
,	,	kIx,
rýhovaná	rýhovaný	k2eAgFnSc1d1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
Platné	platný	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
aversu	avers	k1gInSc2
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
reversuMotiv	reversuMotit	k5eAaPmDgInS
aversuMotiv	aversuMotit	k5eAaPmDgInS
reversuBarevný	reversuBarevný	k2eAgMnSc5d1
tónŠířkaVýška	tónŠířkaVýška	k1gFnSc1
</s>
<s>
¥	¥	k?
<g/>
1000	#num#	k4
</s>
<s>
Hidejo	Hidejo	k6eAd1
NogučiFudži	NogučiFudž	k1gFnSc3
<g/>
,	,	kIx,
jezero	jezero	k1gNnSc4
Motosu	Motos	k1gInSc2
<g/>
,	,	kIx,
sakura	sakura	k1gFnSc1
ozdobnámodrá	ozdobnámodrý	k2eAgFnSc1d1
</s>
<s>
150	#num#	k4
mm	mm	kA
<g/>
76	#num#	k4
mm	mm	kA
</s>
<s>
¥	¥	k?
<g/>
2000	#num#	k4
</s>
<s>
ŠureimonPříběh	ŠureimonPříběh	k1gInSc1
prince	princ	k1gMnSc2
Gendžiho	Gendži	k1gMnSc2
<g/>
,	,	kIx,
Murasaki	Murasak	k1gFnSc2
Šikibuzelená	Šikibuzelený	k2eAgFnSc1d1
<g/>
154	#num#	k4
mm	mm	kA
</s>
<s>
¥	¥	k?
<g/>
5000	#num#	k4
</s>
<s>
Ičijó	Ičijó	k?
HigučiKakitsubata-zu	HigučiKakitsubata-za	k1gFnSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
malba	malba	k1gFnSc1
kosatců	kosatec	k1gInPc2
od	od	k7c2
japonského	japonský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Ogaty	Ogata	k1gMnSc2
Kórina	Kórin	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
purpurová	purpurový	k2eAgFnSc1d1
<g/>
156	#num#	k4
mm	mm	kA
</s>
<s>
¥	¥	k?
<g/>
10000	#num#	k4
</s>
<s>
Jukiči	Jukič	k1gMnPc1
FukuzawaSocha	FukuzawaSoch	k1gMnSc2
feng-chuang	feng-chuanga	k1gFnPc2
ve	v	k7c6
chrámu	chrám	k1gInSc6
Bjódóinhnědá	Bjódóinhnědý	k2eAgFnSc1d1
</s>
<s>
160	#num#	k4
mm	mm	kA
</s>
<s>
Bankovky	bankovka	k1gFnPc4
série	série	k1gFnSc2
F	F	kA
(	(	kIx(
<g/>
2024	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
aversu	avers	k1gInSc2
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
reversuMotiv	reversuMotit	k5eAaPmDgInS
aversuMotiv	aversuMotit	k5eAaPmDgInS
reversuBarevný	reversuBarevný	k2eAgInSc4d1
tónŠířkaVýška	tónŠířkaVýšek	k1gMnSc4
</s>
<s>
¥	¥	k?
<g/>
1000	#num#	k4
</s>
<s>
Šibasaburó	Šibasaburó	k?
KitasatoVelká	KitasatoVelká	k1gFnSc1
vlna	vlna	k1gFnSc1
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
Kanagawy	Kanagawa	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
36	#num#	k4
pohledů	pohled	k1gInPc2
na	na	k7c4
horu	hora	k1gFnSc4
Fudži	Fudž	k1gFnSc3
<g/>
,	,	kIx,
od	od	k7c2
Kacušika	Kacušikum	k1gNnSc2
Hokusaie	Hokusaie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
150	#num#	k4
mm	mm	kA
<g/>
76	#num#	k4
mm	mm	kA
</s>
<s>
¥	¥	k?
<g/>
5000	#num#	k4
</s>
<s>
Umeko	Umeko	k1gNnSc1
Cudavistárie	Cudavistárie	k1gFnSc2
</s>
<s>
purpurová	purpurový	k2eAgFnSc1d1
<g/>
156	#num#	k4
mm	mm	kA
</s>
<s>
¥	¥	k?
<g/>
10000	#num#	k4
</s>
<s>
Eiiči	Eiič	k1gFnSc3
ŠibusawaTokijské	ŠibusawaTokijský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
Marunouči	Marunouč	k1gInPc7
<g/>
)	)	kIx)
<g/>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
160	#num#	k4
mm	mm	kA
</s>
<s>
Směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Japonský	japonský	k2eAgInSc1d1
jen	jen	k9
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Mince	mince	k1gFnSc1
japonského	japonský	k2eAgInSc2d1
jenu	jen	k1gInSc2
</s>
<s>
Mince	mince	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
5	#num#	k4
jenů	jen	k1gInPc2
</s>
<s>
Mince	mince	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
10	#num#	k4
jenů	jen	k1gInPc2
</s>
<s>
Líc	líc	k1gInSc1
10	#num#	k4
jenové	jenový	k2eAgFnSc2d1
mince	mince	k1gFnSc2
s	s	k7c7
obrázkem	obrázek	k1gInSc7
chrámu	chrám	k1gInSc2
Bjódóin	Bjódóina	k1gFnPc2
</s>
<s>
Mince	mince	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
jenů	jen	k1gInPc2
</s>
<s>
Bankovka	bankovka	k1gFnSc1
2000	#num#	k4
jenů	jen	k1gInPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.kurzy.cz/kurzy-men/kurzy.asp?A=H&	http://www.kurzy.cz/kurzy-men/kurzy.asp?A=H&	k?
Kurzy	kurz	k1gInPc1
měn	měna	k1gFnPc2
-	-	kIx~
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
-	-	kIx~
JPY	JPY	kA
<g/>
,	,	kIx,
Ekonomika	ekonomika	k1gFnSc1
ČNB	ČNB	kA
<g/>
,	,	kIx,
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Issuance	Issuance	k1gFnSc1
and	and	k?
Security	Securit	k1gInPc1
Features	Features	k1gInSc1
of	of	k?
2,000	2,000	k4
Yen	Yen	k1gFnSc1
Note	Note	k1gFnSc1
https://www.boj.or.jp/en/note_tfjgs/note/security/gizo2s.htm/	https://www.boj.or.jp/en/note_tfjgs/note/security/gizo2s.htm/	k4
<g/>
↑	↑	k?
Unwanted	Unwanted	k1gMnSc1
and	and	k?
unloved	unloved	k1gMnSc1
<g/>
,	,	kIx,
2,000	2,000	k4
yen	yen	k?
bills	bills	k1gInSc1
find	find	k?
few	few	k?
fans	fans	k1gInSc1
https://www.japantimes.co.jp/news/2006/09/06/business/unwanted-and-unloved-2000-yen-bills-find-few-fans/#.W4AuLecuCUl	https://www.japantimes.co.jp/news/2006/09/06/business/unwanted-and-unloved-2000-yen-bills-find-few-fans/#.W4AuLecuCUl	k1gMnSc1
<g/>
↑	↑	k?
Historical	Historical	k1gMnSc1
exchange	exchang	k1gFnSc2
rates	rates	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fxtop	Fxtop	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhledávání	vyhledávání	k1gNnSc1
100	#num#	k4
<g/>
_JPY_USD__	_JPY_USD__	k?
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
_	_	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
_	_	kIx~
<g/>
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
japonský	japonský	k2eAgInSc4d1
jen	jen	k9
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
jen	jen	k9
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
FAQ	FAQ	kA
o	o	k7c6
japonské	japonský	k2eAgFnSc6d1
měně	měna	k1gFnSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Japonské	japonský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Globální	globální	k2eAgFnSc2d1
rezervní	rezervní	k2eAgFnSc2d1
měny	měna	k1gFnSc2
</s>
<s>
Americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
USD	USD	kA
<g/>
)	)	kIx)
•	•	k?
Euro	euro	k1gNnSc1
(	(	kIx(
<g/>
EUR	euro	k1gNnPc2
<g/>
)	)	kIx)
•	•	k?
Čínský	čínský	k2eAgInSc4d1
jüan	jüan	k1gInSc4
(	(	kIx(
<g/>
CNY	CNY	kA
<g/>
)	)	kIx)
•	•	k?
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
GBP	GBP	kA
<g/>
)	)	kIx)
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
(	(	kIx(
<g/>
JPY	JPY	kA
<g/>
)	)	kIx)
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
(	(	kIx(
<g/>
CHF	CHF	kA
<g/>
)	)	kIx)
</s>
<s>
Měny	měna	k1gFnPc4
Asie	Asie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
Střední	střední	k2eAgFnSc2d1
</s>
<s>
Kazachstánský	kazachstánský	k2eAgInSc1d1
tenge	tenge	k1gInSc1
•	•	k?
Kyrgyzský	kyrgyzský	k2eAgInSc1d1
som	soma	k1gFnPc2
•	•	k?
Tádžický	tádžický	k2eAgInSc1d1
somoni	somoň	k1gFnSc3
•	•	k?
Turkmenský	turkmenský	k2eAgMnSc1d1
manat	manat	k2eAgMnSc1d1
•	•	k?
Uzbecký	uzbecký	k2eAgInSc1d1
sum	suma	k1gFnPc2
Jihozápadní	jihozápadní	k2eAgInSc5d1
</s>
<s>
Abchazský	abchazský	k2eAgInSc1d1
apsar	apsar	k1gInSc1
•	•	k?
Arménský	arménský	k2eAgInSc1d1
dram	drama	k1gFnPc2
•	•	k?
Ázerbájdžánský	ázerbájdžánský	k2eAgInSc1d1
manat	manat	k1gInSc1
•	•	k?
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Irácký	irácký	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Íránský	íránský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
•	•	k?
Jemenský	jemenský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Katarský	katarský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Kuvajtský	kuvajtský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Libanonská	libanonský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Ománský	ománský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Saúdský	saúdský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
afghání	afghání	k1gNnSc6
•	•	k?
Bangladéšská	bangladéšský	k2eAgFnSc1d1
taka	taka	k1gFnSc1
•	•	k?
Bhútánský	bhútánský	k2eAgInSc1d1
ngultrum	ngultrum	k1gNnSc1
•	•	k?
Indická	indický	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Maledivská	maledivský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Nepálská	nepálský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Pákistánská	pákistánský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Šrílanská	Šrílanský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
•	•	k?
Hongkongský	hongkongský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
•	•	k?
Jihokorejský	jihokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Macajská	macajský	k2eAgFnSc1d1
pataca	pataca	k1gFnSc1
•	•	k?
Mongolský	mongolský	k2eAgInSc1d1
tugrik	tugrik	k1gInSc1
•	•	k?
Tchajwanský	tchajwanský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
</s>
<s>
Brunejský	brunejský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Filipínské	filipínský	k2eAgNnSc4d1
peso	peso	k1gNnSc4
•	•	k?
Indonéská	indonéský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Kambodžský	kambodžský	k2eAgInSc1d1
riel	riel	k1gInSc1
•	•	k?
Laoský	laoský	k2eAgInSc1d1
kip	kip	k?
•	•	k?
Malajsijský	malajsijský	k2eAgInSc1d1
ringgit	ringgit	k1gInSc1
•	•	k?
Myanmarský	Myanmarský	k2eAgInSc1d1
kyat	kyat	k1gInSc1
•	•	k?
Severokorejský	severokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Singapurský	singapurský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Thajský	thajský	k2eAgInSc1d1
baht	baht	k1gInSc1
•	•	k?
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
•	•	k?
Východotimorské	Východotimorský	k2eAgFnPc4d1
centavové	centavový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4330513-1	4330513-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
91006335	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
91006335	#num#	k4
</s>
