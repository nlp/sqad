<s>
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Vladimír	Vladimír	k1gMnSc1	Vladimír
Štancl	Štancl	k1gMnSc1	Štancl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
cirkusové	cirkusový	k2eAgFnSc2d1	cirkusová
rodiny	rodina	k1gFnSc2	rodina
Kludských	Kludský	k2eAgMnPc2d1	Kludský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
matka	matka	k1gFnSc1	matka
Edita	Edita	k1gFnSc1	Edita
Štanclová	Štanclová	k1gFnSc1	Štanclová
rozená	rozený	k2eAgFnSc1d1	rozená
Kludská	Kludský	k2eAgFnSc1d1	Kludská
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
mimka	mimka	k1gFnSc1	mimka
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
Dagmar	Dagmar	k1gFnSc1	Dagmar
Kludská	Kludský	k2eAgFnSc1d1	Kludská
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
kartářka	kartářka	k1gFnSc1	kartářka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
příbuzný	příbuzný	k1gMnSc1	příbuzný
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
pouťové	pouťový	k2eAgFnPc4d1	pouťová
atrakce	atrakce	k1gFnPc4	atrakce
<g/>
,	,	kIx,	,
kolotoče	kolotoč	k1gInPc4	kolotoč
-	-	kIx~	-
rodiny	rodina	k1gFnPc4	rodina
Kočkových	Kočkových	k2eAgFnPc4d1	Kočkových
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
hudební	hudební	k2eAgFnSc6d1	hudební
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
založil	založit	k5eAaPmAgMnS	založit
jazzové	jazzový	k2eAgNnSc4d1	jazzové
kvarteto	kvarteto	k1gNnSc4	kvarteto
"	"	kIx"	"
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Hudebník	hudebník	k1gMnSc1	hudebník
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Pražských	pražský	k2eAgInPc6d1	pražský
jazzových	jazzový	k2eAgInPc6d1	jazzový
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Kroky	krok	k1gInPc1	krok
Františka	František	k1gMnSc2	František
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Kratochvílovou	Kratochvílová	k1gFnSc7	Kratochvílová
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hudebním	hudební	k2eAgInSc6d1	hudební
festivalu	festival	k1gInSc6	festival
Intertalent	Intertalent	k1gMnSc1	Intertalent
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Nenapovídej	napovídat	k5eNaBmRp2nS	napovídat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
novým	nový	k2eAgInSc7d1	nový
popovým	popový	k2eAgInSc7d1	popový
idolem	idol	k1gInSc7	idol
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
zní	znět	k5eAaImIp3nS	znět
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Vítr	vítr	k1gInSc1	vítr
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
z	z	k7c2	z
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
,	,	kIx,	,
Discopříběh	Discopříběh	k1gInSc4	Discopříběh
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
a	a	k8xC	a
Decibely	decibel	k1gInPc4	decibel
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Kroky	krok	k1gInPc1	krok
Františka	František	k1gMnSc2	František
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
)	)	kIx)	)
a	a	k8xC	a
prorežimní	prorežimní	k2eAgFnSc4d1	prorežimní
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
písně	píseň	k1gFnSc2	píseň
Poupata	poupě	k1gNnPc1	poupě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
Spartakiády	spartakiáda	k1gFnSc2	spartakiáda
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
v	v	k7c6	v
devadesatých	devadesatý	k2eAgInPc6d1	devadesatý
letech	let	k1gInPc6	let
vnímán	vnímat	k5eAaImNgInS	vnímat
převážně	převážně	k6eAd1	převážně
negativně	negativně	k6eAd1	negativně
jakožto	jakožto	k8xS	jakožto
exponent	exponent	k1gMnSc1	exponent
minulého	minulý	k2eAgInSc2d1	minulý
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
umělci	umělec	k1gMnPc7	umělec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
)	)	kIx)	)
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Komponuje	komponovat	k5eAaImIp3nS	komponovat
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
muzikálové	muzikálový	k2eAgFnSc3d1	muzikálová
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1995	[number]	k4	1995
založil	založit	k5eAaPmAgInS	založit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
společnost	společnost	k1gFnSc4	společnost
D.	D.	kA	D.
D.	D.	kA	D.
records	recordsa	k1gFnPc2	recordsa
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
Marcela	Marcela	k1gFnSc1	Marcela
Davidová	Davidová	k1gFnSc1	Davidová
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2001	[number]	k4	2001
svůj	svůj	k3xOyFgInSc4	svůj
poloviční	poloviční	k2eAgInSc4d1	poloviční
podíl	podíl	k1gInSc4	podíl
prodala	prodat	k5eAaPmAgFnS	prodat
Oldřichu	Oldřich	k1gMnSc3	Oldřich
Lichtenbergovi	Lichtenberg	k1gMnSc3	Lichtenberg
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
názvu	název	k1gInSc2	název
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
Cleopatra	Cleopatr	k1gMnSc4	Cleopatr
Musical	musical	k1gInSc4	musical
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
čeští	český	k2eAgMnPc1d1	český
hokejisté	hokejista	k1gMnPc1	hokejista
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladby	skladba	k1gFnPc1	skladba
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
byly	být	k5eAaImAgFnP	být
jejich	jejich	k3xOp3gInSc4	jejich
motivační	motivační	k2eAgInSc4d1	motivační
oporou	opora	k1gFnSc7	opora
v	v	k7c6	v
šatně	šatna	k1gFnSc6	šatna
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgMnS	využít
vlnu	vlna	k1gFnSc4	vlna
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
píseň	píseň	k1gFnSc1	píseň
Správnej	Správnej	k?	Správnej
tým	tým	k1gInSc1	tým
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
veřejného	veřejný	k2eAgInSc2d1	veřejný
a	a	k8xC	a
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
píseň	píseň	k1gFnSc1	píseň
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
noc	noc	k1gFnSc1	noc
složená	složený	k2eAgFnSc1d1	složená
pro	pro	k7c4	pro
Helenu	Helena	k1gFnSc4	Helena
Vondráčkovou	Vondráčková	k1gFnSc4	Vondráčková
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
podle	podle	k7c2	podle
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nakopl	nakopnout	k5eAaPmAgMnS	nakopnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
diskotékách	diskotéka	k1gFnPc6	diskotéka
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
tvorbě	tvorba	k1gFnSc3	tvorba
muzikálů	muzikál	k1gInPc2	muzikál
a	a	k8xC	a
komponování	komponování	k1gNnSc2	komponování
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
politických	politický	k2eAgNnPc6d1	politické
setkáních	setkání	k1gNnPc6	setkání
podporuje	podporovat	k5eAaImIp3nS	podporovat
ODS	ODS	kA	ODS
i	i	k8xC	i
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
mu	on	k3xPp3gMnSc3	on
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
Diamant	diamant	k1gInSc1	diamant
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
Šárka	Šárka	k1gFnSc1	Šárka
Grossová	Grossová	k1gFnSc1	Grossová
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
vydal	vydat	k5eAaPmAgInS	vydat
16	[number]	k4	16
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
například	například	k6eAd1	například
i	i	k8xC	i
v	v	k7c6	v
horroru	horror	k1gInSc6	horror
Hostel	Hostel	k1gInSc1	Hostel
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
tenistkou	tenistka	k1gFnSc7	tenistka
Marcelou	Marcela	k1gFnSc7	Marcela
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Skuherskou	Skuherský	k2eAgFnSc7d1	Skuherská
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Kláru	Klára	k1gFnSc4	Klára
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
fanynek	fanynka	k1gFnPc2	fanynka
nemanželského	manželský	k2eNgMnSc2d1	nemanželský
syna	syn	k1gMnSc2	syn
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Michaela	Michael	k1gMnSc2	Michael
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c6	na
leukemii	leukemie	k1gFnSc6	leukemie
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Křeslice	Křeslice	k1gFnSc2	Křeslice
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc1d1	vlastní
malé	malý	k2eAgNnSc1d1	malé
nahrávací	nahrávací	k2eAgNnSc1d1	nahrávací
studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
Zmrzlina	zmrzlina	k1gFnSc1	zmrzlina
-	-	kIx~	-
muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
Lou	Lou	k1gFnSc2	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1999	[number]	k4	1999
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
-	-	kIx~	-
autoři	autor	k1gMnPc1	autor
hudby	hudba	k1gFnSc2	hudba
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Malásek	Malásek	k1gMnSc1	Malásek
-	-	kIx~	-
texty	text	k1gInPc1	text
Václav	Václav	k1gMnSc1	Václav
Kopta	Kopt	k1gMnSc2	Kopt
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
1999	[number]	k4	1999
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
-	-	kIx~	-
texty	text	k1gInPc1	text
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Borovec	Borovec	k1gInSc1	Borovec
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Stropnická	stropnický	k2eAgFnSc1d1	Stropnická
a	a	k8xC	a
Lou	Lou	k1gFnSc1	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
Hagen	Hagen	k2eAgInSc1d1	Hagen
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
2002	[number]	k4	2002
Tři	tři	k4xCgMnPc4	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
hudby	hudba	k1gFnSc2	hudba
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
a	a	k8xC	a
Bryan	Bryan	k1gMnSc1	Bryan
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
Lou	Lou	k1gFnPc2	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
Angelika	Angelika	k1gFnSc1	Angelika
-	-	kIx~	-
texty	text	k1gInPc1	text
Lou	Lou	k1gFnSc1	Lou
Fanánek	Fanánek	k1gInSc1	Fanánek
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
-	-	kIx~	-
autoři	autor	k1gMnPc1	autor
hudby	hudba	k1gFnSc2	hudba
Bohouš	Bohouš	k1gMnSc1	Bohouš
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
též	též	k9	též
<g />
.	.	kIx.	.
</s>
<s>
producent	producent	k1gMnSc1	producent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
Lou	Lou	k1gFnPc2	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
Děti	dítě	k1gFnPc4	dítě
ráje	ráj	k1gInSc2	ráj
-	-	kIx~	-
muzikál	muzikál	k1gInSc1	muzikál
sestavený	sestavený	k2eAgInSc1d1	sestavený
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
Františka	František	k1gMnSc2	František
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
,	,	kIx,	,
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Bartáka	Barták	k1gMnSc2	Barták
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zabít	zabít	k5eAaPmF	zabít
Davida	David	k1gMnSc4	David
-	-	kIx~	-
parodie	parodie	k1gFnSc1	parodie
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
Michala	Michal	k1gMnSc2	Michal
Davida	David	k1gMnSc2	David
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
Kat	kat	k1gMnSc1	kat
Mydlář	mydlář	k1gMnSc1	mydlář
-	-	kIx~	-
historický	historický	k2eAgInSc1d1	historický
muzikál	muzikál	k1gInSc1	muzikál
popisující	popisující	k2eAgInSc1d1	popisující
strhující	strhující	k2eAgInSc4d1	strhující
osud	osud	k1gInSc4	osud
známého	známý	k2eAgMnSc2d1	známý
pražského	pražský	k2eAgMnSc2d1	pražský
kata	kat	k1gMnSc2	kat
Mydláře	mydlář	k1gMnSc2	mydlář
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
Andílci	andílek	k1gMnPc1	andílek
za	za	k7c7	za
školou	škola	k1gFnSc7	škola
-	-	kIx~	-
teenagerský	teenagerský	k2eAgInSc1d1	teenagerský
muzikál	muzikál	k1gInSc1	muzikál
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
5	[number]	k4	5
<g/>
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
Lou	Lou	k1gFnPc2	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
2012	[number]	k4	2012
Mata	mást	k5eAaImSgInS	mást
Hari	Har	k1gMnPc5	Har
-	-	kIx~	-
retro	retro	k1gNnSc4	retro
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
Lou	Lou	k1gFnPc2	Lou
Fanánek	Fanánka	k1gFnPc2	Fanánka
<g />
.	.	kIx.	.
</s>
<s>
Hagen	Hagen	k1gInSc1	Hagen
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
2013	[number]	k4	2013
1982	[number]	k4	1982
-	-	kIx~	-
Nenapovídej	napovídat	k5eNaBmRp2nS	napovídat
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenapovídej	napovídat	k5eNaBmRp2nS	napovídat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
galaxie	galaxie	k1gFnSc1	galaxie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Líbezná	líbezný	k2eAgFnSc1d1	líbezná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dívčí	dívčí	k2eAgInSc1d1	dívčí
pláč	pláč	k1gInSc1	pláč
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jsi	být	k5eAaImIp2nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
můžeš	moct	k5eAaImIp2nS	moct
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Královna	královna	k1gFnSc1	královna
krásy	krása	k1gFnSc2	krása
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
-	-	kIx~	-
Non	Non	k1gFnSc1	Non
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Non	Non	k1gFnSc1	Non
stop	stopa	k1gFnPc2	stopa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tak	tak	k8xS	tak
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
měj	mít	k5eAaImRp2nS	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
máme	mít	k5eAaImIp1nP	mít
prima	prima	k2eAgMnPc1d1	prima
rodiče	rodič	k1gMnPc1	rodič
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zůstaň	zůstat	k5eAaPmRp2nS	zůstat
a	a	k8xC	a
neodcházej	odcházet	k5eNaImRp2nS	odcházet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zastav	zastavit	k5eAaPmRp2nS	zastavit
můj	můj	k1gMnSc1	můj
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1985	[number]	k4	1985
-	-	kIx~	-
Rodinná	rodinný	k2eAgNnPc1d1	rodinné
show	show	k1gNnPc1	show
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Céčka	céčko	k1gNnPc4	céčko
<g/>
,	,	kIx,	,
sbírá	sbírat	k5eAaImIp3nS	sbírat
céčka	céčko	k1gNnPc1	céčko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vyznání	vyznání	k1gNnSc2	vyznání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Šoumen	Šoumen	k1gInSc1	Šoumen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Valčík	valčík	k1gInSc1	valčík
pro	pro	k7c4	pro
mámu	máma	k1gFnSc4	máma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
-	-	kIx~	-
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
oslaví	oslavit	k5eAaPmIp3nS	oslavit
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zabitej	Zabitej	k?	Zabitej
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nálezů	nález	k1gInPc2	nález
a	a	k8xC	a
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
slogan	slogan	k1gInSc1	slogan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
-	-	kIx~	-
Děti	dítě	k1gFnPc1	dítě
ráje	ráje	k1gFnPc1	ráje
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
ráje	ráje	k1gFnPc1	ráje
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
mi	já	k3xPp1nSc3	já
tě	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
závidí	závidět	k5eAaImIp3nP	závidět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
nezměním	změnit	k5eNaPmIp1nS	změnit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Diskoborci	Diskoborce	k1gMnSc6	Diskoborce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
příběh	příběh	k1gInSc1	příběh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
-	-	kIx~	-
Discopříběh	Discopříběh	k1gInSc4	Discopříběh
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Discopříběh	Discopříběh	k1gInSc1	Discopříběh
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vidím	vidět	k5eAaImIp1nS	vidět
tě	ty	k3xPp2nSc4	ty
všude	všude	k6eAd1	všude
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
blízko	blízko	k6eAd1	blízko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Máma	máma	k1gFnSc1	máma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hele	Hela	k1gFnSc3	Hela
nemachruj	machrovat	k5eNaBmRp2nS	machrovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
-	-	kIx~	-
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pár	pár	k4xCyI	pár
přátel	přítel	k1gMnPc2	přítel
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ikaros	Ikaros	k1gMnSc1	Ikaros
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Diskžokej	diskžokej	k1gMnSc1	diskžokej
nepřijel	přijet	k5eNaPmAgMnS	přijet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Samotář	samotář	k1gMnSc1	samotář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Výtah	výtah	k1gInSc1	výtah
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
-	-	kIx~	-
Allegro	allegro	k1gNnSc4	allegro
(	(	kIx(	(
<g/>
hity	hit	k1gInPc7	hit
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
snadno	snadno	k6eAd1	snadno
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
se	se	k3xPyFc4	se
lhářům	lhář	k1gMnPc3	lhář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vůně	vůně	k1gFnSc1	vůně
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Potlesk	potlesk	k1gInSc1	potlesk
patří	patřit	k5eAaImIp3nS	patřit
vítězům	vítěz	k1gMnPc3	vítěz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
pátý	pátý	k4xOgMnSc1	pátý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
-	-	kIx~	-
Discopříběh	Discopříběh	k1gInSc1	Discopříběh
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Právě	právě	k6eAd1	právě
začínáme	začínat	k5eAaImIp1nP	začínat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Past	past	k1gFnSc1	past
na	na	k7c4	na
mýho	mýho	k?	mýho
tátu	táta	k1gMnSc4	táta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Půjdu	jít	k5eAaImIp1nS	jít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Jindřich	Jindřich	k1gMnSc1	Jindřich
Parma	Parma	k1gFnSc1	Parma
-	-	kIx~	-
"	"	kIx"	"
<g/>
Žádná	žádný	k3yNgFnSc1	žádný
bouřka	bouřka	k1gFnSc1	bouřka
věčně	věčně	k6eAd1	věčně
netrvá	trvat	k5eNaImIp3nS	trvat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Když	když	k8xS	když
tátové	táta	k1gMnPc1	táta
blbnou	blbnout	k5eAaImIp3nP	blbnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tak	tak	k9	tak
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
šlápni	šlápnout	k5eAaPmRp2nS	šlápnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
-	-	kIx~	-
Leo	Leo	k1gMnSc1	Leo
Music	Music	k1gMnSc1	Music
<g/>
:	:	kIx,	:
Erotic	Erotice	k1gFnPc2	Erotice
House	house	k1gNnSc1	house
1998	[number]	k4	1998
-	-	kIx~	-
Super	super	k2eAgFnSc1d1	super
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Správnej	Správnej	k?	Správnej
tým	tým	k1gInSc1	tým
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
ruku	ruka	k1gFnSc4	ruka
mi	já	k3xPp1nSc3	já
dej	dát	k5eAaPmRp2nS	dát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zas	zas	k9	zas
bude	být	k5eAaImBp3nS	být
OK	oko	k1gNnPc2	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
To	ten	k3xDgNnSc4	ten
ti	ty	k3xPp2nSc3	ty
nikdy	nikdy	k6eAd1	nikdy
neslíbím	slíbit	k5eNaPmIp1nS	slíbit
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsou	být	k5eAaImIp3nP	být
rána	rána	k1gFnSc1	rána
tíživá	tíživý	k2eAgFnSc1d1	tíživá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lásko	láska	k1gFnSc5	láska
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
bejval	bejvat	k5eAaPmAgInS	bejvat
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Už	už	k9	už
mi	já	k3xPp1nSc3	já
nevolej	volat	k5eNaImRp2nS	volat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Abnormální	abnormální	k2eAgFnSc1d1	abnormální
hic	hic	k?	hic
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ruská	ruský	k2eAgFnSc1d1	ruská
Máša	Máša	k1gFnSc1	Máša
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hraj	hrát	k5eAaImRp2nS	hrát
<g/>
,	,	kIx,	,
jen	jen	k9	jen
hraj	hrát	k5eAaImRp2nS	hrát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fámy	fáma	k1gFnPc1	fáma
o	o	k7c4	o
nás	my	k3xPp1nPc4	my
<g />
.	.	kIx.	.
</s>
<s>
jdou	jít	k5eAaImIp3nP	jít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
neztrácej	ztrácet	k5eNaImRp2nS	ztrácet
čas	čas	k1gInSc4	čas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
-	-	kIx~	-
Love	lov	k1gInSc5	lov
Songs	Songs	k1gInSc4	Songs
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každý	každý	k3xTgMnSc1	každý
mi	já	k3xPp1nSc3	já
tě	ty	k3xPp2nSc4	ty
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
závidí	závidět	k5eAaImIp3nP	závidět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
snadno	snadno	k6eAd1	snadno
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
se	se	k3xPyFc4	se
lhářům	lhář	k1gMnPc3	lhář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nálezů	nález	k1gInPc2	nález
a	a	k8xC	a
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Disco	disco	k1gNnSc4	disco
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Decibely	decibel	k1gInPc1	decibel
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dívka	dívka	k1gFnSc1	dívka
na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nonstop	nonstop	k6eAd1	nonstop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc4	ten
zas	zas	k6eAd1	zas
byl	být	k5eAaImAgInS	být
den	den	k1gInSc1	den
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Po	po	k7c6	po
cestách	cesta	k1gFnPc6	cesta
růžových	růžový	k2eAgMnPc2d1	růžový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Muzikálové	muzikálový	k2eAgFnSc2d1	muzikálová
balady	balada	k1gFnSc2	balada
(	(	kIx(	(
<g/>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teď	teď	k6eAd1	teď
královnou	královna	k1gFnSc7	královna
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Čas	čas	k1gInSc1	čas
na	na	k7c4	na
lásku	láska	k1gFnSc4	láska
mít	mít	k5eAaImF	mít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zázrak	zázrak	k1gInSc1	zázrak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Nilu	Nil	k1gInSc6	Nil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
50	[number]	k4	50
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
+	+	kIx~	+
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
50	[number]	k4	50
<g/>
:	:	kIx,	:
Mejdan	mejdan	k1gInSc1	mejdan
roku	rok	k1gInSc2	rok
-	-	kIx~	-
O2	O2	k1gFnSc1	O2
Arena	Arena	k1gFnSc1	Arena
Live	Live	k1gFnSc1	Live
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Čas	čas	k1gInSc1	čas
vítězství	vítězství	k1gNnSc1	vítězství
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
hity	hit	k1gInPc1	hit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čas	čas	k1gInSc1	čas
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tajfun	tajfun	k1gInSc1	tajfun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
vázne	váznout	k5eAaImIp3nS	váznout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nekonečno	nekonečno	k1gNnSc4	nekonečno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
-	-	kIx~	-
Lucerna	lucerna	k1gFnSc1	lucerna
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
special	special	k1gMnSc1	special
guest	guest	k1gMnSc1	guest
<g/>
:	:	kIx,	:
Ricchi	Ricchi	k1gNnSc1	Ricchi
e	e	k0	e
Poveri	Pover	k1gInSc3	Pover
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
-	-	kIx~	-
kategorie	kategorie	k1gFnPc1	kategorie
Zpěváků	Zpěvák	k1gMnPc2	Zpěvák
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
TýTý	TýTý	k1gFnPc2	TýTý
-	-	kIx~	-
kategorie	kategorie	k1gFnSc1	kategorie
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
