<s>
Frankfort	Frankfort	k1gInSc1	Frankfort
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Franklin	Franklina	k1gFnPc2	Franklina
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
25527	[number]	k4	25527
obyvatel	obyvatel	k1gMnPc2	obyvatel
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
tedy	tedy	k9	tedy
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejméně	málo	k6eAd3	málo
obydlené	obydlený	k2eAgNnSc4d1	obydlené
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
25	[number]	k4	25
527	[number]	k4	527
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlilo	sídlit	k5eAaImAgNnS	sídlit
27	[number]	k4	27
741	[number]	k4	741
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
12	[number]	k4	12
314	[number]	k4	314
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
6	[number]	k4	6
945	[number]	k4	945
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
721,1	[number]	k4	721,1
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
77,1	[number]	k4	77,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
16,5	[number]	k4	16,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,9	[number]	k4	2,9
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
3,8	[number]	k4	3,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
21,6	[number]	k4	21,6
%	%	kIx~	%
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
11,7	[number]	k4	11,7
%	%	kIx~	%
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
30,3	[number]	k4	30,3
%	%	kIx~	%
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
22,4	[number]	k4	22,4
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
14	[number]	k4	14
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
36	[number]	k4	36
let	léto	k1gNnPc2	léto
Daniel	Daniel	k1gMnSc1	Daniel
Weisiger	Weisiger	k1gMnSc1	Weisiger
Adams	Adamsa	k1gFnPc2	Adamsa
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
-	-	kIx~	-
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
konfederační	konfederační	k2eAgFnSc2d1	konfederační
armády	armáda	k1gFnSc2	armáda
během	během	k7c2	během
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
San	San	k1gFnPc2	San
Pedro	Pedro	k1gNnSc1	Pedro
de	de	k?	de
Macorís	Macorísa	k1gFnPc2	Macorísa
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
</s>
