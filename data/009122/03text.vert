<p>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
slib	slib	k1gInSc1	slib
přednáší	přednášet	k5eAaImIp3nS	přednášet
sportovec	sportovec	k1gMnSc1	sportovec
a	a	k8xC	a
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zahajovacího	zahajovací	k2eAgInSc2d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sportovec	sportovec	k1gMnSc1	sportovec
z	z	k7c2	z
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
drží	držet	k5eAaImIp3nS	držet
cíp	cíp	k1gInSc1	cíp
olympijské	olympijský	k2eAgFnSc2d1	olympijská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
přednáší	přednášet	k5eAaImIp3nP	přednášet
slova	slovo	k1gNnPc1	slovo
slibu	slib	k1gInSc2	slib
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jménem	jméno	k1gNnSc7	jméno
všech	všecek	k3xTgMnPc2	všecek
závodníků	závodník	k1gMnPc2	závodník
slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystoupíme	vystoupit	k5eAaPmIp1nP	vystoupit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
jako	jako	k8xC	jako
čestní	čestný	k2eAgMnPc1d1	čestný
soupeři	soupeř	k1gMnPc1	soupeř
<g/>
,	,	kIx,	,
poslušni	poslušen	k2eAgMnPc1d1	poslušen
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
,	,	kIx,	,
za	za	k7c4	za
sport	sport	k1gInSc4	sport
bez	bez	k7c2	bez
dopingu	doping	k1gInSc2	doping
a	a	k8xC	a
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
v	v	k7c6	v
rytířském	rytířský	k2eAgMnSc6d1	rytířský
duchu	duch	k1gMnSc6	duch
pro	pro	k7c4	pro
slávu	sláva	k1gFnSc4	sláva
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
čest	čest	k1gFnSc1	čest
našich	náš	k3xOp1gNnPc2	náš
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
<g/>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
také	také	k9	také
z	z	k7c2	z
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
slibuje	slibovat	k5eAaImIp3nS	slibovat
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
s	s	k7c7	s
cípem	cíp	k1gInSc7	cíp
olympijské	olympijský	k2eAgFnSc2d1	olympijská
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jménem	jméno	k1gNnSc7	jméno
všech	všecek	k3xTgMnPc2	všecek
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
a	a	k8xC	a
činovníků	činovník	k1gMnPc2	činovník
slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
splníme	splnit	k5eAaPmIp1nP	splnit
nám	my	k3xPp1nPc3	my
uložené	uložený	k2eAgInPc4d1	uložený
úkoly	úkol	k1gInPc4	úkol
ve	v	k7c6	v
vší	všecek	k3xTgFnSc6	všecek
nestrannosti	nestrannost	k1gFnSc6	nestrannost
<g/>
,	,	kIx,	,
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
k	k	k7c3	k
řádům	řád	k1gInPc3	řád
a	a	k8xC	a
věrni	věren	k2eAgMnPc1d1	věren
zásadám	zásada	k1gFnPc3	zásada
sportovního	sportovní	k2eAgMnSc2d1	sportovní
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
další	další	k2eAgInSc4d1	další
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
trenéry	trenér	k1gMnPc4	trenér
z	z	k7c2	z
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jménem	jméno	k1gNnSc7	jméno
všech	všecek	k3xTgMnPc2	všecek
trenérů	trenér	k1gMnPc2	trenér
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
doprovodu	doprovod	k1gInSc2	doprovod
závodníků	závodník	k1gMnPc2	závodník
slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
zavazujeme	zavazovat	k5eAaImIp1nP	zavazovat
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
dodržovat	dodržovat	k5eAaImF	dodržovat
zásady	zásada	k1gFnPc1	zásada
sportovního	sportovní	k2eAgMnSc2d1	sportovní
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
hry	hra	k1gFnSc2	hra
fair	fair	k6eAd1	fair
play	play	k0	play
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
principy	princip	k1gInPc7	princip
olympismu	olympismus	k1gInSc2	olympismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Olympijský	olympijský	k2eAgInSc4d1	olympijský
slib	slib	k1gInSc4	slib
napsal	napsat	k5eAaPmAgMnS	napsat
baron	baron	k1gMnSc1	baron
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k2eAgMnSc1d1	Coubertin
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
zazněl	zaznět	k5eAaImAgInS	zaznět
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1920	[number]	k4	1920
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
olympijského	olympijský	k2eAgInSc2d1	olympijský
slibu	slib	k1gInSc2	slib
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
mírně	mírně	k6eAd1	mírně
měnil	měnit	k5eAaImAgInS	měnit
<g/>
,	,	kIx,	,
desetibojař	desetibojař	k1gMnSc1	desetibojař
Victor	Victor	k1gMnSc1	Victor
Boin	Boin	k1gMnSc1	Boin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přísaháme	přísahat	k5eAaImIp1nP	přísahat
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastníme	zúčastnit	k5eAaPmIp1nP	zúčastnit
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
rytířském	rytířský	k2eAgMnSc6d1	rytířský
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
čest	čest	k1gFnSc4	čest
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
a	a	k8xC	a
slávu	sláva	k1gFnSc4	sláva
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
přísaháme	přísahat	k5eAaImIp1nP	přísahat
<g/>
"	"	kIx"	"
zaměněno	zaměnit	k5eAaPmNgNnS	zaměnit
za	za	k7c7	za
"	"	kIx"	"
<g/>
slibujeme	slibovat	k5eAaImIp1nP	slibovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
za	za	k7c4	za
"	"	kIx"	"
<g/>
družstvo	družstvo	k1gNnSc4	družstvo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
dopingu	doping	k1gInSc2	doping
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
až	až	k9	až
pro	pro	k7c4	pro
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
skládali	skládat	k5eAaImAgMnP	skládat
první	první	k4xOgInSc4	první
slib	slib	k1gInSc4	slib
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1972	[number]	k4	1972
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovci	sportovec	k1gMnPc1	sportovec
a	a	k8xC	a
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
skládali	skládat	k5eAaImAgMnP	skládat
slib	slib	k1gInSc4	slib
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
