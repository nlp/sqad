<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
označoval	označovat	k5eAaImAgInS	označovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
strunný	strunný	k2eAgInSc4d1	strunný
nástroj	nástroj	k1gInSc4	nástroj
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
kontextu	kontext	k1gInSc6	kontext
jako	jako	k9	jako
muzika	muzika	k1gFnSc1	muzika
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
muzikant	muzikant	k1gMnSc1	muzikant
<g/>
)	)	kIx)	)
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
systém	systém	k1gInSc4	systém
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
rytmické	rytmický	k2eAgNnSc4d1	rytmické
členění	členění	k1gNnSc4	členění
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
uspořádání	uspořádání	k1gNnSc6	uspořádání
určují	určovat	k5eAaImIp3nP	určovat
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
estetické	estetický	k2eAgNnSc4d1	estetické
působení	působení	k1gNnSc4	působení
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
estetické	estetický	k2eAgNnSc1d1	estetické
působení	působení	k1gNnSc1	působení
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
historicky	historicky	k6eAd1	historicky
proměnných	proměnný	k2eAgNnPc2d1	proměnné
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
dobového	dobový	k2eAgInSc2d1	dobový
vkusu	vkus	k1gInSc2	vkus
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
ritus	ritus	k1gInSc4	ritus
a	a	k8xC	a
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
umění	umění	k1gNnSc4	umění
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojené	spojený	k2eAgFnPc4d1	spojená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
muzikologie	muzikologie	k1gFnSc1	muzikologie
nebo	nebo	k8xC	nebo
též	též	k9	též
hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
umění	umění	k1gNnPc2	umění
se	se	k3xPyFc4	se
u	u	k7c2	u
hudby	hudba	k1gFnSc2	hudba
nelze	lze	k6eNd1	lze
příliš	příliš	k6eAd1	příliš
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
přímé	přímý	k2eAgInPc4d1	přímý
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
teorie	teorie	k1gFnPc4	teorie
nutně	nutně	k6eAd1	nutně
částečně	částečně	k6eAd1	částečně
spekulativní	spekulativní	k2eAgMnPc1d1	spekulativní
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
následující	následující	k2eAgInPc1d1	následující
názory	názor	k1gInPc1	názor
<g/>
:	:	kIx,	:
Hudba	hudba	k1gFnSc1	hudba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k8xS	jako
doprovod	doprovod	k1gInSc1	doprovod
společné	společný	k2eAgFnSc2d1	společná
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
socializace	socializace	k1gFnSc2	socializace
<g/>
.	.	kIx.	.
</s>
<s>
Rytmická	rytmický	k2eAgFnSc1d1	rytmická
složka	složka	k1gFnSc1	složka
hudby	hudba	k1gFnSc2	hudba
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
pracovní	pracovní	k2eAgNnSc4d1	pracovní
tempo	tempo	k1gNnSc4	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
napodobováním	napodobování	k1gNnSc7	napodobování
přírodních	přírodní	k2eAgInPc2d1	přírodní
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zpěvu	zpěv	k1gInSc3	zpěv
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
povaha	povaha	k1gFnSc1	povaha
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mimetická	mimetický	k2eAgFnSc1d1	mimetická
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pohled	pohled	k1gInSc4	pohled
hovoří	hovořit	k5eAaImIp3nS	hovořit
pojetí	pojetí	k1gNnSc1	pojetí
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
primitivních	primitivní	k2eAgFnPc6d1	primitivní
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
společně	společně	k6eAd1	společně
s	s	k7c7	s
řečí	řeč	k1gFnSc7	řeč
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jakýsi	jakýsi	k3yIgInSc1	jakýsi
prajazyk	prajazyk	k1gInSc1	prajazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
povaze	povaha	k1gFnSc6	povaha
tzv.	tzv.	kA	tzv.
tónových	tónový	k2eAgInPc2d1	tónový
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
čínština	čínština	k1gFnSc1	čínština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
význam	význam	k1gInSc1	význam
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
pomocí	pomocí	k7c2	pomocí
melodie	melodie	k1gFnSc2	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
pro	pro	k7c4	pro
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
neexistují	existovat	k5eNaImIp3nP	existovat
nezvratné	zvratný	k2eNgInPc1d1	nezvratný
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
odlišného	odlišný	k2eAgNnSc2d1	odlišné
pojetí	pojetí	k1gNnSc2	pojetí
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kulturách	kultura	k1gFnPc6	kultura
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
právě	právě	k6eAd1	právě
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
zdroj	zdroj	k1gInSc1	zdroj
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Libozvučnost	libozvučnost	k1gFnSc1	libozvučnost
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
sociální	sociální	k2eAgInSc4d1	sociální
konstrukt	konstrukt	k1gInSc4	konstrukt
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
však	však	k9	však
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
tradovanému	tradovaný	k2eAgNnSc3d1	tradované
mínění	mínění	k1gNnSc3	mínění
<g/>
,	,	kIx,	,
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
zlepšení	zlepšení	k1gNnSc1	zlepšení
jazykových	jazykový	k2eAgFnPc2d1	jazyková
a	a	k8xC	a
matematických	matematický	k2eAgFnPc2d1	matematická
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
dělit	dělit	k5eAaImF	dělit
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
dělení	dělení	k1gNnSc4	dělení
patří	patřit	k5eAaImIp3nS	patřit
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
ke	k	k7c3	k
spiritualitě	spiritualita	k1gFnSc3	spiritualita
<g/>
:	:	kIx,	:
Duchovní	duchovní	k2eAgInSc1d1	duchovní
–	–	k?	–
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
k	k	k7c3	k
duchovnu	duchovno	k1gNnSc3	duchovno
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc3	náboženství
Světská	světský	k2eAgFnSc1d1	světská
–	–	k?	–
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ostatní	ostatní	k2eAgNnPc4d1	ostatní
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
náměty	námět	k1gInPc4	námět
<g/>
.	.	kIx.	.
</s>
<s>
Spadá	spadat	k5eAaImIp3nS	spadat
sem	sem	k6eAd1	sem
hudba	hudba	k1gFnSc1	hudba
koncertantní	koncertantní	k2eAgFnSc1d1	koncertantní
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc1d1	taneční
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
<g/>
:	:	kIx,	:
Umělá	umělý	k2eAgFnSc1d1	umělá
–	–	k?	–
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
práce	práce	k1gFnSc2	práce
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
či	či	k8xC	či
skupiny	skupina	k1gFnPc1	skupina
Lidová	lidový	k2eAgFnSc1d1	lidová
–	–	k?	–
autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
časem	časem	k6eAd1	časem
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
Podle	podle	k7c2	podle
záměru	záměr	k1gInSc2	záměr
autora	autor	k1gMnSc2	autor
<g/>
:	:	kIx,	:
Artificiální	artificiální	k2eAgFnSc1d1	artificiální
(	(	kIx(	(
<g/>
umělecká	umělecký	k2eAgFnSc1d1	umělecká
<g/>
)	)	kIx)	)
-	-	kIx~	-
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
vložit	vložit	k5eAaPmF	vložit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
a	a	k8xC	a
estetickou	estetický	k2eAgFnSc4d1	estetická
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nonartificiální	Nonartificiální	k2eAgFnSc1d1	Nonartificiální
(	(	kIx(	(
<g/>
neumělecká	umělecký	k2eNgFnSc1d1	neumělecká
<g/>
)	)	kIx)	)
-	-	kIx~	-
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
nesnaží	snažit	k5eNaImIp3nS	snažit
o	o	k7c4	o
vysoké	vysoký	k2eAgInPc4d1	vysoký
umělecké	umělecký	k2eAgInPc4d1	umělecký
a	a	k8xC	a
estetické	estetický	k2eAgInPc4d1	estetický
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Jejím	její	k3xOp3gInSc7	její
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
zabavit	zabavit	k5eAaPmF	zabavit
publikum	publikum	k1gNnSc1	publikum
<g/>
)	)	kIx)	)
Tyto	tento	k3xDgFnPc1	tento
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
orientační	orientační	k2eAgFnPc1d1	orientační
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
proto	proto	k8xC	proto
striktně	striktně	k6eAd1	striktně
oddělené	oddělený	k2eAgFnPc4d1	oddělená
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lidová	lidový	k2eAgFnSc1d1	lidová
duchovní	duchovní	k2eAgFnSc1d1	duchovní
hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
gospel	gospel	k1gInSc1	gospel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
žánru	žánr	k1gInSc2	žánr
<g/>
:	:	kIx,	:
viz	vidět	k5eAaImRp2nS	vidět
příslušné	příslušný	k2eAgInPc4d1	příslušný
oddíly	oddíl	k1gInPc4	oddíl
Moderní	moderní	k2eAgFnSc1d1	moderní
hudba	hudba	k1gFnSc1	hudba
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
,	,	kIx,	,
punk	punk	k1gInSc1	punk
<g/>
,	,	kIx,	,
techno	techno	k1gNnSc1	techno
<g/>
,	,	kIx,	,
dubstep	dubstep	k1gInSc1	dubstep
<g/>
,	,	kIx,	,
pop	pop	k1gInSc1	pop
a	a	k8xC	a
hip-hop	hipop	k1gInSc1	hip-hop
Muzikologie	muzikologie	k1gFnSc2	muzikologie
je	být	k5eAaImIp3nS	být
vědou	věda	k1gFnSc7	věda
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Willy	Willa	k1gFnSc2	Willa
Apel	apel	k1gInSc1	apel
<g/>
,	,	kIx,	,
Harvard	Harvard	k1gInSc1	Harvard
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
s.	s.	k?	s.
473	[number]	k4	473
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
oblastí	oblast	k1gFnSc7	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
získáváním	získávání	k1gNnSc7	získávání
a	a	k8xC	a
systematizací	systematizace	k1gFnSc7	systematizace
poznatků	poznatek	k1gInPc2	poznatek
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
(	(	kIx(	(
<g/>
Glen	Glen	k1gMnSc1	Glen
Haydon	Haydon	k1gMnSc1	Haydon
<g/>
,	,	kIx,	,
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
musicology	musicolog	k1gMnPc4	musicolog
<g/>
,	,	kIx,	,
Chapel	Chapel	k1gMnSc1	Chapel
Hill	Hill	k1gMnSc1	Hill
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
)	)	kIx)	)
Pod	pod	k7c7	pod
hudební	hudební	k2eAgFnSc7d1	hudební
vědou	věda	k1gFnSc7	věda
<g />
.	.	kIx.	.
</s>
<s>
sdružujeme	sdružovat	k5eAaImIp1nP	sdružovat
dnes	dnes	k6eAd1	dnes
ve	v	k7c4	v
všeobecnosti	všeobecnost	k1gFnPc4	všeobecnost
a	a	k8xC	a
<g/>
)	)	kIx)	)
akustiku	akustika	k1gFnSc4	akustika
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
)	)	kIx)	)
tónovou	tónový	k2eAgFnSc4d1	tónová
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
tónovou	tónový	k2eAgFnSc4d1	tónová
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
)	)	kIx)	)
hudební	hudební	k2eAgFnSc4d1	hudební
estetiku	estetika	k1gFnSc4	estetika
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
)	)	kIx)	)
hudební	hudební	k2eAgFnSc4d1	hudební
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
e	e	k0	e
<g/>
)	)	kIx)	)
hudební	hudební	k2eAgFnPc1d1	hudební
dějiny	dějiny	k1gFnPc1	dějiny
(	(	kIx(	(
<g/>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Schiedermeier	Schiedermeier	k1gMnSc1	Schiedermeier
<g/>
:	:	kIx,	:
Einführung	Einführung	k1gMnSc1	Einführung
in	in	k?	in
das	das	k?	das
Studium	studium	k1gNnSc1	studium
der	drát	k5eAaImRp2nS	drát
Musikgeschichte	Musikgeschicht	k1gMnSc5	Musikgeschicht
(	(	kIx(	(
<g/>
Bonn	Bonn	k1gInSc1	Bonn
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
vědec	vědec	k1gMnSc1	vědec
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
nebo	nebo	k8xC	nebo
písemné	písemný	k2eAgFnSc2d1	písemná
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
může	moct	k5eAaImIp3nS	moct
osvětlit	osvětlit	k5eAaPmF	osvětlit
její	její	k3xOp3gFnPc1	její
lidské	lidský	k2eAgFnPc1d1	lidská
vazby	vazba	k1gFnPc1	vazba
<g/>
...	...	k?	...
poznatky	poznatek	k1gInPc1	poznatek
hudebního	hudební	k2eAgMnSc2d1	hudební
vědce	vědec	k1gMnSc2	vědec
musí	muset	k5eAaImIp3nP	muset
pokrýt	pokrýt	k5eAaPmF	pokrýt
všechny	všechen	k3xTgFnPc4	všechen
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
dějinný	dějinný	k2eAgInSc4d1	dějinný
průběh	průběh	k1gInSc4	průběh
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výzkumu	výzkum	k1gInSc6	výzkum
se	se	k3xPyFc4	se
hudební	hudební	k2eAgMnSc1d1	hudební
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
vědec	vědec	k1gMnSc1	vědec
je	být	k5eAaImIp3nS	být
především	především	k9	především
historikem	historik	k1gMnSc7	historik
...	...	k?	...
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
včera	včera	k6eAd1	včera
přednesená	přednesený	k2eAgFnSc1d1	přednesená
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Harrison	Harrison	k1gMnSc1	Harrison
–	–	k?	–
Hood	Hood	k1gMnSc1	Hood
–	–	k?	–
Palisca	Palisca	k1gMnSc1	Palisca
<g/>
:	:	kIx,	:
Musicology	Musicolog	k1gMnPc7	Musicolog
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
s.	s.	k?	s.
116	[number]	k4	116
<g/>
,	,	kIx,	,
119	[number]	k4	119
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
