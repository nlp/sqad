<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojené	spojený	k2eAgFnPc4d1	spojená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
muzikologie	muzikologie	k1gFnSc1	muzikologie
nebo	nebo	k8xC	nebo
též	též	k9	též
hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
