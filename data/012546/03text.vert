<p>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc4	maso
tura	tur	k1gMnSc2	tur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgFnSc4	třetí
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgNnSc4d1	používané
maso	maso	k1gNnSc4	maso
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
kuchyních	kuchyně	k1gFnPc6	kuchyně
(	(	kIx(	(
za	za	k7c7	za
vepřovým	vepřový	k2eAgNnSc7d1	vepřové
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
kuřecím	kuřecí	k2eAgInPc3d1	kuřecí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
úpravách	úprava	k1gFnPc6	úprava
<g/>
:	:	kIx,	:
v	v	k7c6	v
celku	celek	k1gInSc6	celek
(	(	kIx(	(
<g/>
jako	jako	k9	jako
např.	např.	kA	např.
hovězí	hovězí	k2eAgFnSc1d1	hovězí
pečeně	pečeně	k1gFnSc1	pečeně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nakrájené	nakrájený	k2eAgInPc4d1	nakrájený
na	na	k7c4	na
plátky	plátek	k1gInPc4	plátek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
steak	steak	k1gInSc1	steak
nebo	nebo	k8xC	nebo
biftek	biftek	k1gInSc1	biftek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
drobné	drobný	k2eAgInPc4d1	drobný
kousky	kousek	k1gInPc4	kousek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
guláš	guláš	k1gInSc4	guláš
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mleté	mletý	k2eAgFnPc1d1	mletá
(	(	kIx(	(
<g/>
karbanátek	karbanátek	k1gInSc1	karbanátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
jej	on	k3xPp3gMnSc4	on
průměrně	průměrně	k6eAd1	průměrně
zkonzumujeme	zkonzumovat	k5eAaPmIp1nP	zkonzumovat
10	[number]	k4	10
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
cihlově	cihlově	k6eAd1	cihlově
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
svalového	svalový	k2eAgNnSc2d1	svalové
barviva	barvivo	k1gNnSc2	barvivo
a	a	k8xC	a
výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
masa	maso	k1gNnSc2	maso
však	však	k9	však
dále	daleko	k6eAd2	daleko
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
věku	věk	k1gInSc6	věk
poraženého	poražený	k2eAgNnSc2d1	poražené
zvířete	zvíře	k1gNnSc2	zvíře
–	–	k?	–
u	u	k7c2	u
mladých	mladý	k2eAgInPc2d1	mladý
kusů	kus	k1gInPc2	kus
je	být	k5eAaImIp3nS	být
bledě	bledě	k6eAd1	bledě
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
u	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
býků	býk	k1gMnPc2	býk
tmavě	tmavě	k6eAd1	tmavě
červená	červený	k2eAgFnSc1d1	červená
</s>
</p>
<p>
<s>
–	–	k?	–
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc4	jeho
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
,	,	kIx,	,
živé	živá	k1gFnPc4	živá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
plemenné	plemenný	k2eAgFnSc2d1	plemenná
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
,	,	kIx,	,
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
zatížení	zatížení	k1gNnSc3	zatížení
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
svalu	sval	k1gInSc2	sval
</s>
</p>
<p>
<s>
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
a	a	k8xC	a
kvalitě	kvalita	k1gFnSc6	kvalita
výživy	výživa	k1gFnSc2	výživa
jatečného	jatečný	k2eAgNnSc2d1	jatečné
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Odstín	odstín	k1gInSc1	odstín
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
také	také	k9	také
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jaké	jaký	k3yRgFnSc2	jaký
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
maso	maso	k1gNnSc1	maso
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Výraznější	výrazný	k2eAgNnSc1d2	výraznější
zbarvení	zbarvení	k1gNnSc1	zbarvení
masa	maso	k1gNnSc2	maso
podporuje	podporovat	k5eAaImIp3nS	podporovat
pohyb	pohyb	k1gInSc1	pohyb
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
krmení	krmení	k1gNnSc2	krmení
zelenou	zelená	k1gFnSc7	zelená
pící	píce	k1gFnSc7	píce
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
krmné	krmný	k2eAgFnSc6d1	krmná
dávce	dávka	k1gFnSc6	dávka
<g/>
,	,	kIx,	,
kastrace	kastrace	k1gFnSc1	kastrace
býků	býk	k1gMnPc2	býk
a	a	k8xC	a
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
výkrm	výkrm	k1gInSc1	výkrm
naopak	naopak	k6eAd1	naopak
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
světlejšímu	světlý	k2eAgInSc3d2	světlejší
odstínu	odstín	k1gInSc3	odstín
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
technologického	technologický	k2eAgNnSc2d1	Technologické
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
jatečně	jatečně	k6eAd1	jatečně
opracovaného	opracovaný	k2eAgInSc2d1	opracovaný
trupu	trup	k1gInSc2	trup
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
masa	maso	k1gNnSc2	maso
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
především	především	k6eAd1	především
pH	ph	kA	ph
–	–	k?	–
při	při	k7c6	při
pH	ph	kA	ph
nižším	nízký	k2eAgInSc6d2	nižší
než	než	k8xS	než
5,6	[number]	k4	5,6
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc4	maso
světlejší	světlý	k2eAgFnSc2d2	světlejší
<g/>
,	,	kIx,	,
při	při	k7c6	při
pH	ph	kA	ph
vyšším	vysoký	k2eAgInSc6d2	vyšší
než	než	k8xS	než
5,8	[number]	k4	5,8
naopak	naopak	k6eAd1	naopak
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Uvařením	uvaření	k1gNnSc7	uvaření
maso	maso	k1gNnSc1	maso
nabývá	nabývat	k5eAaImIp3nS	nabývat
šedohnědé	šedohnědý	k2eAgFnPc4d1	šedohnědá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
chladírenském	chladírenský	k2eAgNnSc6d1	chladírenské
skladování	skladování	k1gNnSc6	skladování
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
optimálně	optimálně	k6eAd1	optimálně
vyzraje	vyzrát	k5eAaPmIp3nS	vyzrát
za	za	k7c4	za
10	[number]	k4	10
až	až	k9	až
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typická	typický	k2eAgFnSc1d1	typická
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
pach	pach	k1gInSc1	pach
masa	maso	k1gNnSc2	maso
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
přítomností	přítomnost	k1gFnSc7	přítomnost
těkavých	těkavý	k2eAgFnPc2d1	těkavá
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
výrazná	výrazný	k2eAgFnSc1d1	výrazná
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
chuťovými	chuťový	k2eAgFnPc7d1	chuťová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
masa	maso	k1gNnSc2	maso
blízce	blízce	k6eAd1	blízce
souvisí	souviset	k5eAaImIp3nS	souviset
šťavnatost	šťavnatost	k1gFnSc4	šťavnatost
a	a	k8xC	a
křehkost	křehkost	k1gFnSc4	křehkost
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
šťavnatost	šťavnatost	k1gFnSc4	šťavnatost
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
schopnost	schopnost	k1gFnSc4	schopnost
masa	maso	k1gNnSc2	maso
udržet	udržet	k5eAaPmF	udržet
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
údržnost	údržnost	k1gFnSc4	údržnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vaznost	vaznost	k1gFnSc1	vaznost
a	a	k8xC	a
stupeň	stupeň	k1gInSc1	stupeň
mramorování	mramorování	k1gNnSc2	mramorování
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
křehkost	křehkost	k1gFnSc1	křehkost
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
svalových	svalový	k2eAgFnPc2d1	svalová
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
aktinu	aktina	k1gFnSc4	aktina
a	a	k8xC	a
myosinu	myosina	k1gFnSc4	myosina
<g/>
)	)	kIx)	)
a	a	k8xC	a
mramorováním	mramorování	k1gNnSc7	mramorování
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
intramuskulárního	intramuskulární	k2eAgInSc2d1	intramuskulární
tuku	tuk	k1gInSc2	tuk
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
kolagenu	kolagen	k1gInSc2	kolagen
a	a	k8xC	a
elastinu	elastin	k1gInSc2	elastin
ve	v	k7c6	v
vazivové	vazivový	k2eAgFnSc6d1	vazivová
tkáni	tkáň	k1gFnSc6	tkáň
mezi	mezi	k7c7	mezi
svalovými	svalový	k2eAgNnPc7d1	svalové
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
od	od	k7c2	od
dobře	dobře	k6eAd1	dobře
vykrmených	vykrmený	k2eAgNnPc2d1	vykrmené
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
křehčí	křehký	k2eAgNnSc1d2	křehčí
než	než	k8xS	než
maso	maso	k1gNnSc1	maso
od	od	k7c2	od
zvířat	zvíře	k1gNnPc2	zvíře
hubenějších	hubený	k2eAgNnPc2d2	hubenější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Složení	složení	k1gNnSc1	složení
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
===	===	k?	===
</s>
</p>
<p>
<s>
Hovězím	hovězí	k2eAgNnSc7d1	hovězí
masem	maso	k1gNnSc7	maso
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
příčně	příčně	k6eAd1	příčně
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
,	,	kIx,	,
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiléhající	přiléhající	k2eAgFnSc1d1	přiléhající
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
tuková	tukový	k2eAgFnSc1d1	tuková
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
pojivo	pojivo	k1gNnSc1	pojivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
nervové	nervový	k2eAgFnSc2d1	nervová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
krevních	krevní	k2eAgFnPc2d1	krevní
a	a	k8xC	a
mízních	mízní	k2eAgFnPc2d1	mízní
cév	céva	k1gFnPc2	céva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
a	a	k8xC	a
nejhodnotnější	hodnotný	k2eAgFnSc7d3	nejhodnotnější
částí	část	k1gFnSc7	část
masa	maso	k1gNnSc2	maso
je	být	k5eAaImIp3nS	být
svalová	svalový	k2eAgFnSc1d1	svalová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jakost	jakost	k1gFnSc1	jakost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
jemností	jemnost	k1gFnSc7	jemnost
svalových	svalový	k2eAgFnPc2d1	svalová
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
typem	typ	k1gInSc7	typ
<g/>
,	,	kIx,	,
podílem	podíl	k1gInSc7	podíl
svalové	svalový	k2eAgFnSc2d1	svalová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
<g/>
Mladé	mladý	k2eAgInPc1d1	mladý
kusy	kus	k1gInPc1	kus
mají	mít	k5eAaImIp3nP	mít
jemně	jemně	k6eAd1	jemně
vláknité	vláknitý	k2eAgNnSc4d1	vláknité
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
svalovina	svalovina	k1gFnSc1	svalovina
starších	starý	k2eAgMnPc2d2	starší
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
hrubě	hrubě	k6eAd1	hrubě
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
a	a	k8xC	a
dojnice	dojnice	k1gFnPc1	dojnice
mají	mít	k5eAaImIp3nP	mít
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
intramuskulárního	intramuskulární	k2eAgInSc2d1	intramuskulární
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
maso	maso	k1gNnSc1	maso
bývá	bývat	k5eAaImIp3nS	bývat
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
a	a	k8xC	a
méně	málo	k6eAd2	málo
chutné	chutný	k2eAgNnSc1d1	chutné
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jalovic	jalovice	k1gFnPc2	jalovice
a	a	k8xC	a
volů	vůl	k1gMnPc2	vůl
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
brzkému	brzký	k2eAgNnSc3d1	brzké
ukládání	ukládání	k1gNnSc3	ukládání
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
maso	maso	k1gNnSc4	maso
výrazně	výrazně	k6eAd1	výrazně
prorůstá	prorůstat	k5eAaImIp3nS	prorůstat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
řezu	řez	k1gInSc6	řez
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
mramorování	mramorování	k1gNnSc4	mramorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnitrosvalový	Vnitrosvalový	k2eAgInSc1d1	Vnitrosvalový
tuk	tuk	k1gInSc1	tuk
zřetelně	zřetelně	k6eAd1	zřetelně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
chuťové	chuťový	k2eAgFnPc4d1	chuťová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
nashromáždí	nashromáždit	k5eAaPmIp3nS	nashromáždit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
však	však	k9	však
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
mezisvalového	mezisvalový	k2eAgInSc2d1	mezisvalový
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xC	jako
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
jatečně	jatečně	k6eAd1	jatečně
opracovaných	opracovaný	k2eAgFnPc6d1	opracovaná
půlkách	půlka	k1gFnPc6	půlka
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
neměl	mít	k5eNaImAgInS	mít
přesahovat	přesahovat	k5eAaImF	přesahovat
5,7	[number]	k4	5,7
<g/>
–	–	k?	–
<g/>
6,2	[number]	k4	6,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
libové	libový	k2eAgNnSc1d1	libové
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
do	do	k7c2	do
3	[number]	k4	3
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
biologicky	biologicky	k6eAd1	biologicky
velmi	velmi	k6eAd1	velmi
hodnotné	hodnotný	k2eAgNnSc1d1	hodnotné
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
esenciálních	esenciální	k2eAgFnPc2d1	esenciální
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nutriční	nutriční	k2eAgInSc4d1	nutriční
profil	profil	k1gInSc4	profil
kulinářsky	kulinářsky	k6eAd1	kulinářsky
a	a	k8xC	a
technologicky	technologicky	k6eAd1	technologicky
zpracovaného	zpracovaný	k2eAgNnSc2d1	zpracované
masa	maso	k1gNnSc2	maso
–	–	k?	–
dušená	dušený	k2eAgFnSc1d1	dušená
hovězí	hovězí	k2eAgFnSc1d1	hovězí
kýta	kýta	k1gFnSc1	kýta
<g/>
,	,	kIx,	,
plátek	plátek	k1gInSc1	plátek
85	[number]	k4	85
g	g	kA	g
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozporcování	rozporcování	k1gNnSc6	rozporcování
hovězí	hovězí	k2eAgFnSc2d1	hovězí
půlky	půlka	k1gFnSc2	půlka
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
různé	různý	k2eAgInPc1d1	různý
díly	díl	k1gInPc1	díl
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Špička	špička	k1gFnSc1	špička
krku	krk	k1gInSc2	krk
a	a	k8xC	a
krk	krk	k1gInSc4	krk
</s>
</p>
<p>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
žebro	žebro	k1gNnSc1	žebro
</s>
</p>
<p>
<s>
Hrudí	hrudí	k1gNnSc1	hrudí
</s>
</p>
<p>
<s>
Podplečí	podplečí	k1gNnSc1	podplečí
</s>
</p>
<p>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
roštěnec	roštěnec	k1gInSc1	roštěnec
</s>
</p>
<p>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
roštěnec	roštěnec	k1gInSc1	roštěnec
</s>
</p>
<p>
<s>
Svíčková	svíčková	k1gFnSc1	svíčková
</s>
</p>
<p>
<s>
Pupeční	pupeční	k2eAgNnSc1d1	pupeční
žebro	žebro	k1gNnSc1	žebro
</s>
</p>
<p>
<s>
Bok	bok	k1gInSc1	bok
bez	bez	k7c2	bez
kostí	kost	k1gFnPc2	kost
</s>
</p>
<p>
<s>
Plec	plec	k1gFnSc1	plec
</s>
</p>
<p>
<s>
Kýta	kýta	k1gFnSc1	kýta
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgInSc1d1	vrchní
šál	šál	k1gInSc1	šál
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInSc1d1	spodní
šál	šál	k1gInSc1	šál
a	a	k8xC	a
ořech	ořech	k1gInSc1	ořech
</s>
</p>
<p>
<s>
Předkýtí	Předkýtit	k5eAaPmIp3nS	Předkýtit
</s>
</p>
<p>
<s>
Špička	špička	k1gFnSc1	špička
kýty	kýta	k1gFnSc2	kýta
</s>
</p>
<p>
<s>
KližkaKýta	KližkaKýta	k1gFnSc1	KližkaKýta
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
vrchní	vrchní	k2eAgInSc4d1	vrchní
šál	šál	k1gInSc4	šál
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInSc4d1	spodní
šál	šál	k1gInSc4	šál
s	s	k7c7	s
karabáčkem	karabáček	k1gInSc7	karabáček
<g/>
,	,	kIx,	,
předkýtí	předkýť	k1gFnSc7	předkýť
a	a	k8xC	a
květovou	květový	k2eAgFnSc4d1	květová
špičku	špička	k1gFnSc4	špička
</s>
</p>
<p>
<s>
Svíčková	svíčková	k1gFnSc1	svíčková
je	být	k5eAaImIp3nS	být
podélný	podélný	k2eAgInSc4d1	podélný
sval	sval	k1gInSc4	sval
uložený	uložený	k2eAgInSc4d1	uložený
pod	pod	k7c7	pod
bederními	bederní	k2eAgInPc7d1	bederní
obratli	obratel	k1gInPc7	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejjemnější	jemný	k2eAgNnSc1d3	nejjemnější
a	a	k8xC	a
nejkřehčí	křehký	k2eAgNnSc1d3	nejkřehčí
maso	maso	k1gNnSc1	maso
</s>
</p>
<p>
<s>
Roštěnec	roštěnec	k1gInSc1	roštěnec
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
a	a	k8xC	a
nízký	nízký	k2eAgInSc4d1	nízký
</s>
</p>
<p>
<s>
Hrudí	hrudí	k1gNnSc1	hrudí
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
protučnělé	protučnělý	k2eAgNnSc1d1	protučnělý
maso	maso	k1gNnSc1	maso
</s>
</p>
<p>
<s>
Žebro	žebro	k1gNnSc1	žebro
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnPc4d1	nízká
a	a	k8xC	a
pupeční	pupeční	k2eAgFnPc4d1	pupeční
</s>
</p>
<p>
<s>
Plec	plec	k1gFnSc1	plec
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
kulatou	kulatý	k2eAgFnSc4d1	kulatá
plec	plec	k1gFnSc4	plec
(	(	kIx(	(
<g/>
nazvývanou	nazvývaný	k2eAgFnSc4d1	nazvývaný
také	také	k9	také
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
falešná	falešný	k2eAgFnSc1d1	falešná
svíčková	svíčková	k1gFnSc1	svíčková
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plochou	plochý	k2eAgFnSc4d1	plochá
plec	plec	k1gFnSc4	plec
a	a	k8xC	a
loupanou	loupaný	k2eAgFnSc4d1	loupaná
plec	plec	k1gFnSc4	plec
</s>
</p>
<p>
<s>
Podplečí	podplečí	k1gNnSc1	podplečí
a	a	k8xC	a
krk	krk	k1gInSc1	krk
má	mít	k5eAaImIp3nS	mít
maso	maso	k1gNnSc4	maso
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
málo	málo	k1gNnSc1	málo
porostlé	porostlý	k2eAgNnSc1d1	porostlé
tukem	tuk	k1gInSc7	tuk
</s>
</p>
<p>
<s>
Kližka	kližka	k1gFnSc1	kližka
<g/>
,	,	kIx,	,
svaly	sval	k1gInPc1	sval
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
prorostlé	prorostlý	k2eAgFnPc1d1	prorostlá
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
producenty	producent	k1gMnPc7	producent
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
producenty	producent	k1gMnPc7	producent
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
masná	masný	k2eAgNnPc1d1	masné
plemena	plemeno	k1gNnPc1	plemeno
nebo	nebo	k8xC	nebo
býci	býk	k1gMnPc1	býk
a	a	k8xC	a
jalovičky	jalovička	k1gFnPc1	jalovička
mléčných	mléčný	k2eAgNnPc2d1	mléčné
a	a	k8xC	a
kombinovaných	kombinovaný	k2eAgNnPc2d1	kombinované
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
kvalitu	kvalita	k1gFnSc4	kvalita
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgInPc4d2	nižší
přírůstky	přírůstek	k1gInPc4	přírůstek
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spotřebu	spotřeba	k1gFnSc4	spotřeba
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
telecího	telecí	k1gNnSc2	telecí
<g/>
)	)	kIx)	)
v	v	k7c4	v
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
živé	živý	k2eAgFnSc2d1	živá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
telecího	telecí	k1gNnSc2	telecí
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
v	v	k7c6	v
tunách	tuna	k1gFnPc6	tuna
jatečné	jatečný	k2eAgFnSc2d1	jatečná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Spotřeba	spotřeba	k1gFnSc1	spotřeba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
hovězího	hovězí	k1gNnSc2	hovězí
masa	maso	k1gNnSc2	maso
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
na	na	k7c6	na
kosti	kost	k1gFnSc6	kost
(	(	kIx(	(
<g/>
v	v	k7c6	v
kg	kg	kA	kg
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
mnoho	mnoho	k4c1	mnoho
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnSc3d1	britská
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc3d1	francouzská
a	a	k8xC	a
české	český	k2eAgFnSc3d1	Česká
kuchyni	kuchyně	k1gFnSc3	kuchyně
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
Středomoří	středomoří	k1gNnSc2	středomoří
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
úpravou	úprava	k1gFnSc7	úprava
je	být	k5eAaImIp3nS	být
pečení	pečení	k1gNnSc1	pečení
vcelku	vcelku	k6eAd1	vcelku
<g/>
:	:	kIx,	:
roast	roast	k5eAaPmF	roast
beef	beef	k1gInSc4	beef
nebo	nebo	k8xC	nebo
po	po	k7c4	po
nakrájení	nakrájení	k1gNnSc4	nakrájení
na	na	k7c4	na
plátky	plátek	k1gInPc4	plátek
biftek	biftek	k1gInSc4	biftek
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgInSc4d1	hovězí
steak	steak	k1gInSc4	steak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kuchyni	kuchyně	k1gFnSc6	kuchyně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k6eAd1	především
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
polévky	polévka	k1gFnSc2	polévka
a	a	k8xC	a
guláše	guláš	k1gInSc2	guláš
<g/>
,	,	kIx,	,
z	z	k7c2	z
nejkvalitnějšího	kvalitní	k2eAgNnSc2d3	nejkvalitnější
masa	maso	k1gNnSc2	maso
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
svíčková	svíčková	k1gFnSc1	svíčková
na	na	k7c6	na
smetaně	smetana	k1gFnSc6	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
jako	jako	k9	jako
mleté	mletý	k2eAgNnSc1d1	mleté
do	do	k7c2	do
hamburgerů	hamburger	k1gInPc2	hamburger
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
chili	chile	k1gFnSc4	chile
con	con	k?	con
carne	carnout	k5eAaPmIp3nS	carnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženské	náboženský	k2eAgInPc1d1	náboženský
předpisy	předpis	k1gInPc1	předpis
==	==	k?	==
</s>
</p>
<p>
<s>
Nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
hovězím	hovězí	k2eAgInSc7d1	hovězí
dobytkem	dobytek	k1gInSc7	dobytek
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
obchodně	obchodně	k6eAd1	obchodně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
statusu	status	k1gInSc2	status
u	u	k7c2	u
náboženské	náboženský	k2eAgFnSc2d1	náboženská
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hinduisty	hinduista	k1gMnPc4	hinduista
je	být	k5eAaImIp3nS	být
kráva	kráva	k1gFnSc1	kráva
posvátná	posvátný	k2eAgFnSc1d1	posvátná
a	a	k8xC	a
chovají	chovat	k5eAaImIp3nP	chovat
je	on	k3xPp3gNnSc4	on
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
péčí	péče	k1gFnSc7	péče
<g/>
.	.	kIx.	.
</s>
<s>
Jatky	jatka	k1gFnPc1	jatka
tohoto	tento	k3xDgNnSc2	tento
zvířectva	zvířectvo	k1gNnSc2	zvířectvo
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
přidělena	přidělit	k5eAaPmNgFnS	přidělit
muslimské	muslimský	k2eAgFnSc3d1	muslimská
části	část	k1gFnSc3	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
zpracováním	zpracování	k1gNnSc7	zpracování
problém	problém	k1gInSc4	problém
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
upravující	upravující	k2eAgFnSc2d1	upravující
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc4	pravidlo
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
bedrech	bedra	k1gNnPc6	bedra
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
je	být	k5eAaImIp3nS	být
zabití	zabití	k1gNnSc4	zabití
či	či	k8xC	či
obchodování	obchodování	k1gNnSc4	obchodování
s	s	k7c7	s
hovězím	hovězí	k1gNnSc7	hovězí
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
a	a	k8xC	a
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
těchto	tento	k3xDgFnPc2	tento
úprav	úprava	k1gFnPc2	úprava
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
lobuje	lobovat	k5eAaBmIp3nS	lobovat
nejedna	nejeden	k4xCyIgFnSc1	nejeden
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Produkce	produkce	k1gFnSc1	produkce
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
produkuje	produkovat	k5eAaImIp3nS	produkovat
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
než	než	k8xS	než
7	[number]	k4	7
<g/>
krát	krát	k6eAd1	krát
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
methan	methan	k1gInSc1	methan
<g/>
)	)	kIx)	)
než	než	k8xS	než
produkce	produkce	k1gFnSc1	produkce
drůbežího	drůbeží	k2eAgNnSc2d1	drůbeží
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hovězí	hovězí	k2eAgFnSc2d1	hovězí
maso	maso	k1gNnSc4	maso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://dobrutky.unas.cz/masoobsah.htm	[url]	k6eAd1	http://dobrutky.unas.cz/masoobsah.htm
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
hovězích	hovězí	k2eAgInPc2d1	hovězí
steaků	steak	k1gInPc2	steak
</s>
</p>
