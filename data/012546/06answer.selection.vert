<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
a	a	k8xC	a
dojnice	dojnice	k1gFnPc1	dojnice
mají	mít	k5eAaImIp3nP	mít
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
intramuskulárního	intramuskulární	k2eAgInSc2d1	intramuskulární
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
maso	maso	k1gNnSc1	maso
bývá	bývat	k5eAaImIp3nS	bývat
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
a	a	k8xC	a
méně	málo	k6eAd2	málo
chutné	chutný	k2eAgNnSc1d1	chutné
<g/>
.	.	kIx.	.
</s>
