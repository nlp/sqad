<s>
"	"	kIx"	"
<g/>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rile	k2eAgFnPc4d1	Rile
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
britské	britský	k2eAgFnSc2d1	britská
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
složená	složený	k2eAgFnSc1d1	složená
kytaristou	kytarista	k1gMnSc7	kytarista
Petem	Pet	k1gMnSc7	Pet
Townshendem	Townshend	k1gMnSc7	Townshend
z	z	k7c2	z
alba	album	k1gNnSc2	album
Who	Who	k1gFnSc2	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Nexta	k1gFnPc2	Nexta
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gInSc1	Roger
Daltrey	Daltrea	k1gFnSc2	Daltrea
zpívá	zpívat	k5eAaImIp3nS	zpívat
většinu	většina	k1gFnSc4	většina
skladby	skladba	k1gFnSc2	skladba
a	a	k8xC	a
Townshend	Townshend	k1gInSc1	Townshend
zpívá	zpívat	k5eAaImIp3nS	zpívat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
verše	verš	k1gInPc4	verš
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
cry	cry	k?	cry
<g/>
/	/	kIx~	/
<g/>
don	don	k1gMnSc1	don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
raise	raise	k1gFnSc2	raise
your	your	k1gMnSc1	your
eye	eye	k?	eye
<g/>
/	/	kIx~	/
<g/>
it	it	k?	it
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
only	onl	k1gMnPc7	onl
teenage	teenage	k1gFnSc4	teenage
wasteland	wastelanda	k1gFnPc2	wastelanda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
skladby	skladba	k1gFnSc2	skladba
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jmen	jméno	k1gNnPc2	jméno
filozofických	filozofický	k2eAgFnPc2d1	filozofická
a	a	k8xC	a
hudebních	hudební	k2eAgFnPc2d1	hudební
osobností	osobnost	k1gFnPc2	osobnost
Mehera	Mehero	k1gNnSc2	Mehero
Baby	baba	k1gFnSc2	baba
a	a	k8xC	a
Terry	Terra	k1gMnSc2	Terra
Rileyho	Riley	k1gMnSc2	Riley
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
inovativní	inovativní	k2eAgFnSc4d1	inovativní
fúzi	fúze	k1gFnSc4	fúze
hard	hard	k6eAd1	hard
rockového	rockový	k2eAgInSc2d1	rockový
zvuku	zvuk	k1gInSc2	zvuk
The	The	k1gFnSc2	The
Who	Who	k1gFnPc2	Who
a	a	k8xC	a
Townshendovu	Townshendův	k2eAgNnSc3d1	Townshendův
elektronickému	elektronický	k2eAgNnSc3d1	elektronické
hudebnímu	hudební	k2eAgNnSc3d1	hudební
experimentování	experimentování	k1gNnSc3	experimentování
<g/>
,	,	kIx,	,
a	a	k8xC	a
riff	riff	k1gInSc1	riff
s	s	k7c7	s
opakováním	opakování	k1gNnSc7	opakování
akordů	akord	k1gInPc2	akord
F-C-	F-C-	k1gFnSc2	F-C-
<g/>
B.	B.	kA	B.
Pete	Pete	k1gFnSc1	Pete
Townshend	Townshend	k1gMnSc1	Townshend
původně	původně	k6eAd1	původně
napsal	napsat	k5eAaPmAgMnS	napsat
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gFnSc2	Rilea
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
projekt	projekt	k1gInSc4	projekt
Lifehouse	Lifehouse	k1gFnSc2	Lifehouse
<g/>
,	,	kIx,	,
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
následovat	následovat	k5eAaImF	následovat
operu	opera	k1gFnSc4	opera
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
-	-	kIx~	-
Tommy	Tomma	k1gFnSc2	Tomma
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
skladbu	skladba	k1gFnSc4	skladba
odvodil	odvodit	k5eAaPmAgMnS	odvodit
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
nahrávání	nahrávání	k1gNnSc2	nahrávání
varhan	varhany	k1gFnPc2	varhany
Lowrey	Lowrea	k1gFnSc2	Lowrea
Berkshire	Berkshir	k1gInSc5	Berkshir
TBO-	TBO-	k1gFnPc7	TBO-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gFnSc2	Rilea
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nP	by
zpíval	zpívat	k5eAaImAgMnS	zpívat
Ray	Ray	k1gMnSc1	Ray
<g/>
,	,	kIx,	,
skotský	skotský	k2eAgMnSc1d1	skotský
farmář	farmář	k1gMnSc1	farmář
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
alba	album	k1gNnSc2	album
jak	jak	k8xS	jak
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
Sally	Salla	k1gFnSc2	Salla
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
začali	začít	k5eAaPmAgMnP	začít
jejich	jejich	k3xOp3gInSc4	jejich
exodus	exodus	k1gInSc4	exodus
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Lifehouse	Lifehouse	k1gFnPc4	Lifehouse
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
většina	většina	k1gFnSc1	většina
songů	song	k1gInPc2	song
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
albu	album	k1gNnSc6	album
Who	Who	k1gFnSc2	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Nexta	k1gFnPc2	Nexta
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gFnSc2	Rilea
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
písní	píseň	k1gFnSc7	píseň
na	na	k7c6	na
Who	Who	k1gFnSc6	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Nexta	k1gFnPc2	Nexta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
byla	být	k5eAaImAgNnP	být
vydána	vydat	k5eAaPmNgNnP	vydat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Keith	Keith	k1gMnSc1	Keith
Moon	Moon	k1gMnSc1	Moon
dostal	dostat	k5eAaPmAgMnS	dostat
nápad	nápad	k1gInSc4	nápad
přidání	přidání	k1gNnSc2	přidání
houslí	housle	k1gFnPc2	housle
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
části	část	k1gFnSc6	část
skladby	skladba	k1gFnSc2	skladba
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
změní	změnit	k5eAaPmIp3nS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc4	styl
z	z	k7c2	z
rocku	rock	k1gInSc2	rock
do	do	k7c2	do
irského	irský	k2eAgInSc2d1	irský
folk-style	folktyl	k1gInSc5	folk-styl
beatu	beat	k1gInSc2	beat
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Arbus	Arbus	k1gMnSc1	Arbus
(	(	kIx(	(
<g/>
East	East	k1gMnSc1	East
od	od	k7c2	od
Eden	Eden	k1gInSc1	Eden
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
studiové	studiový	k2eAgFnSc6d1	studiová
nahrávce	nahrávka	k1gFnSc6	nahrávka
a	a	k8xC	a
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
je	on	k3xPp3gNnSc4	on
nahradili	nahradit	k5eAaPmAgMnP	nahradit
Rogerem	Roger	k1gInSc7	Roger
Daltreym	Daltreymum	k1gNnPc2	Daltreymum
a	a	k8xC	a
foukací	foukací	k2eAgFnSc7d1	foukací
harmonikou	harmonika	k1gFnSc7	harmonika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
hrál	hrát	k5eAaImAgMnS	hrát
housle	housle	k1gFnPc4	housle
Nigel	Niglo	k1gNnPc2	Niglo
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
Houslové	houslový	k2eAgNnSc1d1	houslové
sólo	sólo	k1gNnSc1	sólo
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
indické	indický	k2eAgFnSc6d1	indická
klasické	klasický	k2eAgFnSc6d1	klasická
hudbě	hudba	k1gFnSc6	hudba
jako	jako	k8xC	jako
pocta	pocta	k1gFnSc1	pocta
Meheru	Meher	k1gMnSc3	Meher
Babovi	Baba	k1gMnSc3	Baba
<g/>
,	,	kIx,	,
indickému	indický	k2eAgMnSc3d1	indický
mystikovi	mystik	k1gMnSc3	mystik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
stopa	stopa	k1gFnSc1	stopa
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
hloubi	hloub	k1gFnSc2	hloub
konceptu	koncept	k1gInSc2	koncept
Lifehouse	Lifehouse	k1gFnSc2	Lifehouse
<g/>
.	.	kIx.	.
</s>
<s>
Towhshend	Towhshend	k1gMnSc1	Towhshend
chtěl	chtít	k5eAaImAgMnS	chtít
vzít	vzít	k5eAaPmF	vzít
životní	životní	k2eAgFnSc4d1	životní
informaci	informace	k1gFnSc4	informace
Mehera	Mehero	k1gNnSc2	Mehero
Baby	baba	k1gFnSc2	baba
a	a	k8xC	a
vložit	vložit	k5eAaPmF	vložit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
syntetizátoru	syntetizátor	k1gInSc2	syntetizátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
generoval	generovat	k5eAaImAgInS	generovat
hudbu	hudba	k1gFnSc4	hudba
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
informaci	informace	k1gFnSc6	informace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hudba	hudba	k1gFnSc1	hudba
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
backing	backing	k1gInSc4	backing
track	tracka	k1gFnPc2	tracka
pro	pro	k7c4	pro
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rile	k1gMnPc4	Rile
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
Towhshend	Towhshend	k1gInSc4	Towhshend
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
backing	backing	k1gInSc1	backing
tracku	tracko	k1gNnSc3	tracko
použil	použít	k5eAaPmAgInS	použít
varhany	varhany	k1gInPc4	varhany
Lowrey	Lowrea	k1gFnSc2	Lowrea
Bekshire	Bekshir	k1gInSc5	Bekshir
TBO-1	TBO-1	k1gMnPc4	TBO-1
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
funkci	funkce	k1gFnSc4	funkce
"	"	kIx"	"
<g/>
Marimba	Marimba	k1gFnSc1	Marimba
Repeat	Repeat	k1gInSc1	Repeat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gFnSc2	Rilea
použita	použit	k2eAgFnSc1d1	použita
jako	jako	k8xS	jako
úvodní	úvodní	k2eAgFnSc1d1	úvodní
znělka	znělka	k1gFnSc1	znělka
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
chybě	chyba	k1gFnSc3	chyba
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
Teenage	Teenage	k1gFnSc1	Teenage
Wasteland	Wastelanda	k1gFnPc2	Wastelanda
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
fráze	fráze	k1gFnSc2	fráze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Teenage	Teenage	k6eAd1	Teenage
Wasteland	Wasteland	k1gInSc1	Wasteland
byl	být	k5eAaImAgInS	být
vlastně	vlastně	k9	vlastně
pracovní	pracovní	k2eAgInSc1d1	pracovní
název	název	k1gInSc1	název
pro	pro	k7c4	pro
skladbu	skladba	k1gFnSc4	skladba
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
prvních	první	k4xOgFnPc6	první
inkarnacích	inkarnace	k1gFnPc6	inkarnace
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
název	název	k1gInSc4	název
Teenage	Teenag	k1gInSc2	Teenag
Wasteland	Wasteland	k1gInSc1	Wasteland
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
úplně	úplně	k6eAd1	úplně
jiné	jiné	k1gNnSc4	jiné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
související	související	k2eAgFnSc6d1	související
skladbě	skladba	k1gFnSc6	skladba
od	od	k7c2	od
Townshenda	Townshend	k1gMnSc2	Townshend
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
textu	text	k1gInSc2	text
a	a	k8xC	a
akordů	akord	k1gInPc2	akord
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
"	"	kIx"	"
<g/>
Teenage	Teenage	k1gInSc1	Teenage
Wasteland	Wasteland	k1gInSc1	Wasteland
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c4	na
The	The	k1gFnSc4	The
Lifehouse	Lifehouse	k1gFnSc1	Lifehouse
Chronicles	Chronicles	k1gMnSc1	Chronicles
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
šesti	šest	k4xCc2	šest
disků	disk	k1gInPc2	disk
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
Lifehouse	Lifehouse	k1gFnSc2	Lifehouse
<g/>
.	.	kIx.	.
</s>
<s>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gFnPc4	Rilea
náleží	náležet	k5eAaImIp3nS	náležet
na	na	k7c4	na
340	[number]	k4	340
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
500	[number]	k4	500
Greatest	Greatest	k1gInSc1	Greatest
Songs	Songs	k1gInSc4	Songs
of	of	k?	of
All	All	k1gMnSc2	All
Time	Tim	k1gMnSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c4	v
Rock	rock	k1gInSc4	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
and	and	k?	and
rollová	rollová	k1gFnSc1	rollová
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
500	[number]	k4	500
Songs	Songsa	k1gFnPc2	Songsa
That	That	k2eAgMnSc1d1	That
Shaped	Shaped	k1gMnSc1	Shaped
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
(	(	kIx(	(
<g/>
500	[number]	k4	500
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
formovaly	formovat	k5eAaImAgFnP	formovat
Rock	rock	k1gInSc4	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Baba	baba	k1gFnSc1	baba
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Riley	Rilea	k1gMnSc2	Rilea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
