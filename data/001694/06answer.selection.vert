<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Mistral	mistral	k1gInSc1	mistral
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
Maillaine	Maillain	k1gMnSc5	Maillain
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Maillaine	Maillain	k1gMnSc5	Maillain
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lyrický	lyrický	k2eAgMnSc1d1	lyrický
a	a	k8xC	a
epický	epický	k2eAgMnSc1d1	epický
básník	básník	k1gMnSc1	básník
z	z	k7c2	z
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
obrození	obrození	k1gNnSc4	obrození
provensálského	provensálský	k2eAgInSc2d1	provensálský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
okcitánštiny	okcitánština	k1gFnSc2	okcitánština
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
společně	společně	k6eAd1	společně
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
dramatikem	dramatik	k1gMnSc7	dramatik
Josém	José	k1gNnSc6	José
Echegaraym	Echegaraymum	k1gNnPc2	Echegaraymum
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
<g/>
.	.	kIx.	.
</s>
