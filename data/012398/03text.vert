<p>
<s>
Sýc	sýc	k1gMnSc1	sýc
rousný	rousný	k2eAgMnSc1d1	rousný
(	(	kIx(	(
<g/>
Aegolius	Aegolius	k1gMnSc1	Aegolius
funereus	funereus	k1gMnSc1	funereus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
druh	druh	k1gInSc1	druh
sovy	sova	k1gFnSc2	sova
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
puštíkovitých	puštíkovitý	k2eAgFnPc2d1	puštíkovitý
(	(	kIx(	(
<g/>
Strigidae	Strigidae	k1gFnPc2	Strigidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
sýčka	sýček	k1gMnSc2	sýček
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
g	g	kA	g
<g/>
,	,	kIx,	,
samic	samice	k1gFnPc2	samice
mezi	mezi	k7c7	mezi
123	[number]	k4	123
<g/>
–	–	k?	–
<g/>
210	[number]	k4	210
g.	g.	k?	g.
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
skvrněním	skvrnění	k1gNnSc7	skvrnění
a	a	k8xC	a
světlým	světlý	k2eAgInSc7d1	světlý
pruhem	pruh	k1gInSc7	pruh
při	při	k7c6	při
kořeni	kořen	k1gInSc6	kořen
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
spodina	spodina	k1gFnSc1	spodina
je	být	k5eAaImIp3nS	být
bělavá	bělavý	k2eAgFnSc1d1	bělavá
<g/>
,	,	kIx,	,
hnědě	hnědě	k6eAd1	hnědě
pruhovaná	pruhovaný	k2eAgFnSc1d1	pruhovaná
<g/>
.	.	kIx.	.
</s>
<s>
Závoj	závoj	k1gInSc1	závoj
je	být	k5eAaImIp3nS	být
světlý	světlý	k2eAgInSc1d1	světlý
<g/>
,	,	kIx,	,
černě	černě	k6eAd1	černě
ohraničený	ohraničený	k2eAgMnSc1d1	ohraničený
<g/>
,	,	kIx,	,
duhovka	duhovka	k1gFnSc1	duhovka
jasně	jasně	k6eAd1	jasně
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
zobák	zobák	k1gInSc1	zobák
žlutý	žlutý	k2eAgInSc1d1	žlutý
a	a	k8xC	a
peří	peřit	k5eAaImIp3nS	peřit
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
čokoládově	čokoládově	k6eAd1	čokoládově
hnědí	hnědit	k5eAaImIp3nS	hnědit
<g/>
.	.	kIx.	.
<g/>
Hlasem	hlasem	k6eAd1	hlasem
je	být	k5eAaImIp3nS	být
dudkovi	dudek	k1gMnSc3	dudek
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
pupupupupu	pupupupupat	k5eAaPmIp1nS	pupupupupat
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
zvláště	zvláště	k6eAd1	zvláště
zjara	zjara	k6eAd1	zjara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
tajgy	tajga	k1gFnSc2	tajga
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
i	i	k8xC	i
Eurasii	Eurasie	k1gFnSc6	Eurasie
<g/>
,	,	kIx,	,
izolovaně	izolovaně	k6eAd1	izolovaně
i	i	k9	i
jižněji	jižně	k6eAd2	jižně
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
až	až	k8xS	až
potulný	potulný	k2eAgInSc1d1	potulný
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
zejména	zejména	k9	zejména
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
proniká	pronikat	k5eAaImIp3nS	pronikat
však	však	k9	však
i	i	k9	i
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1	[number]	k4	1
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
000	[number]	k4	000
párů	pár	k1gInPc2	pár
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
550	[number]	k4	550
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
párům	pár	k1gInPc3	pár
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
<g/>
Žije	žít	k5eAaImIp3nS	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
rozlehlých	rozlehlý	k2eAgInPc6d1	rozlehlý
starých	starý	k2eAgInPc6d1	starý
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
obývá	obývat	k5eAaImIp3nS	obývat
i	i	k9	i
čistě	čistě	k6eAd1	čistě
listnaté	listnatý	k2eAgInPc4d1	listnatý
porosty	porost	k1gInPc4	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
především	především	k9	především
drobné	drobný	k2eAgMnPc4d1	drobný
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
hraboše	hraboš	k1gMnPc4	hraboš
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
menší	malý	k2eAgMnPc4d2	menší
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
i	i	k9	i
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Pokorný	Pokorný	k1gMnSc1	Pokorný
určil	určit	k5eAaPmAgMnS	určit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
vývržků	vývržek	k1gInPc2	vývržek
nalezených	nalezený	k2eAgInPc2d1	nalezený
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
podíl	podíl	k1gInSc4	podíl
hlodavců	hlodavec	k1gMnPc2	hlodavec
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
sýce	sýc	k1gMnSc2	sýc
rousného	rousný	k2eAgInSc2d1	rousný
na	na	k7c4	na
96,5	[number]	k4	96,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
na	na	k7c4	na
3,4	[number]	k4	3,4
%	%	kIx~	%
a	a	k8xC	a
hmyzu	hmyz	k1gInSc2	hmyz
na	na	k7c4	na
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
číhá	číhat	k5eAaImIp3nS	číhat
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
ji	on	k3xPp3gFnSc4	on
rychlým	rychlý	k2eAgInSc7d1	rychlý
výpadem	výpad	k1gInSc7	výpad
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
vytesaných	vytesaný	k2eAgInPc2d1	vytesaný
datlem	datel	k1gMnSc7	datel
nebo	nebo	k8xC	nebo
žlunou	žluna	k1gFnSc7	žluna
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
v	v	k7c6	v
budkách	budka	k1gFnPc6	budka
<g/>
.	.	kIx.	.
</s>
<s>
Podestýlkou	podestýlka	k1gFnSc7	podestýlka
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
vrstva	vrstva	k1gFnSc1	vrstva
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
vývržků	vývržek	k1gInPc2	vývržek
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
čistě	čistě	k6eAd1	čistě
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
32,5	[number]	k4	32,5
×	×	k?	×
26,5	[number]	k4	26,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
26	[number]	k4	26
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
samec	samec	k1gInSc1	samec
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
sám	sám	k3xTgMnSc1	sám
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
potravu	potrava	k1gFnSc4	potrava
i	i	k9	i
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
předává	předávat	k5eAaImIp3nS	předávat
samici	samice	k1gFnSc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sýc	sýc	k1gMnSc1	sýc
rousný	rousný	k2eAgMnSc1d1	rousný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sýc	sýc	k1gMnSc1	sýc
rousný	rousný	k2eAgMnSc1d1	rousný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Aegolius	Aegolius	k1gInSc1	Aegolius
funereus	funereus	k1gInSc1	funereus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
