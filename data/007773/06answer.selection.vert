<s>
Suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
želvy	želva	k1gFnPc1	želva
také	také	k9	také
svlékají	svlékat	k5eAaImIp3nP	svlékat
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
vodním	vodní	k2eAgFnPc3d1	vodní
želvám	želva	k1gFnPc3	želva
nikdy	nikdy	k6eAd1	nikdy
neodlupuje	odlupovat	k5eNaImIp3nS	odlupovat
a	a	k8xC	a
vrství	vrstvit	k5eAaImIp3nS	vrstvit
se	se	k3xPyFc4	se
do	do	k7c2	do
tlustých	tlustý	k2eAgInPc2d1	tlustý
'	'	kIx"	'
<g/>
kopečků	kopeček	k1gInPc2	kopeček
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
letokruhů	letokruh	k1gInPc2	letokruh
<g/>
.	.	kIx.	.
</s>
