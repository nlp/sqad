<s>
Březen	březen	k1gInSc1	březen
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
třetí	třetí	k4xOgInSc4	třetí
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
měsíce	měsíc	k1gInSc2	měsíc
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rašení	rašení	k1gNnSc2	rašení
bříz	bříza	k1gFnPc2	bříza
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
březosti	březost	k1gFnSc2	březost
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
nastává	nastávat	k5eAaImIp3nS	nastávat
jarní	jarní	k2eAgFnSc1d1	jarní
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
zdánlivém	zdánlivý	k2eAgInSc6d1	zdánlivý
ročním	roční	k2eAgInSc6d1	roční
pohybu	pohyb	k1gInSc6	pohyb
protíná	protínat	k5eAaImIp3nS	protínat
světový	světový	k2eAgInSc1d1	světový
rovník	rovník	k1gInSc1	rovník
-	-	kIx~	-
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
březen	březen	k1gInSc1	březen
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Březen	březen	k1gInSc1	březen
začíná	začínat	k5eAaImIp3nS	začínat
vždy	vždy	k6eAd1	vždy
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jako	jako	k8xC	jako
listopad	listopad	k1gInSc4	listopad
a	a	k8xC	a
v	v	k7c4	v
nepřestupný	přestupný	k2eNgInSc4d1	nepřestupný
rok	rok	k1gInSc4	rok
jako	jako	k8xC	jako
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
</s>
<s>
Bouřka	bouřka	k1gFnSc1	bouřka
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
dobrý	dobrý	k2eAgInSc4d1	dobrý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Březen	březen	k1gInSc1	březen
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
bez	bez	k7c2	bez
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Březen	březen	k1gInSc1	březen
hřmí	hřmět	k5eAaImIp3nS	hřmět
-	-	kIx~	-
květen	květen	k1gInSc4	květen
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
za	za	k7c4	za
kamna	kamna	k1gNnPc4	kamna
vlezem	vlezem	k?	vlezem
<g/>
.	.	kIx.	.
</s>
<s>
Březnový	březnový	k2eAgInSc1d1	březnový
sníh	sníh	k1gInSc1	sníh
škodí	škodit	k5eAaImIp3nS	škodit
polím	pole	k1gFnPc3	pole
<g/>
.	.	kIx.	.
</s>
<s>
Druhdy	druhdy	k6eAd1	druhdy
i	i	k9	i
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
hýl	hýl	k1gMnSc1	hýl
na	na	k7c4	na
nos	nos	k1gInSc4	nos
se	se	k3xPyFc4	se
posadí	posadit	k5eAaPmIp3nS	posadit
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
býti	být	k5eAaImF	být
od	od	k7c2	od
hadu	had	k1gMnSc3	had
uštknutu	uštknut	k2eAgFnSc4d1	uštknuta
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
od	od	k7c2	od
slunce	slunce	k1gNnSc2	slunce
ohřitu	ohřit	k1gInSc2	ohřit
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
chladný	chladný	k2eAgInSc1d1	chladný
máj	máj	k1gInSc1	máj
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
humno	humno	k1gNnSc1	humno
jako	jako	k8xS	jako
ráj	ráj	k1gInSc1	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
březen	březen	k1gInSc1	březen
<g/>
,	,	kIx,	,
mokrý	mokrý	k2eAgInSc1d1	mokrý
máj	máj	k1gInSc1	máj
-	-	kIx~	-
bude	být	k5eAaImBp3nS	být
humno	humno	k1gNnSc1	humno
jako	jako	k8xC	jako
ráj	ráj	k1gInSc1	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
březen	březen	k1gInSc1	březen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
březen	březen	k1gInSc1	březen
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Březen	březen	k1gInSc1	březen
-	-	kIx~	-
za	za	k7c4	za
kamna	kamna	k1gNnPc4	kamna
vlezem	vlezem	k?	vlezem
<g/>
,	,	kIx,	,
duben	duben	k1gInSc1	duben
-	-	kIx~	-
<g/>
-	-	kIx~	-
ještě	ještě	k6eAd1	ještě
tam	tam	k6eAd1	tam
budem	budem	k?	budem
<g/>
,	,	kIx,	,
máj	máj	k1gFnSc1	máj
-	-	kIx~	-
vyženem	vyženem	k?	vyženem
kozy	koza	k1gFnSc2	koza
v	v	k7c4	v
háj	háj	k1gInSc4	háj
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1848	[number]	k4	1848
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
měsíc	měsíc	k1gInSc4	měsíc
knihy	kniha	k1gFnSc2	kniha
měsíc	měsíc	k1gInSc4	měsíc
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
měsíc	měsíc	k1gInSc4	měsíc
čtenářů	čtenář	k1gMnPc2	čtenář
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
