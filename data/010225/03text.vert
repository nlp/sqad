<p>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
či	či	k8xC	či
Galaxie	galaxie	k1gFnSc1	galaxie
je	být	k5eAaImIp3nS	být
galaxie	galaxie	k1gFnSc1	galaxie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Slunce	slunce	k1gNnSc1	slunce
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
spirální	spirální	k2eAgFnSc1d1	spirální
galaxie	galaxie	k1gFnSc1	galaxie
s	s	k7c7	s
příčkou	příčka	k1gFnSc7	příčka
typu	typ	k1gInSc2	typ
SBc	SBc	k1gFnSc2	SBc
dle	dle	k7c2	dle
Hubbleovy	Hubbleův	k2eAgFnSc2d1	Hubbleova
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Rozměrem	rozměr	k1gInSc7	rozměr
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
galaxii	galaxie	k1gFnSc4	galaxie
(	(	kIx(	(
<g/>
po	po	k7c6	po
galaxii	galaxie	k1gFnSc6	galaxie
M31	M31	k1gFnSc2	M31
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
)	)	kIx)	)
v	v	k7c6	v
Místní	místní	k2eAgFnSc6d1	místní
skupině	skupina	k1gFnSc6	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
největší	veliký	k2eAgMnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměr	průměr	k1gInSc1	průměr
disku	disk	k1gInSc2	disk
Galaxie	galaxie	k1gFnSc2	galaxie
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
000	[number]	k4	000
pc	pc	k?	pc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
8,6	[number]	k4	8,6
<g/>
×	×	k?	×
<g/>
1017	[number]	k4	1017
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výrazně	výrazně	k6eAd1	výrazně
plochý	plochý	k2eAgInSc4d1	plochý
systém	systém	k1gInSc4	systém
–	–	k?	–
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tloušťka	tloušťka	k1gFnSc1	tloušťka
galaktického	galaktický	k2eAgInSc2d1	galaktický
disku	disk	k1gInSc2	disk
asi	asi	k9	asi
920	[number]	k4	920
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
<s>
Poměrem	poměr	k1gInSc7	poměr
průměru	průměr	k1gInSc2	průměr
a	a	k8xC	a
tloušťky	tloušťka	k1gFnSc2	tloušťka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tvar	tvar	k1gInSc1	tvar
Galaxie	galaxie	k1gFnSc2	galaxie
dal	dát	k5eAaPmAgInS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
hudebnímu	hudební	k2eAgNnSc3d1	hudební
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
disku	disk	k1gInSc2	disk
pouze	pouze	k6eAd1	pouze
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
středová	středový	k2eAgFnSc1d1	středová
příčková	příčkový	k2eAgFnSc1d1	příčková
výduť	výduť	k1gFnSc1	výduť
–	–	k?	–
galaktické	galaktický	k2eAgNnSc4d1	Galaktické
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
Galaxie	galaxie	k1gFnSc1	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
plochého	plochý	k2eAgInSc2d1	plochý
disku	disk	k1gInSc2	disk
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
000	[number]	k4	000
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
<s>
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
prohnutý	prohnutý	k2eAgInSc1d1	prohnutý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
spirální	spirální	k2eAgFnSc4d1	spirální
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rameny	rameno	k1gNnPc7	rameno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
logaritmických	logaritmický	k2eAgFnPc2d1	logaritmická
spirál	spirála	k1gFnPc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Spirální	spirální	k2eAgNnPc1d1	spirální
ramena	rameno	k1gNnPc1	rameno
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
galaktické	galaktický	k2eAgFnSc2d1	Galaktická
příčky	příčka	k1gFnSc2	příčka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
Galaxie	galaxie	k1gFnSc1	galaxie
–	–	k?	–
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Spirální	spirální	k2eAgInSc1d1	spirální
disk	disk	k1gInSc1	disk
a	a	k8xC	a
střed	střed	k1gInSc1	střed
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
galaktické	galaktický	k2eAgNnSc4d1	Galaktické
halo	halo	k1gNnSc4	halo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
elipsoidu	elipsoid	k1gInSc2	elipsoid
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
000	[number]	k4	000
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
galaktickým	galaktický	k2eAgNnSc7d1	Galaktické
halem	halo	k1gNnSc7	halo
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
galaktická	galaktický	k2eAgFnSc1d1	Galaktická
koróna	koróna	k1gFnSc1	koróna
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
okolo	okolo	k7c2	okolo
200	[number]	k4	200
000	[number]	k4	000
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
se	se	k3xPyFc4	se
rozměrem	rozměr	k1gInSc7	rozměr
i	i	k8xC	i
tvarem	tvar	k1gInSc7	tvar
podobá	podobat	k5eAaImIp3nS	podobat
galaxiím	galaxie	k1gFnPc3	galaxie
M61	M61	k1gFnPc2	M61
nebo	nebo	k8xC	nebo
M	M	kA	M
<g/>
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ramena	rameno	k1gNnPc4	rameno
Galaxie	galaxie	k1gFnSc2	galaxie
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
zatím	zatím	k6eAd1	zatím
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
disk	disk	k1gInSc1	disk
Galaxie	galaxie	k1gFnSc2	galaxie
několik	několik	k4yIc4	několik
galaktických	galaktický	k2eAgNnPc2d1	Galaktické
ramen	rameno	k1gNnPc2	rameno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
logaritmických	logaritmický	k2eAgFnPc2d1	logaritmická
spirál	spirála	k1gFnPc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
hvězdy	hvězda	k1gFnPc4	hvězda
populace	populace	k1gFnSc2	populace
I.	I.	kA	I.
<g/>
,	,	kIx,	,
difuzní	difuzní	k2eAgFnSc2d1	difuzní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
mladé	mladý	k2eAgInPc4d1	mladý
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
Galaxie	galaxie	k1gFnSc2	galaxie
jsou	být	k5eAaImIp3nP	být
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
podle	podle	k7c2	podle
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jejich	jejich	k3xOp3gFnSc4	jejich
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnSc2	číslo
označují	označovat	k5eAaImIp3nP	označovat
umístění	umístění	k1gNnSc4	umístění
ramen	rameno	k1gNnPc2	rameno
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
a	a	k8xC	a
8	[number]	k4	8
–	–	k?	–
rameno	rameno	k1gNnSc1	rameno
Persea	Perseus	k1gMnSc2	Perseus
</s>
</p>
<p>
<s>
11	[number]	k4	11
–	–	k?	–
rameno	rameno	k1gNnSc4	rameno
Orionu	orion	k1gInSc2	orion
(	(	kIx(	(
<g/>
též	též	k9	též
rameno	rameno	k1gNnSc1	rameno
Oriona	Orion	k1gMnSc2	Orion
–	–	k?	–
Labutě	labuť	k1gFnSc2	labuť
<g/>
,	,	kIx,	,
místní	místní	k2eAgNnSc4d1	místní
rameno	rameno	k1gNnSc4	rameno
–	–	k?	–
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
Slunce	slunce	k1gNnSc1	slunce
–	–	k?	–
číslo	číslo	k1gNnSc1	číslo
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
5	[number]	k4	5
a	a	k8xC	a
9	[number]	k4	9
–	–	k?	–
rameno	rameno	k1gNnSc1	rameno
Střelce	Střelec	k1gMnSc2	Střelec
(	(	kIx(	(
<g/>
též	též	k9	též
rameno	rameno	k1gNnSc4	rameno
Lodního	lodní	k2eAgInSc2d1	lodní
kýlu	kýl	k1gInSc2	kýl
–	–	k?	–
Střelce	Střelec	k1gMnSc2	Střelec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
a	a	k8xC	a
10	[number]	k4	10
–	–	k?	–
rameno	rameno	k1gNnSc1	rameno
Štítu-Kentaura	Štítu-Kentaura	k1gFnSc1	Štítu-Kentaura
(	(	kIx(	(
<g/>
též	též	k9	též
rameno	rameno	k1gNnSc1	rameno
Štítu	štít	k1gInSc2	štít
–	–	k?	–
Jižního	jižní	k2eAgInSc2d1	jižní
kříže	kříž	k1gInSc2	kříž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
–	–	k?	–
rameno	rameno	k1gNnSc1	rameno
Pravítka	pravítko	k1gNnSc2	pravítko
a	a	k8xC	a
vnější	vnější	k2eAgNnSc4d1	vnější
rameno	rameno	k1gNnSc4	rameno
</s>
</p>
<p>
<s>
===	===	k?	===
Jádro	jádro	k1gNnSc4	jádro
galaxie	galaxie	k1gFnPc1	galaxie
===	===	k?	===
</s>
</p>
<p>
<s>
Galaktické	galaktický	k2eAgNnSc1d1	Galaktické
jádro	jádro	k1gNnSc1	jádro
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
7,6	[number]	k4	7,6
kiloparseků	kiloparsek	k1gInPc2	kiloparsek
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
k	k	k7c3	k
souhvězdí	souhvězdí	k1gNnSc3	souhvězdí
Střelce	Střelec	k1gMnSc2	Střelec
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
přichází	přicházet	k5eAaImIp3nS	přicházet
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
radioemise	radioemise	k1gFnSc1	radioemise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obří	obří	k2eAgFnSc1d1	obří
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
Sagittarius	Sagittarius	k1gMnSc1	Sagittarius
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
a	a	k8xC	a
lokace	lokace	k1gFnPc4	lokace
====	====	k?	====
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
Zemí	zem	k1gFnSc7	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
chladné	chladný	k2eAgNnSc1d1	chladné
mračno	mračno	k1gNnSc1	mračno
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
studovat	studovat	k5eAaImF	studovat
vizuálně	vizuálně	k6eAd1	vizuálně
<g/>
,	,	kIx,	,
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgInSc6d1	ultrafialový
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
ani	ani	k8xC	ani
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
slabého	slabý	k2eAgNnSc2d1	slabé
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Galaktické	galaktický	k2eAgNnSc1d1	Galaktické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
zkoumat	zkoumat	k5eAaImF	zkoumat
pomocí	pomocí	k7c2	pomocí
záření	záření	k1gNnSc2	záření
gama	gama	k1gNnSc2	gama
<g/>
,	,	kIx,	,
silného	silný	k2eAgNnSc2d1	silné
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
submilimetrových	submilimetrový	k2eAgFnPc2d1	submilimetrová
a	a	k8xC	a
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
radioastronomie	radioastronomie	k1gFnSc1	radioastronomie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souřadnice	souřadnice	k1gFnSc1	souřadnice
umístění	umístění	k1gNnSc2	umístění
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
centra	centrum	k1gNnSc2	centrum
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
určeny	určit	k5eAaPmNgInP	určit
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
americkým	americký	k2eAgMnSc7d1	americký
astronomem	astronom	k1gMnSc7	astronom
Harlowem	Harlow	k1gMnSc7	Harlow
Shapleym	Shapleym	k1gInSc4	Shapleym
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
rozmístění	rozmístění	k1gNnSc2	rozmístění
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souřadnice	souřadnice	k1gFnSc1	souřadnice
jádra	jádro	k1gNnSc2	jádro
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
RA	ra	k0	ra
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
h	h	k?	h
<g/>
45	[number]	k4	45
<g/>
m	m	kA	m
<g/>
40.04	[number]	k4	40.04
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Deklinace	deklinace	k1gFnSc1	deklinace
<g/>
:	:	kIx,	:
−	−	k?	−
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
'	'	kIx"	'
28.1	[number]	k4	28.1
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
galaktickém	galaktický	k2eAgNnSc6d1	Galaktické
jádru	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
komplex	komplex	k1gInSc1	komplex
radioemitujích	radioemituje	k1gFnPc6	radioemituje
zdrojů	zdroj	k1gInPc2	zdroj
zvaný	zvaný	k2eAgInSc1d1	zvaný
Sagittarius	Sagittarius	k1gInSc1	Sagittarius
A	A	kA	A
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
rádiový	rádiový	k2eAgInSc1d1	rádiový
zdroj	zdroj	k1gInSc1	zdroj
Sagittarius	Sagittarius	k1gInSc1	Sagittarius
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
téměř	téměř	k6eAd1	téměř
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
obří	obří	k2eAgFnSc7d1	obří
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jádra	jádro	k1gNnSc2	jádro
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
okruhu	okruh	k1gInSc6	okruh
<g/>
"	"	kIx"	"
jednoho	jeden	k4xCgInSc2	jeden
parseku	parsec	k1gInSc2	parsec
okolo	okolo	k7c2	okolo
Sagittaria	Sagittarium	k1gNnSc2	Sagittarium
A	A	kA	A
<g/>
*	*	kIx~	*
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tisíce	tisíc	k4xCgInPc1	tisíc
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
rudé	rudý	k2eAgFnPc1d1	rudá
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Galaktické	galaktický	k2eAgNnSc1d1	Galaktické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
bohaté	bohatý	k2eAgInPc4d1	bohatý
na	na	k7c4	na
masivní	masivní	k2eAgFnPc4d1	masivní
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
objevena	objevit	k5eAaPmNgFnS	objevit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovka	stovka	k1gFnSc1	stovka
Wolf-Rayetových	Wolf-Rayetový	k2eAgFnPc2d1	Wolf-Rayetový
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
hvězdy	hvězda	k1gFnSc2	hvězda
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
O	O	kA	O
a	a	k8xC	a
B.	B.	kA	B.
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
byly	být	k5eAaImAgFnP	být
zformovány	zformovat	k5eAaPmNgFnP	zformovat
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
formace	formace	k1gFnSc2	formace
před	před	k7c7	před
několika	několik	k4yIc7	několik
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
"	"	kIx"	"
<g/>
mladých	mladý	k1gMnPc2	mladý
<g/>
"	"	kIx"	"
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
odborníky	odborník	k1gMnPc4	odborník
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slapové	slapový	k2eAgInPc4d1	slapový
síly	síl	k1gInPc4	síl
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
blízkou	blízký	k2eAgFnSc4d1	blízká
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
takovému	takový	k3xDgNnSc3	takový
zformování	zformování	k1gNnSc3	zformování
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
pozoruhodnější	pozoruhodný	k2eAgFnPc1d2	pozoruhodnější
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
velice	velice	k6eAd1	velice
blízkých	blízký	k2eAgFnPc6d1	blízká
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
okolo	okolo	k7c2	okolo
Sagittaria	Sagittarium	k1gNnSc2	Sagittarium
A	A	kA	A
<g/>
*	*	kIx~	*
jako	jako	k8xS	jako
například	například	k6eAd1	například
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc1d1	různá
teorie	teorie	k1gFnPc1	teorie
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
taková	takový	k3xDgFnSc1	takový
formace	formace	k1gFnSc1	formace
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
mimo	mimo	k7c4	mimo
galaktické	galaktický	k2eAgNnSc4d1	Galaktické
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
již	již	k6eAd1	již
ve	v	k7c6	v
zformovaném	zformovaný	k2eAgInSc6d1	zformovaný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galaktické	galaktický	k2eAgNnSc1d1	Galaktické
halo	halo	k1gNnSc1	halo
===	===	k?	===
</s>
</p>
<p>
<s>
Střed	střed	k1gInSc1	střed
a	a	k8xC	a
ramena	rameno	k1gNnSc2	rameno
Galaxie	galaxie	k1gFnSc2	galaxie
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
galaktické	galaktický	k2eAgNnSc4d1	Galaktické
halo	halo	k1gNnSc4	halo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvar	tvar	k1gInSc4	tvar
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poloměr	poloměr	k1gInSc1	poloměr
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
pc	pc	k?	pc
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
miliard	miliarda	k4xCgFnPc2	miliarda
M	M	kA	M
<g/>
☉	☉	k?	☉
<g/>
.	.	kIx.	.
</s>
<s>
Halo	halo	k1gNnSc1	halo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
hmotné	hmotný	k2eAgFnPc1d1	hmotná
jako	jako	k8xS	jako
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
část	část	k1gFnSc1	část
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Halo	halo	k1gNnSc1	halo
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
z	z	k7c2	z
prvotního	prvotní	k2eAgNnSc2d1	prvotní
vývojového	vývojový	k2eAgNnSc2d1	vývojové
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
rotujícího	rotující	k2eAgInSc2d1	rotující
kulového	kulový	k2eAgInSc2d1	kulový
oblaku	oblak	k1gInSc2	oblak
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
první	první	k4xOgFnPc1	první
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgInPc1d3	nejstarší
objekty	objekt	k1gInPc1	objekt
Galaxie	galaxie	k1gFnSc2	galaxie
–	–	k?	–
staré	starý	k2eAgFnSc2d1	stará
hvězdy	hvězda	k1gFnSc2	hvězda
populace	populace	k1gFnSc2	populace
II	II	kA	II
a	a	k8xC	a
kulové	kulový	k2eAgFnSc2d1	kulová
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
z	z	k7c2	z
Messierova	Messierův	k2eAgInSc2d1	Messierův
katalogu	katalog	k1gInSc2	katalog
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
v	v	k7c4	v
halu	hala	k1gFnSc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
halu	halo	k1gNnSc6	halo
mají	mít	k5eAaImIp3nP	mít
malou	malý	k2eAgFnSc4d1	malá
hmotnost	hmotnost	k1gFnSc4	hmotnost
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnPc4d2	menší
než	než	k8xS	než
0,8	[number]	k4	0,8
M	M	kA	M
<g/>
☉	☉	k?	☉
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
hmotnější	hmotný	k2eAgFnPc1d2	hmotnější
hvězdy	hvězda	k1gFnPc1	hvězda
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
vyhořely	vyhořet	k5eAaPmAgFnP	vyhořet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galaktická	galaktický	k2eAgFnSc1d1	Galaktická
koróna	koróna	k1gFnSc1	koróna
===	===	k?	===
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
Galaxie	galaxie	k1gFnSc1	galaxie
a	a	k8xC	a
galaktické	galaktický	k2eAgNnSc1d1	Galaktické
halo	halo	k1gNnSc1	halo
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
obrovský	obrovský	k2eAgInSc4d1	obrovský
kulový	kulový	k2eAgInSc4d1	kulový
oblak	oblak	k1gInSc4	oblak
řídkého	řídký	k2eAgInSc2d1	řídký
plynu	plyn	k1gInSc2	plyn
–	–	k?	–
galaktická	galaktický	k2eAgFnSc1d1	Galaktická
koróna	koróna	k1gFnSc1	koróna
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nezářící	zářící	k2eNgFnSc2d1	nezářící
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
gravitační	gravitační	k2eAgInSc1d1	gravitační
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
a	a	k8xC	a
blízkých	blízký	k2eAgFnPc2d1	blízká
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
galaktické	galaktický	k2eAgFnSc2d1	Galaktická
koróny	koróna	k1gFnSc2	koróna
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
pás	pás	k1gInSc4	pás
táhnoucí	táhnoucí	k2eAgInSc4d1	táhnoucí
se	se	k3xPyFc4	se
celou	celý	k2eAgFnSc7d1	celá
oblohou	obloha	k1gFnSc7	obloha
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
svit	svit	k1gInSc4	svit
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
množství	množství	k1gNnSc1	množství
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Lidově	lidově	k6eAd1	lidově
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Korektnější	korektní	k2eAgMnSc1d2	korektnější
by	by	kYmCp3nS	by
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
Galaxie	galaxie	k1gFnSc1	galaxie
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Ze	z	k7c2
Země	zem	k1gFnSc2
se	se	k3xPyFc4
Mléčná	mléčný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
jeví	jevit	k5eAaImIp3nS
jako	jako	k8xS
pás	pás	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
Země	země	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
uvnitř	uvnitř	k7c2
jejího	její	k3xOp3gInSc2
galaktického	galaktický	k2eAgInSc2d1
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dvě	dva	k4xCgNnPc1	dva
ramena	rameno	k1gNnPc1	rameno
<g/>
:	:	kIx,	:
rameno	rameno	k1gNnSc4	rameno
Střelce	Střelec	k1gMnSc2	Střelec
a	a	k8xC	a
rameno	rameno	k1gNnSc1	rameno
Orionu	orion	k1gInSc2	orion
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
disku	disk	k1gInSc2	disk
a	a	k8xC	a
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
díváme	dívat	k5eAaImIp1nP	dívat
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
,	,	kIx,	,
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
než	než	k8xS	než
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
za	za	k7c2	za
jasných	jasný	k2eAgFnPc2d1	jasná
nocí	noc	k1gFnPc2	noc
a	a	k8xC	a
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
vzdálených	vzdálený	k2eAgNnPc6d1	vzdálené
od	od	k7c2	od
velkých	velký	k2eAgInPc2d1	velký
zdrojů	zdroj	k1gInPc2	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
světelnému	světelný	k2eAgNnSc3d1	světelné
znečištění	znečištění	k1gNnSc3	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
mnoho	mnoho	k4c4	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
prostému	prostý	k2eAgNnSc3d1	prosté
oku	oko	k1gNnSc3	oko
skryta	skryt	k2eAgNnPc4d1	skryto
a	a	k8xC	a
pozorovat	pozorovat	k5eAaImF	pozorovat
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
teprve	teprve	k6eAd1	teprve
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
triedr	triedr	k1gInSc1	triedr
jich	on	k3xPp3gFnPc2	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
pozorování	pozorování	k1gNnPc4	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
hvězd	hvězda	k1gFnPc2	hvězda
tvořena	tvořit	k5eAaImNgFnS	tvořit
i	i	k9	i
temným	temný	k2eAgInSc7d1	temný
mezihvězdným	mezihvězdný	k2eAgInSc7d1	mezihvězdný
plynem	plyn	k1gInSc7	plyn
či	či	k8xC	či
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nám	my	k3xPp1nPc3	my
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
pohlédnout	pohlédnout	k5eAaPmF	pohlédnout
do	do	k7c2	do
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
oblastí	oblast	k1gFnPc2	oblast
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
některými	některý	k3yIgInPc7	některý
směry	směr	k1gInPc7	směr
je	být	k5eAaImIp3nS	být
hvězd	hvězda	k1gFnPc2	hvězda
méně	málo	k6eAd2	málo
nebo	nebo	k8xC	nebo
že	že	k8xS	že
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rameni	rameno	k1gNnSc6	rameno
Orion	orion	k1gInSc4	orion
(	(	kIx(	(
<g/>
místní	místní	k2eAgNnSc4d1	místní
rameno	rameno	k1gNnSc4	rameno
<g/>
)	)	kIx)	)
asi	asi	k9	asi
8	[number]	k4	8
000	[number]	k4	000
pc	pc	k?	pc
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
hvězdami	hvězda	k1gFnPc7	hvězda
obíhá	obíhat	k5eAaImIp3nS	obíhat
střed	střed	k1gInSc1	střed
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
galaktickém	galaktický	k2eAgInSc6d1	galaktický
epicyklu	epicykl	k1gInSc6	epicykl
přibližně	přibližně	k6eAd1	přibližně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
220	[number]	k4	220
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oběžný	oběžný	k2eAgInSc1d1	oběžný
pohyb	pohyb	k1gInSc1	pohyb
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
souhvězdí	souhvězdí	k1gNnSc3	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
oběhlo	oběhnout	k5eAaPmAgNnS	oběhnout
Slunce	slunce	k1gNnSc1	slunce
střed	střed	k1gInSc1	střed
galaxie	galaxie	k1gFnSc1	galaxie
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bod	bod	k1gInSc1	bod
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
Slunce	slunce	k1gNnSc2	slunce
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
okolním	okolní	k2eAgFnPc3d1	okolní
hvězdám	hvězda	k1gFnPc3	hvězda
směřuje	směřovat	k5eAaImIp3nS	směřovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sluneční	sluneční	k2eAgInSc1d1	sluneční
apex	apex	k1gInSc1	apex
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Herkules	Herkules	k1gMnSc1	Herkules
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgInSc2	ten
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
sekulární	sekulární	k2eAgFnSc1d1	sekulární
paralaxa	paralaxa	k1gFnSc1	paralaxa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnSc1	okolí
Galaxie	galaxie	k1gFnSc1	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
galaxií	galaxie	k1gFnPc2	galaxie
===	===	k?	===
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
kupě	kupa	k1gFnSc6	kupa
galaxií	galaxie	k1gFnPc2	galaxie
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
spirální	spirální	k2eAgFnSc7d1	spirální
galaxií	galaxie	k1gFnSc7	galaxie
M31	M31	k1gFnSc2	M31
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Andromedy	Andromeda	k1gMnSc2	Andromeda
a	a	k8xC	a
M33	M33	k1gMnSc2	M33
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaxii	galaxie	k1gFnSc3	galaxie
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
několik	několik	k4yIc1	několik
gravitačně	gravitačně	k6eAd1	gravitačně
vázaných	vázaný	k2eAgFnPc2d1	vázaná
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgMnPc1d3	nejbližší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
galaxie	galaxie	k1gFnPc4	galaxie
Velký	velký	k2eAgMnSc1d1	velký
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
19	[number]	k4	19
900	[number]	k4	900
pc	pc	k?	pc
od	od	k7c2	od
galaktického	galaktický	k2eAgInSc2d1	galaktický
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
satelitní	satelitní	k2eAgFnSc7d1	satelitní
galaxií	galaxie	k1gFnSc7	galaxie
je	být	k5eAaImIp3nS	být
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
galaxie	galaxie	k1gFnSc1	galaxie
Velký	velký	k2eAgInSc1d1	velký
Magellanův	Magellanův	k2eAgInSc1d1	Magellanův
oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
rozměr	rozměr	k1gInSc1	rozměr
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
200	[number]	k4	200
pc	pc	k?	pc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
55	[number]	k4	55
000	[number]	k4	000
pc	pc	k?	pc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgFnPc1d3	nejvzdálenější
galaxie	galaxie	k1gFnPc1	galaxie
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
galaxie	galaxie	k1gFnSc1	galaxie
GR	GR	kA	GR
8	[number]	k4	8
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
asi	asi	k9	asi
1	[number]	k4	1
595	[number]	k4	595
kpc	kpc	k?	kpc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
galaxie	galaxie	k1gFnPc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
a	a	k8xC	a
za	za	k7c4	za
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
splynou	splynout	k5eAaPmIp3nP	splynout
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
galaxii	galaxie	k1gFnSc4	galaxie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Milkdromeda	Milkdromeda	k1gMnSc1	Milkdromeda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Místní	místní	k2eAgFnSc6d1	místní
skupině	skupina	k1gFnSc6	skupina
galaxií	galaxie	k1gFnPc2	galaxie
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
objeveno	objevit	k5eAaPmNgNnS	objevit
42	[number]	k4	42
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
3	[number]	k4	3
velké	velký	k2eAgFnSc2d1	velká
galaxie	galaxie	k1gFnSc2	galaxie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
naší	náš	k3xOp1gFnSc2	náš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Místní	místní	k2eAgFnSc1d1	místní
nadkupa	nadkupa	k1gFnSc1	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
===	===	k?	===
</s>
</p>
<p>
<s>
Místní	místní	k2eAgFnSc1d1	místní
skupina	skupina	k1gFnSc1	skupina
galaxií	galaxie	k1gFnPc2	galaxie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadkupě	nadkupa	k1gFnSc6	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
jako	jako	k8xC	jako
Místní	místní	k2eAgFnSc1d1	místní
nadkupa	nadkupa	k1gFnSc1	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Supergalaxie	supergalaxie	k1gFnSc1	supergalaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nadkupě	nadkupa	k1gFnSc6	nadkupa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
kup	kupa	k1gFnPc2	kupa
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
Panně	Panna	k1gFnSc6	Panna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
asi	asi	k9	asi
18,4	[number]	k4	18,4
Mpc	Mpc	k1gFnPc2	Mpc
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
Místní	místní	k2eAgFnSc2d1	místní
nadkupy	nadkupa	k1gFnSc2	nadkupa
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
46	[number]	k4	46
Mpc	Mpc	k1gFnPc2	Mpc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
Galaxii	galaxie	k1gFnSc4	galaxie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
nejednoznačná	jednoznačný	k2eNgFnSc1d1	nejednoznačná
terminologie	terminologie	k1gFnSc1	terminologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
historický	historický	k2eAgInSc4d1	historický
název	název	k1gInSc4	název
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
současně	současně	k6eAd1	současně
o	o	k7c4	o
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
název	název	k1gInSc4	název
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
terminologie	terminologie	k1gFnSc1	terminologie
není	být	k5eNaImIp3nS	být
zajedno	zajedno	k6eAd1	zajedno
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
převažuje	převažovat	k5eAaImIp3nS	převažovat
používání	používání	k1gNnSc1	používání
názvu	název	k1gInSc2	název
Milky	Milka	k1gFnSc2	Milka
Way	Way	k1gMnPc7	Way
či	či	k8xC	či
Milky	Milek	k1gMnPc7	Milek
Way	Way	k1gFnSc2	Way
Galaxy	Galax	k1gInPc4	Galax
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
prolínání	prolínání	k1gNnSc3	prolínání
těchto	tento	k3xDgInPc2	tento
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
u	u	k7c2	u
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
takto	takto	k6eAd1	takto
nazýval	nazývat	k5eAaImAgInS	nazývat
pás	pás	k1gInSc1	pás
největší	veliký	k2eAgFnSc2d3	veliký
vizuální	vizuální	k2eAgFnSc2d1	vizuální
koncentrace	koncentrace	k1gFnSc2	koncentrace
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
část	část	k1gFnSc4	část
Galaxie	galaxie	k1gFnSc2	galaxie
(	(	kIx(	(
<g/>
průmět	průmět	k1gInSc1	průmět
galaktického	galaktický	k2eAgInSc2d1	galaktický
rovníku	rovník	k1gInSc2	rovník
na	na	k7c4	na
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
sféru	sféra	k1gFnSc4	sféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
označení	označení	k1gNnSc2	označení
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
pás	pás	k1gInSc1	pás
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
referovat	referovat	k5eAaBmF	referovat
jako	jako	k9	jako
o	o	k7c4	o
galaxii	galaxie	k1gFnSc4	galaxie
Mléčné	mléčný	k2eAgFnSc3d1	mléčná
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
snaha	snaha	k1gFnSc1	snaha
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
nahradit	nahradit	k5eAaPmF	nahradit
názvem	název	k1gInSc7	název
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
zažité	zažitý	k2eAgNnSc4d1	zažité
používání	používání	k1gNnSc4	používání
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
obou	dva	k4xCgMnPc2	dva
těchto	tento	k3xDgInPc2	tento
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
referuje	referovat	k5eAaBmIp3nS	referovat
jako	jako	k9	jako
o	o	k7c4	o
galaxii	galaxie	k1gFnSc4	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
či	či	k8xC	či
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
Galaxii	galaxie	k1gFnSc6	galaxie
či	či	k8xC	či
naší	náš	k3xOp1gFnSc3	náš
Galaxii	galaxie	k1gFnSc3	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Melia	Melia	k1gFnSc1	Melia
<g/>
,	,	kIx,	,
Fulvio	Fulvio	k1gMnSc1	Fulvio
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Hole	hole	k1gFnSc2	hole
in	in	k?	in
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
Our	Our	k1gMnSc2	Our
Galaxy	Galax	k1gInPc4	Galax
<g/>
,	,	kIx,	,
Princeton	Princeton	k1gInSc4	Princeton
U	u	k7c2	u
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Eckart	Eckart	k1gInSc1	Eckart
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Schödel	Schödlo	k1gNnPc2	Schödlo
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Straubmeier	Straubmeier	k1gMnSc1	Straubmeier
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Black	Black	k1gInSc1	Black
Hole	hole	k1gFnSc1	hole
at	at	k?	at
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
the	the	k?	the
Milky	Milka	k1gFnSc2	Milka
Way	Way	k1gMnSc1	Way
<g/>
,	,	kIx,	,
Imperial	Imperial	k1gMnSc1	Imperial
College	Colleg	k1gFnSc2	Colleg
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Melia	Melia	k1gFnSc1	Melia
<g/>
,	,	kIx,	,
Fulvio	Fulvio	k1gNnSc1	Fulvio
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Galactic	Galactice	k1gFnPc2	Galactice
Supermassive	Supermassiev	k1gFnSc2	Supermassiev
Black	Black	k1gMnSc1	Black
Hole	hole	k1gFnSc1	hole
<g/>
,	,	kIx,	,
Princeton	Princeton	k1gInSc1	Princeton
U	u	k7c2	u
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
</s>
</p>
<p>
<s>
Galaktické	galaktický	k2eAgFnPc4d1	Galaktická
souřadnice	souřadnice	k1gFnPc4	souřadnice
</s>
</p>
<p>
<s>
Messierův	Messierův	k2eAgInSc1d1	Messierův
katalog	katalog	k1gInSc1	katalog
</s>
</p>
<p>
<s>
Abellův	Abellův	k2eAgInSc1d1	Abellův
katalog	katalog	k1gInSc1	katalog
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Galaxie	galaxie	k1gFnSc1	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
panoramatický	panoramatický	k2eAgInSc1d1	panoramatický
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
Galaxii	galaxie	k1gFnSc6	galaxie
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
Chandra	chandra	k1gFnSc1	chandra
</s>
</p>
<p>
<s>
UCLA	UCLA	kA	UCLA
Galactic	Galactice	k1gFnPc2	Galactice
Center	centrum	k1gNnPc2	centrum
Group	Group	k1gMnSc1	Group
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Extraterrestrial	Extraterrestrial	k1gInSc4	Extraterrestrial
Physics	Physics	k1gInSc1	Physics
Galactic	Galactice	k1gFnPc2	Galactice
Center	centrum	k1gNnPc2	centrum
Group	Group	k1gMnSc1	Group
</s>
</p>
<p>
<s>
The	The	k?	The
Galactic	Galactice	k1gFnPc2	Galactice
Supermassive	Supermassiev	k1gFnSc2	Supermassiev
Black	Blacka	k1gFnPc2	Blacka
Hole	hole	k1gFnSc2	hole
</s>
</p>
<p>
<s>
The	The	k?	The
Black	Black	k1gInSc1	Black
Hole	hole	k1gFnSc1	hole
at	at	k?	at
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
the	the	k?	the
Milky	Milka	k1gFnSc2	Milka
Way	Way	k1gFnSc2	Way
</s>
</p>
<p>
<s>
The	The	k?	The
dark	dark	k1gInSc1	dark
heart	heart	k1gInSc1	heart
of	of	k?	of
the	the	k?	the
Milky	Milka	k1gFnSc2	Milka
Way	Way	k1gFnSc2	Way
</s>
</p>
<p>
<s>
Dramatic	Dramatice	k1gFnPc2	Dramatice
Increase	Increasa	k1gFnSc3	Increasa
in	in	k?	in
Supernova	supernova	k1gFnSc1	supernova
Explosions	Explosions	k1gInSc1	Explosions
Looms	Looms	k1gInSc1	Looms
</s>
</p>
<p>
<s>
APOD	apod	kA	apod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Journey	Journe	k1gMnPc4	Journe
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
the	the	k?	the
Galaxy	Galax	k1gInPc7	Galax
</s>
</p>
<p>
<s>
A	a	k9	a
Galactic	Galactice	k1gFnPc2	Galactice
Cloud	Cloud	k1gMnSc1	Cloud
of	of	k?	of
Antimatter	Antimatter	k1gMnSc1	Antimatter
</s>
</p>
<p>
<s>
Fast	Fast	k2eAgInSc1d1	Fast
Stars	Stars	k1gInSc1	Stars
Near	Near	k1gInSc1	Near
the	the	k?	the
Galactic	Galactice	k1gFnPc2	Galactice
Center	centrum	k1gNnPc2	centrum
</s>
</p>
<p>
<s>
At	At	k?	At
the	the	k?	the
Center	centrum	k1gNnPc2	centrum
of	of	k?	of
the	the	k?	the
Milky	Milka	k1gFnSc2	Milka
Way	Way	k1gFnSc2	Way
</s>
</p>
<p>
<s>
Galactic	Galactice	k1gFnPc2	Galactice
Centre	centr	k1gInSc5	centr
Starscape	Starscap	k1gInSc5	Starscap
</s>
</p>
