<s>
Andy	Anda	k1gFnPc1
</s>
<s>
Andy	Anda	k1gFnPc1
Aconcagua	Aconcaguum	k1gNnSc2
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
6	#num#	k4
959	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
Aconcagua	Aconcagua	k1gMnSc1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
7	#num#	k4
240	#num#	k4
km	km	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
200	#num#	k4
až	až	k6eAd1
700	#num#	k4
km	km	kA
</s>
<s>
Nadřazená	nadřazený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Kordillery	Kordillery	k1gFnPc1
</s>
<s>
Světadíl	světadíl	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Chile	Chile	k1gNnPc4
ChileArgentina	ChileArgentin	k2eAgFnSc1d1
ArgentinaBolívie	ArgentinaBolívie	k1gFnSc1
BolíviePeru	BolíviePer	k1gInSc2
PeruEkvádor	PeruEkvádora	k1gFnPc2
EkvádorKolumbie	EkvádorKolumbie	k1gFnSc1
KolumbieVenezuela	KolumbieVenezuela	k1gFnSc1
Venezuela	Venezuela	k1gFnSc1
Povodí	povodit	k5eAaPmIp3nS
</s>
<s>
Amazonka	Amazonka	k1gFnSc1
<g/>
,	,	kIx,
Orinoco	Orinoco	k1gNnSc1
<g/>
,	,	kIx,
Paraná	Paraná	k1gFnSc1
<g/>
,	,	kIx,
Magdalena	Magdalena	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
70	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andy	Anda	k1gFnPc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Cordillera	Cordiller	k1gMnSc2
de	de	k?
los	los	k1gInSc1
Andes	Andes	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
nejdelší	dlouhý	k2eAgNnSc4d3
horské	horský	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velehory	velehora	k1gFnPc1
mají	mít	k5eAaImIp3nP
délku	délka	k1gFnSc4
7	#num#	k4
240	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
šířku	šířka	k1gFnSc4
200	#num#	k4
až	až	k9
700	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládají	rozkládat	k5eAaImIp3nP
se	se	k3xPyFc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
od	od	k7c2
Panamy	Panama	k1gFnSc2
a	a	k8xC
Karibského	karibský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
až	až	k9
po	po	k7c4
Drakeův	Drakeův	k2eAgInSc4d1
průliv	průliv	k1gInSc4
u	u	k7c2
Ohňové	ohňový	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
11	#num#	k4
<g/>
°	°	k?
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
po	po	k7c6
56	#num#	k4
<g/>
°	°	k?
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
prochází	procházet	k5eAaImIp3nS
státy	stát	k1gInPc4
<g/>
:	:	kIx,
Venezuela	Venezuela	k1gFnSc1
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
Ekvádor	Ekvádor	k1gInSc1
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
,	,	kIx,
Bolívie	Bolívie	k1gFnSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
a	a	k8xC
Argentina	Argentina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
rozsahu	rozsah	k1gInSc3
od	od	k7c2
tropického	tropický	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
až	až	k9
po	po	k7c4
mírné	mírný	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
a	a	k8xC
vysokým	vysoký	k2eAgFnPc3d1
nadmořským	nadmořský	k2eAgFnPc3d1
výškám	výška	k1gFnPc3
mají	mít	k5eAaImIp3nP
Andy	Anda	k1gFnPc1
nejrůznorodější	různorodý	k2eAgFnSc4d3
krajinu	krajina	k1gFnSc4
ze	z	k7c2
všech	všecek	k3xTgFnPc2
velehor	velehora	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgNnPc6
obdobích	období	k1gNnPc6
vzniku	vznik	k1gInSc2
pohoří	pohoří	k1gNnSc2
zde	zde	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
silnému	silný	k2eAgInSc3d1
vulkanismu	vulkanismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
50	#num#	k4
činných	činný	k2eAgMnPc2d1
a	a	k8xC
40	#num#	k4
vyhaslých	vyhaslý	k2eAgInPc2d1
vulkánů	vulkán	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Andy	Anda	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
jihoamerickou	jihoamerický	k2eAgFnSc4d1
část	část	k1gFnSc4
Kordiller	Kordillery	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
Andy	Anda	k1gFnSc2
pravděpodobně	pravděpodobně	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
kečuánského	kečuánský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
anti	ant	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
východ	východ	k1gInSc4
<g/>
,	,	kIx,
respektive	respektive	k9
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
vychází	vycházet	k5eAaImIp3nS
Slunce	slunce	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Huascarán	Huascarán	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
768	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Seskládaný	seskládaný	k2eAgInSc1d1
satelitní	satelitní	k2eAgInSc1d1
obraz	obraz	k1gInSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
jsou	být	k5eAaImIp3nP
zřetelně	zřetelně	k6eAd1
vidět	vidět	k5eAaImF
</s>
<s>
Andy	Anda	k1gFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
několik	několik	k4yIc4
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
v	v	k7c6
Argentině	Argentina	k1gFnSc6
a	a	k8xC
Chile	Chile	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrální	centrální	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
zasahují	zasahovat	k5eAaImIp3nP
do	do	k7c2
chilské	chilský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
pokračují	pokračovat	k5eAaImIp3nP
do	do	k7c2
Peru	Peru	k1gNnSc2
a	a	k8xC
Bolívie	Bolívie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
zasahují	zasahovat	k5eAaImIp3nP
do	do	k7c2
Venezuely	Venezuela	k1gFnSc2
<g/>
,	,	kIx,
Kolumbie	Kolumbie	k1gFnSc2
a	a	k8xC
Ekvádoru	Ekvádor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
je	být	k5eAaImIp3nS
tvoří	tvořit	k5eAaImIp3nP
dva	dva	k4xCgInPc4
paralelní	paralelní	k2eAgInPc4d1
hřbety	hřbet	k1gInPc4
Cordillera	Cordiller	k1gMnSc2
Occidental	Occidental	k1gMnSc1
a	a	k8xC
Cordillera	Cordiller	k1gMnSc4
Oriental	Oriental	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
<g/>
,	,	kIx,
severně	severně	k6eAd1
od	od	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Ekvádorem	Ekvádor	k1gInSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozdělují	rozdělovat	k5eAaImIp3nP
na	na	k7c4
tři	tři	k4xCgInPc4
rovnoběžné	rovnoběžný	k2eAgInPc4d1
hřbety	hřbet	k1gInPc4
–	–	k?
západní	západní	k2eAgFnSc4d1
<g/>
,	,	kIx,
centrální	centrální	k2eAgFnSc4d1
a	a	k8xC
východní	východní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInPc1d1
je	být	k5eAaImIp3nS
jediným	jediné	k1gNnSc7
z	z	k7c2
horských	horský	k2eAgInPc2d1
hřbetů	hřbet	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
Venezuely	Venezuela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
střední	střední	k2eAgFnSc4d1
a	a	k8xC
severní	severní	k2eAgFnSc4d1
část	část	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
několik	několik	k4yIc1
paralelních	paralelní	k2eAgInPc2d1
hřebenů	hřeben	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
Bolívie	Bolívie	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
tzv.	tzv.	kA
Altiplano	Altiplana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
cordillera	cordiller	k1gMnSc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
španělštiny	španělština	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
provaz	provaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andy	Anda	k1gFnPc1
jsou	být	k5eAaImIp3nP
asi	asi	k9
200	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
km	km	kA
široké	široký	k2eAgFnSc2d1
téměř	téměř	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
kromě	kromě	k7c2
bolivijského	bolivijský	k2eAgInSc2d1
ohybu	ohyb	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dosahují	dosahovat	k5eAaImIp3nP
šířky	šířka	k1gFnPc1
640	#num#	k4
<g/>
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsevernější	severní	k2eAgFnPc4d3
části	část	k1gFnPc4
pohoří	pohoří	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nP
ostrovy	ostrov	k1gInPc1
Aruba	Arub	k1gMnSc2
<g/>
,	,	kIx,
Bonaire	Bonair	k1gInSc5
a	a	k8xC
Curaçao	curaçao	k1gNnPc1
v	v	k7c6
Karibském	karibský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
And	Anda	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Patagonské	patagonský	k2eAgFnSc2d1
Andy	Anda	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
zaledněných	zaledněný	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
severopatagonské	severopatagonský	k2eAgNnSc1d1
ledovcové	ledovcový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
jihopatagonské	jihopatagonský	k2eAgNnSc1d1
ledovcové	ledovcový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
a	a	k8xC
ledovec	ledovec	k1gInSc1
v	v	k7c6
Darwinově	Darwinův	k2eAgNnSc6d1
pohoří	pohoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Severní	severní	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Kolumbijské	kolumbijský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
rozdělené	rozdělený	k2eAgInPc1d1
údolími	údolí	k1gNnPc7
řek	řek	k1gMnSc1
Cauca	Cauca	k1gMnSc1
a	a	k8xC
Magdalena	Magdalena	k1gFnSc1
na	na	k7c4
tři	tři	k4xCgMnPc4
téměř	téměř	k6eAd1
rovnoběžně	rovnoběžně	k6eAd1
se	se	k3xPyFc4
rozkládající	rozkládající	k2eAgNnPc1d1
horská	horský	k2eAgNnPc1d1
pásma	pásmo	k1gNnPc1
ve	v	k7c6
směru	směr	k1gInSc6
severovýchod-jihozápad	severovýchod-jihozápad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejzápadněji	západně	k6eAd3
leží	ležet	k5eAaImIp3nP
Západní	západní	k2eAgMnPc1d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Occidental	Occidental	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
horské	horský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
má	mít	k5eAaImIp3nS
nadmořskou	nadmořský	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
okolo	okolo	k7c2
3	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
u	u	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Ekvádorem	Ekvádor	k1gInSc7
<g/>
,	,	kIx,
prudce	prudko	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
Nevado	Nevada	k1gFnSc5
de	de	k?
Cumbal	Cumbal	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
764	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
Kolumbijských	kolumbijský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nS
Střední	střední	k1gMnSc1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Central	Central	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silný	silný	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
zde	zde	k6eAd1
vytvořil	vytvořit	k5eAaPmAgInS
řadu	řada	k1gFnSc4
mohutných	mohutný	k2eAgInPc2d1
stratovulkánů	stratovulkán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hlavním	hlavní	k2eAgMnPc3d1
činným	činný	k2eAgMnPc3d1
vulkánům	vulkán	k1gInPc3
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
náleží	náležet	k5eAaImIp3nS
<g/>
:	:	kIx,
Nevado	Nevada	k1gFnSc5
del	del	k?
Ruiz	Ruiz	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
400	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nevado	Nevada	k1gFnSc5
del	del	k?
Tolima	Tolimum	k1gNnPc1
(	(	kIx(
<g/>
5	#num#	k4
215	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nevado	Nevada	k1gFnSc5
del	del	k?
Huila	Huilo	k1gNnPc1
(	(	kIx(
<g/>
5	#num#	k4
757	#num#	k4
m	m	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Puracé	Puracá	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
756	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračováním	pokračování	k1gNnSc7
Střední	střední	k2eAgFnPc4d1
Kordillery	Kordillery	k1gFnPc4
je	být	k5eAaImIp3nS
i	i	k9
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Karibského	karibský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
ležící	ležící	k2eAgFnSc1d1
<g/>
,	,	kIx,
pohoří	pohořet	k5eAaPmIp3nS
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
de	de	k?
Santa	Santa	k1gFnSc1
Marta	Marta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Kolumbijských	kolumbijský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nS
pásmo	pásmo	k1gNnSc1
Východní	východní	k2eAgFnSc2d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Oriental	Oriental	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jih	jih	k1gInSc1
pásma	pásmo	k1gNnSc2
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
úzkým	úzký	k2eAgInSc7d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
vysokým	vysoký	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
pak	pak	k6eAd1
vystupuje	vystupovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
část	část	k1gFnSc4
Východní	východní	k2eAgFnPc4d1
Kordillery	Kordillery	k1gFnPc4
pohoří	pohořet	k5eAaPmIp3nS
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
de	de	k?
Cocuy	Cocua	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Venezuelské	venezuelský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Na	na	k7c4
kolumbijskou	kolumbijský	k2eAgFnSc4d1
Východní	východní	k2eAgFnSc4d1
Kordilleru	Kordiller	k1gInSc6
navazuje	navazovat	k5eAaImIp3nS
severovýchodním	severovýchodní	k2eAgInSc7d1
směrem	směr	k1gInSc7
venezuelská	venezuelský	k2eAgFnSc1d1
Cordillera	Cordiller	k1gMnSc2
de	de	k?
Mérida	Mérid	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
horské	horský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
okolo	okolo	k7c2
500	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
jádro	jádro	k1gNnSc1
pohoří	pohoří	k1gNnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
krystalických	krystalický	k2eAgFnPc2d1
žul	žula	k1gFnPc2
a	a	k8xC
rul	rula	k1gFnPc2
<g/>
,	,	kIx,
povrch	povrch	k6eAd1wR
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
tvořený	tvořený	k2eAgInSc4d1
sedimenty	sediment	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Sierry	Sierra	k1gFnSc2
Nevady	Nevada	k1gFnSc2
de	de	k?
Mérida	Mérida	k1gFnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Venezuely	Venezuela	k1gFnSc2
Pico	Pico	k1gNnSc1
Bolívar	Bolívar	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
007	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekvádorské	ekvádorský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
V	v	k7c6
Ekvádoru	Ekvádor	k1gInSc6
se	se	k3xPyFc4
Andy	Anda	k1gFnPc1
zužují	zužovat	k5eAaImIp3nP
na	na	k7c4
šířku	šířka	k1gFnSc4
150	#num#	k4
až	až	k9
200	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
je	on	k3xPp3gInPc4
dva	dva	k4xCgInPc4
paralelní	paralelní	k2eAgInPc4d1
vysoké	vysoký	k2eAgInPc4d1
horské	horský	k2eAgInPc4d1
hřebeny	hřeben	k1gInPc4
<g/>
:	:	kIx,
Západní	západní	k2eAgInSc1d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Occidental	Occidental	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Východní	východní	k2eAgFnPc1d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Oriental	Oriental	k1gMnSc1
<g/>
)	)	kIx)
s	s	k7c7
délkou	délka	k1gFnSc7
okolo	okolo	k7c2
650	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekvádorská	ekvádorský	k2eAgFnSc1d1
Západní	západní	k2eAgFnSc1d1
Kordillera	Kordiller	k1gMnSc4
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
<g/>
,	,	kIx,
rozčleněná	rozčleněný	k2eAgFnSc1d1
řadou	řada	k1gFnSc7
údolí	údolí	k1gNnSc2
s	s	k7c7
řekami	řeka	k1gFnPc7
směřujícími	směřující	k2eAgFnPc7d1
do	do	k7c2
Tichého	Tichého	k2eAgInSc2d1
Oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Kordilleře	Kordillera	k1gFnSc6
leží	ležet	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
ekvádorských	ekvádorský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
Chimborazo	Chimboraza	k1gFnSc5
(	(	kIx(
<g/>
6	#num#	k4
263	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInPc1d1
Kordillera	Kordiller	k1gMnSc4
je	být	k5eAaImIp3nS
širší	široký	k2eAgFnSc1d2
<g/>
,	,	kIx,
méně	málo	k6eAd2
častá	častý	k2eAgNnPc1d1
říční	říční	k2eAgNnPc1d1
údolí	údolí	k1gNnPc1
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
hlubokých	hluboký	k2eAgInPc2d1
kaňonů	kaňon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hlavním	hlavní	k2eAgMnPc3d1
vrcholům	vrchol	k1gInPc3
tohoto	tento	k3xDgNnSc2
horského	horský	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
náleží	náležet	k5eAaImIp3nP
vulkány	vulkán	k1gInPc1
Cotopaxi	Cotopaxe	k1gFnSc6
(	(	kIx(
<g/>
5	#num#	k4
897	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Cayambe	Cayamb	k1gInSc5
(	(	kIx(
<g/>
5	#num#	k4
790	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Illimani	Illiman	k1gMnPc1
</s>
<s>
Chimborazo	Chimboraza	k1gFnSc5
</s>
<s>
Střední	střední	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Peruánské	peruánský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Peruánské	peruánský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnPc1d3
části	část	k1gFnPc1
severních	severní	k2eAgFnPc2d1
a	a	k8xC
středních	střední	k2eAgFnPc2d1
And	Anda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
39	#num#	k4
vrcholů	vrchol	k1gInPc2
peruánských	peruánský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
překračuje	překračovat	k5eAaImIp3nS
6	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
174	#num#	k4
vrcholů	vrchol	k1gInPc2
je	být	k5eAaImIp3nS
vyšších	vysoký	k2eAgInPc6d2
než	než	k8xS
5	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
And	Anda	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
Peru	Peru	k1gNnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
700	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
je	být	k5eAaImIp3nS
šířka	šířka	k1gFnSc1
pohoří	pohoří	k1gNnPc2
150	#num#	k4
kilometrů	kilometr	k1gInPc2
na	na	k7c6
jihu	jih	k1gInSc6
Peru	Peru	k1gNnSc2
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
na	na	k7c4
450	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
peruánských	peruánský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
horská	horský	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
<g/>
:	:	kIx,
Západní	západní	k2eAgNnSc4d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Occidental	Occidental	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Střední	střední	k1gMnSc1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Central	Central	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
Východní	východní	k2eAgFnPc1d1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
Oriental	Oriental	k1gMnSc1
<g/>
)	)	kIx)
oddělené	oddělený	k2eAgFnSc2d1
hlubokými	hluboký	k2eAgNnPc7d1
údolími	údolí	k1gNnPc7
řek	řek	k1gMnSc1
Marañ	Marañ	k1gMnSc1
<g/>
,	,	kIx,
Huallaga	Huallaga	k1gFnSc1
a	a	k8xC
Ucayali	Ucayali	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Peru	Peru	k1gNnSc2
pokračují	pokračovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
dvě	dva	k4xCgNnPc4
horská	horský	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
<g/>
:	:	kIx,
Západní	západní	k2eAgNnSc4d1
Kordillera	Kordiller	k1gMnSc2
a	a	k8xC
Východní	východní	k2eAgMnSc1d1
Kordillera	Kordiller	k1gMnSc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
And	Anda	k1gFnPc2
Altiplano	Altiplana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
peruánských	peruánský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
severozápadě	severozápad	k1gInSc6
ležící	ležící	k2eAgInSc4d1
Huascarán	Huascarán	k1gInSc4
(	(	kIx(
<g/>
6	#num#	k4
768	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bolívijské	Bolívijský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Bolívie	Bolívie	k1gFnSc2
se	se	k3xPyFc4
horská	horský	k2eAgFnSc1d1
pásma	pásmo	k1gNnSc2
Západní	západní	k2eAgFnPc1d1
Kordillery	Kordillery	k1gFnPc1
a	a	k8xC
Východní	východní	k2eAgFnPc1d1
Kordillery	Kordillery	k1gFnPc1
rozestupují	rozestupovat	k5eAaImIp3nP
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
400	#num#	k4
až	až	k9
600	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostora	k1gFnPc2
mezi	mezi	k7c7
horskými	horský	k2eAgNnPc7d1
pásmy	pásmo	k1gNnPc7
vyplňuje	vyplňovat	k5eAaImIp3nS
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
Altiplano	Altiplana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
Východní	východní	k2eAgFnPc1d1
Kordillery	Kordillery	k1gFnPc1
<g/>
,	,	kIx,
východně	východně	k6eAd1
od	od	k7c2
jezera	jezero	k1gNnSc2
Titicaca	Titicacum	k1gNnSc2
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
silně	silně	k6eAd1
zaledněné	zaledněný	k2eAgInPc1d1
a	a	k8xC
zasněžené	zasněžený	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
pohoří	pohořet	k5eAaPmIp3nP
Cordillera	Cordiller	k1gMnSc4
Real	Real	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
druhý	druhý	k4xOgInSc4
a	a	k8xC
čtvrtý	čtvrtý	k4xOgInSc4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Bolívie	Bolívie	k1gFnSc2
Illimani	Illiman	k1gMnPc1
(	(	kIx(
<g/>
6	#num#	k4
439	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Illampu	Illampa	k1gFnSc4
(	(	kIx(
<g/>
6	#num#	k4
368	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Západní	západní	k2eAgFnSc6d1
Kordilleře	Kordillera	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Bolívie	Bolívie	k1gFnSc2
<g/>
,	,	kIx,
podél	podél	k7c2
hranice	hranice	k1gFnSc2
se	s	k7c7
severním	severní	k2eAgNnSc7d1
Chile	Chile	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
pak	pak	k6eAd1
nachází	nacházet	k5eAaImIp3nS
vulkanické	vulkanický	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
z	z	k7c2
konce	konec	k1gInSc2
třetihor	třetihory	k1gFnPc2
a	a	k8xC
počátku	počátek	k1gInSc6
čtvrtohor	čtvrtohory	k1gFnPc2
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Bolívijských	Bolívijský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
vulkánem	vulkán	k1gInSc7
Sajama	Sajamum	k1gNnSc2
(	(	kIx(
<g/>
6	#num#	k4
542	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc2d1
Andy	Anda	k1gFnSc2
</s>
<s>
Jižní	jižní	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
Chilsko-argentinské	chilsko-argentinský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
nejsevernější	severní	k2eAgFnSc1d3
část	část	k1gFnSc1
Chile	Chile	k1gNnSc2
s	s	k7c7
řadou	řada	k1gFnSc7
činných	činný	k2eAgInPc2d1
vulkánů	vulkán	k1gInPc2
<g/>
:	:	kIx,
Guallatiri	Guallatiri	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
071	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Parinacota	Parinacota	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
384	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pomerape	Pomerap	k1gMnSc5
(	(	kIx(
<g/>
6	#num#	k4
282	#num#	k4
m	m	kA
<g/>
)	)	kIx)
přináleží	přináležet	k5eAaImIp3nS
ke	k	k7c3
Středním	střední	k2eAgFnPc3d1
Andám	Anda	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chilsko-argentinské	chilsko-argentinský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
navazují	navazovat	k5eAaImIp3nP
na	na	k7c4
Střední	střední	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
v	v	k7c6
místě	místo	k1gNnSc6
hraničního	hraniční	k2eAgNnSc2d1
trojmezí	trojmezí	k1gNnSc2
mezi	mezi	k7c7
Argentinou	Argentina	k1gFnSc7
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc7
a	a	k8xC
Bolívií	Bolívie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chilsko-argentinské	chilsko-argentinský	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
jsou	být	k5eAaImIp3nP
užší	úzký	k2eAgMnPc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
tvoří	tvořit	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
část	část	k1gFnSc4
And	Anda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
Chilsko-argentinských	chilsko-argentinský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
horských	horský	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
složených	složený	k2eAgNnPc2d1
z	z	k7c2
různých	různý	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
Pobřežní	pobřežní	k1gMnSc1
Kordillera	Kordiller	k1gMnSc2
(	(	kIx(
<g/>
Cordillera	Cordiller	k1gMnSc2
de	de	k?
la	la	k1gNnSc1
Costa	Cost	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Chilské	chilský	k2eAgNnSc1d1
podélné	podélný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
za	za	k7c7
nímž	jenž	k3xRgInSc7
vystupuje	vystupovat	k5eAaImIp3nS
Chilská	chilský	k2eAgFnSc1d1
Prekordillera	Prekordiller	k1gMnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
část	část	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc4
Cordillera	Cordiller	k1gMnSc2
Domeyko	Domeyko	k1gNnSc4
(	(	kIx(
<g/>
4	#num#	k4
890	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
ležící	ležící	k2eAgFnSc1d1
Preandská	Preandský	k2eAgFnSc1d1
sníženina	sníženina	k1gFnSc1
odděluje	oddělovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
a	a	k8xC
hlavní	hlavní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
And	Anda	k1gFnPc2
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
na	na	k7c4
chilsko-argentincké	chilsko-argentincké	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
činný	činný	k2eAgInSc1d1
vulkán	vulkán	k1gInSc1
Lascar	Lascara	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
592	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
vyhaslý	vyhaslý	k2eAgInSc4d1
vulkán	vulkán	k1gInSc4
Llullaillaco	Llullaillaco	k6eAd1
(	(	kIx(
<g/>
6	#num#	k4
739	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
argentinské	argentinský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
leží	ležet	k5eAaImIp3nS
vulkán	vulkán	k1gInSc1
Antofalla	Antofallo	k1gNnSc2
(	(	kIx(
<g/>
6	#num#	k4
409	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
chilsko-argentinské	chilsko-argentinský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
Socompa	Socompa	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
051	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
pásmo	pásmo	k1gNnSc1
Puny	Pun	k1gMnPc4
<g/>
,	,	kIx,
zvlněné	zvlněný	k2eAgFnPc1d1
náhorní	náhorní	k2eAgFnPc1d1
plošiny	plošina	k1gFnPc1
s	s	k7c7
řadou	řada	k1gFnSc7
bezodtokých	bezodtoký	k2eAgFnPc2d1
pánví	pánev	k1gFnPc2
se	s	k7c7
solnými	solný	k2eAgNnPc7d1
jezery	jezero	k1gNnPc7
a	a	k8xC
močály	močál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Punu	Pun	k1gMnSc6
na	na	k7c6
východě	východ	k1gInSc6
navazují	navazovat	k5eAaImIp3nP
třetihorní	třetihorní	k2eAgNnPc4d1
horská	horský	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
v	v	k7c6
okolí	okolí	k1gNnSc6
měst	město	k1gNnPc2
Salta	salto	k1gNnSc2
a	a	k8xC
Tucumán	Tucumán	k2eAgInSc1d1
a	a	k8xC
dále	daleko	k6eAd2
východně	východně	k6eAd1
ležící	ležící	k2eAgFnSc1d1
sedimentární	sedimentární	k2eAgFnSc1d1
Subandská	Subandský	k2eAgFnSc1d1
Sierra	Sierra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ojos	Ojos	k1gInSc1
del	del	k?
Salado	Salada	k1gFnSc5
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
And	Anda	k1gFnPc2
</s>
<s>
Střední	střední	k2eAgFnPc1d1
Chilsko-argentinské	chilsko-argentinský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
od	od	k7c2
jižního	jižní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
pouště	poušť	k1gFnSc2
Atacama	Atacam	k1gMnSc2
až	až	k9
k	k	k7c3
řece	řeka	k1gFnSc3
Bío-Bío	Bío-Bío	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
Chile	Chile	k1gNnSc2
a	a	k8xC
pramenům	pramen	k1gInPc3
řeky	řeka	k1gFnSc2
Agrio	Agrio	k6eAd1
v	v	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
jsou	být	k5eAaImIp3nP
Andy	Anda	k1gFnPc1
rozděleny	rozdělit	k5eAaPmNgFnP
na	na	k7c4
dvě	dva	k4xCgNnPc4
horská	horský	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Chilsko-argentinské	chilsko-argentinský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
tzv.	tzv.	kA
Hlavní	hlavní	k2eAgMnSc1d1
Kordillera	Kordiller	k1gMnSc4
<g/>
,	,	kIx,
Cordillera	Cordiller	k1gMnSc4
Principal	Principal	k1gMnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
tvoří	tvořit	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgNnPc4d3
horstva	horstvo	k1gNnPc4
And	Anda	k1gFnPc2
a	a	k8xC
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
zde	zde	k6eAd1
leží	ležet	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
vulkán	vulkán	k1gInSc4
světa	svět	k1gInSc2
Ojos	Ojos	k1gInSc1
del	del	k?
Salado	Salada	k1gFnSc5
(	(	kIx(
<g/>
6	#num#	k4
893	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pátá	pátý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Ameriky	Amerika	k1gFnSc2
Cerro	Cerro	k1gNnSc4
Bonete	Bone	k1gNnSc2
(	(	kIx(
<g/>
6	#num#	k4
759	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mercedario	Mercedario	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
720	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
And	Anda	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Ameriky	Amerika	k1gFnPc1
Aconcagua	Aconcagu	k2eAgFnSc1d1
a	a	k8xC
jižně	jižně	k6eAd1
od	od	k7c2
ní	on	k3xPp3gFnSc2
šestá	šestý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
And	Anda	k1gFnPc2
Tupungato	Tupungat	k2eAgNnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
565	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hlavní	hlavní	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
Aconcaguy	Aconcagua	k1gFnSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
13	#num#	k4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
hlavního	hlavní	k2eAgInSc2d1
hřebene	hřeben	k1gInSc2
Cordillery	Cordiller	k1gInPc4
Principal	Principal	k1gMnPc2
na	na	k7c6
hranicích	hranice	k1gFnPc6
Chile	Chile	k1gNnSc2
a	a	k8xC
Argentiny	Argentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolí	údolí	k1gNnSc1
řek	řeka	k1gFnPc2
Río	Río	k1gFnSc2
de	de	k?
los	los	k1gInSc1
Patos	patos	k1gInSc1
a	a	k8xC
Tunuyán	Tunuyán	k1gInSc1
pak	pak	k6eAd1
oddělují	oddělovat	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgInSc4d3
hlavní	hlavní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
And	Anda	k1gFnPc2
od	od	k7c2
východně	východně	k6eAd1
níže	nízce	k6eAd2
položeného	položený	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
Cordillera	Cordiller	k1gMnSc2
Frontal	Frontal	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
950	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
Chilsko-argentinských	chilsko-argentinský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Patagonské	patagonský	k2eAgFnSc2d1
Andy	Anda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tvoří	tvořit	k5eAaImIp3nS
je	on	k3xPp3gNnPc4
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc4
horské	horský	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
směrem	směr	k1gInSc7
k	k	k7c3
jihu	jih	k1gInSc3
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnými	činný	k2eAgInPc7d1
vulkány	vulkán	k1gInPc7
v	v	k7c6
Patagonských	patagonský	k2eAgFnPc6d1
Andách	Anda	k1gFnPc6
jsou	být	k5eAaImIp3nP
Llaima	Llaima	k1gNnSc4
(	(	kIx(
<g/>
3	#num#	k4
125	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Osorno	Osorno	k1gNnSc1
(	(	kIx(
<g/>
2	#num#	k4
652	#num#	k4
m	m	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Corcovado	Corcovada	k1gFnSc5
(	(	kIx(
<g/>
2	#num#	k4
300	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejvíce	nejvíce	k6eAd1,k6eAd3
známým	známý	k2eAgInPc3d1
vrcholům	vrchol	k1gInPc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
náleží	náležet	k5eAaImIp3nS
Lanín	Lanín	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
776	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tronador	Tronador	k1gMnSc1
(	(	kIx(
3491	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Patagonských	patagonský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
Monte	Mont	k1gInSc5
San	San	k1gMnSc7
Valentín	Valentín	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
058	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patagonské	patagonský	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
zaledněny	zaledněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
chilsko-argentinských	chilsko-argentinský	k2eAgFnPc2d1
And	Anda	k1gFnPc2
je	být	k5eAaImIp3nS
sněžná	sněžný	k2eAgFnSc1d1
čára	čára	k1gFnSc1
nad	nad	k7c7
4	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nejjižnější	jižní	k2eAgFnSc6d3
části	část	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Ohňové	ohňový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
okolo	okolo	k7c2
600	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sajama	Sajama	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Bolívie	Bolívie	k1gFnSc2
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3
vrcholy	vrchol	k1gInPc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
šestitisícovek	šestitisícovka	k1gFnPc2
v	v	k7c6
Andách	Anda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Andách	Anda	k1gFnPc6
je	být	k5eAaImIp3nS
91	#num#	k4
šestitisícovek	šestitisícovka	k1gFnPc2
<g/>
,	,	kIx,
14	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
výšku	výška	k1gFnSc4
nad	nad	k7c7
6	#num#	k4
500	#num#	k4
m.	m.	k?
Nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
And	Anda	k1gFnPc2
je	být	k5eAaImIp3nS
Aconcagua	Aconcagua	k1gFnSc1
v	v	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
,	,	kIx,
vysoká	vysoký	k2eAgFnSc1d1
6	#num#	k4
959	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgInPc4d1
významné	významný	k2eAgInPc4d1
vrcholy	vrchol	k1gInPc4
náleží	náležet	k5eAaImIp3nS
<g/>
:	:	kIx,
Ojos	Ojos	k1gInSc1
del	del	k?
Salado	Salada	k1gFnSc5
(	(	kIx(
<g/>
6	#num#	k4
893	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Chile	Chile	k1gNnSc2
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc1d3
vulkán	vulkán	k1gInSc1
světa	svět	k1gInSc2
<g/>
;	;	kIx,
Huascarán	Huascarán	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
768	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Peru	Peru	k1gNnSc2
<g/>
;	;	kIx,
Sajama	Sajama	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
542	#num#	k4
m	m	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Bolívie	Bolívie	k1gFnSc1
<g/>
;	;	kIx,
Yerupajá	Yerupajá	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
635	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chimborazo	Chimboraza	k1gFnSc5
(	(	kIx(
<g/>
6	#num#	k4
310	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Ekvádoru	Ekvádor	k1gInSc2
<g/>
;	;	kIx,
Huayna	Huayna	k1gFnSc1
Potosí	Potosý	k2eAgMnPc1d1
(	(	kIx(
<g/>
6	#num#	k4
088	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Bolívie	Bolívie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alpamayo	Alpamayo	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
947	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cotopaxi	Cotopaxe	k1gFnSc4
(	(	kIx(
<g/>
5	#num#	k4
897	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Ekvádor	Ekvádor	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Fitz	Fitz	k1gMnSc1
Roy	Roy	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
405	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Cerro	Cerro	k1gNnSc1
Torre	torr	k1gInSc5
(	(	kIx(
<g/>
3	#num#	k4
133	#num#	k4
m	m	kA
<g/>
)	)	kIx)
v	v	k7c6
Patagonských	patagonský	k2eAgFnPc6d1
Andách	Anda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Bolivijské	bolivijský	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
</s>
<s>
Cerro	Cerro	k6eAd1
Chaltén	Chaltén	k1gInSc1
</s>
<s>
Horský	horský	k2eAgInSc1d1
pás	pás	k1gInSc1
And	Anda	k1gFnPc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
formovat	formovat	k5eAaImF
v	v	k7c6
období	období	k1gNnSc6
křídy	křída	k1gFnSc2
před	před	k7c7
138	#num#	k4
milióny	milión	k4xCgInPc7
až	až	k8xS
65	#num#	k4
milióny	milión	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
zdvihu	zdvih	k1gInSc2
a	a	k8xC
vrásnění	vrásnění	k1gNnSc2
usazených	usazený	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
bylo	být	k5eAaImAgNnS
pomalé	pomalý	k2eAgNnSc1d1
nasouvání	nasouvání	k1gNnSc1
tichomořské	tichomořský	k2eAgFnSc2d1
kry	kra	k1gFnSc2
pod	pod	k7c4
jihoamerickou	jihoamerický	k2eAgFnSc4d1
pevninskou	pevninský	k2eAgFnSc4d1
kru	kra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
pohoří	pohoří	k1gNnPc2
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgInPc1d1
tektonické	tektonický	k2eAgInPc1d1
poruchy	poruch	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
příčinou	příčina	k1gFnSc7
zemětřesení	zemětřesení	k1gNnSc2
a	a	k8xC
vulkanické	vulkanický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
And	Anda	k1gFnPc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
procesy	proces	k1gInPc7
deskové	deskový	k2eAgFnSc2d1
tektoniky	tektonika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
následek	následek	k1gInSc4
subdukce	subdukce	k1gFnSc2
desky	deska	k1gFnSc2
Nazca	Nazc	k1gInSc2
pod	pod	k7c4
jihoamerickou	jihoamerický	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
mezi	mezi	k7c7
deskami	deska	k1gFnPc7
je	být	k5eAaImIp3nS
v	v	k7c6
oceánu	oceán	k1gInSc6
tvořena	tvořit	k5eAaImNgFnS
Peruánsko-chilským	peruánsko-chilský	k2eAgInSc7d1
příkopem	příkop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3
andské	andský	k2eAgFnPc1d1
horniny	hornina	k1gFnPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
prekambria	prekambrium	k1gNnSc2
a	a	k8xC
nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
Chile	Chile	k1gNnSc6
a	a	k8xC
Argentině	Argentina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
například	například	k6eAd1
slabě	slabě	k6eAd1
metamorfované	metamorfovaný	k2eAgInPc1d1
flyšové	flyšový	k2eAgInPc1d1
sedimenty	sediment	k1gInPc1
jednotky	jednotka	k1gFnSc2
Puncoviscana	Puncoviscana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
kambria	kambrium	k1gNnSc2
až	až	k8xS
triasu	trias	k1gInSc2
na	na	k7c4
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Gondwany	Gondwana	k1gFnSc2
narazilo	narazit	k5eAaPmAgNnS
několik	několik	k4yIc1
mikrokontinentů	mikrokontinent	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
její	její	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
srážky	srážka	k1gFnPc1
byly	být	k5eAaImAgFnP
spojeny	spojit	k5eAaPmNgFnP
s	s	k7c7
výrazným	výrazný	k2eAgInSc7d1
vulkanismem	vulkanismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
jury	jura	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
And	Anda	k1gFnPc2
vyvinulo	vyvinout	k5eAaPmAgNnS
několik	několik	k4yIc1
sedimentačních	sedimentační	k2eAgFnPc2d1
pánví	pánev	k1gFnPc2
a	a	k8xC
vulkanických	vulkanický	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
různého	různý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Severních	severní	k2eAgFnPc6d1
Andách	Anda	k1gFnPc6
vznikl	vzniknout	k5eAaPmAgInS
ve	v	k7c6
formaci	formace	k1gFnSc6
La	la	k1gNnSc2
Negra	negr	k1gInSc2
vulkanický	vulkanický	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
a	a	k8xC
předoblouková	předobloukový	k2eAgFnSc1d1
Tarapacská	Tarapacský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
titónu	titón	k1gInSc2
po	po	k7c6
hoteriv	hoteriv	k6eAd1
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
pánev	pánev	k1gFnSc1
zaplavena	zaplavit	k5eAaPmNgFnS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
během	během	k7c2
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
ji	on	k3xPp3gFnSc4
ovlivňoval	ovlivňovat	k5eAaImAgInS
okolní	okolní	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
a	a	k8xC
usazovaly	usazovat	k5eAaImAgFnP
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
zejména	zejména	k9
červené	červený	k2eAgInPc4d1
kontinentální	kontinentální	k2eAgInPc4d1
sedimenty	sediment	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
křídy	křída	k1gFnSc2
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
pánev	pánev	k1gFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
a	a	k8xC
z	z	k7c2
hornin	hornina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
vytlačeny	vytlačit	k5eAaPmNgFnP
z	z	k7c2
tohoto	tento	k3xDgInSc2
prostoru	prostor	k1gInSc2
vznikla	vzniknout	k5eAaPmAgFnS
tzv.	tzv.	kA
Proto	proto	k8xC
Cordillera	Cordiller	k1gMnSc2
Domeyko	Domeyko	k1gNnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
období	období	k1gNnSc6
výrazně	výrazně	k6eAd1
destruovala	destruovat	k5eAaImAgFnS
eroze	eroze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
po	po	k7c4
eocén	eocén	k1gInSc4
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
subdukce	subdukce	k1gFnSc2
desky	deska	k1gFnSc2
Nazca	Nazca	k1gMnSc1
vulkanismus	vulkanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
Oligocénu	oligocén	k1gInSc2
vystřídaly	vystřídat	k5eAaPmAgFnP
intruze	intruze	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Chile	Chile	k1gNnSc6
významná	významný	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
měděných	měděný	k2eAgFnPc2d1
–	–	k?
porfyrových	porfyrový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Subdukce	Subdukce	k1gFnSc1
desky	deska	k1gFnSc2
Nazca	Nazc	k2eAgFnSc1d1
způsobila	způsobit	k5eAaPmAgFnS
i	i	k9
vznik	vznik	k1gInSc4
Atacamského	Atacamský	k2eAgInSc2d1
příkopu	příkop	k1gInSc2
<g/>
,	,	kIx,
významného	významný	k2eAgInSc2d1
zlomu	zlom	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
postihl	postihnout	k5eAaPmAgInS
Pobřežní	pobřežní	k2eAgFnSc4d1
kordilieru	kordiliera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
prostoru	prostor	k1gInSc6
následně	následně	k6eAd1
během	během	k7c2
jury	jura	k1gFnSc2
a	a	k8xC
rané	raný	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
mohl	moct	k5eAaImAgInS
vystoupit	vystoupit	k5eAaPmF
granitoidní	granitoidní	k2eAgInSc1d1
pobřežní	pobřežní	k2eAgInSc1d1
batolit	batolit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
centrálním	centrální	k2eAgInSc6d1
segmentu	segment	k1gInSc6
And	Anda	k1gFnPc2
během	během	k7c2
vrchní	vrchní	k2eAgFnSc2d1
jury	jura	k1gFnSc2
a	a	k8xC
spodní	spodní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgInP
paralelní	paralelní	k2eAgInPc1d1
vulkanické	vulkanický	k2eAgInPc1d1
oblouky	oblouk	k1gInPc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
menší	malý	k2eAgFnPc1d2
zaobloukové	zaobloukový	k2eAgFnPc1d1
a	a	k8xC
okrajové	okrajový	k2eAgFnPc1d1
pánve	pánev	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kůru	kůr	k1gInSc2
také	také	k6eAd1
postihly	postihnout	k5eAaPmAgFnP
několik	několik	k4yIc4
intruzí	intruze	k1gFnPc2
hlubinných	hlubinný	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
v	v	k7c6
juře	jura	k1gFnSc6
zasáhla	zasáhnout	k5eAaPmAgFnS
silná	silný	k2eAgFnSc1d1
transgrese	transgrese	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
vznik	vznik	k1gInSc4
platformy	platforma	k1gFnSc2
Aconcagua	Aconcagu	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
z	z	k7c2
oblasti	oblast	k1gFnSc2
moře	moře	k1gNnSc1
ustoupilo	ustoupit	k5eAaPmAgNnS
a	a	k8xC
následoval	následovat	k5eAaImAgInS
intenzivní	intenzivní	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vrchní	vrchní	k2eAgFnSc6d1
křídě	křída	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
tektonického	tektonický	k2eAgInSc2d1
režimu	režim	k1gInSc2
z	z	k7c2
klidné	klidný	k2eAgFnSc2d1
subdukce	subdukce	k1gFnSc2
Mariánského	mariánský	k2eAgInSc2d1
typu	typ	k1gInSc2
na	na	k7c4
napjatý	napjatý	k2eAgInSc4d1
Chilský	chilský	k2eAgInSc4d1
typ	typ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavřely	uzavřít	k5eAaPmAgFnP
se	se	k3xPyFc4
starší	starý	k2eAgFnPc1d2
sedimentační	sedimentační	k2eAgFnPc1d1
pánve	pánev	k1gFnPc1
a	a	k8xC
vulkanismus	vulkanismus	k1gInSc1
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgInS
více	hodně	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
uzavření	uzavření	k1gNnSc1
způsobil	způsobit	k5eAaPmAgInS
vznik	vznik	k1gInSc4
Aconcaguského	Aconcaguský	k2eAgNnSc2d1
vrásovo-násunového	vrásovo-násunový	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
,	,	kIx,
podél	podél	k7c2
východní	východní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Cordillera	Cordiller	k1gMnSc2
Principal	Principal	k1gMnSc2
ve	v	k7c6
vrchní	vrchní	k2eAgFnSc6d1
křídě	křída	k1gFnSc6
až	až	k9
miocénu	miocén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvartérní	kvartérní	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
už	už	k6eAd1
neproběhl	proběhnout	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Jižních	jižní	k2eAgFnPc6d1
Andách	Anda	k1gFnPc6
od	od	k7c2
jury	jura	k1gFnSc2
po	po	k7c4
kvartér	kvartér	k1gInSc4
probíhal	probíhat	k5eAaImAgInS
neustálý	neustálý	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
Cordillera	Cordiller	k1gMnSc2
Principal	Principal	k1gMnSc2
a	a	k8xC
předobloukové	předobloukový	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
Neuquen	Neuquna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
plnila	plnit	k5eAaImAgFnS
mořskými	mořský	k2eAgFnPc7d1
i	i	k8xC
kontinentálními	kontinentální	k2eAgFnPc7d1
uloženinami	uloženina	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
vhodné	vhodný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
akumulaci	akumulace	k1gFnSc4
uhlovodíků	uhlovodík	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Andách	Anda	k1gFnPc6
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
pokračující	pokračující	k2eAgFnSc2d1
subdukce	subdukce	k1gFnSc2
desky	deska	k1gFnSc2
Nazca	Nazc	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
mnoho	mnoho	k4c1
sopek	sopka	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Cotopaxi	Cotopaxe	k1gFnSc3
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvyšších	vysoký	k2eAgFnPc2d3
sopek	sopka	k1gFnPc2
současného	současný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Klima	klima	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
Andách	Anda	k1gFnPc6
významně	významně	k6eAd1
liší	lišit	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
<g/>
,	,	kIx,
blízkosti	blízkost	k1gFnSc6
moře	moře	k1gNnSc2
a	a	k8xC
šířky	šířka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižních	jižní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
And	Anda	k1gFnPc2
často	často	k6eAd1
prší	pršet	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
chladněji	chladně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrální	centrální	k2eAgFnPc4d1
Andy	Anda	k1gFnPc4
mají	mít	k5eAaImIp3nP
suché	suchý	k2eAgNnSc1d1
klima	klima	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
mají	mít	k5eAaImIp3nP
vlhké	vlhký	k2eAgNnSc4d1
a	a	k8xC
teplé	teplý	k2eAgNnSc4d1
klima	klima	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
dosahuje	dosahovat	k5eAaImIp3nS
průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
18	#num#	k4
°	°	k?
<g/>
C.	C.	kA
V	v	k7c6
důsledku	důsledek	k1gInSc6
velkých	velký	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
nadmořských	nadmořský	k2eAgFnPc2d1
výšek	výška	k1gFnPc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
deštné	deštný	k2eAgInPc1d1
lesy	les	k1gInPc1
nacházet	nacházet	k5eAaImF
pouze	pouze	k6eAd1
několik	několik	k4yIc4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
zasněžených	zasněžený	k2eAgInPc2d1
štítů	štít	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hory	hora	k1gFnSc2
mají	mít	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
nejen	nejen	k6eAd1
na	na	k7c4
teplotu	teplota	k1gFnSc4
ale	ale	k8xC
i	i	k9
srážky	srážka	k1gFnPc1
v	v	k7c6
blízkých	blízký	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněžná	sněžný	k2eAgFnSc1d1
čára	čára	k1gFnSc1
je	být	k5eAaImIp3nS
pohyblivá	pohyblivý	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
4	#num#	k4
500	#num#	k4
do	do	k7c2
4800	#num#	k4
m	m	kA
v	v	k7c6
tropickém	tropický	k2eAgInSc6d1
Ekvádoru	Ekvádor	k1gInSc6
<g/>
,	,	kIx,
Kolumbii	Kolumbie	k1gFnSc6
<g/>
,	,	kIx,
Venezuele	Venezuela	k1gFnSc6
a	a	k8xC
Peru	Peru	k1gNnSc6
<g/>
,	,	kIx,
4	#num#	k4
800	#num#	k4
až	až	k9
5200	#num#	k4
m	m	kA
v	v	k7c6
sušších	suchý	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
Peru	Peru	k1gNnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
opět	opět	k6eAd1
klesá	klesat	k5eAaImIp3nS
do	do	k7c2
4500	#num#	k4
m	m	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
Aconcagua	Aconcagu	k1gInSc2
(	(	kIx(
<g/>
32	#num#	k4
<g/>
°	°	k?
<g/>
j.š.	j.š.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2000	#num#	k4
m	m	kA
dosahuje	dosahovat	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
40	#num#	k4
<g/>
°	°	k?
j.š.	j.š.	k?
<g/>
,	,	kIx,
500	#num#	k4
m	m	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
50	#num#	k4
<g/>
°	°	k?
j.	j.	k?
š.	š.	k?
a	a	k8xC
od	od	k7c2
55	#num#	k4
<g/>
°	°	k?
j.š.	j.š.	k?
několik	několik	k4yIc4
"	"	kIx"
<g/>
horských	horský	k2eAgInPc2d1
<g/>
"	"	kIx"
ledovců	ledovec	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
mořské	mořský	k2eAgFnPc4d1
hladiny	hladina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Agroekologické	Agroekologický	k2eAgFnPc1d1
zóny	zóna	k1gFnPc1
</s>
<s>
Andy	Anda	k1gFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
řadu	řada	k1gFnSc4
agroekologických	agroekologický	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
horizontální	horizontální	k2eAgFnSc4d1
a	a	k8xC
vertikální	vertikální	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horizontální	horizontální	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
pro	pro	k7c4
každý	každý	k3xTgInSc4
stát	stát	k1gInSc4
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
vertikální	vertikální	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
všude	všude	k6eAd1
stejné	stejný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
Chala	Chala	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
Costa	Costa	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Yunga	Yung	k1gMnSc4
<g/>
,	,	kIx,
Quechua	Quechuus	k1gMnSc4
<g/>
,	,	kIx,
Suni	Sune	k1gFnSc4
<g/>
,	,	kIx,
Puna	Pun	k1gMnSc4
(	(	kIx(
<g/>
neboli	neboli	k8xC
Jalca	Jalca	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Janca	Janca	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
Cordillera	Cordiller	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Universum	universum	k1gNnSc4
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
738	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1061	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
194	#num#	k4
<g/>
,	,	kIx,
195	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Merriam-Webster	Merriam-Webster	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Geographical	Geographical	k1gFnSc7
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Springfield	Springfield	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
U.	U.	kA
S.	S.	kA
A.	A.	kA
<g/>
:	:	kIx,
Merriam-Webster	Merriam-Webster	k1gMnSc1
<g/>
,	,	kIx,
Incorporporated	Incorporporated	k1gMnSc1
<g/>
,	,	kIx,
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
1361	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87779	#num#	k4
<g/>
-	-	kIx~
<g/>
546	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
47	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Der	drát	k5eAaImRp2nS
Neue	Neue	k1gNnSc4
Weltatlas	Weltatlas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
München	München	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Kunth	Kunth	k1gMnSc1
Gmbh	Gmbh	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
248	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8166	#num#	k4
<g/>
-	-	kIx~
<g/>
2248	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
150	#num#	k4
<g/>
,	,	kIx,
151	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AJACOPA	AJACOPA	kA
<g/>
,	,	kIx,
Teofilo	Teofila	k1gFnSc5
Laime	Laim	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diccionario	Diccionario	k1gNnSc1
biligüe	biligüe	k1gFnPc2
<g/>
,	,	kIx,
Quechua-Castellano	Quechua-Castellana	k1gFnSc5
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc7
Paz	Paz	k1gFnPc2
<g/>
,	,	kIx,
Bolívie	Bolívie	k1gFnSc1
<g/>
:	:	kIx,
Enero	Enero	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
215	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
ŠLÉGL	ŠLÉGL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
a	a	k8xC
KOLEKTIV	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
pohoří	pohořet	k5eAaPmIp3nS
<g/>
,	,	kIx,
Amerika	Amerika	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Balios	Balios	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
176	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
960	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Andy	Anda	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
138	#num#	k4
-	-	kIx~
157	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Andy	Anda	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Andy	Anda	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Andy	Anda	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Jeden	jeden	k4xCgInSc1
neúspěšný	úspěšný	k2eNgInSc1d1
pokus	pokus	k1gInSc1
o	o	k7c4
výstup	výstup	k1gInSc4
na	na	k7c6
Aconcaguu	Aconcaguus	k1gInSc6
</s>
<s>
Jihoamerické	jihoamerický	k2eAgFnPc1d1
Andy	Anda	k1gFnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
zhroutit	zhroutit	k5eAaPmF
a	a	k8xC
zmizet	zmizet	k5eAaPmF
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
134021	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4001912-3	4001912-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
316874805	#num#	k4
</s>
