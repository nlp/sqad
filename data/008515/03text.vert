<p>
<s>
Akvamarín	akvamarín	k1gInSc1	akvamarín
je	být	k5eAaImIp3nS	být
šesterečný	šesterečný	k2eAgInSc4d1	šesterečný
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
hlinitokřemičitan	hlinitokřemičitan	k1gInSc4	hlinitokřemičitan
berylnatý	berylnatý	k2eAgInSc4d1	berylnatý
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc4d1	chemický
vzorec	vzorec	k1gInSc4	vzorec
Be	Be	k1gFnPc2	Be
<g/>
3	[number]	k4	3
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
Si	se	k3xPyFc3	se
<g/>
6	[number]	k4	6
<g/>
O	o	k7c4	o
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
minerálu	minerál	k1gInSc2	minerál
je	být	k5eAaImIp3nS	být
odvozený	odvozený	k2eAgInSc1d1	odvozený
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
aqua	aqu	k1gInSc2	aqu
marina	marina	k1gFnSc1	marina
(	(	kIx(	(
<g/>
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Akvamarín	akvamarín	k1gInSc1	akvamarín
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
berylu	beryl	k1gInSc2	beryl
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
modrou	modrý	k2eAgFnSc4d1	modrá
až	až	k8xS	až
zelenomodrou	zelenomodrý	k2eAgFnSc4d1	zelenomodrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
7,5	[number]	k4	7,5
–	–	k?	–
8	[number]	k4	8
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,6	[number]	k4	2,6
–	–	k?	–
2,8	[number]	k4	2,8
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
,	,	kIx,	,
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
<g/>
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
<g/>
,	,	kIx,	,
<g/>
vzácně	vzácně	k6eAd1	vzácně
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k8xS	až
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Be	Be	k1gFnSc1	Be
5,03	[number]	k4	5,03
%	%	kIx~	%
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
10,04	[number]	k4	10,04
%	%	kIx~	%
<g/>
,	,	kIx,	,
Si	se	k3xPyFc3	se
31,35	[number]	k4	31,35
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
53,58	[number]	k4	53,58
%	%	kIx~	%
<g/>
,	,	kIx,	,
příměsi	příměs	k1gFnSc3	příměs
Fe	Fe	k1gMnPc2	Fe
<g/>
,	,	kIx,	,
Ti	ten	k3xDgMnPc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
HF	HF	kA	HF
<g/>
,	,	kIx,	,
před	před	k7c7	před
dmuchavkou	dmuchavka	k1gFnSc7	dmuchavka
se	se	k3xPyFc4	se
netaví	tavit	k5eNaImIp3nS	tavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
jako	jako	k8xC	jako
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
fasetové	fasetový	k2eAgInPc1d1	fasetový
brusy	brus	k1gInPc1	brus
<g/>
,	,	kIx,	,
kabošony	kabošon	k1gInPc1	kabošon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náleziště	Náleziště	k1gNnSc2	Náleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Minas	Minasa	k1gFnPc2	Minasa
Gerais	Gerais	k1gFnSc7	Gerais
(	(	kIx(	(
<g/>
u	u	k7c2	u
města	město	k1gNnSc2	město
Marambaia	Marambaius	k1gMnSc4	Marambaius
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
nalezen	nalezen	k2eAgInSc1d1	nalezen
největší	veliký	k2eAgInSc1d3	veliký
akvamarín	akvamarín	k1gInSc1	akvamarín
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
vážil	vážit	k5eAaImAgMnS	vážit
110	[number]	k4	110
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Espírito	Espírita	k1gFnSc5	Espírita
Santo	Santo	k1gNnSc1	Santo
a	a	k8xC	a
Bahia	Bahia	k1gFnSc1	Bahia
</s>
</p>
<p>
<s>
USA	USA	kA	USA
-	-	kIx~	-
Mt	Mt	k1gMnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Antero	Antero	k1gNnSc1	Antero
v	v	k7c6	v
Sawatch	Sawat	k1gFnPc6	Sawat
Range	Rang	k1gInSc2	Rang
(	(	kIx(	(
<g/>
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnPc1	lanko
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
</s>
</p>
<p>
<s>
Malawi	Malawi	k1gNnSc1	Malawi
</s>
</p>
<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
</s>
</p>
<p>
<s>
Keňa	Keňa	k1gFnSc1	Keňa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akvamarín	akvamarín	k1gInSc1	akvamarín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
