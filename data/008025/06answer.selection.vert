<s>
Golf	golf	k1gInSc1	golf
je	být	k5eAaImIp3nS	být
venkovní	venkovní	k2eAgInSc1d1	venkovní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
samotný	samotný	k2eAgMnSc1d1	samotný
hráč	hráč	k1gMnSc1	hráč
nebo	nebo	k8xC	nebo
malá	malý	k2eAgFnSc1d1	malá
skupinka	skupinka	k1gFnSc1	skupinka
hraje	hrát	k5eAaImIp3nS	hrát
malým	malý	k2eAgInSc7d1	malý
golfovým	golfový	k2eAgInSc7d1	golfový
míčkem	míček	k1gInSc7	míček
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
přitom	přitom	k6eAd1	přitom
různé	různý	k2eAgFnPc4d1	různá
hole	hole	k1gFnPc4	hole
<g/>
.	.	kIx.	.
</s>
