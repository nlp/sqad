<s>
Golf	golf	k1gInSc1	golf
je	být	k5eAaImIp3nS	být
venkovní	venkovní	k2eAgInSc1d1	venkovní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
samotný	samotný	k2eAgMnSc1d1	samotný
hráč	hráč	k1gMnSc1	hráč
nebo	nebo	k8xC	nebo
malá	malý	k2eAgFnSc1d1	malá
skupinka	skupinka	k1gFnSc1	skupinka
hraje	hrát	k5eAaImIp3nS	hrát
malým	malý	k2eAgInSc7d1	malý
golfovým	golfový	k2eAgInSc7d1	golfový
míčkem	míček	k1gInSc7	míček
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
přitom	přitom	k6eAd1	přitom
různé	různý	k2eAgFnPc4d1	různá
hole	hole	k1gFnPc4	hole
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
golfu	golf	k1gInSc2	golf
definují	definovat	k5eAaBmIp3nP	definovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
golfová	golfový	k2eAgFnSc1d1	golfová
"	"	kIx"	"
<g/>
hra	hra	k1gFnSc1	hra
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
hraní	hraní	k1gNnSc2	hraní
míčkem	míček	k1gInSc7	míček
z	z	k7c2	z
odpaliště	odpaliště	k1gNnSc2	odpaliště
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
ranou	rána	k1gFnSc7	rána
nebo	nebo	k8xC	nebo
postupnými	postupný	k2eAgFnPc7d1	postupná
ranami	rána	k1gFnPc7	rána
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
golf	golf	k1gInSc4	golf
tak	tak	k9	tak
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
hrán	hrát	k5eAaImNgInS	hrát
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
po	po	k7c6	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
golfu	golf	k1gInSc2	golf
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
už	už	k9	už
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
Čchuej-wan	Čchuejana	k1gFnPc2	Čchuej-wana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
hrán	hrát	k5eAaImNgMnS	hrát
výhradně	výhradně	k6eAd1	výhradně
šlechtou	šlechta	k1gFnSc7	šlechta
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Golf	golf	k1gInSc1	golf
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
olympijské	olympijský	k2eAgInPc4d1	olympijský
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
programu	program	k1gInSc2	program
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1900	[number]	k4	1900
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1904	[number]	k4	1904
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
112	[number]	k4	112
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2016	[number]	k4	2016
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
provedl	provést	k5eAaPmAgMnS	provést
americký	americký	k2eAgMnSc1d1	americký
astronaut	astronaut	k1gMnSc1	astronaut
Alan	Alan	k1gMnSc1	Alan
Shepard	Shepard	k1gMnSc1	Shepard
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
golfový	golfový	k2eAgInSc4d1	golfový
odpal	odpal	k1gInSc4	odpal
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
sport	sport	k1gInSc4	sport
stal	stát	k5eAaPmAgMnS	stát
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zahrán	zahrát	k5eAaPmNgInS	zahrát
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zatímco	zatímco	k8xS	zatímco
moderní	moderní	k2eAgInSc1d1	moderní
golf	golf	k1gInSc1	golf
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
pradávný	pradávný	k2eAgInSc1d1	pradávný
původ	původ	k1gInSc1	původ
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
uvádí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
sport	sport	k1gInSc1	sport
prvotně	prvotně	k6eAd1	prvotně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
hry	hra	k1gFnSc2	hra
Paganica	Paganic	k1gInSc2	Paganic
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
hráči	hráč	k1gMnPc1	hráč
používali	používat	k5eAaImAgMnP	používat
ohnutou	ohnutý	k2eAgFnSc4d1	ohnutá
tyč	tyč	k1gFnSc4	tyč
a	a	k8xC	a
strefovali	strefovat	k5eAaImAgMnP	strefovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vycpaného	vycpaný	k2eAgInSc2d1	vycpaný
koženého	kožený	k2eAgInSc2d1	kožený
míče	míč	k1gInSc2	míč
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Paganica	Paganica	k1gFnSc1	Paganica
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
když	když	k8xS	když
Římané	Říman	k1gMnPc1	Říman
dobyli	dobýt	k5eAaPmAgMnP	dobýt
většinu	většina	k1gFnSc4	většina
kontinentu	kontinent	k1gInSc2	kontinent
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prvního	první	k4xOgNnSc2	první
století	století	k1gNnSc2	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c4	v
moderní	moderní	k2eAgFnSc4d1	moderní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
citují	citovat	k5eAaBmIp3nP	citovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předchůdcem	předchůdce	k1gMnSc7	předchůdce
golfu	golf	k1gInSc2	golf
byla	být	k5eAaImAgFnS	být
čínská	čínský	k2eAgFnSc1d1	čínská
hra	hra	k1gFnSc1	hra
čchuej-wan	čchuejan	k1gMnSc1	čchuej-wan
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Chui	Chui	k1gNnSc1	Chui
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
překvapující	překvapující	k2eAgFnSc1d1	překvapující
a	a	k8xC	a
"	"	kIx"	"
<g/>
wan	wan	k?	wan
<g/>
"	"	kIx"	"
malé	malý	k2eAgFnPc1d1	malá
kuličky	kulička	k1gFnPc1	kulička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hraná	hraný	k2eAgFnSc1d1	hraná
mezi	mezi	k7c4	mezi
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
golfu	golf	k1gInSc6	golf
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1457	[number]	k4	1457
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jakub	Jakub	k1gMnSc1	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k1gInSc1	skotský
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
zakázal	zakázat	k5eAaPmAgMnS	zakázat
jako	jako	k9	jako
nevítané	vítaný	k2eNgNnSc4d1	nevítané
rozptýlení	rozptýlení	k1gNnSc4	rozptýlení
od	od	k7c2	od
učení	učení	k1gNnSc2	učení
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k1gInSc1	skotský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
zákaz	zákaz	k1gInSc1	zákaz
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
golfistou	golfista	k1gMnSc7	golfista
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
Old	Olda	k1gFnPc2	Olda
Course	Course	k1gFnSc2	Course
v	v	k7c6	v
St.	st.	kA	st.
Andrews	Andrews	k1gInSc4	Andrews
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
v	v	k7c6	v
St.	st.	kA	st.
Andrews	Andrewsa	k1gFnPc2	Andrewsa
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
standardní	standardní	k2eAgNnSc4d1	standardní
18	[number]	k4	18
<g/>
jamkové	jamkový	k2eAgFnPc4d1	jamková
hřiště	hřiště	k1gNnPc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
golfová	golfový	k2eAgNnPc1d1	golfové
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1744	[number]	k4	1744
pro	pro	k7c4	pro
Company	Compan	k1gMnPc4	Compan
of	of	k?	of
Gentlemen	Gentlemen	k2eAgInSc1d1	Gentlemen
Golfers	Golfers	k1gInSc1	Golfers
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
The	The	k1gFnSc4	The
Honourable	Honourable	k1gFnSc2	Honourable
Company	Compana	k1gFnSc2	Compana
of	of	k?	of
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
Golfers	Golfersa	k1gFnPc2	Golfersa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
skotském	skotský	k2eAgNnSc6d1	skotské
městě	město	k1gNnSc6	město
Leith	Leitha	k1gFnPc2	Leitha
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
nejstarším	starý	k2eAgInSc7d3	nejstarší
golfovým	golfový	k2eAgInSc7d1	golfový
turnajem	turnaj	k1gInSc7	turnaj
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc1	The
Open	Open	k1gMnSc1	Open
Championship	Championship	k1gMnSc1	Championship
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
hrál	hrát	k5eAaImAgInS	hrát
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
v	v	k7c4	v
golf	golf	k1gInSc4	golf
clubu	club	k1gInSc2	club
Prestwick	Prestwicka	k1gFnPc2	Prestwicka
ve	v	k7c6	v
skotském	skotský	k2eAgInSc6d1	skotský
Ayrshiru	Ayrshir	k1gInSc6	Ayrshir
<g/>
.	.	kIx.	.
</s>
<s>
Golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
9	[number]	k4	9
nebo	nebo	k8xC	nebo
18	[number]	k4	18
jamek	jamka	k1gFnPc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
jamka	jamka	k1gFnSc1	jamka
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
samotné	samotný	k2eAgFnSc2d1	samotná
prohlubně	prohlubeň	k1gFnSc2	prohlubeň
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
míček	míček	k1gInSc1	míček
dostat	dostat	k5eAaPmF	dostat
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
hřiště	hřiště	k1gNnSc2	hřiště
od	od	k7c2	od
daného	daný	k2eAgNnSc2d1	dané
odpaliště	odpaliště	k1gNnSc2	odpaliště
až	až	k9	až
k	k	k7c3	k
jemu	on	k3xPp3gInSc3	on
příslušnému	příslušný	k2eAgInSc3d1	příslušný
greenu	green	k1gInSc3	green
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
vyvrtána	vyvrtán	k2eAgFnSc1d1	vyvrtána
vlastní	vlastní	k2eAgFnSc1d1	vlastní
jamka	jamka	k1gFnSc1	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jamek	jamka	k1gFnPc2	jamka
má	mít	k5eAaImIp3nS	mít
green	greet	k5eAaImNgMnS	greet
od	od	k7c2	od
odpaliště	odpaliště	k1gNnSc2	odpaliště
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
první	první	k4xOgFnSc7	první
ranou	rána	k1gFnSc7	rána
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
greenem	greeno	k1gNnSc7	greeno
a	a	k8xC	a
odpalištěm	odpaliště	k1gNnSc7	odpaliště
nízko	nízko	k6eAd1	nízko
sekaný	sekaný	k2eAgInSc1d1	sekaný
pruh	pruh	k1gInSc1	pruh
zvaný	zvaný	k2eAgInSc1d1	zvaný
fairway	fairway	k1gInPc4	fairway
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
hráč	hráč	k1gMnSc1	hráč
postupuje	postupovat	k5eAaImIp3nS	postupovat
k	k	k7c3	k
jamce	jamka	k1gFnSc3	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
fairwayí	fairway	k1gFnSc7	fairway
a	a	k8xC	a
greenem	green	k1gInSc7	green
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
foregreen	foregreen	k1gInSc1	foregreen
<g/>
,	,	kIx,	,
tráva	tráva	k1gFnSc1	tráva
sekaná	sekaná	k1gFnSc1	sekaná
mírně	mírně	k6eAd1	mírně
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
green	green	k1gInSc4	green
<g/>
.	.	kIx.	.
</s>
<s>
Nesekaná	sekaný	k2eNgFnSc1d1	nesekaná
tráva	tráva	k1gFnSc1	tráva
okolo	okolo	k7c2	okolo
fairwaye	fairway	k1gInSc2	fairway
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rough	rough	k1gInSc1	rough
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
další	další	k2eAgFnSc4d1	další
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
urychlení	urychlení	k1gNnSc1	urychlení
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
tráva	tráva	k1gFnSc1	tráva
bezprostředně	bezprostředně	k6eAd1	bezprostředně
vedle	vedle	k7c2	vedle
ferveje	fervej	k1gInSc2	fervej
seká	sekat	k5eAaImIp3nS	sekat
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
semirough	semirougha	k1gFnPc2	semirougha
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c4	o
něco	něco	k3yInSc4	něco
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
fairway	fairwaa	k1gFnPc1	fairwaa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
níž	nízce	k6eAd2	nízce
než	než	k8xS	než
rough	rough	k1gInSc4	rough
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jamkách	jamka	k1gFnPc6	jamka
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
různé	různý	k2eAgFnPc1d1	různá
překážky	překážka	k1gFnPc1	překážka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
bunker	bunker	k1gInSc4	bunker
(	(	kIx(	(
<g/>
písečná	písečný	k2eAgFnSc1d1	písečná
překážka	překážka	k1gFnSc1	překážka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vodní	vodní	k2eAgFnSc1d1	vodní
překážka	překážka	k1gFnSc1	překážka
(	(	kIx(	(
<g/>
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
překážky	překážka	k1gFnPc1	překážka
pravidla	pravidlo	k1gNnSc2	pravidlo
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
příčné	příčný	k2eAgInPc4d1	příčný
a	a	k8xC	a
podélné	podélný	k2eAgMnPc4d1	podélný
-	-	kIx~	-
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
postup	postup	k1gInSc1	postup
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
míček	míček	k1gInSc1	míček
skončí	skončit	k5eAaPmIp3nS	skončit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
překážkách	překážka	k1gFnPc6	překážka
platí	platit	k5eAaImIp3nP	platit
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
znesnadňují	znesnadňovat	k5eAaImIp3nP	znesnadňovat
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
bunkru	bunkr	k1gInSc6	bunkr
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
odpalem	odpal	k1gInSc7	odpal
nesmí	smět	k5eNaImIp3nS	smět
dotknout	dotknout	k5eAaPmF	dotknout
holí	hole	k1gFnSc7	hole
písku	písek	k1gInSc2	písek
ani	ani	k8xC	ani
nesmí	smět	k5eNaImIp3nS	smět
provést	provést	k5eAaPmF	provést
cvičný	cvičný	k2eAgInSc4d1	cvičný
švih	švih	k1gInSc4	švih
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míč	míč	k1gInSc1	míč
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
označené	označený	k2eAgNnSc1d1	označené
jako	jako	k8xC	jako
vodní	vodní	k2eAgFnSc1d1	vodní
překážka	překážka	k1gFnSc1	překážka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jej	on	k3xPp3gMnSc4	on
hráč	hráč	k1gMnSc1	hráč
hrát	hrát	k5eAaImF	hrát
podle	podle	k7c2	podle
podobných	podobný	k2eAgNnPc2d1	podobné
pravidel	pravidlo	k1gNnPc2	pravidlo
jako	jako	k8xC	jako
v	v	k7c6	v
bunkeru	bunker	k1gInSc6	bunker
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
připočtením	připočtení	k1gNnSc7	připočtení
trestné	trestný	k2eAgFnSc2d1	trestná
rány	rána	k1gFnSc2	rána
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
mimo	mimo	k7c4	mimo
překážku	překážka	k1gFnSc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míček	míček	k1gInSc1	míček
skončí	skončit	k5eAaPmIp3nS	skončit
za	za	k7c7	za
označenými	označený	k2eAgFnPc7d1	označená
hranicemi	hranice	k1gFnPc7	hranice
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
daného	daný	k2eAgNnSc2d1	dané
místa	místo	k1gNnSc2	místo
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
hřištích	hřiště	k1gNnPc6	hřiště
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tzv.	tzv.	kA	tzv.
biozóny	biozón	k1gInPc4	biozón
-	-	kIx~	-
označené	označený	k2eAgFnPc4d1	označená
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
vstupovat	vstupovat	k5eAaImF	vstupovat
nebo	nebo	k8xC	nebo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Tráva	tráva	k1gFnSc1	tráva
na	na	k7c6	na
greenu	green	k1gInSc6	green
je	být	k5eAaImIp3nS	být
sekaná	sekaná	k1gFnSc1	sekaná
velice	velice	k6eAd1	velice
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
jen	jen	k9	jen
na	na	k7c4	na
3	[number]	k4	3
mm	mm	kA	mm
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
koulet	koulet	k5eAaImF	koulet
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ráně	Rána	k1gFnSc3	Rána
na	na	k7c4	na
greenu	greena	k1gFnSc4	greena
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
míček	míček	k1gInSc1	míček
nevznese	vznést	k5eNaPmIp3nS	vznést
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
putt	putt	k5eAaPmF	putt
(	(	kIx(	(
<g/>
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
patuje	patovat	k5eAaPmIp3nS	patovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Greeny	Greena	k1gFnPc1	Greena
bývají	bývat	k5eAaImIp3nP	bývat
různě	různě	k6eAd1	různě
zvlněné	zvlněný	k2eAgFnPc1d1	zvlněná
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
správné	správný	k2eAgNnSc1d1	správné
míření	míření	k1gNnSc1	míření
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
jamka	jamka	k1gFnSc1	jamka
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
10,8	[number]	k4	10,8
cm	cm	kA	cm
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
mít	mít	k5eAaImF	mít
hloubku	hloubka	k1gFnSc4	hloubka
10,16	[number]	k4	10,16
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vyztužuje	vyztužovat	k5eAaImIp3nS	vyztužovat
se	se	k3xPyFc4	se
plastovou	plastový	k2eAgFnSc7d1	plastová
vložkou	vložka	k1gFnSc7	vložka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
lze	lze	k6eAd1	lze
zapíchnout	zapíchnout	k5eAaPmF	zapíchnout
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tyč	tyč	k1gFnSc4	tyč
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
viditelnost	viditelnost	k1gFnSc4	viditelnost
jamky	jamka	k1gFnSc2	jamka
i	i	k9	i
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
jamky	jamka	k1gFnSc2	jamka
na	na	k7c6	na
greenu	green	k1gInSc6	green
není	být	k5eNaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
den	den	k1gInSc4	den
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
koly	kolo	k1gNnPc7	kolo
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
podle	podle	k7c2	podle
pin	pin	k1gInSc4	pin
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
jamka	jamka	k1gFnSc1	jamka
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
parem	pare	k1gNnSc7	pare
<g/>
.	.	kIx.	.
</s>
<s>
Par	para	k1gFnPc2	para
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
obtížnosti	obtížnost	k1gFnSc6	obtížnost
každé	každý	k3xTgFnSc2	každý
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Par	para	k1gFnPc2	para
je	být	k5eAaImIp3nS	být
teoretický	teoretický	k2eAgInSc1d1	teoretický
počet	počet	k1gInSc1	počet
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
by	by	kYmCp3nS	by
zkušený	zkušený	k2eAgMnSc1d1	zkušený
golfista	golfista	k1gMnSc1	golfista
měl	mít	k5eAaImAgMnS	mít
jamku	jamka	k1gFnSc4	jamka
zahrát	zahrát	k5eAaPmF	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Golfista	golfista	k1gMnSc1	golfista
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
rány	rána	k1gFnPc4	rána
pod	pod	k7c7	pod
par	para	k1gFnPc2	para
měl	mít	k5eAaImAgMnS	mít
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
green	green	k1gInSc4	green
a	a	k8xC	a
pak	pak	k6eAd1	pak
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
paty	pat	k1gInPc4	pat
na	na	k7c4	na
dosažení	dosažení	k1gNnSc4	dosažení
paru	para	k1gFnSc4	para
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
dosažení	dosažení	k1gNnSc1	dosažení
greenu	green	k1gInSc2	green
v	v	k7c6	v
regulaci	regulace	k1gFnSc6	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
hodnota	hodnota	k1gFnSc1	hodnota
paru	para	k1gFnSc4	para
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jamky	jamka	k1gFnPc4	jamka
vzdálené	vzdálený	k2eAgFnPc4d1	vzdálená
do	do	k7c2	do
224	[number]	k4	224
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
odpaliště	odpaliště	k1gNnSc2	odpaliště
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jamek	jamka	k1gFnPc2	jamka
par	para	k1gFnPc2	para
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
225	[number]	k4	225
do	do	k7c2	do
434	[number]	k4	434
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
pro	pro	k7c4	pro
par	para	k1gFnPc2	para
5	[number]	k4	5
více	hodně	k6eAd2	hodně
než	než	k8xS	než
435	[number]	k4	435
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Např.	např.	kA	např.
u	u	k7c2	u
krátkých	krátká	k1gFnPc2	krátká
zalomených	zalomený	k2eAgFnPc2d1	zalomená
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
dogleg	dogleg	k1gInSc4	dogleg
<g/>
)	)	kIx)	)
jamek	jamka	k1gFnPc2	jamka
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hrát	hrát	k5eAaImF	hrát
první	první	k4xOgFnSc7	první
ranou	rána	k1gFnSc7	rána
na	na	k7c4	na
green	green	k1gInSc4	green
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
je	být	k5eAaImIp3nS	být
jamka	jamka	k1gFnSc1	jamka
od	od	k7c2	od
odpaliště	odpaliště	k1gNnSc2	odpaliště
méně	málo	k6eAd2	málo
než	než	k8xS	než
224	[number]	k4	224
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
a	a	k8xC	a
paru	para	k1gFnSc4	para
tedy	tedy	k9	tedy
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
parů	par	k1gInPc2	par
všech	všecek	k3xTgFnPc2	všecek
jamek	jamka	k1gFnPc2	jamka
daného	daný	k2eAgNnSc2d1	dané
hřiště	hřiště	k1gNnSc2	hřiště
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
par	para	k1gFnPc2	para
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
hodnota	hodnota	k1gFnSc1	hodnota
paru	para	k1gFnSc4	para
osmnáctijamkového	osmnáctijamkový	k2eAgNnSc2d1	osmnáctijamkové
hřiště	hřiště	k1gNnSc2	hřiště
je	být	k5eAaImIp3nS	být
72	[number]	k4	72
<g/>
;	;	kIx,	;
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
průměrný	průměrný	k2eAgInSc1d1	průměrný
par	para	k1gFnPc2	para
jamky	jamka	k1gFnSc2	jamka
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zahrajeme	zahrát	k5eAaPmIp1nP	zahrát
na	na	k7c4	na
paru	para	k1gFnSc4	para
4	[number]	k4	4
čtyři	čtyři	k4xCgFnPc4	čtyři
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
dosažený	dosažený	k2eAgInSc4d1	dosažený
výsledek	výsledek	k1gInSc4	výsledek
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tři	tři	k4xCgNnPc4	tři
<g/>
,	,	kIx,	,
tak	tak	k9	tak
birdie	birdie	k1gFnSc1	birdie
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
tak	tak	k9	tak
eagle	eagle	k6eAd1	eagle
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
jamky	jamka	k1gFnPc4	jamka
první	první	k4xOgFnSc7	první
ranou	rána	k1gFnSc7	rána
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
odpaliště	odpaliště	k1gNnSc2	odpaliště
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
hole-in-one	holenn	k1gMnSc5	hole-in-on
<g/>
.	.	kIx.	.
</s>
<s>
Přehledná	přehledný	k2eAgFnSc1d1	přehledná
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Muži	muž	k1gMnPc1	muž
<g/>
:	:	kIx,	:
Par	para	k1gFnPc2	para
3	[number]	k4	3
–	–	k?	–
230	[number]	k4	230
m	m	kA	m
(	(	kIx(	(
<g/>
250	[number]	k4	250
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
a	a	k8xC	a
méně	málo	k6eAd2	málo
Par	para	k1gFnPc2	para
4	[number]	k4	4
–	–	k?	–
231	[number]	k4	231
až	až	k9	až
410	[number]	k4	410
m	m	kA	m
(	(	kIx(	(
<g/>
450	[number]	k4	450
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
Par	para	k1gFnPc2	para
5	[number]	k4	5
–	–	k?	–
411	[number]	k4	411
až	až	k9	až
630	[number]	k4	630
m	m	kA	m
(	(	kIx(	(
<g/>
451	[number]	k4	451
až	až	k9	až
690	[number]	k4	690
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
Par	para	k1gFnPc2	para
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
631	[number]	k4	631
m	m	kA	m
(	(	kIx(	(
<g/>
691	[number]	k4	691
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
a	a	k8xC	a
více	hodně	k6eAd2	hodně
Ženy	žena	k1gFnSc2	žena
<g/>
:	:	kIx,	:
Par	para	k1gFnPc2	para
3	[number]	k4	3
–	–	k?	–
190	[number]	k4	190
m	m	kA	m
(	(	kIx(	(
<g/>
210	[number]	k4	210
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
a	a	k8xC	a
méně	málo	k6eAd2	málo
Par	para	k1gFnPc2	para
4	[number]	k4	4
–	–	k?	–
191	[number]	k4	191
až	až	k9	až
370	[number]	k4	370
m	m	kA	m
(	(	kIx(	(
<g/>
211	[number]	k4	211
až	až	k9	až
400	[number]	k4	400
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
Par	para	k1gFnPc2	para
5	[number]	k4	5
–	–	k?	–
371	[number]	k4	371
až	až	k9	až
526	[number]	k4	526
m	m	kA	m
(	(	kIx(	(
<g/>
401	[number]	k4	401
až	až	k9	až
575	[number]	k4	575
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
Par	para	k1gFnPc2	para
6	[number]	k4	6
–	–	k?	–
527	[number]	k4	527
m	m	kA	m
(	(	kIx(	(
<g/>
575	[number]	k4	575
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
a	a	k8xC	a
více	hodně	k6eAd2	hodně
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
hřišť	hřiště	k1gNnPc2	hřiště
bývají	bývat	k5eAaImIp3nP	bývat
další	další	k2eAgNnPc1d1	další
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
herního	herní	k2eAgInSc2d1	herní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tréninkové	tréninkový	k2eAgInPc1d1	tréninkový
prostory	prostor	k1gInPc1	prostor
<g/>
:	:	kIx,	:
cvičný	cvičný	k2eAgInSc1d1	cvičný
green	green	k1gInSc1	green
<g/>
,	,	kIx,	,
bunker	bunker	k1gInSc1	bunker
a	a	k8xC	a
tréninková	tréninkový	k2eAgFnSc1d1	tréninková
louka	louka	k1gFnSc1	louka
(	(	kIx(	(
<g/>
driving	driving	k1gInSc1	driving
range	rang	k1gInSc2	rang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1	Zelené
trávníky	trávník	k1gInPc1	trávník
golfových	golfový	k2eAgNnPc2d1	golfové
hřišť	hřiště	k1gNnPc2	hřiště
mohou	moct	k5eAaImIp3nP	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sport	sport	k1gInSc4	sport
"	"	kIx"	"
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
každodenní	každodenní	k2eAgFnSc6d1	každodenní
údržbě	údržba	k1gFnSc6	údržba
hřišť	hřiště	k1gNnPc2	hřiště
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
pesticidy	pesticid	k1gInPc1	pesticid
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
herbicidy	herbicid	k1gInPc1	herbicid
(	(	kIx(	(
<g/>
chemikálie	chemikálie	k1gFnPc1	chemikálie
likvidující	likvidující	k2eAgInPc4d1	likvidující
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
druhy	druh	k1gInPc4	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
insekticidy	insekticid	k1gInPc1	insekticid
(	(	kIx(	(
<g/>
likvidace	likvidace	k1gFnSc1	likvidace
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
)	)	kIx)	)
a	a	k8xC	a
fungicidy	fungicid	k1gInPc1	fungicid
(	(	kIx(	(
<g/>
k	k	k7c3	k
eliminaci	eliminace	k1gFnSc3	eliminace
plísní	plíseň	k1gFnPc2	plíseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
používání	používání	k1gNnSc1	používání
pesticidů	pesticid	k1gInPc2	pesticid
obavy	obava	k1gFnSc2	obava
úřadů	úřad	k1gInPc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
greenkeeperů	greenkeeper	k1gMnPc2	greenkeeper
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
stanovována	stanovován	k2eAgNnPc4d1	stanovováno
závazná	závazný	k2eAgNnPc4d1	závazné
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
minimalizaci	minimalizace	k1gFnSc4	minimalizace
rizik	riziko	k1gNnPc2	riziko
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
je	být	k5eAaImIp3nS	být
také	také	k9	také
spotřeba	spotřeba	k1gFnSc1	spotřeba
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
zavlažování	zavlažování	k1gNnSc4	zavlažování
trávníku	trávník	k1gInSc2	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
náročné	náročný	k2eAgFnPc1d1	náročná
travnaté	travnatý	k2eAgFnPc1d1	travnatá
plochy	plocha	k1gFnPc1	plocha
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
zdroj	zdroj	k1gInSc4	zdroj
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jezírka	jezírko	k1gNnSc2	jezírko
s	s	k7c7	s
užitkovou	užitkový	k2eAgFnSc7d1	užitková
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
při	při	k7c6	při
deštivých	deštivý	k2eAgInPc6d1	deštivý
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Zalévání	zalévání	k1gNnSc1	zalévání
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
vodovodního	vodovodní	k2eAgInSc2d1	vodovodní
řadu	řad	k1gInSc2	řad
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
a	a	k8xC	a
s	s	k7c7	s
nevhodnými	vhodný	k2eNgFnPc7d1	nevhodná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zavlažování	zavlažování	k1gNnSc3	zavlažování
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgInPc1d1	speciální
postřikovače	postřikovač	k1gInPc1	postřikovač
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
nastavit	nastavit	k5eAaPmF	nastavit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
délce	délka	k1gFnSc6	délka
postřiku	postřik	k1gInSc2	postřik
a	a	k8xC	a
průtoku	průtok	k1gInSc2	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgInSc1d1	provozní
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
zavlažovacím	zavlažovací	k2eAgInSc6d1	zavlažovací
systému	systém	k1gInSc6	systém
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
8	[number]	k4	8
bar	bar	k1gInSc4	bar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
golfu	golf	k1gInSc6	golf
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
typy	typ	k1gInPc1	typ
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
známější	známý	k2eAgFnSc7d2	známější
a	a	k8xC	a
častěji	často	k6eAd2	často
hranou	hrana	k1gFnSc7	hrana
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
jamky	jamka	k1gFnPc4	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
rány	rána	k1gFnPc4	rána
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
určitého	určitý	k2eAgInSc2d1	určitý
počtu	počet	k1gInSc2	počet
jamek	jamka	k1gFnPc2	jamka
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
18	[number]	k4	18
jamek	jamka	k1gFnPc2	jamka
<g/>
,	,	kIx,	,
na	na	k7c6	na
devítijamkových	devítijamkový	k2eAgNnPc6d1	devítijamkové
hřištích	hřiště	k1gNnPc6	hřiště
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
chodí	chodit	k5eAaImIp3nS	chodit
dvakrát	dvakrát	k6eAd1	dvakrát
9	[number]	k4	9
jamek	jamka	k1gFnPc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
jamce	jamka	k1gFnSc6	jamka
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
odpalem	odpal	k1gInSc7	odpal
z	z	k7c2	z
odpaliště	odpaliště	k1gNnSc2	odpaliště
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	se	k3xPyFc4	se
dalšími	další	k2eAgFnPc7d1	další
ranami	rána	k1gFnPc7	rána
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
míček	míček	k1gInSc1	míček
neskončí	skončit	k5eNaPmIp3nS	skončit
v	v	k7c6	v
jamce	jamka	k1gFnSc6	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
použít	použít	k5eAaPmF	použít
co	co	k3yInSc4	co
nejméně	málo	k6eAd3	málo
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
jamky	jamka	k1gFnPc4	jamka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jamkovka	jamkovka	k1gFnSc1	jamkovka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
rány	rána	k1gFnPc4	rána
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
hrají	hrát	k5eAaImIp3nP	hrát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
jamce	jamka	k1gFnSc6	jamka
ten	ten	k3xDgMnSc1	ten
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
zahraje	zahrát	k5eAaPmIp3nS	zahrát
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
obdrží	obdržet	k5eAaPmIp3nS	obdržet
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
zahrají	zahrát	k5eAaPmIp3nP	zahrát
danou	daný	k2eAgFnSc4d1	daná
jamku	jamka	k1gFnSc4	jamka
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
úderů	úder	k1gInPc2	úder
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
půl	půl	k1xP	půl
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
ten	ten	k3xDgMnSc1	ten
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
odehraném	odehraný	k2eAgNnSc6d1	odehrané
kole	kolo	k1gNnSc6	kolo
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
může	moct	k5eAaImIp3nS	moct
skončit	skončit	k5eAaPmF	skončit
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
všech	všecek	k3xTgFnPc2	všecek
jamek	jamka	k1gFnPc2	jamka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
již	již	k9	již
na	na	k7c6	na
zbývajících	zbývající	k2eAgFnPc6d1	zbývající
jamkách	jamka	k1gFnPc6	jamka
nemůže	moct	k5eNaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
předstihnout	předstihnout	k5eAaPmF	předstihnout
svého	svůj	k3xOyFgMnSc4	svůj
protihráče	protihráč	k1gMnSc4	protihráč
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k9	i
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
18	[number]	k4	18
jamek	jamka	k1gFnPc2	jamka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
odehraných	odehraný	k2eAgNnPc6d1	odehrané
18	[number]	k4	18
jamkách	jamka	k1gFnPc6	jamka
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
určit	určit	k5eAaPmF	určit
vítěze	vítěz	k1gMnPc4	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmové	týmový	k2eAgFnSc6d1	týmová
soutěži	soutěž	k1gFnSc6	soutěž
Ryder	Ryder	k1gInSc4	Ryder
Cup	cup	k1gInSc1	cup
se	se	k3xPyFc4	se
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
nepokračuje	pokračovat	k5eNaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
získává	získávat	k5eAaImIp3nS	získávat
půl	půl	k1xP	půl
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
chodí	chodit	k5eAaImIp3nP	chodit
po	po	k7c6	po
hřišti	hřiště	k1gNnSc6	hřiště
po	po	k7c6	po
skupinkách	skupinka	k1gFnPc6	skupinka
zvaných	zvaný	k2eAgFnPc2d1	zvaná
flight	flighta	k1gFnPc2	flighta
<g/>
.	.	kIx.	.
</s>
<s>
Flight	Flight	k1gInSc4	Flight
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeden	jeden	k4xCgMnSc1	jeden
až	až	k8xS	až
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
doprovázeni	doprovázen	k2eAgMnPc1d1	doprovázen
nosiči	nosič	k1gMnPc1	nosič
(	(	kIx(	(
<g/>
caddie	caddie	k1gFnSc1	caddie
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kedy	keda	k1gFnPc4	keda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nosí	nosit	k5eAaImIp3nP	nosit
hole	hole	k6eAd1	hole
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
hráči	hráč	k1gMnPc1	hráč
(	(	kIx(	(
<g/>
radí	radit	k5eAaImIp3nP	radit
kam	kam	k6eAd1	kam
zahrát	zahrát	k5eAaPmF	zahrát
míček	míček	k1gInSc4	míček
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
hůl	hůl	k1gFnSc4	hůl
vzít	vzít	k5eAaPmF	vzít
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
hraje	hrát	k5eAaImIp3nS	hrát
svým	svůj	k3xOyFgInSc7	svůj
míčem	míč	k1gInSc7	míč
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
jiných	jiný	k2eAgFnPc2d1	jiná
variant	varianta	k1gFnPc2	varianta
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
od	od	k7c2	od
odpaliště	odpaliště	k1gNnSc2	odpaliště
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
z	z	k7c2	z
odpaliště	odpaliště	k1gNnSc2	odpaliště
hraje	hrát	k5eAaImIp3nS	hrát
první	první	k4xOgNnSc4	první
ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
míč	míč	k1gInSc4	míč
je	být	k5eAaImIp3nS	být
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
jamky	jamka	k1gFnSc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
odpališti	odpaliště	k1gNnSc6	odpaliště
hraje	hrát	k5eAaImIp3nS	hrát
první	první	k4xOgInSc4	první
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c6	na
předchozí	předchozí	k2eAgFnSc6d1	předchozí
jamce	jamka	k1gFnSc6	jamka
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
počtu	počet	k1gInSc2	počet
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
zapisovatel	zapisovatel	k1gMnSc1	zapisovatel
jinému	jiný	k2eAgMnSc3d1	jiný
hráči	hráč	k1gMnSc3	hráč
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
skórkarty	skórkarta	k1gFnSc2	skórkarta
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jeho	jeho	k3xOp3gInPc4	jeho
výsledky	výsledek	k1gInPc4	výsledek
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
jamkách	jamka	k1gFnPc6	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
zapisovat	zapisovat	k5eAaImF	zapisovat
i	i	k9	i
vlastní	vlastní	k2eAgInPc4d1	vlastní
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
rány	rána	k1gFnPc4	rána
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
součet	součet	k1gInSc4	součet
počtu	počet	k1gInSc2	počet
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
trestných	trestný	k2eAgFnPc2d1	trestná
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Trestná	trestný	k2eAgFnSc1d1	trestná
rána	rána	k1gFnSc1	rána
není	být	k5eNaImIp3nS	být
skutečná	skutečný	k2eAgFnSc1d1	skutečná
rána	rána	k1gFnSc1	rána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trest	trest	k1gInSc4	trest
uložený	uložený	k2eAgInSc4d1	uložený
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Porušení	porušení	k1gNnSc1	porušení
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dotek	dotek	k1gInSc4	dotek
písku	písek	k1gInSc2	písek
v	v	k7c6	v
bunkru	bunkr	k1gInSc6	bunkr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
trestá	trestat	k5eAaImIp3nS	trestat
dvěma	dva	k4xCgFnPc7	dva
trestnými	trestný	k2eAgFnPc7d1	trestná
ranami	rána	k1gFnPc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
trestnou	trestný	k2eAgFnSc4d1	trestná
ránu	rána	k1gFnSc4	rána
získá	získat	k5eAaPmIp3nS	získat
hráč	hráč	k1gMnSc1	hráč
např.	např.	kA	např.
pokud	pokud	k8xS	pokud
míč	míč	k1gInSc1	míč
skončí	skončit	k5eAaPmIp3nS	skončit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ztracen	ztratit	k5eAaPmNgMnS	ztratit
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nehratelné	hratelný	k2eNgFnSc6d1	nehratelná
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Golfová	golfový	k2eAgFnSc1d1	golfová
hůl	hůl	k1gFnSc1	hůl
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
k	k	k7c3	k
odehrání	odehrání	k1gNnSc3	odehrání
míčku	míček	k1gInSc2	míček
používá	používat	k5eAaImIp3nS	používat
hole	hole	k6eAd1	hole
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
clubs	clubs	k6eAd1	clubs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Golfová	golfový	k2eAgFnSc1d1	golfová
hůl	hůl	k1gFnSc1	hůl
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
clubhead	clubhead	k1gInSc1	clubhead
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
násady	násada	k1gFnPc4	násada
(	(	kIx(	(
<g/>
shaft	shaft	k5eAaPmF	shaft
<g/>
)	)	kIx)	)
a	a	k8xC	a
držadla	držadlo	k1gNnPc1	držadlo
(	(	kIx(	(
<g/>
grip	grip	k1gInSc1	grip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hole	hole	k6eAd1	hole
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
patry	patro	k1gNnPc7	patro
(	(	kIx(	(
<g/>
putters	putters	k1gInSc1	putters
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
irons	irons	k6eAd1	irons
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hybridy	hybrid	k1gInPc1	hybrid
(	(	kIx(	(
<g/>
hybrids	hybrids	k6eAd1	hybrids
<g/>
)	)	kIx)	)
a	a	k8xC	a
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
woods	woods	k6eAd1	woods
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Puttery	Putter	k1gInPc1	Putter
jsou	být	k5eAaImIp3nP	být
hole	hole	k6eAd1	hole
specifické	specifický	k2eAgFnPc1d1	specifická
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
doražení	doražení	k1gNnSc4	doražení
<g/>
"	"	kIx"	"
míče	míč	k1gInSc2	míč
do	do	k7c2	do
jamky	jamka	k1gFnSc2	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
úderová	úderový	k2eAgFnSc1d1	úderová
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
míče	míč	k1gInPc1	míč
se	se	k3xPyFc4	se
jen	jen	k9	jen
kutálejí	kutálet	k5eAaImIp3nP	kutálet
<g/>
.	.	kIx.	.
</s>
<s>
Železa	železo	k1gNnPc1	železo
mají	mít	k5eAaImIp3nP	mít
hlavu	hlava	k1gFnSc4	hlava
vyrobenu	vyroben	k2eAgFnSc4d1	vyrobena
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Hybridy	hybrid	k1gInPc1	hybrid
jsou	být	k5eAaImIp3nP	být
hole	hole	k1gNnSc4	hole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vypadá	vypadat	k5eAaImIp3nS	vypadat
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
váhou	váha	k1gFnSc7	váha
a	a	k8xC	a
výplní	výplň	k1gFnSc7	výplň
báně	báně	k1gFnSc2	báně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
něco	něco	k3yInSc1	něco
mezi	mezi	k7c7	mezi
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dřeva	dřevo	k1gNnPc1	dřevo
byla	být	k5eAaImAgNnP	být
původně	původně	k6eAd1	původně
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
na	na	k7c4	na
úderovou	úderový	k2eAgFnSc4d1	úderová
plochu	plocha	k1gFnSc4	plocha
ocel	ocel	k1gFnSc4	ocel
a	a	k8xC	a
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bývá	bývat	k5eAaImIp3nS	bývat
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
hole	hole	k1gFnSc1	hole
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
)	)	kIx)	)
dutá	dutat	k5eAaImIp3nS	dutat
<g/>
.	.	kIx.	.
</s>
<s>
Železa	železo	k1gNnPc1	železo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
obecně	obecně	k6eAd1	obecně
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
a	a	k8xC	a
přesnější	přesný	k2eAgFnSc4d2	přesnější
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnPc4	dřevo
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Hole	hole	k6eAd1	hole
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
délkou	délka	k1gFnSc7	délka
a	a	k8xC	a
sklonem	sklon	k1gInSc7	sklon
úderové	úderový	k2eAgFnSc2d1	úderová
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
loft	loft	k1gInSc1	loft
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železa	železo	k1gNnPc1	železo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
označení	označení	k1gNnSc6	označení
1	[number]	k4	1
až	až	k9	až
9	[number]	k4	9
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejmenší	malý	k2eAgInSc4d3	nejmenší
sklon	sklon	k1gInSc4	sklon
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
délku	délka	k1gFnSc4	délka
má	mít	k5eAaImIp3nS	mít
hůl	hůl	k1gFnSc1	hůl
č.	č.	k?	č.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
menší	malý	k2eAgNnSc1d2	menší
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
menší	malý	k2eAgFnPc1d2	menší
sklon	sklon	k1gInSc4	sklon
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
delší	dlouhý	k2eAgInSc4d2	delší
shaft	shaft	k1gInSc4	shaft
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nejkratší	krátký	k2eAgFnPc4d3	nejkratší
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
přihrávky	přihrávka	k1gFnPc4	přihrávka
na	na	k7c4	na
green	green	k1gInSc4	green
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
wedge	wedge	k1gInSc4	wedge
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc1d3	nejmenší
sklon	sklon	k1gInSc1	sklon
má	mít	k5eAaImIp3nS	mít
pitching	pitching	k1gInSc4	pitching
wedge	wedg	k1gFnSc2	wedg
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sklon	sklon	k1gInSc1	sklon
hlavy	hlava	k1gFnSc2	hlava
této	tento	k3xDgFnSc2	tento
hole	hole	k1gFnSc2	hole
je	být	k5eAaImIp3nS	být
48	[number]	k4	48
<g/>
°	°	k?	°
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	s	k7c7	s
PW	PW	kA	PW
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
gap	gap	k?	gap
wedge	wedge	k1gInSc1	wedge
–	–	k?	–
52	[number]	k4	52
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc6	označení
G.	G.	kA	G.
Další	další	k2eAgFnSc4d1	další
hůl	hůl	k1gFnSc4	hůl
sandwedge	sandwedge	k6eAd1	sandwedge
má	mít	k5eAaImIp3nS	mít
sklon	sklon	k1gInSc4	sklon
54	[number]	k4	54
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
SW	SW	kA	SW
<g/>
.	.	kIx.	.
</s>
<s>
Lobwedge	Lobwedgat	k5eAaPmIp3nS	Lobwedgat
se	se	k3xPyFc4	se
sklonem	sklon	k1gInSc7	sklon
58	[number]	k4	58
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
buďto	buďto	k8xC	buďto
sklonem	sklon	k1gInSc7	sklon
hlavy	hlava	k1gFnSc2	hlava
nebo	nebo	k8xC	nebo
písmenem	písmeno	k1gNnSc7	písmeno
L.	L.	kA	L.
Jak	jak	k6eAd1	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
pitching	pitching	k1gInSc1	pitching
wedge	wedg	k1gFnSc2	wedg
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
hraní	hraní	k1gNnSc6	hraní
úderů	úder	k1gInPc2	úder
chip	chip	k1gInSc1	chip
a	a	k8xC	a
pitch	pitch	k1gInSc1	pitch
<g/>
.	.	kIx.	.
</s>
<s>
Gapwedge	Gapwedge	k6eAd1	Gapwedge
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
z	z	k7c2	z
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
jam	jáma	k1gFnPc2	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Sandwedge	Sandwedge	k1gFnSc1	Sandwedge
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
z	z	k7c2	z
pískové	pískový	k2eAgFnSc2d1	písková
překážky	překážka	k1gFnSc2	překážka
(	(	kIx(	(
<g/>
bunkru	bunkr	k1gInSc2	bunkr
<g/>
)	)	kIx)	)
a	a	k8xC	a
lobwedge	lobwedge	k6eAd1	lobwedge
a	a	k8xC	a
trouble	trouble	k6eAd1	trouble
wedge	wedge	k6eAd1	wedge
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
krátké	krátký	k2eAgInPc4d1	krátký
obloučky	oblouček	k1gInPc4	oblouček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
posledních	poslední	k2eAgFnPc2d1	poslední
holí	hole	k1gFnPc2	hole
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
ještě	ještě	k9	ještě
parametr	parametr	k1gInSc1	parametr
bounce	bounec	k1gInSc2	bounec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vyboulení	vyboulení	k1gNnSc1	vyboulení
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hluboko	hluboko	k6eAd1	hluboko
se	se	k3xPyFc4	se
hůl	hůl	k1gFnSc1	hůl
ponoří	ponořit	k5eAaPmIp3nS	ponořit
při	při	k7c6	při
úderu	úder	k1gInSc6	úder
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dřeva	dřevo	k1gNnPc1	dřevo
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
rány	rána	k1gFnPc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
drivery	driver	k1gInPc4	driver
a	a	k8xC	a
fairwayová	fairwayová	k1gFnSc1	fairwayová
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fairwayová	Fairwayový	k2eAgNnPc1d1	Fairwayový
dřeva	dřevo	k1gNnPc1	dřevo
jsou	být	k5eAaImIp3nP	být
určena	určit	k5eAaPmNgNnP	určit
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
z	z	k7c2	z
fairwaye	fairway	k1gFnSc2	fairway
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
krátce	krátce	k6eAd1	krátce
střiženou	střižený	k2eAgFnSc4d1	střižená
část	část	k1gFnSc4	část
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Tráva	tráva	k1gFnSc1	tráva
se	se	k3xPyFc4	se
stříhá	stříhat	k5eAaImIp3nS	stříhat
na	na	k7c4	na
cca	cca	kA	cca
2	[number]	k4	2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
úder	úder	k1gInSc4	úder
dřevem	dřevo	k1gNnSc7	dřevo
a	a	k8xC	a
jakoby	jakoby	k8xS	jakoby
smést	smést	k5eAaPmF	smést
míček	míček	k1gInSc4	míček
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Driver	driver	k1gInSc1	driver
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
nejdelší	dlouhý	k2eAgFnPc4d3	nejdelší
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
driverem	driver	k1gInSc7	driver
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
z	z	k7c2	z
týčka	týček	k1gInSc2	týček
(	(	kIx(	(
<g/>
tee	tee	k?	tee
<g/>
)	)	kIx)	)
-	-	kIx~	-
většinou	většina	k1gFnSc7	většina
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
kolíčku	kolíček	k1gInSc2	kolíček
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
odpaluje	odpalovat	k5eAaImIp3nS	odpalovat
(	(	kIx(	(
<g/>
míček	míček	k1gInSc1	míček
se	se	k3xPyFc4	se
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
vrch	vrch	k1gInSc4	vrch
týčka	týček	k1gInSc2	týček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
už	už	k6eAd1	už
ale	ale	k9	ale
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
týčka	týčka	k6eAd1	týčka
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
úderu	úder	k1gInSc6	úder
nerozlomí	rozlomit	k5eNaPmIp3nS	rozlomit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
odpalu	odpal	k1gInSc6	odpal
nenajde	najít	k5eNaPmIp3nS	najít
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Driver	driver	k1gInSc1	driver
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
značen	značen	k2eAgInSc1d1	značen
jako	jako	k8xC	jako
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
sklonem	sklon	k1gInSc7	sklon
úderové	úderový	k2eAgFnSc2d1	úderová
plochy	plocha	k1gFnSc2	plocha
většinou	většina	k1gFnSc7	většina
od	od	k7c2	od
8,5	[number]	k4	8,5
<g/>
°	°	k?	°
do	do	k7c2	do
11	[number]	k4	11
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hráč	hráč	k1gMnSc1	hráč
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
nejvýše	vysoce	k6eAd3	vysoce
14	[number]	k4	14
holí	hole	k1gFnPc2	hole
<g/>
.	.	kIx.	.
</s>
<s>
Golf	golf	k1gInSc1	golf
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
Masters	Mastersa	k1gFnPc2	Mastersa
Tournament	Tournament	k1gInSc1	Tournament
-	-	kIx~	-
muži	muž	k1gMnPc1	muž
Kraft	Kraft	k1gMnSc1	Kraft
Nabisco	Nabisco	k1gMnSc1	Nabisco
Championship	Championship	k1gMnSc1	Championship
-	-	kIx~	-
ženy	žena	k1gFnPc1	žena
Ryder	Ryder	k1gInSc4	Ryder
Cup	cup	k1gInSc1	cup
-	-	kIx~	-
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
soutěž	soutěž	k1gFnSc1	soutěž
mužských	mužský	k2eAgInPc2d1	mužský
týmů	tým	k1gInPc2	tým
</s>
