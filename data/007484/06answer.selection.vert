<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dokladem	doklad	k1gInSc7	doklad
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
území	území	k1gNnSc2	území
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
rukou	ruka	k1gFnPc2	ruka
opracovaný	opracovaný	k2eAgInSc1d1	opracovaný
kámen	kámen	k1gInSc1	kámen
nalezený	nalezený	k2eAgInSc1d1	nalezený
na	na	k7c6	na
Červeném	červený	k2eAgInSc6d1	červený
kopci	kopec	k1gInSc6	kopec
starý	starý	k2eAgInSc4d1	starý
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
