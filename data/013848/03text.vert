<s>
Přebor	přebor	k1gInSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Přebor	přebor	k1gInSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
Ondrášovka	Ondrášovka	k1gFnSc1
Krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
krajskými	krajský	k2eAgInPc7d1
přebory	přebor	k1gInPc7
mezi	mezi	k7c4
páté	pátá	k1gFnPc4
nejvyšší	vysoký	k2eAgFnSc2d3
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
řízen	řídit	k5eAaImNgInS
Jihočeským	jihočeský	k2eAgInSc7d1
krajským	krajský	k2eAgInSc7d1
fotbalovým	fotbalový	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
od	od	k7c2
léta	léto	k1gNnSc2
do	do	k7c2
jara	jaro	k1gNnSc2
se	s	k7c7
zimní	zimní	k2eAgFnSc7d1
přestávkou	přestávka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastní	účastnit	k5eAaImIp3nS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
16	#num#	k4
týmů	tým	k1gInPc2
-	-	kIx~
z	z	k7c2
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
s	s	k7c7
každým	každý	k3xTgInSc7
hraje	hrát	k5eAaImIp3nS
jednou	jednou	k6eAd1
na	na	k7c6
domácím	domácí	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
a	a	k8xC
jednou	jednou	k6eAd1
na	na	k7c6
hřišti	hřiště	k1gNnSc6
soupeře	soupeř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celek	k1gInSc7
se	se	k3xPyFc4
tedy	tedy	k9
hraje	hrát	k5eAaImIp3nS
30	#num#	k4
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězem	vítěz	k1gMnSc7
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
tým	tým	k1gInSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
počtem	počet	k1gInSc7
bodů	bod	k1gInPc2
v	v	k7c6
tabulce	tabulka	k1gFnSc6
a	a	k8xC
postupuje	postupovat	k5eAaImIp3nS
do	do	k7c2
Divize	divize	k1gFnSc2
A.	A.	kA
Poslední	poslední	k2eAgInPc4d1
dva	dva	k4xCgInPc4
týmy	tým	k1gInPc4
sestupují	sestupovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
do	do	k7c2
I.A	I.A	k1gMnPc2
třídy	třída	k1gFnSc2
-	-	kIx~
skupina	skupina	k1gFnSc1
A	a	k9
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
do	do	k7c2
I.A	I.A	k1gMnPc2
třídy	třída	k1gFnSc2
-	-	kIx~
skupina	skupina	k1gFnSc1
B.	B.	kA
Do	do	k7c2
Přeboru	přebor	k1gInSc2
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
vždy	vždy	k6eAd1
postupuje	postupovat	k5eAaImIp3nS
vítěz	vítěz	k1gMnSc1
I.A	I.A	k1gFnSc2
třídy	třída	k1gFnSc2
-	-	kIx~
skupina	skupina	k1gFnSc1
A	a	k9
a	a	k8xC
vítěz	vítěz	k1gMnSc1
I.A	I.A	k1gFnSc2
třídy	třída	k1gFnSc2
-	-	kIx~
skupina	skupina	k1gFnSc1
B.	B.	kA
</s>
<s>
Vítězové	vítěz	k1gMnPc1
</s>
<s>
RočníkVítězný	RočníkVítězný	k2eAgInSc1d1
týmLink	týmLink	k1gInSc1
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
SK	Sk	kA
JankovZde	JankovZd	k1gMnSc5
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
TJ	tj	kA
Malše	Malše	k1gFnSc2
RoudnéZde	RoudnéZd	k1gInSc5
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
FC	FC	kA
PísekZde	PísekZd	k1gInSc5
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
FC	FC	kA
ZVVZ	ZVVZ	kA
MilevskoZde	milevskozde	k6eAd1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
TJ	tj	kA
Malše	Malše	k1gFnSc2
RoudnéZde	RoudnéZd	k1gMnSc5
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
FK	FK	kA
Sezimovo	Sezimův	k2eAgNnSc1d1
Ústí	ústí	k1gNnSc1
BZde	BZd	k1gFnSc2
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
TJ	tj	kA
Sokol	Sokol	k1gMnSc1
ČížováZde	ČížováZd	k1gMnSc5
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
TJ	tj	kA
DražiceZde	DražiceZd	k1gMnSc5
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
TJ	tj	kA
DražiceZde	DražiceZd	k1gInSc5
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
TJ	tj	kA
DražiceZde	DražiceZd	k1gInSc5
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
FK	FK	kA
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
1910	#num#	k4
<g/>
Zde	zde	k6eAd1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
SK	Sk	kA
JankovZde	JankovZd	k1gMnSc5
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
FK	FK	kA
Spartak	Spartak	k1gInSc1
SoběslavZde	SoběslavZd	k1gMnSc5
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
TJ	tj	kA
Sokol	Sokol	k1gMnSc1
ŽelečZde	ŽelečZd	k1gInSc5
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
FK	FK	kA
OlešníkZde	OlešníkZd	k1gInSc5
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
FK	FK	kA
Olešník	olešník	k1gInSc1
</s>
<s>
Zde	zde	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Přebor	přebor	k1gInSc1
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Krajské	krajský	k2eAgInPc1d1
přebory	přebor	k1gInPc1
v	v	k7c6
kopané	kopaná	k1gFnSc6
</s>
<s>
Praha	Praha	k1gFnSc1
•	•	k?
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Severočeský	severočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Brněnský	brněnský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
