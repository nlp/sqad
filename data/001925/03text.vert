<s>
Vakuola	vakuola	k1gFnSc1	vakuola
je	být	k5eAaImIp3nS	být
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
membránou	membrána	k1gFnSc7	membrána
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
protist	protist	k1gInSc1	protist
<g/>
,	,	kIx,	,
kvasinek	kvasinka	k1gFnPc2	kvasinka
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
vakuoly	vakuola	k1gFnSc2	vakuola
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tonoplast	tonoplast	k1gInSc1	tonoplast
<g/>
.	.	kIx.	.
</s>
<s>
Vakuola	vakuola	k1gFnSc1	vakuola
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
jen	jen	k9	jen
u	u	k7c2	u
určitých	určitý	k2eAgInPc2d1	určitý
druhů	druh	k1gInPc2	druh
vakuoly	vakuola	k1gFnSc2	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
funkce	funkce	k1gFnPc4	funkce
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
izoluje	izolovat	k5eAaBmIp3nS	izolovat
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pro	pro	k7c4	pro
buňku	buňka	k1gFnSc4	buňka
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
buňkách	buňka	k1gFnPc6	buňka
udržuje	udržovat	k5eAaImIp3nS	udržovat
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
hydrostatický	hydrostatický	k2eAgInSc1d1	hydrostatický
tlak	tlak	k1gInSc1	tlak
buňky	buňka	k1gFnSc2	buňka
udržuje	udržovat	k5eAaImIp3nS	udržovat
rovnoměrné	rovnoměrný	k2eAgFnSc2d1	rovnoměrná
pH	ph	kA	ph
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
malé	malý	k2eAgFnPc4d1	malá
molekuly	molekula	k1gFnPc4	molekula
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
nechtěné	chtěný	k2eNgFnPc4d1	nechtěná
látky	látka	k1gFnPc4	látka
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rostlinám	rostlina	k1gFnPc3	rostlina
udržení	udržení	k1gNnSc2	udržení
tvaru	tvar	k1gInSc2	tvar
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tlaku	tlak	k1gInSc3	tlak
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
vakuole	vakuola	k1gFnSc6	vakuola
v	v	k7c6	v
semenech	semeno	k1gNnPc6	semeno
ukládá	ukládat	k5eAaImIp3nS	ukládat
lehce	lehko	k6eAd1	lehko
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
vakuola	vakuola	k1gFnSc1	vakuola
živiny	živina	k1gFnSc2	živina
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
V	v	k7c6	v
nezralých	zralý	k2eNgFnPc6d1	nezralá
nebo	nebo	k8xC	nebo
mladých	mladý	k2eAgFnPc6d1	mladá
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
buňkách	buňka	k1gFnPc6	buňka
pracujících	pracující	k1gFnPc2	pracující
dělivých	dělivý	k2eAgNnPc2d1	dělivé
pletiv	pletivo	k1gNnPc2	pletivo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
malých	malý	k2eAgFnPc2d1	malá
vakuol	vakuola	k1gFnPc2	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
a	a	k8xC	a
specializace	specializace	k1gFnSc2	specializace
diferenciace	diferenciace	k1gFnSc2	diferenciace
nebo	nebo	k8xC	nebo
své	svůj	k3xOyFgFnPc4	svůj
funkční	funkční	k2eAgFnPc4d1	funkční
aktivity	aktivita	k1gFnPc4	aktivita
růst	růst	k5eAaImF	růst
a	a	k8xC	a
splývat	splývat	k5eAaImF	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zralých	zralý	k2eAgFnPc6d1	zralá
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
aktivních	aktivní	k2eAgFnPc6d1	aktivní
buňkách	buňka	k1gFnPc6	buňka
pak	pak	k6eAd1	pak
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
obří	obří	k2eAgFnSc4d1	obří
vakuolu	vakuola	k1gFnSc4	vakuola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
zaplnit	zaplnit	k5eAaPmF	zaplnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
jejich	jejich	k3xOp3gInSc2	jejich
buněčného	buněčný	k2eAgInSc2d1	buněčný
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
vakuola	vakuola	k1gFnSc1	vakuola
představuje	představovat	k5eAaImIp3nS	představovat
zásobárnu	zásobárna	k1gFnSc4	zásobárna
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
dalších	další	k2eAgFnPc2d1	další
organických	organický	k2eAgFnPc2d1	organická
i	i	k8xC	i
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
cukry	cukr	k1gInPc1	cukr
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
organické	organický	k2eAgFnPc1d1	organická
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
alkaloidy	alkaloid	k1gInPc1	alkaloid
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
,	,	kIx,	,
barviva	barvivo	k1gNnPc1	barvivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
buněčnému	buněčný	k2eAgNnSc3d1	buněčné
trávení	trávení	k1gNnSc3	trávení
(	(	kIx(	(
<g/>
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
nepřítomné	přítomný	k2eNgInPc4d1	nepřítomný
lyzozomy	lyzozom	k1gInPc4	lyzozom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tekutý	tekutý	k2eAgInSc1d1	tekutý
obsah	obsah	k1gInSc1	obsah
vakuoly	vakuola	k1gFnSc2	vakuola
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
buněčná	buněčný	k2eAgFnSc1d1	buněčná
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
nashromážděné	nashromážděný	k2eAgFnPc1d1	nashromážděná
látky	látka	k1gFnPc1	látka
ve	v	k7c6	v
vakuolách	vakuola	k1gFnPc6	vakuola
vykrystalizovat	vykrystalizovat	k5eAaPmF	vykrystalizovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
inkluze	inkluze	k1gFnSc2	inkluze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šťavelan	šťavelan	k1gInSc4	šťavelan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
všech	všecek	k3xTgFnPc2	všecek
vakuol	vakuola	k1gFnPc2	vakuola
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
vakuom	vakuom	k1gInSc1	vakuom
<g/>
.	.	kIx.	.
</s>
<s>
Vakuoly	vakuola	k1gFnPc4	vakuola
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
živočišných	živočišný	k2eAgFnPc6d1	živočišná
buňkách	buňka	k1gFnPc6	buňka
-	-	kIx~	-
např.	např.	kA	např.
tukové	tukový	k2eAgFnPc4d1	tuková
buňky	buňka	k1gFnPc4	buňka
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
obsah	obsah	k1gInSc4	obsah
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
tukovou	tukový	k2eAgFnSc4d1	tuková
vakuolou	vakuola	k1gFnSc7	vakuola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
stlačí	stlačit	k5eAaPmIp3nP	stlačit
ostatní	ostatní	k2eAgFnPc4d1	ostatní
organely	organela	k1gFnPc4	organela
k	k	k7c3	k
cytoplasmatické	cytoplasmatický	k2eAgFnSc3d1	cytoplasmatická
membráně	membrána	k1gFnSc3	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
živočišné	živočišný	k2eAgFnPc1d1	živočišná
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
drobné	drobný	k2eAgFnPc1d1	drobná
vakuoly	vakuola	k1gFnPc1	vakuola
naplněné	naplněný	k2eAgInPc1d1	naplněný
trávicími	trávicí	k2eAgFnPc7d1	trávicí
šťávami	šťáva	k1gFnPc7	šťáva
(	(	kIx(	(
<g/>
enzymy	enzym	k1gInPc1	enzym
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
potřeby	potřeba	k1gFnSc2	potřeba
(	(	kIx(	(
<g/>
trávení	trávení	k1gNnSc1	trávení
<g/>
)	)	kIx)	)
uvolňovány	uvolňován	k2eAgInPc1d1	uvolňován
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
lumen	lumen	k1gNnSc1	lumen
střev	střevo	k1gNnPc2	střevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
vakuoly	vakuola	k1gFnPc1	vakuola
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
mléčné	mléčný	k2eAgFnSc2d1	mléčná
žlázy	žláza	k1gFnSc2	žláza
nebo	nebo	k8xC	nebo
pigmentových	pigmentový	k2eAgFnPc6d1	pigmentová
buňkách	buňka	k1gFnPc6	buňka
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
střádají	střádat	k5eAaImIp3nP	střádat
mléčné	mléčný	k2eAgFnSc2d1	mléčná
kapky	kapka	k1gFnSc2	kapka
nebo	nebo	k8xC	nebo
ochranný	ochranný	k2eAgInSc1d1	ochranný
pigment	pigment	k1gInSc1	pigment
melanin	melanin	k1gInSc1	melanin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
poškození	poškození	k1gNnSc4	poškození
tkáně	tkáň	k1gFnSc2	tkáň
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
světlem	světlo	k1gNnSc7	světlo
vysílaným	vysílaný	k2eAgNnSc7d1	vysílané
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
vakuoly	vakuola	k1gFnPc4	vakuola
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
synaptické	synaptický	k2eAgInPc4d1	synaptický
váčky	váček	k1gInPc4	váček
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
látky	látka	k1gFnPc1	látka
-	-	kIx~	-
mediátory	mediátor	k1gInPc1	mediátor
-	-	kIx~	-
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
a	a	k8xC	a
aktivují	aktivovat	k5eAaBmIp3nP	aktivovat
nebo	nebo	k8xC	nebo
tlumí	tlumit	k5eAaImIp3nP	tlumit
nervový	nervový	k2eAgInSc4d1	nervový
impuls	impuls	k1gInSc4	impuls
<g/>
.	.	kIx.	.
</s>
<s>
U	U	kA	U
protist	protist	k1gInSc1	protist
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vakuoly	vakuola	k1gFnPc4	vakuola
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
modifikované	modifikovaný	k2eAgInPc1d1	modifikovaný
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
modifikacemi	modifikace	k1gFnPc7	modifikace
jsou	být	k5eAaImIp3nP	být
pulsující	pulsující	k2eAgFnSc1d1	pulsující
vakuoly	vakuola	k1gFnPc1	vakuola
a	a	k8xC	a
potravní	potravní	k2eAgFnPc1d1	potravní
vakuoly	vakuola	k1gFnPc1	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
stažitelná	stažitelný	k2eAgFnSc1d1	stažitelná
vakuola	vakuola	k1gFnSc1	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Pulsující	pulsující	k2eAgFnPc1d1	pulsující
vakuoly	vakuola	k1gFnPc1	vakuola
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
u	u	k7c2	u
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
protist	protist	k1gInSc4	protist
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
a	a	k8xC	a
vypuzovat	vypuzovat	k5eAaImF	vypuzovat
vodu	voda	k1gFnSc4	voda
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
hypotonického	hypotonický	k2eAgNnSc2d1	hypotonické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
ředění	ředění	k1gNnSc4	ředění
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
osmotický	osmotický	k2eAgInSc4d1	osmotický
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
potravní	potravní	k2eAgFnSc1d1	potravní
vakuola	vakuola	k1gFnSc1	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
trávení	trávení	k1gNnSc3	trávení
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vakuol	vakuola	k1gFnPc2	vakuola
rostlin	rostlina	k1gFnPc2	rostlina
vznikají	vznikat	k5eAaImIp3nP	vznikat
a	a	k8xC	a
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
.	.	kIx.	.
</s>
