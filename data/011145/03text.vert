<p>
<s>
Bianka	Bianka	k6eAd1	Bianka
Panovová	Panovový	k2eAgFnSc1d1	Panovový
(	(	kIx(	(
<g/>
Б	Б	k?	Б
П	П	k?	П
<g/>
,	,	kIx,	,
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1970	[number]	k4	1970
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
bulharská	bulharský	k2eAgFnSc1d1	bulharská
moderní	moderní	k2eAgFnSc1d1	moderní
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
,	,	kIx,	,
devítinásobná	devítinásobný	k2eAgFnSc1d1	devítinásobná
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
generace	generace	k1gFnSc1	generace
bulharských	bulharský	k2eAgFnPc2d1	bulharská
šampiónek	šampiónka	k1gFnPc2	šampiónka
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
Zlatá	zlatý	k2eAgNnPc1d1	Zlaté
děvčata	děvče	k1gNnPc1	děvče
(	(	kIx(	(
<g/>
З	З	k?	З
м	м	k?	м
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Připravovala	připravovat	k5eAaImAgFnS	připravovat
se	se	k3xPyFc4	se
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Levski	Levsk	k1gFnSc2	Levsk
Sofia	Sofia	k1gFnSc1	Sofia
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
trenérky	trenérka	k1gFnSc2	trenérka
Nešky	Neška	k1gFnSc2	Neška
Robevové	Robevová	k1gFnSc2	Robevová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
cvičení	cvičení	k1gNnSc6	cvičení
se	s	k7c7	s
stuhou	stuha	k1gFnSc7	stuha
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
MS	MS	kA	MS
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
ve	v	k7c6	v
Varně	Varna	k1gFnSc6	Varna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
závodnice	závodnice	k1gFnSc1	závodnice
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
víceboj	víceboj	k1gInSc4	víceboj
i	i	k8xC	i
všechna	všechen	k3xTgNnPc4	všechen
čtyři	čtyři	k4xCgNnPc4	čtyři
nářadí	nářadí	k1gNnPc4	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
sestavy	sestava	k1gFnPc4	sestava
také	také	k6eAd1	také
dostala	dostat	k5eAaPmAgFnS	dostat
plný	plný	k2eAgInSc4d1	plný
počet	počet	k1gInSc4	počet
deseti	deset	k4xCc2	deset
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
do	do	k7c2	do
Guinnessovy	Guinnessův	k2eAgFnSc2d1	Guinnessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
rozešla	rozejít	k5eAaPmAgFnS	rozejít
s	s	k7c7	s
trenérkou	trenérka	k1gFnSc7	trenérka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
s	s	k7c7	s
obručí	obruč	k1gFnSc7	obruč
a	a	k8xC	a
stuhou	stuha	k1gFnSc7	stuha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
byla	být	k5eAaImAgFnS	být
až	až	k9	až
pátá	pátý	k4xOgFnSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
pokazila	pokazit	k5eAaPmAgFnS	pokazit
cvičení	cvičení	k1gNnSc4	cvičení
s	s	k7c7	s
kužely	kužel	k1gInPc7	kužel
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nového	nový	k2eAgMnSc2d1	nový
trenéra	trenér	k1gMnSc2	trenér
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc2	svůj
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
manžela	manžel	k1gMnSc2	manžel
Čavdara	Čavdar	k1gMnSc2	Čavdar
Ninova	Ninovo	k1gNnSc2	Ninovo
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
1989	[number]	k4	1989
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ukončila	ukončit	k5eAaPmAgFnS	ukončit
závodní	závodní	k2eAgFnSc4d1	závodní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Georgi	Georg	k1gFnPc4	Georg
Djulgerova	Djulgerův	k2eAgFnSc1d1	Djulgerův
AkaTaMus	AkaTaMus	k1gInSc1	AkaTaMus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
působila	působit	k5eAaImAgFnS	působit
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
jako	jako	k8xS	jako
gymnastická	gymnastický	k2eAgFnSc1d1	gymnastická
trenérka	trenérka	k1gFnSc1	trenérka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
taneční	taneční	k2eAgFnSc4d1	taneční
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
bulharskou	bulharský	k2eAgFnSc4d1	bulharská
verzi	verze	k1gFnSc4	verze
televizní	televizní	k2eAgFnSc2d1	televizní
taneční	taneční	k2eAgFnSc2d1	taneční
soutěže	soutěž	k1gFnSc2	soutěž
Dancing	dancing	k1gInSc1	dancing
with	with	k1gMnSc1	with
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
otevřeně	otevřeně	k6eAd1	otevřeně
popsala	popsat	k5eAaPmAgFnS	popsat
drastické	drastický	k2eAgFnPc4d1	drastická
metody	metoda	k1gFnPc4	metoda
tréninku	trénink	k1gInSc2	trénink
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
nadvládě	nadvláda	k1gFnSc3	nadvláda
bulharských	bulharský	k2eAgFnPc2d1	bulharská
moderních	moderní	k2eAgFnPc2d1	moderní
gymnastek	gymnastka	k1gFnPc2	gymnastka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bianka	Bianka	k6eAd1	Bianka
Panovová	Panovový	k2eAgFnSc1d1	Panovový
<g/>
:	:	kIx,	:
autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
9789542812289	[number]	k4	9789542812289
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
AkaTaMus	AkaTaMus	k1gInSc1	AkaTaMus
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Georgi	Georgi	k1gNnSc1	Georgi
Djulgerov	Djulgerovo	k1gNnPc2	Djulgerovo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.nsa.bg/bg/faculty/department/branch,48/subpage,93	[url]	k4	http://www.nsa.bg/bg/faculty/department/branch,48/subpage,93
</s>
</p>
<p>
<s>
https://web.archive.org/web/20110718150059/http://www.gymn.ca/gymnasticgreats/rsg/panova.htm	[url]	k6eAd1	https://web.archive.org/web/20110718150059/http://www.gymn.ca/gymnasticgreats/rsg/panova.htm
</s>
</p>
