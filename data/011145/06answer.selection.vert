<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
otevřeně	otevřeně	k6eAd1	otevřeně
popsala	popsat	k5eAaPmAgFnS	popsat
drastické	drastický	k2eAgFnPc4d1	drastická
metody	metoda	k1gFnPc4	metoda
tréninku	trénink	k1gInSc2	trénink
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
nadvládě	nadvláda	k1gFnSc3	nadvláda
bulharských	bulharský	k2eAgFnPc2d1	bulharská
moderních	moderní	k2eAgFnPc2d1	moderní
gymnastek	gymnastka	k1gFnPc2	gymnastka
<g/>
.	.	kIx.	.
</s>
