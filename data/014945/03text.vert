<s>
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
stylistika	stylistika	k1gFnSc1
některých	některý	k3yIgFnPc2
vět	věta	k1gFnPc2
<g/>
,	,	kIx,
wikifikace	wikifikace	k1gFnSc1
</s>
<s>
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
Most	most	k1gInSc1
Nové	Nová	k1gFnSc2
EvropyZákladní	EvropyZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Stát	stát	k5eAaImF,k5eAaPmF
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
BulharskoRumunsko	BulharskoRumunsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
Přes	přes	k7c4
</s>
<s>
Dunaj	Dunaj	k1gInSc1
Otevřen	otevřen	k2eAgInSc1d1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
18,37	18,37	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
51,02	51,02	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Parametry	parametr	k1gInPc1
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
971	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
32	#num#	k4
m	m	kA
Mapa	mapa	k1gFnSc1
</s>
<s>
Další	další	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pan-European	Pan-European	k1gMnSc1
corridor	corridor	k1gMnSc1
IV	IV	kA
highlighted	highlighted	k1gMnSc1
in	in	k?
red	red	k?
</s>
<s>
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
М	М	k?
Н	Н	k?
Е	Е	k?
<g/>
,	,	kIx,
rumunsky	rumunsky	k6eAd1
''	''	k?
<g/>
Podul	podout	k5eAaPmAgMnS
Noua	Noua	k1gMnSc1
Europă	Europă	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
slavnostně	slavnostně	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojuje	spojovat	k5eAaImIp3nS
sousedící	sousedící	k2eAgNnPc4d1
města	město	k1gNnPc4
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
a	a	k8xC
Bulharsku	Bulharsko	k1gNnSc6
-	-	kIx~
Vidin	vidina	k1gFnPc2
a	a	k8xC
Calafat	Calafat	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autorem	autor	k1gMnSc7
podoby	podoba	k1gFnSc2
mostu	most	k1gInSc2
je	být	k5eAaImIp3nS
Fernández	Fernández	k1gInSc1
Casado	Casada	k1gInSc1
<g/>
,	,	kIx,
FCC	FCC	kA
Construcción	Construcción	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
pojmenovaný	pojmenovaný	k2eAgInSc1d1
most	most	k1gInSc1
Calafat-Vidin	Calafat-Vidin	k2eAgInSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Д	Д	k?
м	м	k?
2	#num#	k4
<g/>
,	,	kIx,
Dunav	Dunav	k1gInSc1
most	most	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aneb	aneb	k?
Vidin	vidina	k1gFnPc2
<g/>
–	–	k?
<g/>
Calafat	Calafat	k1gMnSc2
Bridge	Bridg	k1gMnSc2
(	(	kIx(
<g/>
М	М	k?
В	В	k?
<g/>
–	–	k?
<g/>
К	К	k?
<g/>
,	,	kIx,
Most	most	k1gInSc1
Vidin	vidina	k1gFnPc2
<g/>
–	–	k?
<g/>
Kalafat	Kalafat	k1gMnPc2
/	/	kIx~
Podul	podout	k5eAaPmAgInS
Calafat	Calafat	k1gFnSc4
<g/>
–	–	k?
<g/>
Vidin	vidina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c7
sousedícími	sousedící	k2eAgInPc7d1
regiony	region	k1gInPc7
Vidin	vidina	k1gFnPc2
a	a	k8xC
Calafat	Calafat	k1gFnPc2
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
důležitou	důležitý	k2eAgFnSc4d1
součást	součást	k1gFnSc4
prioritních	prioritní	k2eAgInPc2d1
silničních	silniční	k2eAgInPc2d1
a	a	k8xC
železničních	železniční	k2eAgInPc2d1
dopravních	dopravní	k2eAgInPc2d1
koridorů	koridor	k1gInPc2
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
díky	díky	k7c3
němu	on	k3xPp3gInSc3
výrazně	výrazně	k6eAd1
zkrátit	zkrátit	k5eAaPmF
doprava	doprava	k1gFnSc1
cestujících	cestující	k1gMnPc2
i	i	k9
přeprava	přeprava	k1gFnSc1
nákladu	náklad	k1gInSc2
mezi	mezi	k7c7
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
<g/>
,	,	kIx,
Tureckem	Turecko	k1gNnSc7
a	a	k8xC
střední	střední	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Most	most	k1gInSc1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
Dunaj	Dunaj	k1gInSc1
zhruba	zhruba	k6eAd1
1	#num#	k4
300	#num#	k4
metrů	metr	k1gInPc2
široký	široký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Most	most	k1gInSc1
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1,8	1,8	k4
km	km	kA
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
4	#num#	k4
jízdní	jízdní	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
železniční	železniční	k2eAgFnSc4d1
kolej	kolej	k1gFnSc4
a	a	k8xC
pásy	pás	k1gInPc4
pro	pro	k7c4
pěší	pěší	k2eAgMnPc4d1
a	a	k8xC
cyklisty	cyklista	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Most	most	k1gInSc1
stál	stát	k5eAaImAgInS
300	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
spolufinancován	spolufinancovat	k5eAaImNgInS
z	z	k7c2
evropského	evropský	k2eAgInSc2d1
fondu	fond	k1gInSc2
soudržnosti	soudržnost	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
projektu	projekt	k1gInSc2
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
panevropského	panevropský	k2eAgInSc2d1
koridoru	koridor	k1gInSc2
IV	IV	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
spojovat	spojovat	k5eAaImF
německé	německý	k2eAgInPc4d1
Drážďany	Drážďany	k1gInPc4
s	s	k7c7
tureckým	turecký	k2eAgInSc7d1
Istanbulem	Istanbul	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Potřebnost	potřebnost	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
výstavby	výstavba	k1gFnSc2
(	(	kIx(
<g/>
most	most	k1gInSc1
Calafat-Vidin	Calafat-Vidin	k1gInSc1
<g/>
)	)	kIx)
definitivně	definitivně	k6eAd1
ukázala	ukázat	k5eAaPmAgFnS
první	první	k4xOgFnSc1
polovina	polovina	k1gFnSc1
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
jezdit	jezdit	k5eAaImF
přes	přes	k7c4
Jugoslávii	Jugoslávie	k1gFnSc4
a	a	k8xC
západní	západní	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
zcela	zcela	k6eAd1
odříznuto	odříznut	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharskou	bulharský	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
zasáhly	zasáhnout	k5eAaPmAgFnP
také	také	k9
balkánské	balkánský	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
uzavření	uzavření	k1gNnSc6
hranic	hranice	k1gFnPc2
se	s	k7c7
Srbskem	Srbsko	k1gNnSc7
(	(	kIx(
<g/>
kvůli	kvůli	k7c3
sankcím	sankce	k1gFnPc3
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
)	)	kIx)
zůstalo	zůstat	k5eAaPmAgNnS
západní	západní	k2eAgNnSc1d1
a	a	k8xC
střední	střední	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
odříznuté	odříznutý	k2eAgNnSc1d1
od	od	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c4
dlouhá	dlouhý	k2eAgNnPc4d1
staletí	staletí	k1gNnPc4
nevedl	vést	k5eNaImAgInS
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Dunaj	Dunaj	k1gInSc1
na	na	k7c6
rumunsko-bulharské	rumunsko-bulharský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
žádný	žádný	k3yNgInSc1
most	most	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantinův	Konstantinův	k2eAgInSc1d1
most	most	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
dob	doba	k1gFnPc2
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
přestal	přestat	k5eAaPmAgInS
sloužit	sloužit	k5eAaImF
asi	asi	k9
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
století	století	k1gNnSc6
a	a	k8xC
první	první	k4xOgInSc1
most	most	k1gInSc1
přes	přes	k7c4
Dunaj	Dunaj	k1gInSc4
mezi	mezi	k7c7
Rumunskem	Rumunsko	k1gNnSc7
a	a	k8xC
Bulharskem	Bulharsko	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
spojuje	spojovat	k5eAaImIp3nS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
společného	společný	k2eAgInSc2d1
úseku	úsek	k1gInSc2
Dunaje	Dunaj	k1gInSc2
bulharské	bulharský	k2eAgNnSc1d1
město	město	k1gNnSc1
Ruse	Rus	k1gMnSc2
s	s	k7c7
rumunským	rumunský	k2eAgInSc7d1
Giurgiu	Giurgium	k1gNnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bukurešť	Bukurešť	k1gFnSc1
zase	zase	k9
chtěla	chtít	k5eAaImAgFnS
postavit	postavit	k5eAaPmF
most	most	k1gInSc4
Calafat-Vidin	Calafat-Vidin	k1gInSc4
východněji	východně	k6eAd2
(	(	kIx(
<g/>
jak	jak	k8xS,k8xC
Turnu	turnus	k1gInSc2
Mâgurele	Mâgurel	k1gInSc2
-	-	kIx~
Nikopol	Nikopol	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
spekulovala	spekulovat	k5eAaImAgFnS
s	s	k7c7
faktem	fakt	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
rozšířením	rozšíření	k1gNnSc7
mezinárodních	mezinárodní	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
přes	přes	k7c4
své	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
získá	získat	k5eAaPmIp3nS
více	hodně	k6eAd2
příjmů	příjem	k1gInPc2
z	z	k7c2
provozu	provoz	k1gInSc2
dálnice	dálnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohoda	dohoda	k1gFnSc1
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
podepsána	podepsat	k5eAaPmNgFnS
v	v	k7c6
červnu	červen	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
novému	nový	k2eAgInSc3d1
mostu	most	k1gInSc3
se	se	k3xPyFc4
cestování	cestování	k1gNnSc1
přes	přes	k7c4
Dunaj	Dunaj	k1gInSc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohraničního	pohraniční	k2eAgInSc2d1
rumunsko-bulharského	rumunsko-bulharský	k2eAgInSc2d1
úseku	úsek	k1gInSc2
významně	významně	k6eAd1
zlevnilo	zlevnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
za	za	k7c4
trajekt	trajekt	k1gInSc4
mezi	mezi	k7c7
Calafatem	Calafat	k1gInSc7
a	a	k8xC
Vidinem	Vidin	k1gInSc7
platili	platit	k5eAaImAgMnP
řidiči	řidič	k1gMnPc1
26	#num#	k4
eur	euro	k1gNnPc2
<g/>
,	,	kIx,
mýtné	mýtné	k1gNnSc1
na	na	k7c6
mostě	most	k1gInSc6
činí	činit	k5eAaImIp3nS
pouze	pouze	k6eAd1
6	#num#	k4
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poplatky	poplatek	k1gInPc1
</s>
<s>
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
nebylo	být	k5eNaImAgNnS
použití	použití	k1gNnSc1
mostu	most	k1gInSc2
zpoplatněné	zpoplatněný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
byl	být	k5eAaImAgMnS
zákonem	zákon	k1gInSc7
ustanoven	ustanoven	k2eAgInSc1d1
následující	následující	k2eAgInSc1d1
ceník	ceník	k1gInSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
:	:	kIx,
</s>
<s>
Vozidlo	vozidlo	k1gNnSc1
</s>
<s>
eur	euro	k1gNnPc2
</s>
<s>
levů	lev	k1gInPc2
</s>
<s>
leu	leu	k?
</s>
<s>
Do	do	k7c2
8	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
sedadel	sedadlo	k1gNnPc2
<g/>
;	;	kIx,
do	do	k7c2
3,5	3,5	k4
tuny	tuna	k1gFnSc2
</s>
<s>
6	#num#	k4
eur	euro	k1gNnPc2
</s>
<s>
12	#num#	k4
levů	lev	k1gInPc2
</s>
<s>
27	#num#	k4
leu	leu	k?
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
do	do	k7c2
7,5	7,5	k4
tuny	tuna	k1gFnPc1
<g/>
;	;	kIx,
Vozidla	vozidlo	k1gNnPc1
s	s	k7c7
počtem	počet	k1gInSc7
sedadel	sedadlo	k1gNnPc2
mezi	mezi	k7c4
9	#num#	k4
a	a	k8xC
23	#num#	k4
</s>
<s>
12	#num#	k4
eur	euro	k1gNnPc2
</s>
<s>
23	#num#	k4
levů	lev	k1gInPc2
</s>
<s>
54	#num#	k4
leu	leu	k?
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
do	do	k7c2
12	#num#	k4
tuny	tuna	k1gFnSc2
</s>
<s>
18	#num#	k4
eur	euro	k1gNnPc2
</s>
<s>
35	#num#	k4
levů	lev	k1gInPc2
</s>
<s>
81	#num#	k4
leu	leu	k?
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
nad	nad	k7c4
12	#num#	k4
tuny	tuna	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
nápravy	náprava	k1gFnSc2
<g/>
;	;	kIx,
vozidla	vozidlo	k1gNnSc2
s	s	k7c7
více	hodně	k6eAd2
jako	jako	k8xS,k8xC
23	#num#	k4
sedadly	sedadlo	k1gNnPc7
</s>
<s>
25	#num#	k4
eur	euro	k1gNnPc2
</s>
<s>
49	#num#	k4
levů	lev	k1gInPc2
</s>
<s>
113	#num#	k4
leu	leu	k?
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
nad	nad	k7c4
12	#num#	k4
tuny	tuna	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
náprav	náprava	k1gFnPc2
</s>
<s>
37	#num#	k4
eur	euro	k1gNnPc2
</s>
<s>
72	#num#	k4
levů	lev	k1gInPc2
</s>
<s>
167	#num#	k4
leu	leu	k?
</s>
<s>
Chodci	chodec	k1gMnPc1
a	a	k8xC
cyklisté	cyklista	k1gMnPc1
</s>
<s>
zdarma	zdarma	k6eAd1
</s>
<s>
zdarma	zdarma	k6eAd1
</s>
<s>
zdarma	zdarma	k6eAd1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
New	New	k1gFnSc2
Europe	Europ	k1gInSc5
Bridge	Bridge	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Most	most	k1gInSc1
Nové	Nová	k1gFnSc2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
symbol	symbol	k1gInSc4
Evropské	evropský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
Info	Info	k1gMnSc1
<g/>
.	.	kIx.
www.ostravainfo.cz	www.ostravainfo.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgInSc4d1
most	most	k1gInSc4
přes	přes	k7c4
Dunaj	Dunaj	k1gInSc4
spojuje	spojovat	k5eAaImIp3nS
bulharský	bulharský	k2eAgMnSc1d1
Vidin	vidina	k1gFnPc2
a	a	k8xC
rumunský	rumunský	k2eAgMnSc1d1
Calafat	Calafat	k1gMnSc1
<g/>
↑	↑	k?
Komisař	komisař	k1gMnSc1
Hahn	Hahn	k1gMnSc1
vítá	vítat	k5eAaImIp3nS
otevření	otevření	k1gNnSc4
„	„	k?
<g/>
mostu	most	k1gInSc2
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
“	“	k?
jako	jako	k8xS,k8xC
významný	významný	k2eAgInSc1d1
symbol	symbol	k1gInSc1
evropské	evropský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
<g/>
,	,	kIx,
Evropská	evropský	k2eAgFnSc1d1
Komise	komise	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2013	#num#	k4
<g/>
↑	↑	k?
Most	most	k1gInSc1
Calafat-Vidin	Calafat-Vidin	k2eAgInSc1d1
jaro	jaro	k6eAd1
v	v	k7c6
rumunské	rumunský	k2eAgFnSc6d1
infrastruktuře	infrastruktura	k1gFnSc6
nedělá	dělat	k5eNaImIp3nS
<g/>
,	,	kIx,
PressEurop	PressEurop	k1gInSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
www.presseurop.eu	www.presseurop.eus	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dunaj	Dunaj	k1gInSc1
na	na	k7c6
bulharsko-rumunské	bulharsko-rumunský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
překlenul	překlenout	k5eAaPmAgMnS
teprve	teprve	k6eAd1
druhý	druhý	k4xOgInSc4
obří	obří	k2eAgInSc4d1
most	most	k1gInSc4
iDnes	iDnesa	k1gFnPc2
CZ	CZ	kA
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
↑	↑	k?
Lodě	loď	k1gFnPc1
už	už	k6eAd1
nejsou	být	k5eNaImIp3nP
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
u	u	k7c2
Calafatu	Calafat	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
stojí	stát	k5eAaImIp3nS
vytoužený	vytoužený	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Televize	televize	k1gFnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2013	#num#	k4
↑	↑	k?
Guvernul	Guvernul	k1gFnPc2
a	a	k8xC
stabilit	stabilita	k1gFnPc2
taxele	taxel	k1gInSc2
pentru	pentr	k1gInSc2
trecerea	trecerea	k6eAd1
peste	peste	k5eAaPmIp2nP
podul	podout	k5eAaPmAgMnS
de	de	k?
la	la	k1gNnSc4
Calafat	Calafat	k1gFnSc2
Vidin	vidina	k1gFnPc2
<g/>
,	,	kIx,
Mediafax	Mediafax	k1gInSc1
<g/>
.	.	kIx.
<g/>
ro	ro	k?
13	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
(	(	kIx(
<g/>
rumunsky	rumunsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
FCC	FCC	kA
<g/>
,	,	kIx,
stavební	stavební	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
FaceBook	FaceBook	k1gInSc1
Most	most	k1gInSc1
Nové	Nové	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
SkyScraperCity	SkyScraperCita	k1gFnPc1
forum	forum	k1gNnSc1
Danube	danub	k1gInSc5
Bridge	Bridge	k1gNnPc1
II	II	kA
(	(	kIx(
<g/>
in	in	k?
English	English	k1gInSc1
<g/>
)	)	kIx)
</s>
