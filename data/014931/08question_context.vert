<s>
Arnold	Arnold	k1gMnSc1
(	(	kIx(
<g/>
Metallspielwarenfabrik	Metallspielwarenfabrik	k1gMnSc1
K.	K.	kA
Arnold	Arnold	k1gMnSc1
GmbH	GmbH	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
plechových	plechový	k2eAgFnPc2d1
hraček	hračka	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
modelové	modelový	k2eAgFnPc4d1
železnice	železnice	k1gFnPc4
se	s	k7c7
skoro	skoro	k6eAd1
stoletou	stoletý	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Podnik	podnik	k1gInSc1
byl	být	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1906	#num#	k4
založen	založit	k5eAaPmNgInS
Karlem	Karel	k1gMnSc7
Arnoldem	Arnold	k1gMnSc7
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
<g/>
.	.	kIx.
</s>