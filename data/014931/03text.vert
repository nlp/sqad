<s>
Arnold	Arnold	k1gMnSc1
(	(	kIx(
<g/>
firma	firma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Metallspielwarenfabrik	Metallspielwarenfabrik	k1gMnSc1
K.	K.	kA
Arnold	Arnold	k1gMnSc1
GmbH	GmbH	k1gMnSc1
&	&	k?
Co	co	k8xS
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
GmbH	GmbH	k?
&	&	k?
Co	co	k9
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1906	#num#	k4
Datum	datum	k1gNnSc1
zániku	zánik	k1gInSc2
</s>
<s>
1997	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
výroba	výroba	k1gFnSc1
převzatá	převzatý	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
Hornby	Hornba	k1gFnSc2
Hobbies	Hobbies	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Karl	Karl	k1gMnSc1
Arnold	Arnold	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Mühlhausen	Mühlhausen	k1gInSc1
<g/>
,	,	kIx,
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
Bavorsko	Bavorsko	k1gNnSc4
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Norimberk	Norimberk	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
hračkářský	hračkářský	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
modelová	modelový	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Hornby	Hornba	k1gMnSc2
Majitel	majitel	k1gMnSc1
</s>
<s>
Hornby	Hornba	k1gFnPc1
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc4
</s>
<s>
Stránky	stránka	k1gFnPc1
značky	značka	k1gFnSc2
Arnold	Arnold	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Hornby	Hornba	k1gFnSc2
International	International	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Arnold	Arnold	k1gMnSc1
(	(	kIx(
<g/>
Metallspielwarenfabrik	Metallspielwarenfabrik	k1gMnSc1
K.	K.	kA
Arnold	Arnold	k1gMnSc1
GmbH	GmbH	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
plechových	plechový	k2eAgFnPc2d1
hraček	hračka	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
modelové	modelový	k2eAgFnPc4d1
železnice	železnice	k1gFnPc4
se	s	k7c7
skoro	skoro	k6eAd1
stoletou	stoletý	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Podnik	podnik	k1gInSc1
byl	být	k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1906	#num#	k4
založen	založit	k5eAaPmNgInS
Karlem	Karel	k1gMnSc7
Arnoldem	Arnold	k1gMnSc7
v	v	k7c6
Norimberku	Norimberk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
vyráběl	vyrábět	k5eAaImAgInS
hlavně	hlavně	k9
plechové	plechový	k2eAgFnPc4d1
hračky	hračka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
sv.	sv.	kA
války	válek	k1gInPc1
byly	být	k5eAaImAgInP
zničeny	zničit	k5eAaPmNgInP
všechny	všechen	k3xTgInPc1
norimberské	norimberský	k2eAgInPc1d1
výrobní	výrobní	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podniku	podnik	k1gInSc2
zůstal	zůstat	k5eAaPmAgInS
jen	jen	k9
pobočný	pobočný	k2eAgInSc1d1
závod	závod	k1gInSc1
v	v	k7c4
Mühlhausenu	Mühlhausen	k2eAgFnSc4d1
v	v	k7c6
Oberpfalzu	Oberpfalz	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
výroba	výroba	k1gFnSc1
obnovila	obnovit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Pozvolným	pozvolný	k2eAgInSc7d1
poklesem	pokles	k1gInSc7
odbytu	odbyt	k1gInSc2
plechových	plechový	k2eAgFnPc2d1
hraček	hračka	k1gFnPc2
<g/>
,	,	kIx,
přešel	přejít	k5eAaPmAgInS
podnik	podnik	k1gInSc1
koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
na	na	k7c4
výrobu	výroba	k1gFnSc4
modelové	modelový	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byly	být	k5eAaImAgInP
na	na	k7c6
norimberském	norimberský	k2eAgInSc6d1
veletrhu	veletrh	k1gInSc6
hraček	hračka	k1gFnPc2
prvně	prvně	k?
představeny	představen	k2eAgInPc4d1
modely	model	k1gInPc4
s	s	k7c7
rozchodem	rozchod	k1gInSc7
kolejí	kolej	k1gFnSc7
9	#num#	k4
mm	mm	kA
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
200	#num#	k4
pod	pod	k7c7
označením	označení	k1gNnSc7
„	„	k?
<g/>
Arnold	Arnold	k1gMnSc1
Rapido	Rapida	k1gFnSc5
200	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
Arnold	Arnold	k1gMnSc1
zakládá	zakládat	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
„	„	k?
<g/>
N	N	kA
<g/>
“	“	k?
označenou	označený	k2eAgFnSc4d1
podle	podle	k7c2
rozchodu	rozchod	k1gInSc2
kolejí	kolej	k1gFnPc2
–	–	k?
„	„	k?
<g/>
neun	neun	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
devět	devět	k4xCc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
byly	být	k5eAaImAgInP
modely	model	k1gInPc1
„	„	k?
<g/>
Arnold	Arnold	k1gMnSc1
Rapido	Rapida	k1gFnSc5
<g/>
“	“	k?
již	již	k6eAd1
zhotovovány	zhotovován	k2eAgInPc1d1
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
160	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
ustanoveno	ustanovit	k5eAaPmNgNnS
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
normě	norma	k1gFnSc6
NEM	NEM	kA
010	#num#	k4
pro	pro	k7c4
velikost	velikost	k1gFnSc4
„	„	k?
<g/>
N	N	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mnoha	mnoho	k4c6
úspěšných	úspěšný	k2eAgNnPc6d1
letech	léto	k1gNnPc6
musel	muset	k5eAaImAgMnS
Arnold	Arnold	k1gMnSc1
roku	rok	k1gInSc2
1995	#num#	k4
vyhlásit	vyhlásit	k5eAaPmF
platební	platební	k2eAgFnSc4d1
neschopnost	neschopnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1997	#num#	k4
byl	být	k5eAaImAgInS
podnik	podnik	k1gInSc1
převzat	převzít	k5eAaPmNgInS
italskou	italský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Rivarossi	Rivarosse	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byla	být	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
v	v	k7c4
Mühlhausenu	Mühlhausen	k2eAgFnSc4d1
zastavena	zastavit	k5eAaPmNgFnS
<g/>
,	,	kIx,
a	a	k8xC
formy	forma	k1gFnPc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
modelů	model	k1gInPc2
převzal	převzít	k5eAaPmAgInS
závod	závod	k1gInSc1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
i	i	k9
Rivarossi	Rivarosse	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
již	již	k6eAd1
převzala	převzít	k5eAaPmAgFnS
výrobu	výroba	k1gFnSc4
dalších	další	k2eAgFnPc2d1
značek	značka	k1gFnPc2
jako	jako	k8xC,k8xS
Jouef	Jouef	k1gInSc1
i	i	k8xC
Lima	Lima	k1gFnSc1
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
vyhlásit	vyhlásit	k5eAaPmF
roku	rok	k1gInSc2
2003	#num#	k4
platební	platební	k2eAgFnSc4d1
neschopnost	neschopnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
získala	získat	k5eAaPmAgFnS
roku	rok	k1gInSc2
2004	#num#	k4
anglická	anglický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Hornby	Hornba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
ve	v	k7c6
výrobním	výrobní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hornby	Hornba	k1gFnSc2
Railways	Railwaysa	k1gFnPc2
přesunula	přesunout	k5eAaPmAgFnS
výrobu	výroba	k1gFnSc4
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
jsou	být	k5eAaImIp3nP
opět	opět	k6eAd1
modely	model	k1gInPc1
pod	pod	k7c7
původní	původní	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
Arnold	Arnold	k1gMnSc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
původními	původní	k2eAgInPc7d1
výrobky	výrobek	k1gInPc7
Jouef	Jouef	k1gInSc1
<g/>
,	,	kIx,
Lima	Lima	k1gFnSc1
a	a	k8xC
Rivarossi	Rivarosse	k1gFnSc4
<g/>
,	,	kIx,
nabízeny	nabízen	k2eAgFnPc1d1
pod	pod	k7c7
označením	označení	k1gNnSc7
Hornby	Hornba	k1gMnSc2
International	International	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Arnold	Arnold	k1gMnSc1
vyráběl	vyrábět	k5eAaImAgMnS
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
modely	model	k1gInPc1
ve	v	k7c6
velikosti	velikost	k1gFnSc6
N.	N.	kA
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
rozšířil	rozšířit	k5eAaPmAgInS
nabídku	nabídka	k1gFnSc4
o	o	k7c4
model	model	k1gInSc4
ve	v	k7c6
velikosti	velikost	k1gFnSc6
TT	TT	kA
v	v	k7c6
několika	několik	k4yIc2
variantách	varianta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
kolejí	kolej	k1gFnPc2
vyráběl	vyrábět	k5eAaImAgMnS
Arnold	Arnold	k1gMnSc1
v	v	k7c6
omezeném	omezený	k2eAgNnSc6d1
množství	množství	k1gNnSc6
také	také	k9
příslušenství	příslušenství	k1gNnSc2
v	v	k7c6
měřítku	měřítko	k1gNnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
160	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	s	k7c7
především	především	k6eAd1
o	o	k7c4
stavby	stavba	k1gFnPc4
a	a	k8xC
silniční	silniční	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Arnold_	Arnold_	k1gFnSc2
<g/>
(	(	kIx(
<g/>
Unternehmen	Unternehmen	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Arnold	Arnold	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
Hornby	Hornba	k1gMnSc2
International	International	k1gMnSc2
se	s	k7c7
značkami	značka	k1gFnPc7
Arnold	Arnold	k1gMnSc1
<g/>
,	,	kIx,
Lima	Lima	k1gFnSc1
<g/>
,	,	kIx,
Jouef	Jouef	k1gInSc1
a	a	k8xC
Rivarossi	Rivarosse	k1gFnSc3
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
