<p>
<s>
Kraken	Kraken	k1gInSc1	Kraken
je	být	k5eAaImIp3nS	být
bájný	bájný	k2eAgMnSc1d1	bájný
obří	obří	k2eAgMnSc1d1	obří
hlavonožec	hlavonožec	k1gMnSc1	hlavonožec
schopný	schopný	k2eAgMnSc1d1	schopný
potápět	potápět	k5eAaImF	potápět
celé	celý	k2eAgFnPc4d1	celá
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
zpráv	zpráva	k1gFnPc2	zpráva
býval	bývat	k5eAaImAgInS	bývat
spatřen	spatřit	k5eAaPmNgInS	spatřit
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
obrovské	obrovský	k2eAgFnSc6d1	obrovská
mořské	mořský	k2eAgFnSc6d1	mořská
příšeře	příšera	k1gFnSc6	příšera
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
chapadly	chapadlo	k1gNnPc7	chapadlo
kolovaly	kolovat	k5eAaImAgInP	kolovat
mezi	mezi	k7c4	mezi
námořníky	námořník	k1gMnPc4	námořník
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
písemné	písemný	k2eAgFnSc2d1	písemná
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
krakenovi	kraken	k1gMnSc6	kraken
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
bergenského	bergenský	k2eAgMnSc2d1	bergenský
biskupa	biskup	k1gMnSc2	biskup
Erika	Erik	k1gMnSc2	Erik
Pontoppidana	Pontoppidan	k1gMnSc2	Pontoppidan
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
námořníci	námořník	k1gMnPc1	námořník
znali	znát	k5eAaImAgMnP	znát
dobře	dobře	k6eAd1	dobře
chobotnice	chobotnice	k1gFnSc2	chobotnice
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
hlavonožce	hlavonožec	k1gMnPc4	hlavonožec
s	s	k7c7	s
chapadly	chapadlo	k1gNnPc7	chapadlo
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
krakena	kraken	k1gMnSc4	kraken
za	za	k7c4	za
odlišný	odlišný	k2eAgInSc4d1	odlišný
druh	druh	k1gInSc4	druh
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Severská	severský	k2eAgFnSc1d1	severská
mytologie	mytologie	k1gFnSc1	mytologie
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Pontoppidan	Pontoppidan	k1gMnSc1	Pontoppidan
===	===	k?	===
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
kraken	krakna	k1gFnPc2	krakna
se	se	k3xPyFc4	se
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
ze	z	k7c2	z
severských	severský	k2eAgFnPc2d1	severská
ság	sága	k1gFnPc2	sága
neobjevilo	objevit	k5eNaPmAgNnS	objevit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
ale	ale	k8xC	ale
popisována	popisován	k2eAgNnPc1d1	popisováno
podobná	podobný	k2eAgNnPc1d1	podobné
mořská	mořský	k2eAgNnPc1d1	mořské
stvoření	stvoření	k1gNnPc1	stvoření
-	-	kIx~	-
hafgufa	hafguf	k1gMnSc4	hafguf
a	a	k8xC	a
lyngbakr	lyngbakr	k1gInSc4	lyngbakr
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
zápisky	zápisek	k1gInPc1	zápisek
biskupa	biskup	k1gMnSc2	biskup
Pontoppidana	Pontoppidan	k1gMnSc2	Pontoppidan
krakena	kraken	k1gMnSc2	kraken
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
zvíře	zvíře	k1gNnSc4	zvíře
"	"	kIx"	"
<g/>
velikosti	velikost	k1gFnSc2	velikost
plovoucího	plovoucí	k2eAgInSc2d1	plovoucí
ostrova	ostrov	k1gInSc2	ostrov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
mořeplavce	mořeplavec	k1gMnSc4	mořeplavec
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
tak	tak	k6eAd1	tak
ve	v	k7c6	v
stvoření	stvoření	k1gNnSc6	stvoření
samotném	samotný	k2eAgNnSc6d1	samotné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spíše	spíše	k9	spíše
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
víru	vír	k1gInSc6	vír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
svým	svůj	k3xOyFgNnSc7	svůj
rychlým	rychlý	k2eAgNnSc7d1	rychlé
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Pontoppidan	Pontoppidan	k1gMnSc1	Pontoppidan
popsal	popsat	k5eAaPmAgMnS	popsat
ničivou	ničivý	k2eAgFnSc4d1	ničivá
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
kraken	kraken	k1gInSc1	kraken
schopen	schopen	k2eAgInSc1d1	schopen
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
popadnout	popadnout	k5eAaPmF	popadnout
i	i	k9	i
tu	ten	k3xDgFnSc4	ten
největší	veliký	k2eAgFnSc4d3	veliký
válečnou	válečný	k2eAgFnSc4d1	válečná
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
ji	on	k3xPp3gFnSc4	on
stáhnout	stáhnout	k5eAaPmF	stáhnout
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kraken	Kraken	k1gInSc1	Kraken
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgInS	lišit
i	i	k9	i
od	od	k7c2	od
mořských	mořský	k2eAgMnPc2d1	mořský
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pontoppidana	Pontoppidana	k1gFnSc1	Pontoppidana
norští	norský	k2eAgMnPc1d1	norský
rybáři	rybář	k1gMnPc1	rybář
často	často	k6eAd1	často
riskovali	riskovat	k5eAaBmAgMnP	riskovat
a	a	k8xC	a
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
krakenem	krakeno	k1gNnSc7	krakeno
lovit	lovit	k5eAaImF	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
měl	mít	k5eAaImAgMnS	mít
rybář	rybář	k1gMnSc1	rybář
neobvykle	obvykle	k6eNd1	obvykle
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
říkávalo	říkávat	k5eAaImAgNnS	říkávat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
určitě	určitě	k6eAd1	určitě
rybařil	rybařit	k5eAaImAgMnS	rybařit
nad	nad	k7c7	nad
krakenem	kraken	k1gInSc7	kraken
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pontoppidan	Pontoppidan	k1gInSc1	Pontoppidan
dále	daleko	k6eAd2	daleko
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příšera	příšera	k1gFnSc1	příšera
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
zaměňována	zaměňovat	k5eAaImNgFnS	zaměňovat
s	s	k7c7	s
ostrovem	ostrov	k1gInSc7	ostrov
a	a	k8xC	a
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
mapy	mapa	k1gFnPc1	mapa
zakreslující	zakreslující	k2eAgInPc4d1	zakreslující
ostrovy	ostrov	k1gInPc4	ostrov
viditelné	viditelný	k2eAgInPc4d1	viditelný
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
krakeny	kraken	k2eAgFnPc1d1	kraken
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
mladý	mladý	k2eAgInSc1d1	mladý
kraken	kraken	k1gInSc1	kraken
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
vyplaven	vyplavit	k5eAaPmNgMnS	vyplavit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
oblasti	oblast	k1gFnSc6	oblast
Alstahaug	Alstahauga	k1gFnPc2	Alstahauga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kraken	kraken	k1gInSc1	kraken
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
řadou	řada	k1gFnSc7	řada
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
však	však	k9	však
jako	jako	k9	jako
obří	obří	k2eAgFnSc1d1	obří
příšera	příšera	k1gFnSc1	příšera
podobná	podobný	k2eAgFnSc1d1	podobná
chobotnici	chobotnice	k1gFnSc3	chobotnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
byla	být	k5eAaImAgFnS	být
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pontoppidanův	Pontoppidanův	k2eAgInSc1d1	Pontoppidanův
kraken	kraken	k1gInSc1	kraken
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
pozorováních	pozorování	k1gNnPc6	pozorování
velkých	velký	k2eAgFnPc2d1	velká
chobotnic	chobotnice	k1gFnPc2	chobotnice
námořníky	námořník	k1gMnPc4	námořník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgInPc6d1	dřívější
popisech	popis	k1gInPc6	popis
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
stvoření	stvoření	k1gNnSc1	stvoření
zobrazováno	zobrazován	k2eAgNnSc1d1	zobrazováno
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
krab	krab	k1gMnSc1	krab
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
chobotnicím	chobotnice	k1gFnPc3	chobotnice
byl	být	k5eAaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
velrybám	velryba	k1gFnPc3	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
krakenových	krakenův	k2eAgInPc2d1	krakenův
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patřilo	patřit	k5eAaImAgNnS	patřit
probublávání	probublávání	k1gNnSc1	probublávání
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
náhlá	náhlý	k2eAgNnPc4d1	náhlé
vynoření	vynoření	k1gNnPc4	vynoření
nových	nový	k2eAgInPc2d1	nový
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
podmořské	podmořský	k2eAgInPc1d1	podmořský
sopečné	sopečný	k2eAgInPc1d1	sopečný
aktivitě	aktivita	k1gFnSc3	aktivita
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
kraken	kraken	k1gInSc1	kraken
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Atlantidou	Atlantida	k1gFnSc7	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
byl	být	k5eAaImAgInS	být
obyvateli	obyvatel	k1gMnPc7	obyvatel
tohoto	tento	k3xDgInSc2	tento
mýtického	mýtický	k2eAgInSc2d1	mýtický
ostrova	ostrov	k1gInSc2	ostrov
záhadně	záhadně	k6eAd1	záhadně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
a	a	k8xC	a
používán	používán	k2eAgInSc1d1	používán
jako	jako	k8xS	jako
obranná	obranný	k2eAgFnSc1d1	obranná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nemusela	muset	k5eNaImAgFnS	muset
to	ten	k3xDgNnSc1	ten
také	také	k9	také
být	být	k5eAaImF	být
jen	jen	k9	jen
živá	živý	k2eAgFnSc1d1	živá
nesvůra	nesvůra	k1gFnSc1	nesvůra
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Kraken	Kraken	k1gInSc1	Kraken
poté	poté	k6eAd1	poté
údajně	údajně	k6eAd1	údajně
zahynul	zahynout	k5eAaPmAgMnS	zahynout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Atlantidou	Atlantida	k1gFnSc7	Atlantida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Montfortovy	Montfortův	k2eAgFnPc1d1	Montfortův
teorie	teorie	k1gFnPc1	teorie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
mořský	mořský	k2eAgMnSc1d1	mořský
biolog	biolog	k1gMnSc1	biolog
Pierre	Pierr	k1gInSc5	Pierr
Denys	Denys	k1gInSc4	Denys
de	de	k?	de
Montfort	Montfort	k1gInSc1	Montfort
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
encyklopedickém	encyklopedický	k2eAgNnSc6d1	encyklopedické
díle	dílo	k1gNnSc6	dílo
o	o	k7c6	o
měkkýších	měkkýší	k2eAgFnPc6d1	měkkýší
Histoire	Histoir	k1gMnSc5	Histoir
Naturelle	Naturell	k1gMnSc5	Naturell
Générale	Général	k1gMnSc5	Général
et	et	k?	et
Particuliè	Particuliè	k1gMnPc1	Particuliè
des	des	k1gNnSc2	des
Mollusques	Mollusquesa	k1gFnPc2	Mollusquesa
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
obřích	obří	k2eAgFnPc2d1	obří
chobotnic	chobotnice	k1gFnPc2	chobotnice
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
chobotnice	chobotnice	k1gFnSc1	chobotnice
kraken	krakna	k1gFnPc2	krakna
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
kraken	kraken	k2eAgInSc4d1	kraken
octopus	octopus	k1gInSc4	octopus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spatřen	spatřit	k5eAaPmNgInS	spatřit
norskými	norský	k2eAgMnPc7d1	norský
námořníky	námořník	k1gMnPc7	námořník
<g/>
,	,	kIx,	,
americkými	americký	k2eAgMnPc7d1	americký
velrybáři	velrybář	k1gMnPc1	velrybář
a	a	k8xC	a
také	také	k6eAd1	také
Pliniem	Plinium	k1gNnSc7	Plinium
starším	starý	k2eAgNnSc7d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
chobotnice	chobotnice	k1gFnSc1	chobotnice
obří	obří	k2eAgFnSc1d1	obří
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
colossal	colossat	k5eAaPmAgMnS	colossat
octopus	octopus	k1gInSc4	octopus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
napadl	napadnout	k5eAaPmAgMnS	napadnout
plachetnici	plachetnice	k1gFnSc4	plachetnice
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Saint-Malo	Saint-Mala	k1gFnSc5	Saint-Mala
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Angoly	Angola	k1gFnSc2	Angola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Montfort	Montfort	k1gInSc1	Montfort
odvážil	odvážit	k5eAaPmAgInS	odvážit
vystoupit	vystoupit	k5eAaPmF	vystoupit
s	s	k7c7	s
odvážným	odvážný	k2eAgNnSc7d1	odvážné
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
:	:	kIx,	:
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
deset	deset	k4xCc4	deset
britských	britský	k2eAgNnPc2d1	Britské
válečných	válečný	k2eAgNnPc2d1	válečné
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
záhadně	záhadně	k6eAd1	záhadně
zmizela	zmizet	k5eAaPmAgFnS	zmizet
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
potopeno	potopit	k5eAaPmNgNnS	potopit
právě	právě	k9	právě
obřími	obří	k2eAgFnPc7d1	obří
chobotnicemi	chobotnice	k1gFnPc7	chobotnice
<g/>
.	.	kIx.	.
</s>
<s>
Naneštěstí	naneštěstí	k9	naneštěstí
pro	pro	k7c4	pro
Montforta	Montfort	k1gMnSc4	Montfort
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
loďmi	loď	k1gFnPc7	loď
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
mělo	mít	k5eAaImAgNnS	mít
velmi	velmi	k6eAd1	velmi
neblahé	blahý	k2eNgInPc4d1	neblahý
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc4	pověst
Denise	Denisa	k1gFnSc3	Denisa
de	de	k?	de
Montforta	Montforta	k1gFnSc1	Montforta
šla	jít	k5eAaImAgFnS	jít
rychle	rychle	k6eAd1	rychle
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
a	a	k8xC	a
Montfort	Montfort	k1gInSc1	Montfort
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc4	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
teorií	teorie	k1gFnPc2	teorie
o	o	k7c4	o
chobotnici	chobotnice	k1gFnSc4	chobotnice
krakenovi	krakenův	k2eAgMnPc1d1	krakenův
nejspíše	nejspíše	k9	nejspíše
směřovala	směřovat	k5eAaImAgFnS	směřovat
ke	k	k7c3	k
krakatici	krakatice	k1gFnSc3	krakatice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odkazy	odkaz	k1gInPc7	odkaz
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Tennyson	Tennyson	k1gMnSc1	Tennyson
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Montfortovým	Montfortův	k2eAgInSc7d1	Montfortův
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
svůj	svůj	k3xOyFgInSc4	svůj
sonet	sonet	k1gInSc4	sonet
Kraken	Kraken	k1gInSc1	Kraken
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc3	The
Kraken	Kraken	k2eAgInSc1d1	Kraken
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tennysonův	Tennysonův	k2eAgInSc4d1	Tennysonův
popis	popis	k1gInSc4	popis
nejspíš	nejspíš	k9	nejspíš
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
také	také	k9	také
Julese	Julese	k1gFnSc2	Julese
Verna	Vern	k1gInSc2	Vern
při	při	k7c6	při
popisování	popisování	k1gNnSc6	popisování
obří	obří	k2eAgFnSc2d1	obří
chobotnice	chobotnice	k1gFnSc2	chobotnice
v	v	k7c6	v
románu	román	k1gInSc6	román
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaPmIp3nS	Vernout
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
často	často	k6eAd1	často
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
krakena	kraken	k1gMnSc4	kraken
a	a	k8xC	a
biskupa	biskup	k1gMnSc4	biskup
Pontoppidana	Pontoppidan	k1gMnSc4	Pontoppidan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
několik	několik	k4yIc4	několik
mrtvých	mrtvý	k1gMnPc2	mrtvý
"	"	kIx"	"
<g/>
krakenů	kraken	k1gMnPc2	kraken
<g/>
"	"	kIx"	"
vyplaveno	vyplavit	k5eAaPmNgNnS	vyplavit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
začali	začít	k5eAaPmAgMnP	začít
vědci	vědec	k1gMnPc1	vědec
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Kraken	Kraken	k1gInSc1	Kraken
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
živočišný	živočišný	k2eAgInSc4d1	živočišný
druh	druh	k1gInSc4	druh
<g/>
:	:	kIx,	:
krakatici	krakatice	k1gFnSc4	krakatice
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejzáhadnějších	záhadný	k2eAgMnPc2d3	nejzáhadnější
obřích	obří	k2eAgMnPc2d1	obří
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc7	vědec
snažili	snažit	k5eAaImAgMnP	snažit
pozorovat	pozorovat	k5eAaImF	pozorovat
krakatice	krakatice	k1gFnPc4	krakatice
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
pokusy	pokus	k1gInPc4	pokus
však	však	k9	však
většinou	většinou	k6eAd1	většinou
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
krakatice	krakatice	k1gFnSc2	krakatice
spatřila	spatřit	k5eAaPmAgFnS	spatřit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
námořníky	námořník	k1gMnPc4	námořník
nebo	nebo	k8xC	nebo
rybáře	rybář	k1gMnSc4	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Živá	živá	k1gFnSc1	živá
setkání	setkání	k1gNnPc2	setkání
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
tvorem	tvor	k1gMnSc7	tvor
proto	proto	k8xC	proto
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
vědecky	vědecky	k6eAd1	vědecky
nepotvrzená	potvrzený	k2eNgFnSc1d1	nepotvrzená
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
živé	živý	k2eAgFnSc2d1	živá
krakatice	krakatice	k1gFnSc2	krakatice
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgNnPc4	všechen
fakta	faktum	k1gNnPc4	faktum
o	o	k7c6	o
těchto	tento	k3xDgMnPc6	tento
hlavonožcích	hlavonožec	k1gMnPc6	hlavonožec
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2004	[number]	k4	2004
se	se	k3xPyFc4	se
japonským	japonský	k2eAgMnPc3d1	japonský
vědcům	vědec	k1gMnPc3	vědec
konečně	konečně	k6eAd1	konečně
podařilo	podařit	k5eAaPmAgNnS	podařit
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
krakatici	krakatice	k1gFnSc4	krakatice
nalákat	nalákat	k5eAaPmF	nalákat
na	na	k7c4	na
návnadu	návnada	k1gFnSc4	návnada
a	a	k8xC	a
pořídit	pořídit	k5eAaPmF	pořídit
přes	přes	k7c4	přes
500	[number]	k4	500
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
krakatici	krakatice	k1gFnSc3	krakatice
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
k	k	k7c3	k
návnadě	návnada	k1gFnSc3	návnada
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
přichyceno	přichycen	k2eAgNnSc1d1	přichyceno
její	její	k3xOp3gMnSc1	její
utržené	utržený	k2eAgFnPc1d1	utržená
5,5	[number]	k4	5,5
m	m	kA	m
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
chapadlo	chapadlo	k1gNnSc1	chapadlo
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnSc2	jaký
velikosti	velikost	k1gFnSc2	velikost
mohou	moct	k5eAaImIp3nP	moct
krakatice	krakatice	k1gFnPc1	krakatice
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
dorůst	dorůst	k5eAaPmF	dorůst
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jizev	jizva	k1gFnPc2	jizva
vorvaňů	vorvaň	k1gMnPc2	vorvaň
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozhodně	rozhodně	k6eAd1	rozhodně
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
kterýkoli	kterýkoli	k3yIgMnSc1	kterýkoli
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
exemplářů	exemplář	k1gInPc2	exemplář
vyplavených	vyplavený	k2eAgFnPc2d1	vyplavená
mořem	moře	k1gNnSc7	moře
-	-	kIx~	-
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
18	[number]	k4	18
m.	m.	k?	m.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
největšího	veliký	k2eAgMnSc2d3	veliký
hlavonožce	hlavonožec	k1gMnSc2	hlavonožec
považován	považován	k2eAgInSc1d1	považován
Kalmar	Kalmar	k1gInSc1	Kalmar
Hamiltonův	Hamiltonův	k2eAgInSc1d1	Hamiltonův
(	(	kIx(	(
<g/>
Mesonychoteuthis	Mesonychoteuthis	k1gInSc1	Mesonychoteuthis
hamiltoni	hamiltoň	k1gFnSc3	hamiltoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
antarktických	antarktický	k2eAgNnPc6d1	antarktické
mořích	moře	k1gNnPc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Cryptid	Cryptid	k1gInSc1	Cryptid
ZOO	zoo	k1gFnSc2	zoo
-	-	kIx~	-
Kraken	Kraken	k1gInSc1	Kraken
</s>
</p>
