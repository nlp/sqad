<p>
<s>
Ojos	Ojos	k1gInSc1	Ojos
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
(	(	kIx(	(
<g/>
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Nevado	Nevada	k1gFnSc5	Nevada
Ojos	Ojos	k1gInSc1	Ojos
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
6893	[number]	k4	6893
m	m	kA	m
druhým	druhý	k4xOgInSc7	druhý
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
horou	hora	k1gFnSc7	hora
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
aktivním	aktivní	k2eAgInSc7d1	aktivní
vulkánem	vulkán	k1gInSc7	vulkán
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Chile	Chile	k1gNnSc2	Chile
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
východní	východní	k2eAgFnSc2d1	východní
hranice	hranice	k1gFnSc2	hranice
pouště	poušť	k1gFnSc2	poušť
Atacama	Atacama	k1gFnSc1	Atacama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
sopka	sopka	k1gFnSc1	sopka
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
i	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
bývají	bývat	k5eAaImIp3nP	bývat
svahy	svah	k1gInPc1	svah
hory	hora	k1gFnSc2	hora
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
sněhem	sníh	k1gInSc7	sníh
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ojos	Ojosa	k1gFnPc2	Ojosa
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ojos	Ojos	k1gInSc1	Ojos
del	del	k?	del
Salado	Salada	k1gFnSc5	Salada
na	na	k7c4	na
Peakware	Peakwar	k1gInSc5	Peakwar
</s>
</p>
