<s>
Windhoek	Windhoek	k1gInSc1	Windhoek
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
namašsky	namašsky	k6eAd1	namašsky
Ai-Gams	Ai-Gamsa	k1gFnPc2	Ai-Gamsa
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
také	také	k9	také
Windhuk	Windhuk	k1gInSc1	Windhuk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
450	[number]	k4	450
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
1656	[number]	k4	1656
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
Khomas	Khomasa	k1gFnPc2	Khomasa
Highland	Highlanda	k1gFnPc2	Highlanda
poblíž	poblíž	k7c2	poblíž
horského	horský	k2eAgInSc2d1	horský
masivu	masiv	k1gInSc2	masiv
Auasberg	Auasberg	k1gMnSc1	Auasberg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
sídlo	sídlo	k1gNnSc4	sídlo
katolického	katolický	k2eAgNnSc2d1	katolické
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
a	a	k8xC	a
anglikánského	anglikánský	k2eAgNnSc2d1	anglikánské
biskupství	biskupství	k1gNnSc2	biskupství
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
je	být	k5eAaImIp3nS	být
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
zde	zde	k6eAd1	zde
i	i	k9	i
tepelné	tepelný	k2eAgFnPc4d1	tepelná
elektrárny	elektrárna	k1gFnPc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Windhoek	Windhoek	k6eAd1	Windhoek
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ovčími	ovčí	k2eAgFnPc7d1	ovčí
kůžemi	kůže	k1gFnPc7	kůže
<g/>
)	)	kIx)	)
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
zpracovatelského	zpracovatelský	k2eAgInSc2d1	zpracovatelský
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
mlékárna	mlékárna	k1gFnSc1	mlékárna
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
rybí	rybí	k2eAgFnPc4d1	rybí
konzervy	konzerva	k1gFnPc4	konzerva
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
německé	německý	k2eAgFnSc2d1	německá
kolonizace	kolonizace	k1gFnSc2	kolonizace
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
město	město	k1gNnSc1	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
značný	značný	k2eAgInSc4d1	značný
územní	územní	k2eAgInSc4d1	územní
rozmach	rozmach	k1gInSc4	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
osady	osada	k1gFnSc2	osada
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
náčelník	náčelník	k1gMnSc1	náčelník
Jan	Jan	k1gMnSc1	Jan
Jonker	Jonker	k1gMnSc1	Jonker
Afrikaner	Afrikaner	k1gMnSc1	Afrikaner
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
území	území	k1gNnSc2	území
obsadili	obsadit	k5eAaPmAgMnP	obsadit
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
Windhoek	Windhoek	k1gInSc1	Windhoek
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Německé	německý	k2eAgFnSc2d1	německá
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
první	první	k4xOgMnPc1	první
běloši	běloch	k1gMnPc1	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
město	město	k1gNnSc1	město
obsadila	obsadit	k5eAaPmAgFnS	obsadit
jihoafrická	jihoafrický	k2eAgNnPc1d1	jihoafrické
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Vznikem	vznik	k1gInSc7	vznik
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Namibie	Namibie	k1gFnSc2	Namibie
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
skončil	skončit	k5eAaPmAgInS	skončit
také	také	k9	také
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
i	i	k8xC	i
politický	politický	k2eAgInSc1d1	politický
vliv	vliv	k1gInSc1	vliv
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Lephalale	Lephalale	k6eAd1	Lephalale
Wetzlar	Wetzlar	k1gInSc1	Wetzlar
Trossingen	Trossingen	k1gInSc1	Trossingen
<g/>
,	,	kIx,	,
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1	Bádensko-Württembersko
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Berlín	Berlín	k1gInSc1	Berlín
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Havana	havana	k1gNnSc6	havana
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Harare	Harar	k1gMnSc5	Harar
Richmond	Richmonda	k1gFnPc2	Richmonda
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnPc1	Virginie
Brémy	Brémy	k1gFnPc1	Brémy
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Vantaa	Vanta	k1gInSc2	Vanta
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Windhoek	Windhoky	k1gFnPc2	Windhoky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Windhoek	Windhoek	k6eAd1	Windhoek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
