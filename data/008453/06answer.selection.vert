<s>
Chufuova	Chufuův	k2eAgFnSc1d1	Chufuova
pyramida	pyramida	k1gFnSc1	pyramida
(	(	kIx(	(
<g/>
též	též	k9	též
Cheopsova	Cheopsův	k2eAgFnSc1d1	Cheopsova
pyramida	pyramida	k1gFnSc1	pyramida
nebo	nebo	k8xC	nebo
Velká	velký	k2eAgFnSc1d1	velká
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
známá	známý	k2eAgFnSc1d1	známá
kamenná	kamenný	k2eAgFnSc1d1	kamenná
stavba	stavba	k1gFnSc1	stavba
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
období	období	k1gNnSc6	období
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
a	a	k8xC	a
současně	současně	k6eAd1	současně
jediný	jediný	k2eAgMnSc1d1	jediný
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zachovaný	zachovaný	k2eAgInSc4d1	zachovaný
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
