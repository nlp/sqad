<s>
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xC	jako
Izák	Izák	k1gMnSc1	Izák
Judovič	Judovič	k1gMnSc1	Judovič
Ozimov	Ozimov	k1gInSc1	Ozimov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Ю	Ю	k?	Ю
О	О	k?	О
<g/>
)	)	kIx)	)
asi	asi	k9	asi
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Petroviči	Petrovič	k1gMnSc6	Petrovič
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
