<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
galaxie	galaxie	k1gFnSc1	galaxie
z	z	k7c2	z
Místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
naši	náš	k3xOp1gFnSc4	náš
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
Galaxii	galaxie	k1gFnSc4	galaxie
v	v	k7c6	v
Trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
