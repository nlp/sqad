<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
je	být	k5eAaImIp3nS	být
spirální	spirální	k2eAgFnSc1d1	spirální
galaxie	galaxie	k1gFnSc1	galaxie
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
(	(	kIx(	(
<g/>
2,4	[number]	k4	2,4
<g/>
×	×	k?	×
<g/>
1019	[number]	k4	1019
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Andromedy	Andromeda	k1gMnSc2	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Messier	Messier	k1gInSc1	Messier
31	[number]	k4	31
<g/>
,	,	kIx,	,
M31	M31	k1gFnSc1	M31
a	a	k8xC	a
NGC	NGC	kA	NGC
224	[number]	k4	224
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
astronomické	astronomický	k2eAgFnSc6d1	astronomická
literatuře	literatura	k1gFnSc6	literatura
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
označení	označení	k1gNnSc4	označení
Velká	velký	k2eAgFnSc1d1	velká
mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
spirální	spirální	k2eAgFnSc1d1	spirální
galaxie	galaxie	k1gFnSc1	galaxie
od	od	k7c2	od
naší	náš	k3xOp1gFnSc2	náš
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
galaxií	galaxie	k1gFnSc7	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
oblasti	oblast	k1gFnSc6	oblast
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
mytologické	mytologický	k2eAgFnSc6d1	mytologická
princezně	princezna	k1gFnSc6	princezna
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
galaxie	galaxie	k1gFnSc1	galaxie
z	z	k7c2	z
Místní	místní	k2eAgFnSc2d1	místní
skupiny	skupina	k1gFnSc2	skupina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
naši	náš	k3xOp1gFnSc4	náš
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
Galaxii	galaxie	k1gFnSc4	galaxie
v	v	k7c6	v
Trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
nejhmotnější	hmotný	k2eAgInPc1d3	hmotný
<g/>
,	,	kIx,	,
nedávná	dávný	k2eNgNnPc1d1	nedávné
zjištění	zjištění	k1gNnPc1	zjištění
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
temné	temný	k2eAgFnPc1d1	temná
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nejhmotnější	hmotný	k2eAgFnSc7d3	nejhmotnější
galaxií	galaxie	k1gFnSc7	galaxie
v	v	k7c6	v
Místní	místní	k2eAgFnSc6d1	místní
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
M31	M31	k1gFnPc2	M31
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bilion	bilion	k4xCgInSc4	bilion
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
)	)	kIx)	)
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
až	až	k9	až
400	[number]	k4	400
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
<g/>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
7,1	[number]	k4	7,1
<g/>
×	×	k?	×
<g/>
1011	[number]	k4	1011
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
odhadla	odhadnout	k5eAaPmAgFnS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
M31	M31	k1gFnPc1	M31
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
odhadla	odhadnout	k5eAaPmAgFnS	odhadnout
hmotnost	hmotnost	k1gFnSc1	hmotnost
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
galaxie	galaxie	k1gFnPc1	galaxie
se	se	k3xPyFc4	se
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
začnou	začít	k5eAaPmIp3nP	začít
spojovat	spojovat	k5eAaImF	spojovat
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
obří	obří	k2eAgFnSc4d1	obří
eliptickou	eliptický	k2eAgFnSc4d1	eliptická
galaxii	galaxie	k1gFnSc4	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
má	mít	k5eAaImIp3nS	mít
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
3,4	[number]	k4	3,4
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejjasnějších	jasný	k2eAgInPc2d3	nejjasnější
objektů	objekt	k1gInPc2	objekt
Messierova	Messierův	k2eAgInSc2d1	Messierův
katalogu	katalog	k1gInSc2	katalog
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
spatřit	spatřit	k5eAaPmF	spatřit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
za	za	k7c4	za
bezměsíčné	bezměsíčný	k2eAgFnPc4d1	bezměsíčná
noci	noc	k1gFnPc4	noc
<g/>
,	,	kIx,	,
i	i	k8xC	i
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
světelným	světelný	k2eAgNnSc7d1	světelné
znečištěním	znečištění	k1gNnSc7	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
pořízených	pořízený	k2eAgInPc2d1	pořízený
větším	veliký	k2eAgInSc7d2	veliký
dalekohledem	dalekohled	k1gInSc7	dalekohled
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
šestkrát	šestkrát	k6eAd1	šestkrát
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
,	,	kIx,	,
viditelná	viditelný	k2eAgFnSc1d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
nebo	nebo	k8xC	nebo
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
či	či	k8xC	či
triedrem	triedr	k1gInSc7	triedr
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
její	její	k3xOp3gFnSc4	její
centrální	centrální	k2eAgFnSc4d1	centrální
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
doložený	doložený	k2eAgInSc1d1	doložený
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
pozorování	pozorování	k1gNnSc6	pozorování
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
964	[number]	k4	964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Knize	kniha	k1gFnSc6	kniha
stálic	stálice	k1gFnPc2	stálice
katalogizoval	katalogizovat	k5eAaImAgMnS	katalogizovat
perský	perský	k2eAgMnSc1d1	perský
astronom	astronom	k1gMnSc1	astronom
as-Súfí	as-Súfí	k1gMnSc1	as-Súfí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
mlhavý	mlhavý	k2eAgInSc4d1	mlhavý
obláček	obláček	k1gInSc4	obláček
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
ji	on	k3xPp3gFnSc4	on
Malý	malý	k2eAgInSc1d1	malý
mrak	mrak	k1gInSc1	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
podstatně	podstatně	k6eAd1	podstatně
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
z	z	k7c2	z
tohoto	tento	k3xDgMnSc2	tento
období	období	k1gNnPc2	období
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xC	jako
malý	malý	k2eAgInSc4d1	malý
obláček	obláček	k1gInSc4	obláček
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
popis	popis	k1gInSc1	popis
objektu	objekt	k1gInSc2	objekt
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc1	pozorování
dalekohledem	dalekohled	k1gInSc7	dalekohled
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
astronoma	astronom	k1gMnSc2	astronom
Simona	Simona	k1gFnSc1	Simona
Maria	Maria	k1gFnSc1	Maria
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1612	[number]	k4	1612
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Messier	Messier	k1gMnSc1	Messier
katalogizoval	katalogizovat	k5eAaImAgMnS	katalogizovat
galaxii	galaxie	k1gFnSc4	galaxie
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Messier	Messier	k1gInSc1	Messier
31	[number]	k4	31
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
objev	objev	k1gInSc1	objev
nesprávně	správně	k6eNd1	správně
připsal	připsat	k5eAaPmAgInS	připsat
Simonu	Simona	k1gFnSc4	Simona
Mariovi	Mariův	k2eAgMnPc1d1	Mariův
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
známo	znám	k2eAgNnSc1d1	známo
dílo	dílo	k1gNnSc1	dílo
as-Sufího	as-Sufí	k1gMnSc2	as-Sufí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
astronom	astronom	k1gMnSc1	astronom
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc4	Herschel
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
slabý	slabý	k2eAgInSc1d1	slabý
načervenalý	načervenalý	k2eAgInSc1d1	načervenalý
odstín	odstín	k1gInSc1	odstín
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
"	"	kIx"	"
<g/>
velkých	velký	k2eAgFnPc2d1	velká
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
mlhoviny	mlhovina	k1gFnSc2	mlhovina
nesprávně	správně	k6eNd1	správně
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
krát	krát	k6eAd1	krát
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
než	než	k8xS	než
Sirius	Sirius	k1gInSc1	Sirius
<g/>
.	.	kIx.	.
<g/>
William	William	k1gInSc1	William
Huggins	Huggins	k1gInSc1	Huggins
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
spektrum	spektrum	k1gNnSc4	spektrum
M31	M31	k1gFnSc2	M31
a	a	k8xC	a
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
spektra	spektrum	k1gNnSc2	spektrum
plynných	plynný	k2eAgFnPc2d1	plynná
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
M31	M31	k1gFnSc2	M31
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
kontinuita	kontinuita	k1gFnSc1	kontinuita
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
s	s	k7c7	s
tmavými	tmavý	k2eAgFnPc7d1	tmavá
spektrálními	spektrální	k2eAgFnPc7d1	spektrální
čarami	čára	k1gFnPc7	čára
a	a	k8xC	a
které	který	k3yIgNnSc4	který
napomáhají	napomáhat	k5eAaBmIp3nP	napomáhat
určit	určit	k5eAaPmF	určit
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
mlhoviny	mlhovina	k1gFnSc2	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
spektru	spektrum	k1gNnSc3	spektrum
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
M31	M31	k1gFnSc1	M31
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
v	v	k7c6	v
M31	M31	k1gFnSc6	M31
supernova	supernova	k1gFnSc1	supernova
(	(	kIx(	(
<g/>
označená	označený	k2eAgFnSc1d1	označená
jako	jako	k8xS	jako
S	s	k7c7	s
Andromedae	Andromedae	k1gFnSc7	Andromedae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
jediná	jediný	k2eAgFnSc1d1	jediná
supernova	supernova	k1gFnSc1	supernova
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
M31	M31	k1gFnSc1	M31
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
blízký	blízký	k2eAgInSc4d1	blízký
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
nebyla	být	k5eNaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
jasný	jasný	k2eAgInSc1d1	jasný
objekt	objekt	k1gInSc1	objekt
nesouvisející	související	k2eNgInSc1d1	nesouvisející
s	s	k7c7	s
novou	nova	k1gFnSc7	nova
a	a	k8xC	a
objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Nova	nova	k1gFnSc1	nova
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
fotografie	fotografie	k1gFnSc1	fotografie
M31	M31	k1gMnPc2	M31
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
Isaacem	Isaace	k1gMnSc7	Isaace
Robertsem	Roberts	k1gMnSc7	Roberts
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
soukromé	soukromý	k2eAgFnSc6d1	soukromá
observatoři	observatoř	k1gFnSc6	observatoř
v	v	k7c6	v
Sussexu	Sussex	k1gInSc6	Sussex
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhotrvající	dlouhotrvající	k2eAgFnSc1d1	dlouhotrvající
expozice	expozice	k1gFnSc1	expozice
poprvé	poprvé	k6eAd1	poprvé
umožnila	umožnit	k5eAaPmAgFnS	umožnit
spatřit	spatřit	k5eAaPmF	spatřit
spirální	spirální	k2eAgFnSc4d1	spirální
strukturu	struktura	k1gFnSc4	struktura
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
mlhovinou	mlhovina	k1gFnSc7	mlhovina
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
i	i	k9	i
Roberts	Roberts	k1gInSc1	Roberts
měl	mít	k5eAaImAgInS	mít
mylnou	mylný	k2eAgFnSc4d1	mylná
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
M31	M31	k1gMnPc1	M31
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
spirální	spirální	k2eAgFnPc1d1	spirální
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vznikající	vznikající	k2eAgFnSc2d1	vznikající
planetární	planetární	k2eAgFnSc2d1	planetární
soustavy	soustava	k1gFnSc2	soustava
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Radiální	radiální	k2eAgFnSc1d1	radiální
rychlost	rychlost	k1gFnSc1	rychlost
objektu	objekt	k1gInSc2	objekt
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
Sluneční	sluneční	k2eAgFnSc3d1	sluneční
soustavě	soustava	k1gFnSc3	soustava
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
změřena	změřit	k5eAaPmNgFnS	změřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
Vesto	vesta	k1gFnSc5	vesta
Slipher	Sliphra	k1gFnPc2	Sliphra
na	na	k7c6	na
Lowellově	Lowellův	k2eAgFnSc6d1	Lowellova
observatoři	observatoř	k1gFnSc6	observatoř
pomocí	pomocí	k7c2	pomocí
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ji	on	k3xPp3gFnSc4	on
Christiaan	Christiaan	k1gMnSc1	Christiaan
Huygens	Huygens	k1gInSc4	Huygens
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
otvor	otvor	k1gInSc4	otvor
do	do	k7c2	do
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
prostorů	prostor	k1gInPc2	prostor
nad	nad	k7c7	nad
oblohou	obloha	k1gFnSc7	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
proniká	pronikat	k5eAaImIp3nS	pronikat
světlo	světlo	k1gNnSc4	světlo
z	z	k7c2	z
věčného	věčný	k2eAgInSc2d1	věčný
dne	den	k1gInSc2	den
–	–	k?	–
tedy	tedy	k9	tedy
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
podle	podle	k7c2	podle
Bible	bible	k1gFnSc2	bible
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1612	[number]	k4	1612
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rozlišit	rozlišit	k5eAaPmF	rozlišit
spirální	spirální	k2eAgNnPc1d1	spirální
ramena	rameno	k1gNnPc1	rameno
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Mayrův	Mayrův	k2eAgInSc4d1	Mayrův
popis	popis	k1gInSc4	popis
převzal	převzít	k5eAaPmAgMnS	převzít
Charles	Charles	k1gMnSc1	Charles
Messier	Messier	k1gMnSc1	Messier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
katalogu	katalog	k1gInSc2	katalog
jako	jako	k9	jako
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
poprvé	poprvé	k6eAd1	poprvé
určil	určit	k5eAaPmAgMnS	určit
Edwin	Edwin	k1gMnSc1	Edwin
Hubble	Hubble	k1gMnSc1	Hubble
její	její	k3xOp3gFnSc4	její
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
cefeid	cefeida	k1gFnPc2	cefeida
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
objevem	objev	k1gInSc7	objev
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgFnPc4d1	další
spirální	spirální	k2eAgFnPc4d1	spirální
mlhoviny	mlhovina	k1gFnPc4	mlhovina
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
galaxiím	galaxie	k1gFnPc3	galaxie
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obrovské	obrovský	k2eAgInPc1d1	obrovský
systémy	systém	k1gInPc1	systém
mnoha	mnoho	k4c2	mnoho
milionů	milion	k4xCgInPc2	milion
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
–	–	k?	–
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
–	–	k?	–
podařilo	podařit	k5eAaPmAgNnS	podařit
rozlišit	rozlišit	k5eAaPmF	rozlišit
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
Edwinu	Edwin	k2eAgMnSc3d1	Edwin
Hubbleovi	Hubbleus	k1gMnSc3	Hubbleus
<g/>
;	;	kIx,	;
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
galaxie	galaxie	k1gFnSc2	galaxie
je	být	k5eAaImIp3nS	být
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
W.	W.	kA	W.
Baade	Baad	k1gInSc5	Baad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teorie	teorie	k1gFnSc1	teorie
ostrovních	ostrovní	k2eAgInPc2d1	ostrovní
vesmírů	vesmír	k1gInPc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Heber	Heber	k1gMnSc1	Heber
Curtis	Curtis	k1gInSc4	Curtis
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
novu	nova	k1gFnSc4	nova
v	v	k7c6	v
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Prohledal	prohledat	k5eAaPmAgInS	prohledat
starší	starý	k2eAgInPc4d2	starší
fotografické	fotografický	k2eAgInPc4d1	fotografický
záznamy	záznam	k1gInPc4	záznam
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
11	[number]	k4	11
dalších	další	k2eAgFnPc2d1	další
nov	nova	k1gFnPc2	nova
<g/>
.	.	kIx.	.
</s>
<s>
Curtis	Curtis	k1gInSc4	Curtis
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
novy	nova	k1gFnPc1	nova
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
10	[number]	k4	10
magnitud	magnitudo	k1gNnPc2	magnitudo
slabší	slabý	k2eAgMnSc1d2	slabší
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
odhadem	odhad	k1gInSc7	odhad
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
objektu	objekt	k1gInSc2	objekt
500	[number]	k4	500
tisíc	tisíc	k4xCgInSc1	tisíc
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
<g/>
×	×	k?	×
<g/>
1010	[number]	k4	1010
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
stoupencem	stoupenec	k1gMnSc7	stoupenec
hypotézy	hypotéza	k1gFnSc2	hypotéza
tzv.	tzv.	kA	tzv.
ostrovních	ostrovní	k2eAgInPc2d1	ostrovní
vesmírů	vesmír	k1gInPc2	vesmír
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
spirální	spirální	k2eAgFnPc1d1	spirální
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
Velká	velký	k2eAgFnSc1d1	velká
debata	debata	k1gFnSc1	debata
mezi	mezi	k7c7	mezi
Harlowem	Harlowum	k1gNnSc7	Harlowum
<g/>
,	,	kIx,	,
Shapleym	Shapleym	k1gInSc1	Shapleym
a	a	k8xC	a
Curtisem	Curtis	k1gInSc7	Curtis
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
spirálních	spirální	k2eAgFnPc6d1	spirální
mlhovinách	mlhovina	k1gFnPc6	mlhovina
a	a	k8xC	a
rozměrech	rozměr	k1gInPc6	rozměr
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svého	svůj	k3xOyFgNnSc2	svůj
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
galaxie	galaxie	k1gFnSc1	galaxie
mimo	mimo	k7c4	mimo
naši	náš	k3xOp1gFnSc4	náš
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
Curtis	Curtis	k1gFnSc2	Curtis
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nachází	nacházet	k5eAaImIp3nP	nacházet
tmavé	tmavý	k2eAgInPc4d1	tmavý
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
prachovým	prachový	k2eAgNnPc3d1	prachové
mračnům	mračno	k1gNnPc3	mračno
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
vlastní	vlastní	k2eAgFnSc6d1	vlastní
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gInSc1	její
výrazný	výrazný	k2eAgInSc1d1	výrazný
Dopplerův	Dopplerův	k2eAgInSc1d1	Dopplerův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
Ernst	Ernst	k1gMnSc1	Ernst
Öpik	Öpik	k1gMnSc1	Öpik
představil	představit	k5eAaPmAgMnS	představit
velmi	velmi	k6eAd1	velmi
elegantní	elegantní	k2eAgFnSc4d1	elegantní
a	a	k8xC	a
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
astrofyzikální	astrofyzikální	k2eAgFnSc4d1	astrofyzikální
metodu	metoda	k1gFnSc4	metoda
k	k	k7c3	k
odhadu	odhad	k1gInSc3	odhad
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výsledek	výsledek	k1gInSc1	výsledek
umístil	umístit	k5eAaPmAgInS	umístit
mlhovinu	mlhovina	k1gFnSc4	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
daleko	daleko	k6eAd1	daleko
mimo	mimo	k6eAd1	mimo
naší	náš	k3xOp1gFnSc3	náš
Galaxii	galaxie	k1gFnSc3	galaxie
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
parseků	parsec	k1gInPc2	parsec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Edwin	Edwin	k1gMnSc1	Edwin
Hubble	Hubble	k1gMnSc1	Hubble
ukončil	ukončit	k5eAaPmAgMnS	ukončit
debatu	debata	k1gFnSc4	debata
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
astronomických	astronomický	k2eAgFnPc6d1	astronomická
fotografiích	fotografia	k1gFnPc6	fotografia
M31	M31	k1gFnSc2	M31
proměnné	proměnná	k1gFnSc2	proměnná
hvězdy	hvězda	k1gFnSc2	hvězda
cefeidy	cefeida	k1gFnSc2	cefeida
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
Hookerova	Hookerův	k2eAgInSc2d1	Hookerův
dalekohledu	dalekohled	k1gInSc2	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Wilsonu	Wilson	k1gInSc2	Wilson
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Velké	velký	k2eAgFnSc2d1	velká
mlhoviny	mlhovina	k1gFnSc2	mlhovina
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Hubblova	Hubblův	k2eAgNnPc1d1	Hubblovo
měření	měření	k1gNnPc1	měření
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
prokázala	prokázat	k5eAaPmAgNnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
shlukem	shluk	k1gInSc7	shluk
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
oddělenou	oddělený	k2eAgFnSc7d1	oddělená
galaxií	galaxie	k1gFnSc7	galaxie
se	se	k3xPyFc4	se
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M31	M31	k4	M31
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
studiích	studie	k1gFnPc6	studie
o	o	k7c6	o
galaxiích	galaxie	k1gFnPc6	galaxie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
spirální	spirální	k2eAgFnSc7d1	spirální
galaxií	galaxie	k1gFnSc7	galaxie
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
galaxií	galaxie	k1gFnSc7	galaxie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
1943	[number]	k4	1943
Walter	Walter	k1gMnSc1	Walter
Baade	Baad	k1gInSc5	Baad
prvním	první	k4xOgMnSc7	první
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
rozlišit	rozlišit	k5eAaPmF	rozlišit
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
oblasti	oblast	k1gFnSc6	oblast
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgMnPc2	svůj
pozorování	pozorování	k1gNnPc2	pozorování
této	tento	k3xDgFnSc2	tento
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopný	schopný	k2eAgMnSc1d1	schopný
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
dva	dva	k4xCgInPc1	dva
odlišné	odlišný	k2eAgInPc1d1	odlišný
populace	populace	k1gFnSc2	populace
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnSc2	jejich
metalicity	metalicita	k1gFnSc2	metalicita
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
mladé	mladý	k1gMnPc4	mladý
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgFnPc1d1	rychlá
hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
disku	disk	k1gInSc6	disk
galaxie	galaxie	k1gFnSc2	galaxie
populace	populace	k1gFnSc2	populace
I	i	k9	i
a	a	k8xC	a
starší	starý	k2eAgFnPc1d2	starší
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
galaktické	galaktický	k2eAgFnSc6d1	Galaktická
výduti	výduť	k1gFnSc6	výduť
populace	populace	k1gFnSc2	populace
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
názvosloví	názvosloví	k1gNnSc1	názvosloví
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
přijato	přijmout	k5eAaPmNgNnS	přijmout
i	i	k9	i
pro	pro	k7c4	pro
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
galaxiích	galaxie	k1gFnPc6	galaxie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c6	na
existenci	existence	k1gFnSc6	existence
dvou	dva	k4xCgFnPc2	dva
odlišných	odlišný	k2eAgFnPc2d1	odlišná
populací	populace	k1gFnPc2	populace
hvězd	hvězda	k1gFnPc2	hvězda
bylo	být	k5eAaImAgNnS	být
poukázáno	poukázat	k5eAaPmNgNnS	poukázat
dříve	dříve	k6eAd2	dříve
Janem	Jan	k1gMnSc7	Jan
Oortem	Oort	k1gMnSc7	Oort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Baade	Baad	k1gInSc5	Baad
také	také	k9	také
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
cefeid	cefeida	k1gFnPc2	cefeida
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
v	v	k7c6	v
zdvojnásobení	zdvojnásobení	k1gNnSc6	zdvojnásobení
odhadu	odhad	k1gInSc2	odhad
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatních	ostatní	k2eAgFnPc2d1	ostatní
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radiové	radiový	k2eAgFnPc1d1	radiová
emise	emise	k1gFnPc1	emise
z	z	k7c2	z
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
Hanburym	Hanburymum	k1gNnPc2	Hanburymum
Brownem	Brown	k1gMnSc7	Brown
a	a	k8xC	a
Cyrilem	Cyril	k1gMnSc7	Cyril
Hazardem	hazard	k1gMnSc7	hazard
v	v	k7c4	v
Jodrell	Jodrell	k1gInSc4	Jodrell
Bank	banka	k1gFnPc2	banka
Observatory	Observator	k1gMnPc4	Observator
pomocí	pomocí	k7c2	pomocí
218	[number]	k4	218
palcového	palcový	k2eAgInSc2d1	palcový
Transit	transit	k1gInSc4	transit
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
,	,	kIx,	,
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dřívější	dřívější	k2eAgNnPc1d1	dřívější
pozorování	pozorování	k1gNnPc1	pozorování
byla	být	k5eAaImAgNnP	být
uskutečněna	uskutečnit	k5eAaPmNgNnP	uskutečnit
průkopníkem	průkopník	k1gMnSc7	průkopník
radioastronomie	radioastronomie	k1gFnPc1	radioastronomie
Grotem	grot	k1gInSc7	grot
Reberem	Reber	k1gMnSc7	Reber
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
neprůkazná	průkazný	k2eNgFnSc1d1	neprůkazná
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
řádově	řádově	k6eAd1	řádově
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
radiové	radiový	k2eAgFnPc1d1	radiová
mapy	mapa	k1gFnPc1	mapa
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Johnem	John	k1gMnSc7	John
Baldwinem	Baldwin	k1gMnSc7	Baldwin
a	a	k8xC	a
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
Radio	radio	k1gNnSc1	radio
Astronomy	astronom	k1gMnPc4	astronom
Group	Group	k1gInSc4	Group
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
radioastronomii	radioastronomie	k1gFnSc6	radioastronomie
označení	označení	k1gNnSc1	označení
2C	[number]	k4	2C
56	[number]	k4	56
v	v	k7c6	v
radioastronomickém	radioastronomický	k2eAgInSc6d1	radioastronomický
2C	[number]	k4	2C
katalogu	katalog	k1gInSc2	katalog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
objevena	objeven	k2eAgFnSc1d1	objevena
první	první	k4xOgFnSc1	první
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
.	.	kIx.	.
</s>
<s>
Kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
byl	být	k5eAaImAgInS	být
detekován	detekovat	k5eAaImNgInS	detekovat
pomocí	pomocí	k7c2	pomocí
techniky	technika	k1gFnSc2	technika
mikročoček	mikročočka	k1gFnPc2	mikročočka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
deformace	deformace	k1gFnSc1	deformace
světla	světlo	k1gNnSc2	světlo
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
průchodu	průchod	k1gInSc6	průchod
poblíž	poblíž	k7c2	poblíž
hmotného	hmotný	k2eAgInSc2d1	hmotný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Odhad	odhad	k1gInSc1	odhad
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
k	k	k7c3	k
Galaxii	galaxie	k1gFnSc3	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
byl	být	k5eAaImAgInS	být
zdvojnásoben	zdvojnásoben	k2eAgInSc1d1	zdvojnásoben
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
populace	populace	k1gFnPc1	populace
cefeid	cefeida	k1gFnPc2	cefeida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
měření	měření	k1gNnSc1	měření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
pomocí	pomocí	k7c2	pomocí
červených	červený	k2eAgMnPc2d1	červený
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
velkého	velký	k2eAgInSc2d1	velký
souboru	soubor	k1gInSc2	soubor
červených	červený	k2eAgFnPc2d1	červená
polních	polní	k2eAgFnPc2d1	polní
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
red	red	k?	red
clump	clump	k1gInSc1	clump
stars	stars	k1gInSc1	stars
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
družice	družice	k1gFnPc1	družice
Hipparcos	Hipparcos	k1gInSc4	Hipparcos
používala	používat	k5eAaImAgFnS	používat
ke	k	k7c3	k
kalibraci	kalibrace	k1gFnSc3	kalibrace
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
cefeid	cefeida	k1gFnPc2	cefeida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
práce	práce	k1gFnSc2	práce
týmu	tým	k1gInSc2	tým
astronomů	astronom	k1gMnPc2	astronom
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
M31	M31	k1gFnSc1	M31
stvořena	stvořit	k5eAaPmNgFnS	stvořit
z	z	k7c2	z
kolizí	kolize	k1gFnPc2	kolize
dvou	dva	k4xCgInPc2	dva
menších	malý	k2eAgFnPc2d2	menší
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
5	[number]	k4	5
až	až	k9	až
9	[number]	k4	9
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
zveřejněný	zveřejněný	k2eAgInSc1d1	zveřejněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
nastínil	nastínit	k5eAaPmAgMnS	nastínit
základní	základní	k2eAgFnSc4d1	základní
historii	historie	k1gFnSc4	historie
M31	M31	k1gFnSc2	M31
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
10	[number]	k4	10
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
sloučením	sloučení	k1gNnSc7	sloučení
mnoha	mnoho	k4c2	mnoho
menších	malý	k2eAgInPc2d2	menší
protogalaxií	protogalaxie	k1gFnSc7	protogalaxie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterou	který	k3yQgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
historii	historie	k1gFnSc6	historie
M31	M31	k1gFnSc2	M31
bylo	být	k5eAaImAgNnS	být
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgNnSc4d1	uvedené
sloučení	sloučení	k1gNnSc4	sloučení
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nastalo	nastat	k5eAaPmAgNnS	nastat
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kolize	kolize	k1gFnSc1	kolize
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
na	na	k7c4	na
kovy	kov	k1gInPc4	kov
bohatých	bohatý	k2eAgFnPc2d1	bohatá
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
halu	halo	k1gNnSc6	halo
galaxie	galaxie	k1gFnSc2	galaxie
a	a	k8xC	a
v	v	k7c6	v
rozšířeném	rozšířený	k2eAgInSc6d1	rozšířený
disku	disk	k1gInSc6	disk
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
epochy	epocha	k1gFnSc2	epocha
byla	být	k5eAaImAgFnS	být
tvorba	tvorba	k1gFnSc1	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
velmi	velmi	k6eAd1	velmi
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Luminous	Luminous	k1gInSc1	Luminous
Infrared	Infrared	k1gInSc4	Infrared
Galaxy	Galax	k1gInPc7	Galax
<g/>
,	,	kIx,	,
svítivou	svítivý	k2eAgFnSc7d1	svítivá
infračervenou	infračervený	k2eAgFnSc7d1	infračervená
galaxií	galaxie	k1gFnSc7	galaxie
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
měla	mít	k5eAaImAgFnS	mít
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
s	s	k7c7	s
Galaxií	galaxie	k1gFnSc7	galaxie
v	v	k7c6	v
Trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
velmi	velmi	k6eAd1	velmi
těsné	těsný	k2eAgNnSc4d1	těsné
přiblížení	přiblížení	k1gNnSc4	přiblížení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
mnoha	mnoho	k4c2	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
disku	disk	k1gInSc6	disk
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
některých	některý	k3yIgFnPc2	některý
jejich	jejich	k3xOp3gFnPc2	jejich
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
narušen	narušen	k2eAgInSc4d1	narušen
vnější	vnější	k2eAgInSc4d1	vnější
disk	disk	k1gInSc4	disk
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
tvorby	tvorba	k1gFnSc2	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
2	[number]	k4	2
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
měla	mít	k5eAaImAgFnS	mít
interakce	interakce	k1gFnPc4	interakce
se	s	k7c7	s
satelitními	satelitní	k2eAgFnPc7d1	satelitní
galaxiemi	galaxie	k1gFnPc7	galaxie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
M32	M32	k1gFnSc1	M32
a	a	k8xC	a
M	M	kA	M
<g/>
110	[number]	k4	110
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
již	již	k6eAd1	již
pohltila	pohltit	k5eAaPmAgFnS	pohltit
a	a	k8xC	a
které	který	k3yRgFnPc1	který
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
obří	obří	k2eAgInSc4d1	obří
proud	proud	k1gInSc4	proud
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
galaxií	galaxie	k1gFnSc7	galaxie
před	před	k7c4	před
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
rotujícího	rotující	k2eAgInSc2d1	rotující
disku	disk	k1gInSc2	disk
plynu	plyn	k1gInSc2	plyn
nalezeného	nalezený	k2eAgInSc2d1	nalezený
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
přítomnost	přítomnost	k1gFnSc1	přítomnost
relativně	relativně	k6eAd1	relativně
mladé	mladý	k2eAgFnSc2d1	mladá
(	(	kIx(	(
<g/>
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
tvorba	tvorba	k1gFnSc1	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
prakticky	prakticky	k6eAd1	prakticky
ustala	ustat	k5eAaPmAgFnS	ustat
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgNnP	být
znovu	znovu	k6eAd1	znovu
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
po	po	k7c6	po
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
nejprozkoumanějším	prozkoumaný	k2eAgInSc7d3	prozkoumaný
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
objektem	objekt	k1gInSc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
kolem	kolem	k7c2	kolem
150	[number]	k4	150
000	[number]	k4	000
–	–	k?	–
200	[number]	k4	200
000	[number]	k4	000
ly	ly	k?	ly
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
450	[number]	k4	450
miliard	miliarda	k4xCgFnPc2	miliarda
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
hmotná	hmotný	k2eAgFnSc1d1	hmotná
jako	jako	k8xS	jako
naše	náš	k3xOp1gFnSc1	náš
Galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Rovina	rovina	k1gFnSc1	rovina
galaxie	galaxie	k1gFnSc2	galaxie
je	být	k5eAaImIp3nS	být
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
od	od	k7c2	od
zorného	zorný	k2eAgInSc2d1	zorný
směru	směr	k1gInSc2	směr
o	o	k7c4	o
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
plně	plně	k6eAd1	plně
nevyniknou	vyniknout	k5eNaPmIp3nP	vyniknout
její	její	k3xOp3gNnPc4	její
spirální	spirální	k2eAgNnPc4d1	spirální
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
galaxie	galaxie	k1gFnSc2	galaxie
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
jádro	jádro	k1gNnSc1	jádro
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnSc2d1	malá
–	–	k?	–
jen	jen	k9	jen
15	[number]	k4	15
<g/>
×	×	k?	×
<g/>
30	[number]	k4	30
ly	ly	k?	ly
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
rotuje	rotovat	k5eAaImIp3nS	rotovat
jako	jako	k9	jako
tuhé	tuhý	k2eAgNnSc1d1	tuhé
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
dynamicky	dynamicky	k6eAd1	dynamicky
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Perioda	perioda	k1gFnSc1	perioda
rotace	rotace	k1gFnSc2	rotace
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
310	[number]	k4	310
milionů	milion	k4xCgInPc2	milion
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
nejprve	nejprve	k6eAd1	nejprve
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc2	rotace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
galaxie	galaxie	k1gFnSc1	galaxie
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
42	[number]	k4	42
tisíc	tisíc	k4xCgInPc2	tisíc
ly	ly	k?	ly
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
–	–	k?	–
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rotace	rotace	k1gFnSc1	rotace
rychlosti	rychlost	k1gFnSc2	rychlost
300	[number]	k4	300
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
vycházejí	vycházet	k5eAaImIp3nP	vycházet
dvě	dva	k4xCgNnPc4	dva
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
hlavní	hlavní	k2eAgNnPc4d1	hlavní
spirální	spirální	k2eAgNnPc4d1	spirální
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
posuvu	posuv	k1gInSc2	posuv
jejich	jejich	k3xOp3gFnPc2	jejich
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pomocí	pomocí	k7c2	pomocí
Dopplerova	Dopplerův	k2eAgInSc2d1	Dopplerův
posunu	posun	k1gInSc2	posun
vypočítat	vypočítat	k5eAaPmF	vypočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
se	se	k3xPyFc4	se
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
naší	náš	k3xOp1gFnSc2	náš
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
rychlostí	rychlost	k1gFnSc7	rychlost
266	[number]	k4	266
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
obě	dva	k4xCgFnPc1	dva
galaxie	galaxie	k1gFnPc1	galaxie
srazí	srazit	k5eAaPmIp3nP	srazit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jediná	jediný	k2eAgFnSc1d1	jediná
galaxie	galaxie	k1gFnSc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
galaxií	galaxie	k1gFnPc2	galaxie
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
ničím	nic	k3yNnSc7	nic
neobvyklým	obvyklý	k2eNgNnSc7d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
samo	sám	k3xTgNnSc1	sám
jádro	jádro	k1gNnSc1	jádro
M31	M31	k1gFnSc2	M31
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
galaxie	galaxie	k1gFnSc1	galaxie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
srážkou	srážka	k1gFnSc7	srážka
dvou	dva	k4xCgFnPc6	dva
menších	malý	k2eAgFnPc2d2	menší
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Místní	místní	k2eAgFnSc6d1	místní
skupině	skupina	k1gFnSc6	skupina
je	být	k5eAaImIp3nS	být
M31	M31	k1gFnSc1	M31
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mléčnou	mléčný	k2eAgFnSc7d1	mléčná
dráhou	dráha	k1gFnSc7	dráha
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
a	a	k8xC	a
těžiště	těžiště	k1gNnSc1	těžiště
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
Mléčné	mléčný	k2eAgFnSc3d1	mléčná
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
M31	M31	k4	M31
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
hvězdy	hvězda	k1gFnPc4	hvězda
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
<g/>
,	,	kIx,	,
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
,	,	kIx,	,
cefeidy	cefeid	k1gMnPc4	cefeid
<g/>
,	,	kIx,	,
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgFnPc4d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc4	hvězdokupa
i	i	k8xC	i
mezihvězdnou	mezihvězdný	k2eAgFnSc4d1	mezihvězdná
hmotu	hmota	k1gFnSc4	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ramenech	rameno	k1gNnPc6	rameno
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
modří	modrý	k2eAgMnPc1d1	modrý
obři	obr	k1gMnPc1	obr
a	a	k8xC	a
veleobři	veleobr	k1gMnPc1	veleobr
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
30	[number]	k4	30
nov	nova	k1gFnPc2	nova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k6eAd1	také
stovky	stovka	k1gFnPc4	stovka
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Satelitní	satelitní	k2eAgFnPc1d1	satelitní
galaxie	galaxie	k1gFnPc1	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
M31	M31	k1gFnSc1	M31
satelitní	satelitní	k2eAgFnSc2d1	satelitní
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Dvěma	dva	k4xCgFnPc7	dva
nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
M32	M32	k1gMnSc1	M32
(	(	kIx(	(
<g/>
NGC	NGC	kA	NGC
221	[number]	k4	221
a	a	k8xC	a
M110	M110	k1gMnSc1	M110
(	(	kIx(	(
<g/>
NGC	NGC	kA	NGC
205	[number]	k4	205
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
satelity	satelit	k1gInPc7	satelit
jsou	být	k5eAaImIp3nP	být
NGC	NGC	kA	NGC
185	[number]	k4	185
<g/>
,	,	kIx,	,
NGC	NGC	kA	NGC
147	[number]	k4	147
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
I	i	k9	i
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
II	II	kA	II
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
III	III	kA	III
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
V	V	kA	V
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
VII	VII	kA	VII
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
VIII	VIII	kA	VIII
<g/>
,	,	kIx,	,
Andromeda	Andromeda	k1gMnSc1	Andromeda
IX	IX	kA	IX
a	a	k8xC	a
Andromeda	Andromeda	k1gMnSc1	Andromeda
X.	X.	kA	X.
Stále	stále	k6eAd1	stále
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
také	také	k9	také
Galaxie	galaxie	k1gFnSc1	galaxie
v	v	k7c6	v
Trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
není	být	k5eNaImIp3nS	být
satelitem	satelit	k1gInSc7	satelit
M	M	kA	M
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnSc4	pozorování
galaxie	galaxie	k1gFnPc1	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
Galaxii	galaxie	k1gFnSc3	galaxie
M31	M31	k1gMnSc1	M31
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Andromedy	Andromeda	k1gMnSc2	Andromeda
poblíž	poblíž	k7c2	poblíž
hvězdy	hvězda	k1gFnSc2	hvězda
ν	ν	k?	ν
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
slabý	slabý	k2eAgInSc1d1	slabý
mlhavý	mlhavý	k2eAgInSc1d1	mlhavý
obláček	obláček	k1gInSc1	obláček
4,8	[number]	k4	4,8
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
i	i	k8xC	i
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vidět	vidět	k5eAaImF	vidět
jen	jen	k6eAd1	jen
centrání	centrání	k1gNnSc4	centrání
nejjasnější	jasný	k2eAgFnSc4d3	nejjasnější
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Galaxii	galaxie	k1gFnSc4	galaxie
lze	lze	k6eAd1	lze
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
světelným	světelný	k2eAgNnSc7d1	světelné
znečištěním	znečištění	k1gNnSc7	znečištění
stupně	stupeň	k1gInSc2	stupeň
B	B	kA	B
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
zkušený	zkušený	k2eAgMnSc1d1	zkušený
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
může	moct	k5eAaImIp3nS	moct
tuto	tento	k3xDgFnSc4	tento
galaxii	galaxie	k1gFnSc4	galaxie
vidět	vidět	k5eAaImF	vidět
za	za	k7c4	za
dobré	dobrý	k2eAgFnPc4d1	dobrá
noci	noc	k1gFnPc4	noc
i	i	k9	i
při	při	k7c6	při
B	B	kA	B
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spirální	spirální	k2eAgNnPc4d1	spirální
ramena	rameno	k1gNnPc4	rameno
lze	lze	k6eAd1	lze
objevit	objevit	k5eAaPmF	objevit
až	až	k9	až
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
silnějších	silný	k2eAgMnPc2d2	silnější
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnPc1d3	nejjasnější
hvězdy	hvězda	k1gFnPc1	hvězda
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
18	[number]	k4	18
magnitudy	magnitud	k1gInPc4	magnitud
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rozeznat	rozeznat	k5eAaPmF	rozeznat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
fotografie	fotografia	k1gFnSc2	fotografia
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
expozicí	expozice	k1gFnSc7	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
nějaká	nějaký	k3yIgNnPc1	nějaký
nova	novum	k1gNnPc1	novum
překročí	překročit	k5eAaPmIp3nP	překročit
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
vzplanula	vzplanout	k5eAaPmAgFnS	vzplanout
dokonce	dokonce	k9	dokonce
supernova	supernova	k1gFnSc1	supernova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
6	[number]	k4	6
magnitudy	magnituda	k1gMnSc2	magnituda
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
viditelnosti	viditelnost	k1gFnSc6	viditelnost
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
Messierově	Messierův	k2eAgInSc6d1	Messierův
katalogu	katalog	k1gInSc6	katalog
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
SEDS	SEDS	kA	SEDS
</s>
</p>
