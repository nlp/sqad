<p>
<s>
Titan	titan	k1gInSc1	titan
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ti	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Titanium	Titanium	k1gNnSc1	Titanium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
odolný	odolný	k2eAgMnSc1d1	odolný
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
i	i	k9	i
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
0,39	[number]	k4	0,39
K	K	kA	K
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výrazně	výrazně	k6eAd1	výrazně
většímu	veliký	k2eAgNnSc3d2	veliký
technologickému	technologický	k2eAgNnSc3d1	Technologické
uplatnění	uplatnění	k1gNnSc3	uplatnění
brání	bránit	k5eAaImIp3nS	bránit
doposud	doposud	k6eAd1	doposud
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
výroby	výroba	k1gFnSc2	výroba
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
protikorozních	protikorozní	k2eAgFnPc2d1	protikorozní
ochranných	ochranný	k2eAgFnPc2d1	ochranná
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
slouží	sloužit	k5eAaImIp3nS	sloužit
často	často	k6eAd1	často
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc1	objev
prvku	prvek	k1gInSc2	prvek
==	==	k?	==
</s>
</p>
<p>
<s>
Titan	titan	k1gInSc1	titan
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
anglickým	anglický	k2eAgMnSc7d1	anglický
chemikem	chemik	k1gMnSc7	chemik
Williamem	William	k1gInSc7	William
Gregorem	Gregor	k1gMnSc7	Gregor
v	v	k7c6	v
minerálu	minerál	k1gInSc6	minerál
ilmenitu	ilmenit	k1gInSc2	ilmenit
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
Martinem	Martin	k1gMnSc7	Martin
H.	H.	kA	H.
Klaprothem	Klaproth	k1gInSc7	Klaproth
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
.	.	kIx.	.
</s>
<s>
Izolován	izolovat	k5eAaBmNgInS	izolovat
byl	být	k5eAaImAgInS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
M.	M.	kA	M.
A.	A.	kA	A.
Hunterem	Hunter	k1gInSc7	Hunter
zahříváním	zahřívání	k1gNnSc7	zahřívání
chloridu	chlorid	k1gInSc2	chlorid
titaničitého	titaničitý	k2eAgInSc2d1	titaničitý
TiCl	TiCl	k1gInSc1	TiCl
<g/>
4	[number]	k4	4
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
sodíkem	sodík	k1gInSc7	sodík
v	v	k7c6	v
ocelové	ocelový	k2eAgFnSc6d1	ocelová
tlakové	tlakový	k2eAgFnSc6d1	tlaková
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
vodičem	vodič	k1gMnSc7	vodič
tepla	teplo	k1gNnSc2	teplo
i	i	k8xC	i
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
chemickou	chemický	k2eAgFnSc7d1	chemická
stálostí	stálost	k1gFnSc7	stálost
–	–	k?	–
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
netečný	netečný	k2eAgMnSc1d1	netečný
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
odolává	odolávat	k5eAaImIp3nS	odolávat
působení	působení	k1gNnSc1	působení
většiny	většina	k1gFnSc2	většina
běžných	běžný	k2eAgFnPc2d1	běžná
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
i	i	k8xC	i
roztoků	roztok	k1gInPc2	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Zvolna	zvolna	k6eAd1	zvolna
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
jeho	jeho	k3xOp3gFnSc3	jeho
povrch	povrch	k1gInSc1	povrch
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
rozpouštění	rozpouštění	k1gNnSc4	rozpouštění
je	být	k5eAaImIp3nS	být
nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
kyselina	kyselina	k1gFnSc1	kyselina
fluorovodíková	fluorovodíkový	k2eAgFnSc1d1	fluorovodíková
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnPc4	její
směsi	směs	k1gFnPc4	směs
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
minerálními	minerální	k2eAgFnPc7d1	minerální
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
teplot	teplota	k1gFnPc2	teplota
však	však	k9	však
titan	titan	k1gInSc1	titan
přímo	přímo	k6eAd1	přímo
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
nekovů	nekov	k1gInPc2	nekov
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
borem	bor	k1gInSc7	bor
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
<g/>
,	,	kIx,	,
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
TiIII	TiIII	k1gFnSc2	TiIII
a	a	k8xC	a
TiIV	TiIV	k1gFnSc2	TiIV
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
čtyřmocného	čtyřmocný	k2eAgInSc2d1	čtyřmocný
titanu	titan	k1gInSc2	titan
jsou	být	k5eAaImIp3nP	být
neomezeně	omezeně	k6eNd1	omezeně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
,	,	kIx,	,
sloučeniny	sloučenina	k1gFnPc1	sloučenina
TiIII	TiIII	k1gFnPc1	TiIII
jsou	být	k5eAaImIp3nP	být
silnými	silný	k2eAgInPc7d1	silný
redukčními	redukční	k2eAgInPc7d1	redukční
činidly	činidlo	k1gNnPc7	činidlo
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
vzdušného	vzdušný	k2eAgMnSc2d1	vzdušný
O2	O2	k1gMnSc2	O2
rychle	rychle	k6eAd1	rychle
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c6	na
TiIV	TiIV	k1gFnSc6	TiIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
sedmým	sedmý	k4xOgInSc7	sedmý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
kovem	kov	k1gInSc7	kov
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
5,7	[number]	k4	5,7
–	–	k?	–
6,3	[number]	k4	6,3
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
chemické	chemický	k2eAgFnSc3d1	chemická
stálosti	stálost	k1gFnSc3	stálost
přítomen	přítomen	k2eAgMnSc1d1	přítomen
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,001	[number]	k4	0,001
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
titanu	titan	k1gInSc2	titan
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
je	být	k5eAaImIp3nS	být
titan	titan	k1gInSc1	titan
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
rudy	ruda	k1gFnPc4	ruda
patří	patřit	k5eAaImIp3nS	patřit
ilmenit	ilmenit	k1gInSc1	ilmenit
–	–	k?	–
(	(	kIx(	(
<g/>
FeTiO	FeTiO	k1gMnSc1	FeTiO
<g/>
3	[number]	k4	3
oxid	oxid	k1gInSc1	oxid
železnato-titaničitý	železnatoitaničitý	k2eAgInSc1d1	železnato-titaničitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
rutil	rutil	k1gInSc1	rutil
(	(	kIx(	(
<g/>
TiO	TiO	k1gFnSc1	TiO
<g/>
2	[number]	k4	2
–	–	k?	–
oxid	oxid	k1gInSc1	oxid
titaničitý	titaničitý	k2eAgInSc1d1	titaničitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
zásoby	zásoba	k1gFnPc1	zásoba
těchto	tento	k3xDgInPc2	tento
minerálů	minerál	k1gInPc2	minerál
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
je	být	k5eAaImIp3nS	být
titan	titan	k1gInSc4	titan
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
i	i	k9	i
na	na	k7c6	na
měsíčním	měsíční	k2eAgInSc6d1	měsíční
povrchu	povrch	k1gInSc6	povrch
–	–	k?	–
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
získala	získat	k5eAaPmAgFnS	získat
mise	mise	k1gFnSc1	mise
Apollo	Apollo	k1gNnSc1	Apollo
17	[number]	k4	17
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
%	%	kIx~	%
TiO	TiO	k1gFnSc2	TiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
vysoké	vysoký	k2eAgNnSc4d1	vysoké
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
byl	být	k5eAaImAgInS	být
čistý	čistý	k2eAgInSc1d1	čistý
kovový	kovový	k2eAgInSc1d1	kovový
titan	titan	k1gInSc1	titan
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
vzácným	vzácný	k2eAgInSc7d1	vzácný
a	a	k8xC	a
drahým	drahý	k2eAgInSc7d1	drahý
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
běžné	běžný	k2eAgFnPc1d1	běžná
hutní	hutní	k2eAgFnPc1d1	hutní
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
titanu	titan	k1gInSc2	titan
neúčinné	účinný	k2eNgFnSc2d1	neúčinná
díky	díky	k7c3	díky
ochotě	ochota	k1gFnSc3	ochota
titanu	titan	k1gInSc2	titan
reagovat	reagovat	k5eAaBmF	reagovat
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
titanu	titan	k1gInSc2	titan
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
tzv.	tzv.	kA	tzv.
Krollův	Krollův	k2eAgInSc4d1	Krollův
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
pyrolýzou	pyrolýza	k1gFnSc7	pyrolýza
ilmenitu	ilmenit	k1gInSc2	ilmenit
nebo	nebo	k8xC	nebo
rutilu	rutil	k1gInSc2	rutil
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
chlorem	chlor	k1gInSc7	chlor
získává	získávat	k5eAaImIp3nS	získávat
chlorid	chlorid	k1gInSc4	chlorid
titaničitý	titaničitý	k2eAgInSc4d1	titaničitý
TiCl	TiCl	k1gInSc4	TiCl
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečištění	přečištění	k1gNnSc6	přečištění
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
páry	pára	k1gFnPc1	pára
redukují	redukovat	k5eAaBmIp3nP	redukovat
hořčíkem	hořčík	k1gInSc7	hořčík
v	v	k7c6	v
inertní	inertní	k2eAgFnSc6d1	inertní
argonové	argonový	k2eAgFnSc6d1	argonová
atmosféře	atmosféra	k1gFnSc6	atmosféra
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
800	[number]	k4	800
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
TiCl	TiCl	k1gInSc1	TiCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
Mg	mg	kA	mg
→	→	k?	→
Ti	ty	k3xPp2nSc3	ty
+	+	kIx~	+
2	[number]	k4	2
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2	[number]	k4	2
<g/>
Titan	titan	k1gInSc1	titan
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
touto	tento	k3xDgFnSc7	tento
reakcí	reakce	k1gFnSc7	reakce
je	být	k5eAaImIp3nS	být
tuhá	tuhý	k2eAgFnSc1d1	tuhá
<g/>
,	,	kIx,	,
pórovitá	pórovitý	k2eAgFnSc1d1	pórovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
a	a	k8xC	a
nezreagovaného	zreagovaný	k2eNgInSc2d1	nezreagovaný
hořčíku	hořčík	k1gInSc2	hořčík
dále	daleko	k6eAd2	daleko
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
elementárního	elementární	k2eAgInSc2d1	elementární
titanu	titan	k1gInSc2	titan
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
především	především	k9	především
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
chemické	chemický	k2eAgFnSc2d1	chemická
odolnosti	odolnost	k1gFnSc2	odolnost
a	a	k8xC	a
malé	malý	k2eAgFnSc2d1	malá
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
třeba	třeba	k6eAd1	třeba
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
výroba	výroba	k1gFnSc1	výroba
titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
relativně	relativně	k6eAd1	relativně
značně	značně	k6eAd1	značně
finančně	finančně	k6eAd1	finančně
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
provozní	provozní	k2eAgNnSc1d1	provozní
nasazení	nasazení	k1gNnSc1	nasazení
titanových	titanový	k2eAgInPc2d1	titanový
komponentů	komponent	k1gInPc2	komponent
je	být	k5eAaImIp3nS	být
účelné	účelný	k2eAgNnSc1d1	účelné
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
levnější	levný	k2eAgFnSc4d2	levnější
alternativu	alternativa	k1gFnSc4	alternativa
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
hořčíku	hořčík	k1gInSc2	hořčík
–	–	k?	–
duralů	dural	k1gInPc2	dural
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
kovového	kovový	k2eAgInSc2d1	kovový
titanu	titan	k1gInSc2	titan
spočívalo	spočívat	k5eAaImAgNnS	spočívat
těžiště	těžiště	k1gNnSc1	těžiště
jeho	jeho	k3xOp3gNnSc2	jeho
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
kosmických	kosmický	k2eAgFnPc6d1	kosmická
technologiích	technologie	k1gFnPc6	technologie
a	a	k8xC	a
speciálních	speciální	k2eAgFnPc6d1	speciální
aplikacích	aplikace	k1gFnPc6	aplikace
leteckého	letecký	k2eAgInSc2d1	letecký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
slitiny	slitina	k1gFnPc1	slitina
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skeletů	skelet	k1gInPc2	skelet
nebo	nebo	k8xC	nebo
povrchových	povrchový	k2eAgInPc2d1	povrchový
ochranných	ochranný	k2eAgInPc2d1	ochranný
štítů	štít	k1gInPc2	štít
kosmických	kosmický	k2eAgInPc2d1	kosmický
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
družice	družice	k1gFnPc1	družice
<g/>
,	,	kIx,	,
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
sondy	sonda	k1gFnPc1	sonda
a	a	k8xC	a
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
stanice	stanice	k1gFnPc1	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
nacházejí	nacházet	k5eAaImIp3nP	nacházet
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
zvláště	zvláště	k6eAd1	zvláště
namáhaných	namáhaný	k2eAgFnPc2d1	namáhaná
součástí	součást	k1gFnPc2	součást
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
především	především	k9	především
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
vojenských	vojenský	k2eAgInPc2d1	vojenský
stíhacích	stíhací	k2eAgInPc2d1	stíhací
letounů	letoun	k1gInPc2	letoun
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
i	i	k9	i
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
komerčních	komerční	k2eAgInPc2d1	komerční
dopravních	dopravní	k2eAgInPc2d1	dopravní
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
chemické	chemický	k2eAgFnSc3d1	chemická
netečnosti	netečnost	k1gFnSc3	netečnost
se	se	k3xPyFc4	se
titan	titan	k1gInSc1	titan
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
metabolizována	metabolizovat	k5eAaImNgFnS	metabolizovat
živými	živý	k2eAgInPc7d1	živý
organizmy	organizmus	k1gInPc7	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
známo	znám	k2eAgNnSc1d1	známo
žádné	žádný	k3yNgNnSc4	žádný
zapojení	zapojení	k1gNnSc4	zapojení
titanu	titan	k1gInSc2	titan
do	do	k7c2	do
enzymatických	enzymatický	k2eAgFnPc2d1	enzymatická
reakcí	reakce	k1gFnPc2	reakce
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
jiné	jiný	k2eAgNnSc4d1	jiné
biologické	biologický	k2eAgNnSc4d1	biologické
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
odolnost	odolnost	k1gFnSc1	odolnost
titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
některých	některý	k3yIgInPc2	některý
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
modní	modní	k2eAgFnPc4d1	modní
piercingové	piercingový	k2eAgFnPc4d1	piercingová
ozdoby	ozdoba	k1gFnPc4	ozdoba
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
titanem	titan	k1gInSc7	titan
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
a	a	k8xC	a
současně	současně	k6eAd1	současně
žádaný	žádaný	k2eAgInSc1d1	žádaný
vzhled	vzhled	k1gInSc1	vzhled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čistého	čistý	k2eAgInSc2d1	čistý
titanu	titan	k1gInSc2	titan
nebo	nebo	k8xC	nebo
titanové	titanový	k2eAgFnSc2d1	titanová
slitiny	slitina	k1gFnSc2	slitina
(	(	kIx(	(
<g/>
Ti-	Ti-	k1gFnSc2	Ti-
<g/>
6	[number]	k4	6
<g/>
Al-	Al-	k1gFnSc1	Al-
<g/>
4	[number]	k4	4
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
implantát	implantát	k1gInSc1	implantát
za	za	k7c2	za
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
ortopedii	ortopedie	k1gFnSc6	ortopedie
<g/>
,	,	kIx,	,
neurochirurgii	neurochirurgie	k1gFnSc6	neurochirurgie
<g/>
,	,	kIx,	,
stomatologii	stomatologie	k1gFnSc6	stomatologie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
obličejové	obličejový	k2eAgFnSc6d1	obličejová
a	a	k8xC	a
plastické	plastický	k2eAgFnSc3d1	plastická
chirurgii	chirurgie	k1gFnSc3	chirurgie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
titan	titan	k1gInSc1	titan
preferován	preferován	k2eAgInSc1d1	preferován
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc4d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc4d1	dobrá
korozivzdornost	korozivzdornost	k1gFnSc4	korozivzdornost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
titan	titan	k1gInSc1	titan
stále	stále	k6eAd1	stále
populárnějším	populární	k2eAgInSc7d2	populárnější
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nebo	nebo	k8xC	nebo
pouhou	pouhý	k2eAgFnSc4d1	pouhá
vystýlku	vystýlka	k1gFnSc4	vystýlka
chemických	chemický	k2eAgInPc2d1	chemický
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
používán	používat	k5eAaImNgMnS	používat
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pracují	pracovat	k5eAaImIp3nP	pracovat
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
součásti	součást	k1gFnPc1	součást
lodí	loď	k1gFnPc2	loď
nebo	nebo	k8xC	nebo
ponorek	ponorka	k1gFnPc2	ponorka
(	(	kIx(	(
<g/>
lodní	lodní	k2eAgInPc4d1	lodní
šrouby	šroub	k1gInPc4	šroub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
komponenty	komponenta	k1gFnPc4	komponenta
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgInPc2d1	sloužící
k	k	k7c3	k
odsolování	odsolování	k1gNnSc3	odsolování
(	(	kIx(	(
<g/>
desalinaci	desalinace	k1gFnSc6	desalinace
<g/>
)	)	kIx)	)
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
s	s	k7c7	s
titanem	titan	k1gInSc7	titan
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
například	například	k6eAd1	například
jako	jako	k8xC	jako
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
luxusních	luxusní	k2eAgFnPc2d1	luxusní
náramkových	náramkový	k2eAgFnPc2d1	náramková
hodinek	hodinka	k1gFnPc2	hodinka
nebo	nebo	k8xC	nebo
částí	část	k1gFnPc2	část
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
těž	těžet	k5eAaImRp2nS	těžet
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
golfových	golfový	k2eAgFnPc2d1	golfová
holí	hole	k1gFnPc2	hole
<g/>
,	,	kIx,	,
luxusních	luxusní	k2eAgInPc2d1	luxusní
rámů	rám	k1gInPc2	rám
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kvalitních	kvalitní	k2eAgNnPc2d1	kvalitní
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
v	v	k7c6	v
období	období	k1gNnSc6	období
"	"	kIx"	"
<g/>
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
ještě	ještě	k6eAd1	ještě
nezvládaly	zvládat	k5eNaImAgFnP	zvládat
technologii	technologie	k1gFnSc4	technologie
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
titanu	titan	k1gInSc2	titan
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nakupovaly	nakupovat	k5eAaBmAgFnP	nakupovat
tento	tento	k3xDgInSc4	tento
materiál	materiál	k1gInSc4	materiál
pomocí	pomocí	k7c2	pomocí
prostředníků	prostředník	k1gMnPc2	prostředník
od	od	k7c2	od
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
špionážní	špionážní	k2eAgInSc1d1	špionážní
letoun	letoun	k1gInSc1	letoun
SR-	SR-	k1gFnSc1	SR-
<g/>
71	[number]	k4	71
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
drak	drak	k1gInSc1	drak
je	být	k5eAaImIp3nS	být
kompletně	kompletně	k6eAd1	kompletně
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
titanu	titan	k1gInSc2	titan
nakoupeného	nakoupený	k2eAgInSc2d1	nakoupený
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
americký	americký	k2eAgInSc1d1	americký
dokument	dokument	k1gInSc1	dokument
"	"	kIx"	"
<g/>
Letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
změnila	změnit	k5eAaPmAgFnS	změnit
svět	svět	k1gInSc4	svět
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Arrow	Arrow	k1gMnSc1	Arrow
International	International	k1gMnSc2	International
media	medium	k1gNnSc2	medium
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
and	and	k?	and
Smithsonian	Smithsonian	k1gInSc1	Smithsonian
Networks	Networks	k1gInSc1	Networks
L.L.C	L.L.C	k1gFnSc1	L.L.C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
titan	titan	k1gInSc1	titan
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
TiIII	TiIII	k1gMnPc2	TiIII
a	a	k8xC	a
TiIV	TiIV	k1gMnPc2	TiIV
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
sloučeniny	sloučenina	k1gFnPc1	sloučenina
čtyřmocného	čtyřmocný	k2eAgInSc2d1	čtyřmocný
titanu	titan	k1gInSc2	titan
jsou	být	k5eAaImIp3nP	být
neomezeně	omezeně	k6eNd1	omezeně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
sloučeninou	sloučenina	k1gFnSc7	sloučenina
titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc4	oxid
titaničitý	titaničitý	k2eAgInSc4d1	titaničitý
TiO	TiO	k1gFnSc7	TiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
krystalickém	krystalický	k2eAgInSc6d1	krystalický
stavu	stav	k1gInSc6	stav
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
3	[number]	k4	3
krystalických	krystalický	k2eAgFnPc6d1	krystalická
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
3	[number]	k4	3
různé	různý	k2eAgInPc1d1	různý
minerály	minerál	k1gInPc1	minerál
–	–	k?	–
rutil	rutil	k1gInSc1	rutil
<g/>
,	,	kIx,	,
anatas	anatas	k1gInSc1	anatas
a	a	k8xC	a
brookit	brookit	k1gInSc1	brookit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejvíce	hodně	k6eAd3	hodně
vhodná	vhodný	k2eAgFnSc1d1	vhodná
amorfní	amorfní	k2eAgFnSc1d1	amorfní
prášková	práškový	k2eAgFnSc1d1	prášková
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
titanová	titanový	k2eAgFnSc1d1	titanová
běloba	běloba	k1gFnSc1	běloba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
bílý	bílý	k2eAgInSc1d1	bílý
pigment	pigment	k1gInSc1	pigment
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
zdravotně	zdravotně	k6eAd1	zdravotně
zcela	zcela	k6eAd1	zcela
nezávadný	závadný	k2eNgInSc1d1	nezávadný
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
krycí	krycí	k2eAgFnSc7d1	krycí
schopností	schopnost	k1gFnSc7	schopnost
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
proto	proto	k8xC	proto
mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgInPc4d3	nejkvalitnější
dostupné	dostupný	k2eAgInPc4d1	dostupný
bílé	bílý	k2eAgInPc4d1	bílý
pigmenty	pigment	k1gInPc4	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
nachází	nacházet	k5eAaImIp3nS	nacházet
jak	jak	k6eAd1	jak
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
a	a	k8xC	a
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vysoce	vysoce	k6eAd1	vysoce
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
plnivo	plnivo	k1gNnSc1	plnivo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
jej	on	k3xPp3gInSc4	on
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
do	do	k7c2	do
zubních	zubní	k2eAgFnPc2d1	zubní
past	pasta	k1gFnPc2	pasta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prochází	procházet	k5eAaImIp3nS	procházet
trávícím	trávící	k2eAgInSc7d1	trávící
traktem	trakt	k1gInSc7	trakt
nepozměněn	pozměněn	k2eNgInSc1d1	nepozměněn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
bělení	bělení	k1gNnSc3	bělení
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
oxid	oxid	k1gInSc1	oxid
titaničitý	titaničitý	k2eAgInSc1d1	titaničitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
spotřeby	spotřeba	k1gFnSc2	spotřeba
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
titaničitý	titaničitý	k2eAgInSc4d1	titaničitý
TiCl	TiCl	k1gInSc4	TiCl
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
o	o	k7c6	o
bodu	bod	k1gInSc6	bod
varu	var	k1gInSc2	var
137	[number]	k4	137
°	°	k?	°
<g/>
C.	C.	kA	C.
Je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
čistého	čistý	k2eAgInSc2d1	čistý
titanu	titan	k1gInSc2	titan
Krollovým	Krollův	k2eAgInSc7d1	Krollův
procesem	proces	k1gInSc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
atmosférickou	atmosférický	k2eAgFnSc7d1	atmosférická
vlhkostí	vlhkost	k1gFnSc7	vlhkost
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
postupné	postupný	k2eAgFnSc3d1	postupná
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
<g/>
TiCl	TiCl	k1gInSc1	TiCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
TiO	TiO	k1gMnSc1	TiO
<g/>
2	[number]	k4	2
+	+	kIx~	+
4	[number]	k4	4
HClVznikající	HClVznikající	k2eAgFnSc1d1	HClVznikající
TiO	TiO	k1gFnSc1	TiO
<g/>
2	[number]	k4	2
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
bílý	bílý	k2eAgInSc1d1	bílý
dým	dým	k1gInSc1	dým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
není	být	k5eNaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
jev	jev	k1gInSc1	jev
nachází	nacházet	k5eAaImIp3nS	nacházet
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
zadýmovacích	zadýmovací	k2eAgInPc2d1	zadýmovací
granátů	granát	k1gInPc2	granát
<g/>
,	,	kIx,	,
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
umělé	umělý	k2eAgFnSc2d1	umělá
mlhy	mlha	k1gFnSc2	mlha
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmů	film	k1gInPc2	film
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
při	při	k7c6	při
leteckých	letecký	k2eAgNnPc6d1	letecké
show	show	k1gNnPc6	show
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TiCl	TiCl	k1gInSc1	TiCl
<g/>
4	[number]	k4	4
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
(	(	kIx(	(
<g/>
Ziegler-Natta	Ziegler-Natta	k1gFnSc1	Ziegler-Natta
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
při	při	k7c6	při
polymeracích	polymerace	k1gFnPc6	polymerace
nenasycených	nasycený	k2eNgInPc2d1	nenasycený
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
<g/>
Chlorid	chlorid	k1gInSc4	chlorid
titanitý	titanitý	k2eAgInSc4d1	titanitý
TiCl	TiCl	k1gInSc4	TiCl
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
titanometrii	titanometrie	k1gFnSc6	titanometrie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
redukční	redukční	k2eAgInPc4d1	redukční
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
taktéž	taktéž	k?	taktéž
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
Ziegler-Nattův	Ziegler-Nattův	k2eAgInSc4d1	Ziegler-Nattův
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
titanu	titan	k1gInSc2	titan
(	(	kIx(	(
<g/>
TiN	Tina	k1gFnPc2	Tina
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jedněm	jeden	k4xCgInPc3	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
známých	známý	k2eAgFnPc2d1	známá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
tvrdost	tvrdost	k1gFnSc1	tvrdost
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
na	na	k7c6	na
10	[number]	k4	10
<g/>
stupňové	stupňový	k2eAgFnSc6d1	stupňová
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
aplikací	aplikace	k1gFnSc7	aplikace
jsou	být	k5eAaImIp3nP	být
brusné	brusný	k2eAgInPc1d1	brusný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
povlakování	povlakování	k1gNnSc1	povlakování
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nástroje	nástroj	k1gInSc2	nástroj
určeného	určený	k2eAgMnSc4d1	určený
pro	pro	k7c4	pro
extrémní	extrémní	k2eAgNnSc4d1	extrémní
fyzické	fyzický	k2eAgNnSc4d1	fyzické
namáhání	namáhání	k1gNnSc4	namáhání
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
tenká	tenký	k2eAgFnSc1d1	tenká
ochranná	ochranný	k2eAgFnSc1d1	ochranná
vrstva	vrstva	k1gFnSc1	vrstva
TiN	Tina	k1gFnPc2	Tina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karbid	karbid	k1gInSc1	karbid
titanu	titan	k1gInSc2	titan
(	(	kIx(	(
<g/>
TiC	TiC	k1gFnSc1	TiC
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xC	jako
TiN	Tina	k1gFnPc2	Tina
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
cermetů	cermet	k1gInPc2	cermet
i	i	k8xC	i
k	k	k7c3	k
povlakování	povlakování	k1gNnSc3	povlakování
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Superpružné	Superpružný	k2eAgFnPc4d1	Superpružný
Ti-Cu	Ti-C	k2eAgFnSc4d1	Ti-C
slitiny	slitina	k1gFnPc4	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Drát	drát	k1gInSc1	drát
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
slitiny	slitina	k1gFnSc2	slitina
lze	lze	k6eAd1	lze
ohnout	ohnout	k5eAaPmF	ohnout
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
plastické	plastický	k2eAgFnSc3d1	plastická
deformaci	deformace	k1gFnSc3	deformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
tvarovou	tvarový	k2eAgFnSc7d1	tvarová
pamětí	paměť	k1gFnSc7	paměť
Ni-Ti	Ni-T	k1gFnSc2	Ni-T
<g/>
.	.	kIx.	.
</s>
<s>
Drát	drát	k1gInSc1	drát
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
slitiny	slitina	k1gFnSc2	slitina
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
<g/>
"	"	kIx"	"
předchozí	předchozí	k2eAgInSc4d1	předchozí
stav	stav	k1gInSc4	stav
před	před	k7c7	před
deformací	deformace	k1gFnSc7	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dodání	dodání	k1gNnSc6	dodání
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
ohřevu	ohřev	k1gInSc2	ohřev
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
titan	titan	k1gInSc1	titan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
titan	titan	k1gInSc1	titan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Metal	metal	k1gInSc1	metal
of	of	k?	of
the	the	k?	the
gods	gods	k6eAd1	gods
</s>
</p>
