<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
částečně	částečně	k6eAd1
uznaném	uznaný	k2eAgInSc6d1
státu	stát	k1gInSc6
vyhlášeném	vyhlášený	k2eAgInSc6d1
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
status	status	k1gInSc1
nečlenského	členský	k2eNgInSc2d1
státu	stát	k1gInSc2
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
prozatímním	prozatímní	k2eAgInSc6d1
administrativním	administrativní	k2eAgInSc6d1
orgánu	orgán	k1gInSc6
spravující	spravující	k2eAgFnSc2d1
části	část	k1gFnSc2
palestinských	palestinský	k2eAgNnPc2d1
území	území	k1gNnPc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
historickém	historický	k2eAgNnSc6d1
území	území	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
د	د	k?
ف	ف	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymnaف	Hymnaف	k?
Geografie	geografie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Ramalláh	Ramalláh	k1gInSc1
(	(	kIx(
<g/>
administrativní	administrativní	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jeruzalém	Jeruzalém	k1gInSc1
(	(	kIx(
<g/>
zamýšlený	zamýšlený	k2eAgInSc1d1
<g/>
)	)	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6220	#num#	k4
km²	km²	k?
Poloha	poloha	k1gFnSc1
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
4	#num#	k4
260	#num#	k4
636	#num#	k4
Jazyk	jazyk	k1gInSc1
</s>
<s>
arabština	arabština	k1gFnSc1
Státní	státní	k2eAgFnSc1d1
útvar	útvar	k1gInSc4
Vznik	vznik	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988	#num#	k4
(	(	kIx(
<g/>
vyhlášením	vyhlášení	k1gNnSc7
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
status	status	k1gInSc1
nečlenského	členský	k2eNgInSc2d1
státu	stát	k1gInSc2
OSN	OSN	kA
získal	získat	k5eAaPmAgInS
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Mahmúd	Mahmúd	k1gMnSc1
Abbás	Abbása	k1gFnPc2
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Ramí	Ramí	k2eAgMnSc1d1
Hamdalláh	Hamdalláh	k1gMnSc1
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
0	#num#	k4
<g/>
20	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
USD	USD	kA
(	(	kIx(
<g/>
134	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+970	+970	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
ps	ps	k0
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
د	د	k?
ف	ف	k?
<g/>
,	,	kIx,
Dawlat	Dawle	k1gNnPc2
Filasṭ	Filasṭ	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zkráceným	zkrácený	k2eAgInSc7d1
názvem	název	k1gInSc7
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
ف	ف	k?
<g/>
,	,	kIx,
Filasṭ	Filasṭ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
stát	stát	k1gInSc1
vyhlášený	vyhlášený	k2eAgInSc1d1
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1988	#num#	k4
v	v	k7c6
exilu	exil	k1gInSc6
v	v	k7c6
Alžírsku	Alžírsko	k1gNnSc6
přijetím	přijetí	k1gNnSc7
jednostranné	jednostranný	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
nezávislosti	nezávislost	k1gFnSc2
Národní	národní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
(	(	kIx(
<g/>
OOP	OOP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
přijetí	přijetí	k1gNnSc1
deklarace	deklarace	k1gFnSc2
nemělo	mít	k5eNaImAgNnS
OOP	OOP	kA
jakoukoli	jakýkoli	k3yIgFnSc4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
si	se	k3xPyFc3
nárokovalo	nárokovat	k5eAaImAgNnS
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
ovládal	ovládat	k5eAaImAgMnS
Izrael	Izrael	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Svým	svůj	k3xOyFgInSc7
rozsahem	rozsah	k1gInSc7
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
nově	nově	k6eAd1
zkonstruovaná	zkonstruovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
palestinská	palestinský	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
definovaná	definovaný	k2eAgFnSc1d1
podle	podle	k7c2
hranic	hranice	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
stát	stát	k5eAaImF,k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
i	i	k9
pojmenování	pojmenování	k1gNnSc1
lidu	lid	k1gInSc2
arabské	arabský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Palestinci	Palestinec	k1gMnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Summit	summit	k1gInSc1
Ligy	liga	k1gFnSc2
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
OOP	OOP	kA
„	„	k?
<g/>
jediným	jediný	k2eAgMnSc7d1
legitimním	legitimní	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
palestinského	palestinský	k2eAgInSc2d1
lidu	lid	k1gInSc2
a	a	k8xC
zdůraznil	zdůraznit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
právo	právo	k1gNnSc4
na	na	k7c6
založení	založení	k1gNnSc6
nezávislého	závislý	k2eNgInSc2d1
státu	stát	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1974	#num#	k4
získalo	získat	k5eAaPmAgNnS
OOP	OOP	kA
pozorovatelský	pozorovatelský	k2eAgInSc4d1
status	status	k1gInSc4
„	„	k?
<g/>
nestátní	státní	k2eNgFnSc2d1
entity	entita	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
Organizaci	organizace	k1gFnSc6
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
mělo	mít	k5eAaImAgNnS
možnost	možnost	k1gFnSc4
vystupovat	vystupovat	k5eAaImF
před	před	k7c7
Valným	valný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
OSN	OSN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nemohlo	moct	k5eNaImAgNnS
hlasovat	hlasovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
tuto	tento	k3xDgFnSc4
deklaraci	deklarace	k1gFnSc4
oficiálně	oficiálně	k6eAd1
„	„	k?
<g/>
vzalo	vzít	k5eAaPmAgNnS
na	na	k7c6
vědomí	vědomí	k1gNnSc6
<g/>
“	“	k?
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
používalo	používat	k5eAaImAgNnS
při	při	k7c6
zmínce	zmínka	k1gFnSc6
o	o	k7c6
palestinském	palestinský	k2eAgMnSc6d1
stálém	stálý	k2eAgMnSc6d1
zástupci	zástupce	k1gMnSc6
název	název	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
,	,	kIx,
namísto	namísto	k7c2
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
29	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
přijalo	přijmout	k5eAaPmAgNnS
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
rezoluci	rezoluce	k1gFnSc4
č.	č.	k?
67	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zvýšila	zvýšit	k5eAaPmAgFnS
status	status	k1gInSc4
palestinského	palestinský	k2eAgMnSc2d1
zástupce	zástupce	k1gMnSc2
na	na	k7c4
nečlenský	členský	k2eNgInSc4d1
stát	stát	k1gInSc4
(	(	kIx(
<g/>
pro	pro	k7c4
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
hlasovalo	hlasovat	k5eAaImAgNnS
138	#num#	k4
zejména	zejména	k6eAd1
muslimských	muslimský	k2eAgMnPc2d1
a	a	k8xC
evropských	evropský	k2eAgMnPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
9	#num#	k4
hlasovalo	hlasovat	k5eAaImAgNnS
proti	proti	k7c3
a	a	k8xC
41	#num#	k4
se	se	k3xPyFc4
zdrželo	zdržet	k5eAaPmAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
Izrael	Izrael	k1gInSc1
uznal	uznat	k5eAaPmAgInS
v	v	k7c6
rámci	rámec	k1gInSc6
mírových	mírový	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
vyjednávací	vyjednávací	k2eAgInSc4d1
tým	tým	k1gInSc4
OOP	OOP	kA
za	za	k7c7
„	„	k?
<g/>
zástupce	zástupce	k1gMnSc1
palestinského	palestinský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
OOP	OOP	kA
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
uznalo	uznat	k5eAaPmAgNnS
právo	právo	k1gNnSc1
Izraele	Izrael	k1gInSc2
na	na	k7c4
existenci	existence	k1gFnSc4
v	v	k7c6
míru	mír	k1gInSc6
<g/>
,	,	kIx,
přijalo	přijmout	k5eAaPmAgNnS
rezoluce	rezoluce	k1gFnSc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
242	#num#	k4
a	a	k8xC
338	#num#	k4
a	a	k8xC
zřeklo	zřeknout	k5eAaPmAgNnS
se	se	k3xPyFc4
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
násilí	násilí	k1gNnSc1
a	a	k8xC
terorismu	terorismus	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
mírových	mírový	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
z	z	k7c2
Osla	Oslo	k1gNnSc2
vznikl	vzniknout	k5eAaPmAgInS
prozatímní	prozatímní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
Palestinská	palestinský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vládne	vládnout	k5eAaImIp3nS
na	na	k7c6
části	část	k1gFnSc6
palestinských	palestinský	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vymezených	vymezený	k2eAgFnPc6d1
několika	několik	k4yIc7
izraelsko-palestinskými	izraelsko-palestinský	k2eAgFnPc7d1
dohodami	dohoda	k1gFnPc7
z	z	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
po	po	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
nátlaku	nátlak	k1gInSc6
jednostranně	jednostranně	k6eAd1
opustil	opustit	k5eAaPmAgMnS
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
a	a	k8xC
po	po	k7c6
převratu	převrat	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
ovládá	ovládat	k5eAaImIp3nS
hnutí	hnutí	k1gNnSc2
Hamás	Hamása	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestinská	palestinský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
,	,	kIx,
vedená	vedený	k2eAgFnSc1d1
hnutím	hnutí	k1gNnSc7
Fatah	Fataha	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
má	mít	k5eAaImIp3nS
fakticky	fakticky	k6eAd1
pouze	pouze	k6eAd1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Západním	západní	k2eAgInSc7d1
břehem	břeh	k1gInSc7
Jordánu	Jordán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
uzavřely	uzavřít	k5eAaPmAgFnP
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
vnitropalestinského	vnitropalestinský	k2eAgInSc2d1
sporu	spor	k1gInSc2
dohodu	dohoda	k1gFnSc4
o	o	k7c6
usmíření	usmíření	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gFnSc1
implementace	implementace	k1gFnSc1
zatím	zatím	k6eAd1
nebyla	být	k5eNaImAgFnS
naplněna	naplnit	k5eAaPmNgFnS
zejména	zejména	k9
pro	pro	k7c4
aspirace	aspirace	k1gFnPc4
Hamasu	Hamas	k1gInSc2
na	na	k7c6
ovládnutí	ovládnutí	k1gNnSc6
i	i	k8xC
západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
s	s	k7c7
vyznačením	vyznačení	k1gNnSc7
států	stát	k1gInPc2
uznávající	uznávající	k2eAgInSc1d1
Stát	stát	k1gInSc1
Palestinu	Palestina	k1gFnSc4
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
K	k	k7c3
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
uznalo	uznat	k5eAaPmAgNnS
Stát	stát	k5eAaImF,k5eAaPmF
Palestina	Palestina	k1gFnSc1
celkem	celkem	k6eAd1
134	#num#	k4
ze	z	k7c2
193	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
OSN	OSN	kA
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
70	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
tento	tento	k3xDgInSc4
stát	stát	k1gInSc4
neuznaly	uznat	k5eNaPmAgFnP
<g/>
,	,	kIx,
uznávají	uznávat	k5eAaImIp3nP
OOP	OOP	kA
jako	jako	k8xS,k8xC
zástupce	zástupce	k1gMnSc1
palestinského	palestinský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
ČSSR	ČSSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
v	v	k7c6
ČSSR	ČSSR	kA
existovalo	existovat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
zastoupení	zastoupení	k1gNnSc4
Organizace	organizace	k1gFnSc2
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
(	(	kIx(
<g/>
OOP	OOP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
s	s	k7c7
diplomatickým	diplomatický	k2eAgInSc7d1
statusem	status	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastoupení	zastoupení	k1gNnSc1
Palestinské	palestinský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
PNS	PNS	kA
<g/>
)	)	kIx)
v	v	k7c6
Česku	Česko	k1gNnSc6
nese	nést	k5eAaImIp3nS
označení	označení	k1gNnSc1
Velvyslanectví	velvyslanectví	k1gNnSc2
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
února	únor	k1gInSc2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČR	ČR	kA
uznala	uznat	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
státy	stát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ke	k	k7c3
dni	den	k1gInSc3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
uznávalo	uznávat	k5eAaImAgNnS
Československo	Československo	k1gNnSc1
a	a	k8xC
to	ten	k3xDgNnSc4
čl	čl	kA
<g/>
.	.	kIx.
5	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
č.	č.	k?
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
opatřeních	opatření	k1gNnPc6
souvisejících	související	k2eAgFnPc2d1
se	s	k7c7
zánikem	zánik	k1gInSc7
ČSFR	ČSFR	kA
<g/>
,	,	kIx,
tedy	tedy	k9
teoreticky	teoreticky	k6eAd1
i	i	k8xC
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
toleruje	tolerovat	k5eAaImIp3nS
status	status	k1gInSc4
quo	quo	k?
palestinského	palestinský	k2eAgNnSc2d1
zastoupení	zastoupení	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vláda	vláda	k1gFnSc1
reciproční	reciproční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
se	s	k7c7
Státem	stát	k1gInSc7
Palestina	Palestina	k1gFnSc1
neudržuje	udržovat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
jedná	jednat	k5eAaImIp3nS
jen	jen	k9
s	s	k7c7
palestinskou	palestinský	k2eAgFnSc7d1
samosprávou	samospráva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírové	mírový	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
z	z	k7c2
Osla	Oslo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
zahraniční	zahraniční	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
PNS	PNS	kA
sice	sice	k8xC
formálně	formálně	k6eAd1
nepřipouštějí	připouštět	k5eNaImIp3nP
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
kodifikují	kodifikovat	k5eAaBmIp3nP
zachování	zachování	k1gNnSc4
úrovně	úroveň	k1gFnSc2
již	již	k6eAd1
existujících	existující	k2eAgFnPc2d1
diplomatických	diplomatický	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
postoj	postoj	k1gInSc4
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
politikou	politika	k1gFnSc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
State	status	k1gInSc5
of	of	k?
Palestine	Palestin	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDP	GDP	kA
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
,	,	kIx,
PPP	PPP	kA
(	(	kIx(
<g/>
current	current	k1gMnSc1
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
$	$	kIx~
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Palestinská	palestinský	k2eAgFnSc1d1
deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
BAROUD	BAROUD	kA
<g/>
,	,	kIx,
Ramzy	Ramz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
Review	Review	k1gMnSc1
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
:	:	kIx,
The	The	k1gMnSc1
Economic	Economic	k1gMnSc1
and	and	k?
Business	business	k1gInSc1
Report	report	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Kogan	Kogan	k1gInSc1
Page	Page	k1gNnSc1
Publishers	Publishers	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
256	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7494	#num#	k4
<g/>
-	-	kIx~
<g/>
4066	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
161	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
BISSIO	BISSIO	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Remo	Remo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Third	Third	k1gMnSc1
World	World	k1gMnSc1
Guide	Guid	k1gInSc5
1995	#num#	k4
<g/>
–	–	k?
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montevideo	Montevideo	k1gNnSc1
<g/>
:	:	kIx,
Instituto	Institut	k2eAgNnSc1d1
del	del	k?
Tercer	Tercer	k1gMnSc1
Mundo	Mundo	k1gNnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85598	#num#	k4
<g/>
-	-	kIx~
<g/>
291	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
443	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BERCOVITCH	BERCOVITCH	kA
<g/>
,	,	kIx,
Jacob	Jacoba	k1gFnPc2
<g/>
;	;	kIx,
ZARTMAN	ZARTMAN	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
;	;	kIx,
KREMENYUK	KREMENYUK	kA
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
SAGE	SAGE	kA
Handbook	handbook	k1gInSc1
of	of	k?
Conflict	Conflict	k2eAgInSc1d1
Resolution	Resolution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thousand	Thousand	k1gInSc1
Oaks	Oaks	k1gInSc4
<g/>
:	:	kIx,
Sage	Sage	k1gFnSc4
Publications	Publicationsa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
704	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1412921923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Palestinian	Palestinian	k1gMnSc1
Authority	Authorita	k1gFnSc2
applies	applies	k1gMnSc1
for	forum	k1gNnPc2
full	fulnout	k5eAaPmAgMnS
UN	UN	kA
membership	membership	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
Radio	radio	k1gNnSc1
<g/>
,	,	kIx,
2011-09-23	2011-09-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AL	ala	k1gFnPc2
MADFAI	MADFAI	kA
<g/>
,	,	kIx,
Madiha	Madiha	k1gFnSc1
Rashid	Rashid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordan	Jordan	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
and	and	k?
the	the	k?
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
Peace	Peace	k1gMnSc1
Process	Processa	k1gFnPc2
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
41523	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rezoluce	rezoluce	k1gFnSc2
VS	VS	kA
OSN	OSN	kA
č.	č.	k?
3237	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
<g/>
,	,	kIx,
1974-11-22	1974-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GELDENHUYS	GELDENHUYS	kA
<g/>
,	,	kIx,
Deon	Deon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Isolated	Isolated	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Comparative	Comparativ	k1gInSc5
Analysis	Analysis	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
40268	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
155	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rezoluce	rezoluce	k1gFnSc2
VS	VS	kA
OSN	OSN	kA
č.	č.	k?
43	#num#	k4
<g/>
/	/	kIx~
<g/>
177	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
<g/>
,	,	kIx,
1998-12-09	1998-12-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HILLIER	HILLIER	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sourcebook	Sourcebook	k1gInSc1
on	on	k3xPp3gMnSc1
Public	publicum	k1gNnPc2
International	International	k1gFnSc1
Law	Law	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
883	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1859410509	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
205	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Palestinians	Palestinians	k1gInSc1
win	win	k?
implicit	implicita	k1gFnPc2
U.	U.	kA
<g/>
N.	N.	kA
recognition	recognition	k1gInSc1
of	of	k?
sovereign	sovereign	k1gInSc1
state	status	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reutersa	k1gFnPc2
<g/>
,	,	kIx,
2012-11-29	2012-11-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MURPHY	MURPHY	kA
<g/>
,	,	kIx,
Kim	Kim	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Israel	Israel	k1gInSc1
and	and	k?
PLO	PLO	kA
<g/>
,	,	kIx,
in	in	k?
Historic	Historic	k1gMnSc1
Bid	Bid	k1gMnSc1
for	forum	k1gNnPc2
Peace	Peace	k1gFnSc1
<g/>
,	,	kIx,
Agree	Agree	k1gFnSc1
to	ten	k3xDgNnSc1
Mutual	Mutual	k1gMnSc1
Recognition	Recognition	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
1993-09-10	1993-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hamas	Hamas	k1gMnSc1
leader	leader	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Tunisia	Tunisia	k1gFnSc1
visit	visita	k1gFnPc2
angers	angers	k6eAd1
Palestinian	Palestiniana	k1gFnPc2
officials	officials	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Arabíja	Al-Arabíj	k1gInSc2
<g/>
,	,	kIx,
2012-01-07	2012-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.palestine.cz	http://www.palestine.cz	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Velvyslanectví	velvyslanectví	k1gNnSc1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
↑	↑	k?
Text	text	k1gInSc4
Palestina	Palestina	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
Ministerstva	ministerstvo	k1gNnSc2
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
ČR	ČR	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Palestinci	Palestinec	k1gMnPc1
</s>
<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
Státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Asii	Asie	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
Bhútán	Bhútán	k1gInSc1
</s>
<s>
Brunej	Brunat	k5eAaImRp2nS,k5eAaPmRp2nS
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
Irák	Irák	k1gInSc1
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Jemen	Jemen	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
</s>
<s>
Katar	katar	k1gMnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Laos	Laos	k1gInSc1
</s>
<s>
Libanon	Libanon	k1gInSc1
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
Myanmar	Myanmar	k1gInSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nepál	Nepál	k1gInSc1
</s>
<s>
Omán	Omán	k1gInSc1
</s>
<s>
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Ashmorův	Ashmorův	k2eAgInSc4d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
</s>
<s>
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Hongkong	Hongkong	k1gInSc4
</s>
<s>
Macao	Macao	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Akrotiri	Akrotiri	k1gNnSc1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
)	)	kIx)
Územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
se	se	k3xPyFc4
spornýmmezinárodním	spornýmmezinárodní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
organizacích	organizace	k1gFnPc6
</s>
<s>
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
Členové	člen	k1gMnPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Albánie	Albánie	k1gFnSc2
•	•	k?
Alžírsko	Alžírsko	k1gNnSc4
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Benin	Benin	k1gMnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Brunej	Brunej	k1gMnSc1
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Gabon	Gabon	k1gInSc1
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Guinea	guinea	k1gFnSc2
•	•	k?
Guinea-Bissau	Guinea-Bissaa	k1gFnSc4
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc4
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Komory	komora	k1gFnSc2
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Libye	Libye	k1gFnSc1
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mali	Mali	k1gNnPc2
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Maroko	Maroko	k1gNnSc4
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc2
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Somálsko	Somálsko	k1gNnSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Súdán	Súdán	k1gInSc1
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Togo	Togo	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc4
Pozastavené	pozastavený	k2eAgNnSc4d1
členství	členství	k1gNnSc4
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
Pozorovatelé	pozorovatel	k1gMnPc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Kypr	Kypr	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
Liga	liga	k1gFnSc1
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
Členové	člen	k1gMnPc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Bahrajn	Bahrajna	k1gFnPc2
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Katar	katar	k1gMnSc1
•	•	k?
Komory	komora	k1gFnSc2
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Libye	Libye	k1gFnSc1
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Somálsko	Somálsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Súdán	Súdán	k1gInSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
Pozastavené	pozastavený	k2eAgNnSc1d1
členství	členství	k1gNnSc3
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
Pozorovatelé	pozorovatel	k1gMnPc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Státy	stát	k1gInPc1
s	s	k7c7
částečným	částečný	k2eAgNnSc7d1
mezinárodním	mezinárodní	k2eAgNnSc7d1
uznáním	uznání	k1gNnSc7
členské	členský	k2eAgFnSc2d1
státy	stát	k1gInPc1
OSN	OSN	kA
</s>
<s>
částečně	částečně	k6eAd1
neuznané	uznaný	k2eNgNnSc1d1
</s>
<s>
Arménie	Arménie	k1gFnSc1
Arménie	Arménie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kypr	Kypr	k1gInSc1
Kypr	Kypr	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
nečlenské	členský	k2eNgInPc1d1
státy	stát	k1gInPc1
OSN	OSN	kA
</s>
<s>
Státy	stát	k1gInPc4
uznané	uznaný	k2eAgInPc4d1
alespoň	alespoň	k9
jednímčlenským	jednímčlenský	k2eAgInSc7d1
státem	stát	k1gInSc7
OSN	OSN	kA
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
Abcházie	Abcházie	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
Kosovo	Kosův	k2eAgNnSc1d1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Palestina	Palestina	k1gFnSc1
Palestina	Palestina	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Saharská	saharský	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgInSc4d1
Kypr	Kypr	k1gInSc4
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
Tchaj-wan	Tchaj-wan	k1gInSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
Státy	stát	k1gInPc4
uznané	uznaný	k2eAgInPc4d1
pouzenečlenskými	pouzenečlenský	k2eAgInPc7d1
státy	stát	k1gInPc7
OSN	OSN	kA
</s>
<s>
Arcach	Arcach	k1gMnSc1
Arcach	Arcach	k1gMnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Podněstří	Podněstří	k1gFnSc1
Podněstří	Podněstří	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
Státy	stát	k1gInPc4
zcela	zcela	k6eAd1
postrádajícímezinárodní	postrádajícímezinárodní	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
</s>
<s>
Somaliland	Somaliland	k1gInSc1
Somaliland	Somaliland	k1gInSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgNnSc1d1
uznání	uznání	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
138926424	#num#	k4
</s>
