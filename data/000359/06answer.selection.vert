<s>
C	C	kA	C
<g/>
#	#	kIx~	#
(	(	kIx(	(
<g/>
vyslovované	vyslovovaný	k2eAgNnSc1d1	vyslovované
anglicky	anglicky	k6eAd1	anglicky
jako	jako	k8xS	jako
C	C	kA	C
Sharp	Sharp	kA	Sharp
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
siː	siː	k?	siː
šaː	šaː	k?	šaː
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
to	ten	k3xDgNnSc1	ten
označuje	označovat	k5eAaImIp3nS	označovat
notu	nota	k1gFnSc4	nota
cis	cis	k1gNnSc1	cis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysokoúrovňový	vysokoúrovňový	k2eAgInSc1d1	vysokoúrovňový
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
firmou	firma	k1gFnSc7	firma
Microsoft	Microsoft	kA	Microsoft
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Framework	Framework	k1gInSc1	Framework
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
schválený	schválený	k2eAgMnSc1d1	schválený
standardizačními	standardizační	k2eAgFnPc7d1	standardizační
komisemi	komise	k1gFnPc7	komise
ECMA	ECMA	kA	ECMA
(	(	kIx(	(
<g/>
ECMA-	ECMA-	k1gFnSc1	ECMA-
<g/>
334	[number]	k4	334
<g/>
)	)	kIx)	)
a	a	k8xC	a
ISO	ISO	kA	ISO
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
23270	[number]	k4	23270
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
