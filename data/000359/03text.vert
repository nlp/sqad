<s>
C	C	kA	C
<g/>
#	#	kIx~	#
(	(	kIx(	(
<g/>
vyslovované	vyslovovaný	k2eAgNnSc1d1	vyslovované
anglicky	anglicky	k6eAd1	anglicky
jako	jako	k8xS	jako
C	C	kA	C
Sharp	Sharp	kA	Sharp
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
siː	siː	k?	siː
šaː	šaː	k?	šaː
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
to	ten	k3xDgNnSc1	ten
označuje	označovat	k5eAaImIp3nS	označovat
notu	nota	k1gFnSc4	nota
cis	cis	k1gNnSc1	cis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vysokoúrovňový	vysokoúrovňový	k2eAgInSc1d1	vysokoúrovňový
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
firmou	firma	k1gFnSc7	firma
Microsoft	Microsoft	kA	Microsoft
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Framework	Framework	k1gInSc1	Framework
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
schválený	schválený	k2eAgMnSc1d1	schválený
standardizačními	standardizační	k2eAgFnPc7d1	standardizační
komisemi	komise	k1gFnPc7	komise
ECMA	ECMA	kA	ECMA
(	(	kIx(	(
<g/>
ECMA-	ECMA-	k1gFnSc1	ECMA-
<g/>
334	[number]	k4	334
<g/>
)	)	kIx)	)
a	a	k8xC	a
ISO	ISO	kA	ISO
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
23270	[number]	k4	23270
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
založil	založit	k5eAaPmAgMnS	založit
C	C	kA	C
<g/>
#	#	kIx~	#
na	na	k7c6	na
jazycích	jazyk	k1gInPc6	jazyk
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
Java	Java	k1gFnSc1	Java
(	(	kIx(	(
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepřímým	přímý	k2eNgMnSc7d1	nepřímý
potomkem	potomek	k1gMnSc7	potomek
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgMnSc2	který
čerpá	čerpat	k5eAaImIp3nS	čerpat
syntaxi	syntaxe	k1gFnSc4	syntaxe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
#	#	kIx~	#
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
databázových	databázový	k2eAgInPc2d1	databázový
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
webových	webový	k2eAgFnPc2d1	webová
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
formulářových	formulářový	k2eAgFnPc2d1	formulářová
aplikací	aplikace	k1gFnPc2	aplikace
ve	v	k7c6	v
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
softwaru	software	k1gInSc2	software
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
PDA	PDA	kA	PDA
a	a	k8xC	a
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Standard	standard	k1gInSc1	standard
ECMA	ECMA	kA	ECMA
definuje	definovat	k5eAaBmIp3nS	definovat
současný	současný	k2eAgInSc1d1	současný
design	design	k1gInSc1	design
C	C	kA	C
<g/>
#	#	kIx~	#
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc4d1	moderní
<g/>
,	,	kIx,	,
mnohoúčelový	mnohoúčelový	k2eAgInSc4d1	mnohoúčelový
a	a	k8xC	a
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
implementace	implementace	k1gFnSc1	implementace
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
principy	princip	k1gInPc4	princip
softwarového	softwarový	k2eAgNnSc2d1	softwarové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
jakými	jaký	k3yRgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
hlídání	hlídání	k1gNnSc1	hlídání
hranic	hranice	k1gFnPc2	hranice
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
detekce	detekce	k1gFnSc1	detekce
použití	použití	k1gNnSc2	použití
neinicializovaných	inicializovaný	k2eNgFnPc2d1	neinicializovaná
proměnných	proměnná	k1gFnPc2	proměnná
a	a	k8xC	a
automatický	automatický	k2eAgInSc1d1	automatický
garbage	garbage	k1gInSc1	garbage
collector	collector	k1gInSc1	collector
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jako	jako	k8xC	jako
robustnost	robustnost	k1gFnSc1	robustnost
<g/>
,	,	kIx,	,
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
a	a	k8xC	a
programátorská	programátorský	k2eAgFnSc1d1	programátorská
produktivita	produktivita	k1gFnSc1	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
softwarových	softwarový	k2eAgFnPc2d1	softwarová
komponent	komponenta	k1gFnPc2	komponenta
distribuovaných	distribuovaný	k2eAgFnPc2d1	distribuovaná
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Přenositelnost	přenositelnost	k1gFnSc1	přenositelnost
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
programátory	programátor	k1gMnPc4	programátor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
obeznámeni	obeznámen	k2eAgMnPc1d1	obeznámen
s	s	k7c7	s
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
podpora	podpora	k1gFnSc1	podpora
je	být	k5eAaImIp3nS	být
též	též	k9	též
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
aplikací	aplikace	k1gFnPc2	aplikace
jak	jak	k8xS	jak
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
se	s	k7c7	s
sofistikovanými	sofistikovaný	k2eAgInPc7d1	sofistikovaný
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
omezenými	omezený	k2eAgFnPc7d1	omezená
možnostmi	možnost	k1gFnPc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
by	by	kYmCp3nP	by
programy	program	k1gInPc1	program
psané	psaný	k2eAgInPc1d1	psaný
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
neměly	mít	k5eNaImAgFnP	mít
plýtvat	plýtvat	k5eAaImF	plýtvat
s	s	k7c7	s
přiděleným	přidělený	k2eAgInSc7d1	přidělený
procesorovým	procesorový	k2eAgInSc7d1	procesorový
časem	čas	k1gInSc7	čas
a	a	k8xC	a
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
měřit	měřit	k5eAaImF	měřit
s	s	k7c7	s
aplikacemi	aplikace	k1gFnPc7	aplikace
psanými	psaný	k2eAgFnPc7d1	psaná
v	v	k7c6	v
C	C	kA	C
nebo	nebo	k8xC	nebo
jazyce	jazyk	k1gInSc6	jazyk
symbolických	symbolický	k2eAgFnPc2d1	symbolická
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
neexistuje	existovat	k5eNaImIp3nS	existovat
vícenásobná	vícenásobný	k2eAgFnSc1d1	vícenásobná
dědičnost	dědičnost	k1gFnSc1	dědičnost
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
třída	třída	k1gFnSc1	třída
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potomkem	potomek	k1gMnSc7	potomek
pouze	pouze	k6eAd1	pouze
jedné	jeden	k4xCgFnSc2	jeden
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
komplikacím	komplikace	k1gFnPc3	komplikace
a	a	k8xC	a
přílišné	přílišný	k2eAgFnSc3d1	přílišná
složitosti	složitost	k1gFnSc3	složitost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
vícenásobnou	vícenásobný	k2eAgFnSc7d1	vícenásobná
dědičností	dědičnost	k1gFnSc7	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
implementovat	implementovat	k5eAaImF	implementovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgFnPc4	žádný
globální	globální	k2eAgFnPc4d1	globální
proměnné	proměnná	k1gFnPc4	proměnná
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
deklarovány	deklarovat	k5eAaBmNgInP	deklarovat
uvnitř	uvnitř	k7c2	uvnitř
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
globální	globální	k2eAgFnPc4d1	globální
proměnné	proměnná	k1gFnPc4	proměnná
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
jsou	být	k5eAaImIp3nP	být
statické	statický	k2eAgFnPc4d1	statická
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
proměnné	proměnná	k1gFnPc4	proměnná
veřejných	veřejný	k2eAgFnPc2d1	veřejná
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
objektově	objektově	k6eAd1	objektově
orientovaném	orientovaný	k2eAgNnSc6d1	orientované
programování	programování	k1gNnSc6	programování
se	se	k3xPyFc4	se
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dodržení	dodržení	k1gNnSc2	dodržení
principu	princip	k1gInSc3	princip
zapouzdření	zapouzdření	k1gNnSc2	zapouzdření
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
vzor	vzor	k1gInSc1	vzor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
datovým	datový	k2eAgInPc3d1	datový
atributům	atribut	k1gInPc3	atribut
třídy	třída	k1gFnSc2	třída
lze	lze	k6eAd1	lze
zvenčí	zvenčí	k6eAd1	zvenčí
přistupovat	přistupovat	k5eAaImF	přistupovat
pouze	pouze	k6eAd1	pouze
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
metod	metoda	k1gFnPc2	metoda
<g/>
:	:	kIx,	:
metody	metoda	k1gFnPc1	metoda
get	get	k?	get
(	(	kIx(	(
<g/>
accessor	accessor	k1gMnSc1	accessor
<g/>
)	)	kIx)	)
a	a	k8xC	a
metody	metoda	k1gFnPc1	metoda
set	set	k1gInSc1	set
(	(	kIx(	(
<g/>
mutator	mutator	k1gInSc1	mutator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
definovat	definovat	k5eAaBmF	definovat
tzv.	tzv.	kA	tzv.
property	propert	k1gInPc1	propert
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvenčí	zvenčí	k6eAd1	zvenčí
stále	stále	k6eAd1	stále
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
datový	datový	k2eAgInSc1d1	datový
atribut	atribut	k1gInSc1	atribut
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvnitř	uvnitř	k6eAd1	uvnitř
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgFnPc2	tento
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
datovým	datový	k2eAgInSc7d1	datový
atributem	atribut	k1gInSc7	atribut
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
principu	princip	k1gInSc2	princip
zapouzdření	zapouzdření	k1gNnSc2	zapouzdření
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
typově	typově	k6eAd1	typově
bezpečnější	bezpečný	k2eAgFnSc1d2	bezpečnější
než	než	k8xS	než
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
předdefinované	předdefinovaný	k2eAgFnSc2d1	předdefinovaná
implicitní	implicitní	k2eAgFnSc2d1	implicitní
konverze	konverze	k1gFnSc2	konverze
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
budiž	budiž	k9	budiž
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
celočíselných	celočíselný	k2eAgInPc2d1	celočíselný
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
32	[number]	k4	32
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
na	na	k7c4	na
64	[number]	k4	64
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
konverze	konverze	k1gFnSc1	konverze
z	z	k7c2	z
odvozeného	odvozený	k2eAgInSc2d1	odvozený
typu	typ	k1gInSc2	typ
na	na	k7c4	na
typ	typ	k1gInSc4	typ
rodičovský	rodičovský	k2eAgInSc4d1	rodičovský
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
však	však	k9	však
implicitní	implicitní	k2eAgFnPc4d1	implicitní
konverze	konverze	k1gFnPc4	konverze
z	z	k7c2	z
celočíselných	celočíselný	k2eAgInPc2d1	celočíselný
typů	typ	k1gInPc2	typ
na	na	k7c4	na
boolean	boolean	k1gInSc4	boolean
ani	ani	k8xC	ani
implicitní	implicitní	k2eAgFnPc4d1	implicitní
konverze	konverze	k1gFnPc4	konverze
mezi	mezi	k7c7	mezi
výčtovými	výčtový	k2eAgInPc7d1	výčtový
a	a	k8xC	a
celočíselnými	celočíselný	k2eAgInPc7d1	celočíselný
typy	typ	k1gInPc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
C	C	kA	C
<g/>
#	#	kIx~	#
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
a	a	k8xC	a
ani	ani	k8xC	ani
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
dopřednou	dopředný	k2eAgFnSc4d1	dopředná
deklaraci	deklarace	k1gFnSc4	deklarace
-	-	kIx~	-
pořadí	pořadí	k1gNnSc4	pořadí
deklarace	deklarace	k1gFnSc2	deklarace
metod	metoda	k1gFnPc2	metoda
není	být	k5eNaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
case	casus	k1gMnSc5	casus
sensitive	sensitiv	k1gMnSc5	sensitiv
-	-	kIx~	-
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgNnPc7d1	velké
a	a	k8xC	a
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Identifikátory	identifikátor	k1gInPc1	identifikátor
"	"	kIx"	"
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Hodnota	hodnota	k1gFnSc1	hodnota
<g/>
"	"	kIx"	"
tedy	tedy	k9	tedy
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
VB	VB	kA	VB
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
,	,	kIx,	,
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
<s>
Common	Common	k1gMnSc1	Common
Type	typ	k1gInSc5	typ
System	Systo	k1gNnSc7	Systo
je	být	k5eAaImIp3nS	být
unifikovaný	unifikovaný	k2eAgInSc1d1	unifikovaný
typový	typový	k2eAgInSc1d1	typový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
všemi	všecek	k3xTgInPc7	všecek
jazyky	jazyk	k1gInPc7	jazyk
pod	pod	k7c7	pod
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Framework	Framework	k1gInSc1	Framework
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k8xC	i
jazykem	jazyk	k1gInSc7	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
VB	VB	kA	VB
<g/>
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
typy	typ	k1gInPc1	typ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
primitivních	primitivní	k2eAgInPc2d1	primitivní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Integer	Integer	k1gInSc4	Integer
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc7	potomek
třídy	třída	k1gFnSc2	třída
System	Syst	k1gMnSc7	Syst
<g/>
.	.	kIx.	.
<g/>
Object	Object	k1gMnSc1	Object
a	a	k8xC	a
dědí	dědit	k5eAaImIp3nS	dědit
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
i	i	k9	i
všechny	všechen	k3xTgFnPc4	všechen
její	její	k3xOp3gFnPc4	její
metody	metoda	k1gFnPc4	metoda
jako	jako	k8xC	jako
například	například	k6eAd1	například
ToString	ToString	k1gInSc4	ToString
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typ	k1gInPc1	typ
v	v	k7c6	v
CTS	CTS	kA	CTS
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
Hodnotové	hodnotový	k2eAgInPc1d1	hodnotový
Referenční	referenční	k2eAgInPc1d1	referenční
Všechny	všechen	k3xTgInPc1	všechen
hodnotové	hodnotový	k2eAgInPc1d1	hodnotový
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
odkazových	odkazový	k2eAgInPc2d1	odkazový
typů	typ	k1gInPc2	typ
alokované	alokovaný	k2eAgFnSc2d1	alokovaná
na	na	k7c6	na
zásobníku	zásobník	k1gInSc6	zásobník
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
výkonnostních	výkonnostní	k2eAgInPc2d1	výkonnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotové	hodnotový	k2eAgInPc1d1	hodnotový
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
Primitivní	primitivní	k2eAgFnSc2d1	primitivní
datové	datový	k2eAgFnSc2d1	datová
typy	typa	k1gFnSc2	typa
-	-	kIx~	-
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
celočíselné	celočíselný	k2eAgInPc1d1	celočíselný
primitivní	primitivní	k2eAgInPc1d1	primitivní
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
Byte	byte	k1gInSc1	byte
<g/>
,	,	kIx,	,
Integer	Integer	k1gInSc1	Integer
<g/>
,	,	kIx,	,
Char	Char	k1gInSc1	Char
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
reálné	reálný	k2eAgInPc4d1	reálný
primitivní	primitivní	k2eAgInPc4d1	primitivní
datové	datový	k2eAgInPc4d1	datový
typy	typ	k1gInPc4	typ
reprezentující	reprezentující	k2eAgNnPc1d1	reprezentující
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
float	float	k1gInSc1	float
<g/>
,	,	kIx,	,
double	double	k1gInSc1	double
<g/>
,	,	kIx,	,
decimal	decimal	k1gInSc1	decimal
<g/>
)	)	kIx)	)
Struktury	struktura	k1gFnPc1	struktura
-	-	kIx~	-
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
uživatelsky	uživatelsky	k6eAd1	uživatelsky
definované	definovaný	k2eAgInPc4d1	definovaný
datové	datový	k2eAgInPc4d1	datový
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
připomínají	připomínat	k5eAaImIp3nP	připomínat
třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohou	moct	k5eNaImIp3nP	moct
dědit	dědit	k5eAaImF	dědit
ani	ani	k8xC	ani
být	být	k5eAaImF	být
děděny	děděn	k2eAgInPc4d1	děděn
a	a	k8xC	a
jako	jako	k9	jako
všechny	všechen	k3xTgInPc4	všechen
hodnotové	hodnotový	k2eAgInPc4d1	hodnotový
typy	typ	k1gInPc4	typ
jsou	být	k5eAaImIp3nP	být
alokovány	alokovat	k5eAaImNgInP	alokovat
na	na	k7c6	na
zásobníku	zásobník	k1gInSc6	zásobník
a	a	k8xC	a
ne	ne	k9	ne
na	na	k7c6	na
haldě	halda	k1gFnSc6	halda
<g/>
.	.	kIx.	.
</s>
<s>
Výčtové	výčtový	k2eAgInPc1d1	výčtový
typy	typ	k1gInPc1	typ
-	-	kIx~	-
Pojetí	pojetí	k1gNnSc1	pojetí
výčtů	výčet	k1gInPc2	výčet
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
oproti	oproti	k7c3	oproti
Javě	Java	k1gFnSc3	Java
značně	značně	k6eAd1	značně
zjednodušené	zjednodušený	k2eAgNnSc4d1	zjednodušené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
výčet	výčet	k1gInSc4	výčet
pouze	pouze	k6eAd1	pouze
množina	množina	k1gFnSc1	množina
předem	předem	k6eAd1	předem
definovaných	definovaný	k2eAgFnPc2d1	definovaná
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Výčet	výčet	k1gInSc4	výčet
DnyVTydnu	DnyVTydna	k1gFnSc4	DnyVTydna
s	s	k7c7	s
hodnotami	hodnota	k1gFnPc7	hodnota
pondělí	pondělí	k1gNnSc2	pondělí
<g/>
,	,	kIx,	,
úterý	úterý	k1gNnSc2	úterý
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
definovat	definovat	k5eAaBmF	definovat
si	se	k3xPyFc3	se
uvnitř	uvnitř	k7c2	uvnitř
výčtu	výčet	k1gInSc2	výčet
metody	metoda	k1gFnSc2	metoda
nebo	nebo	k8xC	nebo
atributy	atribut	k1gInPc4	atribut
<g/>
,	,	kIx,	,
indexery	indexer	k1gInPc4	indexer
nebo	nebo	k8xC	nebo
implementovat	implementovat	k5eAaImF	implementovat
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Referenční	referenční	k2eAgInPc1d1	referenční
typy	typ	k1gInPc1	typ
neuchovávají	uchovávat	k5eNaImIp3nP	uchovávat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
typů	typ	k1gInPc2	typ
hodnotových	hodnotový	k2eAgInPc2d1	hodnotový
pouze	pouze	k6eAd1	pouze
hodnotu	hodnota	k1gFnSc4	hodnota
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
instance	instance	k1gFnSc1	instance
uložena	uložit	k5eAaPmNgFnS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
odkazové	odkazový	k2eAgInPc1d1	odkazový
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
alokovány	alokovat	k5eAaImNgInP	alokovat
na	na	k7c6	na
haldě	halda	k1gFnSc6	halda
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yInSc4	co
nejvíce	hodně	k6eAd3	hodně
zohledňoval	zohledňovat	k5eAaImAgMnS	zohledňovat
strukturu	struktura	k1gFnSc4	struktura
Common	Common	k1gMnSc1	Common
Language	language	k1gFnPc2	language
Infrastructure	Infrastructur	k1gMnSc5	Infrastructur
(	(	kIx(	(
<g/>
CLI	clít	k5eAaImRp2nS	clít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
přímo	přímo	k6eAd1	přímo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
základním	základní	k2eAgInPc3d1	základní
typům	typ	k1gInPc3	typ
v	v	k7c6	v
platformě	platforma	k1gFnSc6	platforma
CLI	clít	k5eAaImRp2nS	clít
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
jazyka	jazyk	k1gInSc2	jazyk
ale	ale	k8xC	ale
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
překladač	překladač	k1gInSc1	překladač
generoval	generovat	k5eAaImAgInS	generovat
Common	Common	k1gInSc4	Common
Intermediate	Intermediat	k1gInSc5	Intermediat
Language	language	k1gFnSc2	language
(	(	kIx(	(
<g/>
CIL	cílit	k5eAaImRp2nS	cílit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
překladač	překladač	k1gInSc1	překladač
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
strojový	strojový	k2eAgInSc4d1	strojový
kód	kód	k1gInSc4	kód
podobný	podobný	k2eAgInSc4d1	podobný
běžným	běžný	k2eAgInPc3d1	běžný
překladačům	překladač	k1gInPc3	překladač
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
všechny	všechen	k3xTgInPc4	všechen
překladače	překladač	k1gInPc4	překladač
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
generují	generovat	k5eAaImIp3nP	generovat
CIL	cílit	k5eAaImRp2nS	cílit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
společně	společně	k6eAd1	společně
s	s	k7c7	s
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Frameworkem	Frameworek	k1gInSc7	Frameworek
1.0	[number]	k4	1.0
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
základní	základní	k2eAgFnSc4d1	základní
podporu	podpora	k1gFnSc4	podpora
objektového	objektový	k2eAgNnSc2d1	objektové
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
aktualizací	aktualizace	k1gFnSc7	aktualizace
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Nativní	nativní	k2eAgFnSc1d1	nativní
podpora	podpora	k1gFnSc1	podpora
generik	generika	k1gFnPc2	generika
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
CLI	clít	k5eAaImRp2nS	clít
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgFnPc1d1	částečná
a	a	k8xC	a
statické	statický	k2eAgFnPc1d1	statická
třídy	třída	k1gFnPc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Iterátory	Iterátor	k1gInPc1	Iterátor
<g/>
.	.	kIx.	.
</s>
<s>
Anonymní	anonymní	k2eAgFnPc1d1	anonymní
metody	metoda	k1gFnPc1	metoda
pro	pro	k7c4	pro
pohodlnější	pohodlný	k2eAgNnSc4d2	pohodlnější
užívání	užívání	k1gNnSc4	užívání
delegátů	delegát	k1gMnPc2	delegát
(	(	kIx(	(
<g/>
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
metody	metoda	k1gFnPc4	metoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nullovatelné	Nullovatelný	k2eAgInPc4d1	Nullovatelný
hodnotové	hodnotový	k2eAgInPc4d1	hodnotový
typy	typ	k1gInPc4	typ
a	a	k8xC	a
operátor	operátor	k1gInSc4	operátor
koalescence	koalescence	k1gFnSc2	koalescence
<g/>
.	.	kIx.	.
</s>
<s>
Generika	Generika	k1gFnSc1	Generika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
parametrizované	parametrizovaný	k2eAgInPc1d1	parametrizovaný
typy	typ	k1gInPc1	typ
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
parametrický	parametrický	k2eAgInSc1d1	parametrický
polymorfizmus	polymorfizmus	k1gInSc1	polymorfizmus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
od	od	k7c2	od
C	C	kA	C
<g/>
#	#	kIx~	#
2.0	[number]	k4	2.0
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
C	C	kA	C
<g/>
++	++	k?	++
šablon	šablona	k1gFnPc2	šablona
jsou	být	k5eAaImIp3nP	být
.	.	kIx.	.
<g/>
NET	NET	kA	NET
parametrizované	parametrizovaný	k2eAgInPc1d1	parametrizovaný
typy	typ	k1gInPc1	typ
instanciovány	instanciován	k2eAgInPc1d1	instanciován
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
při	při	k7c6	při
kompilaci	kompilace	k1gFnSc6	kompilace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
i	i	k9	i
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgFnP	být
napsány	napsat	k5eAaBmNgFnP	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Podporují	podporovat	k5eAaImIp3nP	podporovat
některé	některý	k3yIgFnPc4	některý
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nejsou	být	k5eNaImIp3nP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
C	C	kA	C
<g/>
++	++	k?	++
šablonách	šablona	k1gFnPc6	šablona
<g/>
,	,	kIx,	,
např.	např.	kA	např.
typové	typový	k2eAgNnSc1d1	typové
omezení	omezení	k1gNnSc1	omezení
na	na	k7c6	na
generických	generický	k2eAgInPc6d1	generický
parametrech	parametr	k1gInPc6	parametr
v	v	k7c6	v
rozhraní	rozhraní	k1gNnSc6	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
#	#	kIx~	#
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
netypové	typový	k2eNgInPc4d1	netypový
generické	generický	k2eAgInPc4d1	generický
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
generik	generika	k1gFnPc2	generika
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
.	.	kIx.	.
<g/>
NET	NET	kA	NET
generika	generik	k1gMnSc2	generik
používá	používat	k5eAaImIp3nS	používat
zhmotnění	zhmotnění	k1gNnSc4	zhmotnění
parametrizovaných	parametrizovaný	k2eAgInPc2d1	parametrizovaný
objektů	objekt	k1gInPc2	objekt
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
CLI	clít	k5eAaImRp2nS	clít
Virtual	Virtual	k1gMnSc1	Virtual
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
optimalizace	optimalizace	k1gFnSc1	optimalizace
a	a	k8xC	a
zachování	zachování	k1gNnSc1	zachování
druhu	druh	k1gInSc2	druh
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgFnPc1d1	částečná
třídy	třída	k1gFnPc1	třída
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgInSc1	každý
soubor	soubor	k1gInSc1	soubor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
třídy	třída	k1gFnSc2	třída
jsou	být	k5eAaImIp3nP	být
generovány	generován	k2eAgFnPc1d1	generována
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
psané	psaný	k2eAgInPc1d1	psaný
programátorem	programátor	k1gInSc7	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
používá	používat	k5eAaImIp3nS	používat
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
kódu	kód	k1gInSc2	kód
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
v	v	k7c6	v
návrháři	návrhář	k1gMnSc6	návrhář
<g/>
.	.	kIx.	.
file	fil	k1gFnSc2	fil
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
:	:	kIx,	:
file	file	k1gInSc1	file
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
:	:	kIx,	:
Statické	statický	k2eAgFnSc2d1	statická
třídy	třída	k1gFnSc2	třída
jsou	být	k5eAaImIp3nP	být
třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
instanciovány	instanciovat	k5eAaBmNgInP	instanciovat
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
dědit	dědit	k5eAaImF	dědit
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
statické	statický	k2eAgMnPc4d1	statický
členy	člen	k1gMnPc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
účel	účel	k1gInSc1	účel
je	být	k5eAaImIp3nS	být
obdobný	obdobný	k2eAgInSc1d1	obdobný
jako	jako	k8xS	jako
moduly	modul	k1gInPc4	modul
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
procedurálních	procedurální	k2eAgInPc6d1	procedurální
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
forma	forma	k1gFnSc1	forma
iterátoru	iterátor	k1gInSc2	iterátor
poskytující	poskytující	k2eAgFnSc1d1	poskytující
funkčnost	funkčnost	k1gFnSc1	funkčnost
generátoru	generátor	k1gInSc2	generátor
používá	používat	k5eAaImIp3nS	používat
konstrukci	konstrukce	k1gFnSc4	konstrukce
yield	yield	k1gMnSc1	yield
return	return	k1gMnSc1	return
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc4d1	podobná
konstrukci	konstrukce	k1gFnSc4	konstrukce
yield	yield	k6eAd1	yield
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
předchůdce	předchůdce	k1gMnSc1	předchůdce
lambda	lambda	k1gNnSc2	lambda
funkcí	funkce	k1gFnPc2	funkce
představených	představený	k2eAgFnPc2d1	představená
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
3.0	[number]	k4	3.0
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
C	C	kA	C
<g/>
#	#	kIx~	#
2.0	[number]	k4	2.0
přidány	přidat	k5eAaPmNgInP	přidat
anonymní	anonymní	k2eAgMnSc1d1	anonymní
delegáti	delegát	k1gMnPc1	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Zavádějí	zavádět	k5eAaImIp3nP	zavádět
funkčnost	funkčnost	k1gFnSc4	funkčnost
uzavření	uzavření	k1gNnSc2	uzavření
do	do	k7c2	do
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
anonymního	anonymní	k2eAgInSc2d1	anonymní
delegátu	delegát	k1gMnSc3	delegát
má	mít	k5eAaImIp3nS	mít
plný	plný	k2eAgInSc4d1	plný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
lokálním	lokální	k2eAgFnPc3d1	lokální
proměnným	proměnná	k1gFnPc3	proměnná
<g/>
,	,	kIx,	,
parametrům	parametr	k1gInPc3	parametr
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
instancím	instance	k1gFnPc3	instance
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
out	out	k?	out
a	a	k8xC	a
ref	ref	k?	ref
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Nullovatelné	Nullovatelný	k2eAgInPc1d1	Nullovatelný
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
označené	označený	k2eAgNnSc1d1	označené
otazníkem	otazník	k1gInSc7	otazník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
int	int	k?	int
<g/>
?	?	kIx.	?
</s>
<s>
i	i	k9	i
=	=	kIx~	=
null	null	k1gInSc1	null
<g/>
)	)	kIx)	)
přidávají	přidávat	k5eAaImIp3nP	přidávat
hodnotu	hodnota	k1gFnSc4	hodnota
null	nulla	k1gFnPc2	nulla
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
povolených	povolený	k2eAgFnPc2d1	povolená
hodnot	hodnota	k1gFnPc2	hodnota
pro	pro	k7c4	pro
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
lepší	dobrý	k2eAgFnSc4d2	lepší
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
SQL	SQL	kA	SQL
databázemi	databáze	k1gFnPc7	databáze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
hodnotou	hodnota	k1gFnSc7	hodnota
null	nullum	k1gNnPc2	nullum
odpovídající	odpovídající	k2eAgFnSc3d1	odpovídající
primitivním	primitivní	k2eAgMnPc3d1	primitivní
typům	typ	k1gInPc3	typ
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
SQL	SQL	kA	SQL
INTEGER	INTEGER	kA	INTEGER
NULL	NULL	kA	NULL
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
int	int	k?	int
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Operátor	operátor	k1gInSc1	operátor
??	??	k?	??
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
operátorem	operátor	k1gInSc7	operátor
koalescence	koalescence	k1gFnSc2	koalescence
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
definování	definování	k1gNnSc4	definování
implicitní	implicitní	k2eAgFnSc2d1	implicitní
hodnoty	hodnota	k1gFnSc2	hodnota
nullovatelných	nullovatelný	k2eAgInPc2d1	nullovatelný
typů	typ	k1gInPc2	typ
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
referenčních	referenční	k2eAgInPc2d1	referenční
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Operátor	operátor	k1gInSc1	operátor
vrací	vracet	k5eAaImIp3nS	vracet
levý	levý	k2eAgInSc4d1	levý
operand	operand	k1gInSc4	operand
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
rovna	roven	k2eAgFnSc1d1	rovna
null	null	k1gInSc1	null
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
vrací	vracet	k5eAaImIp3nS	vracet
pravý	pravý	k2eAgInSc4d1	pravý
operand	operand	k1gInSc4	operand
<g/>
.	.	kIx.	.
</s>
<s>
Primárně	primárně	k6eAd1	primárně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
operátor	operátor	k1gInSc1	operátor
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
přiřazení	přiřazení	k1gNnSc3	přiřazení
hodnoty	hodnota	k1gFnSc2	hodnota
nullovatelného	nullovatelný	k2eAgInSc2d1	nullovatelný
typu	typ	k1gInSc2	typ
do	do	k7c2	do
nenullovatelné	nullovatelný	k2eNgFnSc2d1	nullovatelný
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
:	:	kIx,	:
Vyšel	vyjít	k5eAaPmAgInS	vyjít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
společně	společně	k6eAd1	společně
s	s	k7c7	s
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Frameworkem	Frameworek	k1gInSc7	Frameworek
3.5	[number]	k4	3.5
a	a	k8xC	a
Visual	Visual	k1gInSc4	Visual
Studiem	studio	k1gNnSc7	studio
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
poměrně	poměrně	k6eAd1	poměrně
revoluční	revoluční	k2eAgFnPc1d1	revoluční
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
změnu	změna	k1gFnSc4	změna
podkladového	podkladový	k2eAgInSc2d1	podkladový
IL	IL	kA	IL
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
aplikace	aplikace	k1gFnPc1	aplikace
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
psané	psaný	k2eAgInPc1d1	psaný
půjdou	jít	k5eAaImIp3nP	jít
spouštět	spouštět	k5eAaImF	spouštět
i	i	k9	i
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
vybavených	vybavený	k2eAgInPc6d1	vybavený
toliko	toliko	k6eAd1	toliko
druhým	druhý	k4xOgInSc7	druhý
Frameworkem	Frameworek	k1gInSc7	Frameworek
<g/>
,	,	kIx,	,
ponesou	nést	k5eAaImIp3nP	nést
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
patřičné	patřičný	k2eAgFnSc2d1	patřičná
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Language	language	k1gFnSc1	language
Integrated	Integrated	k1gInSc1	Integrated
Query	Quera	k1gFnSc2	Quera
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dotazovací	dotazovací	k2eAgInSc1d1	dotazovací
jazyk	jazyk	k1gInSc1	jazyk
přináší	přinášet	k5eAaImIp3nS	přinášet
nový	nový	k2eAgInSc4d1	nový
způsob	způsob	k1gInSc4	způsob
pro	pro	k7c4	pro
dotazování	dotazování	k1gNnSc4	dotazování
nad	nad	k7c7	nad
jakýmikoliv	jakýkoliv	k3yIgNnPc7	jakýkoliv
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
třídění	třídění	k1gNnSc4	třídění
a	a	k8xC	a
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
LINQ	LINQ	kA	LINQ
to	ten	k3xDgNnSc1	ten
Objects	Objects	k1gInSc4	Objects
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dotazování	dotazování	k1gNnSc1	dotazování
nad	nad	k7c7	nad
normálními	normální	k2eAgInPc7d1	normální
objekty	objekt	k1gInPc7	objekt
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jejich	jejich	k3xOp3gFnPc7	jejich
kolekcemi	kolekce	k1gFnPc7	kolekce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LINQ	LINQ	kA	LINQ
to	ten	k3xDgNnSc4	ten
SQL	SQL	kA	SQL
přináší	přinášet	k5eAaImIp3nS	přinášet
nový	nový	k2eAgInSc4d1	nový
způsob	způsob	k1gInSc4	způsob
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
databázemi	databáze	k1gFnPc7	databáze
a	a	k8xC	a
LINQ	LINQ	kA	LINQ
to	ten	k3xDgNnSc4	ten
XML	XML	kA	XML
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
XML	XML	kA	XML
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příklad	příklad	k1gInSc1	příklad
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dotaz	dotaz	k1gInSc4	dotaz
LINQ	LINQ	kA	LINQ
který	který	k3yQgInSc1	který
nám	my	k3xPp1nPc3	my
ze	z	k7c2	z
zdrojového	zdrojový	k2eAgNnSc2d1	zdrojové
pole	pole	k1gNnSc2	pole
vrátí	vrátit	k5eAaPmIp3nS	vrátit
druhé	druhý	k4xOgFnPc4	druhý
mocniny	mocnina	k1gFnPc4	mocnina
všech	všecek	k3xTgNnPc2	všecek
lichých	lichý	k2eAgNnPc2d1	liché
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
seřadí	seřadit	k5eAaPmIp3nP	seřadit
sestupně	sestupně	k6eAd1	sestupně
<g/>
.	.	kIx.	.
</s>
<s>
Všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
podobnosti	podobnost	k1gFnPc4	podobnost
se	s	k7c7	s
syntaxí	syntax	k1gFnSc7	syntax
SQL	SQL	kA	SQL
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
lambda	lambda	k1gNnSc2	lambda
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
si	se	k3xPyFc3	se
berou	brát	k5eAaImIp3nP	brát
inspiraci	inspirace	k1gFnSc4	inspirace
z	z	k7c2	z
funkcionálního	funkcionální	k2eAgNnSc2d1	funkcionální
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tvořit	tvořit	k5eAaImF	tvořit
anonymní	anonymní	k2eAgFnPc4d1	anonymní
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jeden	jeden	k4xCgInSc4	jeden
výraz	výraz	k1gInSc4	výraz
nebo	nebo	k8xC	nebo
několik	několik	k4yIc4	několik
příkazů	příkaz	k1gInPc2	příkaz
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
očekávána	očekáván	k2eAgFnSc1d1	očekávána
instance	instance	k1gFnSc1	instance
delegáta	delegát	k1gMnSc2	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
lambda	lambda	k1gNnSc2	lambda
výrazů	výraz	k1gInPc2	výraz
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
C	C	kA	C
<g/>
#	#	kIx~	#
3.0	[number]	k4	3.0
uveden	uveden	k2eAgInSc1d1	uveden
nový	nový	k2eAgInSc1d1	nový
operátor	operátor	k1gInSc1	operátor
=	=	kIx~	=
<g/>
>	>	kIx)	>
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
2.0	[number]	k4	2.0
bychom	by	kYmCp1nP	by
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
pomocí	pomocí	k7c2	pomocí
anonymní	anonymní	k2eAgFnSc2d1	anonymní
metody	metoda	k1gFnSc2	metoda
napsali	napsat	k5eAaBmAgMnP	napsat
například	například	k6eAd1	například
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
A	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
samá	samý	k3xTgFnSc1	samý
funkčnost	funkčnost	k1gFnSc1	funkčnost
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pomocí	pomocí	k7c2	pomocí
lambda	lambda	k1gNnSc2	lambda
výrazu	výraz	k1gInSc2	výraz
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
3.0	[number]	k4	3.0
<g/>
:	:	kIx,	:
Všimněte	všimnout	k5eAaPmRp2nP	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
typy	typ	k1gInPc1	typ
argumentů	argument	k1gInPc2	argument
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
že	že	k8xS	že
i	i	k9	i
je	být	k5eAaImIp3nS	být
Integer	Integer	k1gInSc1	Integer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
klíčového	klíčový	k2eAgNnSc2d1	klíčové
slova	slovo	k1gNnSc2	slovo
var	var	k1gInSc1	var
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
argumentu	argument	k1gInSc2	argument
odvozen	odvodit	k5eAaPmNgInS	odvodit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kompilace	kompilace	k1gFnSc2	kompilace
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ne	ne	k9	ne
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dodržena	dodržet	k5eAaPmNgFnS	dodržet
typová	typový	k2eAgFnSc1d1	typová
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
)	)	kIx)	)
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
lambda	lambda	k1gNnSc4	lambda
výraz	výraz	k1gInSc4	výraz
zapisujeme	zapisovat	k5eAaImIp1nP	zapisovat
jako	jako	k9	jako
(	(	kIx(	(
<g/>
vstupní	vstupní	k2eAgInPc4d1	vstupní
argumenty	argument	k1gInPc4	argument
<g/>
)	)	kIx)	)
=	=	kIx~	=
<g/>
>	>	kIx)	>
výraz	výraz	k1gInSc4	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
zkráceně	zkráceně	k6eAd1	zkráceně
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
Zápis	zápis	k1gInSc1	zápis
inicializace	inicializace	k1gFnSc2	inicializace
kolekcí	kolekce	k1gFnPc2	kolekce
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
také	také	k9	také
zkrátit	zkrátit	k5eAaPmF	zkrátit
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
na	na	k7c4	na
zkrácené	zkrácený	k2eAgInPc4d1	zkrácený
<g/>
:	:	kIx,	:
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
naše	náš	k3xOp1gFnSc1	náš
třída	třída	k1gFnSc1	třída
MujSeznam	MujSeznam	k1gInSc4	MujSeznam
implementuje	implementovat	k5eAaImIp3nS	implementovat
rozhraní	rozhraní	k1gNnSc1	rozhraní
System	Syst	k1gInSc7	Syst
<g/>
.	.	kIx.	.
<g/>
Collections	Collections	k1gInSc1	Collections
<g/>
.	.	kIx.	.
<g/>
IEnumerable	IEnumerable	k1gFnSc1	IEnumerable
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
metodu	metoda	k1gFnSc4	metoda
Add	Add	k1gFnSc2	Add
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
rozšiřujících	rozšiřující	k2eAgFnPc2d1	rozšiřující
metod	metoda	k1gFnPc2	metoda
můžeme	moct	k5eAaImIp1nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
třída	třída	k1gFnSc1	třída
má	mít	k5eAaImIp3nS	mít
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zapsány	zapsán	k2eAgFnPc1d1	zapsána
mimo	mimo	k7c4	mimo
tuto	tento	k3xDgFnSc4	tento
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřující	rozšiřující	k2eAgFnPc1d1	rozšiřující
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
statické	statický	k2eAgFnPc1d1	statická
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
volat	volat	k5eAaImF	volat
jako	jako	k9	jako
metody	metoda	k1gFnPc4	metoda
instance	instance	k1gFnPc4	instance
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příkaz	příkaz	k1gInSc1	příkaz
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
můžeme	moct	k5eAaImIp1nP	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
třídu	třída	k1gFnSc4	třída
string	string	k1gInSc4	string
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
deklarujeme	deklarovat	k5eAaBmIp1nP	deklarovat
v	v	k7c6	v
oddělené	oddělený	k2eAgFnSc6d1	oddělená
třídě	třída	k1gFnSc6	třída
StringExtensions	StringExtensionsa	k1gFnPc2	StringExtensionsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
instanci	instance	k1gFnSc6	instance
třídy	třída	k1gFnSc2	třída
string	string	k1gInSc4	string
poté	poté	k6eAd1	poté
můžeme	moct	k5eAaImIp1nP	moct
volat	volat	k5eAaImF	volat
naši	náš	k3xOp1gFnSc4	náš
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
nyní	nyní	k6eAd1	nyní
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
Typ	typ	k1gInSc1	typ
proměnné	proměnná	k1gFnSc2	proměnná
x	x	k?	x
bude	být	k5eAaImBp3nS	být
určen	určit	k5eAaPmNgInS	určit
podle	podle	k7c2	podle
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
výrazu	výraz	k1gInSc2	výraz
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
překladu	překlad	k1gInSc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
zkrácení	zkrácení	k1gNnSc1	zkrácení
zápisu	zápis	k1gInSc2	zápis
pro	pro	k7c4	pro
inicializaci	inicializace	k1gFnSc4	inicializace
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
deklaraci	deklarace	k1gFnSc6	deklarace
proměnných	proměnná	k1gFnPc2	proměnná
anonymních	anonymní	k2eAgInPc2d1	anonymní
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Expression	Expression	k1gInSc1	Expression
Trees	Treesa	k1gFnPc2	Treesa
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Výrazové	výrazový	k2eAgInPc1d1	výrazový
stromy	strom	k1gInPc1	strom
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
se	s	k7c7	s
spustitelnými	spustitelný	k2eAgInPc7d1	spustitelný
příkazy	příkaz	k1gInPc7	příkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
vytvořit	vytvořit	k5eAaPmF	vytvořit
stromovou	stromový	k2eAgFnSc4d1	stromová
strukturu	struktura	k1gFnSc4	struktura
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
té	ten	k3xDgFnSc2	ten
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
její	její	k3xOp3gFnPc4	její
veřejné	veřejný	k2eAgFnPc4d1	veřejná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
ji	on	k3xPp3gFnSc4	on
analyzovat	analyzovat	k5eAaImF	analyzovat
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
všechny	všechen	k3xTgFnPc4	všechen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ji	on	k3xPp3gFnSc4	on
optimalizovat	optimalizovat	k5eAaBmF	optimalizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
dále	daleko	k6eAd2	daleko
zkompilovat	zkompilovat	k5eAaPmF	zkompilovat
do	do	k7c2	do
spustitelné	spustitelný	k2eAgFnSc2d1	spustitelná
podoby	podoba	k1gFnSc2	podoba
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
compile	compile	k6eAd1	compile
<g/>
.	.	kIx.	.
</s>
<s>
Anonymní	anonymní	k2eAgFnPc4d1	anonymní
třídy	třída	k1gFnPc4	třída
umožňující	umožňující	k2eAgNnSc1d1	umožňující
např.	např.	kA	např.
rychlé	rychlý	k2eAgNnSc1d1	rychlé
vytvoření	vytvoření	k1gNnSc1	vytvoření
objektů	objekt	k1gInPc2	objekt
přenášejících	přenášející	k2eAgInPc2d1	přenášející
informace	informace	k1gFnPc4	informace
vyžádané	vyžádaný	k2eAgFnPc4d1	vyžádaná
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
přes	přes	k7c4	přes
LINQ	LINQ	kA	LINQ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dynamickými	dynamický	k2eAgInPc7d1	dynamický
aspekty	aspekt	k1gInPc7	aspekt
programování	programování	k1gNnSc2	programování
a	a	k8xC	a
frameworky	frameworka	k1gFnSc2	frameworka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
DLR	DLR	kA	DLR
a	a	k8xC	a
COM	COM	kA	COM
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
novinky	novinka	k1gFnPc4	novinka
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Kovariance	Kovarianec	k1gInPc1	Kovarianec
a	a	k8xC	a
kontravariance	kontravarianec	k1gInPc1	kontravarianec
Volitelné	volitelný	k2eAgInPc1d1	volitelný
parametry	parametr	k1gInPc1	parametr
a	a	k8xC	a
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
parametry	parametr	k1gInPc1	parametr
Dynamicky	dynamicky	k6eAd1	dynamicky
typované	typovaný	k2eAgInPc1d1	typovaný
objekty	objekt	k1gInPc1	objekt
Verze	verze	k1gFnSc1	verze
5.0	[number]	k4	5.0
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
společně	společně	k6eAd1	společně
s	s	k7c7	s
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Framework	Framework	k1gInSc4	Framework
4.5	[number]	k4	4.5
a	a	k8xC	a
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
prostředím	prostředí	k1gNnSc7	prostředí
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
Mono	mono	k6eAd1	mono
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
asynchronního	asynchronní	k2eAgNnSc2d1	asynchronní
programování	programování	k1gNnSc2	programování
přidáním	přidání	k1gNnSc7	přidání
klíčových	klíčový	k2eAgNnPc2d1	klíčové
slov	slovo	k1gNnPc2	slovo
async	async	k1gInSc1	async
a	a	k8xC	a
await	await	k1gInSc1	await
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
novinkou	novinka	k1gFnSc7	novinka
jsou	být	k5eAaImIp3nP	být
Caller	Caller	k1gInSc4	Caller
Information	Information	k1gInSc1	Information
atributy	atribut	k1gInPc7	atribut
pro	pro	k7c4	pro
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
zjištění	zjištění	k1gNnSc4	zjištění
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
volající	volající	k2eAgFnSc6d1	volající
metodě	metoda	k1gFnSc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
zpětně	zpětně	k6eAd1	zpětně
nekompatibilním	kompatibilní	k2eNgFnPc3d1	nekompatibilní
změnám	změna	k1gFnPc3	změna
(	(	kIx(	(
<g/>
breaking	breaking	k1gInSc1	breaking
changes	changes	k1gInSc1	changes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
výrazná	výrazný	k2eAgFnSc1d1	výrazná
změna	změna	k1gFnSc1	změna
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
zachycení	zachycení	k1gNnSc6	zachycení
iterační	iterační	k2eAgFnSc2d1	iterační
proměnné	proměnná	k1gFnSc2	proměnná
cyklu	cyklus	k1gInSc2	cyklus
foreach	foreacha	k1gFnPc2	foreacha
v	v	k7c6	v
anonymních	anonymní	k2eAgFnPc6d1	anonymní
metodách	metoda	k1gFnPc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
verzí	verze	k1gFnSc7	verze
C	C	kA	C
<g/>
#	#	kIx~	#
5.0	[number]	k4	5.0
byla	být	k5eAaImAgFnS	být
iterační	iterační	k2eAgFnSc1d1	iterační
proměnná	proměnná	k1gFnSc1	proměnná
umístěna	umístit	k5eAaPmNgFnS	umístit
vně	vně	k7c2	vně
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
iterace	iterace	k1gFnPc4	iterace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
C	C	kA	C
<g/>
#	#	kIx~	#
5.0	[number]	k4	5.0
je	být	k5eAaImIp3nS	být
iterační	iterační	k2eAgFnSc1d1	iterační
proměnná	proměnná	k1gFnSc1	proměnná
uvnitř	uvnitř	k7c2	uvnitř
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
iteraci	iterace	k1gFnSc6	iterace
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
proměnnou	proměnná	k1gFnSc4	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
pořadí	pořadí	k1gNnSc2	pořadí
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
parametrů	parametr	k1gInPc2	parametr
metod	metoda	k1gFnPc2	metoda
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
verzi	verze	k1gFnSc6	verze
jazyka	jazyk	k1gInSc2	jazyk
byly	být	k5eAaImAgInP	být
nejprve	nejprve	k6eAd1	nejprve
vyhodnoceny	vyhodnocen	k2eAgInPc1d1	vyhodnocen
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
parametry	parametr	k1gInPc1	parametr
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
ostatní	ostatní	k2eAgInPc4d1	ostatní
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
C	C	kA	C
<g/>
#	#	kIx~	#
5.0	[number]	k4	5.0
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
parametry	parametr	k1gInPc1	parametr
vyhodnocovány	vyhodnocovat	k5eAaImNgInP	vyhodnocovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
v	v	k7c6	v
jakém	jaký	k3yQgNnSc6	jaký
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
konzolové	konzolový	k2eAgFnPc4d1	konzolová
aplikace	aplikace	k1gFnPc4	aplikace
vypíše	vypsat	k5eAaPmIp3nS	vypsat
"	"	kIx"	"
<g/>
Ahoj	ahoj	k0	ahoj
<g/>
,	,	kIx,	,
světe	svět	k1gInSc5	svět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Rozeberme	rozebrat	k5eAaPmRp1nP	rozebrat
krátce	krátce	k6eAd1	krátce
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
příkazy	příkaz	k1gInPc4	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnPc1d1	základní
jednotky	jednotka	k1gFnPc1	jednotka
objektového	objektový	k2eAgNnSc2d1	objektové
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
orientaci	orientace	k1gFnSc4	orientace
a	a	k8xC	a
jednoznačnost	jednoznačnost	k1gFnSc4	jednoznačnost
názvů	název	k1gInPc2	název
do	do	k7c2	do
jmenných	jmenný	k2eAgInPc2d1	jmenný
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
jmenujeme	jmenovat	k5eAaImIp1nP	jmenovat
příkazem	příkaz	k1gInSc7	příkaz
using	usinga	k1gFnPc2	usinga
jmenné	jmenný	k2eAgInPc1d1	jmenný
prostory	prostor	k1gInPc1	prostor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
budeme	být	k5eAaImBp1nP	být
používat	používat	k5eAaImF	používat
-	-	kIx~	-
nebudeme	být	k5eNaImBp1nP	být
pak	pak	k6eAd1	pak
muset	muset	k5eAaImF	muset
rozepisovat	rozepisovat	k5eAaImF	rozepisovat
jejich	jejich	k3xOp3gInSc4	jejich
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
třídy	třída	k1gFnPc1	třída
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
hned	hned	k6eAd1	hned
přístupny	přístupen	k2eAgFnPc1d1	přístupna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
řádku	řádek	k1gInSc6	řádek
příkazem	příkaz	k1gInSc7	příkaz
namespace	namespace	k1gFnSc2	namespace
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chceme	chtít	k5eAaImIp1nP	chtít
zařadit	zařadit	k5eAaPmF	zařadit
kód	kód	k1gInSc4	kód
vymezený	vymezený	k2eAgInSc4d1	vymezený
následujícími	následující	k2eAgFnPc7d1	následující
složenými	složený	k2eAgFnPc7d1	složená
závorkami	závorka	k1gFnPc7	závorka
do	do	k7c2	do
jmenného	jmenný	k2eAgInSc2d1	jmenný
prostoru	prostor	k1gInSc2	prostor
MojeKonzolováAplikace	MojeKonzolováAplikace	k1gFnSc2	MojeKonzolováAplikace
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
definujeme	definovat	k5eAaBmIp1nP	definovat
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
slovem	slovo	k1gNnSc7	slovo
class	class	k1gInSc1	class
třídu	třída	k1gFnSc4	třída
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
vymezen	vymezit	k5eAaPmNgInS	vymezit
dalšími	další	k2eAgFnPc7d1	další
složenými	složený	k2eAgFnPc7d1	složená
závorkami	závorka	k1gFnPc7	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc4	kód
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odsazovat	odsazovat	k5eAaImF	odsazovat
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgInPc1d1	bílý
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
ignorují	ignorovat	k5eAaImIp3nP	ignorovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
praktické	praktický	k2eAgNnSc1d1	praktické
<g/>
.	.	kIx.	.
</s>
<s>
Všimněme	všimnout	k5eAaPmRp1nP	všimnout
si	se	k3xPyFc3	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
identifikátory	identifikátor	k1gInPc1	identifikátor
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
písmenka	písmenko	k1gNnPc1	písmenko
s	s	k7c7	s
háčky	háček	k1gMnPc7	háček
a	a	k8xC	a
čárkami	čárka	k1gFnPc7	čárka
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
od	od	k7c2	od
prvních	první	k4xOgFnPc2	první
verzí	verze	k1gFnPc2	verze
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Překladač	překladač	k1gInSc1	překladač
hledá	hledat	k5eAaImIp3nS	hledat
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
spustitelného	spustitelný	k2eAgInSc2d1	spustitelný
souboru	soubor	k1gInSc2	soubor
vstupní	vstupní	k2eAgInSc4d1	vstupní
bod	bod	k1gInSc4	bod
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
statickou	statický	k2eAgFnSc4d1	statická
metodu	metoda	k1gFnSc4	metoda
nevracející	vracející	k2eNgFnSc4d1	vracející
žádnou	žádný	k3yNgFnSc4	žádný
hodnotu	hodnota	k1gFnSc4	hodnota
nebo	nebo	k8xC	nebo
typ	typ	k1gInSc4	typ
int	int	k?	int
(	(	kIx(	(
<g/>
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
buď	buď	k8xC	buď
nepřebírá	přebírat	k5eNaImIp3nS	přebírat
žádné	žádný	k3yNgInPc4	žádný
argumenty	argument	k1gInPc4	argument
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pole	pole	k1gNnSc1	pole
řetězců	řetězec	k1gInPc2	řetězec
(	(	kIx(	(
<g/>
stringů	string	k1gInPc2	string
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Main	Main	k1gInSc1	Main
<g/>
.	.	kIx.	.
</s>
<s>
Deklaraci	deklarace	k1gFnSc4	deklarace
takové	takový	k3xDgFnSc2	takový
metody	metoda	k1gFnSc2	metoda
vidíme	vidět	k5eAaImIp1nP	vidět
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
řádku	řádek	k1gInSc6	řádek
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
slovo	slovo	k1gNnSc1	slovo
static	statice	k1gFnPc2	statice
značí	značit	k5eAaImIp3nS	značit
statickou	statický	k2eAgFnSc4d1	statická
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
takovou	takový	k3xDgFnSc4	takový
část	část	k1gFnSc4	část
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
volat	volat	k5eAaImF	volat
bez	bez	k7c2	bez
vytvoření	vytvoření	k1gNnSc2	vytvoření
instance	instance	k1gFnSc2	instance
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Klíčové	klíčový	k2eAgNnSc1d1	klíčové
slovo	slovo	k1gNnSc1	slovo
void	void	k6eAd1	void
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
metoda	metoda	k1gFnSc1	metoda
nic	nic	k3yNnSc4	nic
nevrací	vracet	k5eNaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Argumenty	argument	k1gInPc1	argument
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
vypisují	vypisovat	k5eAaImIp3nP	vypisovat
do	do	k7c2	do
obyčejných	obyčejný	k2eAgFnPc2d1	obyčejná
závorek	závorka	k1gFnPc2	závorka
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
při	při	k7c6	při
deklarování	deklarování	k1gNnSc6	deklarování
proměnných	proměnná	k1gFnPc2	proměnná
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
uvádí	uvádět	k5eAaImIp3nS	uvádět
typ	typ	k1gInSc1	typ
proměnné	proměnná	k1gFnSc2	proměnná
(	(	kIx(	(
<g/>
string	string	k1gInSc1	string
<g/>
[	[	kIx(	[
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
args	args	k1gInSc1	args
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gNnSc4	pole
značíme	značit	k5eAaImIp1nP	značit
dvojicí	dvojice	k1gFnSc7	dvojice
hranatých	hranatý	k2eAgFnPc2d1	hranatá
závorek	závorka	k1gFnPc2	závorka
za	za	k7c7	za
názvem	název	k1gInSc7	název
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
metody	metoda	k1gFnSc2	metoda
tvoří	tvořit	k5eAaImIp3nS	tvořit
jediný	jediný	k2eAgInSc4d1	jediný
řádek	řádek	k1gInSc4	řádek
ukončený	ukončený	k2eAgInSc4d1	ukončený
středníkem	středník	k1gInSc7	středník
<g/>
.	.	kIx.	.
</s>
<s>
Volá	volat	k5eAaImIp3nS	volat
statickou	statický	k2eAgFnSc4d1	statická
metodu	metoda	k1gFnSc4	metoda
třídy	třída	k1gFnSc2	třída
Console	Console	k1gFnSc2	Console
(	(	kIx(	(
<g/>
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
jmenném	jmenný	k2eAgInSc6d1	jmenný
prostoru	prostor	k1gInSc6	prostor
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
WriteLine	WriteLin	k1gInSc5	WriteLin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
za	za	k7c4	za
argument	argument	k1gInSc4	argument
pojímá	pojímat	k5eAaImIp3nS	pojímat
jedinou	jediný	k2eAgFnSc4d1	jediná
proměnnou	proměnná	k1gFnSc4	proměnná
typu	typ	k1gInSc2	typ
string	string	k1gInSc4	string
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vypíše	vypsat	k5eAaPmIp3nS	vypsat
uživateli	uživatel	k1gMnSc3	uživatel
do	do	k7c2	do
konzole	konzola	k1gFnSc3	konzola
<g/>
.	.	kIx.	.
</s>
<s>
Řetězce	řetězec	k1gInPc1	řetězec
se	se	k3xPyFc4	se
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
počítačovými	počítačový	k2eAgFnPc7d1	počítačová
uvozovkami	uvozovka	k1gFnPc7	uvozovka
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgNnSc4d1	oficiální
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnSc4	prostředí
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
Express	express	k1gInSc1	express
edice	edice	k1gFnSc2	edice
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
edice	edice	k1gFnPc1	edice
jsou	být	k5eAaImIp3nP	být
zpoplatněny	zpoplatněn	k2eAgFnPc1d1	zpoplatněna
<g/>
.	.	kIx.	.
</s>
<s>
Turbo	turba	k1gFnSc5	turba
C	C	kA	C
<g/>
#	#	kIx~	#
Explorer	Explorer	k1gMnSc1	Explorer
-	-	kIx~	-
nástroj	nástroj	k1gInSc1	nástroj
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Borland	Borland	kA	Borland
SharpDevelop	SharpDevelop	k1gInSc1	SharpDevelop
-	-	kIx~	-
OpenSource	OpenSourka	k1gFnSc6	OpenSourka
nástroj	nástroj	k1gInSc1	nástroj
fungující	fungující	k2eAgInSc1d1	fungující
jen	jen	k9	jen
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
MonoDevelop	MonoDevelop	k1gInSc4	MonoDevelop
-	-	kIx~	-
multiplatformní	multiplatformní	k2eAgInSc4d1	multiplatformní
Open	Open	k1gInSc4	Open
Source	Sourec	k1gInSc2	Sourec
nástroj	nástroj	k1gInSc1	nástroj
využívající	využívající	k2eAgInSc1d1	využívající
Mono	mono	k6eAd1	mono
a	a	k8xC	a
Gtk	Gtk	k1gFnSc1	Gtk
<g/>
#	#	kIx~	#
Baltík	Baltík	k1gInSc1	Baltík
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
programovací	programovací	k2eAgInSc1d1	programovací
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
programování	programování	k1gNnSc2	programování
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
Systém	systém	k1gInSc1	systém
dokumentace	dokumentace	k1gFnSc2	dokumentace
kódu	kód	k1gInSc2	kód
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc4d1	podobný
JavaDoc	JavaDoc	k1gInSc4	JavaDoc
<g/>
,	,	kIx,	,
používanému	používaný	k2eAgInSc3d1	používaný
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
příkaz	příkaz	k1gInSc1	příkaz
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
metodě	metoda	k1gFnSc3	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
řádek	řádek	k1gInSc1	řádek
komentáře	komentář	k1gInSc2	komentář
musí	muset	k5eAaImIp3nS	muset
začínat	začínat	k5eAaImF	začínat
řetězcem	řetězec	k1gInSc7	řetězec
"	"	kIx"	"
<g/>
///	///	k?	///
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
komentáře	komentář	k1gInPc1	komentář
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xC	jako
IntelliSense	IntelliSense	k1gFnSc2	IntelliSense
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gMnSc1	Visual
Studiu	studio	k1gNnSc3	studio
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
programátorovi	programátor	k1gMnSc3	programátor
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
kódu	kód	k1gInSc2	kód
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
notace	notace	k1gFnSc2	notace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
křížek	křížek	k1gInSc1	křížek
označuje	označovat	k5eAaImIp3nS	označovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
noty	nota	k1gFnSc2	nota
o	o	k7c6	o
půl	půl	k1xP	půl
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
označoval	označovat	k5eAaImAgInS	označovat
notu	nota	k1gFnSc4	nota
cis	cis	k1gNnSc2	cis
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
C	C	kA	C
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
o	o	k7c4	o
půl	půl	k1xP	půl
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
jako	jako	k8xS	jako
zlepšení	zlepšení	k1gNnSc1	zlepšení
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
++	++	k?	++
<g/>
"	"	kIx"	"
totiž	totiž	k9	totiž
v	v	k7c6	v
syntaxi	syntax	k1gFnSc6	syntax
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
znamená	znamenat	k5eAaImIp3nS	znamenat
zvýšení	zvýšení	k1gNnSc4	zvýšení
hodnoty	hodnota	k1gFnSc2	hodnota
proměnné	proměnná	k1gFnSc2	proměnná
o	o	k7c4	o
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Křížek	křížek	k1gInSc1	křížek
na	na	k7c6	na
počítačové	počítačový	k2eAgFnSc6d1	počítačová
klávesnici	klávesnice	k1gFnSc6	klávesnice
(	(	kIx(	(
<g/>
#	#	kIx~	#
<g/>
)	)	kIx)	)
a	a	k8xC	a
křížek	křížek	k1gInSc4	křížek
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
nauce	nauka	k1gFnSc6	nauka
(	(	kIx(	(
<g/>
♯	♯	k?	♯
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
odlišné	odlišný	k2eAgInPc4d1	odlišný
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
názvu	název	k1gInSc2	název
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
Sharp	Sharp	kA	Sharp
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
znak	znak	k1gInSc1	znak
hudebního	hudební	k2eAgInSc2d1	hudební
křížku	křížek	k1gInSc2	křížek
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tento	tento	k3xDgInSc1	tento
se	se	k3xPyFc4	se
na	na	k7c6	na
standardní	standardní	k2eAgFnSc6d1	standardní
klávesnici	klávesnice	k1gFnSc6	klávesnice
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
klasický	klasický	k2eAgInSc1d1	klasický
křížek	křížek	k1gInSc1	křížek
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
ve	v	k7c6	v
specifikaci	specifikace	k1gFnSc6	specifikace
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
,	,	kIx,	,
ECMA-	ECMA-	k1gFnSc1	ECMA-
<g/>
334	[number]	k4	334
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
jsme	být	k5eAaImIp1nP	být
již	již	k6eAd1	již
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
praktického	praktický	k2eAgInSc2d1	praktický
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
případech	případ	k1gInPc6	případ
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
marketingové	marketingový	k2eAgInPc1d1	marketingový
materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
znak	znak	k1gInSc1	znak
křížku	křížek	k1gInSc2	křížek
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
notace	notace	k1gFnSc2	notace
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
C	C	kA	C
<g/>
#	#	kIx~	#
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
C	C	kA	C
Sharp	sharp	k1gInSc1	sharp
dotNETportal	dotNETportat	k5eAaPmAgInS	dotNETportat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Český	český	k2eAgInSc4d1	český
portál	portál	k1gInSc4	portál
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
,	,	kIx,	,
Visual	Visual	k1gMnSc1	Visual
Basic	Basic	kA	Basic
a	a	k8xC	a
.	.	kIx.	.
<g/>
NET	NET	kA	NET
Framework	Framework	k1gInSc1	Framework
Srovnání	srovnání	k1gNnSc1	srovnání
Visual	Visual	k1gInSc1	Visual
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
,	,	kIx,	,
Turbo	turba	k1gFnSc5	turba
C	C	kA	C
<g/>
#	#	kIx~	#
a	a	k8xC	a
SharpDevelop	SharpDevelop	k1gInSc4	SharpDevelop
na	na	k7c6	na
serveru	server	k1gInSc6	server
zive	ziv	k1gInSc2	ziv
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
C	C	kA	C
<g/>
#	#	kIx~	#
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
</s>
