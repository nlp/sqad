<s>
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Cataclysm	Cataclysm	k1gMnSc1	Cataclysm
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
3	[number]	k4	3
<g/>
.	.	kIx.	.
datadisk	datadisk	k1gInSc1	datadisk
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
MMORPG	MMORPG	kA	MMORPG
světa	svět	k1gInSc2	svět
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
na	na	k7c6	na
BlizzConu	BlizzCon	k1gInSc6	BlizzCon
2009	[number]	k4	2009
a	a	k8xC	a
vydán	vydán	k2eAgInSc1d1	vydán
7	[number]	k4	7
.	.	kIx.	.
<g/>
12	[number]	k4	12
.	.	kIx.	.
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
3,3	[number]	k4	3,3
milionu	milion	k4xCgInSc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
překonán	překonat	k5eAaPmNgInS	překonat
rekord	rekord	k1gInSc1	rekord
předchozího	předchozí	k2eAgInSc2d1	předchozí
datadisku	datadisek	k1gInSc2	datadisek
Wrath	Wrath	k1gMnSc1	Wrath
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
maximální	maximální	k2eAgFnSc1d1	maximální
úroveň	úroveň	k1gFnSc1	úroveň
postavy	postava	k1gFnSc2	postava
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
na	na	k7c4	na
85	[number]	k4	85
<g/>
.	.	kIx.	.
2	[number]	k4	2
nové	nový	k2eAgFnSc2d1	nová
hratelné	hratelný	k2eAgFnSc2d1	hratelná
rasy	rasa	k1gFnSc2	rasa
<g/>
:	:	kIx,	:
Worgeni	Worgen	k2eAgMnPc1d1	Worgen
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
<g/>
)	)	kIx)	)
a	a	k8xC	a
Goblini	Goblin	k2eAgMnPc1d1	Goblin
(	(	kIx(	(
<g/>
Horda	horda	k1gFnSc1	horda
<g/>
)	)	kIx)	)
nově	nově	k6eAd1	nově
přístupné	přístupný	k2eAgFnPc4d1	přístupná
kombinace	kombinace	k1gFnPc4	kombinace
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
povolání	povolání	k1gNnSc2	povolání
létající	létající	k2eAgFnSc2d1	létající
mounti	mounť	k1gFnSc2	mounť
v	v	k7c4	v
Azeroth	Azeroth	k1gInSc4	Azeroth
Azeroth	Azeroth	k1gInSc1	Azeroth
přetvořen	přetvořen	k2eAgInSc1d1	přetvořen
-	-	kIx~	-
klasické	klasický	k2eAgFnPc1d1	klasická
zóny	zóna	k1gFnPc1	zóna
byly	být	k5eAaImAgFnP	být
postiženy	postihnout	k5eAaPmNgFnP	postihnout
pohromou	pohroma	k1gFnSc7	pohroma
"	"	kIx"	"
<g/>
Cataclysmem	Cataclysm	k1gInSc7	Cataclysm
<g/>
"	"	kIx"	"
nová	nový	k2eAgFnSc1d1	nová
sekundární	sekundární	k2eAgFnSc1d1	sekundární
profese	profese	k1gFnSc1	profese
archeologie	archeologie	k1gFnSc2	archeologie
někteří	některý	k3yIgMnPc1	některý
bossové	boss	k1gMnPc1	boss
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
verze	verze	k1gFnSc2	verze
hry	hra	k1gFnPc1	hra
byli	být	k5eAaImAgMnP	být
oživeni	oživen	k2eAgMnPc1d1	oživen
<g/>
,	,	kIx,	,
přepracováni	přepracován	k2eAgMnPc1d1	přepracován
a	a	k8xC	a
zasazeni	zasazen	k2eAgMnPc1d1	zasazen
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
instancí	instance	k1gFnPc2	instance
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ragnaros	Ragnarosa	k1gFnPc2	Ragnarosa
<g/>
)	)	kIx)	)
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
guild	guild	k1gInSc1	guild
-	-	kIx~	-
levelování	levelování	k1gNnSc1	levelování
guildy	guilda	k1gFnSc2	guilda
s	s	k7c7	s
následnými	následný	k2eAgInPc7d1	následný
bonusy	bonus	k1gInPc7	bonus
a	a	k8xC	a
výhodami	výhoda	k1gFnPc7	výhoda
změna	změna	k1gFnSc1	změna
talentových	talentový	k2eAgInPc2d1	talentový
stromů	strom	k1gInPc2	strom
změna	změna	k1gFnSc1	změna
u	u	k7c2	u
herní	herní	k2eAgFnSc2d1	herní
mechaniky	mechanika	k1gFnSc2	mechanika
některých	některý	k3yIgNnPc2	některý
povolání	povolání	k1gNnPc2	povolání
nová	nový	k2eAgFnSc1d1	nová
sekundární	sekundární	k2eAgFnSc1d1	sekundární
vlastnost	vlastnost	k1gFnSc1	vlastnost
Mastery	master	k1gMnPc4	master
-	-	kIx~	-
unikátní	unikátní	k2eAgMnSc1d1	unikátní
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
povolání	povolání	k1gNnSc4	povolání
změny	změna	k1gFnSc2	změna
některých	některý	k3yIgFnPc2	některý
vlastností	vlastnost	k1gFnPc2	vlastnost
odstranění	odstranění	k1gNnSc2	odstranění
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
vlastností	vlastnost	k1gFnPc2	vlastnost
z	z	k7c2	z
vybavení	vybavení	k1gNnSc2	vybavení
Transmogrification	Transmogrification	k1gInSc1	Transmogrification
-	-	kIx~	-
úprava	úprava	k1gFnSc1	úprava
vzhledu	vzhled	k1gInSc2	vzhled
<g />
.	.	kIx.	.
</s>
<s>
vybavení	vybavení	k1gNnSc1	vybavení
nové	nový	k2eAgInPc1d1	nový
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
zóny	zóna	k1gFnPc1	zóna
<g/>
,	,	kIx,	,
dungeony	dungeon	k1gInPc1	dungeon
<g/>
,	,	kIx,	,
raidy	raid	k1gInPc1	raid
<g/>
,	,	kIx,	,
monstra	monstrum	k1gNnPc1	monstrum
<g/>
,	,	kIx,	,
battlegroundy	battleground	k1gInPc1	battleground
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc1	předmět
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
oživení	oživení	k1gNnSc3	oživení
starých	starý	k2eAgInPc2d1	starý
dungeonů	dungeon	k1gInPc2	dungeon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Deadmines	Deadmines	k1gInSc1	Deadmines
a	a	k8xC	a
Shadowfang	Shadowfang	k1gInSc1	Shadowfang
Keep	Keep	k1gInSc1	Keep
byly	být	k5eAaImAgFnP	být
předěnály	předěnála	k1gFnPc1	předěnála
na	na	k7c6	na
HC	HC	kA	HC
pro	pro	k7c4	pro
hráče	hráč	k1gMnSc4	hráč
s	s	k7c7	s
úrovní	úroveň	k1gFnSc7	úroveň
85	[number]	k4	85
<g/>
,	,	kIx,	,
Zul	Zul	k1gMnSc1	Zul
<g/>
'	'	kIx"	'
<g/>
Gurub	Gurub	k1gMnSc1	Gurub
a	a	k8xC	a
Zul	Zul	k1gMnSc1	Zul
<g/>
'	'	kIx"	'
<g/>
Aman	Aman	k1gInSc1	Aman
byly	být	k5eAaImAgFnP	být
předělány	předělat	k5eAaPmNgInP	předělat
na	na	k7c4	na
HC	HC	kA	HC
dungeony	dungeona	k1gFnSc2	dungeona
pro	pro	k7c4	pro
5	[number]	k4	5
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
Deathwing	Deathwing	k1gInSc1	Deathwing
přinese	přinést	k5eAaPmIp3nS	přinést
na	na	k7c4	na
Azeroth	Azeroth	k1gInSc4	Azeroth
zkázu	zkáza	k1gFnSc4	zkáza
a	a	k8xC	a
komplexně	komplexně	k6eAd1	komplexně
jej	on	k3xPp3gMnSc4	on
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nezmění	změnit	k5eNaPmIp3nS	změnit
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
(	(	kIx(	(
<g/>
Elwynn	Elwynn	k1gMnSc1	Elwynn
Forest	Forest	k1gMnSc1	Forest
<g/>
,	,	kIx,	,
Winterspring	Winterspring	k1gInSc1	Winterspring
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc4	oblast
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
trochu	trochu	k6eAd1	trochu
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
Durotar	Durotara	k1gFnPc2	Durotara
<g/>
,	,	kIx,	,
Ashenvale	Ashenvala	k1gFnSc6	Ashenvala
<g/>
,	,	kIx,	,
Tanaris	Tanaris	k1gFnSc6	Tanaris
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nP	změnit
k	k	k7c3	k
nepoznání	nepoznání	k1gNnSc3	nepoznání
(	(	kIx(	(
<g/>
rozdělení	rozdělení	k1gNnPc1	rozdělení
Barrens	Barrensa	k1gFnPc2	Barrensa
<g/>
,	,	kIx,	,
zatopení	zatopení	k1gNnSc1	zatopení
Thousand	Thousanda	k1gFnPc2	Thousanda
Needless	Needlessa	k1gFnPc2	Needlessa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
se	se	k3xPyFc4	se
výrázně	výrázně	k6eAd1	výrázně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
ta	ten	k3xDgFnSc1	ten
země	země	k1gFnSc1	země
u	u	k7c2	u
Teldrassilu	Teldrassil	k1gInSc2	Teldrassil
(	(	kIx(	(
<g/>
hádanka	hádanka	k1gFnSc1	hádanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rasa	rasa	k1gFnSc1	rasa
již	již	k6eAd1	již
ve	v	k7c6	v
WoW	WoW	k1gFnSc6	WoW
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
hratelnou	hratelný	k2eAgFnSc7d1	hratelná
<g/>
.	.	kIx.	.
</s>
<s>
Gobliny	Goblina	k1gFnPc1	Goblina
můžete	moct	k5eAaImIp2nP	moct
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
v	v	k7c6	v
Ratchetu	Ratchet	k1gInSc6	Ratchet
<g/>
,	,	kIx,	,
Gadgetzanu	Gadgetzan	k1gInSc6	Gadgetzan
nebo	nebo	k8xC	nebo
Booty	Boota	k1gFnSc2	Boota
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
přístupná	přístupný	k2eAgFnSc1d1	přístupná
všechna	všechen	k3xTgNnPc4	všechen
povolání	povolání	k1gNnSc4	povolání
kromě	kromě	k7c2	kromě
paladina	paladin	k1gMnSc2	paladin
a	a	k8xC	a
druida	druid	k1gMnSc2	druid
<g/>
.	.	kIx.	.
</s>
<s>
Rasové	rasový	k2eAgFnPc1d1	rasová
schopnosti	schopnost	k1gFnPc1	schopnost
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
zahození	zahození	k1gNnSc3	zahození
(	(	kIx(	(
<g/>
vystřelení	vystřelení	k1gNnSc3	vystřelení
rakety	raketa	k1gFnSc2	raketa
nebo	nebo	k8xC	nebo
sebe	sebe	k3xPyFc4	sebe
-	-	kIx~	-
společný	společný	k2eAgInSc1d1	společný
Cooldown	Cooldown	k1gInSc1	Cooldown
<g/>
;	;	kIx,	;
Time	Time	k1gInSc1	Time
is	is	k?	is
Money	Monea	k1gFnSc2	Monea
-	-	kIx~	-
zvýšení	zvýšení	k1gNnSc3	zvýšení
útočné	útočný	k2eAgFnSc2d1	útočná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
;	;	kIx,	;
Pack	Pack	k1gInSc1	Pack
Hobogoblin	Hobogoblin	k1gInSc1	Hobogoblin
-	-	kIx~	-
"	"	kIx"	"
<g/>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
banka	banka	k1gFnSc1	banka
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Best	Best	k2eAgInSc1d1	Best
Deals	Deals	k1gInSc1	Deals
Everywhere	Everywher	k1gInSc5	Everywher
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
možná	možný	k2eAgFnSc1d1	možná
sleva	sleva	k1gFnSc1	sleva
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
reputaci	reputace	k1gFnSc4	reputace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
startovní	startovní	k2eAgFnSc1d1	startovní
lokace	lokace	k1gFnSc1	lokace
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kezan	Kezana	k1gFnPc2	Kezana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
před	před	k7c7	před
výbuchem	výbuch	k1gInSc7	výbuch
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zaprodáte	zaprodat	k5eAaPmIp2nP	zaprodat
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgInSc7	svůj
jměním	jmění	k1gNnSc7	jmění
obchodnímu	obchodní	k2eAgMnSc3d1	obchodní
princi	princ	k1gMnSc3	princ
Gallywixovi	Gallywixa	k1gMnSc3	Gallywixa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vás	vy	k3xPp2nPc4	vy
vzal	vzít	k5eAaPmAgMnS	vzít
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
únikovou	únikový	k2eAgFnSc4d1	úniková
loď	loď	k1gFnSc4	loď
potopí	potopit	k5eAaPmIp3nS	potopit
loď	loď	k1gFnSc1	loď
Aliance	aliance	k1gFnSc1	aliance
při	při	k7c6	při
přestřelce	přestřelka	k1gFnSc6	přestřelka
s	s	k7c7	s
Hordou	horda	k1gFnSc7	horda
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
vyplaveni	vyplaven	k2eAgMnPc1d1	vyplaven
skoro	skoro	k6eAd1	skoro
mrtví	mrtvý	k1gMnPc1	mrtvý
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Lost	Lost	k2eAgInSc4d1	Lost
Isles	Isles	k1gInSc4	Isles
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
postupně	postupně	k6eAd1	postupně
zjistíte	zjistit	k5eAaPmIp2nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Aliance	aliance	k1gFnSc1	aliance
útočila	útočit	k5eAaImAgFnS	útočit
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vezla	vézt	k5eAaImAgFnS	vézt
Thralla	Thrall	k1gMnSc4	Thrall
a	a	k8xC	a
unesla	unést	k5eAaPmAgFnS	unést
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
záchraně	záchrana	k1gFnSc6	záchrana
vám	vy	k3xPp2nPc3	vy
dovolí	dovolit	k5eAaPmIp3nS	dovolit
přidat	přidat	k5eAaPmF	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
Hordě	horda	k1gFnSc3	horda
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
frakcí	frakce	k1gFnSc7	frakce
skřetů	skřet	k1gMnPc2	skřet
z	z	k7c2	z
Kezanu	Kezan	k1gInSc2	Kezan
a	a	k8xC	a
Gallywix	Gallywix	k1gInSc4	Gallywix
je	být	k5eAaImIp3nS	být
varován	varovat	k5eAaImNgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prozatím	prozatím	k6eAd1	prozatím
zůstane	zůstat	k5eAaPmIp3nS	zůstat
vůdcem	vůdce	k1gMnSc7	vůdce
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
tolerováno	tolerován	k2eAgNnSc1d1	tolerováno
<g/>
.	.	kIx.	.
</s>
<s>
Rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
vlků	vlk	k1gMnPc2	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
boj	boj	k1gInSc4	boj
budete	být	k5eAaImBp2nP	být
mít	mít	k5eAaImF	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
chcete	chtít	k5eAaImIp2nP	chtít
mít	mít	k5eAaImF	mít
vzhled	vzhled	k1gInSc4	vzhled
člověka	člověk	k1gMnSc2	člověk
nebo	nebo	k8xC	nebo
vlkodlaka	vlkodlak	k1gMnSc2	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
automaticky	automaticky	k6eAd1	automaticky
přepne	přepnout	k5eAaPmIp3nS	přepnout
vzhled	vzhled	k1gInSc4	vzhled
na	na	k7c4	na
vlkodlaka	vlkodlak	k1gMnSc4	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
startovní	startovní	k2eAgFnSc1d1	startovní
lokace	lokace	k1gFnSc1	lokace
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Gilneas	Gilneasa	k1gFnPc2	Gilneasa
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Silverpine	Silverpin	k1gInSc5	Silverpin
Forest	Forest	k1gFnSc4	Forest
<g/>
,	,	kIx,	,
za	za	k7c4	za
Greymane	Greyman	k1gMnSc5	Greyman
Wall	Wall	k1gInSc4	Wall
<g/>
.	.	kIx.	.
</s>
<s>
Rasové	rasový	k2eAgFnPc1d1	rasová
schopnosti	schopnost	k1gFnPc1	schopnost
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
40	[number]	k4	40
%	%	kIx~	%
sprint	sprint	k1gInSc4	sprint
na	na	k7c4	na
6	[number]	k4	6
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
šance	šance	k1gFnSc2	šance
na	na	k7c4	na
critical	criticat	k5eAaPmAgInS	criticat
strike	strik	k1gMnPc4	strik
o	o	k7c4	o
1	[number]	k4	1
%	%	kIx~	%
<g/>
,	,	kIx,	,
zmenšení	zmenšení	k1gNnSc2	zmenšení
trvání	trvání	k1gNnSc2	trvání
Curse	Curse	k1gFnSc2	Curse
a	a	k8xC	a
Disease	Diseasa	k1gFnSc6	Diseasa
o	o	k7c4	o
15	[number]	k4	15
%	%	kIx~	%
<g/>
,	,	kIx,	,
ke	k	k7c3	k
skinningu	skinning	k1gInSc3	skinning
přibude	přibýt	k5eAaPmIp3nS	přibýt
bonus	bonus	k1gInSc1	bonus
+15	+15	k4	+15
skillů	skill	k1gMnPc2	skill
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
stát	stát	k1gInSc1	stát
Gilneas	Gilneas	k1gInSc1	Gilneas
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
válkou	válka	k1gFnSc7	válka
od	od	k7c2	od
Aliance	aliance	k1gFnSc2	aliance
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
zmítán	zmítat	k5eAaImNgInS	zmítat
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
mezi	mezi	k7c7	mezi
nekontrolovanými	kontrolovaný	k2eNgInPc7d1	nekontrolovaný
Worgeny	Worgen	k1gInPc7	Worgen
a	a	k8xC	a
zbytky	zbytek	k1gInPc1	zbytek
lidskosti	lidskost	k1gFnSc2	lidskost
a	a	k8xC	a
náhlým	náhlý	k2eAgInSc7d1	náhlý
útokem	útok	k1gInSc7	útok
nemrtvých	nemrtvý	k1gMnPc2	nemrtvý
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc7	jejich
jedinou	jediný	k2eAgFnSc7d1	jediná
nadějí	naděje	k1gFnSc7	naděje
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
k	k	k7c3	k
Alianci	aliance	k1gFnSc3	aliance
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
ji	on	k3xPp3gFnSc4	on
pojí	pojíst	k5eAaPmIp3nS	pojíst
lidský	lidský	k2eAgInSc1d1	lidský
původ	původ	k1gInSc1	původ
a	a	k8xC	a
dávná	dávný	k2eAgFnSc1d1	dávná
přítomnost	přítomnost	k1gFnSc1	přítomnost
nočních	noční	k2eAgMnPc2d1	noční
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
vlčí	vlčí	k2eAgNnSc4d1	vlčí
prokletí	prokletí	k1gNnSc4	prokletí
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
profese	profese	k1gFnSc1	profese
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
sbíráte	sbírat	k5eAaImIp2nP	sbírat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
legendárních	legendární	k2eAgInPc6d1	legendární
předmětech	předmět	k1gInPc6	předmět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
můžete	moct	k5eAaImIp2nP	moct
i	i	k9	i
nějaké	nějaký	k3yIgInPc1	nějaký
prastaré	prastarý	k2eAgInPc1d1	prastarý
artefakty	artefakt	k1gInPc1	artefakt
sami	sám	k3xTgMnPc1	sám
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
z	z	k7c2	z
úlomků	úlomek	k1gInPc2	úlomek
je	být	k5eAaImIp3nS	být
sestavit	sestavit	k5eAaPmF	sestavit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
artefaktů	artefakt	k1gInPc2	artefakt
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
jen	jen	k9	jen
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obohatí	obohatit	k5eAaPmIp3nS	obohatit
svět	svět	k1gInSc4	svět
a	a	k8xC	a
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
jich	on	k3xPp3gFnPc2	on
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
okrasnou	okrasný	k2eAgFnSc4d1	okrasná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
noví	nový	k2eAgMnPc1d1	nový
mounti	mount	k1gMnPc1	mount
nebo	nebo	k8xC	nebo
mazlíčci	mazlíček	k1gMnPc1	mazlíček
a	a	k8xC	a
jen	jen	k6eAd1	jen
pár	pár	k4xCyI	pár
bude	být	k5eAaImBp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Blackrock	Blackrock	k1gInSc1	Blackrock
Caverns	Caverns	k1gInSc1	Caverns
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
5	[number]	k4	5
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Deadmines	Deadmines	k1gInSc1	Deadmines
-	-	kIx~	-
v	v	k7c6	v
Cataclysmu	Cataclysm	k1gInSc6	Cataclysm
bylo	být	k5eAaImAgNnS	být
tomuto	tento	k3xDgInSc3	tento
clasic	clasic	k1gMnSc1	clasic
dungeonu	dungeon	k1gInSc2	dungeon
přidán	přidán	k2eAgMnSc1d1	přidán
i	i	k8xC	i
HC	HC	kA	HC
mod	mod	k?	mod
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
levelu	level	k1gInSc2	level
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
<s>
Normal	Normal	k1gInSc1	Normal
mod	mod	k?	mod
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
End	End	k?	End
Time	Time	k1gFnSc1	Time
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
náhodně	náhodně	k6eAd1	náhodně
2	[number]	k4	2
ze	z	k7c2	z
4	[number]	k4	4
bossů	boss	k1gMnPc2	boss
plus	plus	k6eAd1	plus
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc4d1	stejný
poslední	poslední	k2eAgInSc4d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
obtížnost	obtížnost	k1gFnSc1	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Dungeon	Dungeon	k1gInSc1	Dungeon
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.3	[number]	k4	4.3
<g/>
.	.	kIx.	.
</s>
<s>
Grim	Grim	k1gMnSc1	Grim
Batol	batolit	k5eAaImRp2nS	batolit
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
4	[number]	k4	4
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Hour	Hour	k1gInSc1	Hour
of	of	k?	of
Twilight	Twilight	k1gInSc1	Twilight
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
3	[number]	k4	3
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
obtížnost	obtížnost	k1gFnSc1	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Dungeon	Dungeon	k1gInSc1	Dungeon
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.3	[number]	k4	4.3
<g/>
.	.	kIx.	.
</s>
<s>
Lost	Lost	k1gMnSc1	Lost
City	City	k1gFnSc2	City
of	of	k?	of
the	the	k?	the
Tol	tol	k1gInSc1	tol
<g/>
'	'	kIx"	'
<g/>
vir	vir	k1gInSc1	vir
-	-	kIx~	-
jdou	jít	k5eAaImIp3nP	jít
zde	zde	k6eAd1	zde
4	[number]	k4	4
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Shadowfang	Shadowfang	k1gMnSc1	Shadowfang
Keep	Keep	k1gMnSc1	Keep
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
Deadmines	Deadminesa	k1gFnPc2	Deadminesa
i	i	k9	i
tomuto	tento	k3xDgMnSc3	tento
clasic	clasice	k1gFnPc2	clasice
dungeonu	dungeon	k1gInSc2	dungeon
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
HC	HC	kA	HC
mod	mod	k?	mod
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
levelu	level	k1gInSc2	level
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
<s>
Normal	Normal	k1gInSc1	Normal
mod	mod	k?	mod
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Stonecore	Stonecor	k1gMnSc5	Stonecor
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
4	[number]	k4	4
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Vortex	Vortex	k1gInSc1	Vortex
Pinnacle	Pinnacle	k1gFnSc1	Pinnacle
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
3	[number]	k4	3
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Throne	Thron	k1gMnSc5	Thron
of	of	k?	of
the	the	k?	the
Tides	Tides	k1gInSc1	Tides
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
5	[number]	k4	5
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Well	Well	k1gInSc4	Well
of	of	k?	of
Eternity	eternit	k1gInPc4	eternit
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
3	[number]	k4	3
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
obtížnost	obtížnost	k1gFnSc1	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Dungeon	Dungeon	k1gInSc1	Dungeon
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.3	[number]	k4	4.3
<g/>
.	.	kIx.	.
</s>
<s>
Zul	Zul	k1gMnSc1	Zul
<g/>
́	́	k?	́
<g/>
Aman	Aman	k1gMnSc1	Aman
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
instance	instance	k1gFnSc1	instance
pro	pro	k7c4	pro
10	[number]	k4	10
hráčů	hráč	k1gMnPc2	hráč
byla	být	k5eAaImAgFnS	být
předělána	předělat	k5eAaPmNgFnS	předělat
na	na	k7c4	na
dungeon	dungeon	k1gInSc4	dungeon
pro	pro	k7c4	pro
5	[number]	k4	5
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
HC	HC	kA	HC
modu	modus	k1gInSc6	modus
<g/>
.	.	kIx.	.
</s>
<s>
Dungeon	Dungeon	k1gInSc1	Dungeon
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.1	[number]	k4	4.1
<g/>
.	.	kIx.	.
</s>
<s>
Zul	Zul	k1gMnSc1	Zul
<g/>
́	́	k?	́
<g/>
Gurub	Gurub	k1gMnSc1	Gurub
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
instance	instance	k1gFnSc1	instance
pro	pro	k7c4	pro
40	[number]	k4	40
hráčů	hráč	k1gMnPc2	hráč
byla	být	k5eAaImAgFnS	být
předělána	předělat	k5eAaPmNgFnS	předělat
na	na	k7c4	na
dungeon	dungeon	k1gInSc4	dungeon
pro	pro	k7c4	pro
5	[number]	k4	5
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
HC	HC	kA	HC
modu	modus	k1gInSc6	modus
<g/>
.	.	kIx.	.
</s>
<s>
Dungeon	Dungeon	k1gInSc1	Dungeon
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.1	[number]	k4	4.1
<g/>
.	.	kIx.	.
</s>
<s>
Baradin	Baradin	k2eAgInSc1d1	Baradin
Hold	hold	k1gInSc1	hold
-	-	kIx~	-
Je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgMnSc1d1	podobný
VoA	VoA	k1gMnSc1	VoA
z	z	k7c2	z
datadisku	datadisek	k1gInSc2	datadisek
WotLK	WotLK	k1gFnSc2	WotLK
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
3	[number]	k4	3
bossové	boss	k1gMnPc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Bastion	bastion	k1gInSc1	bastion
of	of	k?	of
Twilight	Twilight	k1gInSc1	Twilight
-	-	kIx~	-
Raid	raid	k1gInSc1	raid
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
boss	boss	k1gMnSc1	boss
je	být	k5eAaImIp3nS	být
Cho	cho	k0	cho
<g/>
́	́	k?	́
<g/>
gall	galla	k1gFnPc2	galla
<g/>
.	.	kIx.	.
</s>
<s>
HC	HC	kA	HC
mod	mod	k?	mod
skrývá	skrývat	k5eAaImIp3nS	skrývat
ještě	ještě	k9	ještě
jednoho	jeden	k4xCgMnSc4	jeden
bosse	boss	k1gMnSc4	boss
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
Synestra	Synestra	k1gFnSc1	Synestra
<g/>
.	.	kIx.	.
</s>
<s>
Blackwing	Blackwing	k1gInSc4	Blackwing
Descent	Descent	k1gInSc4	Descent
-	-	kIx~	-
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
raidu	raid	k1gInSc6	raid
najdeme	najít	k5eAaPmIp1nP	najít
šest	šest	k4xCc4	šest
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
boss	boss	k1gMnSc1	boss
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgMnSc1d1	legendární
drak	drak	k1gMnSc1	drak
Nefarian	Nefarian	k1gMnSc1	Nefarian
<g/>
.	.	kIx.	.
</s>
<s>
Dragon	Dragon	k1gMnSc1	Dragon
Soul	Soul	k1gInSc1	Soul
-	-	kIx~	-
Raid	raid	k1gInSc1	raid
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.3	[number]	k4	4.3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
osm	osm	k4xCc1	osm
bossů	boss	k1gMnPc2	boss
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgInPc2	dva
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
posledním	poslední	k2eAgInSc7d1	poslední
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
bossem	boss	k1gMnSc7	boss
datadisku	datadisek	k1gInSc2	datadisek
Cataclysm	Cataclysm	k1gInSc1	Cataclysm
Deathwingem	Deathwing	k1gInSc7	Deathwing
<g/>
.	.	kIx.	.
</s>
<s>
Firelands	Firelands	k1gInSc1	Firelands
-	-	kIx~	-
Tento	tento	k3xDgInSc1	tento
raid	raid	k1gInSc1	raid
přinesl	přinést	k5eAaPmAgInS	přinést
patch	patch	k1gInSc4	patch
4.2	[number]	k4	4.2
a	a	k8xC	a
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sedm	sedm	k4xCc4	sedm
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
boss	boss	k1gMnSc1	boss
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgMnSc1d1	legendární
Ragnaros	Ragnarosa	k1gFnPc2	Ragnarosa
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
instance	instance	k1gFnSc2	instance
Molten	Molten	k2eAgInSc4d1	Molten
Core	Core	k1gInSc4	Core
<g/>
.	.	kIx.	.
</s>
<s>
Thone	Thonout	k5eAaImIp3nS	Thonout
of	of	k?	of
Four	Four	k1gInSc1	Four
Winds	Winds	k1gInSc1	Winds
-	-	kIx~	-
Raid	raid	k1gInSc1	raid
skládající	skládající	k2eAgInSc1d1	skládající
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
bossů	boss	k1gMnPc2	boss
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
boss	boss	k1gMnSc1	boss
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc4	tři
NPC	NPC	kA	NPC
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
boss	boss	k1gMnSc1	boss
je	být	k5eAaImIp3nS	být
Al	ala	k1gFnPc2	ala
<g/>
́	́	k?	́
<g/>
Akir	Akira	k1gFnPc2	Akira
<g/>
.	.	kIx.	.
</s>
<s>
Pomoci	pomoc	k1gFnPc1	pomoc
funkce	funkce	k1gFnSc2	funkce
Reforge	Reforg	k1gFnSc2	Reforg
lze	lze	k6eAd1	lze
přesunovat	přesunovat	k5eAaImF	přesunovat
body	bod	k1gInPc4	bod
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
schopnosti	schopnost	k1gFnSc2	schopnost
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Přesunout	přesunout	k5eAaPmF	přesunout
jde	jít	k5eAaImIp3nS	jít
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
40	[number]	k4	40
%	%	kIx~	%
bodů	bod	k1gInPc2	bod
dané	daný	k2eAgFnSc2d1	daná
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Body	bod	k1gInPc4	bod
však	však	k9	však
lze	lze	k6eAd1	lze
přesunovat	přesunovat	k5eAaImF	přesunovat
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
sekundárními	sekundární	k2eAgFnPc7d1	sekundární
schopnostmi	schopnost	k1gFnPc7	schopnost
a	a	k8xC	a
spiritem	spiritem	k?	spiritem
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
primární	primární	k2eAgFnPc4d1	primární
schopnosti	schopnost	k1gFnPc4	schopnost
takto	takto	k6eAd1	takto
měnit	měnit	k5eAaImF	měnit
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
