<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
Ц	Ц	k?	Ц
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Crna	Crn	k2eAgFnSc1d1	Crna
Gora	Gora	k1gFnSc1	Gora
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
černohorsky	černohorsky	k6eAd1	černohorsky
Republika	republika	k1gFnSc1	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
sousedící	sousedící	k2eAgNnSc1d1	sousedící
s	s	k7c7	s
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
<g/>
,	,	kIx,	,
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
<g/>
,	,	kIx,	,
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
,	,	kIx,	,
Albánií	Albánie	k1gFnSc7	Albánie
a	a	k8xC	a
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
13	[number]	k4	13
812	[number]	k4	812
km2	km2	k4	km2
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
666	[number]	k4	666
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
Podgorica	Podgorica	k1gFnSc1	Podgorica
(	(	kIx(	(
<g/>
173	[number]	k4	173
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
členem	člen	k1gMnSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kandidátskou	kandidátský	k2eAgFnSc7d1	kandidátská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dobách	doba	k1gFnPc6	doba
po	po	k7c6	po
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
Ilyry	Ilyr	k1gMnPc4	Ilyr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
byli	být	k5eAaImAgMnP	být
po	po	k7c4	po
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
Slovany	Slovan	k1gMnPc4	Slovan
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tu	tu	k6eAd1	tu
existovalo	existovat	k5eAaImAgNnS	existovat
po	po	k7c4	po
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
století	století	k1gNnPc2	století
knížectví	knížectví	k1gNnSc2	knížectví
Zeta	Zet	k1gMnSc4	Zet
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
Zety	Zet	k1gMnPc4	Zet
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nesmířila	smířit	k5eNaPmAgFnS	smířit
a	a	k8xC	a
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
do	do	k7c2	do
nepřístupných	přístupný	k2eNgFnPc2d1	nepřístupná
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založila	založit	k5eAaPmAgFnS	založit
samotný	samotný	k2eAgInSc4d1	samotný
stát	stát	k1gInSc4	stát
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnPc1	izolace
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
tak	tak	k9	tak
státeček	státeček	k1gInSc4	státeček
sice	sice	k8xC	sice
ochránila	ochránit	k5eAaPmAgFnS	ochránit
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
brzdila	brzdit	k5eAaImAgFnS	brzdit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
společenský	společenský	k2eAgInSc4d1	společenský
a	a	k8xC	a
technický	technický	k2eAgInSc4d1	technický
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
klany	klan	k1gInPc4	klan
<g/>
.	.	kIx.	.
</s>
<s>
Černohorci	Černohorec	k1gMnPc1	Černohorec
museli	muset	k5eAaImAgMnP	muset
často	často	k6eAd1	často
odrážet	odrážet	k5eAaImF	odrážet
útoky	útok	k1gInPc4	útok
osmanských	osmanský	k2eAgInPc2d1	osmanský
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Černou	Černá	k1gFnSc4	Černá
Horu	hora	k1gFnSc4	hora
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
neovládli	ovládnout	k5eNaPmAgMnP	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
tu	tu	k6eAd1	tu
vládli	vládnout	k5eAaImAgMnP	vládnout
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
metropolité	metropolita	k1gMnPc1	metropolita
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Cetinji	Cetinj	k1gFnSc6	Cetinj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
světským	světský	k2eAgNnSc7d1	světské
knížectvím	knížectví	k1gNnSc7	knížectví
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Petrovićů-Njegošů	Petrovićů-Njegoš	k1gMnPc2	Petrovićů-Njegoš
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgFnSc4d1	definitivní
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
uznání	uznání	k1gNnSc4	uznání
získala	získat	k5eAaPmAgFnS	získat
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
po	po	k7c6	po
rusko-turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Knížectví	knížectví	k1gNnSc1	knížectví
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
balkánských	balkánský	k2eAgFnPc2d1	balkánská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
islamizaci	islamizace	k1gFnSc4	islamizace
<g/>
,	,	kIx,	,
Černá	černat	k5eAaImIp3nS	černat
Hora	hora	k1gFnSc1	hora
žádnými	žádný	k3yNgFnPc7	žádný
většími	veliký	k2eAgFnPc7d2	veliký
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
náboženském	náboženský	k2eAgNnSc6d1	náboženské
složení	složení	k1gNnSc6	složení
neprošla	projít	k5eNaPmAgFnS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
i	i	k9	i
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1916	[number]	k4	1916
obsazena	obsadit	k5eAaPmNgFnS	obsadit
rakousko-uherskou	rakouskoherský	k2eAgFnSc7d1	rakousko-uherská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
připojena	připojit	k5eAaPmNgFnS	připojit
ke	k	k7c3	k
Království	království	k1gNnSc3	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
..	..	k?	..
Srbskem	Srbsko	k1gNnSc7	Srbsko
ustavená	ustavený	k2eAgFnSc1d1	ustavená
Podgorická	Podgorický	k2eAgFnSc1d1	Podgorická
skupština	skupština	k1gFnSc1	skupština
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
vydala	vydat	k5eAaPmAgFnS	vydat
usnesení	usnesení	k1gNnSc4	usnesení
jímž	jenž	k3xRgInSc7	jenž
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
připojení	připojení	k1gNnSc4	připojení
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
k	k	k7c3	k
Srbsku	Srbsko	k1gNnSc3	Srbsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Království	království	k1gNnSc2	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nadále	nadále	k6eAd1	nadále
existovala	existovat	k5eAaImAgFnS	existovat
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
původního	původní	k2eAgNnSc2d1	původní
Černohorského	Černohorského	k2eAgNnSc2d1	Černohorského
království	království	k1gNnSc2	království
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
Bokou	boka	k1gFnSc7	boka
Kotorskou	kotorský	k2eAgFnSc7d1	Kotorská
do	do	k7c2	do
nově	nova	k1gFnSc3	nova
zřízené	zřízený	k2eAgFnSc2d1	zřízená
Zetské	Zetský	k2eAgFnSc2d1	Zetský
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
menší	malý	k2eAgFnSc1d2	menší
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
bývalého	bývalý	k2eAgNnSc2d1	bývalé
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
území	území	k1gNnSc1	území
moderních	moderní	k2eAgNnPc2d1	moderní
měst	město	k1gNnPc2	město
Pljevlja	Pljevlj	k1gInSc2	Pljevlj
a	a	k8xC	a
Bijelo	Bijela	k1gFnSc5	Bijela
Polje	Polj	k1gMnSc2	Polj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgFnSc2d1	zřízená
Užické	Užický	k2eAgFnSc2d1	Užický
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
administrativní	administrativní	k2eAgFnSc6d1	administrativní
reformě	reforma	k1gFnSc6	reforma
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
přestaly	přestat	k5eAaPmAgFnP	přestat
hranice	hranice	k1gFnPc1	hranice
původního	původní	k2eAgNnSc2d1	původní
království	království	k1gNnSc2	království
zcela	zcela	k6eAd1	zcela
existovat	existovat	k5eAaImF	existovat
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
moderní	moderní	k2eAgFnSc2d1	moderní
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
bylo	být	k5eAaImAgNnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
územím	území	k1gNnSc7	území
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Republiky	republika	k1gFnSc2	republika
Dubrovník	Dubrovník	k1gInSc1	Dubrovník
<g/>
,	,	kIx,	,
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
<g/>
,	,	kIx,	,
jihozápadem	jihozápad	k1gInSc7	jihozápad
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
severem	sever	k1gInSc7	sever
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kosova	Kosův	k2eAgInSc2d1	Kosův
začleněno	začleněn	k2eAgNnSc4d1	začleněno
do	do	k7c2	do
Zetské	Zetský	k2eAgFnSc2d1	Zetský
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cíp	cíp	k1gInSc1	cíp
původního	původní	k2eAgNnSc2d1	původní
Černohorského	Černohorského	k2eAgNnSc2d1	Černohorského
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dnes	dnes	k6eAd1	dnes
kosovského	kosovský	k2eAgNnSc2d1	kosovské
města	město	k1gNnSc2	město
Peć	Peć	k1gFnSc2	Peć
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
Vardarské	Vardarský	k2eAgFnSc2d1	Vardarská
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
vydržel	vydržet	k5eAaPmAgInS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
obsazena	obsadit	k5eAaPmNgFnS	obsadit
jednotkami	jednotka	k1gFnPc7	jednotka
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
Černohorské	Černohorské	k2eAgNnSc1d1	Černohorské
království	království	k1gNnSc1	království
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
jako	jako	k8xS	jako
loutkový	loutkový	k2eAgInSc1d1	loutkový
stát	stát	k1gInSc1	stát
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
moderní	moderní	k2eAgFnSc2d1	moderní
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
včetně	včetně	k7c2	včetně
okrajového	okrajový	k2eAgNnSc2d1	okrajové
území	území	k1gNnSc2	území
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
Srbska	Srbsko	k1gNnSc2	Srbsko
(	(	kIx(	(
<g/>
Prijepolje	Prijepolje	k1gFnSc1	Prijepolje
a	a	k8xC	a
Sjenica	Sjenica	k1gFnSc1	Sjenica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
nenáležela	náležet	k5eNaImAgFnS	náležet
Boka	boka	k1gFnSc1	boka
Kotorska	Kotorska	k1gFnSc1	Kotorska
(	(	kIx(	(
<g/>
připojená	připojený	k2eAgFnSc1d1	připojená
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
Ustašovskému	ustašovský	k2eAgNnSc3d1	ustašovské
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
okrajové	okrajový	k2eAgFnSc2d1	okrajová
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgFnSc2d1	připojená
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
Albánii	Albánie	k1gFnSc3	Albánie
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
jako	jako	k8xS	jako
Lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
republik	republika	k1gFnPc2	republika
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
SFRJ	SFRJ	kA	SFRJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
původního	původní	k2eAgMnSc2d1	původní
Černohorského	Černohorský	k1gMnSc2	Černohorský
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
existujícího	existující	k2eAgInSc2d1	existující
před	před	k7c7	před
sloučením	sloučení	k1gNnSc7	sloučení
se	s	k7c7	s
Srbským	srbský	k2eAgNnSc7d1	srbské
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
Boka	boka	k1gFnSc1	boka
Kotorska	Kotorska	k1gFnSc1	Kotorska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
součástí	součást	k1gFnSc7	součást
původního	původní	k2eAgNnSc2d1	původní
království	království	k1gNnSc2	království
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgNnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
původního	původní	k2eAgNnSc2d1	původní
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
město	město	k1gNnSc4	město
Brodarevo	Brodarevo	k1gNnSc1	Brodarevo
a	a	k8xC	a
Albánci	Albánec	k1gMnPc1	Albánec
osídlené	osídlený	k2eAgNnSc4d1	osídlené
širší	široký	k2eAgNnSc4d2	širší
okolí	okolí	k1gNnSc4	okolí
města	město	k1gNnSc2	město
Peć	Peć	k1gMnSc2	Peć
(	(	kIx(	(
<g/>
města	město	k1gNnSc2	město
Pejë	Pejë	k1gFnSc2	Pejë
<g/>
/	/	kIx~	/
<g/>
Peć	Peć	k1gMnSc1	Peć
<g/>
,	,	kIx,	,
Gjakovë	Gjakovë	k1gMnSc1	Gjakovë
<g/>
/	/	kIx~	/
<g/>
Đakovica	Đakovica	k1gMnSc1	Đakovica
<g/>
,	,	kIx,	,
Deçan	Deçan	k1gMnSc1	Deçan
<g/>
/	/	kIx~	/
<g/>
Dečani	Dečan	k1gMnPc1	Dečan
<g/>
,	,	kIx,	,
Burim	Burim	k1gMnSc1	Burim
<g/>
/	/	kIx~	/
<g/>
Istok	Istok	k1gInSc1	Istok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Srbska	Srbsko	k1gNnSc2	Srbsko
součástí	součást	k1gFnPc2	součást
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
z	z	k7c2	z
dvacetitisícové	dvacetitisícový	k2eAgFnSc2d1	dvacetitisícová
Cetinje	Cetinj	k1gFnSc2	Cetinj
do	do	k7c2	do
Podgorice	Podgorika	k1gFnSc3	Podgorika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
vůdce	vůdce	k1gMnSc2	vůdce
země	zem	k1gFnSc2	zem
a	a	k8xC	a
partyzánského	partyzánský	k2eAgMnSc2d1	partyzánský
bojovníka	bojovník	k1gMnSc2	bojovník
Tita	Tit	k1gInSc2	Tit
přejmenována	přejmenován	k2eAgMnSc4d1	přejmenován
na	na	k7c4	na
Titograd	Titograd	k1gInSc4	Titograd
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
metropoli	metropole	k1gFnSc4	metropole
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkolepé	velkolepý	k2eAgFnSc3d1	velkolepá
přestavbě	přestavba	k1gFnSc3	přestavba
města	město	k1gNnSc2	město
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
chudší	chudý	k2eAgMnSc1d2	chudší
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
federace	federace	k1gFnSc2	federace
dostávala	dostávat	k5eAaImAgFnS	dostávat
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
mnoho	mnoho	k6eAd1	mnoho
investičních	investiční	k2eAgInPc2d1	investiční
prostředků	prostředek	k1gInPc2	prostředek
od	od	k7c2	od
bohatších	bohatý	k2eAgMnPc2d2	bohatší
členů	člen	k1gMnPc2	člen
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
však	však	k9	však
skončilo	skončit	k5eAaPmAgNnS	skončit
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
SFRJ	SFRJ	kA	SFRJ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
zbytek	zbytek	k1gInSc1	zbytek
se	se	k3xPyFc4	se
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Svazovou	svazový	k2eAgFnSc4d1	svazová
republiku	republika	k1gFnSc4	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
neodtrhla	odtrhnout	k5eNaPmAgFnS	odtrhnout
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jí	jíst	k5eAaImIp3nS	jíst
věrná	věrný	k2eAgFnSc1d1	věrná
<g/>
,	,	kIx,	,
nezapletla	zaplést	k5eNaPmAgFnS	zaplést
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
spojenectvím	spojenectví	k1gNnPc3	spojenectví
s	s	k7c7	s
Miloševićovým	Miloševićův	k2eAgNnSc7d1	Miloševićův
Srbskem	Srbsko	k1gNnSc7	Srbsko
ale	ale	k8xC	ale
musela	muset	k5eAaImAgFnS	muset
nést	nést	k5eAaImF	nést
jeho	on	k3xPp3gNnSc2	on
břemena	břemeno	k1gNnSc2	břemeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
embargo	embargo	k1gNnSc1	embargo
<g/>
,	,	kIx,	,
bombardování	bombardování	k1gNnSc1	bombardování
silami	síla	k1gFnPc7	síla
NATO	NATO	kA	NATO
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
snížilo	snížit	k5eAaPmAgNnS	snížit
již	již	k9	již
tak	tak	k6eAd1	tak
nízkou	nízký	k2eAgFnSc4d1	nízká
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celých	celý	k2eAgNnPc2d1	celé
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tak	tak	k9	tak
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
se	s	k7c7	s
severním	severní	k2eAgMnSc7d1	severní
sousedem	soused	k1gMnSc7	soused
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
získala	získat	k5eAaPmAgFnS	získat
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgFnSc4d2	vyšší
autonomii	autonomie	k1gFnSc4	autonomie
vytvořením	vytvoření	k1gNnSc7	vytvoření
státního	státní	k2eAgNnSc2d1	státní
společenství	společenství	k1gNnSc2	společenství
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Černá	Černá	k1gFnSc1	Černá
Hora	hora	k1gFnSc1	hora
transformací	transformace	k1gFnSc7	transformace
Svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
plnou	plný	k2eAgFnSc4d1	plná
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
dalším	další	k2eAgNnSc6d1	další
setrvání	setrvání	k1gNnSc6	setrvání
v	v	k7c6	v
unii	unie	k1gFnSc6	unie
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	s	k7c7	s
55,4	[number]	k4	55,4
<g/>
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
samostatnost	samostatnost	k1gFnSc4	samostatnost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
ve	v	k7c6	v
20.34	[number]	k4	20.34
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
parlament	parlament	k1gInSc1	parlament
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
zasedání	zasedání	k1gNnSc6	zasedání
ratifikoval	ratifikovat	k5eAaBmAgMnS	ratifikovat
výsledky	výsledek	k1gInPc4	výsledek
referenda	referendum	k1gNnSc2	referendum
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
Usnesení	usnesení	k1gNnSc4	usnesení
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Republiky	republika	k1gFnSc2	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Deklaraci	deklarace	k1gFnSc4	deklarace
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Republiky	republika	k1gFnSc2	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
formální	formální	k2eAgNnSc4d1	formální
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
stala	stát	k5eAaPmAgFnS	stát
192	[number]	k4	192
<g/>
.	.	kIx.	.
členem	člen	k1gInSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
součástí	součást	k1gFnSc7	součást
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Milo	milo	k6eAd1	milo
Đukanović	Đukanović	k1gFnSc4	Đukanović
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Paříže	Paříž	k1gFnSc2	Paříž
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
formálně	formálně	k6eAd1	formálně
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
Rada	rada	k1gFnSc1	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
oficiálně	oficiálně	k6eAd1	oficiálně
udělila	udělit	k5eAaPmAgFnS	udělit
Černé	Černé	k2eAgNnSc4d1	Černé
Hoře	hoře	k1gNnSc4	hoře
status	status	k1gInSc1	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přístupové	přístupový	k2eAgInPc1d1	přístupový
rozhovory	rozhovor	k1gInPc1	rozhovor
probíhají	probíhat	k5eAaImIp3nP	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
získání	získání	k1gNnSc2	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
se	se	k3xPyFc4	se
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
účastnila	účastnit	k5eAaImAgFnS	účastnit
programu	program	k1gInSc3	program
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
Partnerství	partnerství	k1gNnSc1	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c4	na
sbližování	sbližování	k1gNnSc4	sbližování
se	se	k3xPyFc4	se
s	s	k7c7	s
aliancí	aliance	k1gFnSc7	aliance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
premiér	premiér	k1gMnSc1	premiér
přání	přání	k1gNnSc4	přání
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podpořili	podpořit	k5eAaPmAgMnP	podpořit
i	i	k8xC	i
představitelé	představitel	k1gMnPc1	představitel
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
a	a	k8xC	a
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgFnSc4d1	formální
pozvánku	pozvánka	k1gFnSc4	pozvánka
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
vydala	vydat	k5eAaPmAgFnS	vydat
aliance	aliance	k1gFnSc1	aliance
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatý	hornatý	k2eAgInSc1d1	hornatý
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Dinárským	dinárský	k2eAgFnPc3d1	Dinárská
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
vrcholky	vrcholek	k1gInPc1	vrcholek
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
2500	[number]	k4	2500
m	m	kA	m
-	-	kIx~	-
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolata	k1gFnSc1	Kolata
(	(	kIx(	(
<g/>
2534	[number]	k4	2534
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dobra	dobro	k1gNnSc2	dobro
Kolata	Kolata	k1gFnSc1	Kolata
(	(	kIx(	(
<g/>
2528	[number]	k4	2528
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rosni	Roseň	k1gFnSc3	Roseň
vrh	vrh	k1gInSc1	vrh
(	(	kIx(	(
<g/>
2524	[number]	k4	2524
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bobotov	Bobotov	k1gInSc1	Bobotov
kuk	kuk	k1gInSc1	kuk
(	(	kIx(	(
<g/>
2523	[number]	k4	2523
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
jeskyně	jeskyně	k1gFnSc1	jeskyně
je	být	k5eAaImIp3nS	být
Iron	iron	k1gInSc4	iron
Deep	Deep	k1gInSc4	Deep
1169	[number]	k4	1169
m	m	kA	m
<g/>
,	,	kIx,	,
aktuálně	aktuálně	k6eAd1	aktuálně
zmapovaná	zmapovaný	k2eAgFnSc1d1	zmapovaná
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
m.	m.	k?	m.
Nejhornatější	hornatý	k2eAgInSc1d3	nejhornatější
je	být	k5eAaImIp3nS	být
sever	sever	k1gInSc1	sever
a	a	k8xC	a
západ	západ	k1gInSc1	západ
státu	stát	k1gInSc2	stát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pruhem	pruh	k1gInSc7	pruh
kolem	kolem	k7c2	kolem
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Skadarského	skadarský	k2eAgNnSc2d1	Skadarské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
středozemního	středozemní	k2eAgInSc2d1	středozemní
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Neteče	téct	k5eNaImIp3nS	téct
zde	zde	k6eAd1	zde
žádná	žádný	k3yNgFnSc1	žádný
velká	velký	k2eAgFnSc1d1	velká
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
:	:	kIx,	:
Zeta	Zet	k1gMnSc4	Zet
<g/>
,	,	kIx,	,
Morača	Moračus	k1gMnSc4	Moračus
<g/>
,	,	kIx,	,
Komarnica	Komarnicus	k1gMnSc4	Komarnicus
(	(	kIx(	(
<g/>
Piva	pivo	k1gNnSc2	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lim	limo	k1gNnPc2	limo
<g/>
,	,	kIx,	,
Tara	Tarum	k1gNnSc2	Tarum
<g/>
,	,	kIx,	,
Cetina	Cetino	k1gNnSc2	Cetino
<g/>
,	,	kIx,	,
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
jezery	jezero	k1gNnPc7	jezero
má	mít	k5eAaImIp3nS	mít
Skadarské	skadarský	k2eAgNnSc1d1	Skadarské
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
"	"	kIx"	"
<g/>
ekologickým	ekologický	k2eAgInSc7d1	ekologický
státem	stát	k1gInSc7	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
pět	pět	k4xCc1	pět
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
:	:	kIx,	:
Lovćen	Lovćen	k1gInSc1	Lovćen
(	(	kIx(	(
<g/>
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
prapůvodní	prapůvodní	k2eAgFnSc4d1	prapůvodní
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Biogradska	Biogradska	k1gFnSc1	Biogradska
Gora	Gora	k1gFnSc1	Gora
<g/>
,	,	kIx,	,
Skadarské	skadarský	k2eAgNnSc1d1	Skadarské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Durmitor	Durmitor	k1gInSc1	Durmitor
a	a	k8xC	a
Prokletije	Prokletije	k1gFnSc1	Prokletije
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vláda	vláda	k1gFnSc1	vláda
Černé	Černá	k1gFnSc2	Černá
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
premiérem	premiér	k1gMnSc7	premiér
Republiky	republika	k1gFnSc2	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
Željko	Željka	k1gFnSc5	Željka
Šturanović	Šturanović	k1gMnSc7	Šturanović
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc7d1	vládní
stranou	strana	k1gFnSc7	strana
je	být	k5eAaImIp3nS	být
středolevá	středolevý	k2eAgFnSc1d1	středolevá
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
DPS	DPS	kA	DPS
<g/>
,	,	kIx,	,
Demokratska	Demokratsek	k1gMnSc2	Demokratsek
Partija	Partijus	k1gMnSc2	Partijus
Socijalista	Socijalista	k1gMnSc1	Socijalista
Crna	Crnus	k1gMnSc2	Crnus
Gore	Gore	k1gNnSc2	Gore
<g/>
)	)	kIx)	)
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
s	s	k7c7	s
Sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
SDP	SDP	kA	SDP
<g/>
,	,	kIx,	,
Socijaldemokratska	Socijaldemokratska	k1gFnSc1	Socijaldemokratska
Partija	Partija	k1gFnSc1	Partija
Crne	Crne	k1gFnSc1	Crne
Gore	Gore	k1gFnSc1	Gore
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
2	[number]	k4	2
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
celkem	celkem	k6eAd1	celkem
23	[number]	k4	23
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
státních	státní	k2eAgNnPc2d1	státní
velvyslanectví	velvyslanectví	k1gNnPc2	velvyslanectví
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
23	[number]	k4	23
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
Republiky	republika	k1gFnSc2	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Podgorice	Podgorika	k1gFnSc6	Podgorika
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Republiky	republika	k1gFnSc2	republika
Černá	Černá	k1gFnSc1	Černá
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
v	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Úkoly	úkol	k1gInPc1	úkol
prezidenta	prezident	k1gMnSc2	prezident
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
doma	doma	k6eAd1	doma
a	a	k8xC	a
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
schvalovat	schvalovat	k5eAaImF	schvalovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Skupštiny	skupština	k1gFnSc2	skupština
<g/>
,	,	kIx,	,
jmenovat	jmenovat	k5eAaBmF	jmenovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
parlamentu	parlament	k1gInSc2	parlament
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
soudce	soudce	k1gMnSc4	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
parlamentu	parlament	k1gInSc2	parlament
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
udělovat	udělovat	k5eAaImF	udělovat
milosti	milost	k1gFnPc4	milost
osobám	osoba	k1gFnPc3	osoba
čelícím	čelící	k2eAgFnPc3d1	čelící
trestnímu	trestní	k2eAgNnSc3d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
,	,	kIx,	,
udělovat	udělovat	k5eAaImF	udělovat
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
,	,	kIx,	,
vykonávat	vykonávat	k5eAaImF	vykonávat
povinnosti	povinnost	k1gFnPc4	povinnost
uložené	uložený	k2eAgFnPc4d1	uložená
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
obranné	obranný	k2eAgFnSc2d1	obranná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
Republiky	republika	k1gFnSc2	republika
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
Skupština	skupština	k1gFnSc1	skupština
Republike	Republike	k1gFnSc1	Republike
Crne	Crne	k1gFnSc1	Crne
Gore	Gore	k1gFnSc1	Gore
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
zákony	zákon	k1gInPc4	zákon
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
ratifikuje	ratifikovat	k5eAaBmIp3nS	ratifikovat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ministry	ministr	k1gMnPc4	ministr
a	a	k8xC	a
soudce	soudce	k1gMnPc4	soudce
<g/>
;	;	kIx,	;
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
a	a	k8xC	a
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
další	další	k2eAgFnPc4d1	další
povinnosti	povinnost	k1gFnPc4	povinnost
uložené	uložený	k2eAgFnPc4d1	uložená
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
může	moct	k5eAaImIp3nS	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
vládě	vláda	k1gFnSc3	vláda
většinou	většinou	k6eAd1	většinou
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
74	[number]	k4	74
poslanců	poslanec	k1gMnPc2	poslanec
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
6000	[number]	k4	6000
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
předsedou	předseda	k1gMnSc7	předseda
Skupštiny	skupština	k1gFnSc2	skupština
je	být	k5eAaImIp3nS	být
Ranko	ranka	k1gFnSc5	ranka
Krivokapić	Krivokapić	k1gFnSc7	Krivokapić
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomik	k1gMnSc4	ekonomik
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
chudší	chudý	k2eAgFnPc4d2	chudší
evropské	evropský	k2eAgFnPc4d1	Evropská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgMnSc1d2	bohatší
než	než	k8xS	než
všechny	všechen	k3xTgInPc1	všechen
okolní	okolní	k2eAgInPc1d1	okolní
státy	stát	k1gInPc1	stát
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
14	[number]	k4	14
666	[number]	k4	666
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
MMF	MMF	kA	MMF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
země	země	k1gFnSc1	země
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
MMF	MMF	kA	MMF
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
Stabilizační	stabilizační	k2eAgFnSc4d1	stabilizační
a	a	k8xC	a
asociační	asociační	k2eAgFnSc4d1	asociační
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dlouhodobějším	dlouhodobý	k2eAgInSc6d2	dlouhodobější
horizontu	horizont	k1gInSc6	horizont
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
brzké	brzký	k2eAgFnSc6d1	brzká
době	doba	k1gFnSc6	doba
nereálné	reálný	k2eNgNnSc1d1	nereálné
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
velice	velice	k6eAd1	velice
slabému	slabý	k2eAgInSc3d1	slabý
výkonu	výkon	k1gInSc3	výkon
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc2d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
míře	míra	k1gFnSc3	míra
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
na	na	k7c6	na
nejvyšších	vysoký	k2eAgInPc6d3	Nejvyšší
úřednických	úřednický	k2eAgInPc6d1	úřednický
postech	post	k1gInPc6	post
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
volné	volný	k2eAgFnSc2d1	volná
politické	politický	k2eAgFnSc2d1	politická
unie	unie	k1gFnSc2	unie
mezi	mezi	k7c7	mezi
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
země	zem	k1gFnSc2	zem
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
roční	roční	k2eAgInSc1d1	roční
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
podle	podle	k7c2	podle
CIA	CIA	kA	CIA
-	-	kIx~	-
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
Factbook	Factbook	k1gInSc4	Factbook
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
měla	mít	k5eAaImAgFnS	mít
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
a	a	k8xC	a
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
nepůjde	jít	k5eNaImIp3nS	jít
bez	bez	k7c2	bez
nutných	nutný	k2eAgFnPc2d1	nutná
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
potlačení	potlačení	k1gNnSc1	potlačení
rozbujelé	rozbujelý	k2eAgFnSc2d1	rozbujelá
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
vysokého	vysoký	k2eAgInSc2d1	vysoký
dluhu	dluh	k1gInSc2	dluh
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
potenciál	potenciál	k1gInSc4	potenciál
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
těžbě	těžba	k1gFnSc6	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dominantní	dominantní	k2eAgNnSc4d1	dominantní
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
odvětví	odvětví	k1gNnSc4	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Platidlem	platidlo	k1gNnSc7	platidlo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
oficiální	oficiální	k2eAgNnSc1d1	oficiální
platidlo	platidlo	k1gNnSc1	platidlo
německé	německý	k2eAgFnPc1d1	německá
marka	marka	k1gFnSc1	marka
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
členem	člen	k1gInSc7	člen
Svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
Hoře	hora	k1gFnSc6	hora
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
kvete	kvést	k5eAaImIp3nS	kvést
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
turistická	turistický	k2eAgNnPc1d1	turistické
letoviska	letovisko	k1gNnPc1	letovisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
města	město	k1gNnPc4	město
Budva	Budvo	k1gNnSc2	Budvo
<g/>
,	,	kIx,	,
Kotor	Kotor	k1gInSc1	Kotor
<g/>
,	,	kIx,	,
Ulcinj	Ulcinj	k1gInSc1	Ulcinj
aj.	aj.	kA	aj.
(	(	kIx(	(
Sveti	Svet	k2eAgMnPc1d1	Svet
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
,	,	kIx,	,
Sutomore	Sutomor	k1gMnSc5	Sutomor
,	,	kIx,	,
Bečići	Bečić	k1gFnPc4	Bečić
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgInPc4d1	hlavní
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
:	:	kIx,	:
HDP	HDP	kA	HDP
-	-	kIx~	-
7,03	[number]	k4	7,03
mld.	mld.	k?	mld.
$	$	kIx~	$
<g/>
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Růst	růst	k1gInSc1	růst
<g />
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
-	-	kIx~	-
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
-	-	kIx~	-
11	[number]	k4	11
200	[number]	k4	200
$	$	kIx~	$
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
-	-	kIx~	-
14,1	[number]	k4	14,1
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Veřejný	veřejný	k2eAgInSc1d1	veřejný
dluh	dluh	k1gInSc1	dluh
-	-	kIx~	-
38	[number]	k4	38
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Inflace	inflace	k1gFnSc1	inflace
-	-	kIx~	-
3,1	[number]	k4	3,1
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
vývozní	vývozní	k2eAgFnPc1d1	vývozní
komodity	komodita	k1gFnPc1	komodita
<g/>
:	:	kIx,	:
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
ocel	ocel	k1gFnSc1	ocel
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
dovozní	dovozní	k2eAgFnPc4d1	dovozní
komodity	komodita	k1gFnPc4	komodita
<g/>
:	:	kIx,	:
Ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
pohonné	pohonný	k2eAgFnPc1d1	pohonná
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
spotřební	spotřební	k2eAgNnSc4d1	spotřební
zboží	zboží	k1gNnSc4	zboží
Milo	milo	k6eAd1	milo
Đukanović	Đukanović	k1gMnSc1	Đukanović
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
z	z	k7c2	z
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
i	i	k9	i
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
protivládní	protivládní	k2eAgInPc1d1	protivládní
protesty	protest	k1gInPc1	protest
požadující	požadující	k2eAgInSc1d1	požadující
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
organizovanému	organizovaný	k2eAgInSc3d1	organizovaný
zločinu	zločin	k1gInSc3	zločin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
policie	policie	k1gFnSc1	policie
násilím	násilí	k1gNnSc7	násilí
rozehnala	rozehnat	k5eAaPmAgFnS	rozehnat
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jenom	jenom	k9	jenom
pašováním	pašování	k1gNnSc7	pašování
cigaret	cigareta	k1gFnPc2	cigareta
z	z	k7c2	z
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
mafií	mafie	k1gFnSc7	mafie
vydělal	vydělat	k5eAaPmAgInS	vydělat
Đukanovićův	Đukanovićův	k2eAgInSc1d1	Đukanovićův
režim	režim	k1gInSc1	režim
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Đukanović	Đukanović	k1gFnPc2	Đukanović
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
společníci	společník	k1gMnPc1	společník
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
"	"	kIx"	"
<g/>
vysáli	vysát	k5eAaPmAgMnP	vysát
<g/>
"	"	kIx"	"
peníze	peníz	k1gInPc4	peníz
ze	z	k7c2	z
státem	stát	k1gInSc7	stát
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
průmysl	průmysl	k1gInSc1	průmysl
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
rozprodán	rozprodat	k5eAaPmNgInS	rozprodat
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
735	[number]	k4	735
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
198	[number]	k4	198
firem	firma	k1gFnPc2	firma
privatizovaných	privatizovaný	k2eAgFnPc2d1	privatizovaná
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
jich	on	k3xPp3gMnPc2	on
176	[number]	k4	176
zkrachovalo	zkrachovat	k5eAaPmAgNnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
důsledku	důsledek	k1gInSc6	důsledek
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c6	o
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
práceschopného	práceschopný	k2eAgNnSc2d1	práceschopné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
stát	stát	k1gInSc4	stát
relativně	relativně	k6eAd1	relativně
multikulturní	multikulturní	k2eAgMnSc1d1	multikulturní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
celkem	celkem	k6eAd1	celkem
620	[number]	k4	620
029	[number]	k4	029
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
Černohorci	Černohorec	k1gMnPc1	Černohorec
tvořili	tvořit	k5eAaImAgMnP	tvořit
pouze	pouze	k6eAd1	pouze
45	[number]	k4	45
%	%	kIx~	%
<g/>
;	;	kIx,	;
další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
tvořili	tvořit	k5eAaImAgMnP	tvořit
Srbové	Srb	k1gMnPc1	Srb
(	(	kIx(	(
<g/>
28,7	[number]	k4	28,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bosňáci	Bosňáci	k?	Bosňáci
(	(	kIx(	(
<g/>
8,6	[number]	k4	8,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Albánci	Albánec	k1gMnPc1	Albánec
(	(	kIx(	(
<g/>
4,9	[number]	k4	4,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
3,3	[number]	k4	3,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgFnPc4d1	jiná
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
druhý	druhý	k4xOgMnSc1	druhý
Černohorec	Černohorec	k1gMnSc1	Černohorec
žije	žít	k5eAaImIp3nS	žít
mimo	mimo	k7c4	mimo
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
72	[number]	k4	72
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
Srbské	srbský	k2eAgFnSc3d1	Srbská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
19,11	[number]	k4	19,11
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
necelá	celý	k2eNgFnSc1d1	necelá
3,44	[number]	k4	3,44
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
ateistů	ateista	k1gMnPc2	ateista
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
výše	výše	k1gFnSc1	výše
řečených	řečený	k2eAgNnPc2d1	řečené
vyznání	vyznání	k1gNnPc2	vyznání
tvoří	tvořit	k5eAaImIp3nP	tvořit
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
slouží	sloužit	k5eAaImIp3nS	sloužit
srbochorvatština	srbochorvatština	k1gFnSc1	srbochorvatština
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
tradičně	tradičně	k6eAd1	tradičně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
srbštinu	srbština	k1gFnSc4	srbština
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gFnSc1	její
ijekavská	ijekavský	k2eAgFnSc1d1	ijekavský
varianta	varianta	k1gFnSc1	varianta
(	(	kIx(	(
<g/>
užívaná	užívaný	k2eAgFnSc1d1	užívaná
ještě	ještě	k9	ještě
také	také	k9	také
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psaná	psaný	k2eAgFnSc1d1	psaná
rovnocenně	rovnocenně	k6eAd1	rovnocenně
latinkou	latinka	k1gFnSc7	latinka
i	i	k8xC	i
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
od	od	k7c2	od
Srbska	Srbsko	k1gNnSc2	Srbsko
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
ustavuje	ustavovat	k5eAaImIp3nS	ustavovat
jako	jako	k9	jako
černohorština	černohorština	k1gFnSc1	černohorština
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
černohorské	černohorský	k2eAgFnSc2d1	černohorská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Mirko	Mirko	k1gMnSc1	Mirko
Petrović-Njegoš	Petrović-Njegoš	k1gMnSc1	Petrović-Njegoš
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
epická	epický	k2eAgFnSc1d1	epická
skladba	skladba	k1gFnSc1	skladba
Junački	Junačk	k1gFnSc2	Junačk
spomenik	spomenik	k1gInSc1	spomenik
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
v	v	k7c4	v
Cetinje	Cetinj	k1gInPc4	Cetinj
<g/>
,	,	kIx,	,
historickém	historický	k2eAgNnSc6d1	historické
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
a	a	k8xC	a
sídle	sídlo	k1gNnSc6	sídlo
černohorských	černohorský	k2eAgMnPc2d1	černohorský
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
a	a	k8xC	a
Černohorce	Černohorec	k1gMnSc4	Černohorec
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
velkém	velký	k2eAgNnSc6d1	velké
vítězství	vítězství	k1gNnSc6	vítězství
černohorského	černohorský	k2eAgInSc2d1	černohorský
národa	národ	k1gInSc2	národ
nad	nad	k7c7	nad
Osmany	Osman	k1gMnPc7	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
básník	básník	k1gMnSc1	básník
Radovan	Radovan	k1gMnSc1	Radovan
Zogović	Zogović	k1gMnSc1	Zogović
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
socialistické	socialistický	k2eAgFnSc2d1	socialistická
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Mirko	Mirko	k1gMnSc1	Mirko
Kovač	Kovač	k1gMnSc1	Kovač
<g/>
,	,	kIx,	,
Miodrag	Miodrag	k1gMnSc1	Miodrag
Bulatović	Bulatović	k1gMnSc1	Bulatović
nebo	nebo	k8xC	nebo
Ćamil	Ćamil	k1gMnSc1	Ćamil
Sijarić	Sijarić	k1gMnSc1	Sijarić
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
k	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
černohorským	černohorský	k2eAgMnPc3d1	černohorský
autorům	autor	k1gMnPc3	autor
patří	patřit	k5eAaImIp3nP	patřit
Borislav	Borislav	k1gMnSc1	Borislav
Jovanović	Jovanović	k1gMnSc1	Jovanović
<g/>
.	.	kIx.	.
</s>
<s>
Černohorcem	Černohorec	k1gMnSc7	Černohorec
byl	být	k5eAaImAgMnS	být
i	i	k9	i
významný	významný	k2eAgMnSc1d1	významný
marxistický	marxistický	k2eAgMnSc1d1	marxistický
teoretik	teoretik	k1gMnSc1	teoretik
a	a	k8xC	a
"	"	kIx"	"
<g/>
prominentní	prominentní	k2eAgMnSc1d1	prominentní
disident	disident	k1gMnSc1	disident
<g/>
"	"	kIx"	"
Titova	Titův	k2eAgInSc2d1	Titův
režimu	režim	k1gInSc2	režim
Milovan	Milovan	k1gMnSc1	Milovan
Djilas	Djilas	k1gMnSc1	Djilas
<g/>
.	.	kIx.	.
</s>
<s>
Vasilije	Vasilít	k5eAaPmIp3nS	Vasilít
Petrović	Petrović	k1gMnPc3	Petrović
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
černohorské	černohorský	k2eAgFnSc2d1	černohorská
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
,	,	kIx,	,
když	když	k8xS	když
napsal	napsat	k5eAaPmAgMnS	napsat
roku	rok	k1gInSc2	rok
1754	[number]	k4	1754
Dějiny	dějiny	k1gFnPc4	dějiny
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
umělcům	umělec	k1gMnPc3	umělec
narozeným	narozený	k2eAgMnPc3d1	narozený
v	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
Hoře	hora	k1gFnSc6	hora
patří	patřit	k5eAaImIp3nS	patřit
performer	performer	k1gMnSc1	performer
a	a	k8xC	a
provokatér	provokatér	k1gMnSc1	provokatér
Rambo	Ramba	k1gFnSc5	Ramba
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
známý	známý	k2eAgMnSc1d1	známý
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
pýchou	pýcha	k1gFnSc7	pýcha
Černohorců	Černohorec	k1gMnPc2	Černohorec
jejich	jejich	k3xOp3gFnSc7	jejich
ženská	ženský	k2eAgFnSc1d1	ženská
reprezentace	reprezentace	k1gFnSc1	reprezentace
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oporám	opora	k1gFnPc3	opora
slavného	slavný	k2eAgInSc2d1	slavný
týmu	tým	k1gInSc2	tým
patřili	patřit	k5eAaImAgMnP	patřit
Jovanka	Jovanka	k1gFnSc1	Jovanka
Radičevićová	Radičevićová	k1gFnSc1	Radičevićová
či	či	k8xC	či
Milena	Milena	k1gFnSc1	Milena
Kneževićová	Kneževićová	k1gFnSc1	Kneževićová
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
přivezl	přivézt	k5eAaPmAgInS	přivézt
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
černohorský	černohorský	k2eAgMnSc1d1	černohorský
házenkář	házenkář	k1gMnSc1	házenkář
Veselin	Veselina	k1gFnPc2	Veselina
Vujović	Vujović	k1gMnSc1	Vujović
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
Hoře	hora	k1gFnSc6	hora
také	také	k9	také
vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
<g/>
,	,	kIx,	,
mužská	mužský	k2eAgFnSc1d1	mužská
reprezentace	reprezentace	k1gFnSc1	reprezentace
získala	získat	k5eAaPmAgFnS	získat
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
vodním	vodní	k2eAgMnPc3d1	vodní
pólistům	pólista	k1gMnPc3	pólista
patří	patřit	k5eAaImIp3nP	patřit
Aleksandar	Aleksandar	k1gMnSc1	Aleksandar
Ivović	Ivović	k1gMnSc1	Ivović
<g/>
.	.	kIx.	.
</s>
<s>
Srđan	Srđan	k1gMnSc1	Srđan
Mrvaljević	Mrvaljević	k1gMnSc1	Mrvaljević
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
stříbro	stříbro	k1gNnSc4	stříbro
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
<g/>
.	.	kIx.	.
</s>
