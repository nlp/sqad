<s>
Slavonice	Slavonice	k1gFnSc1	Slavonice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Zlabings	Zlabings	k1gInSc1	Zlabings
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
přezdívané	přezdívaný	k2eAgNnSc1d1	přezdívané
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
Telč	Telč	k1gFnSc1	Telč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
v	v	k7c6	v
moravské	moravský	k2eAgFnSc6d1	Moravská
části	část	k1gFnSc6	část
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
11	[number]	k4	11
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Dačic	Dačice	k1gFnPc2	Dačice
<g/>
,	,	kIx,	,
na	na	k7c6	na
česko-moravsko-dolnorakouském	českooravskoolnorakouský	k2eAgNnSc6d1	česko-moravsko-dolnorakouský
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
Slavonický	Slavonický	k2eAgInSc1d1	Slavonický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nedaleko	nedaleko	k7c2	nedaleko
Slavonic	Slavonice	k1gFnPc2	Slavonice
<g/>
,	,	kIx,	,
u	u	k7c2	u
Stálkova	Stálkův	k2eAgNnSc2d1	Stálkův
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Slavonicích	Slavonik	k1gInPc6	Slavonik
2	[number]	k4	2
484	[number]	k4	484
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
železniční	železniční	k2eAgFnSc7d1	železniční
tratě	trať	k1gFnPc4	trať
Kostelec	Kostelec	k1gInSc4	Kostelec
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
-	-	kIx~	-
Slavonice	Slavonice	k1gFnSc2	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
strážní	strážní	k2eAgFnSc1d1	strážní
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
asi	asi	k9	asi
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
zemské	zemský	k2eAgFnSc6d1	zemská
stezce	stezka	k1gFnSc6	stezka
spojující	spojující	k2eAgFnSc4d1	spojující
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
pětilistá	pětilistý	k2eAgFnSc1d1	pětilistá
růže	růže	k1gFnSc1	růže
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
osada	osada	k1gFnSc1	osada
díky	díky	k7c3	díky
velkolepé	velkolepý	k2eAgFnSc3d1	velkolepá
výstavbě	výstavba	k1gFnSc3	výstavba
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ve	v	k7c4	v
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
náměstí	náměstí	k1gNnPc4	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jako	jako	k8xS	jako
náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
a	a	k8xC	a
Horní	horní	k2eAgNnSc4d1	horní
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
středověkými	středověký	k2eAgInPc7d1	středověký
měšťanskými	měšťanský	k2eAgInPc7d1	měšťanský
domy	dům	k1gInPc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
na	na	k7c6	na
velice	velice	k6eAd1	velice
úzkých	úzký	k2eAgInPc6d1	úzký
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
majitelé	majitel	k1gMnPc1	majitel
své	svůj	k3xOyFgNnSc4	svůj
domy	dům	k1gInPc1	dům
a	a	k8xC	a
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
hospodářství	hospodářství	k1gNnSc4	hospodářství
směřovali	směřovat	k5eAaImAgMnP	směřovat
hlavním	hlavní	k2eAgInSc7d1	hlavní
vchodem	vchod	k1gInSc7	vchod
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
budovy	budova	k1gFnPc4	budova
na	na	k7c4	na
zadní	zadní	k2eAgInPc4d1	zadní
trakty	trakt	k1gInPc4	trakt
svých	svůj	k3xOyFgInPc2	svůj
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
města	město	k1gNnSc2	město
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
k	k	k7c3	k
luteránské	luteránský	k2eAgFnSc3d1	luteránská
reformaci	reformace	k1gFnSc3	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Reformační	reformační	k2eAgFnSc1d1	reformační
sgrafitová	sgrafitový	k2eAgFnSc1d1	sgrafitová
výzdoba	výzdoba	k1gFnSc1	výzdoba
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
luteránského	luteránský	k2eAgNnSc2d1	luteránské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
významně	významně	k6eAd1	významně
spoluutváří	spoluutvářet	k5eAaImIp3nS	spoluutvářet
unikátní	unikátní	k2eAgFnSc4d1	unikátní
atmosféru	atmosféra	k1gFnSc4	atmosféra
města	město	k1gNnSc2	město
Slavonice	Slavonice	k1gFnSc2	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
vypleněno	vypleněn	k2eAgNnSc1d1	vypleněno
švédskými	švédský	k2eAgInPc7d1	švédský
vojsky	vojsky	k6eAd1	vojsky
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1680-1681	[number]	k4	1680-1681
postihl	postihnout	k5eAaPmAgInS	postihnout
slavonické	slavonický	k2eAgMnPc4d1	slavonický
obyvatele	obyvatel	k1gMnPc4	obyvatel
mor	mora	k1gFnPc2	mora
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pokosil	pokosit	k5eAaPmAgInS	pokosit
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
velkých	velký	k2eAgFnPc2d1	velká
útrap	útrapa	k1gFnPc2	útrapa
a	a	k8xC	a
válek	válka	k1gFnPc2	válka
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS	vzpamatovat
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nemělo	mít	k5eNaImAgNnS	mít
však	však	k9	však
být	být	k5eAaImF	být
všem	všecek	k3xTgNnPc3	všecek
neštěstím	neštěstí	k1gNnPc3	neštěstí
a	a	k8xC	a
katastrofám	katastrofa	k1gFnPc3	katastrofa
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1750	[number]	k4	1750
vzplál	vzplát	k5eAaPmAgInS	vzplát
ve	v	k7c6	v
městě	město	k1gNnSc6	město
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zničil	zničit	k5eAaPmAgInS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
obou	dva	k4xCgFnPc2	dva
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
44	[number]	k4	44
domů	dům	k1gInPc2	dům
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
následoval	následovat	k5eAaImAgInS	následovat
ještě	ještě	k6eAd1	ještě
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
přeložení	přeložení	k1gNnSc1	přeložení
poštovní	poštovní	k2eAgFnSc2d1	poštovní
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
přes	přes	k7c4	přes
Znojmo	Znojmo	k1gNnSc4	Znojmo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
č.	č.	k?	č.
p.	p.	k?	p.
<g/>
62	[number]	k4	62
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
přepřahací	přepřahací	k2eAgFnSc7d1	přepřahací
stanicí	stanice	k1gFnSc7	stanice
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1902	[number]	k4	1902
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
a	a	k8xC	a
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Slavonicemi	Slavonice	k1gFnPc7	Slavonice
a	a	k8xC	a
Telčí	Telč	k1gFnSc7	Telč
a	a	k8xC	a
také	také	k9	také
budova	budova	k1gFnSc1	budova
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
ve	v	k7c6	v
Slavonicích	Slavonice	k1gFnPc6	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
Slavonic	Slavonice	k1gFnPc2	Slavonice
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
Waidhofen	Waidhofna	k1gFnPc2	Waidhofna
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Thaya	Thay	k2eAgNnPc1d1	Thay
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
se	s	k7c7	s
stanicí	stanice	k1gFnSc7	stanice
Schwarzenau	Schwarzenaus	k1gInSc2	Schwarzenaus
na	na	k7c6	na
císařské	císařský	k2eAgFnSc6d1	císařská
železnici	železnice	k1gFnSc6	železnice
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
město	město	k1gNnSc1	město
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
připojením	připojení	k1gNnSc7	připojení
k	k	k7c3	k
provincii	provincie	k1gFnSc3	provincie
Deutsch	Deutscha	k1gFnPc2	Deutscha
Südmähren	Südmährno	k1gNnPc2	Südmährno
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
do	do	k7c2	do
města	město	k1gNnSc2	město
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
povolána	povolán	k2eAgFnSc1d1	povolána
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
aby	aby	kYmCp3nS	aby
podrobila	podrobit	k5eAaPmAgFnS	podrobit
město	město	k1gNnSc4	město
moci	moc	k1gFnSc2	moc
nového	nový	k2eAgInSc2d1	nový
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozpad	rozpad	k1gInSc1	rozpad
monarchie	monarchie	k1gFnSc2	monarchie
ale	ale	k8xC	ale
znamenal	znamenat	k5eAaImAgInS	znamenat
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
textilní	textilní	k2eAgFnPc4d1	textilní
továrny	továrna	k1gFnPc4	továrna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
tradiční	tradiční	k2eAgNnSc4d1	tradiční
odbytiště	odbytiště	k1gNnSc4	odbytiště
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
monarchii	monarchie	k1gFnSc6	monarchie
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
připojením	připojení	k1gNnSc7	připojení
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
ZME	ZME	kA	ZME
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
až	až	k6eAd1	až
1938	[number]	k4	1938
vybudována	vybudován	k2eAgFnSc1d1	vybudována
soustava	soustava	k1gFnSc1	soustava
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zásluhou	zásluha	k1gFnSc7	zásluha
nadšenců	nadšenec	k1gMnPc2	nadšenec
udržovány	udržovat	k5eAaImNgInP	udržovat
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
přístupny	přístupen	k2eAgFnPc4d1	přístupna
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
pevností	pevnost	k1gFnPc2	pevnost
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
intravilánu	intravilán	k1gInSc6	intravilán
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1938	[number]	k4	1938
postoupeno	postoupit	k5eAaPmNgNnS	postoupit
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Slavonice	Slavonice	k1gFnSc1	Slavonice
se	se	k3xPyFc4	se
však	však	k9	však
nestaly	stát	k5eNaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
Župy	župa	k1gFnPc4	župa
Sudety	Sudety	k1gFnPc4	Sudety
na	na	k7c6	na
západních	západní	k2eAgFnPc6d1	západní
hranicích	hranice	k1gFnPc6	hranice
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
přičleněny	přičlenit	k5eAaPmNgFnP	přičlenit
do	do	k7c2	do
říšské	říšský	k2eAgFnSc2d1	říšská
župy	župa	k1gFnSc2	župa
Dolní	dolní	k2eAgNnSc1d1	dolní
Podunají	Podunají	k1gNnSc1	Podunají
(	(	kIx(	(
<g/>
Niederdonau	Niederdonaus	k1gInSc2	Niederdonaus
<g/>
)	)	kIx)	)
sousedního	sousední	k2eAgNnSc2d1	sousední
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
byly	být	k5eAaImAgFnP	být
posunuty	posunout	k5eAaPmNgFnP	posunout
o	o	k7c4	o
10	[number]	k4	10
km	km	kA	km
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Slavonic	Slavonice	k1gFnPc2	Slavonice
<g/>
,	,	kIx,	,
seke	seke	k6eAd1	seke
Slavonicím	Slavonice	k1gFnPc3	Slavonice
všemi	všecek	k3xTgInPc7	všecek
důsledky	důsledek	k1gInPc7	důsledek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
německého	německý	k2eAgInSc2d1	německý
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přinesla	přinést	k5eAaPmAgFnS	přinést
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odsunuto	odsunout	k5eAaPmNgNnS	odsunout
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
vojsky	vojsky	k6eAd1	vojsky
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vítězných	vítězný	k2eAgFnPc2d1	vítězná
velmocí	velmoc	k1gFnPc2	velmoc
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vlnách	vlna	k1gFnPc6	vlna
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
a	a	k8xC	a
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
odsunuto	odsunut	k2eAgNnSc1d1	odsunuto
veškeré	veškerý	k3xTgNnSc4	veškerý
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
a	a	k8xC	a
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapřičinilo	zapřičinit	k5eAaImAgNnS	zapřičinit
značnou	značný	k2eAgFnSc4d1	značná
izolaci	izolace	k1gFnSc4	izolace
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
od	od	k7c2	od
ostatní	ostatní	k2eAgFnSc2d1	ostatní
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
omezilo	omezit	k5eAaPmAgNnS	omezit
i	i	k9	i
případný	případný	k2eAgInSc4d1	případný
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
při	při	k7c6	při
reorganizaci	reorganizace	k1gFnSc6	reorganizace
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
krajů	kraj	k1gInPc2	kraj
bylo	být	k5eAaImAgNnS	být
zastavěné	zastavěný	k2eAgNnSc4d1	zastavěné
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
vyjmuto	vyjmout	k5eAaPmNgNnS	vyjmout
z	z	k7c2	z
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
začít	začít	k5eAaPmF	začít
využívat	využívat	k5eAaPmF	využívat
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
byly	být	k5eAaImAgFnP	být
Slavonice	Slavonice	k1gFnPc1	Slavonice
prohlášeny	prohlásit	k5eAaPmNgFnP	prohlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
správní	správní	k2eAgFnSc2d1	správní
reformy	reforma	k1gFnSc2	reforma
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
náležejí	náležet	k5eAaImIp3nP	náležet
Slavonice	Slavonice	k1gFnPc1	Slavonice
k	k	k7c3	k
Jihočeskému	jihočeský	k2eAgInSc3d1	jihočeský
kraji	kraj	k1gInSc3	kraj
<g/>
,	,	kIx,	,
okresu	okres	k1gInSc3	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
integrace	integrace	k1gFnSc2	integrace
obcí	obec	k1gFnPc2	obec
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
ke	k	k7c3	k
Slavoních	Slavoň	k1gMnPc6	Slavoň
připojeny	připojit	k5eAaPmNgFnP	připojit
dříve	dříve	k6eAd2	dříve
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Vlastkovec	Vlastkovec	k1gInSc1	Vlastkovec
<g/>
,	,	kIx,	,
Mutišov	Mutišov	k1gInSc1	Mutišov
a	a	k8xC	a
Maříž	Maříž	k1gFnSc1	Maříž
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
také	také	k9	také
poslední	poslední	k2eAgFnSc1d1	poslední
připojená	připojený	k2eAgFnSc1d1	připojená
obec	obec	k1gFnSc1	obec
Stálkov	Stálkov	k1gInSc1	Stálkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
-	-	kIx~	-
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vybudován	vybudován	k2eAgInSc1d1	vybudován
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
,	,	kIx,	,
čistička	čistička	k1gFnSc1	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc1d1	centrální
kotelna	kotelna	k1gFnSc1	kotelna
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
nové	nový	k2eAgFnSc2d1	nová
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
650	[number]	k4	650
žáků	žák	k1gMnPc2	žák
se	s	k7c7	s
školní	školní	k2eAgFnSc7d1	školní
jídelnou	jídelna	k1gFnSc7	jídelna
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
tělocvičnami	tělocvična	k1gFnPc7	tělocvična
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
120	[number]	k4	120
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
hasičská	hasičský	k2eAgFnSc1d1	hasičská
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
<g/>
,	,	kIx,	,
autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
kuželna	kuželna	k1gFnSc1	kuželna
<g/>
,	,	kIx,	,
postaveno	postaven	k2eAgNnSc4d1	postaveno
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
se	s	k7c7	s
126	[number]	k4	126
městskými	městský	k2eAgInPc7d1	městský
a	a	k8xC	a
družstevními	družstevní	k2eAgInPc7d1	družstevní
byty	byt	k1gInPc7	byt
a	a	k8xC	a
připravena	připraven	k2eAgFnSc1d1	připravena
nová	nový	k2eAgFnSc1d1	nová
obytná	obytný	k2eAgFnSc1d1	obytná
zóna	zóna	k1gFnSc1	zóna
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
nových	nový	k2eAgInPc2d1	nový
individuálních	individuální	k2eAgInPc2d1	individuální
a	a	k8xC	a
řadových	řadový	k2eAgInPc2d1	řadový
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
generála	generál	k1gMnSc2	generál
Svobody	Svoboda	k1gMnSc2	Svoboda
a	a	k8xC	a
Lebeděvova	Lebeděvův	k2eAgNnSc2d1	Lebeděvův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
místní	místní	k2eAgMnPc1d1	místní
občané	občan	k1gMnPc1	občan
postavili	postavit	k5eAaPmAgMnP	postavit
77	[number]	k4	77
nových	nový	k2eAgInPc2d1	nový
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
místní	místní	k2eAgInPc1d1	místní
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
postavily	postavit	k5eAaPmAgFnP	postavit
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
kapacitou	kapacita	k1gFnSc7	kapacita
přes	přes	k7c4	přes
50	[number]	k4	50
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
také	také	k9	také
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
a	a	k8xC	a
řádné	řádný	k2eAgFnPc1d1	řádná
opravy	oprava	k1gFnPc1	oprava
historického	historický	k2eAgInSc2d1	historický
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
fasád	fasáda	k1gFnPc2	fasáda
domů	domů	k6eAd1	domů
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
slavonických	slavonický	k2eAgNnPc6d1	slavonické
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
valné	valný	k2eAgFnSc6d1	valná
většině	většina	k1gFnSc6	většina
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výstavba	výstavba	k1gFnSc1	výstavba
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
střediska	středisko	k1gNnSc2	středisko
a	a	k8xC	a
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgInPc1	čtyři
městské	městský	k2eAgInPc1d1	městský
panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
s	s	k7c7	s
56	[number]	k4	56
bytovými	bytový	k2eAgFnPc7d1	bytová
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
bytová	bytový	k2eAgFnSc1d1	bytová
výstavba	výstavba	k1gFnSc1	výstavba
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
nerozvíjela	rozvíjet	k5eNaImAgFnS	rozvíjet
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
stavebních	stavební	k2eAgFnPc2d1	stavební
parcel	parcela	k1gFnPc2	parcela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
nových	nový	k2eAgFnPc2d1	nová
staveb	stavba	k1gFnPc2	stavba
občanské	občanský	k2eAgFnSc2d1	občanská
vybavenosti	vybavenost	k1gFnSc2	vybavenost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadionu	stadion	k1gInSc6	stadion
a	a	k8xC	a
dům	dům	k1gInSc1	dům
s	s	k7c7	s
pečovatelskou	pečovatelský	k2eAgFnSc7d1	pečovatelská
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
budova	budova	k1gFnSc1	budova
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
střediska	středisko	k1gNnSc2	středisko
a	a	k8xC	a
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
víceúčelové	víceúčelový	k2eAgNnSc1d1	víceúčelové
hřiště	hřiště	k1gNnSc1	hřiště
s	s	k7c7	s
umělým	umělý	k2eAgInSc7d1	umělý
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
závod	závod	k1gInSc1	závod
na	na	k7c6	na
zpracování	zpracování	k1gNnSc6	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
postavil	postavit	k5eAaPmAgInS	postavit
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
spalovnu	spalovna	k1gFnSc4	spalovna
dřevního	dřevní	k2eAgInSc2d1	dřevní
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
vyvážel	vyvážet	k5eAaImAgInS	vyvážet
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
užitku	užitek	k1gInSc2	užitek
na	na	k7c4	na
skládku	skládka	k1gFnSc4	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Spalovna	spalovna	k1gFnSc1	spalovna
nyní	nyní	k6eAd1	nyní
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
stát	stát	k1gInSc1	stát
i	i	k8xC	i
obec	obec	k1gFnSc1	obec
investují	investovat	k5eAaBmIp3nP	investovat
značné	značný	k2eAgInPc1d1	značný
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
a	a	k8xC	a
regeneraci	regenerace	k1gFnSc4	regenerace
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
přiveden	přiveden	k2eAgInSc1d1	přiveden
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
provedeny	proveden	k2eAgFnPc1d1	provedena
rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
povrchů	povrch	k1gInPc2	povrch
obou	dva	k4xCgNnPc2	dva
slavonických	slavonický	k2eAgNnPc2d1	slavonické
náměstí	náměstí	k1gNnPc2	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
opravit	opravit	k5eAaPmF	opravit
část	část	k1gFnSc4	část
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
opravováno	opravován	k2eAgNnSc1d1	opravováno
a	a	k8xC	a
zpřístupňováno	zpřístupňován	k2eAgNnSc1d1	zpřístupňováno
veřejnosti	veřejnost	k1gFnSc3	veřejnost
městské	městský	k2eAgNnSc1d1	Městské
podzemí	podzemí	k1gNnSc1	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
čističky	čistička	k1gFnSc2	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
Slavonice	Slavonice	k1gFnPc1	Slavonice
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tu	tu	k6eAd1	tu
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
většina	většina	k1gFnSc1	většina
občanů	občan	k1gMnPc2	občan
národnosti	národnost	k1gFnSc2	národnost
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
prvním	první	k4xOgMnSc7	první
ředitelem	ředitel	k1gMnSc7	ředitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
František	František	k1gMnSc1	František
Janda	Janda	k1gMnSc1	Janda
<g/>
.	.	kIx.	.
</s>
<s>
Chodily	chodit	k5eAaImAgFnP	chodit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zejména	zejména	k9	zejména
děti	dítě	k1gFnPc1	dítě
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Slavonic	Slavonice	k1gFnPc2	Slavonice
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
novorenesanční	novorenesanční	k2eAgFnSc6d1	novorenesanční
budově	budova	k1gFnSc6	budova
původní	původní	k2eAgFnSc2d1	původní
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
základní	základní	k2eAgFnSc1d1	základní
praktická	praktický	k2eAgFnSc1d1	praktická
a	a	k8xC	a
speciální	speciální	k2eAgFnSc1d1	speciální
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
přijeli	přijet	k5eAaPmAgMnP	přijet
do	do	k7c2	do
Slavonic	Slavonice	k1gFnPc2	Slavonice
němečtí	německý	k2eAgMnPc1d1	německý
okupanti	okupant	k1gMnPc1	okupant
a	a	k8xC	a
českou	český	k2eAgFnSc4d1	Česká
školu	škola	k1gFnSc4	škola
zrušili	zrušit	k5eAaPmAgMnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
válku	válka	k1gFnSc4	válka
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
škola	škola	k1gFnSc1	škola
německá	německý	k2eAgFnSc1d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nastal	nastat	k5eAaPmAgInS	nastat
odsun	odsun	k1gInSc4	odsun
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
osidlování	osidlování	k1gNnSc1	osidlování
českými	český	k2eAgMnPc7d1	český
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Slavonicích	Slavonik	k1gInPc6	Slavonik
zcela	zcela	k6eAd1	zcela
zrušena	zrušit	k5eAaPmNgNnP	zrušit
zřízeny	zřízen	k2eAgFnPc1d1	zřízena
byly	být	k5eAaImAgFnP	být
nově	nově	k6eAd1	nově
dvě	dva	k4xCgFnPc4	dva
české	český	k2eAgFnPc4d1	Česká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
obecná	obecná	k1gFnSc1	obecná
se	s	k7c7	s
149	[number]	k4	149
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
l.	l.	k?	l.
až	až	k9	až
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
postupném	postupný	k2eAgInSc6d1	postupný
ročníku	ročník	k1gInSc6	ročník
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgNnPc1d1	umístěné
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
čp.	čp.	k?	čp.
39	[number]	k4	39
na	na	k7c6	na
nám.	nám.	k?	nám.
Míru	mír	k1gInSc6	mír
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
čp.	čp.	k?	čp.
473	[number]	k4	473
<g/>
)	)	kIx)	)
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
<g/>
,	,	kIx,	,
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
jako	jako	k8xC	jako
čtyřtřídní	čtyřtřídní	k2eAgFnSc1d1	čtyřtřídní
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1945	[number]	k4	1945
začalo	začít	k5eAaPmAgNnS	začít
do	do	k7c2	do
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
chodit	chodit	k5eAaImF	chodit
179	[number]	k4	179
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
žactva	žactvo	k1gNnSc2	žactvo
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
147	[number]	k4	147
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
třídy	třída	k1gFnPc1	třída
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
poschodí	poschodí	k1gNnSc2	poschodí
budovy	budova	k1gFnSc2	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
čp.	čp.	k?	čp.
21	[number]	k4	21
-	-	kIx~	-
22	[number]	k4	22
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
čp.	čp.	k?	čp.
456	[number]	k4	456
<g/>
)	)	kIx)	)
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
školy	škola	k1gFnPc1	škola
sloučily	sloučit	k5eAaPmAgFnP	sloučit
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
Osmiletou	osmiletý	k2eAgFnSc4d1	osmiletá
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
se	se	k3xPyFc4	se
dovídáme	dovídat	k5eAaImIp1nP	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
zdejší	zdejší	k2eAgFnSc6d1	zdejší
škole	škola	k1gFnSc6	škola
každoročně	každoročně	k6eAd1	každoročně
zahajoval	zahajovat	k5eAaImAgInS	zahajovat
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
zahajovací	zahajovací	k2eAgFnSc7d1	zahajovací
slavností	slavnost	k1gFnSc7	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
oslavy	oslava	k1gFnPc1	oslava
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
důležitých	důležitý	k2eAgFnPc2d1	důležitá
osobností	osobnost	k1gFnPc2	osobnost
např.	např.	kA	např.
<g/>
:	:	kIx,	:
V.	V.	kA	V.
I.	I.	kA	I.
Lenina	Lenin	k1gMnSc2	Lenin
nebo	nebo	k8xC	nebo
K.	K.	kA	K.
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
také	také	k9	také
připravovaly	připravovat	k5eAaImAgFnP	připravovat
besídky	besídka	k1gFnPc1	besídka
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
MDŽ	MDŽ	kA	MDŽ
nebo	nebo	k8xC	nebo
VŘSR	VŘSR	kA	VŘSR
nebo	nebo	k8xC	nebo
chodily	chodit	k5eAaImAgFnP	chodit
na	na	k7c4	na
bramborové	bramborový	k2eAgFnPc4d1	bramborová
brigády	brigáda	k1gFnPc4	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
ze	z	k7c2	z
života	život	k1gInSc2	život
školy	škola	k1gFnSc2	škola
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
archeologickém	archeologický	k2eAgInSc6d1	archeologický
průzkumu	průzkum	k1gInSc6	průzkum
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
středověké	středověký	k2eAgFnSc2d1	středověká
vesnice	vesnice	k1gFnSc2	vesnice
Pfaffenschlag	Pfaffenschlaga	k1gFnPc2	Pfaffenschlaga
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
průzkumu	průzkum	k1gInSc6	průzkum
se	se	k3xPyFc4	se
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
podílely	podílet	k5eAaImAgFnP	podílet
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
pana	pan	k1gMnSc2	pan
učitele	učitel	k1gMnSc2	učitel
Josefa	Josef	k1gMnSc2	Josef
Střechy	Střecha	k1gMnSc2	Střecha
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
stoupal	stoupat	k5eAaImAgInS	stoupat
počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
slavonické	slavonický	k2eAgFnSc6d1	slavonická
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
počet	počet	k1gInSc1	počet
využívaných	využívaný	k2eAgFnPc2d1	využívaná
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
využívalo	využívat	k5eAaImAgNnS	využívat
630	[number]	k4	630
žáků	žák	k1gMnPc2	žák
osm	osm	k4xCc4	osm
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
ve	v	k7c6	v
čtyřech	čtyři	k4xCgMnPc6	čtyři
byly	být	k5eAaImAgFnP	být
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
v	v	k7c6	v
ostatních	ostatní	k1gNnPc6	ostatní
byla	být	k5eAaImAgFnS	být
jídelna	jídelna	k1gFnSc1	jídelna
<g/>
,	,	kIx,	,
sokolovna	sokolovna	k1gFnSc1	sokolovna
atd.	atd.	kA	atd.
Tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
školy	škola	k1gFnSc2	škola
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
škola	škola	k1gFnSc1	škola
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
velké	velký	k2eAgFnSc2d1	velká
budovy	budova	k1gFnSc2	budova
mezi	mezi	k7c7	mezi
dnešními	dnešní	k2eAgFnPc7d1	dnešní
ulicemi	ulice	k1gFnPc7	ulice
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
a	a	k8xC	a
Rudolecká	Rudolecký	k2eAgNnPc4d1	Rudolecký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
pavilony	pavilon	k1gInPc1	pavilon
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
školní	školní	k2eAgFnPc4d1	školní
jídelny	jídelna	k1gFnPc4	jídelna
kompletně	kompletně	k6eAd1	kompletně
zatepleny	zateplen	k2eAgFnPc1d1	zateplena
a	a	k8xC	a
budovy	budova	k1gFnPc1	budova
také	také	k9	také
dostaly	dostat	k5eAaPmAgFnP	dostat
novou	nový	k2eAgFnSc4d1	nová
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
-	-	kIx~	-
16	[number]	k4	16
školu	škola	k1gFnSc4	škola
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
okolo	okolo	k7c2	okolo
300	[number]	k4	300
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
devět	devět	k4xCc4	devět
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednopatrová	jednopatrový	k2eAgFnSc1d1	jednopatrová
a	a	k8xC	a
nově	nově	k6eAd1	nově
opravená	opravený	k2eAgFnSc1d1	opravená
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
450	[number]	k4	450
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Slavonicích	Slavonice	k1gFnPc6	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Pevnostní	pevnostní	k2eAgInSc1d1	pevnostní
areál	areál	k1gInSc1	areál
Slavonice	Slavonice	k1gFnSc1	Slavonice
-	-	kIx~	-
rekonstruované	rekonstruovaný	k2eAgInPc1d1	rekonstruovaný
zbytky	zbytek	k1gInPc1	zbytek
opevnění	opevnění	k1gNnPc2	opevnění
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Staré	Stará	k1gFnSc6	Stará
Město	město	k1gNnSc1	město
viz	vidět	k5eAaImRp2nS	vidět
odkaz	odkaz	k1gInSc4	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Pfaffenschlag	Pfaffenschlag	k1gInSc1	Pfaffenschlag
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
středověká	středověký	k2eAgFnSc1d1	středověká
vesnice	vesnice	k1gFnSc1	vesnice
3	[number]	k4	3
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Slavonic	Slavonice	k1gFnPc2	Slavonice
-	-	kIx~	-
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Stálkova	Stálkův	k2eAgFnSc1d1	Stálkova
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
města	město	k1gNnSc2	město
Zbytky	zbytek	k1gInPc1	zbytek
opevnění	opevnění	k1gNnSc2	opevnění
Kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
-	-	kIx~	-
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
;	;	kIx,	;
základy	základ	k1gInPc1	základ
byly	být	k5eAaImAgInP	být
položeny	položit	k5eAaPmNgInP	položit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Městská	městský	k2eAgFnSc1d1	městská
věž	věž	k1gFnSc1	věž
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1503	[number]	k4	1503
<g/>
-	-	kIx~	-
<g/>
1549	[number]	k4	1549
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
podpory	podpora	k1gFnSc2	podpora
soukenického	soukenický	k2eAgInSc2d1	soukenický
cechu	cech	k1gInSc2	cech
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1	barokní
střecha	střecha	k1gFnSc1	střecha
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
Slavonice	Slavonice	k1gFnSc1	Slavonice
<g/>
)	)	kIx)	)
-	-	kIx~	-
jednolodní	jednolodní	k2eAgInSc1d1	jednolodní
kostel	kostel	k1gInSc1	kostel
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
sgrafitová	sgrafitový	k2eAgNnPc1d1	sgrafitové
pole	pole	k1gNnPc1	pole
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1582	[number]	k4	1582
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
Ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
,	,	kIx,	,
Kladení	kladení	k1gNnSc2	kladení
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
Setkání	setkání	k1gNnSc1	setkání
Ježíše	Ježíš	k1gMnSc2	Ježíš
s	s	k7c7	s
Veronikou	Veronika	k1gFnSc7	Veronika
<g/>
.	.	kIx.	.
</s>
<s>
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
-	-	kIx~	-
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
<g/>
,	,	kIx,	,
do	do	k7c2	do
průčelí	průčelí	k1gNnSc2	průčelí
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
portál	portál	k1gInSc1	portál
hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
brány	brána	k1gFnSc2	brána
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1586	[number]	k4	1586
s	s	k7c7	s
reliéfní	reliéfní	k2eAgFnSc7d1	reliéfní
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
výzdobou	výzdoba	k1gFnSc7	výzdoba
-	-	kIx~	-
figury	figura	k1gFnSc2	figura
sv.	sv.	kA	sv.
Jana	Jana	k1gFnSc1	Jana
a	a	k8xC	a
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Kristus	Kristus	k1gMnSc1	Kristus
-	-	kIx~	-
soudce	soudce	k1gMnSc1	soudce
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
duze	duha	k1gFnSc6	duha
<g/>
,	,	kIx,	,
štíty	štít	k1gInPc1	štít
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
ze	z	k7c2	z
Šlejnic	Šlejnice	k1gFnPc2	Šlejnice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
na	na	k7c6	na
klenáku	klenák	k1gInSc6	klenák
mistrovská	mistrovský	k2eAgFnSc1d1	mistrovská
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
iniciály	iniciála	k1gFnPc1	iniciála
LO	LO	kA	LO
(	(	kIx(	(
<g/>
Leopold	Leopold	k1gMnSc1	Leopold
Österreicher	Österreichra	k1gFnPc2	Österreichra
<g/>
)	)	kIx)	)
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Božího	boží	k2eAgNnSc2d1	boží
Těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
Svatého	svatý	k2eAgMnSc2d1	svatý
Ducha	duch	k1gMnSc2	duch
<g/>
)	)	kIx)	)
s	s	k7c7	s
přistavěnou	přistavěný	k2eAgFnSc7d1	přistavěná
Loretánskou	loretánský	k2eAgFnSc7d1	Loretánská
kaplí	kaple	k1gFnSc7	kaple
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
-	-	kIx~	-
barokní	barokní	k2eAgFnSc1d1	barokní
stavba	stavba	k1gFnSc1	stavba
asi	asi	k9	asi
z	z	k7c2	z
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
polokruhovým	polokruhový	k2eAgInSc7d1	polokruhový
uzávěrem	uzávěr	k1gInSc7	uzávěr
<g/>
,	,	kIx,	,
průčelí	průčelí	k1gNnSc1	průčelí
má	mít	k5eAaImIp3nS	mít
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
polokruhovou	polokruhový	k2eAgFnSc4d1	polokruhová
arkádu	arkáda	k1gFnSc4	arkáda
a	a	k8xC	a
volutový	volutový	k2eAgInSc1d1	volutový
štít	štít	k1gInSc1	štít
završený	završený	k2eAgInSc1d1	završený
barokní	barokní	k2eAgFnSc7d1	barokní
postavou	postava	k1gFnSc7	postava
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Immaculaty	Immacule	k1gNnPc7	Immacule
ve	v	k7c4	v
svatozáři	svatozáře	k1gFnSc4	svatozáře
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
Slavonice	Slavonice	k1gFnSc2	Slavonice
Dačická	dačický	k2eAgFnSc1d1	Dačická
brána	brána	k1gFnSc1	brána
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
(	(	kIx(	(
<g/>
Jemnická	jemnický	k2eAgFnSc1d1	Jemnická
<g/>
)	)	kIx)	)
brána	brána	k1gFnSc1	brána
Slavonické	Slavonický	k2eAgNnSc1d1	Slavonické
podzemí	podzemí	k1gNnSc1	podzemí
-	-	kIx~	-
unikátní	unikátní	k2eAgFnSc1d1	unikátní
technická	technický	k2eAgFnSc1d1	technická
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
památka	památka	k1gFnSc1	památka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc2	veřejnost
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
Pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
posouzení	posouzení	k1gNnSc4	posouzení
kolísání	kolísání	k1gNnSc2	kolísání
pobyvatel	pobyvatel	k1gMnSc1	pobyvatel
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
obdobích	období	k1gNnPc6	období
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
integrace	integrace	k1gFnSc2	integrace
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
ke	k	k7c3	k
Slavoních	Slavoň	k1gMnPc6	Slavoň
připojeny	připojit	k5eAaPmNgFnP	připojit
dříve	dříve	k6eAd2	dříve
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Vlastkovec	Vlastkovec	k1gInSc1	Vlastkovec
<g/>
,	,	kIx,	,
Mutišov	Mutišov	k1gInSc1	Mutišov
a	a	k8xC	a
Maříž	Maříž	k1gFnSc1	Maříž
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
připojena	připojen	k2eAgFnSc1d1	připojena
obec	obec	k1gFnSc1	obec
Stálkov	Stálkov	k1gInSc1	Stálkov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnSc6d1	uvedená
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
dochází	docházet	k5eAaImIp3nS	docházet
každoročně	každoročně	k6eAd1	každoročně
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Příčinu	příčina	k1gFnSc4	příčina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
naprostém	naprostý	k2eAgInSc6d1	naprostý
nedostatku	nedostatek	k1gInSc6	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
odcházet	odcházet	k5eAaImF	odcházet
za	za	k7c2	za
prací	práce	k1gFnPc2	práce
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Slavonice	Slavonice	k1gFnPc1	Slavonice
stávají	stávat	k5eAaImIp3nP	stávat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistě	jistě	k6eAd1	jistě
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
,	,	kIx,	,
bohémů	bohém	k1gMnPc2	bohém
<g/>
,	,	kIx,	,
bezdomovců	bezdomovec	k1gMnPc2	bezdomovec
a	a	k8xC	a
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Slavonice	Slavonice	k1gFnSc2	Slavonice
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
částí	část	k1gFnPc2	část
na	na	k7c6	na
osmi	osm	k4xCc6	osm
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Slavonice	Slavonice	k1gFnSc1	Slavonice
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Kadolec	Kadolec	k1gMnSc1	Kadolec
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kadolec	Kadolec	k1gInSc1	Kadolec
u	u	k7c2	u
Slavonic	Slavonice	k1gFnPc2	Slavonice
<g/>
)	)	kIx)	)
Maříž	Maříž	k1gFnSc1	Maříž
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Mutišov	Mutišov	k1gInSc1	Mutišov
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Rubašov	Rubašov	k1gInSc1	Rubašov
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Dolní	dolní	k2eAgInSc4d1	dolní
Bolíkov-Rubašov	Bolíkov-Rubašov	k1gInSc4	Bolíkov-Rubašov
<g/>
)	)	kIx)	)
Stálkov	Stálkov	k1gInSc4	Stálkov
(	(	kIx(	(
<g/>
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Vlastkovec	Vlastkovec	k1gMnSc1	Vlastkovec
(	(	kIx(	(
<g/>
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
k.	k.	k?	k.
ú.	ú.	k?	ú.
Léštnice	Léštnice	k1gFnSc1	Léštnice
-	-	kIx~	-
ves	ves	k1gFnSc1	ves
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
4	[number]	k4	4
km	km	kA	km
ZJZ	ZJZ	kA	ZJZ
od	od	k7c2	od
Slavonic	Slavonice	k1gFnPc2	Slavonice
Pfaffenschlag	Pfaffenschlaga	k1gFnPc2	Pfaffenschlaga
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
středověká	středověký	k2eAgFnSc1d1	středověká
vesnice	vesnice	k1gFnSc1	vesnice
3	[number]	k4	3
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Slavonic	Slavonice	k1gFnPc2	Slavonice
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Slavonice	Slavonice	k1gFnSc2	Slavonice
Jan	Jan	k1gMnSc1	Jan
Matěj	Matěj	k1gMnSc1	Matěj
Butz	Butz	k1gMnSc1	Butz
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
Rollsbergu	Rollsberg	k1gInSc2	Rollsberg
(	(	kIx(	(
<g/>
1712	[number]	k4	1712
<g/>
-	-	kIx~	-
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
kanovník	kanovník	k1gMnSc1	kanovník
<g/>
,	,	kIx,	,
prelát	prelát	k1gMnSc1	prelát
kapituly	kapitula	k1gFnSc2	kapitula
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
Vladimír	Vladimír	k1gMnSc1	Vladimír
Nekuda	Nekuda	k1gMnSc1	Nekuda
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
města	město	k1gNnSc2	město
Zikmund	Zikmund	k1gMnSc1	Zikmund
Polášek	Polášek	k1gMnSc1	Polášek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
</s>
