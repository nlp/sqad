<s>
Celé	celý	k2eAgNnSc1d1	celé
tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
a	a	k8xC	a
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
jednobarevný	jednobarevný	k2eAgInSc4d1	jednobarevný
černošedý	černošedý	k2eAgInSc4d1	černošedý
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
našich	naši	k1gMnPc2	naši
rejsků	rejsek	k1gInPc2	rejsek
<g/>
.	.	kIx.	.
</s>
