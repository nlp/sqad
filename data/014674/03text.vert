<s>
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
</s>
<s>
Rotující	rotující	k2eAgFnSc1d1
Země	země	k1gFnSc1
se	s	k7c7
znázorněním	znázornění	k1gNnSc7
osy	osa	k1gFnSc2
</s>
<s>
Pohyb	pohyb	k1gInSc1
úhlu	úhel	k1gInSc2
zemské	zemský	k2eAgFnSc2d1
rotace	rotace	k1gFnSc2
vůči	vůči	k7c3
povrchu	povrch	k1gInSc3
Země	zem	k1gFnSc2
v	v	k7c6
ECEF	ECEF	kA
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
čase	čas	k1gInSc6
v	v	k7c6
MJD	MJD	kA
<g/>
.	.	kIx.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
je	být	k5eAaImIp3nS
osa	osa	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
rotace	rotace	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
myšlená	myšlený	k2eAgFnSc1d1
přímka	přímka	k1gFnSc1
<g/>
,	,	kIx,
okolo	okolo	k7c2
které	který	k3yRgFnSc2
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
planeta	planeta	k1gFnSc1
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
spojuje	spojovat	k5eAaImIp3nS
severní	severní	k2eAgFnSc1d1
a	a	k8xC
jižní	jižní	k2eAgInSc1d1
pól	pól	k1gInSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
točnu	točna	k1gFnSc4
<g/>
)	)	kIx)
na	na	k7c6
povrchu	povrch	k1gInSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rotace	rotace	k1gFnSc1
kolem	kolem	k7c2
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
způsobuje	způsobovat	k5eAaImIp3nS
střídání	střídání	k1gNnSc1
dne	den	k1gInSc2
a	a	k8xC
noci	noc	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
sklon	sklon	k1gInSc1
pak	pak	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
střídání	střídání	k1gNnSc1
ročních	roční	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
jako	jako	k8xS,k8xC
těleso	těleso	k1gNnSc1
koná	konat	k5eAaImIp3nS
mnoho	mnoho	k4c4
pohybů	pohyb	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
rotace	rotace	k1gFnSc1
kolem	kolem	k7c2
osy	osa	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
kolem	kolem	k7c2
jakési	jakýsi	k3yIgFnSc2
myšlené	myšlený	k2eAgFnSc2d1
tyčky	tyčka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
Země	země	k1gFnSc1
jakoby	jakoby	k8xS
navlečena	navlečen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	zem	k1gFnSc2
ale	ale	k8xC
není	být	k5eNaImIp3nS
naprosto	naprosto	k6eAd1
pevné	pevný	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
a	a	k8xC
tak	tak	k6eAd1
ani	ani	k8xC
okamžitá	okamžitý	k2eAgFnSc1d1
osa	osa	k1gFnSc1
rotace	rotace	k1gFnSc1
nesouhlasí	souhlasit	k5eNaImIp3nS
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
osou	osa	k1gFnSc7
setrvačnosti	setrvačnost	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
rotace	rotace	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sama	sám	k3xTgFnSc1
zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
rotace	rotace	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
prostoru	prostor	k1gInSc6
nepatrně	nepatrně	k6eAd1,k6eNd1
pohybuje	pohybovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mění	měnit	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
polohu	poloha	k1gFnSc4
vůči	vůči	k7c3
zemskému	zemský	k2eAgInSc3d1
povrchu	povrch	k1gInSc3
(	(	kIx(
<g/>
pohyb	pohyb	k1gInSc1
pólů	pól	k1gInPc2
vůči	vůči	k7c3
zemské	zemský	k2eAgFnSc3d1
ose	osa	k1gFnSc3
elipsoidu	elipsoid	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koná	konat	k5eAaImIp3nS
precesi	precese	k1gFnSc4
a	a	k8xC
nutaci	nutace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
z	z	k7c2
povahy	povaha	k1gFnSc2
setrvačníku	setrvačník	k1gInSc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
držet	držet	k5eAaImF
pevný	pevný	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukazuje	ukazovat	k5eAaImIp3nS
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
Polárky	Polárka	k1gFnSc2
a	a	k8xC
s	s	k7c7
rovinou	rovina	k1gFnSc7
ekliptiky	ekliptika	k1gFnSc2
svírá	svírat	k5eAaImIp3nS
úhel	úhel	k1gInSc4
66,55	66,55	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s>
Průsečíky	průsečík	k1gInPc1
zemské	zemský	k2eAgFnSc2d1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
s	s	k7c7
povrchem	povrch	k1gInSc7
Země	zem	k1gFnSc2
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
zeměpisné	zeměpisný	k2eAgInPc1d1
póly	pól	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průsečíky	průsečík	k1gInPc4
s	s	k7c7
nebeskou	nebeský	k2eAgFnSc7d1
sférou	sféra	k1gFnSc7
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
nebeské	nebeský	k2eAgInPc1d1
póly	pól	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Základ	základ	k1gInSc1
zeměpisných	zeměpisný	k2eAgFnPc2d1
souřadnic	souřadnice	k1gFnPc2
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
zeměpisných	zeměpisný	k2eAgFnPc2d1
souřadnic	souřadnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemská	zemský	k2eAgFnSc1d1
osa	osa	k1gFnSc1
určuje	určovat	k5eAaImIp3nS
polohu	poloha	k1gFnSc4
severního	severní	k2eAgInSc2d1
a	a	k8xC
jižního	jižní	k2eAgInSc2d1
pólu	pól	k1gInSc2
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
procházejí	procházet	k5eAaImIp3nP
všechny	všechen	k3xTgMnPc4
poledníky	poledník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středem	středem	k7c2
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
a	a	k8xC
kolmo	kolmo	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
prochází	procházet	k5eAaImIp3nS
rovina	rovina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
leží	ležet	k5eAaImIp3nS
rovník	rovník	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
ostatní	ostatní	k2eAgFnPc1d1
rovnoběžky	rovnoběžka	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
rovinách	rovina	k1gFnPc6
kolmých	kolmý	k2eAgFnPc2d1
na	na	k7c4
zemskou	zemský	k2eAgFnSc4d1
osu	osa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poloha	poloha	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
totiž	totiž	k9
ovlivňuje	ovlivňovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
a	a	k8xC
pod	pod	k7c7
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
úhlem	úhel	k1gInSc7
dopadá	dopadat	k5eAaImIp3nS
na	na	k7c4
libovolné	libovolný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
Země	zem	k1gFnSc2
sluneční	sluneční	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
ovlivňuje	ovlivňovat	k5eAaImIp3nS
podmínky	podmínka	k1gFnPc4
na	na	k7c6
celém	celý	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sklon	sklon	k1gInSc1
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
</s>
<s>
Rotace	rotace	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Precese	precese	k1gFnSc1
zemské	zemský	k2eAgFnSc2d1
osy	osa	k1gFnSc2
</s>
<s>
Ekliptika	ekliptika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
4237	#num#	k4
</s>
