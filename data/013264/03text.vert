<p>
<s>
Liberty	Libert	k1gInPc1	Libert
Hyde	Hyd	k1gMnSc2	Hyd
Bailey	Bailea	k1gMnSc2	Bailea
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1858	[number]	k4	1858
South	South	k1gInSc1	South
Haven	Haven	k2eAgInSc1d1	Haven
(	(	kIx(	(
<g/>
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1954	[number]	k4	1954
Ithaca	Ithacum	k1gNnSc2	Ithacum
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
botanik	botanik	k1gMnSc1	botanik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Bailey	Baile	k1gMnPc4	Baile
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c4	na
Michigan	Michigan	k1gInSc4	Michigan
Agricultural	Agricultural	k1gMnSc2	Agricultural
College	Colleg	k1gMnSc2	Colleg
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Michigan	Michigan	k1gInSc1	Michigan
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Cornell	Cornell	k1gInSc4	Cornell
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k1gMnPc3	vedoucí
oddělení	oddělení	k1gNnSc4	oddělení
agrárních	agrární	k2eAgFnPc2d1	agrární
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
American	American	k1gInSc4	American
Society	societa	k1gFnSc2	societa
for	forum	k1gNnPc2	forum
Horticultural	Horticultural	k1gMnSc1	Horticultural
Science	Science	k1gFnSc2	Science
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
rod	rod	k1gInSc1	rod
Baileya	Baileyum	k1gNnSc2	Baileyum
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Asteraceae	Asteraceae	k1gNnSc2	Asteraceae
a	a	k8xC	a
Liberbaileya	Liberbaileyum	k1gNnSc2	Liberbaileyum
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Arecaceae	Arecacea	k1gFnSc2	Arecacea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
časopis	časopis	k1gInSc1	časopis
Baileya	Bailey	k1gInSc2	Bailey
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Fruit-Growing	Fruit-Growing	k1gInSc1	Fruit-Growing
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Nursery	Nurser	k1gInPc1	Nurser
Book	Book	k1gInSc1	Book
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plant-Breeding	Plant-Breeding	k1gInSc1	Plant-Breeding
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Pruning	Pruning	k1gInSc1	Pruning
Manual	Manual	k1gInSc1	Manual
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sketch	Sketch	k1gInSc1	Sketch
of	of	k?	of
the	the	k?	the
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
our	our	k?	our
Native	Natiev	k1gFnSc2	Natiev
Fruits	Fruitsa	k1gFnPc2	Fruitsa
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Agriculture	Agricultur	k1gMnSc5	Agricultur
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
Vegetable	Vegetable	k1gFnSc1	Vegetable
Gardening	Gardening	k1gInSc1	Gardening
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
State	status	k1gInSc5	status
and	and	k?	and
the	the	k?	the
Farmer	Farmer	k1gInSc1	Farmer
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Nature	Natur	k1gMnSc5	Natur
Study	stud	k1gInPc4	stud
Idea	idea	k1gFnSc1	idea
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Training	Training	k1gInSc1	Training
of	of	k?	of
Farmers	Farmers	k1gInSc1	Farmers
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Manual	Manual	k1gInSc1	Manual
of	of	k?	of
Gardening	Gardening	k1gInSc1	Gardening
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Outlook	Outlook	k1gInSc1	Outlook
to	ten	k3xDgNnSc1	ten
Nature	Natur	k1gMnSc5	Natur
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Country	country	k2eAgFnSc1d1	country
Life	Life	k1gFnSc1	Life
Movement	Movement	k1gMnSc1	Movement
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Practical	Practical	k1gFnPc1	Practical
Garden	Gardna	k1gFnPc2	Gardna
Book	Booka	k1gFnPc2	Booka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Liberty	Libert	k1gInPc4	Libert
Hyde	Hyd	k1gFnPc1	Hyd
Bailey	Baile	k2eAgFnPc1d1	Baile
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
