<s>
Oblast	oblast	k1gFnSc1	oblast
spravuje	spravovat	k5eAaImIp3nS	spravovat
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
Správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
evidována	evidovat	k5eAaImNgFnS	evidovat
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
UNESCO	UNESCO	kA	UNESCO
jako	jako	k8xS	jako
biosférická	biosférický	k2eAgFnSc1d1	biosférická
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
,	,	kIx,	,
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
