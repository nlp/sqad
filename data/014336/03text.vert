<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
doporučená	doporučený	k2eAgFnSc1d1
značka	značka	k1gFnSc1
jednotky	jednotka	k1gFnSc2
au	au	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
astronomical	astronomicat	k5eAaPmAgInS
unit	unit	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
ua	ua	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
unité	unita	k1gMnPc1
astronomique	astronomique	k1gNnSc1
<g/>
;	;	kIx,
běžně	běžně	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
značka	značka	k1gFnSc1
AU	au	k0
<g/>
,	,	kIx,
případně	případně	k6eAd1
UA	UA	kA
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
zavedené	zavedený	k2eAgInPc1d1
dříve	dříve	k6eAd2
platnými	platný	k2eAgFnPc7d1
normami	norma	k1gFnPc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
,	,	kIx,
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
astronomii	astronomie	k1gFnSc6
<g/>
,	,	kIx,
původně	původně	k6eAd1
definovaná	definovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
střední	střední	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
Země	zem	k1gFnSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemné	vzájemný	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
planet	planeta	k1gFnPc2
či	či	k8xC
jiných	jiný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
vyjádřené	vyjádřený	k2eAgFnSc2d1
v	v	k7c6
au	au	k0
poskytují	poskytovat	k5eAaImIp3nP
relativně	relativně	k6eAd1
názorné	názorný	k2eAgNnSc4d1
měřítko	měřítko	k1gNnSc4
(	(	kIx(
<g/>
poměr	poměr	k1gInSc1
<g/>
)	)	kIx)
vzdáleností	vzdálenost	k1gFnSc7
těchto	tento	k3xDgInPc2
objektů	objekt	k1gInPc2
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
au	au	k0
=	=	kIx~
1	#num#	k4
ua	ua	k?
=	=	kIx~
1	#num#	k4
AU	au	k0
=	=	kIx~
1	#num#	k4
UA	UA	kA
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
700	#num#	k4
m	m	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
definice	definice	k1gFnSc2
</s>
<s>
Původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
definována	definovat	k5eAaBmNgFnS
jako	jako	k8xS,k8xC
střední	střední	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
Země	zem	k1gFnSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1771	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
pozorování	pozorování	k1gNnSc6
přechodu	přechod	k1gInSc2
Venuše	Venuše	k1gFnSc2
přes	přes	k7c4
sluneční	sluneční	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
,	,	kIx,
francouzským	francouzský	k2eAgMnSc7d1
astronomem	astronom	k1gMnSc7
Jérôme	Jérôm	k1gInSc5
Lalandem	Lalando	k1gNnSc7
jako	jako	k9
153	#num#	k4
<g/>
±	±	k?
<g/>
1	#num#	k4
milion	milion	k4xCgInSc4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Poprvé	poprvé	k6eAd1
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
už	už	k6eAd1
1672	#num#	k4
(	(	kIx(
<g/>
Cassini	Cassin	k2eAgMnPc1d1
<g/>
,	,	kIx,
Picard	Picarda	k1gFnPc2
<g/>
,	,	kIx,
Richer	Richra	k1gFnPc2
<g/>
)	)	kIx)
jako	jako	k9
138	#num#	k4
milionů	milion	k4xCgInPc2
km	km	kA
(	(	kIx(
<g/>
z	z	k7c2
měření	měření	k1gNnSc2
opozice	opozice	k1gFnSc2
Marsu	Mars	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kvůli	kvůli	k7c3
vyšší	vysoký	k2eAgFnSc3d2
přesnosti	přesnost	k1gFnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
IAU	IAU	kA
<g/>
)	)	kIx)
přijala	přijmout	k5eAaPmAgFnS
v	v	k7c6
r.	r.	kA
1976	#num#	k4
novou	nový	k2eAgFnSc4d1
definici	definice	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
je	být	k5eAaImIp3nS
astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
rovna	roven	k2eAgFnSc1d1
délce	délka	k1gFnSc3
poloměru	poloměr	k1gInSc2
nerušené	rušený	k2eNgFnSc2d1
oběžné	oběžný	k2eAgFnSc2d1
kruhové	kruhový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
tělesa	těleso	k1gNnSc2
se	s	k7c7
zanedbatelnou	zanedbatelný	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
<g/>
,	,	kIx,
pohybujícího	pohybující	k2eAgMnSc2d1
se	se	k3xPyFc4
okolo	okolo	k7c2
Slunce	slunce	k1gNnSc2
rychlostí	rychlost	k1gFnSc7
0,017	0,017	k4
202	#num#	k4
098	#num#	k4
950	#num#	k4
radiánů	radián	k1gInPc2
za	za	k7c4
den	den	k1gInSc4
(	(	kIx(
<g/>
86	#num#	k4
400	#num#	k4
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
váže	vázat	k5eAaImIp3nS
délku	délka	k1gFnSc4
astronomické	astronomický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
s	s	k7c7
gravitačním	gravitační	k2eAgInSc7d1
parametrem	parametr	k1gInSc7
Slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
gravitační	gravitační	k2eAgInSc1d1
parametr	parametr	k1gInSc1
byl	být	k5eAaImAgInS
roven	roven	k2eAgInSc1d1
přesně	přesně	k6eAd1
(	(	kIx(
<g/>
0,017	0,017	k4
202	#num#	k4
098	#num#	k4
95	#num#	k4
<g/>
)	)	kIx)
<g/>
²	²	k?
AU³	AU³	k1gFnSc1
<g/>
/	/	kIx~
<g/>
d²	d²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
při	při	k7c6
výpočtech	výpočet	k1gInPc6
používají	používat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
jednotky	jednotka	k1gFnPc1
hmotnost	hmotnost	k1gFnSc4
Slunce	slunce	k1gNnSc1
a	a	k8xC
astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
namísto	namísto	k7c2
kilogramů	kilogram	k1gInPc2
a	a	k8xC
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
potřeba	potřeba	k6eAd1
dosazovat	dosazovat	k5eAaImF
hmotnost	hmotnost	k1gFnSc4
Slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
resp.	resp.	kA
hodnotu	hodnota	k1gFnSc4
gravitačního	gravitační	k2eAgInSc2d1
parametru	parametr	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
totiž	totiž	k9
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
s	s	k7c7
tak	tak	k9
vysokou	vysoký	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
jako	jako	k8xS,k8xC
některé	některý	k3yIgInPc4
další	další	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Experimentálně	experimentálně	k6eAd1
zjištěná	zjištěný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
odpovídající	odpovídající	k2eAgFnSc4d1
definici	definice	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
1	#num#	k4
AU	au	k0
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
700	#num#	k4
±	±	k?
3	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
2012	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
Valném	valný	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
IAU	IAU	kA
v	v	k7c6
Pekingu	Peking	k1gInSc6
doporučeno	doporučit	k5eAaPmNgNnS
sjednotit	sjednotit	k5eAaPmF
značení	značení	k1gNnSc4
astronomické	astronomický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
na	na	k7c6
au	au	k0
a	a	k8xC
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
nově	nově	k6eAd1
definována	definovat	k5eAaBmNgFnS
přesným	přesný	k2eAgInSc7d1
převodním	převodní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
k	k	k7c3
metru	metro	k1gNnSc3
<g/>
:	:	kIx,
1	#num#	k4
au	au	k0
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
700	#num#	k4
m	m	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Odpadly	odpadnout	k5eAaPmAgFnP
tak	tak	k9
<g />
.	.	kIx.
</s>
<s hack="1">
diskuse	diskuse	k1gFnSc2
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
uvažováním	uvažování	k1gNnSc7
relativistických	relativistický	k2eAgInPc2d1
efektů	efekt	k1gInPc2
a	a	k8xC
úbytku	úbytek	k1gInSc2
hmotnosti	hmotnost	k1gFnSc2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c4
dřívější	dřívější	k2eAgFnSc4d1
definici	definice	k1gFnSc4
nezmiňované	zmiňovaný	k2eNgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
vyloučil	vyloučit	k5eAaPmAgInS
vliv	vliv	k1gInSc4
naměřeného	naměřený	k2eAgMnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
zatím	zatím	k6eAd1
nevysvětleného	vysvětlený	k2eNgInSc2d1
nárůstu	nárůst	k1gInSc2
vzdálenosti	vzdálenost	k1gFnSc2
Země	zem	k1gFnSc2
a	a	k8xC
Slunce	slunce	k1gNnSc2
mezi	mezi	k7c4
roky	rok	k1gInPc4
1976	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
změřeného	změřený	k2eAgInSc2d1
pomocí	pomocí	k7c2
telemetrie	telemetrie	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
měřitelně	měřitelně	k6eAd1
(	(	kIx(
<g/>
podle	podle	k7c2
elementů	element	k1gInPc2
drah	draha	k1gFnPc2
<g/>
)	)	kIx)
měnil	měnit	k5eAaImAgInS
gravitační	gravitační	k2eAgInSc1d1
parametr	parametr	k1gInSc1
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
podle	podle	k7c2
soustavy	soustava	k1gFnSc2
SI	se	k3xPyFc3
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	si	k1gNnSc2
vedlejší	vedlejší	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
délky	délka	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
značena	značit	k5eAaImNgFnS
UA	UA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kategorie	kategorie	k1gFnPc1
vedlejších	vedlejší	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
zrušena	zrušit	k5eAaPmNgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
používání	používání	k1gNnSc1
některých	některý	k3yIgFnPc2
mimosoustavových	mimosoustavový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
SI	si	k1gNnSc2
se	se	k3xPyFc4
nadále	nadále	k6eAd1
připouští	připouštět	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
značená	značený	k2eAgFnSc1d1
ua	ua	k?
<g/>
,	,	kIx,
řazena	řazen	k2eAgFnSc1d1
do	do	k7c2
kategorie	kategorie	k1gFnSc2
mimosoustavových	mimosoustavový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
používání	používání	k1gNnSc1
je	být	k5eAaImIp3nS
akceptováno	akceptovat	k5eAaBmNgNnS
souběžně	souběžně	k6eAd1
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
SI	si	k1gNnSc2
a	a	k8xC
jejichž	jejichž	k3xOyRp3gInSc1
vztah	vztah	k1gInSc1
k	k	k7c3
jednotkám	jednotka	k1gFnPc3
SI	si	k1gNnSc2
není	být	k5eNaImIp3nS
definován	definovat	k5eAaBmNgInS
pevně	pevně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
experimentálním	experimentální	k2eAgNnSc6d1
určení	určení	k1gNnSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
elektronvolt	elektronvolt	k1gInSc4
či	či	k8xC
dalton	dalton	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
zachovávala	zachovávat	k5eAaImAgFnS
dřívější	dřívější	k2eAgFnSc4d1
definici	definice	k1gFnSc4
platnou	platný	k2eAgFnSc4d1
od	od	k7c2
r.	r.	kA
1976	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
jako	jako	k9
experimentálně	experimentálně	k6eAd1
zjištěnou	zjištěný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
byl	být	k5eAaImAgInS
uváděn	uvádět	k5eAaImNgInS
starší	starý	k2eAgInSc1d2
údaj	údaj	k1gInSc1
z	z	k7c2
r.	r.	kA
1995	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
ua	ua	k?
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
691	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
m.	m.	k?
</s>
<s>
Dodatkem	dodatek	k1gInSc7
Příručky	příručka	k1gFnSc2
SI	si	k1gNnSc2
z	z	k7c2
r.	r.	kA
2014	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
přesná	přesný	k2eAgFnSc1d1
definice	definice	k1gFnSc1
astronomické	astronomický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
podle	podle	k7c2
IAU	IAU	kA
se	s	k7c7
značkou	značka	k1gFnSc7
au	au	k0
(	(	kIx(
<g/>
1	#num#	k4
au	au	k0
=	=	kIx~
149	#num#	k4
597	#num#	k4
870	#num#	k4
700	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
jednotka	jednotka	k1gFnSc1
byla	být	k5eAaImAgFnS
přeřazena	přeřadit	k5eAaPmNgFnS
do	do	k7c2
kategorie	kategorie	k1gFnSc2
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
používání	používání	k1gNnSc1
je	být	k5eAaImIp3nS
akceptováno	akceptovat	k5eAaBmNgNnS
souběžně	souběžně	k6eAd1
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
SI	se	k3xPyFc3
(	(	kIx(
<g/>
a	a	k8xC
přitom	přitom	k6eAd1
nezávisí	záviset	k5eNaImIp3nS
na	na	k7c6
experimentálním	experimentální	k2eAgNnSc6d1
určení	určení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
Wikipedie	Wikipedie	k1gFnPc1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nP
zatím	zatím	k6eAd1
nejrozšířenějšího	rozšířený	k2eAgNnSc2d3
značení	značení	k1gNnSc2
AU	au	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proto	proto	k8xC
nadále	nadále	k6eAd1
použito	použít	k5eAaPmNgNnS
i	i	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1
Země	zem	k1gFnSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
1,00	1,00	k4
±	±	k?
0,02	0,02	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Měsíc	měsíc	k1gInSc1
obíhá	obíhat	k5eAaImIp3nS
kolem	kolem	k7c2
Země	zem	k1gFnSc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
0,0026	0,0026	k4
±	±	k?
0,0001	0,0001	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Mars	Mars	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Slunce	slunce	k1gNnSc2
vzdálen	vzdálen	k2eAgMnSc1d1
1,52	1,52	k4
±	±	k?
0,14	0,14	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Slunce	slunce	k1gNnSc2
vzdálen	vzdálen	k2eAgMnSc1d1
5,20	5,20	k4
±	±	k?
0,05	0,05	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Pluto	Pluto	k1gMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Slunce	slunce	k1gNnSc2
vzdáleno	vzdálit	k5eAaPmNgNnS
39,5	39,5	k4
±	±	k?
9,8	9,8	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
planetky	planetka	k1gFnSc2
90377	#num#	k4
Sedna	Sedn	k1gInSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
76	#num#	k4
a	a	k8xC
915	#num#	k4
AU	au	k0
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
momentálně	momentálně	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
90	#num#	k4
AU	au	k0
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgFnSc1d3
člověkem	člověk	k1gMnSc7
vyrobené	vyrobený	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
sonda	sonda	k1gFnSc1
Voyager	Voyager	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
v	v	k7c6
listopadu	listopad	k1gInSc6
2017	#num#	k4
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
140,9	140,9	k4
AU	au	k0
od	od	k7c2
Slunce	slunce	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průměr	průměr	k1gInSc1
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
bez	bez	k7c2
Oortova	Oortův	k2eAgInSc2d1
oblaku	oblak	k1gInSc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
105	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Průměr	průměr	k1gInSc1
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
s	s	k7c7
Oortovým	Oortův	k2eAgInSc7d1
oblakem	oblak	k1gInSc7
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
50	#num#	k4
000	#num#	k4
až	až	k9
100	#num#	k4
000	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc1d3
hvězda	hvězda	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
Slunci	slunce	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Proxima	Proxima	k1gNnSc1
Centauri	Centaur	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
268	#num#	k4
000	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Průměr	průměr	k1gInSc1
hvězdy	hvězda	k1gFnSc2
Betelgeuze	Betelgeuze	k1gFnSc2
je	být	k5eAaImIp3nS
2,57	2,57	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1
Slunce	slunce	k1gNnSc2
od	od	k7c2
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1,7	1,7	k4
<g/>
×	×	k?
<g/>
109	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
viditelného	viditelný	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
je	být	k5eAaImIp3nS
asi	asi	k9
8,66	8,66	k4
<g/>
×	×	k?
<g/>
1014	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s>
Násobky	násobek	k1gInPc1
</s>
<s>
Historickou	historický	k2eAgFnSc4d1
<g/>
,	,	kIx,
řídce	řídce	k6eAd1
používanou	používaný	k2eAgFnSc7d1
astronomickou	astronomický	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
je	být	k5eAaImIp3nS
miliónnásobek	miliónnásobek	k1gInSc1
astronomické	astronomický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
se	s	k7c7
samostatným	samostatný	k2eAgInSc7d1
názvem	název	k1gInSc7
siriometr	siriometr	k1gInSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
též	též	k9
nazývaný	nazývaný	k2eAgInSc1d1
astron	astron	k1gInSc1
<g/>
,	,	kIx,
makron	makron	k1gInSc1
či	či	k8xC
metron	metron	k1gInSc1
(	(	kIx(
<g/>
bez	bez	k7c2
zavedených	zavedený	k2eAgFnPc2d1
zkrácených	zkrácený	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Převodní	převodní	k2eAgInSc1d1
vztah	vztah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
siriometr	siriometr	k1gInSc1
=	=	kIx~
1	#num#	k4
astron	astron	k1gInSc1
=	=	kIx~
1	#num#	k4
makron	makron	k1gInSc1
=	=	kIx~
1	#num#	k4
metron	metron	k1gInSc1
=	=	kIx~
106	#num#	k4
AU	au	k0
</s>
<s>
1	#num#	k4
siriometr	siriometr	k1gInSc1
zhruba	zhruba	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
dvojnásobku	dvojnásobek	k1gInSc3
vzdálenosti	vzdálenost	k1gFnSc2
mezi	mezi	k7c7
Zemí	zem	k1gFnSc7
a	a	k8xC
Siriem	Sirium	k1gNnSc7
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jednotku	jednotka	k1gFnSc4
siriometr	siriometr	k1gInSc1
navrhl	navrhnout	k5eAaPmAgInS
v	v	k7c6
r.	r.	kA
1911	#num#	k4
švédský	švédský	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
Carl	Carl	k1gMnSc1
Vilhelm	Vilhelm	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Charlier	Charlier	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Převod	převod	k1gInSc1
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
</s>
<s>
1	#num#	k4
AU	au	k0
=	=	kIx~
149	#num#	k4
597	#num#	k4
870,7	870,7	k4
km	km	kA
(	(	kIx(
<g/>
přesně	přesně	k6eAd1
<g/>
)	)	kIx)
≈	≈	k?
150	#num#	k4
milionů	milion	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
(	(	kIx(
<g/>
150	#num#	k4
gigametrů	gigametr	k1gInPc2
<g/>
,	,	kIx,
150	#num#	k4
Gm	Gm	k1gFnPc2
<g/>
)	)	kIx)
≈	≈	k?
8,317	8,317	k4
světelných	světelný	k2eAgFnPc2d1
minut	minuta	k1gFnPc2
≈	≈	k?
499	#num#	k4
světelných	světelný	k2eAgFnPc2d1
sekund	sekunda	k1gFnPc2
</s>
<s>
1	#num#	k4
světelná	světelný	k2eAgFnSc1d1
sekunda	sekunda	k1gFnSc1
≈	≈	k?
0,002	0,002	k4
AU	au	k0
</s>
<s>
1	#num#	k4
světelná	světelný	k2eAgFnSc1d1
minuta	minuta	k1gFnSc1
≈	≈	k?
0,120	0,120	k4
AU	au	k0
</s>
<s>
1	#num#	k4
světelná	světelný	k2eAgFnSc1d1
hodina	hodina	k1gFnSc1
≈	≈	k?
7,214	7,214	k4
AU	au	k0
</s>
<s>
1	#num#	k4
světelný	světelný	k2eAgInSc4d1
den	den	k1gInSc4
≈	≈	k?
173	#num#	k4
AU	au	k0
</s>
<s>
1	#num#	k4
světelný	světelný	k2eAgInSc4d1
rok	rok	k1gInSc4
≈	≈	k?
63	#num#	k4
241	#num#	k4
AU	au	k0
</s>
<s>
1	#num#	k4
parsec	parsec	k1gInSc1
≈	≈	k?
206	#num#	k4
265	#num#	k4
AU	au	k0
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Rezoluce	rezoluce	k1gFnSc1
<g/>
,	,	kIx,
přijatá	přijatý	k2eAgFnSc1d1
XXVIII	XXVIII	kA
<g/>
.	.	kIx.
valným	valný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
Mezinárodní	mezinárodní	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
Rezoluce	rezoluce	k1gFnSc1
B	B	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IAU	IAU	kA
<g/>
,	,	kIx,
2012	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Příručka	příručka	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
Tabulka	tabulka	k1gFnSc1
akceptovaných	akceptovaný	k2eAgFnPc2d1
mimosoustavových	mimosoustavový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
↑	↑	k?
ČSN	ČSN	kA
ISO	ISO	kA
80000-3	80000-3	k4
Veličiny	veličina	k1gFnPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Prostor	prostor	k1gInSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příloha	příloha	k1gFnSc1
C.	C.	kA
Český	český	k2eAgInSc1d1
normalizační	normalizační	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
↑	↑	k?
ČSN	ČSN	kA
ISO	ISO	kA
31-1	31-1	k4
Veličiny	veličina	k1gFnPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
Prostor	prostor	k1gInSc4
a	a	k8xC
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příloha	příloha	k1gFnSc1
B.	B.	kA
Český	český	k2eAgInSc1d1
normalizační	normalizační	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
19941	#num#	k4
2	#num#	k4
ČSN	ČSN	kA
01	#num#	k4
1300	#num#	k4
Zákonné	zákonný	k2eAgFnSc2d1
měřicí	měřicí	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddíl	oddíl	k1gInSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedlejší	vedlejší	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
tab	tab	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydavatelství	vydavatelství	k1gNnSc1
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
a	a	k8xC
měření	měření	k1gNnSc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1979	#num#	k4
(	(	kIx(
<g/>
novelizace	novelizace	k1gFnSc1
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://astro.physics.muni.cz/download/documents/skripta/F6560.pdf	http://astro.physics.muni.cz/download/documents/skripta/F6560.pdf	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
IAU	IAU	kA
28	#num#	k4
<g/>
th	th	k?
General	General	k1gMnSc1
Assembly	Assembly	k1gMnSc1
farewells	farewellsa	k1gFnPc2
Beijing	Beijing	k1gInSc4
and	and	k?
says	says	k1gInSc1
‘	‘	k?
<g/>
Aloha	Aloha	k1gFnSc1
<g/>
’	’	k?
to	ten	k3xDgNnSc1
Hawaii	Hawaie	k1gFnSc4
for	forum	k1gNnPc2
2015	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
KUBALA	Kubala	k1gMnSc1
Petr	Petr	k1gMnSc1
<g/>
:	:	kIx,
AU	au	k0
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
definice	definice	k1gFnSc1
astronomické	astronomický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ScienceWORLD	ScienceWORLD	k1gFnPc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
↑	↑	k?
http://www.newscientist.com/article/dn17228-why-is-the-earth-moving-away-from-the-sun.html	http://www.newscientist.com/article/dn17228-why-is-the-earth-moving-away-from-the-sun.html	k1gMnSc1
-	-	kIx~
Why	Why	k1gMnSc1
is	is	k?
the	the	k?
Earth	Earth	k1gInSc1
moving	moving	k1gInSc1
away	awaa	k1gFnSc2
from	froma	k1gFnPc2
the	the	k?
sun	suna	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
ANDERSON	Anderson	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
D.	D.	kA
<g/>
;	;	kIx,
NIETO	NIETO	kA
<g/>
,	,	kIx,
M.	M.	kA
M.	M.	kA
Astrometric	Astrometric	k1gMnSc1
solar-system	solar-syst	k1gInSc7
anomalies	anomaliesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
KLIONER	KLIONER	kA
<g/>
,	,	kIx,
S.	S.	kA
A.	A.	kA
<g/>
;	;	kIx,
SEIDELMANN	SEIDELMANN	kA
<g/>
,	,	kIx,
P.	P.	kA
K.	K.	kA
<g/>
;	;	kIx,
SOFFEL	SOFFEL	kA
<g/>
,	,	kIx,
M.	M.	kA
H.	H.	kA
Relativity	relativita	k1gFnPc1
in	in	k?
Fundamental	Fundamental	k1gMnSc1
Astronomy	astronom	k1gMnPc4
<g/>
:	:	kIx,
Dynamics	Dynamics	k1gInSc1
<g/>
,	,	kIx,
Reference	reference	k1gFnPc1
Frames	Frames	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Data	datum	k1gNnSc2
Analysis	Analysis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc4
IAU	IAU	kA
Symposium	symposium	k1gNnSc1
261	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780521764810	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
189	#num#	k4
<g/>
–	–	k?
<g/>
197	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://arxiv.org/pdf/1001.1697.pdf	http://arxiv.org/pdf/1001.1697.pdf	k1gInSc1
-	-	kIx~
Effect	Effect	k1gInSc1
of	of	k?
Sun	Sun	kA
and	and	k?
Planet-Bound	Planet-Bound	k1gMnSc1
Dark	Dark	k1gMnSc1
Matter	Matter	k1gMnSc1
on	on	k3xPp3gMnSc1
Planet	planeta	k1gFnPc2
and	and	k?
Satellite	Satellit	k1gInSc5
Dynamics	Dynamicsa	k1gFnPc2
in	in	k?
the	the	k?
Solar	Solara	k1gFnPc2
System	Syst	k1gMnSc7
<g/>
↑	↑	k?
Příruška	Příruška	k1gFnSc1
SI	si	k1gNnSc2
<g/>
:	:	kIx,
Tabulka	tabulka	k1gFnSc1
mimosoustavových	mimosoustavový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
závislých	závislý	k2eAgFnPc2d1
na	na	k7c6
experimentálním	experimentální	k2eAgNnSc6d1
určení	určení	k1gNnSc6
<g/>
↑	↑	k?
The	The	k1gFnSc2
International	International	k1gMnSc7
System	Syst	k1gMnSc7
of	of	k?
Units	Units	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Supplement	Supplement	k1gInSc1
2014	#num#	k4
<g/>
:	:	kIx,
Updates	Updates	k1gMnSc1
to	ten	k3xDgNnSc4
the	the	k?
8	#num#	k4
<g/>
th	th	k?
edition	edition	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
of	of	k?
the	the	k?
SI	se	k3xPyFc3
Brochure	Brochur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BIPM	BIPM	kA
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
PDF	PDF	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
NASA	NASA	kA
<g/>
:	:	kIx,
Where	Wher	k1gInSc5
are	ar	k1gInSc5
the	the	k?
Voyagers	Voyagersa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BREZINŠČAK	BREZINŠČAK	kA
<g/>
,	,	kIx,
Marian	Mariana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veličiny	veličina	k1gFnPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
v	v	k7c6
technické	technický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Zdeněk	Zdeněk	k1gMnSc1
Štoud	Štoud	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
Běťák	Běťák	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
468	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
8	#num#	k4
obrazových	obrazový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
příloha	příloha	k1gFnSc1
pod	pod	k7c7
páskou	páska	k1gFnSc7
<g/>
.	.	kIx.
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
B	B	kA
<g/>
.1	.1	k4
<g/>
.3	.3	k4
<g/>
.2	.2	k4
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠINDELÁŘ	Šindelář	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
SMRŽ	Smrž	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
BEŤÁK	BEŤÁK	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
672	#num#	k4
s.	s.	k?
14	#num#	k4
<g/>
-	-	kIx~
<g/>
539	#num#	k4
<g/>
-	-	kIx~
<g/>
81	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
V	v	k7c6
<g/>
.5	.5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnSc1d2
a	a	k8xC
cizí	cizí	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
s.	s.	k?
456	#num#	k4
<g/>
-	-	kIx~
<g/>
462	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CHARLIER	CHARLIER	kA
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Vilhelm	Vilhelm	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lectures	Lectures	k1gInSc4
on	on	k3xPp3gMnSc1
Stellar	Stellar	k1gMnSc1
Statistics	Statisticsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lund	Lund	k1gInSc1
<g/>
:	:	kIx,
Scientia	Scientia	k1gFnSc1
Publisher	Publishra	k1gFnPc2
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
.	.	kIx.
49	#num#	k4
s.	s.	k?
elektronické	elektronický	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
2007	#num#	k4
v	v	k7c4
Project	Project	k2eAgInSc4d1
Gutenberg	Gutenberg	k1gInSc4
eBook	eBook	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1077621957	#num#	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
542	#num#	k4
</s>
