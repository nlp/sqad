<s>
Astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
doporučená	doporučený	k2eAgFnSc1d1
značka	značka	k1gFnSc1
jednotky	jednotka	k1gFnSc2
au	au	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
astronomical	astronomicat	k5eAaPmAgInS
unit	unit	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
ua	ua	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
unité	unita	k1gMnPc1
astronomique	astronomique	k1gNnSc1
<g/>
;	;	kIx,
běžně	běžně	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
značka	značka	k1gFnSc1
AU	au	k0
<g/>
,	,	kIx,
případně	případně	k6eAd1
UA	UA	kA
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
zavedené	zavedený	k2eAgInPc1d1
dříve	dříve	k6eAd2
platnými	platný	k2eAgFnPc7d1
normami	norma	k1gFnPc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
,	,	kIx,
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
astronomii	astronomie	k1gFnSc6
<g/>
,	,	kIx,
původně	původně	k6eAd1
definovaná	definovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
střední	střední	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
Země	zem	k1gFnSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>