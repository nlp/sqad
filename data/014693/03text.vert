<s>
Anna	Anna	k1gFnSc1
Haagová	Haagový	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Haagová	Haagový	k2eAgFnSc1d1
Portrét	portrét	k1gInSc4
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Köping	Köping	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
163	#num#	k4
cm	cm	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.annahaag.com	www.annahaag.com	k1gInSc1
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sport	sport	k1gInSc1
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
Klub	klub	k1gInSc1
</s>
<s>
IFK	IFK	kA
Mora	mora	k1gFnSc1
SK	Sk	kA
Lyže	lyže	k1gFnPc1
</s>
<s>
Atomic	Atomic	k1gMnSc1
Ukončení	ukončení	k1gNnSc2
kariéry	kariéra	k1gFnSc2
</s>
<s>
2018	#num#	k4
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
Debut	debut	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
Nejlepší	dobrý	k2eAgInSc4d3
umístění	umístění	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
výher	výhra	k1gFnPc2
</s>
<s>
1	#num#	k4
Nejlépe	dobře	k6eAd3
v	v	k7c6
TdS	TdS	k1gFnSc6
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
Medailový	medailový	k2eAgInSc1d1
zisk	zisk	k1gInSc1
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
1	#num#	k4
-	-	kIx~
3	#num#	k4
-	-	kIx~
0	#num#	k4
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
0	#num#	k4
-	-	kIx~
3	#num#	k4
-	-	kIx~
1	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Běh	běh	k1gInSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Vancouver	Vancouver	k1gInSc1
2010	#num#	k4
</s>
<s>
skiatlon	skiatlon	k1gInSc1
15	#num#	k4
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Vancouver	Vancouver	k1gInSc1
2010	#num#	k4
</s>
<s>
týmový	týmový	k2eAgInSc4d1
sprint	sprint	k1gInSc4
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Soči	Soči	k1gNnSc1
2014	#num#	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
5	#num#	k4
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Pchjongčchang	Pchjongčchang	k1gInSc1
2018	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
km	km	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
2009	#num#	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
5	#num#	k4
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
2011	#num#	k4
</s>
<s>
Štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
5	#num#	k4
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
2013	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
5	#num#	k4
<g/>
km	km	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
2017	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
5	#num#	k4
<g/>
km	km	kA
</s>
<s>
Anna	Anna	k1gFnSc1
Haagová	Haagový	k2eAgFnSc1d1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Anna	Anna	k1gFnSc1
Hanssonová	Hanssonová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986	#num#	k4
v	v	k7c6
Köpingu	Köping	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
švédská	švédský	k2eAgFnSc1d1
běžkyně	běžkyně	k1gFnSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodila	závodit	k5eAaImAgFnS
za	za	k7c4
klub	klub	k1gInSc4
IFK	IFK	kA
Mora	mora	k1gFnSc1
SK	Sk	kA
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInPc4d3
úspěchy	úspěch	k1gInPc4
</s>
<s>
V	v	k7c6
závodě	závod	k1gInSc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
dospělých	dospělí	k1gMnPc2
poprvé	poprvé	k6eAd1
startovala	startovat	k5eAaBmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
ve	v	k7c6
švédském	švédský	k2eAgInSc6d1
Gällivare	Gällivar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
startovala	startovat	k5eAaBmAgNnP
poprvé	poprvé	k6eAd1
v	v	k7c6
Liberci	Liberec	k1gInSc6
2009	#num#	k4
a	a	k8xC
získala	získat	k5eAaPmAgFnS
tam	tam	k6eAd1
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
v	v	k7c6
závodu	závod	k1gInSc6
štafet	štafeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
staru	star	k1gInSc6
na	na	k7c6
ZOH	ZOH	kA
obsadila	obsadit	k5eAaPmAgFnS
ve	v	k7c6
Vancouveru	Vancouver	k1gInSc6
2010	#num#	k4
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
10	#num#	k4
km	km	kA
volně	volně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
pak	pak	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
životního	životní	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
ziskem	zisk	k1gInSc7
dvou	dva	k4xCgFnPc2
stříbrných	stříbrný	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
–	–	k?
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
na	na	k7c4
15	#num#	k4
km	km	kA
a	a	k8xC
v	v	k7c6
týmovém	týmový	k2eAgInSc6d1
sprintu	sprint	k1gInSc6
(	(	kIx(
<g/>
s	s	k7c7
Charlotte	Charlott	k1gInSc5
Kalla	Kallo	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodech	závod	k1gInPc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c6
lyžích	lyže	k1gFnPc6
byla	být	k5eAaImAgFnS
zatím	zatím	k6eAd1
nejlépe	dobře	k6eAd3
dvakrát	dvakrát	k6eAd1
třetí	třetí	k4xOgMnSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
v	v	k7c6
Beitostø	Beitostø	k1gInSc6
10	#num#	k4
km	km	kA
volně	volně	k6eAd1
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
v	v	k7c6
Rogle	Rogla	k1gFnSc6
15	#num#	k4
km	km	kA
klasicky	klasicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
Tour	Tour	k1gMnSc1
de	de	k?
Ski	ski	k1gFnSc2
byla	být	k5eAaImAgFnS
třináctá	třináctý	k4xOgFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
svobodná	svobodný	k2eAgFnSc1d1
a	a	k8xC
žije	žít	k5eAaImIp3nS
v	v	k7c6
Moře	mora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
přítelem	přítel	k1gMnSc7
je	být	k5eAaImIp3nS
švédský	švédský	k2eAgMnSc1d1
běžec	běžec	k1gMnSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
Emil	Emil	k1gMnSc1
Jönsson	Jönsson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
2008	#num#	k4
změnila	změnit	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
příjmení	příjmení	k1gNnSc4
z	z	k7c2
Hanssonová	Hanssonová	k1gFnSc1
na	na	k7c4
Haagová	Haagový	k2eAgNnPc4d1
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
rodné	rodný	k2eAgNnSc4d1
příjmení	příjmení	k1gNnSc4
její	její	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Anna	Anna	k1gFnSc1
Haagová	Haagový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc4
Anny	Anna	k1gFnSc2
Haag	Haag	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
FIS	fis	k1gNnSc2
</s>
<s>
Anna	Anna	k1gFnSc1
Haagová	Haagový	k2eAgFnSc1d1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Olympijské	olympijský	k2eAgFnPc1d1
vítězky	vítězka	k1gFnPc1
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c6
lyžích	lyže	k1gFnPc6
–	–	k?
štafeta	štafeta	k1gFnSc1
</s>
<s>
1956	#num#	k4
Polkunen	Polkunen	k1gInSc1
<g/>
,	,	kIx,
Hietamiesová	Hietamiesový	k2eAgFnSc1d1
a	a	k8xC
Rantanenová	Rantanenový	k2eAgFnSc1d1
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1960	#num#	k4
Johansson	Johansson	k1gMnSc1
<g/>
,	,	kIx,
Strandbergová	Strandbergový	k2eAgFnSc1d1
a	a	k8xC
Edströmová	Edströmová	k1gFnSc1
(	(	kIx(
<g/>
SWE	SWE	kA
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
Kolčinová	Kolčinová	k1gFnSc1
<g/>
,	,	kIx,
Mekšilová	Mekšilová	k1gFnSc1
a	a	k8xC
Bojarskichová	Bojarskichová	k1gFnSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1968	#num#	k4
Auflesová	Auflesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Damonová	Damonový	k2eAgFnSc1d1
a	a	k8xC
Lammedalová	Lammedalový	k2eAgFnSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
Muchačjovová	Muchačjovová	k1gFnSc1
<g/>
,	,	kIx,
Oljuninová	Oljuninový	k2eAgFnSc1d1
a	a	k8xC
Kulakovová	Kulakovová	k1gFnSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
Fjodorovová	Fjodorovová	k1gFnSc1
<g/>
,	,	kIx,
Amosovová	Amosovová	k1gFnSc1
<g/>
,	,	kIx,
Smetaninová	Smetaninový	k2eAgFnSc1d1
a	a	k8xC
Kulakovová	Kulakovová	k1gFnSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
Rostocková	Rostocková	k1gFnSc1
<g/>
,	,	kIx,
Andingová	Andingový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Schmidtová	Schmidtová	k1gFnSc1
a	a	k8xC
Petzoldová	Petzoldová	k1gFnSc1
(	(	kIx(
<g/>
GDR	GDR	kA
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Nybrå	Nybrå	k1gFnSc1
<g/>
,	,	kIx,
Jahrenová	Jahrenová	k1gFnSc1
<g/>
,	,	kIx,
Pettersenová	Pettersenový	k2eAgFnSc1d1
a	a	k8xC
Aunliová	Aunliový	k2eAgFnSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
Nagejkinová	Nagejkinová	k1gFnSc1
<g/>
,	,	kIx,
Gavriljuková	Gavriljukový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Tichonovová	Tichonovový	k2eAgFnSc1d1
a	a	k8xC
Rezcovová	Rezcovová	k1gFnSc1
(	(	kIx(
<g/>
URS	URS	kA
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
Vjalbeová	Vjalbeová	k1gFnSc1
<g/>
,	,	kIx,
Smetaninová	Smetaninový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Lazutinová	Lazutinová	k1gFnSc1
a	a	k8xC
Jegorovová	Jegorovová	k1gFnSc1
(	(	kIx(
<g/>
EUN	EUN	kA
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
Vjalbeová	Vjalbeová	k1gFnSc1
<g/>
,	,	kIx,
Lazutinová	Lazutinová	k1gFnSc1
<g/>
,	,	kIx,
Gavriljuková	Gavriljukový	k2eAgFnSc1d1
a	a	k8xC
Jegorovová	Jegorovový	k2eAgFnSc1d1
(	(	kIx(
<g/>
RUS	Rus	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Gavriljuková	Gavriljukový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Danilovová	Danilovová	k1gFnSc1
<g/>
,	,	kIx,
Vjalbeová	Vjalbeová	k1gFnSc1
a	a	k8xC
Lazutinová	Lazutinová	k1gFnSc1
(	(	kIx(
<g/>
RUS	Rus	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Henkelová	Henkelová	k1gFnSc1
<g/>
,	,	kIx,
Bauerová	Bauerová	k1gFnSc1
<g/>
,	,	kIx,
Künzelová	Künzelová	k1gFnSc1
a	a	k8xC
Sachenbacherová-Stehleová	Sachenbacherová-Stehleová	k1gFnSc1
(	(	kIx(
<g/>
GER	Gera	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Baranová	Baranová	k1gFnSc1
<g/>
,	,	kIx,
Kurkinová	Kurkinová	k1gFnSc1
<g/>
,	,	kIx,
Čepalovová	Čepalovový	k2eAgFnSc1d1
a	a	k8xC
Medveděvová	Medveděvový	k2eAgFnSc1d1
(	(	kIx(
<g/>
RUS	Rus	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
Skofterudová	Skofterudová	k1gFnSc1
<g/>
,	,	kIx,
Johaugová	Johaugový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Steiraová	Steiraový	k2eAgFnSc1d1
a	a	k8xC
Bjø	Bjø	k2eAgFnSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Haagová	Haagový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ingemarsdotterová	Ingemarsdotterový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Kallaová	Kallaový	k2eAgFnSc1d1
a	a	k8xC
Wikénová	Wikénový	k2eAgFnSc1d1
(	(	kIx(
<g/>
SWE	SWE	kA
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
Ø	Ø	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jacobsenová	Jacobsenový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Hagaová	Hagaový	k2eAgFnSc1d1
a	a	k8xC
Bjø	Bjø	k2eAgFnSc1d1
(	(	kIx(
<g/>
NOR	Nor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
:	:	kIx,
štafeta	štafeta	k1gFnSc1
3	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
km	km	kA
od	od	k7c2
1976	#num#	k4
<g/>
:	:	kIx,
štafeta	štafeta	k1gFnSc1
4	#num#	k4
<g/>
×	×	k?
<g/>
5	#num#	k4
km	km	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
|	|	kIx~
Švédsko	Švédsko	k1gNnSc1
</s>
