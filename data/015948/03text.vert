<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Google	Google	k1gFnSc1
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
vydání	vydání	k1gNnSc6
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
91.0	91.0	k4
<g/>
.4472	.4472	k4
<g/>
.57	.57	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
Připravovaná	připravovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
72.0	72.0	k4
<g/>
.3626	.3626	k4
<g/>
.81	.81	k4
Operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
</s>
<s>
Android	android	k1gInSc1
4.1	4.1	k4
a	a	k8xC
vyššíChrome	vyššíChrom	k1gInSc5
OSIOS	OSIOS	kA
9	#num#	k4
a	a	k8xC
vyšší	vysoký	k2eAgFnSc1d2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
(	(	kIx(
<g/>
GTK	GTK	kA
v	v	k7c6
<g/>
2.24	2.24	k4
a	a	k8xC
vyšší	vysoký	k2eAgFnSc1d2
<g/>
)	)	kIx)
<g/>
macOS	macOS	k?
10.9	10.9	k4
a	a	k8xC
vyššíWindows	vyššíWindows	k1gInSc1
7	#num#	k4
a	a	k8xC
vyšší	vysoký	k2eAgFnSc1d2
Platforma	platforma	k1gFnSc1
</s>
<s>
IA-	IA-	k?
<g/>
32	#num#	k4
<g/>
,	,	kIx,
x	x	k?
<g/>
64	#num#	k4
<g/>
,	,	kIx,
ARMv	ARMv	k1gInSc1
<g/>
7	#num#	k4
Vyvíjeno	vyvíjet	k5eAaImNgNnS
v	v	k7c4
</s>
<s>
C	C	kA
<g/>
++	++	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
webový	webový	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
<g/>
,	,	kIx,
mobilní	mobilní	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
Licence	licence	k1gFnSc2
</s>
<s>
BSD	BSD	kA
licence	licence	k1gFnSc1
(	(	kIx(
<g/>
zdrojové	zdrojový	k2eAgInPc1d1
kódy	kód	k1gInPc1
a	a	k8xC
spustitelný	spustitelný	k2eAgInSc1d1
Chromium	Chromium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Google	Google	k1gNnSc1
Chrome	chromat	k5eAaImIp3nS
Terms	Terms	k1gInSc4
of	of	k?
Service	Service	k1gFnSc2
(	(	kIx(
<g/>
spustitelný	spustitelný	k2eAgInSc4d1
Google	Google	k1gInSc4
Chrome	chromat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Web	web	k1gInSc1
</s>
<s>
www.google.com/chrome	www.google.com/chrom	k1gInSc5
Český	český	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.google.cz/chrome	www.google.cz/chrom	k1gInSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Google	Google	k1gInSc1
Chrome	Chrome	k1gInSc1
je	on	k3xPp3gNnSc4
víceplatformní	víceplatformní	k2eAgMnSc1d1
webový	webový	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
vyvíjený	vyvíjený	k2eAgMnSc1d1
společností	společnost	k1gFnSc7
Google	Google	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
pro	pro	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Microsoft	Microsoft	kA
Windows	Windows	kA
a	a	k8xC
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
přenesen	přenést	k5eAaPmNgInS
na	na	k7c4
systémy	systém	k1gInPc4
Linux	Linux	kA
<g/>
,	,	kIx,
MacOS	MacOS	k1gFnSc1
<g/>
,	,	kIx,
iOS	iOS	k?
a	a	k8xC
Android	android	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlížeč	prohlížeč	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
hlavní	hlavní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
platforma	platforma	k1gFnSc1
pro	pro	k7c4
webové	webový	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
svobodného	svobodný	k2eAgInSc2d1
a	a	k8xC
otevřeného	otevřený	k2eAgInSc2d1
projektu	projekt	k1gInSc2
Google	Google	k1gNnSc2
Chromium	Chromium	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
samotný	samotný	k2eAgMnSc1d1
Chrome	chromat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
šířen	šířit	k5eAaImNgInS
jako	jako	k8xS,k8xC
sice	sice	k8xC
bezplatný	bezplatný	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
uzavřený	uzavřený	k2eAgInSc1d1
freeware	freeware	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgNnSc7d1
vykreslovacím	vykreslovací	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
prohlížeče	prohlížeč	k1gInSc2
WebKit	WebKita	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
Google	Google	k1gFnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
nakonec	nakonec	k6eAd1
vytvořil	vytvořit	k5eAaPmAgMnS
alternativní	alternativní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
(	(	kIx(
<g/>
fork	fork	k1gInSc4
<g/>
)	)	kIx)
nazvaný	nazvaný	k2eAgInSc4d1
Blink	blink	k0
<g/>
;	;	kIx,
Všechny	všechen	k3xTgFnPc4
varianty	varianta	k1gFnPc4
prohlížeče	prohlížeč	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
kromě	kromě	k7c2
pro	pro	k7c4
systém	systém	k1gInSc4
iOS	iOS	k?
nyní	nyní	k6eAd1
používají	používat	k5eAaImIp3nP
jádro	jádro	k1gNnSc4
Blink	blink	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
dubnu	duben	k1gInSc3
2020	#num#	k4
analytik	analytik	k1gMnSc1
webu	web	k1gInSc2
StatCounter	StatCounter	k1gMnSc1
odhadoval	odhadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Chrome	chromat	k5eAaImIp3nS
má	mít	k5eAaImIp3nS
68	#num#	k4
%	%	kIx~
celosvětového	celosvětový	k2eAgInSc2d1
podílu	podíl	k1gInSc2
webových	webový	k2eAgInPc2d1
prohlížečů	prohlížeč	k1gInPc2
(	(	kIx(
<g/>
po	po	k7c6
špičce	špička	k1gFnSc6
72	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
na	na	k7c6
tradičních	tradiční	k2eAgInPc6d1
stolních	stolní	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
a	a	k8xC
64	#num#	k4
%	%	kIx~
napříč	napříč	k7c7
všemi	všecek	k3xTgFnPc7
platformami	platforma	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
tomuto	tento	k3xDgInSc3
úspěchu	úspěch	k1gInSc3
rozšířila	rozšířit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Google	Google	k1gFnSc2
značku	značka	k1gFnSc4
„	„	k?
<g/>
Chrome	chromat	k5eAaImIp3nS
<g/>
“	“	k?
na	na	k7c4
další	další	k2eAgInPc4d1
produkty	produkt	k1gInPc4
<g/>
:	:	kIx,
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
<g/>
,	,	kIx,
Chromecast	Chromecast	k1gFnSc1
<g/>
,	,	kIx,
Chromebook	Chromebook	k1gInSc1
<g/>
,	,	kIx,
Chromebit	Chromebit	k1gFnSc1
<g/>
,	,	kIx,
Chromebox	Chromebox	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Chromebase	Chromebasa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
Googlu	Googl	k1gInSc2
Eric	Eric	k1gInSc1
Schmidt	Schmidt	k1gMnSc1
odmítal	odmítat	k5eAaImAgMnS
plány	plán	k1gInPc4
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
vlastního	vlastní	k2eAgMnSc2d1
prohlížeče	prohlížeč	k1gMnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
už	už	k6eAd1
spekulovalo	spekulovat	k5eAaImAgNnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Google	Google	k1gFnSc1
„	„	k?
<g/>
něco	něco	k6eAd1
vlastního	vlastní	k2eAgInSc2d1
<g/>
“	“	k?
chystá	chystat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
prohlížeči	prohlížeč	k1gInSc6
se	se	k3xPyFc4
pracovalo	pracovat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
Google	Google	k1gInSc1
zaměstnal	zaměstnat	k5eAaPmAgInS
vývojáře	vývojář	k1gMnSc4
Firefoxu	Firefox	k1gInSc2
Bena	Bena	k?
Goodgera	Goodgera	k1gFnSc1
a	a	k8xC
Darina	Darina	k1gFnSc1
Fishera	Fishera	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Vývojáři	vývojář	k1gMnPc1
prohlížeč	prohlížeč	k1gMnSc1
poskládali	poskládat	k5eAaPmAgMnP
z	z	k7c2
různých	různý	k2eAgInPc2d1
projektů	projekt	k1gInPc2
otevřeného	otevřený	k2eAgInSc2d1
software	software	k1gInSc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
WebKitu	WebKit	k1gInSc2
to	ten	k3xDgNnSc4
byly	být	k5eAaImAgInP
např.	např.	kA
Netscape	Netscap	k1gInSc5
Portable	portable	k1gInSc1
Runtime	runtime	k1gInSc1
<g/>
,	,	kIx,
Network	network	k1gInSc1
Security	Securita	k1gFnSc2
Services	Servicesa	k1gFnPc2
<g/>
,	,	kIx,
NPAPI	NPAPI	kA
<g/>
,	,	kIx,
Skia	Skia	k1gFnSc1
Graphics	Graphicsa	k1gFnPc2
Engine	Engin	k1gMnSc5
a	a	k8xC
SQLite	SQLit	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgNnSc1
oficiální	oficiální	k2eAgNnSc1d1
beta	beta	k1gNnSc1
verze	verze	k1gFnSc2
pro	pro	k7c4
systém	systém	k1gInSc4
Microsoft	Microsoft	kA
Windows	Windows	kA
vyšla	vyjít	k5eAaPmAgFnS
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
43	#num#	k4
jazycích	jazyk	k1gInPc6
včetně	včetně	k7c2
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
1	#num#	k4
první	první	k4xOgFnSc2
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
témže	týž	k3xTgInSc6
měsíci	měsíc	k1gInSc6
se	se	k3xPyFc4
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
páté	pátý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
užívání	užívání	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c4
Internet	Internet	k1gInSc4
Exploreru	Explorer	k1gInSc2
<g/>
,	,	kIx,
Mozille	Mozille	k1gFnSc1
Firefox	Firefox	k1gInSc1
<g/>
,	,	kIx,
Opera	opera	k1gFnSc1
a	a	k8xC
Safari	safari	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
2	#num#	k4
přibyla	přibýt	k5eAaPmAgFnS
funkce	funkce	k1gFnSc1
celoobrazovkového	celoobrazovkový	k2eAgInSc2d1
módu	mód	k1gInSc2
<g/>
,	,	kIx,
automatické	automatický	k2eAgNnSc1d1
vyplňování	vyplňování	k1gNnSc1
údajů	údaj	k1gInPc2
ve	v	k7c6
formulářích	formulář	k1gInPc6
<g/>
,	,	kIx,
zrychlení	zrychlení	k1gNnSc1
JavaScriptu	JavaScript	k1gInSc2
o	o	k7c4
30	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
oproti	oproti	k7c3
verzi	verze	k1gFnSc3
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
3	#num#	k4
podpora	podpora	k1gFnSc1
skinů	skin	k1gMnPc2
(	(	kIx(
<g/>
změna	změna	k1gFnSc1
vzhledu	vzhled	k1gInSc2
prohlížeče	prohlížeč	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
4	#num#	k4
přidává	přidávat	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
automatického	automatický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
cizích	cizí	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
a	a	k8xC
synchronizace	synchronizace	k1gFnSc2
záložek	záložka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
testu	test	k1gInSc6
Acid	Acido	k1gNnPc2
<g/>
3	#num#	k4
Google	Google	k1gNnSc2
Chrome	chromat	k5eAaImIp3nS
4	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
hodnocení	hodnocení	k1gNnSc4
100	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
např.	např.	kA
prohlížeči	prohlížeč	k1gInSc6
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
novinky	novinka	k1gFnPc4
patří	patřit	k5eAaImIp3nS
implementace	implementace	k1gFnSc1
nových	nový	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
HTML	HTML	kA
5	#num#	k4
a	a	k8xC
podporu	podpora	k1gFnSc4
doplňků	doplněk	k1gInPc2
<g/>
,	,	kIx,
obdobných	obdobný	k2eAgInPc2d1
jaké	jaký	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
např.	např.	kA
z	z	k7c2
prohlížeče	prohlížeč	k1gInSc2
Mozilla	Mozillo	k1gNnSc2
Firefox	Firefox	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
5	#num#	k4
synchronizace	synchronizace	k1gFnSc2
nastavení	nastavení	k1gNnPc2
a	a	k8xC
témat	téma	k1gNnPc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
geolokace	geolokace	k1gFnSc1
<g/>
,	,	kIx,
rozšíření	rozšíření	k1gNnSc1
i	i	k8xC
v	v	k7c6
anonymním	anonymní	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
,	,	kIx,
manuální	manuální	k2eAgInSc1d1
překlad	překlad	k1gInSc1
stránek	stránka	k1gFnPc2
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc4
zakazovat	zakazovat	k5eAaImF
jednotlivé	jednotlivý	k2eAgFnPc4d1
pluginy	plugina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlížeč	prohlížeč	k1gMnSc1
si	se	k3xPyFc3
pamatuje	pamatovat	k5eAaImIp3nS
zvolené	zvolený	k2eAgNnSc4d1
zvětšení	zvětšení	k1gNnSc4
zobrazení	zobrazení	k1gNnSc2
pro	pro	k7c4
každou	každý	k3xTgFnSc4
doménu	doména	k1gFnSc4
<g/>
,	,	kIx,
umí	umět	k5eAaImIp3nS
odstraňovat	odstraňovat	k5eAaImF
jednotlivé	jednotlivý	k2eAgFnPc4d1
položky	položka	k1gFnPc4
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
nový	nový	k2eAgMnSc1d1
správce	správce	k1gMnSc1
oblíbených	oblíbený	k2eAgFnPc2d1
položek	položka	k1gFnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
nově	nově	k6eAd1
integrovaný	integrovaný	k2eAgInSc1d1
modul	modul	k1gInSc1
Adobe	Adobe	kA
Flash	Flash	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
6	#num#	k4
přepracované	přepracovaný	k2eAgNnSc1d1
grafické	grafický	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
formátu	formát	k1gInSc2
WebM	WebM	k1gFnSc2
<g/>
,	,	kIx,
integrace	integrace	k1gFnSc2
modulu	modul	k1gInSc2
PDF	PDF	kA
<g/>
,	,	kIx,
skrytí	skrytí	k1gNnSc1
řetězce	řetězec	k1gInSc2
http	http	k1gInSc1
<g/>
:	:	kIx,
<g/>
//	//	k?
v	v	k7c6
adresním	adresní	k2eAgInSc6d1
řádku	řádek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
7	#num#	k4
cloudový	cloudový	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
9	#num#	k4
podpora	podpora	k1gFnSc1
3D	3D	k4
grafiky	grafika	k1gFnSc2
(	(	kIx(
<g/>
WebGL	WebGL	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dynamické	dynamický	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
10	#num#	k4
oddělení	oddělení	k1gNnPc2
grafických	grafický	k2eAgInPc2d1
procesů	proces	k1gInPc2
(	(	kIx(
<g/>
sandbox	sandbox	k1gInSc1
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
přihlášení	přihlášení	k1gNnSc4
ke	k	k7c3
Google	Google	k1gNnSc3
Cloud	Clouda	k1gFnPc2
Print	Printo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
12	#num#	k4
akcelerace	akcelerace	k1gFnSc2
vykreslování	vykreslování	k1gNnSc2
3D	3D	k4
efektů	efekt	k1gInPc2
CSS	CSS	kA
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
13	#num#	k4
funkce	funkce	k1gFnSc2
Instant	Instant	k1gInSc1
Pages	Pages	k1gInSc1
(	(	kIx(
<g/>
inteligentní	inteligentní	k2eAgNnPc1d1
vyhledávání	vyhledávání	k1gNnPc1
na	na	k7c6
webu	web	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
15	#num#	k4
vylepšena	vylepšen	k2eAgFnSc1d1
„	„	k?
<g/>
nová	nový	k2eAgFnSc1d1
karta	karta	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
17	#num#	k4
podpora	podpora	k1gFnSc1
skriptovacího	skriptovací	k2eAgInSc2d1
enginu	engin	k1gInSc2
Java	Javum	k1gNnSc2
V	v	k7c6
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
ochrana	ochrana	k1gFnSc1
proti	proti	k7c3
phishingu	phishing	k1gInSc3
a	a	k8xC
malwaru	malwar	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
19	#num#	k4
možnost	možnost	k1gFnSc4
synchronizace	synchronizace	k1gFnSc2
otevřených	otevřený	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
vylepšení	vylepšení	k1gNnSc1
designu	design	k1gInSc2
stránky	stránka	k1gFnSc2
Nastavení	nastavení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
21	#num#	k4
podpora	podpora	k1gFnSc1
vysokého	vysoký	k2eAgNnSc2d1
rozlišení	rozlišení	k1gNnSc2
obrazovky	obrazovka	k1gFnSc2
<g/>
,	,	kIx,
getUserMedia	getUserMedium	k1gNnSc2
API	API	kA
(	(	kIx(
<g/>
komunikaci	komunikace	k1gFnSc4
v	v	k7c6
prohlížeči	prohlížeč	k1gInSc6
přes	přes	k7c4
internet	internet	k1gInSc4
bez	bez	k7c2
použití	použití	k1gNnSc2
pluginu	plugin	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
Metro	metro	k1gNnSc4
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
23	#num#	k4
podpora	podpora	k1gFnSc1
Do	do	k7c2
Not	nota	k1gFnPc2
Track	Tracka	k1gFnPc2
<g/>
,	,	kIx,
hardwarová	hardwarový	k2eAgFnSc1d1
akcelerace	akcelerace	k1gFnSc1
videa	video	k1gNnSc2
<g/>
,	,	kIx,
správa	správa	k1gFnSc1
oprávnění	oprávnění	k1gNnSc2
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
24	#num#	k4
podpora	podpora	k1gFnSc1
MathML	MathML	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
25	#num#	k4
podpora	podpora	k1gFnSc1
rozpoznání	rozpoznání	k1gNnSc2
řeči	řeč	k1gFnSc2
<g/>
,	,	kIx,
deaktivace	deaktivace	k1gFnSc2
podpory	podpora	k1gFnSc2
tiché	tichý	k2eAgFnSc2d1
instalace	instalace	k1gFnSc2
rozšíření	rozšíření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
26	#num#	k4
vylepšené	vylepšený	k2eAgFnSc2d1
možnosti	možnost	k1gFnSc2
nastavení	nastavení	k1gNnSc2
kontroly	kontrola	k1gFnSc2
pravopisu	pravopis	k1gInSc2
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc4
vytvoření	vytvoření	k1gNnSc2
zástupců	zástupce	k1gMnPc2
pro	pro	k7c4
více	hodně	k6eAd2
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
28	#num#	k4
přechod	přechod	k1gInSc1
z	z	k7c2
vykreslujícího	vykreslující	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
WebKit	WebKit	k1gInSc4
na	na	k7c4
Blink	blink	k0
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
29	#num#	k4
přidáno	přidán	k2eAgNnSc4d1
resetovací	resetovací	k2eAgNnSc4d1
tlačítko	tlačítko	k1gNnSc4
v	v	k7c6
nastavení	nastavení	k1gNnSc6
<g/>
,	,	kIx,
vylepšený	vylepšený	k2eAgInSc1d1
omnibox	omnibox	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
30	#num#	k4
vyhledávání	vyhledávání	k1gNnPc2
pomocí	pomocí	k7c2
obrázků	obrázek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
32	#num#	k4
dostala	dostat	k5eAaPmAgFnS
novinku	novinka	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
indikace	indikace	k1gFnSc1
přehrávání	přehrávání	k1gNnSc4
v	v	k7c6
panelu	panel	k1gInSc6
<g/>
,	,	kIx,
zobrazení	zobrazení	k1gNnSc6
stavové	stavový	k2eAgFnSc2d1
ikony	ikona	k1gFnSc2
spuštěné	spuštěný	k2eAgFnSc2d1
webkamery	webkamera	k1gFnSc2
a	a	k8xC
aktivního	aktivní	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
do	do	k7c2
Chromecastu	Chromecast	k1gInSc2
a	a	k8xC
tedy	tedy	k9
vzdáleného	vzdálený	k2eAgInSc2d1
displeje	displej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plus	plus	k1gInSc1
na	na	k7c4
Windows	Windows	kA
8	#num#	k4
možná	možný	k2eAgFnSc1d1
změna	změna	k1gFnSc1
grafického	grafický	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
prohlížeče	prohlížeč	k1gMnSc2
na	na	k7c4
design	design	k1gInSc4
systému	systém	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
byly	být	k5eAaImAgInP
zavedeny	zaveden	k2eAgInPc1d1
lokální	lokální	k2eAgInPc1d1
účty	účet	k1gInPc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
nutno	nutno	k6eAd1
zadávat	zadávat	k5eAaImF
heslo	heslo	k1gNnSc4
při	při	k7c6
přihlášení	přihlášení	k1gNnSc6
k	k	k7c3
Gmailu	Gmail	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
34	#num#	k4
upraven	upraven	k2eAgInSc1d1
vhled	vhled	k1gInSc1
režimu	režim	k1gInSc2
pro	pro	k7c4
rozhraní	rozhraní	k1gNnSc4
Windows	Windows	kA
8	#num#	k4
<g/>
,	,	kIx,
plus	plus	k6eAd1
podpora	podpora	k1gFnSc1
atributu	atribut	k1gInSc2
srcset	srcset	k5eAaImF,k5eAaPmF,k5eAaBmF
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
lze	lze	k6eAd1
uživatelům	uživatel	k1gMnPc3
doručovat	doručovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
velikosti	velikost	k1gFnPc4
obrázků	obrázek	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
typu	typ	k1gInSc6
zařízení	zařízení	k1gNnSc2
a	a	k8xC
rozlišení	rozlišení	k1gNnSc2
displeje	displej	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
36	#num#	k4
získala	získat	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
designu	design	k1gInSc2
v	v	k7c6
režimu	režim	k1gInSc6
inkognito	inkognito	k6eAd1
<g/>
,	,	kIx,
vylepšený	vylepšený	k2eAgInSc1d1
systém	systém	k1gInSc1
notifikací	notifikace	k1gFnPc2
<g/>
,	,	kIx,
doplnění	doplnění	k1gNnSc1
informací	informace	k1gFnPc2
k	k	k7c3
obnově	obnova	k1gFnSc3
po	po	k7c6
pádu	pád	k1gInSc6
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
i	i	k9
spouštěč	spouštěč	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
aplikací	aplikace	k1gFnSc7
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
37	#num#	k4
přinesl	přinést	k5eAaPmAgInS
funkci	funkce	k1gFnSc4
DirectWrite	DirectWrit	k1gInSc5
pro	pro	k7c4
lepší	dobrý	k2eAgNnSc4d2
vykreslování	vykreslování	k1gNnSc4
fontů	font	k1gInPc2
a	a	k8xC
64	#num#	k4
<g/>
bitovou	bitový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pro	pro	k7c4
Windows	Windows	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Pozor	pozor	k1gInSc1
–	–	k?
od	od	k7c2
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
experimentální	experimentální	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
„	„	k?
<g/>
Karty	karta	k1gFnSc2
ve	v	k7c6
štosu	štosu	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
práci	práce	k1gFnSc4
s	s	k7c7
více	hodně	k6eAd2
otevřenými	otevřený	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
podporována	podporovat	k5eAaImNgFnS
jen	jen	k9
na	na	k7c6
mobilních	mobilní	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
39	#num#	k4
pak	pak	k6eAd1
získal	získat	k5eAaPmAgMnS
(	(	kIx(
<g/>
mimo	mimo	k7c4
vylepšení	vylepšení	k1gNnSc4
stability	stabilita	k1gFnSc2
a	a	k8xC
výkonu	výkon	k1gInSc2
pod	pod	k7c7
kapotou	kapota	k1gFnSc7
<g/>
)	)	kIx)
rozšířeni	rozšířen	k2eAgMnPc1d1
apps	appsa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
API	API	kA
i	i	k9
64	#num#	k4
<g/>
bitovou	bitový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pro	pro	k7c4
Mac	Mac	kA
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
40	#num#	k4
měl	mít	k5eAaImAgInS
implicitně	implicitně	k6eAd1
zakázanou	zakázaný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
protokolu	protokol	k1gInSc2
SSLv	SSLvum	k1gNnPc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylepšen	vylepšen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
info	info	k6eAd1
dialog	dialog	k1gInSc4
pro	pro	k7c4
Chrome	chromat	k5eAaImIp3nS
aplikace	aplikace	k1gFnPc4
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
pro	pro	k7c4
Windows	Windows	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
69	#num#	k4
přinesl	přinést	k5eAaPmAgInS
vzhled	vzhled	k1gInSc1
Material	Material	k1gInSc1
design	design	k1gInSc1
refresh	refresha	k1gFnPc2
a	a	k8xC
vyvolání	vyvolání	k1gNnSc4
nabídky	nabídka	k1gFnSc2
pro	pro	k7c4
vygerování	vygerování	k1gNnSc4
hesla	heslo	k1gNnSc2
kdykoliv	kdykoliv	k6eAd1
prohlížeč	prohlížeč	k1gInSc1
detekuje	detekovat	k5eAaImIp3nS
stránku	stránka	k1gFnSc4
s	s	k7c7
vytvářením	vytváření	k1gNnSc7
nového	nový	k2eAgInSc2d1
účtu	účet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
Zabezpečení	zabezpečení	k1gNnSc1
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
načítá	načítat	k5eAaBmIp3nS,k5eAaPmIp3nS
aktualizace	aktualizace	k1gFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
černých	černý	k2eAgFnPc2d1
listin	listina	k1gFnPc2
(	(	kIx(
<g/>
jednu	jeden	k4xCgFnSc4
pro	pro	k7c4
phishing	phishing	k1gInSc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
pro	pro	k7c4
malware	malwar	k1gMnSc5
<g/>
)	)	kIx)
a	a	k8xC
varuje	varovat	k5eAaImIp3nS
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
pokoušejí	pokoušet	k5eAaImIp3nP
navštívit	navštívit	k5eAaPmF
škodlivou	škodlivý	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
všem	všecek	k3xTgMnPc3
vývojářům	vývojář	k1gMnPc3
pomocí	pomocí	k7c2
veřejného	veřejný	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
Google	Google	k1gFnSc2
Safe	safe	k1gInSc1
Browsing	Browsing	k1gInSc1
API	API	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zařazení	zařazení	k1gNnSc6
na	na	k7c4
černo	černo	k1gNnSc4
listinu	listina	k1gFnSc4
kontaktuje	kontaktovat	k5eAaImIp3nS
Google	Google	k1gInSc4
majitele	majitel	k1gMnSc2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
o	o	k7c4
existenci	existence	k1gFnSc4
malware	malwar	k1gMnSc5
nemusejí	muset	k5eNaImIp3nP
vědět	vědět	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
vytvoří	vytvořit	k5eAaPmIp3nS
pro	pro	k7c4
každou	každý	k3xTgFnSc4
záložku	záložka	k1gFnSc4
samostatný	samostatný	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
efektivně	efektivně	k6eAd1
od	od	k7c2
sebe	se	k3xPyFc2
jednotlivé	jednotlivý	k2eAgInPc4d1
panely	panel	k1gInPc4
izoluje	izolovat	k5eAaBmIp3nS
a	a	k8xC
může	moct	k5eAaImIp3nS
též	též	k9
zabránit	zabránit	k5eAaPmF
malwaru	malwara	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
systému	systém	k1gInSc2
nainstaloval	nainstalovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
celý	celý	k2eAgInSc1d1
model	model	k1gInSc1
alokace	alokace	k1gFnSc2
procesů	proces	k1gInPc2
je	být	k5eAaImIp3nS
složitější	složitý	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
princip	princip	k1gInSc4
nejnižších	nízký	k2eAgNnPc2d3
privilegií	privilegium	k1gNnPc2
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc4
proces	proces	k1gInSc4
zbaven	zbavit	k5eAaPmNgMnS
maxima	maximum	k1gNnSc2
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
oprávnění	oprávnění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
využívat	využívat	k5eAaPmF,k5eAaImF
procesor	procesor	k1gInSc4
pro	pro	k7c4
výpočty	výpočet	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nemůže	moct	k5eNaImIp3nS
zapisovat	zapisovat	k5eAaImF
nebo	nebo	k8xC
číst	číst	k5eAaImF
soubory	soubor	k1gInPc4
z	z	k7c2
citlivých	citlivý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
dokumenty	dokument	k1gInPc1
<g/>
,	,	kIx,
plocha	plocha	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c4
obdobu	obdoba	k1gFnSc4
„	„	k?
<g/>
Chráněného	chráněný	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
“	“	k?
v	v	k7c6
prohlížeči	prohlížeč	k1gInSc6
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
v	v	k7c6
systému	systém	k1gInSc6
Windows	Windows	kA
Vista	vista	k2eAgFnSc4d1
a	a	k8xC
Windows	Windows	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sandbox	Sandbox	k1gInSc1
Team	team	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tuto	tento	k3xDgFnSc4
vlastnost	vlastnost	k1gFnSc4
naprogramoval	naprogramovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgMnS
pomocí	pomocí	k7c2
specifických	specifický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
systému	systém	k1gInSc3
obdobu	obdoba	k1gFnSc4
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
se	se	k3xPyFc4
izolovaný	izolovaný	k2eAgInSc4d1
proces	proces	k1gInSc4
nedostane	dostat	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Škodlivý	škodlivý	k2eAgInSc4d1
software	software	k1gInSc4
běžící	běžící	k2eAgInSc4d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
panelu	panel	k1gInSc6
pak	pak	k6eAd1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgMnSc1d1
zjistit	zjistit	k5eAaPmF
čísla	číslo	k1gNnPc4
kreditních	kreditní	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
vložených	vložený	k2eAgFnPc2d1
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
panelu	panel	k1gInSc6
<g/>
,	,	kIx,
komunikovat	komunikovat	k5eAaImF
se	s	k7c7
vstupy	vstup	k1gInPc7
myši	myš	k1gFnSc2
<g/>
,	,	kIx,
zajistit	zajistit	k5eAaPmF
spuštění	spuštění	k1gNnSc4
programu	program	k1gInSc2
při	při	k7c6
startu	start	k1gInSc6
systému	systém	k1gInSc2
a	a	k8xC
bude	být	k5eAaImBp3nS
ukončen	ukončit	k5eAaPmNgInS
po	po	k7c4
zavření	zavření	k1gNnSc4
panelu	panel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
uvnitř	uvnitř	k7c2
systému	systém	k1gInSc2
víceúrovňový	víceúrovňový	k2eAgInSc1d1
bezpečnostní	bezpečnostní	k2eAgInSc1d1
model	model	k1gInSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
úrovněmi	úroveň	k1gFnPc7
<g/>
:	:	kIx,
uživatel	uživatel	k1gMnSc1
a	a	k8xC
sandbox	sandbox	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sandbox	Sandbox	k1gInSc1
může	moct	k5eAaImIp3nS
reagovat	reagovat	k5eAaBmF
jen	jen	k9
na	na	k7c4
výzvy	výzev	k1gInPc4
uživatele	uživatel	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Linuxu	linux	k1gInSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
sandboxing	sandboxing	k1gInSc4
používán	používán	k2eAgInSc4d1
seccomp	seccomp	k1gInSc4
režim	režim	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pluginy	Plugina	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Adobe	Adobe	kA
Flash	Flash	k1gMnSc1
Player	Player	k1gMnSc1
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
typicky	typicky	k6eAd1
standardizovány	standardizován	k2eAgFnPc1d1
a	a	k8xC
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
umístěny	umístit	k5eAaPmNgInP
do	do	k7c2
sandboxu	sandbox	k1gInSc2
podobně	podobně	k6eAd1
jako	jako	k9
panely	panel	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
musí	muset	k5eAaImIp3nS
běžet	běžet	k5eAaImF
na	na	k7c6
úrovni	úroveň	k1gFnSc6
nebo	nebo	k8xC
dokonce	dokonce	k9
nad	nad	k7c7
úrovní	úroveň	k1gFnSc7
zabezpečení	zabezpečení	k1gNnSc2
samotného	samotný	k2eAgMnSc2d1
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
snížení	snížení	k1gNnSc4
možnosti	možnost	k1gFnSc2
úspěchu	úspěch	k1gInSc2
útoku	útok	k1gInSc2
skrze	skrze	k?
plugin	plugina	k1gFnPc2
jsou	být	k5eAaImIp3nP
pluginy	plugin	k1gInPc1
spouštěny	spouštěn	k2eAgInPc1d1
v	v	k7c6
oddělených	oddělený	k2eAgInPc6d1
procesech	proces	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
komunikují	komunikovat	k5eAaImIp3nP
s	s	k7c7
renderovacím	renderovací	k2eAgNnSc7d1
jádrem	jádro	k1gNnSc7
a	a	k8xC
to	ten	k3xDgNnSc1
pak	pak	k6eAd1
běží	běžet	k5eAaImIp3nS
s	s	k7c7
velmi	velmi	k6eAd1
malými	malý	k2eAgInPc7d1
oprávněními	oprávnění	k1gNnPc7
ve	v	k7c6
vlastním	vlastní	k2eAgInSc6d1
procesu	proces	k1gInSc6
pro	pro	k7c4
každý	každý	k3xTgInSc4
panel	panel	k1gInSc4
zvlášť	zvlášť	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pluginy	Plugin	k2eAgInPc1d1
budou	být	k5eAaImBp3nP
muset	muset	k5eAaImF
být	být	k5eAaImF
upraveny	upravit	k5eAaPmNgInP
pro	pro	k7c4
provoz	provoz	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
softwarové	softwarový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
výše	vysoce	k6eAd2
zmíněný	zmíněný	k2eAgInSc4d1
princip	princip	k1gInSc4
nejnižších	nízký	k2eAgNnPc2d3
oprávnění	oprávnění	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Funkce	funkce	k1gFnSc1
soukromého	soukromý	k2eAgNnSc2d1
prohlížení	prohlížení	k1gNnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
anonymní	anonymní	k2eAgInSc1d1
režim	režim	k1gInSc1
<g/>
“	“	k?
brání	bránit	k5eAaImIp3nS
prohlížeči	prohlížeč	k1gInPc7
v	v	k7c6
ukládání	ukládání	k1gNnSc6
jakékoliv	jakýkoliv	k3yIgFnSc2
informace	informace	k1gFnSc2
o	o	k7c4
historii	historie	k1gFnSc4
a	a	k8xC
cookies	cookies	k1gInSc4
z	z	k7c2
navštívených	navštívený	k2eAgFnPc2d1
internetových	internetový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Anonymní	anonymní	k2eAgInSc1d1
režim	režim	k1gInSc1
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
soukromému	soukromý	k2eAgNnSc3d1
procházení	procházení	k1gNnSc3
v	v	k7c6
aplikaci	aplikace	k1gFnSc6
Internet	Internet	k1gInSc1
Explorer	Explorra	k1gFnPc2
<g/>
,	,	kIx,
Mozilla	Mozilla	k1gFnSc1
Firefox	Firefox	k1gInSc1
<g/>
,	,	kIx,
Opera	opera	k1gFnSc1
či	či	k8xC
Safari	safari	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
bylo	být	k5eAaImAgNnS
do	do	k7c2
kódu	kód	k1gInSc2
prohlížeče	prohlížeč	k1gMnSc2
přidáno	přidán	k2eAgNnSc4d1
varování	varování	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zobrazilo	zobrazit	k5eAaPmAgNnS
při	při	k7c6
nastavení	nastavení	k1gNnSc6
proměnné	proměnná	k1gFnSc2
prostředí	prostředí	k1gNnSc2
SSLKEYLOGFILE	SSLKEYLOGFILE	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožnilo	umožnit	k5eAaPmAgNnS
únik	únik	k1gInSc4
privátních	privátní	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
pro	pro	k7c4
šifrování	šifrování	k1gNnSc4
protokolu	protokol	k1gInSc2
https	httpsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Nebylo	být	k5eNaImAgNnS
ale	ale	k8xC
očekáváno	očekávat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
varování	varování	k1gNnSc1
dostane	dostat	k5eAaPmIp3nS
do	do	k7c2
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
prohlížeče	prohlížeč	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
tuto	tento	k3xDgFnSc4
techniku	technika	k1gFnSc4
používají	používat	k5eAaImIp3nP
antiviry	antivir	k1gInPc4
a	a	k8xC
hrozilo	hrozit	k5eAaImAgNnS
by	by	kYmCp3nS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
by	by	kYmCp3nP
pak	pak	k6eAd1
musely	muset	k5eAaImAgFnP
používat	používat	k5eAaImF
jiné	jiný	k2eAgFnPc1d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
stabilní	stabilní	k2eAgFnPc1d1
<g/>
,	,	kIx,
přístupy	přístup	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Blokování	blokování	k1gNnSc1
malware	malwar	k1gMnSc5
</s>
<s>
Statistiky	statistika	k1gFnPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
uživatelé	uživatel	k1gMnPc1
mají	mít	k5eAaImIp3nP
čtyřikrát	čtyřikrát	k6eAd1
větší	veliký	k2eAgFnSc4d2
pravděpodobnost	pravděpodobnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
podvedeni	podveden	k2eAgMnPc1d1
ke	k	k7c3
stažení	stažení	k1gNnSc3
malware	malwar	k1gMnSc5
<g/>
,	,	kIx,
než	než	k8xS
že	že	k8xS
budou	být	k5eAaImBp3nP
ohroženi	ohrozit	k5eAaPmNgMnP
jeho	jeho	k3xOp3gNnSc7
použitím	použití	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
studii	studie	k1gFnSc6
<g/>
,	,	kIx,
Chrome	chromat	k5eAaImIp3nS
10	#num#	k4
blokoval	blokovat	k5eAaImAgInS
pouze	pouze	k6eAd1
13	#num#	k4
%	%	kIx~
nebezpečných	bezpečný	k2eNgFnPc2d1
adres	adresa	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
se	s	k7c7
Safari	safari	k1gNnSc7
a	a	k8xC
Firefoxem	Firefox	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
9	#num#	k4
blokoval	blokovat	k5eAaImAgInS
92	#num#	k4
%	%	kIx~
malwaru	malwara	k1gFnSc4
s	s	k7c7
filtrováním	filtrování	k1gNnSc7
adres	adresa	k1gFnPc2
a	a	k8xC
100	#num#	k4
%	%	kIx~
s	s	k7c7
filtrováním	filtrování	k1gNnSc7
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
zablokoval	zablokovat	k5eAaPmAgMnS
90	#num#	k4
%	%	kIx~
malwaru	malwar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malware	Malwar	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
instaluje	instalovat	k5eAaBmIp3nS
bez	bez	k7c2
vědomí	vědomí	k1gNnSc2
uživatele	uživatel	k1gMnSc2
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
ve	v	k7c4
studii	studie	k1gFnSc4
zahrnut	zahrnut	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
17	#num#	k4
prohlížeč	prohlížeč	k1gInSc1
nabízel	nabízet	k5eAaImAgInS
vlastní	vlastní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
ochranu	ochrana	k1gFnSc4
proti	proti	k7c3
phishingu	phishing	k1gInSc3
a	a	k8xC
malwaru	malwar	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
testu	test	k1gInSc6
NNS	NNS	kA
labs	labs	k6eAd1
z	z	k7c2
března	březen	k1gInSc2
2014	#num#	k4
skončil	skončit	k5eAaPmAgInS
celkově	celkově	k6eAd1
Chrome	chromat	k5eAaImIp3nS
třetí	třetí	k4xOgMnSc1
(	(	kIx(
<g/>
70,7	70,7	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
druhý	druhý	k4xOgMnSc1
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
čínský	čínský	k2eAgMnSc1d1
Liebao	Liebao	k1gMnSc1
(	(	kIx(
<g/>
Cheetah	Cheetah	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
85,1	85,1	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
skončil	skončit	k5eAaPmAgMnS
IE	IE	kA
(	(	kIx(
<g/>
99,9	99,9	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pluginy	Plugina	k1gFnPc1
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
Google	Google	k1gFnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejnovější	nový	k2eAgFnSc1d3
vývojová	vývojový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
zahrnuje	zahrnovat	k5eAaImIp3nS
Adobe	Adobe	kA
Flash	Flash	k1gMnSc1
plugin	plugin	k1gMnSc1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
prohlížeče	prohlížeč	k1gMnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
není	být	k5eNaImIp3nS
nutno	nutno	k6eAd1
stahovat	stahovat	k5eAaImF
a	a	k8xC
instalovat	instalovat	k5eAaBmF
ho	on	k3xPp3gMnSc4
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flash	Flasha	k1gFnPc2
bude	být	k5eAaImBp3nS
aktualizován	aktualizovat	k5eAaBmNgInS
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
vlastní	vlastní	k2eAgFnSc2d1
aktualizace	aktualizace	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Podpora	podpora	k1gFnSc1
Javy	Java	k1gFnSc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
Chrome	chrom	k1gInSc5
s	s	k7c7
Java	Java	k1gMnSc1
6	#num#	k4
Update	update	k1gInSc1
12	#num#	k4
a	a	k8xC
výše	vysoce	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Podpora	podpora	k1gFnSc1
Javy	Java	k1gFnSc2
pod	pod	k7c7
macOS	macOS	k?
byla	být	k5eAaImAgFnS
poskytována	poskytovat	k5eAaImNgFnS
updatem	update	k1gInSc7
Javy	Java	k1gFnSc2
vydaným	vydaný	k2eAgInSc7d1
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zpočátku	zpočátku	k6eAd1
Chrome	chromat	k5eAaImIp3nS
podporoval	podporovat	k5eAaImAgMnS
všechny	všechen	k3xTgInPc4
pluginy	plugin	k1gInPc4
NPAPI	NPAPI	kA
<g/>
,	,	kIx,
od	od	k7c2
verze	verze	k1gFnSc2
35	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jejich	jejich	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
ukončil	ukončit	k5eAaPmAgMnS
<g/>
,	,	kIx,
nadále	nadále	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
jen	jen	k9
Pepper	Pepper	k1gMnSc1
Plugin	Plugin	k1gMnSc1
API	API	kA
(	(	kIx(
<g/>
PPAPI	PPAPI	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pluginy	Plugina	k1gFnSc2
NPAPI	NPAPI	kA
však	však	k9
v	v	k7c6
prohlížeči	prohlížeč	k1gMnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
to	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
nadále	nadále	k6eAd1
fungují	fungovat	k5eAaImIp3nP
např.	např.	kA
Java	Javum	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nepodporuje	podporovat	k5eNaImIp3nS
vkládání	vkládání	k1gNnSc1
ovládacích	ovládací	k2eAgInPc2d1
prvků	prvek	k1gInPc2
ActiveX	ActiveX	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Motivy	motiv	k1gInPc1
</s>
<s>
Od	od	k7c2
Google	Googl	k1gMnSc2
Chrome	chromat	k5eAaImIp3nS
3.0	3.0	k4
mohou	moct	k5eAaImIp3nP
uživatelé	uživatel	k1gMnPc1
instalovat	instalovat	k5eAaBmF
motivy	motiv	k1gInPc4
pro	pro	k7c4
změnu	změna	k1gFnSc4
vzhledu	vzhled	k1gInSc2
prohlížeče	prohlížeč	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k4c1
volných	volný	k2eAgInPc2d1
motivů	motiv	k1gInPc2
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
online	onlinout	k5eAaPmIp3nS
galerii	galerie	k1gFnSc4
Chrome	chromat	k5eAaImIp3nS
Web	web	k1gInSc1
Store	Stor	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
4	#num#	k4
jsou	být	k5eAaImIp3nP
oficiálně	oficiálně	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k8xC
rozšíření	rozšíření	k1gNnSc3
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vlastně	vlastně	k9
o	o	k7c4
prográmky	prográmek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
rozšiřují	rozšiřovat	k5eAaImIp3nP
možnosti	možnost	k1gFnPc4
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
např.	např.	kA
o	o	k7c4
různá	různý	k2eAgNnPc4d1
tlačítka	tlačítko	k1gNnPc4
pro	pro	k7c4
nastavení	nastavení	k1gNnSc4
<g/>
,	,	kIx,
programy	program	k1gInPc4
na	na	k7c4
blokování	blokování	k1gNnSc4
reklam	reklama	k1gFnPc2
a	a	k8xC
flashe	flashe	k1gFnPc2
<g/>
,	,	kIx,
RSS	RSS	kA
čtečky	čtečka	k1gFnPc1
<g/>
,	,	kIx,
předpovědi	předpověď	k1gFnPc1
počasí	počasí	k1gNnSc1
<g/>
,	,	kIx,
VPN	VPN	kA
<g/>
,	,	kIx,
zálohy	záloha	k1gFnSc2
stránek	stránka	k1gFnPc2
nebo	nebo	k8xC
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
rozšíření	rozšíření	k1gNnSc4
pro	pro	k7c4
správu	správa	k1gFnSc4
karet	kareta	k1gFnPc2
a	a	k8xC
uvolnění	uvolnění	k1gNnSc4
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
snižují	snižovat	k5eAaImIp3nP
náročnost	náročnost	k1gFnSc4
na	na	k7c4
operační	operační	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
kritizovaný	kritizovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
šlo	jít	k5eAaImAgNnS
tyto	tento	k3xDgInPc4
programy	program	k1gInPc4
instalovat	instalovat	k5eAaBmF
i	i	k9
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
hlavně	hlavně	k9
kvůli	kvůli	k7c3
bezpečnosti	bezpečnost	k1gFnSc3
(	(	kIx(
<g/>
od	od	k7c2
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
Windows	Windows	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lze	lze	k6eAd1
stahovat	stahovat	k5eAaImF
tyto	tento	k3xDgInPc4
programy	program	k1gInPc4
jen	jen	k9
z	z	k7c2
oficiálního	oficiální	k2eAgInSc2d1
Internetového	internetový	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Experimentální	experimentální	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
takové	takový	k3xDgNnSc4
skryté	skrytý	k2eAgNnSc4d1
nastavení	nastavení	k1gNnSc4
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jimi	on	k3xPp3gMnPc7
povolit	povolit	k5eAaPmF
či	či	k8xC
zakázat	zakázat	k5eAaPmF
např.	např.	kA
3D	3D	k4
akceleraci	akcelerace	k1gFnSc4
<g/>
,	,	kIx,
úpravu	úprava	k1gFnSc4
dotyku	dotyk	k1gInSc2
<g/>
,	,	kIx,
ukládání	ukládání	k1gNnSc1
stránek	stránka	k1gFnPc2
ve	v	k7c6
formátu	formát	k1gInSc6
MHTML	MHTML	kA
atd.	atd.	kA
Tyto	tento	k3xDgFnPc1
funkce	funkce	k1gFnPc1
jsou	být	k5eAaImIp3nP
občas	občas	k6eAd1
nestabilní	stabilní	k2eNgFnPc1d1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
zapříčinit	zapříčinit	k5eAaPmF
pád	pád	k1gInSc4
prohlížeče	prohlížeč	k1gInSc2
či	či	k8xC
vymazání	vymazání	k1gNnSc1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
doporučuje	doporučovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc4
použití	použití	k1gNnSc4
jen	jen	k9
zkušeným	zkušený	k2eAgMnPc3d1
uživatelům	uživatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
po	po	k7c6
zadání	zadání	k1gNnSc6
chrome	chromat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
//	//	k?
<g/>
flags	flags	k1gInSc1
do	do	k7c2
adresového	adresový	k2eAgInSc2d1
řádku	řádek	k1gInSc2
prohlížeče	prohlížeč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Automatický	automatický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
stránek	stránka	k1gFnPc2
</s>
<s>
Počínaje	počínaje	k7c7
verzí	verze	k1gFnSc7
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
4.1	4.1	k4
je	být	k5eAaImIp3nS
přidán	přidat	k5eAaPmNgInS
v	v	k7c6
panelu	panel	k1gInSc6
vestavěný	vestavěný	k2eAgInSc4d1
překladač	překladač	k1gInSc4
od	od	k7c2
společnosti	společnost	k1gFnSc2
Google	Google	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
52	#num#	k4
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
aplikací	aplikace	k1gFnPc2
a	a	k8xC
webů	web	k1gInPc2
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
umožňuje	umožňovat	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
si	se	k3xPyFc3
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
oblíbené	oblíbený	k2eAgInPc4d1
weby	web	k1gInPc4
či	či	k8xC
aplikace	aplikace	k1gFnSc2
zástupce	zástupce	k1gMnSc2
přímo	přímo	k6eAd1
na	na	k7c6
ploše	plocha	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
nabídce	nabídka	k1gFnSc6
start	start	k1gInSc4
či	či	k8xC
v	v	k7c6
panelu	panel	k1gInSc6
snadné	snadný	k2eAgNnSc1d1
spuštění	spuštění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
následném	následný	k2eAgNnSc6d1
kliknutí	kliknutí	k1gNnSc6
na	na	k7c4
ikonku	ikonka	k1gFnSc4
se	se	k3xPyFc4
načte	načíst	k5eAaPmIp3nS,k5eAaBmIp3nS
samostatné	samostatný	k2eAgNnSc1d1
okno	okno	k1gNnSc1
Chrome	chromat	k5eAaImIp3nS
<g/>
,	,	kIx,
bez	bez	k7c2
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
tlačítek	tlačítko	k1gNnPc2
<g/>
,	,	kIx,
adresního	adresní	k2eAgInSc2d1
řádku	řádek	k1gInSc2
nebo	nebo	k8xC
nabídek	nabídka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
základní	základní	k2eAgNnSc1d1
menu	menu	k1gNnSc1
lze	lze	k6eAd1
pak	pak	k6eAd1
vyvolat	vyvolat	k5eAaPmF
kliknutím	kliknutí	k1gNnSc7
na	na	k7c4
ikonku	ikonka	k1gFnSc4
Chrome	chromat	k5eAaImIp3nS
v	v	k7c6
záhlaví	záhlaví	k1gNnSc6
rámce	rámec	k1gInSc2
stránky	stránka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Not	nota	k1gFnPc2
Track	Tracka	k1gFnPc2
</s>
<s>
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
říká	říkat	k5eAaImIp3nS
webům	web	k1gInPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nesledovali	sledovat	k5eNaImAgMnP
a	a	k8xC
nesbírali	sbírat	k5eNaImAgMnP
o	o	k7c6
uživatelích	uživatel	k1gMnPc6
informace	informace	k1gFnSc2
např.	např.	kA
k	k	k7c3
posílání	posílání	k1gNnSc3
cílené	cílený	k2eAgFnSc2d1
reklamy	reklama	k1gFnSc2
do	do	k7c2
prohlížeče	prohlížeč	k1gInSc2
<g/>
,	,	kIx,
či	či	k8xC
poskytování	poskytování	k1gNnSc1
údajů	údaj	k1gInPc2
třetím	třetí	k4xOgFnPc3
stranám	strana	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
Do	do	k7c2
Not	nota	k1gFnPc2
Track	Tracka	k1gFnPc2
většina	většina	k1gFnSc1
webů	web	k1gInPc2
ignoruje	ignorovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
obsahuje	obsahovat	k5eAaImIp3nS
od	od	k7c2
verze	verze	k1gFnSc2
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Správce	správce	k1gMnSc1
úloh	úloha	k1gFnPc2
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
monitor	monitor	k1gInSc4
všech	všecek	k3xTgInPc2
aktivních	aktivní	k2eAgInPc2d1
procesů	proces	k1gInPc2
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udává	udávat	k5eAaImIp3nS
údaje	údaj	k1gInPc4
o	o	k7c6
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
rozšířeních	rozšíření	k1gNnPc6
<g/>
,	,	kIx,
pluginech	plugin	k1gInPc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc1
obsazení	obsazení	k1gNnSc1
v	v	k7c6
paměti	paměť	k1gFnSc6
<g/>
,	,	kIx,
zatížení	zatížení	k1gNnSc1
cpu	cpát	k5eAaImIp1nS
<g/>
,	,	kIx,
gpu	gpu	k?
atd.	atd.	kA
Pomocí	pomocí	k7c2
správce	správce	k1gMnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
zamrzlé	zamrzlý	k2eAgInPc4d1
procesy	proces	k1gInPc4
či	či	k8xC
ty	ten	k3xDgMnPc4
co	co	k9
spotřebovávají	spotřebovávat	k5eAaImIp3nP
moc	moc	k1gFnSc4
paměti	paměť	k1gFnSc2
i	i	k9
ukončit	ukončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
Menu	menu	k1gNnPc6
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
nebo	nebo	k8xC
lze	lze	k6eAd1
vyvolat	vyvolat	k5eAaPmF
pomocí	pomocí	k7c2
kláves	klávesa	k1gFnPc2
Shift	Shift	kA
<g/>
+	+	kIx~
<g/>
Esc	Esc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
barev	barva	k1gFnPc2
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
podporuje	podporovat	k5eAaImIp3nS
správu	správa	k1gFnSc4
barev	barva	k1gFnPc2
podle	podle	k7c2
specifikací	specifikace	k1gFnPc2
ICC	ICC	kA
v	v	k7c6
<g/>
2	#num#	k4
na	na	k7c6
všech	všecek	k3xTgFnPc6
platformách	platforma	k1gFnPc6
od	od	k7c2
verze	verze	k1gFnSc2
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
macOSu	macOSa	k1gFnSc4
i	i	k8xC
verzi	verze	k1gFnSc4
ICC	ICC	kA
v	v	k7c6
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Synchronizace	synchronizace	k1gFnSc1
</s>
<s>
Prohlížeč	prohlížeč	k1gMnSc1
umožňuje	umožňovat	k5eAaImIp3nS
synchronizovat	synchronizovat	k5eAaBmF
uživatelům	uživatel	k1gMnPc3
své	své	k1gNnSc4
záložky	záložka	k1gFnSc2
<g/>
,	,	kIx,
nastavení	nastavení	k1gNnSc2
a	a	k8xC
historie	historie	k1gFnSc2
ve	v	k7c6
všech	všecek	k3xTgNnPc6
zařízeních	zařízení	k1gNnPc6
a	a	k8xC
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
prostřednictvím	prostřednictvím	k7c2
účtu	účet	k1gInSc2
!!!	!!!	k?
<g/>
Google	Google	k1gInSc1
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
tam	tam	k6eAd1
chyba	chyba	k1gFnSc1
již	již	k6eAd1
nejde	jít	k5eNaImIp3nS
synchronizace	synchronizace	k1gFnSc1
na	na	k7c6
internetu	internet	k1gInSc6
je	být	k5eAaImIp3nS
spousty	spousta	k1gFnPc4
dotazů	dotaz	k1gInPc2
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
a	a	k8xC
nikdo	nikdo	k3yNnSc1
si	se	k3xPyFc3
s	s	k7c7
tím	ten	k3xDgNnSc7
neví	vědět	k5eNaImIp3nS
rady	rada	k1gFnPc4
na	na	k7c6
podpoře	podpora	k1gFnSc6
mlčí	mlčet	k5eAaImIp3nS
!!!!!	!!!!!	k?
</s>
<s>
Centrum	centrum	k1gNnSc1
nápovědy	nápověda	k1gFnSc2
</s>
<s>
Prohlížeč	prohlížeč	k1gMnSc1
nemá	mít	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
klasickou	klasický	k2eAgFnSc4d1
offline	offlin	k1gInSc5
nápovědu	nápověda	k1gFnSc4
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
tzv.	tzv.	kA
on-line	on-lin	k1gInSc5
nápovědu	nápověda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mimo	mimo	k6eAd1
jiné	jiný	k2eAgNnSc1d1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
lze	lze	k6eAd1
spustit	spustit	k5eAaPmF
<g/>
,	,	kIx,
až	až	k8xS
když	když	k8xS
je	být	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
síti	síť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
handicap	handicap	k1gInSc1
vynahrazuje	vynahrazovat	k5eAaImIp3nS
bohatou	bohatý	k2eAgFnSc7d1
databází	databáze	k1gFnSc7
nápověd	nápověda	k1gFnPc2
<g/>
,	,	kIx,
rad	rada	k1gFnPc2
a	a	k8xC
řešení	řešení	k1gNnPc2
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kanály	kanál	k1gInPc1
a	a	k8xC
aktualizace	aktualizace	k1gFnSc1
vydávání	vydávání	k1gNnSc2
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
vydávaní	vydávaný	k2eAgMnPc1d1
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
tři	tři	k4xCgInPc1
kanály	kanál	k1gInPc1
relací	relace	k1gFnPc2
<g/>
,	,	kIx,
dev	dev	k?
<g/>
,	,	kIx,
beta	beta	k1gNnSc1
a	a	k8xC
stable	stable	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
už	už	k9
názvy	název	k1gInPc1
napovídají	napovídat	k5eAaBmIp3nP
<g/>
,	,	kIx,
dev	dev	k?
kanál	kanál	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
relace	relace	k1gFnPc4
ryze	ryze	k6eAd1
experimentální	experimentální	k2eAgNnSc1d1
a	a	k8xC
nové	nový	k2eAgNnSc1d1
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
funkcí	funkce	k1gFnPc2
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
testováno	testovat	k5eAaImNgNnS
a	a	k8xC
jen	jen	k9
hrstka	hrstka	k1gFnSc1
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
uznáno	uznán	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
funkční	funkční	k2eAgNnSc1d1
a	a	k8xC
praktické	praktický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
funkce	funkce	k1gFnPc1
dostávají	dostávat	k5eAaImIp3nP
i	i	k9
do	do	k7c2
beta	beta	k1gNnSc2
relace	relace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
podrobena	podrobit	k5eAaPmNgFnS
širokému	široký	k2eAgNnSc3d1
testování	testování	k1gNnSc3
a	a	k8xC
následně	následně	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
projeví	projevit	k5eAaPmIp3nS
jako	jako	k9
stabilní	stabilní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
překlopeny	překlopit	k5eAaPmNgFnP
do	do	k7c2
stable	stable	k6eAd1
verze	verze	k1gFnSc1
prohlížeče	prohlížeč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
celého	celý	k2eAgInSc2d1
procesu	proces	k1gInSc2
je	být	k5eAaImIp3nS
proměnlivá	proměnlivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
kostce	kostka	k1gFnSc6
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
dev	dev	k?
verze	verze	k1gFnPc1
objevují	objevovat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
jednou	jeden	k4xCgFnSc7
až	až	k6eAd1
dvakrát	dvakrát	k6eAd1
týdně	týdně	k6eAd1
<g/>
,	,	kIx,
beta	beta	k1gNnSc1
a	a	k8xC
stable	stable	k6eAd1
verze	verze	k1gFnSc1
v	v	k7c6
průměru	průměr	k1gInSc6
jednou	jednou	k6eAd1
za	za	k7c4
6	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
beta	beta	k1gNnSc1
verze	verze	k1gFnSc2
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
stable	stable	k6eAd1
asi	asi	k9
o	o	k7c4
měsíc	měsíc	k1gInSc4
posunutá	posunutý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
těchto	tento	k3xDgInPc2
kanálů	kanál	k1gInPc2
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
ještě	ještě	k9
verze	verze	k1gFnSc1
canary	canara	k1gFnSc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
tzn.	tzn.	kA
noční	noční	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
aktuálnější	aktuální	k2eAgFnSc1d2
než	než	k8xS
verze	verze	k1gFnSc1
dev	dev	k?
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nainstalována	nainstalovat	k5eAaPmNgFnS
souběžně	souběžně	k6eAd1
s	s	k7c7
verzí	verze	k1gFnSc7
stable	stable	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
výchozím	výchozí	k2eAgInSc7d1
prohlížečem	prohlížeč	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmostí	samozřejmost	k1gFnPc2
pak	pak	k6eAd1
jsou	být	k5eAaImIp3nP
občasné	občasný	k2eAgFnPc1d1
chyby	chyba	k1gFnPc1
a	a	k8xC
zamrzání	zamrzání	k1gNnSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
ale	ale	k8xC
to	ten	k3xDgNnSc1
k	k	k7c3
vývojové	vývojový	k2eAgFnSc3d1
verzi	verze	k1gFnSc3
tak	tak	k6eAd1
nějak	nějak	k6eAd1
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizuje	aktualizovat	k5eAaBmIp3nS
se	se	k3xPyFc4
většinou	většinou	k6eAd1
každou	každý	k3xTgFnSc4
noc	noc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Automatické	automatický	k2eAgFnPc1d1
aktualizace	aktualizace	k1gFnPc1
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
obsahuje	obsahovat	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
automatické	automatický	k2eAgFnSc2d1
aktualizace	aktualizace	k1gFnSc2
prohlížeče	prohlížeč	k1gMnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
program	program	k1gInSc1
zjisti	zjistit	k5eAaPmRp2nS
novou	nový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
je	být	k5eAaImIp3nS
ihned	ihned	k6eAd1
stažena	stáhnout	k5eAaPmNgFnS
a	a	k8xC
po	po	k7c6
restartu	restart	k1gInSc6
ihned	ihned	k6eAd1
funkční	funkční	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnPc4
má	mít	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jste	být	k5eAaImIp2nP
neustále	neustále	k6eAd1
chráněni	chránit	k5eAaImNgMnP
nejnovějšími	nový	k2eAgInPc7d3
aktualizacemi	aktualizace	k1gFnPc7
zabezpečení	zabezpečení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
na	na	k7c6
pozadí	pozadí	k1gNnSc6
a	a	k8xC
uživatel	uživatel	k1gMnSc1
není	být	k5eNaImIp3nS
tímto	tento	k3xDgInSc7
procesem	proces	k1gInSc7
nějak	nějak	k6eAd1
rušen	rušit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizaci	aktualizace	k1gFnSc4
lze	lze	k6eAd1
zkontrolovat	zkontrolovat	k5eAaPmF
i	i	k9
ručně	ručně	k6eAd1
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
Menu	menu	k1gNnSc6
vybrat	vybrat	k5eAaPmF
možnost	možnost	k1gFnSc4
O	o	k7c6
aplikaci	aplikace	k1gFnSc6
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
Web	web	k1gInSc1
Store	Stor	k1gInSc5
</s>
<s>
Prohlížeč	prohlížeč	k1gMnSc1
Chrome	chromat	k5eAaImIp3nS
od	od	k7c2
své	svůj	k3xOyFgFnSc2
9	#num#	k4
<g/>
.	.	kIx.
verze	verze	k1gFnSc1
může	moct	k5eAaImIp3nS
využívat	využívat	k5eAaPmF,k5eAaImF
i	i	k9
Internetový	internetový	k2eAgInSc4d1
obchod	obchod	k1gInSc4
Chrome	chromat	k5eAaImIp3nS
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
lze	lze	k6eAd1
zdarma	zdarma	k6eAd1
i	i	k9
za	za	k7c4
peníze	peníz	k1gInPc4
stáhnout	stáhnout	k5eAaPmF
nové	nový	k2eAgInPc4d1
motivy	motiv	k1gInPc4
vzhledu	vzhled	k1gInSc2
<g/>
,	,	kIx,
rozšíření	rozšíření	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
například	například	k6eAd1
i	i	k9
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
pro	pro	k7c4
firemní	firemní	k2eAgNnSc4d1
použití	použití	k1gNnSc4
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
uvolněna	uvolnit	k5eAaPmNgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
teprve	teprve	k6eAd1
od	od	k7c2
verze	verze	k1gFnSc2
24	#num#	k4
je	být	k5eAaImIp3nS
Chrome	chromat	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
připraven	připravit	k5eAaPmNgMnS
k	k	k7c3
plnohodnotnému	plnohodnotný	k2eAgNnSc3d1
využívání	využívání	k1gNnSc3
ve	v	k7c6
firemní	firemní	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
instalační	instalační	k2eAgInPc1d1
balíčky	balíček	k1gInPc1
MSI	MSI	kA
jak	jak	k8xC,k8xS
pro	pro	k7c4
32	#num#	k4
tak	tak	k6eAd1
i	i	k8xC
64	#num#	k4
<g/>
bitovou	bitový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
systému	systém	k1gInSc2
Windows	Windows	kA
(	(	kIx(
<g/>
pro	pro	k7c4
ostatní	ostatní	k2eAgInPc4d1
systémy	systém	k1gInPc4
se	se	k3xPyFc4
instaluje	instalovat	k5eAaBmIp3nS
klasická	klasický	k2eAgFnSc1d1
instalace	instalace	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
než	než	k8xS
u	u	k7c2
prohlížeče	prohlížeč	k1gInSc2
pro	pro	k7c4
normální	normální	k2eAgFnSc4d1
uživatelé	uživatel	k1gMnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
cloudové	cloudový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
správy	správa	k1gFnPc4
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
100	#num#	k4
zásad	zásada	k1gFnPc2
zabezpečení	zabezpečení	k1gNnSc2
a	a	k8xC
různých	různý	k2eAgNnPc2d1
nastavení	nastavení	k1gNnPc2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
pro	pro	k7c4
mobilní	mobilní	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
Android	android	k1gInSc1
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
pro	pro	k7c4
mobilní	mobilní	k2eAgInSc4d1
systém	systém	k1gInSc4
Android	android	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založená	založený	k2eAgFnSc1d1
na	na	k7c4
desktop	desktop	k1gInSc4
verzi	verze	k1gFnSc4
Chrome	chromat	k5eAaImIp3nS
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovala	obsahovat	k5eAaImAgFnS
podporu	podpora	k1gFnSc4
Google	Google	k1gFnSc2
účtů	účet	k1gInPc2
<g/>
,	,	kIx,
synchronizaci	synchronizace	k1gFnSc4
<g/>
,	,	kIx,
anonymní	anonymní	k2eAgInSc4d1
režim	režim	k1gInSc4
či	či	k8xC
předvyplněné	předvyplněný	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
do	do	k7c2
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
YouTube	YouTub	k1gInSc5
<g/>
,	,	kIx,
Mail	mail	k1gInSc1
a	a	k8xC
Analytics	Analytics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
18	#num#	k4
první	první	k4xOgFnSc2
stabilní	stabilní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
Android	android	k1gInSc4
4.0	4.0	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
již	již	k6eAd1
i	i	k9
češtinu	čeština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
v	v	k7c6
září	září	k1gNnSc6
přichází	přicházet	k5eAaImIp3nS
i	i	k9
podpora	podpora	k1gFnSc1
pro	pro	k7c4
mobilní	mobilní	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
Intel	Intel	kA
x	x	k?
<g/>
86	#num#	k4
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
podpora	podpora	k1gFnSc1
pro	pro	k7c4
Nexus	nexus	k1gInSc4
4	#num#	k4
a	a	k8xC
Nexus	nexus	k1gInSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
25	#num#	k4
vylepšení	vylepšení	k1gNnPc2
výkonu	výkon	k1gInSc2
JavaScriptu	JavaScript	k1gInSc2
<g/>
,	,	kIx,
HTML	HTML	kA
5	#num#	k4
a	a	k8xC
audia	audius	k1gMnSc4
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
V8	V8	k1gMnSc1
JavaScript	JavaScript	k1gMnSc1
engine	enginout	k5eAaPmIp3nS
<g/>
,	,	kIx,
oprava	oprava	k1gFnSc1
zoomu	zoom	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
rolování	rolování	k1gNnSc1
stránek	stránka	k1gFnPc2
a	a	k8xC
zlepšení	zlepšení	k1gNnSc4
jasnosti	jasnost	k1gFnSc2
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
26	#num#	k4
update	update	k1gInSc1
auto	auto	k1gNnSc4
vyplňování	vyplňování	k1gNnSc1
<g/>
,	,	kIx,
synchronizace	synchronizace	k1gFnPc1
hesel	heslo	k1gNnPc2
<g/>
,	,	kIx,
oprava	oprava	k1gFnSc1
načítání	načítání	k1gNnSc2
prázdné	prázdný	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
27	#num#	k4
jednodušší	jednoduchý	k2eAgNnSc4d2
hledání	hledání	k1gNnSc4
z	z	k7c2
omniboxu	omnibox	k1gInSc2
<g/>
,	,	kIx,
vylepšení	vylepšení	k1gNnSc1
full	fulla	k1gFnPc2
screen	screna	k1gFnPc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
certifikátu	certifikát	k1gInSc2
na	na	k7c6
straně	strana	k1gFnSc6
klienta	klient	k1gMnSc2
<g/>
,	,	kIx,
přidána	přidán	k2eAgFnSc1d1
karta	karta	k1gFnSc1
historie	historie	k1gFnSc2
pro	pro	k7c4
tablety	tableta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
28	#num#	k4
podpora	podpora	k1gFnSc1
automatického	automatický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
,	,	kIx,
full	fulnout	k5eAaPmAgInS
screen	screen	k1gInSc1
pro	pro	k7c4
tablety	tableta	k1gFnPc4
<g/>
,	,	kIx,
nové	nový	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
jazyky	jazyk	k1gInPc4
atd.	atd.	kA
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
29	#num#	k4
přidána	přidán	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
těchto	tento	k3xDgNnPc2
rozhraní	rozhraní	k1gNnPc2
<g/>
:	:	kIx,
WebRTC	WebRTC	k1gMnPc2
API	API	kA
<g/>
,	,	kIx,
WebAudio	WebAudio	k1gNnSc1
API	API	kA
<g/>
,	,	kIx,
vizuální	vizuální	k2eAgFnSc1d1
indikace	indikace	k1gFnSc1
při	při	k7c6
listování	listování	k1gNnSc6
v	v	k7c6
horní	horní	k2eAgFnSc6d1
nebo	nebo	k8xC
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
stránky	stránka	k1gFnPc4
a	a	k8xC
nový	nový	k2eAgInSc4d1
dialog	dialog	k1gInSc4
pro	pro	k7c4
výběr	výběr	k1gInSc4
barvy	barva	k1gFnSc2
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
webové	webový	k2eAgInPc4d1
formuláře	formulář	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprava	oprava	k1gFnSc1
chybných	chybný	k2eAgInPc2d1
znaků	znak	k1gInPc2
na	na	k7c6
japonských	japonský	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
<g/>
,	,	kIx,
špatného	špatný	k2eAgInSc2d1
výběru	výběr	k1gInSc2
písma	písmo	k1gNnSc2
v	v	k7c6
některých	některý	k3yIgInPc6
android	android	k1gInSc1
zařízeních	zařízení	k1gNnPc6
a	a	k8xC
havárie	havárie	k1gFnSc1
WebAudia	WebAudium	k1gNnSc2
na	na	k7c6
WWW	WWW	kA
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
30	#num#	k4
aktualizace	aktualizace	k1gFnSc2
nových	nový	k2eAgFnPc2d1
gest	gesto	k1gNnPc2
<g/>
,	,	kIx,
rychlejší	rychlý	k2eAgNnSc1d2
vyhledávání	vyhledávání	k1gNnSc1
z	z	k7c2
přechodu	přechod	k1gInSc2
vyhledávání	vyhledávání	k1gNnSc2
Chrome	chromat	k5eAaImIp3nS
app	app	k?
<g/>
,	,	kIx,
zlepšení	zlepšení	k1gNnSc4
synchronizace	synchronizace	k1gFnSc2
hesel	heslo	k1gNnPc2
<g/>
,	,	kIx,
vyhledávání	vyhledávání	k1gNnSc1
obrázků	obrázek	k1gInPc2
na	na	k7c4
dlouhý	dlouhý	k2eAgInSc4d1
stisk	stisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprava	oprava	k1gFnSc1
pádu	pád	k1gInSc2
při	při	k7c6
změně	změna	k1gFnSc6
letního	letní	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
31	#num#	k4
přidána	přidán	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
tisku	tisk	k1gInSc2
přímo	přímo	k6eAd1
z	z	k7c2
prohlížeče	prohlížeč	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c4
Android	android	k1gInSc4
Kitkat	Kitkat	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vylepšené	vylepšený	k2eAgNnSc1d1
automatické	automatický	k2eAgNnSc1d1
vyplňování	vyplňování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
32	#num#	k4
snadnější	snadný	k2eAgInSc1d2
přidání	přidání	k1gNnPc2
zástupce	zástupce	k1gMnSc1
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
na	na	k7c4
domovskou	domovský	k2eAgFnSc4d1
obrazovku	obrazovka	k1gFnSc4
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc4
objemu	objem	k1gInSc2
dat	datum	k1gNnPc2
v	v	k7c6
prohlížeči	prohlížeč	k1gInSc6
až	až	k9
o	o	k7c4
50	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
33	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
vylepšenou	vylepšený	k2eAgFnSc4d1
nápovědu	nápověda	k1gFnSc4
<g/>
,	,	kIx,
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
UI	UI	kA
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
pro	pro	k7c4
tag	tag	k1gInSc4
<datalist>
a	a	k8xC
opravu	oprava	k1gFnSc4
chyb	chyba	k1gFnPc2
paměti	paměť	k1gFnSc2
v	v	k7c6
javascriptu	javascript	k1gInSc6
V	v	k7c4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
GPU	GPU	kA
bufferu	buffer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
34	#num#	k4
oprava	oprava	k1gFnSc1
crash	crasha	k1gFnPc2
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
optimalizace	optimalizace	k1gFnPc1
výkonu	výkon	k1gInSc2
baterie	baterie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
upravený	upravený	k2eAgInSc4d1
a	a	k8xC
optimalizovaný	optimalizovaný	k2eAgInSc4d1
posuvník	posuvník	k1gInSc4
pro	pro	k7c4
Metro	metro	k1gNnSc4
styl	styl	k1gInSc1
a	a	k8xC
Desktop	desktop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
35	#num#	k4
přidána	přidán	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
Fullscreen	Fullscreen	k2eAgInSc1d1
videa	video	k1gNnSc2
s	s	k7c7
titulky	titulek	k1gInPc7
a	a	k8xC
ovládacími	ovládací	k2eAgInPc7d1
prvky	prvek	k1gInPc7
HTML	HTML	kA
5	#num#	k4
<g/>
,	,	kIx,
vracena	vracen	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
Tab	tab	kA
Close	Close	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
36	#num#	k4
vylepšené	vylepšený	k2eAgInPc1d1
vykreslování	vykreslování	k1gNnSc4
textu	text	k1gInSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
neoptimalizovaných	optimalizovaný	k2eNgFnPc6d1
pro	pro	k7c4
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
37	#num#	k4
vylepšení	vylepšení	k1gNnPc2
vzhledu	vzhled	k1gInSc2
prohlížeče	prohlížeč	k1gMnSc2
a	a	k8xC
přihlašování	přihlašování	k1gNnSc1
do	do	k7c2
Chrome	chrom	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
38	#num#	k4
podpora	podpora	k1gFnSc1
stavu	stav	k1gInSc2
baterie	baterie	k1gFnSc1
a	a	k8xC
orientace	orientace	k1gFnSc1
obrazovky	obrazovka	k1gFnSc2
API	API	kA
<g/>
,	,	kIx,
plus	plus	k6eAd1
další	další	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
vzhledu	vzhled	k1gInSc2
prohlížeče	prohlížeč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
39	#num#	k4
oprava	oprava	k1gFnSc1
mnoha	mnoho	k4c2
chyb	chyba	k1gFnPc2
a	a	k8xC
výkonové	výkonový	k2eAgFnSc2d1
optimalizace	optimalizace	k1gFnSc2
<g/>
,	,	kIx,
experimentální	experimentální	k2eAgInSc1d1
režim	režim	k1gInSc1
Reader	Readra	k1gFnPc2
Mode	modus	k1gInSc5
(	(	kIx(
<g/>
zapíná	zapínat	k5eAaImIp3nS
se	se	k3xPyFc4
přes	přes	k7c4
chrome	chromat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
//	//	k?
<g/>
flags	flags	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
této	tento	k3xDgFnSc6
verzi	verze	k1gFnSc6
nedostupný	dostupný	k2eNgInSc4d1
pro	pro	k7c4
tablety	tableta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
40	#num#	k4
vylepšená	vylepšený	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
informace	informace	k1gFnSc2
o	o	k7c6
stránce	stránka	k1gFnSc6
(	(	kIx(
<g/>
page	pagat	k5eAaPmIp3nS
info	info	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
pro	pro	k7c4
mobilní	mobilní	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
iOS	iOS	k?
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2012	#num#	k4
–	–	k?
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
první	první	k4xOgFnSc1
stabilní	stabilní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
19.0	19.0	k4
<g/>
.1084	.1084	k4
<g/>
.60	.60	k4
pro	pro	k7c4
iOS	iOS	k?
4.3	4.3	k4
a	a	k8xC
novější	nový	k2eAgFnSc1d2
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
na	na	k7c4
desktop	desktop	k1gInSc4
verzi	verze	k1gFnSc4
19.0	19.0	k4
<g/>
.1084	.1084	k4
<g/>
.52	.52	k4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
21	#num#	k4
podpora	podpora	k1gFnSc1
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
5	#num#	k4
<g/>
,	,	kIx,
plus	plus	k6eAd1
oprava	oprava	k1gFnSc1
Gmail	Gmaila	k1gFnPc2
pro	pro	k7c4
iOS	iOS	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
23	#num#	k4
oprava	oprava	k1gFnSc1
problému	problém	k1gInSc2
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
zvuku	zvuk	k1gInSc2
v	v	k7c6
prohlížeči	prohlížeč	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
25	#num#	k4
vylepšený	vylepšený	k2eAgInSc1d1
přístup	přístup	k1gInSc1
stránek	stránka	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
pomocí	pomocí	k7c2
tlačítka	tlačítko	k1gNnSc2
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
nové	nový	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
pro	pro	k7c4
vyhledávání	vyhledávání	k1gNnSc4
přímo	přímo	k6eAd1
v	v	k7c6
omniboxu	omnibox	k1gInSc6
a	a	k8xC
přehlednější	přehlední	k2eAgMnSc1d2
zobrazovaní	zobrazovaný	k2eAgMnPc1d1
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
přidáno	přidán	k2eAgNnSc4d1
sdílení	sdílení	k1gNnSc4
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
26	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
aktualizace	aktualizace	k1gFnSc1
fullscreen	fullscrena	k1gFnPc2
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
a	a	k8xC
iPod	iPod	k1gMnSc1
Touch	Touch	k1gMnSc1
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
pro	pro	k7c4
Google	Google	k1gInSc4
Cloud	Cloud	k1gMnSc1
Print	Print	k1gMnSc1
<g/>
,	,	kIx,
Air	Air	k1gMnSc1
Print	Print	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
27	#num#	k4
vylepšené	vylepšený	k2eAgNnSc4d1
hlasové	hlasový	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
<g/>
,	,	kIx,
rychlejší	rychlý	k2eAgNnSc1d2
načítání	načítání	k1gNnSc1
stránek	stránka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
28	#num#	k4
zlepšená	zlepšený	k2eAgFnSc1d1
kompatibilita	kompatibilita	k1gFnSc1
s	s	k7c7
Google	Google	k1gNnSc7
Apps	Appsa	k1gFnPc2
<g/>
,	,	kIx,
hlasové	hlasový	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
<g/>
,	,	kIx,
full	fulnout	k5eAaPmAgInS
screen	screen	k1gInSc1
<g/>
,	,	kIx,
spora	spora	k1gFnSc1
přenosu	přenos	k1gInSc2
dat	datum	k1gNnPc2
a	a	k8xC
přehrávání	přehrávání	k1gNnSc6
videa	video	k1gNnSc2
na	na	k7c6
iOS	iOS	k?
5.1	5.1	k4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
29	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
nových	nový	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
např.	např.	kA
zobrazení	zobrazení	k1gNnSc2
datové	datový	k2eAgFnSc2d1
úspory	úspora	k1gFnSc2
v	v	k7c6
nastavení	nastavení	k1gNnSc6
řízení	řízení	k1gNnSc2
šířky	šířka	k1gFnSc2
pásma	pásmo	k1gNnSc2
<g/>
,	,	kIx,
vylepšené	vylepšený	k2eAgNnSc4d1
hlasové	hlasový	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
a	a	k8xC
také	také	k9
textové	textový	k2eAgNnSc4d1
vyhledávání	vyhledávání	k1gNnSc4
s	s	k7c7
funkcí	funkce	k1gFnSc7
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
30	#num#	k4
nový	nový	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
a	a	k8xC
funkce	funkce	k1gFnSc1
pro	pro	k7c4
iPhone	iPhon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlepšení	zlepšení	k1gNnSc1
Fullscreen	Fullscrena	k1gFnPc2
pro	pro	k7c4
iPad	iPad	k1gInSc4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
iOS	iOS	k?
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc6
nastavení	nastavení	k1gNnSc1
UI	UI	kA
+	+	kIx~
oprava	oprava	k1gFnSc1
mnoha	mnoho	k4c2
chyb	chyba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
minimálních	minimální	k2eAgInPc2d1
požadavků	požadavek	k1gInPc2
na	na	k7c4
systém	systém	k1gInSc4
<g/>
,	,	kIx,
iOS	iOS	k?
6	#num#	k4
a	a	k8xC
vyšší	vysoký	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
31	#num#	k4
vylepšeno	vylepšit	k5eAaPmNgNnS
automatické	automatický	k2eAgNnSc1d1
vyplňování	vyplňování	k1gNnSc1
<g/>
,	,	kIx,
přidán	přidán	k2eAgInSc1d1
dlouhý	dlouhý	k2eAgInSc1d1
stisk	stisk	k1gInSc1
na	na	k7c4
obrázek	obrázek	k1gInSc4
pro	pro	k7c4
vyhledávání	vyhledávání	k1gNnSc4
souvisejících	související	k2eAgInPc2d1
obrázků	obrázek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
32	#num#	k4
nově	nova	k1gFnSc3
funkce	funkce	k1gFnSc2
automatického	automatický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
známou	známá	k1gFnSc4
z	z	k7c2
desktop	desktop	k1gInSc4
Chrome	chromat	k5eAaImIp3nS
<g/>
,	,	kIx,
možnost	možnost	k1gFnSc1
nastavení	nastavení	k1gNnSc2
snížení	snížení	k1gNnSc2
úspory	úspora	k1gFnSc2
dat	datum	k1gNnPc2
až	až	k6eAd1
o	o	k7c6
50	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
rychlejší	rychlý	k2eAgNnSc4d2
a	a	k8xC
jednodušší	jednoduchý	k2eAgNnSc4d2
vyhledávání	vyhledávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
34	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
první	první	k4xOgNnSc4
spuštění	spuštění	k1gNnSc4
aplikace	aplikace	k1gFnSc2
<g/>
,	,	kIx,
vylepšení	vylepšení	k1gNnSc1
automatického	automatický	k2eAgNnSc2d1
doplňování	doplňování	k1gNnSc2
v	v	k7c6
omniboxu	omnibox	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
35	#num#	k4
podpora	podpora	k1gFnSc1
v	v	k7c6
omniboxu	omnibox	k1gInSc6
pro	pro	k7c4
arabštinu	arabština	k1gFnSc4
a	a	k8xC
hebrejštinu	hebrejština	k1gFnSc4
<g/>
,	,	kIx,
nově	nově	k6eAd1
v	v	k7c6
omniboxu	omnibox	k1gInSc6
podpora	podpora	k1gFnSc1
vyhledávání	vyhledávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
38	#num#	k4
přidána	přidán	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
ke	k	k7c3
stažení	stažení	k1gNnSc3
a	a	k8xC
otevření	otevření	k1gNnSc3
souborů	soubor	k1gInPc2
v	v	k7c6
Google	Googla	k1gFnSc6
Drive	drive	k1gInSc4
<g/>
,	,	kIx,
vylepšení	vylepšení	k1gNnSc4
podpory	podpora	k1gFnSc2
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
6	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Chrome	chromat	k5eAaImIp3nS
40	#num#	k4
nový	nový	k2eAgInSc1d1
hutnější	hutný	k2eAgInSc1d2
vzhled	vzhled	k1gInSc1
(	(	kIx(
<g/>
Material	Material	k1gInSc1
Design	design	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
optimalizace	optimalizace	k1gFnSc1
iOS	iOS	k?
8	#num#	k4
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
pro	pro	k7c4
větší	veliký	k2eAgInPc4d2
telefony	telefon	k1gInPc4
iPhone	iPhon	k1gInSc5
6	#num#	k4
Plus	plus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chromium	Chromium	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Chromium	Chromium	k1gNnSc1
(	(	kIx(
<g/>
webový	webový	k2eAgMnSc1d1
prohlížeč	prohlížeč	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chromium	Chromium	k1gNnSc1
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc1d1
a	a	k8xC
svobodný	svobodný	k2eAgInSc1d1
projekt	projekt	k1gInSc1
stojící	stojící	k2eAgInSc1d1
za	za	k7c4
Google	Google	k1gInSc4
Chromem	chrom	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
vydávaný	vydávaný	k2eAgInSc1d1
pod	pod	k7c7
svobodnou	svobodný	k2eAgFnSc7d1
licencí	licence	k1gFnSc7
BSD	BSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkčně	funkčně	k6eAd1
se	se	k3xPyFc4
takřka	takřka	k6eAd1
s	s	k7c7
Chromem	chrom	k1gInSc7
shoduje	shodovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
používá	používat	k5eAaImIp3nS
odlišný	odlišný	k2eAgInSc4d1
design	design	k1gInSc4
a	a	k8xC
logo	logo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
absenci	absence	k1gFnSc6
vestavěného	vestavěný	k2eAgInSc2d1
Flash	Flash	k1gInSc1
playeru	player	k1gMnSc3
<g/>
,	,	kIx,
automatických	automatický	k2eAgFnPc6d1
aktualizacích	aktualizace	k1gFnPc6
a	a	k8xC
RLZ	RLZ	kA
knihovny	knihovna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Neoficiální	oficiální	k2eNgFnSc1d1,k2eAgFnSc1d1
verze	verze	k1gFnSc1
Chromia	Chromium	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
vydala	vydat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
CodeWeavers	CodeWeavers	k1gInSc4
neoficiální	neoficiální	k2eAgInSc1d1,k2eNgInSc1d1
balík	balík	k1gInSc1
derivátů	derivát	k1gInPc2
pro	pro	k7c4
Wine	Wine	k1gFnSc4
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
vývojového	vývojový	k2eAgNnSc2d1
sestavení	sestavení	k1gNnSc2
Chromia	Chromium	k1gNnSc2
pro	pro	k7c4
systémy	systém	k1gInPc4
Linux	Linux	kA
a	a	k8xC
macOS	macOS	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nazvala	nazvat	k5eAaPmAgFnS,k5eAaBmAgFnS
CrossOver	CrossOvra	k1gFnPc2
Chromium	Chromium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
SRWare	SRWar	k1gMnSc5
Iron	iron	k1gInSc1
je	on	k3xPp3gNnPc4
vydání	vydání	k1gNnPc4
Chromia	Chromium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
odstraňuje	odstraňovat	k5eAaImIp3nS
shromažďování	shromažďování	k1gNnSc1
a	a	k8xC
posílání	posílání	k1gNnSc1
některých	některý	k3yIgFnPc2
informací	informace	k1gFnPc2
Googlu	Googl	k1gInSc2
ke	k	k7c3
zpracování	zpracování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
také	také	k9
navíc	navíc	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
filtrování	filtrování	k1gNnSc4
reklam	reklama	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ChromePlus	ChromePlus	k1gMnSc1
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nástavbu	nástavba	k1gFnSc4
Chromia	Chromium	k1gNnSc2
<g/>
,	,	kIx,
mimo	mimo	k6eAd1
vždy	vždy	k6eAd1
novější	nový	k2eAgFnSc1d2
verze	verze	k1gFnSc1
prohlížeče	prohlížeč	k1gMnSc2
obsahuje	obsahovat	k5eAaImIp3nS
navíc	navíc	k6eAd1
i	i	k9
IE	IE	kA
tab	tab	kA
<g/>
,	,	kIx,
ovládání	ovládání	k1gNnSc4
prohlížeče	prohlížeč	k1gMnSc2
pomocí	pomocí	k7c2
gest	gesto	k1gNnPc2
myši	myš	k1gFnSc2
<g/>
,	,	kIx,
Super	super	k2eAgInSc4d1
drag	drag	k1gInSc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
download	download	k6eAd1
manažerů	manažer	k1gMnPc2
atd.	atd.	kA
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
</s>
<s>
První	první	k4xOgFnSc1
oficiální	oficiální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
byla	být	k5eAaImAgFnS
představena	představen	k2eAgFnSc1d1
dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
v	v	k7c6
San	San	k1gFnSc6
Francisku	Francisek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
prohlížeči	prohlížeč	k1gInSc6
Chrome	chromat	k5eAaImIp3nS
a	a	k8xC
značně	značně	k6eAd1
redukovaném	redukovaný	k2eAgInSc6d1
Linuxu	linux	k1gInSc6
<g/>
,	,	kIx,
z	z	k7c2
většiny	většina	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
Google	Google	k1gFnSc1
on-line	on-lin	k1gInSc5
aplikace	aplikace	k1gFnPc4
a	a	k8xC
cloudové	cloudový	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
šířen	šířit	k5eAaImNgInS
pod	pod	k7c7
licencí	licence	k1gFnPc2
GPL	GPL	kA
verze	verze	k1gFnSc1
2	#num#	k4
a	a	k8xC
běží	běžet	k5eAaImIp3nS
jak	jak	k6eAd1
na	na	k7c6
platformě	platforma	k1gFnSc6
x	x	k?
<g/>
86	#num#	k4
tak	tak	k8xS,k8xC
i	i	k9
ARM	ARM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
jednoduchost	jednoduchost	k1gFnSc4
a	a	k8xC
minimální	minimální	k2eAgInPc4d1
hardwarové	hardwarový	k2eAgInPc4d1
nároky	nárok	k1gInPc4
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
netbooky	netbook	k1gInPc4
a	a	k8xC
mobilní	mobilní	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
s	s	k7c7
přístupem	přístup	k1gInSc7
na	na	k7c4
internet	internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
malé	malý	k2eAgInPc4d1
notebooky	notebook	k1gInPc4
se	se	k3xPyFc4
ujalo	ujmout	k5eAaPmAgNnS
na	na	k7c6
trhu	trh	k1gInSc6
označení	označení	k1gNnSc2
Chromebook	Chromebook	k1gInSc1
a	a	k8xC
jsou	být	k5eAaImIp3nP
vyráběny	vyrábět	k5eAaImNgFnP
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
výrobců	výrobce	k1gMnPc2
od	od	k7c2
ASUSu	ASUSus	k1gInSc2
<g/>
,	,	kIx,
Aceru	Acer	k1gInSc2
<g/>
,	,	kIx,
Samsungu	Samsung	k1gInSc2
<g/>
,	,	kIx,
DELLu	DELLus	k1gInSc2
<g/>
,	,	kIx,
HP	HP	kA
až	až	k9
po	po	k7c6
např.	např.	kA
Lenovo	Lenovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
(	(	kIx(
<g/>
desktop	desktop	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
1.0	1.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
2.0	2.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
3.0	3.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
4.0	4.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
5.0	5.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
6.0	6.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
7.0	7.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
8.0	8.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
9.0	9.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
10.0	10.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
11.0	11.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
12.0	12.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
13.0	13.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
14.0	14.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
15.0	15.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
16.0	16.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
17.0	17.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
18.0	18.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
19.0	19.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
20.0	20.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
21.0	21.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
22.0	22.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
23.0	23.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
24.0	24.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
25.0	25.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
26.0	26.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
27.0	27.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
28.0	28.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
29.0	29.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
30.0	30.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
31.0	31.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
32.0	32.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
33.0	33.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
34.0	34.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
35.0	35.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
36.0	36.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
37.0	37.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
38.0	38.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
39.0	39.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
40.0	40.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
41.0	41.0	k4
dev	dev	k?
a	a	k8xC
beta	beta	k1gNnSc1
verze	verze	k1gFnSc2
–	–	k?
41.0	41.0	k4
<g/>
.2272	.2272	k4
<g/>
.35	.35	k4
Beta	beta	k1gNnPc2
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
42.0	42.0	k4
dev	dev	k?
a	a	k8xC
beta	beta	k1gNnSc1
verze	verze	k1gFnSc2
–	–	k?
42.0	42.0	k4
<g/>
.2288	.2288	k4
<g/>
.8	.8	k4
Dev	Dev	k1gFnPc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Google	Google	k6eAd1
Chrome	chromat	k5eAaImIp3nS
–	–	k?
43.0	43.0	k4
vydána	vydán	k2eAgFnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
JavaScriptu	JavaScript	k1gInSc2
</s>
<s>
Jíž	jenž	k3xRgFnSc2
od	od	k7c2
prvních	první	k4xOgFnPc2
verzí	verze	k1gFnPc2
vyniká	vynikat	k5eAaImIp3nS
prohlížeč	prohlížeč	k1gInSc4
velkou	velký	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
interpretace	interpretace	k1gFnSc2
javascriptového	javascriptový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
beta	beta	k1gNnSc2
verzí	verze	k1gFnPc2
se	se	k3xPyFc4
dokonce	dokonce	k9
umístila	umístit	k5eAaPmAgFnS
na	na	k7c6
webu	web	k1gInSc6
cnet	cneta	k1gFnPc2
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
před	před	k7c7
Firefoxem	Firefox	k1gInSc7
a	a	k8xC
IE	IE	kA
(	(	kIx(
<g/>
v	v	k7c6
testu	test	k1gInSc6
nebyla	být	k5eNaImAgFnS
ještě	ještě	k6eAd1
započínaná	započínaný	k2eAgFnSc1d1
Opera	opera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pří	přít	k5eAaImIp3nS
testování	testování	k1gNnSc1
JavaSriptu	JavaSript	k1gInSc2
Chrome	chromat	k5eAaImIp3nS
27	#num#	k4
s	s	k7c7
jádrem	jádro	k1gNnSc7
WebKit	WebKita	k1gFnPc2
na	na	k7c6
webu	web	k1gInSc6
TomsHardware	TomsHardwar	k1gMnSc5
skončil	skončit	k5eAaPmAgInS
prohlížeč	prohlížeč	k1gInSc4
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
(	(	kIx(
<g/>
nepočítá	počítat	k5eNaImIp3nS
se	se	k3xPyFc4
Opera	opera	k1gFnSc1
Next	Nexta	k1gFnPc2
<g/>
)	)	kIx)
za	za	k7c7
druhým	druhý	k4xOgInSc7
Firefoxem	Firefox	k1gInSc7
a	a	k8xC
první	první	k4xOgFnSc7
Operou	opera	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
Blink	blink	k0
dopadlo	dopadnout	k5eAaPmAgNnS
o	o	k7c4
poznání	poznání	k1gNnSc4
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
na	na	k7c6
blogu	blog	k1gInSc6
Midas	Midas	k1gMnSc1
proběhl	proběhnout	k5eAaPmAgInS
test	test	k1gInSc4
nejpoužívanějších	používaný	k2eAgMnPc2d3
prohlížečů	prohlížeč	k1gMnPc2
a	a	k8xC
Chrome	chromat	k5eAaImIp3nS
31	#num#	k4
skončil	skončit	k5eAaPmAgMnS
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
před	před	k7c7
druhou	druhý	k4xOgFnSc7
Operou	opera	k1gFnSc7
a	a	k8xC
třetím	třetí	k4xOgInSc7
Firefoxem	Firefox	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systémové	systémový	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
</s>
<s>
Doporučené	doporučený	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
optimální	optimální	k2eAgInSc4d1
chod	chod	k1gInSc4
Chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Windows	Windows	kA
<g/>
:	:	kIx,
XP	XP	kA
Service	Service	k1gFnSc1
Pack	Pack	k1gInSc1
2	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
Windows	Windows	kA
Vista	vista	k2eAgFnPc7d1
<g/>
,	,	kIx,
Windows	Windows	kA
7	#num#	k4
a	a	k8xC
Windows	Windows	kA
8	#num#	k4
<g/>
;	;	kIx,
Intel	Intel	kA
Pentium	Pentium	kA
4	#num#	k4
nebo	nebo	k8xC
novější	nový	k2eAgFnSc1d2
<g/>
;	;	kIx,
350	#num#	k4
MiB	MiB	k1gFnPc2
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
;	;	kIx,
512	#num#	k4
MiB	MiB	k1gFnSc7
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
</s>
<s>
macOS	macOS	k?
<g/>
:	:	kIx,
10.6	10.6	k4
nebo	nebo	k8xC
novější	nový	k2eAgFnSc2d2
<g/>
;	;	kIx,
Intel	Intel	kA
(	(	kIx(
<g/>
ne	ne	k9
PowerPC	PowerPC	k1gFnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
350	#num#	k4
MiB	MiB	k1gFnPc2
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
;	;	kIx,
512	#num#	k4
MiB	MiB	k1gFnSc7
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
</s>
<s>
Linux	Linux	kA
<g/>
:	:	kIx,
Ubuntu	Ubunt	k1gInSc2
12.04	12.04	k4
nebo	nebo	k8xC
novější	nový	k2eAgInSc4d2
<g/>
,	,	kIx,
Debian	Debian	k1gInSc4
7	#num#	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
,	,	kIx,
OpenSuse	OpenSuse	k1gFnSc1
12.2	12.2	k4
a	a	k8xC
novější	nový	k2eAgInSc4d2
<g/>
,	,	kIx,
Fedora	Fedora	k1gFnSc1
17	#num#	k4
<g/>
;	;	kIx,
Intel	Intel	kA
Pentium	Pentium	kA
4	#num#	k4
a	a	k8xC
novější	nový	k2eAgFnSc1d2
<g/>
;	;	kIx,
350	#num#	k4
MiB	MiB	k1gFnPc2
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
;	;	kIx,
512	#num#	k4
MiB	MiB	k1gFnSc7
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
pro	pro	k7c4
32	#num#	k4
<g/>
bitové	bitový	k2eAgFnSc2d1
Macy	Maca	k1gFnSc2
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2014	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
verze	verze	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
39	#num#	k4
podporuje	podporovat	k5eAaImIp3nS
64	#num#	k4
<g/>
bitovou	bitový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
macOS	macOS	k?
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
systému	systém	k1gInSc2
Windows	Windows	kA
XP	XP	kA
prodloužena	prodloužen	k2eAgMnSc4d1
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
35	#num#	k4
již	již	k6eAd1
nelze	lze	k6eNd1
spustit	spustit	k5eAaPmF
prohlížeč	prohlížeč	k1gInSc4
na	na	k7c6
počítačích	počítač	k1gInPc6
s	s	k7c7
procesory	procesor	k1gInPc7
Pentium	Pentium	kA
III	III	kA
<g/>
,	,	kIx,
Athlon	Athlon	k1gInSc1
<g/>
,	,	kIx,
Duron	Duron	k1gInSc1
<g/>
,	,	kIx,
Sempron	Sempron	k1gInSc1
<g/>
,	,	kIx,
Geode	Geod	k1gInSc5
a	a	k8xC
ostatních	ostatní	k1gNnPc2
bez	bez	k7c2
instrukcí	instrukce	k1gFnPc2
SSE	SSE	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Google	Google	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
často	často	k6eAd1
přibalen	přibalen	k2eAgInSc1d1
součástí	součást	k1gFnSc7
instalace	instalace	k1gFnSc1
jiných	jiný	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
méně	málo	k6eAd2
zkušených	zkušený	k2eAgMnPc2d1
uživatelům	uživatel	k1gMnPc3
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
do	do	k7c2
PC	PC	kA
bez	bez	k7c2
jejich	jejich	k3xOp3gNnSc2
vědomí	vědomí	k1gNnSc2
</s>
<s>
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
Firefoxem	Firefox	k1gInSc7
neobsahuje	obsahovat	k5eNaImIp3nS
tolik	tolik	k4yIc1,k4xDc1
doplňků	doplněk	k1gInPc2
</s>
<s>
je	být	k5eAaImIp3nS
náročný	náročný	k2eAgInSc1d1
na	na	k7c4
operační	operační	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
RAM	RAM	kA
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
načítá	načítat	k5eAaPmIp3nS,k5eAaBmIp3nS
stránky	stránka	k1gFnPc4
dopředu	dopředu	k6eAd1
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
mít	mít	k5eAaImF
aktuální	aktuální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
panel	panel	k1gInSc4
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
nový	nový	k2eAgInSc1d1
proces	proces	k1gInSc1
ve	v	k7c6
Správci	správce	k1gMnSc6
úloh	úloha	k1gFnPc2
</s>
<s>
první	první	k4xOgFnPc1
verze	verze	k1gFnPc1
byly	být	k5eAaImAgFnP
svižnější	svižný	k2eAgFnPc1d2
než	než	k8xS
ty	ten	k3xDgFnPc1
současné	současný	k2eAgFnPc1d1
</s>
<s>
propagace	propagace	k1gFnSc1
prohlížeče	prohlížeč	k1gMnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
kanály	kanál	k1gInPc4
–	–	k?
venkovní	venkovní	k2eAgFnSc4d1
<g/>
,	,	kIx,
televizní	televizní	k2eAgFnSc4d1
reklamu	reklama	k1gFnSc4
a	a	k8xC
nebo	nebo	k8xC
propagaci	propagace	k1gFnSc4
ve	v	k7c6
službách	služba	k1gFnPc6
od	od	k7c2
Googlu	Googl	k1gInSc2
</s>
<s>
spotřeba	spotřeba	k1gFnSc1
systémových	systémový	k2eAgMnPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
zejména	zejména	k9
RAM	RAM	kA
</s>
<s>
vykreslovací	vykreslovací	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
prohlížečů	prohlížeč	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
měly	mít	k5eAaImAgFnP
své	svůj	k3xOyFgNnSc4
jádro	jádro	k1gNnSc4
(	(	kIx(
<g/>
Opera	opera	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vynucené	vynucený	k2eAgNnSc1d1
přihlašování	přihlašování	k1gNnSc1
uživatele	uživatel	k1gMnSc2
do	do	k7c2
prohlížeče	prohlížeč	k1gInSc2
</s>
<s>
Ve	v	k7c6
verzi	verze	k1gFnSc6
Chrome	chromat	k5eAaImIp3nS
69	#num#	k4
bylo	být	k5eAaImAgNnS
zavedeno	zavést	k5eAaPmNgNnS
vynucené	vynucený	k2eAgNnSc1d1
jednotné	jednotný	k2eAgNnSc1d1
přihlašování	přihlašování	k1gNnSc1
uživatele	uživatel	k1gMnSc2
k	k	k7c3
prohlížeči	prohlížeč	k1gInSc3
a	a	k8xC
službám	služba	k1gFnPc3
Googlu	Googl	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
funguje	fungovat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
se	se	k3xPyFc4
uživatel	uživatel	k1gMnSc1
přihlásí	přihlásit	k5eAaPmIp3nS
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
službě	služba	k1gFnSc3
Googlu	Googla	k1gFnSc4
<g/>
,	,	kIx,
automaticky	automaticky	k6eAd1
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
přihlášení	přihlášení	k1gNnSc1
aplikuje	aplikovat	k5eAaBmIp3nS
i	i	k9
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
prohlížeč	prohlížeč	k1gInSc4
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
analytiků	analytik	k1gMnPc2
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc1
chování	chování	k1gNnSc1
potenciálním	potenciální	k2eAgInSc7d1
problémem	problém	k1gInSc7
pro	pro	k7c4
soukromí	soukromí	k1gNnSc4
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vlně	vlna	k1gFnSc6
kritiky	kritika	k1gFnSc2
této	tento	k3xDgFnSc2
funkce	funkce	k1gFnSc2
Google	Googl	k1gMnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
verzi	verze	k1gFnSc6
Chrome	chromat	k5eAaImIp3nS
70	#num#	k4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
možnost	možnost	k1gFnSc4
automatického	automatický	k2eAgNnSc2d1
přihlašování	přihlašování	k1gNnSc2
do	do	k7c2
prohlížeče	prohlížeč	k1gInSc2
vypínatelná	vypínatelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Omezení	omezení	k1gNnSc1
mazání	mazání	k1gNnSc2
historie	historie	k1gFnSc2
</s>
<s>
V	v	k7c6
Google	Googla	k1gFnSc6
Chrome	chromat	k5eAaImIp3nS
byla	být	k5eAaImAgFnS
omezena	omezit	k5eAaPmNgFnS
možnost	možnost	k1gFnSc1
mazání	mazání	k1gNnSc2
cookies	cookiesa	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
cookies	cookies	k1gInSc1
od	od	k7c2
Googlu	Googl	k1gInSc2
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
smazat	smazat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Možnost	možnost	k1gFnSc1
smazání	smazání	k1gNnSc1
všech	všecek	k3xTgFnPc2
cookies	cookiesa	k1gFnPc2
byla	být	k5eAaImAgFnS
Googlem	Googlo	k1gNnSc7
ohlášena	ohlásit	k5eAaPmNgFnS,k5eAaImNgFnS
pro	pro	k7c4
verzi	verze	k1gFnSc4
Chrome	chromat	k5eAaImIp3nS
70	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úvahy	úvaha	k1gFnPc1
o	o	k7c6
změně	změna	k1gFnSc6
v	v	k7c6
zobrazení	zobrazení	k1gNnSc6
URL	URL	kA
</s>
<s>
S	s	k7c7
verzí	verze	k1gFnSc7
Chrome	chromat	k5eAaImIp3nS
69	#num#	k4
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
úvahy	úvaha	k1gFnPc1
o	o	k7c6
změně	změna	k1gFnSc6
zobrazení	zobrazení	k1gNnSc2
URL	URL	kA
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
tvar	tvar	k1gInSc1
adresy	adresa	k1gFnSc2
zkomplikoval	zkomplikovat	k5eAaPmAgInS
a	a	k8xC
podle	podle	k7c2
Googlu	Googla	k1gFnSc4
uživatelé	uživatel	k1gMnPc1
přestali	přestat	k5eAaPmAgMnP
mít	mít	k5eAaImF
jasno	jasno	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
část	část	k1gFnSc4
URL	URL	kA
k	k	k7c3
čemu	co	k3yQnSc3,k3yInSc3,k3yRnSc3
slouží	sloužit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
experimentu	experiment	k1gInSc2
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
doménu	doména	k1gFnSc4
a	a	k8xC
celou	celá	k1gFnSc4
URL	URL	kA
zobrazí	zobrazit	k5eAaPmIp3nS
až	až	k9
po	po	k7c6
kliknutí	kliknutí	k1gNnSc6
na	na	k7c4
adresní	adresní	k2eAgInSc4d1
řádek	řádek	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
pro	pro	k7c4
velmi	velmi	k6eAd1
nepříznivé	příznivý	k2eNgNnSc4d1
přijetí	přijetí	k1gNnPc4
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
experiment	experiment	k1gInSc1
ukončen	ukončit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Google	Google	k6eAd1
</s>
<s>
Internet	Internet	k1gInSc1
Explorer	Explorra	k1gFnPc2
</s>
<s>
Mozilla	Mozilla	k6eAd1
Firefox	Firefox	k1gInSc1
</s>
<s>
Safari	safari	k1gNnSc1
</s>
<s>
Opera	opera	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Google	Google	k1gNnSc1
Chrome	chromat	k5eAaImIp3nS
on	on	k3xPp3gMnSc1
the	the	k?
App	App	k1gMnSc5
Store	Stor	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-10-26	2017-10-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ITunes	ITunes	k1gMnSc1
Preview	Preview	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEXTRAIT	LEXTRAIT	kA
<g/>
,	,	kIx,
Vincent	Vincent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Programming	Programming	k1gInSc1
Languages	Languages	k1gMnSc1
Beacon	Beacon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
Terms	Terms	k1gInSc4
of	of	k?
Service	Service	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrome	chromat	k5eAaImIp3nS
má	mít	k5eAaImIp3nS
10	#num#	k4
let	léto	k1gNnPc2
<g/>
:	:	kIx,
nejrozšířenější	rozšířený	k2eAgInSc1d3
prohlížeč	prohlížeč	k1gInSc1
používá	používat	k5eAaImIp3nS
60	#num#	k4
%	%	kIx~
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
root	root	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
StatCounter	StatCounter	k1gMnSc1
Global	globat	k5eAaImAgMnS
Stats	Stats	k1gInSc4
-	-	kIx~
Browser	Browsra	k1gFnPc2
<g/>
,	,	kIx,
OS	osa	k1gFnPc2
<g/>
,	,	kIx,
Search	Searcha	k1gFnPc2
Engine	Engin	k1gInSc5
including	including	k1gInSc1
Mobile	mobile	k1gNnSc6
Usage	Usag	k1gFnSc2
Share	Shar	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
StatCounter	StatCounter	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Top	topit	k5eAaImRp2nS
5	#num#	k4
Browsers	Browsers	k1gInSc1
Nov	nov	k1gInSc4
-	-	kIx~
Dec	Dec	k1gFnSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrome	chromat	k5eAaImIp3nS
změnil	změnit	k5eAaPmAgInS
vzhled	vzhled	k1gInSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
generátor	generátor	k1gInSc1
hesel	heslo	k1gNnPc2
a	a	k8xC
chce	chtít	k5eAaImIp3nS
upravit	upravit	k5eAaPmF
zobrazení	zobrazení	k1gNnSc4
URL	URL	kA
<g/>
.	.	kIx.
root	root	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Philipp	Philipp	k1gInSc1
Lenssen	Lenssen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
on	on	k3xPp3gMnSc1
Google	Googl	k1gMnSc4
Chrome	chromat	k5eAaImIp3nS
–	–	k?
comic	comic	k1gMnSc1
book	book	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-09-01	2008-09-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PŘÍJMENÍ	příjmení	k1gNnSc1
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marc	Marc	k1gInSc1
Chung	Chung	k1gInSc4
<g/>
:	:	kIx,
Chrome	chromat	k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Process	Process	k1gInSc1
Model	model	k1gInSc1
Explained	Explained	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-09-05	2008-09-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Robert	Robert	k1gMnSc1
Hensing	Hensing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrome	chromat	k5eAaImIp3nS
Sandboxing	Sandboxing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
2008-09-03	2008-09-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Robert	Robert	k1gMnSc1
Hensing	Hensing	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Blog	Bloga	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechNet	TechNeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Google	Google	k1gNnPc2
Chrome	chromat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2008-09-01	2008-09-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Book	Booka	k1gFnPc2
Search	Searcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BARTH	BARTH	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
,	,	kIx,
Collin	Collin	k2eAgMnSc1d1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Reis	Reis	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
The	The	k1gFnSc1
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
Team	team	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Security	Securita	k1gFnSc2
Architecture	Architectur	k1gMnSc5
of	of	k?
the	the	k?
Chromium	Chromium	k1gNnSc1
Browser	Browser	k1gMnSc1
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanford	Stanford	k1gInSc1
Security	Securita	k1gFnSc2
Laboratory	Laborator	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Markus	Markus	k1gInSc1
Gutschke	Gutschke	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Re	re	k9
<g/>
:	:	kIx,
PATCH	PATCH	kA
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
:	:	kIx,
seccomp	seccomp	k1gInSc1
<g/>
:	:	kIx,
fix	fix	k1gInSc1
32	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
syscall	syscalla	k1gFnPc2
hole	hole	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-05-06	2009-05-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jake	Jak	k1gMnPc4
Edge	Edg	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Chromium	Chromium	k1gNnSc1
sandbox	sandbox	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-08-19	2009-08-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Anonymní	anonymní	k2eAgNnSc4d1
prohlížení	prohlížení	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Nápověda	nápověda	k1gFnSc1
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
LAWRENCE	LAWRENCE	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spying	Spying	k1gInSc1
on	on	k3xPp3gMnSc1
HTTPS	HTTPS	kA
<g/>
.	.	kIx.
text	text	k1gInSc1
<g/>
/	/	kIx~
<g/>
plain	plain	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HUGHES	HUGHES	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virus	virus	k1gInSc1
Bulletin	bulletin	k1gInSc4
:	:	kIx,
2010	#num#	k4
–	–	k?
Social	Social	k1gInSc1
engineering	engineering	k1gInSc1
trumps	trumps	k1gInSc1
a	a	k8xC
zero-day	zero-da	k2eAgInPc1d1
every	ever	k1gInPc1
time	tim	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virus	virus	k1gInSc1
Bulletin	bulletin	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BRIGHT	BRIGHT	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
9	#num#	k4
utterly	utterla	k1gFnSc2
dominates	dominatesa	k1gFnPc2
malware-blocking	malware-blocking	k1gInSc1
stats	stats	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ars	Ars	k1gMnSc1
Technica	Technica	k1gMnSc1
<g/>
,	,	kIx,
2011-07-15	2011-07-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
BIZ	BIZ	kA
&	&	k?
IT	IT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Web	web	k1gInSc1
Browser	Browser	k1gMnSc1
Group	Group	k1gMnSc1
Test	test	k1gMnSc1
Socially-Engineered	Socially-Engineered	k1gMnSc1
Malware	Malwar	k1gMnSc5
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSS	NSS	kA
Labs	Labsa	k1gFnPc2
<g/>
,	,	kIx,
2014-03-31	2014-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PAUL	Paul	k1gMnSc1
<g/>
,	,	kIx,
Ryan	Ryan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
bakes	bakesa	k1gFnPc2
Flash	Flasha	k1gFnPc2
into	into	k6eAd1
Chrome	chromat	k5eAaImIp3nS
<g/>
,	,	kIx,
hopes	hopes	k1gInSc1
to	ten	k3xDgNnSc1
improve	improv	k1gInSc5
plug-in	plug-in	k2eAgMnSc1d1
API	API	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
WIRED	WIRED	kA
Media	medium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2010-03-31	2010-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
BIZ	BIZ	kA
&	&	k?
IT	IT	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Java	Jav	k2eAgFnSc1d1
and	and	k?
Google	Google	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
Browser	Browser	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oracle	Oracle	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Issue	Issue	k1gNnSc1
10812	#num#	k4
–	–	k?
chromium	chromium	k1gNnSc4
–	–	k?
No	no	k9
java	java	k6eAd1
plugin	plugin	k2eAgInSc1d1
support	support	k1gInSc1
yet	yet	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2009-04-21	2009-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Glen	Glen	k1gNnSc4
Murphy	Murpha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
splash	splash	k1gMnSc1
of	of	k?
color	color	k1gMnSc1
to	ten	k3xDgNnSc4
your	your	k1gMnSc1
browser	browser	k1gMnSc1
<g/>
:	:	kIx,
Artist	Artist	k1gMnSc1
Themes	Themes	k1gMnSc1
for	forum	k1gNnPc2
Google	Google	k1gNnSc7
Chrome	chromat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2009-10-05	2009-10-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
Blog	Blog	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
Themes	Themes	k1gInSc1
Gallery	Galler	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrome	chromat	k5eAaImIp3nS
web	web	k1gInSc4
store	stor	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Support	support	k1gInSc1
Google	Googl	k1gMnSc2
Chrome	chromat	k5eAaImIp3nS
–	–	k?
Automatic	Automatice	k1gFnPc2
web	web	k1gInSc4
page	pagat	k5eAaPmIp3nS
translation	translation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
Help	help	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stephen	Stephen	k2eAgInSc4d1
Shankland	Shankland	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speed	Speed	k1gInSc1
test	test	k1gInSc4
<g/>
:	:	kIx,
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
Interactive	Interactiv	k1gInSc5
Inc	Inc	k1gMnPc7
<g/>
,	,	kIx,
2008-10-07	2008-10-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
CNET	CNET	kA
Business	business	k1gInSc1
Tech	Tech	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OVERA	OVERA	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JavaScript	JavaScripta	k1gFnPc2
And	Anda	k1gFnPc2
DOM	DOM	k?
Performance	performance	k1gFnSc2
-	-	kIx~
Chrome	chromat	k5eAaImIp3nS
27	#num#	k4
<g/>
,	,	kIx,
Firefox	Firefox	k1gInSc1
22	#num#	k4
<g/>
,	,	kIx,
IE	IE	kA
<g/>
10	#num#	k4
<g/>
,	,	kIx,
And	Anda	k1gFnPc2
Opera	opera	k1gFnSc1
Next	Next	k1gInSc1
<g/>
,	,	kIx,
Benchmarked	Benchmarked	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Purch	Purcha	k1gFnPc2
<g/>
,	,	kIx,
2013-06-30	2013-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
BROWSERS	BROWSERS	kA
>	>	kIx)
ROUND-UP	ROUND-UP	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
midas	midas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Best	Best	k2eAgInSc4d1
Web	web	k1gInSc4
Browser	Browsra	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Explorer	Explorer	k1gInSc1
11	#num#	k4
<g/>
,	,	kIx,
Chrome	chromat	k5eAaImIp3nS
31	#num#	k4
<g/>
,	,	kIx,
Firefox	Firefox	k1gInSc1
25	#num#	k4
<g/>
,	,	kIx,
Opera	opera	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
or	or	k?
Safari	safari	k1gNnSc1
5	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
|	|	kIx~
MIDAS	Midas	k1gMnSc1
–	–	k?
Web-Based	Web-Based	k1gInSc1
Room	Room	k1gMnSc1
Scheduling	Scheduling	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-11-13	2013-11-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Tech	Tech	k?
Insight	Insight	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MIDAS	Midas	k1gMnSc1
BLOG	BLOG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Megan	Megan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
System	Systo	k1gNnSc7
requirements	requirementsa	k1gFnPc2
:	:	kIx,
Install	Install	k1gInSc1
or	or	k?
update	update	k1gInSc1
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
:	:	kIx,
Google	Googla	k1gFnSc3
Chrome	chromat	k5eAaImIp3nS
Help	help	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
2011-04-08	2011-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Google	Google	k1gInSc1
Chrome	chromat	k5eAaImIp3nS
Help	help	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
JEŽEK	Ježek	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrome	chromat	k5eAaImIp3nS
69	#num#	k4
si	se	k3xPyFc3
vynucuje	vynucovat	k5eAaImIp3nS
login	login	k1gMnSc1
<g/>
,	,	kIx,
Qualcomm	Qualcomm	k1gMnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Apple	Apple	kA
krade	krást	k5eAaImIp3nS
<g/>
.	.	kIx.
root	root	k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Beta	beta	k1gNnSc2
verze	verze	k1gFnSc2
prohlížeče	prohlížeč	k1gMnSc2
Google	Googl	k1gMnSc2
Chrome	chromat	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
na	na	k7c4
trh	trh	k1gInSc4
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
projektu	projekt	k1gInSc2
Chromium	Chromium	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Chromium	Chromium	k1gNnSc1
Win	Win	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Mac	Mac	kA
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
<g/>
/	/	kIx~
<g/>
Android	android	k1gInSc1
nejnovější	nový	k2eAgFnSc1d3
spustitelná	spustitelný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
bez	bez	k7c2
EULA	EULA	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Canary	Canar	k1gInPc1
<g/>
,	,	kIx,
Dev	Dev	k1gFnPc1
<g/>
,	,	kIx,
Beta	beta	k1gNnSc1
a	a	k8xC
Stable	Stable	k1gFnSc1
verze	verze	k1gFnSc1
Google	Google	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google	Google	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
Portable	portable	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Chromium	Chromium	k1gNnSc1
Portable	portable	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google	Google	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
komix	komix	k1gInSc4
od	od	k7c2
Scotta	Scott	k1gInSc2
McClouda	McCloudo	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Článek	článek	k1gInSc1
o	o	k7c6
problematice	problematika	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
Google	Google	k1gFnSc2
Chrome	chromat	k5eAaImIp3nS
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google	Googla	k1gFnSc3
Chrome	chromat	k5eAaImIp3nS
Releases	Releases	k1gMnSc1
blog	blog	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Slovenská	slovenský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
o	o	k7c6
Google	Googla	k1gFnSc6
chrome	chromat	k5eAaImIp3nS
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Chrome	chromat	k5eAaImIp3nS
Web	web	k1gInSc4
Store	Stor	k1gInSc5
–	–	k?
aplikace	aplikace	k1gFnSc1
–	–	k?
rozšíření	rozšíření	k1gNnSc2
–	–	k?
motivy	motiv	k1gInPc7
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google	Googla	k1gFnSc3
Chrome	chromat	k5eAaImIp3nS
Plugins	Plugins	k1gInSc1
<g/>
,	,	kIx,
Themes	Themes	k1gInSc1
a	a	k8xC
Add-ons	Add-ons	k1gInSc1
–	–	k?
neoficiální	oficiální	k2eNgInSc1d1,k2eAgInSc1d1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Chrome	chromat	k5eAaImIp3nS
Experiments	Experiments	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7631519-8	7631519-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2009043824	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
175276668	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2009043824	#num#	k4
</s>
