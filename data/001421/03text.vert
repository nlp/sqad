<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
William	William	k1gInSc1	William
Hawking	Hawking	k1gInSc1	Hawking
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
Oxford	Oxford	k1gInSc1	Oxford
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
vědců	vědec	k1gMnPc2	vědec
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgInS	přispět
zejména	zejména	k9	zejména
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
oborům	obor	k1gInPc3	obor
kosmologie	kosmologie	k1gFnSc2	kosmologie
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
až	až	k9	až
2009	[number]	k4	2009
zastával	zastávat	k5eAaImAgInS	zastávat
post	post	k1gInSc4	post
lukasiánského	lukasiánský	k2eAgMnSc2d1	lukasiánský
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
univerzitním	univerzitní	k2eAgNnSc6d1	univerzitní
městě	město	k1gNnSc6	město
Oxfordu	Oxford	k1gInSc2	Oxford
rodičům	rodič	k1gMnPc3	rodič
Frankovi	Frank	k1gMnSc6	Frank
a	a	k8xC	a
Isobel	Isobel	k1gInSc4	Isobel
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Walkerové	Walkerové	k2eAgMnSc1d1	Walkerové
<g/>
)	)	kIx)	)
Hawkingovým	Hawkingový	k2eAgInSc7d1	Hawkingový
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
mladší	mladý	k2eAgFnPc4d2	mladší
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Philippu	Philippa	k1gFnSc4	Philippa
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
a	a	k8xC	a
adoptivního	adoptivní	k2eAgMnSc2d1	adoptivní
bratra	bratr	k1gMnSc2	bratr
Edwarda	Edward	k1gMnSc2	Edward
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Frank	Frank	k1gMnSc1	Frank
Hawking	Hawking	k1gInSc4	Hawking
byl	být	k5eAaImAgMnS	být
výzkumníkem	výzkumník	k1gMnSc7	výzkumník
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
tropické	tropický	k2eAgFnSc2d1	tropická
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Isobel	Isobela	k1gFnPc2	Isobela
Hawkingová	Hawkingový	k2eAgFnSc1d1	Hawkingová
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
v	v	k7c6	v
levicové	levicový	k2eAgFnSc6d1	levicová
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
narozením	narození	k1gNnSc7	narození
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
následkům	následek	k1gInPc3	následek
bombardování	bombardování	k1gNnSc1	bombardování
města	město	k1gNnSc2	město
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
opět	opět	k6eAd1	opět
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
St.	st.	kA	st.
Albans	Albans	k1gInSc1	Albans
severně	severně	k6eAd1	severně
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hawking	Hawking	k1gInSc1	Hawking
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
místní	místní	k2eAgFnSc4d1	místní
výběrovou	výběrový	k2eAgFnSc4d1	výběrová
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
měl	mít	k5eAaImAgMnS	mít
studovat	studovat	k5eAaImF	studovat
lékařství	lékařství	k1gNnSc4	lékařství
jako	jako	k8xC	jako
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
však	však	k9	však
živě	živě	k6eAd1	živě
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
maturitou	maturita	k1gFnSc7	maturita
podal	podat	k5eAaPmAgMnS	podat
žádost	žádost	k1gFnSc4	žádost
na	na	k7c4	na
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c4	na
Oxfordskou	oxfordský	k2eAgFnSc4d1	Oxfordská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
překvapení	překvapení	k1gNnSc3	překvapení
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
studijní	studijní	k2eAgNnSc4d1	studijní
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
věnoval	věnovat	k5eAaPmAgMnS	věnovat
fyzice	fyzika	k1gFnSc3	fyzika
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejnadanějších	nadaný	k2eAgMnPc2d3	nejnadanější
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
přáním	přání	k1gNnSc7	přání
bylo	být	k5eAaImAgNnS	být
studovat	studovat	k5eAaImF	studovat
kosmologii	kosmologie	k1gFnSc4	kosmologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
složil	složit	k5eAaPmAgInS	složit
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
úspěchem	úspěch	k1gInSc7	úspěch
zkoušku	zkouška	k1gFnSc4	zkouška
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
doktorské	doktorský	k2eAgFnSc6d1	doktorská
práci	práce	k1gFnSc6	práce
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Denise	Denisa	k1gFnSc3	Denisa
Sciama	Sciama	k1gFnSc1	Sciama
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
úspěšně	úspěšně	k6eAd1	úspěšně
obhájil	obhájit	k5eAaPmAgMnS	obhájit
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
titulu	titul	k1gInSc3	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
výzkumníkem	výzkumník	k1gMnSc7	výzkumník
(	(	kIx(	(
<g/>
Research	Research	k1gMnSc1	Research
Fellow	Fellow	k1gMnSc1	Fellow
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
(	(	kIx(	(
<g/>
Professorial	Professorial	k1gMnSc1	Professorial
Fellow	Fellow	k1gFnSc2	Fellow
<g/>
)	)	kIx)	)
univerzity	univerzita	k1gFnSc2	univerzita
Gonville	Gonville	k1gFnSc2	Gonville
and	and	k?	and
Caius	Caius	k1gInSc1	Caius
College	College	k1gFnPc2	College
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
opustil	opustit	k5eAaPmAgInS	opustit
Astronomický	astronomický	k2eAgInSc1d1	astronomický
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgInS	přejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
zastával	zastávat	k5eAaImAgInS	zastávat
pozici	pozice	k1gFnSc4	pozice
Lukasiánského	Lukasiánský	k2eAgMnSc2d1	Lukasiánský
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
vůli	vůle	k1gFnSc6	vůle
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1663	[number]	k4	1663
člen	člen	k1gInSc1	člen
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
parlamentu	parlament	k1gInSc2	parlament
reverend	reverend	k1gMnSc1	reverend
Henry	Henry	k1gMnSc1	Henry
Lucas	Lucas	k1gMnSc1	Lucas
<g/>
.	.	kIx.	.
</s>
<s>
Lukasiánským	Lukasiánský	k2eAgMnSc7d1	Lukasiánský
profesorem	profesor	k1gMnSc7	profesor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc1	newton
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
mnoha	mnoho	k4c2	mnoho
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
ocenění	ocenění	k1gNnPc2	ocenění
a	a	k8xC	a
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
učených	učený	k2eAgFnPc2d1	učená
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
britské	britský	k2eAgFnSc2d1	britská
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
od	od	k7c2	od
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
postižení	postižení	k1gNnSc4	postižení
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
aktivně	aktivně	k6eAd1	aktivně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vědeckém	vědecký	k2eAgInSc6d1	vědecký
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
tři	tři	k4xCgNnPc1	tři
vnoučata	vnouče	k1gNnPc1	vnouče
<g/>
)	)	kIx)	)
s	s	k7c7	s
výzkumy	výzkum	k1gInPc7	výzkum
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
náročným	náročný	k2eAgInSc7d1	náročný
cestovním	cestovní	k2eAgInSc7d1	cestovní
programem	program	k1gInSc7	program
veřejných	veřejný	k2eAgFnPc2d1	veřejná
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
cestovat	cestovat	k5eAaImF	cestovat
časem	časem	k6eAd1	časem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
obydlila	obydlit	k5eAaPmAgFnS	obydlit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
zničená	zničený	k2eAgFnSc1d1	zničená
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasicky	klasicky	k6eAd1	klasicky
vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
jeho	jeho	k3xOp3gFnSc2	jeho
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
významných	významný	k2eAgMnPc2d1	významný
neurologů	neurolog	k1gMnPc2	neurolog
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c6	o
ALS	ALS	kA	ALS
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c6	o
spinální	spinální	k2eAgFnSc6d1	spinální
muskulární	muskulární	k2eAgFnSc6d1	muskulární
atrofii	atrofie	k1gFnSc6	atrofie
-	-	kIx~	-
SMA	SMA	kA	SMA
<g/>
,	,	kIx,	,
Hawkingův	Hawkingův	k2eAgInSc1d1	Hawkingův
případ	případ	k1gInSc1	případ
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
objasněn	objasněn	k2eAgMnSc1d1	objasněn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
napadá	napadat	k5eAaPmIp3nS	napadat
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
postupné	postupný	k2eAgNnSc4d1	postupné
ochrnutí	ochrnutí	k1gNnSc4	ochrnutí
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k9	jen
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vozíčku	vozíček	k1gInSc2	vozíček
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
si	se	k3xPyFc3	se
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
byl	být	k5eAaImAgMnS	být
kormidelníkem	kormidelník	k1gMnSc7	kormidelník
veslařského	veslařský	k2eAgInSc2d1	veslařský
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
ulehčit	ulehčit	k5eAaPmF	ulehčit
nesmírnou	smírný	k2eNgFnSc4d1	nesmírná
univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
nudu	nuda	k1gFnSc4	nuda
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
choroby	choroba	k1gFnSc2	choroba
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Ztratil	ztratit	k5eAaPmAgInS	ztratit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
a	a	k8xC	a
spadl	spadnout	k5eAaPmAgInS	spadnout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bál	bát	k5eAaImAgMnS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
Mensa	mensa	k1gFnSc1	mensa
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ověřil	ověřit	k5eAaPmAgMnS	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
intelektuální	intelektuální	k2eAgFnPc1d1	intelektuální
schopnosti	schopnost	k1gFnPc1	schopnost
nebyly	být	k5eNaImAgFnP	být
narušeny	narušit	k5eAaPmNgFnP	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
svatbou	svatba	k1gFnSc7	svatba
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
lékařů	lékař	k1gMnPc2	lékař
neměl	mít	k5eNaImAgMnS	mít
žít	žít	k5eAaImF	žít
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
pažemi	paže	k1gFnPc7	paže
<g/>
,	,	kIx,	,
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
hlasem	hlas	k1gInSc7	hlas
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
úplně	úplně	k6eAd1	úplně
paralyzován	paralyzovat	k5eAaBmNgMnS	paralyzovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
centra	centrum	k1gNnSc2	centrum
CERN	CERN	kA	CERN
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
nakazil	nakazit	k5eAaPmAgInS	nakazit
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
případě	případ	k1gInSc6	případ
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ohrožení	ohrožení	k1gNnSc1	ohrožení
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byly	být	k5eAaImAgFnP	být
akutní	akutní	k2eAgFnPc1d1	akutní
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vyřešit	vyřešit	k5eAaPmF	vyřešit
jedině	jedině	k6eAd1	jedině
pomocí	pomocí	k7c2	pomocí
tracheotomie	tracheotomie	k1gFnSc2	tracheotomie
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
elektronický	elektronický	k2eAgInSc1d1	elektronický
hlasový	hlasový	k2eAgInSc1d1	hlasový
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
přístroj	přístroj	k1gInSc1	přístroj
měl	mít	k5eAaImAgInS	mít
americký	americký	k2eAgInSc1d1	americký
přízvuk	přízvuk	k1gInSc1	přízvuk
a	a	k8xC	a
Hawking	Hawking	k1gInSc1	Hawking
ho	on	k3xPp3gMnSc4	on
používal	používat	k5eAaImAgInS	používat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
neslyšel	slyšet	k5eNaImAgMnS	slyšet
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
víc	hodně	k6eAd2	hodně
líbil	líbit	k5eAaImAgInS	líbit
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ztotožnil	ztotožnit	k5eAaPmAgMnS	ztotožnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
náhrada	náhrada	k1gFnSc1	náhrada
našla	najít	k5eAaPmAgFnS	najít
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc4	systém
VoiceText	VoiceTexta	k1gFnPc2	VoiceTexta
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
NeoSpeech	NeoSpeech	k1gInSc1	NeoSpeech
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k6eAd1	navzdory
svému	svůj	k3xOyFgMnSc3	svůj
postižení	postižení	k1gNnSc1	postižení
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
šťastlivci	šťastlivec	k1gMnPc1	šťastlivec
<g/>
"	"	kIx"	"
nejenom	nejenom	k6eAd1	nejenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomalý	pomalý	k2eAgInSc1d1	pomalý
postup	postup	k1gInSc1	postup
nemoci	nemoc	k1gFnSc2	nemoc
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
čas	čas	k1gInSc4	čas
učinit	učinit	k5eAaImF	učinit
významné	významný	k2eAgInPc4d1	významný
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
okouzlující	okouzlující	k2eAgFnSc4d1	okouzlující
rodinu	rodina	k1gFnSc4	rodina
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc2	jeho
první	první	k4xOgFnSc2	první
ženy	žena	k1gFnSc2	žena
Jane	Jan	k1gMnSc5	Jan
ptali	ptat	k5eAaImAgMnP	ptat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
zbývají	zbývat	k5eAaImIp3nP	zbývat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
doba	doba	k1gFnSc1	doba
obav	obava	k1gFnPc2	obava
z	z	k7c2	z
atomové	atomový	k2eAgFnSc2d1	atomová
zkázy	zkáza	k1gFnSc2	zkáza
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vyhlídky	vyhlídka	k1gFnPc1	vyhlídka
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
život	život	k1gInSc4	život
jsme	být	k5eAaImIp1nP	být
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
základních	základní	k2eAgInPc6d1	základní
zákonech	zákon	k1gInPc6	zákon
fungování	fungování	k1gNnSc2	fungování
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Penrosem	Penros	k1gMnSc7	Penros
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc1	čas
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
má	mít	k5eAaImIp3nS	mít
počátek	počátek	k1gInSc4	počátek
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
a	a	k8xC	a
konec	konec	k1gInSc4	konec
v	v	k7c6	v
černých	černý	k2eAgFnPc6d1	černá
dírách	díra	k1gFnPc6	díra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
sladit	sladit	k5eAaImF	sladit
obecnou	obecný	k2eAgFnSc4d1	obecná
relativitu	relativita	k1gFnSc4	relativita
s	s	k7c7	s
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
vědeckým	vědecký	k2eAgInSc7d1	vědecký
objevem	objev	k1gInSc7	objev
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
těchto	tento	k3xDgInPc2	tento
výzkumů	výzkum	k1gInPc2	výzkum
byl	být	k5eAaImAgInS	být
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgFnP	mít
být	být	k5eAaImF	být
úplně	úplně	k6eAd1	úplně
černé	černý	k2eAgNnSc1d1	černé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
emitovat	emitovat	k5eAaBmF	emitovat
záření	záření	k1gNnSc3	záření
a	a	k8xC	a
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
i	i	k9	i
postupně	postupně	k6eAd1	postupně
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zcela	zcela	k6eAd1	zcela
zmizet	zmizet	k5eAaPmF	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
je	být	k5eAaImIp3nS	být
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
pomyslném	pomyslný	k2eAgInSc6d1	pomyslný
čase	čas	k1gInSc6	čas
žádný	žádný	k3yNgInSc4	žádný
okraj	okraj	k1gInSc4	okraj
nebo	nebo	k8xC	nebo
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
vesmír	vesmír	k1gInSc1	vesmír
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
dán	dát	k5eAaPmNgInS	dát
vědeckými	vědecký	k2eAgInPc7d1	vědecký
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gFnPc2	jeho
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
publikací	publikace	k1gFnPc2	publikace
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
stům	sto	k4xCgNnPc3	sto
a	a	k8xC	a
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
příkladnými	příkladný	k2eAgInPc7d1	příkladný
vědecky	vědecky	k6eAd1	vědecky
odbornými	odborný	k2eAgInPc7d1	odborný
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
i	i	k9	i
populárně-naučné	populárněaučný	k2eAgNnSc1d1	populárně-naučné
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
i	i	k9	i
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
zasvěcenému	zasvěcený	k2eAgMnSc3d1	zasvěcený
čtenáři	čtenář	k1gMnSc3	čtenář
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
přitom	přitom	k6eAd1	přitom
ztratily	ztratit	k5eAaPmAgInP	ztratit
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
odbornosti	odbornost	k1gFnPc4	odbornost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
jeho	jeho	k3xOp3gFnSc4	jeho
Stručnou	stručný	k2eAgFnSc4d1	stručná
historii	historie	k1gFnSc4	historie
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
A	a	k9	a
Brief	Brief	k1gInSc1	Brief
History	Histor	k1gInPc1	Histor
of	of	k?	of
Time	Tim	k1gInSc2	Tim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vesmír	vesmír	k1gInSc1	vesmír
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Universe	Universe	k1gFnSc2	Universe
in	in	k?	in
a	a	k8xC	a
Nutshell	Nutshellum	k1gNnPc2	Nutshellum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Ilustrovanou	ilustrovaný	k2eAgFnSc4d1	ilustrovaná
teorii	teorie	k1gFnSc4	teorie
všeho	všecek	k3xTgNnSc2	všecek
(	(	kIx(	(
<g/>
The	The	k1gMnPc2	The
Illustrated	Illustrated	k1gMnSc1	Illustrated
Theory	Theora	k1gFnSc2	Theora
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejčtenějším	čtený	k2eAgFnPc3d3	Nejčtenější
knihám	kniha	k1gFnPc3	kniha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
The	The	k1gMnSc2	The
Large	Larg	k1gMnSc2	Larg
Scale	Scal	k1gMnSc2	Scal
Structure	Structur	k1gMnSc5	Structur
of	of	k?	of
Spacetime	Spacetim	k1gMnSc5	Spacetim
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
G.	G.	kA	G.
F.	F.	kA	F.
R.	R.	kA	R.
Ellis	Ellis	k1gFnSc1	Ellis
<g/>
)	)	kIx)	)
General	General	k1gFnSc1	General
Relativity	relativita	k1gFnSc2	relativita
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
Einstein	Einstein	k1gMnSc1	Einstein
Centenary	Centenara	k1gFnSc2	Centenara
Survey	Survea	k1gFnSc2	Survea
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
W.	W.	kA	W.
Israael	Israael	k1gInSc1	Israael
<g/>
)	)	kIx)	)
Superspace	Superspace	k1gFnSc1	Superspace
and	and	k?	and
Supergravity	Supergravita	k1gFnSc2	Supergravita
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1981	[number]	k4	1981
The	The	k1gFnSc1	The
Very	Ver	k2eAgMnPc4d1	Ver
Early	earl	k1gMnPc4	earl
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
A	a	k9	a
Brief	Brief	k1gInSc1	Brief
History	Histor	k1gInPc1	Histor
of	of	k?	of
Time	Tim	k1gInSc2	Tim
<g/>
:	:	kIx,	:
From	From	k1gMnSc1	From
the	the	k?	the
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
to	ten	k3xDgNnSc4	ten
Black	Black	k1gMnSc1	Black
Holes	Holes	k1gMnSc1	Holes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Holes	Holes	k1gMnSc1	Holes
and	and	k?	and
Baby	baba	k1gFnSc2	baba
<g />
.	.	kIx.	.
</s>
<s>
Universes	Universes	k1gInSc1	Universes
and	and	k?	and
Other	Other	k1gInSc1	Other
Essays	Essays	k1gInSc1	Essays
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1995	[number]	k4	1995
The	The	k1gMnSc1	The
Illustrated	Illustrated	k1gMnSc1	Illustrated
A	a	k8xC	a
Brief	Brief	k1gInSc1	Brief
History	Histor	k1gInPc1	Histor
of	of	k?	of
Time	Time	k1gInSc1	Time
<g/>
:	:	kIx,	:
Updated	Updated	k1gInSc1	Updated
and	and	k?	and
Expanded	Expanded	k1gInSc1	Expanded
Edition	Edition	k1gInSc1	Edition
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
ilustrované	ilustrovaný	k2eAgNnSc1d1	ilustrované
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Povaha	povaha	k1gFnSc1	povaha
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Nature	Natur	k1gMnSc5	Natur
of	of	k?	of
Space	Spaka	k1gFnSc6	Spaka
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Time	Time	k1gInSc1	Time
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
<g/>
:	:	kIx,	:
R.	R.	kA	R.
Penrose	Penrosa	k1gFnSc6	Penrosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2000	[number]	k4	2000
Vesmír	vesmír	k1gInSc4	vesmír
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Universe	Universe	k1gFnSc2	Universe
in	in	k?	in
a	a	k8xC	a
Nutshell	Nutshellum	k1gNnPc2	Nutshellum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2002	[number]	k4	2002
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
teorie	teorie	k1gFnSc1	teorie
všeho	všecek	k3xTgNnSc2	všecek
(	(	kIx(	(
<g/>
The	The	k1gMnPc2	The
Illustrated	Illustrated	k1gMnSc1	Illustrated
Theory	Theora	k1gFnSc2	Theora
of	of	k?	of
Everything	Everything	k1gInSc1	Everything
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Jirkův	Jirkův	k2eAgInSc1d1	Jirkův
tajný	tajný	k2eAgInSc1d1	tajný
klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
vesmíru	vesmír	k1gInSc3	vesmír
(	(	kIx(	(
<g/>
George	George	k1gNnSc3	George
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Secret	Secret	k1gMnSc1	Secret
Key	Key	k1gMnSc1	Key
To	to	k9	to
The	The	k1gMnSc1	The
Universe	Universe	k1gFnSc2	Universe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2008	[number]	k4	2008
Jirkův	Jirkův	k2eAgInSc1d1	Jirkův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
lov	lov	k1gInSc1	lov
pokladů	poklad	k1gInPc2	poklad
(	(	kIx(	(
<g/>
George	George	k1gInSc1	George
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Space	Spaka	k1gFnSc3	Spaka
Treasure	Treasur	k1gMnSc5	Treasur
<g />
.	.	kIx.	.
</s>
<s>
Hunt	hunt	k1gInSc1	hunt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2009	[number]	k4	2009
Jirka	Jirka	k1gMnSc1	Jirka
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
třesk	třesk	k1gInSc1	třesk
(	(	kIx(	(
<g/>
George	George	k1gInSc1	George
and	and	k?	and
the	the	k?	the
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Jirka	Jirka	k1gFnSc1	Jirka
a	a	k8xC	a
neprolomitelná	prolomitelný	k2eNgFnSc1d1	neprolomitelná
šifra	šifra	k1gFnSc1	šifra
(	(	kIx(	(
George	George	k1gFnSc1	George
and	and	k?	and
the	the	k?	the
Unbreakable	Unbreakable	k1gMnSc2	Unbreakable
Code	Cod	k1gMnSc2	Cod
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
Velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
plán	plán	k1gInSc1	plán
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Grand	grand	k1gMnSc1	grand
Design	design	k1gInSc1	design
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2011	[number]	k4	2011
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
televizního	televizní	k2eAgInSc2d1	televizní
dokumentu	dokument	k1gInSc2	dokument
Hawking	Hawking	k1gInSc1	Hawking
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Suche	Such	k1gFnSc2	Such
nach	nach	k1gInSc1	nach
dem	dem	k?	dem
Anfang	Anfang	k1gInSc1	Anfang
der	drát	k5eAaImRp2nS	drát
Zeit	Zeit	k1gInSc1	Zeit
(	(	kIx(	(
<g/>
Hawking	Hawking	k1gInSc1	Hawking
<g/>
:	:	kIx,	:
Pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
počátku	počátek	k1gInSc6	počátek
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
německo-francouzskou	německorancouzský	k2eAgFnSc7d1	německo-francouzská
televizní	televizní	k2eAgFnSc7d1	televizní
společností	společnost	k1gFnSc7	společnost
Arte	Art	k1gFnSc2	Art
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
zálibě	záliba	k1gFnSc3	záliba
v	v	k7c6	v
ironii	ironie	k1gFnSc6	ironie
a	a	k8xC	a
sarkasmu	sarkasmus	k1gInSc2	sarkasmus
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
krátkých	krátký	k2eAgInPc6d1	krátký
výstupech	výstup	k1gInPc6	výstup
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
skečích	skeč	k1gInPc6	skeč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sám	sám	k3xTgMnSc1	sám
hostoval	hostovat	k5eAaImAgInS	hostovat
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgInS	zahrát
hologram	hologram	k1gInSc1	hologram
sama	sám	k3xTgMnSc4	sám
sebe	sebe	k3xPyFc4	sebe
ve	v	k7c6	v
dvojepizodě	dvojepizoda	k1gFnSc6	dvojepizoda
"	"	kIx"	"
<g/>
Vpád	vpád	k1gInSc1	vpád
<g/>
"	"	kIx"	"
seriálu	seriál	k1gInSc2	seriál
Star	star	k1gInSc1	star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
student	student	k1gMnSc1	student
byl	být	k5eAaImAgMnS	být
horlivým	horlivý	k2eAgMnSc7d1	horlivý
čtenářem	čtenář	k1gMnSc7	čtenář
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vsedě	vsedě	k6eAd1	vsedě
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vozíku	vozík	k1gInSc6	vozík
přestavěném	přestavěný	k2eAgInSc6d1	přestavěný
na	na	k7c4	na
vrtulník	vrtulník	k1gInSc4	vrtulník
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roli	role	k1gFnSc6	role
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
dílech	díl	k1gInPc6	díl
seriálu	seriál	k1gInSc2	seriál
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Hawkingova	Hawkingův	k2eAgFnSc1d1	Hawkingova
excitace	excitace	k1gFnSc1	excitace
<g/>
"	"	kIx"	"
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
epizodě	epizoda	k1gFnSc6	epizoda
šesté	šestý	k4xOgFnSc2	šestý
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
díl	díl	k1gInSc1	díl
"	"	kIx"	"
<g/>
Výmaz	výmaz	k1gInSc1	výmaz
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pouze	pouze	k6eAd1	pouze
hovořil	hovořit	k5eAaImAgMnS	hovořit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Futurama	Futurama	k?	Futurama
představoval	představovat	k5eAaImAgMnS	představovat
člena	člen	k1gMnSc4	člen
přísně	přísně	k6eAd1	přísně
tajné	tajný	k2eAgFnPc1d1	tajná
skupiny	skupina	k1gFnPc1	skupina
starající	starající	k2eAgFnPc1d1	starající
se	se	k3xPyFc4	se
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
kontinua	kontinuum	k1gNnSc2	kontinuum
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
syntetizovaný	syntetizovaný	k2eAgInSc1d1	syntetizovaný
hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
skladeb	skladba	k1gFnPc2	skladba
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
Talking	Talking	k1gInSc1	Talking
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Talkin	Talkin	k2eAgInSc4d1	Talkin
<g/>
'	'	kIx"	'
Hawkin	Hawkin	k1gInSc4	Hawkin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Endless	Endlessa	k1gFnPc2	Endlessa
River	Rivra	k1gFnPc2	Rivra
<g/>
)	)	kIx)	)
britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skupiny	skupina	k1gFnPc1	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
byl	být	k5eAaImAgInS	být
britským	britský	k2eAgMnSc7d1	britský
režisérem	režisér	k1gMnSc7	režisér
Jamesem	James	k1gMnSc7	James
Marshem	Marsh	k1gInSc7	Marsh
natočen	natočen	k2eAgInSc4d1	natočen
životopisný	životopisný	k2eAgInSc4d1	životopisný
snímek	snímek	k1gInSc4	snímek
Teorie	teorie	k1gFnSc1	teorie
všeho	všecek	k3xTgInSc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Stephena	Stephen	k2eAgFnSc1d1	Stephena
Hawkinga	Hawkinga	k1gFnSc1	Hawkinga
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Eddie	Eddie	k1gFnPc1	Eddie
Redmayne	Redmayn	k1gInSc5	Redmayn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
připravoval	připravovat	k5eAaImAgInS	připravovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
skutečným	skutečný	k2eAgMnSc7d1	skutečný
Stephenem	Stephen	k1gMnSc7	Stephen
Hawkingem	Hawking	k1gInSc7	Hawking
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
Cen	cena	k1gFnPc2	cena
Akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
jediného	jediný	k2eAgMnSc4d1	jediný
Oscara	Oscar	k1gMnSc4	Oscar
získal	získat	k5eAaPmAgMnS	získat
Eddie	Eddie	k1gFnPc1	Eddie
Redmayne	Redmayn	k1gInSc5	Redmayn
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Eddingtonova	Eddingtonův	k2eAgFnSc1d1	Eddingtonova
medaile	medaile	k1gFnSc1	medaile
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
Hughesova	Hughesův	k2eAgFnSc1d1	Hughesova
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
udělována	udělován	k2eAgFnSc1d1	udělována
Královskou	královský	k2eAgFnSc7d1	královská
společností	společnost	k1gFnSc7	společnost
1985	[number]	k4	1985
<g/>
:	:	kIx,	:
Gold	Gold	k1gMnSc1	Gold
Medal	Medal	k1gMnSc1	Medal
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gInSc4	Royal
Astronomical	Astronomical	k1gMnSc4	Astronomical
Society	societa	k1gFnSc2	societa
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
člen	člen	k1gMnSc1	člen
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
Wolfova	Wolfův	k2eAgFnSc1d1	Wolfova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Asturias	Asturias	k1gInSc1	Asturias
Awards	Awards	k1gInSc1	Awards
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
společníků	společník	k1gMnPc2	společník
cti	čest	k1gFnSc2	čest
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Julius	Julius	k1gMnSc1	Julius
Edgar	Edgar	k1gMnSc1	Edgar
Lilienfeld	Lilienfeld	k1gMnSc1	Lilienfeld
Prize	Prize	k1gFnSc1	Prize
</s>
