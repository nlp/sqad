<s>
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ソ	ソ	k?	ソ
<g/>
・	・	k?	・
<g/>
オ	オ	k?	オ
<g/>
,	,	kIx,	,
Sódo	Sódo	k1gMnSc1	Sódo
Áto	Áto	k1gMnSc1	Áto
Onrain	Onrain	k1gMnSc1	Onrain
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
sci-fi	scii	k1gFnSc1	sci-fi
série	série	k1gFnSc2	série
light	lighta	k1gFnPc2	lighta
novel	novela	k1gFnPc2	novela
od	od	k7c2	od
Reki	Rek	k1gFnSc2	Rek
Kawahary	Kawahara	k1gFnSc2	Kawahara
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
proslulého	proslulý	k2eAgInSc2d1	proslulý
sérií	série	k1gFnSc7	série
Accel	Acclo	k1gNnPc2	Acclo
World	Worlda	k1gFnPc2	Worlda
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
námětem	námět	k1gInSc7	námět
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
light	lighta	k1gFnPc2	lighta
novel	novela	k1gFnPc2	novela
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
již	již	k9	již
osmi	osm	k4xCc2	osm
mang	mango	k1gNnPc2	mango
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
anime	animat	k5eAaPmIp3nS	animat
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
motivy	motiv	k1gInPc4	motiv
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
hry	hra	k1gFnSc2	hra
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc5	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
:	:	kIx,	:
Infinity	Infinita	k1gFnSc2	Infinita
Moment	moment	k1gInSc1	moment
pro	pro	k7c4	pro
PSP	PSP	kA	PSP
a	a	k8xC	a
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
<g/>
:	:	kIx,	:
Hollow	Hollow	k1gFnSc1	Hollow
Fragment	fragment	k1gInSc4	fragment
pro	pro	k7c4	pro
PS	PS	kA	PS
Vita	vít	k5eAaImNgFnS	vít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
se	se	k3xPyFc4	se
vysílala	vysílat	k5eAaImAgFnS	vysílat
druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
seriálu	seriál	k1gInSc2	seriál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
získala	získat	k5eAaPmAgFnS	získat
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
Skydance	Skydanec	k1gInSc2	Skydanec
Media	medium	k1gNnSc2	medium
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
hraného	hraný	k2eAgInSc2d1	hraný
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2022	[number]	k4	2022
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
virtuální	virtuální	k2eAgFnSc1d1	virtuální
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
VMMORPG	VMMORPG	kA	VMMORPG
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
SAO	SAO	kA	SAO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
pomocí	pomocí	k7c2	pomocí
zařízení	zařízení	k1gNnSc2	zařízení
zvaného	zvaný	k2eAgMnSc4d1	zvaný
NerveGear	NerveGear	k1gMnSc1	NerveGear
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
helmu	helma	k1gFnSc4	helma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
nervové	nervový	k2eAgInPc4d1	nervový
podněty	podnět	k1gInPc4	podnět
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
hráče	hráč	k1gMnSc2	hráč
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
ovládá	ovládat	k5eAaImIp3nS	ovládat
postavu	postava	k1gFnSc4	postava
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2022	[number]	k4	2022
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
připojí	připojit	k5eAaPmIp3nP	připojit
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
tvůrce	tvůrce	k1gMnSc2	tvůrce
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc2	Art
Online	Onlin	k1gInSc5	Onlin
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
odhlásit	odhlásit	k5eAaPmF	odhlásit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
jen	jen	k6eAd1	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zvládnou	zvládnout	k5eAaPmIp3nP	zvládnout
projít	projít	k5eAaPmF	projít
všech	všecek	k3xTgInPc2	všecek
100	[number]	k4	100
pater	patro	k1gNnPc2	patro
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
čeká	čekat	k5eAaImIp3nS	čekat
finální	finální	k2eAgMnSc1d1	finální
boss	boss	k1gMnSc1	boss
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
jim	on	k3xPp3gMnPc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
zemřou	zemřít	k5eAaPmIp3nP	zemřít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
zemřou	zemřít	k5eAaPmIp3nP	zemřít
i	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
realitě	realita	k1gFnSc6	realita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
chlapec	chlapec	k1gMnSc1	chlapec
jménem	jméno	k1gNnSc7	jméno
Kirigaya	Kirigayus	k1gMnSc2	Kirigayus
Kazuto	Kazut	k2eAgNnSc1d1	Kazuto
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
veřejně	veřejně	k6eAd1	veřejně
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Kirito	Kirit	k2eAgNnSc4d1	Kirit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
betatester	betatester	k1gMnSc1	betatester
a	a	k8xC	a
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
hráč	hráč	k1gMnSc1	hráč
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
ukončit	ukončit	k5eAaPmF	ukončit
hru	hra	k1gFnSc4	hra
pro	pro	k7c4	pro
dobro	dobro	k1gNnSc4	dobro
všech	všecek	k3xTgInPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
hra	hra	k1gFnSc1	hra
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
jménem	jméno	k1gNnSc7	jméno
Asuna	Asuna	k1gFnSc1	Asuna
Yukki	Yukki	k1gNnSc2	Yukki
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
také	také	k9	také
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
společně	společně	k6eAd1	společně
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jakým	jaký	k3yIgInSc7	jaký
účtem	účet	k1gInSc7	účet
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
tvůrce	tvůrce	k1gMnSc1	tvůrce
SAO	SAO	kA	SAO
<g/>
,	,	kIx,	,
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
porazit	porazit	k5eAaPmF	porazit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
Kiritovi	Kirita	k1gMnSc3	Kirita
také	také	k9	také
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
Kirito	Kirit	k2eAgNnSc1d1	Kirit
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Asuna	Asuna	k1gFnSc1	Asuna
a	a	k8xC	a
část	část	k1gFnSc1	část
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
virtuálním	virtuální	k2eAgInSc6d1	virtuální
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Alfheim	Alfheim	k1gInSc1	Alfheim
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
ALO	alo	k0wR	alo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převzala	převzít	k5eAaPmAgFnS	převzít
servery	server	k1gInPc4	server
SAO	SAO	kA	SAO
po	po	k7c6	po
zkrachovalé	zkrachovalý	k2eAgFnSc6d1	zkrachovalá
firmě	firma	k1gFnSc6	firma
tvůrců	tvůrce	k1gMnPc2	tvůrce
SAO	SAO	kA	SAO
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c2	za
svědomí	svědomí	k1gNnSc2	svědomí
programátor	programátor	k1gMnSc1	programátor
Nobujuki	Nobujuk	k1gFnSc2	Nobujuk
Sugó	Sugó	k1gMnSc1	Sugó
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
hráčích	hráč	k1gMnPc6	hráč
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
nelegální	legální	k2eNgInPc1d1	nelegální
experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Asuny	Asuna	k1gFnSc2	Asuna
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vzít	vzít	k5eAaPmF	vzít
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
převzít	převzít	k5eAaPmF	převzít
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ji	on	k3xPp3gFnSc4	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Asunin	Asunin	k2eAgMnSc1d1	Asunin
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Leafy	Leaf	k1gInPc1	Leaf
(	(	kIx(	(
<g/>
hráčky	hráčka	k1gFnPc1	hráčka
z	z	k7c2	z
ALO	ala	k1gFnSc5	ala
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgNnSc4	který
Kirito	Kirit	k2eAgNnSc4d1	Kirit
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
Asunu	Asuna	k1gFnSc4	Asuna
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ději	děj	k1gInSc6	děj
ALO	ala	k1gFnSc5	ala
se	se	k3xPyFc4	se
Kirito	Kirit	k2eAgNnSc1d1	Kirit
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
VRMMORPG	VRMMORPG	kA	VRMMORPG
jménem	jméno	k1gNnSc7	jméno
Gun	Gun	k1gMnSc5	Gun
Gale	Gal	k1gMnSc5	Gal
Online	Onlin	k1gMnSc5	Onlin
(	(	kIx(	(
<g/>
GGO	GGO	kA	GGO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
hráči	hráč	k1gMnPc1	hráč
umírají	umírat	k5eAaImIp3nP	umírat
i	i	k9	i
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
je	on	k3xPp3gMnPc4	on
někdo	někdo	k3yInSc1	někdo
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
určitou	určitý	k2eAgFnSc7d1	určitá
pistolí	pistol	k1gFnSc7	pistol
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pachatelé	pachatel	k1gMnPc1	pachatel
jsou	být	k5eAaImIp3nP	být
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
gildy	gilda	k1gFnSc2	gilda
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
SAO	SAO	kA	SAO
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tam	tam	k6eAd1	tam
vraždila	vraždit	k5eAaImAgFnS	vraždit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
odhalí	odhalit	k5eAaPmIp3nS	odhalit
pachatele	pachatel	k1gMnSc4	pachatel
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
GGO	GGO	kA	GGO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Kirito	Kirit	k2eAgNnSc1d1	Kirit
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
nejmodernější	moderní	k2eAgFnSc2d3	nejmodernější
hry	hra	k1gFnSc2	hra
jménem	jméno	k1gNnSc7	jméno
UnderWorld	UnderWorlda	k1gFnPc2	UnderWorlda
(	(	kIx(	(
<g/>
UW	UW	kA	UW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
rozhraní	rozhraní	k1gNnSc1	rozhraní
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
realističtější	realistický	k2eAgInSc1d2	realističtější
a	a	k8xC	a
komplexnější	komplexní	k2eAgInSc1d2	komplexnější
než	než	k8xS	než
u	u	k7c2	u
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
doposud	doposud	k6eAd1	doposud
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
utíkat	utíkat	k5eAaImF	utíkat
čas	čas	k1gInSc4	čas
tisíckrát	tisíckrát	k6eAd1	tisíckrát
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
Kirito	Kirit	k2eAgNnSc1d1	Kirit
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
pasti	past	k1gFnSc2	past
a	a	k8xC	a
probudí	probudit	k5eAaPmIp3nS	probudit
se	se	k3xPyFc4	se
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
odhlášení	odhlášení	k1gNnSc2	odhlášení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
série	série	k1gFnSc2	série
light	light	k1gMnSc1	light
novel	novela	k1gFnPc2	novela
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
napsal	napsat	k5eAaPmAgInS	napsat
Reki	Rek	k1gFnSc2	Rek
Kawahara	Kawahar	k1gMnSc2	Kawahar
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
literární	literární	k2eAgFnPc1d1	literární
soutěže	soutěž	k1gFnPc1	soutěž
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
ASCII	ascii	kA	ascii
Media	medium	k1gNnSc2	medium
Works	Works	kA	Works
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dengeki	Dengeki	k1gNnSc2	Dengeki
Game	game	k1gInSc1	game
šósecu	šósecus	k1gInSc2	šósecus
taišó	taišó	k?	taišó
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
překročení	překročení	k1gNnSc4	překročení
daného	daný	k2eAgInSc2d1	daný
rozsahu	rozsah	k1gInSc2	rozsah
stran	strana	k1gFnPc2	strana
se	se	k3xPyFc4	se
soutěže	soutěž	k1gFnPc1	soutěž
nakonec	nakonec	k6eAd1	nakonec
neúčastnil	účastnit	k5eNaImAgInS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
však	však	k9	však
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
publikovat	publikovat	k5eAaBmF	publikovat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Fumio	Fumio	k1gNnSc1	Fumio
Kunori	Kunor	k1gFnSc2	Kunor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
publikoval	publikovat	k5eAaBmAgMnS	publikovat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc4	čtyři
díly	díl	k1gInPc4	díl
série	série	k1gFnSc1	série
a	a	k8xC	a
několik	několik	k4yIc1	několik
kratších	krátký	k2eAgInPc2d2	kratší
příběhů	příběh	k1gInPc2	příběh
ze	z	k7c2	z
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc5	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
stejné	stejný	k2eAgFnPc4d1	stejná
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Accel	Accel	k1gMnSc1	Accel
World	World	k1gMnSc1	World
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
Kawahara	Kawahara	k1gFnSc1	Kawahara
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vydávat	vydávat	k5eAaImF	vydávat
nejen	nejen	k6eAd1	nejen
novely	novela	k1gFnSc2	novela
ze	z	k7c2	z
série	série	k1gFnSc2	série
Accel	Accel	k1gMnSc1	Accel
World	World	k1gMnSc1	World
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
Kawaharu	Kawahara	k1gFnSc4	Kawahara
oslovilo	oslovit	k5eAaPmAgNnS	oslovit
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
vydávat	vydávat	k5eAaPmF	vydávat
i	i	k9	i
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
publikovaný	publikovaný	k2eAgInSc4d1	publikovaný
materiál	materiál	k1gInSc4	materiál
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
internetové	internetový	k2eAgFnSc2d1	internetová
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Aincrad	Aincrad	k1gInSc1	Aincrad
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
verze	verze	k1gFnSc2	verze
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vydavatelem	vydavatel	k1gMnSc7	vydavatel
vyškrtnuta	vyškrtnut	k2eAgFnSc1d1	vyškrtnuta
kapitola	kapitola	k1gFnSc1	kapitola
16.5	[number]	k4	16.5
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
erotického	erotický	k2eAgInSc2d1	erotický
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc5	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
:	:	kIx,	:
Progressive	Progressiev	k1gFnSc2	Progressiev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
převyprávěním	převyprávění	k1gNnSc7	převyprávění
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
původní	původní	k2eAgFnSc2d1	původní
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc5	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
produkovala	produkovat	k5eAaImAgFnS	produkovat
společnost	společnost	k1gFnSc4	společnost
Aniplex	Aniplex	k1gInSc1	Aniplex
a	a	k8xC	a
pod	pod	k7c7	pod
režijní	režijní	k2eAgFnSc7d1	režijní
taktovkou	taktovka	k1gFnSc7	taktovka
Tomohika	Tomohikum	k1gNnSc2	Tomohikum
Itóa	Itó	k1gInSc2	Itó
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
studio	studio	k1gNnSc1	studio
A-1	A-1	k1gFnSc2	A-1
Pictures	Picturesa	k1gFnPc2	Picturesa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
děj	děj	k1gInSc4	děj
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
dílů	díl	k1gInPc2	díl
light	lighta	k1gFnPc2	lighta
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k8xC	i
osmého	osmý	k4xOgInSc2	osmý
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vysílala	vysílat	k5eAaImAgFnS	vysílat
na	na	k7c6	na
japonských	japonský	k2eAgFnPc6d1	japonská
televizních	televizní	k2eAgFnPc6d1	televizní
stanicích	stanice	k1gFnPc6	stanice
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
odvysílán	odvysílán	k2eAgInSc1d1	odvysílán
hodinový	hodinový	k2eAgInSc1d1	hodinový
silvestrovský	silvestrovský	k2eAgInSc1d1	silvestrovský
speciál	speciál	k1gInSc1	speciál
s	s	k7c7	s
názvem	název	k1gInSc7	název
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
Extra	extra	k2eAgFnSc7d1	extra
Edition	Edition	k1gInSc4	Edition
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
rekapitulaci	rekapitulace	k1gFnSc4	rekapitulace
děje	děj	k1gInSc2	děj
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
s	s	k7c7	s
několika	několik	k4yIc2	několik
přidanými	přidaný	k2eAgFnPc7d1	přidaná
scénami	scéna	k1gFnPc7	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
speciálu	speciál	k1gInSc2	speciál
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
potvrzení	potvrzení	k1gNnSc1	potvrzení
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
odbyla	odbýt	k5eAaPmAgFnS	odbýt
premiéru	premiéra	k1gFnSc4	premiéra
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dílů	díl	k1gInPc2	díl
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
adaptuje	adaptovat	k5eAaBmIp3nS	adaptovat
děj	děj	k1gInSc4	děj
pátého	pátý	k4xOgNnSc2	pátý
a	a	k8xC	a
šestého	šestý	k4xOgInSc2	šestý
dílu	díl	k1gInSc2	díl
light	light	k1gMnSc1	light
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
díly	díl	k1gInPc1	díl
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
adaptují	adaptovat	k5eAaBmIp3nP	adaptovat
děj	děj	k1gInSc1	děj
osmého	osmý	k4xOgInSc2	osmý
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
díly	díl	k1gInPc1	díl
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
děj	děj	k1gInSc1	děj
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
oběma	dva	k4xCgFnPc3	dva
řadám	řada	k1gFnPc3	řada
seriálu	seriál	k1gInSc2	seriál
složila	složit	k5eAaPmAgFnS	složit
Juki	Juke	k1gFnSc4	Juke
Kadžiura	Kadžiura	k1gFnSc1	Kadžiura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
také	také	k9	také
jednou	jednou	k6eAd1	jednou
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
úvodní	úvodní	k2eAgFnSc1d1	úvodní
i	i	k8xC	i
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
znělka	znělka	k1gFnSc1	znělka
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dílů	díl	k1gInPc2	díl
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc7d1	úvodní
znělkou	znělka	k1gFnSc7	znělka
skladba	skladba	k1gFnSc1	skladba
crossing	crossing	k1gInSc4	crossing
field	field	k6eAd1	field
od	od	k7c2	od
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
LiSA	Lisa	k1gFnSc1	Lisa
a	a	k8xC	a
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
znělkou	znělka	k1gFnSc7	znělka
skladba	skladba	k1gFnSc1	skladba
Jume	Jume	k1gFnSc1	Jume
sekai	seka	k1gFnSc2	seka
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ユ	ユ	k?	ユ
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Svět	svět	k1gInSc1	svět
snů	sen	k1gInPc2	sen
<g/>
)	)	kIx)	)
od	od	k7c2	od
Haruky	Haruk	k1gInPc7	Haruk
Tomacu	Tomacus	k1gInSc2	Tomacus
<g/>
,	,	kIx,	,
dabérky	dabérka	k1gFnSc2	dabérka
Asuny	Asuna	k1gFnSc2	Asuna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
patnáctého	patnáctý	k4xOgInSc2	patnáctý
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc7d1	úvodní
znělkou	znělka	k1gFnSc7	znělka
skladba	skladba	k1gFnSc1	skladba
INNOCENCE	INNOCENCE	kA	INNOCENCE
od	od	k7c2	od
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Eir	Eir	k1gMnSc2	Eir
Aoi	Aoi	k1gMnSc2	Aoi
a	a	k8xC	a
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
znělkou	znělka	k1gFnSc7	znělka
skladba	skladba	k1gFnSc1	skladba
Overfly	Overfly	k1gFnSc1	Overfly
od	od	k7c2	od
Luny	luna	k1gFnSc2	luna
Haruny	Haruna	k1gFnSc2	Haruna
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dílů	díl	k1gInPc2	díl
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc7d1	úvodní
znělkou	znělka	k1gFnSc7	znělka
skladba	skladba	k1gFnSc1	skladba
IGNITE	IGNITE	kA	IGNITE
od	od	k7c2	od
Eir	Eir	k1gFnSc2	Eir
Aoi	Aoi	k1gFnSc2	Aoi
<g/>
,	,	kIx,	,
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
až	až	k6eAd1	až
třiadvacátého	třiadvacátý	k4xOgInSc2	třiadvacátý
dílů	díl	k1gInPc2	díl
pak	pak	k6eAd1	pak
Courage	Courage	k1gNnPc1	Courage
od	od	k7c2	od
Haruky	Haruk	k1gMnPc7	Haruk
Tomacu	Tomacus	k1gInSc2	Tomacus
a	a	k8xC	a
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
pak	pak	k6eAd1	pak
Separate	Separat	k1gInSc5	Separat
Ways	Ways	k1gInSc4	Ways
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
セ	セ	k?	セ
<g/>
・	・	k?	・
<g/>
ウ	ウ	k?	ウ
<g/>
)	)	kIx)	)
od	od	k7c2	od
téže	týž	k3xTgFnSc2	týž
interpretky	interpretka	k1gFnSc2	interpretka
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
znělkou	znělka	k1gFnSc7	znělka
prvních	první	k4xOgFnPc2	první
čtrnácti	čtrnáct	k4xCc2	čtrnáct
dílů	díl	k1gInPc2	díl
je	být	k5eAaImIp3nS	být
Startear	Startear	k1gInSc1	Startear
od	od	k7c2	od
Luny	luna	k1gFnSc2	luna
Haruny	Haruna	k1gFnSc2	Haruna
<g/>
,	,	kIx,	,
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
až	až	k6eAd1	až
sedmnáctého	sedmnáctý	k4xOgInSc2	sedmnáctý
dílu	díl	k1gInSc2	díl
pak	pak	k6eAd1	pak
No	no	k9	no
More	mor	k1gInSc5	mor
Time	Timus	k1gInSc5	Timus
Machine	Machin	k1gInSc5	Machin
od	od	k7c2	od
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
LiSA	Lisa	k1gFnSc1	Lisa
a	a	k8xC	a
pro	pro	k7c4	pro
zbylé	zbylý	k2eAgInPc4d1	zbylý
díly	díl	k1gInPc4	díl
pak	pak	k6eAd1	pak
Širuši	Širuch	k1gMnPc1	Širuch
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
シ	シ	k?	シ
<g/>
)	)	kIx)	)
od	od	k7c2	od
téže	týž	k3xTgFnSc2	týž
interpretky	interpretka	k1gFnSc2	interpretka
<g/>
.	.	kIx.	.
</s>
