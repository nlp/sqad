<s>
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
je	být	k5eAaImIp3nS	být
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bývalý	bývalý	k2eAgInSc4d1	bývalý
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
s	s	k7c7	s
mincovnou	mincovna	k1gFnSc7	mincovna
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
<g/>
.	.	kIx.	.
</s>
<s>
Razily	razit	k5eAaImAgFnP	razit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
mince	mince	k1gFnSc2	mince
-	-	kIx~	-
pražské	pražský	k2eAgInPc1d1	pražský
groše	groš	k1gInPc1	groš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
také	také	k9	také
jako	jako	k9	jako
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Z	z	k7c2	z
nevelkého	velký	k2eNgNnSc2d1	nevelké
nádvoří	nádvoří	k1gNnSc2	nádvoří
jsou	být	k5eAaImIp3nP	být
vstupy	vstup	k1gInPc4	vstup
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
dílen	dílna	k1gFnPc2	dílna
(	(	kIx(	(
<g/>
šmiten	šmitna	k1gFnPc2	šmitna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
šmitna	šmitna	k1gFnSc1	šmitna
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
znaky	znak	k1gInPc4	znak
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
<g/>
,	,	kIx,	,
Kladska	Kladsko	k1gNnSc2	Kladsko
<g/>
,	,	kIx,	,
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Opavy	Opava	k1gFnSc2	Opava
<g/>
,	,	kIx,	,
Mostu	most	k1gInSc2	most
a	a	k8xC	a
Písku	Písek	k1gInSc2	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc1	palác
s	s	k7c7	s
komnatami	komnata	k1gFnPc7	komnata
<g/>
,	,	kIx,	,
s	s	k7c7	s
honosnou	honosný	k2eAgFnSc7d1	honosná
domácí	domácí	k2eAgFnSc7d1	domácí
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
pokladnice	pokladnice	k1gFnSc1	pokladnice
s	s	k7c7	s
okovanými	okovaný	k2eAgFnPc7d1	okovaná
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
tesanou	tesaný	k2eAgFnSc4d1	tesaná
latinskou	latinský	k2eAgFnSc4d1	Latinská
výstrahu	výstraha	k1gFnSc4	výstraha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
překlad	překlad	k1gInSc1	překlad
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
Nedotýkej	dotýkat	k5eNaImRp2nS	dotýkat
se	se	k3xPyFc4	se
mne	já	k3xPp1nSc2	já
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
V	v	k7c6	v
zasedací	zasedací	k2eAgFnSc6d1	zasedací
místnosti	místnost	k1gFnSc6	místnost
jsou	být	k5eAaImIp3nP	být
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
<g/>
:	:	kIx,	:
Dekret	dekret	k1gInSc1	dekret
Kutnohorský	kutnohorský	k2eAgInSc1d1	kutnohorský
a	a	k8xC	a
Volba	volba	k1gFnSc1	volba
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
od	od	k7c2	od
malířů	malíř	k1gMnPc2	malíř
Klusáčka	klusáček	k1gMnSc2	klusáček
a	a	k8xC	a
bratří	bratr	k1gMnPc2	bratr
Špillarových	Špillarův	k2eAgMnPc2d1	Špillarův
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
konšelská	konšelský	k2eAgFnSc1d1	Konšelská
lavice	lavice	k1gFnSc1	lavice
a	a	k8xC	a
bronzová	bronzový	k2eAgFnSc1d1	bronzová
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
DURDÍK	DURDÍK	kA	DURDÍK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českých	český	k2eAgInPc2d1	český
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
736	[number]	k4	736
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc4	heslo
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
s.	s.	k?	s.
601	[number]	k4	601
<g/>
-	-	kIx~	-
<g/>
602	[number]	k4	602
<g/>
.	.	kIx.	.
</s>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
zámky	zámek	k1gInPc1	zámek
a	a	k8xC	a
tvrze	tvrz	k1gFnPc1	tvrz
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
:	:	kIx,	:
Čáslavsko	Čáslavsko	k1gNnSc1	Čáslavsko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čížek	Čížek	k1gMnSc1	Čížek
-	-	kIx~	-
ViGo	ViGo	k1gMnSc1	ViGo
agency	agenca	k1gFnSc2	agenca
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
331	[number]	k4	331
s.	s.	k?	s.
Kapitola	kapitola	k1gFnSc1	kapitola
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
,	,	kIx,	,
s.	s.	k?	s.
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
259	[number]	k4	259
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlašský	vlašský	k2eAgInSc4d1	vlašský
dvůr	dvůr	k1gInSc4	dvůr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
-	-	kIx~	-
královská	královský	k2eAgFnSc1d1	královská
mincovna	mincovna	k1gFnSc1	mincovna
Informace	informace	k1gFnSc1	informace
o	o	k7c6	o
Vlašském	vlašský	k2eAgInSc6d1	vlašský
dvoře	dvůr	k1gInSc6	dvůr
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Průvodcovské	průvodcovský	k2eAgFnSc2d1	průvodcovská
služby	služba	k1gFnSc2	služba
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
