<s>
Chopinova	Chopinův	k2eAgFnSc1d1	Chopinova
klavírní	klavírní	k2eAgFnSc1d1	klavírní
hudba	hudba	k1gFnSc1	hudba
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
unikátní	unikátní	k2eAgInSc4d1	unikátní
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
rytmus	rytmus	k1gInSc4	rytmus
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
využívá	využívat	k5eAaImIp3nS	využívat
rubato	rubato	k6eAd1	rubato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
užití	užití	k1gNnSc1	užití
chromatiky	chromatika	k1gFnSc2	chromatika
a	a	k8xC	a
kontrapunktu	kontrapunkt	k1gInSc2	kontrapunkt
<g/>
.	.	kIx.	.
</s>
