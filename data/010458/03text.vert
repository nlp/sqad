<p>
<s>
Micpa	Micpa	k1gFnSc1	Micpa
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
<g/>
ִ	ִ	k?	ִ
<g/>
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
פ	פ	k?	פ
<g/>
ָ	ָ	k?	ָ
<g/>
ּ	ּ	k?	ּ
<g/>
ה	ה	k?	ה
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Mitzpa	Mitzpa	k1gFnSc1	Mitzpa
<g/>
,	,	kIx,	,
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
seznamu	seznam	k1gInSc6	seznam
sídel	sídlo	k1gNnPc2	sídlo
Mizpa	Mizpa	k1gFnSc1	Mizpa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
typu	typ	k1gInSc2	typ
mošava	mošava	k1gFnSc1	mošava
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severním	severní	k2eAgInSc6d1	severní
distriktu	distrikt	k1gInSc6	distrikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Oblastní	oblastní	k2eAgFnSc6d1	oblastní
radě	rada	k1gFnSc6	rada
Dolní	dolní	k2eAgFnSc1d1	dolní
Galilea	Galilea	k1gFnSc1	Galilea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
76	[number]	k4	76
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Galileji	Galilea	k1gFnSc6	Galilea
<g/>
,	,	kIx,	,
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
cca	cca	kA	cca
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Galilejského	galilejský	k2eAgNnSc2d1	Galilejské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zastavěného	zastavěný	k2eAgNnSc2d1	zastavěné
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
Tiberias	Tiberias	k1gMnSc1	Tiberias
<g/>
,	,	kIx,	,
jen	jen	k9	jen
cca	cca	kA	cca
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
cca	cca	kA	cca
105	[number]	k4	105
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
a	a	k8xC	a
cca	cca	kA	cca
48	[number]	k4	48
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Haify	Haifa	k1gFnSc2	Haifa
<g/>
.	.	kIx.	.
</s>
<s>
Micpu	Micpa	k1gFnSc4	Micpa
obývají	obývat	k5eAaImIp3nP	obývat
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
židovské	židovský	k2eAgNnSc1d1	Židovské
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
Chamam	Chamam	k1gInSc1	Chamam
cca	cca	kA	cca
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obývají	obývat	k5eAaImIp3nP	obývat
izraelští	izraelský	k2eAgMnPc1d1	izraelský
Arabové	Arab	k1gMnPc1	Arab
respektive	respektive	k9	respektive
arabští	arabský	k2eAgMnPc1d1	arabský
Beduíni	Beduín	k1gMnPc1	Beduín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Micpa	Micpa	k1gFnSc1	Micpa
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
napojena	napojen	k2eAgMnSc4d1	napojen
pomocí	pomocí	k7c2	pomocí
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
77	[number]	k4	77
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spojuje	spojovat	k5eAaImIp3nS	spojovat
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
Haify	Haifa	k1gFnSc2	Haifa
s	s	k7c7	s
Tiberiasem	Tiberias	k1gInSc7	Tiberias
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Micpa	Micpa	k1gFnSc1	Micpa
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
zakladateli	zakladatel	k1gMnPc7	zakladatel
byli	být	k5eAaImAgMnP	být
židovští	židovský	k2eAgMnPc1d1	židovský
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
turecké	turecký	k2eAgFnSc2d1	turecká
Palestiny	Palestina	k1gFnSc2	Palestina
dorazili	dorazit	k5eAaPmAgMnP	dorazit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhé	druhý	k4xOgFnSc2	druhý
alije	alij	k1gFnSc2	alij
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
do	do	k7c2	do
židovského	židovský	k2eAgNnSc2d1	Židovské
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
nabyla	nabýt	k5eAaPmAgFnS	nabýt
Jewish	Jewish	k1gInSc4	Jewish
Colonization	Colonization	k1gInSc1	Colonization
Association	Association	k1gInSc1	Association
<g/>
.	.	kIx.	.
</s>
<s>
Zástavba	zástavba	k1gFnSc1	zástavba
sestávala	sestávat	k5eAaImAgFnS	sestávat
v	v	k7c6	v
malých	malá	k1gFnPc6	malá
domů	dům	k1gInPc2	dům
z	z	k7c2	z
tmavého	tmavý	k2eAgInSc2d1	tmavý
bazaltového	bazaltový	k2eAgInSc2d1	bazaltový
kamene	kámen	k1gInSc2	kámen
obehnaných	obehnaný	k2eAgFnPc2d1	obehnaná
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgMnPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInSc1d1	zdejší
sladkovodní	sladkovodní	k2eAgInSc1d1	sladkovodní
pramen	pramen	k1gInSc1	pramen
byl	být	k5eAaImAgInS	být
nepostačující	postačující	k2eNgMnSc1d1	nepostačující
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
pro	pro	k7c4	pro
10	[number]	k4	10
rodinných	rodinný	k2eAgFnPc2d1	rodinná
farem	farma	k1gFnPc2	farma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
měla	mít	k5eAaImAgFnS	mít
přidělenou	přidělený	k2eAgFnSc4d1	přidělená
výměru	výměra	k1gFnSc4	výměra
pozemků	pozemek	k1gInPc2	pozemek
přes	přes	k7c4	přes
200	[number]	k4	200
dunamů	dunam	k1gInPc2	dunam
(	(	kIx(	(
<g/>
20	[number]	k4	20
hektarů	hektar	k1gInPc2	hektar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
malá	malý	k2eAgFnSc1d1	malá
osada	osada	k1gFnSc1	osada
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
měla	mít	k5eAaImAgFnS	mít
Micpa	Micpa	k1gFnSc1	Micpa
jen	jen	k9	jen
75	[number]	k4	75
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozlohu	rozloha	k1gFnSc4	rozloha
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
4802	[number]	k4	4802
dunamů	dunam	k1gInPc2	dunam
(	(	kIx(	(
<g/>
4,802	[number]	k4	4,802
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
získala	získat	k5eAaPmAgFnS	získat
vesnice	vesnice	k1gFnSc1	vesnice
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
vodovodní	vodovodní	k2eAgFnSc4d1	vodovodní
síť	síť	k1gFnSc4	síť
společnosti	společnost	k1gFnSc2	společnost
Mekorot	Mekorota	k1gFnPc2	Mekorota
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
,	,	kIx,	,
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
turistických	turistický	k2eAgFnPc6d1	turistická
službách	služba	k1gFnPc6	služba
nebo	nebo	k8xC	nebo
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
mimo	mimo	k7c4	mimo
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
brání	bránit	k5eAaImIp3nS	bránit
výraznější	výrazný	k2eAgFnSc1d2	výraznější
stavební	stavební	k2eAgFnSc3d1	stavební
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
v	v	k7c4	v
Micpa	Micp	k1gMnSc4	Micp
je	být	k5eAaImIp3nS	být
sekulární	sekulární	k2eAgInSc1d1	sekulární
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
tvořili	tvořit	k5eAaImAgMnP	tvořit
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Micpa	Micp	k1gMnSc2	Micp
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
statistické	statistický	k2eAgFnSc2d1	statistická
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
nearabské	arabský	k2eNgMnPc4d1	nearabský
obyvatele	obyvatel	k1gMnPc4	obyvatel
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
ale	ale	k8xC	ale
bez	bez	k7c2	bez
formální	formální	k2eAgFnSc2d1	formální
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
židovskému	židovský	k2eAgNnSc3d1	Židovské
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
menší	malý	k2eAgNnSc4d2	menší
sídlo	sídlo	k1gNnSc4	sídlo
vesnického	vesnický	k2eAgInSc2d1	vesnický
typu	typ	k1gInSc2	typ
s	s	k7c7	s
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stagnující	stagnující	k2eAgFnSc7d1	stagnující
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
142	[number]	k4	142
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
populace	populace	k1gFnSc1	populace
klesla	klesnout	k5eAaPmAgFnS	klesnout
o	o	k7c4	o
12,9	[number]	k4	12,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
mošava	mošava	k1gFnSc1	mošava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
profil	profil	k1gInSc1	profil
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Bet	Bet	k1gMnSc1	Bet
Alon	Alon	k1gMnSc1	Alon
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
profil	profil	k1gInSc1	profil
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Galil	Galila	k1gFnPc2	Galila
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
profil	profil	k1gInSc1	profil
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Rom	Rom	k1gMnSc1	Rom
Galil	Galil	k1gMnSc1	Galil
</s>
</p>
