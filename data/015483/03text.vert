<s>
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgMnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgMnSc1d1
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgMnSc1d1
Narození	narození	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1889	#num#	k4
ZeměchyRakousko-Uhersko	ZeměchyRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Velvary	Velvar	k1gInPc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Povolání	povolání	k1gNnSc4
</s>
<s>
fotograf	fotograf	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Italská	italský	k2eAgFnSc1d1
houfnice	houfnice	k1gFnSc1
Obice	Obice	k1gFnSc2
305	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
ukořistěná	ukořistěný	k2eAgFnSc1d1
rakouskými	rakouský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
blízko	blízko	k6eAd1
Udine	Udin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foto	foto	k1gNnSc1
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgInSc5d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1889	#num#	k4
Zeměchy	Zeměcha	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1949	#num#	k4
Velvary	Velvar	k1gInPc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
válečný	válečný	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
nadporučíkem	nadporučík	k1gMnSc7
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
plukovní	plukovní	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
47	#num#	k4
<g/>
.	.	kIx.
pěšího	pěší	k2eAgInSc2d1
regimentu	regiment	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
válce	válka	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
stavař	stavař	k1gMnSc1
ve	v	k7c6
Velvarech	Velvar	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
stavební	stavební	k2eAgFnSc4d1
průmyslovou	průmyslový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
povinnou	povinný	k2eAgFnSc4d1
vojnu	vojna	k1gFnSc4
narukoval	narukovat	k5eAaPmAgMnS
jako	jako	k9
kreslič	kreslič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
vypukla	vypuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přidělen	přidělit	k5eAaPmNgInS
do	do	k7c2
Grazu	Graz	k1gInSc2
k	k	k7c3
47	#num#	k4
<g/>
.	.	kIx.
pěšímu	pěší	k2eAgInSc3d1
regimentu	regiment	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Haliče	Halič	k1gFnSc2
i	i	k8xC
italských	italský	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spřátelil	spřátelit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
plukovním	plukovní	k2eAgMnSc7d1
historikem	historik	k1gMnSc7
baronem	baron	k1gMnSc7
Ludwigem	Ludwig	k1gMnSc7
von	von	k1gInSc4
Vogelsangem	Vogelsang	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
později	pozdě	k6eAd2
sepsal	sepsat	k5eAaPmAgMnS
historii	historie	k1gFnSc4
regimentu	regiment	k1gInSc2
a	a	k8xC
použil	použít	k5eAaPmAgMnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
mnoho	mnoho	k4c1
fotografií	fotografia	k1gFnPc2
a	a	k8xC
akvarelů	akvarel	k1gInPc2
Bišického	Bišický	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
jako	jako	k8xS,k8xC
stavitel	stavitel	k1gMnSc1
rodinných	rodinný	k2eAgInPc2d1
domů	dům	k1gInPc2
ve	v	k7c6
Velvarech	Velvar	k1gInPc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
místní	místní	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Spořilov	Spořilov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografování	fotografování	k1gNnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k6eAd1
koníčkem	koníček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
60	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Znovuobjevení	znovuobjevení	k1gNnSc1
Bišického	Bišický	k2eAgInSc2d1
</s>
<s>
Od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
roku	rok	k1gInSc2
2009	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
konala	konat	k5eAaImAgFnS
výstava	výstava	k1gFnSc1
s	s	k7c7
názvem	název	k1gInSc7
Pěšky	pěšky	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objektivem	objektiv	k1gInSc7
neznámého	známý	k2eNgMnSc4d1
vojáka	voják	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představovala	představovat	k5eAaImAgFnS
zejména	zejména	k9
snímky	snímek	k1gInPc4
z	z	k7c2
italské	italský	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
z	z	k7c2
východní	východní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
v	v	k7c6
Haliči	Halič	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
výstavu	výstav	k1gInSc6
se	se	k3xPyFc4
přišel	přijít	k5eAaPmAgMnS
podívat	podívat	k5eAaPmF,k5eAaImF
historik	historik	k1gMnSc1
Michal	Michal	k1gMnSc1
Rybák	Rybák	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ve	v	k7c6
vystavených	vystavený	k2eAgInPc6d1
exponátech	exponát	k1gInPc6
poznal	poznat	k5eAaPmAgMnS
dílo	dílo	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
dědečka	dědeček	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
Bišického	Bišický	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Jaroslavem	Jaroslav	k1gMnSc7
Kučerou	Kučera	k1gMnSc7
pak	pak	k6eAd1
výstavu	výstava	k1gFnSc4
doplnili	doplnit	k5eAaPmAgMnP
a	a	k8xC
opravili	opravit	k5eAaPmAgMnP
titulky	titulek	k1gInPc4
k	k	k7c3
fotografiím	fotografia	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Rybák	Rybák	k1gMnSc1
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
půjčil	půjčit	k5eAaPmAgMnS
dědečkovy	dědečkův	k2eAgInPc4d1
negativy	negativ	k1gInPc4
spolužákovi	spolužák	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
sliboval	slibovat	k5eAaImAgMnS
uspořádání	uspořádání	k1gNnSc1
výstavy	výstava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
však	však	k9
vyloučen	vyloučit	k5eAaPmNgInS
ze	z	k7c2
školy	škola	k1gFnSc2
a	a	k8xC
zmizel	zmizet	k5eAaPmAgMnS
i	i	k9
s	s	k7c7
negativy	negativ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rybák	Rybák	k1gMnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
dávno	dávno	k6eAd1
ztracené	ztracený	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
řadu	řada	k1gFnSc4
prostředníků	prostředník	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
dostaly	dostat	k5eAaPmAgInP
k	k	k7c3
Jaroslavu	Jaroslav	k1gMnSc3
Kučerovi	Kučera	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnosti	farnost	k1gFnSc2
Zeměchy	Zeměcha	k1gFnSc2
<g/>
↑	↑	k?
ŠÁLKOVÁ	Šálková	k1gFnSc1
<g/>
,	,	kIx,
Soňa	Soňa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
mají	mít	k5eAaImIp3nP
svého	svůj	k3xOyFgMnSc4
autora	autor	k1gMnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artmagazin	Artmagazin	k1gInSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
,	,	kIx,
2009-8-3	2009-8-3	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
GAZDÍK	GAZDÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhada	záhada	k1gFnSc1
rozluštěna	rozluštěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátní	unikátní	k2eAgFnPc4d1
fotky	fotka	k1gFnPc4
z	z	k7c2
války	válka	k1gFnSc2
mají	mít	k5eAaImIp3nP
autora	autor	k1gMnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jindřich	Jindřich	k1gMnSc1
Bišický	Bišický	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
101204	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6834	#num#	k4
9189	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
96946083	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
