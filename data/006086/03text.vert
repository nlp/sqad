<s>
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgInPc4	žádný
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
symbolem	symbol	k1gInSc7	symbol
přeškrtnuté	přeškrtnutý	k2eAgFnSc2d1	přeškrtnutá
nuly	nula	k1gFnSc2	nula
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∅	∅	k?	∅
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
emptyset	emptyseta	k1gFnPc2	emptyseta
}	}	kIx)	}
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
psané	psaný	k2eAgFnPc1d1	psaná
též	též	k9	též
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∅	∅	k?	∅
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varnothing	varnothing	k1gInSc4	varnothing
}	}	kIx)	}
,	,	kIx,	,
popř.	popř.	kA	popř.
∅	∅	k?	∅
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
symbolem	symbol	k1gInSc7	symbol
prázdných	prázdný	k2eAgFnPc2d1	prázdná
množinových	množinový	k2eAgFnPc2d1	množinová
závorek	závorka	k1gFnPc2	závorka
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
množina	množina	k1gFnSc1	množina
obsahující	obsahující	k2eAgInPc4d1	obsahující
nějaké	nějaký	k3yIgInPc4	nějaký
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
množina	množina	k1gFnSc1	množina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
používaném	používaný	k2eAgInSc6d1	používaný
axiomatickém	axiomatický	k2eAgInSc6d1	axiomatický
systému	systém	k1gInSc6	systém
Zermelově-Fraenkelově	Zermelově-Fraenkelův	k2eAgInSc6d1	Zermelově-Fraenkelův
teorii	teorie	k1gFnSc4	teorie
množin	množina	k1gFnPc2	množina
se	se	k3xPyFc4	se
existence	existence	k1gFnSc1	existence
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
ze	z	k7c2	z
schématu	schéma	k1gNnSc2	schéma
axiomů	axiom	k1gInPc2	axiom
vydělení	vydělení	k1gNnSc2	vydělení
a	a	k8xC	a
axiomu	axiom	k1gInSc2	axiom
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
množina	množina	k1gFnSc1	množina
<g/>
)	)	kIx)	)
formulí	formule	k1gFnPc2	formule
pro	pro	k7c4	pro
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
definujme	definovat	k5eAaBmRp1nP	definovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∅	∅	k?	∅
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
<g/>
=	=	kIx~	=
{	{	kIx(	{
y	y	k?	y
∈	∈	k?	∈
x	x	k?	x
;	;	kIx,	;
:	:	kIx,	:
y	y	k?	y
≠	≠	k?	≠
y	y	k?	y
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
emptyset	emptyset	k5eAaImF	emptyset
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
x	x	k?	x
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
axiomu	axiom	k1gInSc2	axiom
extenzionality	extenzionalita	k1gFnSc2	extenzionalita
pak	pak	k6eAd1	pak
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
libovolné	libovolný	k2eAgFnPc4d1	libovolná
dvě	dva	k4xCgFnPc4	dva
prázdné	prázdný	k2eAgFnPc4d1	prázdná
množiny	množina	k1gFnPc4	množina
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
.	.	kIx.	.
</s>
<s>
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
podmnožinou	podmnožina	k1gFnSc7	podmnožina
libovolné	libovolný	k2eAgFnSc2d1	libovolná
množiny	množina	k1gFnSc2	množina
<g/>
:	:	kIx,	:
∀	∀	k?	∀
A	A	kA	A
<g/>
:	:	kIx,	:
∅	∅	k?	∅
⊆	⊆	k?	⊆
A	a	k8xC	a
Libovolná	libovolný	k2eAgFnSc1d1	libovolná
množina	množina	k1gFnSc1	množina
se	s	k7c7	s
sjednocením	sjednocení	k1gNnSc7	sjednocení
s	s	k7c7	s
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
množinou	množina	k1gFnSc7	množina
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
:	:	kIx,	:
∀	∀	k?	∀
A	A	kA	A
<g/>
:	:	kIx,	:
∅	∅	k?	∅
∪	∪	k?	∪
A	A	kA	A
=	=	kIx~	=
A	a	k8xC	a
Průnik	průnik	k1gInSc1	průnik
libovolné	libovolný	k2eAgFnSc2d1	libovolná
množiny	množina	k1gFnSc2	množina
s	s	k7c7	s
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
množinou	množina	k1gFnSc7	množina
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
<g/>
:	:	kIx,	:
∀	∀	k?	∀
A	A	kA	A
<g/>
:	:	kIx,	:
∅	∅	k?	∅
<g />
.	.	kIx.	.
</s>
<s>
∩	∩	k?	∩
A	A	kA	A
=	=	kIx~	=
∅	∅	k?	∅
Kartézský	kartézský	k2eAgInSc4d1	kartézský
součin	součin	k1gInSc4	součin
libovolné	libovolný	k2eAgFnSc2d1	libovolná
množiny	množina	k1gFnSc2	množina
s	s	k7c7	s
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
množinou	množina	k1gFnSc7	množina
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
<g/>
:	:	kIx,	:
∀	∀	k?	∀
A	A	kA	A
<g/>
:	:	kIx,	:
∅	∅	k?	∅
×	×	k?	×
A	A	kA	A
=	=	kIx~	=
A	A	kA	A
×	×	k?	×
∅	∅	k?	∅
=	=	kIx~	=
∅	∅	k?	∅
Jedinou	jediný	k2eAgFnSc7d1	jediná
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
<g/>
)	)	kIx)	)
podmnožinou	podmnožina	k1gFnSc7	podmnožina
prázdné	prázdná	k1gFnSc2	prázdná
množiny	množina	k1gFnSc2	množina
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
<g/>
;	;	kIx,	;
žádné	žádný	k3yNgFnSc2	žádný
vlastní	vlastní	k2eAgFnSc2d1	vlastní
podmnožiny	podmnožina	k1gFnSc2	podmnožina
prázdná	prázdná	k1gFnSc1	prázdná
<g />
.	.	kIx.	.
</s>
<s>
množina	množina	k1gFnSc1	množina
nemá	mít	k5eNaImIp3nS	mít
<g/>
:	:	kIx,	:
∀	∀	k?	∀
A	a	k9	a
<g/>
:	:	kIx,	:
A	a	k9	a
⊆	⊆	k?	⊆
∅	∅	k?	∅
⇒	⇒	k?	⇒
A	A	kA	A
=	=	kIx~	=
∅	∅	k?	∅
Mohutnost	mohutnost	k1gFnSc4	mohutnost
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
:	:	kIx,	:
|	|	kIx~	|
<g/>
∅	∅	k?	∅
<g/>
|	|	kIx~	|
=	=	kIx~	=
0	[number]	k4	0
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
jako	jako	k8xC	jako
topologický	topologický	k2eAgInSc1d1	topologický
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
prvků	prvek	k1gInPc2	prvek
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
0	[number]	k4	0
<g/>
,	,	kIx,	,
součin	součin	k1gInSc1	součin
prvků	prvek	k1gInPc2	prvek
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
jako	jako	k8xS	jako
1	[number]	k4	1
<g/>
,	,	kIx,	,
supremum	supremum	k1gInSc1	supremum
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
a	a	k8xC	a
infimum	infimum	k1gInSc1	infimum
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
podmnožiny	podmnožina	k1gFnSc2	podmnožina
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
prvek	prvek	k1gInSc1	prvek
podmnožiny	podmnožina	k1gFnSc2	podmnožina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
prvkem	prvek	k1gInSc7	prvek
druhé	druhý	k4xOgFnSc2	druhý
množiny	množina	k1gFnSc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
kvantifikátor	kvantifikátor	k1gInSc1	kvantifikátor
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
platí	platit	k5eAaImIp3nP	platit
je	on	k3xPp3gFnPc4	on
u	u	k7c2	u
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
vždy	vždy	k6eAd1	vždy
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
elementárních	elementární	k2eAgFnPc2d1	elementární
pravidel	pravidlo	k1gNnPc2	pravidlo
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
např.	např.	kA	např.
A	A	kA	A
=	=	kIx~	=
{	{	kIx(	{
<g/>
∅	∅	k?	∅
<g/>
}	}	kIx)	}
není	být	k5eNaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
množina	množina	k1gFnSc1	množina
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
prvku	prvek	k1gInSc6	prvek
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
jeden	jeden	k4xCgInSc4	jeden
prvek	prvek	k1gInSc4	prvek
množiny	množina	k1gFnSc2	množina
A	a	k9	a
je	být	k5eAaImIp3nS	být
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
množinou	množina	k1gFnSc7	množina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikací	aplikace	k1gFnSc7	aplikace
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
je	být	k5eAaImIp3nS	být
množinové	množinový	k2eAgNnSc1d1	množinový
zavedení	zavedení	k1gNnSc1	zavedení
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
reprezentována	reprezentován	k2eAgFnSc1d1	reprezentována
∅	∅	k?	∅
<g/>
,	,	kIx,	,
1	[number]	k4	1
jako	jako	k8xS	jako
{	{	kIx(	{
<g/>
∅	∅	k?	∅
<g/>
}	}	kIx)	}
a	a	k8xC	a
n	n	k0	n
jako	jako	k9	jako
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ŠTĚPÁNEK	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
BALCAR	Balcar	k1gMnSc1	Balcar
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
470	[number]	k4	470
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
množina	množina	k1gFnSc1	množina
</s>
